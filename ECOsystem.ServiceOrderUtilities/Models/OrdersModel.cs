﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.ServiceOrderUtilities.Models
{
    public class OrdersModel
    {

        public int VehicleId { get; set; }

        public int OrderType { get; set; }

        public string ServiceUrl { get; set; }

        public string OrderName { get; set; }

        public string EncryptName { get; set; }

        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptName); }
            set { EncryptName = TripleDesEncryption.Encrypt(value); }
        }

        public string ContactName { get; set; }

        public string MainPhone { get; set; }

        public string SecundaryPhone { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string VehicleName { get; set; }

        public string PlateId { get; set; }

        public string Chassis { get; set; }

        public string Manufacturer { get; set; }

        public string VehicleModel { get; set; }

        public string Type { get; set; }

        public string Colour { get; set; }

        public int Year { get; set; }

        public string ErrorMessage { get; set; }
    }

    public class OrdersDetail
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
