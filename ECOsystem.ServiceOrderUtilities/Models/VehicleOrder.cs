﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.ServiceOrderUtilities.Models
{
    public class VehicleOrderResult
    {
        public string ResponseCode { get; set; }

        public string ResponseMessage { get; set; }
    }

    public class VehicleOrder
    {
        public int OrderId { get; set; }

        public string OrderCode { get; set; }

        public int OrderType { get; set; }

        public int StatusId { get; set; }

        public int VehicleId { get; set; }

        public int Composition { get; set; }

        public string CommandObservations { get; set; }

        public bool HasOpenDoors { get; set; }

        public bool HasTurnOff { get; set; }

        public bool HasDallas { get; set; }

        public int ProviderId { get; set; }

        public int ZoneId { get; set; }

        public string InstallatorName { get; set; }

        public string InstallationLocation { get; set; }

        public string ESN { get; set; }

        public string Serial { get; set; }

        public int DeviceModelId { get; set; }

        public int ClassificationId { get; set; }
    }
}