﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.ServiceOrderUtilities.Models
{
    public class OrderAPISettingsModel
    {
        public string URL { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string DecryptUserName { get { return string.IsNullOrEmpty(UserName) ? string.Empty : TripleDesEncryption.Decrypt(UserName); } }

        public string DecryptPassword { get { return string.IsNullOrEmpty(Password) ? string.Empty : TripleDesEncryption.Decrypt(Password); } }
    }
}
