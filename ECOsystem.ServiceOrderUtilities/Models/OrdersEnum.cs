﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.ServiceOrderUtilities.Models
{
    public enum OrdersEnum
    {           
        Install = 1,        
        Uninstall = 2,          
        Change = 3,
        Check = 4,
        ChangeOptions = 5,
        FailOrder = 6
    }
}
