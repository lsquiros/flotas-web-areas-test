﻿using ECOsystem.DataAccess;
using ECOsystem.ServiceOrderUtilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.ServiceOrderUtilities.Business
{
    class OrderBusiness : IDisposable
    {
        public List<OrdersModel> RetrieveVehiclesOrderInformation(int OrderType, int? CustomerId, int? VehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<OrdersModel>("[General].[Sp_VehiclesOrderInformation_Retrieve]",
                    new
                    {
                        OrderType,
                        CustomerId,
                        VehicleId
                    }).ToList();
            }
        }

        public OrderAPISettingsModel RetrieveOrderSettings(int? CustomerId, int? VehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<OrderAPISettingsModel>("[General].[Sp_RetrieveOrderSettings_Retrieve]", 
                    new
                    {
                        CustomerId,
                        VehicleId
                    }).FirstOrDefault();
            }
        }

        public void EmailOrderInformation(OrdersModel model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_EmailOrderInformation_Send]",
                    new
                    {                           
                        model.OrderName,
                        CustomerName = model.DecryptedName,
                        model.ContactName,
                        model.MainPhone,
                        model.SecundaryPhone,
                        model.Email,
                        model.Address,
                        model.VehicleName,
                        model.PlateId,
                        model.Chassis,
                        model.Manufacturer,
                        model.VehicleModel,
                        model.Type,
                        model.Colour,
                        model.Year
                    });
            }
        }

        public List<OrdersDetail> RetrieveOrderTypes()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<OrdersDetail>("[General].[Sp_RetrieveOrderTypes_Retrieve]",  new   {  }).ToList();
            }
        }

        public List<OrdersDetail> RetrieveOrderStatus()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<OrdersDetail>("[General].[Sp_RetrieveOrderStatus_Retrieve]", new { }).ToList();
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
