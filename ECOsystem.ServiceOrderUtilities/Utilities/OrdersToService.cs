﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ECOsystem.ServiceOrderUtilities.Models;

namespace ECOsystem.ServiceOrderUtilities
{
    public class OrdersToService : IDisposable
    {
        public string CreateOrder(OrdersModel model, OrderAPISettingsModel apiSettings)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiSettings.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", apiSettings.DecryptUserName, apiSettings.DecryptPassword))));
                var uri = string.Format("api/Intrack/WorkOrder/{0}/{1}", model.VehicleId, model.OrderType);

                var response = client.PostAsJsonAsync(uri, string.Empty).Result;
                if (!response.IsSuccessStatusCode) throw new Exception(response.Content.ReadAsStringAsync().Result);
                var respMessage = response.Content.ReadAsByteArrayAsync().Result;
                return ("Success");
            }
        }

        public string RetrieveWorkOrder(OrdersModel model, OrderAPISettingsModel apiSettings)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiSettings.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", apiSettings.DecryptUserName, apiSettings.DecryptPassword))));
                var encodedContent = new FormUrlEncodedContent(new Dictionary<string, string> { { "vehicleId", model.VehicleId.ToString() } });

                var response = client.GetAsync("api/Intrack/WorkOrder/" + model.VehicleId).Result;
                if (!response.IsSuccessStatusCode) throw new Exception(response.Content.ReadAsStringAsync().Result);
                return response.Content.ReadAsStringAsync().Result;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
