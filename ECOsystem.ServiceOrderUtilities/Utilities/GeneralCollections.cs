﻿using ECOsystem.ServiceOrderUtilities.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ECOsystem.ServiceOrderUtilities.Utilities
{
    public class GeneralCollections
    {
        public static SelectList GetOrderTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new OrderBusiness())
                {
                    var result = business.RetrieveOrderTypes();
                    foreach (var r in result)
                    {
                        dictionary.Add(r.Id, r.Description);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetOrderStatus
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new OrderBusiness())
                {
                    var result = business.RetrieveOrderStatus();
                    foreach (var r in result)
                    {
                        dictionary.Add(r.Id, r.Description);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
    }
}
