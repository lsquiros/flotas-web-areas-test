﻿using ECOsystem.ServiceOrderUtilities.Business;
using ECOsystem.ServiceOrderUtilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ECOsystem.ServiceOrderUtilities.Utilities
{
    public class OrdersUtilities
    {
        public static VehicleOrder GetOrderInformation(int VehicleId)
        {
            using (var or = new OrdersToService())
            {
                using (var bus = new OrderBusiness())
                {
                    //Get the result from API and deserialize the json into models
                    var result = new JavaScriptSerializer().Deserialize<VehicleOrderResult>(or.RetrieveWorkOrder(new Models.OrdersModel() { VehicleId = VehicleId }, bus.RetrieveOrderSettings(null, VehicleId)));
                    return new JavaScriptSerializer().Deserialize<List<VehicleOrder>>(result.ResponseMessage).FirstOrDefault();
                }
            }
        }

        public static List<OrdersModel> SendOrder(int TypeOrder, int? CustomerId, int? VehicleId, bool? Email)
        {
            using (var or = new OrdersToService())
            {
                using (var bus = new OrderBusiness())
                {
                    var list = bus.RetrieveVehiclesOrderInformation(TypeOrder, CustomerId, VehicleId);
                    var settings = bus.RetrieveOrderSettings(CustomerId, VehicleId);

                    foreach (var item in list)
                    {
                        try
                        {
                            or.CreateOrder(item, settings);
                            if (Email != null && (bool)Email)
                            {
                                bus.EmailOrderInformation(item);
                            }
                        }
                        catch (Exception e)
                        {
                            item.ErrorMessage = e.Message;
                        }
                    }
                    return list;
                }
            }
        }
    }
}