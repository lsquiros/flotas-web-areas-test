﻿using System.Collections.Generic;

namespace ECOsystem.DataAccess
{
#pragma warning disable 1591

    public class ISearchResult<T>
    {
        public List<T> Data { get; set; }

        public long? Total { get; set; }
    }
}