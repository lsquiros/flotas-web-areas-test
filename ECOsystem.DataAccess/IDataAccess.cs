﻿using System.Collections.Generic;

namespace ECOsystem.DataAccess
{
#pragma warning disable 1591

    /// <summary>
    /// IDataAccess
    /// </summary>
    public interface IDataAccess
    {
        int ExecuteNonQuery(string pSpName, dynamic pInParams);

        ISearchResult<T> ExecuteReader<T>(string pSpName, dynamic pInParams, int pPageSize, int pPageNumber);

        IList<T> ExecuteReader<T>(string pSpName, dynamic pInParams);

        T ExecuteScalar<T>(string pSpName, dynamic pInParams);

    }
}
