﻿/************************************************************************************************************
*  File    : DataAccess.cs
*  Summary : Data Access Layer
*  Author  : Manuel Azofeifa    
*  Date    : 03/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace ECOsystem.DataAccess
{
    /// <summary>
    /// Data Access
    /// </summary>
    public class DataBaseAccess : IDataAccess, IDisposable
    {
        private readonly string _databaseAppKey;

        /// <summary>
        /// Data Access constructor
        /// </summary>
        public DataBaseAccess() : this("ConnectionStringName") { }

        /// <summary>
        /// Data Access constructor
        /// </summary>
        /// <param name="databaseAppKey"></param>
        public DataBaseAccess(string databaseAppKey) { _databaseAppKey = databaseAppKey; }

        /// <summary>
        /// Execute Reader
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="spName"></param>
        /// <param name="spInParams"></param>
        /// <returns></returns>
        public IList<T> ExecuteReader<T>(string spName, dynamic spInParams)
        {
            IList<T> result = new List<T>();
            using (DataAccessAppBlock dataAccess = new DataAccessAppBlock(_databaseAppKey))
            {
                dataAccess.CommandTimeOut = 99999;//36000;
                using (IDataReader dataReader = dataAccess.ApplyStoreProcCommandDataReader(spName, spInParams))
                {
                    while (dataReader.Read())
                    {
                        result.Add(dataAccess.GetValues<T>(dataReader));
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Execute Reader just for Paging Purposes
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="spName"></param>
        /// <param name="spInParams"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public ISearchResult<T> ExecuteReader<T>(string spName, dynamic spInParams, int pageSize, int pageNumber)
        {
            List<T> data = new List<T>();
            long? total = 0;
            using (DataAccessAppBlock dataAccess = new DataAccessAppBlock(_databaseAppKey))
            {
                using (IDataReader dataReader = dataAccess.ApplyStoreProcCommandDataReader(spName, spInParams, pageSize, pageNumber))
                {
                    while (dataReader.Read())
                    {
                        data.Add(dataAccess.GetValues<T>(dataReader));
                    }
                    dataReader.NextResult();
                    while (dataReader.Read())
                    {
                        total = dataAccess.GetValue<long?>(dataReader["RowCount"]);
                    }
                }
            }
            return new SearchResult<T> { Data = data, Total = total };
        }

        /// <summary>
        /// Execute Non Query
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="spInParams"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string spName, dynamic spInParams)
        {
            using (var dataAccess = new DataAccessAppBlock(_databaseAppKey))
            {
                return dataAccess.ApplyStoreProcCommandNonQuery(spName, spInParams);
            }
        }



        /// <summary>
        /// Execute Scalar
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="spName"></param>
        /// <param name="spInParams"></param>
        /// <returns></returns>
        public T ExecuteScalar<T>(string spName, dynamic spInParams)
        {
            using (var dataAccess = new DataAccessAppBlock(_databaseAppKey))
            {
                dataAccess.AddInParameters(spInParams);
                return (T)dataAccess.ApplyStoreProcCommandScalar(spName, spInParams);
            }
        }


        /// <summary>
        /// Flag for disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (!disposing)
            {
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }

    /// <summary>
    /// Data Access App Block
    /// </summary>
    internal class DataAccessAppBlock : IDisposable
    {

        #region "Variables"

        private DatabaseContextClassic _appDataBase;

        private int _commandTimeOut = 3000;
        public int CommandTimeOut
        {
            get
            {
                return _commandTimeOut;
            }
            set
            {
                _commandTimeOut = value;
            }
        }

        #endregion

        #region "Constructors / Destructors"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pDataBase"></param>
        public DataAccessAppBlock(string pDataBase)
        {
            Init(pDataBase);
        }

        #endregion

        #region "Other Functions / Procedures"

        /// <summary>
        /// Database Context Classic
        /// </summary>      
        /// <returns></returns>
        public DatabaseContextClassic DatabaseContextFactory(string name)
        {
            var conectionName = ConfigurationManager.AppSettings[name];
            if (conectionName == null)
                throw new ApplicationException("Unable to initialize database for the following Application Key : " + name);

            return new DatabaseContextClassic(ConfigurationManager.ConnectionStrings[conectionName].ConnectionString);          
        }

        /// <summary>
        /// Init
        /// </summary>       
        /// <param name="configurationKey"></param>
        public void Init(string configurationKey)
        {
            _appDataBase = DatabaseContextFactory(configurationKey);
            _appDataBase.CreateConnection();
            _appDataBase.OpenConnection();
        }
      

        /// <summary>
        /// Apply StoreProc Command Data Reader
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="spInParams"></param>
        /// <returns></returns>
        public IDataReader ApplyStoreProcCommandDataReader(string spName, dynamic spInParams)
        {
            using (var commandWrapper = _appDataBase.GetStoredProcCommand(spName))
            {
                commandWrapper.CommandTimeout = CommandTimeOut;
                if (spInParams != null)
                {
                    commandWrapper.Parameters.AddRange(AddInParameters(spInParams).ToArray());
                }
                return commandWrapper.ExecuteReader();
            }
        }

        /// <summary>
        /// Apply StoreProc Command Data Reader
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="spInParams"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public IDataReader ApplyStoreProcCommandDataReader(string spName, dynamic spInParams, int pageSize, int pageNumber)
        {
            using (var commandWrapper = _appDataBase.GetStoredProcCommand(spName))
            {
                commandWrapper.CommandTimeout = CommandTimeOut;
                commandWrapper.Parameters.AddRange(AddInParameters(spInParams).ToArray());

                dynamic spInpaging = new {pageSize, pageNumber };
                commandWrapper.Parameters.AddRange(AddInParameters(spInpaging).ToArray());
                return commandWrapper.ExecuteReader();
            }
        }

        /// <summary>
        /// Apply Store Proc Command Non Query
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="spInParams"></param>
        /// <returns></returns>
        public int ApplyStoreProcCommandNonQuery(string spName, dynamic spInParams)
        {
            using (var commandWrapper = _appDataBase.GetStoredProcCommand(spName))
            {
                commandWrapper.CommandTimeout = CommandTimeOut;
                commandWrapper.Parameters.AddRange(AddInParameters(spInParams).ToArray());
                return commandWrapper.ExecuteNonQuery();
            }

        }

        /// <summary>
        /// Exceute a Query which return an Object as result
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="spInParams"></param>
        /// <returns></returns>
        public object ApplyStoreProcCommandScalar(string spName, dynamic spInParams)
        {
            using (var commandWrapper = _appDataBase.GetStoredProcCommand(spName))
            {
                commandWrapper.CommandTimeout = 5000000;
                commandWrapper.Parameters.AddRange(AddInParameters(spInParams).ToArray()); 
                return commandWrapper.ExecuteScalar();
            }

        }

        /// <summary>
        /// validated a sql sentences
        /// </summary>
        /// <param name="spName">The name of the sp will execute the SQL statement</param>
        /// <param name="spInParams">The paramenters</param>
        /// <returns>true is valid</returns>
        public bool IsValidSqlSentence(string spName, dynamic spInParams)
        {
            using (var commandWrapper = _appDataBase.GetStoredProcCommand(spName))
            {
                commandWrapper.CommandTimeout = CommandTimeOut;
                commandWrapper.Parameters.AddRange(AddInParameters(spInParams).ToArray());

                using (var dataReader = commandWrapper.ExecuteReader())
                {
                    var result = false;
                    do
                    {
                        while (dataReader.Read())
                        {
                            result = true;
                        }
                    } while (dataReader.NextResult());

                    return result;
                }

            }

        }

        /// <summary>
        /// validated a sql sentences
        /// </summary>
        /// <param name="spName">The name of the sp will execute the SQL statement</param>
        /// <param name="spInParams">The paramenters</param>
        /// <param name="keys"> </param>
        /// <exception cref="ConfigurationException"></exception>
        /// <returns>true is valid</returns>
        public IDictionary<Int32, IList<List<object>>> ExecuteSqlSentence(string spName, dynamic spInParams, IDictionary<Int32, bool> keys)
        {
            using (var commandWrapper = _appDataBase.GetStoredProcCommand(spName))
            {
                commandWrapper.CommandTimeout = CommandTimeOut;
                commandWrapper.Parameters.AddRange(AddInParameters(spInParams).ToArray());
                var result = new Dictionary<Int32, IList<List<object>>>();

                var index = 0;

                using (var dataReader = commandWrapper.ExecuteReader())
                {
                    do
                    {
                        if (dataReader.HasRows)
                        {
                            var isValidReq = false;
                            var data = new List<List<object>>();

                            while (dataReader.Read())
                            {
                                if (Equals(dataReader[0], Int32.MinValue))
                                {
                                    if (dataReader.VisibleFieldCount == 2)
                                    {
                                        result.Add(Int32.MinValue, new List<List<object>> { new List<object> { dataReader.GetInt32(1) } });
                                    }
                                }
                                else
                                {
                                    var rows = new List<object>();
                                    for (var i = 0; i < dataReader.FieldCount; i++)
                                    {
                                        rows.Add(dataReader[i]);
                                        if (!string.IsNullOrEmpty(Convert.ToString(dataReader[i])) && !isValidReq)
                                        {
                                            isValidReq = true;
                                        }
                                    }

                                    data.Add(rows);

                                }

                            }
                            if (keys.Count > index)
                            {
                                if (keys.ElementAt(index).Value && !isValidReq)
                                {
                                    throw new ConfigurationErrorsException(
                                        string.Format(
                                            "The primitive ID {0} is required and no value was returned with the sql sentence",
                                            keys.ElementAt(index).Key));
                                }
                                result.Add(keys.ElementAt(index).Key, data);
                            }
                        }
                        else
                        {
                            result.Add(keys.ElementAt(index).Key, null);
                        }
                        index += 1;

                    } while (dataReader.NextResult());
                }
                return result;

            }

        }       
     

        #endregion

        #region "Add In Parameters"

        /// <summary>
        /// AddInParameters
        /// </summary>
        /// <param name="spInParam"></param>
        /// <exception cref="NotImplementedException"></exception>
        public IList<SqlParameter> AddInParameters(dynamic spInParam)
        {
            IList<SqlParameter> result = new List<SqlParameter>();
            if (spInParam == null) return result;
            foreach (PropertyInfo prop in spInParam.GetType().GetProperties())
            {
                object value = prop.GetValue(spInParam, null);
                if (value == null)
                    continue;
                DbType temp;
                switch (value.GetType().ToString())
                {
                    case "System.Byte":
                        temp = DbType.Byte;
                        break;
                    case "System.Int16":
                        temp = DbType.Int16;
                        break;
                    case "System.Int32":
                        temp = DbType.Int32;
                        break;
                    case "System.Int64":
                        temp = DbType.Int64;
                        break;
                    case "System.Decimal":
                        temp = DbType.Decimal;
                        break;
                    case "System.Double":
                        temp = DbType.Double;
                        break;
                    case "System.Single":
                        temp = DbType.Single;
                        break;
                    case "System.Guid":
                        temp = DbType.Guid;
                        break;
                    case "System.Boolean":
                        temp = DbType.Boolean;
                        break;
                    case "System.DateTime":
                        temp = DbType.DateTime;
                        break;
                    case "System.TimeSpan":
                        temp = DbType.Time;
                        break;
                    case "System.String":
                        temp = DbType.String;
                        break;
                    case "System.Byte[]":
                        temp = DbType.Binary;
                        break;

                    default:
                        throw new NotImplementedException("The type: " + value.GetType() + " has not been implemented");
                }
                result.Add(_appDataBase.AddInParameter(prop.Name, temp, ref value));
            }
            return result;
        }


        /// <summary>
        /// AddInParameters
        /// </summary>
        /// <param name="spInParam"></param>
        /// <exception cref="NotImplementedException"></exception>
        public IList<SqlParameter> AddOutParameters(dynamic spInParam)
        {
            IList<SqlParameter> result = new List<SqlParameter>();
            if (spInParam == null) return result;
            foreach (PropertyInfo prop in spInParam.GetType().GetProperties())
            {
                object value = prop.GetValue(spInParam, null);
                if (value == null)
                    continue;
                DbType temp;
                switch (value.GetType().ToString())
                {
                    case "System.Boolean":
                        temp = DbType.Boolean;
                        break;
                    case "System.DateTime":
                        temp = DbType.DateTime;
                        break;
                    case "System.String":
                        temp = DbType.String;
                        break;
                    case "System.Int32":
                        temp = DbType.Int32;
                        break;
                    case "System.Double":
                        temp = DbType.Double;
                        break;
                    case "System.Decimal":
                        temp = DbType.Decimal;
                        break;
                    default:
                        throw new NotImplementedException("The type: " + value.GetType() + " has not been implemented");
                }
                var size = Marshal.SizeOf(value.GetType());
                result.Add(_appDataBase.AddOutParameter(prop.Name, temp, size));
            }
            return result;
        }



        #endregion



        #region "Get Reader Values"

        /// <summary>
        /// Get Value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public T GetValue<T>(object value)
        {
            return (T)(Convert.IsDBNull(value) ? null : value);
        }


        /// <summary>
        /// Get Values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataReader"></param>
        /// <returns></returns>
        public T GetValues<T>(IDataReader dataReader)
        {
            var result = Activator.CreateInstance<T>();
            var entityType = result.GetType();
            for (int i = 0, j = dataReader.FieldCount; i < j; i++)
            {
                var property = entityType.GetProperty(dataReader.GetName(i));
                if (property == null) continue;
                var dri = dataReader[i];
                property.SetValue(result, Convert.IsDBNull(dri) ? null : dri, null);
            }
            return result;
        }
        #endregion

        #region "IDisposable Support"

        // To detect redundant calls
        private bool _disposedValue;

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                }
                _appDataBase.Dispose();
                // TODO: free your own state (unmanaged objects).
                // TODO: set large fields to null.
                _appDataBase = null;

            }
            _disposedValue = true;
        }

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }


    /// <summary>
    /// Database Context Classic
    /// </summary>
    [SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
    public class DatabaseContextClassic : IDisposable
    {

        private IDbConnection _dbConnection;
        /// <summary>
        /// </summary>
        public string ConnectionString { get; set; }


        /// <summary>
        /// Database Context Classic
        /// </summary>
        public DatabaseContextClassic(string connString)
        {
            ConnectionString = connString;
        }

        /// <summary>
        /// Add In Parameter
        /// </summary>
        /// <returns></returns>
        public SqlParameter AddInParameter(string name, DbType dbType, ref object value)
        {
            return new SqlParameter
            {
                ParameterName = "@p" + name,
                Value = value ?? DBNull.Value,
                DbType = dbType,
                Direction = ParameterDirection.Input
            };
        }

        /// <summary>
        /// Add Out Parameter
        /// </summary>
        /// <param name="pName"></param>
        /// <param name="pDbType"></param>
        /// <param name="pSize"></param>
        /// <returns></returns>
        public SqlParameter AddOutParameter(string pName, DbType pDbType, int pSize)
        {
            return new SqlParameter
            {
                ParameterName = pName,
                DbType = pDbType,
                Size = pSize,
                Direction = ParameterDirection.Output
            };
        }

        /// <summary>
        /// CreateConnection
        /// </summary>
        public void CreateConnection()
        {
            _dbConnection = new SqlConnection(ConnectionString);
        }

        /// <summary>
        /// CloseConnection
        /// </summary>
        public void CloseConnection()
        {
            if (_dbConnection != null)
            {
                _dbConnection.Close();
            }
        }

        /// <summary>
        /// Get Stored Proc Command
        /// </summary>
        /// <param name="pStoredProcedureName"></param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public DbCommand GetStoredProcCommand(string pStoredProcedureName)
        {
            return new SqlCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = pStoredProcedureName,
                Connection = _dbConnection as SqlConnection
            };
        }

        /// <summary>
        /// OpenConnection
        /// </summary>
        public void OpenConnection()
        {
            _dbConnection.Open();
        }

        /// <summary>
        /// Get Open Connection
        /// </summary>
        /// <returns></returns>
        public IDbConnection GetOpenConnection()
        {
            return _dbConnection;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        public void Dispose()
        {
            if (_dbConnection != null)
            { _dbConnection.Dispose(); }
        }
    }
}
