USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerDemoAlert_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerDemoAlert_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 30/05/2018
-- Description:	Retrieve customer demo alert role
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_CustomerDemoAlert_Retrieve]
AS
BEGIN
	
	SET NOCOUNT ON
    
	SELECT c.[CustomerId],
		   c.[Name] [EncryptName],
		   c.[InsertDate],
		   ca.[DateAlert],
		   asp.[Email] [EncryptEmail],
		   (
				SELECT COUNT(1)
				FROM [Control].[CreditCard]
				WHERE [CustomerId] = c.[CustomerId]
				AND [StatusId] NOT IN (2, 6, 9)
		   ) [CreditCards],
		   (
				SELECT COUNT(1)
				FROM [General].[Vehicles]
				WHERE [CustomerId] = c.[CustomerId]
				AND ([IsDeleted] IS NULL OR [IsDeleted] = 0)
		   ) [Vehicles]
	FROM [General].[Customers] c
	INNER JOIN [General].[CustomerDemoAlert] ca
		ON c.[CustomerId] = ca.[CustomerId]
	INNER JOIN [General].[Users] u
		ON c.[CustomerManagerId] = u.[UserId]
	INNER JOIN [dbo].[AspNetUsers] asp
		ON u.[AspNetUserId] = asp.[Id]
	INNER JOIN [General].[Countries] co   		
		ON co.[CountryId] = c.[CountryId] 
	WHERE ca.[DateAlert] > DATEADD(S, -1, CAST(DATEDIFF(DAY, 0, DATEADD(HOUR, co.[TimeZone], GETDATE())) AS DATETIME))

	SET NOCOUNT OFF
END