USE [ECOSystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NonTransmissionGPSJob_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_NonTransmissionGPSJob_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_NonTransmissionGPSJob_Retrieve]    Script Date: 17/07/2018 11:56:40 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 11/07/2018
-- Description:	Obtiene los vehiculos con mas de 24 sin transmision del GPS, que tenga las alarmas generales configuradas
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_NonTransmissionGPSJob_Retrieve]
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		RNT.[TypeId], 
		RNT.[AlarmId],
		RNT.[AlarmPhone],  
		RNT.[AlarmEmail], 
		RNT.[GPSDateTime],  
		RNT.[Longitude],  
		RNT.[Latitude],  
		RNT.[ReportID],
		(SELECT [Efficiency].[Fn_GetAddressByLatLon_Retrieve](RNT.[Longitude], RNT.[Latitude], RNT.[CustomerId])) Direction,  
		RNT.[CountryCode],
		RNT.[VehicleId],
		RNT.[PlateId],
		RNT.[Name] [VehicleName],
		RNT.[CustomerId],
		RNT.[HoursNoTransmisionGPS],
		RNT.[ReportLasteDate]
	FROM (SELECT   
				T.[TypeId], 
				A.[AlarmId],
				A.[Phone] [AlarmPhone],  
				A.[Email]  [AlarmEmail], 
				RL.[Device],
				RL.[GPSDateTime],  
				RL.[Longitude],  
				RL.[Latitude],  
				RL.[ReportID],
				COUN.[Code] [CountryCode],
				V.[VehicleId],
				V.[PlateId],
				V.[Name],
				V.[CustomerId],
				DATEDIFF (Hour, RL.[GPSDateTime], GETDATE()) [HoursNoTransmisionGPS],
				concat(convert(varchar, RL.[GPSDateTime], 103),' ',convert(varchar, RL.[GPSDateTime], 24),' ') ReportLasteDate
			FROM [dbo].[Report_Last] RL
				INNER JOIN [General].[Vehicles] V
					ON RL.[Device] = V.[DeviceReference]
				INNER JOIN [General].[Customers] C
					ON V.[CustomerId] = C.[CustomerId]
				INNER JOIN [General].[Countries] COUN 
					ON C.[CountryId] = COUN.[CountryId]
				INNER JOIN	[General].[Types] T  
					ON T.[Code] = 'NON_TRANSMISSION_GPS_KEY'
				INNER JOIN [General].[Alarms] A
					ON T.[TypeId] =  A.[AlarmTriggerId] AND A.[EntityId] = C.[CustomerId]
			WHERE --V.[CustomerId] = 59 AND 
				(DATEDIFF (Hour, RL.[GPSDateTime], GETDATE()) > 24)
			) AS RNT
	WHERE (RNT.[AlarmPhone] IS NOT NULL) AND (RNT.[AlarmEmail] IS NOT NULL)
			AND RNT.[HoursNoTransmisionGPS] < CAST(ISNULL(( SELECT [NumericValue] FROM [dbo].[GeneralParameters]
																WHERE [ParameterID] = 'MAXTIME_SENDALARM_NOGPSTRANSMISSION')
															,0) AS INT) 
			AND RNT.[VehicleId] NOT IN (SELECT [EntityId] FROM [General].[Alarms]
											WHERE [AlarmTriggerId] = 13 
												AND [ReportLasteDate] = RNT.[ReportLasteDate])
    SET NOCOUNT OFF
END
