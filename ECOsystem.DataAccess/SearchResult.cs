﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ECOsystem.DataAccess
{
#pragma warning disable 108, 114
    /// <summary>
    /// ISearc hResult
    /// </summary>
    /// <typeparam name="T">Type T of returning object</typeparam>
    public class SearchResult<T> : ISearchResult<T>
    {

        /// <summary>
        /// Data contains a list of objects to return
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1061:DoNotHideBaseClassMethods")]
        public IList<T> Data { get; set; }

        /// <summary>
        /// Count list of objects to return
        /// </summary>
        public long? Total { get; set; }
    }


}
