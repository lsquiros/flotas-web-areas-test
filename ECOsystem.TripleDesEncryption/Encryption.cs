﻿using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace ECOsystem
{
    /// <summary>
    /// Enables simple TripleDES encryption with or without hashing
    /// </summary>
    public class TripleDesEncryption
    {
        /// <summary>
        /// Encryption key. Used to encrypt and decrypt.
        /// </summary>
        static readonly string EncryptionKey = ConfigurationSettings.AppSettings["TripleDesEncryptionKey"];

        /// <summary>
        /// Encrypt text string
        /// </summary>
        /// <param name="toEncrypt">The string of data to encrypt</param>
        /// <param name="useHashing">Weather hashing is used or not</param>
        /// <returns>An encrypted string</returns>
        public static string Encrypt(string toEncrypt, bool useHashing = true)
        {
            if (string.IsNullOrEmpty(toEncrypt))
                return "";

            byte[] keyArray;
            var toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);

            // If hashing use get hashcode regards to your key
            if (useHashing)
            {
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(EncryptionKey));
                hashmd5.Clear();
            }
            else
                keyArray = Encoding.UTF8.GetBytes(EncryptionKey);

            // Set the secret key for the tripleDES algorithm
            var tdes = new TripleDESCryptoServiceProvider
            {
                KeySize = 192,
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };

            // Transform the specified region of bytes array to resultArray
            var cTransform = tdes.CreateEncryptor();
            var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();

            // Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        /// <summary>
        /// Decrypts a text string
        /// </summary>
        /// <param name="cipherString">The encrypted string</param>
        /// <param name="useHashing">Weather hashing is used or not</param>
        /// <returns>Decrypted text string</returns>
        public static string Decrypt(string cipherString, bool useHashing = true)
        {
            try
            {
                if (string.IsNullOrEmpty(cipherString))
                    return "";

                byte[] keyArray;
                var toEncryptArray = Convert.FromBase64String(cipherString.Replace(' ', '+'));

                if (useHashing)
                {
                    // If hashing was used get the hash code with regards to your key
                    var hashmd5 = new MD5CryptoServiceProvider();
                    keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(EncryptionKey));
                    hashmd5.Clear();
                }
                else
                {
                    // If hashing was not implemented get the byte code of the key
                    keyArray = Encoding.UTF8.GetBytes(EncryptionKey);
                }

                // Set the secret key for the tripleDES algorithm
                var tdes = new TripleDESCryptoServiceProvider
                {
                    KeySize = 192,
                    Key = keyArray,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };

                var cTransform = tdes.CreateDecryptor();
                var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                tdes.Clear();

                // Return the Clear decrypted TEXT
                return Encoding.UTF8.GetString(resultArray);
            }
            catch (Exception)
            {

                return cipherString;
            }
            
        }


        /// <summary>
        /// Retrieve MD5 Hash from string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input

            MD5 md5 = MD5.Create();

            byte[] inputBytes = Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }
    }
}