﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Models.Home
{
    public class RoleValidateModel
    {
        public string RoleId { get; set; }
        public string Name { get; set; }
    }
}