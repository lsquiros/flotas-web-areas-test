﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Models.Home
{
    public class ModulesUrl
    {
        public string CONTROL_URI { get; set; }

        public string OPERATION_URI { get; set; }

        public string ADMIN_URI { get; set; }

        public string COINCA_URI { get; set; }

        public string PARTNER_URI { get; set; }

        public string ECOMANUAL_URI { get; set; }

        public string INSURANCE_URI { get; set; }

        public string EFFICIENCY_URI { get; set; }

        public string MAPS_URI { get; set; }

        public string REPORTS_URI { get; set; }

        public string SERVICESTATIONS_URI { get; set; }

        public string MANAGEMENT_URI { get; set; }
    }
}