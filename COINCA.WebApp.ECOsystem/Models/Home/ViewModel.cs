﻿using System.Collections.Generic;
using ECOsystem.Models.Core;
/************************************************************************************************************
*  File    : ViewModel.cs
*  Summary : ViewModel
*  Author  : Manuel Azofeifa Hidalgo 
*  Date    : 17/Nov/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

namespace ECOsystem.Models.Home
{
    /// <summary>
    /// View Model
    /// </summary>
    public class ViewModel
    {
        /// <summary>
        /// rssModels
        /// </summary>
        public IEnumerable<PartnerNews> rssModels { get; set; }

        /// <summary>
        /// caIndicator
        /// </summary>
        public CAIndicators caIndicator { get; set; }

        /// <summary>
        /// realVsBudgetFuelsReportBase
        /// </summary>
        public RealVsBudgetFuelsReportBase realVsBudgetFuelsReportBase { get; set; }     

        /// <summary>
        /// Dasboad Modules
        /// </summary>
        public IEnumerable<DashboardModule> dasboadModules { get; set; }


        /// <summary>
        /// CustomerByPartner
        /// </summary>
        public CustomerByPartnerData Data { get; set; }

        /// <summary>
        /// Dashboard Core Indicators
        /// </summary>
        public DashboardCoreIndicators DataIndicators { get; set; }

        /// <summary>
        /// Cant Text Delimiter
        /// </summary>
        public int cantTextNewsDelimiter { get; set; }
    }

    /// <summary>
    /// FuelChange
    /// </summary>
    public class FuelChange
    {
        /// <summary>
        /// Val
        /// </summary>
        public string Val { get; set; }
    }
}