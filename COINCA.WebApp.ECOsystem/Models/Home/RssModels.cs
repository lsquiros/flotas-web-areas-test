﻿/************************************************************************************************************
*  File    : RssModels.cs
*  Summary : RSS Models
*  Author  : Cristian Martínez 
*  Date    : 01/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
namespace ECOsystem.Models.Home
{
    /// <summary>
    /// RssModels
    /// </summary>
    public class RssModels
    {   
        /// <summary>
        /// property
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string link { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string image { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string date { get; set; }
    }
}