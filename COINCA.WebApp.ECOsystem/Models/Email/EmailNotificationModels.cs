﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Email
{
    /// <summary>
    /// Email Base
    /// </summary>
    public class EmailNotificationBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public EmailNotificationBase()
        {
            CreateEmail = new EmailNotificationCreate();            
            List = new List<EmailNotificationCreate>();
        }

        /// <summary>
        /// Model for Creation
        /// </summary>
        public EmailNotificationCreate CreateEmail { get; set; }

        /// <summary>
        /// List of Emails for Grid
        /// </summary>
        public IEnumerable<EmailNotificationCreate> List { get; set; }
    }

    /// <summary>
    /// This model loads the emails in the creditcard creations
    /// </summary>
    public class EmailNotification
    {
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
    }

    /// <summary>
    /// Add or Edit Notifications
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class EmailNotificationCreate : ModelAncestor
    {
        /// <summary>
        /// Email Id
        /// </summary>
        [NotMappedColumn]
        public int? ID_Email { get; set; }

        /// <summary>
        /// Email Notification
        /// </summary>
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Correo Electrónico")]
        [Required(ErrorMessage = "{0} requerido")]
        public string  Email { get; set; }

        /// <summary>
        /// Partner Id
        /// </summary>  
        [Display(Name = "Socio")]
        [Required(ErrorMessage = "{0} requerido")]
        public int PartnerId { get; set; }

        /// <summary>
        /// Status id - Active
        /// </summary>
        [NotMappedColumn]
        public bool Activo { get; set; }

        /// <summary>
        /// Type - Email notification or other
        /// </summary>
        [NotMappedColumn]
        public int? TypeId { get; set; }
        
    }
}