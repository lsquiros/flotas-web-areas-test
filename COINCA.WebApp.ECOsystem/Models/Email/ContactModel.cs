﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECOsystem.Models.Email
{
    public class ContactModel
    {
        [DisplayName("Nombre Completo")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Correo Electrónico")]
        public string Email { get; set; }

        [DisplayName("Teléfono Residencial")]
        public string HomePhone { get; set; }

        [DisplayName("Teléfono de Oficina")]
        public string OfficePhone { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Teléfono Celular")]
        public string CellPhone { get; set; }

        [DisplayName("Dirección")]
        public string Address { get; set; }

        [DisplayName("Comentarios")]
        public string Comments { get; set; }


    }
}