﻿/************************************************************************************************************
*  File    : Email.cs
*  Summary : Email
*  Author  : Cristian Martínez 
*  Date    : 28/Nov/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.Models.Miscellaneous;

namespace ECOsystem.Models.Email
{
    /// <summary>
    /// Email Model
    /// </summary>
    public class Email : ModelAncestor
    {
        /// <summary>
        /// EmailId is a PK of Emails Table
        /// </summary>
        public int EmailId { get; set; }

        /// <summary>
        /// property 
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string CC { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public bool IsHtml { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public bool Sent { get; set; }
    }
}
