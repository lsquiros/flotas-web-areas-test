﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ECOsystem.Models.Identity
{
#pragma warning disable 1591

    public class ApplicationUserRole : IdentityUserRole<string> { }
    /// <summary>
    /// You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Generate User IdentityAsync
        /// </summary>
        /// <param name="manager">manager</param>
        /// <returns></returns>
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        /// <summary>
        /// GenerateUserIdentity
        /// </summary>
        /// <param name="manager">manager</param>
        /// <returns>Claims Identity</returns>
        public ClaimsIdentity GenerateUserIdentity(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    /// <summary>
    /// Application DbContext
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Application DbContext Default constructor
        /// </summary>
        public ApplicationDbContext() : base("DefaultConnection")
        {
        }

        static ApplicationDbContext()
        {
            Database.SetInitializer<ApplicationDbContext>(null);
        }

        /// <summary>
        /// Default Create
        /// </summary>
        /// <returns></returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// </summary>
        public virtual IDbSet<ApplicationGroup> ApplicationGroups { get; set; }

        /// <summary>
        ///     Maps table names, and sets up relationships between the various user entities
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationGroup>()
                .HasMany(g => g.ApplicationUsers)
                .WithRequired().HasForeignKey(ag => ag.ApplicationGroupId);
            modelBuilder.Entity<ApplicationUserGroup>()
                .HasKey(r =>
                    new
                    {
                        r.ApplicationUserId, r.ApplicationGroupId
                    }).ToTable("ApplicationUserGroups");

            modelBuilder.Entity<ApplicationGroup>()
                .HasMany(g => g.ApplicationRoles)
                .WithRequired().HasForeignKey(ap => ap.ApplicationGroupId);
            modelBuilder.Entity<ApplicationGroupRole>().HasKey(gr =>
                new
                {
                    gr.ApplicationRoleId, gr.ApplicationGroupId
                }).ToTable("ApplicationGroupRoles");

        }

    }

    /// <summary>
    /// </summary>
    public class ApplicationGroup
    {
        /// <summary>
        /// </summary>
        public ApplicationGroup()
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// </summary>
        /// <param name="name"></param>
        public ApplicationGroup(string name)
            : this()
        {
            Name = name;
        }

        /// <summary>
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        public ApplicationGroup(string name, string description)
            : this(name)
        {
            Description = description;
        }

        /// <summary>
        /// </summary>
        [Key]
        public string Id { get; set; }
        /// <summary>
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// </summary>
        public virtual ICollection<ApplicationGroupRole> ApplicationRoles { get; set; }

        /// <summary>
        /// </summary>
        public virtual ICollection<ApplicationUserGroup> ApplicationUsers { get; set; }
    }

    public class ApplicationUserGroup
    {
        [Key]
        [Column(Order = 0)]
        public string ApplicationUserId { get; set; }
        [Key]
        [Column(Order = 1)]
        public string ApplicationGroupId { get; set; }
    }

    public class ApplicationGroupRole
    {
        [Key]
        [Column(Order = 0)]
        public string ApplicationGroupId { get; set; }
        [Key]
        [Column(Order = 1)]
        public string ApplicationRoleId { get; set; }
    }

    // Must be expressed in terms of our custom UserRole:
    public class ApplicationRole : IdentityRole<string, ApplicationUserRole>
    {
        public ApplicationRole()
        {
            Id = Guid.NewGuid().ToString();
        }

        public ApplicationRole(string name) : this()
        {
            Name = name;
        }

        // Add any custom Role properties/code here
    }

    public class ApplicationRoleStore : 
        RoleStore<ApplicationRole, string, ApplicationUserRole>
    {
        public ApplicationRoleStore() : base(new IdentityDbContext())
        {
            DisposeContext = true;
        }

        public ApplicationRoleStore(DbContext context) : base(context) { }
    }
}