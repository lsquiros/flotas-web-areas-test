﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ECOsystem.Models
{
#pragma warning disable 1591

    public class GroupViewModel
        {
            public GroupViewModel()
            {
            UsersList = new List<SelectListItem>();
            RolesList = new List<SelectListItem>();
            }
            [Required(AllowEmptyStrings = false)]
            public string Id { get; set; }
            [Required(AllowEmptyStrings = false)]
            public string Name { get; set; }
            public string Description { get; set; }
            public ICollection<SelectListItem> UsersList { get; set; }
            public ICollection<SelectListItem> RolesList { get; set; }
        }
        public class EditUserViewModel
        {
            public EditUserViewModel()
            {
            RolesList = new List<SelectListItem>();
            GroupsList = new List<SelectListItem>();
            }
            public string Id { get; set; }
            [Required(AllowEmptyStrings = false)]
            [Display(Name = "Email")]
            [EmailAddress]
            public string Email { get; set; }

            // We will still use this, so leave it here:
            public ICollection<SelectListItem> RolesList { get; set; }

            // Add a GroupsList Property:
            public ICollection<SelectListItem> GroupsList { get; set; }
        }

        public class RegisterViewModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

}