﻿/************************************************************************************************************
*  File    : ModelAncestor.cs
*  Summary : Model Ancestor
*  Author  : Berman Romero
*  Date    : 09/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Miscellaneous
{
#pragma warning disable 1591

    /// <summary>
    /// Model Ancestor
    /// </summary>
    public class ModelAncestor
    {
        public int? _loggedUserId;

        /// <summary>
        /// Logged UserId
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int? LoggedUserId
        {
            get {
                if (_loggedUserId != null)
                {
                    return _loggedUserId;
                }
                return Session.GetUserInfo().UserId;
                
              }
            set { _loggedUserId = value; }
        }

        private byte[] _rowVersion;

        /// <summary>
        /// Row Version to prevent Bad insert/update
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public byte[] RowVersion
        {
            get { return _rowVersion ?? (_rowVersion = new byte[] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 }); }
            set { _rowVersion = value; }
        }
    }
}