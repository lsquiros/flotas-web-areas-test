﻿/************************************************************************************************************
*  File    : CustomError.cs
*  Summary : Custom Error
*  Author  : Berman Romero
*  Date    : 09/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

namespace ECOsystem.Models.Miscellaneous
{
    /// <summary>
    /// Custom Error Model
    /// </summary>
    public class CustomError
    {
        /// <summary>
        /// Title of error
        /// </summary>
        public string TitleError { get; set; }

        /// <summary>
        /// Friendly Error Property in order to shows the error message
        /// </summary>
        public string FriendlyError
        {
            get { return "No se logró completar su solicitud en estos momentos, Por favor intentar nuevamente."; }
        }

        /// <summary>
        /// Detailed Technical Error
        /// </summary>
        public string TechnicalError { get; set; }

        /// <summary>
        /// Return ULR
        /// </summary>
        public string ReturnUlr { get; set; }

        /// <summary>
        /// Redirect URL
        /// </summary>
        public string RedirectUrl { get; set; }

    }

}