﻿/************************************************************************************************************
*  File    : ApiAncestor.cs
*  Summary : API Ancestor
*  Author  : Berman Romero
*  Date    : 09/19/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

namespace ECOsystem.Models.Miscellaneous
{
    /// <summary>
    /// Api Ancestor
    /// </summary>
    public class ApiAncestor
    {
        /// <summary>
        /// Logged User Id
        /// </summary>
        public int? LoggedUserId
        {
            get { return 0; }
        }

    }
}