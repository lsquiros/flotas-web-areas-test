﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Models.Miscellaneous
{
    /// <summary>
    /// Modelo para una configuración de servicio API
    /// </summary>
    public class ApiServiceModel
    {
        /// <summary>
        /// Llave de configuracion
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Valor de configuracion
        /// </summary>
        public string Value { get; set; }
    }
}