﻿/************************************************************************************************************
*  File    : SqlError.cs
*  Summary : Sql Error
*  Author  : Berman Romero
*  Date    : 09/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

namespace ECOsystem.Models.Miscellaneous
{
    /// <summary>
    /// Sql Error Model
    /// </summary>
    public class SqlError
    {
        /// <summary>
        /// Error Number
        /// </summary>
        public int ErrorNumber { get; set; }

        /// <summary>
        /// Error Severity
        /// </summary>
        public int ErrorSeverity { get; set; }
        
        /// <summary>
        /// Error State
        /// </summary>
        public int ErrorState { get; set; }

        /// <summary>
        /// Error Message
        /// </summary>
        public string ErrorMessage { get; set; }

    }
}