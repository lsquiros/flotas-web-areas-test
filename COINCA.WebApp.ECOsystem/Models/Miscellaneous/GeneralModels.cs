﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Models.Miscellaneous
{
    /// <summary>
    /// ControlCar Partner Info
    /// </summary>
    public class ControlCarPartnerInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int IdCRM { get; set; }
        public string RoleName { get; set; }
    }
}