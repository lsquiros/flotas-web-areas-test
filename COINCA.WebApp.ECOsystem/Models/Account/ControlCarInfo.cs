﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Models.Account
{
    public class ControlCarInfo
    {
        public string LVE_Servicios { get; set; }
        public string LVE_Info { get; set; }
        public string LVE_Robo { get; set; }
        public string LVE_UserTerms { get; set; }
        public string LVE_Numbers { get; set; }
    }
}