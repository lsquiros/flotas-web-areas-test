﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ECOsystem.Models.Account
{
    /// <summary>
    /// 
    /// </summary>
    public class ModulesBase
    {
        /// <summary>
        /// 
        /// </summary>
        public ModulesBase()
        {
            Data = new Modules();
            List = new List<Modules>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// 
        /// </summary>
        public Modules Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Modules> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    
    }

    /// <summary>
    /// 
    /// </summary>
    public class Modules
    {
        /// <summary>
        /// 
        /// </summary>
        public int ModuleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Nombre del Módulo")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Ico { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Action { get; set; }              

        /// <summary>
        /// 
        /// </summary>
        public int Order { get; set; }

    }
}