﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ECOsystem.Models.Account
{
    /// <summary>
    /// 
    /// </summary>
    public class AlarmsBase
    {
        /// <summary>
        /// 
        /// </summary>
        public AlarmsBase()
        {
            Data = new Alarms();
            List = new List<Alarms>();
        }

        /// <summary>
        /// 
        /// </summary>
        public Alarms Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Alarms> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<AccountMenus> Menus { get; set; }
    
    }

    /// <summary>
    /// 
    /// </summary>
    public class Alarms
    {
        /// <summary>
        /// 
        /// </summary>
        public int AlarmId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AlarmClass { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CountClass { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string View { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AlarmTitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool HasParameter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? ParameterValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LabelParameter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string URL { get; set; }   
    }
}