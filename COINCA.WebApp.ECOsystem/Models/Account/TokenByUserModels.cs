﻿using ECOsystem.Models.Core;
using Newtonsoft.Json;

namespace ECOsystem.Models.Account
{
    public class TokenByUserModel
    {
        public string  UserId { get; set; }

        public string  Context { get; set; }

        public string SerializeUserInfo { get; set; }

        public Users User { get { return JsonConvert.DeserializeObject<Users>(SerializeUserInfo); } } 
    }
}