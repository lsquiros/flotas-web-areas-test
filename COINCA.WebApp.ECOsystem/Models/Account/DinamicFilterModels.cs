﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace ECOsystem.Models.Account
{
    
    /// <summary>
    /// 
    /// </summary>
    public class DinamicFilterBase
    {
        /// <summary>
        /// 
        /// </summary>
        public DinamicFilterBase()
        {
            List = new List<DinamicFilter>();
            Data = new DinamicFilter();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// List of Dinamic Filters
        /// </summary>
        public IEnumerable<DinamicFilter> List { get; set; }

        /// <summary>
        /// Dinamic Filter Data
        /// </summary>
        public DinamicFilter Data { get; set; }

        /// <summary>
        /// List of Menus to show
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DinamicFilter
    {
        /// <summary>
        /// Id
        /// </summary>
        public int DinamicFilterId { get; set; }

        /// <summary>
        /// UserId
        /// </summary>
        public int UserId { get; set; }
        

        /// <summary>
        /// Filter
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// Description - Save the filter Value
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// CostCenterId
        /// </summary>
        public int CostCenterId { get; set; }

        /// <summary>
        /// CostCenterName
        /// </summary>
        public string CostCenterName { get; set; }

        /// <summary>
        /// VehicleId
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        public string VehicleName { get; set; }


        /// <summary>
        /// PlateId
        /// </summary>
        public string PlateId { get; set; }

        /// <summary>
        /// VehicleGroupId
        /// </summary>
        public int VehicleGroupId { get; set; }

        /// <summary>
        /// VehicleGroupName
        /// </summary>
        public string VehicleGroupName { get; set; }
        
        /// <summary>
        /// Values from the DB
        /// </summary>
        public int items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<SelectListItem> List { get; set; }
    }


    /// <summary>
    /// Get the data from the DB when the items have been checked 
    /// </summary>
    public class GetDataChecked
    {
        /// <summary>
        /// 
        /// </summary>
        public string items { get; set; }
    }
}

