﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Controllers.Account;
using ECOsystem.Models.Core;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Account
{
#pragma warning disable 1591

    /// <summary>
    /// User Roles Base
    /// </summary>
    public class UserRolesBase
    {
        /// <summary>
        /// 
        /// </summary>
        public UserRolesBase()
        {
            Data = new UsersRoles();
            List = new List<UsersRoles>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// 
        /// </summary>
        public UsersRoles Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<UsersRoles> List { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// User Permission Base
    /// </summary>
    public class UsersPermissionsBase
    {
        /// <summary>
        /// 
        /// </summary>
        public UsersPermissionsBase()
        {
            Data = new UsersPermissions();
            List = new List<UsersPermissions>();
            Menus = new List<AccountMenus>();
        }
        /// <summary>
        /// 
        /// </summary>
        public UsersPermissions Data { get; set; }

        public IEnumerable<UsersPermissions> List { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// User Roles Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class UsersRoles
    {
        /// <summary>
        /// RoleId
        /// </summary>
        [NotMappedColumn]
        public string RoleId { get; set; }

        /// <summary>
        ///Role Name
        /// </summary>
        [DisplayName("Nombre del Rol")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Name { get; set; }

        /// <summary>
        /// RoleParent
        /// </summary>
        [DisplayName("Perfíl")]
        public string Parent { get; set; }

        /// <summary>
        /// RoleParentName
        /// </summary>
        public string ParentName
        {
            get
            {
                return UserRolesPermissionController.RetrieveRoles().ToList().Where(p => p.Value == Parent).Select(p => p.Text).FirstOrDefault();
            }
        }
        /// <summary>
        /// RolesList
        /// </summary>
        public SelectList RolesList { get { return UserRolesPermissionController.RetrieveRoles(); } }

        public List<Customers> CustomerList { get; set; }
    }


    /// <summary>
    /// User Permissions MOdel
    /// </summary>
    public class UsersPermissions
    {
        /// <summary>
        /// PermissionsId
        /// </summary>
        [NotMappedColumn]
        public string PermissionsId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [DisplayName("Nombre del Permiso")]
        public string  Name { get; set; }

        /// <summary>
        /// Resource Controller
        /// </summary>
        [DisplayName("Nombre de la Tarea")]
        public string Resource { get; set; }

        /// <summary>
        /// Action Controller
        /// </summary>
        [DisplayName("Nombre de la Acción")]
        public string Action { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        [DisplayName("Descripción")]
        public string Description { get; set; }


        /// <summary>
        /// Partner
        /// </summary>
        [DisplayName("Role")]
        public string Role { get; set; }

        /// <summary>
        /// Parent Menu
        /// </summary>
        public int Parent { get; set; }

        /// <summary>
        /// View
        /// </summary>
        public bool View { get; set; }

        /// <summary>
        /// Edit
        /// </summary>
        public bool Edit { get; set; }

        /// <summary>
        /// Delete
        /// </summary>
        public bool Delete { get; set; }

        /// <summary>
        /// Menu
        /// </summary>
        public string MenuId { get; set; }
    }
}