﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECOsystem.Models.Account
{
    public class LoginDataModel
    {
        public int? UserId { get; set; }

        public string AspNetId { get; set; }

        public bool IsLocked { get; set; }

        public bool IsAuthenticate { get; set; }

        public bool EmailConfirmed { get; set; }

        public bool ChangePassword { get; set; }

        public bool IsActive { get; set; }
    }
}
