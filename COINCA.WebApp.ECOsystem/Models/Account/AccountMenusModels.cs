﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace ECOsystem.Models.Account
{
    /// <summary>
    /// Get the Menus information
    /// </summary>
    public class AccountMenus
    {
        /// <summary>
        /// MenuID
        /// </summary>
        public int MenuId { get; set; }

        /// <summary>
        /// MenuName
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Menu Ico
        /// </summary>
        public string Ico { get; set; }

        /// <summary>
        /// Menu Controller
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// Menu Action
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Menu Parent
        /// </summary>
        public int? Parent { get; set; }

        /// <summary>
        /// Menu Header
        /// </summary>
        public int Header { get; set; }

        /// <summary>
        /// Menu Order
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Menu Area
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// IsParent
        /// </summary>
        public bool IsParent 
        {
            get
            {
                if (Parent == null)
                    return true;
                return false;
            } 
        }

        /// <summary>
        /// 
        /// </summary>
        public string Active { get; set; }

        /// <summary>
        /// ParentName
        /// </summary>
        public string ParentName { get; set; }

        /// <summary>
        /// IsChecked values = true or false
        /// </summary>
        public string IsChecked { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ValidFields { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<SelectListItem> List { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string visibleMenu { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string showMenu { get; set; }

        /// <summary>
        /// Role Id
        /// </summary>
        public string RoleId { get; set; }


        public AccountMenus()
        {
            visibleMenu = "hidden";
            showMenu = "hidden";  
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class AccountMenusBase
    {

        /// <summary>
        /// 
        /// </summary>
        public AccountMenusBase()
        {
            Data = new AccountMenus();
            List = new List<AccountMenus>();
            RoleBase = new UserRolesBase();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// 
        /// </summary>
        public AccountMenus Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public UserRolesBase RoleBase { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
           
    }

    /// <summary>
    /// 
    /// </summary>
    public class ValidateCustomerModelList
    {
        public string Value { get; set; }

        public string Name { get; set; }
    }
}