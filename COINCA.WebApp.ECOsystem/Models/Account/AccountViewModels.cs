﻿/************************************************************************************************************
*  File    : LoginViewModel.cs
*  Summary : Login Models
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ECOsystem.Utilities;

namespace ECOsystem.Models.Account
{
    /// <summary>
    /// Login View Model Class for Login
    /// </summary>
    public class LoginViewModel
    {
        /// <summary>
        /// property
        /// </summary>
        [Display(Name = "Email")]
        public string EncryptedEmail { get; set; }
        //public string EncryptedEmail
        //{
        //    get { return Utilities.TripleDesEncryption.Decrypt(EncryptedEmail.ToLower()); }
        //    set { EncryptedEmail = value; }
        //}

        /// <summary>
        /// property
        /// </summary>
        [Required(ErrorMessage = "Correo es requerido.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Por favor digite un correo válido.")]
        public string DecryptedEmail
        {
            get { return TripleDesEncryption.Decrypt(EncryptedEmail); }
            set { EncryptedEmail = TripleDesEncryption.Encrypt(value.ToLower()); }
            //set { EncryptedEmail = Utilities.TripleDesEncryption.Encrypt(value); }
            //set { EncryptedEmail = value.ToLower(); }
        }

        /// <summary>
        /// property
        /// </summary>
        [Required(ErrorMessage = "Digite una contraseña.")]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [Display(Name = "¿Recordarme?")]
        public bool RememberMe { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public int? TimezoneOffset { get; set; }


        /// <summary>
        /// property
        /// </summary>
        public int? FilterType { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public int? CustomerId { get; set; }


        /// <summary>
        /// </summary>
        public IEnumerable<SelectListItem> CList { get; set; }


    }

    /// <summary>
    /// Reset Password View Model Class
    /// </summary>
    public class ResetPasswordViewModel
    {
        /// <summary>
        /// Email property
        /// </summary>
        [Required(ErrorMessage = "Correo es requerido")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        /// <summary>
        /// Password property
        /// </summary>
        [Required(ErrorMessage = "Contraseña es requerida.")]
        [StringLength(100, ErrorMessage = "La contraseña debe contener mínimo 6 caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        /// <summary>
        /// Confirm Password property
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Confirmación de contraseña invalida")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Token for reset the password property
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Token for reset the password property
        /// </summary>
        public bool FirstTime { get; set; }

        /// <summary>
        /// Password expired indicator property
        /// </summary>
        public bool IsPasswordExpired { get; set; }

        /// <summary>
        /// Error indicator property
        /// </summary>
        public bool Error { get; set; }

        /// <summary>
        /// Error message property
        /// </summary>
        [Display(Name = "")]
        public string ErrorMessage { get; set; }
    
    }

    /// <summary>
    /// ForgotPasswordViewModel
    /// </summary>
    public class ForgotPasswordViewModel
    {
        /// <summary>
        /// Email property
        /// </summary>
        [Required(ErrorMessage = "Correo es requerido.")]
        [EmailAddress(ErrorMessage = "Por favor digite un correo válido.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    /// <summary>
    /// PasswordsHistory Model
    /// </summary>
    public class PasswordsHistoryModel
    {
        /// <summary>
        /// User Id property
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Password Date property
        /// </summary>
        public DateTime PasswordDate { get; set; }

        private string _password;

        /// <summary>
        /// Password Encrypted property
        /// </summary>
        public string Password
        {
            get { return _password; }
            set { _password = TripleDesEncryption.Encrypt(value); }
        }

    }
}
