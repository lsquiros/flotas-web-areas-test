﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Models.Core
{
    public class VehicleCommands
    {
        /// <summary>
        /// VehicleId
        /// </summary>
        public int VehicleId { get; set; }
        /// <summary>
        /// VehicleName
        /// </summary>
        public string VehicleName { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// StatusDate
        /// </summary>
        public DateTime StatusDate { get; set; }
        /// <summary>
        /// PhoneNumber
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Commands
        /// </summary>
        public List<Command> Commands { get; set; }        
    }
    public class Command
    {
        public string Type { get; set; }
        public Boolean Available { get; set; }
        public int Delay { get; set; }
        public int WaitingTime { get; set; }
        public string Call { get; set; }
        public List<string> Online { get; set; }
        public List<string> Enables { get; set; }
        public List<string> Disables { get; set; }
        public string Description { get; set; }
    }

    public class VehicleCommandReport
    {
        public string Placa { get; set; }
        public string Comando_Ejecutado { get; set; }
        public DateTime Fecha_Ejecucion { get; set; }
        public string Usuario { get; set; }
        public string Origen { get; set; }
        public string MetodoEnvio { get; set; }
    }
}