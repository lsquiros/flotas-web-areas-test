﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Core
{
    /// <summary>
    /// Currencies Base
    /// </summary>
    public class PartnersNewsBase
    {
        /// <summary>
        /// PartnersBase default constructor 
        /// </summary>
        public PartnersNewsBase()
        {
            Data = new PartnerNews();
            List = new List<PartnerNews>();
            ListNotifications = new List<PartnerNotifications>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public PartnerNews Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PartnerNews> List { get; set; }

        public List<PartnerNotifications> ListNotifications { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

    } 

    /// <summary>
    /// Partner News
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PartnerNews : ModelAncestor
    {
        /// <summary>
        /// NewsId
        /// </summary>
        [NotMappedColumn]
        public int? NewsId { get; set; }

        /// <summary>
        /// PartnerId
        /// </summary>
        [DisplayName("Entidad Bancaria")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int PartnerId { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [DisplayName("Título")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string Title { get; set; }

        /// <summary>
        /// Content
        /// </summary>
        [DisplayName("Contenido")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string Content { get; set; }

        /// <summary>
        /// Image
        /// </summary>
        [NotMappedColumn]
        public string Image { get; set; }

        /// <summary>
        /// Active
        /// </summary>
        [DisplayName("Activo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public bool IsActive { get; set; }

    }

    public class PartnerNotifications
    {
        public int? Id { get; set; }

        [DisplayName("Título")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Title { get; set; }

        [DisplayName("Mensaje")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Message { get; set; }

        public string Content { get; set; }
                
        public DateTime? SendDate { get; set; }

        [DisplayName("Fecha Envio")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string SendDateStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(SendDate); }
            set { SendDate = Utilities.Miscellaneous.SetDate(value); }
        }

        public string Xml
        {
            get
            {
                var list = string.IsNullOrEmpty(Content) ? new List<string>() : Content.Trim().Replace("\n", string.Empty).Replace("\r", string.Empty).Split(';').ToList().Distinct().ToList();
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Contains("@")) list[i] = Utilities.TripleDesEncryption.Encrypt(list[i].Trim());
                }
                return Utilities.Miscellaneous.GetXML(list);
            }
            set
            {
                var list = value == null ? new List<string>() : Utilities.Miscellaneous.GetXMLDeserialize<List<string>>(value, string.Empty);
                for (int i = 0; i < list.Count; i++)
                {
                    list[i] = Utilities.TripleDesEncryption.Decrypt(list[i]);
                }
                Content = string.Join(";", list.Distinct());
            }
        }

        public bool Active { get; set; }
    }
}