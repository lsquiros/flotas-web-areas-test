﻿using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ECOsystem.Models
{
    public class ProgramSendReportsBase
    {
        public ProgramSendReportsBase()
        {
            Data = new ProgramSendReportsShow();
            List = new List<ProgramSendReportsShow>();
            Menus = new List<AccountMenus>();
            ReportsByModules = new List<ProgramSendReportModule>();
        }

        public ProgramSendReportsShow Data { get; set; }
        public IEnumerable<ProgramSendReportsShow> List { get; set; }

        public IEnumerable<ProgramSendReportsShow> GridList { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

        public IEnumerable<ProgramSendReportModule> ReportsByModules { get; set; }
    }

    public class ProgramSendReportModule
    {
        public string Name { get; set; }

        public char Module { get; set; }

        public SelectList Reports { get; set; }

        public IEnumerable<ProgramSendReportsShow> Data { get; set; }
    }

    public class ProgramSendReportsShow
    {
        public int Id { get; set; }

        public int ReportId { get; set; }

        public string ReportName { get; set; }

        public string DownloadName { get; set; }

        public string DownloadNameToShow
        {
            get { return DownloadName == null || DownloadName == "" ? "Todos" : DownloadName; }
        }

        public string Emails { get; set; }

        public string EmailsToShow
        {
            get { return Emails == null ? "" : (Emails.Count(q => q == ';') > 0 ? (Emails.Split(';')[0] + " ...") : Emails); }
        }

        public string Module { get; set; }

        public int UserId { get; set; }

        public string EncryptedUserEmail { get; set; }

        public string UserEmail
        {
            get { return TripleDesEncryption.Decrypt(EncryptedUserEmail).Trim(); }
        }

        public string EncryptedName { get; set; }

        public string Name
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
        }

        public string User { get { return UserId == -1 ? "Todos" : (Name + " " + UserEmail); } }

        public DateTime? StartDate { get; set; }

        public string StartDateStr { get { return StartDate == null ? null : DateTime.Parse(StartDate.ToString()).ToString("dd/MM/yyyy"); } }

        public DateTime? EndDate { get; set; }

        public string EndDateStr { get { return EndDate == null ? null : DateTime.Parse(EndDate.ToString()).ToString("dd/MM/yyyy"); } }

        public int? CustomerId { get; set; }

        public int? PartnerId { get; set; }

        public int? VehicleId { get; set; }

        public int? CostCenterId { get; set; }

        public string CostCenterName { get; set; }

        public int? VehicleGroupId { get; set; }

        public int? VehicleUnitId { get; set; }

        public int? DriverId { get; set; }

        public int Elapse { get; set; }

        public string ElapseStr
        {
            get
            {
                switch (Elapse)
                {
                    case 1:
                        return "Diario";
                    case 7:
                        return "Semanal";
                    case 30:
                        return "Mensual";
                    default:
                        return string.Empty;
                }
            }
        }

        public string Days { get; set; }

        public string DaysNames
        {
            get
            {
                if (Elapse == 7)
                {
                    var numbers = Days.Split(',');
                    string result = "";
                    var last = numbers.Last();

                    foreach (var number in numbers.Where(x => x != ""))
                    {
                        if (last == number)
                        {
                            var culture = new CultureInfo("es-ES");
                            result += culture.DateTimeFormat.DayNames[Convert.ToInt32(number) - 1];
                        }
                        else
                        {
                            var culture = new CultureInfo("es-ES");
                            result += culture.DateTimeFormat.DayNames[Convert.ToInt32(number) - 1] + ", ";
                        }


                    }
                    return result;
                }
                else if (Elapse == 30)
                {
                    return Days;
                }
                else
                {
                    return "Todos";
                }                
            }
        }

        public bool Active { get; set; }

        public int VehicleName { get; set; }

        public bool TerminalDetail { get; set; }

        public bool IsAlert { get; set; }

        public ReportParameters Parameters { get; set; }

        public List<ProgramSummaryEventByCostCenters> CostCenterList { get; set; }
    }

    public class ProgramSendReports
    {
        [ScriptIgnore]
        public int Id { get; set; }

        [ScriptIgnore]
        public int ReportId { get; set; }

        [ScriptIgnore]
        public string ReportName { get; set; }

        [ScriptIgnore]
        public string DownloadName { get; set; }

        [ScriptIgnore]
        public DateTime? StartDate { get; set; }

        [ScriptIgnore]
        public string StartDateStr { get { return StartDate == null ? null : DateTime.Parse(StartDate.ToString()).ToString("dd/MM/yyyy"); } }

        [ScriptIgnore]
        public DateTime? EndDate { get; set; }

        [ScriptIgnore]
        public string EndDateStr { get { return EndDate == null ? null : DateTime.Parse(EndDate.ToString()).ToString("dd/MM/yyyy"); } }

        [ScriptIgnore]
        public int? CustomerId { get; set; }

        [ScriptIgnore]
        public int? PartnerId { get; set; }

        [ScriptIgnore]
        public int? VehicleId { get; set; }

        [ScriptIgnore]
        public int? VehicleUnitId { get; set; }

        [ScriptIgnore]
        public int? DriverId { get; set; }

        [ScriptIgnore]
        public int Elapse { get; set; }

        [ScriptIgnore]
        public string Days { get; set; }

        [ScriptIgnore]
        public bool Active { get; set; }

        [ScriptIgnore]
        public int VehicleName { get; set; }

        [ScriptIgnore]
        public bool TerminalDetail { get; set; }

        [ScriptIgnore]
        public bool IsAlert { get; set; }

        [ScriptIgnore]
        public List<ProgramSummaryEventByCostCenters> CostCenterList { get; set; }

        public string Module { get; set; }

        [ScriptIgnore]
        public int? CostCenterId { get; set; }

        [ScriptIgnore]
        public int? VehicleGroupId { get; set; }

        [ScriptIgnore]
        public string Emails { get; set; }
       
        public int UserId { get; set; }
    }

    public class ProgramSummaryEventByCostCenters
    {
        public int? CostCenterId { get; set; }

        public int? Id { get; set; }

        public string CostCenterEmails { get; set; }

        public string CostCenterName { get; set; }
    }

    public class ReportParameters
    {
        public SelectList CostCenterOrVehicleGroupSelect {
            get {
                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(1, "Centro de costo");
                dictionary.Add(0, "Grupos de Vehículos");
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public SelectList EmailOrUserSelect
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(0, "Correo electrónico");
                dictionary.Add(1, "Usuarios");
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public string ModuleName { get; set; }

        public int Id { get; set; }

        public int ReportId { get; set; }

        public bool Customer { get; set; }

        public bool Vehicle { get; set; }

        public bool CostCenter { get; set; }

        public bool VehicleGroup { get; set; }

        public bool VehicleUnit { get; set; }

        public bool Driver { get; set; }

        public bool StartDateShow { get; set; }

        public bool EndDateShow { get; set; }

        public bool CostCenterTable { get; set; }

        public bool MainEmail { get; set; }

        public bool TerminalDetail { get; set; }

        public bool IsAlert { get; set; }
    }
}