﻿/************************************************************************************************************
*  File    : ApplicantsReportModels.cs
*  Summary : Applicants Report Models
*  Author  : Danilo Hidalgo
*  Date    : 12/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Core
{
    /// <summary>
    /// Applicants Report Base
    /// </summary>
    public class ApplicantsReportBase
    {
        /// <summary>
        /// Applicants Reports Base Constructor
        /// </summary>
        public ApplicantsReportBase()
        {
            Parameters = new AdministrationReportsBase();
            List = new List<dynamic>();
            UsersList = new List<SelectListItem>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public AdministrationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// List of users
        /// </summary>
        public IEnumerable<SelectListItem> UsersList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
}

    /// <summary>
    /// Applicants Report Model
    /// </summary>
    public class ApplicantsReport
    {
        /// <summary>
        /// Encrypted CustomerName
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Empresa", Width = "120px", SortEnabled = true)]
        public string CustomerName { get; set; }

        /// <summary>
        /// Decryped CustomerName
        /// </summary>
        [DisplayName("Empresa")]
        [ExcelMappedColumn("Empresa")]
        [GridColumn(Title = "Empresa", Width = "120px", SortEnabled = true)]
        public string DecrypedCustomerName {
            get { return TripleDesEncryption.Decrypt(CustomerName); }
            set { CustomerName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Fecha", Width = "120px", SortEnabled = true)]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha")]
        [ExcelMappedColumn("Fecha")]
        [GridColumn(Title = "Fecha", Width = "120px", SortEnabled = true)]
        public string DateStr { get { return Utilities.Miscellaneous.GetDateFormat(Date); } }

        /// <summary>
        /// CreditCardType
        /// </summary>
        [DisplayName("Tipo Tarjeta")]
        [ExcelMappedColumn("Tipo Tarjeta")]
        [GridColumn(Title = "Tipo Tarjeta", Width = "120px", SortEnabled = true)]
        public string CreditCardType { get; set; }

        /// <summary>
        /// CurrencyName
        /// </summary>
        [DisplayName("Moneda")]
        [ExcelMappedColumn("Moneda")]
        [GridColumn(Title = "Moneda", Width = "120px", SortEnabled = true)]
        public string CurrencyName { get; set; }

        /// <summary>
        /// FleetCard
        /// </summary>
        [DisplayName("FleetCard")]
        [ExcelMappedColumn("FleetCard")]
        [GridColumn(Title = "FleetCard", Width = "120px", SortEnabled = true)]
        public string FleetCard { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        [DisplayName("Cantidad Tarjetas")]
        [ExcelMappedColumn("Cantidad Tarjetas")]
        [GridColumn(Title = "Cantidad Tarjetas", Width = "120px", SortEnabled = true)]
        public int Quantity { get; set; }

        /// <summary>
        /// CountryName
        /// </summary>
        [DisplayName("País")]
        [ExcelMappedColumn("País")]
        [GridColumn(Title = "País", Width = "120px", SortEnabled = true)]
        public string CountryName { get; set; }

    }
}
