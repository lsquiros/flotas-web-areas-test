﻿using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Utilities;

namespace ECOsystem.Models.Core
{
#pragma warning disable 1591


    public class CustomerByPartnerBase
    {

        public CustomerByPartnerBase()
        {
            Data = new CustomerByPartner();
            List = new List<CustomerByPartner>();
        }

        public CustomerByPartner Data { get; set; }

        public List<CustomerByPartner> List { get; set; }
    
    }

    public class CustomerByPartner
    {

        /// <summary>
        ///User Id
        /// </summary>
        [DisplayName("Id de usuario")]
        public int? UserId { get; set; }

        ///<summary>
        ///UserNameEncrypted
        ///</summary>
        [DisplayName("Nombre Usuario")]
        public string EncryptedUserName { get; set; }

        ///<summary>
        ///UserNameDecrypted
        ///</summary>
        [DisplayName("Nombre Usuario")]
        public string DecryptedUserName {
            get { return TripleDesEncryption.Decrypt(EncryptedUserName); }
            set { EncryptedUserName = TripleDesEncryption.Encrypt(value); }
        
        }

        ///<summary>
        ///CustomerNameEncrypted
        ///</summary>
        [DisplayName("Nombre Customer")]
        public string EncryptedCustomerName { get; set; }

        ///<summary>
        ///CustomerNameDecrypted
        ///</summary>
        [DisplayName("Nombre Customer")]
        public string DecryptedCustomerName {
            get { return TripleDesEncryption.Decrypt(EncryptedCustomerName); }
            set { EncryptedCustomerName = TripleDesEncryption.Encrypt(value); }
        }

        ///<summary>
        ///CustomerId
        ///</summary>
        [DisplayName("Id Customer")]
        public int CustomerId { get; set; }

        ///<summary>
        ///PartnerName
        ///</summary>
        [DisplayName("Nombre Partner")]
        public string PartnerName { get; set; }

        ///<summary>
        ///PartnerId
        ///</summary>
        [DisplayName("Partner Id")]
        public int PartnerId { get; set; }

        ///<summary>
        ///RoleId
        ///</summary>
        [DisplayName("Role Id")]
        public string RoleId { get; set; }

        ///<summary>
        ///RoleName
        ///</summary>
        [DisplayName("Role Name")]
        public string RoleName { get; set; }
    }
}