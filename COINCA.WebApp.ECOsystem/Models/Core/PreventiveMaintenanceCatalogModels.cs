﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
/************************************************************************************************************
*  File    : PreventiveMaintenanceCatalogModels.cs
*  Summary : Preventive Maintenance Catalog Models
*  Author  : Cristian Martínez
*  Date    : 01/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

namespace ECOsystem.Models.Core
{
    /// <summary>
    /// Operators Base Class for Views
    /// </summary>
    public class PreventiveMaintenanceCatalogBase
    {
        /// <summary>
        /// PreventiveMaintenanceBase Constructor
        /// </summary>
        public PreventiveMaintenanceCatalogBase()
        {
            Data = new PreventiveMaintenanceCatalog();
            List = new List<PreventiveMaintenanceCatalog>();
            CostList = new List<PreventiveMaintenanceCost>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public PreventiveMaintenanceCatalog Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceCatalog> List { get; set; }

        /// <summary>
        /// List of entities to use it for cost grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceCost> CostList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }

    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceCatalog : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PreventiveMaintenanceCatalogId { get; set; }

        /// <summary>
        /// Kilometers Frequency
        /// </summary>
        [DisplayName("Frecuencia (Km)")]
        public double? FrequencyKm { get; set; }

        /// <summary>
        /// Kilometers Frequency with format
        /// </summary>
        [DisplayName("Frecuencia (Km)")]
        [NotMappedColumn]
        [GridColumn(Title = "Frecuencia (Km)", Width = "250px", SortEnabled = true)]
        public string FrequencyKmStr
        {
            get { return (FrequencyKm != null) ? Utilities.Miscellaneous.GetNumberFormat((decimal)FrequencyKm) : Utilities.Miscellaneous.GetNumberFormat(0); }
        }

        /// <summary>
        /// Alert Before of Km
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Alertar Antes (Km)")]
        public int? AlertBeforeKm { get; set; }

        /// <summary>
        /// Month Frequency
        /// </summary>
        [DisplayName("Frecuencia (Meses)")]
        [GridColumn(Title = "Frecuencia (Meses)", Width = "250px", SortEnabled = true)]
        public double? FrequencyMonth { get; set; }

        /// <summary>
        /// Alert Before of Month
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Alertar Antes (Meses)")]
        public int? AlertBeforeMonth { get; set; }

        /// <summary>
        /// Date Frequency
        /// </summary>
        [DisplayName("Frecuencia (Fecha)")]
        public DateTime? FrequencyDate { get; set; }

        /// <summary>
        /// Date of Preventive Maintenance entity
        /// </summary>
        [DisplayName("Frecuencia (Fecha)")]
        [GridColumn(Title = "Frecuencia (Fecha)", Width = "250px", SortEnabled = true)]
        //[RequiredIf("FrequencyMonth === null && FrequencyKm === null", ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string FrequencyDateStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(FrequencyDate); }
            set { FrequencyDate = Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Currency Symbol 
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Alert Before of Date
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Alertar Antes (Fecha)")]
        public int? AlertBeforeDate { get; set; }

        ///// <summary>
        ///// Frequency of Preventive Maintenance
        ///// </summary>
        //[DisplayName("Frecuencia")]
        //[RequiredIf("FrequencyTypeId == 300 || FrequencyTypeId == 301", ErrorMessage = "{0} es requerido")]
        //[GridColumn(Title = "Frecuencia", Width = "250px", SortEnabled = true)]
        //public string Frequency { get; set; }

        ///// <summary>
        ///// Name of Frequency Type
        ///// </summary>
        //[DisplayName("Tipo Frecuencia")]
        //[GridColumn(Title = "Tipo Frecuencia", Width = "250px", SortEnabled = true)]
        //public string FrequencyName { get; set; }

        ///// <summary>
        ///// ID Frequency Type of Preventive Maintenance
        ///// </summary>
        //[NotMappedColumn]
        //[DisplayName("Tipo Frecuencia")]
        //[Required(ErrorMessage = "{0} es requerido")]
        //public int FrequencyTypeId { get; set; }

        ///// <summary>
        ///// Alert Before of Preventive Maintenance 
        ///// </summary>
        //[NotMappedColumn]
        //[DisplayName("Alertar Antes")]
        //public String AlertBefore { get; set; }

        ///// <summary>
        ///// String Alert Before of Preventive Maintenance
        ///// </summary>
        //[DisplayName("Alertar Antes")]
        //[RequiredIf("FrequencyTypeId != 302", ErrorMessage = "{0} es requerido")]
        //[GridColumn(Title = "Alertar Antes", Width = "150px", SortEnabled = true)]
        //public string AlertBeforeStr
        //{
        //    get
        //    {
        //        return AlertBefore + " " + FrequencyName;
        //    }
        //    set
        //    {
        //        AlertBefore = value;
        //    }
        //}

        /// <summary>
        /// Description of Preventive Maintenance entity
        /// </summary>
        [DisplayName("Nombre")]
        [GridColumn(Title = "Nombre", Width = "400px", SortEnabled = true)]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Description { get; set; }

        ///// <summary>
        ///// Date Maintenance Preventive 
        ///// </summary>
        //[NotMappedColumn]
        //[DisplayName("Fecha de Mantenimiento")]
        //public DateTime? DateMaintenancePreventive { get; set; }

        /// <summary>
        /// Description of Preventive Maintenance entity
        /// </summary>
        [DisplayName("Costo")]
        public decimal Cost { get; set; }

        /// <summary>
        /// Cost with format
        /// </summary>
        [DisplayName("Costo")]
        [GridColumn(Title = "Costo", Width = "100px", SortEnabled = true)]
        public string CostStr {
            get { return Utilities.Miscellaneous.GetCurrencyFormat(Cost, CurrencySymbol); }
        }

        ///// <summary>
        ///// Date of Preventive Maintenance entity
        ///// </summary>
        //[DisplayName("Fecha de Mantenimiento")]
        //[RequiredIf("FrequencyTypeId == 302", ErrorMessage = "{0} es requerido")]
        //[NotMappedColumn]
        //public string DateMaintenancePreventiveStr
        //{
        //    get { return Utilities.Miscellaneous.GetDateFormat(DateMaintenancePreventive); }
        //    set { DateMaintenancePreventive = Utilities.Miscellaneous.SetDate(value); }
        //}

        ///// <summary>
        ///// Date of Preventive Maintenance entity
        ///// </summary>
        //[DisplayName("Periodicidad")]
        //[RequiredIf("FrequencyTypeId == 302", ErrorMessage = "{0} es requerido")]
        //[NotMappedColumn]
        //public int? PeriodicityTypeId { get; set; }

        /// <summary>
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<PreventiveMaintenanceCost> CostList { get; set; }


    }



}
