﻿/************************************************************************************************************
*  File    : CustomersModels.cs
*  Summary : Customers Models
*  Author  : Berman Romero
*  Date    : 09/15/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using GridMvc.DataAnnotations;

#pragma warning disable CS1591
namespace ECOsystem.Models.Core
{
    public class CustomersBase
    {
        public CustomersBase()
        {
            Data = new Customers();
            List = new List<Customers>();
            Menus = new List<AccountMenus>();
        }

        public Customers Data { get; set; }

        public IEnumerable<Customers> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

        public bool CanCreateCustomer
        {
            get
            {
                return Session.UserCanCreateCustomer();
            }
        }

    }

    public class CustomersEditBase
    {
        public CustomersEditBase()
        {
            AddInfo = new CustomerAdditionalInformation();
            Data = new Customers();
            Card = new CustomerCreditCards();
            List = new List<CustomerCreditCards>();
            CardRequest = new CardRequest();
            RequestList = new List<CardRequest>();
            UserList = new List<Users>();
            User = new Users();
            CustomerBudget = new CustomerBudget();
            Menus = new List<AccountMenus>();
            DataTerminal = new CustomerTerminals();
            Vehicles = new List<Vehicles>();
            Vehicle = new Vehicles();
            ContractInformation = new CustomerContractInformation();
            ContractsInformation = new List<CustomerContractInformation>();
            CustomerBinnacleInformation = new CustomerBinnacleBase();
            BranchCustomerList = new List<BranchCustomers>();
        }

        public CustomerAdditionalInformation AddInfo { get; set; }

        public Customers Data { get; set; }

        public CustomerCreditCards Card { get; set; }

        public IEnumerable<CustomerCreditCards> List { get; set; }

        public IEnumerable<CardRequest> RequestList { get; set; }

        public CustomerTerminals DataTerminal { get; set; }

        public CardRequest CardRequest { get; set; }

        public IEnumerable<Users> UserList { get; set; }

        public Users User { get; set; }

        public CustomerBudget CustomerBudget { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

        public Vehicles Vehicle { get; set; }

        public List<Vehicles> Vehicles { get; set; }

        public List<CustomerContractInformation> ContractsInformation { get; set; }

        public CustomerContractInformation ContractInformation { get; set; }

        public CustomerBinnacleBase CustomerBinnacleInformation { get; set; }

        public IEnumerable<BranchCustomers> BranchCustomerList { get; set; }

    }

    public class Customers : ModelAncestor
    {
        [DisplayName("FleetIO")]
        [NotMappedColumn]
        public bool HasFleetio { get; set; }

        /// <summary>
        /// InitialMargin
        /// </summary>
        public int InitialMargin { get; set; }

        /// <summary>
        /// FinalMargin
        /// </summary>
        public int FinalMargin { get; set; }

        /// <summary>
        /// FuelMargin
        /// </summary>
        public string FuelMargin { get { return $"{InitialMargin}-{FinalMargin}";  } }

        /// <summary>
        /// Permission to assign several fuels
        /// </summary>
        [DisplayName("¿Permiso de asignacion a varios combustibles?")]
        [NotMappedColumn]
        public bool FuelPermit { get; set; }

        [NotMappedColumn]
        public int? CustomerId { get; set; }

        [NotMappedColumn]
        public int? TerminalId { get; set; }

        [NotMappedColumn]
        public bool IsDefault { get; set; }

        [DisplayName("País")]
        [NotMappedColumn]
        public int? CountryId { get; set; }

        [DisplayName("País")]
        [NotMappedColumn]
        public string CountryName { get; set; }

        [DisplayName("Código País")]
        [NotMappedColumn]
        public string CountryCode { get; set; }

        public int TimeZone { get; set; }

        [DisplayName("Nombre del Cliente")]
        [NotMappedColumn]
        public string EncryptedName { get; set; }

        [DisplayName("Nombre del Cliente")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre del Cliente", Width = "300px", SortEnabled = true)]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }

        [DisplayName("Moneda")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int CurrencyId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Moneda")]
        [GridColumn(Title = "Moneda", Width = "50px")]
        public string CurrencyName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Logo")]
        [NotMappedColumn]
        public string Logo { get; set; }


        private string _accountNumber;

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Número de Cuenta")]
        [NotMappedColumn]
        public string AccountNumber
        {
            get { return _accountNumber ?? ""; }
            set { _accountNumber = value; }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Número de Cuenta")]
        [GridColumn(Title = "Número de Cuenta", Width = "150px")]
        public string RestrictAccountNumber { get { return Utilities.Miscellaneous.GetDisplayAccountNumberMask(DisplayAccountNumber); } }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Número de Cuenta")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        [NotMappedColumn]
        public string DisplayAccountNumber
        {
            get { return TripleDesEncryption.Decrypt(AccountNumber); }
            set { AccountNumber = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Activo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public bool IsActive { get; set; }

        /// <summary>
        /// TransactionsOffline
        /// </summary>
        [NotMappedColumn]
        public bool TransactionsOffline { get; set; }

        public bool SMSAlarms { get; set; }

        /// <summary>
        /// DemoMode
        /// </summary>
        [NotMappedColumn]
        public bool isDemo { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Unit Of Capacity Id from Types
        /// </summary>
        [DisplayName("Unidad de Capacidad")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? UnitOfCapacityId { get; set; }

        /// <summary>
        /// Unit Of Capacity Name from Types
        /// </summary>
        [NotMappedColumn]
        public string UnitOfCapacityName { get; set; }

        /// <summary>
        /// Issue For Id from Types
        /// </summary>
        [DisplayName("Emisión de Tarjetas por")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? IssueForId { get; set; }

        /// <summary>
        /// Credit Card Type
        /// </summary>
        [GridColumn(Title = "Fleetcard", Width = "150px")]
        public string IssueForStr { get; set; }


        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Currency Symbol Dollar
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbolDollar { get { return "$"; } }


        /// <summary>
        /// Credit Card Type
        /// </summary>
        [DisplayName("Tipo de Tarjeta")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string CreditCardType { get; set; }


        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        [NotMappedColumn]
        public DateTime? InsertDate { get; set; }

        /// <summary>
        /// Insert Date as string
        /// </summary>
        [GridColumn(Title = "Creado", Width = "100px")]
        public string InsertDateStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(InsertDate); }
        }

        /// <summary>
        /// Admin Email
        /// </summary>
        [DisplayName("Correo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string AdminEmail { get; set; }

        /// <summary>
        /// Admin Identification
        /// </summary>
        [DisplayName("Identificación")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string AdminIdentification { get; set; }

        /// <summary>
        /// Admin Name
        /// </summary>
        [DisplayName("Administrador")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string AdminName { get; set; }

        /// <summary>
        /// Modulos Disponibles
        /// </summary>
        [DisplayName("Módulos habilitados")]
        [NotMappedColumn]
        public string AvailableModules { get; set; }

        /// <summary>
        /// Modulo E
        /// </summary>
        [DisplayName("Eficiencia")]
        [NotMappedColumn]
        public bool ModuleEfficiency { get; set; }

        /// <summary>
        /// Modulo C
        /// </summary>
        [DisplayName("Control")]
        [NotMappedColumn]
        public bool ModuleControl { get; set; }

        /// <summary>
        /// Modulo O
        /// </summary>
        [DisplayName("Operación")]
        [NotMappedColumn]
        public bool ModuleOperation { get; set; }

        /// <summary>
        /// Modulo O
        /// </summary>
        [DisplayName("Gestión")]
        [NotMappedColumn]
        public bool ModuleManagement { get; set; }

        /// <summary>
        /// Costo de Administración String
        /// </summary>
        [DisplayName("Costo Tarjeta Administ.")]
        [NotMappedColumn]
        public string CostCardAdminStr { get; set; }

        /// <summary>
        /// Costo de Administración
        /// </summary>
        [NotMappedColumn]
        public decimal CostCardAdmin { get; set; }

        /// <summary>
        /// Costo de Emision String
        /// </summary>
        [DisplayName("Costo Tarjeta Emision")]
        [NotMappedColumn]
        public string CostCardEmisionStr { get; set; }

        /// <summary>
        /// Costo de Emision
        /// </summary>
        [NotMappedColumn]
        public decimal CostCardEmision { get; set; }

        /// <summary>
        /// Costo de ModuleEO String
        /// </summary>
        [DisplayName("Costo Modulo E/O")]
        [NotMappedColumn]
        public string CostModuleEOStr { get; set; }

        /// <summary>
        /// Costo de ModuleEO
        /// </summary>
        [NotMappedColumn]
        public decimal CostModuleEO { get; set; }

        /// <summary>
        /// Modality of GPS
        /// </summary>
        [DisplayName("Modalidad de Uso del Dispositivo")]
        [NotMappedColumn]
        public int? GPSModality { get; set; }

        /// <summary>
        /// Costo de Equipo Vendido
        /// </summary>
        [DisplayName("Costo Equipo Coinca")]
        public decimal CostRentedGPS { get; set; }

        [DisplayName("Costo Equipo BAC")]
        public decimal CostRentedBAC { get; set; }

        /// <summary>
        /// Costo de ModuleEO String
        /// </summary>
        [DisplayName("Costo Modalidad")]
        [NotMappedColumn]
        public string CostRentedGPSStr { get; set; }

        [DisplayName("Costo Modalidad")]
        [NotMappedColumn]
        public string CostRentedBACStr { get; set; }

        /// <summary>
        /// Membership Fee
        /// </summary>
        [DisplayName("Monitoreo")]
        public decimal MembershipFee { get; set; }

        /// <summary>
        /// Membership Fee : String
        /// </summary>
        [DisplayName("Monitoreo")]
        [NotMappedColumn]
        public string MembershipFeeStr { get; set; }

        /// <summary>
        /// Cost card reposition: decimal
        /// </summary>
        [DisplayName("Costo Reposición Tarjeta")]
        [NotMappedColumn]
        public decimal CostCardReposition { get; set; }

        /// <summary>
        /// Cost card reposition : String
        /// </summary>
        [DisplayName("Costo Reposición Tarjeta")]
        [NotMappedColumn]
        public string CostCardRepositionStr { get; set; }

        /// <summary>
        /// SAP CODE of Employee
        /// </summary>
        [DisplayName("Número de SAP")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public string SapCode { get; set; }

        /// <summary>
        /// RoleName of the Customer
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// RoleName of the Customer
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// RoleName of the Customer Temp
        /// </summary>
        public string RoleIdTemp { get; set; }

        /// <summary>
        /// RoleName of the Customer Temp
        /// </summary>
        public string RoleNameTemp { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// CustomerLogo
        /// </summary>
        public string CustomerLogo { get; set; }

        /// <summary>
        /// CostCenterByDriver
        /// </summary>
        public bool CostCenterByDriver { get; set; }

        /// <summary>
        /// Cantidad de Vehiculos Activos
        /// </summary>
        public int VehicleCant { get; set; }

        /// <summary>
        /// Cantidad de Tarjetas Bloqueadas y Activas
        /// </summary>
        public int CreditCardCant { get; set; }

        /// <summary>
        /// Vehiculos con GPS
        /// </summary>
        public int GPSCant { get; set; }

        public int PartnerId { get; set; }

        public string PartnerName { get; set; }

        public string StartDateStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(IsDemoAlert); }
            set { IsDemoAlert = Utilities.Miscellaneous.SetDate(value); }
        }

        public string IsDemoAlertStr { get; set; }

        public DateTime? IsDemoAlert { get; set; }

        public string InChargeEmails { get; set; }

        public bool ShowDemoAlert { get; set; }

        public string ManagementPricesXml { get; set; }

        public List<CustomerPricesModel> ManagmentPricesList { get { return string.IsNullOrEmpty(ManagementPricesXml) ? new List<CustomerPricesModel>() : Utilities.Miscellaneous.GetXMLDeserialize<List<CustomerPricesModel>>(ManagementPricesXml, typeof(CustomerPricesModel).Name); } }

        public string SMSPricesXml { get; set; }

        public List<CustomerPricesModel> SMSPricesList { get { return string.IsNullOrEmpty(SMSPricesXml) ? new List<CustomerPricesModel>() : Utilities.Miscellaneous.GetXMLDeserialize<List<CustomerPricesModel>>(SMSPricesXml, typeof(CustomerPricesModel).Name); } }

        public int ManagementPriceId { get; set; }

        public int SMSPriceId { get; set; }

        public int? CustomerCharges { get; set; }

        public int TotalRows { get; set; }

        public string PrimaryEmail { get; set; }

        public string DecryptedPrimaryEmail
        {
            get { return TripleDesEncryption.Decrypt(PrimaryEmail); }
            set { PrimaryEmail = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Type OfDistribution by Liters or By Amount
        /// </summary>
        public int TypeBudgetDistribution { get; set; }

        public bool CostCenterFuelDistribution { get; set; }


        #region AddiotionalInfo
        public int? AdditionalId { get; set; }

        [DisplayName("Razón Social")]
        public string BusinessName { get; set; }

        [DisplayName("Cédula Jurídica")]
        public string LegalDocument { get; set; }

        [DisplayName("Teléfono Principal")]
        public string MainPhone { get; set; }

        [DisplayName("Teléfono Secundario")]
        public string SecundaryPhone { get; set; }

        [DisplayName("Correo Electrónico")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Digite un correo válido.")]
        public string Email { get; set; }

        public string ContactName { get; set; }

        public string FinancialContact { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Digite un correo válido.")]
        public string FinancialEmail { get; set; }

        public string RegisteredOwner { get; set; }

        [DisplayName("Zona")]
        public int? ZoneId { get; set; }

        public int? StateId { get; set; }

        public int? CountyId { get; set; }

        public int? CityId { get; set; }

        [DisplayName("Dirección")]
        public string Address { get; set; }

        [DisplayName("Longitud")]
        public string Longitude { get; set; }

        [DisplayName("Latitud")]
        public string Latitude { get; set; }

        [DisplayName("Encargado")]
        public int? CustomerManagerId { get; set; }

        [DisplayName("Cliente Solicitante")]
        public string ApplicantClient { get; set; }

        [DisplayName("# Tiquete")]
        public string SupportTicket { get; set; }

        public string SupportTicketURL { get; set; }
        #endregion
    }

    public class CustomerByPartnerData : ModelAncestor
    {
        /// <summary>
        /// default Constructor
        /// </summary>
        public CustomerByPartnerData()
        {
            AvailableCustomersList = new List<Customers>();
            ConnectedCustomersList = new List<Customers>();
        }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Partner model
        /// </summary>
        public int? PartnerId { get; set; }

        /// <summary>
        /// Partner Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Customer model
        /// </summary>

        public int? CustomerId { get; set; }

        /// <summary>
        /// Available VehicleGroup List
        /// </summary>
        public List<Customers> AvailableCustomersList { get; set; }

        /// <summary>
        /// Connected VehicleGroup List
        /// </summary>
        public IEnumerable<Customers> ConnectedCustomersList { get; set; }

        /// <summary>
        /// RoleId of the Customer
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// RoleName of the Customer
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// RoleName of the Customer Temp
        /// </summary>
        public string RoleNameTemp { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string CustomerName { get; set; }


        /// <summary>
        /// property
        /// </summary>        
        public string EncryptedName { get; set; }


        /// <summary>
        /// CustomerLogo
        /// </summary>
        public string CustomerLogo { get; set; }

        /// <summary>
        /// Country Id
        /// </summary>
        public int? CountryId { get; set; }

        /// <summary>
        /// Decrypted Customer Name
        /// </summary>
        /// 

        public string DecryptedCustomerName
        {
            get { return CustomerName != null ? TripleDesEncryption.Decrypt(CustomerName) : TripleDesEncryption.Decrypt(EncryptedName); }
            set { DecryptedCustomerName = TripleDesEncryption.Encrypt(value); }
        }
    }

    public class CustomerByPartnersBase
    {
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public CustomerByPartnerData Data { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        [DisplayName("Socio")]
        public int? PartnerId { get; set; }

        /// <summary>
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    public class CustomerCreditCards : ModelAncestor
    {

        public CustomerCreditCards()
        {
            Parameters = new Filters();
        }


        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? CustomerCreditCardId { get; set; }


        /// <summary>
        /// Customer Id
        /// </summary>
        [NotMappedColumn]
        public int CustomerId { get; set; }


        private string _creditCardNumber;

        /// <summary>
        /// Credit Card Number
        /// </summary>
        [DisplayName("Número Tarjeta")]
        [NotMappedColumn]
        public string CreditCardNumber
        {
            get { return _creditCardNumber ?? ""; }
            set { _creditCardNumber = value; }
        }

        /// <summary>
        /// Credit Card Number with restrict display
        /// </summary>
        [DisplayName("Número Tarjeta")]
        [NotMappedColumn]
        public string RestrictCreditCardNumber { get { return Utilities.Miscellaneous.GetDisplayCreditCardMask(DisplayCreditCardNumber); } }


        /// <summary>
        /// Credit Card Number for display
        /// </summary>
        [DisplayName("Número Tarjeta")]
        [Required(ErrorMessage = "{0} es requerido")]
        //[RegularExpression(@"^(\d{4}-\d{4}-\d{4}-\d{4})$", ErrorMessage = "Formato de Tarjeta inválido")]
        [NotMappedColumn]
        public string DisplayCreditCardNumber
        {
            get { return TripleDesEncryption.Decrypt(CreditCardNumber); }
            set { CreditCardNumber = TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// Credit Card Expiration Year
        /// </summary>
        [DisplayName("Año de Expiración")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? ExpirationYear { get; set; }

        /// <summary>
        /// Credit Card ExpirationM onth
        /// </summary>
        [DisplayName("Mes de Expiración")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? ExpirationMonth { get; set; }

        /// <summary>
        /// Month Name
        /// </summary>
        [NotMappedColumn]
        public string ExpirationMonthName
        {
            get { return Utilities.Miscellaneous.GetMonthName(ExpirationMonth); }
        }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [NotMappedColumn]
        public decimal? CreditLimit { get; set; }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string CreditLimitStr
        {
            get { return Utilities.Miscellaneous.GetCurrencyFormat(CreditLimit, ""); } //GetNumberFormat(CreditLimit); }
            set { CreditLimit = Convert.ToDecimal(value.Replace(".", "").Split(',')[0]); }
        }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [NotMappedColumn]
        public string CreditLimitDisplayStr
        {
            get { return Utilities.Miscellaneous.GetCurrencyFormat(CreditLimit, CurrencySymbol); }
        }

        /// <summary>
        /// Status Id
        /// </summary>
        [DisplayName("Estado")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? StatusId { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencyName { get; set; }

        /// <summary>
        /// Expiration Date 
        /// </summary>
        [DisplayName("Fecha de Expiración")]
        [NotMappedColumn]
        public string ExpirationDate
        {
            get { return ExpirationMonthName + "/" + ExpirationYear; }
        }

        /// <summary>
        /// Status Name
        /// </summary>
        [NotMappedColumn]
        public string StatusName { get; set; }


        /// <summary>
        /// Credit Card Holder
        /// </summary>
        [DisplayName("Titular")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string CreditCardHolder { get; set; }

        /// <summary>
        /// Internation Card
        /// </summary>
        [DisplayName("Tarjeta Internacional")]
        [NotMappedColumn]
        public bool InternationalCard { get; set; }


        /// <summary>
        /// Assigned of credit
        /// </summary>
        public decimal? AssignedCredit { get; set; }

        public List<Payments> Payments { get; set; }

        public Filters Parameters { get; set; }

    }

    public class Filters
    {
        public Filters()
        {
            var currentDate = DateTimeOffset.Now;
            ReportCriteriaId = (int?)ReportCriteria.Period;
            Year = currentDate.Year;
            Month = currentDate.Month;
            StartDate = currentDate.Date;
            EndDate = currentDate.Date;
        }

        public int? Year { get; set; }

        public int? Month { get; set; }

        public DateTime? StartDate { get; set; }

        public string StartDateStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = Utilities.Miscellaneous.SetDate(value); }
        }

        public DateTime? EndDate { get; set; }

        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : Utilities.Miscellaneous.SetDate(value); }
        }

        public int? ReportCriteriaId { get; set; }

        public int CustomerId { get; set; }
    }

    public class Payments
    {
        public int Id { get; set; }

        public decimal PaymentAmount { get; set; }

        public string CurrencySymbol { get; set; }

        [DisplayName("Pago")]
        public string PaymentAmountStr { get { return Utilities.Miscellaneous.GetCurrencyFormat(PaymentAmount, CurrencySymbol); } }

        public decimal SumPayments { get; set; }

        [DisplayName("Suma de Pagos")]
        public string SumPaymentsStr { get { return Utilities.Miscellaneous.GetCurrencyFormat(SumPayments, CurrencySymbol); } }

        public decimal SpendAmount { get; set; }

        [DisplayName("Gastos")]
        public string SpendAmountStr { get { return Utilities.Miscellaneous.GetCurrencyFormat(SpendAmount, CurrencySymbol); } }

        public decimal Balance { get { return SpendAmount - SumPayments; } }

        [DisplayName("Balance")]
        public string BalanceStr { get { return Utilities.Miscellaneous.GetCurrencyFormat(Balance, CurrencySymbol); } }

        [DisplayName("Fecha del Pago")]
        public DateTime InsertDate { get; set; }
    }

    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class CardRequest : ModelAncestor
    {
        public int PaymentMethod { get; set; }

        public string PaymentName { get; set; }
        /// <summary>
        /// Card Request default creator
        /// </summary>
        public CardRequest()
        {

        }

        /// <summary>
        /// Card Request creator using CardRequestCsv data
        /// </summary>
        public CardRequest(CardRequestCsv data, int custId)
        {
            CustomerId = custId;
            PlateId = data.PlateId;
            DriverName = data.DriverName;
            DriverIdentification = data.DriverIdentification;
            DeliveryState = data.DeliveryState;
            DeliveryCounty = data.DeliveryCounty;
            DeliveryCity = data.DeliveryCity;
            AddressLine1 = data.AddressLine1;
            AddressLine2 = data.AddressLine2;
            PaymentReference = data.PaymentReference;
            AuthorizedPerson = data.AuthorizedPerson;
            ContactPhone = data.ContactPhone;
            EstimatedDelivery = DateTime.ParseExact(data.EstimatedDelivery, "yyyyMMdd", CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? CardRequestId { get; set; }


        /// <summary>
        /// Customer Id
        /// </summary>
        [NotMappedColumn]
        public int? CustomerId { get; set; }


        /// <summary>
        /// PlateId
        /// </summary>
        [DisplayName("Matrícula del Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string PlateId { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        [DisplayName("Nombre del Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }

        /// <summary>
        /// DriverName
        /// </summary>
        [DisplayName("Nombre del Conductor")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DriverName { get; set; }

        /// <summary>
        /// DecryptedDriverName
        /// </summary>
        public string DecryptedDriverName
        {
            get { return TripleDesEncryption.Decrypt(DriverName); }
        }

        /// <summary>
        /// DriverIdentification
        /// </summary>
        [DisplayName("Identificación")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DriverIdentification { get; set; }

        /// <summary>
        /// DecryptedName
        /// </summary>
        public string DecryptedDriverIdentification
        {
            get { return TripleDesEncryption.Decrypt(DriverIdentification); }
        }

        /// <summary>
        /// UserId 
        /// </summary>
        [Required(ErrorMessage = "{0} es requerido")]
        public int DriverUserId { get; set; }

        /// <summary>
        /// AddressLine1
        /// </summary>
        [DisplayName("Dirección de Entrega")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string AddressLine1 { get; set; }

        /// <summary>
        /// AddressLine2
        /// </summary>
        [DisplayName("Otras Señas")]
        [NotMappedColumn]
        public string AddressLine2 { get; set; }

        /// <summary>
        /// PaymentReference
        /// </summary>
        [DisplayName("No. Deposito (COINCA)")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string PaymentReference { get; set; }

        /// <summary>
        /// AuthorizedPerson
        /// </summary>
        [DisplayName("Persona Autorizada")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string AuthorizedPerson { get; set; }


        /// <summary>
        /// ContactPhone
        /// </summary>
        [DisplayName("Teléfono")]
        [Required(ErrorMessage = "{0} es requerido")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        [NotMappedColumn]
        public string ContactPhone { get; set; }

        /// <summary>
        /// Estimated Delivery
        /// </summary>
        [DisplayName("Fecha de Entrega")]
        [NotMappedColumn]
        public DateTime? EstimatedDelivery { get; set; }

        /// <summary>
        /// Estimated Delivery as String
        /// </summary>
        [DisplayName("Fecha de Entrega")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string EstimatedDeliveryStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(EstimatedDelivery); }
            set { EstimatedDelivery = Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Status Id
        /// </summary>
        [DisplayName("Estado")]
        [NotMappedColumn]
        public int? StatusId { get; set; }

        /// <summary>
        /// Status Name
        /// </summary>
        [NotMappedColumn]
        public string StatusName { get; set; }

        /// <summary>
        /// Issue For Id from Types
        /// </summary>
        [DisplayName("Emisión de Tarjetas por")]
        [NotMappedColumn]
        public int? IssueForId { get; set; }

        /// <summary>
        /// Country Id
        /// </summary>
        [NotMappedColumn]
        public int? CountryId { get; set; }

        /// <summary>
        /// State Id
        /// </summary>
        [DisplayName("Provincia")]
        [NotMappedColumn]
        [Required(ErrorMessage = "{0} es requerido")]
        public int? StateId { get; set; }

        /// <summary>
        /// State Name
        /// </summary>
        [NotMappedColumn]
        public string StateName { get; set; }

        /// <summary>
        /// County Id
        /// </summary>
        [DisplayName("Cantón")]
        [NotMappedColumn]
        [Required(ErrorMessage = "{0} es requerido")]
        public int? CountyId { get; set; }

        /// <summary>
        /// County Name
        /// </summary>
        [NotMappedColumn]
        public string CountyName { get; set; }

        /// <summary>
        /// City Id
        /// </summary>
        [DisplayName("Distrito")]
        [NotMappedColumn]
        [Required(ErrorMessage = "{0} es requerido")]
        public int? CityId { get; set; }

        /// <summary>
        /// City Name
        /// </summary>
        [NotMappedColumn]
        public string CityName { get; set; }

        /// <summary>
        /// Delivery State
        /// </summary>
        [DisplayName("Provincia")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DeliveryState { get; set; }

        /// <summary>
        /// Delivery County
        /// </summary>
        [DisplayName("Cantón")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DeliveryCounty { get; set; }

        /// <summary>
        /// Delivery City
        /// </summary>
        [DisplayName("Distrito")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DeliveryCity { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// City Text for Grid
        /// </summary>
        [NotMappedColumn]
        public string CityText
        {
            get { return string.IsNullOrEmpty(DeliveryCity) ? CityName : DeliveryCity; }
        }

        /// <summary>
        /// Decrypted Customer Name
        /// </summary>
        public string DecryptedCustomerName
        {
            get { return TripleDesEncryption.Decrypt(CustomerName); }
            set { DecryptedCustomerName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Activated Date
        /// </summary>
        [DisplayName("Fecha Activa")]
        [NotMappedColumn]
        public DateTime? ActivatedDate { get; set; }

        /// <summary>
        /// Activated Date as String
        /// </summary>
        [DisplayName("Fecha Activa")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string ActivatedDateStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(ActivatedDate); }
            set { ActivatedDate = Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Closed Date 
        /// </summary>
        [DisplayName("Fecha Cerrada")]
        [NotMappedColumn]
        public DateTime? ClosedDate { get; set; }

        /// <summary>
        /// Closed Date as String
        /// </summary>
        [DisplayName("Fecha Cerrada")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string ClosedDateStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(ClosedDate); }
            set { ClosedDate = Utilities.Miscellaneous.SetDate(value); }
        }
    }

    public class CardRequestCsv
    {
        /// <summary>
        /// PlateId
        /// </summary>
        public string PlateId { get; set; }

        /// <summary>
        /// DriverName
        /// </summary>
        public string DriverName { get; set; }

        /// <summary>
        /// DriverIdentification
        /// </summary>
        public string DriverIdentification { get; set; }

        /// <summary>
        /// Delivery State
        /// </summary>
        public string DeliveryState { get; set; }

        /// <summary>
        /// Delivery County
        /// </summary>
        public string DeliveryCounty { get; set; }

        /// <summary>
        /// Delivery City
        /// </summary>
        public string DeliveryCity { get; set; }

        /// <summary>
        /// AddressLine1
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// AddressLine2
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// PaymentReference
        /// </summary>
        public string PaymentReference { get; set; }

        /// <summary>
        /// AuthorizedPerson
        /// </summary>
        public string AuthorizedPerson { get; set; }


        /// <summary>
        /// ContactPhone
        /// </summary>
        public string ContactPhone { get; set; }

        /// <summary>
        /// Estimated Delivery
        /// </summary>
        public string EstimatedDelivery { get; set; }

    }

    public class CustomersApi : ApiAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string CustomerAccountNumber { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string CustomerCreditCard { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string CustomerEmail { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public int PartnerId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string CurrencyCode { get; set; }
    }

    public class CustomerTerminals
    {
        /// <summary>
        /// CustomersTerminal Constructor
        /// </summary>
        public CustomerTerminals()
        {
            TerminalList = new List<CustomerTerminals>();
        }
        // <summary>
        /// List of entities to use it for load grid information
        /// 
        /// 
        public IEnumerable<CustomerTerminals> TerminalList { get; set; }

        /// <summary>
        /// Customer Terminal Id
        /// </summary>
        [NotMappedColumn]
        [Display(Name = "ID de la Terminal")]
        [Required(ErrorMessage = "{0} requerido")]
        public string TerminalId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        [Display(Name = "Descripcion")]
        public string MerchantDescription { get; set; }
    }

    public class CustomerBudget
    {
        public int? CustomerId { get; set; }

        public int Periodicity { get; set; }

        public int NameDay { get; set; }

        public int NameDate { get; set; }

        public int Repeat { get; set; }

        public string CreditCardType { get; set; }

        public int CurrencyId { get; set; }

        public int? IssueForId { get; set; }

        public bool TransactionsOffline { get; set; }

        public List<CustomerCreditCards> MasterCards { get; set; }

        public int? UnitOfCapacityId { get; set; }

        public bool CostCenterFuelDistribution { get; set; }

        public string AccountNumber { get; set; }

        public string RestrictAccountNumber { get { return Utilities.Miscellaneous.GetDisplayAccountNumberMask(DisplayAccountNumber); } }

        [DisplayName("Número de Cuenta")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public string DisplayAccountNumber
        {
            get { return TripleDesEncryption.Decrypt(AccountNumber); }
            set { AccountNumber = TripleDesEncryption.Encrypt(value); }
        }
    }

    public class CustomerPricesModel
    {
        public int? Id { get; set; }

        public int From { get; set; }

        public int To { get; set; }

        public decimal Price { get; set; }
    }

    public class CustomerValidatePrices
    {
        public bool WithinRange { get; set; }

        public int? NotInRange { get; set; }
    }

    public class DynamicPartialModel
    {
        public List<CustomerPricesModel> list { get; set; }

        public int priceId { get; set; }
    }

    public class CustomerAdditionalInformation
    {
        public int? AdditionalId { get; set; }

        [DisplayName("Razón Social")]
        public string BusinessName { get; set; }

        [DisplayName("Cédula Jurídica")]
        public string LegalDocument { get; set; }

        [DisplayName("Teléfono Principal")]
        public string MainPhone { get; set; }

        [DisplayName("Teléfono Secundario")]
        public string SecundaryPhone { get; set; }

        [DisplayName("Correo Electrónico")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Correo Electrónico No Válido!")]
        public string Email { get; set; }

        public string ContactName { get; set; }

        [DisplayName("Zona")]
        public int? ZoneId { get; set; }

        public int? StateId { get; set; }

        public int? CountyId { get; set; }

        public int? CityId { get; set; }

        [DisplayName("Dirección")]
        public string Address { get; set; }

        [DisplayName("Longitud")]
        public string Longitude { get; set; }

        [DisplayName("Latitud")]
        public string Latitude { get; set; }

        public int? CustomerId { get; set; }

        public int? CountryId { get; set; }

        [DisplayName("Encargado")]
        public int? CustomerManagerId { get; set; }
    }

    public class CustomerContractInformation
    {
        public CustomerContractInformation()
        {
            Vehicles = new List<Vehicles>();
            IsProductLineVitaECO = false;
        }

        public bool IsProductLineVitaECO { get; set; }

        public bool UseMonths { get; set; }

        public int? ContractId { get; set; }

        public DateTime? Since { get; set; }

        [NotMapped]
        public string SinceStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(Since); }
            set { Since = Utilities.Miscellaneous.SetDate(value); }
        }

        public DateTime? FirstSince { get; set; }

        [NotMapped]
        public string FirstSinceStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(FirstSince); }
            set { FirstSince = Utilities.Miscellaneous.SetDate(value); }
        }

        public int ContractYears { get; set; }

        public int YearsOrMonths { get; set; }

        public DateTime To
        {
            get
            {
                return Since == null ?
                             DateTime.Today :
                                 ((this.UseMonths) ?
                                     DateTime.Parse(Since.ToString()).AddMonths(YearsOrMonths)
                                         : DateTime.Parse(Since.ToString()).AddYears(YearsOrMonths));
            }
        }
        [NotMapped]
        public DateTime ExpireDate { get; set; }

        [NotMapped]
        public bool IsExpired { get { return ExpireDate < DateTime.Today; } }

        [NotMapped]
        public string ToStr { get { return Utilities.Miscellaneous.GetDateFormat(ExpireDate); } }

        [NotMapped]
        public bool AllVehicles { get; set; }

        [NotMapped]
        public int AlertDays { get; set; }

        [NotMapped]
        public int DaysLeft { get; set; }

        [NotMapped]
        public List<Vehicles> Vehicles { get; set; }

        [NotMapped]
        public string SelectedVehiclesStr { get; set; }

        [NotMapped]
        public string InfoClass
        {
            get
            {
                if (DaysLeft < 0 || ContractId == null)
                {
                    return "danger";
                }
                else if (DaysLeft < AlertDays)
                {
                    return "warning";
                }
                return "info";
            }
        }

        [NotMapped]
        public string InfoText
        {
            get
            {
                if (ContractId == null)
                {
                    return "El Cliente no tiene Contrato.";
                }
                else if (DaysLeft < 0)
                {
                    return "El Contrato se encuentra vencido.";
                }
                return string.Format("El Contrato vence en {0}", DateDifference);
            }
        }

        [NotMapped]
        public int CustomerId { get; set; }

        public string DateDifference { get; set; }

        [NotMapped]
        public string EncryptCustomerName { get; set; }

        public string DecryptCustomerName { get { return string.IsNullOrEmpty(EncryptCustomerName) ? string.Empty : TripleDesEncryption.Decrypt(EncryptCustomerName); } }

        public string PartnerName { get; set; }

        public bool? Active { get; set; }

        [NotMapped]
        public bool? Renewed { get; set; }

        public string Status
        {
            get
            {
                return Renewed == null || !(bool)Renewed ? "Inicial" : "Renovado";
            }
        }

        public DateTime? ModifyDate { get; set; }

        #region APITokenInformation 
        [NotMapped]
        public string APIToken { get; set; }

        [NotMapped]
        public DateTime TokenExpirationDate { get; set; }

        [NotMapped]
        public string TokenExpirationDateStr { get { return Utilities.Miscellaneous.GetDateFormat(TokenExpirationDate); } }
        #endregion
    }

    /// <summary>
    /// Customer Binnacle Base
    /// </summary>
    public class CustomerBinnacleBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CustomerBinnacleBase()
        {
            Data = new CustomerBinnacle();
            List = new List<CustomerBinnacle>();
        }

        /// <summary>
        /// Data
        /// </summary>
        public CustomerBinnacle Data { get; set; }

        /// <summary>
        /// List
        /// </summary>
        public IEnumerable<CustomerBinnacle> List { get; set; }
    }

    /// <summary>
    /// Branch Customers
    /// </summary>
    public class BranchCustomers
    {
        /// <summary>
        /// Customer Branch List
        /// </summary>
        public int CustomerId { get; set; }

    }

    /// <summary>
    /// Customer Binnacle
    /// </summary>
    public class CustomerBinnacle
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// CustomerId
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Comment
        /// </summary>
        [DisplayName("Comentario")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Comment { get; set; }

        /// <summary>
        /// EncryptedUserName
        /// </summary>
        public string EncryptedUserName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>        
        public string DecryptedUserName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedUserName); }
            set { EncryptedUserName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// InsertDate
        /// </summary>        
        public DateTime InsertDate { get; set; }

        /// <summary>
        /// Is deleted
        /// </summary>
        public int Deleted { get; set; }

    }
}
#pragma warning restore CS1591