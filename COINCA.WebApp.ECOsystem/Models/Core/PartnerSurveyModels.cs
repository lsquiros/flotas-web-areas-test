﻿/*******************************************************      
 * Henry Retana 
 * 26/06/2019
 * Partner Survey Models, Classes will be used all around
 * the application, this is why they are being created in
 * the main module.
 ********************************************************/
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

#pragma warning disable CS1591
namespace ECOsystem.Models.Core
{
    public class PartnerSurvey
    {
        public int? Id { get; set; }
        public int PartnerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? TargetId { get; set; }
        public string TargetDescription { get; set; }
        public bool Active { get; set; }
        public bool Mandatory { get; set; }
        public bool Delete { get; set; }
        public int? Elements { get; set; }
        public string QuestionsStr { get; set; }
        public List<PartnerSurveyQuestion> Questions
        {
            get { return string.IsNullOrEmpty(QuestionsStr) ? new List<PartnerSurveyQuestion>() : Utilities.Miscellaneous.GetXMLDeserialize<List<PartnerSurveyQuestion>>(QuestionsStr, typeof(PartnerSurveyQuestion).Name); }
        }
        public List<PartnerSurveyQuestion> NewQuestions { get; set; }
    }

    public class PartnerSurveyQuestion
    {
        public int? Id { get; set; }
        public int? PartnerSurveyId { get; set; }
        public int? QuestionTypeId { get; set; }
        public string QuestionTypeText { get; set; }
        public string Text { get; set; }
        public int? Order { get; set; }
        public bool OneOption { get; set; }
        public bool Delete { get; set; }
        public bool ShowOptions { get; set; }
        public bool? Finish { get; set; }
        public string AnswersStr { get; set; }
        public List<PartnerSurveyAnswer> Answers
        {
            get { return string.IsNullOrEmpty(AnswersStr) ? new List<PartnerSurveyAnswer>() : Utilities.Miscellaneous.GetXMLDeserialize<List<PartnerSurveyAnswer>>(AnswersStr, typeof(PartnerSurveyAnswer).Name); }
        }
        public List<PartnerSurveyAnswer> NewAnswers { get; set; }
    }

    public class PartnerSurveyAnswer
    {
        public int? Id { get; set; }
        public int? PartnerSurveyQuestionId { get; set; }
        public string Text { get; set; }
        public int Order { get; set; }
        public bool Delete { get; set; }
        public int? NextSurveyQuestionId { get; set; }
        public bool ShowNextQuestion { get; set; }
        public bool? Checked { get; set; }
        public string AnswerText { get; set; }
        public List<SelectListItem> QuestionsList { get; set; }
    }

    public class PartnerSurveyQuestionTypes
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? OneOption { get; set; }
        public bool?  ShowOptions { get; set; }
    }

    public class PartnerSurveyReport
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Target { get; set; }
        public int Elements { get; set; }
        public string CustomerNameEncrypt { get; set; }
        public string CustomerNameDecrypt { get { return TripleDesEncryption.Decrypt(CustomerNameEncrypt); } }
        public DateTime Date { get; set; }
        public string ReportDetailStr { get; set; }
        public List<PartnerSurveyReportDetail> ReportDetail
        {
            get { return string.IsNullOrEmpty(ReportDetailStr) ? new List<PartnerSurveyReportDetail>() : Utilities.Miscellaneous.GetXMLDeserialize<List<PartnerSurveyReportDetail>>(ReportDetailStr, typeof(PartnerSurveyReportDetail).Name); }
        }
    }

    public class PartnerSurveyReportDetail
    {
        public int SurveyByCustomerId { get; set; }
        public string Question { get; set; }
        public int QuestionOrder { get; set; }
        public string Answer { get; set; }
        public string UserNameEncrypt { get; set; }
        public string UserNameDecrypt { get { return TripleDesEncryption.Decrypt(UserNameEncrypt); } }
    }

    public class PartnerSurveyTargets
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }

    public class PartnerSurveyShow
    {
        public int? SurveyId { get; set; }
        public int? Elements { get; set; }
    }

    public class OrdenItems
    {
        public int Id { get; set; }
        public int Order { get; set; }
    }

    public enum QuestionTypes
    {
        RadioButton = 1, 
        CheckBox = 2, 
        TextBox = 3
    }
}
#pragma warning restore CS1591