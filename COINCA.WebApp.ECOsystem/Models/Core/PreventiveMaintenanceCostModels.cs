﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceCostModels.cs
*  Summary : PreventiveMaintenanceCost Models
*  Author  : Danilo Hidalgo
*  Date    : 12/30/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Core
{
    //public class PreventiveMaintenanceCostModels
    //{
    //}

    /// <summary>
    /// PreventiveMaintenanceCost Base
    /// </summary>
    public class PreventiveMaintenanceCostBase
    {
        /// <summary>
        /// PreventiveMaintenanceCost Constructor
        /// </summary>
        public PreventiveMaintenanceCostBase()
        {
            Data = new PreventiveMaintenanceCost();
            List = new List<PreventiveMaintenanceCost>();
        }
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public PreventiveMaintenanceCost Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceCost> List { get; set; }
    }

    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceCost : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PreventiveMaintenanceCatalogId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PreventiveMaintenanceCostId { get; set; }

        /// <summary>
        /// Name of Cost Preventive Maintenance
        /// </summary>
        //[DisplayName("Nombre")]
        //[Required(ErrorMessage = "{0} es requerido")]
        //[GridColumn(Title = "Nombre", Width = "250px", SortEnabled = true)]
        [NotMappedColumn]
        public string Description { get; set; }

        /// <summary>
        /// Name of Frequency Type
        /// </summary>
        //[DisplayName("Costo")]
        //[GridColumn(Title = "Costo", Width = "250px", SortEnabled = true)]
        [NotMappedColumn]
        public decimal Cost { get; set; }

        /// <summary>
        /// Symbol of currency
        /// </summary>
        [NotMappedColumn]
        public string Symbol { get; set; }

    }

}

