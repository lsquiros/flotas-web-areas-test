﻿using System;
using System.Globalization;
using System.ComponentModel.DataAnnotations;

#pragma warning disable CS1591
namespace ECOsystem.Models
{
    public class Vehicles 
    {
        public int VehicleNumber { get; set; }

        public string CodView { get; set; }

        public int? VehicleId { get; set; }

        public string Name { get; set; }

        public string Manufacturer { get; set; }

        public string VehicleModel { get; set; }

        public string VehicleType { get; set; }

        [Required(ErrorMessage = "Año es requerido.")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public int? VehicleYear { get; set; }

        public string PlateId { get; set; }

        public int CostCenterId { get; set; }

        public string CostCenterName { get; set; }

        public string UnitName { get; set; }

        public int VehicleGroupId { get; set; }

        public int? CustomerId { get; set; }

        public int DefaultFuelId { get; set; }

        public int? UserId { get; set; }

        public string EncryptedUserName { get; set; }

        public string DecryptedUserName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedUserName); }
            set { EncryptedUserName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        public int VehicleCategoryId { get; set; }

        public string CategoryType { get; set; }

        public string FuelName { get; set; }

        public bool Active { get; set; }

        public bool HasGPS { get; set; }

        public string GPSIcon
        {
            get
            {
                switch (HasGPS)
                {
                    case true:
                        return "<span2 class='text-success mdi mdi-access-point-network' data-toggle='tooltip' data-placement='right' title='Tiene GPS'></span2>";
                    default:
                        return "";
                }
            }
            set { }
        }

        public string Colour { get; set; }

        public string Chassis { get; set; }

        public string LastDallas { get; set; }

        public int FreightTemperature { get; set; }

        public int FreightTemperatureThreshold1 { get; set; }

        public int FreightTemperatureThreshold2 { get; set; }

        public bool Predictive { get; set; }

        public string AVL { get; set; }
                  
        public string CabinPhone { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DriverLicenseExpiration { get; set; }

        public int DailyTransactionLimit { get; set; }

        [Required(ErrorMessage = "Odómetro es requerido")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digite cero o solo valores numéricos.")]
        public int Odometer { get; set; }

        public int Liters { get; set; }

        public int PartnerId { get; set; }

        public int PartnerCapacityUnitId { get; set; }

        public string Insurance { get; set; }

        public DateTime? DateExpirationInsurance { get; set; }

        public string DateExpirationInsuranceStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(DateExpirationInsurance); }
            set { DateExpirationInsurance = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        public string CoverageType { get; set; }

        public string NameEnterpriseInsurance { get; set; }

        public int? PeriodicityId { get; set; }

        public decimal? Cost { get; set; }

        public int AdministrativeSpeedLimit { get; set; }

        public int? IntrackReference { get; set; }

        public decimal? Imei { get; set; }

        public decimal? DefaultPerformance { get; set; }

        public decimal MonthlyBudget { get { return TotalBudget - AdditionalBudget; } }

        public decimal AdditionalBudget { get; set; }

        public decimal TotalBudget { get; set; }

        public decimal MonthlyPurchases { get { return TotalBudget - Available; } }

        public decimal Available { get; set; }

        public bool PullPreviousBudget { get; set; }

        public int? ExternalId { get; set; }

        public Double OdometerVehicle { get; set; }

        public bool HasCooler { get; set; }
        
        public int TempSensorCount { get; set; }

        public decimal MinTemperature { get; set; }
        
        public decimal MaxTemperature { get; set; }

        public string MaxTemperatureStr { get { return this.MaxTemperature.ToString("#.00", CultureInfo.CreateSpecificCulture("es-CR")); } }

        public bool Selected { get; set; }
	
	public bool Disabled { get; set; }

        public int? ClassificationId { get; set; }

        public string ClassificationName { get; set; }

        public string OperationBAC { get; set; }

        public string SupportTicket { get; set; }

        public string SupportTicketURL { get; set; }

	public bool HasOpenDoors { get; set; }

        public bool HasTurnOff { get; set; }

        public bool HasDallas { get; set; }
        //public Int64 FleetioId { get; set; }
    }
}
#pragma warning restore CS1591
