﻿/************************************************************************************************************
*  File    : ParametersModels.cs
*  Summary : Parameters Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;

namespace ECOsystem.Models.Core
{
#pragma warning disable 1591

    /// <summary>
    /// Partner Models
    /// </summary>
    public class CustomerThresholds : ModelAncestor
    {
        /// <summary>
        /// Safe Speed Margin property
        /// </summary>
        public int? CodingThresholdsId { get; set; }
        
        /// <summary>
        /// Safe Speed Margin property
        /// </summary>
        [DisplayName("CustomerId")]
        public int? CustomerId { get; set; }

        /// <summary>
        /// Error margin of odometer with AVL
        /// </summary>
        [DisplayName("Mínimo")]
        [Required (ErrorMessage = "{0} es requerido") ]
        [Range(0, 100, ErrorMessage = "{0} debe estar  entre un rango númerico de  0 y 100.")]
        public int? RedLower { get; set; }

        /// <summary>
        /// Error margin of odometer with POS
        /// </summary>
        [DisplayName("Máximo")]
        [Required (ErrorMessage = "{0} es requerido") ]
        [Range(0, 100, ErrorMessage = "{0} debe estar  entre un rango númerico de  0 y 100.")]
        public int? RedHigher { get; set; }

        /// <summary>
        /// Tolerance time to get arrival to point
        /// </summary>
        [DisplayName("Mínimo")]
        [Required (ErrorMessage = "{0} es requerido")]
        [Range(0, 100, ErrorMessage = "{0} debe estar  entre un rango númerico de  0 y 100.")]
        public int? YellowLower { get; set; }

        /// <summary>
        /// Error Margin of geopoint
        /// </summary>
        [DisplayName("Máximo")]
        [Required (ErrorMessage = "{0} es requerido")]
        [Range(0, 100, ErrorMessage = "{0} debe estar  entre un rango númerico de  0 y 100.")]

        public int? YellowHigher  { get; set; }

        [DisplayName("Mínimo")]
        
        [Required (ErrorMessage = "{0} es requerido")]
        [Range(0, 100, ErrorMessage = "{0} debe estar  entre un rango númerico de  0 y 100.")]
        public int? GreenLower { get; set; }

        /// <summary>
        /// Passwords to Validate Repetition
        /// </summary>
        [DisplayName("Máximo")]
        [Required (ErrorMessage = "{0} es requerido")]
        [Range(0, 100, ErrorMessage = "{0} debe estar entre un rango  númerico de  0 y 100.")]
        public int? GreenHigher { get; set; }

        /// <summary>
        /// Credit Percentage Minimun
        /// </summary>
        [DisplayName("Porcentaje Mínimo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [Range(0, 100, ErrorMessage = "{0} debe estar entre un rango  númerico de  0 y 100.")]
        public int? CreditPercentageMin { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }
}