﻿/************************************************************************************************************
*  File    : CreditCardsReportModels.cs
*  Summary : CreditCards Report Models
*  Author  : Danilo Hidalgo
*  Date    : 20/01/2015
*  Modify  : Gerald Solano
*  Mod.Date: 03/04/2016
*  Descrip.: Added new model CreditCardsReportByClient
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Core
{
    /// <summary>
    /// Applicants Report Base
    /// </summary>
    public class CreditCardsReportBase
    {
        /// <summary>
        /// Applicants Reports Base Constructor
        /// </summary>
        public CreditCardsReportBase()
        {
            Parameters = new AdministrationReportsBase();
            List = new List<dynamic>();
            UsersList = new List<SelectListItem>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public AdministrationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// List of users
        /// </summary>
        public IEnumerable<SelectListItem> UsersList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
}

    /// <summary>
    /// Applicants Report Model
    /// </summary>
    public class CreditCardsReport
    {
        /// <summary>
        /// # de Solicitud
        /// </summary>
        [DisplayName("# Solicitud")]
        [ExcelMappedColumn("# Solicitud")]
        [GridColumn(Title = "# Solicitud", Width = "90px", SortEnabled = true)]
        public int RequestNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Encrypted CreditCardNumber
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        //[GridColumn(Title = "Tarjeta", Width = "120px", SortEnabled = true)]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// Decryped CreditCardNumber
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Tarjeta", Width = "120px", SortEnabled = true)]
        public string DecrypedCreditCardNumber {
            get { return TripleDesEncryption.Decrypt(CreditCardNumber); }
            set { CreditCardNumber = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Credit Card Number with restrict display
        /// </summary>
        [DisplayName("Número Tarjeta")]
        [ExcelMappedColumn("Número Tarjeta")]
        [GridColumn(Title = "Tarjeta", Width = "120px", SortEnabled = true)]
        public string RestrictCreditCardNumber { get { return Utilities.Miscellaneous.GetDisplayCreditCardMask(DecrypedCreditCardNumber); } }

        /// <summary>
        /// Encrypted ApplicantName
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        //[GridColumn(Title = "Solicitante", Width = "120px", SortEnabled = true)]
        public string ApplicantName { get; set; }
        
        /// <summary>
        /// Decryped ApplicantName
        /// </summary>
        [DisplayName("Solicitante")]
        [ExcelMappedColumn("Solicitante")]
        [GridColumn(Title = "Solicitante", Width = "120px", SortEnabled = true)]
        public string DecrypedApplicantName {
            get { return TripleDesEncryption.Decrypt(ApplicantName); }
            set { ApplicantName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// StatusName
        /// </summary>
        [DisplayName("Status")]
        [ExcelMappedColumn("Estado")]
        [GridColumn(Title = "Status", Width = "120px", SortEnabled = true)]
        public string StatusName { get; set; }

        /// <summary>
        /// RequestDate Date
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Solicitada", Width = "120px", SortEnabled = true)]
        public DateTime? RequestDate { get; set; }

        /// <summary>
        /// RequestDate Str
        /// </summary>
        [DisplayName("Solicitada")]
        [ExcelMappedColumn("Solicitada")]
        [GridColumn(Title = "Solicitada", Width = "120px", SortEnabled = true)]
        public string RequestDateStr { get { return Utilities.Miscellaneous.GetDateFormat(RequestDate); } }

        /// <summary>
        /// DeliveryDate 
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Entregada", Width = "120px", SortEnabled = true)]
        public DateTime? DeliveryDate { get; set; }

        /// <summary>
        /// DeliveryDate Str
        /// </summary>
        [DisplayName("Entregada")]
        [ExcelMappedColumn("Entregada")]
        [GridColumn(Title = "Entregada", Width = "120px", SortEnabled = true)]
        public string DeliveryDateStr { get { return Utilities.Miscellaneous.GetDateFormat(DeliveryDate); } }
    }

    /// <summary>
    /// Credit Cards Report By Client
    /// </summary>
    public class CreditCardsReportByClient
    {
        public int RangePermit { get; set; }
        /// <summary>
        /// Encrypted Client Name
        /// </summary>
        [NotMappedColumn]
        public string ClientName { get; set; }

        /// <summary>
        /// Decryped Client Name
        /// </summary>
        [DisplayName("Nombre Cliente")]
        [GridColumn(Title = "Nombre Cliente", Width = "120px", SortEnabled = true)]
        public string DecrypedClientName
        {
            get { return TripleDesEncryption.Decrypt(ClientName); }
            set { ClientName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Encrypted AccountNumber
        /// </summary>
        [NotMappedColumn]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Decryped AccountNumber
        /// </summary>
        [NotMappedColumn]
        [GridColumn(Title = "Número Cuenta", Width = "120px", SortEnabled = true)]
        public string DecrypedAccountNumber
        {
            get { return TripleDesEncryption.Decrypt(AccountNumber); }
            set { AccountNumber = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Account Number with restrict display
        /// </summary>
        [DisplayName("Número de Cuenta")]
        [GridColumn(Title = "Número de Cuenta", Width = "120px", SortEnabled = true)]
        public string RestrictAccountNumber { get { return Utilities.Miscellaneous.GetDisplayCreditCardMask(DecrypedAccountNumber); } }

        /// <summary>
        /// StatusName
        /// </summary>
        [DisplayName("Estado")]
        [GridColumn(Title = "Estado", Width = "120px", SortEnabled = true)]
        public string StatusName { get; set; }

        /// <summary>
        /// StatusName
        /// </summary>
        [DisplayName("# SAP")]
        [GridColumn(Title = "# SAP", Width = "120px", SortEnabled = true)]
        public string SAPCode { get; set; }

        /// <summary>
        /// StatusName
        /// </summary>
        [DisplayName("Cédula Jurídica")]
        [GridColumn(Title = "Cédula Jurídica", Width = "120px", SortEnabled = true)]
        public string LegalDocument { get; set; }

        /// <summary>
        /// RequestDate Date
        /// </summary>
        [NotMappedColumn]
        [GridColumn(Title = "Creado", Width = "120px", SortEnabled = true)]
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Create Date Str
        /// </summary>
        [DisplayName("Creado")]
        [GridColumn(Title = "Creado", Width = "120px", SortEnabled = true)]
        public string CreateDateStr { get { return Utilities.Miscellaneous.GetDateFormat(CreateDate); } }

        /// <summary>
        /// Activo
        /// </summary>
        [DisplayName("Activo")]
        public bool Active { get; set; }

        /// <summary>
        /// Total de Tarjetas Administrativas
        /// </summary>
        [DisplayName("Administración")]
        public int TotalCantCards { get; set; }

        /// <summary>
        /// Costo de Administración
        /// </summary>
        [DisplayName("Costo Admin.")]
        public decimal CostAdmin { get; set; }

        /// <summary>
        /// Total de Tarjetas Administrativas
        /// </summary>
        [DisplayName("Emisión")]
        public int CurrentCantCards { get; set; }

        /// <summary>
        /// Costo de Administración
        /// </summary>
        [DisplayName("Costo Emsión")]
        public decimal CostEmision { get; set; }

        /// <summary>
        /// Total de Tarjetas Cerradas
        /// </summary>
        [DisplayName("TJ Cerradas")]
        public int CantCloseCards { get; set; }

        /// <summary>
        /// Total de Tarjetas Bloqueadas
        /// </summary>
        [DisplayName("TJ Bloqueadas")]
        public int CantBlockCards { get; set; }

        /// <summary>
        /// Total de Tarjetas Activas
        /// </summary>
        [DisplayName("TJ Activas")]
        public int CantActiveCards { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        public string CurrencySymbol { get; set; }


        /// <summary>
        /// Currency Symbol
        /// </summary>
        public string CurrencySymbolDollar { get { return "$"; } }

        /// <summary>
        /// Total of Reposition Cards
        /// </summary>
        public int CantCardReposition { get; set; }

        /// <summary>
        /// Total amount to charge for replace card
        /// </summary>
        public decimal AmountCardReposition { get; set; }
    }

    /// <summary>
    /// Credit Cards Report Detail by Client
    /// </summary>
    public class CreditCardsReportDetail
    {
        /// <summary>
        /// CreditCardId
        /// </summary>
        public int CreditCardId { get; set; }

        /// <summary>
        /// Holder
        /// </summary>
        public string Holder { get; set; }

        /// <summary>
        /// DecrypedHolder
        /// </summary>
        public string DecrypedHolder
        {
            get { return TripleDesEncryption.Decrypt(Holder); }
            set { Holder = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Encrypted CreditCardNumber
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        //[GridColumn(Title = "Tarjeta", Width = "120px", SortEnabled = true)]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// Decryped CreditCardNumber
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Tarjeta", Width = "120px", SortEnabled = true)]
        public string DecrypedCreditCardNumber
        {
            get { return TripleDesEncryption.Decrypt(CreditCardNumber); }
            set { CreditCardNumber = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Credit Card Number with restrict display
        /// </summary>
        [DisplayName("Número Tarjeta")]
        [ExcelMappedColumn("Número Tarjeta")]
        [GridColumn(Title = "Tarjeta", Width = "120px", SortEnabled = true)]
        public string RestrictCreditCardNumber { get { return Utilities.Miscellaneous.GetDisplayCreditCardMask(DecrypedCreditCardNumber); } }

        /// <summary>
        /// StatusName
        /// </summary>
        [DisplayName("Status")]
        [ExcelMappedColumn("Estado")]
        [GridColumn(Title = "Status", Width = "120px", SortEnabled = true)]
        public string StatusName { get; set; }

        /// <summary>
        /// RequestDate Date
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Solicitada", Width = "120px", SortEnabled = true)]
        public DateTime? RequestDate { get; set; }

        /// <summary>
        /// RequestDate Str
        /// </summary>
        [DisplayName("Solicitada")]
        [ExcelMappedColumn("Solicitada")]
        [GridColumn(Title = "Solicitada", Width = "120px", SortEnabled = true)]
        public string RequestDateStr { get { return Utilities.Miscellaneous.GetDateFormat(RequestDate); } }

        /// <summary>
        /// DeliveryDate 
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Entregada", Width = "120px", SortEnabled = true)]
        public DateTime? DeliveryDate { get; set; }

        /// <summary>
        /// DeliveryDate Str
        /// </summary>
        [DisplayName("Entregada")]
        [ExcelMappedColumn("Entregada")]
        [GridColumn(Title = "Entregada", Width = "120px", SortEnabled = true)]
        public string DeliveryDateStr { get { return Utilities.Miscellaneous.GetDateFormat(DeliveryDate); } }
    }
}
