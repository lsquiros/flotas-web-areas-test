﻿/************************************************************************************************************
*  File    : HomeIndicators.cs
*  Summary : Home Indicators
*  Author  : Berman Romero
*  Date    : 17/Nov/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

namespace ECOsystem.Models.Core
{
    /// <summary>
    /// Super Admin Landing page Indicators
    /// </summary>
    public class SAIndicators
    {
        /// <summary>
        /// Partners
        /// </summary>
        public int Partners { get; set; }

        /// <summary>
        /// Customers
        /// </summary>
        public int Customers { get; set; }

        /// <summary>
        /// Vehicles
        /// </summary>
        public int Vehicles { get; set; }

        /// <summary>
        /// Drivers
        /// </summary>
        public int Drivers { get; set; }
    }


    /// <summary>
    /// Customer Admin Landing page Indicators
    /// </summary>
    public class CAIndicators
    {
        /// <summary>
        /// Routes
        /// </summary>
        public int Routes { get; set; }

        /// <summary>
        /// Vehicles
        /// </summary>
        public int Vehicles { get; set; }

        /// <summary>
        /// Drivers
        /// </summary>
        public int Drivers { get; set; }

        /// <summary>
        /// CreditCards
        /// </summary>
        public int CreditCards { get; set; }
    }


    /// <summary>
    /// Partner Admin Landing page Indicators
    /// </summary>
    public class PAIndicators
    {
        
        /// <summary>
        /// Customers
        /// </summary>
        public int Customers { get; set; }

        /// <summary>
        /// Cards
        /// </summary>
        public int Cards { get; set; }

        /// <summary>
        /// Vehicles
        /// </summary>
        public int Vehicles { get; set; }


        /// <summary>
        /// Users
        /// </summary>
        public int Users { get; set; }
    }

    /// <summary>
    /// Application Info
    /// </summary>
    public class AppInfo
    {
        public string Version { get; set; }
    }
}