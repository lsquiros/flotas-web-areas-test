﻿/************************************************************************************************************
*  File    : DashboardModuleModels.cs
*  Summary : Dashboard Module Models
*  Author  : Manuel Azofeifa Hidalgo
*  Date    : 02/Dic/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;

namespace ECOsystem.Models.Core
{
#pragma warning disable 108, 114
    /// <summary>
    /// 
    /// </summary>
    public class DashboardModuleBase
    {
        /// <summary>
        /// 
        /// </summary>
        public DashboardModuleBase()
        {
            Data = new DashboardModule();
            List = new List<DashboardModule>();
            Menus = new List<AccountMenus>();
        }
        /// <summary>
        /// Data
        /// </summary>
        public DashboardModule Data { get; set; }

        /// <summary>
        /// IEnumerable List of Modules
        /// </summary>
        public IEnumerable<DashboardModule> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Dashboard Module Models
    /// </summary>
    public class DashboardModule : ModelAncestor
    {
        /// <summary>
        /// Dashboard Module Id
        /// </summary>
        public int DashboardModuleId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }
        
        /// <summary>
        /// Order
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Logged User Id
        /// </summary>
        public int LoggedUserId { get; set; }

        /// <summary>
        /// Row Version
        /// </summary>
        public byte[] RowVersion { get; set; }
    }

    /// <summary>
    /// Statistics Indicator Model
    /// </summary>
    public class DashboardCoreIndicators
    {
        /// <summary>
        /// Quantity Vehicles Registered
        /// </summary>
        public int RegisterVehicles { get; set; }

        /// <summary>
        /// Quantity Vehicles With AVL
        /// </summary>
        public int VehiclesWithAVL { get; set; }

        /// <summary>
        /// Score Average Count
        /// </summary>
        public int ScoreAverageCount { get; set; }

        /// <summary>
        /// Score Average Count
        /// </summary>
        public decimal ScorePercentAverage { get; set; }

        /// <summary>
        /// Score Above Average
        /// </summary>
        public int ScoreAboveAverage { get; set; }

        /// <summary>
        /// Score Below Average
        /// </summary>
        public int ScoreBelowAverage { get; set; }

        /// <summary>
        /// Active Cards
        /// </summary>
        public int ActiveCards { get; set; }

        /// <summary>
        /// Request Cards
        /// </summary>
        public int RequestCards { get; set; }

        /// <summary>
        /// Register Cards
        /// </summary>
        public int RegisterCards {
            get {
                return ActiveCards + RequestCards;
            }
        }

        /// <summary>
        /// Cant. Transactions Process
        /// </summary>
        public int CantProcTransaction { get; set; }

        /// <summary>
        /// Cant. Transactions Deny
        /// </summary>
        public int CantDenyTransaction { get; set; }

        /// <summary>
        /// Total Transactions
        /// </summary>
        public int TotalTransactions {
            get {
                return CantDenyTransaction + CantProcTransaction;
            }
        }

        public string FuelAvailableLabels { get; set; }
        public string FuelAvailableAmmount { get; set; }

    }

    public class DashboardFuelAvailable 
    {
        public int FuelId { get; set; }
        public decimal? Available { get; set; }
        public string FuelName { get; set; }
    }

}
