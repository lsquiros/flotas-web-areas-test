﻿
/************************************************************************************************************
*  File    : SettingOdometersModels.cs
*  Summary : Setting Odometers Models
*  Author  : Stefano Quirós
*  Date    : 03/29/2016
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Core
{
#pragma warning disable 1591

    public class SettingOdometersBase
    {
        /// <summary>
        /// SetiingOdometers Base Constructor
        /// </summary>
       public SettingOdometersBase()
        {
            //Parameters = new ControlFuelsReportsBase();
            List = new List<SettingOdometers>();
            Menus = new List<AccountMenus>();
        }

       /// <summary>
       /// Is Result
       /// </summary>
       public bool IsResult { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        //public IEnumerable<dynamic> List { get; set; }
        public IEnumerable<SettingOdometers> List { get; set; }
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Transactions Report Model
    /// </summary>
    public class SettingOdometers
    {       

        /// <summary>
        /// Result after insert
        /// </summary>
        [NotMappedColumn]
        public int Result { get; set; }

        /// <summary>
        /// Result after insert
        /// </summary>
        [NotMappedColumn]
        public string ResultStr
        {
            get
            {
                switch (Result)
                {
                    case 0:
                        return "<span2 class='text-success glyphicon glyphicon-ok-circle' data-toggle='tooltip' data-placement='right' title='Registro actualizado exitosamente.'></span2>";
                    case -1:
                        return "<span2 class='text-warning glyphicon glyphicon-ban-circle' data-toggle='tooltip' data-placement='right' title='Dato existente, Acción: No reescribir valor.'></span2>";
                    case -100:
                        return "<span2 class='text-info glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' title='Registro recientemente incluido, No se tomó ninguna acción.'></span2>";
                    case 50001:
                        return "<span2 class='text-danger glyphicon glyphicon-remove-circle' data-toggle='tooltip' data-placement='right' title='Error: Falló por control de Versión'></span2>";
                    case 50002:
                        return "<span2 class='text-danger glyphicon glyphicon-remove-circle' data-toggle='tooltip' data-placement='right' title='Error: No hay disponible crédito'></span2>";
                    case 101:
                        return "<span2 class='text-info glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' title='No se tomó ninguna acción sobre este valor.'></span2>";
                    default:
                        return "<span2 class='text-danger glyphicon glyphicon-thumbs-down' data-toggle='tooltip' data-placement='right' title='Operación Inválida, Número error: " + Result + "'></span2>";
                }
            }
            set { }
        }



        /// <summary>
        /// TransactionId
        /// </summary>        
        public int? TransactionId { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        [GridColumn(Title = "Fecha", Width = "120px", SortEnabled = true)]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Date To Report Export
        /// </summary>
        [DisplayName("Fecha Emisión")]
        [NotMappedColumn]
        public string DateToExport
        {
            get
            {
                return ((Date != null) ? Convert.ToDateTime(Date).ToString("dd/MM/yyyy hh:mm:ss tt") : string.Empty);
            }
        }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Fecha", Width = "120px", SortEnabled = true)]
        public string DateStr
        {
            get
            {
                return Utilities.Miscellaneous.GetDateFormat(Date);
            }
        }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Fecha", Width = "120px", SortEnabled = true)]
        public string DateTimeStr { get { return Utilities.Miscellaneous.GetDateTimeFormat(Date); } }

        /// <summary>
        /// VehicleName
        /// </summary>
        [ExcelMappedColumn("Nombre del Vehiculo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }


        /// <summary>
        /// VehiclePlate
        /// </summary>
        [ExcelMappedColumn("Placa")]
        [NotMappedColumn]
        public string PlateId { get; set; }


        /// <summary>
        /// Recorrido
        /// </summary>
        [DisplayName("Consumo")]
        [GridColumn(Title = "Consumo", Width = "120px", SortEnabled = true)]
        public decimal Consumption { get { return ((Travel > 0) ? Decimal.Round(Travel / Performance, 2) : 0); } }

        /// <summary>
        /// Odometer
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double Odometer { get; set; }

        /// <summary>
        /// Odometer format
        /// </summary>
        [DisplayName("Odómetro")]
        [GridColumn(Title = "Odómetro", Width = "120px", SortEnabled = true)]
        public string OdometerStr { get { return Utilities.Miscellaneous.GetNumberFormat(Convert.ToDecimal(Odometer)); } }

        /// <summary>
        /// OdometerLast
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double OdometerLast { get; set; }

        /// <summary>
        /// Odometer format
        /// </summary>
        [DisplayName("Ultimo Odómetro")]
        [GridColumn(Title = "Ultimo Odómetro", Width = "120px", SortEnabled = true)]
        public string OdometerLastStr { get { return Utilities.Miscellaneous.GetNumberFormat(Convert.ToDecimal(OdometerLast)); } }


        /// <summary>
        /// Recorrido
        /// </summary>
        [DisplayName("Travel")]
        [GridColumn(Title = "Recorrido", Width = "120px", SortEnabled = true)]
        public decimal Travel { get { return (Convert.ToDecimal(Odometer - OdometerLast)); } set { } }

        /// <summary>
        /// Rendimiento
        /// </summary>
        [DisplayName("Performance")]
        [GridColumn(Title = "Rendimiento", Width = "120px", SortEnabled = true)]
        public decimal Performance { get { return ((Liters > 0) ? Decimal.Round(Travel / Liters, 2) : 0); } set { } }

         /// <summary>
        /// Rendimiento por default
        /// </summary>
        [DisplayName("DefaultPerformance")]
        [GridColumn(Title = "Rendimiento por Default", Width = "120px", SortEnabled = true)]
        public decimal DefaultPerformance { get; set; }


        /// <summary>
        /// Setting Odometers Filter
        /// </summary>
        public int OdometerType { get; set; }


        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);       
               

        /// <summary>
        /// FilterType
        /// </summary>        
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int FilterType { get; set; }

       
        /// <summary>
        /// Liters
        /// </summary>
        [DisplayName("Litros")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Litros", Width = "120px", SortEnabled = true)]
        public decimal Liters { get; set; }
         

        /// <summary>
        /// LitersLast
        /// </summary>
        [DisplayName("LitersLast")]
        [ExcelNoMappedColumn]
        public decimal LitersLast { get; set; }

        
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal CapacityUnitValue
        {
            get { return Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, Session.GetCustomerCapacityUnitId(), Liters); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [ExcelMappedColumn("Litros")]
        [GridColumn(Title = "Unidad de Capacidad", Width = "120px", SortEnabled = true)]
        public string CapacityUnitValueStr
        {
            get { return Utilities.Miscellaneous.GetNumberFormat(CapacityUnitValue); }
        }
              
                

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Fecha Creada")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public DateTime? InsertDate { get; set; }
       

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Customer ID")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int CustomerId { get; set; }        
        [DisplayName("Customer Name")]        
        [NotMappedColumn]
        public string CustomerName
        {
            get
            {
                return TripleDesEncryption.Decrypt(_customerName);
            }
            set
            {
                _customerName = value;
            }
        }

        private string _customerName;
    } 

    
}

