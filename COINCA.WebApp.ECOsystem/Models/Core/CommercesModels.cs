﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECOsystem.Models.Core
{
    public class CommercesBase
    {
        public CommercesBase()
        {
            Data = new Commerces();
            //Report = new CommercesReport();
            List = new List<Commerces>();
        }
        public Commerces Data { get; set; }
        //public CommercesReport Report { get; set; }
        public IEnumerable<Commerces> List { get; set; }
    }

    public class Commerces
    {
        public int? Id { get; set; }

        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Name { get; set; }

        [DisplayName("Descripción")]        
        public string Description { get; set; }

        [DisplayName("Latitud")]
        public double? Latitude { get; set; }

        [DisplayName("Longitud")]
        public double? Longitude { get; set; }

        [DisplayName("Dirección")]
        public string Address { get; set; }

        public int? Type { get; set; }

        public string Code { get; set; }

        public bool? FromMaps { get; set; }
    }

    //public class CommercesReport
    //{
    //    [DisplayName("Agente")]
    //    public int AgenteId { get; set; }
                
    //    private DateTime _StartDate;

    //    [DisplayName("Fecha Inicio")]
    //    public DateTime StartDate
    //    {
    //        get { return DateTime.Now; } 
    //        set { _StartDate = value; }
    //    }

    //    private DateTime _EndDate;

    //    [DisplayName("Fecha Fin")]
    //    public DateTime EndDate
    //    {
    //        get { return DateTime.Now; }
    //        set { _EndDate = value; }
    //    }
    //}
}