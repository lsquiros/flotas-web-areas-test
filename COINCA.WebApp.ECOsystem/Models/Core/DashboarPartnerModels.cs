﻿namespace ECOsystem.Models.Core
{
    /// <summary>
    /// Information to show in the dashboard for Partners
    /// </summary>
    public class DashboarPartnerModel
    {
        /// <summary>
        /// Vehicles
        /// </summary>
        public int Vehicles { get; set; }

        /// <summary>
        /// Vehicles AVL
        /// </summary>
        public int VehiclesAVL { get; set; }

        /// <summary>
        /// Customers
        /// </summary>
        public int Customers { get; set; }

        /// <summary>
        /// Last Customer in the month
        /// </summary>
        public int LastCustomer { get; set; }

        /// <summary>
        /// Credit card Active
        /// </summary>
        public int CCardsActive { get; set; }

        /// <summary>
        /// Credit card request
        /// </summary>
        public int CCardsRequest { get; set; }

        /// <summary>
        /// Total
        /// </summary>
        public int CreditCards 
        {
            get { return CCardsActive + CCardsRequest; }
        }
    }
}