﻿using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Models.Core
{
    public class ReportSources
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public int SysTypeId { get; set; }
    }

    public class ReportColumns
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public int Order { get; set; }
        public bool IsFormula { get; set; }
        public string Formula { get; set; }
        public string FormulaName { get; set; }
        public string FormulaDetail { get; set; }
        public bool IsEncrypted { get; set; }
        public bool FinalSummation { get; set; }
    }

    public class Reports
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public string DownloadName { get; set; }
        public string SheetName { get; set; }
    }

    public class CustomReport
    {

        public CustomReport()
        {
            Parameters = new ControlFuelsReportsBase();
            Menus = new List<AccountMenus>();
        }

        public int ReportId { get; set; }

        public string ReportName { get; set; }

        public string SheetName { get; set; }

        public List<ReportColumns> ColumnList { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

        public ControlFuelsReportsBase Parameters { get; set; }
    }
}
