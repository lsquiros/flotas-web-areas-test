﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Models.Core
{
    public class TransactionsTypes
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}