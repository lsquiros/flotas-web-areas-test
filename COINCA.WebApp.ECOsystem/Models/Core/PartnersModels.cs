﻿/************************************************************************************************************
*  File    : PartnersModels.cs
*  Summary : Partners Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Core
{
#pragma warning disable 1591

    /// <summary>
    /// Currencies Base
    /// </summary>
    public class PartnersBase
    {
        /// <summary>
        /// PartnersBase default constructor 
        /// </summary>
        public PartnersBase()
        {
            Data = new Partners();
            List = new List<Partners>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Partners Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Partners> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

    }    

    /// <summary>
    /// Partner Models
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Partners : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? PartnerId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("País")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int CountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("País")]
        [NotMappedColumn]
        public string CountryName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Logo")]
        [NotMappedColumn]
        public string Logo { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string ApiUserName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string ApiPassword { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Usuario API")]
        [NotMappedColumn]
        public string DisplayApiUserName
        {
            get { return TripleDesEncryption.Decrypt(ApiUserName); }
            set { ApiUserName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Contraseña API")]
        [NotMappedColumn]
        public string DisplayApiPassword
        {
            get { return TripleDesEncryption.Decrypt(ApiPassword); }
            set { ApiPassword = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Confirmar Contraseña")]
        [NotMappedColumn]
        public string PasswordConfirm { get; set; }

        /// <summary>
        /// Capacity Unit property
        /// </summary>
        [NotMappedColumn]
        public bool ChangePassword { get; set; }

        [DisplayName("Tipo de producto")]
        [NotMappedColumn]
        public int? ProductTypesId { get; set; }

        /// <summary>
        /// Capacity Unit property
        /// </summary>
        [DisplayName("Unidad de Medida de Combustible")]
        [NotMappedColumn]
        public int? CapacityUnitId { get; set; }

        /// <summary>
        /// Partner Type property Id
        /// </summary>
        [DisplayName("Tipo de Socio")]
        [NotMappedColumn]
        public int PartnerTypeId { get; set; }

        /// <summary>
        /// Partner Type property name
        /// </summary>
        [NotMappedColumn]
        public string PartnerTypeName { get; set; }

        /// <summary>
        /// Capacity Unit Name property
        /// </summary>
        [NotMappedColumn]
        public string CapacityUnitName { get; set; }

        /// <summary>
        /// Temp Users List
        /// </summary>
        [NotMappedColumn]
        public IList<PartnerTempUser> TempUsersList { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Porcentaje Error en Precio de Combustible")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int FuelErrorPercent { get; set; }

        /// <summary>
        /// RoleId of the Partner
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Rol")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string RoleName { get; set; }

        /// <summary>
        /// RoleName of the Customer Temp
        /// </summary>
        public string RoleIdTemp { get; set; }

        /// <summary>
        /// RoleName of the Customer Temp
        /// </summary>
        public string RoleNameTemp { get; set; }

        public string IVRLink { get; set; }

        [RegularExpression(@"^\(?([0-9]{3})\)?\d+$", ErrorMessage = "Digitar código de área y solo valores numéricos. Ej: (123)123456789")]
        public string IVRPhone { get; set; }

        public bool HasPayments { get; set; }

    }

    /// <summary>
    /// Partner Temp User Info
    /// </summary>
    public class PartnerTempUser
    {
        /// <summary>
        /// UserFullName
        /// </summary>
        public string UserFullName { get; set; }

        /// <summary>
        /// UserEmail
        /// </summary>
        public string UserEmail { get; set; }
        
        /// <summary>
        /// UserEmail
        /// </summary>
        public string RoleName { get; set; }


    }


}