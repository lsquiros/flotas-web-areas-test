﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
/************************************************************************************************************
*  File    : OperatorsModels.cs
*  Summary : Operators Models
*  Author  : Cristian Martínez
*  Date    : 01/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

namespace ECOsystem.Models.Core
{
    /// <summary>
    /// Operators Base Class for Views
    /// </summary>
    public class OperatorsBase 
    {
        /// <summary>
        /// OperatorsBase Constructor
        /// </summary>
        public OperatorsBase() 
        {
            Data = new Operators();
            List = new List<Operators>();
        }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public Operators Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Operators> List { get; set; }
    }
    
    /// <summary>
    /// Operators Model has all properties of Operator's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled=true, PageSize=30)]
    public class Operators : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? OperatorId { get; set; }

        /// <summary>
        /// Name of Operators entity
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre de Operador", Width = "250px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// Rate of operator
        /// </summary>
        [DisplayName("Tarifa")]
        [GridColumn(Title = "Tarifa", Width = "250px", SortEnabled = true)]
        [NotMappedColumn]
        public decimal? Rate { get; set; }


        /// <summary>
        /// String Rate of Operator for grid
        /// </summary>        
        [NotMappedColumn]
        [Required(ErrorMessage = "Tarifa es requerido")]
        public string RateStr
        {
            get
            {
                return Rate==null?String.Empty: ((decimal)Rate).ToString("N", CultureInfo.CreateSpecificCulture("es-CR"));
            }
        }

        /// <summary>
        /// String Rate of Operator for grid
        /// </summary>        
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Tarifa", Width = "250px", SortEnabled = true)]
        public string RateCurrencyStr {
            get {
                return Rate == null ? String.Empty : CurrencySymbol + ((decimal)Rate).ToString("N", CultureInfo.CreateSpecificCulture("es-CR"));
            }
        }

        /// <summary>
        /// Id Reference of currency
        /// </summary>
        [DisplayName("Tipo Moneda")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int CurrencyId { get; set; }

        /// <summary>
        /// Name of currency
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }
    }
}