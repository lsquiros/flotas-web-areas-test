﻿/************************************************************************************************************
*  File    : VehicleCategoriesModels.cs
*  Summary : VehicleCategories Models
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Xml.Linq;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using Newtonsoft.Json;

namespace ECOsystem.Models.Core
{
    /// <summary>
    /// VehicleCategories Base
    /// </summary>
    public class VehicleCategoriesBase
    {
        /// <summary>
        /// Vehicle Categories Base Default constructor
        /// </summary>

        public VehicleCategoriesBase()
        {
            Data = new VehicleCategories();
            List = new List<VehicleCategories>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleCategories Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleCategories> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }


    }


    /// <summary>
    /// VehicleCategories Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class VehicleCategories : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? VehicleCategoryId { get; set; }


        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Fabricante")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Fabricante", Width = "100px", SortEnabled = true)]
        public string Manufacturer { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Modelo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Modelo", Width = "100px", SortEnabled = true)]
        public string VehicleModel { get; set; }

        /// <summary>
        /// Vehicle year
        /// </summary>
        [DisplayName("Año")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? Year { get; set; }

        /// <summary>
        /// Weight
        /// </summary>
        [DisplayName("Peso (Toneladas)")]
        [NotMappedColumn]
        public decimal? Weight { get; set; }

        /// <summary>
        /// Weight
        /// </summary>
        [DisplayName("Peso (Toneladas)")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string WeightStr { get; set; }


        /// <summary>
        /// Vehicle year
        /// </summary>
        [DisplayName("Carga")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int LoadType { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Tipo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Tipo", Width = "50px", SortEnabled = true)]
        public string Type { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Combustible Predeterminado")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int DefaultFuelId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Litros")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int? Liters { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Litros")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Litros", Width = "50px", SortEnabled = true)]
        public string LitersStr {
            get { return (Liters != null) ? Utilities.Miscellaneous.GetNumberFormatWithDecimal((decimal)Liters, 3) : Utilities.Miscellaneous.GetNumberFormat(0); } 
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Combustible")]
        [GridColumn(Title = "Combustible", Width = "100px", SortEnabled = true)]
        public string FuelName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Icono")]
        [NotMappedColumn]
        public string Icon { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Velocidad Máxima")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? MaximumSpeed { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("RPM Máximo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? MaximumRPM { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Rendimiento")]
        [Required(ErrorMessage = "{0} es requerido")]
        //[RegularExpression(@"^\d+([,.])$", ErrorMessage = "Digitar solo números.")]//^\d+$
        [NotMappedColumn]
        public string DefaultPerformanceStr { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Rendimiento")]
        [NotMappedColumn]
        public decimal? DefaultPerformance { get; set; }
                
        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Velocidad Delimitada por Empresa (km/h)")]
        [Required(ErrorMessage = "Velocidad Delimitada es requerida")]
        [NotMappedColumn]
        public int? SpeedDelimited { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Cilindrada")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? CylinderCapacity { get; set; }

        private string _jsonFuelsList;
        /// <summary>
        /// Json Fuel sList
        /// </summary>
        [NotMappedColumn]
        public string JsonFuelsList
        {
            get { return _jsonFuelsList; }
            set
            {
                _jsonFuelsList = value;
                FuelsList = JsonFuelsList != null ? JsonConvert.DeserializeObject<IList<FuelsByVehicleCategory>>(value) : null;
            }
        }

        /// <summary>
        /// Fuels By Vehicle Category List
        /// </summary>
        [NotMappedColumn]
        public IList<FuelsByVehicleCategory> FuelsList { get; set; }

        /// <summary>
        /// Fuels List xml
        /// </summary>
        [NotMappedColumn]
        public string FuelsListXml
        {
            get
            {
                var doc = new XDocument(new XElement("xmldata"));
                var root = doc.Root;
                foreach (var item in FuelsList.Where(x => x.IsFuelChecked))
                {
                    root.Add(new XElement("Fuel", new XAttribute("FuelId", item.FuelId)));
                }
                return doc.ToString(); 
            }

        }


    }

    /// <summary>
    /// Fuels By Vehicle Category
    /// </summary>
    public class FuelsByVehicleCategory
    {
        /// <summary>
        /// FuelByVehicleCategoryId is a PK of FuelsByVehicleCategory Table
        /// </summary>
        public int? FuelByVehicleCategoryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public int? VehicleCategoryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public int? FuelId { get; set; }

        /// <summary>
        /// Fuel Name
        /// </summary>
        public string FuelName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public bool IsFuelChecked { get; set; }

    }
}