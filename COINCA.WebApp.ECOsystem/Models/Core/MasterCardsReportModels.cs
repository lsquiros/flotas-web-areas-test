﻿/************************************************************************************************************
*  File    : MasterCardsReportModels.cs
*  Summary : MasterCards Report Models
*  Author  : Danilo Hidalgo
*  Date    : 01/20/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Web.Mvc;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;

namespace ECOsystem.Models.Administration
{
    /// <summary>
    /// Applicants Report Base
    /// </summary>
    public class MasterCardsReportBase
    {
        /// <summary>
        /// Applicants Reports Base Constructor
        /// </summary>
        public MasterCardsReportBase()
        {
            Parameters = new AdministrationReportsBase();
            List = new List<dynamic>();
            UsersList = new List<SelectListItem>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public AdministrationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// List of users
        /// </summary>
        public IEnumerable<SelectListItem> UsersList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
}

    /// <summary>
    /// Applicants Report Model
    /// </summary>
    public class MasterCardsReport
    {

        /// <summary>
        /// Encrypted CreditCardNumber
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        //[GridColumn(Title = "Tarjeta", Width = "120px", SortEnabled = true)]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// Decryped CreditCardNumber
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Tarjeta", Width = "120px", SortEnabled = true)]
        public string DecrypedCreditCardNumber {
            get { return Utilities.TripleDesEncryption.Decrypt(CreditCardNumber); }
            set { CreditCardNumber = Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Credit Card Number with restrict display
        /// </summary>
        [DisplayName("Número Tarjeta")]
        [ExcelMappedColumn("Número Tarjeta")]
        [GridColumn(Title = "Tarjeta", Width = "120px", SortEnabled = true)]
        public string RestrictCreditCardNumber { get { return Utilities.Miscellaneous.GetDisplayCreditCardMask(DecrypedCreditCardNumber); } }

        /// <summary>
        /// Credit Card Holder
        /// </summary>
        [DisplayName("Titular")]
        [ExcelMappedColumn("Titular")]
        [GridColumn(Title = "Titular", Width = "120px", SortEnabled = true)]
        public string CreditCardHolder { get; set; }

        /// <summary>
        /// Credit Card Expiration
        /// </summary>
        [DisplayName("Vencimiento")]
        [ExcelMappedColumn("Vencimiento")]
        [GridColumn(Title = "Vencimiento", Width = "120px", SortEnabled = true)]
        public string Expiration { get; set; }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public decimal? CreditLimit { get; set; }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditLimitStr
        {
            get { return Utilities.Miscellaneous.GetNumberFormat(CreditLimit); }
        }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [ExcelMappedColumn("Monto Límite")]
        [GridColumn(Title = "Monto Límite", Width = "100px", SortEnabled = true)]
        public string CreditLimitDisplayStr
        {
            get { return Utilities.Miscellaneous.GetCurrencyFormat(CreditLimit, CurrencySymbol); }
        }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Credit Card Currency Name
        /// </summary>
        [DisplayName("Moneda")]
        [ExcelMappedColumn("Moneda")]
        [GridColumn(Title = "Moneda", Width = "60px", SortEnabled = true)]
        public string CurrencyName { get; set; }

        /// <summary>
        /// Credit Card Status
        /// </summary>
        [DisplayName("Status")]
        [ExcelMappedColumn("Estado")]
        [GridColumn(Title = "Status", Width = "60px", SortEnabled = true)]
        public string StatusName { get; set; }
        
    }
}
