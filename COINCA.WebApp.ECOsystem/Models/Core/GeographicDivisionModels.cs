﻿/************************************************************************************************************
*  File    : CountriesModels.cs
*  Summary : Countries Models
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Core
{
    /// <summary>
    /// States Base
    /// </summary>
    public class StatesBase
    {
        /// <summary>
        /// Default StatesBase constructor
        /// </summary>
        public StatesBase()
        {
            Data = new States();
            List = new List<States>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public States Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<States> List { get; set; }

    }


    /// <summary>
    /// States Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class States : ModelAncestor
    {
        /// <summary>
        /// State Id
        /// </summary>
        [NotMappedColumn]
        public int? StateId { get; set; }

        /// <summary>
        /// State Name
        /// </summary>
        [DisplayName("Nombre Estado")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre Estado", Width = "200px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// Country Id
        /// </summary>
        [NotMappedColumn]
        public int? CountryId { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        [DisplayName("Código")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Código", Width = "30px", SortEnabled = true)]
        public string Code { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        [DisplayName("Latitud")]
        [GridColumn(Title = "Latitud", Width = "50px", SortEnabled = true)]
        public double? Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        [DisplayName("Longitud")]
        [GridColumn(Title = "Longitud", Width = "50px", SortEnabled = true)]
        public double? Longitude { get; set; }
    }

    /// <summary>
    /// Counties Base
    /// </summary>
    public class CountiesBase
    {
        /// <summary>
        /// Default CountiesBase constructor
        /// </summary>
        public CountiesBase()
        {
            Data = new Counties();
            List = new List<Counties>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Counties Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Counties> List { get; set; }

    }


    /// <summary>
    /// Counties Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Counties : ModelAncestor
    {
        /// <summary>
        /// County Id
        /// </summary>
        [NotMappedColumn]
        public int? CountyId { get; set; }

        /// <summary>
        /// County Name
        /// </summary>
        [DisplayName("Nombre Condado")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre Condado", Width = "200px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// State Id
        /// </summary>
        [NotMappedColumn]
        public int? StateId { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        [DisplayName("Código")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Código", Width = "30px", SortEnabled = true)]
        public string Code { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        [DisplayName("Latitud")]
        [GridColumn(Title = "Latitud", Width = "50px", SortEnabled = true)]
        public double? Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        [DisplayName("Longitud")]
        [GridColumn(Title = "Longitud", Width = "50px", SortEnabled = true)]
        public double? Longitude { get; set; }

        /// <summary>
        /// Country Id
        /// </summary>
        [NotMappedColumn]
        public int? CountryId { get; set; }
    
    }

    /// <summary>
    /// Cities Base
    /// </summary>
    public class CitiesBase
    {
        /// <summary>
        /// Default CitiesBase constructor
        /// </summary>
        public CitiesBase()
        {
            Data = new Cities();
            List = new List<Cities>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Cities Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Cities> List { get; set; }

    }


    /// <summary>
    /// Cities Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Cities : ModelAncestor
    {
        /// <summary>
        /// City Id
        /// </summary>
        [NotMappedColumn]
        public int? CityId { get; set; }

        /// <summary>
        /// City Name
        /// </summary>
        [DisplayName("Nombre Ciudad")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre Ciudad", Width = "200px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// County Id
        /// </summary>
        [NotMappedColumn]
        public int? CountyId { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        [DisplayName("Código")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Código", Width = "30px", SortEnabled = true)]
        public string Code { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        [DisplayName("Latitud")]
        [GridColumn(Title = "Latitud", Width = "50px", SortEnabled = true)]
        public double? Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        [DisplayName("Longitud")]
        [GridColumn(Title = "Longitud", Width = "50px", SortEnabled = true)]
        public double? Longitude { get; set; }

        /// <summary>
        /// State Id
        /// </summary>
        [NotMappedColumn]
        public int? StateId { get; set; }

        /// <summary>
        /// Country Id
        /// </summary>
        [NotMappedColumn]
        public int? CountryId { get; set; }

    }

}