﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ECOsystem.Business.Core;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Core
{
#pragma warning disable 1591

    public class DallasKeysBase
    {
         /// <summary>
        /// Dallas Keys Base Default constructor
        /// </summary>

        public DallasKeysBase()
        {
            Data = new DallasKeys();
            List = new List<DallasKeys>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public DallasKeys Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<DallasKeys> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// DallasKeys Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class DallasKeys : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? DallasId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? CustomerId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? UserId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Código Dallas")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Código Dallas", Width = "100px", SortEnabled = true)]
        public string DallasCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [GridColumn(Title = "En Uso", Width = "100px", SortEnabled = true)]
        public bool IsUsed { get; set; }

        /// <summary>
        /// DriverName
        /// </summary>
        public string DriverName
        {
            get
            {
                return (CurrentDriver != null) ? CurrentDriver.DecryptedName : "Ninguno";
            }
        }

        /// <summary>
        /// CurrentDriver
        /// </summary>
        public Users CurrentDriver
        {
            get
            {
                if (UserId == null) return null;
                using (var business = new UsersBusiness())
                {
                    var user = business.RetrieveUsers(UserId).FirstOrDefault();
                    return user;
                }
            }
        }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool IsDeleted { get; set; }

    }
}