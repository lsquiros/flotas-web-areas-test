﻿/************************************************************************************************************
*  File    : UsersModels.cs
*  Summary : Users Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Script.Serialization;
using ECOsystem.Business.Core;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECOsystem.Models.Core
{
    /// <summary>
    /// Users Base Class for Views
    /// </summary>
    public class UsersBase
    {
        /// <summary>
        /// UsersBase Constructor
        /// </summary>
        public UsersBase()
        {
            Parameters = new AdminUsersReportsBase ();
            Data = new Users();
            List = new List<Users>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public AdminUsersReportsBase Parameters { get; set; }
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Users Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Users> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }


    /// <summary>
    /// User Model has all properties of User's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Users : ModelAncestor
    {

        public override string ToString()
        {
            return $"PhoneNumber: {DecryptedPhoneNumber} || Name:{DecryptedName}";
        }
        /// <summary>
        /// UsersBase Constructor
        /// </summary>
        public Users()
        {
            DriverUser = new DriversUsers();
            List = new List<CustomerByPartner>();
            Menus = new List<AccountMenus>();
            CodView = "DriMan";
        }

        /// <summary>
        /// For get Alarm of Vehicle
        /// </summary>
        [NotMappedColumn]
        public string CodView { get; set; }

        /// <summary>
        /// FK Periodicity Type from Types
        /// </summary>
        [DisplayName("Periodicidad:")]
        //[RequiredIf("AlarmTriggerId == 300", ErrorMessage = "{0} es requerido")]
        [RequiredIf("PeriodicityTypeId == 300", ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? PeriodicityTypeId { get; set; }

        #region PartnerUser Information

        /// <summary>
        /// CustomerUserId is a PK of CustomerUsers Table
        /// </summary>
        [NotMappedColumn]
        public int? PartnerUserId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Socio")]
        [NotMappedColumn]
        public int PartnerId { get; set; }

        /// <summary>
        /// RoleName
        /// </summary>
        [NotMappedColumn]
        public string Name { get; set; }


        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int PartnerGroupId { get; set; }


        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        [NotMappedColumn]
        public DateTime? InsertDate { get; set; }

        /// <summary>
        /// Insert Date as string
        /// </summary>
        [NotMappedColumn]
        public string InsertDateStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(InsertDate); }
        }

        /// <summary>
        /// Is BAC flag
        /// </summary>
        [NotMappedColumn]
        public int? TypeId { get; set; } 

        #endregion

        /// <summary>
        /// CustomerUserId is a PK of CustomerUsers Table
        /// </summary>
        [NotMappedColumn]
        public int? CustomerUserId { get; set; }
        
        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Ajuste de Odómetros")]
        [NotMappedColumn]
        public bool ChangeOdometer { get; set; }
        
        #region DriversInformation

        /// <summary>
        /// UserId is a PK of Users Table
        /// </summary>
        [NotMappedColumn]
        public int? UserId { get; set; }

        /// <summary>
        /// DriversUserId is a PK of DriversUsers Table
        /// </summary>
        [NotMappedColumn]
        public int? DriversUserId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Cliente")]
        [NotMappedColumn]
        public int? CustomerId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre", Width = "250px", SortEnabled = true)]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
            //set { EncryptedName = Utilities.TripleDesEncryption.Encrypt(value); }
        }

          /// <summary>
        /// property
        /// </summary>
        [DisplayName("Identificación")]
        [NotMappedColumn]
        public string EncryptedIdentification { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Identificación")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DecryptedIdentification
        {
            get { return TripleDesEncryption.Decrypt(EncryptedIdentification); }
            set { EncryptedIdentification = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Código")]
        [NotMappedColumn]
        public string EncryptedCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Código")]
        [NotMappedColumn]
        public string DecryptedCode
        {
            get { return TripleDesEncryption.Decrypt(EncryptedCode); }
            set { EncryptedCode = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("No. Licencia")]
        [NotMappedColumn]
        public string EncryptedLicense { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("No. Licencia")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        [NotMappedColumn]
        public string DecryptedLicense
        {
            get { return TripleDesEncryption.Decrypt(EncryptedLicense); }
            set { EncryptedLicense = TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// Driver License Expiration
        /// </summary>
        [DisplayName("Fecha Expiración")]
        [NotMappedColumn]
        public DateTime? LicenseExpiration { get; set; }

        /// <summary>
        /// Driver License Expiration as String
        /// </summary>
        [DisplayName("Fecha Expiración")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string LicenseExpirationStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(LicenseExpiration); }
            set { LicenseExpiration = Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Dallas")]
        [NotMappedColumn]
        public string EncryptedDallas { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Llave Dallas")]
        [NotMappedColumn]
        public string DecryptedDallas
        {
            get { return TripleDesEncryption.Decrypt(EncryptedDallas); }
            set { EncryptedDallas = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Llave Dallas")]
        [NotMappedColumn]
        public int? DallasId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        [ScriptIgnore]
        public DallasKeys DallasKey
        {
            get
            {
                if (DallasId != null)
                {
                    using (var business = new DallasKeysBusiness())
                    {
                        var dk = business.RetrieveDallasKeys(DallasId).FirstOrDefault();
                        if (dk != null)
                        {
                            return dk;
                        }
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int CountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Máx. Transacciones diario")]
        [Range(0, 100, ErrorMessage="Debe ser menor a las 100 transacciones.")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? DailyTransactionLimit { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Solicitud de Tarjeta")]
        [Required(ErrorMessage = "Solicitud de Tarjeta es requerido")]
        [NotMappedColumn]
        public int? CardRequestId { get; set; }

        /// <summary>
        /// PullPreviousBudget
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Trasladar el disponible al siguiente mes")]
        public bool PullPreviousBudget { get; set; }
		#endregion

		#region Dynamic Filter 

		public int? SourceCustomerId { get; set; }

		public string EncryptedSourceCustomerName { get; set; }

		public string DecryptedSourceCustomerName
		{
			get { return TripleDesEncryption.Decrypt(EncryptedSourceCustomerName).Trim(); }
			set { EncryptedSourceCustomerName = TripleDesEncryption.Encrypt(value.ToLower()); }
		}
		#endregion

		/// <summary>
		/// Name of current entity
		/// </summary>
		[DisplayName("Correo")]
        [NotMappedColumn]
        public string EncryptedEmail { get; set; }

        /// <summary>
        /// Decrypted Email
        /// </summary>
        [DisplayName("Correo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [EmailAddress(ErrorMessage = "Por favor, digite una dirección de correo válida.")]
        [NotMappedColumn]
        public string DecryptedEmail
        {
            get { return TripleDesEncryption.Decrypt(EncryptedEmail).Trim(); }
            set { EncryptedEmail = TripleDesEncryption.Encrypt(value.ToLower()); }
            //set { EncryptedEmail = Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Contraseña")]
        [NotMappedColumn]
        public string EncryptedPassword { get; set; }

        /// <summary>
        /// Decrypted Email
        /// </summary>
        [DisplayName("Correo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [EmailAddress(ErrorMessage = "Por favor, digite una contrseña válida.")]
        [NotMappedColumn]
        public string DecryptedPassword
        {
            get { return TripleDesEncryption.Decrypt(EncryptedPassword).Trim(); }
            set { EncryptedPassword = TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// Email for send of the alarm
        /// </summary>
        [DisplayName("Correos:")]
        [Required(ErrorMessage = "{0} es requerido.")]
        [NotMappedColumn]
        public string Emails { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Celular")]
        [NotMappedColumn]
        public string EncryptedPhoneNumber { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Celular")]
        [NotMappedColumn]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public string DecryptedPhoneNumber
        {
            get { return TripleDesEncryption.Decrypt(EncryptedPhoneNumber); }
            set { EncryptedPhoneNumber = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Whatsapp bit 
        /// send mesage
        /// </summary>
        public short Whatsapp { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Usuario Web")]
        [NotMappedColumn]
        public string EncryptedUserName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Usuario Web")]
        [GridColumn(Title = "Usuario Web", Width = "200px", SortEnabled = true)]
        public string DecryptedUserName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedUserName); }
            set { EncryptedUserName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Foto")]
        [NotMappedColumn]
        public string Photo { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Logo Compañía")]
        [NotMappedColumn]
        public string EntityLogo { get; set; }
        

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Activo")]
        [NotMappedColumn]
        public bool IsActive { get; set; }


        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Bloqueado")]
        [NotMappedColumn]
        public bool IsLockedOut { get; set; }


        /// <summary>
        /// Active Alarm
        /// </summary>
        [DisplayName("Habilitada")]
        [NotMappedColumn]
        public bool Active { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Cambiar Contraseña")]
        [NotMappedColumn]
        public bool ChangePassword { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Rol")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string RoleId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Rol")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string RoleName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Cambiar Contraseña Anterior")]
        [NotMappedColumn]
        public bool ChangePrevPassword { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Contraseña Anterior")]
        [NotMappedColumn]
        public string LabelPrevPassword { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Contraseña Anterior")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string PrevPasswordTemp { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("RoleIdTemp")]
        [NotMappedColumn]
        public string RoleIdTemp { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("RoleNameTemp")]
        [NotMappedColumn]
        public string RoleNameTemp { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("CustomerNameTemp")]
        [NotMappedColumn]
        public string CustomerNameTemp { get; set; }

        /// <summary>
        /// TemporalCustomerId
        /// </summary>         
        [NotMappedColumn]
        public int? ProfileIdTemp { get; set; }

        /// <summary>
        /// Customer User Aditional Information
        /// </summary>
        [NotMappedColumn]
        public CustomerUsers CustomerUser { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool IsCustomerUser { get; set; }

        /// <summary>
        /// Drivers User Aditional Information
        /// </summary>
        [NotMappedColumn]
        public DriversUsers DriverUser { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool IsDriverUser { get; set; }

        /// <summary>
        /// Partner User Aditional Information
        /// </summary>
        [NotMappedColumn]
        public PartnerUsers PartnerUser { get; set; }

        /// <summary>
        /// Logged User, is customer or partner
        /// </summary>
        public string LoggedUser { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool IsPartnerUser { get; set; }

        /// <summary>
        /// property
        /// </summary>
        private bool _IsCoincaUser;
        [NotMappedColumn]
        public bool IsCoincaUser
        {
            get { return !IsCustomerUser && !IsDriverUser && !IsPartnerUser; }
            set { _IsCoincaUser = value; }
        }

        /// <summary>
        /// Password Expiration Date
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Vencimiento de la contraseña")]
        public DateTime? PasswordExpirationDate { get; set; }

         
        [NotMappedColumn]
        public bool IsAgent { get; set; }

        /// <summary>
        /// Tipo Agente o Conductor
        /// </summary>
        [NotMappedColumn]
        public string Type { get { return IsAgent == true ? "Agente" : "Conductor"; } }

        /// <summary>
        /// HasWhatsApp
        /// </summary>
        [NotMappedColumn]
        public bool HasWhatsApp { get; set; }

        /// <summary>
        /// Password Expired Indicator
        /// </summary>
        [NotMappedColumn]
        public bool IsPasswordExpired { get; set; }

        /// <summary>
        /// Email  Expired Confirmed
        /// </summary>
        [NotMappedColumn]
        public bool IsEmailConfirmed { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<CustomerByPartner> List { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Asignar Cliente")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string ClientName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Asignar Cliente")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string ClientId { get; set; }

        /// <summary>
        /// Validation for the Service Stations
        /// </summary>
        public bool IsServiceStationUser { get; set; }

        /// <summary>
        /// Information of the list of service stations
        /// </summary>
        public IEnumerable<ServiceStationsUser> ServiceStationUser { get; set; }

        /// <summary>
        /// CostCenterId
        /// </summary>
        [DisplayName("Centro de Costo")]
        public int? CostCenterId { get; set; }

        public bool? ShowNews { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Alarms of Drivers
        /// </summary>
        public AlarmsModels Alarm { get; set; }

        public bool EmailConfirmed { get; set; }

        public bool AddCustomers { get; set; }

        public bool IsSupportUser { get; set; }

        public string MapsToken { get; set; }

        public string MapsRefreshToken { get; set; }

        public DateTime MapsExpirateDateToken { get; set; }

        public double MapsExpirateTimeToken
        {
            get
            {
                var time = Math.Floor((MapsExpirateDateToken - DateTime.Now).TotalMinutes);
                return time > 0 ? time : 0;
            }
        }
    }

    /// <summary>
    /// Customer Users Information
    /// </summary>
    public class CustomerUsers
    {
        /// <summary>
        /// CustomerUserId is a PK of CustomerUsers Table
        /// </summary>
        [NotMappedColumn]
        public int? CustomerUserId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Cliente")]
        [NotMappedColumn]
        public int? CustomerId { get; set; }
  


        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int CountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Identificación")]
        [NotMappedColumn]
        public string EncryptedIdentification { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Identificación")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DecryptedIdentification
        {
            get { return TripleDesEncryption.Decrypt(EncryptedIdentification); }
            set { EncryptedIdentification = TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Ajuste de Odómetros")]
        [NotMappedColumn]
        public bool ChangeOdometer { get; set; }

    }

    /// <summary>
    /// Drivers Users Information
    /// </summary>
    public class DriversUsers
    {
        /// <summary>
        /// DriversUserId is a PK of DriversUsers Table
        /// </summary>
        [NotMappedColumn]
        public int? DriversUserId { get; set; }

        /// <summary>
        /// UserId 
        /// </summary>
        [NotMappedColumn]
        public int? UserId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Cliente")]
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre", Width = "250px", SortEnabled = true)]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
            //set { EncryptedName = Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Correo")]
        [NotMappedColumn]
        public string EncryptedEmail { get; set; }

        /// <summary>
        /// Decrypted Email
        /// </summary>
        [DisplayName("Correo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [EmailAddress(ErrorMessage = "Por favor, digite una dirección de correo válida.")]
        [NotMappedColumn]
        public string DecryptedEmail
        {
            get { return TripleDesEncryption.Decrypt(EncryptedEmail).Trim(); }
            set { EncryptedEmail = TripleDesEncryption.Encrypt(value.ToLower()); }
            //set { EncryptedEmail = Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Identificación")]
        [NotMappedColumn]
        public string EncryptedIdentification { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Identificación")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DecryptedIdentification
        {
            get { return TripleDesEncryption.Decrypt(EncryptedIdentification); }
            set { EncryptedIdentification = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Código")]
        [NotMappedColumn]
        public string EncryptedCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Código")]
        [NotMappedColumn]
        public string DecryptedCode
        {
            get { return TripleDesEncryption.Decrypt(EncryptedCode); }
            set { EncryptedCode = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("No. Licencia")]
        [NotMappedColumn]
        public string EncryptedLicense { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("No. Licencia")]
        [Required(ErrorMessage = "{0} es requerido")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        [NotMappedColumn]
        public string DecryptedLicense
        {
            get { return TripleDesEncryption.Decrypt(EncryptedLicense); }
            set { EncryptedLicense = TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// Driver License Expiration
        /// </summary>
        [DisplayName("Fecha Expiración")]
        [NotMappedColumn]
        public DateTime? LicenseExpiration { get; set; }

        /// <summary>
        /// Driver License Expiration as String
        /// </summary>
        [DisplayName("Fecha Expiración")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string LicenseExpirationStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(LicenseExpiration); }
            set { LicenseExpiration = Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Dallas")]
        [NotMappedColumn]
        public string EncryptedDallas { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Llave Dallas")]
        [NotMappedColumn]
        public string DecryptedDallas
        {
            get { return TripleDesEncryption.Decrypt(EncryptedDallas); }
            set { EncryptedDallas = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Llave Dallas")]
        [NotMappedColumn]
        public int? DallasId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        [ScriptIgnore]
        public DallasKeys DallasKey
        {
            get
            {
                if (DallasId != null)
                {
                    using (var business = new DallasKeysBusiness())
                    {
                        var dk = business.RetrieveDallasKeys(DallasId).FirstOrDefault();
                        if (dk != null)
                        {
                            return dk;
                        }
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int CountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Máx. Transacciones diario")]
        [Range(0, 100, ErrorMessage="Debe ser menor a las 100 transacciones.")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? DailyTransactionLimit { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Solicitud de Tarjeta")]
        [Required(ErrorMessage = "Solicitud de Tarjeta es requerido")]
        [NotMappedColumn]
        public int? CardRequestId { get; set; }

        [NotMappedColumn]
        public string LastDateDriving { get; set; }
       
        [NotMappedColumn]
        public string LastDateDrivingConvert
        {
            get
            {
                switch (LastDateDriving)
                {
                    case null: 
                        return "Actualmente Activo&nbsp;<span2 class='text-success mdi mdi-checkbox-marked-circle' data-toggle='tooltip' data-placement='right'></span2>";
                    default:
                        return LastDateDriving;
                }
            }
            set { }
        }
    }

    /// <summary>
    /// PartnerUsers Information
    /// </summary>
    public class PartnerUsers
    {
        /// <summary>
        /// CustomerUserId is a PK of CustomerUsers Table
        /// </summary>
        [NotMappedColumn]
        public int? PartnerUserId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Socio")]
        [NotMappedColumn]
        public int PartnerId { get; set; }

        /// <summary>
        /// RoleName
        /// </summary>
        [NotMappedColumn]
        public string RoleName { get; set; }

        /// <summary>
        /// RoleName
        /// </summary>
        [NotMappedColumn]
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int CountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int PartnerGroupId { get; set; }


        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        [NotMappedColumn]
        public DateTime? InsertDate { get; set; }

        /// <summary>
        /// Insert Date as string
        /// </summary>
        [NotMappedColumn]
        public string InsertDateStr
        {
            get { return Utilities.Miscellaneous.GetDateFormat(InsertDate); }
        }

        /// <summary>
        /// Countries Access Xml
        /// </summary>
        [DisplayName("País de Acceso")]
        [NotMappedColumn]
        public string CountriesAccessXml { get; set; }

        /// <summary>
        /// Countries Access codes
        /// </summary>
        [NotMappedColumn]
        public string CountriesAccessCodes
        {
            get
            {
                return string.Join<string>(",", CountriesAccessList == null ? new List<string>() : CountriesAccessList.Where(x => x.IsCountryChecked).Select(x => x.CountryCode));
            }
        }

        /// <summary>
        /// Country Access List
        /// </summary>
        public IList<PartnerUsersByCountry> CountriesAccessList { get; set; }

    }

    /// <summary>
    /// Partner Users By Country
    /// </summary>
    public class PartnerUsersByCountry
    {
        /// <summary>
        /// PartnerUsersByCountryId is a PK of PartnerUsersByCountry Table
        /// </summary>
        [NotMappedColumn]
        public int? PartnerUsersByCountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int PartnerUserId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int CountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string CountryName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string CountryCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool IsCountryChecked { get; set; }


    }

    public class ServiceStationsUser {

         /// <summary>
        /// List of the services stations
        /// </summary>
        public int ServiceStationId { get; set; }

        /// <summary>
        /// PartnerId associate to the service station
        /// </summary>
        public int PartnerId { get; set; }
    }

}
