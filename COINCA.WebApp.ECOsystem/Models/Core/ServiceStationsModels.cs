﻿
using System.Collections.Generic;
using GridMvc.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ECOsystem.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using System.Globalization;

namespace ECOsystem.Models.Core
{
    /// <summary>
    /// Base Model
    /// </summary>
    public class ServiceStationsBase
    {
        /// <summary>
        /// Constructor ServiceStationsBase
        /// </summary>
        public ServiceStationsBase()
        {
            Data = new ServiceStations();
            List = new List<ServiceStations>();
            Menus = new List<AccountMenus>();
        }
        /// <summary>
        /// Service statios model
        /// </summary>
        public ServiceStations Data { get; set; }

        /// <summary>
        /// ServiceStatiosList
        /// </summary>
        public IEnumerable<ServiceStations> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }

    /// <summary>
    /// Service statios model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class ServiceStations
    {
        /// <summary>
        /// Constructor ServiceStationsBase
        /// </summary>
        public ServiceStations()
        {
            CostCenterList = new List<BlockedCostsCenter>();
        }

        /// <summary>
        /// ServiceStatiosList
        /// </summary>
        public List<BlockedCostsCenter> CostCenterList { get; set; }

        /// <summary>
        /// ServiceStationId
        /// </summary>
        public int? ServiceStationId { get; set; }

        /// <summary>
        /// BacId
        /// </summary>
        [DisplayName("Afiliado")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string BacId { get; set; }


        /// <summary>
        /// Name
        /// </summary>
        [DisplayName("Nombre Afiliado")]
        [MaxLength(50, ErrorMessage = "Nombre Afiliado no debe ser mayor a 50 caracteres")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Name { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        [DisplayName("Dirección")]
        public string Address { get; set; }

        /// <summary>
        /// Canton
        /// </summary>
        public int CantonId { get; set; }

        /// <summary>
        /// Show the text for the Canton
        /// </summary>
        [DisplayName("Cantón")]
        public string CantonString
        {
            get { return ServiceStationsBusiness.GetCantonNameById(CantonId); }
        }

        /// <summary>
        /// Province
        /// </summary>
        public int ProvinceId { get; set; }

        /// <summary>
        /// CountryId 
        /// </summary>
        public int CountryId
        {
            get
            {
                var user = Session.GetUserInfo();

                if (user.PartnerId == null)
                {
                    return 0;
                }
                else
                {
                    int country = 0;
                    country = user.CountryId;
                    return country;
                }
            }
        }

        /// <summary>
        /// Show the text for the Provincia
        /// </summary>
        [DisplayName("Provincia")]
        public string ProvinceString
        {
            get { return ServiceStationsBusiness.GetProvinceNameById(ProvinceId); }
        }

        /// <summary>
        /// Latitude
        /// </summary>
        [DisplayName("Latitud")]
        public double? Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        [DisplayName("Longitud")]
        public double? Longitude { get; set; }

        /// <summary>
        /// SAP Prov
        /// </summary>
        [DisplayName("Número de Proveedor - SAP")]
        public string SAPProv { get; set; }

        /// <summary>
        /// Legal_Id
        /// </summary>
        [DisplayName("Cédula Jurídica")]
        [MaxLength(15, ErrorMessage = "Cédula Jurídica no debe ser mayor a 15 caracteres")]
        public string Legal_Id { get; set; }

        /// <summary>
        /// Terminal
        /// </summary>
        [DisplayName("Terminal")]
        [MaxLength(8, ErrorMessage = "Terminal no debe ser mayor a 8 caracteres")]
        public string Terminal { get; set; }

        /// <summary>
        /// Number_Provider
        /// </summary>
        [MaxLength(14, ErrorMessage = "Número de Proveedor no debe ser mayor a 14 caracteres")]
        [DisplayName("Número de Proveedor")]
        public string Number_Provider { get; set; }


        /// <summary>
        /// Top Monetary Limit
        /// </summary>
        [DisplayName("Tope Monetario")]
        public decimal? TopMonetaryLimit { get; set; }

        /// <summary>
        /// Total as String
        /// </summary>
        public string TopMonetaryLimitStr
        {
            get { return TopMonetaryLimit == null ? "" : ((decimal)TopMonetaryLimit).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }


        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<SelectListItem> ProvList
        {
            get
            {
                if (CountryId == 0)
                {
                    var list = new List<SelectListItem>();
                    return list;
                }
                else
                {
                    return ServiceStationsBusiness.GetStatesServiceStations(CountryId);
                }
            }
        }

        /// <summary>
        /// CantonLists
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<SelectListItem> CantList
        {
            get
            {
                if (ProvinceId == 0)
                {
                    var list = new List<SelectListItem>();
                    return list;
                }
                else
                {
                    return ServiceStationsBusiness.GetCountiesEditServiceStatios(ProvinceId, CountryId);
                }
            }
        }

        /// <summary>
        /// Is Blocked
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Estación Bloqueada")]
        public bool IsBlocked { get; set; }

        public List<Terminals> TerminalList { get; set; }

        [NotMappedColumn]
        public int? PartnerId { get; set; }

        [NotMappedColumn]
        public int CostCenterId { get; set; }

        [NotMappedColumn]
        [DisplayName("Bloquear Centro de Costo")]
        public string CostCenterName { get; set; }

        public bool CostCenterBlocked { get; set; }

        public string TerminalId { get; set; }

        /// <summary>
        /// OverView elements by Class
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"BacId: {BacId} Name:{Name} Legal_Id:{Legal_Id}";
    }
    /// <summary>
    /// Drivers Error Class
    /// </summary>
    public class ServiceStationErrors
    {
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public string Item { get; set; }
    }

    public class Terminals
    {
        public int? Id { get; set; }

        public string TerminalId { get; set; }

        public int? ServiceStationId { get; set; }
    }

    public class BlockedCostsCenter
    {
        public int CostCenterId { get; set; }

        public string CostCenterName { get; set; }
    }
}