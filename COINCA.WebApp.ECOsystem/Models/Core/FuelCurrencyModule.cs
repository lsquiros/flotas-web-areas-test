﻿using System.ComponentModel.DataAnnotations;

namespace ECOsystem.Models.Core
{
    public class FuelCurrencyModule
    {
        [Display(Name = "Cálculo de distribución de combustible por monto.")]
        public bool ByCurrency { get; set; }
        [Display(Name = "Cálculo de distribución de combustible por Combustible.")]
        public bool ByFuel { get; set; }
        public int CustomerId { get; set; }
    }
}
