﻿$(function () {
    //Detail News Reveal
    $("[detail-news-id]").click(function () {

        $("#news-title").html("");
        $("#news-content").html("");

        var idNews = $(this).attr("detail-news-id");
        var title = $("#news-title-" + idNews).val();
        var content = $("#news-content-" + idNews).val();
        $("#news-title").html(title);
        $("#news-content").html(content);
    });


    //Quotes rotator
    var divs = $('.cbp-qtcontent');

    function fade() {
        var current = $('.current');
        var currentIndex = divs.index(current),
            nextIndex = currentIndex + 1;

        if (nextIndex >= divs.length) {
            nextIndex = 0;
        }

        var next = divs.eq(nextIndex);

        next.stop().fadeIn(800, function () {
            $(this).addClass('current');
        });

        current.stop().fadeOut(800, function () {
            $(this).removeClass('current');
            _startProgress()
            setTimeout(fade, 8000);
        });
    }

    function _startProgress() {
        $(".cbp-qtprogress").removeAttr('style');
        $(".cbp-qtprogress").animate({
            width: "92%",
        }, 8000);
    }

    _startProgress()
    setTimeout(fade, 8000);
    
});