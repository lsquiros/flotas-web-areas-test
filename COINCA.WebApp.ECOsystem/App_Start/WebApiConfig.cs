﻿/************************************************************************************************************
*  File    : WebApiConfig.cs
*  Summary : WebApiConfig for Web API 
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Web.Http;

namespace ECOsystem
{
    /// <summary>
    /// WebApiConfig Class
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Required Register Method
        /// </summary>
        /// <param name="config">Represents a configuration of <see cref="T:System.Web.Http.HttpServer"/> instances.</param>
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
              name: "AreaApi",
              routeTemplate: "{area}/api/{controller}/{id}",
              defaults: new { id = RouteParameter.Optional }
          );
            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();
        }
    }
}
