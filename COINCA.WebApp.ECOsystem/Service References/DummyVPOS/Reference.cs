﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ECOsystem.DummyVPOS {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="clsWSResponseData", Namespace="http://schemas.datacontract.org/2004/07/COINCA.WebService.TestingBAC.Models")]
    [System.SerializableAttribute()]
    public partial class clsWSResponseData : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string addressValidationField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string authorizationNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string avsValidationField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string balanceAmountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string cardHolderNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string clientNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string confirmNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string detResponseCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string detResponseCodeDescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string fraudScoreField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string giftMessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string hostDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string hostTimeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string iccHostTagsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryAmount1Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryAmount2Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryAmount3Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryAmount4Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryAmount5Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryAmount6Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryAmount7Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryAmount8Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryCashPayDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryCurrency1Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryCurrency2Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryMinPayDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string inquiryTypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string invoiceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string referenceNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string refundsAmountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string refundsTransactionsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string responseCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string responseCodeDescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string salesAmountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string salesTaxField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string salesTaxDiscountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string salesTipField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string salesTransactionsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string securityCodeValidationField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string systemTraceNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string taxDiscountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string transactionIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string zipCodeValidationField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string addressValidation {
            get {
                return this.addressValidationField;
            }
            set {
                if ((object.ReferenceEquals(this.addressValidationField, value) != true)) {
                    this.addressValidationField = value;
                    this.RaisePropertyChanged("addressValidation");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string authorizationNumber {
            get {
                return this.authorizationNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.authorizationNumberField, value) != true)) {
                    this.authorizationNumberField = value;
                    this.RaisePropertyChanged("authorizationNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string avsValidation {
            get {
                return this.avsValidationField;
            }
            set {
                if ((object.ReferenceEquals(this.avsValidationField, value) != true)) {
                    this.avsValidationField = value;
                    this.RaisePropertyChanged("avsValidation");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string balanceAmount {
            get {
                return this.balanceAmountField;
            }
            set {
                if ((object.ReferenceEquals(this.balanceAmountField, value) != true)) {
                    this.balanceAmountField = value;
                    this.RaisePropertyChanged("balanceAmount");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string cardHolderName {
            get {
                return this.cardHolderNameField;
            }
            set {
                if ((object.ReferenceEquals(this.cardHolderNameField, value) != true)) {
                    this.cardHolderNameField = value;
                    this.RaisePropertyChanged("cardHolderName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string clientName {
            get {
                return this.clientNameField;
            }
            set {
                if ((object.ReferenceEquals(this.clientNameField, value) != true)) {
                    this.clientNameField = value;
                    this.RaisePropertyChanged("clientName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string confirmNumber {
            get {
                return this.confirmNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.confirmNumberField, value) != true)) {
                    this.confirmNumberField = value;
                    this.RaisePropertyChanged("confirmNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string detResponseCode {
            get {
                return this.detResponseCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.detResponseCodeField, value) != true)) {
                    this.detResponseCodeField = value;
                    this.RaisePropertyChanged("detResponseCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string detResponseCodeDescription {
            get {
                return this.detResponseCodeDescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.detResponseCodeDescriptionField, value) != true)) {
                    this.detResponseCodeDescriptionField = value;
                    this.RaisePropertyChanged("detResponseCodeDescription");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string fraudScore {
            get {
                return this.fraudScoreField;
            }
            set {
                if ((object.ReferenceEquals(this.fraudScoreField, value) != true)) {
                    this.fraudScoreField = value;
                    this.RaisePropertyChanged("fraudScore");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string giftMessage {
            get {
                return this.giftMessageField;
            }
            set {
                if ((object.ReferenceEquals(this.giftMessageField, value) != true)) {
                    this.giftMessageField = value;
                    this.RaisePropertyChanged("giftMessage");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string hostDate {
            get {
                return this.hostDateField;
            }
            set {
                if ((object.ReferenceEquals(this.hostDateField, value) != true)) {
                    this.hostDateField = value;
                    this.RaisePropertyChanged("hostDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string hostTime {
            get {
                return this.hostTimeField;
            }
            set {
                if ((object.ReferenceEquals(this.hostTimeField, value) != true)) {
                    this.hostTimeField = value;
                    this.RaisePropertyChanged("hostTime");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string iccHostTags {
            get {
                return this.iccHostTagsField;
            }
            set {
                if ((object.ReferenceEquals(this.iccHostTagsField, value) != true)) {
                    this.iccHostTagsField = value;
                    this.RaisePropertyChanged("iccHostTags");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryAmount1 {
            get {
                return this.inquiryAmount1Field;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryAmount1Field, value) != true)) {
                    this.inquiryAmount1Field = value;
                    this.RaisePropertyChanged("inquiryAmount1");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryAmount2 {
            get {
                return this.inquiryAmount2Field;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryAmount2Field, value) != true)) {
                    this.inquiryAmount2Field = value;
                    this.RaisePropertyChanged("inquiryAmount2");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryAmount3 {
            get {
                return this.inquiryAmount3Field;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryAmount3Field, value) != true)) {
                    this.inquiryAmount3Field = value;
                    this.RaisePropertyChanged("inquiryAmount3");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryAmount4 {
            get {
                return this.inquiryAmount4Field;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryAmount4Field, value) != true)) {
                    this.inquiryAmount4Field = value;
                    this.RaisePropertyChanged("inquiryAmount4");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryAmount5 {
            get {
                return this.inquiryAmount5Field;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryAmount5Field, value) != true)) {
                    this.inquiryAmount5Field = value;
                    this.RaisePropertyChanged("inquiryAmount5");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryAmount6 {
            get {
                return this.inquiryAmount6Field;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryAmount6Field, value) != true)) {
                    this.inquiryAmount6Field = value;
                    this.RaisePropertyChanged("inquiryAmount6");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryAmount7 {
            get {
                return this.inquiryAmount7Field;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryAmount7Field, value) != true)) {
                    this.inquiryAmount7Field = value;
                    this.RaisePropertyChanged("inquiryAmount7");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryAmount8 {
            get {
                return this.inquiryAmount8Field;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryAmount8Field, value) != true)) {
                    this.inquiryAmount8Field = value;
                    this.RaisePropertyChanged("inquiryAmount8");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryCashPayDate {
            get {
                return this.inquiryCashPayDateField;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryCashPayDateField, value) != true)) {
                    this.inquiryCashPayDateField = value;
                    this.RaisePropertyChanged("inquiryCashPayDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryCurrency1 {
            get {
                return this.inquiryCurrency1Field;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryCurrency1Field, value) != true)) {
                    this.inquiryCurrency1Field = value;
                    this.RaisePropertyChanged("inquiryCurrency1");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryCurrency2 {
            get {
                return this.inquiryCurrency2Field;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryCurrency2Field, value) != true)) {
                    this.inquiryCurrency2Field = value;
                    this.RaisePropertyChanged("inquiryCurrency2");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryMinPayDate {
            get {
                return this.inquiryMinPayDateField;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryMinPayDateField, value) != true)) {
                    this.inquiryMinPayDateField = value;
                    this.RaisePropertyChanged("inquiryMinPayDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string inquiryType {
            get {
                return this.inquiryTypeField;
            }
            set {
                if ((object.ReferenceEquals(this.inquiryTypeField, value) != true)) {
                    this.inquiryTypeField = value;
                    this.RaisePropertyChanged("inquiryType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string invoice {
            get {
                return this.invoiceField;
            }
            set {
                if ((object.ReferenceEquals(this.invoiceField, value) != true)) {
                    this.invoiceField = value;
                    this.RaisePropertyChanged("invoice");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string referenceNumber {
            get {
                return this.referenceNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.referenceNumberField, value) != true)) {
                    this.referenceNumberField = value;
                    this.RaisePropertyChanged("referenceNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string refundsAmount {
            get {
                return this.refundsAmountField;
            }
            set {
                if ((object.ReferenceEquals(this.refundsAmountField, value) != true)) {
                    this.refundsAmountField = value;
                    this.RaisePropertyChanged("refundsAmount");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string refundsTransactions {
            get {
                return this.refundsTransactionsField;
            }
            set {
                if ((object.ReferenceEquals(this.refundsTransactionsField, value) != true)) {
                    this.refundsTransactionsField = value;
                    this.RaisePropertyChanged("refundsTransactions");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string responseCode {
            get {
                return this.responseCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.responseCodeField, value) != true)) {
                    this.responseCodeField = value;
                    this.RaisePropertyChanged("responseCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string responseCodeDescription {
            get {
                return this.responseCodeDescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.responseCodeDescriptionField, value) != true)) {
                    this.responseCodeDescriptionField = value;
                    this.RaisePropertyChanged("responseCodeDescription");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string salesAmount {
            get {
                return this.salesAmountField;
            }
            set {
                if ((object.ReferenceEquals(this.salesAmountField, value) != true)) {
                    this.salesAmountField = value;
                    this.RaisePropertyChanged("salesAmount");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string salesTax {
            get {
                return this.salesTaxField;
            }
            set {
                if ((object.ReferenceEquals(this.salesTaxField, value) != true)) {
                    this.salesTaxField = value;
                    this.RaisePropertyChanged("salesTax");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string salesTaxDiscount {
            get {
                return this.salesTaxDiscountField;
            }
            set {
                if ((object.ReferenceEquals(this.salesTaxDiscountField, value) != true)) {
                    this.salesTaxDiscountField = value;
                    this.RaisePropertyChanged("salesTaxDiscount");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string salesTip {
            get {
                return this.salesTipField;
            }
            set {
                if ((object.ReferenceEquals(this.salesTipField, value) != true)) {
                    this.salesTipField = value;
                    this.RaisePropertyChanged("salesTip");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string salesTransactions {
            get {
                return this.salesTransactionsField;
            }
            set {
                if ((object.ReferenceEquals(this.salesTransactionsField, value) != true)) {
                    this.salesTransactionsField = value;
                    this.RaisePropertyChanged("salesTransactions");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string securityCodeValidation {
            get {
                return this.securityCodeValidationField;
            }
            set {
                if ((object.ReferenceEquals(this.securityCodeValidationField, value) != true)) {
                    this.securityCodeValidationField = value;
                    this.RaisePropertyChanged("securityCodeValidation");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string systemTraceNumber {
            get {
                return this.systemTraceNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.systemTraceNumberField, value) != true)) {
                    this.systemTraceNumberField = value;
                    this.RaisePropertyChanged("systemTraceNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string taxDiscount {
            get {
                return this.taxDiscountField;
            }
            set {
                if ((object.ReferenceEquals(this.taxDiscountField, value) != true)) {
                    this.taxDiscountField = value;
                    this.RaisePropertyChanged("taxDiscount");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string transactionId {
            get {
                return this.transactionIdField;
            }
            set {
                if ((object.ReferenceEquals(this.transactionIdField, value) != true)) {
                    this.transactionIdField = value;
                    this.RaisePropertyChanged("transactionId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string zipCodeValidation {
            get {
                return this.zipCodeValidationField;
            }
            set {
                if ((object.ReferenceEquals(this.zipCodeValidationField, value) != true)) {
                    this.zipCodeValidationField = value;
                    this.RaisePropertyChanged("zipCodeValidation");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="DummyVPOS.IService")]
    public interface IService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/executeTransaction", ReplyAction="http://tempuri.org/IService/executeTransactionResponse")]
        ECOsystem.DummyVPOS.clsWSResponseData executeTransaction();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/executeTransaction", ReplyAction="http://tempuri.org/IService/executeTransactionResponse")]
        System.Threading.Tasks.Task<ECOsystem.DummyVPOS.clsWSResponseData> executeTransactionAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceChannel : ECOsystem.DummyVPOS.IService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceClient : System.ServiceModel.ClientBase<ECOsystem.DummyVPOS.IService>, ECOsystem.DummyVPOS.IService {
        
        public ServiceClient() {
        }
        
        public ServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public ECOsystem.DummyVPOS.clsWSResponseData executeTransaction() {
            return base.Channel.executeTransaction();
        }
        
        public System.Threading.Tasks.Task<ECOsystem.DummyVPOS.clsWSResponseData> executeTransactionAsync() {
            return base.Channel.executeTransactionAsync();
        }
    }
}
