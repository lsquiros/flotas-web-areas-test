﻿using System;
using System.Web;
using System.Data;
using ECOsystem.Utilities;
using Microsoft.Reporting.WebForms;


namespace ECOsystem.ReportsData
{
    /// <summary>
    /// Class which is going to populate the reports rdlc with the data
    /// </summary>
    public partial class reports : System.Web.UI.Page
    {
        /// <summary>
        /// Load the information for the reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack != true)
                ShowReport();
        }

        /// <summary>
        /// Show Reports
        /// </summary>
        private void ShowReport()
        {
            var datos = (DataTable)Session["datosReporte"];
            var reportName = Convert.ToString(Session["nombreReporte"]);
            var path = HttpContext.Current.Server.MapPath("~/ReportsData/");
            var dataSetName = "";

            ReportViewer1.Reset();
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            var r = ReportViewer1.LocalReport;
            r.ReportPath = path + reportName + ".rdlc";

            DataTable dtReports = new DeniedTransactionsReport.DeniedTransactionsReportDataTable();
            if (reportName == "DeniedTransactionsReport")
            {
                dtReports = new DeniedTransactionsReport.DeniedTransactionsReportDataTable();
                dataSetName = "DeniedTransactionsReport";
            }

            if (reportName == "ReversedTransactionsReport")
            {
                dtReports = new TransactionsReport.TransactionsReportDataTable();
                dataSetName = "TransactionsReport";
            }

            if (reportName == "TransactionsReport")
            {
                dtReports = new TransactionsReport.TransactionsReportDataTable();
                dataSetName = "TransactionsReport";
            }

            if (reportName == "TransactionDetailReport")
            {
                dtReports = new TransactionDetailReport.TransactionDetailReportDataTable();
                dataSetName = "TransactionDetailReport";
            }

            if (reportName == "TransactionDetailReportGV")
            {
                dtReports = new TransactionDetailReport.TransactionDetailReportDataTable();
                dataSetName = "TransactionDetailReport";
            }
            if (reportName == "TransactionsSAPReport")
            {
                dtReports = new TransactionsSAPReport.TransactionsSAPReportDataTable();
                dataSetName = "TransactionsSAPReport";
            }
            if (reportName == "TransactionsSAPReport")
            {
                dtReports = new TransactionsSAPReport.TransactionsSAPReportDataTable();
                dataSetName = "TransactionsSAPReport";
            }
            if (reportName == "CreditCardsReportByClient")
            {
                dtReports = new dsCreditCardReportByClient.CreditCardReportByClientDataTable();
                dataSetName = "dsCreditCardReportByClient";
            }

            if (reportName == "ApprovalTransactionsReport")
            {
                dtReports = new ApprovalTransactionsReport.ApprovalTransactionsDataTable();
                dataSetName = "ApprovalTransactionsReport";
            }

            var drReports = dtReports.NewRow();
            dtReports.Rows.Add(drReports);
            var rds = new ReportDataSource
            {
                Name = reportName,
                Value = datos
            };
            r.DataSources.Add(rds);

            var cus = Utilities.Session.GetCustomerInfo();
            string username;

            if (cus != null)
                username = cus.DecryptedName;
            else
            {
                var part = Utilities.Session.GetPartnerInfo();
                username = part.Name;
            }

            string logopath = null, site = null, url = null; 

            switch (Utilities.Session.GetProductInfo())
            {
                case ProductInfo.IntrackECO:
//Intrack
                    logopath = "file:///" + HttpContext.Current.Server.MapPath("~/Content/Images/logo.png");
                    site = "www.intrack-eco.com";
                    url = "https://www.intrack-eco.com/ECOSystem/";
                    break;
                case ProductInfo.LineVitaECO:
//LineVita
                    logopath = "file:///" + HttpContext.Current.Server.MapPath("~/Content/Images/LineVita/logo.png");
                    site = "www.line-vita.com";
                    url = "http://www.line-vita.com/";
                    break;
                case ProductInfo.BACFlota:
                    logopath = "file:///" + HttpContext.Current.Server.MapPath("~/Content/Images/BAC/logo.gif");
                    site = "www.bacflota.com";
                    url = "https://www.bacflota.com/";
                    break;
                case ProductInfo.Services:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }


            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(dataSetName, datos));
            ReportViewer1.LocalReport.EnableHyperlinks = true;
            ReportViewer1.LocalReport.EnableExternalImages = true;
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("UserName", username, false));
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("URL", url, false));
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("Site", site, false));
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("PictureLogo", logopath, true));
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.DataBind();

            #region Excel Render

            Warning[] warnings;
            string[] streamIds;
            string mimeType;
            string encoding;
            string extension;

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = path + reportName + ".rdlc";

            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;


            var nameDownload = string.Empty;
            if (reportName == "DeniedTransactionsReport") nameDownload = "ReporteTransaccionesDenegadas";
            if (reportName == "ReversedTransactionsReport") nameDownload = "ReporteTransaccionesReversadas";
            if (reportName == "TransactionsReport") nameDownload = "ReporteTransaccionesProcesadas";
            if (reportName == "TransactionsSAPReport") nameDownload = "ReporteTransaccionesConsolidadasSAP";
            if (reportName == "ApprovalTransactionsReport") nameDownload = "ReporteConciliacionDeTransacciones";
            if (reportName == "TransactionDetailReport")
            {
                var startDate = Session["StartDate"].ToString();
                var endDate = Session["EndDate"].ToString();
                nameDownload = "Reporte Transaccional Detallado " + cus.DecryptedName + startDate + " - " + endDate;
                Session["StartDate"] = null;
                Session["EndDate"] = null;
            }

            if (reportName == "TransactionDetailReportGV")
            {
                var startDate = Session["StartDate"].ToString();
                var endDate = Session["EndDate"].ToString();
                nameDownload = "Reporte Transaccional Detallado " + cus.DecryptedName + startDate + " - " + endDate;
                Session["StartDate"] = null;
                Session["EndDate"] = null;
            }
            if (reportName == "CreditCardsReportByClient")
            {
                nameDownload = "Reporte de Tarjetas";
            }


            Response.AddHeader("content-disposition", "attachment; filename=" + nameDownload.Replace(" ", "_").Replace(",", "_") + "." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();

            #endregion
        }
    }
}
