﻿using System;
using System.Configuration;
using System.Web;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;

namespace ECOsystem.Utilities
{
    public class GetAccountUtility
    {

        public void GetUserInformation(ref HttpContextBase context) //, int? userId)
        {
            HttpCookie aCookie = context.Request.Cookies["ECOCFG"];            

            if (aCookie != null)
            {
                aCookie.Expires = DateTime.Now.AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["ECOCFG_TIME"]));                
                string text = aCookie.Value;
                TokenByUserModel userActive = AccountManagement.TokenUserRetrieve(text);

                if (!context.Request.IsAuthenticated)
                {
                    //if the customer has info it will log with the customer information
                    if (userActive != null && userActive.SerializeUserInfo != null)
                    {
                        context.Session.Add("LOGGED_USER_INFORMATION", userActive.User);
                    }                   
                }
                else
                {
                    //if the customer has info it will log with the customer information
                    if (userActive != null && userActive.SerializeUserInfo != null)
                    {
                        context.Session.Add("LOGGED_USER_INFORMATION", userActive.User);
                    }                    
                }                           
            }          

        }

    }
}