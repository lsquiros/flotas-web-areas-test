﻿/************************************************************************************************************
*  File    : TransactionsReportUtility.cs
*  Summary : Sort the data for the reports which are going to use rdlc files
*  Author  : Henry Retana
*  Date    : 11/03/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ECOsystem.Utilities.Helpers;
using System.Data;
//using ECOsystem.Models.Control;
using ECOsystem.Business.Administration;
//using ECOsystem.Business.Control;
using ECOsystem.Models.Settings;
using ECOsystem.Models.Reports;
using ECOsystem.Business.Report;


namespace ECOsystem.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public class TransactionsReportUtility : IDisposable
    {
        /// <summary>
        /// Get the data table ready for the Denied Transactions Report by Customer
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable DeniedTransactionsReport(IEnumerable<TransactionDenyReportModel> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("CreditCardNumber");
                dt.Columns.Add("ResponseCode");
                dt.Columns.Add("ResponseCodeDescription");
                dt.Columns.Add("Message");
                dt.Columns.Add("CreditCardHolder");
                dt.Columns.Add("InsertDate");
                dt.Columns.Add("TerminalId");

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["CreditCardNumber"] = item.CreditCardNumber;
                    dr["ResponseCode"] = item.ResponseCode;
                    dr["ResponseCodeDescription"] = item.ResponseCodeDescription;
                    dr["Message"] = item.Message;
                    dr["CreditCardHolder"] = item.CreditCardHolder;
                    dr["InsertDate"] = item.InsertDate;
                    dr["TerminalId"] = item.TerminalId;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Get the datatable ready for the Transactions Report by Customer
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable TransactionsReportData(IEnumerable<TransactionsReport> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Invoice");
                dt.Columns.Add("MerchantDescription");
                dt.Columns.Add("SystemTraceCode");
                dt.Columns.Add("FuelName");
                dt.Columns.Add("State");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("HolderNameStr");
                dt.Columns.Add("DateStr");
                dt.Columns.Add("CapacityUnitValueStr");
                dt.Columns.Add("FuelAmountStr");
                dt.Columns.Add("OdometerStr");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("CreditCardNumber");

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["CustomerName"] = item.CustomerName;
                    dr["Invoice"] = item.Invoice;
                    dr["MerchantDescription"] = item.MerchantDescription;
                    dr["SystemTraceCode"] = item.SystemTraceCode;
                    dr["FuelName"] = item.FuelName;
                    dr["State"] = item.State;
                    dr["CostCenterName"] = item.CostCenterName;
                    dr["TerminalId"] = item.TerminalId;
                    dr["HolderNameStr"] = item.HolderNameStr;
                    dr["DateStr"] = item.DateToExport;
                    dr["CapacityUnitValueStr"] = item.CapacityUnitValueStr;
                    dr["FuelAmountStr"] = item.FuelAmountStr;
                    dr["OdometerStr"] = item.OdometerStr;
                    dr["VehicleName"] = item.VehicleName;
		            dr["CreditCardNumber"] = item.CreditCardNumberWithMask;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Get the information for the reports by Partner
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable TransactionsReportDataPartner(IEnumerable<TransactionsReportDeniedByPartner> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Invoice");
                dt.Columns.Add("MerchantDescription");
                dt.Columns.Add("SystemTraceCode");
                dt.Columns.Add("FuelName");
                dt.Columns.Add("State");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("HolderNameStr");
                dt.Columns.Add("DateStr");
                dt.Columns.Add("CapacityUnitValueStr");
                dt.Columns.Add("FuelAmountStr");
                dt.Columns.Add("OdometerStr");
                dt.Columns.Add("CreditCardNumber");

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["CustomerName"] = item.CustomerName;
                    dr["Invoice"] = item.Invoice;
                    dr["MerchantDescription"] = item.MerchantDescription;
                    dr["SystemTraceCode"] = item.SystemTraceCode;
                    dr["FuelName"] = item.FuelName;
                    dr["State"] = item.State;
                    dr["CostCenterName"] = item.CostCenterName;
                    dr["TerminalId"] = item.TerminalId;
                    dr["HolderNameStr"] = item.HolderNameStr;
                    dr["DateStr"] = item.DateStr;
                    dr["CapacityUnitValueStr"] = item.CapacityUnitValueStr;
                    dr["FuelAmountStr"] = item.FuelAmountStr;
                    dr["OdometerStr"] = item.OdometerStr;
                    dr["CreditCardNumber"] = item.CreditCardNumber;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        internal DataTable ApprovalTransactionsReport(List<Models.Reports.ApprovalTransactionDisplayModel> list)
        {
            IList<ApprovalTransactionTypeModel> list_types;
            
            using (var business = new ApprovalTransactionBusiness())
            {
                list_types = business.GetTypes();
            }   
      

            using (var dt = new DataTable())
            {
                dt.Columns.Add("Date");
                dt.Columns.Add("Status");
                dt.Columns.Add("VoucherId");
                dt.Columns.Add("CostCenter");
                dt.Columns.Add("Vehicle");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("CreditCard");
                dt.Columns.Add("Identification");
                dt.Columns.Add("DriverName");
                dt.Columns.Add("Fuel");
                dt.Columns.Add("Amount");
                dt.Columns.Add("Type");
                dt.Columns.Add("ServiceStation");
                dt.Columns.Add("Terminal");

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["Date"] = item.Date.ToString("dd/MM/yyyy hh:mm:ss"); //(DateTime)String.Format("dd/MM/yyyy hh:mm:ss",item.Date);
                    dr["Status"] = list_types.Where(m => m.Id == item.Status).FirstOrDefault().Name; 
                    dr["VoucherId"] = item.InvoiceId;
                    dr["CostCenter"] = item.CostCenter;
                    dr["Vehicle"] = item.Vehicle;
                    dr["PlateId"] = item.PlateId;
                    dr["CreditCard"] = item.CreditCard;
                    dr["Identification"] = item.DriverId;
                    dr["DriverName"] = item.DecryptDriverName;
                    dr["Fuel"] = item.Fuel;
                    dr["Amount"] = item.Amount;
                    dr["Type"] = item.Type;
                    dr["ServiceStation"] = item.ServiceStation;
                    dr["Terminal"] = item.AffiliateNumber;                

                    dt.Rows.Add(dr);
                }

                return dt;
            }


        }
    }
}
