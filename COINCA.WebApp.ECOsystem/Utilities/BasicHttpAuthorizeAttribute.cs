﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using ECOsystem.Audit;
using ECOsystem.Business.Utilities;
using AuthorizeAttribute = System.Web.Mvc.AuthorizeAttribute;

namespace ECOsystem.Utilities
{
    /// <summary>
    /// Basic Http Authorize Attribute
    /// </summary>
    public class BasicHttpAuthorizeAttribute : AuthorizeAttribute
    {
        private bool _requireSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["APIBasicHttpAuthorizationRequireSSL"]);

        /// <summary>
        /// Require SSL
        /// </summary>
        public bool RequireSsl
        {
            get { return _requireSsl; }
            set { _requireSsl = value; }
        }


        private bool _requireAuthentication = true;

        /// <summary>
        /// Require Authentication
        /// </summary>
        public bool RequireAuthentication
        {
            get { return _requireAuthentication; }
            set { _requireAuthentication = value; }
        }


        /// <summary>
        /// On Authorization
        /// </summary>
        /// <param name="actionContext">actionContext</param>
        public virtual void OnAuthorization(HttpActionContext actionContext)
        {
            try
            {
                //actionContext.Request
                if (Authenticate() || !RequireAuthentication)
                    return;
                HandleUnauthorizedRequest(actionContext);

            }
            catch (Exception e)
            {
                //Add log event
                AddLogEvent(
                        message: string.Empty,
                        method: "Authenticate",
                        exception: e,
                        isError: true);

                throw;
            }
        }

        /// <summary>
        /// Handle Unauthorized Request
        /// </summary>
        /// <param name="actionContext">action Context</param>
        protected virtual void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
            {
                Content = new StringContent("401 Unauthorized")
            };
            throw new HttpResponseException(response);
        }

        /// <summary>
        /// Authenticate
        /// </summary>
        /// <returns>bool if is Authenticated</returns>
        private bool Authenticate() //HttpRequestMessage input)
        {
            if (RequireSsl && !HttpContext.Current.Request.IsSecureConnection && !HttpContext.Current.Request.IsLocal)
            {
                //log.Error("Failed to login: SSL:" + HttpContext.Current.Request.IsSecureConnection);
                var response = new HttpResponseMessage(HttpStatusCode.Forbidden)
                {
                    Content = new StringContent("(SSL) Secure Connection is required")
                };
                throw new HttpResponseException(response);
            }

            if (!HttpContext.Current.Request.Headers.AllKeys.Contains("Authorization")) return false;

            var authHeader = HttpContext.Current.Request.Headers["Authorization"];

            IPrincipal principal;
            
            if (!TryGetPrincipal(authHeader, out principal)) return false;

            HttpContext.Current.User = principal;
            return true;
        }


        /// <summary>
        /// Try Get Principal
        /// </summary>
        /// <param name="authHeader">authHeader</param>
        /// <param name="principal">principal</param>
        /// <returns>bool</returns>
        private bool TryGetPrincipal(string authHeader, out IPrincipal principal)
        {
            var creds = ParseAuthHeader(authHeader);
            if (creds != null)
            {
                if (TryGetPrincipal(creds[0], creds[1], out principal)) return true;
            }

            principal = null;
            return false;
        }


        /// <summary>
        /// Parse AuthHeader
        /// </summary>
        /// <param name="authHeader">authHeader</param>
        /// <returns>string[]</returns>
        private static string[] ParseAuthHeader(string authHeader)
        {
            // Check this is a Basic Auth header 
            if (string.IsNullOrEmpty(authHeader) || !authHeader.StartsWith("Basic")) return null;

            // Pull out the Credentials with are seperated by ':' and Base64 encoded 
            var base64Credentials = authHeader.Substring(6);
            var credentials = Encoding.ASCII.GetString(Convert.FromBase64String(base64Credentials)).Split(':');

            if (credentials.Length != 2 || string.IsNullOrEmpty(credentials[0]) || string.IsNullOrEmpty(credentials[0])) return null;

            // Okay this is the credentials 
            return credentials;
        }

        /// <summary>
        /// Try Get Principal
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <param name="principal">principal</param>
        /// <returns>bool when Try to Get Principal</returns>
        private bool TryGetPrincipal(string username, string password, out IPrincipal principal)
        {
            // this is the method that does the authentication

            //users often add a copy/paste space at the end of the username
            username = username.Trim();
            password = password.Trim();

            bool returnResult;

            if (AccountManagement.ApiLogin(username, password))
            {
                // once the user is verified, assign it to an IPrincipal with the identity name and applicable roles
                principal = new GenericPrincipal(new GenericIdentity(username), null);
                returnResult = true;
            }
            else if (AccountManagement.ApiAppLogin(username, password))
            {
                // once the user is verified, assign it to an IPrincipal with the identity name and applicable roles
                principal = new GenericPrincipal(new GenericIdentity(username), null);
                returnResult = true;
            }
            else
            {
                principal = null;
                returnResult = false;

                //Add log event
                AddLogEvent(
                        message: string.Format("Auth_Warning: las credenciales suministradas Usr:[{0}] y Pw:[{1}] para acceder al servicio NO son válidas.", username, password),
                        method: "Authenticate",
                        exception: null,
                        isWarning:true);
            }

            return returnResult;
        }


        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="message"></param>
        /// <param name="method"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, string method, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = exception != null ? string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace) : message;

            EventLogBusiness.AddEventLog(new EventLog
            {
                UserId = 0,
                CustomerId = 0,
                PartnerId = 0,
                Controller = "BasicHttpAuthorizeAttribute",
                Action = method,
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}