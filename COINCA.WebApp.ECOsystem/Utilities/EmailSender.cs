﻿/************************************************************************************************************
*  File    : EmailSender.cs
*  Summary : Email Sender
*  Author  : Manuel Azofeifa
*  Date    : 11/24/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using ECOsystem.Business.Core;
using Microsoft.AspNet.Identity;

namespace ECOsystem.Utilities
{
    /// <summary>
    /// EmailSender
    /// </summary>
    public class EmailSender : IDisposable
    {
        /// <summary>
        /// Get email body with template format
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="partnerId">Partner Id</param>
        /// <param name="templateId">Template Id</param>
        /// <param name="fields">List of field to replace</param>
        /// <returns>HTML formated email body</returns>
        public static string GenerateEmailBody(int? customerId, int? partnerId, int templateId, params string[] fields)
        {
            try
            {
                var templates = EmailTemplateBusiness.Retrieve(customerId, partnerId, templateId);
                var template = templates.FirstOrDefault();
                var html = template.HTML.Replace("{}", template.DscProductTypes);
                var body = String.Format(html, fields);
                return body;
            }
            catch (Exception e)
            {
                // TODO: Exception
                return null;
            }
        }

        /// <summary>
        /// Send Email with template format
        /// </summary>
        /// <param name="to">Receptor Email</param>
        /// <param name="subject">Email Subject</param>
        /// <param name="templateId">Template Id</param>
        /// <param name="fields">List of field to replace</param>
        /// <returns>Status of sender</returns>
        public static bool SendEmail(string to, string subject, int templateId, params string[] fields)
        {
            var customerId = Session.GetCustomerId();
            var partnerId = Session.GetPartnerId();
            var body = GenerateEmailBody(customerId, partnerId, templateId, fields);
            var sendResult = body == null ? false : new EmailSender().SendEmail(to, subject, body);
            return sendResult;
        }

        /// <summary>
        /// Send Email to confirm or reset the User Acount (Password)
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<bool> SendEmailAsync(IdentityMessage message)
        {
            MailMessage mail = new MailMessage();

            SmtpClient smtpServer = new SmtpClient(ConfigurationManager.AppSettings["SMTPSERVER"]);
            smtpServer.UseDefaultCredentials = false;
            smtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUSERACCOUNT"], ConfigurationManager.AppSettings["SMTPPASSWORDACCOUNT"]);
            smtpServer.Port = 587; // Gmail works on this port
            smtpServer.EnableSsl = true;

            mail.From = new MailAddress(ConfigurationManager.AppSettings["SMTPUSERACCOUNT"]);
            string mailDecrypted = TripleDesEncryption.Decrypt(message.Destination);
            mail.To.Add(mailDecrypted);
            mail.Subject = message.Subject;
            mail.Body = message.Body;

            mail.IsBodyHtml = true;
            smtpServer.EnableSsl = true;

            try
            {
                await smtpServer.SendMailAsync(mail);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// SendEmail
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public bool SendEmail(string to, string subject, string body)
        {
            MailMessage mail = new MailMessage();

            SmtpClient smtpServer = new SmtpClient(ConfigurationManager.AppSettings["SMTPSERVER"]);
            smtpServer.UseDefaultCredentials = false;
            smtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUSERACCOUNT"], ConfigurationManager.AppSettings["SMTPPASSWORDACCOUNT"]);
            smtpServer.Port = 587; // Gmail works on this port
            smtpServer.EnableSsl = true;

            mail.From = new MailAddress(ConfigurationManager.AppSettings["SMTPUSERACCOUNT"]);
            mail.To.Add(to);
            mail.Subject = subject;
            mail.Body = body;

            mail.IsBodyHtml = true;
            smtpServer.EnableSsl = true;

            try
            {
                smtpServer.Send(mail);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}