﻿using ModuleConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ECOsystem.Utilities
{
    public class ReportsUtilities : IDisposable
    {
        /// <summary>
        /// GetReport
        /// </summary>
        /// <param name="datos"></param>
        /// <param name="reportName"></param>
        /// <param name="nameSpace"></param>
        /// <param name="downloadName"></param>
        /// <returns></returns>
        public ActionResult GetReportDataTable(string datos, string reportName, string nameSpace, string downloadName)
        {
            var odoType = HttpContext.Current.Session["OdometerType"] == null ? string.Empty : "|" + HttpContext.Current.Session["OdometerType"];
            var username = Session.GetUserInfo().DecryptedName ?? Session.GetPartnerInfo().Name;

            var Reports = new COINCA.WebAPI.ECOSystemReports.Controllers.ReportsController();
            var respMessage  =   Reports.ReportDataTable(datos.Replace("|", "") + "|" + reportName + "|" + username + "|" + Session.GetProductInfo() + "|" + nameSpace + odoType).Content.ReadAsByteArrayAsync().Result;

            return new FileStreamResult(new MemoryStream(respMessage), "application/ms-excel") { FileDownloadName = downloadName + ".xls" };

            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(ModuleURI.GetURI(ModuleTypeURI.ModuleReports));
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //    var response = client.PostAsJsonAsync("reports/reportdatatable/", datos.Replace("|", "") + "|" + reportName + "|" + username + "|" + Session.GetProductInfo() + "|" + nameSpace + odoType).Result;
            //    if (!response.IsSuccessStatusCode) throw new Exception(response.Content.ReadAsStringAsync().Result);
            //    var respMessage = response.Content.ReadAsByteArrayAsync().Result;
            //    return new FileStreamResult(new MemoryStream(respMessage), "application/ms-excel") { FileDownloadName = downloadName + ".xls" };
            //}
        }

        /// <summary>
        /// GetReport
        /// </summary>
        /// <param name="datos"></param>
        /// <param name="reportName"></param>
        /// <param name="nameSpace"></param>
        /// <param name="downloadName"></param>
        /// <returns></returns>
        public ActionResult GetReportDataSet(string datos, string reportName, string nameSpace, string downloadName)
        {
            var username = Session.GetUserInfo().DecryptedName ?? Session.GetPartnerInfo().Name;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ModuleURI.GetURI(ModuleTypeURI.ModuleReports));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.PostAsJsonAsync("reports/reportdataset/", datos.Replace("|", "") + "|" + reportName + "|" + username + "|" + Session.GetProductInfo().ToString() + "|" + nameSpace).Result;
                if (!response.IsSuccessStatusCode) throw new Exception(response.Content.ReadAsStringAsync().Result);
                var respMessage = response.Content.ReadAsByteArrayAsync().Result;
                return new FileStreamResult(new MemoryStream(respMessage), "application/ms-excel") { FileDownloadName = downloadName + ".xls" };
            }

        }
        
        /// <summary>
        /// GC
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}