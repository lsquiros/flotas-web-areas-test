﻿/************************************************************************************************************
*  File    : GlobalReportUtility.cs
*  Summary : Sort the data for the global reports which are going to use rdlc files
*  Author  : Gerald Solano
*  Date    : 08/04/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using ECOsystem.Models.Core;

namespace ECOsystem.Utilities
{
    /// <summary>
    /// Sort the data for the global reports which are going to use rdlc files
    /// </summary>
    public class GlobalReportUtility : IDisposable
    {

        /// <summary>
        /// Get the data table ready for the Credit Card Report By Client
        /// </summary>
        /// <param name="list"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public DataTable GetCCReportByClientDataTable(IEnumerable<CreditCardsReportByClient> list, DateTime startDate, DateTime endDate)
        {
            var Partner = Session.GetPartnerInfo().Name;
            using (var dt = new DataTable())
            {
                dt.TableName = "CreditCardReportByClient";
                dt.Columns.Add("ClientName");
                dt.Columns.Add("AccountNumber");
                dt.Columns.Add("StatusName");
                dt.Columns.Add("CreateDate");
                dt.Columns.Add("Active");
                dt.Columns.Add("CostAdmin");
                dt.Columns.Add("CostEmision");
                dt.Columns.Add("TotalCantCards");
                dt.Columns.Add("CurrentCantCards");
                dt.Columns.Add("TotalCostAdminCards");
                dt.Columns.Add("TotalCostEmisionCards");
                dt.Columns.Add("CantActiveCards");
                dt.Columns.Add("CantBlockCards");
                dt.Columns.Add("CantCloseCards");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("CantCardReposition");
                dt.Columns.Add("AmountCardReposition");
                dt.Columns.Add("Partner");
                dt.Columns.Add("LegalDocument");
                dt.Columns.Add("SAPCode");
                dt.Columns.Add("ActiveRanges");

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));

                //Get array letters
                char[] letters = startDateStr.ToCharArray();

                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);

                // return the array made of the new char array
                startDateStr = new string(letters);

                letters = endDateStr.ToCharArray();

                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);

                // return the array made of the new char array
                endDateStr = new string(letters);

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    string formatCostAdmin = Miscellaneous.ConvertCurrencyWithCultureStr("es-CR", item.CostAdmin.ToString(), false, true);
                    string formatCostEmision = Miscellaneous.ConvertCurrencyWithCultureStr("es-CR", item.CostEmision.ToString(), false, true);

                    decimal totalCostAdmin = item.TotalCantCards * item.CostAdmin;
                    decimal totalCostEmision = (item.CurrentCantCards * item.CostEmision);

                    string formatTotalCostAdmin = Miscellaneous.ConvertCurrencyWithCultureStr("es-CR", totalCostAdmin.ToString(), false, true);
                    string formatTotalCostEmision = Miscellaneous.ConvertCurrencyWithCultureStr("es-CR", totalCostEmision.ToString(), false, true);

                    dr["ClientName"] = item.DecrypedClientName;
                    dr["AccountNumber"] = item.RestrictAccountNumber;
                    dr["StatusName"] = item.StatusName;
                    dr["CreateDate"] = item.CreateDateStr;
                    dr["Active"] = ((item.Active)? "✔" : "-");
                    dr["CostAdmin"] = formatCostAdmin; // item.CostAdmin.ToString();
                    dr["CostEmision"] = formatCostEmision; // item.CostEmision.ToString();
                    dr["TotalCantCards"] = item.TotalCantCards;
                    dr["CurrentCantCards"] = item.CurrentCantCards;
                    dr["TotalCostAdminCards"] = formatTotalCostAdmin; //totalCostAdmin.ToString();
                    dr["TotalCostEmisionCards"] = formatTotalCostEmision; //totalCostEmision.ToString();
                    dr["CantActiveCards"] = item.CantActiveCards;
                    dr["CantBlockCards"] = item.CantBlockCards;
                    dr["CantCloseCards"] = item.CantCloseCards;
                    dr["InitRangeDate"] = startDateStr;
                    dr["FiniRangeDate"] = endDateStr;
                    dr["CantCardReposition"] = item.CantCardReposition;
                    dr["AmountCardReposition"] = item.AmountCardReposition;
                    dr["Partner"] = Partner;
                    dr["LegalDocument"] = item.LegalDocument;
                    dr["SAPCode"] = item.SAPCode;
                    dr["ActiveRanges"] = (item.RangePermit == 1) ? "✔" : "-";

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Get the data table ready for the Credit Card Report Detail By Client
        /// </summary>
        /// <param name="list"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public DataTable GetCCReportDetailByClientDataTable(IEnumerable<CreditCardsReportDetail> list, DateTime startDate, DateTime endDate)
        {
            var Partner = Session.GetPartnerInfo().Name;
            using (var dt = new DataTable())
            {
             
                dt.TableName = "CreditCardReportByClientDetailed";
                dt.Columns.Add("CreditCardNumber");
                dt.Columns.Add("Holder");
                dt.Columns.Add("StatusName");
                dt.Columns.Add("RequestDate");
                dt.Columns.Add("DeliveryDate");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("Partner");

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));

                //Get array letters
                char[] letters = startDateStr.ToCharArray();

                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);

                // return the array made of the new char array
                startDateStr = new string(letters);

                letters = endDateStr.ToCharArray();

                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);

                // return the array made of the new char array
                endDateStr = new string(letters);

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["CreditCardNumber"] = item.RestrictCreditCardNumber;
                    dr["Holder"] = item.DecrypedHolder;
                    dr["StatusName"] = item.StatusName;
                    dr["RequestDate"] = item.RequestDateStr;
                    dr["DeliveryDate"] = item.DeliveryDateStr;
                    dr["InitRangeDate"] = startDateStr;
                    dr["FiniRangeDate"] = endDateStr;
                    dr["Partner"] = Partner;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

    }
}
