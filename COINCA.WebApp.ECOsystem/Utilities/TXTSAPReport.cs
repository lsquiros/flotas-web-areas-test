﻿/************************************************************************************************************
*  File    : TXTSAPReport.cs
*  Summary : TXT Report Methods
*  Author  : Stefano Quiros
*  Date    : 11/03/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ECOsystem.Utilities.Helpers;
using System.Data;
using ECOsystem.Models.Control;
using ECOsystem.Business.Administration;
//using ECOsystem.Business.Control;

namespace ECOsystem.Utilities
{
    /// <summary>
    /// Performs operation to create TXTs files methods
    /// </summary>
    public class TXTSAPReport : IDisposable
    {       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="modelList">modelList</param>
        public byte[] CreateTxtFormat(IEnumerable<TransactionsReport> modelList)
        {
            try
            {
                int LinesNum = 0;
                decimal AmountCount = 0;
                byte[] binaryData = null;

               using (var ms = new MemoryStream())
               {                
                    var sw = new StreamWriter(ms);
                    foreach (var item in modelList)
                    {
                        AmountCount = AmountCount + item.FuelAmount;

                        sw.WriteLine(FormatFile(item.CreditCardNumberDescrypt, "c") + FormatFile(item.SystemTraceCode, "an") + FormatFile(item.Date.ToString(), "hr") +
                             GetMerchantDescription(item.MerchantDescription) + FormatFile(item.OdometerStr, "od") + FormatFile(item.Liters.ToString(), "fl") + 
                             FormatFile(item.FuelAmount.ToString(), "cl") + FormatFile(item.TransportData, "cdc") + FormatFile(item.Invoice, "fc") + 
                             FormatFile(item.MCC.ToString(), "mcc"));                        
                        sw.Flush();
                    }

                    binaryData = ms.ToArray();

                    var fms = new MemoryStream(binaryData);
                    var r = new StreamReader(fms);
                    string line;

                   
                    while ((line = r.ReadLine()) != null)
                    {
                        LinesNum++;
                    }

                    var newms = new MemoryStream();
                    var newfile = new StreamWriter(newms);
                    fms = new MemoryStream(binaryData);
                    r = new StreamReader(fms);

                    newfile.WriteLine(FormatFile(LinesNum.ToString(), "nl"));
                    newfile.WriteLine(AmountCount.ToString("0.00").PadLeft(13, '0'));
                    while ((line = r.ReadLine()) != null)
                    {
                        newfile.WriteLine(line);
                    }

                    newfile.Flush();

                    return newms.ToArray();                    
                }
            }
            catch (Exception)
            {
                throw new SystemException("System Exception when server tries to generate excel file");
            }
        }


        /// <summary>
        /// Format to the download file
        /// </summary>
        /// <param name="wfile"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private string FormatFile(string wfile, string key)
        {
            try
            {
               
                string rt = "";

                switch (key)
                {
                    case "c":
                        rt = wfile.Replace("-", string.Empty);
                        return rt;                          

                    case "an" :  
                        string tn = wfile.ToString().PadLeft(6, '0');
                        rt = tn.ToString().PadLeft(15, ' ');
                        return rt;
                        
                    case "hr" :
                        DateTime dt = Convert.ToDateTime(wfile);
                        rt = dt.ToString("dd/MM/yyyy HH:mm:ss");
                        return rt; 

                    case "od" :
                        wfile = wfile.ToString().PadLeft(8, '0');
                        rt = wfile.ToString().PadLeft(10, ' ');
                        return rt;

                    case "fl":
                        wfile = wfile.ToString().PadLeft(7, '0');
                        rt = wfile.ToString().PadLeft(12, ' ');
                        return rt;

                    case "cl":
                        rt = wfile.Replace(',', '.');
                        rt = rt.ToString().PadLeft(12, ' ');
                        return rt;

                    case "cdc":

                        if (wfile == null || wfile == string.Empty)
                        {
                            rt = "";
                            rt = rt.ToString().PadLeft(10, '0');
                            return rt;
                        }

                        wfile = wfile.Replace("\"", string.Empty).Trim();                        
                        string[] cartag = wfile.Split(',');
                        rt = (from s in cartag where s.Contains("carTag") select s).Single();
                        
                        string[] t = rt.Split(':');
                        
                        rt = t[1].Trim();

                        using (var cod = new VehiclesByDriverBusiness())
                        {
                            rt = cod.RetrieveDriverIdByDriverCode(Utilities.TripleDesEncryption.Encrypt(rt), Convert.ToInt32(Session.GetCustomerId()));
                            rt = Utilities.TripleDesEncryption.Decrypt(rt);

                            rt = rt.PadLeft(10, '0');
                            return rt;
                        }
                      

                    case "fc":

                        if (wfile.Length > 8)
                            wfile = wfile.Remove(0, 2);

                        rt = wfile.ToString().PadLeft(8, '0');
                        return rt;

                    case "mcc":
                        rt = wfile.ToString().PadLeft(4, '0');
                        return rt;

                    case "nl" :
                        if (wfile.Length == 1)
                            return rt = "0" + wfile;
                        else
                            return wfile;
                }
                
                return "";
            }
            catch (Exception)
            {               
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tlog"></param>
        /// <param name="merchant"></param>
        /// <returns></returns>
        private string GetMerchantDescription(string merchant)
        {
            string rt = "";            
           
            rt = merchant.ToString().PadRight(50, ' ');
            return rt;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }



    }
}