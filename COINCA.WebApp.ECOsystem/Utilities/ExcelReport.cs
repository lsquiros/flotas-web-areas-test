﻿/************************************************************************************************************
*  File    : ExcelReport.cs
*  Summary : Excel Report Methods
*  Author  : Berman Romero
*  Date    : 10/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Validation;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities.Helpers;

namespace ECOsystem.Utilities
{
#pragma warning disable 1591
    /// <summary>
    /// Performs operation to create Excel files methods
    /// </summary>
    public class ExcelReport : IDisposable
    {
        private readonly string[] _columnNames = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };
        private string _mtitle;    

        public bool HasCustomRows = false;
        public uint IndexHeaderCustomRow = 7;
        public uint IndexBodyCustomRow = 8;

        /// <summary>
        /// Create Spread sheet Workbook
        /// </summary>
        /// <param name="title"></param>
        /// <param name="modelList">modelList</param>
        public byte[] CreateSpreadsheetWorkbook<T>(string title, IList<T> modelList)
        {
            _mtitle = title;
            try
            {
                using (var ms = new MemoryStream())
                {
                    var spreadsheetDocument = SpreadsheetDocument.Create(ms, SpreadsheetDocumentType.Workbook); // Create a spreadsheet document by supplying the filepath By default, AutoSave = true, Editable = true, and Type = xlsx.

                    var workbookpart = spreadsheetDocument.AddWorkbookPart(); // Add a WorkbookPart to the document.
                    AddStyleSheet(ref workbookpart);

                    var worksheetPart = InsertWorksheet(workbookpart); // Add a WorksheetPart to the WorkbookPart.

                    if (title != null)
                    {
                        InsertValue("A", 1, title, worksheetPart);
                    }
                    InsertDataValues(modelList, worksheetPart);

                    workbookpart.Workbook.Save();

                    // Validate Sheet
                    IsSheetValid(spreadsheetDocument);

                    spreadsheetDocument.Close(); // Close the document.
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                // Add log exception
                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                throw new SystemException("System Exception when server tries to generate excel file");
            }
        }

        /// <summary>
        /// Read xlsx file
        /// </summary>
        /// <param name="path">File path</param>
        /// <returns>DataTable</returns>
        public DataTable ReadExcel(string path)
        {
            DataTable table = new DataTable();
            using (FileStream fs = File.OpenRead(path))
            {
                using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false))
                {
                    WorkbookPart workbookPart = doc.WorkbookPart;

                    SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                    SharedStringTable sst = sstpart.SharedStringTable;

                    WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                    Worksheet sheet = worksheetPart.Worksheet;

                    var cells = sheet.Descendants<Cell>();
                    var rows = sheet.Descendants<Row>();

                    // Or... via each row
                    for (int r = 0; r < rows.Count(); r++)
                    {
                        Row row = rows.ElementAt<Row>(r);
                        DataRow newRow = table.NewRow();

                        for (int c = 0; c < row.Elements<Cell>().Count(); c++)
                        {
                            Cell column = row.Elements<Cell>().ElementAt<Cell>(c);
                            string columnValue = null;

                            if (column.DataType == "s")
                            {
                                int ssid = int.Parse(column.CellValue.Text);
                                columnValue = sst.ChildElements[ssid].InnerText;
                            }
                            else if (column.CellValue != null)
                            {
                                columnValue = column.CellValue.Text;
                            }

                            //Set DataTable columns using first row
                            if (r == 0)
                            {
                                DataColumn newColumn = new DataColumn();
                                newColumn.ColumnName = columnValue;
                                table.Columns.Add(newColumn);
                            }
                            else
                            {
                                var index = Array.IndexOf(_columnNames, column.CellReference.Value.ToCharArray()[0].ToString());
                                newRow[index] = columnValue;
                            }
                        }

                        //First row represent the columns
                        if (r > 0)
                        {
                            table.Rows.Add(newRow);
                        }
                    }
                }
            }
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            return table;
        }

        /// <summary>
        /// Given a WorkbookPart, inserts a new worksheet.
        /// </summary>
        /// <param name="workbookPart">workbookPart</param>
        /// <returns>WorksheetPart</returns>
        private WorksheetPart InsertWorksheet(WorkbookPart workbookPart)
        {
            workbookPart.Workbook = new Workbook();
            // Add a new worksheet part to the workbook.
            var newWorksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());

            // Add Sheets to the Workbook.
            var sheets = workbookPart.Workbook.AppendChild(new Sheets());

            // Append a new worksheet and associate it with the workbook.
            var sheet = new Sheet
            {
                Id = workbookPart.GetIdOfPart(newWorksheetPart),
                SheetId = 1,
                Name = "Reporte"
            };
            sheets.Append(sheet);

            if (_mtitle == null || !_mtitle.Equals("Hábitos de Conducción")) return newWorksheetPart;
            var table = new DataTable();
            table.Columns.Add("Lista Acrónimos", typeof(string));
            table.Columns.Add("_", typeof(string));
            table.Columns.Add("-", typeof(string));

            table.Rows.Add("ΣDT= Sumatoria Distancia Total", "VEC= Velocidad en Exceso", "NEC= Cantidad de Excesos de Velocidad");
            table.Rows.Add("ΣDEC= Sumatoria Distancia Total Exceso de Velocidad", "IRC= Porcentaje de Distancia en Exceso de Velocidad", "x̄VC= Cantidad de Excesos de Velocidad");
            table.Rows.Add("S= Desviación Estandar", "CS= Calificación");

            ExportDataTable(table, workbookPart);

            return newWorksheetPart;
        }


        public void ExportDataTable(DataTable table, WorkbookPart workbookPart)
        {
            var workbook = workbookPart;
            //create a reference to Sheet1 
            var worksheet = workbook.WorksheetParts.Last();
            var data = worksheet.Worksheet.GetFirstChild<SheetData>();

            //add column names to the first row 
            var header = new Row {RowIndex = (uint) 1};

            foreach (DataColumn column in table.Columns)
            {
                var headerCell = CreateTextCell(table.Columns.IndexOf(column) + 1, 1, column.ColumnName);

                header.AppendChild(headerCell);
            }
            data.AppendChild(header);

            //loop through each data row  
            for (var i = 0; i < table.Rows.Count; i++)
            {
                var contentRow = table.Rows[i];
                data.AppendChild(CreateContentRow(contentRow, i + 2));
            }
        }

        private Cell CreateTextCell(
            int columnIndex,
            int rowIndex,
            object cellValue)
        {
            var cell = new Cell
            {
                DataType = CellValues.InlineString,
                CellReference = getColumnName(columnIndex) + rowIndex
            };


            var inlineString = new InlineString();
            var t = new Text {Text = cellValue.ToString()};

            inlineString.AppendChild(t);
            cell.AppendChild(inlineString);

            return cell;
        }
        private string getColumnName(int columnIndex)
        {
            var dividend = columnIndex;
            var columnName = string.Empty;

            while (dividend > 0)
            {
                var modifier = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modifier) + columnName;
                dividend = (dividend - modifier) / 26;
            }

            return columnName;
        }


        private Row CreateContentRow(
            DataRow dataRow,
            int rowIndex)
        {
            var row = new Row
            {
                RowIndex = (uint)rowIndex
            };

            for (var i = 0; i < dataRow.Table.Columns.Count; i++)
            {
                var dataCell = CreateTextCell(i + 1, rowIndex, dataRow[i]);
                row.AppendChild(dataCell);
            }
            return row;
        }


        /// <summary>
        /// Given a column name, a row index, and a WorksheetPart, inserts a cell into the worksheet. 
        /// If the cell already exists, returns it. 
        /// </summary>
        /// <param name="columnName">columnName</param>
        /// <param name="rowIndex">rowIndex</param>
        /// <param name="worksheetPart">worksheetPart</param>
        /// <returns>Cell</returns>
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            var worksheet = worksheetPart.Worksheet;
            var sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Count(r => r.RowIndex == rowIndex) != 0)
            {
                row = sheetData.Elements<Row>().First(r => r.RowIndex == rowIndex);
            }
            else
            {
                row = new Row { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Count(c => c.CellReference.Value == columnName + rowIndex) > 0)
            {
                return row.Elements<Cell>().First(c => c.CellReference.Value == cellReference);
            }
            // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
            var refCell = row.Elements<Cell>().FirstOrDefault(cell => String.Compare(cell.CellReference.Value, cellReference, StringComparison.OrdinalIgnoreCase) > 0);
            var newCell = new Cell { CellReference = cellReference };
            row.InsertBefore(newCell, refCell);
            return newCell;
        }

        /// <summary>
        /// Insert Value into worksheet
        /// </summary>
        /// <param name="columnName">columnName</param>
        /// <param name="rowIndex">rowIndex</param>
        /// <param name="value">value</param>
        /// <param name="worksheetPart">worksheetPart</param>
        private void InsertValue(string columnName, uint rowIndex, string value, WorksheetPart worksheetPart)
        {
            var cell = InsertCellInWorksheet(columnName, rowIndex, worksheetPart);

            cell.CellValue = new CellValue(value);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        /// <summary>
        /// Insert Data Values into worksheet
        /// </summary>
        /// <param name="modelList">modelList</param>
        /// <param name="worksheetPart">worksheetPart</param>
        private void InsertDataValues<T>(IList<T> modelList, WorksheetPart worksheetPart)
        {
            if (modelList == null) return;
            
            CellValues[] cellTypes = GetCellTypes(modelList.FirstOrDefault());

            #region Header Cells

            InsertHeaderValues(modelList.FirstOrDefault(), worksheetPart);

            #endregion

            #region Body Cells

            uint rowIndex = ((HasCustomRows) ? IndexBodyCustomRow : 3);

            foreach (var model in modelList)
            {
                var colIndex = 0;
                foreach (var property in model.GetType().GetProperties())
                {
                    if (property.GetCustomAttributes(typeof(ExcelNoMappedColumnAttribute), true).Length != 0) continue;
                    if (colIndex >= cellTypes.Count()) continue;
                    var cell = InsertCellInWorksheet(_columnNames[colIndex], rowIndex, worksheetPart);
                    var pValue = property.GetValue(model, null);
                    var cellValue = string.Empty;

                    if(pValue != null){
                        cellValue = pValue.ToString();
                    }

                    //Values have been set like string value for prevent corruption damage with Microsoft Excel
                    cell.CellValue = new CellValue(cellValue);
                    cell.DataType = new EnumValue<CellValues>(CellValues.String);

                    colIndex++;
                }
                rowIndex++;
            }

            #endregion

         
        }


        /// <summary>
        /// Insert Header Values
        /// </summary>
        /// <param name="model">model</param>
        /// <param name="worksheetPart">worksheetPart</param>
        private void InsertHeaderValues(dynamic model, WorksheetPart worksheetPart)
        {
            if (model == null) return;

            var result = new List<string>();
            foreach (PropertyInfo property in model.GetType().GetProperties())
            {
                if (property.GetCustomAttributes(typeof(ExcelNoMappedColumnAttribute), true).Length != 0) continue;
                var displayName = "";
                if (property.GetCustomAttributes(typeof(ExcelMappedColumnAttribute), true).Length > 0)
                {
                    displayName = (property.GetCustomAttributes(typeof(ExcelMappedColumnAttribute), true)[0] as ExcelMappedColumnAttribute).DisplayName;
                }
                else if (property.GetCustomAttributes(typeof(DisplayNameAttribute), true).Length > 0)
                {
                    displayName = (property.GetCustomAttributes(typeof(DisplayNameAttribute), true)[0] as DisplayNameAttribute).DisplayName;
                }
                else
                {
                    displayName = property.Name;
                }

                result.Add(displayName);
            }

            for (var i = 0; i < result.Count; ++i)
            {
               var rowIndex = ((HasCustomRows) ? IndexHeaderCustomRow : 2);
               InsertValue(_columnNames[i], rowIndex, result[i], worksheetPart);
            }
        }

        /// <summary>
        /// Get Cell Types
        /// </summary>
        /// <param name="model">model</param>
        /// <returns>List of CellValues</returns>
        private static CellValues[] GetCellTypes(dynamic model)
        {
            var result = new List<CellValues>();
            if (model == null) return result.ToArray();

            foreach (PropertyInfo property in model.GetType().GetProperties())
            {
                if (property.GetCustomAttributes(typeof(ExcelNoMappedColumnAttribute), true).Length == 0)
                {
                    var temp = CellValues.SharedString;
                    object value = property.GetValue(model, null);
                    if (value != null)
                    {
                        switch (value.GetType().ToString())
                        {
                            case "System.Byte":
                                temp = CellValues.String;
                                break;
                            case "System.Int16":
                                temp = CellValues.Number;
                                break;
                            case "System.Int32":
                                temp = CellValues.Number;
                                break;
                            case "System.Int64":
                                temp = CellValues.Number;
                                break;
                            case "System.Decimal":
                                temp = CellValues.Number;
                                break;
                            case "System.Double":
                                temp = CellValues.Number;
                                break;
                            case "System.Single":
                                temp = CellValues.Number;
                                break;
                            case "System.Guid":
                                temp = CellValues.String;
                                break;
                            case "System.Boolean":
                                temp = CellValues.Boolean;
                                break;
                            case "System.DateTime":
                                temp = CellValues.Date;
                                break;
                            case "System.TimeSpan":
                                temp = CellValues.Date;
                                break;
                            case "System.String":
                                temp = CellValues.String;
                                break;
                            case "System.Byte[]":
                                temp = CellValues.String;
                                break;

                            default:
                                throw new NotImplementedException("The type: " + value.GetType() + " has not been implemented for excel file");
                        }
                    }
                    result.Add(temp);
                }
            }

            return result.ToArray();
        }

        /// <summary>
        /// Add Wrapping Cell Style
        /// </summary>
        /// <param name="spreadSheet"></param>
        /// <returns></returns>
        public int InsertWrappingCellStyle(SpreadsheetDocument spreadSheet)
        {
            Stylesheet styleSheet = spreadSheet.WorkbookPart.WorkbookStylesPart.Stylesheet;
            styleSheet.CellFormats.AppendChild(
                new CellFormat(
                    new Alignment { WrapText = new BooleanValue(true) }
                ) { ApplyAlignment = new BooleanValue(true) }
            );
            styleSheet.CellFormats.Count++;
            spreadSheet.WorkbookPart.WorkbookStylesPart.Stylesheet.Save();

            return (int)styleSheet.CellFormats.Count.Value - 1;
        }

        /// <summary>
        /// Add StyleSheet to Work Book Part
        /// </summary>
        private static void AddStyleSheet(ref WorkbookPart workbookPart)
        {
            var stylesheet = workbookPart.AddNewPart<WorkbookStylesPart>();
            var workbookstylesheet = new Stylesheet();

            // <Fonts>
            var font0 = new Font();         // Default font

            var fonts = new Fonts();      // <APENDING Fonts>
            fonts.Append(font0);

            // <Fills>
            var fill0 = new Fill();        // Default fill

            var fills = new Fills();      // <APENDING Fills>
            fills.Append(fill0);

            // <Borders>
            var border0 = new Border();     // Defualt border

            var borders = new Borders();    // <APENDING Borders>
            borders.Append(border0);

            // <CellFormats>
            var cellformat0 = new CellFormat { FontId = 0, FillId = 0, BorderId = 0 }; // Default style : Mandatory

            var cellformat1 = new CellFormat(new Alignment { WrapText = true });  // Style with textwrap set

            // <APENDING CellFormats>
            var cellformats = new CellFormats();
            cellformats.Append(cellformat0);
            cellformats.Append(cellformat1);


            // Append FONTS, FILLS , BORDERS & CellFormats to stylesheet <Preserve the ORDER>
            workbookstylesheet.Append(fonts);
            workbookstylesheet.Append(fills);
            workbookstylesheet.Append(borders);
            workbookstylesheet.Append(cellformats);

            // Finalize
            stylesheet.Stylesheet = workbookstylesheet;
            stylesheet.Stylesheet.Save();
        }

        /// <summary>
        /// Validate Sheet
        /// </summary>
        /// <param name="mySheet"></param>
        /// <returns></returns>
        private static void IsSheetValid(SpreadsheetDocument mySheet)
        {
            var validator = new OpenXmlValidator();
            var errors = validator.Validate(mySheet).ToList();
            var errorsStr = errors.Aggregate(string.Empty, (current, error) => current + "Id: {error.Id} | Description: {error.Description} | Path: {error.Path.XPath} //");

            if (!errors.Any())
                return;
            throw new Exception(errorsStr);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}