﻿/************************************************************************************************************
*  File    : Utilities.cs
*  Summary : Utilities has common Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
*  Modify  : Gerald Solano
*  Mod.Date: 03/04/2016
*  Descrip : Add string to Guid validation
*  
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;

using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECOsystem.Business.Administration;
using ECOsystem.Business.Core;
using ECOsystem.Business.Home;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using SharpRaven;
using SharpRaven.Data;
using System.Configuration;

namespace ECOsystem.Utilities
{
    /// <summary>
    /// Performs operations like Set() or Get of T() with Session
    /// </summary>
    public static class Session
    {
        /// <summary>
        /// Get an element of T form Session
        /// </summary>
        /// <typeparam name="T">Model or class to return</typeparam>
        /// <param name="key">Key of the element stored in Session</param>
        /// <returns>T</returns>
        public static T Get<T>(string key)
        {
            var obj = HttpContext.Current.Session[key];
            if (obj != null)
                return (T)obj;
            return Activator.CreateInstance<T>();
        }

        /// <summary>
        /// Set an element of T into Session
        /// </summary>
        /// <param name="key">Key of the element to be stored in Session</param>
        /// <param name="value">The element to be stored in Session</param>
        public static void Set(string key, object value)
        {
            if (value == null)
                HttpContext.Current.Session.Remove(key);
            else
                HttpContext.Current.Session[key] = value;
        }

        /// <summary>
        /// Get the Application version
        /// </summary>
        /// <returns></returns>
        public static string GetVersionInfo()
        {
            try
            {
                using (var business = new HomeBusiness())
                {
                    var version = business.RetrieveAppVersion().FirstOrDefault();
                    HttpContext.Current.Session.Add("APPLICATION_VERSION", version.Version);

                    return version.Version;
                }
            }
            catch (Exception ex)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex, true);

                return "0.0.0";
            }
        }


        /// <summary>
        /// Get User Information from Session
        /// </summary>
        /// <returns>Users object with all properties</returns>
        public static Users GetUserInfo()
        {
            var obj = HttpContext.Current.Session["LOGGED_USER_INFORMATION"];
            if (obj != null)
                return (Users)obj;

            var context = new RequestContext(new HttpContextWrapper(HttpContext.Current), new RouteData());
            var urlHelper = new UrlHelper(context);
            var url = urlHelper.Action("Login", "Account");
            if (url == null) return new Users();

            //Set IsAuthenticated to false...
            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            if (!HttpContext.Current.Response.IsRequestBeingRedirected)
            {
                HttpContext.Current.Response.Redirect(url);
            }
            return new Users();
        }

        /// <summary>
        /// Get User Information from Session
        /// </summary>
        /// <returns>Users object with all properties</returns>
        public static IEnumerable<ServiceStationsUser> GetServiceStattionsInfo()
        {
            var user = GetUserInfo();
            if (!user.IsServiceStationUser) return null;
            if (user.ServiceStationUser != null) return user.ServiceStationUser;
            using (var business = new HomeBusiness())
            {
                user.ServiceStationUser = business.RetrieveServiceStationUserInfo((int)user.UserId);
                HttpContext.Current.Session.Add("LOGGED_USER_INFORMATION", user);
                return user.ServiceStationUser;
            }
        }

        /// <summary>
        /// Get Customer Information from Session
        /// </summary>
        /// <returns>Customers object with all properties</returns>
        public static Customers GetCustomerInfo(int? first_time = null)
        {
            var user = GetUserInfo();
            if (!user.IsCustomerUser && !user.IsDriverUser) return null;
            if (first_time != null)
            {
                using (var business = new CustomersBusiness())
                {
                    var customer = business.RetrieveCustomers(GetCustomerId(), GetCountryId()).FirstOrDefault();
                    HttpContext.Current.Session.Add("CUSTOMER_INFORMATION_FROM_LOGGED_USER", customer);
                    return customer;
                }
            }
            var obj = HttpContext.Current.Session["CUSTOMER_INFORMATION_FROM_LOGGED_USER"];
            if (obj != null)
                return (Customers)obj;

            using (var business = new CustomersBusiness())
            {
                var customer = business.RetrieveCustomers(GetCustomerId(), GetCountryId()).FirstOrDefault();
                HttpContext.Current.Session.Add("CUSTOMER_INFORMATION_FROM_LOGGED_USER", customer);
                return customer;
            }
        }

        /// <summary>
        /// Get Partner Information from Session
        /// </summary>
        /// <returns>Partner object with all properties</returns>
        public static Partners GetPartnerInfo()
        {
            //var user = GetUserInfo();
            //if (!user.IsPartnerUser || !user.IsCoincaUser) return null;
            var obj = HttpContext.Current.Session["PARTNER_INFORMATION_FROM_LOGGED_USER"];
            if (obj != null)
                return (Partners)obj;

            using (var business = new GeneralCollections())
            {
                var partner = business.RetrievePartners(GetPartnerId()).FirstOrDefault();
                HttpContext.Current.Session.Add("PARTNER_INFORMATION_FROM_LOGGED_USER", partner);
                return partner;
            }
        }

        /// <summary>
        /// Get Customer Id from User in Session
        /// </summary>
        /// <returns>int CustomerId</returns>
        public static int? GetCustomerId()
        {
            return GetUserInfo().CustomerId;
        }

        /// <summary>
        /// Get Customers Id from User in Session
        /// </summary>
        /// <returns>int CustomerId</returns>
        public static List<int?> GetCustomersIdByPartner()
        {
            var user = GetUserInfo();

            if (user.IsPartnerUser)
            {
                using (var business = new CustomersBusiness())
                {
                    if (HttpContext.Current.Session["CUSTOMERS_ID_LIST"] == null)
                    {
                        HttpContext.Current.Session["CUSTOMERS_ID_LIST"] = business.RetrieveCustomersByPartners(GetPartnerId()).Select(l => l.CustomerId).ToList();
                        return (List<int?>)HttpContext.Current.Session["CUSTOMERS_ID_LIST"];
                    }
                    return (List<int?>)HttpContext.Current.Session["CUSTOMERS_ID_LIST"];
                }
            }
            throw new Exception();
        }

        /// <summary>
        /// Get Customer Capacity Unit Id from Customer in Session
        /// </summary>
        /// <returns>int UnitOfCapacityId</returns>
        public static int GetCustomerCapacityUnitId()
        {
            int unitOfCapacityId = 0;
            Customers customer = GetCustomerInfo();
            if (customer != null && customer.UnitOfCapacityId != null)
            {
                unitOfCapacityId = Convert.ToInt16(customer.UnitOfCapacityId);
            }
            return unitOfCapacityId;
        }

        /// <summary>
        /// Get Customer Capacity Unit Name from Customer in Session
        /// </summary>
        /// <returns>string UnitOfCapacityName</returns>
        public static string GetCustomerCapacityUnitName()
        {
            string capacityUnitName = "Litros";
            Customers customer = GetCustomerInfo();
            if (customer != null)
            {
                capacityUnitName = customer.UnitOfCapacityName;
            }
            return capacityUnitName;
        }

        /// <summary>
        /// Get Customer Capacity Unit Singular Name from Customer in Session
        /// </summary>
        /// <returns>string UnitOfCapacityName</returns>
        public static string GetCustomerCapacityUnitSingularName()
        {
            string capacityUnitName = GetCustomerCapacityUnitName();
            switch (capacityUnitName)
            {
                case "Litros":
                    return "Litro";
                case "Galones":
                    return "Galón";
                default:
                    return "Litro";
            }
        }

        /// <summary>
        /// Get Partner Capacity Unit Id from Partner in Session
        /// </summary>
        /// <returns>int capacityUnitId</returns>
        public static int GetPartnerCapacityUnitId()
        {
            int capacityUnitId = 0;
            Partners partner = GetPartnerInfo();
            if (partner != null)
            {
                capacityUnitId = partner.CapacityUnitId ?? 0;
            }
            return capacityUnitId;
        }

        /// <summary>
        /// Get Partner Capacity Unit Name from Partner in Session
        /// </summary>
        /// <returns>string CapacityUnitName</returns>
        public static string GetPartnerCapacityUnitName()
        {
            string capacityUnitName = "Litros";
            Partners partner = GetPartnerInfo();
            if (partner != null)
            {
                capacityUnitName = partner.CapacityUnitName;
            }
            return capacityUnitName;
        }

        /// <summary>
        /// Get Partner Id from User in Session
        /// </summary>
        /// <returns>int PartnerId</returns>
        public static int? GetPartnerId()
        {
            var user = GetUserInfo();
            if (user.IsPartnerUser)
                return user.PartnerId;
            if (user.CustomerId != null)
            {
                using (var business = new GeneralCollections())
                {
                    return business.RetrievePartnerByCustomerId(user.CustomerId);
                }
            }
            return null; // When isn't  Partner User
        }

        /// <summary>
        /// Get Country Id from user in Session - ONLY when logged user IsCustomerUser
        /// </summary>
        /// <returns>int CountryId</returns>
        public static int? GetCountryId()
        {
            return GetUserInfo().CountryId;
        }

        /// <summary>
        /// Get the custom name geopolitical level according to country of origin
        /// </summary>
        /// <returns>Geopolitical name</returns>
        public static List<string> GetGeopoliticalNames()
        {
            var countryId = GetCountryId();
            var geopoliticalNames = new List<string>();
            if (countryId != null)
            {
                var business = new CountriesBusiness();
                var countries = business.RetrieveCountries(countryId).ToList();
                if (countries.Any())
                {
                    var country = countries.FirstOrDefault();
                    geopoliticalNames.Add(country.GeopoliticalLevel1);
                    geopoliticalNames.Add(country.GeopoliticalLevel2);
                    geopoliticalNames.Add(country.GeopoliticalLevel3);
                }
            }
            return geopoliticalNames;
        }

        /// <summary>
        /// Validates whether the current user can create the customer
        /// </summary>
        /// <returns></returns>
        public static bool UserCanCreateCustomer()
        {
            var currentUser = GetUserInfo();
            //var partner = GetPartnerInfo();  partner.CountryId
            var countryId = GetCountryId();
            if (currentUser.PartnerUser != null)
            {
                if (currentUser.PartnerUser.CountriesAccessList.Any(w => w.CountryId == countryId && w.IsCountryChecked))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Get User Profile image
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string GetUserProfileImage(int? userId)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    string imageInfo = business.RetrieveProfileImage(userId: userId);

                    return imageInfo;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userType"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetPartnerCustomerLogo(string userType, string id)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    int profileId = 0;
                    Int32.TryParse(id, out profileId);

                    string imageInfo = business.RetrieveProfileImage(profileId: profileId, profile: userType);

                    return imageInfo;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// RoleName
        /// </summary>
        /// <param name="RoleName"></param>
        /// <returns></returns>
        public static string GetProfileByRole(string RoleId)
        {
            if (RoleId == null)
            {
                var user = GetUserInfo();
                RoleId = user.RoleId;
            }

            using (var business = new UserRolesPermissionsBusiness())
            {
                return business.RetrieveMasterRoleByRole(RoleId);
            }
        }

        /// <summary>
        /// Valida si el usuario tiene acceso a el dashboard de control
        /// </summary>
        /// <returns></returns>
        public static bool DashboardControlAcces()
        {
            var user = GetUserInfo();

            List<AccountMenus> Menus = new List<AccountMenus>();

            Menus = ASPMenusBusiness.RetrieveMenus(3, null, user.RoleId).ToList();

            List<GetDataChecked> DinamicFilters = new List<GetDataChecked>();

            using (var bus = new DinamicFilterBusiness())
            {
                DinamicFilters = bus.TreeViewCheckedRetrieve("VH", Convert.ToInt32(user.UserId)).ToList();
            }

            int count = 0;

            foreach (var item in Menus)
            {
                if (item.Name == "Por Combustible")
                    count++;
                if (item.Name == "Por Vehículos")
                    count++;
            }

            //Validates if the User has filters, otherwise user wont be able to see the dashboard
            if (DinamicFilters.Any())
                count = 0;

            if (count == 2)
                return true;
            return false;
        }

        /// <summary>
        /// If the customer has access shows the reports otherwise it will not
        /// </summary>
        /// <returns></returns>
        public static bool DashboardControlAccess()
        {
            var user = GetUserInfo();

            List<AccountMenus> Menus = new List<AccountMenus>();

            Menus = ASPMenusBusiness.RetrieveMenus(3, null, user.RoleId).ToList();

            if (Menus.Count > 1)
                return true;
            return false;
        }

        /// <summary>
        /// If the customer has access shows the reports otherwise it will not
        /// </summary>
        /// <returns></returns>
        public static bool DashboardOperacionAccess()
        {
            var user = GetUserInfo();

            List<AccountMenus> Menus = new List<AccountMenus>();

            Menus = ASPMenusBusiness.RetrieveMenus(4, null, user.RoleId).ToList();

            if (Menus.Count > 1)
                return true;
            return false;
        }

        /// <summary>
        /// If the customer has access shows the info otherwise it will not
        /// </summary>
        /// <returns></returns>
        public static bool DashboardAdminAccess()
        {
            var user = GetUserInfo();

            List<AccountMenus> Menus = new List<AccountMenus>();

            Menus = ASPMenusBusiness.RetrieveMenus(8, null, user.RoleId).ToList();

            if (Menus.Count > 1)
                return true;
            return false;
        }


        /// <summary>
        /// Get product info type
        /// </summary>
        /// <returns></returns>
        public static ProductInfo GetProductInfo()
        {

            var p = ConfigurationManager.AppSettings["TEMPLATE_TYPE"];

            switch (p)
            {
                case "0":
                    return ProductInfo.Services;
                case "1":
                    return ProductInfo.IntrackECO;
                case "2":
                    return ProductInfo.BACFlota;
                case "3":
                    return ProductInfo.LineVitaECO;
                default:
                    break;
            }

            return ProductInfo.IntrackECO;
        }

        /// <summary>
        /// Get product info type by Partner
        /// </summary>
        /// <returns></returns>
        public static ProductInfo GetProductInfoByPartner(int partnerId)
        {
            using (var business = new GeneralCollections())
            {
                return business.RetrieveProductTypeByPartner(partnerId);
            }
        }

        /// <summary>
        /// Capture Exceptions to Sentry
        /// </summary>
        /// <param name="e"></param>
        public static void SentrySupport(Exception e)
        {
            var ravenClient = new RavenClient(ConfigurationManager.AppSettings["SentryDSN"]);
            ravenClient.Environment = ConfigurationManager.AppSettings["SentryEnv"];

            var user = GetUserInfo();
            var customer = GetCustomerInfo();
            var partner = GetPartnerInfo();

            e.Data.Add("UserId", user.UserId);
            e.Data.Add("UserEmail", user.DecryptedEmail);
            e.Data.Add("UserName", user.DecryptedName);
            e.Data.Add("CustomerId", customer != null ? customer.CustomerId : null);
            e.Data.Add("CustomerName", customer != null ? customer.DecryptedName : null);
            e.Data.Add("PartnerId", partner != null ? partner.PartnerId : null);
            e.Data.Add("PartnerName", partner != null ? partner.Name : null);

            ravenClient.Capture(new SentryEvent(e));
        }
    }

    /// <summary>
    /// Performs operations like Set() or Get of T() with Session
    /// </summary>
    public static class Miscellaneous
    {
        /// <summary>
        /// Get Display CreditCard Mask
        /// </summary>
        /// <param name="value">Value to apply mask</param>
        /// <returns>The display format with mask</returns>
        public static string GetDisplayCreditCardMask(string value)
        {
            var mask = string.Empty.ToArray(); 

            if(value.Count() == 19)
                mask = ConfigurationManager.AppSettings["DisplayCreditCardMask"].ToCharArray();
            else
                mask = ConfigurationManager.AppSettings["DisplayCreditCardMaskAmex"].ToCharArray();

            var result = value.ToCharArray();

            if (mask.Length == result.Length)
            {
                for (var i = 0; i < result.Length; ++i)
                {
                    if (mask[i] == '*')
                        result[i] = '*';
                }
                return new string(result);
            }
            return GetDefaultMask(value);
        }

        /// <summary>
        /// Convert CreditCard To Internal Format
        /// </summary>
        /// <param name="value">Value to apply mask</param>
        /// <returns>The display format with mask</returns>
        public static string ConvertCreditCardToInternalFormat(long value)
        {
            return String.Format("{0:0000-0000-0000-0000}", value);
        }

        /// <summary>
        /// Get Display Account Number Mask
        /// </summary>
        /// <param name="value">Value to apply mask</param>
        /// <returns>The display format with mask</returns>
        public static string GetDisplayAccountNumberMask(string value)
        {
            return GetDefaultMask(value);
        }

        /// <summary>
        /// Get Default Mask
        /// </summary>
        /// <param name="value">Value to apply mask</param>
        /// <returns>The display format with mask</returns>
        public static string GetDefaultMask(string value)
        {
            var result = value.ToCharArray();
            for (var i = 3; i < result.Length - 2; i++)
            {
                result[i] = '*';
            }
            return new string(result);
        }

        /// <summary>
        /// Get Format Currency
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <param name="symbol">Currency Symbol</param>
        /// <returns>The display format string</returns>
        public static string GetCurrencyFormat(decimal? value, string symbol)
        {
            return value == null ? "" : symbol + ((decimal)value).ToString("N", CultureInfo.CreateSpecificCulture("es-CR"));
        }


        /// <summary>
        /// Get Format Number
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <returns>The display format string</returns>
        public static string GetNumberFormatCR(decimal? value)
        {
            return value == null ? "" : ((decimal)value).ToString("N", CultureInfo.CreateSpecificCulture("en-US"));
        }



        /// <summary>
        /// Get Format Number
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <returns>The display format string</returns>
        public static string GetNumberFormatCRNoDecimals(decimal? value)
        {
            //string val = ((decimal)value).ToString("G", CultureInfo.CreateSpecificCulture("en-US"));

            return value == null ? "" : Convert.ToInt32(value).ToString("N0", CultureInfo.CreateSpecificCulture("en-US"));
        }


        /// <summary>
        /// Get Format CurrencyNumber
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <param name="symbol">Currency Symbol</param>
        /// <returns>The display format string</returns>
        public static string GetNumberFormatCurrencyCR(decimal? value, string symbol)
        {
            return value == null ? "" : symbol + ((decimal)value).ToString("N", CultureInfo.CreateSpecificCulture("es-CR"));
        }

        /// <summary>
        /// Get Format Number
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <returns>The display format string</returns>
        public static string GetNumberFormat(decimal? value)
        {
            return value == null ? "" : (Math.Round((decimal)value, 0)).ToString();
        }


        /// <summary>
        /// Get Format Number
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <returns>The display format string</returns>
        public static string GetNumberFormatWithDecimal(decimal? value, int cantDec)
        {
            string strresult = "";
            string strFormat = "N" + cantDec;
            decimal valorConDecimal = value == null ? 0 : (decimal)value;
            long valorSinDecimal = (long)valorConDecimal;
            if ((valorConDecimal - valorSinDecimal) > 0)
                strresult = value == null ? "" : (Math.Round((decimal)value, cantDec)).ToString(strFormat, CultureInfo.CreateSpecificCulture("en-US"));
            else
                strresult = valorSinDecimal.ToString();

            return strresult;
        }

        public static string GetNumberFormatWithDecimal(decimal? value, int cantDec, string specificCulture, bool forceDecimal)
        {
            string strresult = "";
            string strFormat = "N" + cantDec;
            decimal valorConDecimal = value == null ? 0 : (decimal)value;
            long valorSinDecimal = (long)valorConDecimal;
            if (!forceDecimal && (valorConDecimal - valorSinDecimal) > 0)
                strresult = value == null ? "" : (Math.Round((decimal)value, cantDec)).ToString(strFormat, CultureInfo.CreateSpecificCulture(specificCulture));
            else
                strresult = valorSinDecimal.ToString("N", CultureInfo.CreateSpecificCulture(specificCulture));

            return strresult;
        }

        /// <summary>
        /// </summary>
        /// Get Format Number
        /// <param name="value">Value to apply format</param>
        /// <returns>The display format string</returns>
        public static string GetPercentageFormat(decimal? value)
        {
            return value == null ? "" : ((decimal)value).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")) + "%";
        }

        /// <summary>
        /// GetPercentageFormat
        /// </summary>
        /// <param name="value"></param>
        /// <param name="rounddecimals"></param>
        /// <returns></returns>
        public static string GetPercentageFormat(decimal value, int rounddecimals)
        {
            return (Math.Round(value, rounddecimals)) + "%";
        }

        /// <summary>
        /// Get Format Number
        /// </summary>
        /// <param name="month">Value to get Name</param>
        /// <returns>The display a month Name</returns>
        public static string GetMonthName(int? month)
        {
            var dateTimeFormatInfo = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR"));
            if (dateTimeFormatInfo == null) return "";
            var monthNames = dateTimeFormatInfo.MonthNames.Take(12).ToList();
            var firstOrDefault = monthNames.Select(m => new { Month = monthNames.IndexOf(m) + 1, MonthNames = new CultureInfo("es-CR").TextInfo.ToTitleCase(m) }).FirstOrDefault(x => x.Month == month);
            return firstOrDefault != null ? firstOrDefault.MonthNames : "";
        }

        /// <summary>
        /// Get Format Date
        /// </summary>
        /// <param name="date">Value to get date</param>
        /// <returns>The display format date</returns>
        public static string GetDateFormat(DateTime? date)
        {
            return date == null ? null : ((DateTime)date).ToString("d", CultureInfo.CreateSpecificCulture("es-CR"));
        }

        /// <summary>
        /// Get Format Date
        /// </summary>
        /// <param name="date">Value to get date</param>
        /// <returns>The display format date</returns>
        public static string GetDateFormat2(DateTime? date)
        {
            return date == null ? null : (((DateTime)date)).ToString("d", CultureInfo.CreateSpecificCulture("es-CR"));
        }

        /// <summary>
        /// Get Format Date Time
        /// </summary>
        /// <param name="date">Value to get date</param>
        /// <returns>The display format date</returns>
        public static string GetDateTimeFormat(DateTime? date)
        {
            return date == null ? null : (((DateTime)date).ToClientTime()).ToString("G", CultureInfo.CreateSpecificCulture("es-CR"));
        }

        /// <summary>
        /// Get Format Date
        /// </summary>
        /// <param name="date">Value to get date</param>
        /// <returns>The display format date</returns>
        public static string GetUtcDateFormat(DateTime? date)
        {
            return date == null ? null : ((DateTime)date).ToString("d", CultureInfo.CreateSpecificCulture("es-CR"));
        }

        /// <summary>
        /// Get Format Date
        /// </summary>
        /// <param name="date">Value to get date</param>
        /// <returns>The display format date</returns>
        public static string GetUtcDateTimeFormat(DateTime? date)
        {
            return date == null ? null : (((DateTime)date)).ToString("dd/MM/yyyy HH:mm:ss");
        }

        /// <summary>
        /// Set Date
        /// </summary>
        /// <param name="date">Value to set date</param>
        /// <returns>A date value</returns>
        public static DateTime? SetDate(string date)
        {
            try
            {
                return date == null ? (DateTime?)null : DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("es-CR"));
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Generate Colors
        /// </summary>
        /// <returns>string array</returns>
        public static IEnumerable<string> GenerateColors(int cantItems = 0)
        {
            var colors = new List<string>
            {
                "rgba(31, 119, 180,1)",
                "rgba(241,92,117,1)",
                "rgba(173, 216, 248,1)",
                "rgba(255, 127, 14,1)",
                "rgba(172, 112, 122,1)",
                "rgba(0, 142, 142,1)",
                "rgba(213, 70, 72,1)",
                "rgba(142, 70, 143,1)",
                "rgba(89, 132, 40,1)",
                "rgba(179, 168, 1,1)",
                "rgba(0, 142, 214,1)",
                "rgba(158, 9, 13,1)",
                "rgba(162, 134, 192,1)",
                "rgba(115, 168, 240,1)",
                "rgba(49, 128, 71,1)",
                "rgba(245, 197, 51,1)",
                "rgba(0, 71, 137,1)",
                "rgba(41, 131, 32,1)",
                "rgba(143, 86, 153,1)",
                "rgba(30, 144, 255,1)",
                "rgba(62, 214, 98,1)",
                "rgba(80, 235, 232,1)",
                "rgba(128, 128, 128,1)",
                "rgba(228, 77, 152,1)",
                "rgba(183, 173, 0,1)"
            };

            if (cantItems > colors.Count)
            {
                var colorsToAdd = colors.ToList();
                var diff = cantItems - colors.Count;
                var each = Convert.ToDecimal(diff) / Convert.ToDecimal(colors.Count);
                var maxEach = Math.Round(each);
                maxEach += (each > maxEach) ? (each - maxEach) : 0;

                for (var i = 0; i < maxEach; i++)
                {
                    colors.AddRange(colorsToAdd);
                }
            }

            var result = colors.ToArray();
            if (cantItems > 0)
            {
                result = colors.Take(cantItems).ToArray();
            }

            return result;
        }

        /// <summary>
        /// Generate Colors
        /// </summary>
        /// <returns>string array</returns>
        public static IEnumerable<string> GenerateAlphaColors()
        {
            var colors = new List<string>
            {
                "rgba(31, 119, 180,0.2)",
                "rgba(255, 127, 14,0.2)",
                "rgba(173, 216, 248,0.2)",
                "rgba(248, 188, 16,0.2)",
                "rgba(140, 186, 0,0.2)",
                "rgba(0, 142, 142,0.2)",
                "rgba(213, 70, 72,0.2)",
                "rgba(142, 70, 143,0.2)",
                "rgba(89, 132, 40,0.2)",
                "rgba(179, 168, 1,0.2)",
                "rgba(0, 142, 214,0.2)",
                "rgba(158, 9, 13,0.2)",
                "rgba(162, 134, 192,0.2)",
                "rgba(115, 168, 240,0.2)",
                "rgba(49, 128, 71,0.2)",
                "rgba(245, 197, 51,0.2)",
                "rgba(0, 71, 137,0.2)",
                "rgba(41, 131, 32,0.2)",
                "rgba(143, 86, 153,0.2)",
                "rgba(30, 144, 255,0.2)",
                "rgba(62, 214, 98,0.2)",
                "rgba(80, 235, 232,0.2)",
                "rgba(128, 128, 128,0.2)",
                "rgba(228, 77, 152,0.2)",
                "rgba(183, 173, 0,0.2)"
            };

            return colors.ToArray();
        }

        /// <summary>
        /// Generate Colors
        /// </summary>
        /// <returns>string array</returns>
        public static IEnumerable<string> GenerateHighlighColors()
        {
            var colors = new List<string>
            {
                "#1F77B4",//rgba(31, 119, 180,0.9)"
                "#FF7F0E",//"rgba(255, 127, 14,0.9)",
                "#ADD8F8",//"rgba(173, 216, 248,0.9)",
                "#F8BC10",//"rgba(248, 188, 16,0.9)",
                "#8CBA00",//"rgba(140, 186, 0,0.9)",
                "#008E8E",//"rgba(0, 142, 142,0.9)",
                "#D54648",//"rgba(213, 70, 72,0.9)",
                "#8E468F",//"rgba(142, 70, 143,0.9)",
                "#598428",//"rgba(89, 132, 40,0.9)",
                "#B3A801",//"rgba(179, 168, 1,0.9)",
                "#008ED6",//"rgba(0, 142, 214,0.9)",
                "#9E090D",//"rgba(158, 9, 13,0.9)",
                "#A286C0",//"rgba(162, 134, 192,0.9)",
                "#73A8F0",//"rgba(115, 168, 240,0.9)",
                "#318047",//"rgba(49, 128, 71,0.9)",
                "#F5C533",//"rgba(245, 197, 51,0.9)",
                "#004789",//"rgba(0, 71, 137,0.9)",
                "#298320",//"rgba(41, 131, 32,0.9)",
                "#8F5699",//"rgba(143, 86, 153,0.9)",
                "#1E90FF",//"rgba(30, 144, 255,0.9)",
                "#3ED662",//"rgba(62, 214, 98,0.9)",
                "#50EBE8",//"rgba(80, 235, 232,0.9)",
                "#808080",//"rgba(128, 128, 128,0.9)",
                "#E44D98",//"rgba(228, 77, 152,0.9)",
                "#B7AD00"//"rgba(183, 173, 0,0.9)"
            };

            return colors.ToArray();
        }

        /// <summary>
        /// Base64 Encode
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns>string Base64</returns>
        public static string Base64Encode(string plainText)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText));
        }

        public static string GeneratePin()
        {
            string result = "0000" + new Random().Next(9999);
            return result.Substring(result.Length - 4);
        }

        /// <summary>
        /// Base64 Decode
        /// </summary>
        /// <param name="encodedText">encodeText</param>
        /// <returns>string plain text</returns>
        public static string Base64Decode(string encodedText)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(encodedText));
        }

        /// <summary>
        /// Liters to Gallons Conversion
        /// </summary>
        /// <param name="liters">liters to convert</param>
        /// <returns>decimal Gallons</returns>
        public static decimal LitersToGallons(decimal? liters)
        {
            decimal litersNotNull = 0;
            decimal conversionValue = Convert.ToDecimal(ConfigurationManager.AppSettings["GALLONSTOLITERSEQUIVALENCE"]);

            if (liters != null)
            {
                litersNotNull = Convert.ToDecimal(liters);
            }
            var gallons = litersNotNull / conversionValue;

            return gallons;
        }

        /// <summary>
        /// Gallons to Liters Conversion
        /// </summary>
        /// <param name="gallons">gallons to convert</param>
        /// <returns>decimal Liters</returns>
        public static decimal GallonsToLiters(decimal? gallons)
        {
            decimal gallonsNotNull = 0;
            decimal conversionValue = Convert.ToDecimal(ConfigurationManager.AppSettings["GALLONSTOLITERSEQUIVALENCE"]);

            if (gallons != null)
            {
                gallonsNotNull = Convert.ToDecimal(gallons);
            }
            var liters = gallonsNotNull * conversionValue;

            return liters;
        }

        /// <summary>
        /// Capacity Unit Conversion
        /// </summary>
        /// <param name="capacityUnitIdFrom">Capacity Unit Id Origin</param>
        /// <param name="capacityUnitIdTo">Capacity Unit Id Destination</param>
        /// <param name="capacityUnitValue">Value to be converted</param>
        /// <returns>decimal capacityUnitValueResult</returns>
        public static decimal CapacityUnitConversion(int capacityUnitIdFrom, int capacityUnitIdTo, decimal? capacityUnitValue)
        {
            decimal capacityUnitValueResult = 0;
            int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
            int gallonsCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["GALLONSCAPACITYUNITID"]);
            if (capacityUnitIdFrom == litersCapacityUnitId && capacityUnitIdTo == gallonsCapacityUnitId)
            {
                capacityUnitValueResult = LitersToGallons(capacityUnitValue);
            }
            else if (capacityUnitIdFrom == gallonsCapacityUnitId && capacityUnitIdTo == litersCapacityUnitId)
            {
                capacityUnitValueResult = GallonsToLiters(capacityUnitValue);
            }
            else
            {                
                capacityUnitValueResult = capacityUnitValue ?? 0;
            }
            return capacityUnitValueResult;
        }

        /// <summary>
        /// Returns the partner name based on the ID
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static string GetPartnerNamebyID(int partnerId)
        {
            using (var p = new PartnerNewsBusiness())
            {
                return p.RetrievePartnerNameById(partnerId);
            }
        }

        /// <summary>
        /// Get Status Checked and Unchecked
        /// </summary>
        /// <param name="Activo"></param>
        /// <returns></returns>
        public static string GetStatusEmailsNotifications(int Activo)
        {
            return Activo == 1 ? "checked=\"checked\"" : "";
        }
        internal static int GetPercentageFormat(double p1, int p2)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Validate Guid Regular Expression
        /// </summary>
        private static readonly Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);

        /// <summary>
        /// Validate if string is a Guid type
        /// </summary>
        /// <param name="candidate"></param>
        /// <returns></returns>
        public static bool IsGuid(string candidate)
        {
            return candidate != null && isGuid.IsMatch(candidate);
        }

        /// <summary>
        /// Return Numeric Currency with culture info
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="currencyAmount"></param>
        /// <returns></returns>
        public static decimal ConvertCurrencyWithCulture(string culture, string currencyAmount, bool defaultCulture = true)
        {
            try
            {
                CultureInfo cultureInfo;
                decimal number;

                if (defaultCulture)
                {
                    cultureInfo = CultureInfo.GetCultureInfo(culture);
                    var decSepar = cultureInfo.NumberFormat.NumberDecimalSeparator;
                    var milSepar = cultureInfo.NumberFormat.NumberGroupSeparator;

                    currencyAmount = currencyAmount.Replace(milSepar, "");
                    currencyAmount = currencyAmount.Replace(decSepar, CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator);

                    number = decimal.Parse(currencyAmount, CultureInfo.InvariantCulture);
                }
                else
                {
                    cultureInfo = CultureInfo.InvariantCulture;
                    var decSepar = cultureInfo.NumberFormat.NumberDecimalSeparator;

                    currencyAmount = currencyAmount.Replace(decSepar, CultureInfo.GetCultureInfo(culture).NumberFormat.NumberDecimalSeparator);

                    number = decimal.Parse(currencyAmount, CultureInfo.GetCultureInfo(culture));
                }

                return number;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Return Numeric Currency with culture info in String format
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="currencyAmount"></param>
        /// <returns></returns>
        public static string ConvertCurrencyWithCultureStr(string culture, string currencyAmount, bool defaultCulture = true, bool currentCulture = false)
        {
            try
            {
                decimal decAmount;
                decimal.TryParse(currencyAmount, out decAmount);

                var cultureInfo = CultureInfo.InvariantCulture;
                var decSepar = cultureInfo.NumberFormat.NumberDecimalSeparator;
                var milSepar = cultureInfo.NumberFormat.NumberGroupSeparator;
                var format = string.Format("#{0}##0{1}00", milSepar, decSepar);

                var number = decAmount.ToString(format);
                currencyAmount = number;

                currencyAmount = currencyAmount.Replace(",", "[M]");
                currencyAmount = currencyAmount.Replace(".", "[D]");

                if (defaultCulture)
                {
                    currencyAmount = currencyAmount.Replace("[D]", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator);
                    currencyAmount = currencyAmount.Replace("[M]", CultureInfo.InvariantCulture.NumberFormat.NumberGroupSeparator);

                    number = currencyAmount;
                }
                else if (currentCulture)
                {
                    currencyAmount = currencyAmount.Replace("[D]", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                    currencyAmount = currencyAmount.Replace("[M]", CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator);

                    number = currencyAmount;
                }
                else
                {
                    currencyAmount = currencyAmount.Replace("[D]", CultureInfo.GetCultureInfo(culture).NumberFormat.NumberDecimalSeparator);
                    currencyAmount = currencyAmount.Replace("[M]", CultureInfo.GetCultureInfo(culture).NumberFormat.NumberGroupSeparator);

                    number = currencyAmount;
                }

                return number;
            }
            catch (Exception)
            {
                return "0,00";
            }
        }

        /// <summary>
        /// Convert StringDate to DateTime
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime ConvertDateFromApprovalToDateTime(string date)
        {   
            if(date.ToUpper().Contains("ENE"))
            {
                date = date.ToUpper().Replace("ENE", "JAN");
            }            
            else if (date.ToUpper().Contains("ABR"))
            {
                date = date.ToUpper().Replace("ABR", "APR");
            }
            else if (date.ToUpper().Contains("AGO"))
            {
                date = date.ToUpper().Replace("AGO", "AUG");
            }
            else if (date.ToUpper().Contains("SET") || date.ToUpper().Contains("SEPT"))
            {
                date = date.ToUpper().Replace("SEPT", "SEP");

                date = date.ToUpper().Replace("SET", "SEP");
            }
            else if (date.ToUpper().Contains("DIC"))
            {
                date = date.ToUpper().Replace("DIC", "DEC");
            }
            
            return DateTime.ParseExact(date, "d-MMM-yy", CultureInfo.InvariantCulture);            
        }

        /// <summary>
        /// Get XML Data 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public static string GetXML<T>(List<T> list, string root = null)
        {
            var XMLData = string.Empty;

            using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<T>));
                if (!string.IsNullOrEmpty(root))
                {
                    xmlSerializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute(root));
                }
                xmlSerializer.Serialize(stringWriter, list);
                return XMLData = stringWriter.ToString().Substring(stringWriter.ToString().IndexOf('\n') + 1);
            }
        }

        /// <summary>
        /// Return List from XML
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="XML"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public static T GetXMLDeserialize<T>(string XML, string root)
        {
            using (XmlReader reader = XmlReader.Create(new StringReader(XML)))
            {
                return (T)new XmlSerializer(typeof(T), new XmlRootAttribute(root)).Deserialize(reader);
            }
        }        
    }
}