﻿/************************************************************************************************************
*  File    :Extensions.cs
*  Summary : Extensions has common Methods for Extens
*  Author  : Berman Romero
*  Date    : 08/01/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web;

namespace ECOsystem.Utilities
{
    /// <summary>
    /// Extensions Class
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Convert the passed datetime into client timezone.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime ToClientTime(this DateTime dt)
        {
            try
            {
                var timeOffSet = HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"];  // read the value from session
                if (timeOffSet == null) return dt.ToLocalTime(); // if there is no offset in session return the datetime in server timezone
                var offset = int.Parse(timeOffSet.ToString());
                dt = dt.AddMinutes(offset);
                return dt;
            }
            catch 
            {
                return dt;
            }
            
        }

        /// <summary>
        /// Convert the passed datetime into client timezone.
        /// </summary>
        /// <returns></returns>
        public static int ToTimeOffSet(this int i)
        {
            var timeOffSet = HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"];  // read the value from session

            if (timeOffSet == null) return 0; // if there is no offset in session return the datetime in server timezone

            i = int.Parse(timeOffSet.ToString());

            return i;
            
        }

        /// <summary>
        /// Validates if object is null return false
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static bool IsNullToFalse(this bool? val)
        {
            return val ?? false;
        }
        
        /// <summary>
        /// Validates if the int is null
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static bool IsNull(this int? val)
        {
            return val == null;
        }
    }
}