﻿/************************************************************************************************************
*  File    : ServiceVPOS.cs
*  Summary : Service VPOS Tool
*  Author  : Gerald Solano
*  Date    : 02/10/2014
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/


using System;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;

namespace ECOsystem.Utilities
{
#pragma warning disable 1587

    /// <summary>
    /// Service VPOS Tool
    /// </summary>
    public class ServiceVPOS : IDisposable
    {
        /// <summary>
        /// Connect to Service with url provided
        /// </summary>
        /// <param name="creditCardTransaction"></param>
        /// <returns></returns>
        //public string ConnectToService(ref CreditCardTransaction creditCardTransaction, WServiceModel servModel)
        //{
        //    try
        //    {
        //        switch (servModel.PartnerCode)
        //        {
        //            case "BAC-CRI": //Web service autorizador de Costa Rica
        //                using (var vpos = new ECOsystem.Utilities.CredomaticVPOSTool())
        //                {
        //                    return vpos.ConnectToCredomatic(ref creditCardTransaction, servModel);
        //                }
        //            case "BAC-HND": //Web service autorizador de Honduras
        //                using (var vpos = new ECOsystem.Utilities.CredomaticVPOSTool())
        //                {
        //                    return vpos.ConnectToCredomatic(ref creditCardTransaction, servModel);
        //                }
        //            default:
        //                break;
        //        }

        //        return string.Empty;    

        //    }
        //    catch (Exception ex)
        //    {
        //        //Add log event
        //        AddLogEvent(
        //                message: string.Empty,
        //                userId: 0,
        //                customerId:  0,
        //                partnerId: 0,
        //                exception: ex,
        //                isError: true);

        //        return string.Empty;
        //    }
        //}

        /// <summary>
        /// Apply Reverse proccess when SALE process is not success for Timeout | Error
        /// </summary>
        /// <param name="creditCardTransaction"></param>
        //public void ApplyReverseProcess(CreditCardTransaction creditCardTransaction, string message)
        //{
        //    try
        //    {
        //        var credomaticVPOS = new CredomaticVPOS.AuthorizationServiceClient();

        //        var identification = new CredomaticVPOS.clsAuthenticationHeader
        //        {
        //            UserName = "", //ConfigurationManager.AppSettings["CREDOMATICVPOSUSER"],
        //            Password = "" //ConfigurationManager.AppSettings["CREDOMATICVPOSPASSWORD"]
        //        };
        //        if (creditCardTransaction.TransactionType == "SALE")
        //        {
        //            var requestREVERSE = new CredomaticVPOS.clsWSRequestData
        //            {
        //                transactionType = "REVERSE",
        //                terminalId = creditCardTransaction.TerminalId,
        //                invoice = creditCardTransaction.Invoice
        //            };

        //            var response = credomaticVPOS.executeTransaction(identification, requestREVERSE);

        //            using (var businessEx = new CreditCardBusiness())
        //            {
        //                var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
        //                                                  response.responseCode,
        //                                                  response.responseCodeDescription,
        //                                                  message,
        //                                                  IsSuccess:true,
        //                                                  IsTimeOut:false,
        //                                                  IsCommunicationError:false);
        //            }
        //        }
        //    }
        //    catch (TimeoutException tex)
        //    {
        //        using (var businessEx = new CreditCardBusiness())
        //        {
        //            var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
        //                                               "APPLY_REVERSE_TIME_OUT",
        //                                               "APPLY_REVERSE_TIME_OUT",
        //                                               string.Format("Message: {0} | InnerException: {1}", tex.Message, tex.InnerException),
        //                                               false,
        //                                               true,
        //                                               false);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        using (var businessEx = new CreditCardBusiness())
        //        {
        //            var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
        //                                               "APPLY_REVERSE_ERROR_COMMUNICATION",
        //                                               "APPLY_REVERSE_ERROR_COMMUNICATION",
        //                                               string.Format("Message: {0} | InnerException: {1}", ex.Message, ex.InnerException),
        //                                               false,
        //                                               false,
        //                                               true);
        //        }
        //    }

        //}

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = "Folder:Utilities",
                Action = "ServiceVPOS",
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}