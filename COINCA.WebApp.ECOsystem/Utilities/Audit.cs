﻿using System;

namespace ECOsystem.Audit
{
#pragma warning disable 1591

    //Enumerador con los tipos de mensajes asociados
    /// <summary>
    /// Types of log messages
    /// </summary>
    public enum TypeOfMessage
    {       
        Error = 3,
        Warning = 2,
        Informative = 1,
        Debug = 0
    }

    /// <summary>
    /// Propiedades de entidad para el manejo de eventos  
    /// </summary>
    public class EventLog
    {
        /// <summary>
        /// EventId
        /// </summary>
        public int? EventId { get; set; }

        /// <summary>
        /// TypeOfMessage
        /// </summary>
        public TypeOfMessage EventType { 
            get {
                var type = TypeOfMessage.Informative;
                if (IsWarning)
                    type = TypeOfMessage.Warning;
                else 
                    if (IsError)
                    type = TypeOfMessage.Error;

                return type;
            } 
        }

        /// <summary>
        /// UserId
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// CustomerId
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// PartnerId
        /// </summary>
        public int? PartnerId { get; set; }

        /// <summary>
        /// Controller
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// Action
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// IsInfo
        /// </summary>
        public bool IsInfo { get; set; }

        /// <summary>
        /// IsWarning
        /// </summary>
        public bool IsWarning { get; set; }

        /// <summary>
        /// IsError
        /// </summary>
        public bool IsError { get; set; }

        /// <summary>
        /// LogDateTime
        /// </summary>
        public DateTime LogDateTime { get; set; }
    }
}