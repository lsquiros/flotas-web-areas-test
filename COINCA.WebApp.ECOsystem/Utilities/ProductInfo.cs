﻿/************************************************************************************************************
*  File    : ProductInfo.cs
*  Summary : Different products templates
*  Author  : Gerald Solano
*  Date    : 11/04/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

namespace ECOsystem.Utilities
{
    /// <summary>
    /// Different products templates
    /// </summary>
    public enum ProductInfo
    {
        /// <summary>
        /// Service Template
        /// </summary>
        Services = 0,
        /// <summary>
        /// Intrack-ECO Template
        /// </summary>
        IntrackECO = 1,
        /// <summary>
        /// BAC Flota Template
        /// </summary>
        BACFlota = 2,
        /// <summary>
        /// LineVitaECO Template
        /// </summary>
        LineVitaECO = 3
    }
}