﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;

namespace ECOsystem.Utilities
{
    public class NumToLetter
    {
        private String[] Units = { "", " un", " dos", " tres", " cuatro", " cinco", " seis", " siete", " ocho", " nueve" };
        private String[] Tens = {" diez", " once", " doce", " trece", " catorce", " quince", " dieciseis",  " diecisiete", " dieciocho", " diecinueve", " veinte", " treinta", " cuarenta", " cincuenta", " sesenta", " setenta", " ochenta", " noventa"};
        private String[] Hundreds = {"", " ciento", " doscientos", " trecientos", " cuatrocientos", " quinientos", " seiscientos", " setecientos", " ochocientos", " novecientos"};

        private Regex r;

        public string ConvertToLetters(String num, bool mayus)
        {
            String literal = "";
            String dec;
            
            num = num.Replace(".", ",");

            if (num.IndexOf(",") == -1)
            {
                num = num + ",00";
            }
            
            r = new Regex(@"\d{1,9},\d{1,2}");
            MatchCollection mc = r.Matches(num);
            if (mc.Count > 0)
            {                
                String[] Num = num.Split(',');

                dec = string.IsNullOrEmpty(Num[1]) ? " " : "," + getDecimals(Num[1].Substring(0, 2)); 
               
                if (int.Parse(Num[0]) == 0)
                {              
                    literal = "cero";
                }
                else if (int.Parse(Num[0]) > 999999)
                {
                    literal = getMillions(Num[0]);
                }
                else if (int.Parse(Num[0]) > 999)
                {
                    literal = getThousands(Num[0]);
                }
                else if (int.Parse(Num[0]) > 99)
                {
                    literal = getHundreds(Num[0]);
                }
                else if (int.Parse(Num[0]) > 9)
                {
                    literal = getTens(Num[0]);
                }
                else
                {
                    literal = getUnits(Num[0]);
                }
                
                if (mayus)
                {
                    return (literal + dec).ToUpper();
                }
                else
                {
                    return (literal + dec + " ");
                }
            }
            else
            {
                return literal = string.Empty;
            }
        }

        private String getUnits(String num)
        {   
            String num1 = num.Substring(num.Length - 1);
            return Units[int.Parse(num1)];
        }

        private String getTens(String num)
        {                     
            int n = int.Parse(num);
            if (n < 10)
            {
                return getUnits(num);
            }
            else if (n > 19)
            {
                String u = getUnits(num);
                if (u.Equals(""))
                { 
                    return Tens[int.Parse(num.Substring(0, 1)) + 8];
                }
                else
                {
                    return Tens[int.Parse(num.Substring(0, 1)) + 8] + " y" + u;
                }
            }
            else
            {
                return Tens[n - 10];
            }
        }

        private String getHundreds(String num)
        {
            if (int.Parse(num) > 99)
            {
                if (int.Parse(num) == 100)
                {
                    return " cien";
                }
                else
                {
                    return Hundreds[int.Parse(num.Substring(0, 1))] + getTens(num.Substring(1));
                }
            }
            else
            {
                return getTens(int.Parse(num) + "");
            }
        }

        private String getThousands(String num)
        {
            String c = num.Substring(num.Length - 3);            
            String m = num.Substring(0, num.Length - 3);
            String n = "";
            
            if (int.Parse(m) > 0)
            {
                n = getHundreds(m);
                return n + " mil" + getHundreds(c);
            }
            else
            {
                return "" + getHundreds(c);
            }

        }

        private String getMillions(String num)
        { 
            String miles = num.Substring(num.Length - 6);            
            String millon = num.Substring(0, num.Length - 6);
            String n = "";
            if (millon.Length > 1)
            {
                n = getHundreds(millon) + " millones";
            }
            else
            {
                n = getUnits(millon) + " millon";
            }
            return n + getThousands(miles);
        }

        private String getDecimals(string num)
        {
            String literal = "";
            if (int.Parse(num) > 9)
            {
                literal = getTens(num);
            }
            else
            {
                literal = getUnits(num);
            }
            return literal;
        }
    }
}