﻿
//using ECOsystem.Models.Control;
/************************************************************************************************************
*  File    : CredomaticVPOSTool.cs
*  Summary : Credomatic VPOS Tool
*  Author  : Manuel Azofeifa Hidalgo
*  Date    : 11/18/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;

namespace ECOsystem.Utilities
{
    /// <summary>
    /// CredomaticVPOSTool
    /// </summary>
    public class CredomaticVPOSTool : IDisposable
    {
#pragma warning disable 1587

        /// <summary>
        /// ConnectToCredomatic
        /// </summary>
        public string ConnectToCredomatic(ref CreditCardTransaction creditCardTransaction, WServiceModel servModel)
        {
            //request.terminalId = ConfigurationManager.AppSettings["CREDOMATICVPOSTERMINAL"];
            var credomaticVPOS = new CredomaticVPOS.AuthorizationServiceClient();

            //credomaticVPOS.Endpoint.Binding.SendTimeout = new TimeSpan(0, 0, 60);
            //credomaticVPOS.Endpoint.Binding.ReceiveTimeout = new TimeSpan(0, 0, 60);

            var identification = new CredomaticVPOS.clsAuthenticationHeader
            {
                UserName = servModel.UserAuth,
                Password = servModel.PasswordAuth
            };

            try
            {
                CredomaticVPOS.clsWSResponseData response = new CredomaticVPOS.clsWSResponseData();

                #region URI Configuration
                credomaticVPOS.Endpoint.Address = new System.ServiceModel.EndpointAddress(servModel.UriService);
                #endregion

                /// Proceso Switch para realizar los procesos de SALE y VOID
                switch (creditCardTransaction.TransactionType)
                {
                    case "SALE":
                        var requestSALE = new CredomaticVPOS.clsWSRequestData
                        {
                            transactionType = creditCardTransaction.TransactionType,
                            terminalId = creditCardTransaction.TerminalId,
                            invoice = creditCardTransaction.Invoice,
                            entryMode = "MNL",
                            accountNumber = creditCardTransaction.AccountNumber, //"4587963287456654"
                            expirationDate = creditCardTransaction.ExpirationDate, // "2502"
                            totalAmount = creditCardTransaction.TotalAmount.ToString("0.00"),
                            localDate = DateTime.Now.ToString("MMddyyyy"),
                            localTime = DateTime.Now.ToString("HHmmss"),
                            carTag = creditCardTransaction.Plate,
                            kilometers = creditCardTransaction.Odometer.ToString(),
                            units = creditCardTransaction.Liters.ToString("0.00")
                            //units = creditCardTransaction.Liters.ToString("0.00")
                        };

                        response = credomaticVPOS.executeTransaction(identification, requestSALE);
                        break;

                    case "PREAUTHORIZED_SALE":
                        var requestPREAUTHORIZED_SALE = new CredomaticVPOS.clsWSRequestData
                        {
                            transactionType = creditCardTransaction.TransactionType,
                            terminalId = creditCardTransaction.TerminalId,
                            invoice = creditCardTransaction.Invoice,
                            entryMode = "MNL",
                            accountNumber = creditCardTransaction.AccountNumber, //"4587963287456654"
                            expirationDate = creditCardTransaction.ExpirationDate, // "2502"
                            totalAmount = creditCardTransaction.TotalAmount.ToString("0.00"),
                            localDate = DateTime.Now.ToString("MMddyyyy"),
                            localTime = DateTime.Now.ToString("HHmmss"),
                            carTag = creditCardTransaction.Plate,
                            kilometers = creditCardTransaction.Odometer.ToString(),
                            units = creditCardTransaction.Liters.ToString("0.00"),
                            authorizationNumber = creditCardTransaction.AuthorizationNumber
                        };

                        response = credomaticVPOS.executeTransaction(identification, requestPREAUTHORIZED_SALE);
                        break;

                    case "VOID":
                        var requestVOID = new CredomaticVPOS.clsWSRequestData
                        {
                            transactionType = creditCardTransaction.TransactionType,
                            terminalId = creditCardTransaction.TerminalId,
                            systemTraceNumber = creditCardTransaction.SystemTraceNumber,
                            referenceNumber = creditCardTransaction.ReferenceNumber,
                            authorizationNumber = creditCardTransaction.AuthorizationNumber
                        };

                        response = credomaticVPOS.executeTransaction(identification, requestVOID);
                        break;
                    default:
                        break;
                }
                using (var business = new CreditCardBusiness())
                {
                    if (response.responseCode.Contains("00") || response.responseCodeDescription.Contains("APROBADA"))
                    {
                        creditCardTransaction.AuthorizationNumber = response.authorizationNumber;
                        creditCardTransaction.ReferenceNumber = response.referenceNumber;
                        creditCardTransaction.SystemTraceNumber = response.systemTraceNumber;

                        var result = business.AddLogTransactionsVPOS(
                                    (int)creditCardTransaction.CCTransactionVPOSId,
                                    response.responseCode,
                                    response.responseCodeDescription,
                                    "Success",
                                    true,
                                    false,
                                    false);
                    }
                    else
                    {
                        //SISTEMA NO DISPONIBLE //REINTENTE TRANSACCION TO

                        //if (response.responseCodeDescription.Contains("SISTEMA NO DISPONIBLE")
                        //        || response.responseCode.Contains("TO")
                        //        || response.responseCodeDescription.Contains("REINTENTE TRANSACCION TO"))
                        //{
                        //    isTimeOut = true;
                        //    isErrorUnknown = false;

                        //    #region Apply REVERSE
                        //    ApplyReverseProcess(creditCardTransaction, "Apply REVERSE");
                        //    #endregion
                        //}

                        var result = business.AddLogTransactionsVPOS(
                                    (int)creditCardTransaction.CCTransactionVPOSId,
                                    response.responseCode,
                                    response.responseCodeDescription,
                                    "Fail",
                                    true,
                                    false,
                                    false);
                    }
                }
                return response.responseCodeDescription;
            }
            catch (TimeoutException tex)
            {
                using (var businessEx = new CreditCardBusiness())
                {
                    var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                       "TIME_OUT",
                                                       "TIME_OUT",
                                                       string.Format("Message: {0} | InnerException: {1}", tex.Message, tex.InnerException),
                                                       false,
                                                       true,
                                                       false);


                    #region Apply REVERSE
                    ApplyReverseProcess(creditCardTransaction, "Apply REVERSE");
                    #endregion
                }

                return "TIME_OUT";
            }
            catch (Exception ex)
            {
                using (var businessEx = new CreditCardBusiness())
                {
                    var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                       "ERROR_COMMUNICATION",
                                                       "ERROR_COMMUNICATION",
                                                       string.Format("Message: {0} | InnerException: {1}", ex.Message, ex.InnerException),
                                                       false,
                                                       false,
                                                       true);
                }

                #region Apply REVERSE
                ApplyReverseProcess(creditCardTransaction, "Apply REVERSE");
                #endregion

                return "ERROR_COMMUNICATION";
            }
        }

        /// <summary>
        /// Apply Reverse proccess when SALE process is not success for Timeout | Error
        /// </summary>
        /// 
        public void ApplyReverseProcess(CreditCardTransaction creditCardTransaction, string message)
        {
            try
            {
                var credomaticVPOS = new CredomaticVPOS.AuthorizationServiceClient();

                var identification = new CredomaticVPOS.clsAuthenticationHeader
                {
                    UserName = ConfigurationManager.AppSettings["CREDOMATICVPOSUSER"],
                    Password = ConfigurationManager.AppSettings["CREDOMATICVPOSPASSWORD"]
                };
                if (creditCardTransaction.TransactionType == "SALE")
                {
                    var requestREVERSE = new CredomaticVPOS.clsWSRequestData
                         {
                             transactionType = "REVERSE",
                             terminalId = creditCardTransaction.TerminalId,
                             invoice = creditCardTransaction.Invoice
                         };

                    var response = credomaticVPOS.executeTransaction(identification, requestREVERSE);

                    using (var businessEx = new CreditCardBusiness())
                    {
                        var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                          response.responseCode,
                                                          response.responseCodeDescription,
                                                          message,
                                                          true,
                                                          false,
                                                          false);
                    }
                }
            }
            catch (TimeoutException tex)
            {
                using (var businessEx = new CreditCardBusiness())
                {
                    var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                       "APPLY_REVERSE_TIME_OUT",
                                                       "APPLY_REVERSE_TIME_OUT",
                                                       string.Format("Message: {0} | InnerException: {1}", tex.Message, tex.InnerException),
                                                       false,
                                                       true,
                                                       false);
                }
            }
            catch (Exception ex)
            {
                using (var businessEx = new CreditCardBusiness())
                {
                    var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                       "APPLY_REVERSE_ERROR_COMMUNICATION",
                                                       "APPLY_REVERSE_ERROR_COMMUNICATION",
                                                       string.Format("Message: {0} | InnerException: {1}", ex.Message, ex.InnerException),
                                                       false,
                                                       false,
                                                       true);
                }
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}