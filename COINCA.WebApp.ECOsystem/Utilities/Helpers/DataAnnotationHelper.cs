﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace ECOsystem.Utilities.Helpers
{
#pragma warning disable 1591

    /// <summary>
    /// RequiredIfAttribute Class
    /// </summary>
    public class RequiredIfAttribute : RequiredAttribute
    {
        /// <summary>
        /// property
        /// </summary>
        private readonly string _condition;

        /// <summary>
        /// RequiredIfAttribute Class
        /// </summary>
        /// <param name="condition"></param>
        public RequiredIfAttribute(string condition)
        {
            _condition = condition;
        }
        /// <summary>
        /// IsValid
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Delegate conditionFunction = CreateExpression(validationContext.ObjectType, _condition);
            bool conditionMet = (bool)conditionFunction.DynamicInvoke(validationContext.ObjectInstance);
            if (conditionMet)
            {
                if (value == null)
                {
                    return new ValidationResult(FormatErrorMessage(null));
                }
            }
            return null;
        }

        /// <summary>
        /// CreateExpression
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        private Delegate CreateExpression(Type objectType, string expression)
        {
            // TODO - add caching
            var lambdaExpression =
                     DynamicExpression.ParseLambda(
                               objectType, typeof(bool), expression);
            var func = lambdaExpression.Compile();
            return func;
        }
    }

    /// <summary>
    /// Author: Andrés Oviedo Brenes
    /// </summary>
    public class ExcelNoMappedColumnAttribute : DisplayNameAttribute
    {

    }

    /// <summary>
    /// Author: Andrés Oviedo Brenes
    /// </summary>
    public class ExcelMappedColumnAttribute : DisplayNameAttribute
    {
        private readonly string m_columnName;

        public ExcelMappedColumnAttribute(string columnName)
        {
            m_columnName = columnName;
        }

        public override string DisplayName
        {
            get
            {
                return m_columnName;
            }
        }
    }

    /// <summary>
    /// Author: Gerald Solano
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class OnlyDecimalAttribute : ValidationAttribute, IClientValidatable
    {
        // Internal field
        readonly string _delimiter;

        /// <summary>
        /// Contructor with delimiter decimal
        /// </summary>
        /// <param name="delimiter"></param>
        public OnlyDecimalAttribute(string delimiter) : base("{0} contiene caracteres inválidos.")
        {
            _delimiter = delimiter;
        }

        /// <summary>
        /// Validation Result Attribute 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null) return ValidationResult.Success;
            var input = (string)value;
            if (MatchesDecimal(_delimiter, input))
                return ValidationResult.Success;

            //Return error is not match
            var errorMessage = FormatErrorMessage(validationContext.DisplayName);
            return new ValidationResult(errorMessage);
        }

        /// <summary>
        /// Match if is decimal
        /// </summary>
        /// <param name="delimiter"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        internal bool MatchesDecimal(string delimiter, string value)
        {
            var rgx = new Regex(@"^(\d[" + delimiter + @"]{1})|\d$");
            return rgx.IsMatch(value);
        }

        /// <summary>
        /// Client Side Validation
        /// </summary>
        /// <param name="metadata"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ModelClientValidationRule>
        GetClientValidationRules(ModelMetadata metadata,
                                 ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "onlydecimal"
            };
            yield return rule;
        }
    }
}