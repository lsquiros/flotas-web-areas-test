﻿using System;
using System.IO;
using System.Web.Mvc;

namespace ECOsystem.Utilities.Helpers
{
    /// <summary>
    /// Panel Extensions
    /// </summary>
    public static class PanelExtensions
    {
        /// <summary>
        /// Custom Panel
        /// </summary>
        /// <param name="helper">HtmlHelper</param>
        /// <param name="icon">icon for the panel</param>
        /// <param name="title">title for the panel</param>
        /// <param name="colorScheme">color scheme like primary</param>
        /// <returns></returns>
        public static MvcPanel Panel(this HtmlHelper helper, string icon, string title, string colorScheme)
        {
            helper.ViewContext.Writer.Write(@"<div class='panel panel-{2}'>
                                                <div class='panel-heading moveleft'>
                                                    <i class='glyphicon glyphicon-{0}'></i>&nbsp;&nbsp;<strong>{1}</strong> 
                                                </div>", icon, title, colorScheme);
            return new MvcPanel(helper.ViewContext);
        }
    }

    /// <summary>
    /// MvcPanel
    /// </summary>
    public class MvcPanel : IDisposable
    {
        /// <summary>
        /// TextWriter
        /// </summary>
        private readonly TextWriter _writer;
        
        /// <summary>
        /// MvcPanel Constructor
        /// </summary>
        /// <param name="viewContext"></param>
        public MvcPanel(ViewContext viewContext)
        {
            _writer = viewContext.Writer;
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            _writer.Write("</div>");
        }
    }
}