﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace ECOsystem.Utilities.Helpers
{
    /// <summary>
    /// </summary>
    public class AntiForgeryHandleErrorAttribute : HandleErrorAttribute
    {
        /// <summary>Called when an exception occurs.</summary>
        /// <exception cref="T:System.ArgumentNullException">The <paramref>
        ///         <name>filterContext</name>
        ///     </paramref>
        ///     parameter is null.</exception>
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is HttpAntiForgeryException)
            {
                var url = string.Empty;
                if (!context.HttpContext.User.Identity.IsAuthenticated)
                {
                    var requestContext = new RequestContext(context.HttpContext, context.RouteData);
                    url = RouteTable.Routes.GetVirtualPath(requestContext, new RouteValueDictionary(new { Controller = "Account", action = "Login" })).VirtualPath;
                }
                else
                {
                    context.HttpContext.Response.StatusCode = 200;
                    context.ExceptionHandled = true;
                    url = GetRedirectUrl(context);
                }
                context.HttpContext.Response.Redirect(url, true);
            }
            else
            {
                base.OnException(context);
            }
        }

        private string GetRedirectUrl(ExceptionContext context)
        {
            try
            {
                var requestContext = new RequestContext(context.HttpContext, context.RouteData);
                var url = RouteTable.Routes.GetVirtualPath(requestContext, new RouteValueDictionary(new { Controller = "Account", action = "AlreadySignIn" })).VirtualPath;

                return url;
            }
            catch (Exception)
            {
                throw new NullReferenceException();
            }
        }
    }
}