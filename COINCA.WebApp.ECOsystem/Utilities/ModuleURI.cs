﻿/************************************************************************************************************
*  File    : ModuleURI.cs
*  Summary : Get Module URI Base for the correct Module Website
*  Author  : Gerald Solano
*  Date    : 12/05/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using ECOsystem.Business.Utilities;
using ECOsystem.Models.Home;

namespace ModuleConfig
{
    /// <summary>
    /// Different Module Sites
    /// </summary>
    public enum ModuleTypeURI
    {
        /// <summary>
        /// Efficiency Module
        /// </summary>
        ModuleEfficiency = 1,
        /// <summary>
        /// Control Module
        /// </summary>
        ModuleControl = 2,
        /// <summary>
        /// Operation Module
        /// </summary>
        ModuleOperation = 3,
        /// <summary>
        /// Administration Module
        /// </summary>
        ModuleAdministration = 4,
        /// <summary>
        /// Coinca Module
        /// </summary>
        ModuleCoinca = 5,
        /// <summary>
        /// Partner Module
        /// </summary>
        ModulePartner = 6,
        /// <summary>
        /// Core
        /// </summary>    
        ModuleCore = 7,
        /// <summary>
        /// ModuleManual
        /// </summary>
        ModuleManual = 8,
        /// <summary>
        /// Insurance Module
        /// </summary>
        ModuleInsurance = 9,
        /// <summary>
        /// Module Maps
        /// </summary>
        ModuleMaps = 10,
        /// <summary>
        /// Module Maps
        /// </summary>
        ModuleReports = 11,
        /// <summary>
        /// Service Stations
        /// </summary>
        ModuleServiceStations = 12,
        /// <summary>
        /// Management
        /// </summary>
        ModuleManagement = 13
    }

    /// <summary>
    /// Utility class for get module base URI
    /// </summary>
    public class ModuleURI
    {
        /// <summary>
        /// Get module base URI 
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public static string GetURI(ModuleTypeURI module)
        {
            try
            {
                string module_uri = string.Empty;
                ModulesUrl model = new ModulesUrl();

                if (System.Web.HttpContext.Current.Session["ModulesURL"] == null)
                {
                    model = ECOsystem.Business.Utilities.GeneralCollections.RetrieveModulesUrl();
                    System.Web.HttpContext.Current.Session["ModulesURL"] = model;
                }
                else
                {
                    model = (ModulesUrl)System.Web.HttpContext.Current.Session["ModulesURL"];
                }

                switch (module)
                {
                    case ModuleTypeURI.ModuleEfficiency:
                        module_uri = GetBaseUrl() + model.EFFICIENCY_URI;
                        break;
                    case ModuleTypeURI.ModuleControl:
                        module_uri = GetBaseUrl() + model.CONTROL_URI;
                        break;
                    case ModuleTypeURI.ModuleOperation:
                        module_uri = GetBaseUrl() + model.OPERATION_URI;
                        break;
                    case ModuleTypeURI.ModuleAdministration:
                        module_uri = GetBaseUrl() + model.ADMIN_URI;
                        break;
                    case ModuleTypeURI.ModuleCoinca:
                        module_uri = GetBaseUrl() + model.COINCA_URI;
                        break;
                    case ModuleTypeURI.ModulePartner:
                        module_uri = GetBaseUrl() + model.PARTNER_URI;
                        break;
                    case ModuleTypeURI.ModuleCore:
                        module_uri = GetBaseUrl();
                        break;
                    case ModuleTypeURI.ModuleManual:
                        module_uri = GetBaseUrl() + model.ECOMANUAL_URI;
                        break;
                    case ModuleTypeURI.ModuleInsurance:
                        module_uri = GetBaseUrl() + model.INSURANCE_URI;
                        break;
                    case ModuleTypeURI.ModuleMaps:
                        module_uri = GetBaseUrl() + model.MAPS_URI;
                        break;
                    case ModuleTypeURI.ModuleReports:
                        module_uri = GetBaseUrl() + model.REPORTS_URI;
                        break;
                    case ModuleTypeURI.ModuleServiceStations:
                        module_uri = GetBaseUrl() + model.SERVICESTATIONS_URI;
                        break;
                    case ModuleTypeURI.ModuleManagement:
                        module_uri = GetBaseUrl() + model.MANAGEMENT_URI;
                        break;
                    default:
                        module_uri = string.Empty;
                        break;
                }

                return module_uri;
            }
            catch (Exception ex)
            {
                var message = string.Format("[ERROR_GET_MODULE_URI] Message: {0} | InnerException: {1}", ex.Message, ex.InnerException);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, message, true);
                return string.Empty;
            }
        }

        /// <summary>
        /// Get the Url for the site
        /// </summary>
        /// <returns></returns>
        static string GetBaseUrl()
        {
            string[] url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Split('/');
            return url[0] + "//" + url[2];
        }
    }
}
