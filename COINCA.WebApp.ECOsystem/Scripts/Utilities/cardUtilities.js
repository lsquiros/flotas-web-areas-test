﻿/*!
 * cardUtilities.js
 * Copyright 2016 Coinca Telematics
 *
 * Librería para la funcionalidad de los indicadores tipo tarjeta
 *
 */
$(function () {

    $("[detail-reveal]").click(function () {
        var idReveal = $(this).attr("detail-reveal");
        $(idReveal).show();
        $("#card-grand-wrapper").removeClass("grand-detail");
    });
    
    $(".reset-hide-detail").click(function () {
        $("#card-grand-wrapper").addClass("grand-detail");
        $(".detail-reveal-hide").each(function () {
            $(this).hide();
        });
    });

    $(".force-close-card-reveal").click(function (e) {
        $("body").find('.card-reveal')
            .velocity(
                { translateY: 0 },
                {
                    duration: 225,
                    queue: false,
                    easing: 'easeInOutQuad',
                    complete: function () {
                        $(this).css({ display: 'none' });
                    }
                }
             );
    });
});
