﻿var ECOsystem = ECOsystem || {};
var enableEditButton = true;

ECOsystem.Utilities = (function () {
    var options = {};
    var defaultContentHeight = $("#wrapper").height();

    /* initialize function */

    var initialize = function (opts) {
        $.extend(options, opts);
        initAjaxPrefilterRequest();
        initMenuItem();
        initNumericInputs();
        //initCommonMethods();
        clickCalendarButton();
        permissionAlert();
        getImageProfile();

        //Tabs Management
        initCurrentTabs();
        initChangeTabs();

        //Get the sized of the grid and adjust its container
        initCardResize();

        //Notifications
        initAlertNotifications();
    }

    var initAjaxPrefilterRequest = function () {
        $.ajaxPrefilter(function (opc) {
            var url = opc.url;
            var host = options.host;
            var newURI = url;

            url = url.replace("https://", "");
            url = url.replace("http://", "");

            var pathArray = url.split('/');
            var secondLevelLocation = pathArray[1];

            if (pathArray.length >= 1) {
                if (secondLevelLocation != host && host != "Home" && secondLevelLocation != "ASPMenus") {
                    newURI = "/" + host + "/" + pathArray[pathArray.length - 2] + "/" + pathArray[pathArray.length - 1];
                }
                opc.url = newURI;
            }
        });
    };

    var initCardResize = function () {
        if ($('table.grid-table').width() != undefined) {
            var GridW = $('table.grid-table').width();
            var CarddW = $('.card').width();

            if (GridW > CarddW) {
                $(".card").width(GridW + 200);
            }
            else {
                return;
            }
        }
    }

    /* Common methods for differents scenarios */
    var initCommonMethods = function () {
        // Wait to render left menu
        window.setTimeout(function () {
            fixTemplateFooterWidth();
        }, 3000);

        //fixTemplateContentHeightMethod();
    }

    /* Select the MenuItem related to the Loaded page */
    var initMenuItem = function () {
        var $menuItem = $("#side-menu").find("a[href*='" + options.menuPath + "']");
        if ($menuItem.html() != null) { //Firs time this object is null, shouldn't remove the class so the default value can be displayed
            $("#side-menu").find(".active").removeClass("active");
        }
        $menuItem.addClass("active");
    }

    /* Fix dynamic width for the footer template when the grid-tables has more pixels  */
    var fixTemplateFooterWidth = function () {
        if ($("table").css("width") != undefined) {

            var currentTableWidth = parseInt($("table").css("width").replace("px", ""));
            var currentLeftPanel = ($(".sidebar-nav").css("width") != undefined) ? parseInt($(".sidebar-nav").css("width").replace("px", "")) : 0;
            var currentContentTemplate = ($("#page-content-wrapper").css("width") != undefined) ? (parseInt($("#page-content-wrapper").css("width").replace("px", "")) - currentLeftPanel) : 0;
            var currentFooterWidth = parseInt($(".site-footer").css("width").replace("px", ""));
            var currentMainTemplate = parseInt($("#principal").css("width").replace("px", ""));

            var diffTableWithMain = currentTableWidth - currentContentTemplate;
            var sumContentTemplateWithDiff = ($("#page-content-wrapper").css("width") != undefined) ? (parseInt($("#page-content-wrapper").css("width").replace("px", "")) + diffTableWithMain) : 0;

            if (diffTableWithMain > 0 && currentFooterWidth < sumContentTemplateWithDiff) {
                var newFooterWidth = currentMainTemplate + diffTableWithMain;
                $(".site-footer").css("width", newFooterWidth + "px");
            }
        }
    }

    /* Fix dynamic height for the Content template when the menu height has more pixels */
    var fixTemplateContentHeightMethod = function () {
        /*Init*/
        defaultContentHeight = $("#wrapper").height();
        setTimeout(initfixTemplateContentHeightForMenu, 1000)
        setTimeout(initfixTemplateContentHeightForTreeView, 1000)

        $(".sidebar-nav").off('click.ApplyTemplateHeight', 'li a').on('click.ApplyTemplateHeight', 'li a', function () {
            setTimeout(initfixTemplateContentHeightForMenu, 1000)
        });
    }

    var initfixTemplateContentHeightForMenu = function () {
        var currentMenuHeight = $("li").parent("#side-menu2").height();

        if (currentMenuHeight >= defaultContentHeight) {
            $("#wrapper").css("height", currentMenuHeight + "px");
        } else {
            $("#wrapper").css("height", defaultContentHeight + "px");
        }
    }

    /* Fix dynamic height for the footer template when the treeviewcontainer has more pixels  */
    var initfixTemplateContentHeightForTreeView = function () {
        if ($("#treeViewContainer").css("height") != undefined) {
            var currentTreeViewHeight = $("#treeViewContainer").height();
            var currentContentHeight = $("#wrapper").height();

            if (currentTreeViewHeight >= defaultContentHeight) {
                currentTreeViewHeight = (currentTreeViewHeight - currentContentHeight) + currentContentHeight;
                $("#wrapper").css("height", currentTreeViewHeight + "px");
            } else {
                $("#wrapper").css("height", defaultContentHeight + "px");
            }
        }
    }

    /* Show modal for edit after that information has been loaded*/
    var alarmModalShow = function () {
        //$("#alarmForm").find('select').not("[custom='true']").select2();
        //$('#alarmModalGeoFences').modal('show');
        //$('#alarmModalSchedule').modal('show');        
    };

    var alarmModalClose = function (e) {
        $('#alarmMaintenanceModal').modal('hide');
        //$('#alarmModalGeoFences').modal('hide');
        //$('#alarmModalSchedule').modal('hide');
        //$('#alarmModalVehicles').modal('hide');
    };

    /* Show modal for edit after that information has been loaded*/
    var addOrEditModalShow = function () {
        $("#addOrEditForm").find('select').not("[custom='true']").select2();
        $('#addOrEditModal').modal('show');
        var width = $(window).width();
        var height = $(window).height() - 30;
        $(".resizeMap").width(width - 50).height(height - 150);
        $(".resizeIframeMap").width(width - 70).height(height - 150);
        $(".body-modal-ResizeMap").width(width - 70).height(height - 175);
    };

    /* Close modal for delete after that information has been deleted*/
    var deleteModalClose = function () {
        $('#deleteModal').modal('hide');
        $('#deleteTModal').modal('hide');
        $('#deleteModalCustomerUsers').modal('hide');
        $('#deleteCustomerbyPartner').modal('hide');
    };

    var addOrEditModalShowCreate = function () {
        $('#addOrEditModal').modal('show');
    };

    /* Close modal for add or edit after that information has been updated*/
    var addOrEditModalClose = function () {
        initCardResize();
        $('#addOrEditModal').modal('hide');
    };

    var permissionAlert = function () {
        $('#permissionModuleModal').modal('hide');
        $('a[permission_Alert]').click(function () {
            $('#permissionModuleModal').modal('show');
        });
    };

    /* Clear fields in ajax begin form */
    var modalClear = function () {
        var addOrEditModal = $('#addOrEditModal');
        addOrEditModal.find('input:text, input[type=number], input:hidden, textarea').val('');
        addOrEditModal.find('select').attr('selectedIndex', '-1').find("option:selected").removeAttr('selected');

        addOrEditModal.find('img').attr('src', '');
        addOrEditModal.find('img').attr('title', '');
        addOrEditModal.find('.field-validation-error').html('');
        addOrEditModal.find('.label-clear').html('');

        addOrEditModal.find('input:checkbox').val('true');
        addOrEditModal.find('input:checkbox').removeAttr("checked");
        addOrEditModal.find('input:checkbox').siblings('input:hidden').val('false');
        addOrEditModal.find('#validationErrors').addClass('hide');

        addOrEditModal.find('#processing').addClass('hide');
        addOrEditModal.find('button').removeAttr("disabled");
        addOrEditModal.find('select').not("[custom='true']").select2();
        type = 0;
        $(".element-name").html('');

    };

    /* Success Search call back function*/
    var successSearch = function () {
        $('body').loader('hide');
        //$('#txtSearch').val('');
    };

    var type;
    var setTitle = function (element) {

        $(window).off('shown.bs.modal').on('shown.bs.modal', function () {
            var name = $(element).parents().find(".grid-row-selected td:first-child").html();
            if (name == null) {

                name = $(element).parent().parent().parent().find("td:first-child").html();
            }

            if (type == 1) { $(".element-name").html(' - ' + name); }
        });
    }

    /* Init events for delete */
    var initGrid = function () {

        $('#gridContainer').off('click.del', 'a[del_row]').on('click.del', 'a[del_row]', function (e) {
            $('#deleteModal').find('#id').val($(this).attr('id'));
            type = 1;
            setTitle($(this));
        });

        $('#gridContainer').off('click.edit', 'a[edit_row]').on('click.edit', 'a[edit_row]', function (e) {
            if (enableEditButton == true) {
                enableEditButton = false;
                ECOsystem.Utilities.ShowLoader();

                $('#loadForm').find('#id').val($(this).attr('id'));
                $('#loadForm').attr("data-ajax-complete", "ECOsystem.Utilities.HideLoader();");
                $('#loadForm').submit();
                type = 1;
                setTitle($(this));

                window.setTimeout(function () {
                    enableEditButton = true;
                }, 5000);
            }
        });


        //Edit for the CostCenter grid
        $('#gridContainer').off('click.edit', 'a[edit_row_costcenter]').on('click.edit', 'a[edit_row_costcenter]', function (e) {
            ECOsystem.Utilities.ShowLoader();
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();
            type = 1;
            //setTitle($(this));
        });

        $('#detailContainer').off('hidden.bs.modal', '#addOrEditModal').on('hidden.bs.modal', '#addOrEditModal', function (e) {
            modalClear();
        });

        $('#detailContainer').off('click.checkbox', 'input:checkbox').on('click.checkbox', 'input:checkbox', function (e) {
            var check = $(this).is(':checked') ? "true" : "false";
            $(this).siblings('input:hidden').val(check);
        });

        try {
            $("#addOrEditAlarmForm").data("validator").settings.submitHandler = function (e) {
                $('#alarmContainer').find('#processing').removeClass('hide');
                $('#alarmContainer').find('button').attr("disabled", "disabled");
            };
        } catch (e) {
        }

        $('#alarmContainer').off('click.checkbox', 'input:checkbox').on('click.checkbox', 'input:checkbox', function (e) {
            var check = $(this).is(':checked') ? "true" : "false";
            $(this).siblings('input:hidden').val(check);
        });

        try {
            $("#addOrEditForm").data("validator").settings.submitHandler = function (e) {
                $('#detailContainer').find('#processing').removeClass('hide');
                $('#detailContainer').find('button').attr("disabled", "disabled");
            };
        }
        catch (e) {
        }
        $("#addOrEditForm").find('select').not("[custom='true']").select2();
        $("#addOrEditAlarmForm").find('select').not("[custom='true']").select2();

    };

    /*Convert str to Number*/
    var toNum = function (num) {
        
        num = ("" + num).replace(/\./g, '').replace(/,/g, '.');
        return isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
    }

    /*Convert str to Float*/
    var toFloat = function (num) {
        num = ("" + num).replace(/,/g, '');
        return isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
    }

    /*Convert str to Int*/
    var toInt = function (num) {
        num = ("" + num).replace(/,/g, '');
        return isNaN(num) || num === '' || num === null ? 0 : parseInt(num);
    }

    var setConfirmation = function () {
        /*window.setTimeout(function () {
            $("#confirmation").addClass("in");
        }, 100);

        window.setTimeout(function () {
            $("#confirmation").addClass("out");
        }, 3000);*/
    }

    /*format Number*/
    var formatNum = function (num) {
        
        return num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    /*format Currency with Symbol*/
    var formatCurrency = function (num, s) {
        return s + '' + num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    var unspin = function (target, icon) {
        $(target).addClass(icon);
        $(target).removeClass("glyphicon-refresh");
        $(target).removeClass("glyphicon-refresh-animate");
    }

    var spin = function (target, icon) {
        $(target).removeClass(icon);
        $(target).addClass("mdi-refresh");
        $(target).addClass("glyphicon-refresh-animate");
    }

    var setConfirmation = function () {
        $("#confirmation").removeClass("out");
        $("#confirmation").css("display", "block");
        window.setTimeout(function () {
            $("#confirmation").addClass("in");
        }, 100);

        window.setTimeout(function () {
            $("#confirmation").removeClass("in");
            $("#confirmation").addClass("out");
            window.setTimeout(function () {
                $("#confirmation").css("display", "none");
            }, 2000);
        }, 2000);
    }

    var setMessageShow = function (message, type) {

        var typeIcon = "";
        var typeMessage = "";
        typeMessage = (type == "SUCCESS") ? "alert-success" : typeMessage;
        typeMessage = (type == "INFO") ? "alert-info" : typeMessage;
        typeMessage = (type == "ALERT") ? "alert-warning" : typeMessage;
        typeMessage = (type == "ERROR") ? "alert-danger" : typeMessage;

        typeIcon = (type == "SUCCESS") ? "glyphicon glyphicon-ok-sign" : typeIcon;
        typeIcon = (type == "INFO") ? "glyphicon glyphicon-info-sign" : typeIcon;
        typeIcon = (type == "ALERT") ? "glyphicon glyphicon-exclamation-sign" : typeIcon;
        typeIcon = (type == "ERROR") ? "glyphicon glyphicon-remove-sign" : typeIcon;

        $("#confirmation").html("<i class='" + typeIcon + "'></i>&nbsp;" + message);
        $("#confirmation").removeClass("alert-success");
        $("#confirmation").removeClass("out");
        $("#confirmation").css("display", "block");

        if (type == "SUCCESS") {
            $("#confirmation").css("color", "#3c763d");
            $("#confirmation").css("background", "#dff0d8");
        }

        if (type == "ALERT") {
            $("#confirmation").css("color", "#FFFFFF");
            $("#confirmation").css("background", "#ffa026");
        }

        if (type == "ERROR") {
            $("#confirmation").css("color", "#FFFFFF");
            $("#confirmation").css("background", "#e4002b");
        }

        if (type == "INFO") {
            $("#confirmation").css("color", "#FFFFFF");
            $("#confirmation").css("background", "#1e45ad");
        }

        $("#confirmation").addClass(typeMessage);

        window.setTimeout(function () {
            $("#confirmation").addClass("in");
        }, 100);

        window.setTimeout(function () {
            $("#confirmation").removeClass("in");
            $("#confirmation").addClass("out");
            window.setTimeout(function () {
                $("#confirmation").css("display", "none");
            }, 4000);
        }, 4000);
    }

    /*Popup Center*/
    var showPopup = function (pageUrl, title) {
        var w = screen.width * 0.60;
        var h = screen.height * 0.95;
        var left = (screen.width / 2) - (w / 2);
        //var top = (screen.height / 2) - (h / 2);
        var popUpObj = window.open(pageUrl, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, ' +
            'width=' + w + ', height=' + h + ', top=' + 0 + ', left=' + left);
        popUpObj.resizeTo(w, h);
        popUpObj.focus();
    }

    /*Set to numeric format to all inputs with attr numeric*/
    var initNumericInputs = function () {
        $("input[numeric]").each(function (index) {
            var obj = $(this);
            obj.numeric("integer");
        });
    };

    /*Event when click in the calendar button*/
    var clickCalendarButton = function () {
        $("body").off('click.Show', '.input-group-addon').on('click.Show', '.input-group-addon', function (e) {
            //$(this).siblings("input[type='text']").datepicker('show');
            $(this).siblings("input[type='text']").trigger("click");
        });
    };

    /*Get Root Folder for EcoSystem Routes*/
    var getUrl = function () {
        return options.Url;
    };

    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
    };

    //menuHover
    //$(".sidebar-nav").hover(function () {
    //    alert(1);
    //    //$(this).fadeOut(100);
    //    //$(this).fadeIn(500);
    //});

    // Get Image in background for users
    var getImageProfile = function (e) {
        $('img[image-profile]').each(function () {
            try {
                var img = $(this);
                var userId = img.attr('userId');
                if (userId != undefined && userId != null) {
                    var postData = { userId: userId };
                    $.ajax({
                        type: "POST",
                        url: '/Users/GetUserProfileImage',
                        data: postData,
                        success: function (data) {
                            img.attr("src", data.result);
                        },
                        dataType: "json",
                        traditional: true
                    });


                } else {
                    console.info("Etiqueta img[image-profile] no tiene UserId asignado.");
                }
            } catch (e) {
                console.error(e.message);
            }
        });
    };

    //Clear Text
    var successEmailNotificationCreation = function () {

        $('#txtEmailNotification').val('');
        $('#ddlEmailNotification').val('');
        $('#EmailNotificationModal').modal('show');
    };

    var showMapsServiceStation = function () {
        $('#ShowMapsServiceStatios').modal('show');
    };

    var setMenuItemActive = function (obj) {
        $.ajax({
            type: "POST",
            url: '/ASPMenus/SetMenuActive',
            data: { MenuId: obj },
            success: function (data) {
                console.log("Success");
            },
            dataType: "json",
            traditional: true
        });
    };

    //Session Init
    var initSessionVars = function () {

        if (!store.enabled) {
            alert('Local storage is not supported by your browser. Please disable "Private Mode", or upgrade to a modern browser.')
            return
        }
        //Restore Session Vars
        store.clear();
    };

    // Set Sessions Var
    var setSessionVar = function (name, value) {

        var result = store.set(name, value);
    };

    // Get Sessions Var
    var getSessionVar = function (name) {

        var value = store.get(name);
        return value;
    };

    // Init Current Tabs
    var initCurrentTabs = function () {
        var panelTab = $(".page-wrap .nav-tabs");
        if (panelTab != null && panelTab != undefined && panelTab.length > 0) {

            var getCurrentTab = getSessionVar("currentTab");

            if (getCurrentTab != null && getCurrentTab != undefined) {
                $(".page-wrap .nav-tabs li").removeClass("active");
                var objLink = $("a[href=" + getCurrentTab + "]");
                objLink.trigger("click");
                objLink.parent("li").addClass("active");
            }

        }
    }

    // Init change Tabs
    var initChangeTabs = function () {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab

            setSessionVar("currentTab", target);
        });
    }

    //Alert Notifications
    var initAlertNotifications = function () {
        $("#LoadRenderAlarms").submit();
    };

    var setTemperatureAlert = function (reportId) {
        switch (reportId) {
            case 31:
                return "HighTemperature";
            case 32:
                return "LowTemperature";
            default:
                return "";
        }
    }

    var setValidation = function () {
        $("#confirmation").html("¡Algunos campos son requeridos!")
        $("#confirmation").removeClass("out");
        $("#confirmation").css("background", "red");
        $("#confirmation").css("color", "white");
        $("#confirmation").css("display", "block");
        window.setTimeout(function () {
            $("#confirmation").addClass("in");
        }, 100);

        window.setTimeout(function () {
            $("#confirmation").removeClass("in");
            $("#confirmation").addClass("out");
            window.setTimeout(function () {
                $("#confirmation").css("display", "none");
            }, 2000);
        }, 2000);
    }

    /* Customer survey methods */
    var showCustomerSurvey = function (survey) {
        $('body').loader('show');
        $.ajax({
            type: "POST",
            url: '/CustomerSurvey/LoadSurvey',
            data: { obj: survey },
            success: function (data) {
                $('#CustomerSurveyContainer').html(data);
                showQuestions();
                $('#CustomerSurveyModal').modal('show');
                $('body').loader('hide');
            }
        });
    }

    var showQuestions = function () {
        $.ajax({
            type: "POST",
            url: '/CustomerSurvey/ShowQuestions',
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    $('#questionsContainer').append(data[i]);
                }
            }
        });
    }

    var onSaveSurvey = function (questionId, questionType, finish) {
        var list = getAnswerInformation(questionId, questionType);

        if (list.length > 0) {           
            $.ajax({
                type: "POST",
                url: '/CustomerSurvey/SaveSurvey',
                data: { answers: JSON.stringify(list) },
                success: function (data) {
                    $('#footerContainer').hide();
                    $('#questionsContainer').html('<div class="row"><div class="col-sm-12 center"><h1>¡Muchas gracias por su tiempo!</h1></div></div>');
                    setTimeout(function () {
                        $('#CustomerSurveyModal').modal('hide');
                    }, 2000);
                }
            });
        }
    }

    var getAnswerInformation = function () {
        var list = [];
        var warningShowed = false;

        $('.questioDetail').each(function () {
            var question = $(this);
            var questionId = question.find('#questionId').val();
            var questionType = question.find('#questionTypeId').val();
            
            if (questionType === "1") {
                var radios = question.find('.rdbAnswers:checked')
                if (radios.length > 0) {
                    question.find('#lblWarning').hide();
                    radios.each(function () {
                        let obj = {
                            'Id': $(this).attr('id'),
                            'PartnerSurveyQuestionId': questionId,
                            'NextSurveyQuestionId': $(this).attr('nextQuestionId'),
                            'Checked': true
                        };
                        list.push(obj);
                    });
                } else {
                    question.find('#lblWarning').show();
                    warningShowed = true;
                }                
            } else if (questionType === "2") {
                var checks = question.find('.chkAnswers:checked');
                if (checks.length > 0) {
                    question.find('#lblWarning').hide();
                    checks.each(function () {
                        let obj = {
                            'Id': $(this).attr('id'),
                            'PartnerSurveyQuestionId': questionId,
                            'Checked': true
                        };
                        list.push(obj);
                    });
                } else {
                    question.find('#lblWarning').show();
                    warningShowed = true;
                }                
            } else if (questionType === "3") {
                let obj = {
                    'PartnerSurveyQuestionId': questionId,
                    'AnswerText': question.find('#txtAnswer').val()
                }
                list.push(obj);
            }
        });

        list = warningShowed ? [] : list;
        return list;
    }

    /* *********************************** */

    /* Public methods */
    return {
        Init: initialize,
        InitGrid: initGrid,
        InitSessionVars: initSessionVars,
        SetSessionVar: setSessionVar,
        GetSessionVar: getSessionVar,
        AlarmModalClose: alarmModalClose,
        AlarmModalShow: alarmModalShow,
        AddOrEditModalClose: addOrEditModalClose,
        AddOrEditModalShow: addOrEditModalShow,
        DeleteModalClose: deleteModalClose,
        SuccessSearch: successSearch,
        FormatCurrency: formatCurrency,
        FormatNum: formatNum,
        ToFloat: toFloat,
        ToInt: toInt,
        ToNum: toNum,
        ShowPopup: showPopup,
        setConfirmation: setConfirmation,
        SetMessageShow: setMessageShow,
        Unspin: unspin,
        Spin: spin,
        ApplyNumericInputs: initNumericInputs,
        GetUrl: getUrl,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        SuccessEmailNotificationCreation: successEmailNotificationCreation,
        ShowMapsServiceStation: showMapsServiceStation,
        SetMenuItemActive: setMenuItemActive,
        AddOrEditModalShowCreate: addOrEditModalShowCreate,
        FixRenderTreeview: initfixTemplateContentHeightForTreeView,
        InitCardResize: initCardResize,
        SetTemperatureAlert: setTemperatureAlert,
        SetValidation: setValidation,
        ShowCustomerSurvey: showCustomerSurvey,
        OnSaveSurvey: onSaveSurvey
    };
})();
