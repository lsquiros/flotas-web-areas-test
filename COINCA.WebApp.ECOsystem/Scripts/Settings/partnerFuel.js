﻿var ECOsystem = ECOsystem || {};

ECOsystem.PartnerFuel = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        var select2 = $("#KeyId").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectChange(data);
                    return fn.apply(this, arguments);
                }
            }
        })(select2.onSelect);


        $('#detailContainer').off('click.btnSubmit', 'button[type="submit"]').on('click.btnSubmit', 'button[type="submit"]', function (e) {
            $('#detailContainer').find('#PartnerId').val($('#KeyId').val());
            $('#LiterPrice').val($('#LiterPrice').val().replace(/,/g, ''));
        });

        $('#detailContainer').find('#LiterPrice').numeric({
            allowMinus   : false
        });

        
    };
    /* partner Select Format*/
    var vehicleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-4">' + json.Name + '</div>' +
                '<div class="col-md-2">' + json.CostCenterName + '</div>' +
                '<div class="col-md-4">' + json.CategoryType + '</div>' +
                '<div class="col-md-2">' + json.FuelName + '</div>' +
                '</div>';
    };

    /*on Select Change*/
    var onSelectChange = function (obj) {
        if (obj.id === '') {
            $('#PartnerFuelDetail').html('');
        } else {
            $("#KeyId").val(obj.id);
            $('#ReloadForm').submit();
        }
    };

    /* Public methods */
    return {
        Init: initialize
    };
})();

