﻿var ECOsystem = ECOsystem || {};

ECOsystem.DriversByVehicle = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        
        //$('#VehicleId').select2({
        //    formatResult: vehiclesSelectFormat,
        //    formatSelection: vehiclesSelectFormat,
        //    escapeMarkup: function (m) { return m; }
        //});
        
        var select2 = $("#VehicleId").select2({
            formatResult: vehiclesSelectFormat,
            formatSelection: vehiclesSelectFormat,
            escapeMarkup: function (m) { return m; }
        }).data('select2');

        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectChange(data);
                    return fn.apply(this, arguments);
                }
            }
        })(select2.onSelect);

        $('.connected').sortable({
            connectWith: '.connected'
        });

        $('#btnSubmit').off('click.btnSubmit').on('click.btnSubmit', function (e) {
            $('#ConnectedDrivers').find('input:hidden').each(function (i) {
                $(this).attr('name', 'ConnectedDriversList[' + i + '].UserId');
            });
            return true;
        });

        $("#saveForm").data("validator").settings.submitHandler = function (e) {
            $('#DragAndDropContainer').find('#processing').removeClass('hide');
            $('#DragAndDropContainer').find('button').attr("disabled", "disabled");
            $('#DragAndDropContainer').find('a[atype="button"]').attr("disabled", "disabled");
        };
    };

    var vehiclesSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-6">' + json.PlateId + '</div>' +
                '<div class="col-md-6">' + json.Name + '</div>' +
                '</div>';
    };

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {        
        initialize();       
        $('body').loader('hide');
    };

    var onSelectChange = function (obj) {        
        $('#id').val(obj.id);
        $("input:hidden[id='VehicleId']").val(obj.id);

        if (obj.id === '') {
            $('#DragAndDropContainer').html('');
        } else {
            $('body').loader('show');
            $('#loadForm').submit();
        }
    };

    var onBeginAddOrEdit = function () {
        $('body').loader('show');
    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad : onSuccessLoad,
        OnBeginAddOrEdit : onBeginAddOrEdit
    };
})();

