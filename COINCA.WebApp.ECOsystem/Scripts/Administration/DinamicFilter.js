﻿var ECOsystem = ECOsystem || {};

ECOsystem.DinamicFilter = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);   
              
        $("#ddlUsers").off('change.ddlUsers').on('change.ddlUsers', function () {
            var val = $("#ddlUsers").val();

            if (val != "")
            {
                onRolesChangeMenu();
                $('#ddlFilterType').attr('disabled', false);                
            }                
            else
            {
                location.reload(); 
            }                
        });
    };
   
    var saveDinamicFilter = function () {
        
        var checked = $('#treeview-checkable').treeview('getChecked');
        var key = $('#ddlFilterType').val();
        var userId = $("#ddlUsers").val();
        var list = [];

        $.each(checked, function () {            
                list.push(this.id);            
        })

        $.ajax({
            url: 'DinamicFilter/SaveTreeViewData',
            type: 'POST',
            data: JSON.stringify({ List: list, Key: key, UserId : userId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data == 1) {                    
                    $('#ConfirmationSaveModalDinamicFilter').modal('hide');
                    $('#InformationMessageModal').modal('show');                    
                    location.reload();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
        });
    }

    var confirmationModalShow = function () {       
        $('#ConfirmationSaveModalDinamicFilter').modal('show');
    };

    var onRolesChangeMenu = function () {
        
        $('#ddlFilterType').attr('disabled', true);
        $('#ddlFilterType').empty();
        $('#ddlFilterType').append('<option value>Seleccione un Tipo de Filtro...</option>');
               
        $.ajax({
            url: 'DinamicFilter/GetFilterType',
            type: 'POST',
            data: JSON.stringify({ }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                
                for (var i = 0; i < data.length; i++) {
                    $('#ddlFilterType').append('<option value="' + data[i].Value + '">'
                            + data[i].Text +
                            '</option>');
                }                
            }
        });      
    }
    
    return {
        Init: initialize,
        SaveDinamicFilter: saveDinamicFilter,
        ConfirmationModalShow: confirmationModalShow
        
    };
})();