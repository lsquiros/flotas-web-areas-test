﻿var ECOsystem = ECOsystem || {};

ECOsystem.SettingOdometers = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        initializeDropDownList();

        validateInputValues();

        onClickbtnAppliesMassive();

        onClickbtnSearch();

        $('#gridContainer').off('blur.inputOnBlur', '.calculateactual').on('blur.inputOnBlur', '.calculateactual', function () {
            
            var transactionid = $(this).attr("TransactionId");
            var odometerlast = $("#txtodometerlast_" + transactionid).html();
            var odometer = $("#txtodometer_" + transactionid).val();
            $("#odometer_" + transactionid).val(odometer);
            $("#lblTravel_" + transactionid).html(odometer - odometerlast);
            var travel = $("#lblTravel_" + transactionid).html();
            $("#travel_" + transactionid).val(travel);
            var liters = $("#liters_" + transactionid).val();
            $("#lblPerformance_" + transactionid).html(travel / liters);
            var performance = $("#lblPerformance_" + transactionid).html();
            $("#performance_" + transactionid).val(performance);
            var row = $(this).closest(".grid-row");
            row.removeClass("grid-row-selected");
            row.addClass("grid-row-selected");
            $('.grid-row-selected td').css("background-color", "transparent");
            $('.grid-row-selected').css("background-color", "#FFFF73");
        });


        try {
            $("#applyAllForm").data("validator").settings.submitHandler = function (e) {
                $('#ControlsContainer').find('#processing').removeClass('hide');
                $('#ControlsContainer').find('button[data-btnmodal=true]').attr("disabled", "disabled");
            };
        } catch (e) {
            
        }        

    };

    $('#datetimepickerStartDate').datepicker({
        language: 'es',
        minDate: new Date(),
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (e) {
        if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects())
            $('#ReloadForm').submit();
        else
            invalidSelectOption();
    });
    $("#Parameters_StartDateStr").alphanum({
        allowNumeric: true,
        allowUpper: false,
        allowLower: false,
        allowCaseless: true,
        allowSpace: false,
        allow: '/',
        maxLength: 10
    });


    $('#datetimepickerEndDate').datepicker({
        language: 'es',
        minDate: new Date(),
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (e) {
        if ($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects())
            $('#ReloadForm').submit();
        else
            invalidSelectOption();
    });

    /* Show or hide errors messages*/
    var setAmountErrorMsj = function (msj, show, typeError) {

        typeError = typeError != null ? typeError : 'e1';

        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').removeClass('hide');
                    $('#errorAmountMsj3').html(msj);
                    $('#validationAmountError3').removeClass('hide');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').addClass('hide');
                    $('#errorAmountMsj3').html('');
                    $('#validationAmountError3').addClass('hide');
                }
                break;
            default:
                break;
        }


    }

   
    /*Convert str to Number*/
    var toNum = function (num) {
        if (num.toString().indexOf(".") > -1 && num.toString().indexOf(",") > -1) {
            num = ("" + num).replace(/\./g, '').replace(/,/g, '.');
        }
        //SQuiros: No valida correctamente la función isNaN(num) || 
        return num === '' || num === null ? 0.00 : parseFloat(num);
    }

    /*format Number*/
    var formatNum = function (num) {
        return num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#Parameters_ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        try {           
            var select4 = $("#Parameters_OdometerType").select2().data('select2');
            select4.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectOdometerTypeChange(data); return fn.apply(this, arguments); }
                }
            })(select4.onSelect);
        } catch (e) {

        }
        try {
            var select5 = $("#Parameters_CustomerId").select2().data('select2');
            select5.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectCustomerIdChange(data); return fn.apply(this, arguments); }
                }
            })(select5.onSelect);
        } catch (e) {

        }

    };

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Parameters_Month").select2('val', '');
            $("#Parameters_Year").select2('val', '');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');

            $('#Parameters_ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Month').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Year').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Odometer Type Change*/
    var onSelectOdometerTypeChange = function (obj) {        
        $('#Parameters_OdometerType').val(obj.id);
        $('#ReloadForm').submit();                
    };

   


    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        if (($('#VehicleGroupId').val() === '') && ($('#CostCenterId').val() === '')) {
            invalidSelectOption();
            return false;
        }
        return true;
    };

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<div class="text-danger top30">Por favor seleccionar las opciones correspondientes para cargar la información.</div>');
        $('#gridContainer').html('');
    };




    var validateInputValues = function () {

        $('#gridContainer').off('click.actualizarMontos', '#actualizarMontos').on('click.actualizarMontos', '#actualizarMontos', function () {
            
            var isValidValues = true;
            var cont = 0;
            $('#gridContainer').find('input[data-form-value]').each(function (i) {
                var value = parseInt($(this).attr('value'));
                if (value <= 0) {
                    isValidValues = false;
                }
                
            });

            $('#gridContainer tr').each(function (i) {

                if ($(this).css('background-color') === 'rgb(255, 255, 115)') {                    
                    cont++;
                }

            });

            if (isValidValues || cont == 0) {
                setAmountErrorMsj('', true, 'e2');
                $('#confirmModalactualizar').modal('show');
            } else {
                if (cont == 0) {
                    var msj = '* No hay ningún valor editado para actualizar';
                }else var msj = '* Ningún monto puede ser igual que 0.00';
                setAmountErrorMsj(msj, true, 'e2');
            }
        });


        $('#gridContainer').off('click.actualizarMontos1', '#actualizarMontos1').on('click.actualizarMontos1', '#actualizarMontos1', function () {
            
            var isValidValues = true;
            var cont = 0;
            $('#gridContainer').find('input[data-form-value]').each(function (i) {
                var value = parseInt($(this).attr('value'));
                if (value <= 0) {
                    isValidValues = false;
                }

            });

            $('#gridContainer tr').each(function (i) {

                if (($(this).css('background-color') === 'rgb(255, 255, 115)')) {
                    cont++;
                }

            });

            if (isValidValues) {
                setAmountErrorMsj('', true, 'e2');
                $('#confirmModalactualizar').modal('show');
            } else {
                if (cont == 0) {
                    var msj = '* No hay ningún valor editado para actualizar';
                }else var msj = '* Ningún monto puede ser igual que 0.00';
                setAmountErrorMsj(msj, true, 'e2');
            }
        });

    };


    /* On click Btn Applies Massive*/
    var onClickbtnAppliesMassive = function () {       
        var _value = null;
        $('#btnAppliesMassive').off('click.btnAppliesMassive').on('click.btnAppliesMassive', function (e) {
            $('#gridContainer tr').each(function (i) {

                if ($(this).css('background-color') === 'rgb(255, 255, 115)') {

                    $(this).find('input[Transactionid]').each(function () {

                            var id = $(this).attr('transactionid');
                            var odometer = $("#odometer_" + id).val();                           
                            if (_value == null)
                                _value = id + '|' + toNum(odometer);
                            else
                                _value = _value + '|' + id + '|' + toNum(odometer);                            
                        });
                      
               


                }

            });

            $('#applyNewValues').find('#ChainValues').val(_value);
            $("#applyNewValues").submit();
            //$("#applyNewValues").submit();
        });
    };


    /* On click Btn Applies Massive*/
    var onClickbtnSearch = function () {
        
        var _value = 0;
        $('#btnSearch').off('click.btnSearch').on('click.btnSearch', function (e) {
            $('#gridContainer tr').each(function (i) {

                if ($(this).css('background-color') === 'rgb(255, 255, 115)') {

                    _value++    
                }

            });
            
            if (_value > 0) $("#confirmModalSearch").modal('show');
            else $("#ReloadForm").submit();
            
        });
    };

    var initCurrency = function () {
        try {
            
            $('#wrapper').off('blur.currencyOnBlur', 'input[data-currency]').on('blur.currencyOnBlur', 'input[data-currency]', function (e) {
                var obj = $(this);

                obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
                obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));


            });
            $('#wrapper').find('input[data-currency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });

        } catch (e) {

        }
    }


    /*Set Applies Massive Validation*/
    var setAppliesMassiveValidation = function () {
        
        $("#errormsj").html('');
        var value = $('#Data_ValueStr').val();
        if (value.length == 0 || ECOsystem.Utilities.ToNum(value) == 0) {
            $("#errormsj").html('Monto requerido y debe ser mayor que 0.00');

        }

        else
            $("#confirmModal").modal('show');
    };



    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        $('#confirmModal').modal('hide');
        $('#confirmModalactualizar').modal('hide');
        $('#ControlsContainer').find('#processing').addClass('hide');
        $('#ControlsContainer').find('button[data-btnmodal=true]').removeAttr("disabled");

        $('#Data_Value').val('');
        $('#Data_ValueStr').val('');
        $('#Data_ValueType').select2('val', '');

        $('#gridContainer').find("span[data-toggle=tooltip]").tooltip('hide');

        $("#AlarmsDdl").load('/ASPMenus/Alarms')

    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,        
        SetAppliesMassiveValidation: setAppliesMassiveValidation,
        //CheckAllValues: checkallvalues
    };
})();
