﻿var ECOsystem = ECOsystem || {};

ECOsystem.UserRolesPermissions = (function () {
    var options = {};
    
    var initialize = function (opts) {
        $.extend(options, opts);

        $('#gridContainer').off('click.edit_row', 'a[edit_row]').on('click.edit_row', 'a[edit_row]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();
        });

        $('#gridContainer').off('click.del', 'a[del_row]').on('click.del', 'a[del_row]', function (e) {
            $('#deleteModal').find('#id').val($(this).attr('id'));
            type = 1;
            setTitle($(this));
        });

        $('#detailContainer').off('blur.inputOnBlur', '#txtRoleName').on('blur.inputOnBlur', '#txtRoleName', function () {
            var roleName = $('#txtRoleName').val();
            
            if (roleName == "" || roleName == null)
            {
                $('#RoleNameValidation').html('El nombre del rol es requerido');
            }
            else
            {
                $.ajax({
                    url: '/UserRolesPermission/CheckValidRoleName',
                    type: 'POST',
                    data: JSON.stringify({ RoleName: roleName, Parent : '' }),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        
                        if (data == 0)
                        {
                            $('#RoleNameValidation').html('El rol ya existe en la base de datos');
                            $('#btnSaveRoles').addClass('disabled');
                        }
                        else
                        {
                            $('#btnSaveRoles').removeClass('disabled');
                        }
                    }
                });
            }
        });

        $('#detailContainer').off('focusin.inputOnBlur', '#txtRoleName').on('focusin.inputOnBlur', '#txtRoleName', function () {
            $('#RoleNameValidation').html('');                      
        });
   
    };

    var onSuccessLoad = function () {
        $('#addOrEditModal').modal('show');
        $('#btnSaveRoles').addClass('disabled');
    };

    var addModulesModal = function () {
        $('#loadForm').find('#id').val('-1');
        $('#loadForm').submit();
    };

    var onRolesChangeMenu = function () {
        $('#ModuleList').attr('disabled', true);
        var vRole = $("#RolList option:selected").text()
        $('#ModuleList').empty();
        $('#ModuleList').append('<option value>Seleccione un Módulo...</option>');

        $.ajax({
            url: '/UserRolesPermission/RetrieveModulesPermissions',
            type: 'POST',
            data: JSON.stringify({ roleName: vRole }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {

                for (var i = 0; i < data.length; i++) {
                    $('#ModuleList').append('<option value="' + data[i].ModuleId + '">'
                            + data[i].Name +
                            '</option>');
                }
                $('#ModuleList').attr('disabled', false);
            }
        });
    }

    var onRolesAddMenus = function () {
        $('#RolList').empty();
        $('#RolList').append('<option value>Seleccione un Rol...</option>');
        $.ajax({
            url: '/UserRolesPermission/RetrieveRolesPermissions',
            type: 'POST',
            data: JSON.stringify({ x: 1 }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {

                for (var i = 0; i < data.length; i++) {
                    $('#RolList').append('<option value="' + data[i].RoleId + '">'
                            + data[i].Name +
                            '</option>');
                }
            }
        });

        $('#addOrEditModal').modal('hide');
    }
    
    var onRolesCancel = function () {        
        $.ajax({
            url: '/UserRolesPermission/CancelSaveAsPermissions',
            type: 'POST',
            data: JSON.stringify({ x: 1 }),
            dataType: 'json',
            contentType: 'application/jon',
            success: function (data) {
                $('#addOrEditModal').modal('hide');
            }
        });        
    }

    var onConfirmationModalOn = function () {
        $('#ConfirmationModalForm').submit();
    }

    var onConfirmationModalShow = function () {
        $('#ConfirmationModal').modal('show');
    }

    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        AddModulesModal: addModulesModal,
        OnRolesChangeMenu: onRolesChangeMenu,
        OnRolesAddMenus: onRolesAddMenus,
        OnRolesCancel: onRolesCancel,
        OnConfirmationModalOn: onConfirmationModalOn,
        OnConfirmationModalShow: onConfirmationModalShow
    };
})();