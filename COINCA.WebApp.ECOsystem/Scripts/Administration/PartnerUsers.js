﻿var ECOsystem = ECOsystem || {};

ECOsystem.PartnerUsers = (function () {
    var options = {};

    
    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        //trigger click on hidden file input
        $('#btnUpload').off('click.btnUpload').on('click.btnUpload', function (e) {
            $('#fileHidden').trigger('click');
            e.stopPropagation();
        });
        fileApiUpload();
        formValidation();
        onClickNew();
        onClickAssignClient();

        $('#gridContainerClient').off('click.del', 'a[del_row]').on('click.del', 'a[del_row]', function (e) {
            $('#deleteCustomerbyPartner').find('#id').val($(this).attr('id'));            
        });

        $('#gridContainer').off('click.SetUser', 'a[set-user-row]').on('click.SetUser', 'a[set-user-row]', function (e) {
            $('#PartnerAuthorizedLoginModal').find('#PUserId').val($(this).attr('id'));
            $('#PartnerAuthorizedLoginModal').find('#PUserName').html($(this).attr('data-username'));
        });        
    }

    /* Standard Error Function for File API */
    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };

    
    /* Update progress for File API */
    var fileApiUpdateProgress = function (e) {
        if (e.lengthComputable) {
            var percentLoaded = Math.round((e.loaded / e.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                $('.percent').css('width', percentLoaded + '%').html(percentLoaded + '%');
            }
        }
    };

    
    /* File Upload implementation */
    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            $('#fileHidden').off('change.fileHidden').on('change.fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onprogress = fileApiUpdateProgress;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {
                    $('#imgPhoto').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        alert("La extensión del archivo es inválida");
                        continue;
                    }
                    // Closure to capture the file information.
                    reader.onload = (function (F) {
                        $('.percent').html('100%').css('width', '100%');
                        setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgPhoto').removeClass('imgloading');", 2000);
                        return function (e) {
                            var lsrc = e.target.result;
                            $('#imgPhoto').attr({
                                src: lsrc,
                                title: escape(F.name)
                            });
                            $('#Photo').val(lsrc);
                        };
                    })(f);
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
            });
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.');
        }
    };

    
    /* Form validation to prevent an invalid submission*/
    var formValidation = function () {
        $('#detailContainer').off('click.btnSaveSubmit', 'button[type=submit]').on('click.btnSaveSubmit', 'button[type=submit]', function (e) {
            setErrorMsj('', false);
            if(validateEmail()/* && validateCountry()*/) {
                return true;
            } else {
                return false;
            }
        });
    };

    
    /* Email validation to prevent an invalid submission*/
    var validateEmail = function () {
        if ($('#DecryptedEmail').val().length == 0) {
            return true;
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#DecryptedEmail').val().match(email)) {
            return true;
        } else {
            setErrorMsj('Correo electrónico invalido', true);
            return false;
        }
    };

    ///*validate at least 1 Country selected*/
    //var validateCountry = function () {
    //    var numberOfChecked = $('input:checkbox[data-checkbox-index]:checked').length;
    //    if (numberOfChecked < 1) {
    //        setErrorMsj('Se debe seleccionar al menos un país', true);
    //        return false;
    //    }

    //    $('input:checkbox[data-checkbox-index]').each(function() {
    //        var index = $(this).attr('data-checkbox-index');
    //        if (this.checked) {
    //            $('#PartnerUser_CountriesAccessList_' + index + '__IsCountryChecked').val('True');
    //        } else {
    //            $('#PartnerUser_CountriesAccessList_' + index + '__IsCountryChecked').val('False');
    //        }
    //    });
    //    return true;
    //}


    /* Show or hide errors messages*/
    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj').html(msj);
            $('#validationErrors').removeClass('hide');
        } else {
            $('#errorMsj').html('');
            $('#validationErrors').addClass('hide');
        }

    }

    
    /* On Success Load after call edit*/
    
    var onSuccessLoad = function () {
        initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    /* Save the information from the treeview*/
    var saveCustomersAuthorized = function () {
        
        var val = $('#ddlSelectionList').val();
        var checked = $('#treeview-checkable').treeview('getChecked');
        var userId = $('#PUserId').val();
        var list = [];

        var role = $('#ddlRoleCustomerList').val();
        if (role == null || role == "") role = $('#ddlRolePartnerList').val();        

        if (role != "") {
            $('#errorMessage').html('');

            $.each(checked, function () {
                list.push(this.id);
            })

            $.ajax({
                url: '/PartnerUsers/SaveTreeViewData',
                type: 'POST',
                data: JSON.stringify({ List: list, UserId: userId, RoleId: role, selectType: val }),
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    if (data == 1) {
                        $('#ConfirmationSaveModalAuthorization').modal('hide');
                        if (val == 0) $('#SpanTitle').html('Asignación de Socios a un Usuario')
                        $('#InformationMessageModal').modal('show');
                        location.reload();
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        $('#treeview-checkable').html('');
                        $('#PartnerAuthorizedLoginModal').modal('hide');
                    }
                }
            });
        }
        else {
            $('#errorMessage').html('Por favor seleccione un rol');
            $('#ConfirmationSaveModalAuthorization').modal('hide');
        }        
    };
      
    /* On click BtnNew*/    
    var onClickNew = function () {
        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });
    };
    
    /* On click BtnNew*/
    var onClickAssignClient = function () {
        $('#btnAssignClient').off('click.btnAssignClient').on('click.btnAssignClient', function (e) {         
            var custid = $('#ddlAssignCustomer').val();
            var UserId = $('#UserIdAddCustomer').val();
            $('#saveFormCustomerbyPartner').find('#Custid').val(custid);
            $('#saveFormCustomerbyPartner').find('#UserId').val(UserId);
            $('#saveFormCustomerbyPartner').submit();
        });
    };
    
    /* Close modal for delete after that information has been deleted*/
    var deleteModalClose = function () {
        $('#deleteCustomerbyPartner').modal('hide');       
    };
        
    /* Public methods */    
    return {
        Init: initialize,
        DeleteModalClose: deleteModalClose,
        OnSuccessLoad: onSuccessLoad,
        SaveCustomersAuthorized: saveCustomersAuthorized
    };
})();

