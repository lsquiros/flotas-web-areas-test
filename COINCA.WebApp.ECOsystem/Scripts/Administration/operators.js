﻿var ECOsystem = ECOsystem || {};
ECOsystem.Operators = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });       

        onClickNew();
        
        $('#detailContainer').off('click.currency', 'a[data-currency]').on('click.currency', 'a[data-currency]', function (e) {            
            $(this).parents(".input-group-btn").find('.btn').html($(this).get(0).text + "<span class=\"caret\"></span>");
            $("#CurrencyId").val($(this).get(0).id);
        });
    }

    /*Convert str to Number*/
    var toNum = function (num) {
        num = ("" + num).replace(/\./g, '').replace(/,/g, '.');
        return isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
    }

    /*Convert str to Float*/
    var toFloat = function (num) {
        num = ("" + num).replace(/,/g, '');
        return isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
    }

    /*format Number*/
    var formatNum = function (num) {
        return num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    /*format Currency with Symbol*/
    var formatCurrency = function (num, s) {
        return s + '' + num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    var addOrEditModalShow = function () {
        var formContent = $('#FormRate');
        formContent.off('blur.inputOnBlur', 'input[IsAmount="true"]').on('blur.inputOnBlur', 'input[IsAmount="true"]', inputOnBlur);
        formContent.find('input[IsAmount="true"]').numeric({
            allowMinus: false,
            maxDecimalPlaces: 2
        });
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    /* input OnBlur */
    var inputOnBlur = function (e) {
        inputValidation(this);
    }

    /* input OnBlur Validation */
    var inputValidation = function (obj) {
        $("#Rate").val(toNum($(obj).val()));
    }

    /* On click BtnNew*/
    var onClickNew = function () {
        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });
    };


    /* Public methods */
    return {
        Init: initialize,
        AddOrEditModalShow : addOrEditModalShow
    };
})();

