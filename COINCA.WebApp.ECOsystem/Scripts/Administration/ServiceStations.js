﻿var ECOsystem = ECOsystem || {};

ECOsystem.ServiceStations = (function () {
    var options = {};

    /* initialize function */    
    var initialize = function (opts)
    {
        $.extend(options, opts);

        $('#gridContainer').off('click.del_row', 'a[del_row]').on('click.del_row', 'a[del_row]', function (e) {
            $('#deleteForm').find('#id').val($(this).attr('id'));            
        });
    }

    var addServiceStationModal = function ()
    {   
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();               
    };

    var onSuccessLoad = function () {
        $('#addOrEditModalServiceStations').modal('show');
    };

    var onCountryChange = function () {

        var vCountryId = $("#ddlPais").val();
        $('#ddlProvincesId').empty();
        $('#ddlProvincesId').append('<option value>Seleccione una Provincia</option>');

        $.ajax({
            url: 'ServiceStations/LoadStates',
            type: 'POST',
            data: JSON.stringify({ countryId: vCountryId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    $('#ddlProvincesId').append('<option value="' + data[i].StateId + '">'
                            + data[i].Name +
                            '</option>');
                }
            }
        });
    };

    var onProvinceChange = function () {
        
        var vCountryId = $("#CountryId").val();
        var vProvinceId = $('#ddlProvincesId').val();
        $('#ddlCantonsId').empty();
        $('#ddlCantonsId').append('<option value>Seleccione un Canton</option>');

        $.ajax({
            url: 'ServiceStations/LoadCantons',
            type: 'POST',
            data: JSON.stringify({ stateId: vProvinceId, countryId: vCountryId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {

                for (var i = 0; i < data.length; i++) {
                    $('#ddlCantonsId').append('<option value="' + data[i].CountyId + '">'
                            + data[i].Name +
                            '</option>');
                }
            }
        });
    };

    var addOrEditModalClose = function ()
    {
        $('#addOrEditModalServiceStations').modal('hide');
    };

    var successSearch = function () {
        
        $('#txtSearchS').val('');
    };

    var addOrEditModalClose = function ()
    {
        $('#addOrEditModalServiceStations').modal('hide');
    }

    var deleteModalClose = function ()
    {
        $('#deleteModalStacionesServicio').modal('hide');
    }

    return {
        Init: initialize,
        AddServiceStationModal: addServiceStationModal,
        OnSuccessLoad: onSuccessLoad,
        OnCountryChange: onCountryChange,
        OnProvinceChange: onProvinceChange,
        AddOrEditModalClose: addOrEditModalClose,
        SuccessSearch: successSearch,
        AddOrEditModalClose: addOrEditModalClose,
        DeleteModalClose: deleteModalClose
       };
})();