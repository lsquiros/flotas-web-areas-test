﻿var ECOsystem = ECOsystem || {};
ECOsystem.VehicleCategories = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        $('#Liters').numeric({
            allowMinus: false,
            allowThouSep: false,
            allowDecSep: false,
            maxDigits: 4,
            max: 5000,
            min: 0,
        });
        $('#CylinderCapacity').numeric({
            allowMinus: false,
            allowThouSep: false,
            allowDecSep: false,
            maxDigits: 7,
        });
        resetAndInitCustomControls();

        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            resetAndInitCustomControls();
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });

        $('#detailContainer').off('click.checkFuel', 'input[data-checkbox-index]').on('click.checkFuel', 'input[data-checkbox-index]', function (e) {
            $('#FuelsList_' + $(this).attr('data-checkbox-index') + '__IsFuelChecked').val(this.checked ? 'True' : 'False');
        });

        //Init format currency
        initCurrency();
    };

    var addOrEditModalShow = function () {
        resetAndInitCustomControls();
        $("#addOrEditForm").find('select').not("[custom='true']").select2();
        $('#addOrEditModal').modal('show');
        $("#MaximumRPM").numeric("integer");
        $("#MaximumSpeed").numeric("integer");
        $("#Liters").numeric("integer");
        $("#Weight").numeric("integer");
        $("#CylinderCapacity").numeric("integer");
        $("#DefaultPerformance").numeric("integer");
        $("#Year").numeric({
            allowMinus: false,
            allowThouSep: false,
            allowDecSep: false,
            maxDigits: 4,
        });
    };

    /* Reset And Init Custom Select*/
    var resetAndInitCustomControls = function () {
        $('#Icon').select2('data', {});
        $('#Icon').select2({
            formatResult: iconSelectFormat,
            formatSelection: iconSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

    };


    /* driver Select Format*/
    var iconSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row" >' +
                '<div class="col-md-12" ><img style="width: 24px; height: 24px;" src="Content/Images/Vehicles/' + json.image + '"/></div>' +
                '</div>';
    };

    var initCurrency = function () {
        try {
            //$('#page-wrap').off('blur.currencyOnBlur', 'input[data-currency]').on('blur.currencyOnBlur', 'input[data-currency]', function (e) {
            //    var obj = $(this);
            //    obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));
            //});
            //$('#page-wrap').find('input[data-currency]').numeric({
            //    allowMinus: false,
            //    maxDecimalPlaces: 2
            //});

            $("input[currency]").each(function () {
                var obj = $(this);

                var valueWithFormat = accounting.formatNumber(parseFloat(obj.val()), {
                    precision: 2,
                    thousand: ""
                });

                obj.val(valueWithFormat);
            });

            $("input[currency]").change(function () {
                
                $(this).val($(this).val().replace(/\,/g, '.'));
                var obj = $(this);

                var valueWithFormat = accounting.formatNumber(parseFloat(obj.val()), {
                                            precision: 2,
                                            thousand: ""
                                        });

                //obj.val(ECOsystem.Utilities.FormatCurrency());
                obj.val(valueWithFormat);
            });

            $('#detailContainer').find('input[currency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });

        } catch (e) {

        }
    }

    /* Public methods */
    return {
        AddOrEditModalShow: addOrEditModalShow,
        Init: initialize,
        InitCurrency: initCurrency
    };
})();

