﻿var ECOsystem = ECOsystem || {};
ECOsystem.PreventiveMaintenanceCatalog = (function () {
    var options = {};
    var hasChanges = 0; // Verifica si hay cambios en el listado de costos de un manteminiento

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        resetAndInitCustomControls();
        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            
            $("#FrequencyTypeId").prop("readonly", false);
            $("#btnCancelPMC").prop('disabled', false);
            $("#btnSavePMC").prop('disabled', false);
            resetAndInitCustomControls();
        });
        $('#detGridContainer').off('click.del', 'a[del_row_detail]').on('click.del', 'a[del_row_detail]', function (e) {
            $('#deleteCostModal').find('#id').val($(this).attr('id'));
            $('#deleteCostModal').modal('show');
            return false;
        });
        $('#btnDeleteCost').off('click.btnDeleteCost').on('click.btnDeleteCost', function (e) {
            var id = $('#deleteCostModal').find('#id').val();
            var cost = $('#Cost-' + id).val();
            if ($('#Cost-' + id).attr("name").replace('Cost-', '') == '0') {
                var tr = $($('#Cost-' + id)).closest("tr").hide();
                tr.fadeOut(400, function () { tr.remove(); });
                $('#deleteCostModal').modal('hide');
                sumatory();
            }
            else {
                var postData = { id: id };
                $.ajax({
                    type: "POST",
                    url: ECOsystem.Utilities.GetUrl() + '/PreventiveMaintenanceCatalog/DeleteCost',
                    data: postData,
                    success: function (data) {
                        var tr = $($('#Cost-' + id)).closest("tr").hide();
                        tr.fadeOut(400, function () { tr.remove(); });
                        $('#deleteCostModal').modal('hide');
                        sumatory();
                        hasChanges = 1;
                    },
                    dataType: "json",
                    traditional: true
                });
            }
        });

        
    };

    $("#btnSaveCost").click(function () {
        var valid = true;
        var count = 0;
        var list = new Array();
        $('a[del_row_detail]').each(
        function (index) {
            if (parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val())) <= 0) { valid = false; }
            if ($.trim($('#Name-' + $(this).attr('id')).val()) == '') { valid = false; }
            var PreventiveMaintenanceCatalogId = $('#costForm').find('#id').val();
            var PreventiveMaintenanceCostId = $('#Cost-' + $(this).attr('id')).attr("name").replace('Cost-', '');
            var Description = $.trim($('#Name-' + $(this).attr('id')).val());
            var Cost = 0;
            if ($('#Cost-' + $(this).attr('id')).hasClass("formatted") == true) {
                Cost = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()));
            }
            else {
                Cost = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()) / 100);
            }
            list.push("{ PreventiveMaintenanceCatalogId:" + parseInt(PreventiveMaintenanceCatalogId) + ", PreventiveMaintenanceCostId:" + parseInt(PreventiveMaintenanceCostId) + ", Description:'" + Description + "', Cost:" + parseFloat(Cost) + " }");
            count = count + 1;
        })
        if (valid == false || count == 0) {
           // $('#validateCostModal').modal('show');
        }
        else {
            var postData = { list: list };
            $.ajax({
                type: "POST",
                url: ECOsystem.Utilities.GetUrl() + '/PreventiveMaintenanceCatalog/SaveCostList',
                data: postData,
                success: function (data) {
                    $('#costModal').modal('hide');
                    submitForm();
                },
                dataType: "json",
                traditional: true
            });
        }
    });


    $("#btnAddCost").click(function () {
        var id = 0;
        $('a[del_row_detail]').each(
            function (index) {
                var newId = parseFloat($(this).attr('id'));
                if (newId > id) {
                    id = newId;
                }
            }
        )
        id = id + 1;
        var row = '';
        row = row + '<tr class="grid-row ">';
        row = row + '<td class="grid-cell grid-col-hide data-Id" data-name="PreventiveMaintenanceCatalogId">';
        row = row + $('#costForm').find('#id').val();
        row = row + '</td>';
        row = row + '<td class="grid-cell grid-col-hide data-Id" data-name="PreventiveMaintenanceCostId">';
        row = row + id;
        row = row + '</td>';
        row = row + '<td class="grid-cell grid-col-name" data-name="Description">';
        row = row + '<div>';
        row = row + '<input type="text" class="form-control nav-justified" value="" id="Name-' + id + '" name="Name-0" data-currency="true">';
        row = row + '</div>';
        row = row + '</td>';
        row = row + '<td class="grid-cell grid-col-center" data-name="Cost">';
        row = row + '<div>';
        row = row + '<div class="input-group">';
        row = row + '<span2 class="input-group-addon">';
        row = row + $('#CurrencySymbol').attr('value');
        row = row + '</span2>';
        row = row + '<input type="text" onblur="ECOsystem.PreventiveMaintenanceCatalog.Sumatory()" class="form-control text-right num-cost" value="0.00" id="Cost-' + id + '" name="Cost-0" data-currency="true">';
        row = row + '</div>';
        row = row + '</div>';
        row = row + '</td>';
        row = row + '<td class="grid-cell grid-col-btn" data-name="">';
        row = row + '<span class="toolBar1">';
        row = row + '<a href="#" class="delete-color" data-toggle="modal" data-target="#deleteCostModal" id="' + id + '" title="Eliminar" del_row_detail=""><i class="mdi mdi-32px mdi-close-box"></i></a>';
        row = row + '</span>';
        row = row + '</td>';
        row = row + '</tr>';
        $('#detGridContainer table').append(row);
        sumatory();
    });

    var resetAndInitCustomControls = function () {
        $("#FrequencyKm").numeric("integer");
        $("#AlertBeforeKm").numeric("integer");
        $("#FrequencyMonth").numeric("integer");
        $("#AlertBeforeMonth").numeric("integer");

        //var select1 = $("#FrequencyTypeId").select2().data('select2');
        //select1.onSelect = (function (fn) {
        //    return function (data, opts) {
        //        var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectChange(data); return fn.apply(this, arguments); }
        //    }
        //})(select1.onSelect);

        $('#datetimepicker1').datepicker({ language: 'es', minDate: new Date(), format: 'dd/mm/yyyy', autoclose: true, todayHighlight: true });
    }

    var initDetail = function () {
        sumatory();
    }

    var sumatory = function() {
        var total = 0;
        $('a[del_row_detail]').each(
            function (index) {
                var num = 0;
                try
                {
                    if ($('#Cost-' + $(this).attr('id')).hasClass("formatted") == true) {
                        num = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()));
                    }
                    else {
                        num = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()) / 100);
                        $('#Cost-' + $(this).attr('id')).addClass("formatted");
                    }
                }
                catch (err)
                {
                    num = 0;                
                }
                $('#Cost-' + $(this).attr('id')).val(ECOsystem.Utilities.FormatNum(num));
                total = total + num;
                $('#Cost-total').val(ECOsystem.Utilities.FormatNum(total));
            }
        )
    }

    //Refresh catalog list if changes data
    var refreshChanges = function () {
        submitForm();
    };

    var submitForm = function () {
        $('#addOrEditForm').submit();
    };

    var onSelectChange = function (obj) {
        changeDiv(obj.id, true);
    };

    var addOrEditModalShow = function () {
        //changeDiv($('#FrequencyTypeId').val(), false);
        //$("#FrequencyTypeId").prop("readonly", true);
        //$('#DateMaintenancePreventive').val($('#Frequency').val());
        //$('#DateMaintenancePreventiveStr').val($('#Frequency').val());
        resetAndInitCustomControls();        
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    var costModalShow = function () {
        $('#costModal').modal('show');
    };

    $('#gridContainer').off('click.cost', 'a[cost_row]').on('click.cost', 'a[cost_row]', function (e) {
        $('#costForm').find('#id').val($(this).attr('id'));
        $('#costForm').submit();
    });

    /* Public methods */
    return {
        Init: initialize,
        AddOrEditModalShow: addOrEditModalShow,
        CostModalShow: costModalShow,
        InitDetail: initDetail,
        Sumatory: sumatory,
        RefreshChanges:refreshChanges
    };
})();


