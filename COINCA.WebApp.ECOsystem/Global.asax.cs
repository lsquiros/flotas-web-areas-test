﻿/************************************************************************************************************
*  File    : Global.asax.cs
*  Summary : Global configuration
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;

namespace ECOsystem
{

    /// <summary>
    /// Mvc Application class
    /// </summary>
    public class MvcApplication : HttpApplication
    {
        /// <summary>
        /// Application Start method
        /// </summary>
        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();
            var customEngine = new RazorViewEngine();
            customEngine.ViewLocationCache = new TwoLevelViewCache(customEngine.ViewLocationCache);
            ViewEngines.Engines.Add(customEngine);

            AreaRegistration.RegisterAllAreas();
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredIfAttribute), typeof(RequiredAttributeAdapter));
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AttributeRoutingHttpConfig.RegisterRoutes(GlobalConfiguration.Configuration.Routes);        
        }

        /// <summary>
        /// Session Expired on AJAX Requests
        /// </summary>
        protected void Application_EndRequest()
        {
            var context = new HttpContextWrapper(Context);
            // If we're an ajax request, and doing a 302, then we actually need to do a 401
            if (Context.Response.StatusCode == 302 && context.Request.IsAjaxRequest())
            {
                if (context.Response.RedirectLocation != "/Account/UnauthorizedPartial")
                {
                    Context.Response.Clear();
                    Context.Response.StatusCode = 401;
                }                
            }
        }
    }
}
