﻿using ECOsystem.Business.Core;
using ECOsystem.Models.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace ECOsystem.Controllers
{
#pragma warning disable CS1591
    public class CustomerSurveyController : Controller
    {
        public ActionResult LoadSurvey(string obj)
        {
            using (var bus = new PartnerSurveyBusiness())
            {
                var survey = JsonConvert.DeserializeObject<PartnerSurveyShow>(obj);
                var model = bus.PartnerSurveyRetrieve(null, Utilities.Session.GetCustomerId(), survey.SurveyId).FirstOrDefault();
                model.Elements = survey.Elements;
                Session["CustomerSurveyOnLine"] = model;
                return PartialView("~/Views/CustomerSurvey/_Survey.cshtml", model);
            }
        }

        public ActionResult ShowQuestions()
        {
            var model = (PartnerSurvey)Session["CustomerSurveyOnLine"];
            var questions = model.Questions;
            var list = new List<string>();
          
            foreach (var item in questions)
            {               
                list.Add(RenderViewToString("~/Views/CustomerSurvey/_QuestionPartial.cshtml", item));
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveSurvey(string answers)
        {
            try
            {
                var model = (PartnerSurvey)Session["CustomerSurveyOnLine"];
                var list = JsonConvert.DeserializeObject<List<PartnerSurveyAnswer>>(answers);                
                using (var bus = new PartnerSurveyBusiness())
                {
                    bus.CustomerAnswersAddOrEdit(model, list, Utilities.Session.GetCustomerId(), Utilities.Session.GetUserInfo().UserId);
                }
                return Json("finish", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        string RenderViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
#pragma warning restore CS1591
}