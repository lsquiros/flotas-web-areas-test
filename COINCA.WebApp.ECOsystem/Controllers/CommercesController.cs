﻿using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Models.Identity;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ECOsystem.Controllers.Administration
{
    public class CommercesController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var bus = new CommercesBusiness())
                {
                    var list = bus.CommercesRetrieve();
                    Session["CommercesList"] = list;

                    return View(new CommercesBase()
                    {
                        List = list
                    });
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LoadCommerces(int? id)
        {
            try
            {
                if (id == null)
                {
                    return PartialView("_partials/_Detail", new Commerces());
                }
                else
                {
                    using (var bus = new CommercesBusiness())
                    {
                        return PartialView("_partials/_Detail", bus.CommercesRetrieve(id).FirstOrDefault());
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(1);
            }
        }

        public ActionResult AddOrEditCommerces(Commerces model)
        {
            try
            {
                using (var bus = new CommercesBusiness())
                {
                    bus.CommercesAddOrEdit(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format((model.Id == null ? "Agrega" : "Edita") + " Comercio con la siguiente información = {0}, Cliente = {1}, Id de Cliente = {2}",
                                                       new JavaScriptSerializer().Serialize(model), ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));

                    if (model.FromMaps != null && (bool)model.FromMaps) return Json("success", JsonRequestBehavior.AllowGet);

                    var list = bus.CommercesRetrieve();
                    Session["CommercesList"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                var message = "Error al agregar o editar comercio.";
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format(message + " Detalle del error = {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = message, TechnicalError = e.Message });
            }
        }

        public ActionResult DeleteCommerces(int id)
        {
            try
            {
                using (var bus = new CommercesBusiness())
                {
                    bus.CommercesDelete(id);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Elimina Comercio, Id del comercio = {0}, Cliente = {1}, Id de Cliente = {2}", id, ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    var list = bus.CommercesRetrieve();
                    Session["CommercesList"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                var message = "Error al Eliminar comercio.";
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format(message + " Detalle del error = {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = message, TechnicalError = e.Message });
            }
        }

        public ActionResult SearchCommerces(string key)
        {
            try
            {
                if (string.IsNullOrEmpty(key))
                {
                    return PartialView("_partials/_Grid", (List<Commerces>)Session["CommercesList"]);
                }
                else
                {
                    return PartialView("_partials/_Grid", ((List<Commerces>)Session["CommercesList"]).Where(x => x.Name.ToUpper().Contains(key.ToUpper())));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        //Get data to download
        public ActionResult GetDataToDownload()
        {
            try
            {
                using (var bus = new CommercesBusiness())
                {
                    var list = (List<Commerces>)Session["CommercesList"];
                    Session["Reporte"] = bus.ReportData(list);

                    if (list.Count == 0)
                        Response.StatusCode = 204; //No content

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        //Download File
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Comercios");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "CommercesReport", this.ToString().Split('.')[2], "Reporte de Comercios");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                ViewBag.ErrorReport = 1;

                var model = new CommercesBase()
                {
                    List = (List<Commerces>)Session["CommercesList"]
                };
                return View("Index", model);
            }
        }
    }
}