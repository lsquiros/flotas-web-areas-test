﻿using System.Web.Mvc;

namespace ECOsystem.Controllers
{
    public class DummyController : Controller
    {
        // GET: Dummy
        public ActionResult Index()
        {
            return View();
        }
    }
}