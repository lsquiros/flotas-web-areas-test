﻿/************************************************************************************************************
*  File    : HomeController.cs
*  Summary : Home Controller Actions
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Linq;
using System.Web.Mvc;
using ECOsystem.Business.Core;
using ECOsystem.Business.Home;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Home;
using ECOsystem.Models.Identity;
using ECOsystem.Utilities;
using ModuleConfig;



namespace ECOsystem.Controllers.Home
{
    /// <summary>
    /// Home Controller Class to manage Home View events
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                ViewBag.login = false;
                var user = Utilities.Session.GetUserInfo();
                var productInfo = Utilities.Session.GetProductInfo();
                var currentProfile = Utilities.Session.GetProfileByRole(user.RoleId);
                var customerId = user.IsCustomerUser == true ? user.CustomerId : Utilities.Session.GetCustomerId();
                customerId = customerId ?? 0;

                if (currentProfile == "VPOS_USER")
                {
                    return Redirect(string.Format("{0}{1}", ModuleURI.GetURI(ModuleTypeURI.ModuleCoinca), "/vpos/AdminPreauthorized"));
                }

                if (user.IsServiceStationUser)
                {
                    return Redirect(string.Format("{0}", ModuleURI.GetURI(ModuleTypeURI.ModuleServiceStations)));
                }

                if (currentProfile == "SUPER_ADMIN" || currentProfile == "SUPER_USER")
                {
                    #region Super - LineVita Configuration
                    if (productInfo == ProductInfo.LineVitaECO) // 3 Line Vita
                    {
                        return RedirectToAction("Index", "Partners");
                    }
                    #endregion

                    return RedirectToAction("SAIndex", "Home");
                }

                if (currentProfile == "PARTNER_ADMIN" || currentProfile == "PARTNER_USER")
                {
                    #region Partner - LineVita Configuration
                    if (productInfo == ProductInfo.LineVitaECO) // 3 Line Vita
                    {
                        return RedirectToAction("IndexControl", "Account", new { title = "Administración", header = "17" });
                    }
                    #endregion

                    return RedirectToAction("PAIndex", "Home");
                }

                if (currentProfile == "INSURANCE_ADMIN" || currentProfile == "INSURANCE_USER")
                {
                    return RedirectToAction("IAIndex", "Home");
                }

                #region Customer - LineVita Configuration
                if (productInfo == ProductInfo.LineVitaECO) // 3 Line Vita
                {
                    if (currentProfile == "LINEVITA_ADMIN")
                    {
                        Response.Redirect(string.Format("{0}{1}", ModuleURI.GetURI(ModuleTypeURI.ModuleEfficiency), "/RealTime/Index/"));
                    }
                }
                #endregion

                ViewBag.Inicio = "active";

                // Default View
                var viewModel = new ViewModel();

                using (PartnerNewsBusiness partnerNews = new PartnerNewsBusiness())
                {
                    viewModel.rssModels = partnerNews.RetrieveActiveNewsByPartners(Utilities.Session.GetPartnerId());
                }

                using (var business = new DashboardModulesBusiness())
                {
                    viewModel.DataIndicators = business.RetrieveDashboardIndicator((int)customerId);
                }

                return View(viewModel);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// SAIndex
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "SUPER_ADMIN, SUPER_USER")]
        [EcoAuthorize]
        public ActionResult SAIndex()
        {
            try
            {
                ViewBag.Inicio = "active";
                var homeBusiness = new HomeBusiness();
                var saIndicators = homeBusiness.RetrieveSAIndicators().FirstOrDefault();
                return View(saIndicators);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// SAIndex
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "PARTNER_ADMIN, PARTNER_USER")]
        [EcoAuthorize]
        public ActionResult PAIndex()
        {
            try
            {
                ViewBag.Inicio = "active";

                using (var business = new HomeBusiness())
                {
                    //return View(business.RetrievePAIndicators(Utilities.Session.GetPartnerId()).FirstOrDefault());
                    return View(business.GetDashboardPartnerIndicatos().FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// SAIndex
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "INSURANCE_ADMIN, INSURANCE_USER")]
        [EcoAuthorize]
        public ActionResult IAIndex()
        {
            try
            {
                ViewBag.Inicio = "active";
                // Default View
                var viewModel = new ViewModel();

                using (PartnerNewsBusiness partnerNews = new PartnerNewsBusiness())
                {
                    viewModel.rssModels = partnerNews.RetrieveActiveNewsByPartners(Utilities.Session.GetPartnerId());

                    //Utilities.Session.
                }

                return View(viewModel);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// CombustibleDistribucion
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult CombustibleDistribucion()
        {
            try
            {
                ViewBag.Combustible = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// CombustibleDistribucionPorVehiculo
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult CombustibleDistribucionPorVehiculo()
        {
            try
            {
                ViewBag.Combustible = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// CombustibleAsignacionesYRecortes
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult CombustibleAsignacionesYRecortes()
        {
            try
            {
                ViewBag.Combustible = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// CombustibleAcreditacionesFuturas
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult CombustibleAcreditacionesFuturas()
        {
            try
            {
                ViewBag.Combustible = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// OtrosCreditosDistribucion
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult OtrosCreditosDistribucion()
        {
            try
            {
                ViewBag.OtrosCreditos = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// OtrosCreditosAsignacionesYRecortes
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult OtrosCreditosAsignacionesYRecortes()
        {
            try
            {
                ViewBag.OtrosCreditos = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// OtrosCreditosAcreditacionesFuturas
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult OtrosCreditosAcreditacionesFuturas()
        {
            try
            {
                ViewBag.OtrosCreditos = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// AceiteDistribucion
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult AceiteDistribucion()
        {
            try
            {
                ViewBag.Aceite = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// AceiteAsignacionesYRecortes
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult AceiteAsignacionesYRecortes()
        {
            try
            {
                ViewBag.Aceite = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// AceiteAcreditacionesFuturas
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult AceiteAcreditacionesFuturas()
        {
            try
            {
                ViewBag.Aceite = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// FlotaDatos
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult FlotaDatos()
        {
            try
            {
                ViewBag.Flota = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// FlotaAjusteOdometros
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize(Roles = "CUSTOMER_CHANGEODOMETER")]
        public ActionResult FlotaAjusteOdometros()
        {
            try
            {
                ViewBag.Flota = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// LocalizacionGeocercas
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult LocalizacionGeocercas()
        {
            try
            {
                ViewBag.Localizacion = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// LocalizacionCreacionRutas
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult LocalizacionCreacionRutas()
        {
            try
            {
                ViewBag.Localizacion = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// LocalizacionDistribucionRutas
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult LocalizacionDistribucionRutas()
        {
            try
            {
                ViewBag.Localizacion = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// MantenimientoPreventivo
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult MantenimientoPreventivo()
        {
            try
            {
                ViewBag.Mantenimiento = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// MantenimientoCorrectivo
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult MantenimientoCorrectivo()
        {
            try
            {
                ViewBag.Mantenimiento = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// MantenimientoPredictivo
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult MantenimientoPredictivo()
        {
            try
            {
                ViewBag.Mantenimiento = "active";
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Se crea este metodo anonimo para que al seleccionar un cliente en el Login se pueda redireccionar al Index
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult IndexCustomer()
        {
            try
            {
                return RedirectToAction("Index", "Home");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manual
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [Authorize]
        public ActionResult Manual()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditShowNews(bool ShowNews)
        {
            try
            {
                using (var bus = new HomeBusiness())
                {
                    ShowNews = ShowNews ? false : true;
                    var user = ECOsystem.Utilities.Session.GetUserInfo();

                    bus.EditShowNews(ShowNews, user.UserId);
                    Session["ShowNews"] = ShowNews;
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}