﻿using ECOsystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem.Business;
using ECOsystem.Models.Account;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Identity;
using System.Web.Script.Serialization;




namespace ECOsystem.Controllers.Administration
{
    public class ProgramSendReportsController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var bus = new ProgramSendReportsBusiness())
                {
                    var list = bus.ProgramSendReportsRetrieve();

                    var model = new ProgramSendReportsBase()
                    {
                        List = list,
                        Menus = new List<AccountMenus>()
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetData(int? Id)
        {
            try
            {
                using (var bus = new ProgramSendReportsBusiness())
                {
                    return Json(bus.ReturnDataToView(Id), JsonRequestBehavior.AllowGet);
                    //return PartialView("_partials/_Parameters", bus.ReturnDataToView(Id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SendProgramReportAddOrEdit(List<ProgramSendReports> List)
        {
            try
            {
                using (var bus = new ProgramSendReportsBusiness())
                {
                    bus.ProgramSendReportAddOrEdit(List);
                    var model = bus.ProgramSendReportsRetrieve().Where(x => x.ReportId == List.FirstOrDefault().ReportId).GroupBy(x => new { x.Elapse, x.Days }).Select(group => group.First()).ToList();
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificacion de los reportes programdos. Detalles: {0}", new JavaScriptSerializer().Serialize(List)));
                    //return Json("Success", JsonRequestBehavior.AllowGet);
                    return PartialView("_partials/_GridList", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al modificar los reportes programados. Detalle: {0}", e.Message));
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SendTestEmail(int? ReportId)
        {
            try
            {
                using (var bus = new ProgramSendReportsBusiness())
                {
                    bus.SendTestProgramEmail(ReportId);
                }
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Envio Reporte de prueba. ReportId: {0}", ReportId));
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Envio Reporte de prueba. ReportId: {0}", ReportId));
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Delete a specific programreport
        /// </summary>
        /// <param name="ReportId"></param>
        /// <returns></returns>
        public ActionResult DeleteProgramReport(bool Validation, string Id, int? ReportId)
        {
            try
            {
                using (var bus = new ProgramSendReportsBusiness())
                {
                    if (Id.Split('_').Count() > 1)
                    {
                        Id = Id.Split('_')[1];
                    }
                    bus.DeleteProgramReport(ReportId, Convert.ToInt32(Id));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se eliminó el reporte. ReportId: {0} | Id: {1}", ReportId, Id));
                    if (Validation == false)
                    {
                        var model = bus.ProgramSendReportsRetrieve().Where(x => x.ReportId == ReportId).GroupBy(x => new { x.Elapse, x.Days }).Select(group => group.First()).ToList();
                        return PartialView("_partials/_GridList", model);
                    }
                    else
                    {
                        return Json("OK");
                    }

                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Eliminación de reporte Falló. ReportId: {0}", Id));
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}