﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ECOsystem.Business.Administration;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ModuleConfig;



namespace ECOsystem.Controllers.Account
{
    /// <summary>
    /// 
    /// </summary>
    public class ASPMenusController : Controller
    {

        /// <summary>
        /// Load Menus
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> LoadMenus(int header)
        {
            try
            {
                var loadMenus = Session["LoadMenusCustomReport"] != null ? (bool)Session["LoadMenusCustomReport"] : false;
                if (Session["HeaderType"] == null || header != (int)Session["HeaderType"] || loadMenus)
                {
                    var user = Utilities.Session.GetUserInfo();
                    var menus = ASPMenusBusiness.RetrieveMenus(header, null, user.RoleId).ToList();
                    Session["ASPMenus"] = menus;

                    var modal = menus.FirstOrDefault();
                    Session["HeaderType"] = modal != null ? modal.Header : 0;
                    Session["LoadMenusCustomReport"] = null;
                }

                return PartialView("~/Views/Shared/_Menu.cshtml", (List<AccountMenus>)Session["ASPMenus"]);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Set Menu Active
        /// </summary>
        /// <param name="MenuId"></param>
        public void SetMenuActive(int MenuId)
        {
            try
            {
                Session["parent"] = MenuId;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
            }
        }

        /// <summary>
        /// Get Header
        /// </summary>
        /// <returns></returns>
        public static int GetHeader()
        {
            try
            {
                var user = Utilities.Session.GetUserInfo();

                switch (Utilities.Session.GetProfileByRole(user.RoleId))
                {
                    case "CUSTOMER_ADMIN":
                        return 8;
                    case "PARTNER_ADMIN":
                        return 17;
                    case "SUPER_ADMIN":
                        return 9;
                }

                return 0;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return 0;
            }
        }

        /// <summary>
        /// Get Alarms
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Alarms()
        {
            try
            {
                using (var bus = new AlertsBusiness())
                {
                    int cCount = 0;

                    var model = new AlarmsBase { List = new List<Alarms>() };
                    model.List = bus.AlertsCountRetrieve();

                    foreach (var item in model.List)
                    {
                        item.URL = item.AlarmId == 1 ? string.Format("{0}/{1}?Alert=true", ModuleURI.GetURI(ModuleTypeURI.ModuleControl), item.Controller)
                                                     : item.AlarmId == 2 ? string.Format("{0}/{1}?Alert=true", ModuleURI.GetURI(ModuleTypeURI.ModuleAdministration), item.Controller)
                                                     : string.Format("{0}/{1}", ModuleURI.GetURI(ModuleTypeURI.ModuleControl), item.Controller);
                        cCount = cCount + item.Count;
                    }

                    model.Data = new Alarms
                    {
                        AlarmTitle = "Alarmas existentes en el sistema",
                        Count = cCount,
                        Name = "Alarmas"
                    };

                    return PartialView(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}