﻿using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;


using System;
using System.Collections.Generic;

using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ECOsystem.Controllers.Account
{
    /// <summary>
    /// 
    /// </summary>     
    public class UserRolesPermissionController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string newRoleName)
        {
            try
            {
                using (var bus = new UserRolesPermissionsBusiness())
                {
                    var model = new AccountMenusBase();
                    if (string.IsNullOrEmpty(newRoleName))
                    {
                        model = new AccountMenusBase
                        {
                            Data = new AccountMenus { List = Enumerable.Empty<SelectListItem>() },
                            List = Business.Administration.ASPMenusBusiness.RetrieveMenus(null, -1),
                            RoleBase = new UserRolesBase()
                        };
                    }
                    else
                    {
                        model = new AccountMenusBase
                        {
                            Data = new AccountMenus { List = Enumerable.Empty<SelectListItem>(), RoleId = bus.RetrieveLastRoleCreated(newRoleName) },
                            List = Business.Administration.ASPMenusBusiness.RetrieveMenus(null, -1),
                            RoleBase = new UserRolesBase()
                        };
                    }
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public PartialViewResult SearchPermissions(string key)
        {
            try
            {
                return PartialView("_partials/_Grid", UserRolesPermissionsBusiness.UserPermissionsRetrieve(null, key));
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "UserRolesPermission")
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>        
        [HttpPost]
        public PartialViewResult SearchMenu(string key)
        {
            try
            {
                var tree = Business.Administration.ASPMenusBusiness.RetrieveMenus(Convert.ToInt32(key), null);

                return PartialView("_partials/_Tree", tree);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ASPMenus")
                });

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult SearchMenuParentsJson()
        {
            try
            {
                var menus = Business.Administration.ASPMenusBusiness.RetrieveMenusParents(null);
                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = menus }

                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns> </returns>
        public ActionResult Roles()
        {
            try
            {
                var model = new UserRolesBase
                {
                    Data = new UsersRoles() { CustomerList = new List<Models.Core.Customers>() },
                    List = UserRolesPermissionsBusiness.RolesRetrieve(null, null, null, null),
                    Menus = new List<AccountMenus>()
                };
                Session["CoincaRoles"] = model.List;
                return View(model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>        
        public PartialViewResult SearchRole(string key)
        {
            try
            {
                var List = (IEnumerable<UsersRoles>)Session["CoincaRoles"];

                List = List.Where(q => q.Name.ToUpperInvariant().Contains(key.ToUpperInvariant()));
                return PartialView("_partials/_GridRoles", List);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Roles", "UserRolesPermission")
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PartialViewResult LoadRoles(string id)
        {
            try
            {
                if (id == "-1") return PartialView("_partials/_DetailRoles", new UsersRoles());
                using (var bus = new UserRolesPermissionsBusiness())
                {
                    var model = UserRolesPermissionsBusiness.RolesRetrieve(id, null, null, null).FirstOrDefault();

                    if (Utilities.Session.GetUserInfo().IsPartnerUser)
                    {
                        model.CustomerList = bus.RoleByCustomerRetrieve(model.RoleId);
                    }
                    return PartialView("_partials/_DetailRoles", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Roles", "UserRolesPermission")
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Modules()
        {
            try
            {
                using (var business = new UserRolesPermissionsBusiness())
                {
                    var user = Utilities.Session.GetUserInfo();

                    var model = new ModulesBase
                    {
                        Data = new Modules(),
                        List = business.RetrieveModules(null, null, user.RoleId),
                        Menus = new List<AccountMenus>()
                    };

                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public PartialViewResult SearchModules(string key)
        {
            try
            {
                using (var business = new UserRolesPermissionsBusiness())
                {
                    var user = Utilities.Session.GetUserInfo();

                    var model = business.RetrieveModules(null, key, user.RoleId);

                    return PartialView("_partials/_GridModules", model);
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Modules", "UserRolesPermission")
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PartialViewResult LoadModules(int id)
        {
            if (id < 0)
            {
                return PartialView("_partials/_DetailModules", new Modules());
            }
            try
            {
                var user = Utilities.Session.GetUserInfo();

                using (var business = new UserRolesPermissionsBusiness())
                {
                    return PartialView("_partials/_DetailModules", business.RetrieveModules(id, null, user.RoleId).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Modules", "UserRolesPermission")
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PartialViewResult DeleteModules(int id)
        {
            try
            {
                var user = Utilities.Session.GetUserInfo();

                using (var business = new UserRolesPermissionsBusiness())
                {
                    business.DeleteModules(id, user.RoleId);

                    #region Event log
                    new EventLogBusiness().AddLogEvent(LogState.ERROR, "Eliminacion de un modulo", true);
                    #endregion

                    return PartialView("_partials/_GridModules", business.RetrieveModules(null, null, user.RoleId));
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Modules", "UserRolesPermission")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Modules", "UserRolesPermission")
                    });
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public PartialViewResult AddOrEditModules(Modules model)
        {
            try
            {
                var user = Utilities.Session.GetUserInfo();

                using (var business = new UserRolesPermissionsBusiness())
                {
                    business.AddOrEditModules(model, user.RoleId);

                    #region Event log
                    new EventLogBusiness().AddLogEvent(LogState.ERROR, "Creacion de modulos", true);
                    #endregion

                    return PartialView("_partials/_GridModules", business.RetrieveModules(null, null, user.RoleId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("ERROR EXIST_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Usuario ya existe",
                        TechnicalError = "El usuario con el correo especificado ya existe en el sistema.",
                        ReturnUlr = Url.Action("Modules", "UserRolesPermission")
                    });

                }
                if (e.Message.Contains("ERROR UPDT_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Error de Actualización",
                        TechnicalError = "El usuario que se desea actualizar presenta inconsistencias, por favor intente de nuevo el proceso de actualización.",
                        ReturnUlr = Url.Action("Modules", "UserRolesPermission")
                    });

                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Modules", "UserRolesPermission")
                });
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public PartialViewResult AddOrEditRoles(UsersRoles model)
        {
            try
            {
                using (var business = new UserRolesPermissionsBusiness())
                {
                    business.AddOrEditRoles(model);
                }

                #region Event log
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Creacion de roles, datos: {0}", new JavaScriptSerializer().Serialize(model)), true);
                #endregion

                var List = UserRolesPermissionsBusiness.RolesRetrieve(null, null, null, null);

                return PartialView("_partials/_GridRoles", List);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("ERROR EXIST_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Usuario ya existe",
                        TechnicalError = "El usuario con el correo especificado ya existe en el sistema.",
                        ReturnUlr = Url.Action("Roles", "UserRolesPermission")
                    });

                }
                if (e.Message.Contains("ERROR UPDT_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Error de Actualización",
                        TechnicalError = "El usuario que se desea actualizar presenta inconsistencias, por favor intente de nuevo el proceso de actualización.",
                        ReturnUlr = Url.Action("Roles", "UserRolesPermission")
                    });

                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Roles", "UserRolesPermission")
                });
            }

        }

        /// <summary>
        /// DeleteRoles
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PartialViewResult DeleteRoles(string id)
        {
            try
            {
                using (var business = new UserRolesPermissionsBusiness())
                {
                    business.DeleteRoles(id);
                    #region Event log
                    new EventLogBusiness().AddLogEvent(LogState.ERROR, "Eliminacion de roles", true);
                    #endregion

                    return PartialView("_partials/_GridRoles", UserRolesPermissionsBusiness.RolesRetrieve(null, null, null, null));
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Roles", "UserRolesPermission")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Roles", "UserRolesPermission")
                    });
                }
            }

        }

        /// <summary>
        /// RetrieveRoles
        /// </summary>
        /// <returns></returns>
        public static SelectList RetrieveRoles()
        {
            try
            {
                var user = Utilities.Session.GetUserInfo();
                var dictionary = new Dictionary<string, string>();

                using (var business = new UserRolesPermissionsBusiness())
                {
                    var result = business.RetrieveRolesByProfile(user.RoleName).ToList();

                    if (Utilities.Session.GetProfileByRole(user.RoleId) == "PARTNER_ADMIN")
                    {
                        foreach (var r in result)
                        {
                            if (r.Name == "PARTNER_USER")
                            {
                                r.Name = "Socio";
                                dictionary.Add(r.RoleId, r.Name);
                            }
                            else if (r.Name == "SERVICESTATION_ADMIN")
                            {
                                r.Name = "Estaciones de Servicio";
                                dictionary.Add(r.RoleId, r.Name);
                            }
                            if (r.Name != "CUSTOMER_ADMIN") continue;
                            r.Name = "Administrador de Flotas";
                            dictionary.Add(r.RoleId, r.Name);
                        }
                    }

                    if (Utilities.Session.GetProfileByRole(user.RoleId) != "SUPER_ADMIN")
                        return new SelectList(dictionary, "key", "value");
                    foreach (var r in result)
                    {
                        if (r.Name == "PARTNER_ADMIN")
                        {
                            r.Name = "Socio Administrador";
                            dictionary.Add(r.RoleId, r.Name);
                        }
                        if (r.Name == "SUPER_ADMIN")
                        {
                            r.Name = "Usuario COINCA";
                            dictionary.Add(r.RoleId, r.Name);
                        }
                    }
                }

                return new SelectList(dictionary, "key", "value");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        /// <summary>
        /// Modules for Permissions
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RetrieveModulesPermissions(string roleId)
        {
            try
            {
                using (var bus = new UserRolesPermissionsBusiness())
                {
                    var modules = bus.RetrieveModules(null, null, roleId);

                    foreach (var item in modules)
                    {
                        item.Name = "{'Name':'" + item.Name + "'}";
                    }

                    return modules == null ? Json(null) : Json(modules.AsEnumerable(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RetrieveRolesPermissions(int x)
        {
            try
            {
                var user = Utilities.Session.GetUserInfo();

                using (var bus = new UserRolesPermissionsBusiness())
                {
                    var roles = bus.RetrieveRolesByMenu(user.RoleId, Convert.ToInt32(user.UserId), false);

                    return roles == null ? Json(null) : Json(roles.AsEnumerable(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Generate menu tree in json format
        /// </summary>
        /// <returns>json representation of options and permissions</returns>
        [HttpPost]
        public ActionResult GetPermissionsTreeData(string key, string roleId)
        {
            try
            {
                var tree = Business.Administration.ASPMenusBusiness.RetrievePermissions(Convert.ToInt32(key), null, roleId).ToList();

                using (var business = new UserRolesPermissionsBusiness())
                {
                    return Json(business.GetPermissionList(tree.ToList()), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Add Permissions to the Roles
        /// </summary>
        /// <param name="role"></param>
        /// <param name="list"></param>
        /// <param name="module"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddOrEditPermissionsbyRoles(string role, List<string> list, int module)
        {
            try
            {
                using (var business = new UserRolesPermissionsBusiness())
                {
                    //business.DeleteModulebyRole(module, role);
                    business.AddOrEditPermissionsbyRoles(LstPermissionbyRole(list, role), module, role);
                }

                #region Event log
                new EventLogBusiness().AddLogEvent(LogState.ERROR, "Creacion de permisos");
                #endregion

                Session["SaveAs"] = null;
                Session["Flag"] = null;

                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "UserRolesPermission")
                });
            }
        }

        /// <summary>
        /// Add Permissions to the Roles with a coinca user. Options save or save as.
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="roleName"></param>
        /// <param name="list"></param>
        /// <param name="module"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddOrEditPermissionsWithCoincaUser(string roleId, string roleName, List<string> list, int module, string parent)
        {
            try
            {
                using (var business = new UserRolesPermissionsBusiness())
                {
                    if (!string.IsNullOrEmpty(roleId))
                    {
                        business.DeleteModulebyRole(module, roleId);
                        business.AddOrEditPermissionsbyRoles(LstPermissionbyRole(list, roleId), module, roleId);
                    }
                    else
                    {
                        UsersRoles model = new UsersRoles
                        {
                            Name = roleName,
                            Parent = parent
                        };
                        roleId = business.AddOrEditRoles(model);
                        business.AddOrEditPermissionsbyRoles(LstPermissionbyRole(list, roleId), module, roleId);
                    }
                }

                #region Event log
                new EventLogBusiness().AddLogEvent(LogState.ERROR, "Creacion de permisos");
                #endregion

                Session["SaveAs"] = null;
                Session["Flag"] = null;

                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "UserRolesPermission")
                });
            }
        }

        /// <summary>
        /// List The permissions
        /// </summary>
        /// <param name="lstIds"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        private static List<UsersPermissions> LstPermissionbyRole(List<string> lstIds, string roleName)
        {
            try
            {
                var lstUserPermission = new List<UsersPermissions>();

                var menuAnt = string.Empty;

                if (lstIds == null) return lstUserPermission;
                foreach (var id in lstIds)
                {
                    var menu = id.Split('.');
                    if (menu[0] == menuAnt) continue;
                    var permission = new UsersPermissions();
                    menuAnt = menu[0];
                    permission.MenuId = menu[0];
                    permission.Role = roleName;
                    lstUserPermission.Add(permission);
                }
                foreach (var item in lstIds)
                {
                    var menu2 = item.Split('.');
                    switch (menu2[1])
                    {
                        case "1":
                            (from a in lstUserPermission
                             where a.MenuId == menu2[0]
                             select a).ToList().ForEach(a => a.View = true);
                            break;
                        case "2":
                            (from a in lstUserPermission
                             where a.MenuId == menu2[0]
                             select a).ToList().ForEach(a => a.Edit = true);
                            break;
                        case "3":
                            (from a in lstUserPermission
                             where a.MenuId == menu2[0]
                             select a).ToList().ForEach(a => a.Delete = true);
                            break;
                    }

                }
                return lstUserPermission;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        /// <summary>
        /// Check if the role to save is valid
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CheckValidRoleName(string roleName, string parent)
        {
            try
            {
                var count = 0;

                var role = UserRolesPermissionsBusiness.RolesRetrieve(null, roleName, null, null).ToList();

                //Check if the role exists in the list of roles already created in the database
                foreach (var x in role)
                {
                    if (x.Name.ToUpper() == roleName.ToUpper())
                    {
                        count++;
                    }
                }

                if (count != 0) return Json(0, JsonRequestBehavior.AllowGet);
                if (Session["Flag"] == null || Convert.ToInt32(Session["Flag"].ToString()) != 1)
                    return Json(1, JsonRequestBehavior.AllowGet);
                if (parent == "CUSTOMER_ADMIN")
                    parent = UserRolesPermissionsBusiness.RolesRetrieve(null, parent, null, null).FirstOrDefault().RoleId;

                using (var business = new UserRolesPermissionsBusiness())
                {
                    var model = new UsersRoles
                    {
                        Name = roleName,
                        Parent = parent
                    };
                    business.AddOrEditRoles(model);
                }
                return Json(2, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Roles", "UserRolesPermission")
                });
            }
        }

        /// <summary>
        /// Save As the Permissions to a new role,
        /// Initialize the session variables which are going 
        /// to be needed
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveAsPermissions()
        {
            try
            {
                Session["SaveAs"] = "true";
                Session["Flag"] = 1;

                return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Cancel Save As Permissions
        /// Clean the session variables
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CancelSaveAsPermissions()
        {
            try
            {
                Session["SaveAs"] = null;
                Session["Flag"] = null;

                return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult RetrieveCustomerAuthByPartner(LoginViewModel model)
        {
            try
            {
                if (model.FilterType == null)
                    return new JsonResult
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = false }
                    };

                var user = Utilities.Session.GetUserInfo();
                var cust = new CustomersBusiness();
                bool flag = false;

                dynamic clientslist;

                int filter = Convert.ToInt32(model.FilterType);

                //If the user has access to the vpos and the customer, change the role to the customer one
                if (Session["CustomerRole"] != null)
                {
                    if (filter == 2 && user.RoleName != (string)Session["CustomerRole"])
                    {
                        user.RoleNameTemp = user.RoleName;
                        user.RoleName = (string)Session["CustomerRole"];
                        user.IsCustomerUser = true;
                        flag = true;
                    }
                    else if (filter == 2)
                    {
                        flag = true;
                    }
                }

                switch (model.FilterType)
                {
                    case 1:
                        clientslist = GetCustomerAuth(model.FilterType, user.UserId, user.RoleName, flag);
                        break;
                    case 2:
                        clientslist = GetCustomerAuth(model.FilterType, user.UserId, user.RoleName, flag);
                        break;
                    default:
                        clientslist = cust.CustomerByCustomerRetrieve(Convert.ToInt32(user.UserId)).OrderBy(x => x.DecryptedName);
                        break;
                }

                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    MaxJsonLength = int.MaxValue,
                    Data = new { result = true, clientslist }
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="userid"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        private List<ValidateCustomerModelList> GetCustomerAuth(int? filter, int? userid, string role, bool flag)
        {
            try
            {
                var list = new List<ValidateCustomerModelList>();

                if (filter == 2)
                {
                    //Customers load 
                    if (flag)
                    {
                        using (var cust = new CustomersBusiness())
                        {
                            var modelR = cust.CustomerByCustomerRetrieve(Convert.ToInt32(userid)).ToList();

                            foreach (var item in modelR.OrderBy(x => x.DecryptedName))
                            {
                                var model = new ValidateCustomerModelList();

                                item.RoleName = item.RoleName.Replace(" ", "_");

                                model.Value = item.CustomerId + "|" + item.RoleId + "|" + item.RoleName + "|" + item.CountryId + "|" + item.CustomerLogo + "|" + 2;
                                model.Name = "{'Name':'" + item.DecryptedName + "'}";

                                list.Add(model);
                            }
                        }

                        return list;
                    }
                    else
                    {
                        using (var business = new UserRolesPermissionsBusiness())
                        {
                            var modelR = business.RetrieveCustomerAuthByPartner(filter, userid, role).OrderBy(x => x.DecryptedCustomerName);

                            foreach (var item in modelR)
                            {
                                var modelL = new ValidateCustomerModelList();

                                modelL.Value = item.CustomerId + "|" + item.RoleId + "|" + item.RoleName + "|" + item.CountryId + "|"
                                                + item.CustomerLogo + "|" + filter.ToString();
                                modelL.Name = "{'Name':'" + item.DecryptedCustomerName + "'}";
                                list.Add(modelL);
                            }
                        }

                        return list;
                    }
                }
                else
                {
                    var modelR = CustomerByPartnerBusiness.PartnerAuthByPartnerRetrieve(Convert.ToInt32(userid)).OrderBy(x => x.Name);

                    foreach (var item in modelR)
                    {
                        var modelL = new ValidateCustomerModelList();

                        modelL.Value = item.PartnerId + "|" + item.RoleId + "|" + item.RoleName + "|" + item.CountryId + "|"
                                        + item.Name + "|" + filter.ToString();
                        modelL.Name = "{'Name':'" + item.Name + "'}";
                        list.Add(modelL);
                    }

                    return list;
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        /// <summary>
        /// ConfirmationModal
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ConfirmationModal()
        {
            try
            {
                return PartialView("_partials/_ConfirmationPartial");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        [HttpPost]
        public ActionResult AddRoleCustomer(int customerId, string roleId, int partnerId)
        {
            try
            {
                using (var business = new UserRolesPermissionsBusiness())
                {
                    business.AddRoleCustomer(customerId, roleId, partnerId);
                    var list = business.RoleByCustomerRetrieve(roleId);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Add Customer to the Role, CustomerId: {0}, RoleId: {1}", customerId, roleId));
                    return PartialView("_Partials/_CustomerByRoles", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al intentar agregar un role al customer. Detalle: {0}", e.Message));
                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteCustomerRole(int customerId, int partnerId, string roleId)
        {
            try
            {
                using (var business = new UserRolesPermissionsBusiness())
                {
                    business.DeleteRoleCustomer(customerId, roleId, partnerId);
                    var list = business.RoleByCustomerRetrieve(roleId);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Add Customer to the Role, CustomerId: {0}, RoleId: {1}", customerId, roleId));
                    return PartialView("_Partials/_CustomerByRoles", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al intentar eliminar un role al customer. Detalle: {0}", e.Message));
                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetCustomersByPartner(int PartnerId)
        {
            try
            {
                using (var bus = new CustomerByPartnerBusiness())
                {
                    var list = bus.CustomersRetrieve(PartnerId).Select(x => new { Value = x.CustomerId, Text = x.DecryptedCustomerName }).ToList();
                    list.Add(new { Value = -1, Text = "Todos" });
                    return Json(list.OrderBy(x => x.Value), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
