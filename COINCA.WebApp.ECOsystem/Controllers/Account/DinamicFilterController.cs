﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web.Mvc;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;



namespace ECOsystem.Controllers.Account
{
    /// <summary>
    ///Dinamic Filter
    /// </summary>
    public class DinamicFilterController : Controller
    {
        /// <summary>
        /// Index Dinamic Filter
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                var model = new DinamicFilterBase
                {
                    List = new List<DinamicFilter>(),
                    Data = new DinamicFilter { List = new List<SelectListItem>() },
                    Menus = new List<AccountMenus>()
                };

                return View(model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static SelectList RetrieveUsers()
        {
            try
            {
                var user = Utilities.Session.GetUserInfo();
                var dictionary = new Dictionary<int, string>();

                using (var business = new UsersBusiness())
                {
                    //var UserList = business.RetrieveUsers(null, customerId: user.CustomerId).Select(x => new { x.UserId, x.DecryptedName }).Distinct();

                    var UserList = business.RetrieveDynamicFilterUsers(null, customerId: user.CustomerId).Select(x => new { x.UserId, x.DecryptedName, x.SourceCustomerId, x.DecryptedSourceCustomerName }).Distinct();

                    foreach (var r in UserList)
                    {
                        string displayCustomerName = string.Empty;
                        if (r.SourceCustomerId != null && r.SourceCustomerId != user.CustomerId)
                            displayCustomerName = " (" + r.DecryptedSourceCustomerName + ")";
                        dictionary.Add(Convert.ToInt32(r.UserId), "{'Name':'" + r.DecryptedName + displayCustomerName + "'}");
                    }

                    return new SelectList(dictionary, "key", "value");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetFilterType()
        {
            try
            {
                var dictionary = new Dictionary<string, string>();

                dictionary.Add("CC", "{'Name':'Centros de Costo'}");
                dictionary.Add("GP", "{'Name':'Grupo de Vehículo'}");

                var obj = new SelectList(dictionary, "key", "value");

                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// GetTreeData
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetTreeData(string Key, int UserId)
        {
            try
            {
                var json = "[";

                using (var bus = new DinamicFilterBusiness())
                {
                    //Retrieve CostCenter data
                    if (Key == "CC")
                    {
                        var model = bus.TreeViewRetrieve(Key).Select(x => new { x.CostCenterId, x.CostCenterName }).Distinct();
                        var children = bus.TreeViewRetrieve(Key);
                        List<GetDataChecked> childrenChecked = bus.TreeViewCheckedRetrieve("VH", UserId).ToList();

                        int? f = null;

                        foreach (var item in model)
                        {
                            bool parentcheckedCC = true;
                            bool childrenSelected = false;
                            if (f != null) json = json + ",";
                            int? y = null;

                            //Verifies if the parent must be checked 
                            foreach (var checks in children.Where(x => x.CostCenterId == item.CostCenterId))
                            {
                                if (childrenChecked.Where(x => x.items == checks.VehicleId.ToString()).Count() == 0)
                                {
                                    parentcheckedCC = false;
                                }
                            }

                            json = json + "{" +
                                    "\"text\": \" " + item.CostCenterName + "\"," +
                                    "\"id\": \"CC." + item.CostCenterId + "\"";

                            if (parentcheckedCC)
                            {
                                json = json + ",\"state\" : { \"checked\": \"true\" }";
                                childrenSelected = true;
                            }

                            json = json + ",\"nodes\": [";

                            foreach (var x in children.Where(c => c.CostCenterId == item.CostCenterId))
                            {
                                if (y != null) json = json + ",";
                                json = json + "{" +
                                              "\"text\": \" " + x.VehicleName + " - " + x.PlateId + "\"," +
                                              "\"id\": \"" + item.CostCenterId + "." + x.VehicleId + "\"";

                                if (childrenChecked.Where(l => l.items == x.VehicleId.ToString()).Count() == 1 || childrenSelected)
                                {
                                    json = json + ",\"state\" : { \"checked\": \"true\" }";
                                }

                                json = json + "}";
                                y = 1;
                            }

                            json = json + "]" + "}";
                            f = 1;
                        }
                        json = json + "]";
                    }

                    //Retrieve VehicleGroups data
                    if (Key == "GP")
                    {
                        var model = bus.TreeViewRetrieve(Key).Select(x => new { x.VehicleGroupId, x.VehicleGroupName }).Distinct();
                        var children = bus.TreeViewRetrieve(Key);
                        List<GetDataChecked> childrenChecked = bus.TreeViewCheckedRetrieve("VH", UserId).ToList();


                        int? f = null;

                        foreach (var item in model)
                        {
                            bool parentCheckedGP = true;
                            bool childrenSelected = false;
                            if (f != null) json = json + ",";
                            int? y = null;

                            //Verifies if the parent must be checked 
                            foreach (var checks in children.Where(x => x.VehicleGroupId == item.VehicleGroupId))
                            {
                                if (childrenChecked.Where(x => x.items == checks.VehicleId.ToString()).Count() == 0)
                                {
                                    parentCheckedGP = false;
                                }
                            }

                            json = json + "{" +
                                    "\"text\": \" " + item.VehicleGroupName + "\"," +
                                    "\"id\": \"GP." + item.VehicleGroupId + "\"";

                            if (parentCheckedGP)
                            {
                                json = json + ",\"state\" : { \"checked\": \"true\" }";
                                childrenSelected = true;
                            }

                            json = json + ",\"nodes\": [";

                            foreach (var x in children.Where(c => c.VehicleGroupId == item.VehicleGroupId))
                            {
                                if (y != null) json = json + ",";
                                json = json + "{" +
                                              "\"text\": \" " + x.VehicleName + " - " + x.PlateId + "\"," +
                                              "\"id\": \"" + item.VehicleGroupId + "." + x.VehicleId + "\"";

                                if (childrenChecked.Where(l => l.items == x.VehicleId.ToString()).Count() == 1 || childrenSelected)
                                {
                                    json = json + ",\"state\" : { \"checked\": \"true\" }";
                                }

                                json = json + "}";
                                y = 1;
                            }

                            json = json + "]" + "}";
                            f = 1;
                        }
                        json = json + "]";
                    }
                    return Json(json, JsonRequestBehavior.AllowGet);
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="List"></param>
        /// <param name="Key"></param>
        /// /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveTreeViewData(List<string> List, string Key, int UserId)
        {
            try
            {
                var newList = new List<DinamicFilter>();
                var ListSave = new List<string>();
                if (List != null)
                {
                    using (var bus = new DinamicFilterBusiness())
                    {
                        if (Key == "CC")
                        {
                            foreach (var val in List)
                            {
                                string[] item = val.Split('.');

                                var l = new DinamicFilter();
                                if (item[0] != "CC")
                                {
                                    l.CostCenterId = Convert.ToInt32(item[0]);
                                    l.VehicleId = Convert.ToInt32(item[1]);
                                }
                                newList.Add(l);
                            }

                            var model = bus.TreeViewRetrieve(Key).Select(x => new { x.CostCenterId, x.CostCenterName }).Distinct();
                            var children = bus.TreeViewRetrieve(Key);

                            string CostCenterString = string.Empty;
                            string VehiclesString = string.Empty;

                            foreach (var item in model)
                            {
                                foreach (var x in newList.Where(l => l.CostCenterId == item.CostCenterId))
                                {
                                    if (VehiclesString == string.Empty)
                                        VehiclesString = x.VehicleId.ToString();
                                    else
                                        VehiclesString = VehiclesString + "," + x.VehicleId;
                                }
                            }

                            ListSave.Add(VehiclesString);

                        }

                        if (Key == "GP")
                        {
                            foreach (var val in List)
                            {
                                string[] item = val.Split('.');
                                var l = new DinamicFilter();

                                if (item[0] != "GP")
                                {
                                    l.VehicleGroupId = Convert.ToInt32(item[0]);
                                    l.VehicleId = Convert.ToInt32(item[1]);
                                    newList.Add(l);
                                }
                            }

                            var model = bus.TreeViewRetrieve(Key).Select(x => new { x.VehicleGroupId, x.VehicleGroupName }).Distinct();
                            var children = bus.TreeViewRetrieve(Key);

                            string VehicleGroupString = string.Empty;
                            string VehiclesString = string.Empty;

                            foreach (var item in model)
                            {
                                foreach (var x in newList.Where(l => l.VehicleGroupId == item.VehicleGroupId))
                                {
                                    if (VehiclesString == string.Empty)
                                        VehiclesString = x.VehicleId.ToString();
                                    else
                                        VehiclesString = VehiclesString + "," + x.VehicleId;
                                }

                            }

                            ListSave.Add(VehiclesString);
                        }
                    }
                }
                else
                {
                    ListSave.Add("");
                }

                using (var bus = new DinamicFilterBusiness())
                {
                    bus.SaveFilters(ListSave, Key, UserId);
                };

                new EventLogBusiness().AddLogEvent(LogState.INFO, "Dinamic Filter Creation");

                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
    }
}