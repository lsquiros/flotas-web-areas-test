﻿using ECOsystem.AccountUtilities;
using ECOsystem.Business.Core;
using ECOsystem.Business.Home;
using ECOsystem.Business.Utilities;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;
using ECOsystem.Models.Home;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ModuleConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace ECOsystem.Controllers.Account
{
    /// <summary>
    /// Account Controller
    /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        #region UserManager
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }
        #endregion

        /// <summary>
        /// Performs Login into Ecosystem, default login View
        /// </summary>
        /// <param name="returnUrl">Call back redirect URL</param>
        /// <returns>Represents the result of an action method.</returns>
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.login = true;
            try
            {
                ViewBag.ReturnUrl = returnUrl;

                if (Session["msgLogin"] != null && !string.IsNullOrEmpty(Session["msgLogin"].ToString()))
                {
                    ViewBag.showModalLoginFails = true;
                    ViewBag.Error = Session["msgLogin"].ToString();
                    Session["msgLogin"] = null;
                }

                #region Filter Templates

                var isCore = ConfigurationManager.AppSettings["MODULE_CORE"];

                if (isCore != "")
                {
                    var model = new LoginViewModel { CList = new List<SelectListItem>() };

                    switch (Utilities.Session.GetProductInfo())
                    {
                        case ProductInfo.Services: // DEFAULT (FOR SERVICES) 
                            return RedirectToAction("DefaultIndex");
                        case ProductInfo.BACFlota: // 2 BAC
                            return View("LoginBAC", model);
                        case ProductInfo.LineVitaECO: // 3 Line Vita
                            return View("LoginLineVita");
                        default:
                            //DEFAULT (COINCA)
                            return View(model);
                    }
                }
                return Redirect(string.Format("{0}{1}", ModuleURI.GetURI(ModuleTypeURI.ModuleCore), "/Account/Login"));

                #endregion

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, "Exception in Render Principal Layout.", true);

                return Redirect(string.Format("{0}{1}", ModuleURI.GetURI(ModuleTypeURI.ModuleCore), "/Account/Login"));
            }

        }

        /// <summary>
        /// Performs Login into Ecosystem, default login View
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [AntiForgeryHandleError]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                #region Filter Templates
                string loginView = "Login";
                switch (Utilities.Session.GetProductInfo())
                {
                    case ProductInfo.BACFlota: // 2 BAC
                        loginView = "LoginBAC";
                        break;
                    case ProductInfo.LineVitaECO: // 3 Line Vita
                        loginView = "LoginLineVita";
                        break;
                    default:
                        //DEFAULT (COINCA)
                        break;
                }
                #endregion

                if (!ModelState.IsValid) return View(loginView, model);

                Session.Add("LOGGED_USER_TIMEZONE_OFF_SET", model.TimezoneOffset ?? 0);
                var loginData = AccountLogin.ECOsystemLogin(model.DecryptedEmail, model.Password);
                if (!loginData.IsAuthenticate)
                {
                    if (loginData.UserId == null)
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Usuario o contraseña incorrectos. Model: {0}.", new JavaScriptSerializer().Serialize(model)), true);
                        ModelState.AddModelError("", "Lo sentimos, La información proporcionada es inválida");
                    }
                    else if (loginData.IsLocked)
                    {
                        ViewBag.showModalLoginFails = true;
                        ViewBag.Error = "Lo sentimos, usuario de ingreso bloqueado por intentos fallidos. Contacte su administrador.";
                    }
                    else if (!loginData.EmailConfirmed)
                    {
                        ViewBag.showModalLoginFails = true;
                        ViewBag.Error = "Lo sentimos, su cuenta de correo no ha sido confirmada aún, favor revisar su correo y confirmar.";
                    }
                    else if (!loginData.IsActive)
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Lo sentimos, usuario desactivado. Model: {0}.", new JavaScriptSerializer().Serialize(model)), true);
                        ViewBag.showModalLoginFails = true;
                        ViewBag.Error = "Lo sentimos, usuario desactivado. Contacte su administrador.";
                    }
                    else if (loginData.ChangePassword)
                    {
                        var token = await UserManager.GeneratePasswordResetTokenAsync(loginData.AspNetId);
                        return View("ResetPassword", new ResetPasswordViewModel { Email = model.DecryptedEmail, Token = token, FirstTime = false, IsPasswordExpired = loginData.ChangePassword });
                    }
                    else
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Usuario o contraseña incorrectos. Model: {0}.", new JavaScriptSerializer().Serialize(model)), true);
                        ModelState.AddModelError("", "Lo sentimos, La información proporcionada es inválida");
                    }
                    model.CList = new List<SelectListItem>();
                    return View(loginView, model);
                }

                Users user = null;
				using (var business = new UsersBusiness())
				{
					user = business.RetrieveUsers(null, userName: model.EncryptedEmail).FirstOrDefault();
				}            
				FormsAuthentication.SetAuthCookie(user.DecryptedEmail, true);
	
				DateTime expire = DateTime.Now.AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["ECOCFG_TIME"]));
				var MyCookie = new HttpCookie("ECOCFG")
				{
					Value = AccountManagement.TokenUserAddOrEdit(user.EncryptedEmail, model.Password),
					Expires = expire
				};
				Response.Cookies.Add(MyCookie);
	
				user.MapsExpirateDateToken = expire;
				user.DecryptedPassword = model.Password;
				Session.Add("LOGGED_USER_INFORMATION", user);

                if (user.IsPartnerUser)
                {
                    if (user.IsServiceStationUser && ECOsystem.Utilities.Session.GetProfileByRole(user.RoleId) == "SERVICESTATION_ADMIN")
                    {
                        model.CList = new List<SelectListItem>();
                        return RedirectToLocal(returnUrl);
                    }
					
                    //OHG - 4Geeks - Para mostrar los Customer a los que se tiene permisos
                    using (var cust = new UserRolesPermissionsBusiness())
                    {
                        var customerAuthByPartner = cust.RetrieveCustomerAuthByPartner(null, user.UserId, user.RoleName).ToList();
                        var partnerAuthByPartner = CustomerByPartnerBusiness.PartnerAuthByPartnerRetrieve(Convert.ToInt32(user.UserId)).ToList();

                        if (!customerAuthByPartner.Any() && !partnerAuthByPartner.Any())
                        {
                            return RedirectToLocal(returnUrl);
                        }

                        ViewBag.showModalCustomer = true;
                        ViewBag.IsCustomer = false;

                        model.CList = new List<SelectListItem>();

                        var configTemplate = ConfigurationManager.AppSettings["TEMPLATE_TYPE"];

                        switch (configTemplate)
                        {
                            case "0": // DEFAULT (FOR SERVICES) 
                                return RedirectToAction("DefaultIndex");
                            case "2": // 2 BAC
                                return View("LoginBAC", model);
                            default:
                                //DEFAULT (COINCA)
                                return View(model);
                        }
                    }
                }

                if (user.IsCustomerUser)
                {
                    using (var cust = new CustomersBusiness())
                    {
                        var customerAuthByCustomer = cust.CustomerByCustomerRetrieve(Convert.ToInt32(user.UserId)).ToList();
                        if (!customerAuthByCustomer.Any()) return RedirectToLocal(returnUrl);

                        ViewBag.showModalCustomer = true;

                        if (GeneralCollections.ValidateVPOSAccess())
                        {
                            model.CList = new List<SelectListItem>();
                        }
                        else
                        {
                            model.CList = GetCustomerAuth(customerAuthByCustomer);
                        }

                        var configTemplate = ConfigurationManager.AppSettings["TEMPLATE_TYPE"];

                        switch (configTemplate)
                        {
                            case "0": // DEFAULT (FOR SERVICES) 
                                return RedirectToAction("DefaultIndex");
                            case "2": // 2 BAC
                                return View("LoginBAC", model);
                            default:
                                //DEFAULT (COINCA)
                                return View(model);
                        }
                    }
                }

                if (user.IsServiceStationUser)
                {
                    model.CList = new List<SelectListItem>();
                    return RedirectToLocal(returnUrl);
                }

                if (user.RoleName == "VPOS_USER")
                {
                    if (!string.IsNullOrEmpty(HomeBusiness.GetCustomerRole()))
                    {
                        ViewBag.showModalCustomer = true;

                        model.CList = new List<SelectListItem>();

                        var configTemplate = ConfigurationManager.AppSettings["TEMPLATE_TYPE"];

                        switch (configTemplate)
                        {
                            case "0": // DEFAULT (FOR SERVICES) 
                                return RedirectToAction("DefaultIndex");
                            case "2": // 2 BAC
                                return View("LoginBAC", model);
                            default:
                                //DEFAULT (COINCA)
                                return View(model);
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(HomeBusiness.GetCustomerRole()))
                {
                    ViewBag.showModalCustomer = true;

                    model.CList = new List<SelectListItem>();

                    var configTemplate = ConfigurationManager.AppSettings["TEMPLATE_TYPE"];

                    switch (configTemplate)
                    {
                        case "0": // DEFAULT (FOR SERVICES) 
                            return RedirectToAction("DefaultIndex");
                        case "2": // 2 BAC
                            return View("LoginBAC", model);
                        default:
                            //DEFAULT (COINCA)
                            return View(model);
                    }
                }

                return RedirectToLocal(returnUrl);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ForgotPassword
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ForgotPassword
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                if (!ModelState.IsValid) return View(model);

                model.Email = model.Email.ToLower();
                var productInfo = ECOsystem.Utilities.Session.GetProductInfo();
                var mailEncrypted = TripleDesEncryption.Encrypt(model.Email);
                var user = await UserManager.FindByNameAsync(mailEncrypted);

                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    TempData["AccountToRecovery"] = model.Email;
                    TempData["PasswordRecoveryStatusCode"] = "NOT_FOUND";
                    return RedirectToAction("ForgotPasswordConfirmation", "Account");
                }

                //Validamos producto activo BAC Flota / Control Car
                switch (productInfo)
                {
                    case ProductInfo.LineVitaECO: //Control Car

                        using (var business = new UsersBusiness())
                        {
                            var result = business.GeneratePasswordReset(mailEncrypted);
                        }

                        break;

                    default:

                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        string token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                        //var callbackUrl = Url.Action("ResetPassword", "Account", new { email = user.Email, token = token, isPasswordExpired = true }, protocol: Request.Url.Scheme);
                        var callbackUrl = Url.Action("ResetPassword", "Account", new RouteValueDictionary(new { emailAcc = user.Email, token, isPasswordExpired = true }), Request.Url.Scheme);

                        await UserManager.SendEmailAsync(user.Id, "Reconfigurar la contraseña", "<h3>Notificación de la Plataforma</h3><br>Por favor reconfigure su contraseña haciendo click <a href=\"" + callbackUrl + "\">aquí</a><br><br><hr>Namutek<br><small>namutek.com</small>");

                        break;
                }

                TempData["AccountToRecovery"] = model.Email;
                TempData["PasswordRecoveryStatusCode"] = "OK";
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e, true);

                TempData["PasswordRecoveryStatusCode"] = "ERROR";
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }
        }

        /// <summary>
        /// ForgotPasswordConfirmation
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            try
            {
                var status = TempData["PasswordRecoveryStatusCode"];
                var email = TempData["AccountToRecovery"];

                if (status != null && status.ToString() == "ERROR")
                {
                    ViewBag.HasError = true;
                }

                ViewBag.AccountRecovery = email;

                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Reset Password View
        /// </summary>
        /// <returns>ActionResult that Represents the result of an action method.</returns>
        [AllowAnonymous]
        public ActionResult ResetPassword(string emailAcc, string token, bool isPasswordExpired)
        {
            try
            {
                var model = new ResetPasswordViewModel { Email = TripleDesEncryption.Decrypt(emailAcc), Token = token, FirstTime = false, IsPasswordExpired = isPasswordExpired };
                return View(model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Reset Password View
        /// </summary>
        /// <returns>ActionResult that Represents the result of an action method.</returns>
        [AllowAnonymous]
        public ActionResult TryConfirmEmail(string e, string t)
        {
            try
            {
                var model = new ResetPasswordViewModel { Email = TripleDesEncryption.Decrypt(e), Token = t, FirstTime = true };
                return View("ResetPassword", model);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex.Message);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Email Confirmation
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="code"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code, string email)
        {
            try
            {
                if (userId == null || code == null)
                {
                    return View("ConfirmationError");
                }

                var lineVitaCodeKey = ConfigurationManager.AppSettings["LineVitaCodeKey"];
                var result = await UserManager.ConfirmEmailAsync(userId, code);

                //Validamos si el code es un hash válido o si proviene de un hash generado por el Servicio LineVitaECO
                if (result.Succeeded || code.Equals(TripleDesEncryption.CalculateMD5Hash(lineVitaCodeKey)))
                {

                    var token = await UserManager.GeneratePasswordResetTokenAsync(userId);
                    return RedirectToAction("TryConfirmEmail", new { e = email, t = token });
                }

                return View("ConfirmationError");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Reset Password View from Request
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ActionResult that Represents the result of an action method.</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                var user = await UserManager.FindByNameAsync(TripleDesEncryption.Encrypt(model.Email));
                if (user == null)
                {
                    //ViewBag.showModalLoginFails = true;
                    //ViewBag.Error = "Lo sentimos, no se completo su solicitud";
                    //return View("Login", new LoginViewModel());
                    Session["msgLogin"] = "Lo sentimos, no se completo su solicitud";
                    return RedirectToAction("Login");
                }

                using (var business = new UsersBusiness())
                {
                    var aux = business.RetrieveUsers(null, userName: TripleDesEncryption.Encrypt(model.Email)).FirstOrDefault();
                    var passwordHistory = new PasswordsHistoryModel();
                    if (aux != null)
                    {
                        int checkPasswordHistory = 0;
                        if (aux.UserId != null)
                        {
                            passwordHistory.UserId = aux.UserId.Value;
                            passwordHistory.Password = model.Password;
                            checkPasswordHistory = business.CheckPasswordsHistory(passwordHistory);
                        }
                        if (checkPasswordHistory == 0)
                        {
                            var result = await UserManager.ResetPasswordAsync(user.Id, model.Token, model.Password);
                            if (result.Succeeded || Convert.ToInt32(ConfigurationManager.AppSettings["TEMPLATE_TYPE"]) == 3)
                            {
                                aux.ChangePassword = false;

                                using (var businessParm = new GeneralCollections())
                                {
                                    var configTemplate = ConfigurationManager.AppSettings["TEMPLATE_TYPE"];
                                    if (Convert.ToInt32(configTemplate) == 3)
                                    {
                                        using (var dba = new DataBaseAccess())
                                        {
                                            var partnerId = dba.ExecuteScalar<int>("[General].[Sp_PartnerLineVita_Retrieve]", new { });

                                            var paramPart = businessParm.RetrievePartnerParameters(partnerId).FirstOrDefault();
                                            aux.PasswordExpirationDate = DateTime.Today.AddDays(Convert.ToDouble(paramPart.ParameterValue));
                                        }
                                    }
                                    else
                                    {
                                        var parms = businessParm.RetrieveParameters().FirstOrDefault();
                                        aux.PasswordExpirationDate = DateTime.Today.AddDays(Convert.ToDouble(parms.PasswordEffectiveDays));
                                    }

                                }

                                business.ExecuteAddOrEditUsers(aux, 0);

                                business.AddPasswordsHistory(passwordHistory, Convert.ToInt32(ConfigurationManager.AppSettings["TEMPLATE_TYPE"]));

                                //ViewBag.showModalLoginFails = true;
                                //ViewBag.Error = (model.FirstTime) ? "Su contraseña ha sido creada exitosamente." : "Su contraseña ha sido cambiada exitosamente.";
                                Session["msgLogin"] = (model.FirstTime) ? "Su contraseña ha sido creada exitosamente." : "Su contraseña ha sido cambiada exitosamente.";

                                if (Convert.ToInt32(ConfigurationManager.AppSettings["TEMPLATE_TYPE"]) != 3)
                                {
                                    await UserManager.SetLockoutEndDateAsync(user.Id, DateTimeOffset.MinValue);
                                    await UserManager.ResetAccessFailedCountAsync(user.Id);
                                }

                                return RedirectToAction("Login");

                                //return View("Login", new LoginViewModel());
                            }
                            AddErrors(result);
                            return View("ResetPassword", new ResetPasswordViewModel { Email = model.Email, Token = model.Token, FirstTime = model.FirstTime, IsPasswordExpired = model.IsPasswordExpired, Error = true, ErrorMessage = "* La contraseña debe contener al menos entre 8 y 15 caracteres, una letra minúscula, una letra mayúscula, un dígito numérico y un carácter especial." });
                        }
                        string errorMessage = "";
                        if (checkPasswordHistory == 1)
                        {
                            errorMessage = "La nueva contraseña no puede ser igual a la última contaseña";
                        }
                        else
                        {
                            errorMessage = "La nueva contraseña no puede ser igual a alguna de las " + checkPasswordHistory + " contraseñas anteriores";
                        }
                        return View("ResetPassword", new ResetPasswordViewModel { Email = model.Email, Token = model.Token, FirstTime = model.FirstTime, IsPasswordExpired = model.IsPasswordExpired, Error = true, ErrorMessage = errorMessage });
                    }
                    //ViewBag.showModalLoginFails = true;
                    //ViewBag.Error = "Lo sentimos, no se completo su solicitud";

                    Session["msgLogin"] = "Lo sentimos, no se completo su solicitud";
                    new EventLogBusiness().AddLogEvent(LogState.WARNING, string.Format("Lo sentimos, no se completo su solicitud. Model: {0}.", new JavaScriptSerializer().Serialize(model)), true);


                    return RedirectToAction("Login");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Log Off Application
        /// </summary>
        /// <returns>ActionResult that Represents the result of an action method.</returns>
        [Authorize]
        public ActionResult LogOff(string data = "")
        {
            try
            {
                AuthenticationManager.SignOut();
                HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
                Session.Clear();
                HttpContext.Request.Cookies.Remove("ECOCFG");

                return Redirect(string.Format("{0}{1}", ModuleURI.GetURI(ModuleTypeURI.ModuleCore), "/Account/Login"));
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Log Off Application
        /// </summary>
        /// <returns>ActionResult that Represents the result of an action method.</returns>
        [HttpPost]
        [Authorize]
        public ActionResult LogOff()
        {
            try
            {
                AuthenticationManager.SignOut();
                HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
                Session.Clear();
                HttpContext.Request.Cookies.Remove("ECOCFG");

                return Redirect(string.Format("{0}{1}", ModuleURI.GetURI(ModuleTypeURI.ModuleCore), "/Account/Login"));
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Return message if the user is current logged
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult AlreadySignIn()
        {
            try
            {
                if (AuthenticationManager.User.Identity.IsAuthenticated)
                {
                    ViewBag.login = true;
                    return View();
                }
                return RedirectToAction("Login");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Default Template
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult DefaultIndex()
        {
            try
            {
                return View("DefaultIndex");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Return view with Unauthorized Message
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Unauthorized()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PartialViewResult UnauthorizedPartial()
        {
            try
            {
                return PartialView("_Unauthorized");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        /// <summary>
        /// IndexControl
        /// </summary>
        /// <param name="title"></param>
        /// <param name="header"></param>
        /// <returns></returns>
        public ActionResult IndexControl(string title, string header)
        {
            try
            {
                ViewBag.Information = title;
                ViewBag.Header = header;

                var model = new AccountMenusBase { Menus = new List<AccountMenus>() };
                return View(model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>ActionResult that Represents the result of an action method.</returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult SetCustomerRole(int? filtertype, string customer = "")
        {
            try
            {
                Users user = Utilities.Session.GetUserInfo();
                var log = Session["LOGGED_USER_INFORMATION"];
                Session.RemoveAll();
                Session["LOGGED_USER_INFORMATION"] = log;

                //Redirect to the VPOS
                if (filtertype == 4)
                {
                    if (GeneralCollections.ValidateVPOSAccess())
                    {
                        var role = (RoleValidateModel)Session["VPOSRole"];
                        user.RoleNameTemp = user.RoleName;
                        user.RoleIdTemp = user.RoleId;
                        user.RoleName = role.Name;
                        user.RoleId = role.RoleId;

                        Session.Remove("LOGGED_USER_INFORMATION");
                        Session.Add("LOGGED_USER_INFORMATION", user);

                        //Update the 
                        HttpCookie aCookie = HttpContext.Request.Cookies["ECOCFG"];
                        bool check = AccountManagement.UpdateTokenInformation(JsonConvert.SerializeObject(user), aCookie.Value);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(user.RoleNameTemp))
                        {
                            user.RoleName = user.RoleNameTemp;
                        }
                    }

                    return Json("OK");
                }

                user.MapsToken = null;

                if (user.IsCustomerUser && user.LoggedUser == "P")
                {
                    user.LoggedUser = "P";
                }
                else if (GeneralCollections.ValidateCustomerAccess() || user.IsCustomerUser)
                {
                    user.RoleName = Session["CustomerRole"].ToString();
                    Session["CustomerRole"] = null;
                    user.LoggedUser = "C";
                }
                else if (user.IsPartnerUser)
                {
                    user.LoggedUser = "P";
                }
                else
                {
                    user.LoggedUser = "CO";
                }

                //Get the information for the Coinca User
                if (customer == "" && filtertype == 3 && GeneralCollections.ValidateSuperAdmin())
                {
                    user.RoleName = Session["SuperAdminRole"].ToString().Split('|')[0];
                    user.RoleId = Session["SuperAdminRole"].ToString().Split('|')[1];
                    user.RoleNameTemp = user.RoleName;

                    user.IsPartnerUser = false;
                    user.IsCoincaUser = true;

                    Session.Remove("LOGGED_USER_INFORMATION");
                    Session.Add("LOGGED_USER_INFORMATION", user);

                    HttpCookie aCookie = HttpContext.Request.Cookies["ECOCFG"];
                    bool check = AccountManagement.UpdateTokenInformation(JsonConvert.SerializeObject(user), aCookie.Value);

                    return Json("OK");
                }

                if (!string.IsNullOrEmpty(customer))
                {
                    var cust = customer.Split('|');


                    if (cust[5] == "1") //FilterType: Partner or Customer
                    {
                        //Falta colocar el CountryId en el combo y obtenerlo aquí
                        //user.PartnerUser = new PartnerUsers();
                        user.PartnerId = cust.Length > 0 ? Convert.ToInt32(cust[0]) : 0;
                        user.CountryId = cust.Length > 0 ? Convert.ToInt32(cust[3]) : 0;
                        //Name of the role
                        user.Name = cust[4];
                        user.IsCustomerUser = false;
                        user.IsPartnerUser = true;

                        //Get Partner Logo
                        user.EntityLogo = Utilities.Session.GetPartnerCustomerLogo("P", cust[0]);
                    }
                    else
                    {
                        //Falta colocar el CountryId en el combo y obtenerlo aquí
                        user.CustomerUser = new CustomerUsers();
                        user.CustomerId = cust.Length > 0 ? Convert.ToInt32(cust[0]) : 0;
                        user.CountryId = cust.Length > 0 ? Convert.ToInt32(cust[3]) : 0;
                        user.IsCustomerUser = true;
                        user.IsPartnerUser = false;

                        //Get Client Logo
                        user.EntityLogo = Utilities.Session.GetPartnerCustomerLogo("C", cust[0]);
                    }

                    //Get User Photo
                    user.Photo = Utilities.Session.GetUserProfileImage(user.UserId);

                    if (cust.Count() > 6)
                    {
                        user.RoleNameTemp = cust[6].Replace("_", " ");
                        user.CustomerNameTemp = cust[0]; //CustomerId //customer;
                        user.ProfileIdTemp = Convert.ToInt32(cust[0]); //CustomerId or PartnerId //customer;

                    }
                    else
                    {
                        user.RoleNameTemp = user.RoleName;
                        user.RoleIdTemp = user.RoleId;
                        user.CustomerNameTemp = customer + "|" + user.RoleName.Replace(" ", "_");
                        user.ProfileIdTemp = Convert.ToInt32(cust[0]); //CustomerId or PartnerId //customer;
                    }

                    user.RoleName = cust.Length > 0 ? !string.IsNullOrEmpty(cust[2]) ? cust[2].Replace("_", " ") : user.RoleName.Replace("_", " ") : user.RoleName.Replace("_", " ");
                    user.RoleId = cust.Length > 0 ? !string.IsNullOrEmpty(cust[1]) ? cust[1] : user.RoleId : user.RoleId;
                    //if (cust[4] == "2")

                    //Update the 
                    HttpCookie aCookie = HttpContext.Request.Cookies["ECOCFG"];
                    bool check = AccountManagement.UpdateTokenInformation(JsonConvert.SerializeObject(user), aCookie.Value);

                    Session.Remove("LOGGED_USER_INFORMATION");
                    Session.Add("LOGGED_USER_INFORMATION", user);

                    return Json("OK");
                }

                if (!string.IsNullOrEmpty(user.RoleNameTemp))
                {
                    user.RoleName = user.RoleNameTemp;
                }

                if (GeneralCollections.ValidateVPOSAccess() && !user.IsCustomerUser)
                {
                    if (user.LoggedUser != "P")
                    {
                        user.RoleNameTemp = user.RoleName;
                        user.RoleName = Session["VPOSRole"].ToString();
                    }
                }

                Session.Remove("LOGGED_USER_INFORMATION");
                Session.Add("LOGGED_USER_INFORMATION", user);

                HttpCookie aCookieN = HttpContext.Request.Cookies["ECOCFG"];
                bool checkN = AccountManagement.UpdateTokenInformation(JsonConvert.SerializeObject(user), aCookieN.Value);

                return Json("ERROR");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private SelectList GetCustomerAuth(List<Customers> listCust)
        {
            try
            {
                var list = new List<ValidateCustomerModelList>();

                foreach (var item in listCust.OrderBy(x => x.DecryptedName))
                {
                    var model = new ValidateCustomerModelList();

                    item.RoleName = item.RoleName.Replace(" ", "_");

                    model.Value = item.CustomerId + "|" + item.RoleId + "|" + item.RoleName + "|" + item.CountryId + "|" + item.CustomerLogo + "|" + 2;
                    model.Name = "{'Name':'" + item.DecryptedName + "'}";

                    list.Add(model);
                }
                return new SelectList(list, "Value", "Name");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        /// <summary>
        /// Terms and conditions for User
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult TermsAndConditions()
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    var terms = business.ControlCarInfoRetrieve().FirstOrDefault();

                    ViewBag.TermsAndConditionsUser = (terms != null) ? terms.LVE_UserTerms : string.Empty;

                    return View();
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// PrivacyPolicy
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult PrivacyPolicy()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        /// <summary>
        /// Used to interact with authentication middleware that have been chained in the pipeline
        /// </summary>
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        /// <summary>
        /// Add Model error to Model State
        /// </summary>
        /// <param name="result">Represents the result of an identity operation</param>
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        /// <summary>
        /// Redirect To Local when true if the URL is local; otherwise, false.
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns>ActionResult that Represents the result of an action method.</returns>
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Represents the result of an unauthorized HTTP request.
        /// </summary>
        internal class ChallengeResult : HttpUnauthorizedResult
        {
            /// <summary>
            /// Default Constructor
            /// </summary>
            /// <param name="provider">provider</param>
            /// <param name="redirectUri">redirectUri</param>
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            /// <summary>
            /// ChallengeResult overload contructuro
            /// </summary>
            /// <param name="provider">provider</param>
            /// <param name="redirectUri">redirectUri</param>
            /// <param name="userId">userId logged</param>
            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            /// <summary>
            /// Login Provider
            /// </summary>
            public string LoginProvider { get; set; }

            /// <summary>
            /// Redirect Uri
            /// </summary>
            public string RedirectUri { get; set; }

            /// <summary>
            /// UserId logged
            /// </summary>
            public string UserId { get; set; }

            /// <summary>
            /// Executes the Result
            /// </summary>
            /// <param name="context">Encapsulates information about an HTTP request that matches specified</param>
            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

    }
}
