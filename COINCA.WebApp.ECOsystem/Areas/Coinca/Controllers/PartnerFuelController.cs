﻿/************************************************************************************************************
*  File    : PartnerFuelController.cs
*  Summary : PartnerFuel Controller Actions
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Business;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Coinca.Models;
using  ECOsystem.Areas.Coinca.Models.Identity;
using ECOsystem.Business.Utilities;
using System.Data;
using ECOsystem.Business.Core;
using ECOsystem.Models.Core;
using System.Collections.Generic;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Web.Script.Serialization;




namespace  ECOsystem.Areas.Coinca.Controllers.Coinca
{
    /// <summary>
    /// PartnerFuel Controller. Implementation of all action results from views
    /// </summary>
    public class PartnerFuelController : Controller
    {
        /// <summary>
        /// PartnerFuel Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>        
        [EcoAuthorize]
        public ActionResult Index()
        {
            return View(new PartnerFuelBase());
        }

        /// <summary>
        /// PartnerFuel Main View
        /// </summary>
        /// <param name="KeyId">PartnerId to load grid</param>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(int KeyId)
        {
            if (KeyId == 0 ) RedirectToAction("Index");
            Session["PartnerId"] = KeyId;
            using (var business = new PartnerFuelBusiness())
            {
                var model = new PartnerFuelBase
                {
                    KeyId = KeyId,
                    Data = new PartnerFuel(),
                    List = business.RetrievePartnerFuel(null, KeyId, null, ECOsystem.Utilities.Session.GetUserInfo().UserId)
                };
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity PartnerFuel
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditPartnerFuel(PartnerFuel model)
        {
            try
            {
                using (var business = new PartnerFuelBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditPartnerFuel(model);
                    }

                    //Log
                    if(model.IsScheduled)
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Programación de Precio, Usuario: {0}, Detalle: {1}", ECOsystem.Utilities.Session.GetUserInfo().UserId, new JavaScriptSerializer().Serialize(model)));
                    else
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edición de Precio, Usuario: {0}, Detalle: {1}", ECOsystem.Utilities.Session.GetUserInfo().UserId, new JavaScriptSerializer().Serialize(model)));

                    return PartialView("_partials/_Grid", business.RetrievePartnerFuel(null, model.PartnerId, null, null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "PartnerFuel")
                });
            }
        }
      
        /// <summary>
        /// Load PartnerFuel
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="partnerId">partner Id to load all information regarding fuel by month and year</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadPartnerFuel(int id, int partnerId, bool IsScheduled)
        {
            try
            {               
                using (var business = new PartnerFuelBusiness())
                {
                    var model = business.RetrievePartnerFuel(id, partnerId, IsScheduled, null).FirstOrDefault();
                    model.IsScheduled = IsScheduled;

                    if (!IsScheduled)                       
                        ViewBag.TitleModal = "Precio Actual";                                           
                    else
                        ViewBag.TitleModal = "Nuevo Precio";

                    return PartialView("_partials/_Detail", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "PartnerFuel")
                });
            }
        } 

        /// <summary>
        /// Get the country Id for the Customer
        /// </summary>
        /// <param name="PartnerId"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult LoadDownloadModal(int PartnerId, string PartnerName)
        {
            try
            {
                using (var business = new PartnerFuelBusiness())
                {
                    Session["PartnerName"] = PartnerName;
                    var model = new PartnerFuel();
                    model.PartnerId = PartnerId;
                    model.CountryId = business.GetCountryIdByPartner(PartnerId);
                    return PartialView("_partials/_DownloadModal", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "PartnerFuel")
                });
            }
        }

        /// <summary>
        /// Get the data for download
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult DownloadExcelFile(PartnerFuel model)
        {
            try
            {
                using (var business = new PartnerFuelBusiness())
                {
                    Session["Reporte"] = business.RetrievePartnerFuelReport(model);                    
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Archivo");
                    return PartialView("_partials/_Grid", business.RetrievePartnerFuel(null, model.PartnerId, null, null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "PartnerFuel")
                });
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                if (report.Rows.Count > 0)
                {
                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Precios de Combustible");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "PartnerFuelReport", this.ToString().Split('.')[2], "Reporte de Precios de Combustible");
                    }
                }
                else throw new Exception();                
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                using (var business = new PartnerFuelBusiness())
                {
                    var model = new PartnerFuelBase
                    {
                        KeyId = (int)Session["PartnerId"],
                        Data = new PartnerFuel(),
                        List = business.RetrievePartnerFuel(null, (int)Session["PartnerId"], null, ECOsystem.Utilities.Session.GetUserInfo().UserId)
                    };
                    return View("Index", model);
                }
            }                        
        }

        /// <summary>
        /// Load Alarm
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alarmTriggerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="entityTypeId"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult LoadAlarm(int id, int alarmTriggerId, int partnerId, int entityTypeId, string title)
        {
            try
            {
                using (var business = new AlarmBusiness())
                {
                    AlarmsModels Model = new AlarmsModels();

                    Model = business.RetrieveFirstAlarm(alarmTriggerId, partnerId, entityTypeId, null, id);
                    Model.TitlePopUp = title;

                    return PartialView("_partials/_Alarms", Model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "PartnerFuel")
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult AddOrEditAlarm(AlarmsModels model)
        {
            try
            {
                using (var business = new AlarmBusiness())
                {
                    model.Phone = (model.Phone == null) ? "0000-0000" : model.Phone;
                    business.AddOrEditAlarm(model);
                }
                ViewBag.ErrorReport = null;
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "PartnerFuel")
                });
            }
        }
    }
}