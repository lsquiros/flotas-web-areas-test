﻿using ECOsystem.Audit;
using  ECOsystem.Areas.Coinca.Business;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using  ECOsystem.Areas.Coinca.Models.Identity;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Coinca.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;




namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// Partner News Controller
    /// </summary>
    public class PartnerNewsController : Controller
    {
        /// <summary>
        /// Partners Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new PartnerNewsBusiness())
            {
                PartnerNews newsModel  = new PartnerNews();
                int? partnerId = ECOsystem.Utilities.Session.GetPartnerId();
                IEnumerable<PartnerNews> partnersNews;
                if (User.IsInRole("PARTNER_ADMIN"))
                {
                    newsModel.PartnerId = (int)partnerId;
                    partnersNews = business.RetrieveNewsByPartners(partnerId, null);
                }
                else {
                    partnerId = null;
                    partnersNews = business.RetrieveAllNewsByPartners(null);
                }
               
                var model = new PartnersNewsBase
                {
                    Data = newsModel,
                    List = partnersNews,
                    Menus = new List<AccountMenus>()
                };
               
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Partners
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditPartnerNews(PartnerNews model)
        {
            try
            {
                using (var business = new PartnerNewsBusiness())
                {
                  
                    business.AddOrEditNewsByPartners(model);

                    IEnumerable<PartnerNews> partnersNews;
                    if (User.IsInRole("PARTNER_ADMIN"))
                    {
                        partnersNews = business.RetrieveNewsByPartners(ECOsystem.Utilities.Session.GetPartnerId(), null);
                    }
                    else
                    {
                        partnersNews = business.RetrieveAllNewsByPartners(null);
                    }
               
                    return PartialView("_partials/_Grid", partnersNews);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Partners")
                });
            }
        }

        /// <summary>
        /// Search Partners
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchPartnerNews(string key)
        {
            try
            {
                using (var business = new PartnerNewsBusiness())
                {
                    IEnumerable<PartnerNews> partnersNews;
                    if (User.IsInRole("PARTNER_ADMIN"))
                    {
                        partnersNews = business.RetrieveNewsByPartners(ECOsystem.Utilities.Session.GetPartnerId(), null, key);
                    }
                    else
                    {
                        partnersNews = business.RetrieveAllNewsByPartners(null);
                    }
                    return PartialView("_partials/_Grid", partnersNews);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Partners")
                });
            }
        }

        /// <summary>
        /// Load Partners
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadPartnerNews(int id)
        {
            try
            {
                using (var business = new PartnerNewsBusiness())
                {
                    if (User.IsInRole("PARTNER_ADMIN"))
                    {
                        return PartialView("_partials/_Detail", business.RetrieveNewsByPartners(ECOsystem.Utilities.Session.GetPartnerId(), id).FirstOrDefault());
                    }
                    else
                    {
                        return PartialView("_partials/_Detail", business.RetrieveNewsByPartners(null, id).FirstOrDefault());
                    }
                  
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Partners")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Partners
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeletePartnerNews(int id)
        {
            try
            {
                using (var business = new PartnerNewsBusiness())
                {
                    business.DeleteNewsByPartners(id);
                    IEnumerable<PartnerNews> partnersNews;
                    if (User.IsInRole("PARTNER_ADMIN"))
                    {
                        partnersNews = business.RetrieveNewsByPartners(ECOsystem.Utilities.Session.GetPartnerId(), null);
                    }
                    else
                    {
                        partnersNews = business.RetrieveAllNewsByPartners(null);
                    }
                    return PartialView("_partials/_Grid", partnersNews);
                }
            }


            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Partners")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Partners")
                    });
                }
            }
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace),
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}