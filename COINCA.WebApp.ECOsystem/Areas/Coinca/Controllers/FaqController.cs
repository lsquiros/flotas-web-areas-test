﻿/************************************************************************************************************
*  File    : FaqController.cs
*  Summary : FAQ Controller Actions
*  Author  : Gerald Solano
*  Date    : 12/03/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Business;
using ECOsystem.Models.Account;
using  ECOsystem.Areas.Coinca.Models.Identity;
using  ECOsystem.Areas.Coinca.Models;




namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// FAQ Controller. Implementation of all action results from views
    /// </summary>
    public class FaqController : Controller
    {
        /// <summary>
        /// FAQ Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            var model = new FaqModelBase();

            ViewBag.Message = "";
            ViewBag.TypeMessage = "";

            string root = Server.MapPath("~/FilesFAQ");
            using (var business = new FaqBusiness())
            {
                model.Data = business.getDataFAQ("Operaciones", root);
            }

            model.Menus = new List<AccountMenus>();
            
            return View(model);
        }

        
        /// <summary>
        /// Actualiza el file FAQ de Operaciones
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        [EcoAuthorize]
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase f)
        {
            IList<FaqModel> dataFAQ = new List<FaqModel>();

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null)
                {
                    try
                    {
                        var partnerId = ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId;
                        string extension = Path.GetExtension(file.FileName);
                        string root = Server.MapPath("~/FilesFAQ");

                        if (extension.Contains("csv"))
                        {
                            string newFileName = string.Format("faq_Operaciones{0}", extension);
                            string path = Path.Combine(root, newFileName);
                            file.SaveAs(path);

                            ViewBag.Message = "Archivo procesado correctamente.";
                            ViewBag.TypeMessage = "SUCCESS";
                        }
                        else {
                            ViewBag.Message = "Se requiere archivos en formato CSV. Por favor intentelo nuevamente.";
                            ViewBag.TypeMessage = "ALERT";
                        }

                        using (var business = new FaqBusiness())
                        {
                            dataFAQ = business.getDataFAQ("Operaciones", root);
                        }

                        if (dataFAQ.Count == 0) 
                        {
                            ViewBag.Message = "No se encontraron datos para procesar. Por favor intente nuevamente.";
                            ViewBag.TypeMessage = "ALERT";
                        }

                        var model = new FaqModelBase();

                        string Nroot = Server.MapPath("~/FilesFAQ");
                        using (var business = new FaqBusiness())
                        {
                            model.Data = business.getDataFAQ("Operaciones", Nroot);
                        }

                        model.Menus = new List<AccountMenus>();

                        return View(model);                         
                    }
                    catch (Exception ex)
                    {
                        ECOsystem.Utilities.Session.SentrySupport(ex);

                        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                        ViewBag.TypeMessage = "ERROR";

                        var model = new FaqModelBase();

                        string Nroot = Server.MapPath("~/FilesFAQ");
                        using (var business = new FaqBusiness())
                        {
                            model.Data = business.getDataFAQ("Operaciones", Nroot);
                        }

                        model.Menus = new List<AccountMenus>();

                        return View(model);  
                    }
                }
            }

            ViewBag.Message = "No has seleccionado ningún archivo.";
            ViewBag.TypeMessage = "ALERT";

            var modelV = new FaqModelBase();

            string NrootV = Server.MapPath("~/FilesFAQ");
            using (var business = new FaqBusiness())
            {
                modelV.Data = business.getDataFAQ("Operaciones", NrootV);
            }
            
            modelV.Menus = new List<AccountMenus>();

            return View(modelV);  
        }
	}
}