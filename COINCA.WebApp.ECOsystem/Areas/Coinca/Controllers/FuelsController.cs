﻿/************************************************************************************************************
*  File    : FuelsController.cs
*  Summary : Fuels Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Business;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Coinca.Models;
using  ECOsystem.Areas.Coinca.Models.Identity;




namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// Fuels Controller. Implementation of all action results from views
    /// </summary>
    public class FuelsController : Controller
    {

        /// <summary>
        /// Fuels Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new FuelsBusiness())
            {
                var model = new FuelsBase
                {
                    Data = new Fuels(),
                    List = business.RetrieveFuels(null)
                };
                return View(model);
            }

        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Fuels
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditFuels(Fuels model)
        {
            try
            {
                using (var business = new FuelsBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditFuels(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveFuels(null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Fuels")
                });
            }
        }

        /// <summary>
        /// Search Fuels
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchFuels(string key)
        {
            try
            {
                using (var business = new FuelsBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveFuels(null, key: key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Fuels")
                });
            }
        }

        /// <summary>
        /// Load Fuels
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadFuels(int id)
        {
            try
            {
                using (var business = new FuelsBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveFuels(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Fuels")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Fuels
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteFuels(int id)
        {
            try
            {
                using (var business = new FuelsBusiness())
                {
                    business.DeleteFuels(id);
                    return PartialView("_partials/_Grid", business.RetrieveFuels(null));
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Fuels")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Fuels")
                    });
                }
            }

        }

    }
}