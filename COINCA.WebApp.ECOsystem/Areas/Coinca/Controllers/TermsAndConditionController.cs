﻿using  ECOsystem.Areas.Coinca.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Models;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Coinca.Business;

namespace  ECOsystem.Areas.Coinca.Controllers
{   
    public class TermsAndConditionsController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var bus = new TermsAndConditionsBusiness())
            {
                return View(bus.RetrieveTermsAndConditions());
            }
        }

        public ActionResult LoadTermsAndConditions(int? PartnerId)
        {
            try
            {
                using (var bus = new TermsAndConditionsBusiness())
                {
                    return PartialView("_partials/_Detail", bus.RetrieveTermsAndConditions(PartnerId).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "TermsAndCondition")
                });
            }
        }

        public ActionResult AddOrEditTermsAndConditions(TermsAndConditions model)
        {
            try
            {
                using (var bus = new TermsAndConditionsBusiness())
                {
                    
                    bus.AddOrEditTermsAndConditions(model);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "TermsAndCondition")
                });
            }
        }
    }
}