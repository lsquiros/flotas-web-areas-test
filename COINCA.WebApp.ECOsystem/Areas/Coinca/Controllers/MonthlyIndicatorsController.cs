﻿using  ECOsystem.Areas.Coinca.Business;
using  ECOsystem.Areas.Coinca.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;




namespace  ECOsystem.Areas.Coinca.Controllers.Coinca
{
    public class MonthlyIndicatorsController : Controller
    {        
        [EcoAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetReportData(int Month, int Year)
        {
            try
            {
                using (var bus = new MonthlyIndicatorsBusiness())
                {
                    var data = bus.GetReportData(Month, Year);
                    if (data.Rows.Count > 0)
                    {
                        Session["Reporte"] = data;
                    }
                    else
                        return Json("NoData", JsonRequestBehavior.AllowGet);

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Genera información para el Reporte de Servicios, parámetros: Mes: {0}, Año: {1}", Month, Year));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Generar información para el Reporte de Servicios, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Reporte de Facturación", TechnicalError = e.Message });
            }
        }

        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de Indicadores");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "MonthlyIndicators", this.ToString().Split('.')[2], "Reporte de Indicadores");
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}