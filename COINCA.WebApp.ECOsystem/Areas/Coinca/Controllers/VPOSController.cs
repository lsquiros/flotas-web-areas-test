﻿using  ECOsystem.Areas.Coinca.Business;
using  ECOsystem.Areas.Coinca.Models;
using  ECOsystem.Areas.Coinca.Models.Identity;
using  ECOsystem.Areas.Coinca.Models.Control;
using ECOsystem.Audit;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Coinca.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using Newtonsoft.Json;
using ECOsystem.Utilities;
using ECOsystem.Models.Core;




namespace  ECOsystem.Areas.Coinca.Controllers.Coinca
{
    public class VPOSController : Controller
    {
        
        /// <summary>
        /// Virtual POS
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>      
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    var model = new CreditCardBase
                    {
                        Details = business.RetrieveCreditCardDetail(null, true),
                        List = business.RetrieveCreditCardVPOS(null),
                        Menus = new List<AccountMenus>()
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                // Return response
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VPOS")
                });
            }
        }


        /// <summary>
        /// Search CreditCard
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchCreditCardVPOS(string key)
        {
            try
            {
                var filter = key;
                key = String.Empty;
                using (var business = new CreditCardBusiness())
                {
                    var models = business.RetrieveCreditCardVPOS(key).Where(creditCard =>
                                            (creditCard.ExpirationDate != null && creditCard.ExpirationDate.ToLower().Contains(filter.ToLower())) ||
                                            (creditCard.CreditCardHolder != null && creditCard.CreditCardHolder.ToLower().Contains(filter.ToLower())) ||
                                            (creditCard.StatusName != null && creditCard.StatusName.ToLower().Contains(filter.ToLower())) ||
                                            (creditCard.RestrictCreditCardNumber != null && creditCard.RestrictCreditCardNumber.ToLower().Contains(filter.ToLower())) ||
                                            (creditCard.DecryptedCustomerName != null && creditCard.DecryptedCustomerName.ToLower().Contains(filter.ToLower()))
                                         );

                    return PartialView("_partials/_GridVPOS", models);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VPOS")
                });
            }
        }

        /// <summary>
        /// Load CreditCardVPOS
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="customer">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="number">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="expirationYear">Expiration Year of the Credit Card</param>
        /// <param name="expirationMonth">Expiration Month of the Credit Card</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadCreditCardVPOS(int id, string customer, string number, string expirationYear, string expirationMonth)
        {
            try
            {
                var creditCardTransaction = new CreditCardTransaction
                {
                    CreditCardId = id,
                    Customer = customer,
                    AccountNumber = number,
                    ExpirationDate = int.Parse(expirationYear).ToString("00") + int.Parse(expirationMonth).ToString("00")
                };

                return PartialView("_partials/_DetailVPOS", creditCardTransaction);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);


                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VPOS")
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="creditCardId"></param>
        /// <returns></returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult LoadRetrieveCreditCardSale(int creditCardId)
        {
            CreditCardTransaction creditCardTransaction = new CreditCardTransaction();
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    creditCardTransaction = business.RetrieveCreditCardTransactionVPOS(creditCardId).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
            return PartialView("_partials/_DetailVPOS_VOID", creditCardTransaction);
        }


        /// <summary>
        /// Performs the the operation of delete on the model CreditCard
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public JsonResult RetrieveCreditCardSale(int creditCardId)
        {
            CreditCardTransaction creditCardTransaction = new CreditCardTransaction();
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    creditCardTransaction = business.RetrieveCreditCardTransactionVPOS(creditCardId).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
            return Json(creditCardTransaction);
        }

        /// <summary>
        /// Performs the the operation of delete on the model CreditCard
        /// </summary>
        /// <param name="creditCardTransaction">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public JsonResult ExecuteCreditCardSale(CreditCardTransaction creditCardTransaction)
        {
            try
            {
                creditCardTransaction.TransactionType = "SALE";
                return Json(ExecuteCreditCard(creditCardTransaction));
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = "" }
                };
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model CreditCard
        /// </summary>
        /// <param name="creditCardTransaction">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public JsonResult ExecuteCreditCardReverse(CreditCardTransaction creditCardTransaction)
        {
            try
            {
                creditCardTransaction.TransactionType = "REVERSE";
                return Json(ExecuteCreditCard(creditCardTransaction));
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = "" }
                };
            }
        }

        ///</summary>
        ///<summary>
        ///Performs the the operation of send a preauthorization for a SALE
        ///</summary>
        ///<param name="creditCardTransaction">The PRIMARY KEY uniquely that identifies each record of this model</param>
        ///<returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public ActionResult ExecuteCreditCardPreauthorizedSale(CreditCardTransaction creditCardTransaction)
        {
            try
            {
                creditCardTransaction.TransactionType = "PREAUTHORIZED_SALE";

                var result = ExecuteCreditCard(creditCardTransaction);

                if (result == null) throw new Exception();

                return Json(result);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = null
                };
            }

        }

        ///<summary>
        ///Performs the the operation of delete on the model CreditCard
        ///</summary>
        ///<param name="creditCardTransaction">The PRIMARY KEY uniquely that identifies each record of this model</param>
        ///<returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public JsonResult ExecuteCreditCardVoid(CreditCardTransaction creditCardTransaction)
        {
            creditCardTransaction.TransactionType = "VOID";
            return Json(ExecuteCreditCard(creditCardTransaction));
        }

        ///<summary>
        ///Execute CreditCard SALE Transaction
        ///</summary>
        ///<param name="creditCardTransaction"></param>
        ///<returns></returns>
        public JsonResult ExecuteCreditCard(CreditCardTransaction creditCardTransaction)
        {
            creditCardTransaction.EntryMode = "MNL";
            creditCardTransaction.TerminalId = (!string.IsNullOrEmpty(creditCardTransaction.TerminalId)) ?
                                                    creditCardTransaction.TerminalId.ToUpper() : creditCardTransaction.TerminalId;
            string AccountNumberTemp;

            try
            {
                using (var business = new CreditCardBusiness())
                {
                    //Get the minutes from the startime to the actual time
                    TimeSpan span = DateTime.Now - Convert.ToDateTime(Session["StartTime"] == null ? DateTime.Now.ToString() : Session["StartTime"].ToString());
                    double MinutesLeft = span.TotalMinutes;

                    if (MinutesLeft > business.GetLapseTime())
                    {
                        return Json("LapseTime", JsonRequestBehavior.AllowGet);
                    }

                    //Get the partner Id
                    creditCardTransaction.PartnerId = Convert.ToInt32(business.GetPartnerIdByCreditCard(Convert.ToInt64(creditCardTransaction.AccountNumber.Replace("-", ""))));

                    //Set the MasterCard as the main card for the transaction, and save the accountNumber
                    AccountNumberTemp = creditCardTransaction.AccountNumber.Replace("-", "").Trim();
                    creditCardTransaction.AccountNumber = creditCardTransaction.MasterCardNumber.Replace("-", "").Trim();
                    creditCardTransaction.ExpirationDate = GetExpirationDateNumber(creditCardTransaction.MasterExpirationDate).ToString();

                    // Agregamos un nuevo registro para una transacción SALE
                    creditCardTransaction = business.AddOrEditCreditCardTransactionVPOS(creditCardTransaction);

                    using (var vpos = new ServiceVPOS())
                    {
                        //Obtenemos la configuración del WCF Web Service autorizador
                        WServiceModel wsm = new WServiceModel();
                        wsm = business.RetrieveWServiceInfoByPartner(creditCardTransaction.PartnerId);

                        //// Envíamos la transacción a Credomatic
                        creditCardTransaction.AnswerCode = vpos.ConnectToService(ref creditCardTransaction, wsm);

                        //Get the transaction answer codes
                        var AnswerCodes = business.GetBacAnswerCodes();

                        switch (AnswerCodes.Where(x => x.Code == creditCardTransaction.AnswerCode).FirstOrDefault().TransactionType)
                        {
                            case TransactionType.Aprobada:
                                //UPDATE the transaction VPOS 
                                business.AddOrEditCreditCardTransactionVPOS(creditCardTransaction);

                                //Add the transaction to the system and set the fleet card to save the transaction
                                creditCardTransaction.AccountNumber = AccountNumberTemp;
                                var response = business.ExecuteAddTransactions(creditCardTransaction);
                                creditCardTransaction.CCTransactionVPOSId = response.TransactionId;
                                Session["InfoBillDownload"] = creditCardTransaction;
                                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[TRANSACTION_ID] {0} | [RESPONSE_CODE] {1} | [MESSAGE] La transacción ha sido PROCESADA.", response.TransactionId, creditCardTransaction.AnswerCode), true);
                                break;

                            case TransactionType.Preautorizada:
                                //UPDATE the transaction VPOS 
                                business.AddOrEditCreditCardTransactionVPOS(creditCardTransaction);

                                var IVRInfo = business.GetUrlPreauthorizedModal((int)creditCardTransaction.PartnerId); //Get the URL for the ticket

                                creditCardTransaction.ErrorMessage = AnswerCodes.Where(x => x.Code == creditCardTransaction.AnswerCode).FirstOrDefault().Answer;
                                creditCardTransaction.TransactionType = "PREAUTHORIZED_SALE";
                                creditCardTransaction.UrlTicket = IVRInfo.IVRLink;
                                creditCardTransaction.PhoneNumber = IVRInfo.IVRPhone;
                                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[RESPONSE_CODE] {0} | [MESSAGE] La transacción ha sido PREAUTORIZADA.", creditCardTransaction.AnswerCode), true);
                                break;

                            default:
                                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("[CC] {0} | [RESPONSE_CODE] {1} | [MESSAGE] ERROR en la Transacción o ha sido DENEGADA.", creditCardTransaction.AccountNumber, creditCardTransaction.AnswerCode), true);
                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message, true);
                return null;
            }

            return Json(creditCardTransaction);
        }

        ///<summary>
        ///Execute CreditCard VOID Transaction 
        ///</summary>
        ///<param name="creditCardTransaction"></param>
        ///<returns></returns>
        [HttpPost]
        public ActionResult ExecuteCreditCardVOID(CreditCardTransaction creditCardTransaction)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    // Validamos que la transacción que deseamos Anular sea realmente un SALE
                    var retrieveCreditCardTransaction = business.RetrieveCreditCardTransactionVPOS(creditCardTransaction.CreditCardId, creditCardTransaction.SystemTraceNumber).FirstOrDefault();
                    //.Where(w => w.TransactionType == "SALE")
                    //.FirstOrDefault();

                    // Agregamos una transacción nueva que indica que se tuvo que Anular la transacción.
                    retrieveCreditCardTransaction.CCTransactionVPOSId = null;
                    retrieveCreditCardTransaction.EntryMode = "MNL";
                    retrieveCreditCardTransaction.TransactionType = "VOID";
                    retrieveCreditCardTransaction.TerminalId = creditCardTransaction.TerminalId;

                    // Obtenemos la transacción seleccionada para anular
                    creditCardTransaction = business.AddOrEditCreditCardTransactionVPOS(retrieveCreditCardTransaction);

                    //using (var vpos = new ServiceVPOS())
                    //{
                    //    //Obtenemos la configuración del WCF Web Service autorizador
                    //    WServiceModel wsm = new WServiceModel();
                    //    wsm = business.RetrieveWServiceInfoByPartner(creditCardTransaction.PartnerId);

                    //    // Envíamos la transacción a Credomatic
                    //    creditCardTransaction.ErrorMessage = vpos.ConnectToService(ref creditCardTransaction, wsm); // Se envia el SystemTrace para el proceso del VOID
                    //}

                    // Validamos la respuesta de la transacción
                    if (creditCardTransaction.ErrorMessage.Contains("APROBADA"))
                    {
                        business.AddOrEditCreditCardTransactionVPOS(creditCardTransaction);
                    }
                    else if (creditCardTransaction.ErrorMessage.Contains("TIME_OUT")
                                || creditCardTransaction.ErrorMessage.Contains("ERROR_COMMUNICATION"))
                    {
                        // Agregamos una transacción nueva que indica que se tuvo pendiente la transacción debido a la respuesta de Credomatic (Excepción por TO, ERROR)
                        creditCardTransaction.CCTransactionVPOSId = null;
                        creditCardTransaction.TransactionType = "VOID";

                        business.AddOrEditCreditCardTransactionVPOS(creditCardTransaction);
                    }

                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
            return Content("Success");
        }

        #region AdminPreauthorized
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [EcoAuthorize]
        public ActionResult AdminPreauthorized()
        {
            var model = new VPOSModelBase()
            {
                Data = new CreditCardTransaction(),
                List = new List<CreditCardTransaction>(),
                Menus = new List<AccountMenus>(),
                Parameters = new ControlFuelsReportsBase()
            };

            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SearchMasterCard(int CreditCardId, int CustomerId, string Plate)
        {
            try
            {
                using (var bus = new CreditCardBusiness())
                {
                    var model = bus.RetrieveMasterCreditCard(CreditCardId, CustomerId, Plate).FirstOrDefault();

                    return Json(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = "" }
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="AccountNumber"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ValidatesCreditCard(string AccountNumber, string PlateId)
        {
            try
            {
                using (var bus = new CreditCardBusiness())
                {
                    var model = (dynamic)null;

                    if (AccountNumber != string.Empty)
                    {
                        model = bus.RetrieveCreditCard(null, null,ECOsystem.Utilities.TripleDesEncryption.Encrypt(AccountNumber.Trim())).FirstOrDefault();
                    }
                    else
                    {
                        model = bus.RetrieveCreditCard(null, null,PlateId).FirstOrDefault();
                    }


                    return Json(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = "" }
                };
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="bacAfiliado"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetServiceStationName(string bacAfiliado)
        {
            var service = ServiceStationBusiness.ServiceStationRetrieve(null, null, null, bacAfiliado).FirstOrDefault();

            if (service != null)
                return Json(service.Name, JsonRequestBehavior.AllowGet);
            else
                return Json(null, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CreditCardId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ValidateDeniedTransaction(string AccountNumber)
        {
            try
            {
                using (var bus = new CreditCardBusiness())
                {
                    string result = bus.ValidateDeniedTransaction(AccountNumber);

                    return Json(result);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = "" }
                };
            }
        }

        /// <summary>
        /// Add the preauthorized transaction to the database 
        /// </summary>
        /// <param name="creditCardTransaction"></param>
        /// <returns></returns>
        public JsonResult AddPreauthorizeTransaction(CreditCardTransaction creditCardTransaction)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    var response = business.ExecuteAddTransactions(creditCardTransaction);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Se agrega Transacción como estado Flotante", true);
                    return Json(response.TransactionId, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex.Message, true);
                return null;
            }
        }

        /// <summary>
        /// GetExpirationDateNumber
        /// </summary>
        /// <param name="expiration"></param>
        /// <returns></returns>
        private int GetExpirationDateNumber(string expiration)
        {
            string[] val = expiration.Split('/');
            var dateTimeFormatInfo = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR"));
            var monthNames = dateTimeFormatInfo.MonthNames.Take(12).ToList();
            int monthNumber = 1;

            foreach (string item in monthNames)
            {
                if (item == val[0].ToLower())
                {
                    break;
                }
                monthNumber++;
            }

            string res = monthNumber.ToString().Length == 1 ? "0" + monthNumber.ToString() : monthNumber.ToString();
            res = val[1].Substring(2, 2) + res;

            return Convert.ToInt32(res);
        }

        public ActionResult DownloadTransactionBill()
        {
            try
            {
                var model = new CreditCardTransaction();
                if (Session["InfoBillDownload"] != null)
                {
                    model = (CreditCardTransaction)Session["InfoBillDownload"];
                    model.Date = Session["StartTime"] != null ? Convert.ToDateTime(Session["StartTime"].ToString()) : DateTime.Now;
                }

                using (var bus = new CreditCardBusiness())
                {
                    var data = bus.GetReportInfo(model);

                    using (var rep = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se descarga factura - {0} por compra de VPOS.", model.AuthorizationNumber), true);
                        return rep.GetReportDataTable(JsonConvert.SerializeObject(data), "VPosTransactionBill", this.ToString().Split('.')[2], string.Format("Factura Transaction - {0}", model.AuthorizationNumber));
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al descargar la factura, Detalle: {0}", e.Message), true);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SendBillEmail(string Email)
        {
            try
            {
                using (var bus = new CreditCardBusiness())
                {
                    var model = new CreditCardTransaction();
                    if (Session["InfoBillDownload"] != null)
                    {
                        model = (CreditCardTransaction)Session["InfoBillDownload"];
                        //Session["InfoBillDownload"] = null; 
                        model.Date = Session["StartTime"] != null ? Convert.ToDateTime(Session["StartTime"].ToString()) : DateTime.Now;

                        bus.SaveEmailToSend(model, Email);

                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se envia factura por correo electrónico a correo: {0}.", Email), true);
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    throw new System.ArgumentException("No existen datos para enviar");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error el enviar factura por correo electrónico a correo: {0}, Detalle: {1}", Email, e.Message), true);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Validates the transation Rules
        /// </summary>
        /// <param name="creditCardTransaction"></param>
        /// <returns></returns>
        public JsonResult ValidateTransactionRules(CreditCardTransaction creditCardTransaction)
        {
            using (var vpos = new ServiceVPOS())
            {
                creditCardTransaction.ExpirationDate = GetExpirationDateNumber(creditCardTransaction.ExpirationDate).ToString();
                vpos.ConnectToValidateRulesService(ref creditCardTransaction);

                if (creditCardTransaction.ResultFromService)
                {
                    return Json(creditCardTransaction.ErrorMessage.Replace("\"", ""), JsonRequestBehavior.AllowGet);
                }
            }

            Session["StartTime"] = DateTime.Now.ToString();
            return Json(creditCardTransaction.ErrorMessage, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PreauthorizeList
        /// <summary>
        /// List of transactions preauthorized
        /// </summary>
        /// <returns></returns>
        [EcoAuthorize]
        public ActionResult PreauthorizeList()
        {
            var model = new VPOSModelBase()
            {
                Data = new CreditCardTransaction(),
                List = new List<CreditCardTransaction>(),
                Menus = new List<AccountMenus>(),
                Parameters = new ControlFuelsReportsBase()
            };

            return View(model);
        }

        /// <summary>
        /// Get the preauthorize transactions
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchTransactions(string key)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    var model = business.PreauthorizedTransactionsRetrieve(key);

                    return PartialView("_partials/_GridPreauthorized", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                var model = new List<CreditCardTransaction>();
                return PartialView("_partials/_GridPreauthorized", model); ;
            }
        }

        /// <summary>
        /// Set the transaction as Processed
        /// </summary>
        [HttpPost]
        public ActionResult SetTransactionProcessed(int TransactionId, string AuthorizationNumber)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    IEnumerable<CreditCardTransaction> model = null;

                    // Get the floating transaction
                    var floatTransaction = business.PreauthorizedTransactionsRetrieve(string.Empty, TransactionId).FirstOrDefault();

                    // Validate exceptions
                    if (floatTransaction == null)
                    {
                        throw new Exception("[NOT_EXIST_TRANSACTION]");
                    }

                    if (floatTransaction.AuthorizationNumber == null ||
                       !floatTransaction.AuthorizationNumber.Equals(AuthorizationNumber))
                    {
                        throw new Exception("[AUTHORIZATION_NUMBER]");
                    }

                    // Validamos Flag para habilitar el envio de la transaccion preautorizada hacia el autorizador
                    if (floatTransaction.FlagToSendPreAuthTransaction)
                    {

                        // Proceso para validar la conexión a Credomatic
                        using (var vpos = new ServiceVPOS())
                        {
                            // Set information to send SALE Transaction
                            CreditCard ccMaster = null;
                            floatTransaction.TransactionType = "SALE";
                            floatTransaction.EntryMode = "MNL";

                            // Get Credit Card Master
                            using (var bus = new CreditCardBusiness())
                            {
                                var ccClient = bus.RetrieveCreditCard(null, null,floatTransaction.AccountNumber.Trim()).FirstOrDefault();

                                ccMaster = (ccClient != null) ?
                                                bus.RetrieveMasterCreditCard(ccClient.CreditCardId, ccClient.CustomerId, floatTransaction.Plate).FirstOrDefault() : null;
                                if (ccClient == null || ccMaster == null)
                                {
                                    throw new Exception("[MASTER_CARD]");
                                }

                                floatTransaction.AccountNumber = ccMaster.DisplayCreditCardNumber.Replace("-", "").Trim();
                                floatTransaction.ExpirationDate = GetExpirationDateNumber(ccMaster.ExpirationDate).ToString();
                            }

                            // Obtenemos la configuración del WCF Web Service autorizador del País de acuerdo al Socio
                            WServiceModel wsm = new WServiceModel();
                            wsm = business.RetrieveWServiceInfoByPartner(floatTransaction.PartnerId);

                            // Envíamos la transacción a Credomatic
                            floatTransaction.AnswerCode = vpos.ConnectToService(ref floatTransaction, wsm);

                            //Get the transaction answer codes
                            var AnswerCodes = business.GetBacAnswerCodes();

                            // Validate transaction Answer Code vs Answer Codes List
                            switch (AnswerCodes.Where(x => x.Code == floatTransaction.AnswerCode).FirstOrDefault().TransactionType)
                            {
                                case TransactionType.Aprobada:

                                    // Set transaction to Processed
                                    model = business.SetPreauthorizedTransactionsProcessed(TransactionId, AuthorizationNumber, floatTransaction.SystemTraceNumber);
                                    break;

                                default:

                                    new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("[TransactionId] {0} | [RESPONSE_CODE] {1} | [MESSAGE] Transacción Flotante ha sido DENEGADA por Credomatic", TransactionId, floatTransaction.AnswerCode), true);

                                    throw new Exception("[DENY_PREAUTHORIZE_TRANSACTION]");
                            }
                        }


                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[TransactionId] {0} | [RESPONSE_CODE] {1} | [MESSAGE] Transacción Flotante ha sido PROCESADA correctamente.", TransactionId, floatTransaction.AnswerCode), true);

                    }
                    else
                    {
                        floatTransaction.SystemTraceNumber = (string.IsNullOrEmpty(floatTransaction.SystemTraceNumber)) ? DateTime.Now.ToString("yyMMddHHmmss") : floatTransaction.SystemTraceNumber;
                        model = business.SetPreauthorizedTransactionsProcessed(TransactionId, AuthorizationNumber, floatTransaction.SystemTraceNumber);
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Transacción Flotante a Procesada", true);
                    }

                    return PartialView("_partials/_GridPreauthorized", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                switch (e.Message)
                {
                    case "[AUTHORIZATION_NUMBER]":

                        new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message, true);

                        return PartialView("_partials/_InformationModal", new CustomError()
                        {
                            TechnicalError = "El Número de Autorización digitado no coincide con ninguna transacción registrada. Por favor verifique la información e intentelo de nuevo.",
                            ReturnUlr = Url.Action("PreauthorizeList", "VPOS")
                        });

                    case "[MASTER_CARD]":

                        new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message, true);

                        return PartialView("_partials/_InformationModal", new CustomError()
                        {
                            TechnicalError = "Por favor intentelo de nuevo, no se pudo obtener la información de la Tarjeta Maestra. Si el problema persiste contactar a un administrador de soporte técnico.",
                            ReturnUlr = Url.Action("PreauthorizeList", "VPOS")
                        });

                    case "[DENY_PREAUTHORIZE_TRANSACTION]":

                        return PartialView("_partials/_InformationModal", new CustomError()
                        {
                            TechnicalError = "La transacción ha sido DENEGADA por Credomatic, por favor intente más tarde. Si el problema persiste contactar a un administrador de soporte técnico.",
                            ReturnUlr = Url.Action("PreauthorizeList", "VPOS")
                        });

                    case "[NOT_EXIST_TRANSACTION]":

                        new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message, true);

                        return PartialView("_partials/_InformationModal", new CustomError()
                        {
                            TechnicalError = "La transacción seleccionada no coincide con los registros en la plataforma. Por favor contactar a un administrador.",
                            ReturnUlr = Url.Action("PreauthorizeList", "VPOS")
                        });
                    default:

                        new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("{0} | {1}", e.Message, e.InnerException), true);
                        return PartialView("_partials/_ErrorModal", new CustomError()
                        {
                            TechnicalError = "Se detectó un error no controlado en el proceso de compra en línea (VPOS), por favor contactar a un administrador de soporte técnico he indique los pasos realizados.",
                            ReturnUlr = Url.Action("PreauthorizeList", "VPOS")
                        });
                }
            }
        }

        /// <summary>
        /// Set the transaction as Processed
        /// </summary>
        [HttpPost]
        public ActionResult SetTransactionReverse(int TransactionId, decimal TotalAmount)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    var model = business.SetPreauthorizedTransactionsReverse(TransactionId, TotalAmount);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[TransactionId] {0} | [Message] La Transacción ha sido REVERSADA", TransactionId), true);
                    return PartialView("_partials/_GridPreauthorized", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message, true);
                return PartialView("_partials/_ErrorModal", new CustomError()
                {
                    TechnicalError = e.Message,
                    ReturnUlr = Url.Action("PreauthorizeList", "VPOS")
                });
            }
        }
        #endregion

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}