﻿using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Models;

using  ECOsystem.Areas.Coinca.Models.Identity;

namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// Api Transacciones
    /// </summary>
    public class TestingApiTrxController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new TestingApiTrx());
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(TestingApiTrx model)
        {
            if (ModelState.IsValid)
            {
                await CallTransactionApi(model);
            }

            return View(model);
        }

        /// <summary>
        /// CallTransactionApi
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task CallTransactionApi(TestingApiTrx model)
        {
            using (var client = new HttpClient())
            {
                var uri_service = ConfigurationManager.AppSettings["SERVICE_URI_TRANSACTION"];

                client.BaseAddress = new Uri(System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Replace(
                    System.Web.HttpContext.Current.Request.Url.PathAndQuery, "/"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", model.Username, model.Password))));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.PostAsJsonAsync("api/transaction/", model);

                var result = ResultToHtml(response.Content.ReadAsStringAsync().Result);
                if (response.IsSuccessStatusCode)
                {

                    model.JsonResponse = new HtmlString("API Response:<br/>"
                        + "<br/>StatusCode:" + response.StatusCode + " (" + (int)response.StatusCode + ")"
                        + "<br/>Response Message:<br/>" + result);
                }
                else
                {
                    model.JsonResponse = new HtmlString("API Response:<br/>"
                        + "<br/>StatusCode:" + response.StatusCode + " (" + (int)response.StatusCode + ")"
                        + "<br/>StackTrace:<br/>" + result);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private static string ResultToHtml(string result)
        {
            var values = new[]  //char + replacement   -- [0], [1]
            {
                new[] {"{","{<br/>&nbsp;&nbsp;&nbsp;&nbsp;"}, 
                new[] {"}","<br/>}"}, 
                new[] {",",",<br/>&nbsp;&nbsp;&nbsp;&nbsp;"},
                new[] {"[","<br/>&nbsp;&nbsp;&nbsp;&nbsp;[<br/>&nbsp;&nbsp;&nbsp;&nbsp;"},
                new[] {"]","<br/>&nbsp;&nbsp;&nbsp;&nbsp;]"}
            };
            return values.Aggregate(result, (current, v) => current.Replace(v[0], v[1]));
        }
    }
}