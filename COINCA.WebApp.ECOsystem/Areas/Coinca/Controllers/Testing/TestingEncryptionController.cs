﻿using System;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Models;

namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// Api Transacciones
    /// </summary>
    public class TestingEncryptionController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult EncryptIndex()
        {
            return View(new TestingEncryption());
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EncryptIndex(TestingEncryption model)
        {
            var listDesEncrypted = model.Value.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var value in listDesEncrypted)
            {
                var v = value.Replace("\r", "");
                model.Result += ECOsystem.Utilities.TripleDesEncryption.Encrypt(v) + "\n";
            }


            return View(model);
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult DecryptIndex()
        {
            return View(new TestingEncryption());
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DecryptIndex(TestingEncryption model)
        {
            var listEncrypted = model.Value.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var value in listEncrypted)
            {
                model.Result += ECOsystem.Utilities.TripleDesEncryption.Decrypt(value) + "\n";
            }
           
            return View(model);
        }

        
    }
}