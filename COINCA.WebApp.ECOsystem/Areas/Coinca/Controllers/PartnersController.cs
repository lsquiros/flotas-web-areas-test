﻿/************************************************************************************************************
*  File    : PartnersController.cs
*  Summary : Partners Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using Microsoft.AspNet.Identity.Owin;
using ECOsystem.Business.Utilities;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using ECOsystem.Audit;
using  ECOsystem.Areas.Coinca.Models;
using  ECOsystem.Areas.Coinca.Models.Identity;
using  ECOsystem.Areas.Coinca.Business;
using ECOsystem.Models.Core;
using ECOsystem.Business.Core;




namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// Partners Controller. Implementation of all action results from views
    /// </summary>
    public class PartnersController : Controller
    {
        private ECOsystem.ApplicationUserManager _userManager;

        /// <summary>
        /// Asp Net User Manager
        /// </summary>
        public ECOsystem.ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? (_userManager = HttpContext.GetOwinContext().GetUserManager<ECOsystem.ApplicationUserManager>());
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// Partners Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "SUPER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new PartnersBusiness())
            {
                var model = new PartnersBase
                {
                    Data = new Partners(),
                    List = business.RetrievePartners(null)
                };
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Partners
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "SUPER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditPartners(Partners model)
        {
            try
            {               
                using (var business = new PartnersBusiness())
                {
                    if (!ModelState.IsValid)
                        return PartialView("_partials/_Grid", business.RetrievePartners(null));

                    var partnerId = business.AddOrEditPartners(model);

                    if (model.TempUsersList == null)
                        return PartialView("_partials/_Grid", business.RetrievePartners(null));

                    // Add Partner User Administrator
                    var countryAccess = new List<PartnerUsersByCountry>(); //Default Country Access
                    countryAccess.Add(new PartnerUsersByCountry()
                    {
                        CountryId = model.CountryId,
                        IsCountryChecked = true,
                        CountryName = model.CountryName,
                    });
                    
                    using (var business2 = new UsersBusiness())
                    {
                        if (model.PartnerTypeId != 801)
                        {
                           
                            foreach (var userinfo in model.TempUsersList.Select(user => new Users
                            {
                                DecryptedName = user.UserFullName,
                                DecryptedEmail = user.UserEmail,
                                IsPartnerUser = true,
                                RoleName = user.RoleName,
                                PartnerId = partnerId,
                                PartnerUser = new PartnerUsers { PartnerId = partnerId, CountryId = model.CountryId, CountriesAccessList = countryAccess  }
                            }))
                            {
                                business2.AddOrEditUsers(userinfo, UserManager, Url, Request);
                            }
                        }
                        else {

                            foreach (var userinfo in model.TempUsersList.Select(user => new Users
                            {
                                DecryptedName = user.UserFullName,
                                DecryptedEmail = user.UserEmail,
                                IsPartnerUser = true,
                                RoleName = "INSURANCE_ADMIN",
                                PartnerUser = new PartnerUsers { PartnerId = partnerId, CountryId = model.CountryId, CountriesAccessList = countryAccess }
                            }))
                            {
                                business2.AddOrEditUsers(userinfo, UserManager, Url, Request);
                            }
                        }
                    }

                    #region Event log

                    var userId = ECOsystem.Utilities.Session.GetUserInfo();

                    //Add log event
                    AddLogEvent(
                            message: string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model)),
                            userId: (userId != null) ? userId.UserId : 0,
                            customerId: (userId != null) ? userId.CustomerId : 0,
                            partnerId: (userId != null) ? userId.PartnerId : 0,
                            exception: null,
                            isInfo: true);
                    #endregion

                    return PartialView("_partials/_Grid", business.RetrievePartners(null));
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);


                if (e.Message.Contains("ERROR EXIST_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Usuario ya existe",
                        TechnicalError = "El usuario con el correo especificado ya existe en el sistema.",
                        ReturnUlr = Url.Action("Index", "Partners")
                    });

                }
                else if (e.Message.Contains("ERROR UPDT_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Error de Actualización",
                        TechnicalError = "El usuario que se desea actualizar presenta inconsistencias, por favor intente de nuevo el proceso de actualización.",
                        ReturnUlr = Url.Action("Index", "Partners")
                    });

                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Partners")
                    });
                }
            }
        }

        /// <summary>
        /// Search Partners
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "SUPER_ADMIN")]        
        public PartialViewResult SearchPartners(string key)
        {
            try
            {
                using (var business = new PartnersBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrievePartners(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Partners")
                });
            }
        }

        /// <summary>
        /// Load Partners
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "SUPER_ADMIN")]
        public PartialViewResult LoadPartners(int id)
        {
            try
            {
                if (id < 0)
                {
                    return PartialView("_partials/_Detail", new Partners());
                }

                using (var business = new PartnersBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrievePartners(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Partners")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Partners
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "SUPER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult DeletePartners(int id)
        {
            try
            {
                using (var business = new PartnersBusiness())
                {
                    business.DeletePartners(id);

                    #region Event log

                    var user = ECOsystem.Utilities.Session.GetUserInfo();

                    //Add log event
                    AddLogEvent(
                            message: string.Format("[DELETE_USR] Cuenta de Usuario ha sido eliminada. Usuario afectado es UserId = {0}", id),
                            userId: (user != null) ? user.UserId : 0,
                            customerId: (user != null) ? user.CustomerId : 0,
                            partnerId: (user != null) ? user.PartnerId : 0,
                            exception: null,
                            isInfo: true);

                    #endregion

                    return PartialView("_partials/_Grid", business.RetrievePartners(null));
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Partners")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Partners")
                    });
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public static SelectList RetrieveRolesPartners(string roleName)
        {            
            var roles = UserRolesPermissionsBusiness.RolesRetrieve(null, null, 1, roleName);
            var dictionary = new Dictionary<string, string>();

            foreach (var r in roles)
            {
                dictionary.Add(r.RoleId, r.Name);
            }

            return new SelectList(dictionary, "key", "value");
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion

    }
}