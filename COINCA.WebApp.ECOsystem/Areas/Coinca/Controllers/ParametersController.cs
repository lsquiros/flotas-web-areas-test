﻿/************************************************************************************************************
*  File    : ParametersController.cs
*  Summary : Parameters Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Business;
using  ECOsystem.Areas.Coinca.Models;
using  ECOsystem.Areas.Coinca.Models.Identity;
using System.Collections.Generic;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// Parameters Controller. Implementation of all action results from views
    /// </summary>
    public class ParametersController : Controller
    {

        /// <summary>
        /// Parameters Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "SUPER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new ParametersBusiness())
            {
                var model = new ParametersBase
                {
                    Data = business.RetrieveParameters().FirstOrDefault(),
                    Menus = new List<AccountMenus>()
                };
                return View(model);
            }

        }

        /// <summary>
        /// Post Invocation from View
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "SUPER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index(Parameters model)
        {
            using (var business = new ParametersBusiness())
            {
                //if (ModelState.IsValid)
                //{
                    business.AddOrEditParameters(model);
                //}

                var modelParameters = new ParametersBase 
                {
                    Data = business.RetrieveParameters().FirstOrDefault(),
                    Menus = new List<AccountMenus>()
                };

                return PartialView("_partials/_Detail", modelParameters.Data);
            }
        }

    }
}