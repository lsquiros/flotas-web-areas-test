﻿/************************************************************************************************************
*  File    : CountriesController.cs
*  Summary : Countries Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Business;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Coinca.Models;
using  ECOsystem.Areas.Coinca.Models.Identity;




namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// Countries Controller. Implementation of all action results from views
    /// </summary>
    public class CountriesController : Controller
    {

        /// <summary>
        /// Countries Main View
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new CountriesBusiness())
            {
                var model = new CountriesBase
                {
                    Data = new Countries(),
                    List = business.RetrieveCountries(null)
                };
                return View(model);
            }

        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Countries
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditCountries(Countries model)
        {
            try
            {
                using (var business = new CountriesBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditCountries(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveCountries(null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Countries")
                });
            }
        }

        /// <summary>
        /// Search Countries
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchCountries(string key)
        {
            try
            {
                using (var business = new CountriesBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveCountries(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Countries")
                });
            }
        }

        /// <summary>
        /// Load Countries
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadCountries(int id)
        {
            try
            {
                using (var business = new CountriesBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveCountries(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Countries")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Countries
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteCountries(int id)
        {
            try
            {
                using (var business = new CountriesBusiness())
                {
                    business.DeleteCountries(id);
                    return PartialView("_partials/_Grid", business.RetrieveCountries(null));
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Countries")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Countries")
                    });
                }
            }
        }

    }
}