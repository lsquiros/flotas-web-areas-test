﻿/************************************************************************************************************
*  File    : FleetCardsImportController.cs
*  Summary : FleetCards Import Controller Actions
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Business;
using  ECOsystem.Areas.Coinca.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using  ECOsystem.Areas.Coinca.Models.Identity;
using ECOsystem.Models.Account;




namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// VehicleUnits Controller. Implementation of all action results from views
    /// </summary>
    public class FleetCardsImportController : Controller
    {
        /// <summary>
        /// Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            var menus = new List<AccountMenus>();
            return View(menus);
        }

        /// <summary>
        /// Process Csv File
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public JsonResult ProcessCsvFile(IList<CreditCard> list)
        {
            var cards = 0;
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    foreach (var card in list)
                    {
                        business.AddOrEditCreditCard(card);
                        cards++;
                    }

                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = true, rows = cards, message = "<b>Archivo procesado exitosamente!</b>" }
                    };

                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = false, rows = cards, message = "<b>Error procesando archivo! </b><br/>"+e.Message }
                };
            }
            
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace),
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion

    }
}