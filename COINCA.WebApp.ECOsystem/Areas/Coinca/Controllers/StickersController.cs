﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Models;
using  ECOsystem.Areas.Coinca.Models.Identity;
using ECOsystem.Business.Utilities;
using  ECOsystem.Areas.Coinca.Business;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace  ECOsystem.Areas.Coinca.Controllers
{
    public class StickersController : Controller
    {
        // GET: Stickers
        public ActionResult Index()
        {
            return View(new StickersModelBase());
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CustomerByPartner
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public ActionResult AddOrEditStickers(string Url, string Data)
        {
            try
            {
                using (var bus = new StickersBusiness())
                {
                    Stickers Stickers = JsonConvert.DeserializeObject<Stickers>(bus.ReadStickersPinPad(Url, Data).Replace(@"\", "").Replace("u000du000a", "").Replace("\"{", "{").Replace("}\"", "}"));
                    if (Stickers.UUIDMIFARE != null && Stickers.UUIDMIFARE != "")
                    {
                        Stickers.Id = bus.AddOrEditStickers(Stickers);
                        return Json(Stickers);
                    }
                    else
                    {
                        return Json("");
                    }                    
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json("");
            }
        }

    }
}