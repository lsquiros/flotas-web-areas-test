﻿/************************************************************************************************************
*  File    : CustomersByPartnerController.cs
*  Summary : CustomersByPartner Controller Actions
*  Author  : Andrés Oviedo
*  Date    : 10/02/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Business;
using  ECOsystem.Areas.Coinca.Models;
using ECOsystem.Models.Miscellaneous;
using System.Linq;
using System.Collections.Generic;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using  ECOsystem.Areas.Coinca.Models.Identity;
using ECOsystem.Models.Account;




namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// CustomersByPartner Controller. Implementation of all action results from views
    /// </summary>
    public class CustomersByPartnerController : Controller
    {
        /// <summary>
        /// CustomersByPartner Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            var model = new CustomerByPartnersBase
            {
                PartnerId = null,
                Data = new CustomerByPartnerData(),
                Menus = new List<AccountMenus>()
            };
            return View(model);
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CustomerByPartner
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditCustomersByPartner(CustomerByPartnerData model)
        {
            try
            {
                using (var business = new CustomerByPartnerBusiness())
                {
                    business.AddOrEditCustomersByPartner(model);

                    CustomerByPartnerData customerByPartnerData = new CustomerByPartnerData();
                    customerByPartnerData.ConnectedCustomersList = business.RetrieveCustomersByPartners(model.PartnerId).ToList();
                    List<Customers> customers = business.RetrieveAllCustomers().ToList();
                    var list2Lookup = customerByPartnerData.ConnectedCustomersList.ToLookup(customer => customer.CustomerId);
                    customerByPartnerData.AvailableCustomersList = customers.Where(customer => (!list2Lookup.Contains(customer.CustomerId))).ToList();
                  
                    return PartialView("_partials/_List", customerByPartnerData);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CustomersByPartner")
                });
            }
        }


        /// <summary>
        /// Load VehiclesByGroup
        /// </summary>
        /// <param name="id">The vehicle group Id to find all vehicles associated</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadCustomersByPartner(int id)
        {
            try
            {
                using (var business = new CustomerByPartnerBusiness())
                {
                    CustomerByPartnerData model = new CustomerByPartnerData();
                    model.ConnectedCustomersList= business.RetrieveCustomersByPartners(id).ToList();
                    List<Customers> customers = business.RetrieveAllCustomers().ToList();
                    var list2Lookup = model.ConnectedCustomersList.ToLookup(customer => customer.CustomerId);
                    model.AvailableCustomersList =  customers.Where(customer => (!list2Lookup.Contains(customer.CustomerId))).ToList();                

                    return PartialView("_partials/_List", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CustomersByPartner")
                });
            }
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion

    }
}
