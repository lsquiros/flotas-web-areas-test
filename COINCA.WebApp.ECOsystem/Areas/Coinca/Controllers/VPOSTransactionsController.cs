﻿using  ECOsystem.Areas.Coinca.Models;
using  ECOsystem.Areas.Coinca.Models.Identity;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using  ECOsystem.Areas.Coinca.Business;
using  ECOsystem.Areas.Coinca.Utilities;
using  ECOsystem.Areas.Coinca.Models.Control;
using ECOsystem.Business.Utilities;
using System.Web.Script.Serialization;




namespace  ECOsystem.Areas.Coinca.Controllers.Control
{
    public class VPOSTransactionsController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {   
            return View(new VPOSModelBase()
                        {
                            Data = new CreditCardTransaction(),
                            List = new List<CreditCardTransaction>(),
                            Menus = new List<AccountMenus>(),
                            Parameters = new ControlFuelsReportsBase()
                        });
        }

        public ActionResult TransactionsRetrieve(VPOSModelBase model)
        {
            try
            {
                using (var bus = new CreditCardBusiness())
                {
                    var result = bus.VPOSTransactionsRetrieve(model.Parameters);
                    Session["VposTransactions"] = result;
                    Session["VposTransactionsParameters"] = model.Parameters;
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Carga de transacciones - Anulación de Transacciones");
                    return PartialView("_partials/_Grid", result);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al ejecutar la Carga de transacciones - Anulación de Transacciones. Error: {0}", e.Message));
                return PartialView("_partials/_Grid", new List<CreditCardTransaction>());
            }
        }

        public ActionResult TransactionsSearch(string key)
        {
            var model = (List<CreditCardTransaction>)Session["VposTransactions"];
            return PartialView("_partials/_Grid", model.Where(x => x.DecryptCustomerName.ToLower().Contains(key) 
                                                                   || x.Plate.ToLower().Contains(key) || ECOsystem.Utilities.TripleDesEncryption.Decrypt(x.AccountNumber).Contains(key)).ToList());
        }

        public ActionResult ExecuteCreditCardVOID(int TransactionId)
        {
            try
            {   
                using (var business = new CreditCardBusiness())
                {
                    var model = (List<CreditCardTransaction>)Session["VposTransactions"];
                    var retrieveCreditCardTransaction = model.Where(x => x.CCTransactionVPOSId == TransactionId).FirstOrDefault();
                    retrieveCreditCardTransaction.EntryMode = "MNL";
                    retrieveCreditCardTransaction.TransactionType = "VOID";

                    if(!retrieveCreditCardTransaction.IsInternalStation)
                    {
                        using (var vpos = new ServiceVPOS())
                        {
                            WServiceModel wsm = new WServiceModel();
                            wsm = business.RetrieveWServiceInfoByPartner(retrieveCreditCardTransaction.PartnerId);
                            retrieveCreditCardTransaction.ErrorMessage = vpos.ConnectToService(ref retrieveCreditCardTransaction, wsm);
                        }
                    }
                    if ((retrieveCreditCardTransaction.IsInternalStation)||(retrieveCreditCardTransaction.ErrorMessage.Contains("APROBADA") || retrieveCreditCardTransaction.ErrorMessage.Contains("00")))
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se aplica la anulación a la transacción, detalle: {0}", new JavaScriptSerializer().Serialize(retrieveCreditCardTransaction)));
                        retrieveCreditCardTransaction.CCTransactionVPOSId = null;
                        business.ExecuteAddTransactions(retrieveCreditCardTransaction, true);                        
                        model = business.VPOSTransactionsRetrieve((ControlFuelsReportsBase)Session["VposTransactionsParameters"]);
                        Session["VposTransactions"] = model;
                    }
                    else if (retrieveCreditCardTransaction.ErrorMessage.Contains("TIME_OUT") || retrieveCreditCardTransaction.ErrorMessage.Contains("ERROR_COMMUNICATION"))
                    {
                        throw new System.Exception(retrieveCreditCardTransaction.ErrorMessage);
                    }
                    return PartialView("_partials/_Grid", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al aplicar anulación a la transacción, Transaccion: {0}, error: {1}", TransactionId, e.Message));
                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }            
        }
    }
}