﻿/************************************************************************************************************
*  File    : SettingsController.cs
*  Summary : Partners Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System.Web.Mvc;

namespace  ECOsystem.Areas.Coinca.Controllers
{
    /// <summary>
    /// Settings Controller. Implementation of all action results from views
    /// </summary>
    public partial class SettingsController : Controller
    {
        /// <summary>
        /// Settings Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [Authorize(Roles = "SUPER_ADMIN,INSURANCE_ADMIN")]
        public ActionResult Index()
        {
            return View();
        }
	}
}