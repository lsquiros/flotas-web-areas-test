﻿
using  ECOsystem.Areas.Coinca.Business;
using  ECOsystem.Areas.Coinca.Models;
using  ECOsystem.Areas.Coinca.Models.Control;
using ECOsystem.Business.Utilities;
using ECOsystem.CredomaticVPOS;
//using ECOsystem.Models.Control;
/************************************************************************************************************
*  File    : CredomaticVPOSTool.cs
*  Summary : Credomatic VPOS Tool
*  Author  : Manuel Azofeifa Hidalgo
*  Date    : 11/18/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;

using System.Net;
using System.ServiceModel;

namespace  ECOsystem.Areas.Coinca.Utilities
{
    /// <summary>
    /// CredomaticVPOSTool
    /// </summary>
    public class CredomaticVPOSTool : IDisposable
    {
        /// <summary>
        /// ConnectToCredomatic
        /// </summary>
        public string ConnectToCredomatic(ref CreditCardTransaction creditCardTransaction, WServiceModel servModel)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            WSHttpBinding wsb = new WSHttpBinding();
            wsb.Security.Mode = SecurityMode.Transport;

            var credomaticVPOS = new  ECOsystem.CredomaticVPOS.AuthorizationServiceClient();
            credomaticVPOS.Endpoint.Binding = wsb;

            //credomaticVPOS.Endpoint.Binding.SendTimeout = new TimeSpan(0, 0, 60);
            //credomaticVPOS.Endpoint.Binding.ReceiveTimeout = new TimeSpan(0, 0, 60);

            var identification = new clsAuthenticationHeader
            {
                UserName = servModel.UserAuth,
                Password = servModel.PasswordAuth
            };

            try
            {
                clsWSResponseData response = new clsWSResponseData();

                using (var business = new CreditCardBusiness())
                {
                    var vposTest = business.GetParametersVPOS();

                    //DUMMY PROCESS: Proceso para pruebas de desarrollo y QA
                    if (vposTest.IsTesting)
                    {
                         ECOsystem.DummyVPOS.clsWSResponseData responseDummy = new ECOsystem.DummyVPOS.clsWSResponseData();
                        try
                        {
                            // ECOsystem.Areas.Coinca.DummyVPOS.ServiceClient scConnect = new  ECOsystem.Areas.Coinca.DummyVPOS.ServiceClient();
                            //scConnect.Endpoint.Address = new System.ServiceModel.EndpointAddress(vposTest.UriDummyVPOS);
                            
                            //responseDummy = scConnect.executeTransaction();
                            //response.responseCode = responseDummy.responseCode;
                            //response.responseCodeDescription = responseDummy.responseCodeDescription;
                            response.responseCode = "57";
                            response.responseCodeDescription = "TRANSACCION NO PERMITIA";
                            response.systemTraceNumber = creditCardTransaction.SystemTraceNumber;
                        }
                        catch (Exception ex)
                        {
                            new EventLogBusiness().AddLogEvent(LogState.ERROR, ex, true);

                            response = new clsWSResponseData();
                            response.responseCode = "00";
                            response.responseCodeDescription = "APROBADA";
                        }
                    }
                    else
                    {
                        #region URI Configuration
                         credomaticVPOS.Endpoint.Address = new System.ServiceModel.EndpointAddress(servModel.UriService);
                        #endregion

                         /// Proceso Switch para realizar los procesos de SALE | PREAUTHORIZED_SALE | VOID
                        switch (creditCardTransaction.TransactionType)
                        {
                            case "SALE":
                                var requestSALE = new clsWSRequestData
                                {
                                    transactionType = creditCardTransaction.TransactionType,
                                    terminalId = creditCardTransaction.TerminalId,
                                    invoice = creditCardTransaction.Invoice,
                                    entryMode = "MNL",
                                    accountNumber = creditCardTransaction.AccountNumber, //"4587963287456654"
                                    expirationDate = creditCardTransaction.ExpirationDate, // "2502"
                                    totalAmount = creditCardTransaction.TotalAmount.ToString("0.00"),
                                    localDate = DateTime.Now.ToString("MMddyyyy"),
                                    localTime = DateTime.Now.ToString("HHmmss")
                                    //------ GSOLANO: No se envía en la trama para que el BAC trate la transacción sin validar hacia COINCA
                                    //carTag = creditCardTransaction.Plate,   
                                    //kilometers = creditCardTransaction.Odometer.ToString(),
                                    //units = creditCardTransaction.Liters.ToString("0.000")
                                };

                                response = credomaticVPOS.executeTransaction(identification, requestSALE);
                                break;

                            case "PREAUTHORIZED_SALE":
                                var requestPREAUTHORIZED_SALE = new clsWSRequestData
                                {
                                    transactionType = creditCardTransaction.TransactionType,
                                    terminalId = creditCardTransaction.TerminalId,
                                    invoice = creditCardTransaction.Invoice,
                                    entryMode = "MNL",
                                    accountNumber = creditCardTransaction.AccountNumber, //"4587963287456654"
                                    expirationDate = creditCardTransaction.ExpirationDate, // "2502"
                                    totalAmount = creditCardTransaction.TotalAmount.ToString("0.00"),
                                    localDate = DateTime.Now.ToString("MMddyyyy"),
                                    localTime = DateTime.Now.ToString("HHmmss"),
                                    //------ GSOLANO: No se envía en la trama para que el BAC trate la transacción sin validar hacia COINCA
                                    //carTag = creditCardTransaction.Plate,
                                    //kilometers = creditCardTransaction.Odometer.ToString(),
                                    //units = creditCardTransaction.Liters.ToString("0.000"),
                                    authorizationNumber = creditCardTransaction.AuthorizationNumber
                                };

                                response = credomaticVPOS.executeTransaction(identification, requestPREAUTHORIZED_SALE);
                                break;

                            case "VOID":
                                var requestVOID = new clsWSRequestData
                                {
                                    transactionType = creditCardTransaction.TransactionType,
                                    terminalId = creditCardTransaction.TerminalId,
                                    systemTraceNumber = creditCardTransaction.SystemTraceNumber,
                                    referenceNumber = creditCardTransaction.ReferenceNumber,
                                    authorizationNumber = creditCardTransaction.AuthorizationNumber
                                };

                                response = credomaticVPOS.executeTransaction(identification, requestVOID);
                                break;
                            default:
                                break;
                        }

                    }


                    if (response.responseCode.Contains("00") || response.responseCodeDescription.Contains("APROBADA"))
                    {
                        creditCardTransaction.AuthorizationNumber = response.authorizationNumber;
                        creditCardTransaction.ReferenceNumber = response.referenceNumber;
                        creditCardTransaction.SystemTraceNumber = (!string.IsNullOrEmpty(response.systemTraceNumber)) ? response.systemTraceNumber : string.Format("{0}{1}", "NF", creditCardTransaction.CCTransactionVPOSId.ToString());

                        var result = business.AddLogTransactionsVPOS(
                                    (int)creditCardTransaction.CCTransactionVPOSId,
                                    response.responseCode,
                                    response.responseCodeDescription,
                                    "Success",
                                    true,
                                    false,
                                    false);
                    }
                    else
                    {
                        var result = business.AddLogTransactionsVPOS(
                                  (int)creditCardTransaction.CCTransactionVPOSId,
                                  response.responseCode,
                                  response.responseCodeDescription,
                                  "Fail - SALE",
                                  true,
                                  false,
                                  false);

                        //SISTEMA NO DISPONIBLE //REINTENTE TRANSACCION TO
                        //if (response.responseCodeDescription.Contains("SISTEMA NO DISPONIBLE")
                        //        || response.responseCode.Contains("TO")
                        //        || response.responseCodeDescription.Contains("REINTENTE TRANSACCION TO"))
                        //{
                        //    //isTimeOut = true;
                        //    //isErrorUnknown = false;

                        //    #region Apply REVERSE
                        //    ApplyReverseProcess(creditCardTransaction, "Apply REVERSE");
                        //    #endregion
                        //}

                      
                    }
                }
                
                return response.responseCode;
            }
            catch (TimeoutException tex)
            {
                using (var businessEx = new CreditCardBusiness())
                {
                    var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                       "TIME_OUT",
                                                       "TIME_OUT",
                                                       string.Format("Message: {0} | InnerException: {1}", tex.Message, tex.InnerException),
                                                       false,
                                                       true,
                                                       false);


                    #region Apply REVERSE
                    ApplyReverseProcess(creditCardTransaction, "Apply REVERSE", servModel);
                    #endregion
                }

                return "TIME_OUT";
            }
            catch (Exception ex)
            {
                using (var businessEx = new CreditCardBusiness())
                {
                    var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                       "ERROR_COMMUNICATION",
                                                       "ERROR_COMMUNICATION",
                                                       string.Format("Message: {0} | InnerException: {1}", ex.Message, ex.InnerException),
                                                       false,
                                                       false,
                                                       true);
                }

                #region Apply REVERSE
                ApplyReverseProcess(creditCardTransaction, "Apply REVERSE", servModel);
                #endregion

                return "ERROR_COMMUNICATION";
            }
        }

        /// <summary>
        /// Apply Reverse proccess when SALE process is not success for Timeout | Error
        /// </summary>
        /// 
        public void ApplyReverseProcess(CreditCardTransaction creditCardTransaction, string message, WServiceModel servModel)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                WSHttpBinding wsb = new WSHttpBinding();
                wsb.Security.Mode = SecurityMode.Transport;

                var credomaticVPOS = new  ECOsystem.CredomaticVPOS.AuthorizationServiceClient();

                var identification = new clsAuthenticationHeader
                {
                    UserName = servModel.UserAuth,
                    Password = servModel.PasswordAuth
                };               

                credomaticVPOS.Endpoint.Address = new System.ServiceModel.EndpointAddress(servModel.UriService);
                credomaticVPOS.Endpoint.Binding = wsb;

                if (creditCardTransaction.TransactionType == "SALE")
                {
                    var requestREVERSE = new clsWSRequestData
                         {
                             transactionType = "REVERSE",
                             terminalId = creditCardTransaction.TerminalId,
                             invoice = creditCardTransaction.Invoice
                         };

                    var response = credomaticVPOS.executeTransaction(identification, requestREVERSE);

                    using (var businessEx = new CreditCardBusiness())
                    {
                        var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                          response.responseCode,
                                                          response.responseCodeDescription,
                                                          message,
                                                          true,
                                                          false,
                                                          false);
                    }
                }
            }
            catch (TimeoutException tex)
            {
                using (var businessEx = new CreditCardBusiness())
                {
                    var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                       "APPLY_REVERSE_TIME_OUT",
                                                       "APPLY_REVERSE_TIME_OUT",
                                                       string.Format("Message: {0} | InnerException: {1}", tex.Message, tex.InnerException),
                                                       false,
                                                       true,
                                                       false);
                }
            }
            catch (Exception ex)
            {
                using (var businessEx = new CreditCardBusiness())
                {
                    var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                       "APPLY_REVERSE_ERROR_COMMUNICATION",
                                                       "APPLY_REVERSE_ERROR_COMMUNICATION",
                                                       string.Format("Message: {0} | InnerException: {1}", ex.Message, ex.InnerException),
                                                       false,
                                                       false,
                                                       true);
                }
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}