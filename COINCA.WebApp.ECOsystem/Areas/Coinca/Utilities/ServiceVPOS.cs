﻿/************************************************************************************************************
*  File    : ServiceVPOS.cs
*  Summary : Service VPOS Tool
*  Author  : Gerald Solano
*  Date    : 02/10/2014
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using  ECOsystem.Areas.Coinca.Utilities;
using ECOsystem.Audit;
using  ECOsystem.Areas.Coinca.Models.Control;
using  ECOsystem.Areas.Coinca.Models;
using ECOsystem.CredomaticVPOS;
using  ECOsystem.Areas.Coinca.Business;
using ECOsystem.Business.Utilities;
using System.Net.Http;

using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;

namespace  ECOsystem.Areas.Coinca.Utilities
{
    /// <summary>
    /// Service VPOS Tool
    /// </summary>
    public class ServiceVPOS : IDisposable
    {
        /// <summary>
        /// Connect to Service with url provided
        /// </summary>
        /// <param name="creditCardTransaction"></param>
        /// <returns></returns>
        public string ConnectToService(ref CreditCardTransaction creditCardTransaction, WServiceModel servModel)
        {
            try
            {           
                using (var vpos = new CredomaticVPOSTool())
                {
                    return vpos.ConnectToCredomatic(ref creditCardTransaction, servModel);
                }

            }
            catch (Exception ex)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("{0} | {1}", ex.Message, ex.InnerException), true);

                return string.Empty;
            }
        }

        /// <summary>
        /// Apply Reverse proccess when SALE process is not success for Timeout | Error
        /// </summary>
        /// <param name="creditCardTransaction"></param>
        public void ApplyReverseProcess(CreditCardTransaction creditCardTransaction, string message)
        {
            try
            {
                var credomaticVPOS = new  ECOsystem.CredomaticVPOS.AuthorizationServiceClient();

                var identification = new clsAuthenticationHeader
                {
                    UserName = "", //ConfigurationManager.AppSettings["CREDOMATICVPOSUSER"],
                    Password = "" //ConfigurationManager.AppSettings["CREDOMATICVPOSPASSWORD"]
                };
                if (creditCardTransaction.TransactionType == "SALE")
                {
                    var requestREVERSE = new clsWSRequestData
                    {
                        transactionType = "REVERSE",
                        terminalId = creditCardTransaction.TerminalId,
                        invoice = creditCardTransaction.Invoice
                    };

                    var response = credomaticVPOS.executeTransaction(identification, requestREVERSE);

                    using (var businessEx = new CreditCardBusiness())
                    {
                        var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                          response.responseCode,
                                                          response.responseCodeDescription,
                                                          message,
                                                          IsSuccess: true,
                                                          IsTimeOut: false,
                                                          IsCommunicationError: false);
                    }
                }
            }
            catch (TimeoutException tex)
            {
                using (var businessEx = new CreditCardBusiness())
                {
                    var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                       "APPLY_REVERSE_TIME_OUT",
                                                       "APPLY_REVERSE_TIME_OUT",
                                                       string.Format("Message: {0} | InnerException: {1}", tex.Message, tex.InnerException),
                                                       false,
                                                       true,
                                                       false);
                }
            }
            catch (Exception ex)
            {
                using (var businessEx = new CreditCardBusiness())
                {
                    var result = businessEx.AddLogTransactionsVPOS((int)creditCardTransaction.CCTransactionVPOSId,
                                                       "APPLY_REVERSE_ERROR_COMMUNICATION",
                                                       "APPLY_REVERSE_ERROR_COMMUNICATION",
                                                       string.Format("Message: {0} | InnerException: {1}", ex.Message, ex.InnerException),
                                                       false,
                                                       false,
                                                       true);
                }
            }

        }

        /// <summary>
        /// ConnectToTransactionsService
        /// </summary>
        /// <returns></returns>
        public void ConnectToValidateRulesService(ref CreditCardTransaction creditCardTransaction)
        {
            try
            {
                var parameters = new TransactionVPOSParameters();

                //Get the parameters for the service
                using (var bus = new CreditCardBusiness())
                {
                    parameters = bus.GetParametersVPOS();
                }                

                #region Change Model
                TestingApiTrx model = ChangeModel(creditCardTransaction);
                model.systemTraceNumber = parameters.systemTraceNumber;
                #endregion                

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(parameters.UriService);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", parameters.UserName, parameters.Password))));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.PostAsJsonAsync("api/transactionrulesvalidate/", model).Result;

                    var result = response.Content.ReadAsStringAsync().Result;

                    var resp = JsonConvert.DeserializeObject<TransactionVPOSResponse>(result);

                    if (resp.ErrorMessage != null)
                    {
                        creditCardTransaction.ResultFromService = true;
                        creditCardTransaction.ErrorMessage = resp.ErrorMessage;
                    }
                    else
                    {
                        creditCardTransaction.ResultFromService = false;
                    }
                }
            }
            catch (Exception e)
            {
                creditCardTransaction.ResultFromService = false;
                creditCardTransaction.ErrorMessage = e.Message;
            }
        }
        
        /// <summary>
        /// Set the model for the transaction service
        /// </summary>
        /// <param name="creditCardTransaction"></param>
        /// <returns></returns>
        private TestingApiTrx ChangeModel(CreditCardTransaction creditCardTransaction)
        {
            TestingApiTrx model = new TestingApiTrx();
            model.invoice = creditCardTransaction.Invoice;
            model.transactionType = creditCardTransaction.TransactionType;
            model.terminalId = creditCardTransaction.TerminalId;
            model.cardNumber = Convert.ToInt64(creditCardTransaction.AccountNumber.Replace("-", ""));
            model.expirationDate = Convert.ToInt16(creditCardTransaction.ExpirationDate);
            model.carTag = !string.IsNullOrEmpty(creditCardTransaction.DriverCode) ? creditCardTransaction.DriverCode : creditCardTransaction.Plate.Split(':')[1].ToString().Trim();
            model.amount = creditCardTransaction.TotalAmount;
            model.kilometers = creditCardTransaction.Odometer;
            model.units = creditCardTransaction.Liters;
            model.referenceNumber = creditCardTransaction.ReferenceNumber;
            model.authorizationNumber = creditCardTransaction.AuthorizationNumber;
            model.merchantDescription = creditCardTransaction.ServiceStationName;

            return model;
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = "Folder:Utilities",
                Action = "ServiceVPOS",
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
