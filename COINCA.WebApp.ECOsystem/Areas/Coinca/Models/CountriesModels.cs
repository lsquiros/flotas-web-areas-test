﻿/************************************************************************************************************
*  File    : CountriesModels.cs
*  Summary : Countries Models
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Coinca.Models
{
    /// <summary>
    /// Countries Base
    /// </summary>
    public class CountriesBase
    {
        /// <summary>
        /// Default CountriesBase constructor
        /// </summary>
        public CountriesBase()
        {
            Data = new Countries();
            List = new List<Countries>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Countries Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Countries> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Countries Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Countries : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? CountryId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre País")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre País", Width = "200px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Código / Siglas")]
        [Required(ErrorMessage = "{0} es requerido")]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "{0} requiere 3 letras, Ejemplo: CRI")]
        [GridColumn(Title = "Código", Width = "30px", SortEnabled = true)]
        public string Code { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Moneda")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int CurrencyId { get; set; }

        /// <summary>
        /// Capacity Unit property
        /// </summary>
        [DisplayName("Unidad de Medida de Combustible")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? CapacityUnitId { get; set; }

        /// <summary>
        /// CapacityUnitName
        /// </summary>
        [GridColumn(Title = "Unidad de Medida de Combustible", Width = "100px", SortEnabled = true)]
        public string CapacityUnitName 
        {
            get { return  ECOsystem.Areas.Coinca.Business.CountriesBusiness.GetCapacityUnitName(CapacityUnitId); }
        }

        /// <summary>
        /// property
        /// </summary>
        [GridColumn(Title = "Moneda", Width = "100px", SortEnabled = true)]
        public string CurrencyName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Nivel Geopolítico 1")]        
        [GridColumn(Title = "Nivel Geopolítico 1", Width = "200px", SortEnabled = true)]
        public string GeopoliticalLevel1 { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Nivel Geopolítico 2")]        
        [GridColumn(Title = "Nivel Geopolítico 2", Width = "200px", SortEnabled = true)]
        public string GeopoliticalLevel2 { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Nivel Geopolítico 3")]        
        [GridColumn(Title = "Nivel Geopolítico 3", Width = "200px", SortEnabled = true)]
        public string GeopoliticalLevel3 { get; set; }
    }
}