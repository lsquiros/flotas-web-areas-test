﻿/************************************************************************************************************
*  File    : ProductsModels.cs
*  Summary : Products Models
*  Author  : Melvin Salas
*  Date    : JUL/20/2016
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.Models.Miscellaneous;

namespace  ECOsystem.Areas.Coinca.Models
{

    /// <summary>
    /// Products Models
    /// </summary>
    public class Products : ModelAncestor
    {
        /// <summary>
        /// Id property
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Description property
        /// </summary>
        public string Description { get; set; }

    }
}