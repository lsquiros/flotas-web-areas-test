﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Models
{
    /// <summary>
    /// Different Transaction Type
    /// </summary>
    public static class TransactionType
    {
        /// <summary>
        /// Aprobada
        /// </summary>
        public const  string Aprobada = "APROBADA";

        /// <summary>
        /// Preautorizada
        /// </summary>
        public const string Preautorizada = "PREAUTORIZADA";
        
        /// <summary>
        /// Cancelada
        /// </summary>
        public const  string Cancelada = "CANCELADA";
    }    
}