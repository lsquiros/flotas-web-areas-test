﻿/************************************************************************************************************
*  File    : PartnersModels.cs
*  Summary : Partners Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;

namespace ECO_Coinca.Models
{
    /// <summary>
    /// Currencies Base
    /// </summary>
    public class PartnersBase
    {
        /// <summary>
        /// PartnersBase default constructor 
        /// </summary>
        public PartnersBase()
        {
            Data = new Partners();
            List = new List<Partners>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Partners Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Partners> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

    }    

    /// <summary>
    /// Partner Models
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Partners : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? PartnerId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("País")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int CountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("País")]
        [NotMappedColumn]
        public string CountryName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Logo")]
        [NotMappedColumn]
        public string Logo { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string ApiUserName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string ApiPassword { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Usuario API")]
        [NotMappedColumn]
        public string DisplayApiUserName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(ApiUserName); }
            set { ApiUserName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Contraseña API")]
        [NotMappedColumn]
        public string DisplayApiPassword
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(ApiPassword); }
            set { ApiPassword = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Confirmar Contraseña")]
        [NotMappedColumn]
        public string PasswordConfirm { get; set; }

        /// <summary>
        /// Capacity Unit property
        /// </summary>
        [NotMappedColumn]
        public bool ChangePassword { get; set; }

        /// <summary>
        /// Capacity Unit property
        /// </summary>
        [DisplayName("Unidad de Medida de Combustible")]
        [NotMappedColumn]
        public int? CapacityUnitId { get; set; }

        /// <summary>
        /// Partner Type property Id
        /// </summary>
        [DisplayName("Tipo de Socio")]
        [NotMappedColumn]
        public int PartnerTypeId { get; set; }

        /// <summary>
        /// Partner Type property name
        /// </summary>
        [NotMappedColumn]
        public string PartnerTypeName { get; set; }

        /// <summary>
        /// Capacity Unit Name property
        /// </summary>
        [NotMappedColumn]
        public string CapacityUnitName { get; set; }

        /// <summary>
        /// Temp Users List
        /// </summary>
        [NotMappedColumn]
        public IList<PartnerTempUser> TempUsersList { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Porcentaje Error en Precio de Combustible")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int FuelErrorPercent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Rol")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string RoleName { get; set; }

    }

    /// <summary>
    /// Partner Temp User Info
    /// </summary>
    public class PartnerTempUser
    {
        /// <summary>
        /// UserFullName
        /// </summary>
        public string UserFullName { get; set; }

        /// <summary>
        /// UserEmail
        /// </summary>
        public string UserEmail { get; set; }
        
        /// <summary>
        /// UserEmail
        /// </summary>
        public string RoleName { get; set; }


    }


}