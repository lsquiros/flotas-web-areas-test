﻿/************************************************************************************************************
*  File    : CustomersModels.cs
*  Summary : Customers Models
*  Author  : Berman Romero
*  Date    : 09/15/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using System.Web.Script.Serialization;

namespace  ECOsystem.Areas.Coinca.Models
{
    /// <summary>
    /// Currencies Base
    /// </summary>
    public class CustomersBase
    {
        /// <summary>
        /// CustomersBase Constructor
        /// </summary>
        public CustomersBase()
        {
            Data = new Customers();
            List = new List<Customers>();
            Menus = new List<AccountMenus>();          
        }


    
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Customers Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Customers> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Validates whether the current user can create the customer
        /// </summary>
        public bool CanCreateCustomer
        {
            get
            {
                return ECOsystem.Utilities.Session.UserCanCreateCustomer();
            }
        }

    }
    
    ///// <summary>
    ///// Customers Edit Base
    ///// </summary>
    //public class CustomersEditBase
    //{
    //    /// <summary>
    //    /// CustomersBase Constructor
    //    /// </summary>
    //    public CustomersEditBase()
    //    {
    //        Data = new Customers();
    //        Card = new CustomerCreditCards();
    //        List = new List<CustomerCreditCards>();

    //        CardRequest = new CardRequest();
    //        RequestList = new List<CardRequest>();
                        
    //        UserList = new List<Users>();
    //        User = new Users();
    //    }

    //    /// <summary>
    //    /// Data property displays the main information of the entity 
    //    /// </summary>
    //    public Customers Data { get; set; }

    //    /// <summary>
    //    /// Card property displays the main information of the entity 
    //    /// </summary>
    //    /// 

    //    public CustomerCreditCards Card { get; set; }

    //    /// <summary>
    //    /// List of entities to use it for load grid information
    //    /// </summary>
    //    public IEnumerable<CustomerCreditCards> List { get; set; }

    //    /// <summary>
    //    /// List of entities to use it for load grid information
    //    /// </summary>
    //    public IEnumerable<CardRequest> RequestList { get; set; }

    //    /// <summary>
    //    /// Card Request property displays the main information of the entity 
    //    /// </summary>
    //    public CardRequest CardRequest { get; set; }

    //    /// <summary>
    //    /// List of entities to use it for load grid information
    //    /// </summary>
    //    public IEnumerable<Users> UserList { get; set; }

    //    /// <summary>
    //    /// Property Display the information about the Users
    //    /// </summary> 
    //    public Users User { get; set; }
    //}


    /// <summary>
    /// Partner Models
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Customers : ModelAncestor
    {
        
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? CustomerId { get; set; }


        /// <summary>
        /// Terminal Id
        /// </summary>
        [NotMappedColumn]
        public int? TerminalId { get; set; }


        /// <summary>
        /// Sets and gets the default partner of the customer
        /// </summary>
        [NotMappedColumn]
        public bool IsDefault { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("País")]
        [NotMappedColumn]
        public int? CountryId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("País")]
        [NotMappedColumn]
        public string CountryName { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre del Cliente")]
        [NotMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre del Cliente")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre del Cliente", Width = "300px", SortEnabled = true)]
        public string DecryptedName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Moneda")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int CurrencyId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Moneda")]
        [GridColumn(Title = "Moneda", Width = "50px")]
        public string CurrencyName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Logo")]
        [NotMappedColumn]
        public string Logo { get; set; }


        private string _accountNumber;

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Número de Cuenta")]
        [NotMappedColumn]
        public string AccountNumber
        {
            get { return _accountNumber ?? ""; }
            set { _accountNumber = value; }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Número de Cuenta")]
        [GridColumn(Title = "Número de Cuenta", Width = "150px")]
        public string RestrictAccountNumber { get { return ECOsystem.Utilities.Miscellaneous.GetDisplayAccountNumberMask(DisplayAccountNumber); } }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Número de Cuenta")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        [NotMappedColumn]
        public string DisplayAccountNumber
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(AccountNumber); }
            set { AccountNumber = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Activo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public bool IsActive { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Unit Of Capacity Id from Types
        /// </summary>
        [DisplayName("Unidad de Capacidad")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? UnitOfCapacityId { get; set; }

        /// <summary>
        /// Unit Of Capacity Name from Types
        /// </summary>
        [NotMappedColumn]
        public string UnitOfCapacityName { get; set; }

        /// <summary>
        /// Issue For Id from Types
        /// </summary>
        [DisplayName("Emisión de Tarjetas por")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? IssueForId { get; set; }

        /// <summary>
        /// Credit Card Type
        /// </summary>
        [GridColumn(Title = "Fleetcard", Width = "150px")]
        public string IssueForStr { get; set; }


        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Credit Card Type
        /// </summary>
        [DisplayName("Tipo de Tarjeta")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string CreditCardType { get; set; }


        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        [NotMappedColumn]
        public DateTime? InsertDate { get; set; }

        /// <summary>
        /// Insert Date as string
        /// </summary>
        [GridColumn(Title = "Creado", Width = "100px")]
        public string InsertDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(InsertDate); }
        }

        /// <summary>
        /// Admin Email
        /// </summary>
        [DisplayName("Correo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string AdminEmail { get; set; }

        /// <summary>
        /// Admin Identification
        /// </summary>
        [DisplayName("Identificación")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string AdminIdentification { get; set; }

        /// <summary>
        /// Admin Name
        /// </summary>
        [DisplayName("Administrador")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string AdminName { get; set; }

        /// <summary>
        /// Modulos Disponibles
        /// </summary>
        [DisplayName("Módulos habilitados")]
        [NotMappedColumn]
        public string AvailableModules { get; set; }

        /// <summary>
        /// Modulo E
        /// </summary>
        [DisplayName("Eficiencia")]
        [NotMappedColumn]
        public bool ModuleEfficiency { get; set; }

        /// <summary>
        /// Modulo C
        /// </summary>
        [DisplayName("Control")]
        [NotMappedColumn]
        public bool ModuleControl { get; set; }

        /// <summary>
        /// Modulo O
        /// </summary>
        [DisplayName("Operación")]
        [NotMappedColumn]
        public bool ModuleOperation { get; set; }

        /// <summary>
        /// Costo de Administración String
        /// </summary>
        [DisplayName("Costo Tarjeta Administ.")]
        [NotMappedColumn]
        public string CostCardAdminStr { get; set; }

        /// <summary>
        /// Costo de Administración
        /// </summary>
        [NotMappedColumn]
        public decimal CostCardAdmin { get; set; }

        /// <summary>
        /// Costo de Emision String
        /// </summary>
        [DisplayName("Costo Tarjeta Emision")]
        [NotMappedColumn]
        public string CostCardEmisionStr { get; set; }

        /// <summary>
        /// Costo de Emision
        /// </summary>
        [NotMappedColumn]
        public decimal CostCardEmision { get; set; }

        /// <summary>
        /// Costo de ModuleEO String
        /// </summary>
        [DisplayName("Costo Modulo E/O")]
        [NotMappedColumn]
        public string CostModuleEOStr { get; set; }

        /// <summary>
        /// Costo de ModuleEO
        /// </summary>
        [NotMappedColumn]
        public decimal CostModuleEO { get; set; }

          /// <summary>
        /// Modality of GPS
        /// </summary>
        [DisplayName("Modalidad de Uso del Dispositivo")]
        [NotMappedColumn]
        public int GPSModality { get; set; }



        /// <summary>
        /// Costo de Equipo Vendido
        /// </summary>
        [DisplayName("Costo Equipo Alquilado")]
        public decimal CostRentedGPS { get; set; }


        /// <summary>
        /// Costo de ModuleEO String
        /// </summary>
        [DisplayName("Costo Renta")]
        [NotMappedColumn]
        public string CostRentedGPSStr { get; set; }


        /// <summary>
        /// RoleName of the Customer
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// RoleName of the Customer Temp
        /// </summary>
        public string RoleNameTemp { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string CustomerName { get; set; }


        /// <summary>
        /// CustomerLogo
        /// </summary>
        public string CustomerLogo { get; set; }
        

    }

    /// <summary>
    /// Customers By Partner Data
    /// </summary>
    public class CustomerByPartnerData : ModelAncestor
    {
        /// <summary>
        /// default Constructor
        /// </summary>
        public CustomerByPartnerData()
        {
            AvailableCustomersList = new List<Customers>();
            ConnectedCustomersList = new List<Customers>();
        }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Partner model
        /// </summary>
        public int? PartnerId { get; set; }

        /// <summary>
        /// Partner Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Customer model
        /// </summary>

        public int? CustomerId { get; set; }

        /// <summary>
        /// Available VehicleGroup List
        /// </summary>
        public List<Customers> AvailableCustomersList { get; set; }

        /// <summary>
        /// Connected VehicleGroup List
        /// </summary>
        public IEnumerable<Customers> ConnectedCustomersList { get; set; }

        /// <summary>
        /// RoleName of the Customer
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// RoleName of the Customer Temp
        /// </summary>
        public string RoleNameTemp { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string CustomerName { get; set; }


        /// <summary>
        /// property
        /// </summary>        
        public string EncryptedName { get; set; }


        /// <summary>
        /// CustomerLogo
        /// </summary>
        public string CustomerLogo { get; set; }

        /// <summary>
        /// Country Id
        /// </summary>
        public int? CountryId { get; set; }

        /// <summary>
        /// Decrypted Customer Name
        /// </summary>
        /// 
        
        public string DecryptedCustomerName
        {
            get { return CustomerName != null ? ECOsystem.Utilities.TripleDesEncryption.Decrypt(CustomerName) : ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedName); }
            set { DecryptedCustomerName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }
    }

    /// <summary>
    /// CustomerByPartners Base
    /// </summary>
    public class CustomerByPartnersBase
    {
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public CustomerByPartnerData Data { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        [DisplayName("Socio")]
        public int? PartnerId { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Customer CreditCard Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class CustomerCreditCards : ModelAncestor
    {

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? CustomerCreditCardId { get; set; }


        /// <summary>
        /// Customer Id
        /// </summary>
        [NotMappedColumn]
        public int CustomerId { get; set; }


        private string _creditCardNumber;

        /// <summary>
        /// Credit Card Number
        /// </summary>
        [DisplayName("Número Tarjeta")]
        [NotMappedColumn]
        public string CreditCardNumber
        {
            get { return _creditCardNumber ?? ""; }
            set { _creditCardNumber = value; }
        }

        /// <summary>
        /// Credit Card Number with restrict display
        /// </summary>
        [DisplayName("Número Tarjeta")]
        [NotMappedColumn]
        public string RestrictCreditCardNumber { get { return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(DisplayCreditCardNumber); } }


        /// <summary>
        /// Credit Card Number for display
        /// </summary>
        [DisplayName("Número Tarjeta")]
        [Required(ErrorMessage = "{0} es requerido")]
        [RegularExpression(@"^(\d{4}-\d{4}-\d{4}-\d{4})$", ErrorMessage = "Formato de Tarjeta inválido")]
        [NotMappedColumn]
        public string DisplayCreditCardNumber
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(CreditCardNumber); }
            set { CreditCardNumber = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// Credit Card Expiration Year
        /// </summary>
        [DisplayName("Año de Expiración")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? ExpirationYear { get; set; }

        /// <summary>
        /// Credit Card ExpirationM onth
        /// </summary>
        [DisplayName("Mes de Expiración")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? ExpirationMonth { get; set; }

        /// <summary>
        /// Month Name
        /// </summary>
        [NotMappedColumn]
        public string ExpirationMonthName
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetMonthName(ExpirationMonth); }
        }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [NotMappedColumn]
        public decimal? CreditLimit { get; set; }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string CreditLimitStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(CreditLimit); }
        }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [NotMappedColumn]
        public string CreditLimitDisplayStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditLimit, CurrencySymbol); }
        }

        /// <summary>
        /// Status Id
        /// </summary>
        [DisplayName("Estado")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? StatusId { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencyName { get; set; }

        /// <summary>
        /// Expiration Date 
        /// </summary>
        [DisplayName("Fecha de Expiración")]
        [NotMappedColumn]
        public string ExpirationDate
        {
            get { return ExpirationMonthName + "/" + ExpirationYear; }
        }

        /// <summary>
        /// Status Name
        /// </summary>
        [NotMappedColumn]
        public string StatusName { get; set; }


        /// <summary>
        /// Credit Card Holder
        /// </summary>
        [DisplayName("Titular")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string CreditCardHolder { get; set; }

    }


    /// <summary>
    /// Customer Card Request Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class CardRequest : ModelAncestor
    {

        /// <summary>
        /// Card Request default creator
        /// </summary>
        public CardRequest()
        {

        }

        /// <summary>
        /// Card Request creator using CardRequestCsv data
        /// </summary>
        public CardRequest(CardRequestCsv data, int custId)
        {
            CustomerId = custId;
            PlateId = data.PlateId;
            DriverName = data.DriverName;
            DriverIdentification = data.DriverIdentification;
            DeliveryState = data.DeliveryState;
            DeliveryCounty = data.DeliveryCounty;
            DeliveryCity = data.DeliveryCity;
            AddressLine1 = data.AddressLine1;
            AddressLine2 = data.AddressLine2;
            PaymentReference = data.PaymentReference;
            AuthorizedPerson = data.AuthorizedPerson;
            ContactPhone = data.ContactPhone;
            EstimatedDelivery = DateTime.ParseExact(data.EstimatedDelivery, "yyyyMMdd", CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? CardRequestId { get; set; }


        /// <summary>
        /// Customer Id
        /// </summary>
        [NotMappedColumn]
        public int? CustomerId { get; set; }


        /// <summary>
        /// PlateId
        /// </summary>
        [DisplayName("Matrícula del Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string PlateId { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        [DisplayName("Nombre del Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }

        /// <summary>
        /// DriverName
        /// </summary>
        [DisplayName("Nombre del Conductor")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DriverName { get; set; }

        /// <summary>
        /// Decrypted Driver Name
        /// </summary>
        public string DecryptedDriverName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(DriverName); }
            set { DecryptedDriverName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// DriverIdentification
        /// </summary>
        [DisplayName("Identificación")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DriverIdentification { get; set; }

        /// <summary>
        /// AddressLine1
        /// </summary>
        [DisplayName("Dirección de Entrega")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string AddressLine1 { get; set; }

        /// <summary>
        /// AddressLine2
        /// </summary>
        [DisplayName("Otras Señas")]
        [NotMappedColumn]
        public string AddressLine2 { get; set; }

        /// <summary>
        /// PaymentReference
        /// </summary>
        [DisplayName("No. Deposito (COINCA)")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string PaymentReference { get; set; }

        /// <summary>
        /// AuthorizedPerson
        /// </summary>
        [DisplayName("Persona Autorizada")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string AuthorizedPerson { get; set; }


        /// <summary>
        /// ContactPhone
        /// </summary>
        [DisplayName("Teléfono")]
        [Required(ErrorMessage = "{0} es requerido")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        [NotMappedColumn]
        public string ContactPhone { get; set; }

        /// <summary>
        /// Estimated Delivery
        /// </summary>
        [DisplayName("Fecha de Entrega")]
        [NotMappedColumn]
        public DateTime? EstimatedDelivery { get; set; }

        /// <summary>
        /// Estimated Delivery as String
        /// </summary>
        [DisplayName("Fecha de Entrega")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string EstimatedDeliveryStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(EstimatedDelivery); }
            set { EstimatedDelivery = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Status Id
        /// </summary>
        [DisplayName("Estado")]
        [NotMappedColumn]
        public int? StatusId { get; set; }

        /// <summary>
        /// Status Name
        /// </summary>
        [NotMappedColumn]
        public string StatusName { get; set; }

        /// <summary>
        /// Issue For Id from Types
        /// </summary>
        [DisplayName("Emisión de Tarjetas por")]
        [NotMappedColumn]
        public int? IssueForId { get; set; }

        /// <summary>
        /// Country Id
        /// </summary>
        [NotMappedColumn]
        public int? CountryId { get; set; }

        /// <summary>
        /// State Id
        /// </summary>
        [DisplayName("Provincia")]
        [NotMappedColumn]
        [Required(ErrorMessage = "{0} es requerido")]
        public int? StateId { get; set; }

        /// <summary>
        /// State Name
        /// </summary>
        [NotMappedColumn]
        public string StateName { get; set; }

        /// <summary>
        /// County Id
        /// </summary>
        [DisplayName("Cantón")]
        [NotMappedColumn]
        [Required(ErrorMessage = "{0} es requerido")]
        public int? CountyId { get; set; }

        /// <summary>
        /// County Name
        /// </summary>
        [NotMappedColumn]
        public string CountyName { get; set; }

        /// <summary>
        /// City Id
        /// </summary>
        [DisplayName("Distrito")]
        [NotMappedColumn]
        [Required(ErrorMessage = "{0} es requerido")]
        public int? CityId { get; set; }

        /// <summary>
        /// City Name
        /// </summary>
        [NotMappedColumn]
        public string CityName { get; set; }

        /// <summary>
        /// Delivery State
        /// </summary>
        [DisplayName("Provincia")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DeliveryState { get; set; }

        /// <summary>
        /// Delivery County
        /// </summary>
        [DisplayName("Cantón")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DeliveryCounty { get; set; }

        /// <summary>
        /// Delivery City
        /// </summary>
        [DisplayName("Distrito")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string DeliveryCity { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// City Text for Grid
        /// </summary>
        [NotMappedColumn]
        public string CityText
        {
            get { return DeliveryCity == null || DeliveryCity == "" ? CityName : DeliveryCity; }
        }

        /// <summary>
        /// Decrypted Customer Name
        /// </summary>
        public string DecryptedCustomerName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(CustomerName); }
            set { DecryptedCustomerName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }
    }


    /// <summary>
    /// Customer Card Request CVS Model
    /// </summary>
    public class CardRequestCsv
    {
        /// <summary>
        /// PlateId
        /// </summary>
        public string PlateId { get; set; }

        /// <summary>
        /// DriverName
        /// </summary>
        public string DriverName { get; set; }

        /// <summary>
        /// DriverIdentification
        /// </summary>
        public string DriverIdentification { get; set; }

        /// <summary>
        /// Delivery State
        /// </summary>
        public string DeliveryState { get; set; }

        /// <summary>
        /// Delivery County
        /// </summary>
        public string DeliveryCounty { get; set; }

        /// <summary>
        /// Delivery City
        /// </summary>
        public string DeliveryCity { get; set; }

        /// <summary>
        /// AddressLine1
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// AddressLine2
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// PaymentReference
        /// </summary>
        public string PaymentReference { get; set; }

        /// <summary>
        /// AuthorizedPerson
        /// </summary>
        public string AuthorizedPerson { get; set; }


        /// <summary>
        /// ContactPhone
        /// </summary>
        public string ContactPhone { get; set; }

        /// <summary>
        /// Estimated Delivery
        /// </summary>
        public string EstimatedDelivery { get; set; }

    }

    /// <summary>
    /// Partner Models
    /// </summary>
    public class CustomersApi : ApiAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string CustomerAccountNumber { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string CustomerCreditCard { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string CustomerEmail { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public int PartnerId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string CurrencyCode { get; set; }
    }




    /// <summary>
    /// Customer Terminal Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class CustomerTerminals
    {        
         /// <summary>
        /// CustomersTerminal Constructor
        /// </summary>
        public CustomerTerminals()
        {
            TerminalList = new List<CustomerTerminals>();
        }
        // <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        /// 
        public IEnumerable<CustomerTerminals> TerminalList { get; set; }
                
        /// <summary>
        /// Customer Terminal Id
        /// </summary>
        [NotMappedColumn]
        [Display(Name = "ID de la Terminal")]
        [Required(ErrorMessage = "{0} requerido")]
        public string TerminalId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        [Display(Name = "Descripcion")]
        public string MerchantDescription { get; set; }
    }
}