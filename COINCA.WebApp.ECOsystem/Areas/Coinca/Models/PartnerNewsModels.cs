﻿using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Models
{
    /// <summary>
    /// Currencies Base
    /// </summary>
    public class PartnersNewsBase
    {
        /// <summary>
        /// PartnersBase default constructor 
        /// </summary>
        public PartnersNewsBase()
        {
            Data = new PartnerNews();
            List = new List<PartnerNews>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public PartnerNews Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PartnerNews> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    } 

    /// <summary>
    /// Partner News
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PartnerNews : ModelAncestor
    {
        /// <summary>
        /// NewsId
        /// </summary>
        [NotMappedColumn]
        public int? NewsId { get; set; }

        /// <summary>
        /// PartnerId
        /// </summary>
        [DisplayName("Entidad Bancaria")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int PartnerId { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [DisplayName("Título")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string Title { get; set; }

        /// <summary>
        /// Content
        /// </summary>
        [DisplayName("Contenido")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string Content { get; set; }

        /// <summary>
        /// Image
        /// </summary>
        [NotMappedColumn]
        public string Image { get; set; }

        /// <summary>
        /// Active
        /// </summary>
        [DisplayName("Activo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public bool IsActive { get; set; }
    }
}