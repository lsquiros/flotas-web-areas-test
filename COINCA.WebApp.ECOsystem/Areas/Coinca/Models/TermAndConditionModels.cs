﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Models
{
    public class TermsAndConditions
    {
        public int? Id { get; set; }

        public string Support { get; set; }

        public string Thefth { get; set; }

        public string Services { get; set; }

        public string Info { get; set; }

        public string Terms { get; set; }

        public int PartnerId { get; set; }

        public bool ShowTerms { get; set; }

        public bool Active { get; set; }
    }
}