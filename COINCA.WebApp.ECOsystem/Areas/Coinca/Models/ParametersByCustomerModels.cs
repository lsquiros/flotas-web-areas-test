﻿/************************************************************************************************************
*  File    : ParametersByCustomerModels.cs
*  Summary : Parameters By Customer Models
*  Author  : Andres Oviedo Brenes
*  Date    : 12/03/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;

namespace  ECOsystem.Areas.Coinca.Models
{

    /// <summary>
    /// Parameters By Customer Models
    /// </summary>
    public class ParametersByCustomers : ModelAncestor
    {      
        /// <summary>
        /// Indentifier of Model
        /// </summary>
        public int? ParameterByCustomerId { get; set; }

        /// <summary>
        /// Indentifier of Model
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Name of the parameter
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Value of the Parameter
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Key of the Model
        /// </summary>
        public string ResuourceKey { get; set; }

    }
}