﻿/************************************************************************************************************
*  File    : PartnerFuelModels.cs
*  Summary : PartnerFuel Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;
using System;

namespace  ECOsystem.Areas.Coinca.Models
{
    /// <summary>
    /// PartnerFuel Base
    /// </summary>
    public class PartnerFuelBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public PartnerFuelBase()
        {
            KeyId = 0;
            Data = new PartnerFuel();
            List = new List<PartnerFuel>();
            Menus = new List<AccountMenus>();
            Alarm = new AlarmsModels
            {
                EntityTypeId = 400,
                AlarmTriggerId = 515,
                TitlePopUp = "Alarma de Precios de Combustible"
            };
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public PartnerFuel Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PartnerFuel> List { get; set; }

        /// <summary>
        /// KeyId for load the information
        /// </summary>
        public int KeyId { get; set; }

        /// <summary>
        /// Menus 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Alarm
        /// </summary>
        public AlarmsModels Alarm { get; set; }
    }


    /// <summary>
    /// PartnerFuel Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PartnerFuel : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PartnerFuelId { get; set; }


        /// <summary>
        /// FK Vehicle Id from Users, each vehicle is associate with one user
        /// </summary>
        [NotMappedColumn]
        public int? PartnerId { get; set; }
        
        /// <summary>
        /// Liters of fuel
        /// </summary>
        [DisplayName("Combustible")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? FuelId { get; set; }

        /// <summary>
        /// Fuel Name
        /// </summary>
        [GridColumn(Title = "Combustible", Width = "150px", SortEnabled = true)]
        public string FuelName { get; set; }

        /// <summary>
        /// Liter Price
        /// </summary>
        [DisplayName("Precio por Litro")]                
        [NotMappedColumn]
        public decimal? LiterPrice { get; set; }

        /// <summary>
        /// LiterPriceStr
        /// </summary>
        [Required(ErrorMessage = "{0} es requerido")]        
        [GridColumn(Title = "Precio", Width = "100px", SortEnabled = true)]
        public string LiterPriceStr 
        {
            get { return (LiterPrice != null) ? ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR((decimal)LiterPrice).ToString() : "0"; }
            set { LiterPrice = Convert.ToDecimal(value, CultureInfo.CurrentCulture); }
        }
        
        /// <summary>
        /// Start Date
        /// </summary>
        [DisplayName("Desde")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Desde", Width = "150px", SortEnabled = true)]
        public string StartDate { get; set; }

        /// <summary>
        /// Start Date
        /// </summary>
        [DisplayName("Hasta")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Hasta", Width = "150px", SortEnabled = true)]
        public string EndDate { get; set; }

        /// <summary>
        /// Is Scheduled
        /// </summary>
        [NotMappedColumn]
        public bool IsScheduled { get; set; }

        /// <summary>
        /// Lapse
        /// </summary>
        [NotMappedColumn]
        public int Lapse { get; set; }

        /// <summary>
        /// Variation
        /// </summary>
        [NotMappedColumn]
        public decimal Variation { get; set; }

        /// <summary>
        /// Variation
        /// </summary>
        [NotMappedColumn]
        public int CountryId { get; set; }

        /// <summary>
        /// PartnerUnitOfCapacity
        /// </summary>
        [NotMappedColumn]
        public int CapacityUnitId { get; set; }
    }
}