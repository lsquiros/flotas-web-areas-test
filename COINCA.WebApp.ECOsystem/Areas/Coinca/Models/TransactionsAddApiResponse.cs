﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace  ECOsystem.Areas.Coinca.Models
{
    public class TransactionsAddApiResponse
    {
        public int TransactionId { get; set; }
        public bool IsInternal { get; set; }
    }
}
