﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Models
{
    public class MonthlyIndicators
    {
        public int PartnerId { get; set; }

        public string Name { get; set; }

        public int Active { get; set; }

        public int Block { get; set; }

        public int Process { get; set; }

        public int Delivered { get; set; }

        public int Closed { get; set; }

        public int Canceled { get; set; }

        public int Month { get; set; }

        public string MonthName
        {
            get { return Month > 12 ? Type == 2 ? "YTD" : "Acumulado"
                                    : ECOsystem.Business.Utilities.GeneralCollections.GetMonthNames.Where(x => x.Value == Month.ToString()).FirstOrDefault().Text; }
        }

        public int Type { get; set; }
    }
}