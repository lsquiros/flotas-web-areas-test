﻿using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Models
{
    public class StickersModelBase
    {

        /// <summary>
        /// Default class constructor
        /// </summary>
        public StickersModelBase()
        {
            Menus = new List<AccountMenus>();
        }
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    public class Stickers
    {
        public int? Id { get; set; }

        public string UUIDMIFARE { get; set; }

        public string EncryptedUID { get { return ECOsystem.Utilities.TripleDesEncryption.Encrypt(UUIDMIFARE); } }
    }
}