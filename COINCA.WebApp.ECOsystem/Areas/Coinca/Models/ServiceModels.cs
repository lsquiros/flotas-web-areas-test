﻿using ECOsystem.Models.Miscellaneous;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Models.Control
{
    public class WServiceModel : ModelAncestor
    {
        public int WSId { get; set; }

        public int PartnerId { get; set; }

        public string PartnerCode { get; set; }

        public string Name { get; set; }

        public string Environment { get; set; }

        public string UserAuth { get; set; }

        public string PasswordAuth { get; set; }

        public string Terminal { get; set; }

        public string WSType { get; set; }

        public string UriService { get; set; }

    }
}