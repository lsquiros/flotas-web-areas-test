﻿/************************************************************************************************************
*  File    : TestingApiTrxModels.cs
*  Summary : Testing Api Trx Models
*  Author  : Berman Romero
*  Date    : 04/Feb/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Web;

namespace  ECOsystem.Areas.Coinca.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class TestingApiTrx : RequestPartnerTransactionApi
    {
        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// JsonResponse
        /// </summary>
        public HtmlString JsonResponse { get; set; }
    }

    public class RequestPartnerTransactionApi
    {
        /// <summary>
        /// Invoice Number
        /// </summary>
        public string invoice { get; set; }

        /// <summary>
        /// transaction Type
        /// </summary>
        public string transactionType { get; set; }

        /// <summary>
        /// terminalId
        /// </summary>
        public string terminalId { get; set; }

        /// <summary>
        /// systemTraceNumber
        /// </summary>
        public string systemTraceNumber { get; set; }

        /// <summary>
        /// cardNumber
        /// </summary>
        public long? cardNumber { get; set; }

        /// <summary>
        /// expirationDate
        /// </summary>
        public int? expirationDate { get; set; }

        /// <summary>
        /// amount
        /// </summary>
        public decimal? amount { get; set; }

        /// <summary>
        /// carTag
        /// </summary>
        public string carTag { get; set; }

        /// <summary>
        /// kilometers
        /// </summary>
        public decimal? kilometers { get; set; }

        /// <summary>
        /// units
        /// </summary>
        public decimal? units { get; set; }

        /// <summary>
        /// Reference Number
        /// </summary>
        public string referenceNumber { get; set; }

        /// <summary>
        /// Authorization Number
        /// </summary>
        public string authorizationNumber { get; set; }

        /// <summary>
        /// Merchant Description
        /// </summary>
        public string merchantDescription { get; set; }
    }

}



