﻿/************************************************************************************************************
*  File    : TestingApiTrxModels.cs
*  Summary : Testing Api Trx Models
*  Author  : Berman Romero
*  Date    : 04/Feb/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Web;

namespace  ECOsystem.Areas.Coinca.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class TestingEncryption
    {
        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Result
        /// </summary>
        public string Result { get; set; }

    }
}


