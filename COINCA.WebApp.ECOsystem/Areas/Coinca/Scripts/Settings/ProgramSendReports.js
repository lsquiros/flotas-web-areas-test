﻿var ECO_Coinca = ECO_Coinca || {};

 ECOsystem.Areas.Coinca.ProgramSendReports = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);
        initToggle();

        $('.datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $(".txtStartDate").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $('.datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $(".txtEndDate").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $(".rdbWeekly").click(function () {
            var id = $(this).attr("reportId");
            toggleDisabledDays(id, false);
            $('#divDays-' + id).removeClass('hide');
            $('#divDayOfTheMonth-' + id).addClass('hide');            
        });

        $(".rdbMonthly").click(function () {
            var id = $(this).attr("reportId");
            toggleDisabledDays(id, true);
            $('#divDays-' + id).addClass('hide');
            $('#divDayOfTheMonth-' + id).removeClass('hide');
        });

        $(".rdbDaily").click(function () {
            var id = $(this).attr("reportId");
            toggleDisabledDays(id, true);
            $('#divDays-' + id).addClass('hide');
            $('#divDayOfTheMonth-' + id).addClass('hide');
        });

        $('.dbVehicles').select2('data', {});
        $('.dbVehicles').select2({
            formatResult: vehicleSelectFormat,
            formatSelection: vehicleSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('.dbCostCenter').select2('data', {});
        $('.dbCostCenter').select2({
            //formatResult: CostCenterSelectFormat,
            //formatSelection: CostCenterSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('dbVehicleUnit').select2('data', {});
        $('dbVehicleUnit').select2({
            formatResult: UnitIdSelectFormat,
            formatSelection: UnitIdSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('.dbDriver').select2('data', {});
        $('.dbDriver').select2({
            formatResult: DriverSelectFormat,
            formatSelection: DriverSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('dbVehicleGroup').select2('data', {});
        $('dbVehicleGroup').select2({            
            escapeMarkup: function (m) { return m; }
        });
                
        $("#confirmChanges").click(function () {
            saveChanges();
        });

        $(".addCostCenter").click(function () {
            var reportid = $(this).attr('reportidattr');

            $('#ValidationMessage_'+reportid).html('');
            $('[data-toggle="tooltip"]').tooltip('disable');

            var costCenterSplitted = $('#CostCenterId_' + reportid + ' option:selected').text().split(':');
            var name = '';

            if (costCenterSplitted.length > 1) {
                name = costCenterSplitted[1].replace(/'/g, '').replace('}', '');
            }
            else {
                name = costCenterSplitted[0].replace(/'/g, '').replace('}', '');
            }

            var emails = $('#txtEmailsCostCenter_'+reportid).val();

            if (name == '' || emails == '' || !validateInputValues(emails)) {
                $('#ValidationMessage_' + reportid).html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                          'El formato de los correos no es el correcto ó no se ha seleccionado un Centro de Costo</div>');
                return;
            }

            addCostCenterToTable($('#CostCenterId_' + reportid).val(), name, emails, reportid);
            $('#txtEmailsCostCenter_' + reportid).val('')
            $('#CostCenterId_' + reportid).val('').change();
        });
        
        $('.DayOfTheMonth').keypress(function (e) {
            //Only numbers
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;

            //Not higher than 31
            if (parseInt(this.value + "" + String.fromCharCode(e.charCode)) > 31 || parseInt(this.value + "" + String.fromCharCode(e.charCode)) < 1) return false;
        });

        $('.btnSendReportNow').click(function () {
            var reportId = $(this).attr('reportid');
            $.ajax({
                url: 'ProgramSendReports/SendTestEmail',
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify({ ReportId: reportId }),
                contentType: 'application/json',
                success: function (data) {
                    if (data == 'Success') ECOsystem.Utilities.setConfirmation();
                }
            });
        });
    };

    var getData = function () {
        $.ajax({
            url: 'ProgramSendReports/GetData',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                showData(data);
            }
        });
    };

    var toggleDisabledDays = function (id, change) {
        $(".Days_" + id).each(function () {
            $(this).attr("disabled", change);
            if (change) $(this).prop("checked", !change);
        });
    };

    var vehicleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-2">' + json.PlateId + '</div>' +
                '<div class="col-md-4">' + json.Name + '</div>' +
                '</div>';
    };

    var CostCenterSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
               '</div>';
    };

    var UnitIdSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
                '</div>';
    };

    var DriverSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
               '</div>';
    };

    var initToggle = function () {
        $('.toggleCheck').bootstrapToggle({
            on: 'Enviar',
            off: 'No Enviar'
        });
    };

    var saveChanges = function () {
        $('#confirmModal').modal('hide');
        
        if (validateAllEmails()) {
            showLoader();
            $('#ControlsContainerPanicBotton').html('');
            
            var list = obtainData();

            for (var i = 0; i < list.length; i++) {
                var costCenterList = obtainCostCenterEmailsData(list[i].Id);

                if ((costCenterList == null || costCenterList == undefined) && costCenterList.length > 0) {
                    $('#ValidationMessage_' + list[i].Id).html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                          'Por favor ingrese un correo</div>');
                    hideLoader();
                    return;
                }
                list[i].CostCenterList = costCenterList;
            }

            $.ajax({
                url: 'ProgramSendReports/SendProgramReportAddOrEdit',
                type: 'POST',
                data: JSON.stringify({list: list}),
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    window.location.reload();
                }
            });    
        }else {
            $('#ControlsContainer').html('<br />' +
                                         '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                         'Por favor verifique que los correos cuentan con el formato correcto. </div>');
            hideLoader();
        }
        
    };
    
    var obtainData = function () {
        var list = [];

        $(".reportCheck").each(function () {
            var check = $(this);
            var data = {}
            var id = check.attr("name");
            
            data.Id = id;

            if (check.prop("checked")) data.Active = 1; else data.Active = 0;

            data.Emails = $("#txtEmails_" + id).val();
            data.StartDate = parseDate($("#StartDate_" + id).val());
            data.EndDate = parseDate($("#EndDate_" + id).val());
            data.VehicleId = $("#VehicleId_" + id).val();
            data.Days = '';
            data.DriverId = $("#DriverId_" + id).val(); 

            if ($("#Monthly-2-" + id).prop("checked")) {
                data.Elapse = 30;
                data.Days = $('#DayOfTheMonth_' + id).val();                
            }else if($("#weekly-2-" + id).prop("checked")){
                data.Elapse = 7;
                if ($("#Sunday_" + id).prop("checked")) data.Days = data.Days + '1';
                if ($("#Monday_" + id).prop("checked")) data.Days = data.Days + ',2';
                if ($("#Tuesday_" + id).prop("checked")) data.Days = data.Days + ',3';
                if ($("#Wednesday_" + id).prop("checked")) data.Days = data.Days + ',4';
                if ($("#Thursday_" + id).prop("checked")) data.Days = data.Days + ',5';
                if ($("#Friday_" + id).prop("checked")) data.Days = data.Days + ',6';
                if ($("#Saturday_" + id).prop("checked")) data.Days = data.Days + ',7';                
            } else if ($("#Daily-2-" + id).prop("checked")) {
                data.Elapse = 1;                
            }

            list.push(data);            
        });
        return list;
    };

    var obtainCostCenterEmailsData = function (reportId) {
        var list = [];
        $('#listCostCenters_' + reportId + ' tr').each(function () {            
            var val = $(this).closest('tr').text();            
            if (val.split('|')[1] != undefined) {
                var arr = {};

                arr.CostCenterId = val.split('|')[0];
                arr.CostCenterEmails = val.split('|')[2];

                list.push(arr);
            }
        });
        return list; 
    }

    var validateAllEmails = function () {
        var val = true;
        $(".reportCheck").each(function () {
            var check = $(this);
            var id = check.attr("name");
            var emails = $("#txtEmails_" + id).val();

            if (emails == undefined) return;

            if (validateInputValues(emails)) {
                if (!val) return;
            } else {
                val = false;
            }            
        });
        return val;
    };

    var parseDate = function (datestr) {
        if (datestr == undefined) return null;

        var newDate = datestr.split("/");
        return new Date(newDate[1] + "/" + newDate[0] + "/" + newDate[2]);
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');      
    };

    var showData = function (data) {
        for (var i = 0; i < data.length; i++) {
            var id = data[i].Id;            
            
            $("#txtEmails_" + id).val(data[i].Emails);
            $("#StartDate_" + id).val(data[i].StartDateStr);
            $("#EndDate_" + id).val(data[i].EndDateStr);
            $("#VehicleId_" + id).val(data[i].VehicleId);
            
            if (data[i].CostCenterList != null || data[i].CostCenterList != undefined) {
                for (var x = 0; x < data[i].CostCenterList.length; x++) {
                    addCostCenterToTable(data[i].CostCenterList[x].CostCenterId, data[i].CostCenterList[x].CostCenterName, data[i].CostCenterList[x].CostCenterEmails, id);
                }
            }
            
            if (data[i].Elapse == 30) {
                $("#Monthly-2-" + id).prop("checked", true);
                $('#DayOfTheMonth_' + id).val(data[i].Days);
                $('#divDays-' + id).addClass('hide');
                $('#divDayOfTheMonth-' + id).removeClass('hide');
            } else if (data[i].Elapse == 7) {
                $("#weekly-2-" + id).prop("checked", true);
                toggleDisabledDays(id, false);
                for (var y = 0; y < data[i].Days.split(",").length; y++) {
                    switch (data[i].Days.split(",")[y]) {
                        case '1':
                            $("#Sunday_" + id).prop("checked", true);
                            break;
                        case '2':
                            $("#Monday_" + id).prop("checked", true);
                            break;
                        case '3':
                            $("#Tuesday_" + id).prop("checked", true);
                            break;
                        case '4':
                            $("#Wednesday_" + id).prop("checked", true);
                            break;
                        case '5':
                            $("#Thursday_" + id).prop("checked", true);
                            break;
                        case '6':
                            $("#Friday_" + id).prop("checked", true);
                            break;
                        case '7':
                            $("#Saturday_" + id).prop("checked", true);
                            break;
                    }
                }
                $('#divDays-' + id).removeClass('hide');
                $('#divDayOfTheMonth-' + id).addClass('hide');
            } else if (data[i].Elapse == 1) {
                $("#Daily-2-" + id).prop("checked", true);
                $('#divDays-' + id).addClass('hide');
                $('#divDayOfTheMonth-' + id).addClass('hide');
            }       
        }
    };

    var validateInputValues = function (emails) {
        if (emails != "") {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            var emailList = emails.split(';');

            for (i = 0; i < emailList.length; i++) {
                if (!re.test(emailList[i].trim())) {
                    return false;
                }
            }
        }        
        return true;
    };

    var addCostCenterToTable = function (costcenterid, costcentername, costcenteremails, reportid) {
        $('#listCostCenters_' + reportid + ' tbody').append('<tr>' +
                                            '<th class="hide">' + costcenterid + '|</th>' +                                        
                                            '<th>' + costcentername + '</th>' +
                                            '<th class="hide">|</th>' +
                                            '<th>' + costcenteremails + '</th>' +
                                            '<th class="hide">|</th>' +
                                            '<th  style="text-align:right;"><a href="#" class="delete-color btnDeleteRow" title="Eliminar" onclick="ECO_Admin.ProgramSendReports.DeleteRow(this)"><i class="mdi mdi-32px mdi-close-box"></i></a></th>' +
                                        '</tr>');
    }

    var deleteRow = function (obj) {
        obj.closest('tr').remove();
    }

    return {
        Init: initialize,
        GetData: getData,
        DeleteRow: deleteRow
    };
})();

