﻿var ECO_Coinca = ECO_Coinca || {};

 ECOsystem.Areas.Coinca.ParametersByPartner = (function () {
    var options = {};
    var line = 0;

    /* initialize function */

    var initialize = function (opts) {
        $.extend(options, opts);       

        $("#LoadMenuForm").submit();
        initCurrencyInputs();
    }

    /*init Currency Inputs*/
    var initCurrencyInputs = function () {
        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        try {

            $('body').off('blur.currencyOnBlur', 'input[data-currency], input[datacurrency]').on('blur.currencyOnBlur', 'input[data-currency], input[datacurrency]', function (e) {
                var obj = $(this);
                obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
                obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));
            });
            $('body').find('input[data-currency], input[datacurrency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });

        } catch (e) {

        }
    };

    /* Public methods */
    return {
        Init: initialize    
    };

})();
