﻿var ECO_Coinca = ECO_Coinca || {};

 ECOsystem.Areas.Coinca.TermsAndConditions = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $("#DDPartnerId").select2().data('select2');

        $('#btnAdd').click(function () {
            var partnerId = $("#DDPartnerId").val();
            if (partnerId > 1) {
                showloader();
                $('#loadForm').find('#PartnerId').val(partnerId);
                $('#loadForm').submit();
            } else {
                $("#DDPartnerId").select2('open');
            }
        });        
    }

    var handleTextArea = function () {
        $('.textAreaExpand').off('focusin.textAreaExpand').on('focusin.textAreaExpand', function () {
            $(this).attr('rows', 10);
        });

        $('.textAreaExpand').off('blur.textAreaExpand').on('blur.textAreaExpand', function () {
            $(this).attr('rows', 1);
        });
    }

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    var onSuccessLoad = function () {
        var partner = $("#DDPartnerId");
        $('#detailContainer').show('slow');
        $('#detailContainer').find('#PartnerId').val(partner.val());
        $('#detailContainer').find('#partnerName').html(partner.select2('data').text);
        handleTextArea();
        hideloader();
    }

    var onSuccessAddOrEdit = function () {
        $('#detailContainer').hide('slow');
        hideloader();
        ECOsystem.Utilities.setConfirmation();
    }

    return {
        Init: initialize,
        ShowLoader: showloader,
        OnSuccessLoad: onSuccessLoad,
        OnSuccessAddOrEdit: onSuccessAddOrEdit
    };
})();