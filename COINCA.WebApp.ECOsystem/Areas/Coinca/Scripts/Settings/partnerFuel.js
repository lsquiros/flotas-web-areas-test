﻿var ECO_Coinca = ECO_Coinca || {};

 ECOsystem.Areas.Coinca.PartnerFuel = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();

        var select2 = $("#KeyId").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectChange(data);
                    return fn.apply(this, arguments);
                }
            }
        })(select2.onSelect);
        
        $('#detailContainer').off('click.btnSubmit', 'button[type="submit"]').on('click.btnSubmit', 'button[type="submit"]', function (e) {
            $('#detailContainer').find('#PartnerId').val($('#KeyId').val());
            $('#LiterPrice').val($('#LiterPrice').val().replace(/,/g, ''));
        });

        $('#gridContainer').off('click.edit', 'a[editfuel_row]').on('click.edit', 'a[editfuel_row]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').find('#IsScheduled').val(false);
            $('#loadForm').submit();
        });

        $('#gridContainer').off('click.program', 'a[program_row]').on('click.program', 'a[program_row]', function (e) {           
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').find('#IsScheduled').val(true);            
            $('#loadForm').submit();            
        });

        //$('#detailContainer').find('#LiterPrice').numeric({
        //    allowMinus   : false
        //});

        $('#btnAddAlarm').click(function () {
            $('#alarmForm').find('#id').val(1);
            $('#alarmForm').find('#partnerId').val($('#KeyId').val());
            $('#alarmForm').submit();
        });
    };
        
    var GasSelectedFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +                
                '<div class="col-md-12">' + json.Name + '</div>' +
                '</div>';
    };

    var onSelectChange = function (obj) {
        if (obj.id === '') {
            $('#PartnerFuelDetail').html('');
        } else {
            $("#KeyId").val(obj.id);
            $('#ReloadForm').submit();
        }
    };

    var initEvents = function () {
        var dateV = $('#StartDate').val();

        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            startDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) {
            $('#datetimepickerEndDate').datepicker('remove');
            var dateV = new Date(e.date);
            var endDate = new Date($('#EndDate').val().split('/')[2] + '-' + $('#EndDate').val().split('/')[1] + '-' + $('#EndDate').val().split('/')[1]);

            //If the end date is higher than the start date force to select a new date
            if (dateV > endDate) $('#EndDate').val('');

            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: dateV,
                startDate: dateV,
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('click', function (e) {
                e.preventDefault();
            });

            $('#datetimepickerEndDate').datepicker('update');
        });

        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: dateV,
            startDate: dateV,
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    };
    
    var downloadModalShow = function () {
        $('#DownloadFileModal').on('shown.bs.modal', function (e) {           
            initDropDownDownload();
            initDatePickersDownload();
            e.stopImmediatePropagation();
        }).modal('show');
    };

    var downloadModal = function () {
        var partnerId = $('#KeyId').val();
        var partnerName = $('#KeyId option:selected').text();
        $('#loadDownloadForm').find('#PartnerId').val(partnerId);
        $('#loadDownloadForm').find('#PartnerName').val(partnerName);
        $('#loadDownloadForm').submit();
    };

    var downloadModalHide = function () {
        $('#DownloadFileModal').modal('hide');
        $('#DonwloadFileForm').submit();
    };
    
    var initDropDownDownload = function () {
        $('#ddxFuelId').select2('data', {});
        $('#ddxFuelId').select2({
            formatResult: GasSelectedFormat,
            formatSelection: GasSelectedFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
    };

    var initDatePickersDownload = function () {
        $('#datetimepickerStartDownloadDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) {
            $('#datetimepickerEndDownloadDate').datepicker('remove');
            var dateV = e.date;

            $('#datetimepickerEndDownloadDate').datepicker({
                language: 'es',
                minDate: dateV,
                startDate: dateV,
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('click', function (e) {
                e.preventDefault();
            });

            $('#datetimepickerEndDownloadDate').datepicker('update');
        });
    };

    var alarmModalShow = function () {
        $('#alarmModal').on('shown.bs.modal', function (e) {
            $("body").tooltip({ selector: '[data-toggle=tooltip]' });
        }).modal('show');        
    };

    var alarmModalClose = function () {
        $('#alarmModal').modal('hide');
    };
   
    return {
        Init: initialize,
        InitDatePickers: initEvents,
        DownloadModalShow: downloadModalShow,
        DownloadModal: downloadModal,
        DownloadModalHide: downloadModalHide,
        AlarmModalShow: alarmModalShow,
        AlarmModalClose: alarmModalClose
    };
})();

