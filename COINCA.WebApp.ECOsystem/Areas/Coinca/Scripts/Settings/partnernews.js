﻿var ECO_Coinca = ECO_Coinca || {};

 ECOsystem.Areas.Coinca.PartnerNews = (function () {
    var options = {};

    
    /* initialize function */
    
    var initialize = function (opts) {

        $.extend(options, opts);
        //trigger click on hidden file input
        $('#btnUpload').off('click.btnUpload').on('click.btnUpload', function (e) {
            $('#fileHidden').trigger('click');
            e.stopPropagation();
        });
        fileApiUpload();
    }


    
    /* Standard Error Function for File API */
    
    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };

    
    /* Update progress for File API */
    
    var fileApiUpdateProgress = function (e) {
        if (e.lengthComputable) {
            var percentLoaded = Math.round((e.loaded / e.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                $('.percent').css('width', percentLoaded + '%').html(percentLoaded + '%');
            }
        }
    };

    
    /* File Upload implementation */
    
    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            $('#fileHidden').off('change.fileHidden').on('change.fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onprogress = fileApiUpdateProgress;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {
                    $('#imgLogo').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        alert("La extensión del archivo es inválida");
                        continue;
                    }
                    // Closure to capture the file information.
                    reader.onload = (function (F) {
                        $('.percent').html('100%').css('width', '100%');
                        setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgLogo').removeClass('imgloading');", 2000);
                        return function (e) {
                            var lsrc = e.target.result;
                            $('#imgLogo').attr({
                                src: lsrc,
                                title: escape(F.name)
                            });
                            $('#Image').val(lsrc);
                        };
                    })(f);
                    // Read in the image file as a data URL.                    
                    reader.readAsDataURL(f);
                }
            });
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.');
        }
    };

    var onSuccessLoad = function () {
        initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    
    /* Public methods */
    
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad
    };
})();

