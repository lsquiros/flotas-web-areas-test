﻿var ECO_Coinca = ECO_Coinca || {};

 ECOsystem.Areas.Coinca.AdminPreauthorized = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $('#btnMasterCardSearch').off('click.btnMasterCardSearch').on('click.btnMasterCardSearch', function (e) {
            searchMasterCardModalShow();
        });

        $('#AccountNumber').off('blur.AccountNumber').on('blur.AccountNumber', function (e) {
            ValidatesCreditCard();
        });

        $('#Plate').off('blur.Plate').on('blur.Plate', function (e) {
            ValidatesPlate();
        });

        //$('#BACAfiliado').off('blur.BACAfiliado').on('blur.BACAfiliado', function () {
        //    GetServiceStation();
        //});

        $('#Odometer').off('blur.Odometer').on('blur.Odometer', function () {
            var plate = $('#Plate').val().match(/[\d\.]+/g);
            var odometer = $(this).val();

            if (plate == odometer) {
                $('#ConfirmationOdometerModal').modal('show');
            }
        });

        $('#btnOdometerFocus').off('click.btnOdometerFocus').on('click.btnOdometerFocus', function (e) {
            $('#ConfirmationOdometerModal').modal('hide');
            $('#Odometer').focus(); 
        });

        $('#Customer').off('focusin.Customer').on('focusin.Customer', function () {
            var accountNumber = $('#AccountNumber').val();
            var plate = $('#Plate').val();
            if (accountNumber == '' || plate == '') { $('#AccountNumber').focus(); }
        });

        $('#ExpirationDate').off('focusin.ExpirationDate').on('focusin.ExpirationDate', function () {
            var accountNumber = $('#AccountNumber').val();
            var plate = $('#Plate').val();
            if (accountNumber == '' && plate == '') { $('#AccountNumber').focus(); }
        });

        $('#BACAfiliado').off('focusin.BACAfiliado').on('focusin.BACAfiliado', function () {
            var accountNumber = $('#AccountNumber').val();
            var plate = $('#Plate').val();
            if (accountNumber == '' && plate == '') { $('#AccountNumber').focus(); }
        });

        $('#TerminalId').off('focusin.TerminalId').on('focusin.TerminalId', function () {
            var accountNumber = $('#AccountNumber').val();
            var plate = $('#Plate').val();
            if (accountNumber == '' && plate == '') { $('#AccountNumber').focus(); }
        });

        $('#ServiceStationName').off('focusin.ServiceStationName').on('focusin.ServiceStationName', function () {
            var accountNumber = $('#AccountNumber').val();
            var plate = $('#Plate').val();
            if (accountNumber == '' && plate == '') { $('#AccountNumber').focus(); }
        });

        $('#TotalAmountStr').off('focusin.TotalAmountStr').on('focusin.TotalAmountStr', function () {
            var accountNumber = $('#AccountNumber').val();
            var plate = $('#Plate').val();
            if (accountNumber == '' && plate == '') { $('#AccountNumber').focus(); }
        });

        $('#Odometer').off('focusin.Odometer').on('focusin.Odometer', function () {
            var accountNumber = $('#AccountNumber').val();
            var plate = $('#Plate').val();
            if (accountNumber == '' && plate == '') { $('#AccountNumber').focus(); }
        });

        $('#DriverCode').off('focusin.DriverCode').on('focusin.DriverCode', function () {
            var accountNumber = $('#AccountNumber').val();
            var plate = $('#Plate').val();
            if (accountNumber == '' && plate == '') { $('#AccountNumber').focus(); }
        });

        $('#Liters').off('focusin.Liters').on('focusin.Liters', function () {
            var accountNumber = $('#AccountNumber').val();
            var plate = $('#Plate').val();
            if (accountNumber == '' && plate == '') { $('#AccountNumber').focus(); }
        });

        $('#btnVoid').off('click.btnVoid').on('click.btnVoid', function (e) {
            $('body').loader('show');
            SetFormData('Void');
        });

        $('#btnReverse').off('click.btnReverse').on('click.btnReverse', function (e) {
            $('body').loader('show');
            SetFormData('Reverse');
        });

        $('#btnAcceptPreauthorize').off('click.btnAcceptPreauthorize').on('click.btnAcceptPreauthorize', function (e) {
            $('#plnAnswerData').addClass('hidden')
            $('#plnAuthorizationNumber').removeClass('hidden');
            $('#btnSaleControls').addClass('hidden');

            setTimeout(function () {
                $('#PreAuthorizationNumber').get(0).focus();
            }, 1);
        });

        $('#btnReverseTransaction').off('click.btnReverseTransaction').on('click.btnReverseTransaction', function (e) {
            window.location.reload();
        });

        $('#btnAcceptSale').off('click.btnAcceptSale').on('click.btnAcceptSale', function (e) {
            $('#btnSaleControls').removeClass('hidden');

            $('body').loader('show');
            SetFormData('Sale');
        });

        //$('#PreAuthorizationNumber').off('blur.PreAuthorizationNumber').on('blur.PreAuthorizationNumber', function () {
        //    var val = $('#PreAuthorizationNumber').val();

        //    if(val == "")
        //    {
        //        $('#PreAuthorizationNumberError').html('Por favor introduzca el número de autorización facilitado por el IVR');
        //        $('#PreAuthorizationNumber').get(0).focus();
        //        $('#btnPreAutorizedSale').attr('disabled', 'disabled');
        //        return;
        //    }
        //    $('#PreAuthorizationNumberError').html('');
        //    $('#btnPreAutorizedSale').removeAttr('disabled');
        //});

        $('#btnPreAutorizedSale').off('click.btnPreAutorizedSale').on('click.btnPreAutorizedSale', function (e) {
            var val = $('#PreAuthorizationNumber').val();

            if (val == "") {
                $('#PreAuthorizationNumberError').html('Por favor introduzca el número de autorización facilitado por el IVR');
                $('#PreAuthorizationNumber').get(0).focus();
                return;
            }

            $('body').loader('show');
            SetFormPreauthorize();
        });

        $('#btnAcceptInfoModal').off('click.btnAcceptInfoModal').on('click.btnAcceptInfoModal', function (e) {
            var val = $('#CloseAttr').val();
            if (val != "") {
                $('body').loader('show');
                window.location.reload();
            }

            return;
        });

        $('#btnProcessPreauthorizedSale').off('click.btnProcessPreauthorizedSale').on('click.btnProcessPreauthorizedSale', function (e) {
            $('#SetTransactionProcessedForm').submit();
        });

        $('#btnReversePreauthorizedSale').off('click.btnReversePreauthorizedSale').on('click.btnReversePreauthorizedSale', function (e) {
            $('#SetTransactionReverseForm').submit();
        });

        $('#gridContainer').off('click.process', 'a[data-execute_transaction]').on('click.process', 'a[data-execute_transaction]', function (e) {
            $('#SetTransactionProcessModal').find('#TransactionId').val($(this).attr('data-transactionid'));
            $('#SetTransactionProcessModal').modal('show');
        });

        $('#gridContainer').off('click.reverse', 'a[data-execute_reverseorvoid]').on('click.reverse', 'a[data-execute_reverseorvoid]', function (e) {
            $('#SetTransactionReverseModal').find('#TransactionId').val($(this).attr('data-transactionid'));
            $('#SetTransactionReverseModal').find('#TotalAmount').val($(this).attr('data-amount'));
            $('#SetTransactionReverseModal').modal('show');
        });

        $('#btnDownloadBill').click(function () {
            $('#DonwloadBillForm').submit();
        });

        $('#btnSendBillEmail').click(function () {
            sendBillEmail($('#txtEmail').val());
        });
    };

    var sendBillEmail = function (email) {
        if (!validateEmails(email)) {
            $('#emailValidation').show();
            return false;
        }

        $('#emailValidationSent').hide();
        $('#emailValidation').hide();
        $('body').loader('show');

        $.ajax({
            url: 'VPOS/SendBillEmail',
            type: 'POST',
            data: JSON.stringify({ Email: email }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $('body').loader('hide');
                $('#emailValidationSent').show();
            }
        });

    };

    var SetControlsForTransaction = function () {
        $("#Customer").attr('disabled', 'disabled');
        $("#AccountNumber").attr('disabled', 'disabled');
        $("#ExpirationDate").attr('disabled', 'disabled');
        $("#TotalAmount").attr('disabled', 'disabled');
        $("#TotalAmountStr").attr('disabled', 'disabled');
        $("#Odometer").attr('disabled', 'disabled');
        $("#Liters").attr('disabled', 'disabled');
        $("#TerminalId").attr('disabled', 'disabled');
        $("#DriverCode").attr('disabled', 'disabled');
        $("#BACAfiliado").attr('disabled', 'disabled');
        $("#Plate").attr('disabled', 'disabled');
        $("#ServiceStationName").attr('disabled', 'disabled');
        $("#btnTransactionRulesValidate").addClass('hidden');
        $("#btnCancelProcess").addClass('hidden');
    };

    var searchMasterCardModalShow = function () {
        var creditCardId = $('#CreditCardId').val();
        var accountNumber = $('#AccountNumber').val();
        var customerId = $('#CustomerId').val();
        var customerName = $('#Customer').val();
        var plate = $('#Plate').val();

        $('#SearchCreditCardId').val(creditCardId);
        $('#searchAccountNumber').val(accountNumber);
        $('#SearchCustomerId').val(customerId);
        $('#searchCustomer').val(customerName);
        $('#searchPlate').val(plate);

        $('#SearchMasterCardModal').modal('show');
    };

    var ValidatesCreditCard = function () {
        var accountNumber = $('#AccountNumber').val();
        var plate = '';

        //if (accountNumber == '') { $('#AccountNumber').focus(); }

        accountNumber = accountNumber.replace(/\-/g, '');

        var finalText = '';
        var c = 1;

        for (var i = 0; i < accountNumber.length; i++) {

            if (c == 4 && i < 13) {
                finalText = finalText + accountNumber.split('')[i] + '-';
                c = 1;
            }
            else {
                finalText = finalText + accountNumber.split('')[i];
                c++;
            }
        }

        $('#AccountNumber').val(finalText);
        accountNumber = finalText;

        if (accountNumber != "") {
            $('body').loader('show');
            $.ajax({
                url: 'VPOS/ValidatesCreditCard',
                type: 'POST',
                data: JSON.stringify({ AccountNumber: accountNumber, PlateId: plate }),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (Data) {

                    $('#CreditCardId').val(Data.CreditCardId);
                    if (accountNumber == '') $('#AccountNumber').val(Data.DisplayCreditCardNumber);
                    $('#CustomerId').val(Data.CustomerId);
                    $('#Customer').val(Data.DecryptedCustomerName);
                    $('#Plate').val(Data.VehiclePlate);
                    $('#ExpirationDate').val(Data.ExpirationDate);

                    GetMasterCard();
                    ValidateDeniedTransaction(accountNumber);

                },
                error: function (xhr) {
                    $('#textMessage').html('La tarjeta ' + accountNumber + ' no existe en Flotas');
                    $('#InformationMessageModal').modal('show');
                    $('body').loader('hide');
                    $('#Plate').val('');
                }
            });
        }
        else {
            $('#CreditCardId').val('');
            $('#CustomerId').val('');
            $('#Customer').val('');
            $('#Plate').val('');
            $('#ExpirationDate').val('');
            $('#MasterCardNumber').val('');
            $('#MasterExpirationDate').val('');
        }
    };

    var setCreditCardFormat = function () {
        var accountNumber = $('#txtSearch').val();

        accountNumber = accountNumber.replace(/\-/g, '');

        var finalText = '';
        var c = 1;

        for (var i = 0; i < accountNumber.length; i++) {

            if (c == 4 && i < 13) {
                finalText = finalText + accountNumber.split('')[i] + '-';
                c = 1;
            }
            else {
                finalText = finalText + accountNumber.split('')[i];
                c++;
            }
        }

        $('#txtSearch').val(finalText);
    };

    var ValidatesPlate = function () {
        var accountNumber = '';
        var plate = $('#Plate').val();

        if (plate.split(': ').length > 1) { plate = plate.split(': ')[1]; }

        if (plate != '') {
            $('body').loader('show');
            $.ajax({
                url: 'VPOS/ValidatesCreditCard',
                type: 'POST',
                data: JSON.stringify({ AccountNumber: accountNumber, PlateId: plate }),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (Data) {

                    $('#CreditCardId').val(Data.CreditCardId);
                    if (accountNumber == '') $('#AccountNumber').val(Data.DisplayCreditCardNumber);
                    $('#CustomerId').val(Data.CustomerId);
                    $('#Customer').val(Data.DecryptedCustomerName);
                    $('#Plate').val(Data.VehiclePlate);
                    $('#ExpirationDate').val(Data.ExpirationDate);

                    GetMasterCard();
                    ValidateDeniedTransaction(accountNumber);

                },
                error: function (xhr) {
                    $('#textMessage').html('El número de placa ' + plate + ' no esta asociado a una tarjeta en Flotas');
                    $('#InformationMessageModal').modal('show');
                    $('body').loader('hide');
                    $('#AccountNumber').val('');
                }
            });
        }
        else {
            $('#CreditCardId').val('');
            $('#CustomerId').val('');
            $('#Customer').val('');
            $('#ExpirationDate').val('');
            $('#MasterCardNumber').val('');
            $('#MasterExpirationDate').val('');
        }
    };

    var ValidateDeniedTransaction = function (obj) {
        $.ajax({
            url: 'VPOS/ValidateDeniedTransaction',
            type: 'POST',
            data: JSON.stringify({ AccountNumber: obj }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (Data) {
                $('#textMessage').html(Data);
                $('#InformationMessageModal').modal('show');
                $('body').loader('hide');
            },
            error: function (xhr) {
                $("#errorMessage").html('Error: ' + xhr.statusText);
            }
        });
    };

    var GetMasterCard = function () {
        var creditCardId = $('#CreditCardId').val();
        var customerId = $('#CustomerId').val();
        var plate = $('#Plate').val().split(': ')[1];

        $.ajax({
            url: 'VPOS/SearchMasterCard',
            type: 'POST',
            data: JSON.stringify({ CreditCardId: creditCardId, CustomerId: customerId, Plate: plate }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (Data) {
                $('#MasterCardNumber').val(Data.DisplayCreditCardNumber);
                $('#MasterExpirationDate').val(Data.ExpirationDate);
                $('#MasterCardNumberText').val(Data.DisplayCreditCardNumber);
                $('#MasterExpirationDateText').val(Data.ExpirationDate);
            },
            error: function (xhr) {
                $("#errorMessage").html('Error: ' + xhr.statusText);
            }
        });
    };

    var GetServiceStation = function () {
        var afiliado = $('#BACAfiliado').val();

        if (afiliado == '') {
            $('#BacAfiliadoRequired').html('El número de afiliado es requerido');
        }
        else {
            $('#BacAfiliadoRequired').html('');
            $('#bacidinformation').hide();
            $.ajax({
                url: '/VPOS/GetServiceStationName',
                type: 'POST',
                data: JSON.stringify({ bacAfiliado: afiliado }),
                dataType: 'json',
                contentType: 'application/json',
                success: function (result) {
                    $('#ServiceStationName').val(result);
                    $("#errorMessage").html('');
                },
                error: function (xhr) {
                    $('#ServiceStationName').val('');
                    $('#bacidinformation').show('slow');
                }
            });
        }
    };

    var loaderShow = function () {
        $('body').loader('show');
    }

    var loaderHide = function (data) {
        
        $('body').loader('hide');

        if (data.result != "") {
            $('#InformationMessageModal').modal('show');

            window.location.reload();
        }        
    }

    var validateEmails = function (email) {
        if (email != "") {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var emailList = email.split(';');

            for (i = 0; i < emailList.length; i++) {
                if (!re.test(emailList[i].trim())) {
                    return false;
                }
            }
        }
        return true;
    };

    var SetFormData = function (transaction) {
        var plate = $("#Plate").val();
        if (plate.split(': ').length > 1) { plate = plate.split(': ')[1]; }

        $("#errorMessage").html("");
        var creditCardTransaction = {
            CreditCardId: $("#CreditCardId").val(),
            PartnerId: $("#partnerId").val(),
            CustomerId: $("#CustomerId").val(),
            Customer: $("#Customer").val(),
            AccountNumber: $("#AccountNumber").val(),
            ExpirationDate: $("#ExpirationDate").val(),
            TotalAmountStr: $("#TotalAmountStr").val(),
            Odometer: $("#Odometer").val(),
            Liters: $("#Liters").val(),
            MasterCardNumber: $('#MasterCardNumber').val(),
            MasterExpirationDate: $('#MasterExpirationDate').val(),
            Plate: plate,
            AuthorizationNumber: 0,
            ReferenceNumber: 0,
            SystemTraceNumber: 0,
            TerminalId: $("#TerminalId").val(),
            DriverCode: $("#DriverCode").val(),
            BACAfiliado: $("#BACAfiliado").val(),
            ServiceStationName: $('#ServiceStationName').val()
        };

        $.ajax({
            url: '/VPOS/ExecuteCreditCard' + transaction,
            type: 'POST',
            data: JSON.stringify(creditCardTransaction),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            async: true,
            success: function (result) {
                $('body').loader('hide');

                if (result.Data == "LapseTime") {
                    $('#textMessage').html('El tiempo de espera para realizar la transacción ha expirado. <br /> Por favor inténtelo nuevamente.');
                    $('#InformationMessageModal').modal('show');
                    $('#CloseAttr').val("1");
                    return;
                }

                if (result.Data.TransactionType == 'PREAUTHORIZED_SALE') {
                    SetControlsForTransaction();

                    $('#ContactLink').replaceWith(function () {
                        return '<a href="' + result.Data.UrlTicket + '" target="_blank"> AQUÍ </a>';
                    });
                    $('#BacErrorMessage').html(result.Data.ErrorMessage);
                    $('#ContactPhone').html(result.Data.PhoneNumber);

                    $('#PreauthorizeMessageModal').modal('show');
                    $('#TransactionType').val(result.Data.TransactionType);
                    $('#SystemTraceNumber').val(result.Data.SystemTraceNumber);
                    $('#Invoice').val(result.Data.Invoice);
                }
                else {
                    SetControlsForTransaction();
                    $("#authorizationNumber").html('Número de Autorización: ' + result.Data.AuthorizationNumber);
                    $('#InformationTxProcessMessageModal').modal('show');

                    if (result.Data.ErrorMessage != null && result.Data.ErrorMessage != "") {
                        $("#errorMessage").html("Se detectó un error no controlado en el proceso de compra en línea (VPOS), por favor contactar a un administrador de soporte técnico he indique los pasos realizados.");
                    }
                    else {
                        ReverseOrVoid();
                    }
                }
            },
            error: function (xhr) {
                $('body').loader('hide');
                $('#textMessage').html('Se presentó un error durante la transacción. <br /> Por favor intentelo nuevamente');
                $('#InformationMessageModal').modal('show');
                $('#CloseAttr').val("");
            }
        });
    }

    var SetFormPreauthorize = function () {
        var plate = $("#Plate").val();

        if (plate.split(': ').length > 1) { plate = plate.split(': ')[1]; }

        $("#errorMessage").html("");
        var creditCardTransaction = {
            CreditCardId: $("#CreditCardId").val(),
            PartnerId: $("#partnerId").val(),
            CustomerId: $("#CustomerId").val(),
            Customer: $("#Customer").val(),
            AccountNumber: $("#AccountNumber").val(),
            ExpirationDate: $("#ExpirationDate").val(),
            TotalAmountStr: $("#TotalAmountStr").val(),
            Odometer: $("#Odometer").val(),
            Liters: $("#Liters").val(),
            MasterCardNumber: $('#MasterCardNumber').val(),
            MasterExpirationDate: $('#MasterExpirationDate').val(),
            Plate: plate,
            SystemTraceNumber: $('#SystemTraceNumber').val(),
            AuthorizationNumber: $('#PreAuthorizationNumber').val(),
            TerminalId: $("#TerminalId").val(),
            DriverCode: $("#DriverCode").val(),
            BACAfiliado: $("#BACAfiliado").val(),
            ServiceStationName: $('#ServiceStationName').val(),
            TransactionType: $('#TransactionType').val(),
            Invoice: $("#Invoice").val()
        };

        $.ajax({
            url: '/VPOS/AddPreauthorizeTransaction',
            type: 'POST',
            data: JSON.stringify(creditCardTransaction),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            async: true,
            success: function (result) {
                $('body').loader('hide');

                if (result != "") {
                    $('#textMessage').html('La transacción pre-autorizada ha sido procesada exitosamente');
                    $('#InformationMessageModal').modal('show');
                    $('#CloseAttr').val("1");
                }
            },
            error: function (xhr) {
                $('body').loader('hide');
                $('#textMessage').html('Se presentó un error durante la transacción. <br /> Por favor intentelo nuevamente');
                $('#InformationMessageModal').modal('show');
                $('#CloseAttr').val("");
            }
        });
    };

    var ReverseOrVoid = function () {
        $("#btnSale").attr("disabled", true);
        $("#totalAmount").attr("disabled", true);
        $("#odometer").attr("disabled", true);
        $("#liters").attr("disabled", true);
        $("#plate").attr("disabled", true);

        $("#btnReverse").attr("disabled", false);
        $("#btnVoid").attr("disabled", false);
    }

    return {
        Init: initialize,
        LoaderShow: loaderShow,
        LoaderHide: loaderHide,
        SetCreditCardFormat: setCreditCardFormat
    };
})();