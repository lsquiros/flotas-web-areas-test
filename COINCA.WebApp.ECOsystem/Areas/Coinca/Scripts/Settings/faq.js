﻿var ECO_Coinca = ECO_Coinca || {};

 ECOsystem.Areas.Coinca.Faq = (function () {
    var options = {};
    var line = 0;

    var initialize = function (opts) {
        $.extend(options, opts);

        $("#faqFileInput").filestyle({
            buttonText: "&nbsp;Seleccionar archivo...",
            buttonBefore: true
        });

        $("#btnSubmitUpload").off("click").on("click", function () {
            //alert("Hola")
            $("#frmUploadFAQ").submit();
        });

        //GUI Messages
        showMessage();
    }

    var showMessage = function ()
    {
        var currentMessage = $("#FAQHiddenMessage").val();
        var typeMessage = $("#TypeHiddenMessage").val();
        if (currentMessage != null && currentMessage != "")
        {
            switch (typeMessage) {
                case "ERROR":
                    ECOsystem.Utilities.SetMessageShow(currentMessage, typeMessage);
                    break;
                case "ALERT":
                    ECOsystem.Utilities.SetMessageShow(currentMessage, typeMessage);
                    break;
                case "INFO":
                    ECOsystem.Utilities.SetMessageShow(currentMessage, typeMessage);
                    break;
                case "SUCCESS":
                    ECOsystem.Utilities.SetMessageShow(currentMessage, typeMessage);
                    break;
                default:
                    break;
            }
        }
    }

    return {
        Init: initialize
    }
})();