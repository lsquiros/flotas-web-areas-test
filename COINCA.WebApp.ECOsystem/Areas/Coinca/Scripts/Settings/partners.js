﻿var ECO_Coinca = ECO_Coinca || {};

 ECOsystem.Areas.Coinca.Partners = (function () {
    var options = {};

    var newUserList = [];

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        onClickbtnUpload();
        adjustForm();
        onClickNew();
        initChangePassword();
        onClickbtnSavePartnerInfo();
    }

    /* trigger click on hidden file input */
    var onClickbtnUpload = function () {
        $('#addOrEditForm').off('click.btnUpload', '#btnUpload').on('click.btnUpload', '#btnUpload', function (e) {
            $('#fileHidden').trigger('click');
            e.stopPropagation();
        });

        fileApiUpload();
    };

    /* trigger click on hidden file input */
    var onClickbtnSavePartnerInfo  = function () {
        $('#addOrEditForm').off('click.btnSavePartnerInfo', '#btnSavePartnerInfo').on('click.btnSavePartnerInfo', '#btnSavePartnerInfo', function (e) {

            var role = $("#RolList option:selected").text();
            var strRolDefect = "Seleccione un Rol";
            setErrorMsjRole('', false);
            if ($("#RolList option:selected").text() == strRolDefect) {
                if ($("#RolList option").length > 0) {
                    $("#RolList").prop("selectedIndex", 1);
                }
            }

            if ($('#ChangePassword').is(":checked")) {
                var pass = $('#DisplayApiPassword').val();
                var confirmpass = $('#PasswordConfirm').val();
                if (pass.length > 0 && confirmpass.length > 0 && pass != confirmpass) {
                    setGlobalErrorMsj('Error validando la contraseña', true);
                    e.stopPropagation();
                    return false;
                }
            }
            var registeredUsers = $('#tableRegisteredUsers').html().trim();
            if (registeredUsers.length == 0 && newUserList.length == 0) {
                setGlobalErrorMsj('Se requiere información de al menos una usuario.', true);
                e.stopPropagation();
                return false;
            }

            return true;
        });
    };

    /* Show or hide errors messages*/
    var setGlobalErrorMsj = function (msj, show) {
        if (show) {
            $('#errorValidationMsj').html(msj);
            $('#globalValidation').removeClass('hide');
        } else {
            $('#errorValidationMsj').html('');
            $('#globalValidation').addClass('hide');
        }
    }

    /*initCheckUsername*/
    var initCheckUsername = function () {
        $('#addOrEditForm').off('change.txtTempEmail', '#txtTempEmail').on('change.txtTempEmail', '#txtTempEmail', function (e) {
            if (validateEmail()) {
                $('#checkForm').find('#username').val($(this).val());
                $('#checkForm').submit();
            }
        });
    }

    /* initChangePassword */
    var initChangePassword = function() {
        $('#addOrEditForm').off('change.ChangePassword', '#ChangePassword').on('change.ChangePassword', '#ChangePassword', function (e) {
            if ($(this).is(":checked")) {
                $('div[data-name="PasswordAPI"]').removeClass('hide');
                $('#DisplayApiPassword').val('');
                $('#PasswordConfirm').val('');
            } else {
                $('div[data-name="PasswordAPI"]').addClass('hide');
                $('#DisplayApiPassword').val('000');
                $('#PasswordConfirm').val('000');
            }
        });
    }

    /* Name validation to prevent an invalid submission*/
    var validateName = function () {
        setErrorMsjName('', false);
        if ($('#txtTempName').val().length == 0) {
            setErrorMsjName('Nombre completo de usuario es requerido', true);
            return false;
        }
        return true;
    };

    /* Email validation to prevent an invalid submission*/
    var validateEmail = function () {
        setErrorMsjEmail('', false);
        if ($('#txtTempEmail').val().length == 0) {
            setErrorMsjEmail('Correo electrónico es requerido', true);
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#txtTempEmail').val().match(email)) {
            return true;
        } else {
            setErrorMsjEmail('Correo electrónico invalido', true);
            return false;
        }
    };

    /* Name validation to prevent an invalid submission*/
    var validateRole = function () {
        var strRolDefect = "Seleccione un Rol";
        setErrorMsjRole('', false);
        if ($("#RolList option:selected").text() == strRolDefect) {
            setErrorMsjRole('Rol es requerido', true);
            return false;
        }
        return true;
    };

    /* Show or hide errors messages*/
    var setErrorMsjName = function (msj, show) {
        if (show) {
            $('#errorUserMsjName').html(msj);
            $('#validationUserName').removeClass('hide');
        } else {
            $('#errorUserMsjName').html('');
            $('#validationUserName').addClass('hide');
        }
    }

    /* Show or hide errors messages*/
    var setErrorMsjEmail = function (msj, show) {
        if (show) {
            $('#errorUserMsjEmail').html(msj);
            $('#validationUserEmail').removeClass('hide');
        } else {
            $('#errorUserMsjEmail').html('');
            $('#validationUserEmail').addClass('hide');
        }
    }

    /* Show or hide errors messages*/
    var setErrorMsjRole = function (msj, show) {
        if (show) {
            $('#errorUserMsjRole').html(msj);
            $('#validationUserRole').removeClass('hide');
        } else {
            $('#errorUserMsjRole').html('');
            $('#validationUserRole').addClass('hide');
        }
    }

    /* On click BtnNew*/
    var onClickNew = function () {
        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });

        $('#addOrEditForm').off('click.btnAddUser', '#btnAddUser').on('click.btnAddUser', '#btnAddUser', function (e) {
             
            var name = $('#txtTempName').val();
            var email = $('#txtTempEmail').val();
            var role = $("#RolList option:selected").text();

            if (validateName() && validateEmail() && validateRole()) {

                newUserList.push({ name: name, email: email, role : role });
                reloadTableUsers();
                $('#txtTempName').val('');
                $('#txtTempEmail').val('');
            }
        });

        $('#addOrEditForm').off('click.data-delete-row', 'a[data-delete-row]').on('click.data-delete-row', 'a[data-delete-row]', function (e) {
            var index = parseInt($(this).attr('data-index'));
            newUserList.splice(index, 1);
            reloadTableUsers();
        });
    };

    /*reloadTableUsers*/
    var reloadTableUsers = function () {
        $('#tableUser').html('');
        for (var i = 0; i < newUserList.length; i++) {
            addUserToTable(newUserList[i].name, newUserList[i].email, newUserList[i].role, i);
        }
    };
    
    /*add User To Table*/
    var addUserToTable = function (name, email, role, i) {
        $('#tableUser').append('<tr>' +
            '<td class="grid-cell">' + name + '</td>' +
            '<td class="grid-cell">' + email + '</td>' +
            '<td class="grid-cell">' + role + '</td>' +
            '<td class="grid-cell grid-col-btn">' +
            '   <span>' +
            '           <a href="#" class="delete-color" title="Eliminar" data-index="' + i + '" data-delete-row><i class="mdi mdi-close"></i></a>' +
            '   </span>' +
            '   <input value="' + name + '" type="hidden" name="TempUsersList[' + i + '].UserFullName" /> ' +
            '   <input value="' + email + '" type="hidden" name="TempUsersList[' + i + '].UserEmail" />' +
            '   <input value="' + role + '" type="hidden" name="TempUsersList[' + i + '].RoleName" />' +
            '</td></tr>');
    };

    /* adjust Form */
    var adjustForm = function () {
        $("#PartnerTypeId").change(function () {
            var type = $("#PartnerTypeId option:selected").val();
            if (type == 800) {
                $(".financial").css("display", "block");
            }
            else {
                $(".financial").css("display", "none");
            }
        });
    }

    /* Standard Error Function for File API */
    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };

    /* Update progress for File API */
    var fileApiUpdateProgress = function (e) {
        if (e.lengthComputable) {
            var percentLoaded = Math.round((e.loaded / e.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                $('.percent').css('width', percentLoaded + '%').html(percentLoaded + '%');
            }
        }
    };
    
    /* File Upload implementation */
    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            $('#addOrEditForm').off('change.fileHidden', '#fileHidden').on('change.fileHidden', '#fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onprogress = fileApiUpdateProgress;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {
                    $('#imgLogo').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    if (files[i].size < 100000) {
                        // Only process image files.
                        if (!f.type.match('image.*')) {
                            alert("La extensión del archivo es inválida");
                            continue;
                        }
                        // Closure to capture the file information.
                        reader.onload = (function (F) {
                            $('.percent').html('100%').css('width', '100%');
                            setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgLogo').removeClass('imgloading');", 2000);
                            return function (e) {
                                var lsrc = e.target.result;
                                $('#imgLogo').attr({
                                    src: lsrc,
                                    title: escape(F.name)
                                });
                                $('#Logo').val(lsrc);
                            };
                        })(f);
                        // Read in the image file as a data URL.
                        reader.readAsDataURL(f);
                    } else {
                        ECOsystem.Utilities.SetMessageShow('El tamaño máximo para el archivo son 100KB.', "ERROR");
                        break;
                    }
                }
            });
        } else {
            ECOsystem.Utilities.SetMessageShow('La opción de subir imágenes no es soportada por este navegador.', "ERROR");
        }
    };

    var onSuccessLoad = function () {
        newUserList = [];
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad
    };
})();

