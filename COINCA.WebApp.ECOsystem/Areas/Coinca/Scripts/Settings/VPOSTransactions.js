﻿var ECO_Coinca = ECO_Coinca || {};

 ECOsystem.Areas.Coinca.VPOSTransactions = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        var select1 = $("#Parameters_ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        $("#Parameters_Month").select2('data', {});
        $("#Parameters_Month").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
        $("#Parameters_Year").select2('data', {});
        $("#Parameters_Year").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
        $("#Parameters_CustomerId").select2('data', {});
        $("#Parameters_CustomerId").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) { });

        $("#Parameters_StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) { });

        $("#Parameters_EndDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $('#btnLoadTransactions').click(function () {
            validateControls();
        });        
    };

    var initGridControls = function () {
        $('#GridContainer').off('click.voidtransaction', 'a[voidtransaction]').on('click.voidtransaction', 'a[voidtransaction]', function () {
            $('#ConfirmationVoidModal').find('#btnApplyVoid').attr('transactionid', $(this).attr('transactionid'));
            $('#ConfirmationVoidModal').modal('show');            
        });

        $('#ConfirmationVoidModal').find('#btnApplyVoid').click(function (e) {
            e.preventDefault();
            var transactionid = $(this).attr('transactionid');
            $('#ConfirmationVoidModal').modal('hide');
            voidTransactions(transactionid);
        });
    }

    var voidTransactions = function (transactionid) {
        showLoader();
        $.ajax({
            url: '/VPOSTransactions/ExecuteCreditCardVOID',
            type: 'POST',
            data: JSON.stringify({TransactionId: transactionid }),
            contentType: 'application/json',
            success: function (data) {
                $('body').loader('hide');
                if (data.split('|')[0] == 'Error') {
                    $('#msgVoid').html('Se presentó un error durante el proceso. <br /> Detalle: ' + data.split('|')[1]);
                } else {
                    $('#msgVoid').html('La transacción ha sido anulada correctamente.');
                    $('#GridContainer').html(data);
                }
                $('#msgVoidModal').modal('show');
            }
        });
    }

    var validateControls = function () {
        if (($('#Parameters_ReportCriteriaId').val() === '')
             || ($('#Parameters_Month').val() === '')
             || ($('#Parameters_Year').val() === '')
             || ($('#Parameters_CustomerId').val() === '')
             || ($('#Parameters_StartDateStr').val() === '')
             || ($('#Parameters_EndDateStr').val() === '')) {

            if (($('#Parameters_Month').val() !== '') && ($('#Parameters_Year').val() !== '') && ($('#Parameters_CustomerId').val() !== '')) {
                $('#validationmessage').hide();
                $('#loadForm').submit();
                return true;
            }
            if (($('#Parameters_StartDateStr').val() !== '') && ($('#Parameters_EndDateStr').val() !== '') && ($('#Parameters_CustomerId').val() !== '')) {
                $('#validationmessage').hide();
                $('#loadForm').submit();
                return true;
            }
            $('#validationmessage').show();
            return true;
        }
        $('#validationmessage').hide();
        $('#loadForm').submit();
    };

    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            return false;
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Parameters_Month").select2('val', '');
            $("#Parameters_Year").select2('val', '');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');

            $('#Parameters_ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
        }
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        initGridControls();
    };

    return {
        Init: initialize,
        ShowLoader: showLoader,
        HideLoader: hideLoader
    };
})();