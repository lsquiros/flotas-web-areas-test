﻿var ECO_Coinca = ECO_Coinca || {};

 ECOsystem.Areas.Coinca.UserRolesPermissions = (function () {
    var options = {};
    
    var initialize = function (opts) {
        $.extend(options, opts);

        $('#gridContainer').off('click.del', 'a[del_row]').on('click.del', 'a[del_row]', function (e) {
            $('#deleteModal').find('#id').val($(this).attr('id'));
            type = 1;
            setTitle($(this));
        });

        $('#detailContainer').off('blur.inputOnBlur', '#txtRoleName').on('blur.inputOnBlur', '#txtRoleName', function () {
            var roleName = $('#txtRoleName').val();
            var oldRoleName = $('#txtOldRoleName').val();
            $('#NewRoleName').val(roleName);
            if (roleName == "" || roleName == null)
            {
                $('#RoleNameValidation').html('El nombre del rol es requerido.');
            }
            else
            {
                if (roleName != oldRoleName) {
                    $('#btnSaveRoles').attr("disabled", "disabled");
                    $.ajax({
                        url: '/UserRolesPermission/CheckValidRoleName',
                        type: 'POST',
                        data: JSON.stringify({ RoleName: roleName, Parent: '' }),
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {

                            if (data == 0) {
                                $('#RoleNameValidation').html('El rol ya existe en la base de datos');
                                $('#btnSaveRoles').attr("disabled", "disabled");
                            }
                            else {
                                $('#btnSaveRoles').removeAttr('disabled');
                            }
                        }
                    });
                } else {
                    $('#btnSaveRoles').removeAttr('disabled');
                }
            }
        });

        $('#detailContainer').off('focusin.inputOnBlur', '#txtRoleName').on('focusin.inputOnBlur', '#txtRoleName', function () {
            $('#RoleNameValidation').html('');                      
        });
        
        $('#btnSavePermissions').click(function () {
            $('#SavePermissionsVerification').val(1);
            $('#ConfirmationSaveModal').modal('show');
        });

        //Init dropDown 
        $("#RolList").select2('data', {});
        $("#RolList").select2({
            formatResult: roleSelectFormat,
            formatSelection: roleSelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        $("#ModuleList").select2('data', {});
        $("#ModuleList").select2({
            formatResult: moduleSelectFormat,
            formatSelection: moduleSelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
   
    };

    /* Select Format*/
    var roleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-6">' + json.Name + '</div>' +                
                '</div>';
    };

    /* Select Format*/
    var moduleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-6">' + json.Name + '</div>' +
                '</div>';
    };

    var onSuccessLoad = function () {
        $('#addOrEditModal').modal('show');
        $('#btnSaveRoles').removeAttr("disabled");
    };

    var addModulesModal = function () {
        $('#loadForm').find('#id').val('-1');
        $('#loadForm').submit();
    };

    var onRolesChangeMenu = function () {
        $('#ModuleList').attr('disabled', true);
        var vRole = $("#RolList").val();
        $('#treeViewContainer').addClass('hide');

        $('#ModuleList').empty();
        $('#ModuleList').append('<option value>Seleccione un Módulo...</option>');

        if ($("#RolList option:selected").text() == "Seleccione un rol...") { return; }

        $.ajax({
            url: '/UserRolesPermission/RetrieveModulesPermissions',
            type: 'POST',
            data: JSON.stringify({ roleId: vRole }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    $('#ModuleList').append('<option value="' + data[i].ModuleId + '">'
                            + data[i].Name +
                            '</option>');
                }
                $('#ModuleList').attr('disabled', false);
            }
        });
    }

    var onRolesAddMenus = function () {
        $('#RolList').empty();
        $('#RolList').append('<option value>Seleccione un Rol...</option>');
        $.ajax({
            url: '/UserRolesPermission/RetrieveRolesPermissions',
            type: 'POST',
            data: JSON.stringify({ x: 1 }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    $('#RolList').append('<option value="' +  data[i].RoleId + '">'
                        + "{ 'Name' : '" + data[i].Name + "'}" +
                            '</option>');
                }
            }
        });

        $('#addOrEditModal').modal('hide');
    }
    
    var onRolesCancel = function () {        
        $.ajax({
            url: '/UserRolesPermission/CancelSaveAsPermissions',
            type: 'POST',
            data: JSON.stringify({ x: 1 }),
            dataType: 'json',
            contentType: 'application/jon',
            success: function (data) {
                $('#addOrEditModal').modal('hide');
            }
        });        
    }

    var onConfirmationModalOn = function () {
        $('#ConfirmationModalForm').submit();
    }

    var onConfirmationModalShow = function () {
        $('#ConfirmationModal').modal('show');
    }

    var addOrEditStart = function () {
        $('#NewRoleId').val($('#RoleId').val());        
    };

    var addOrEditModalClose = function () {
        $('#addOrEditModal').modal('hide');
    };

    var successSearch = function () {
        $('body').loader('hide');
    };

    var addOrEditModalHide = function () {
        $('#addOrEditModal').modal('hide');
        var newrole = $('#NewRoleId').val();
        if(newrole == '')
        {
            $('#AssingPermissionsModal').modal('show');
        }
    };

    var redirectToPermissions = function () {
        var newRoleName = $('#NewRoleName').val();
        var url = '/Coinca/UserRolesPermission/Index?newRoleName=' + newRoleName; 
        window.location.href = url;
    };

    var onSubmitRoles = function () {
        ECOsystem.Utilities.ShowLoader();
        $('#addOrEditModal').modal('hide');
    }

    var reload = function () {
        location.reload();
    }
    
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        AddModulesModal: addModulesModal,
        OnRolesChangeMenu: onRolesChangeMenu,
        OnRolesAddMenus: onRolesAddMenus,
        OnRolesCancel: onRolesCancel,
        OnConfirmationModalOn: onConfirmationModalOn,
        OnConfirmationModalShow: onConfirmationModalShow,
        AddOrEditStart: addOrEditStart,
        AddOrEditModalHide: addOrEditModalHide,
        RedirectToPermissions: redirectToPermissions,
        AddOrEditModalClose: addOrEditModalClose,
        SuccessSearch: successSearch,
        Reload: reload,
        OnSubmitRoles: onSubmitRoles
    };
})();