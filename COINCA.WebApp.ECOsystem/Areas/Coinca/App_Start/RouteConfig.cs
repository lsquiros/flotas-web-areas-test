﻿/************************************************************************************************************
*  File    : RouteConfig.cs
*  Summary : RouteConfig for MVC
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Web.Mvc;
using System.Web.Routing;

namespace ECO_Coinca
{
    /// <summary>
    /// Route Config Class
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Required Register Routes Method
        /// </summary>
        /// <param name="routes">Provides a collection of routes for ASP.NET routing.</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
