﻿/************************************************************************************************************
*  File    : FilterConfig.cs
*  Summary : Filter Config from MVC
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Web.Mvc;

namespace ECO_Coinca
{
    /// <summary>
    /// Filter Config Class
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Register Global Filters Method
        /// </summary>
        /// <param name="filters">filters</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
