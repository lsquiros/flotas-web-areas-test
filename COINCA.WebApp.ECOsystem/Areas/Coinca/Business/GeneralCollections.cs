﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Utilities;
using System.Globalization;
using System;
using ECOsystem.Models.Core;

namespace  ECOsystem.Areas.Coinca.Business
{
    /// <summary>
    /// General Collections Class Contains all methods for load Drop Down List
    /// </summary>
    public class GeneralCollections
    {        
        /// <summary>
        /// Return a list of Countries in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCountries
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new CountriesBusiness())
                {
                    var result = business.RetrieveCountries(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CountryId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
                
        /// <summary>
        /// Return a list of Acces Countries for selected user in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCountriesAccess
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var user = Session.GetUserInfo();
                if (!user.IsPartnerUser) return new SelectList(dictionary, "Key", "Value");

                foreach (var r in user.PartnerUser.CountriesAccessList.Where(x => x.IsCountryChecked))
                {
                    dictionary.Add(r.CountryId, r.CountryName);
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of currencies in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCurrencies
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new CurrenciesBusiness())
                {
                    var result = business.RetrieveCurrencies(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CurrencyId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Customer Credit Card Status in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomerCreditCardStatus
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new StatusBusiness())
                {
                    var user = Session.GetUserInfo();
                    var result = business.RetrieveStatus("CUSTOMER_CREDIT_CARD_" + ECOsystem.Utilities.Session.GetProfileByRole(user.RoleId));
                    foreach (var r in result)
                    {
                        dictionary.Add(r.StatusId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }        

        /// <summary>
        /// Return a list of Fuels in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetFuels
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new FuelsBusiness())
                {
                    var result = business.RetrieveFuels(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.FuelId, r.Name + " - " + r.CountryName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Fuels in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetFuelsByPartner(int CountryId)
        {
            
            var dictionary = new Dictionary<int?, string>();

            using (var business = new FuelsBusiness())
            {
                var result = business.RetrieveFuels(null).Where(m => m.CountryId == CountryId);
                    
                foreach (var r in result)
                {                        
                    dictionary.Add(r.FuelId, "{'Name':'" + r.Name + "'}");
                }
            }
            return new SelectList(dictionary, "Key", "Value");            
        }

        /// <summary>
        /// Return a list of Partners in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetPartners
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new PartnersBusiness())
                {
                    var result = business.RetrievePartners(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.PartnerId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }   

        /// <summary>
        /// Return a list of Customers Units Of Capacity for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersUnitsOfCapacity
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("CUSTOMERS_CAPACITY_UNITS");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Partner Types to populate DropDownList
        /// </summary>
        public static SelectList GetPartnerTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("PARTNER_TYPE");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Products Types to populate DropDownList
        /// </summary>
        public static SelectList GetProductTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new ProductBusiness())
                {
                    var result = business.GetAll();
                    foreach (var item in result)
                    {
                        dictionary.Add(item.Id, item.Description);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
        
        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetMonthNames
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year - 5; i < year + 9; i++)
                {
                    dictionary.Add(i, i);
                }
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }        
                 
        public static SelectList GetReportCriteria
        {
            get
            {
                return new SelectList(new Dictionary<int, string> { 
                    {(int)ReportCriteria.Period,"Por Período"},
                    {(int)ReportCriteria.DateRange,"Rango de Fechas"}
                }, "Key", "Value", "");
            }
        }

        /// <summary>
        /// Return a list of Partners in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetBFCustomers
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new PartnersBusiness())
                {
                    var result = business.RetrieveBFCustomers(null).OrderBy(x => x.DecryptedName);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CustomerId, r.DecryptedName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        #region CommentProperties       
        ///// <summary>
        ///// Return a current Month Name
        ///// </summary>
        //public static SelectList GetCurrentMonthName
        //{
        //    get
        //    {
        //        var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
        //        var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
        //        var dictionary = result.Where(w => w.Id == DateTime.Now.Month).ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
        //        return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
        //    }
        //}

        ///// <summary>
        ///// Return a list of Load Types for populating DropDownListFor
        ///// </summary>
        //public static SelectList GetVehicleLoadTypes
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();

        //        using (var business = new TypesBusiness())
        //        {
        //            var result = business.RetrieveTypes("VEHICLES_LOAD_WEIGHT");
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.TypeId, r.Name);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Customers Credit Card Issue For populate DropDownListFor
        ///// </summary>
        //public static SelectList GetCustomersCreditCardIssueForId
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();

        //        using (var business = new TypesBusiness())
        //        {
        //            var result = business.RetrieveTypes("CUSTOMERS_CREDIT_CARDS_ISSUE_FOR");
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.TypeId, r.Name);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Customers Credit Card Types For populate DropDownListFor
        ///// </summary>
        //public static SelectList GetCustomersCreditCardTypes
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<string, string> { { "C", "Crédito" }, { "D", "Débito" } };
        //        return new SelectList(dictionary, "Key", "Value", "C");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Periodicity For populate DropDownListFor
        ///// </summary>
        //public static SelectList GetPeriodicity
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();
        //        using (var business = new TypesBusiness())
        //        {
        //            var result = business.RetrieveTypes("CUSTOMER_PERIODICITY_VEHICLE");
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.TypeId, r.Name);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Periodicity Alarm For populate DropDownListFor
        ///// </summary>
        //public static SelectList GetPeriodicityAlarm
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();
        //        using (var business = new TypesBusiness())
        //        {
        //            var result = business.RetrieveTypes("CUSTOMERS_PERIODICITY_ALARM");
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.TypeId, r.Name);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Frequency For populallte DropDownListFor
        ///// </summary>
        //public static SelectList GetFrequency
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();
        //        using (var business = new TypesBusiness())
        //        {
        //            var result = business.RetrieveTypes("CUSTOMER_FREQUENCY");
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.TypeId, r.Name);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}
        //public static SelectList GetCardYears
        //{
        //    get
        //    { 
        //        var dictionary = new Dictionary<int?, int>();
        //        var year = DateTime.UtcNow.Year;

        //        for (var i = year ; i < year + 15; i++)
        //        {   //ValidCreditCard for 15 years
        //            dictionary.Add(i, i);
        //        }
        //        year = year + 1;
        //        return new SelectList(dictionary, "Key", "Value", year);
        //    }
        //}


        ///// <summary>
        ///// Return a list of Transactions Types it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetTransactionTypes
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();
        //        var year = DateTime.UtcNow.Year;
        //        dictionary.Add(1,"Procesadas");
        //        //dictionary.Add(2,"Flotantes");
        //        dictionary.Add(3,"Reversadas");
        //        //dictionary.Add(4,"Duplicadas");
        //        //dictionary.Add(5,"Ajustes");
        //        dictionary.Add(6,"Rechazadas");
        //        return new SelectList(dictionary, "Key", "Value", year);
        //    }
        //}


        ///// <summary>
        ///// Return a list of Filtersfor populate DropDownListFor
        ///// </summary>
        //public static SelectList GetFilterDetailTransacReport
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();
        //        var year = DateTime.UtcNow.Year;
        //        dictionary.Add(1, "Centro de Costo");
        //        dictionary.Add(2, "Grupo de Vehículos");
        //        return new SelectList(dictionary, "Key", "Value", year);
        //    }
        //}

        ///// <summary>
        ///// Return a list of Filtersfor populate DropDownListFor
        ///// </summary>
        //public static SelectList GetFilterSettingOdometers
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();                
        //        dictionary.Add(1, "Odómetros Erróneos");
        //        dictionary.Add(2, "Historial de Odómetros");
        //        return new SelectList(dictionary, "Key", "Value", 1);
        //    }
        //}


        ///// <summary>
        ///// Return a list of Filtersfor populate DropDownListFor
        ///// </summary>
        //public static SelectList GetFilterPartnerCustomer
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();
        //        var year = DateTime.UtcNow.Year;
        //        dictionary.Add(1, "Socio");
        //        dictionary.Add(2, "Clientes");
        //        return new SelectList(dictionary, "Key", "Value", year);
        //    }
        //}

        ///// <summary>
        ///// Return the modality of the GPS
        ///// </summary>
        //public static SelectList GetGPSModality
        //{
        //    get
        //    {
        //        return new SelectList(new Dictionary<int, string> { 
        //            {1,"Alquilado"},
        //            {2,"Propio"}
        //        }, "Key", "Value", "");
        //    }
        //}

        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        //public static SelectList GetDriverReportCriteria
        //{
        //    get
        //    {
        //        return new SelectList(new Dictionary<int, string> { 
        //             {(int)ReportCriteria.Period,"Por Período"},
        //             {(int)ReportCriteria.DateRange,"Rango de Fechas"},
        //             {(int)ReportCriteria.DayDate,"Por Día"}
        //        }, "Key", "Value", "");
        //    }
        //}

        /// <summary>
        /// Return a list of Preventive Maintenance Report Status
        /// </summary>
        //public static SelectList GetReportPreventiveMaintenanceStatus
        //{
        //    get
        //    {
        //        return new SelectList(new Dictionary<int, string> { 
        //            {(int)ReportPreventiveMaintenanceStatus.Pendientes,"Mantenimientos Pendientes"},
        //            {(int)ReportPreventiveMaintenanceStatus.Realizado,"Mantenimientos Realizados"},
        //            {(int)ReportPreventiveMaintenanceStatus.Vencidos,"Mantenimientos Vencidos"},
        //        }, "Key", "Value", (int)ReportPreventiveMaintenanceStatus.Pendientes);
        //    }
        //}

        ///// <summary>
        ///// Return a list of Report Criteria in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetReportCriteriaForTrendReports
        //{
        //    get
        //    {
        //        return new SelectList(new Dictionary<int, string> { 
        //            {(int)ReportCriteria.Period,"Por Período"}
        //        }, "Key", "Value", (int)ReportCriteria.Period);
        //    }
        //}

        ///// <summary>
        ///// Return a list of Report Report Fuel Types in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetReportFuelTypes
        //{
        //    get
        //    {
        //        return new SelectList(new Dictionary<int, string> { 
        //            {(int)ReportFuelTypes.Vehicles,"Vehículos"},
        //            {(int)ReportFuelTypes.VehicleGroups,"Grupo de Vehículos"},
        //            {(int)ReportFuelTypes.VehicleCostCenters,"Centro de Costos"},
        //            //{(int)ReportFuelTypes.CostCenter,"Centro de Costo"},
        //        }, "Key", "Value", (int)ReportFuelTypes.Vehicles);
        //    }
        //}

        ///// <summary>
        ///// Return a list of Report Report Fuel Types in order to using it for populate DropDownListFor
        ///// It's the same GetReportFuelTypes + VehicleUnits, Requirement only for CurrentFuelsReport
        ///// When more fuel's reports required VehicleUnits delete this method
        ///// </summary>
        //public static SelectList GetReportFuelTypes2
        //{
        //    get
        //    {
        //        return new SelectList(new Dictionary<int, string> { 
        //            {(int)ReportFuelTypes.Vehicles,"Vehículos"},
        //            {(int)ReportFuelTypes.VehicleGroups,"Grupo de Vehículos"},
        //            {(int)ReportFuelTypes.VehicleCostCenters,"Centro de Costos"},
        //            {(int)ReportFuelTypes.VehicleUnits,"Unidades"},
        //        }, "Key", "Value", (int)ReportFuelTypes.Vehicles);
        //    }
        //}


        //public static SelectList GetReporCompartativeTypes
        //{
        //    get
        //    {
        //        return new SelectList(new Dictionary<int, string> { 
        //            {(int) ReportComparativeTypes.Vehicles,"Vehiculo"},
        //            {(int) ReportComparativeTypes.brand,"Marca"},
        //            {(int) ReportComparativeTypes.model,"Modelo"},
        //           {(int) ReportComparativeTypes.route,"Ruta"},
        //           {(int) ReportComparativeTypes.cilinder,"Cilindrajes"},


        //        }, "Key", "Value", (int)ReportFuelTypes.Vehicles);
        //    }
        //}

        ///// <summary>
        ///// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetVehiclesByCustomer
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();

        //        using (var business = new VehiclesBusiness())
        //        {
        //            var result = business.RetrieveVehicles(null);
        //            foreach (var r in result)
        //            {
        //                if (!dictionary.ContainsKey(r.VehicleId))
        //                {
        //                    dictionary.Add(r.VehicleId, "{'PlateId':'" + r.PlateId + "', 'Name':'" + r.Name + "', 'CategoryType':'" + r.CategoryType + "', 'FuelName': '" + r.FuelName + "', 'CostCenterName': '" + r.CostCenterName + "'}");
        //                }
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Vehicle Manufacturers in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetVehiclesManufacturers
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<string, string>();

        //        using (var business = new VehicleCategoriesBusiness())
        //        {
        //            var result = business.RetrieveVehicleCategories(null);
        //            foreach (var r in result)
        //            {
        //                if (!dictionary.ContainsKey(r.Manufacturer)) dictionary.Add(r.Manufacturer, r.Manufacturer);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Vehicle Models in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetVehiclesModels
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<string, string>();

        //        using (var business = new VehicleCategoriesBusiness())
        //        {
        //            var result = business.RetrieveVehicleCategories(null);
        //            foreach (var r in result)
        //            {
        //                if (!dictionary.ContainsKey(r.VehicleModel)) dictionary.Add(r.VehicleModel, r.VehicleModel);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Vehicle Types in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetVehiclesTypes
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<string, string>();

        //        using (var business = new VehicleCategoriesBusiness())
        //        {
        //            var result = business.RetrieveVehicleCategories(null);
        //            foreach (var r in result)
        //            {
        //                if (!dictionary.ContainsKey(r.Type)) dictionary.Add(r.Type, r.Type);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Vehicle Types in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetVehiclesCylinderCapacities
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, int?>();

        //        using (var business = new VehicleCategoriesBusiness())
        //        {
        //            var result = business.RetrieveVehicleCategories(null);
        //            foreach (var r in result)
        //            {
        //                if (!dictionary.ContainsKey(r.CylinderCapacity)) dictionary.Add(r.CylinderCapacity, r.CylinderCapacity);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Vehicle Years in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetVehiclesYears
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, int?>();

        //        using (var business = new VehicleCategoriesBusiness())
        //        {
        //            var result = business.RetrieveVehicleCategories(null);
        //            foreach (var r in result)
        //            {
        //                if (!dictionary.ContainsKey(r.Year)) dictionary.Add(r.Year, r.Year);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of currencies in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetOperators
        //{

        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();
        //        using (var bussiness = new OperatorsBusiness())
        //        {
        //            var result = bussiness.RetrieveOperators(customerId: ECOsystem.Utilities.Session.GetCustomerId(), operatorId: null, key: null);
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.OperatorId, r.Name);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of currencies in order to using it for populate DropDownList
        ///// </summary>
        //public static SelectList GetPreventiveMaintenanceByCustomer
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();
        //        using (var business = new PreventiveMaintenanceCatalogBusiness())
        //        {
        //            var result = business.RetrievePreventiveMaintenance(ECOsystem.Utilities.Session.GetCustomerId(), preventiveMaintenanceCatalogId: null, key: null);
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.PreventiveMaintenanceCatalogId, "{'Description':'" + r.Description + "', 'FrequencyKm':'" + r.FrequencyKm + "', 'AlertBeforeKm':'" + r.FrequencyMonth + "', 'FrequencyMonth':'" + r.FrequencyMonth + "', 'AlertBeforeMonth':'" + r.AlertBeforeMonth + "', 'FrequencyDate':'" + r.FrequencyDate + "', 'AlertBeforeDate':'" + r.AlertBeforeDate + "'}");
        //            }
        //            return new SelectList(dictionary, "Key", "Value");
        //        }
        //    }
        //}

        ///// <summary>
        ///// Return a list of currencies in order to using it for populate DropDownList
        ///// </summary>
        //public static SelectList GetPreventiveMaintenanceCatalog
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();
        //        using (var business = new PreventiveMaintenanceCatalogBusiness())
        //        {
        //            var result = business.RetrievePreventiveMaintenance(ECOsystem.Utilities.Session.GetCustomerId(), preventiveMaintenanceCatalogId: null, key: null);
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.PreventiveMaintenanceCatalogId, r.Description);
        //            }
        //            return new SelectList(dictionary, "Key", "Value");
        //        }
        //    }
        //}

        ///// <summary>
        ///// Return a list of Icon of vehicle in order to using for populate DropDownList 
        ///// </summary>
        //public static SelectList GetIconVehicleCategory
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<string, string>();

        //        string path = HttpContext.Current.Server.MapPath("~/Content/Images/Vehicles/");
        //        if (Directory.Exists(path))
        //        {
        //            List<String> listImage = Directory.GetFiles(path, "*.png", SearchOption.AllDirectories).ToList();
        //            foreach (String itemImage in listImage)
        //            {
        //                dictionary.Add(Path.GetFileName(itemImage), "{'image':'" + Path.GetFileName(itemImage) + "'}");
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of currencies in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetCurrenciesByCustomer
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();

        //        using (var business = new CurrenciesBusiness())
        //        {
        //            var result = business.RetrieveCurrenciesByCustomer(ECOsystem.Utilities.Session.GetCustomerId());
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.CurrencyId, r.Name);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of currency symbol in order to using it for populate DropDownListFor 
        ///// </summary>
        //public static SelectList GetCurrencySymbolByCustomer
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();

        //        using (var business = new CurrenciesBusiness())
        //        {
        //            var result = business.RetrieveCurrenciesByCustomer(ECOsystem.Utilities.Session.GetCustomerId());
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.CurrencyId, r.Symbol);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of GeoFences in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetGeoFences
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();

        //        using (var business = new GeoFencesBusiness())
        //        {
        //            var result = business.RetrieveGeoFences(null);
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.GeoFenceId, r.Name);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        ///// <summary>
        ///// Return a list of Routes in order to using it for populate DropDownListFor
        ///// </summary>
        //public static SelectList GetRoutesByVehicle
        //{
        //    get
        //    {
        //        var dictionary = new Dictionary<int?, string>();

        //        using (var business = new RoutesBusiness())
        //        {
        //            var result = business.RetrieveRoutes(null);
        //            foreach (var r in result)
        //            {
        //                dictionary.Add(r.RouteId, r.Name);
        //            }
        //        }
        //        return new SelectList(dictionary, "Key", "Value");
        //    }
        //}

        // /// <summary>
        // /// Return a list of Roles to populate DropDownListFor
        // /// </summary>
        // public static SelectList GetCustomerRoles
        // {
        //     get
        //     {
        //         return new SelectList(new Dictionary<string, string> { 
        //             {"CUSTOMER_ADMIN","Administrador"},
        //             {"CUSTOMER_USER","Usuario Regular"},
        //         }, "Key", "Value");
        //     }
        // }

        // /// <summary>
        // /// Return a list of Roles to populate DropDownListFor
        // /// </summary>
        // public static SelectList GetDriverRoles
        // {
        //     get
        //     {
        //         return new SelectList(new Dictionary<string, string> { 
        //             {"CUSTOMER_DRIVER","Conductor"}
        //         }, "Key", "Value");
        //     }
        // }

        // /// <summary>
        // /// Return a list of Card Request available from Customer
        // /// </summary>
        // public static SelectList GetDriverCardRequestByCustomer
        // {
        //     get
        //     {
        //         var dictionary = new Dictionary<int?, string>();

        //         using (var business = new CustomersBusiness())
        //         {
        //             var result = business.RetrieveCustomerCardRequest(ECOsystem.Utilities.Session.GetCustomerId(), null);

        //             foreach (var r in result.Where(w => w.IssueForId == 100 && !string.IsNullOrEmpty(w.DriverName)))
        //             {
        //                 if (!dictionary.Keys.Contains(r.CardRequestId))
        //                 {
        //                     dictionary.Add(r.CardRequestId, "{'NumSolicitud':'" + string.Format("#{0}", r.CardRequestId) + "', 'DriverName':'" + string.Format("({0}){1}", r.DriverIdentification, r.DriverName) + "', 'StateName': '" + ((!string.IsNullOrEmpty(r.StateName)) ? r.StateName : "No Disponible") + "', 'CityName': '" + ((!string.IsNullOrEmpty(r.CityName)) ? r.CityName : "No Disponible") + "'}");
        //                 }
        //             }
        //         }

        //         return new SelectList(dictionary, "Key", "Value");
        //     }
        // }

        // /// <summary>
        // /// Return a list of Roles to populate DropDownListFor
        // /// </summary>
        // public static SelectList GetSuperRoles
        // {
        //     get
        //     {
        //         return new SelectList(new Dictionary<string, string> { 
        //             {"SUPER_ADMIN","Administrador"},
        //             {"SUPER_USER","Usuario Regular"},
        //         }, "Key", "Value");
        //     }
        // }

        // /// <summary>
        // /// Return a list of Roles to populate DropDownListFor
        // /// </summary>
        // public static SelectList GetPartnerRoles
        // {
        //     get
        //     {
        //         if (ECOsystem.Utilities.Session.GetPartnerInfo().PartnerTypeId != 801)
        //         {
        //             return new SelectList(new Dictionary<string, string> { 
        //             {"PARTNER_ADMIN","Administrador"},
        //             {"PARTNER_USER","Usuario Regular"},
        //             }, "Key", "Value");
        //         }
        //         else
        //         {
        //             return new SelectList(new Dictionary<string, string> { 
        //             {"INSURANCE_ADMIN","Administrador"},
        //             {"INSURANCE_USER","Usuario Regular"},
        //             }, "Key", "Value");

        //         }
        //     }
        // }

        // public static SelectList GetRoles
        // {
        //     get
        //     {
        //         var dictionary = new Dictionary<int?, string>();

        //         using (var business = new ScoreTypesBusiness())
        //         {
        //             var result = business.RetrieveScoreTypes(null);
        //             foreach (var r in result)
        //             {
        //                 dictionary.Add(r.ScoreTypeId, r.Code + " " + r.Name);
        //             }
        //         }
        //         return new SelectList(dictionary, "Key", "Value");
        //     }
        // }

        // /// <summary>
        // /// Return a list of Score Types in order to using it for populate DropDownListFor
        // /// </summary>
        // public static SelectList GetScoreTypes
        // {
        //     get
        //     {
        //         var dictionary = new Dictionary<int?, string>();

        //         using (var business = new ScoreTypesBusiness())
        //         {
        //             var result = business.RetrieveScoreTypes(null);
        //             foreach (var r in result)
        //             {
        //                 dictionary.Add(r.ScoreTypeId, r.Code + " " + r.Name);
        //             }
        //         }
        //         return new SelectList(dictionary, "Key", "Value");
        //     }
        // }

        // /// <summary>
        // /// Return a list of AvailableDallasKeys in order to using it for populate DropDownListFor
        // /// </summary>
        // public static SelectList GetAvailableDallasKeys
        // {
        //     get
        //     {
        //         var dictionary = new Dictionary<int?, string>();
        //         dictionary.Add(0, "{'Code':'Ninguna'}");
        //         using (var business = new DallasKeysBusiness())
        //         {
        //             var result = business.RetrieveDallasKeys(null, isUsed: false);
        //             foreach (var r in result)
        //             {
        //                 if (!dictionary.ContainsKey(r.DallasId))
        //                 {
        //                     dictionary.Add(r.DallasId, "{'Code':'" + r.DallasCode + "'}");
        //                 }
        //             }
        //         }

        //         return new SelectList(dictionary, "Key", "Value");
        //     }
        // }




        // /// <summary>
        // /// Return a list of Month Names in order to using it for populate DropDownListFor
        // /// </summary>
        // public static HtmlString GetMonthCardNames(int p)
        // {     
        //       if (p == DateTime.Now.Year)
        //     {

        //         var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
        //         var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
        //         var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name))
        //                                .SkipWhile(element => element.Key < DateTime.Now.Month).ToList();
        //         return new HtmlString(JsonConvert.SerializeObject(dictionary.ToList()));
        //     }
        //     else
        //     {
        //         var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
        //         var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
        //         var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
        //         return new HtmlString(JsonConvert.SerializeObject(dictionary.ToList()));
        //     }
        //}
        #endregion       
    }
}