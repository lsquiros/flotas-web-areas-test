﻿/************************************************************************************************************
*  File    : CreditCardBusiness.cs
*  Summary : CreditCard Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Coinca.Models;
using ECOsystem.Utilities;
using  ECOsystem.Areas.Coinca.Models.Control;
using ECOsystem.Business.Utilities;
using System.Data;
using System.Xml.Linq;

namespace  ECOsystem.Areas.Coinca.Business
{
    /// <summary>
    /// CreditCard Class
    /// </summary>
    public class CreditCardBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve CreditCard
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CreditCard> RetrieveCreditCard(int? creditCardId, int? paymentInstrumentType,string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCard_Retrieve]",
                    new
                    {
                        CreditCardId = creditCardId,
                        CustomerId = Session.GetCustomerId() ?? null,
                        Key = string.IsNullOrEmpty(key) ? null : key,
                        user.UserId,
                        PaymentInstrumentType = paymentInstrumentType
                    });
            }
        }

        /// <summary>
        /// Retrieve CreditCard that are close to the limit
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CreditCard> RetrieveCreditCardCloseToLimite(int? creditCardId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardCloseCreditLimit_Retrieve]",
                    new
                    {
                        CreditCardId = creditCardId,
                        CustomerId = Session.GetCustomerId() ?? 0,
                        Key = string.IsNullOrEmpty(key) ? null : key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve CreditCardByNumber
        /// </summary>
        /// <param name="creditCardNumber">The Credit Card Number that identifies each Credit Card</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CreditCard> RetrieveCreditCardByNumber(string creditCardNumber)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardByNumber_Retrieve]",
                    new
                    {
                        CreditCardNumber = creditCardNumber
                    });
            }
        }

        /// <summary>
        /// Retrieve Credit Card Holder By Number
        /// </summary>
        /// <param name="creditCardNumber">The Credit Card Number that identifies each Credit Card</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public string RetrieveCreditCardHolderByNumber(string creditCardNumber)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardHolder_Retrieve]",
                    new
                    {
                        CreditCardNumber = creditCardNumber
                    }).ToList().FirstOrDefault();

                return (result != null)? result.CreditCardHolder : string.Empty;
            }
        }

        /// <summary>
        /// Retrieve CardFileParams
        /// </summary>        
        /// <returns></returns>
        public IEnumerable<CardFileParams> RetrieveCardFileParams()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CardFileParams>("[Control].[Sp_CardFileParams_Retrieve]", null);
            }
        }

        /// <summary>
        /// RetrieveCreditCardVPOS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<CreditCard> RetrieveCreditCardVPOS(string key)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardVPOS_Retrieve]",
                    new
                    {
                        Key = key
                    });
            }
        }

        /// <summary>
        /// RetrieveCreditCardVPOS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<CreditCardTransaction> RetrieveTransactionVOSForReverse()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCardTransaction>("[Control].[Sp_TransactionsVPOSForReverse_Retrieve]", null);
            }
        }

        /// <summary>
        /// RetrieveCreditCardVPOS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string RetrieveCreditCardNumber(int creditCardId)
        {
            using (var dba = new DataBaseAccess())
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(dba.ExecuteScalar<string>("[Control].[Sp_CreditCardNumber_Retrieve]",
                    new
                    {
                        CreditCardId = creditCardId
                    }));
            }
        }

        /// <summary>
        /// RetrieveCreditCardVPOS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public int RetrieveIDCostumerbyCreditCardNumber(long creditCardNumber)
        {
            using (var dba = new DataBaseAccess())
            {
                try
                {
                    //TripleDesEncryption.Encrypt(Miscellaneous.ConvertCreditCardToInternalFormat((long)data.cardNumber))

                    string cc_number = ECOsystem.Utilities.TripleDesEncryption.Encrypt(ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat(creditCardNumber));

                    return dba.ExecuteScalar<int>("[Control].[Sp_CustomerByCreditCardNumber_Retrieve]",
                        new
                        {
                            CreditCardNumber = cc_number
                        });
                }
                catch (Exception)
                {
                    return 0; // Default return
                }
                
            }
        }

        /// <summary>
        /// RetrieveCreditCardVPOS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<CreditCardTransaction> RetrieveCreditCardTransactionVPOS(int creditCardId, string systemTraceNumber = "")
        {
            using (var dba = new DataBaseAccess())
            {
                var dataList = dba.ExecuteReader<CreditCardTransaction>("[Control].[Sp_CreditCardTransaction_Retrieve]",
                    new
                    {
                        CreditCardId = creditCardId,
                        SystemTraceNumber = (string.IsNullOrEmpty(systemTraceNumber))? null : systemTraceNumber
                    });

                foreach (var item in dataList)
                {
                    item.TransactionSaleList = dba.ExecuteReader<TransactionSale>("[Control].[Sp_CreditCardTransactionSALE_Retrieve]",
                        new
                        {
                            CreditCardId = creditCardId
                        }).ToList();
                }

                return dataList;
            }
        }

        /// <summary>
        /// AddCreditCardTransactionVPOS
        /// </summary>
        /// <param name="creditCardTransaction"></param>
        /// <returns></returns>
        public CreditCardTransaction AddOrEditCreditCardTransactionVPOS(CreditCardTransaction model)
        {
            using (var dba = new DataBaseAccess())
            {
                string invoiceTemp = (model.TransactionType.Contains("VOID")) ? model.Invoice : string.Empty;
                model.DriverCode = (!string.IsNullOrEmpty(model.DriverCode)) ? model.DriverCode : "CetWXCoQ4hw="; // 0000
                model.LoggedUserId = (model.LoggedUserId != null) ? model.LoggedUserId : 0;

                model.Invoice = dba.ExecuteScalar<int>("[Control].[Sp_CreditCardTransaction_AddOrEdit]",
                    new
                    {
                        model.CCTransactionVPOSId,
                        model.CreditCardId,
                        model.Customer,
                        model.TransactionType,
                        model.TerminalId,
                        model.Invoice,
                        model.EntryMode,
                        model.AccountNumber,
                        model.ExpirationDate,
                        model.TotalAmount,
                        model.Odometer,
                        model.Liters,
                        model.Plate,
                        model.AuthorizationNumber,
                        model.ReferenceNumber,
                        model.SystemTraceNumber,
                        model.LoggedUserId,
                        BacId = model.BACAfiliado,
                        model.DriverCode
                    }).ToString();

                model.CCTransactionVPOSId = Convert.ToInt32(model.Invoice);
                model.Invoice = (model.TransactionType.Contains("VOID")) ? invoiceTemp : model.Invoice;
            }
            return model;
        }

        /// <summary>
        /// Add Log for Transaction VPOS 
        /// </summary>
        /// <param name="CCTransactionVPOSId"></param>
        /// <param name="ResponseCode"></param>
        /// <param name="ResponseCodeDescription"></param>
        /// <param name="Message"></param>
        /// <param name="IsSuccess"></param>
        /// <param name="IsTimeOut"></param>
        /// <param name="IsCommunicationError"></param>
        /// <returns></returns>
        public int AddLogTransactionsVPOS(int CCTransactionVPOSId, string ResponseCode, string ResponseCodeDescription, string Message, bool IsSuccess, bool IsTimeOut, bool IsCommunicationError)
        {
            try
            {
                int LogID;

                using (var dba = new DataBaseAccess())
                {
                    LogID = dba.ExecuteScalar<int>("[Control].[Sp_LogTransactionsVPOS_Add]",
                     new
                     {
                         CCTransactionVPOSId,
                         ResponseCode,
                         ResponseCodeDescription,
                         Message,
                         IsSuccess,
                         IsTimeOut,
                         IsCommunicationError
                     });

                }
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
            
        }

        /// <summary>
        /// Retrieve CreditCard
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="isForNew">Flag For New View</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public CreditCardDetails RetrieveCreditCardDetail(int? creditCardId, bool isForNew)
        {

            //var result = new CreditCardDetails { Data = isForNew ? new CreditCard() : RetrieveCreditCard(creditCardId).FirstOrDefault() };

            //if (isForNew)
            //{
            //    using (var business = new CustomersBusiness())
            //    {
            //        var customer = business.RetrieveCustomers(Session.GetCustomerId()).FirstOrDefault();
            //        if (customer != null)
            //        {
            //            result.Data.CurrencySymbol = customer.CurrencySymbol;
            //            result.Data.IssueForId = customer.IssueForId;
            //        }
            //    }
            //}

            //using (var business = new CustomerCreditsBusiness())
            //{
            //    result.CreditInfo = business.RetrieveCustomerCredits().FirstOrDefault();
            //}
            //return result;

            return null;
        }

        /// <summary>
        /// Add Log for Transaction POS 
        /// </summary>
        /// <param name="CCTransactionPOSId"></param>
        /// <param name="ResponseCode"></param>
        /// <param name="ResponseCodeDescription"></param>
        /// <param name="Message"></param>
        /// <param name="IsSuccess"></param>
        /// <param name="IsTimeOut"></param>
        /// <param name="IsCommunicationError"></param>
        /// <returns></returns>
        public int AddLogTransactionsPOS(int? CCTransactionPOSId, string ResponseCode, string ResponseCodeDescription, string Message, string TransportData, bool IsSuccess, bool IsFail,int CustomerID = 0)

        {
            try
            {
                int LogID;

                using (var dba = new DataBaseAccess())
                {
                    LogID = dba.ExecuteScalar<int>("[Control].[Sp_LogTransactionsPOS_Add]",
                     new
                     {
                         CCTransactionPOSId,
                         ResponseCode,
                         ResponseCodeDescription,
                         Message,
                         TransportData,
                         IsSuccess,
                         IsFail,
                         CustomerID
                     });

                }
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }  

        /// <summary>
        /// Retrieve CreditCard Balance for API
        /// </summary>
        /// <param name="creditCardNumber">Credit Card Number</param>
        /// <param name="expirationDate">Expiration Date</param>
        /// <returns></returns>
        public decimal RetrieveCreditCardBalance(long creditCardNumber, int? expirationDate)
        {
            int? year = null;
            int? mont = null;

            if (expirationDate != null)
            {
                var expiration = expirationDate.ToString();
                year = expiration.Length == 4 ? int.Parse(expiration.Substring(0, 2)) + 2000 : 0;
                mont = expiration.Length == 4 ? int.Parse(expiration.Substring(2, 2)) : 0;
            }

            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteScalar<decimal?>("[Control].[Sp_CreditCard_Balance_Retrieve]",
                    new
                    {
                        CreditCardNumber = TripleDesEncryption.Encrypt(ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat(creditCardNumber)),
                        ExpirationYear = year,
                        ExpirationMonth = mont
                    });
                return result ?? 0.00m;
            }
        }

        /// <summary>
        /// Retrieve Card Detail
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public CreditCardDetails RetrieveCardDetail(int? creditCardId, int? paymentInstrumentType)
        {
            var result = new CreditCardDetails();
            
            result.CardInfo = RetrieveCreditCard(creditCardId,paymentInstrumentType).FirstOrDefault();
            result.RequestInfo = RetrieveCustomerCardRequest(result.CardInfo.CustomerId, result.CardInfo.CardRequestId).FirstOrDefault();                      
            
            result.CreditInfo = GetCustomerCredits(result.CardInfo.CustomerId).FirstOrDefault();
            
            return result;
        }

        /// <summary>
        /// Retrieve Customers
        /// </summary>
        /// <param name="customerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="cardRequestId">customer cardRequestId</param>
        /// <param name="pendingOnly">pendingOnly</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CardRequest> RetrieveCustomerCardRequest(int? customerId, int? cardRequestId, bool? pendingOnly = null, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CardRequest>("[Control].[Sp_CardRequest_Retrieve]",
                    new
                    {
                        CustomerId = customerId,
                        CardRequestId = cardRequestId,
                        PendingOnly = pendingOnly,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="custId"></param>
        /// <returns></returns>
        private IEnumerable<CustomerCredits> GetCustomerCredits(int? custId)
        {
            var currentDate = DateTimeOffset.Now;
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerCredits>("[Control].[Sp_CustomerCredits_Retrieve]",
                    new
                    {
                        CustomerId =  custId,
                        currentDate.Year,
                        currentDate.Month
                    });
            }
        }
        
        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CreditCard
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditCreditCard(CreditCard model)
        {
            var currentDate = DateTimeOffset.Now; //to update CustomerCredits on SP

            var customer = Session.GetCustomerInfo();

            if (model.CreditCardId == null)
            {
                //var partnerId = (customer.)
                model.CreditCardNumberId = GetNextCreditCardNumber(model.CreditCardType ?? (customer != null ? customer.CreditCardType : null)
                            , customer.CustomerId);                                                
                    //, model.PartnerId ?? Session.GetPartnerId() ?? -1);
            }

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_CreditCard_AddOrEdit]",
                    new
                    {
                        model.CreditCardId,
                        model.CreditCardNumberId,
                        model.CreditLimit,
                        model.ExpirationMonth,
                        model.ExpirationYear,
                        model.StatusId,
                        model.CardRequestId,
                        CustomerId = model.CustomerId ?? Session.GetCustomerId(),
                        currentDate.Year,
                        currentDate.Month,
                        UserId = model.UserId == -1 ? null : model.UserId,
                        VehicleId = model.VehicleId == -1 ? null : model.VehicleId,
                        model.LoggedUserId,
                        Pin = model.CreditCardId == null ? ECOsystem.Utilities.Miscellaneous.GeneratePin() : model.Pin,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the maintenancer Update of the entity CreditCard
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void EditCreditCard(CreditCard model)
        {
            var currentDate = DateTimeOffset.Now;

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_CreditCard_AddOrEdit]",
                    new
                    {
                        model.CreditCardId,
                        model.CreditLimit,
                        model.ExpirationMonth,
                        model.ExpirationYear,
                        CustomerId = Session.GetCustomerId(),
                        currentDate.Year,
                        currentDate.Month,
                        model.StatusId,
                        UserId = model.UserId == -1 ? null : model.UserId,
                        VehicleId = model.VehicleId == -1 ? null : model.VehicleId,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Change the status of the credit card
        /// </summary>
        /// <param name="IdList"></param>
        /// <param name="StatusId"></param>
        public void EditCreditCardStatus(List<int> IdList, int StatusId, int? PaymentInstrumentType, List<int> PaymentList)
        {

            if (PaymentInstrumentType == null)
            {
                PaymentInstrumentType = 0;
            }

            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;

            for (int i = 0; i < IdList.Count; i++)
            {
                if (PaymentList == null)
                {
                    root.Add(new XElement("CreditCardId", new XAttribute("Id", "" + IdList[i]), new XAttribute("PaymentType", "" + PaymentInstrumentType)));
                }
                else {
                    root.Add(new XElement("CreditCardId", new XAttribute("Id", "" + IdList[i]), new XAttribute("PaymentType", "" + PaymentList[i])));
                }
            }
            
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_CreditCardCoincaStatus_AddOrEdit]",
                    new
                    {
                        XMLDoc = doc.ToString(),
                        StatusId,
                        UserId = Session.GetUserInfo().UserId
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model CreditCard
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteCreditCard(int creditCardId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_CreditCard_Delete]",
                    new { CreditCardId = creditCardId });
            }
        }

        /// <summary>
        /// Get Next Credit Card Number
        /// </summary>
        /// <param name="creditCardType"></param>
        /// <param name="partnerId"></param>
        /// <returns>Credit Card Number Id</returns>
        private int GetNextCreditCardNumber(string creditCardType, int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = 0;
                //while (result == 0)
                //{
                    //int cardIdRandom = GetCreditCardRandomIndexByPartner(partnerId);
                    result = dba.ExecuteScalar<int>("[Control].[Sp_CreditCardNumbers_Retrieve]",
                    new
                    {
                        //CreditCardNumberId =  cardIdRandom,
                        CreditCardType = creditCardType,
                        CustomerId = customerId
                    });

                    if (result == 0)
                    {
                        throw new Exception("ERR-CARD-NOT-AVAILABLE");
                    }

                //}
                return result;
            }
        }

        /// <summary>
        /// Get Credit Card Random Index By Partner
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        private int GetCreditCardRandomIndexByPartner(int partnerId)
        {
            return new Random().Next(1, 1000);           
        }

        /// <summary>
        /// Retrieve Card Request Detail
        /// </summary>
        /// <param name="cardRequestId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CardRequest> RetrieveCardRequestNoRequest(int customerId, int? cardRequestId, string key)
        {            
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CardRequest>("[Control].[Sp_CardRequestNoCustomer_Retrieve]",
                new
                {
                    CustomerId = customerId,
                    CardRequestId = cardRequestId,
                    PendingOnly = true,
                    Key = key
                });
            }
        }

        /// <summary>
        /// Retrieve the information for the master credit card 
        /// </summary>
        /// <param name="creditCardId"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<CreditCard> RetrieveMasterCreditCard(int? creditCardId, int? customerId, string Plate = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCard>("[General].[Sp_CustomerCreditCardMaster_Retrieve]",
                new
                {                    
                    CustomerId = customerId,
                    CreditCardId = creditCardId,
                    PlateId = Plate == "" ? null : Plate
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="custId"></param>
        /// <returns></returns>
        public string ValidateDeniedTransaction(string AccountNumber)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[Control].[Sp_TransactionDeniedValidteByCard_Retrieve]",
                    new
                    {
                        EncryptedCreditCard = ECOsystem.Utilities.TripleDesEncryption.Encrypt(AccountNumber),
                        CreditCard = AccountNumber.Replace("-","").Trim()
                    });
            }
        }

        /// <summary>
        /// Retrieve web service configuration by partner
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public WServiceModel RetrieveWServiceInfoByPartner(int? partnerId)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    var wsInfo = dba.ExecuteReader<WServiceModel>("[General].[Sp_WService_Retrieve]",
                        new
                        {
                            PartnerId = partnerId

                        }).FirstOrDefault();

                    return wsInfo;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the parameters for the VPOS service
        /// </summary>
        /// <returns></returns>
        public TransactionVPOSParameters GetParametersVPOS()
        {   
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    return dba.ExecuteReader<TransactionVPOSParameters>("[General].[Sp_ParameterVPOSService_Retrieve]", new { }).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return null;
            }
        }
        
        /// <summary>
        ///  Retrieve the partner id 
        /// </summary>
        /// <returns></returns>
        public int? GetPartnerIdByCreditCard(long creditCardNumber)
        {
            try
            {
                string cc_number = ECOsystem.Utilities.TripleDesEncryption.Encrypt(ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat(creditCardNumber));
                using (var dba = new DataBaseAccess())
                {
                    return dba.ExecuteScalar<int>("[General].[Sp_PartnerIdByCreditCard_Retrieve]",
                        new
                        {
                            CreditCard = cc_number
                        });
                }
            }
            catch (Exception)
            {                
                return null;
            }
            
        }

        /// <summary>
        /// Get the answer codes from Credomatic
        /// </summary>
        /// <returns></returns>
        public IEnumerable<BACAnswerCodes> GetBacAnswerCodes()
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    return dba.ExecuteReader<BACAnswerCodes>("[Control].[Sp_BACAnswerCodeParameters_Retrieve]", new { });
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Execute Add Transactions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public TransactionsAddApiResponse ExecuteAddTransactions(CreditCardTransaction model, bool? IsVoidOrReverse = null)
        {
            
            bool isFloating = false;
            string authorizationNumber = null;
            
            model.DriverCode = (!string.IsNullOrEmpty(model.DriverCode)) ? TripleDesEncryption.Encrypt(model.DriverCode) : "CetWXCoQ4hw="; // 0000

            if (model.TransactionType == "PREAUTHORIZED_SALE")
            {
                isFloating = true;
                authorizationNumber = model.AuthorizationNumber;
                model.AccountNumber = model.AccountNumber.Replace("-", "");
            }

            Int64 par;
            if(Int64.TryParse(model.AccountNumber, out par)) model.AccountNumber = ECOsystem.Utilities.TripleDesEncryption.Encrypt(ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat(Convert.ToInt64(model.AccountNumber)));

            using (var dba = new DataBaseAccess())
            {
                #region Validate SystemTrace / TransactionPOS format

                if (!string.IsNullOrEmpty(model.SystemTraceNumber))
                {
                    //Format: 000087
                    int formatLength = 6;
                    int stLength = model.SystemTraceNumber.Trim().Length;

                    if (formatLength > stLength)
                    {
                        string currentFormat = "";

                        for (int i = 1; i <= (formatLength - stLength); i++)
                        {
                            currentFormat += "0";
                        }

                        model.SystemTraceNumber = string.Format("{0}{1}", currentFormat, model.SystemTraceNumber.Trim());
                    }
                }

                #endregion

                return dba.ExecuteReader<TransactionsAddApiResponse>("[Control].[Sp_Transactions_AddFromAPI]",
                        new
                        {
                            PlateId = model.Plate,
                            CreditCardNumber = model.AccountNumber,
                            Date = DateTime.Now,
                            Odometer = model.Odometer,
                            Liters = model.Liters,
                            FuelAmount = model.TotalAmount,
                            model.Invoice,
                            TransactionPOS = model.SystemTraceNumber,
                            SchemePOS = "SchemeVPOS",
                            MerchantDescription = model.ServiceStationName,
                            ProcessorId = model.TerminalId,
                            IsFloating = isFloating,
                            IsReversed = false,
                            IsDuplicated = false,
                            IsVoid = IsVoidOrReverse != null ? (bool)IsVoidOrReverse : false,
                            LoggedUserId = model.LoggedUserId,
                            CustomerId = model.CustomerId,
                            DriverCode = model.DriverCode,
                            IsInternational = false,
                            AuthorizationNumber = string.IsNullOrEmpty(authorizationNumber) ? model.AuthorizationNumber : authorizationNumber
                        }).FirstOrDefault();
            }
        }
        
        /// <summary>
        ///  Get the lapse time for the VPOS transaction Wait
        /// </summary>
        /// <returns></returns>
        public double GetLapseTime()
        {
            try
            {                
                using (var dba = new DataBaseAccess())
                {
                    return dba.ExecuteScalar<double>("[Control].[Sp_GetLapseTimeVPOS_Retrieve]", new { });
                }
            }
            catch (Exception)
            {                
                return 0;
            }            
        }

        /// <summary>
        /// Get the transations preauthorized
        /// </summary>
        /// <param name="CCNumber"></param>
        /// <param name="TransactionId"></param>
        /// <returns></returns>
        public IEnumerable<CreditCardTransaction> PreauthorizedTransactionsRetrieve(string CCNumber, int TransactionId = 0)
        {
            try
            {
                var CreditCardNumber = (string.IsNullOrEmpty(CCNumber)) ? null: TripleDesEncryption.Encrypt(CCNumber);

                using (var dba = new DataBaseAccess())
                {
                    return dba.ExecuteReader<CreditCardTransaction>("[Control].[Sp_PreauthorizedTransactions_Retrieve]",
                        new {
                            CreditCardNumber,
                            TransactionId
                        });
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Set Preauthorized Transactions Processed
        /// </summary>
        /// <param name="CCNumber"></param>
        /// <returns></returns>
        public IEnumerable<CreditCardTransaction> SetPreauthorizedTransactionsProcessed(int TransactionId, string AuthorizationNumber, string SystemTrace)
        {            
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCardTransaction>("[Control].[Sp_SetTransactionProcess_AddOrEdit]",
                    new
                    {
                        TransactionId,
                        AuthorizationNumber,
                        SystemTrace
                    });
            }           
        }

        /// <summary>
        /// Set Preauthorized Transactions Reverse
        /// </summary>
        /// <param name="CCNumber"></param>
        /// <returns></returns>
        public IEnumerable<CreditCardTransaction> SetPreauthorizedTransactionsReverse(int TransactionId, decimal TotalAmount)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCardTransaction>("[Control].[Sp_SetTransactionReverse_AddOrEdit]",
                    new
                    {
                        TransactionId,
                        TotalAmount
                    });
            }
        }

        /// <summary>
        /// Get Url Preauthorized Modal
        /// </summary>
        /// <param name="PartnerId"></param>
        /// <returns></returns>
        public IVRInformation GetUrlPreauthorizedModal(int PartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<IVRInformation>("[General].[Sp_URLPreauthorizedService_Retrieve]",
                    new
                    {
                        PartnerId
                    }).FirstOrDefault();
            }
        }

        public DataTable GetReportInfo(CreditCardTransaction model)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("BillDate");
                dt.Columns.Add("Customer");
                dt.Columns.Add("PaymentInstrument");
                dt.Columns.Add("PaymentInstrumentType");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("ServiceStation");
                dt.Columns.Add("BacId");
                dt.Columns.Add("Amount");
                dt.Columns.Add("Odometer");
                dt.Columns.Add("Liters");
                dt.Columns.Add("AuthorizationNumber");
                dt.Columns.Add("TerminalId");
                
                DataRow row = dt.NewRow();
                row["BillDate"] = model.Date.ToString("dd/MM/yyyy hh:mm:ss tt");
                row["Customer"] = model.Customer;
                row["PaymentInstrument"] = model.PaymentInstrument;
                row["PaymentInstrumentType"] = model.PaymentInstrumentType;
                row["PlateId"] = model.Plate;
                row["ServiceStation"] = model.ServiceStationName;
                row["BacId"] = model.BACAfiliado;
                row["Amount"] = model.TotalAmountStr;
                row["Odometer"] = model.Odometer;
                row["Liters"] = model.Liters;
                row["AuthorizationNumber"] = model.AuthorizationNumber;
                row["TerminalId"] = model.TerminalId;
                dt.Rows.Add(row);
                return dt;
            }
        }

        public void SaveEmailToSend(CreditCardTransaction model, string email)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_EmailBillToSend_Add]",
                    new
                    {
                        BillDate = model.Date,
                        CustomerName = model.Customer,
                        CreditCardNumber = ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(TripleDesEncryption.Decrypt(model.AccountNumber)), 
                        PlateId = model.Plate, 
                        ServiceStation = model.ServiceStationName, 
                        BacId = model.BACAfiliado,
                        Amount = model.TotalAmountStr, 
                        Odometer = model.Odometer, 
                        Liters = model.Liters, 
                        AuthorizationNumber = model.AuthorizationNumber, 
                        TerminalId = model.TerminalId,                         
                        Email = email,
                        CustomerId = model.CustomerId,
                        UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        public List<CreditCardTransaction> VPOSTransactionsRetrieve(ECOsystem.Models.Core.ControlFuelsReportsBase model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCardTransaction>("[General].[Sp_VPOSTransactions_Retrieve]",
                    new
                    {
                        model.StartDate,
                        model.EndDate, 
                        model.Year,
                        model.Month,
                        model.CustomerId
                    }).ToList();
            }
        }

        public CreditCardTransaction GetSaleTransaction(int TransactionId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCardTransaction>("[Control].[Sp_SaleTransaction_Retrieve]", new { TransactionId }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
        
    }    
}