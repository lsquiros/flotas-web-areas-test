﻿/************************************************************************************************************
*  File    : ParametersByCustomerBusiness.cs
*  Summary : Parameters By Customer Business Methods
*  Author  : Andrés Oviedo
*  Date    : 12/03/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Coinca.Models;

namespace  ECOsystem.Areas.Coinca.Business
{
    /// <summary>
    /// Parameters By Customer Business Class
    /// </summary>
    public class ParametersByCustomerBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Parameters By Customers
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<ParametersByCustomers> RetrieveParametersByCustomer(int? CustomerId, string ResourceKey)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ParametersByCustomers>("[General].[Sp_ParametersByCustomer_Retrieve]", new
                    {
                        CustomerId = CustomerId,
                        ResuourceKey = ResourceKey
                      
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Parameters By Customer
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditParametersByCustomer(ParametersByCustomers model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ParametersByCustomer_AddOrEdit]", new { 
                
                 ParameterByCustomerId = model.ParameterByCustomerId
	            ,CustomerId = model.CustomerId
	            ,Name=model.Name
	            ,Value=model.Value
	            ,ResuourceKey=model.ResuourceKey
	            ,LoggedUserId=model.LoggedUserId
	            ,RowVersion=model.RowVersion

                });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}