﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using  ECOsystem.Areas.Coinca.Models;
using ECOsystem.DataAccess;
using Newtonsoft.Json.Linq;

namespace  ECOsystem.Areas.Coinca.Business
{
    public class StickersBusiness : IDisposable
    {

        public int AddOrEditStickers(Stickers model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Stickers_AddOrEdit]", new
                {
                    UID = model.EncryptedUID
                   ,InsertUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public string ReadStickersPinPad(string Url, string Data)
        {
            using (var client = new HttpClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json")); 
                var response = client.PostAsJsonAsync(Url, Data).Result;
                var responsedetail = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode) throw new Exception(response.Content.ReadAsStringAsync().Result);

                return responsedetail;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }    
}