﻿using  ECOsystem.Areas.Coinca.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Business
{
    public class TermsAndConditionsBusiness : IDisposable
    {
        public List<TermsAndConditions> RetrieveTermsAndConditions(int? PartnerId = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<TermsAndConditions>("[General].[Sp_TermsAndConditions_Retrieve]", new
                {
                    PartnerId
                }).ToList();
            }
        }

        public void AddOrEditTermsAndConditions(TermsAndConditions model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_TermsAndConditions_AddOrEdit]", new
                {
                    model.Id,
                    model.Support,
                    model.Thefth,
                    model.Services,
                    model.Info,
                    model.Terms,
                    model.PartnerId,
                    model.ShowTerms,
                    model.Active,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}