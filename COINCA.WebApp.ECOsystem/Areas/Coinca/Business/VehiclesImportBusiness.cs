﻿using  ECOsystem.Areas.Coinca.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Business
{
    public class VehiclesImportBusiness : IDisposable
    {

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicles(Vehicles model)
        {           
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Vehicles_AddOrEdit]",
                new
                {
                    VehicleId = model.VehicleId,
                    PlateId = model.PlateId,
                    Name = model.Name,
                    CostCenterId = model.CostCenterId,
                    CustomerId = model.CustomerId ?? ECOsystem.Utilities.Session.GetCustomerId(),
                    UserId = model.UserId,
                    VehicleCategoryId = model.VehicleCategoryId,
                    Active = model.Active,
                    Colour = model.Colour,
                    Chassis = model.Chassis,
                    LastDallas = model.LastDallas,
                    FreightTemperature = model.FreightTemperature,
                    FreightTemperatureThreshold1 = model.FreightTemperatureThreshold1,
                    FreightTemperatureThreshold2 = model.FreightTemperatureThreshold2,
                    Predictive = model.Predictive,
                    AVL = model.AVL,
                    PhoneNumber = model.PhoneNumber,
                    Insurance = model.Insurance,
                    DateExpirationInsurance = model.DateExpirationInsurance,
                    CoverageType = model.CoverageType,
                    CabinPhone = model.CabinPhone,
                    NameEnterpriseInsurance = model.NameEnterpriseInsurance,
                    PeriodicityId = model.PeriodicityId,
                    Cost = model.Cost,
                    Odometer = model.Odometer,
                    LoggedUserId = model.LoggedUserId,
                    AdministrativeSpeedLimit = model.AdministrativeSpeedLimit,
                    IntrackReference = model.IntrackReference,
                    Imei = model.Imei,
                    RowVersion = model.RowVersion
                });
            }            
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}