﻿/************************************************************************************************************
*  File    : ParametersBusiness.cs
*  Summary : Parameters Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Coinca.Models;

namespace  ECOsystem.Areas.Coinca.Business
{
    /// <summary>
    /// Parameters Business Class
    /// </summary>
    public class ParametersBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Parameters
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Parameters> RetrieveParameters()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Parameters>("[General].[Sp_Parameters_Retrieve]", null);
            }
        }

        /// <summary>
        /// Retrieve Partner Parameters
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<ParametersByPartner> RetrievePartnerParameters(int? partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ParametersByPartner>("[General].[Sp_PartnerParameterRules_Retrieve]", new {
                    PartnerId = partnerId
                });
            }
        }


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Parameters
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditParameters(Parameters model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Parameters_AddOrEdit]", model);
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}