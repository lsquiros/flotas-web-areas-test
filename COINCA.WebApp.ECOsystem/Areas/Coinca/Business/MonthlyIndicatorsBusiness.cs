﻿using  ECOsystem.Areas.Coinca.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Business
{
    public class MonthlyIndicatorsBusiness : IDisposable
    {
        public IEnumerable<MonthlyIndicators> GetMonthlyIndicators(int Month, int Year)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<MonthlyIndicators>("[General].[Sp_MonthlyIndicator_Retrieve]", new
                {                    
                    StartDate = new DateTime(Year, Month, 1),
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public DataTable GetReportData(int Month, int Year)
        {
            var data = GetMonthlyIndicators(Month, Year);

            using (var dt = new DataTable())
            {
                dt.Columns.Add("Name");
                dt.Columns.Add("Active");
                dt.Columns.Add("Block");
                dt.Columns.Add("Process");
                dt.Columns.Add("Delivered");
                dt.Columns.Add("Closed");
                dt.Columns.Add("Canceled");
                dt.Columns.Add("Month");
                dt.Columns.Add("MonthId");
                dt.Columns.Add("ActiveYTD");
                dt.Columns.Add("BlockYTD");
                dt.Columns.Add("ProcessYTD");
                dt.Columns.Add("DeliveredYTD");
                dt.Columns.Add("ClosedYTD");
                dt.Columns.Add("CanceledYTD");
                dt.Columns.Add("ActiveFull");
                dt.Columns.Add("BlockFull");
                dt.Columns.Add("ProcessFull");
                dt.Columns.Add("DeliveredFull");
                dt.Columns.Add("ClosedFull");
                dt.Columns.Add("CanceledFull");
                dt.Columns.Add("MonthReport");
                dt.Columns.Add("PartnerId");

                foreach (var item in data)
                {
                    var dr = dt.NewRow();

                    dr["Name"] = item.Name ;
                    dr["Active"] = item.Active;
                    dr["Block"] = item.Block;
                    dr["Process"] = item.Process;
                    dr["Delivered"] = item.Delivered;
                    dr["Closed"] = item.Closed;
                    dr["Canceled"] = item.Canceled;
                    dr["Month"] = item.MonthName;
                    dr["MonthId"] = item.Month;
                    dr["ActiveYTD"] = data.Where(x => x.Type == 2 && x.PartnerId == item.PartnerId).FirstOrDefault().Active;
                    dr["BlockYTD"] = data.Where(x => x.Type == 2 && x.PartnerId == item.PartnerId).FirstOrDefault().Block;
                    dr["ProcessYTD"] = data.Where(x => x.Type == 2 && x.PartnerId == item.PartnerId).FirstOrDefault().Process;
                    dr["DeliveredYTD"] = data.Where(x => x.Type == 2 && x.PartnerId == item.PartnerId).FirstOrDefault().Delivered;
                    dr["ClosedYTD"] = data.Where(x => x.Type == 2 && x.PartnerId == item.PartnerId).FirstOrDefault().Closed;
                    dr["CanceledYTD"] = data.Where(x => x.Type == 2 && x.PartnerId == item.PartnerId).FirstOrDefault().Canceled;                    
                    dr["ActiveFull"] = data.Where(x => x.Type == 3 && x.PartnerId == item.PartnerId).FirstOrDefault().Active;
                    dr["BlockFull"] = data.Where(x => x.Type == 3 && x.PartnerId == item.PartnerId).FirstOrDefault().Block;
                    dr["ProcessFull"] = data.Where(x => x.Type == 3 && x.PartnerId == item.PartnerId).FirstOrDefault().Process;
                    dr["DeliveredFull"] = data.Where(x => x.Type == 3 && x.PartnerId == item.PartnerId).FirstOrDefault().Delivered;
                    dr["ClosedFull"] = data.Where(x => x.Type == 3 && x.PartnerId == item.PartnerId).FirstOrDefault().Closed;
                    dr["CanceledFull"] = data.Where(x => x.Type == 3 && x.PartnerId == item.PartnerId).FirstOrDefault().Canceled;
                    dr["MonthReport"] = string.Format("{0} {1}", ECOsystem.Business.Utilities.GeneralCollections.GetMonthNames.Where(x => x.Value == Month.ToString()).FirstOrDefault().Text, Year);
                    dr["PartnerId"] = item.PartnerId;

                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}