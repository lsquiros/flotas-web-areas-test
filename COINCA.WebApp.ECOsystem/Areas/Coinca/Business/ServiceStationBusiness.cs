﻿using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Business
{
    public class ServiceStationBusiness
    {
        /// <summary>
        /// Retrieve Service Stations
        /// </summary>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static IEnumerable<ServiceStations> ServiceStationRetrieve(int? id, int? countryId, int? user_id, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ServiceStations>("[General].[Sp_ServiceStations_Retrieve]", new
                {
                    ServiceStationId = id,
                    CountryId = countryId,
                    Key = key,
                    UserId = user_id
                });
            }
        }
    }
}