﻿/************************************************************************************************************
*  File    : FaqBusiness.cs
*  Summary : Faq Business Methods
*  Author  : Gerald Solano
*  Date    : 12/03/2015
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using  ECOsystem.Areas.Coinca.Models;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Business
{
    /// <summary>
    /// FAQ Business Retrive Data
    /// </summary>
    public class FaqBusiness : IDisposable
    {
        /// <summary>
        /// Get Data from file CSV
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public IList<FaqModel> getDataFAQ(string typeName, string root) 
        {
            try
            {
                string fileName = string.Format("faq_{0}.csv", typeName);
                string path = Path.Combine(root, fileName);
                if (File.Exists(path))
                {
                    using (CsvFileReader reader = new CsvFileReader(path))
                    {

                        int indexLine = 0; // 0 = title Names
                        CsvRow row = new CsvRow();
                        var dataFromCSV = new List<FaqModel>();
                        
                        while (reader.ReadRow(row))
                        {
                            if (indexLine > 0)
                            {
                                dataFromCSV.Add(new FaqModel() { Index = indexLine, Question = row[0], Answer = row[1] });
                            }
                            indexLine++;
                        }

                        return dataFromCSV;
                    }

                }

                return new List<FaqModel>();

            }
            catch (Exception)
            {
                return new List<FaqModel>();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}