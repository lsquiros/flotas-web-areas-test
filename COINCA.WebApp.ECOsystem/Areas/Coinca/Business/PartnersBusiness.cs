﻿/************************************************************************************************************
*  File    : PartnersBusiness.cs
*  Summary : Partners Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Business.Core;
using ECOsystem.Models.Core;

namespace  ECOsystem.Areas.Coinca.Business
{
    /// <summary>
    /// Partners Business Class
    /// </summary>
    public class PartnersBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Partners
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Partners> RetrievePartners(int? partnerId, string key = null)
        {
            IList<Partners> result;
            using (var dba = new DataBaseAccess())
            {
                result = dba.ExecuteReader<Partners>("[General].[Sp_Partners_Retrieve]",
                    new
                    {
                        PartnerId = partnerId,
                        Key = key
                    });
            }
            if (partnerId == null || result.FirstOrDefault() == null) return result;

            using (var business = new UsersBusiness())
            {
                var aux = result.FirstOrDefault();
                aux.TempUsersList = business.RetrieveUsers(null, partnerId: partnerId).Select(x => new PartnerTempUser { UserFullName = x.DecryptedName, UserEmail = x.DecryptedEmail, RoleName = x.RoleName }).ToList();
                return new List<Partners> { aux };
            }
        }

        /// <summary>
        /// Retrieve Partners
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Partners> RetrievePartnersByLoggedUser()
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                if (user.IsPartnerUser)
                {
                    return dba.ExecuteReader<Partners>("[General].[Sp_Partners_Retrieve]",
                    new
                    {
                        user.PartnerGroupId,
                        user.PartnerId
                    });
                }
                return new List<Partners>();
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Partners
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public int AddOrEditPartners(Partners model)
        {
            int? returnValue;
            using (var dba = new DataBaseAccess())
            {
                returnValue = dba.ExecuteScalar<int>("[General].[Sp_Partners_AddOrEdit]",
                    new
                    {
                        model.PartnerId,
                        model.Name,
                        model.CountryId,
                        model.Logo,
                        model.ApiUserName,
                        model.ApiPassword,
                        model.CapacityUnitId,
                        model.PartnerTypeId,
                        model.LoggedUserId,
                        model.FuelErrorPercent,
                        model.IVRLink,
                        model.IVRPhone,
                        model.RowVersion,
                        model.ProductTypesId
                    });
            }
            return returnValue ?? 0;
        }

        /// <summary>
        /// Performs the the operation of delete on the model Partners
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeletePartners(int partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Partners_Delete]",
                    new { PartnerId = partnerId });
            }
        }

        public IEnumerable<Customers> RetrieveBFCustomers(int? CustomerId = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Customers>("[General].[Sp_BFCustomers_Retrieve]", new { CustomerId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}