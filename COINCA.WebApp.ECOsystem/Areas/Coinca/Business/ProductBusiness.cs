﻿/************************************************************************************************************
*  File    : TypesBusiness.cs
*  Summary : Types Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Coinca.Models;

namespace  ECOsystem.Areas.Coinca.Business
{
    /// <summary>
    /// Parameters Business Class
    /// </summary>
    public class ProductBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Parameters
        /// </summary>
        /// <param name="usage"></param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Products> GetAll()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Products>("[General].[Sp_ProductTypes_Retrieve]", null);
            }
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}