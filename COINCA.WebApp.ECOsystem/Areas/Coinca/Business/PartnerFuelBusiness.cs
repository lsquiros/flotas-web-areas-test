﻿/************************************************************************************************************
*  File    : PartnerFuelBusiness.cs
*  Summary : PartnerFuel Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Coinca.Models;
using System.Globalization;
using System.Data;
using System.Web;

namespace  ECOsystem.Areas.Coinca.Business
{
    /// <summary>
    /// PartnerFuel Class
    /// </summary>
    public class PartnerFuelBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve PartnerFuel
        /// </summary>
        /// <param name="partnerFuelId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="partnerId">The FK from partners table</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PartnerFuel> RetrievePartnerFuel(int? partnerFuelId, int? partnerId, bool? IsScheduled, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerFuel>("[Control].[Sp_PartnerFuel_Retrieve]",
                    new
                    {
                        PartnerFuelId = partnerFuelId,
                        PartnerId = partnerId,
                        IsScheduled,
                        UserId
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity PartnerFuel
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditPartnerFuel(PartnerFuel model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_PartnerFuel_AddOrEdit]",
                    new
                    {
                        model.PartnerFuelId,
                        model.PartnerId,
                        model.FuelId,
                        model.LiterPrice,
                        StartDate = model.StartDate, //Convert.ToDateTime(DateTime.ParseExact(model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.CurrentCulture)),
                        EndDate = model.EndDate, //Convert.ToDateTime(DateTime.ParseExact(model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.CurrentCulture)),
                        model.IsScheduled,
                        model.LoggedUserId
                    });
            }

        }

        /// <summary>
        /// Retrieve the information for the Report
        /// </summary>
        /// <param name="PartnerId"></param>
        /// <param name="FuelId"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public DataTable RetrievePartnerFuelReport(PartnerFuel model)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<PartnerFuel>("[Control].[Sp_PartnerFuelReport_Retrieve]",
                    new
                    {
                        model.PartnerId,
                        model.FuelId,
                        StartDate = model.StartDate, //Convert.ToDateTime(DateTime.ParseExact(model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)),
                        EndDate = model.EndDate //Convert.ToDateTime(DateTime.ParseExact(model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)),                        
                    });

                return GetReportDataTable(result);
            }
        }

        /// <summary>
        /// Get the CountryId by partner
        /// </summary>
        /// <param name="PartnerId"></param>
        /// <returns></returns>
        public int GetCountryIdByPartner(int PartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_CountryIdByPartner_Retrieve]",
                    new
                    {
                        PartnerId
                    });
            }
        }

        /// <summary>
        /// Get the data table ready for the Denied Transactions Report by Customer
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private DataTable GetReportDataTable(IEnumerable<PartnerFuel> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("Name");
                dt.Columns.Add("LiterPrice");
                dt.Columns.Add("StartDate");
                dt.Columns.Add("EndDate");
                dt.Columns.Add("Lapse");
                dt.Columns.Add("Variation");
                dt.Columns.Add("FuelName");
                dt.Columns.Add("PartnerName");
                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["Name"] = item.FuelName;
                    dr["LiterPrice"] = item.LiterPrice;
                    dr["StartDate"] = item.StartDate;
                    dr["EndDate"] = item.EndDate;
                    dr["Lapse"] = item.Lapse;
                    dr["Variation"] = item.Variation;
                    dr["FuelName"] = item.FuelName;
                    dr["PartnerName"] = HttpContext.Current.Session["PartnerName"].ToString();
                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }
        
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}