﻿using ECOsystem.DataAccess;
using  ECOsystem.Areas.Coinca.Models;
using System;
using System.Collections.Generic;

namespace  ECOsystem.Areas.Coinca.Business
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomerByPartnerBusiness : IDisposable
    {

        /// <summary>
        /// Retrieve Customers by Partner
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Customers> RetrieveCustomersByPartners(int? partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Customers>("[General].[Sp_CustomersByPartners_Retrieve]",
                    new
                    {
                        PartnerId = partnerId
                    });
            }
        }

        /// <summary>
        /// Retrieve Customers
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Customers> RetrieveAllCustomers()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Customers>("[General].[Sp_AllCustomers_Retrieve]",
                    new
                    {

                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CustomersByPartner
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditCustomersByPartner(CustomerByPartnerData model)
        {
            DeleteCustomersByPartners(model.PartnerId);
            using (var dba = new DataBaseAccess())
            {
                foreach (Customers customer in model.ConnectedCustomersList)
                {
                    dba.ExecuteNonQuery("[General].[Sp_CustomersByPartners_AddOrEdit]",
                        new
                        {
                            customer.CustomerId,
                            model.PartnerId,
                            model.LoggedUserId
                        });
                }
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model CustomersByPartners
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteCustomersByPartners(int? partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_CustomersByPartners_Delete]",
                    new { PartnerId = partnerId });
            }
        }
                
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}