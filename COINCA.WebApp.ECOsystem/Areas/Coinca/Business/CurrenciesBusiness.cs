﻿/************************************************************************************************************
*  File    : CurrenciesBusiness.cs
*  Summary : Currencies Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Coinca.Models;

namespace  ECOsystem.Areas.Coinca.Business
{
    /// <summary>
    /// Currencies Class
    /// </summary>
    public class CurrenciesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Currencies
        /// </summary>
        /// <param name="currencyId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Currencies> RetrieveCurrencies(int? currencyId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Currencies>("[Control].[Sp_Currencies_Retrieve]",
                new
                {
                    CurrencyId = currencyId,
                    Key = key
                });
            }
        }


        /// <summary>
        /// Retrieve Currencies
        /// </summary>
        /// <param name="customerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Currencies> RetrieveCurrenciesByCustomer(int? customerId)
        {
            using (var dba = new DataBaseAccess())
            { 
                return dba.ExecuteReader<Currencies>("[Control].[Sp_CurrenciesByCustomer_Retrieve]",
                new {
                    CustomerId = customerId
                });
            }
        }


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Currencies
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditCurrencies(Currencies model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_Currencies_AddOrEdit]",
                new
                {
                    model.CurrencyId,
                    model.Name,
                    model.Code,
                    model.Symbol,
                    model.LoggedUserId,
                    model.RowVersion
                });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model Currencies
        /// </summary>
        /// <param name="currencyId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteCurrencies(int currencyId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_Currencies_Delete]",
                    new { CurrencyId = currencyId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}