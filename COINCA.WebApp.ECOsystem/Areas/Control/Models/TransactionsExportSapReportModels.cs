﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models
{
    public class TransactionsExportSapReport
    {
        public string VehicleSAP { get; set; }

        public DateTime Date { get; set; }

        public string DateStr { get { return Date.ToString("dd/MM/yyyy"); } }

        public string TimeStr { get { return Date.ToString("HH:mm:ss"); } }

        public string Responsable { get; set; }

        public string ResponsableDecrypt { get { return string.IsNullOrEmpty(Responsable) ? Responsable : ECOsystem.Utilities.TripleDesEncryption.Decrypt(Responsable); } }

        public string ServiceStation { get; set; }

        public decimal Liters { get; set; }

        public string Route { get; set; }

        public int Odometer { get; set; }
    }
}