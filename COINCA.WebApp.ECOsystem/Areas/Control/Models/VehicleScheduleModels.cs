﻿/************************************************************************************************************
*  File    : VehicleScheduleModels.cs
*  Summary : VehicleSchedule Models
*  Author  : Berman Romero
*  Date    : 09/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using System.Collections.Generic;

namespace ECO_Control.Models.Control
{
    /// <summary>
    /// VehicleSchedule Base
    /// </summary>
    public class VehicleScheduleBase
    {

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleSchedule Data { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Vehicle model
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Alarms of VehicleSchedule
        /// </summary>
        public AlarmsModels Alarm { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// VehicleSchedule Model
    /// </summary>
    public class VehicleSchedule : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Vehicles model
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Json Schedule to load canvas grid
        /// </summary>
        public string JsonSchedule { get; set; }

        /// <summary>
        /// Json Schedule to get the Schedule from DB
        /// </summary>
        public string XmlSchedule { get; set; }

    }

    /// <summary>
    /// Vehicle Schedule for Json parse
    /// </summary>
    public class VehicleScheduleJson
    {
        /// <summary>
        /// id with schedule selected
        /// </summary>
        public string id { get; set; }

    }
}