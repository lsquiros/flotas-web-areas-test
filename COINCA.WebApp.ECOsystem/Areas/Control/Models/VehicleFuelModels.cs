﻿/************************************************************************************************************
*  File    : VehicleFuelModels.cs
*  Summary : VehicleFuel Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// VehicleFuel Base
    /// </summary>
    public class VehicleFuelBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public VehicleFuelBase()
        {
            KeyId = 0;
            Data = new VehicleFuel();
            List = new List<VehicleFuel>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleFuel Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleFuel> List { get; set; }

        /// <summary>
        /// Menus 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// KeyId for load the fuel by Month and Year
        /// </summary>
        public int KeyId { get; set; }

        /// <summary>
        /// Liter Price by current FuelId
        /// </summary>
        public decimal? LiterPrice { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }
    }


    /// <summary>
    /// VehicleFuel Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class VehicleFuel : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? Id { get; set; }


        /// <summary>
        /// FK Vehicle Id from Users, each vehicle is associate with one user
        /// </summary>
        [DisplayName("Vehículo")]
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Fuel Name
        /// </summary>
        [DisplayName("Combustible")]
        [GridColumn(Title = "Combustible", Width = "150px", SortEnabled = true)]
        public string FuelName { get; set; }

        /// <summary>
        /// Fuel Id
        /// </summary>
        [NotMappedColumn]
        public int? FuelId { get; set; }

        private decimal? _liters; 

        /// <summary>
        /// Liters of fuel
        /// </summary>
        [DisplayName("Litros")]
        [NotMappedColumn]
        public decimal? Liters
        {   get { return _liters; }
            set { _liters = value; } 
        }

        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [GridColumn(Title = "Unidad de Capacidad", Width = "50px", SortEnabled = true)]
        public decimal CapacityUnitValue
        {
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters); }
            set { Liters = Convert.ToDecimal(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, value)); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [DisplayName("Unidad de Capacidad")]
        [Required(ErrorMessage = "{0} es requerido")]
        [StringLength(16, ErrorMessage = "{0} tiene un máximo de 10 dígitos.")]
        [GridColumn(Title = "Unidad de Capacidad", Width = "50px", SortEnabled = true)]
        public string CapacityUnitValueStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(CapacityUnitValue); }
        } 

        /// <summary>
        /// Year
        /// </summary>
        [DisplayName("Año")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        [DisplayName("Mes")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? Month { get; set; }

        /// <summary>
        /// Month Name
        /// </summary>
        [DisplayName("Mes")]
        [NotMappedColumn]
        public string MonthName
        {
            get
            {
                if (Month == null) return "";
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var firstOrDefault = monthNames.Select(m => new { Month = monthNames.IndexOf(m) + 1, MonthNames = new CultureInfo("es-CR").TextInfo.ToTitleCase(m) }).FirstOrDefault(x => x.Month == Month);
                if (firstOrDefault != null) return firstOrDefault.MonthNames;
                return "";
            }
        }

        /// <summary>
        /// Credit Amount
        /// </summary>
        [NotMappedColumn]
        public decimal? Amount { get; set; }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [DisplayName("Monto")]
        [Required(ErrorMessage = "{0} es requerido")]
        [StringLength(16, ErrorMessage = "{0} tiene un máximo de 10 dígitos.")]
        [NotMappedColumn]
        public string AmountStr
        {
            get { return Amount == null ? "" : ((decimal)Amount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }
        /// <summary>
        /// Amount Str as Currency
        /// </summary>
        [GridColumn(Title = "Monto Asignado", Width = "100px", SortEnabled = true)]
        public string AmountStrCurrency
        {
            get { return CurrencySymbol + AmountStr; }
        }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        public int? DriverId { get; set; }

        public int? DriverFuelId { get; set; }
        
    }

    /// <summary>
    /// Vehicle by Fuel Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class VehicleByFuel : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? Id { get; set; }

        /// <summary>
        /// Fuel Id
        /// </summary>
        [NotMappedColumn]
        public int? FuelId { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Vehicle Plate Id
        /// </summary>
        [GridColumn(Title = "Matrícula", Width = "50px")]
        public string PlateId { get; set; }

        /// <summary>
        /// Vehicle Name
        /// </summary>
        [GridColumn(Title = "Nombre del Vehículo", Width = "150px")]
        public string VehicleName { get; set; }

        /// <summary>
        /// Vehicle Model
        /// </summary>
        [GridColumn(Title = "Modelo", Width = "100px")]
        public string VehicleModel { get; set; }

        /// <summary>
        /// Liters of fuel
        /// </summary>
        [NotMappedColumn]
        public decimal? Liters { get; set; }

        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [NotMappedColumn]
        public decimal CapacityUnitValue
        {
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters); }
            set { Liters = ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, value);  }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [DisplayName("Unidad de Capacidad")]
        [NotMappedColumn]
        public string CapacityUnitValueStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(CapacityUnitValue); }
        }

        /// <summary>
        /// Credit Amount
        /// </summary>
        [NotMappedColumn]
        public decimal? Amount { get; set; }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [GridColumn(Title = "Monto Combustible", Width = "90px")]
        public string AmountStr
        {
            get { return Amount == null ? "" : CurrencySymbol + ((decimal)Amount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

    }
}