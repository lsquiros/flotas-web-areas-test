﻿/************************************************************************************************************
*  File    : TransactionsReportModels.cs
*  Summary : Transaction Report Models
*  Author  : Danilo Hidalgo
*  Date    : 12/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Collections.Generic;
using System.ComponentModel;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using Newtonsoft.Json;
using ECOsystem.Utilities;
using ECOsystem.Models.Account;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Transactions Report Base
    /// </summary>
    public class TransactionsReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public TransactionsReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<TransactionsReport>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        //public IEnumerable<dynamic> List { get; set; }
        public IEnumerable<TransactionsReport> List { get; set; }
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Transactions Report Model
    /// </summary>
    public class TransactionsReport
    {
        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Transacción Id")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int TransactionId { get; set; }

        /// <summary>
        /// FilterType
        /// </summary>        
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int FilterType { get; set; }

        /// <summary>
        /// PaymentInstrumentType
        /// </summary>
        [DisplayName("Medio de Pago")]
        public string PaymentInstrumentType { get; set; }

        /// <summary>
        /// CreditCard With Format Number and Mask
        /// </summary>        
        [DisplayName("Tarjeta")]
        [NotMappedColumn]
        public string CreditCardNumberWithMask { get { return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(CreditCardNumberDescrypt); } }

        /// <summary>
        /// Credit Card Number
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// CreditCard With Format Number
        /// </summary>        
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardNumberDescrypt { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(CreditCardNumber); } }

        /// <summary>
        /// PaymentInstrumentCode
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string PaymentInstrumentCode { get; set; }

        /// <summary>
        /// PaymentInstrument
        /// </summary>
        [ExcelMappedColumn("# Medio de Pago")]
        [GridColumn(Title = "Medio de Pago", Width = "80px", SortEnabled = true)]
        public string PaymentInstrument
        {
            get
            {
                return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(ECOsystem.Utilities.TripleDesEncryption.Decrypt(PaymentInstrumentCode));
            }
        }
        /// <summary>
        /// HolderName
        /// </summary>
        [DisplayName("Titular")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Titular", Width = "120px", SortEnabled = true)]
        public string HolderName { get; set; }

        /// <summary>
        /// HolderNameStr
        /// </summary>
        [DisplayName("Titular")]
        [GridColumn(Title = "Titular", Width = "120px", SortEnabled = true)]
        public string HolderNameStr { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(HolderName); } }

        /// <summary>
        /// Invoice - # Factura
        /// </summary>
        [DisplayName("#Factura")]
        public string Invoice { get; set; }

        /// <summary>
        /// System Trace Code
        /// </summary>
        [DisplayName("SystemTraceCode")]
        public string SystemTraceCode { get; set; }

        public string AuthorizationNumber { get; set; }
        public int UnitOfCapacityId { get; set; }

        /// <summary>
        /// MerchantDescription - Gasolinera
        /// </summary>
        [DisplayName("Gasolinera")]
        public string MerchantDescription { get; set; }

        /// <summary>
        /// FuelName
        /// </summary>
        [DisplayName("Combustible")]
        [GridColumn(Title = "Combustible", Width = "120px", SortEnabled = true)]
        public string FuelName { get; set; }

        /// <summary>
        /// Liters
        /// </summary>
        [DisplayName("Litros")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Litros", Width = "120px", SortEnabled = true)]
        public decimal Liters { get; set; }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [DisplayName("Litros")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Litros", Width = "120px", SortEnabled = true)]
        public string LitersStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Liters, 3); }
        }

        /// <summary>
        /// LitersLast
        /// </summary>
        [DisplayName("LitersLast")]
        [ExcelNoMappedColumn]
        public decimal LitersLast { get; set; }


        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal? CapacityUnitValue
        {
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [ExcelMappedColumn("Litros")]
        [GridColumn(Title = "Unidad de Capacidad", Width = "120px", SortEnabled = true)]
        public string CapacityUnitValueStr
        {
            //get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(CapacityUnitValue); }
            get
            {
                return CapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(CapacityUnitValue, 3);//.Replace(",","."); 
            }

        }

        /// <summary>
        /// Odometer
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double Odometer { get; set; }

        /// <summary>
        /// Odometer format
        /// </summary>
        [DisplayName("Odómetro")]
        [GridColumn(Title = "Odómetro", Width = "120px", SortEnabled = true)]
        public string OdometerStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Convert.ToDecimal(Odometer)); } }

        /// <summary>
        /// Odometer format
        /// </summary>
        [DisplayName("Odómetro")]
        [GridColumn(Title = "Odómetro", Width = "120px", SortEnabled = true)]
        public string OdometerStrCR { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(Convert.ToDecimal(Odometer)); } }

        /// <summary>
        /// OdometerLast
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double OdometerLast { get; set; }

        /// <summary>
        /// Odometer format
        /// </summary>
        [DisplayName("Ultimo Odómetro")]
        [GridColumn(Title = "Ultimo Odómetro", Width = "120px", SortEnabled = true)]
        public string OdometerLastStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(Convert.ToDecimal(OdometerLast)); } }

        /// <summary>
        /// Recorrido
        /// </summary>
        [DisplayName("Travel")]
        [GridColumn(Title = "Recorrido", Width = "120px", SortEnabled = true)]
        public decimal Travel { get { return (Convert.ToDecimal(Odometer - OdometerLast)); } }


        /// <summary>
        /// Rendimiento
        /// </summary>
        [DisplayName("Travel")]
        [GridColumn(Title = "Recorrido", Width = "120px", SortEnabled = true)]
        public string TravelStrCR
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(Travel); }
        }


        /// <summary>
        /// Rendimiento
        /// </summary>
        [DisplayName("Performance")]
        [GridColumn(Title = "Rendimiento", Width = "120px", SortEnabled = true)]
        public decimal Performance { get { return ((CapacityUnitValue > 0) ? Decimal.Round(Travel / (decimal)CapacityUnitValue, 2) : 0); } }

        /// <summary>
        /// Rendimiento
        /// </summary>
        [DisplayName("Performance")]
        [GridColumn(Title = "Rendimiento", Width = "120px", SortEnabled = true)]
        public string PerformanceStrCR
        {
            //get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(FuelAmount); }
            //get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AssignedAmount, CurrencySymbol); }
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(Performance); }
        }


        /// <summary>
        /// KilometersCost
        /// </summary>
        [DisplayName("KilometersCost")]
        [GridColumn(Title = "Costo por Kilómetro", Width = "120px", SortEnabled = true)]
        public decimal KilometersCost { get { return ((Travel > 0) ? Decimal.Round(RealAmount / Travel, 2) : 0); } }

        /// <summary>
        /// KilometersCost
        /// </summary>
        [DisplayName("KilometersCost")]
        [GridColumn(Title = "Costo por Kilómetro", Width = "120px", SortEnabled = true)]
        public string KilometersCostStrCR
        {
            //get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(FuelAmount); }
            //get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AssignedAmount, CurrencySymbol); }
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(KilometersCost); }
        }



        /// <summary>
        /// FuelAmount
        /// </summary>
        [DisplayName("Monto")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Monto", Width = "120px", SortEnabled = true)]
        public decimal FuelAmount { get; set; }

        /// <summary>
        /// Fuel Amount as String
        /// </summary>
        [DisplayName("Monto")]
        [NotMappedColumn]
        public string FuelAmountStr
        {
            //get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(FuelAmount); }
            //get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AssignedAmount, CurrencySymbol); }
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCurrencyCR(FuelAmount, CurrencySymbol); }
        }

        /// <summary>
        /// Fuel Amount as String
        /// </summary>
        [DisplayName("Monto")]
        [NotMappedColumn]
        public string FuelAmountStrCR
        {
            //get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(FuelAmount); }
            //get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AssignedAmount, CurrencySymbol); }
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(FuelAmount); }
        }




        /// <summary>
        /// Reference
        /// </summary>
        [DisplayName("Reference")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Referencia", Width = "120px", SortEnabled = true)]
        public string Reference { get; set; }

        /// <summary>
        /// Transaction State
        /// </summary>
        [DisplayName("Estado Transacción")]
        public string State { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        [GridColumn(Title = "Fecha", Width = "120px", SortEnabled = true)]
        public DateTime? Date { get; set; }

        public string ExpirationDate { get; set; }

        public string ExpirationDateStr
        {
            get
            {
                if (string.IsNullOrEmpty(ExpirationDate))
                    return "";
                else
                {
                    var arr = ExpirationDate.ToCharArray();
                    return arr.Length == 4 ? String.Format("{0}{1}/{2}{3}", arr[2], arr[3], arr[0], arr[1]) : "";
                }
            }
        }
        /// <summary>
        /// Date To Report Export
        /// </summary>
        [DisplayName("Fecha Emisión")]
        [NotMappedColumn]
        public string DateToExport
        {
            get
            {
                return ((Date != null) ? Convert.ToDateTime(Date).ToString("dd/MM/yyyy hh:mm:ss tt") : string.Empty);
            }
        }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Fecha", Width = "120px", SortEnabled = true)]
        public string DateStr
        {
            get
            {
                return ECOsystem.Utilities.Miscellaneous.GetDateFormat(Date);
            }
        }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Fecha", Width = "120px", SortEnabled = true)]
        public string DateTimeStr { get { return ECOsystem.Utilities.Miscellaneous.GetDateTimeFormat(Date); } }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Moneda")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        [DisplayName("Mensaje")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string Message { get; set; }

        /// <summary>
        /// CreditCardHolder
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardHolder { get; set; }

        /// <summary>
        /// Transport Data
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string TransportData { get; set; }

        /// <summary>
        ///  Transport Data Obj
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public TransportDataModel TransportDataObject
        {
            get
            {
                return JsonConvert.DeserializeObject<TransportDataModel>(this.TransportData);
            }
        }

        public string Response { get; set; }

        public string ResponseCodeSplitted
        {
            get
            {
                return Response != null ? Response.Split('|')[0] : null;
            }
        }

        public string ResponseDescriptionSplitted
        {
            get
            {
                return Response != null ? Response.Split('|')[1] : null;
            }
        }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Fecha Creada")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public DateTime? InsertDate { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Codigo de Respuesta")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string ResponseCode { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("descripcion Respuesta")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string ResponseCodeDescription { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Customer ID")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// CostCenter Name
        /// </summary>
        [DisplayName("CostCenter Name")]
        [ExcelMappedColumn("Centro de Costo")]
        [NotMappedColumn]
        public string CostCenterName { get; set; }

        public int? CostCenterId { get; set; }

        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Service Station Name
        /// </summary>
        [DisplayName("Estación de servicio")]
        public string ServiceStationName { get; set; }


        /// <summary>
        /// VehicleGroupName
        /// </summary>
        [DisplayName("VehicleGroupName")]
        [ExcelMappedColumn("Grupo de Vehículo")]
        [NotMappedColumn]
        public string VehicleGroupName { get; set; }

        /// <summary>
        /// DriverId
        /// </summary>
        [DisplayName("DriverId")]
        [ExcelMappedColumn("Numero de cedula")]
        [NotMappedColumn]
        public string DriverId { get; set; }


        /// <summary>
        /// DriverId Desencriptado
        /// </summary>        
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string DriverIdDescrypt { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(DriverId); } }

        /// <summary>
        /// DriverName
        /// </summary>
        [DisplayName("DriverName")]
        [ExcelMappedColumn("Nombre del conductor")]
        [NotMappedColumn]
        public string DriverName { get; set; }


        /// <summary>
        /// DriverName Desencriptado
        /// </summary>        
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string DriverNameDescrypt { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(DriverName); } }

        /// <summary>
        /// UnitName
        /// </summary>
        [DisplayName("UnitName")]
        [ExcelMappedColumn("Nombre de la Unidad")]
        [NotMappedColumn]
        public string UnitName { get; set; }


        /// <summary>
        /// Manufacturer
        /// </summary>
        [DisplayName("Manufacturer")]
        [ExcelMappedColumn("Fabricante")]
        [NotMappedColumn]
        public string Manufacturer { get; set; }


        /// <summary>
        /// MCC
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int MCC { get; set; }

        /// <summary>
        /// Identification
        /// </summary>        
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string HolderId { get; set; }

        /// <summary>
        /// Identification
        /// </summary>        
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string HolderIdDescrypt { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(HolderId); } }

        /// <summary>
        /// Terminal Id
        /// </summary>
        [ExcelMappedColumn("Terminal Id")]
        [NotMappedColumn]
        public string TerminalId { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        [ExcelMappedColumn("Nombre Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }

        /// <summary>
        /// VehiclePlate
        /// </summary>
        [ExcelMappedColumn("Placa")]
        [NotMappedColumn]
        public string PlateId { get; set; }

        /// <summary>
        /// CustomerName
        /// </summary>
        [DisplayName("Customer Name")]
        [NotMappedColumn]
        public string CustomerName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(_customerName);
            }
            set
            {
                this._customerName = value;
            }
        }

        private string _customerName;

        /// <summary>nch
        /// CustomerBranchName
        /// </summary>
        [DisplayName("Customer Branch Name")]
        [NotMappedColumn]
        public string CustomerBranchName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(_customerBranchName);
            }
            set
            {
                this._customerBranchName = value;
            }
        }

        private string _customerBranchName;

        /// <summary>
        /// ElectronicInvoice Field
        /// </summary>
        [DisplayName("Electronic Invoice")]
        [NotMappedColumn]
        public string ElectronicInvoice { get; set; }

        /// <summary>nch
        /// IsCustomerBranch
        /// </summary>
        [DisplayName("IsCustomerBranch")]
        [NotMappedColumn]
        public int IsCustomerBranch { get; set; }

        /// <summary>
        /// Country Name
        /// </summary>
        [DisplayName("País")]
        [ExcelMappedColumn("País")]
        [GridColumn(Title = "País", Width = "120px", SortEnabled = true)]
        public string CountryName { get; set; }

        /// <summary>
        /// Total of Trx Traveled Odometer
        /// </summary>
        [DisplayName("Tipo Cambio")]
        [ExcelMappedColumn("Tipo Cambio")]
        [GridColumn(Title = "Tipo Cambio", Width = "120px", SortEnabled = true)]
        public string ExchangeValue { get; set; }

        /// <summary>
        /// Real Amount of Trx 
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal RealAmount { get; set; }

        /// <summary>
        /// Real Amount String
        /// </summary>
        [DisplayName("Monto Real")]
        [ExcelMappedColumn("Monto Real")]
        [GridColumn(Title = "Monto Real", Width = "120px", SortEnabled = true)]
        public string RealAmountFormatted
        {
            get { return RealAmount == null ? "N/A" : ((decimal)RealAmount).ToString("N", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); }
        }

        /// <summary>
        /// Validate if the MasterCard IsInternational
        /// </summary>
        public bool IsInternational { get; set; }
    }
    /// <summary>
    /// Transaction Deny Report Model
    /// </summary>
    public class TransactionDenyReportModel
    {
        /// <summary>
        /// ElectronicInvoice Field
        /// </summary>
        /// <summary>
        /// ElectronicInvoice Field
        /// </summary>
        [DisplayName("Electronic Invoice")]
        [NotMappedColumn]
        public string ElectronicInvoice { get; set; }

        private string _customerBranchName;

        private string _customerName;
        public string CustomerBranchName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(_customerBranchName);
            }
            set
            {
                this._customerBranchName = value;
            }
        }

        public int IsCustomerBranch { get; set; }

        public string CustomerName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(_customerName);
            }
            set
            {
                this._customerName = value;
            }
        }
        /// <summary>
        /// Credit Card Number
        /// </summary>
        [DisplayName("#Tarjeta")]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// PaymentInstrumentType
        /// </summary>
        [DisplayName("Medio de Pago")]
        public string PaymentInstrumentType { get; set; }

        /// <summary>
        /// PaymentInstrumentCode
        /// </summary>
        public string PaymentInstrumentCode { get; set; }

        /// <summary>
        /// PaymentInstrument
        /// </summary>
        [DisplayName("# Medio de Pago")]
        public string PaymentInstrument
        {
            get
            {
                if (PaymentInstrumentType == "Tarjeta" || PaymentInstrumentType == null)
                    return CreditCardNumber;
                else
                    return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(ECOsystem.Utilities.TripleDesEncryption.Decrypt(PaymentInstrumentCode));
            }
        }

        /// <summary>
        /// ResponseCode
        /// </summary>
        [DisplayName("Código")]
        public string ResponseCode { get; set; }

        /// <summary>
        /// ResponseCodeDescription
        /// </summary>
        [DisplayName("Descripción")]
        public string ResponseCodeDescription { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        [DisplayName("Mensaje")]
        public string Message { get; set; }

        /// <summary>
        /// CreditCardHolder
        /// </summary>
        [DisplayName("Titular")]
        public string CreditCardHolder { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        [ExcelNoMappedColumn]
        public DateTime? InsertDate { get; set; }

        /// <summary>
        /// Date To Report Export
        /// </summary>
        [DisplayName("Fecha Emisión")]
        [NotMappedColumn]
        public string DateToExport
        {
            get
            {
                return ((InsertDate != null) ? Convert.ToDateTime(InsertDate).ToString("dd/MM/yyyy hh:mm:ss tt") : string.Empty);
            }
        }

        /// <summary>
        /// Terminal Id
        /// </summary>
        [ExcelMappedColumn("Terminal Id")]
        [NotMappedColumn]
        public string TerminalId { get; set; }

        /// <summary>
        /// Service Station Name
        /// </summary>
        [DisplayName("Estación de servicio")]
        public string ServiceStationName { get; set; }

        /// <summary>
        /// Country Name
        /// </summary>
        [ExcelMappedColumn("País")]
        [DisplayName("País")]
        [GridColumn(Title = "País", Width = "120px", SortEnabled = true)]
        public string CountryName { get; set; }

        //// <summary>
        /// Total of Trx Traveled Odometer
        /// </summary>
        public string ExchangeValue { get; set; }

        /// <summary>
        /// Real Amount of Trx 
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? RealAmount { get; set; }

        /// <summary>
        /// Real Amount String
        /// </summary>
        [DisplayName("Monto Real")]
        [ExcelMappedColumn("Monto Real")]
        [GridColumn(Title = "Monto Real", Width = "120px", SortEnabled = true)]
        public string RealAmountFormatted
        {
            get { return RealAmount == null ? "N/A" : ((decimal)RealAmount).ToString("N", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); }
        }
    }

    /// <summary>
    /// Transport data for transactions log
    /// </summary>
    public class TransportDataModel
    {

        public string transactionType { get; set; }

        /// <summary>
        /// PaymentInstrumentType
        /// </summary>
        [DisplayName("Medio de Pago")]
        public string PaymentInstrumentType { get; set; }

        /// <summary>
        /// PaymentInstrumentCode
        /// </summary>
        public string PaymentInstrumentCode { get; set; }

        public long? cardNumber { get; set; }

        [DisplayName("Número de Tarjeta")]
        public string CreditCardNumberWithMask
        {
            get
            {
                var cardNumberDecrypt = cardNumber == null ? "" : ECOsystem.Utilities.TripleDesEncryption.Decrypt(ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat((long)cardNumber));
                return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(cardNumberDecrypt);
            }
        }

        /// <summary>
        /// PaymentInstrument
        /// </summary>
        [DisplayName("# Medio de Pago")]
        public string PaymentInstrument
        {
            get
            {
                if (PaymentInstrumentType == "Tarjeta" || PaymentInstrumentType == null)
                    return CreditCardNumberWithMask;
                else
                    return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(ECOsystem.Utilities.TripleDesEncryption.Decrypt(PaymentInstrumentCode));
            }
        }

        [DisplayName("Código de Conductor")]
        public string carTag { get; set; }

        [DisplayName("Fecha de Expiración")]
        public string expirationDate { get; set; }

        [DisplayName("Id de la Terminal")]
        public string terminalId { get; set; }

        [DisplayName("Monto")]
        public decimal? amount { get; set; }

        [DisplayName("Kilómetros")]
        public int? kilometers { get; set; }

        [DisplayName("Litros")]
        public decimal? units { get; set; }

        public string responseCode { get; set; }

        public string responseDescription { get; set; }

        public string systemTraceNumber { get; set; }

        public int FilterType { get; set; }

        /// <summary>
        /// ResponseStr
        /// </summary>
        [DisplayName("Código de Respuesta")]
        public string ResponseStr
        {
            get
            {
                return (responseCode + " - " + responseDescription);
            }
        }

    }
}

