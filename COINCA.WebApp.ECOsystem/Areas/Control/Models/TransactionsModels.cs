﻿/************************************************************************************************************
*  File    : TransactionsModels.cs
*  Summary : Transactions Models
*  Author  : Berman Romero
*  Date    : 09/15/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Globalization;
using ECOsystem.Models.Miscellaneous;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Transaction Model
    /// </summary>
    public class Transactions
    {
        /// <summary>
        /// PK of Transactions
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// Date of Transaction
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Odometer of Vehicle
        /// </summary>
		public int Odometer {get;set;}

        /// <summary>
        /// Liters of Transaction
        /// </summary>
        public decimal Liters { get; set; }

        /// <summary>
        /// Gallons
        /// </summary>
        public decimal Galons
        {
            get { return ECOsystem.Utilities.Miscellaneous.LitersToGallons(Liters); }
        }
    }

    /// <summary>
    /// Transaction Model
    /// </summary>
    public class ReverseTransactions
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies Credit Card Id
        /// </summary>
        public int? CreditCardId { get; set; }

        /// <summary>
        /// VehicleId as FK from Vehicles table
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// PlateId from Vehicles table
        /// </summary>
        public string PlateId { get; set; }

        /// <summary>
        /// FuelId as FK from Fuels table
        /// </summary>
        public int FuelId { get; set; }

        /// <summary>
        /// Odometer of Vehicle
        /// </summary>
        public int Odometer { get; set; }

        /// <summary>
        /// Transaction Fuel Amount
        /// </summary>
        public decimal? FuelAmount { get; set; }

        /// <summary>
        /// Liters of Transaction
        /// </summary>
        public decimal Liters { get; set; }

        /// <summary>
        /// Encrypted Credit Card Number
        /// </summary>
        public string EncryptedCreditCardNumber { get; set; }

    }


    /// <summary>
    /// Transactions Models
    /// </summary>
    public class TransactionsApi : ApiAncestor
    {
        /// <summary>
        /// Invoice Number
        /// </summary>
        public string Invoice { get; set; }

        /// <summary>
        /// Plate Id, Plate number for the vehicle
        /// </summary>
        public string PlateId { get; set; }

        /// <summary>
        /// Driver Code
        /// </summary>
        public string DriverCode { get; set; }

        /// <summary>
        /// Driver Code Encrypt
        /// </summary>
        public string DriverCodeEncrypt {
            get {
                return ECOsystem.Utilities.TripleDesEncryption.Encrypt(this.DriverCode);
            }
        }

        /// <summary>
        /// FuelId as FK from Fuels table
        /// </summary>
        public int? FuelId { get; set; }

        /// <summary>
        /// Transaction Date
        /// </summary>
        public DateTime Date
        {
            get
            {
                DateTime dateValue;
                DateTime.TryParseExact(TrxDate, "yyyyMMdd hh:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue);
                return dateValue.ToUniversalTime();
            }
        }

        /// <summary>
        /// Transaction Date, format expected "yyyyMMdd hh:mm:ss" Standard ISO
        /// </summary>
        public string TrxDate { get; set; }

        /// <summary>
        /// Vehicle's Odometer
        /// </summary>
        public long? Odometer { get; set; }

        /// <summary>
        /// Odometer format
        /// </summary>
        public string OdometerStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Odometer); }
        }

        /// <summary>
        /// Transaction Fuel Amount
        /// </summary>
        public decimal? FuelAmount { get; set; }

        /// <summary>
        /// Liters
        /// </summary>
        public decimal? Liters { get; set; }

        /// <summary>
        /// Gallons
        /// </summary>
        public decimal Galons
        {
            get { return ECOsystem.Utilities.Miscellaneous.LitersToGallons(Liters); }
        }

        /// <summary>
        /// Transaction POS
        /// </summary>
        public string TransactionPOS { get; set; }

        /// <summary>
        /// Scheme POS
        /// </summary>
        public string SchemePOS { get; set; }

        /// <summary>
        /// Merchant Description
        /// </summary>
        public string MerchantDescription { get; set; }

        /// <summary>
        /// Processor Id
        /// </summary>
        public string ProcessorId { get; set; }

        /// <summary>
        /// Processor Id
        /// </summary>
        public int FixedOdometer { get; set; }

        /// <summary>
        /// Processor Id
        /// </summary>
        public bool IsFloating { get; set; }

        /// <summary>
        /// Processor Id
        /// </summary>
        public bool IsReversed { get; set; }

        /// <summary>
        /// VOID flag
        /// </summary>
        public bool IsVoid { get; set; }

        /// <summary>
        /// Processor Id
        /// </summary>
        public bool IsDuplicated { get; set; }

        /// <summary>
        /// Encrypted Credit Card Number
        /// </summary>
        public string CreditCardNumber { get; set; }

    }

    /// <summary>
    /// Transactions Models
    /// </summary>
    public class RequestPartnerTransactionApi
    {
        /// <summary>
        /// Invoice Number
        /// </summary>
        public string invoice { get; set; }

        /// <summary>
        /// transaction Type
        /// </summary>
        public string transactionType { get; set; }

        /// <summary>
        /// terminalId
        /// </summary>
        public string terminalId { get; set; }

        /// <summary>
        /// systemTraceNumber
        /// </summary>
        public string systemTraceNumber { get; set; }

        /// <summary>
        /// cardNumber
        /// </summary>
        public long? cardNumber { get; set; }

        /// <summary>
        /// expirationDate
        /// </summary>
        public int? expirationDate { get; set; }

        /// <summary>
        /// amount
        /// </summary>
        public decimal? amount { get; set; }

        /// <summary>
        /// carTag
        /// </summary>
        public string carTag { get; set; }

        /// <summary>
        /// kilometers
        /// </summary>
        public decimal? kilometers { get; set; }

        /// <summary>
        /// units
        /// </summary>
        public decimal? units { get; set; }

        /// <summary>
        /// Reference Number
        /// </summary>
        public string referenceNumber { get; set; }

        /// <summary>
        /// Authorization Number
        /// </summary>
        public string authorizationNumber { get; set; }

        /// <summary>
        /// Merchant Description
        /// </summary>
        public string merchantDescription { get; set; }
    }

    /// <summary>
    /// Transactions Models
    /// </summary>
    public class ResponsePartnerTransactionApi
    {
        /// <summary>
        /// Response code
        /// </summary>
        public string responseCode { get; set; }

        /// <summary>
        /// cardNumber
        /// </summary>
        public long? cardNumber { get; set; }

        /// <summary>
        /// expirationDate
        /// </summary>
        public int? expirationDate { get; set; }
        
        /// <summary>
        /// additionalMessage
        /// </summary>
        public string additionalMessage { 
            get {
                if (this.advice != null && this.advice.Contains("F")) {
                    return "No requiere autorización por parte de Credomatic.";
                }
                return string.Empty;
            } 
            set {
                this.advice = string.Empty;
            } 
        }

        /// <summary>
        /// amount
        /// </summary>
        public decimal? amount { get; set; }

        /// <summary>
        ///Required Authorization T | F  = TRUE | FALSE
        /// </summary>
        public string advice { get; set; }
        
    }
    
}