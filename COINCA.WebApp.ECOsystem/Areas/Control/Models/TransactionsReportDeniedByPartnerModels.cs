﻿/************************************************************************************************************
*  File    : TransactionsReportModels.cs
*  Summary : Transaction Report Models
*  Author  : Danilo Hidalgo
*  Date    : 12/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Collections.Generic;
using System.ComponentModel;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using Newtonsoft.Json;
using ECOsystem.Models.Account;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Transactions Report Base
    /// </summary>
    public class TransactionsReportDeniedByPartnerBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public TransactionsReportDeniedByPartnerBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<TransactionsReportDeniedByPartner>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<TransactionsReportDeniedByPartner> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Transactions Report Model
    /// </summary>
    public class TransactionsReportDeniedByPartner
    {
        private string _customerName;

        /// <summary>
        /// CustomerName
        /// </summary>
        [DisplayName("Customer Name")]
        [ExcelMappedColumn("Nombre de Cliente")]
        [NotMappedColumn]
        public string CustomerName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(_customerName);
            }
            set
            {
                this._customerName = value;
            }
        }



        private string _customerBranchName;
        /// <summary>
        /// CustomerName
        /// </summary>
        [DisplayName("CustomerBranchName")]
        [ExcelMappedColumn("Empresa Matriz")]
        [NotMappedColumn]
        public string CustomerBranchName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(_customerBranchName);
            }
            set
            {
                this._customerBranchName = value;
            }
        }

        public int IsCustomerBranch { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Transacción Id")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int TransactionId { get; set; }

        /// <summary>
        /// System Trace Code
        /// </summary>
        [DisplayName("Código Transacción")]
        public string SystemTraceCode { get; set; }

        /// <summary>
        /// Invoice - # Factura
        /// </summary>
        [DisplayName("#Factura")]
        public string Invoice { get; set; }

        /// <summary>
        /// MerchantDescription - Gasolinera
        /// </summary>
        [DisplayName("Gasolinera")]
        public string MerchantDescription { get; set; }

        /// <summary>
        /// Transaction State
        /// </summary>
        [DisplayName("Estado Transacción")]
        public string State { get; set; }

        /// <summary>
        /// HolderName
        /// </summary>
        [DisplayName("Titular")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Titular", Width = "120px", SortEnabled = true)]
        public string HolderName { get; set; }

        /// <summary>
        /// HolderNameStr
        /// </summary>
        [DisplayName("Titular")]
        [GridColumn(Title = "Titular", Width = "120px", SortEnabled = true)]
        public string HolderNameStr { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(HolderName); } }


        /// <summary>
        /// Date
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha")]
        [GridColumn(Title = "Fecha", Width = "120px", SortEnabled = true)]
        public string DateStr { get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(Date); } }

        /// <summary>
        /// FuelName
        /// </summary>
        [DisplayName("Combustible")]
        [GridColumn(Title = "Combustible", Width = "120px", SortEnabled = true)]
        public string FuelName { get; set; }

        /// <summary>
        /// Liters
        /// </summary>
        [DisplayName("Litros")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Litros", Width = "120px", SortEnabled = true)]
        public decimal Liters { get; set; }

        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal CapacityUnitValue
        {
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Unidad de Capacidad", Width = "120px", SortEnabled = true)]
        public string CapacityUnitValueStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(CapacityUnitValue); }
        }

        /// <summary>
        /// FuelAmount
        /// </summary>
        [DisplayName("Monto")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Monto", Width = "120px", SortEnabled = true)]
        public decimal FuelAmount { get; set; }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Moneda")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string CurrencySymbol { get; set; }


        /// <summary>
        /// Fuel Amount as String
        /// </summary>
        [DisplayName("Monto")]
        [NotMappedColumn]
        public string FuelAmountStr
        {
            //get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(FuelAmount); }
            //get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AssignedAmount, CurrencySymbol); }
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(FuelAmount, CurrencySymbol); }
        }

        /// <summary>
        /// Odometer
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double Odometer { get; set; }

        /// <summary>
        /// Odometer format
        /// </summary>
        [DisplayName("Odómetro")]
        [GridColumn(Title = "Odómetro", Width = "120px", SortEnabled = true)]
        public string OdometerStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Convert.ToDecimal(Odometer)); } }


        /// <summary>
        /// Message
        /// </summary>
        [DisplayName("Mensaje")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string Message { get; set; }

        /// <summary>
        /// CreditCardHolder
        /// </summary>
        /// 
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardHolder { get; set; }

        /// <summary>
        /// CreditCard Format Number
        /// </summary>
        /// 
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// Card Number from Transaction Data
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public long CreditCardNumberTransaction { 
            get {
                return ((this.TransportDataObject != null && this.TransportDataObject.cardNumber != null) ? (long)this.TransportDataObject.cardNumber : 0);
            } 
        }

        /// <summary>
        /// Card Number from Transaction Data With Format
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardNumberTransactionWithFormat
        {
            get
            {
                return ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat(this.CreditCardNumberTransaction);
            }
        }

        /// <summary>
        /// Card Number from Transaction Data With MASK
        /// </summary>
        [ExcelMappedColumn("#Tarjeta")]
        [NotMappedColumn]
        public string CreditCardNumberTransactionWithMask
        {
            get
            {
                return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(this.CreditCardNumberTransactionWithFormat);
            }
        }

        /// <summary>
        /// Card Number from Transaction Data Encrypted
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardNumberTransactionEncrypted
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Encrypt(this.CreditCardNumberTransactionWithFormat);
            }
        }

        /// <summary>
        /// PaymentInstrumentType
        /// </summary>
        [DisplayName("Medio de Pago")]
        public string PaymentInstrumentType { get; set; }

        /// <summary>
        /// PaymentInstrumentCode
        /// </summary>
        public string PaymentInstrumentCode { get; set; }

        /// <summary>
        /// PaymentInstrument
        /// </summary>
        [DisplayName("# Medio de Pago")]
        public string PaymentInstrument
        {
            get
            {
                if (PaymentInstrumentType == "Tarjeta" || PaymentInstrumentType == null)
                    return CreditCardNumberTransactionWithMask;
                else
                    return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(ECOsystem.Utilities.TripleDesEncryption.Decrypt(PaymentInstrumentCode));
            }
        }

        /// <summary>
        /// Transport Data
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string TransportData { get; set; }

        /// <summary>
        ///  Transport Data Obj
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public TransportDataModel TransportDataObject
        {
            get
            {
                return JsonConvert.DeserializeObject<TransportDataModel>(this.TransportData);
            }
        }

        /// <summary>
        /// InsertDate
        /// </summary>
        [DisplayName("Fecha Creada")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public DateTime? InsertDate { get; set; }

        /// <summary>
        /// ResponseCodeDescription
        /// </summary>
        [DisplayName("Codigo de Respuesta")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string ResponseCode { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("descripcion Respuesta")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string ResponseCodeDescription { get; set; }

        /// <summary>
        /// CustomerId
        /// </summary>
        [DisplayName("Customer ID")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// CostCenterName
        /// </summary>
        [DisplayName("CostCenter Name")]
        [ExcelMappedColumn("Centro de Costo")]
        [NotMappedColumn]
        public string CostCenterName { get; set; }

        public int? CostCenterId { get; set; }

        public string VehicleGroupName { get; set; }

        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Terminal Id
        /// </summary>
        [ExcelMappedColumn("Terminal ID")]
        [NotMappedColumn]
        public string TerminalId { get; set; }

        /// <summary>
        /// Service Station Name
        /// </summary>
        [DisplayName("Estación de servicio")]
        public string ServiceStationName { get; set; }

        /// <summary>
        /// DateToExport
        /// </summary>
        [DisplayName("Fecha Emision")]
        [ExcelMappedColumn("Fecha Emision")]
        [NotMappedColumn]
        public string DateToExport
        {
            get
            {
                return ((InsertDate != null) ? Convert.ToDateTime(InsertDate).ToString("dd/MM/yyyy hh:mm:ss tt") : string.Empty);
            }
        }

        /// <summary>
        /// Country Name
        /// </summary>
        [DisplayName("País")]
        [ExcelMappedColumn("País")]
        [GridColumn(Title = "País", Width = "120px", SortEnabled = true)]
        public string CountryName { get; set; }

        //// <summary>
        /// Total of Trx Traveled Odometer
        /// </summary>
        [ExcelNoMappedColumn]
        public string ExchangeValue { get; set; }
        
        /// <summary>
        /// Real Amount of Trx 
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? RealAmount { get; set; }

        /// <summary>
        /// Real Amount String
        /// </summary>
        [DisplayName("Monto Real")]
        [ExcelMappedColumn("Monto Real")]
        [GridColumn(Title = "Monto Real", Width = "120px", SortEnabled = true)]
        public string RealAmountFormatted
        {
            get { return RealAmount == null ? "N/A" : ((decimal)RealAmount).ToString("N", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); }
        }
    }
}

