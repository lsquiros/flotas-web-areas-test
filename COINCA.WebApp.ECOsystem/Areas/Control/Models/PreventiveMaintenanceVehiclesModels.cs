﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceVehiclesModels.cs
*  Summary : Preventive Maintenance Vehicles Models
*  Author  : Danilo Hidalgo
*  Date    : 01/07/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Operators Base Class for Views
    /// </summary>
    public class PreventiveMaintenanceVehiclesBase
    {        
        /// <summary>
        /// PreventiveMaintenanceVehiclesBase Constructor 
        /// </summary>
        public PreventiveMaintenanceVehiclesBase()
        {
            Data = new PreventiveMaintenanceVehicles();
            List = new List<PreventiveMaintenanceVehicles>();
        }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public PreventiveMaintenanceVehicles Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceVehicles> List { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Preventive Maintenance Catalog model
        /// </summary>
        [DisplayName("Tipo Mantenimiento Preventivo")]
        public int? PreventiveMaintenanceCatalogId { get; set; }

    }
    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance By Vehicle's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceVehicles : ModelAncestor{
        
        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public int VehicleId { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceCatalogId { get; set; }

        /// <summary>
        /// Plate of Vehicle
        /// </summary>
        [GridColumn(Title = "Matrícula", Width = "150px")]
        public string PlateId { get; set; }

        /// <summary>
        /// Name of Vehicle
        /// </summary>
        [GridColumn(Title = "Vehículo", Width = "150px")]
        public string VehicleName { get; set; }

        /// <summary>
        /// CostCenterName of Vehicle for Preventive Maintenance
        /// </summary>
        [GridColumn(Title = "Centro de Costos", Width = "250px")]
        public string CostCenterName { get; set; }

        /// <summary>
        /// TypeName of Vehicle for Preventive Maintenance
        /// </summary>
        [GridColumn(Title = "Clase", Width = "200px")]
        public string TypeName { get; set; }

    }
}
