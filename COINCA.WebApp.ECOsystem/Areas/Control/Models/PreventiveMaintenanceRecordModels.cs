﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceRecordModels.cs
*  Summary : PreventiveMaintenanceRecord Models
*  Author  : Danilo Hidalgo
*  Date    : 01/08/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;

namespace  ECOsystem.Areas.Control.Models.Control
{

    /// <summary>
    /// PreventiveMaintenanceCost Base
    /// </summary>
    public class PreventiveMaintenanceRecordBase
    {
        /// <summary>
        /// PreventiveMaintenanceRecord Constructor
        /// </summary>
        public PreventiveMaintenanceRecordBase()
        {
            Data = new PreventiveMaintenanceRecord();
            List = new List<PreventiveMaintenanceRecord>();
        }
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public PreventiveMaintenanceRecord Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceRecord> List { get; set; }
    }

    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceRecord : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PreventiveMaintenanceRecordByVehicleId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PreventiveMaintenanceRecordByVehicleDetailId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PreventiveMaintenanceId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PreventiveMaintenanceCostId { get; set; }

        
        /// <summary>
        /// Description of Cost Preventive Maintenance
        /// </summary>
        //[DisplayName("Nombre")]
        //[Required(ErrorMessage = "{0} es requerido")]
        //[GridColumn(Title = "Nombre", Width = "250px", SortEnabled = true)]
        [NotMappedColumn]
        public string Description { get; set; }

        /// <summary>
        /// Name of Frequency Type
        /// </summary>
        //[DisplayName("Costo")]
        //[GridColumn(Title = "Costo", Width = "250px", SortEnabled = true)]
        [NotMappedColumn]
        public decimal Cost { get; set; }

        /// <summary>
        /// Name of Frequency Type
        /// </summary>
        //[DisplayName("Costo")]
        //[GridColumn(Title = "Costo", Width = "250px", SortEnabled = true)]
        [NotMappedColumn]
        public decimal Record { get; set; }

        /// <summary>
        /// Symbol of currency
        /// </summary>
        [NotMappedColumn]
        public string Symbol { get; set; }

    }

}
