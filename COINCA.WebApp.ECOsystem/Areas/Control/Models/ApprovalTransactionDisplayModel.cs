﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

/*
 * Author:      Melvin Salas
 * Date:        March 14, 2016
 * Description: This model contains data of approval transaction reporting
 * */

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Approval Transaction Report Model
    /// </summary>
    public class ApprovalTransactionDisplayModel
    {
        public int TransactionId { get; set; }
        [Display(Name = "Fecha y Hora")]
        public DateTime Date { get; set; }

        [Display(Name = "Nombre Vehículo")]
        public String Vehicle { get; set; }

        [Display(Name = "Placa")]
        public String PlateId { get; set; }
      
        public String DriverName { get; set; }

        [Display(Name = "Nombre Conductor")]
        public String DecryptDriverName
        {
            get {return ECOsystem.Utilities.TripleDesEncryption.Decrypt(DriverName); }
        }

        [Display(Name = "Tarjeta")]
        public String CreditCard {
            get { return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(_CreditCard); }
            set { _CreditCard = ECOsystem.Utilities.TripleDesEncryption.Decrypt(value); } 
        }


        [Display(Name = "Centro de Costo")]
        public String CostCenter { get; set; }
        public String CostCenterOld { get; set; }

        public int CostCenterId { get; set; }
        public int CostCenterIdOld { get; set; }

        public int TotalRows { get; set; }

        [Display(Name = "Cédula")]
        public String DriverId {
            get { return _DriverId; }
            set { _DriverId = ECOsystem.Utilities.TripleDesEncryption.Decrypt(value); } 
        }

        [Display(Name = "Fabricante")]
        public String Manufacturer { get; set; }

        [Display(Name = "Cantidad")]
        public Decimal Fuel { get; set; }

        /// <summary>
        /// Fuel Value as String with 3 dec
        /// </summary>
        [Display(Name = "Cantidad")]
        public string FuelStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Fuel, 3); }
        }

        public String LegalId { get; set; }

        [Display(Name = "Monto")]
        public Decimal Amount { get; set; }

        public int Count { get; set; }

        [Display(Name = "Combustible")]
        public String Type { get; set; }

        public int? ServiceStationId { get; set; }

        [Display(Name = "Estación de Servicio")]
        public String ServiceStation { get; set; }

        public String BacId { get; set; }

        [Display(Name = "# Terminal")]
        public String AffiliateNumber { get; set; }

        [Display(Name = "Factura")]
        public String InvoiceId { get; set; }
        public String InvoiceOldId { get; set; }

        public Guid Status { get; set; }
        public Guid StatusOld { get; set; }
        
        private String _CreditCard;
        private String _DriverId;

        public bool IsChanged { get; set; }
        //{
        //    get
        //    {
        //        if(ServiceStationId != null)
        //        {
        //            return true;
        //        }
                
        //        if (CostCenter == null)
        //        {
        //            return (InvoiceId != InvoiceOldId || Status != StatusOld);
        //        }
                
        //        else
        //        {
        //            return (InvoiceId != InvoiceOldId || CostCenter != CostCenterIdOld.ToString() || Status != StatusOld);
        //        }
        //    }
        //}

        /// <summary>
        /// Country Name
        /// </summary>
        [Display(Name = "País")]
        public string CountryName { get; set; }

        /// <summary>
        /// Total of Trx Traveled Odometer
        /// </summary>
        [Display(Name = "Tipo Cambio")]
        public string ExchangeValue { get; set; }               

        /// <summary>
        /// Total of Trx Traveled Odometer
        /// </summary>
        public decimal? RealAmount { get; set; }

        /// <summary>
        /// Country Name
        /// </summary>
        [Display(Name ="Monto Real")]
        public string RealAmountFormatted
        {
            get { return RealAmount == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Convert.ToDecimal(RealAmount), 2); }
        }
    }
}
