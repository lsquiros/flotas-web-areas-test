﻿using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models
{
    public class VehicleOdometerSAPReportBase
    {
        public VehicleOdometerSAPReportBase()
        {
            Data = new VehicleOdometerSAPReport();
            List = new List<VehicleOdometerSAPReport>();
            Menus = new List<AccountMenus>();
        }
        public VehicleOdometerSAPReport Data { get; set; }
        public List<VehicleOdometerSAPReport> List { get; set; }
        public List<AccountMenus> Menus { get; set; }
    }

    public class VehicleOdometerSAPReport
    {
        public int VehicleId { get; set; }
        public int? ExternalID { get; set; }
        public int Odometer { get; set; }
    }
}