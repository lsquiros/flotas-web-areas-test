﻿/************************************************************************************************************
*  File    : EfficiencyFuelsReportModels.cs
*  Summary : Efficiency Fuels Report Models
*  Author  : Berman Romero
*  Date    : 21/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Real Vs Budget Fuels Report Base
    /// </summary>
    public class EfficiencyFuelsReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public EfficiencyFuelsReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<dynamic>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        public IEnumerable<AccountMenus> Menus  { get; set; }

        /// <summary>
        /// Month Count
        /// </summary>
        public int MonthCount { get; set; }

        /// <summary>
        /// Vehicle Count
        /// </summary>
        public int VehicleCount { get; set; }

    }


    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class EfficiencyFuelsReport
    {
        /// <summary>
        /// Year
        /// </summary>
        [DisplayName("Año")]
        [NotMappedColumn]
        public int Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        [DisplayName("# Mes")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int Month { get; set; }

        /// <summary>
        /// Month Name
        /// </summary>
        [DisplayName("Mes")]
        [NotMappedColumn]
        public string MonthName
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetMonthName(Month); }
        }

        /// <summary>
        /// Liters
        /// </summary>
        [DisplayName("Rendimiento")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal Performance { get; set; }

        /// <summary>
        /// Liters as String
        /// </summary>
        [DisplayName("Rendimiento")]
        [NotMappedColumn]
        public string PerformanceStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Performance); }
        }

        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal CapacityUnitPerformance
        {
            //Los parámetros se pasan invertidos porque lo que se convierte es el redimiento (con 1 galón se recorren 3.78541178 veces los km que se recorren con 1 litro) 
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, Performance); } 
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [DisplayName("Rendimiento por Unidad de Capacidad")]
        [NotMappedColumn]
        public string CapacityUnitPerformanceStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(CapacityUnitPerformance); }
        }
    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class EfficiencyFuelsByVehicleSubUnitReport : EfficiencyFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Centro de Costo")]
        [NotMappedColumn]
        public string CostCenterName { get; set; }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class EfficiencyFuelsByVehicleGroupReport : EfficiencyFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Grupo de Vehículos")]
        [NotMappedColumn]
        public string VehicleGroupName { get; set; }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class EfficiencyFuelsByVehicle : EfficiencyFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Matrícula")]
        [NotMappedColumn]
        public string PlateNumber { get; set; }

    }
}