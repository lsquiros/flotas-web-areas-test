﻿/************************************************************************************************************
*  File    : ConsolidateLiquidationReportModels.cs
*  Summary : ConsolidateLiquidationReport Models
*  Author  : Andrés Oviedo
*  Date    : 21/01/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;
using GridMvc.DataAnnotations;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// ConsolidateLiquidationReport Base
    /// </summary>
    public class ConsolidateLiquidationReportBase
    {
        public ConsolidateLiquidationReportBase()
        {
            Data = new ConsolidateLiquidationReports();
            List = new List<ConsolidateLiquidationReports>();
            Menus = new List<AccountMenus>();

            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;

            ReportCriteriaId = (int?)ReportCriteria.Period;
            ReportFuelTypeId = (int?)ReportFuelTypes.Vehicles;
            ReportComparativeId = (int?)ReportComparativeTypes.Vehicles;
            key = null;
        }

        /// <summary>
        /// List of Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Searched text
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }
        
        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }

        /// <summary>
        /// Report Fuel Type Id
        /// </summary>
        public int? ReportFuelTypeId { get; set; }


        /// <summary>
        /// Report compartive Id
        /// </summary>
        public int? ReportComparativeId { get; set; }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public ConsolidateLiquidationReports Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<ConsolidateLiquidationReports> List { get; set; }
        
        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64 { get; set; }

    }

    /// <summary>
    /// ConsolidateLiquidationReport Model
    /// </summary>
    public class ConsolidateLiquidationReports : ModelAncestor
    {
        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Identifier of the vehicle
        /// </summary>
        [ExcelMappedColumn("Número")]
        public int? VehicleId { get; set; }

        /// <summary>
        /// LicensePlate
        /// </summary>
        [ExcelMappedColumn("Placa")]
        public string PlateId { get; set; }


        /// <summary>
        /// VehicleName
        /// </summary>
        [ExcelMappedColumn("Nombre Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }


        /// <summary>
        /// Identification of the driver
        /// </summary>
        [ExcelNoMappedColumn]
        public string Identification { get; set; }

        /// <summary>
        /// Decrypted Identification of the driver
        /// </summary>
        [ExcelMappedColumn("Identificación")]
        public string IdentificationStr { get{ return ECOsystem.Utilities.TripleDesEncryption.Decrypt(Identification);}}

        /// <summary>
        /// Monthly Liters by vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal Liters { get; set; }

        /// <summary>
        /// Monthly Liters by vehicle
        /// </summary>
        [ExcelMappedColumn("Litros(Mes)")]
        public string LitersStr { get{return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Liters, 3);}  }
        
        /// <summary>
        /// Maximun Km of the Odometer by vehicle
        /// </summary>
        [ExcelMappedColumn("KM Odómetro")]
        public decimal TotalOdometer { get; set; }
        
        /// <summary>
        /// Capacity in liters of the tank of the vehicle
        /// </summary>
        [ExcelMappedColumn("Capacidad Tanque")]
        public string TotalOdometerStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TotalOdometer); } }

        /// <summary>
        /// Capacity in liters of the tank of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public int? TankCapacity { get; set; }

        /// <summary>
        /// Capacity in liters of the tank of the vehicle
        /// </summary>
        [ExcelMappedColumn("Capacidad Tanque")]
        public string TankCapacityStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TankCapacity); } }

        /// <summary>
        /// Identifier of the fuel type
        /// </summary>
        [ExcelNoMappedColumn]
        public int? FuelId { get; set; }

        /// <summary>
        /// Name of the fuel type
        /// </summary>
        [ExcelMappedColumn("Combustible")]
        public string FuelName { get; set; }

        /// <summary>
        /// Identifier of the unid
        /// </summary>
        [ExcelNoMappedColumn]
        public int? UnitId { get; set; }

        /// <summary>
        /// Name of the unid
        /// </summary>
        [ExcelMappedColumn("Unidad")]
        public string UnitName { get; set; }

        /// <summary>
        /// Identifier of the cost center
        /// </summary>
        [ExcelNoMappedColumn]
        public int? CostCenterId { get; set; }

        /// <summary>
        /// Name of the cost center
        /// </summary>
        [ExcelMappedColumn("Centro de Costo")]
        public string CostCenterName { get; set; }

        /// <summary>
        /// Identifier of the vehicle group
        /// </summary>
        [ExcelNoMappedColumn]
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Name of the vehicle group
        /// </summary>
        [ExcelMappedColumn("Grupo de vehículos")]
        public string VehicleGroupName { get; set; }

        /// <summary>
        /// Name of the brand of the vehicle
        /// </summary>
        [ExcelMappedColumn("Marca")]
        public string Manufacturer { get; set; }
    
        /// <summary>
        /// Model´s Name of the vehicle
        /// </summary>
        [ExcelMappedColumn("Modelo")]
        public string VehicleModel { get; set; }
		
		/// <summary>
        /// Year of the vehicle
        /// </summary>
        [ExcelMappedColumn("Año")]
        public int Year { get; set; }

        /// <summary>
        /// Code of the driver
        /// </summary>
          [ExcelNoMappedColumn]
        public string Code { get; set; }

        /// <summary>
        /// Code of the driver
        /// </summary>
        [ExcelMappedColumn("Código de Conductor")]
        public string CodeStr { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(Code); } }

        /// <summary>
        /// PaymentInstrumentType
        /// </summary>
        [ExcelMappedColumn("# Medio de Pago")]
        public string PaymentInstrumentType { get; set; }

        /// <summary>
        /// CreditCard Number of the driver or vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// PaymentInstrumentCode
        /// </summary>
        [ExcelNoMappedColumn]
        public string PaymentInstrumentCode { get; set; }

        /// <summary>
        /// Decrypted Identification of the driver
        /// </summary>
        [ExcelMappedColumn("Tarjeta")]
        public string CreditCardNumberStr { get { return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask( ECOsystem.Utilities.TripleDesEncryption.Decrypt(CreditCardNumber)); } }

        /// <summary>
        /// PaymentInstrument
        /// </summary>
        public string PaymentInstrument
        {
            get
            {
                if (PaymentInstrumentType == "Tarjeta" || PaymentInstrumentType == null)
                    return CreditCardNumberStr;
                else
                    return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(ECOsystem.Utilities.TripleDesEncryption.Decrypt(PaymentInstrumentCode));
            }
        }

        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        public decimal? CapacityUnitValue
        {
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        public string CapacityUnitValueStr
        {
            get
            {
                return CapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(CapacityUnitValue, 3);
            }

        }

    }

}