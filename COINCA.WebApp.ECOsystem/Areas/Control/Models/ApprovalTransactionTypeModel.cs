﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models.Control
{
    public class ApprovalTransactionTypeModel
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
        public Boolean Default { get; set; }
    }
}