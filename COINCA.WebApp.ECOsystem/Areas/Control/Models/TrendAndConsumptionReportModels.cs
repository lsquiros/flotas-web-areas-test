﻿/************************************************************************************************************
*  File    : TrendAndConsumptionReportModels.cs
*  Summary : Trend And Consumption Report Models
*  Author  : Berman Romero
*  Date    : 03/16/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Trend And Consumption Report Base
    /// </summary>
    public class TrendAndConsumptionReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public TrendAndConsumptionReportBase()
        {
            Parameters = new TrendAndConsumptionParameters();
            List = new List<TrendAndConsumptionReport>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>ss
        /// Report Parameters
        /// </summary>
        public TrendAndConsumptionParameters Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<TrendAndConsumptionReport> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// Control Fuels Reports Base Model
    /// </summary>
    public class TrendAndConsumptionParameters
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public TrendAndConsumptionParameters()
        {
            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;
            ReportCriteriaId = (int?)ReportCriteria.Period;
            Key = null;
        }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date for Report when dates range is selected
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(EndDate); }
            set { EndDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }


        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64 { get; set; }

        /// <summary>
        /// Key Filter
        /// </summary>
        public string Key { get; set; }

    }


    /// <summary>
    /// Trend And Consumption Report
    /// </summary>
    public class TrendAndConsumptionReport
    {
        /// <summary>
        /// Identifier of the vehicle
        /// </summary>
        [ExcelMappedColumn("Vehículo")]
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// LicensePlate
        /// </summary>
        [ExcelMappedColumn("Placa")]
        [NotMappedColumn]
        public string PlateId { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        [ExcelMappedColumn("Nombre Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }


        /// <summary>
        /// Max Fuel Consumption of the vehicle in all the times
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public decimal MaxConsumption { get; set; }

        /// <summary>
        /// Med Fuel Consumption of the vehicle in all the times
        /// </summary>
        [ExcelMappedColumn("Max")]
        [NotMappedColumn]
        public string MaxConsumptionStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(MaxConsumption); } }

        /// <summary>
        /// Med Fuel Consumption of the vehicle in all the times
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public decimal AvgConsumption { get; set; }

        /// <summary>
        /// Med Fuel Consumption of the vehicle in all the times
        /// </summary>
        [ExcelMappedColumn("Med")]
        [NotMappedColumn]
        public string AvgConsumptionStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(AvgConsumption); } }

        /// <summary>
        /// Min Fuel Consumption  of the vehicle in all the times
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public decimal MinConsumption { get; set; }

        /// <summary>
        /// Med Fuel Consumption of the vehicle in all the times
        /// </summary>
        [ExcelMappedColumn("Consumo Mínimo")]
        [NotMappedColumn]
        public string MinConsumptionStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(MinConsumption); } }


        /// <summary>
        /// Fuel Consumption of the vehicle in the month
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public decimal TotalConsumption { get; set; }

        /// <summary>
        /// Fuel Consumption of the vehicle in the month
        /// </summary>
        [ExcelMappedColumn("Total de Consumo")]
        [NotMappedColumn]
        public string TotalConsumptionStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TotalConsumption); } }

        /// <summary>
        /// Fuel Amount 
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public decimal TotalFuelAmount { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Fuel Consumption of the vehicle in the month
        /// </summary>
        [ExcelMappedColumn("Monto")]
        [NotMappedColumn]
        public string TotalFuelAmountStr { get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(TotalFuelAmount, CurrencySymbol); } }

        
    }
}