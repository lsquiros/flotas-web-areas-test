﻿/************************************************************************************************************
*  File    : AlarmByOdometerModels.cs
*  Summary : Alarm By Odometer Models
*  Author  : Cristian Martínez
*  Date    : 22/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Alarm By Odometer Base for Views
    /// </summary>
    public class AlarmByOdometerBase
    {
        /// <summary>
        /// Alarm By Odometer Base Constructor
        /// </summary>
        public AlarmByOdometerBase() 
        {
            Data = new AlarmByOdometer();
            List = new List<AlarmByOdometer>();
        }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public AlarmByOdometer Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<AlarmByOdometer> List { get; set; }

    }

    /// <summary>
    /// Operators Model has all properties of Alarm By Odometer table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class AlarmByOdometer : ModelAncestor
    {
        public AlarmByOdometer()
        {
            CodView = "AlarOdom";
        }

        /// <summary>
        /// For get Alarm of Vehicle
        /// </summary>
        [NotMappedColumn]
        public string CodView { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? AlarmOdometerId { get; set; }

        /// <summary>
        /// property 
        /// </summary>
        [NotMappedColumn]
        public int VehicleId { get; set; }

        /// <summary>
        /// PlateId of Vehicle
        /// </summary>
        [DisplayName("Placa")]
        [GridColumn(Title = "Placa", Width = "400px", SortEnabled = true)]
        public string PlateId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Fecha de Registro")]
        [GridColumn(Title = "Fecha de Registro", Width = "400px", SortEnabled = true)]
        public DateTime Date { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Odómetros Esperados (Kms)")]
        [GridColumn(Title = "Odómetros Esperados", Width = "400px", SortEnabled = true, Format = "{0:n0}")]
        public int ExpectedOdometer { get; set; }

        /// <summary>
        /// property 
        /// </summary>
        [DisplayName("Odómetrso Reportados ")]
        [GridColumn(Title = "Odómetros Reportados (Kms)", Width = "400px", SortEnabled = true, Format = "{0:n0}")]
        public int ReportedOdometer { get; set; }

        /// <summary>
        /// new value of the odometer
        /// </summary>
        [NotMappedColumn]
        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Valor Actual del Odómetro")]
        public int? NewOdometer { get; set; }
    }

    /// <summary>
    /// Alarms Models
    /// </summary>
    public class AlarmsModels : ModelAncestor
    {
        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<AlarmsModels> List { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model 
        /// </summary>
        [NotMappedColumn]
        public int? AlarmId { get; set; }

        /// <summary>
        /// Fk User Id From User
        /// </summary>
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// Phones for send of the alarm
        /// </summary>
        [DisplayName("SMS (Teléfonos):")]
        //[Required(ErrorMessage="{0} es requerido.")]
        [NotMappedColumn]
        public string Phone { get; set; }

        /// <summary>
        /// Email for send of the alarm
        /// </summary>
        [DisplayName("Correos:")]
        [Required(ErrorMessage = "{0} es requerido.")]
        [NotMappedColumn]
        public string Email { get; set; }

        /// <summary>
        /// FK AlarmTriggerId from Types
        /// </summary>
        [NotMappedColumn]
        public int AlarmTriggerId { get; set; }

        /// <summary>
        /// FK Periodicity Type from Types
        /// </summary>
        [DisplayName("Periodicidad:")]
        [NotMappedColumn]
        public int? PeriodicityTypeId { get; set; }

        /// <summary>
        /// FK Entity Type Id From Types
        /// </summary>
        [NotMappedColumn]
        public int EntityTypeId { get; set; }

        /// <summary>
        /// FK EntityId from Drives, Vehicles, Group, SUbUnit, Unit
        /// </summary>
        [NotMappedColumn]
        public int EntityId { get; set; }

        /// <summary>
        /// Active Alarm
        /// </summary>
        [DisplayName("Habilitada")]
        [NotMappedColumn]
        public bool Active { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string TitlePopUp { get; set; }

        /// <summary>
        /// Property for indicate that have periodicity
        /// </summary>
        [NotMappedColumn]
        public bool HasPeriodicity { get; set; }

        /// <summary>
        /// Type of the alarm
        /// </summary>
        [DisplayName("Tipo de Alarma:")]
        [NotMappedColumn]
        public string AlarmType { get; set; }

        /// <summary>
        /// Name of the entity of the alarm
        /// </summary>
        [NotMappedColumn]
        public string Entity { get; set; }

        /// <summary>
        /// Type of tht entity
        /// </summary>
        [DisplayName("Tipo de Entidad:")]
        [NotMappedColumn]
        public string EntityType { get; set; }

        /// <summary>
        /// Periodiciy of the alarm
        /// </summary>
        [DisplayName("Periodicidad:")]
        [NotMappedColumn]
        public string Periodicity { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public System.DateTime? NextAlarm { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Siguiente Alarma:")]
        [NotMappedColumn]
        public string NextAlarmStr { get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(NextAlarm); } }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool IsEncrypted { get; set; }   

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Entidad:")]
        [NotMappedColumn]
        public string EntityStr
        {
            get
            {
                if (IsEncrypted)
                {
                    return ECOsystem.Utilities.TripleDesEncryption.Decrypt(Entity);
                }
                else
                {
                    return Entity;
                }
            }
        }
    }
}