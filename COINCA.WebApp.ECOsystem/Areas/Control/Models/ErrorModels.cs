﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models.Control
{
    public class ErrorModels
    {
        public string Title { get; set; }
        public string TechnicalError { get; set; }
    }
}