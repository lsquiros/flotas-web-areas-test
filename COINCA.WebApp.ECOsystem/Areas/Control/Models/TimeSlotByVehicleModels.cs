﻿/************************************************************************************************************
*  File    : TimeSlotByVehicleModels.cs
*  Summary : TimeSlotByVehicle Models
*  Author  : Danilo Hidalgo
*  Date    : 12/04/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using System.Collections.Generic;

namespace  ECOsystem.Areas.Control.Models.Control
{
    public class TimeSlotByVehicleBase
    {
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public TimeSlotByVehicle Data { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Vehicle model
        /// </summary>
        public int? VehicleId { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// TimeSlotByVehicle Model
    /// </summary>
    public class TimeSlotByVehicle : ModelAncestor
    {
        public TimeSlotByVehicle()
        {
            CodView = "TimSloVeh";
        }

        /// <summary>
        /// For get Alarm of Vehicle
        /// </summary>
        public string CodView { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Vehicles model
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Json TimeSlot to load canvas grid
        /// </summary>
        public string JsonTimeSlot { get; set; }

        /// <summary>
        /// Json TimeSlot to get the TimeSlot from DB
        /// </summary>
        public string XmlTimeSlot { get; set; }

    }

    /// <summary>
    /// TimeSlotByVehicle Model
    /// </summary>
    public class ValidateTimeSlotByVehicle : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Vehicles model
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Json TimeSlot to load canvas grid
        /// </summary>
        public int? Day { get; set; }

        /// <summary>
        /// Json TimeSlot to get the TimeSlot from DB
        /// </summary>
        public string Time { get; set; }

    }

    /// <summary>
    /// Vehicle TimeSlot for Json parse
    /// </summary>
    public class TimeSlotByVehicleJson
    {
        /// <summary>
        /// id with TimeSlot selected
        /// </summary>
        public string id { get; set; }

    }
}
