﻿/************************************************************************************************************
*  File    : ComparativeEfficiencyFuelsReportModels.cs
*  Summary : Efficiency Fuels Report Models
*  Author  : Berman Romero
*  Date    : 21/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Real Vs Budget Fuels Report Base
    /// </summary>
    public class ComparativeEfficiencyFuelsReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public ComparativeEfficiencyFuelsReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<dynamic>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// Month Count
        /// </summary>
        public int MonthCount { get; set; }

        /// <summary>
        /// Vehicle Count
        /// </summary>
        public int VehicleCount { get; set; }

        public ScoreGlobalReportBase ScoreGlobalReportBase { get; set; }
    }


    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class ComparativeEfficiencyFuelsReport
    {
        /// <summary>
        /// Year
        /// </summary>
        [DisplayName("Año")]
        [NotMappedColumn]
        [ExcelMappedColumn("Año")]
        public int Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        [DisplayName("# Mes")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int Month { get; set; }

        /// <summary>
        /// Month Name
        /// </summary>
        [DisplayName("Mes")]
        [NotMappedColumn]
        [ExcelMappedColumn("Mes")]
        public string MonthName
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetMonthName(Month); }
        }

        /// <summary>
        /// Rendimiento
        /// </summary>
        [DisplayName("Rendimiento")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal AVGPerformance { get; set; }


        /// <summary>
        /// Rendimiento
        /// </summary>
        [DisplayName("Kilometraje")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal TotalOdometer { get; set; }


        /// <summary>
        /// Rendimiento
        /// </summary>
        [DisplayName("Kilometraje")]
        [NotMappedColumn]
        [ExcelMappedColumn("Kilómetros")]
        public string TotalOdometerStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TotalOdometer); }  }


        [DisplayName("Litros")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal TotalLiters { get; set; }

        
        [NotMappedColumn]
        [ExcelMappedColumn("Litros")]
        public string TotalLitersStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(TotalLiters, 3); } }

        [DisplayName("Muestras")]
        [NotMappedColumn]
        [ExcelMappedColumn("Muestras")]
        public int Samples { get; set; }


        [NotMappedColumn]
        [ExcelMappedColumn("Cilindraje")]
        public int CylinderCapacity { get; set; }


        [NotMappedColumn]
        [ExcelMappedColumn("Modelo")]
        public int Model { get; set; }

        [NotMappedColumn]
        [ExcelMappedColumn("Tipo")]
        public string Type { get; set; }

        [NotMappedColumn]
        [ExcelMappedColumn("Marca")]
        public string Manufacturer { get; set; }


        /// <summary>
        /// 
        /// </summary>

        [DisplayName("Pendiente")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal slope { get; set; }


        [DisplayName("Tendencia")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string slopeimage
        {
            get
            {
                if (slope > 0.030M) return "slopeup.png";
                else if (slope < -0.030M) return "slopedown.png";
                else return "slopesame.png";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Rutas")]
        [NotMappedColumn]
        [ExcelMappedColumn("Rutas")]
        public string Routes
        { get; set; }

        /// <summary>
        /// 
        /// </summary>
      
        [NotMappedColumn]
        [ExcelMappedColumn("Rendimiento")]
        public decimal Performance
        { get; set; }

        [NotMappedColumn]
        [ExcelMappedColumn("Rendimiento Predeterminado")]
        public decimal DefaultPerformance
        { get; set; }


        /// <summary>
        /// Default Performance as String
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string DefaultPerformanceStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(DefaultPerformance); }
        }


        /// <summary>
        /// Liters as String
        /// </summary>
        [DisplayName("Rendimiento")]
        [NotMappedColumn]
        [ExcelMappedColumn("Rendimiento")]
        public string PerformanceStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Performance); }
        }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class ComparativeEfficiencyFuelsByVehicleSubUnitReport : ComparativeEfficiencyFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Centro de Costo")]
        [NotMappedColumn]
        public string CostCenterName { get; set; }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class ComparativeEfficiencyFuelsByVehicleGroupReport : ComparativeEfficiencyFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Grupo de Vehículos")]
        [NotMappedColumn]
        public string VehicleGroupName { get; set; }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class ComparativeEfficiencyFuelsByVehicle : ComparativeEfficiencyFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Matrícula")]
        [NotMappedColumn]
        [ExcelMappedColumn("Matrícula")]
        public string PlateNumber { get; set; }

        /// <summary>
        /// Identifier of the vehicle
        /// </summary>
        [DisplayName("Vehículo")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int? VehicleId { get; set; }

    }
}