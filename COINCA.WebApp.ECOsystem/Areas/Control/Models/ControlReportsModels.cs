﻿/************************************************************************************************************
*  File    : ControlReportsModels.cs
*  Summary : Control Reports Models
*  Author  : Berman Romero
*  Date    : 21/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Globalization;
using System.Web;
using GridMvc.DataAnnotations;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Report Fuel Types Enum
    /// </summary>
    public enum ReportFuelTypes
    {
        /// <summary>
        /// Vehicles
        /// </summary>
        Vehicles = 100,
        /// <summary>
        /// Vehicle Groups
        /// </summary>
        VehicleGroups = 200,
        /// <summary>
        /// Vehicle Cost Centers
        /// </summary>
        VehicleCostCenters = 300,
        /// <summary>
        /// Vehicle Units
        /// </summary>
        VehicleUnits = 400
    };

    /// <summary>
    /// Report Fuel Types Enum
    /// </summary>
    public enum ReportCriteria
    {
        /// <summary>
        /// Period
        /// </summary>
        Period = 1000,
        /// <summary>
        /// Date Range
        /// </summary>
        DateRange = 2000,
        /// <summary>
        /// Date Range
        /// </summary>
        DayDate = 3000
    };

    public enum ReportComparativeTypes
    {
        /// <summary>
        /// Vehicles
        /// </summary>
        Vehicles = 100,
        /// <summary>
        ///Ruta
        /// </summary>
        route = 200,
        /// <summary>
        /// Modelo
        /// </summary>
        model = 300,
        /// <summary>
        /// Marca
        /// </summary>
        brand = 400,
        /// <summary>
        /// Cilindraje
        /// </summary>
        cilinder = 500
    };

    public enum ReportPreventiveMaintenanceStatus
    {
        /// <summary>
        /// Pendiente
        /// </summary>
        Pendientes = 1,
        /// <summary>
        ///Vencidos
        /// </summary>
        Vencidos = 2,
        /// <summary>
        /// Realizados
        /// </summary>
        Realizado = 3,
    };

    /// <summary>
    /// Control Fuels Reports Base Model
    /// </summary>
    public class ControlFuelsReportsBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public ControlFuelsReportsBase()
        {
            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;
            ReportCriteriaId = (int?)ReportCriteria.Period;
            ReportFuelTypeId = (int?)ReportFuelTypes.Vehicles;
            ReportComparativeId = (int?)ReportComparativeTypes.Vehicles;
            ReportStatus = (int?)ReportPreventiveMaintenanceStatus.Pendientes;            
            key = null;
        }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart titles
        /// </summary>
        public HtmlString Titles { get; set; }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart labels
        /// </summary>
        public HtmlString Labels { get; set; }

        /// <summary>
        /// Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString Colors { get; set; }

        /// <summary>
        /// Alpha Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString AlphaColors { get; set; }


        /// <summary>
        /// Highlight Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString HighlightColors { get; set; }

        /// <summary>
        /// Data formatted as JSON for second chart serie
        /// </summary>
        public HtmlString Data { get; set; }


        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date for Report when dates range is selected
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null)? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null)? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }

        /// <summary>
        /// Report Fuel Type Id
        /// </summary>
        public int? ReportFuelTypeId { get; set; }

        /// <summary>
        /// Report compartive Id
        /// </summary>
        public int? ReportComparativeId { get; set; }

        /// <summary>
        /// Report Status
        /// </summary>
        public int? ReportStatus { get; set; }

        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64 { get; set; }

        /// <summary>
        /// Key Filter Vehicle
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// Report Transaction Type
        /// </summary>
        public int TransactionType { get; set; }

        /// <summary>
        /// Report Vehicle filter
        /// </summary>
        public int FilterVehicle { get; set; }

        /// <summary>
        /// Setting Odometers Filter
        /// </summary>
        public int OdometerType { get; set; }

        /// <summary>
        /// Report Frequency Type Id
        /// </summary>
        public int? ReportFrequencyTypeId { get; set; }

        public int? RedLower { get; set; }

        /// <summary>
        /// Error margin of odometer with POS
        /// </summary>
        public int? RedHigher { get; set; }

        /// <summary>
        /// Tolerance time to get arrival to point
        /// </summary>
        public int? YellowLower { get; set; }

        /// <summary>
        /// Error Margin of geopoint
        /// </summary>
        public int? YellowHigher { get; set; }

        public int? GreenLower { get; set; }

        /// <summary>
        /// Passwords to Validate Repetition
        /// </summary>
        public int? GreenHigher { get; set; }

        /// <summary>
        /// Cost center createria for reports
        /// </summary>
        public int? CostCenterId { get; set; }

    }

    /// <summary>
    /// Control Fuels Reports Base Model
    /// </summary>
    public class ControlPreventiveMaintenanceReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public ControlPreventiveMaintenanceReportBase()
        {
            MaintenanceCatalogId = null;
            Status = (int)ReportPreventiveMaintenanceStatus.Pendientes;
            //Status = ReportPreventiveMaintenanceStatus.Pendientes;
            DateStr = null;
            Odometer = null;
            StartDateStr = null;
            EndDateStr = null;
            CostCenter = null;
            Key = null;
            FilterType = null;
        }

        /// <summary>
        /// Maintenance Catalog Id
        /// </summary>
        public int? MaintenanceCatalogId { get; set; }

        /// <summary>
        /// Status of the Maintenance
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// Date for overdue maintenance
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Date Format
        /// </summary>
        public string DateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(Date); }
            set { Date = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Odometer for overdue maintenance
        /// </summary>
        public int? Odometer { get; set; }

        /// <summary>
        /// Start date for filter
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start date format
        /// </summary>
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End date for filter
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End date format
        /// </summary>
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null)? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Cost center filter
        /// </summary>
        public int? CostCenter { get; set; }

        /// <summary>
        /// key filter
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Filter taype
        /// </summary>
        public int? FilterType { get; set; }



    }

}
