﻿/************************************************************************************************************
*  File    : FuelsByCreditModels.cs
*  Summary : FuelsByCredit Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Models.Account;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// FuelsByCredit Base
    /// </summary>
    public class FuelsByCreditBase
    {

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public IList<FuelsByCredit> Data { get; set; }

        /// <summary>
        /// List of entities to use it for load history information
        /// </summary>
        public IList<FuelsByCredit> List { get; set; }

        /// <summary>
        /// List of Vehicles to use it for load history information
        /// </summary>
        public IList<FuelDistribution> VehiclesList { get; set; }

        /// <summary>
        /// Credit Info property type of CustomerCredits Model
        /// </summary>
        public CustomerCredits CreditInfo { get; set; }

        /// <summary>
        /// Assigned of credit
        /// </summary>
        public decimal? AssignedCredit { get; set; }

        /// <summary>
        /// Assigned of credit
        /// </summary>
        public decimal? AssignedVehicles { get; set; }

        /// Assigned of litters
        /// </summary>
        public decimal? AssignedLiters { get; set; }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        public string AssignedCreditStr
        {
            get { return AssignedCredit == null ? "" : (CreditInfo!=null?CreditInfo.CurrencySymbol:"") + ((decimal)AssignedCredit).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        public string AssignedLitersStr
        {
            get { return AssignedLiters == null ? "" : (CreditInfo != null ? CreditInfo.CurrencySymbol : "") + ((decimal)AssignedLiters).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// % Assigned Credit
        /// </summary>
        public decimal? PctAssignedCredit { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

        public int CostCenterId { get; set; }

    }


    /// <summary>
    /// Fuels By Credit Model
    /// </summary>
    public class FuelsByCredit : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        public int? FuelByCreditId { get; set; }

        /// <summary>
        /// The FuelId FK property identifies fuels model
        /// </summary>
        public int? FuelId { get; set; }

        /// <summary>
        /// The FuelName from fuels model
        /// </summary>
        public string FuelName { get; set; }


        /// <summary>
        /// Year
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// Month Name
        /// </summary>
        public string MonthName
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetMonthName(Month); }
        }
        

        /// <summary>
        /// Total of credit
        /// </summary>
        public decimal? Total { get; set; }

        /// <summary>
        /// Total as String
        /// </summary>
        public string TotalStr
        {
            get { return Total == null ? "" : ((decimal)Total).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }//ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Total,3); }
        }

        /// <summary>
        /// Total as CurrencyStr
        /// </summary>
        public string TotalCurrencyStr
        {
            get { return CurrencySymbol+(TotalStr.Length == 0 ? "0,00" : TotalStr); }
        }

        /// <summary>
        /// Assigned credit
        /// </summary>
        public decimal? Assigned { get; set; }

        /// <summary>
        /// Assigned as String
        /// </summary>
        public string AssignedStr
        {
            get { return Assigned == null ? "" : CurrencySymbol + ((decimal)Assigned).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Available credit
        /// </summary>
        public decimal? Available { get; set; }

        /// <summary>
        /// Available as String
        /// </summary>
        public string AvailableStr
        {
            get { return Available == null ? "" : CurrencySymbol + ((decimal)Available).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Equivalent Liters with Total of credit
        /// </summary>
        public decimal? Liters { get; set; }

        /// <summary>
        /// Liters as String
        /// </summary>
        public string LitersStr
        {
            get { 
                //return Liters == null ? "" : ((decimal)Liters).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); 
                return Liters == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Liters);
            }
        }

        /// <summary>
        /// Liter Price by current FuelId
        /// </summary>
        public decimal? LiterPrice { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// % Assigned Credit
        /// </summary>
        public string PctAvailableCredit
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(((decimal)((Total > 0) ? ((Available / Total) * 100) : 0))); }
            
        }

        private decimal _capacityUnitValue;
        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        public decimal CapacityUnitValue
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters), 3); }
            set { _capacityUnitValue = value;  }
        }

        /// <summary>
        /// CustomerCreditByCostCenterId
        /// </summary>
        public int CustomerCreditByCostCenterId { get; set; }

        /// <summary>
        /// CostCenterId
        /// </summary>
        public int CostCenterId { get; set; }

    }

    /// <summary>
    /// FuelsByCredit Base
    /// </summary>
    public class FuelsByCreditModel : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        public int? FuelByCreditId { get; set; }

        /// <summary>
        /// CustomerCreditByCostCenterId
        /// </summary>
        public int CustomerCreditByCostCenterId { get; set; }

        /// <summary>
        /// CostCenterId
        /// </summary>
        public int CostCenterId { get; set; }

        /// <summary>
        /// The FuelId FK property identifies fuels model
        /// </summary>
        public int? FuelId { get; set; }

        /// <summary>
        /// Total of credit
        /// </summary>
        public decimal? Total { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int? CustomerId
        {
            get { return ECOsystem.Utilities.Session.GetCustomerId(); }
        }

    }

    /// <summary>
    /// FuelsByCredit Base
    /// </summary>
    public class FuelsByCreditSummary
    {

        /// <summary>
        /// List of credit cards entities close to he credit limit
        /// </summary>
        public IList<CreditCard> CreditCardList { get; set; }

        /// <summary>
        /// List of entities to use it for load history information
        /// </summary>
        public IList<FuelsByCredit> List { get; set; }

        /// <summary>
        /// List of Vehicles to use it for load history information
        /// </summary>
        //public IList<VehicleByFuel> VehiclesList { get; set; }
        public IList<FuelDistribution> VehiclesList { get; set; }

        /// <summary>
        /// Credit Info property type of CustomerCredits Model
        /// </summary>
        public CustomerCredits CreditInfo { get; set; }

        /// <summary>
        /// Assigned of credit
        /// </summary>
        public decimal? AssignedCredit { get; set; }


        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        public string AssignedCreditStr
        {
            get { return AssignedCredit == null ? "" : (CreditInfo != null ? CreditInfo.CurrencySymbol : "") + ((decimal)AssignedCredit).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// % Assigned Credit
        /// </summary>
        public decimal? PctAssignedCredit { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Dashboard Indicators
        /// </summary>
        public DashboardControlIndicators DashboardIndicators { get; set; }
    }


    /// <summary>
    /// CreditCard Model
    /// </summary>
    public class CreditCard : ModelAncestor
    {

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        public int? CreditCardId { get; set; }

        /// <summary>
        /// Credit Card NumberId
        /// </summary>
        public int? CreditCardNumberId { get; set; }

        /// <summary>
        /// Credit Card NumberId
        /// </summary>
        public int? PartnerId { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string Pin { get; set; }

        /// <summary>
        /// Credi tCard Type
        /// </summary>
        public string CreditCardType { get; set; }


        /// <summary>
        /// Customer Name
        /// </summary>
        public string EncryptedCustomerName { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string DecryptedCustomerName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedCustomerName); }
            set { EncryptedCustomerName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        private string _creditCardNumber;

        /// <summary>
        /// Credit Card Number
        /// </summary>
        public string CreditCardNumber
        {
            get { return _creditCardNumber ?? ""; }
            set { _creditCardNumber = value; }
        }

        /// <summary>
        /// Credit Card Number with restrict display
        /// </summary>
        public string RestrictCreditCardNumber { get { return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(DisplayCreditCardNumber); } }


        /// <summary>
        /// Credit Card Number for display
        /// </summary>
        public string DisplayCreditCardNumber
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(CreditCardNumber); }
            set { CreditCardNumber = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// Credit Card Expiration Year
        /// </summary>
        public int? ExpirationYear { get; set; }

        /// <summary>
        /// Credit Card ExpirationM onth
        /// </summary>
        public int? ExpirationMonth { get; set; }

        /// <summary>
        /// Month Name
        /// </summary>
        public string ExpirationMonthName
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetMonthName(ExpirationMonth); }
        }

        /// <summary>
        /// Credit Limit
        /// </summary>
        public decimal? CreditLimit { get; set; }

        /// <summary>
        /// Credit Limit
        /// </summary>
        public string CreditLimitStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(CreditLimit); }
        }

        /// <summary>
        /// Credit Limit
        /// </summary>
        public string CreditLimitFormatted
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditLimit, CurrencySymbol); }
        }

        /// <summary>
        /// Credit Available
        /// </summary>
        public decimal? CreditAvailable { get; set; }

        /// <summary>
        /// Credit Available as String
        /// </summary>
        public string CreditAvailableFormatted
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditAvailable, CurrencySymbol); }
        }

        /// <summary>
        /// Credit Extra
        /// </summary>
        public decimal? CreditExtra { get; set; }


        /// <summary>
        /// Credit Available as String
        /// </summary>
        public string CreditExtraFormatted
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditExtra, CurrencySymbol); }
        }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        public string CurrencySymbol { get; set; }


        /// <summary>
        /// Expiration Date 
        /// </summary>
        public string ExpirationDate
        {
            get { return ExpirationMonthName + "/" + ExpirationYear; }
        }

        /// <summary>
        /// Status Id
        /// </summary>
        public int? StatusId { get; set; }


        /// <summary>
        /// Status Name
        /// </summary>
        public string StatusName { get; set; }

        /// <summary>
        /// Encrypted Driver Name
        /// </summary>
        public string EncryptedDriverName { get; set; }


        /// <summary>
        /// Driver Name
        /// </summary>
        public string DecryptedDriverName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedDriverName); }
            set { EncryptedDriverName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Vehicle Plate Description
        /// </summary>
        public string VehiclePlate { get; set; }

        /// <summary>
        /// Plate Id for Validate Transactions
        /// </summary>
        public string TransactionPlate { get; set; }

        /// <summary>
        /// Status Name
        /// </summary>
        public string CreditCardHolder
        {
            get { return IssueForId == 100 ? DecryptedDriverName : IssueForId == 101 ? VehiclePlate : ""; }
            set
            {
                switch (IssueForId)
                {
                    case 100:
                        DecryptedDriverName = value;
                        break;
                    case 101:
                        VehiclePlate = value;
                        break;
                }
            }
        }

        /// <summary>
        /// User Id
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// User Id
        /// </summary>
        public int? VehicleId { get; set; }


        /// <summary>
        /// Credit card Issue For Id 
        /// </summary>
        public int? IssueForId { get; set; }

        /// <summary>
        /// Card RequestId 
        /// </summary>
        public int? CardRequestId { get; set; }

        /// <summary>
        /// Estimated Delivery
        /// </summary>
        public DateTime? EstimatedDelivery { get; set; }

        /// <summary>
        /// Estimated Delivery
        /// </summary>
        public string EstimatedDeliveryStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(EstimatedDelivery); }
            set { EstimatedDelivery = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// BAC Afiliado
        /// </summary>
        public string UnitName { get; set; }
    }

    /// <summary>
    /// Dashboard properties
    /// </summary>
    public class DashboardControlIndicators {

        /// <summary>
        /// FORMAT Monto total de compras de la semana anterior
        /// </summary>
        public string LastWeekTotalAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(LastWeekTotalAmount, ""); }
        }

        /// <summary>
        /// Monto total de compras de la semana anterior
        /// </summary>
        public decimal? LastWeekTotalAmount { get; set; }

        /// <summary>
        /// FORMAT Monto total de compras de la semana actual
        /// </summary>
        public string WeekTotalAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(WeekTotalAmount, ""); }
        }

        /// <summary>
        /// Monto total de compras de la semana actual
        /// </summary>
        public decimal? WeekTotalAmount { get; set; }


        /// <summary>
        /// FORMAT Monto total de compras de la semana actual
        /// </summary>
        public string DiffTotalAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(DiffTotalAmount, ""); }
        }

        /// <summary>
        /// Diferencia del monto inicial (semana anterior) y monto final (semana actual)
        /// </summary>
        public decimal? DiffTotalAmount {
            get {
                return (LastWeekTotalAmount - WeekTotalAmount);
            }
        }

        /// <summary>
        /// Crecimiento o Decrecimiento de las compras
        /// </summary>
        public decimal GrowthDownOrUp { get; set; }
            
        /// <summary>
        /// Cantidad de Transacciones procesadas
        /// </summary>
        public int CountProcessTransactions { get; set; }

        /// <summary>
        /// Cantidad de Transacciones denegadas
        /// </summary>
        public int CountDenyTransactions { get; set; }

        /// <summary>
        /// Total de Transacciones
        /// </summary>
        public int TotalTransactions {
            get {
                return (CountProcessTransactions + CountDenyTransactions);    
            }
        }
    }
}