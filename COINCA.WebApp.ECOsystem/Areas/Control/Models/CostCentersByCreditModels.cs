﻿/************************************************************************************************************
*  File    : CostCentersByCreditModels.cs
*  Summary : CostCentersByCredit Models
*  Author  : Berman Romero
*  Date    : 09/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Collections.Generic;
using System.Globalization;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// CostCentersByCredit Base
    /// </summary>
    public class CostCentersByCreditBase
    {
        /// <summary>
        /// Credit Info property type of CustomerCredits Model
        /// </summary>
        public CustomerCredits CreditInfo { get; set; }

        /// <summary>
        /// Assigned of credit
        /// </summary>
        public decimal? AssignedCredit { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        public string AssignedCreditStr
        {
            get { return AssignedCredit == null ? "" : (CreditInfo != null ? CreditInfo.CurrencySymbol : "") + ((decimal)AssignedCredit).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// % Assigned Credit
        /// </summary>
        public decimal? PctAssignedCredit { get; set; }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public CostCentersByCredit Data { get; set; }

        /// <summary>
        /// Grid Info property displays the main information of the entity 
        /// </summary>
        public CostCentersByCreditGrid GridInfo { get; set; }

        
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleCostCenters model
        /// </summary>
        public int? CostCenterId { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }
    }


    /// <summary>
    /// CostCentersByCredit Model
    /// </summary>
    public class CostCentersByCredit : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        public int? CostCenterId { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public decimal? Value { get; set; }

        /// <summary>
        /// Value as string
        /// </summary>
        public string ValueStr { get; set; }


        /// <summary>
        /// Currency Symbol
        /// </summary>
        public string CurrencySymbol { get; set; }

    }


    /// <summary>
    /// CostCentersByCredit Model
    /// </summary>
    public class CostCentersValues
    {
        /// <summary>
        /// Vehicle Id
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public decimal? Value { get; set; }

        /// <summary>
        /// Liters
        /// </summary>
        public decimal? CapacityUnitValue { get; set; }

    }
    /// <summary>
    /// Vehicle Fuel By Group to Load Grid
    /// </summary>
    public class CostCentersByCreditGrid
    {
        /// <summary>
        /// Is Result
        /// </summary>
        public bool IsResult { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IList<CostCentersByCreditModel> List { get; set; }
    }

    /// <summary>
    /// CostCentersByCredit Model
    /// </summary>
    public class CostCentersByCreditModel : ModelAncestor
    {
        /// <summary>
        /// PlateId of vehicle
        /// </summary>
        [GridColumn(Title = "Matrícula", Width = "80px")]
        public string PlateId { get; set; }
        
        /// <summary>
        /// Vehicle Name
        /// </summary>
        [GridColumn(Title = "Vehículo", Width = "100px")]
        public string VehicleName { get; set; }

        /// <summary>
        /// Vehicle Model
        /// </summary>
        [GridColumn(Title = "Modelo", Width = "100px")]
        public string VehicleModel { get; set; }

        /// <summary>
        /// Fuel Name
        /// </summary>
        [GridColumn(Title = "Combustible", Width = "100px")]
        public string FuelName { get; set; }

        private decimal? _liters;

        /// <summary>
        /// Liters of fuel
        /// </summary>
        [NotMappedColumn]
        public decimal? Liters
        {
            get { return _liters; }
            set { _liters = value; }
        }

        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [GridColumn(Title = "Unidad de Capacidad", Width = "70px")]
        public decimal CapacityUnitValue
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters), 2); }
            set { Liters = Convert.ToDecimal(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, value)); }
        }

        /// <summary>
        /// Credit Amount
        /// </summary>
        [NotMappedColumn]
        public decimal? Amount { get; set; }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string AmountStr
        {
            get { return Amount == null ? "" : ((decimal)Amount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Amount Str as Currency
        /// </summary>
        [GridColumn(Title = "Monto Asignado", Width = "100px")]
        public string AmountStrCurrency
        {
            get { return CurrencySymbol + AmountStr; }
        }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? Id { get; set; }


        /// <summary>
        /// FK Vehicle Id from Users, each vehicle is associate with one user
        /// </summary>
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Fuel Id
        /// </summary>
        [NotMappedColumn]
        public int? DefaultFuelId { get; set; }

        /// <summary>
        /// Cost Center Code
        /// </summary>
        [NotMappedColumn]
        public string CostCenter { get; set; }
        /// <summary>
        /// Result after insert
        /// </summary>
        [NotMappedColumn]
        public int Result { get; set; }

        /// <summary>
        /// Result after insert
        /// </summary>
        [NotMappedColumn]
        public string ResultStr {
            get
            {
                switch (Result)
                {
                    case 0:
                        return "<span2 class='text-success glyphicon glyphicon-ok-circle' data-toggle='tooltip' data-placement='right' title='Registro actualizado exitosamente.'></span2>";
                    case -1:
                        return "<span2 class='text-warning glyphicon glyphicon-ban-circle' data-toggle='tooltip' data-placement='right' title='Dato existente, Acción: No reescribir valor.'></span2>";
                    case -100:
                        return "<span2 class='text-info glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' title='Registro recientemente incluido, No se tomó ninguna acción.'></span2>";
                    case 50001:
                        return "<span2 class='text-danger glyphicon glyphicon-remove-circle' data-toggle='tooltip' data-placement='right' title='Error: Falló por control de Versión'></span2>";
                    case 50002:
                        return "<span2 class='text-danger glyphicon glyphicon-remove-circle' data-toggle='tooltip' data-placement='right' title='Error: No hay disponible crédito'></span2>";
                    default:
                        return "<span2 class='text-danger glyphicon glyphicon-thumbs-down' data-toggle='tooltip' data-placement='right' title='Operación Inválida, Número error: "+Result+"'></span2>";
                }
            }
        }

    }

}