﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Models.Miscellaneous;


namespace  ECOsystem.Areas.Control.Models.Control
{
    public class OdometerAlarm : ModelAncestor
    {
        public int SetAlarm { get; set; }

        public double Odometer { get; set; }

        public double MaxPosibleOdometer { get; set; }

        public double LastOdometer { get; set; }
    }
}