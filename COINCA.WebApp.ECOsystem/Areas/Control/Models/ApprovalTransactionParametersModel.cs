﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace  ECOsystem.Areas.Control.Models.Control
{
    public class ApprovalTransactionParametersModel
    {
        public ApprovalTransactionParametersModel()
        {
            Criteria = (int)ReportCriteria.Period;
            StartDateStr = null;
            EndDateStr = null;
        }

        public Guid? Status { get; set; }

        public int Criteria { get; set; }

        public int? Month { get; set; }

        public int? Year { get; set; }

        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        public DateTime? StartDate { get; set; }

        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        public DateTime? EndDate { get; set; }

        public String SearchText { get; set; }

        public int? RetrieveType { get; set; }

        public int? CostCenterId { get; set; }    
       
    }
}
