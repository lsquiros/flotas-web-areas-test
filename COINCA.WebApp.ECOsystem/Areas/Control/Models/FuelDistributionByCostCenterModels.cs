﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models
{
    public class FuelDistributionByCostCenter
    {
        public int? CustomerCreditByCostCenterId { get; set; }
        public int CostCenterId { get; set; }
        public string CostCenterName { get; set; }
        public string UnitName { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal CreditAssigned { get; set; }
        public decimal CreditCardLimit { get; set; }
        public decimal TotalCreditAmount { get; set; }
        public decimal PctTotalAssigned { get { return CreditCardLimit == 0 ? 0 : (TotalCreditAmount / CreditCardLimit) * 100; } }
        public string CurrencySymbol { get; set; }
        public string TotalMasterCardStr { get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditCardLimit, CurrencySymbol); } }
        public string AssignedStr { get { return CurrencySymbol + TotalCreditAmount.ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); } }
        public bool Selected { get; set; }
        public string SelectedStr { get { return Selected ? "SelectColor" : string.Empty; } }
        public int LineNumber { get; set; }
    }
}