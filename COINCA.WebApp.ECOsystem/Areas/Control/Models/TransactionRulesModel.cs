﻿using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// 
    /// </summary>
    public class TransactionRulesModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public TransactionRulesModelBase()
        {
            Data = new TransactionRulesModel();
            List = new List<TransactionRulesModel>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// 
        /// </summary>
        public TransactionRulesModel Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<TransactionRulesModel> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }
    public class TransactionRulesModel
    {
        public int CustomerId { get; set; }

        public int? RuleCustomerId { get; set; }

        public int RuleId { get; set; }

        public int UserId { get; set; }

        public string RuleName { get; set; }

        public string RuleDescription { get; set; }

        public int RuleType { get; set; }

        public bool RuleActive { get; set; }

        public double RuleValue { get; set; }
    }

    /// <summary>
    /// Model which is going to get the parameters data for some of the transaction rules
    /// </summary>
    public class TransactionRulesParameters
    {
        /// <summary>
        /// Name of the parameter
        /// </summary>
        public string ParameterName { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }
    }
}