﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace  ECOsystem.Areas.Control.Models.Control
{
    public class TransactionsAddApiResponse
    {
        public int TransactionId { get; set; }
        public bool IsInternal { get; set; }
    }
}
