﻿using ECOsystem.Models.Miscellaneous;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models
{
    /// <summary>
    /// Fuels Model
    /// </summary>
    public class Fuels : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        public int? FuelId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string CountryName { get; set; }
    }

    /// <summary>
    /// Fuels By Credit Model
    /// </summary>
    public class FuelsCost
    {

        /// <summary>
        /// The FuelId FK property identifies fuels model
        /// </summary>
        public int? FuelId { get; set; }

        /// <summary>
        /// Liter Price by current FuelId
        /// </summary>
        public decimal? LiterPrice { get; set; }
    }
}