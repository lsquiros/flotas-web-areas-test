﻿using System;
/************************************************************************************************************
*  File    : PreventiveMaintenanceCatalogModels.cs
*  Summary : Preventive Maintenance Catalog Models
*  Author  : Cristian Martínez
*  Date    : 01/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Operators Base Class for Views
    /// </summary>
    public class PreventiveMaintenanceCatalogBase
    {
        /// <summary>
        /// PreventiveMaintenanceBase Constructor
        /// </summary>
        public PreventiveMaintenanceCatalogBase()
        {
            Data = new PreventiveMaintenanceCatalog();
            List = new List<PreventiveMaintenanceCatalog>();            
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public PreventiveMaintenanceCatalog Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceCatalog> List { get; set; }   

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }

    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceCatalog : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PreventiveMaintenanceCatalogId { get; set; }

        /// <summary>
        /// Kilometers Frequency
        /// </summary>
        [DisplayName("Frecuencia (Km)")]
        public double? FrequencyKm { get; set; }

        /// <summary>
        /// Kilometers Frequency with format
        /// </summary>
        [DisplayName("Frecuencia (Km)")]
        [NotMappedColumn]
        [GridColumn(Title = "Frecuencia (Km)", Width = "250px", SortEnabled = true)]
        public string FrequencyKmStr
        {
            get { return (FrequencyKm != null) ? ECOsystem.Utilities.Miscellaneous.GetNumberFormat((decimal)FrequencyKm) : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(0); }
        }

        /// <summary>
        /// Alert Before of Km
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Alertar Antes (Km)")]
        public int? AlertBeforeKm { get; set; }

        /// <summary>
        /// Month Frequency
        /// </summary>
        [DisplayName("Frecuencia (Meses)")]
        [GridColumn(Title = "Frecuencia (Meses)", Width = "250px", SortEnabled = true)]
        public double? FrequencyMonth { get; set; }

        /// <summary>
        /// Alert Before of Month
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Alertar Antes (Meses)")]
        public int? AlertBeforeMonth { get; set; }

        /// <summary>
        /// Date Frequency
        /// </summary>
        [DisplayName("Frecuencia (Fecha)")]
        public DateTime? FrequencyDate { get; set; }

        /// <summary>
        /// Date of Preventive Maintenance entity
        /// </summary>
        [DisplayName("Frecuencia (Fecha)")]
        [GridColumn(Title = "Frecuencia (Fecha)", Width = "250px", SortEnabled = true)]
        //[RequiredIf("FrequencyMonth === null && FrequencyKm === null", ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string FrequencyDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(FrequencyDate); }
            set { FrequencyDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Currency Symbol 
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Alert Before of Date
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Alertar Antes (Fecha)")]
        public int? AlertBeforeDate { get; set; }
                
        /// <summary>
        /// Description of Preventive Maintenance entity
        /// </summary>
        [DisplayName("Nombre")]
        [GridColumn(Title = "Nombre", Width = "400px", SortEnabled = true)]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Description { get; set; }

        ///// <summary>
        ///// Date Maintenance Preventive 
        ///// </summary>
        //[NotMappedColumn]
        //[DisplayName("Fecha de Mantenimiento")]
        //public DateTime? DateMaintenancePreventive { get; set; }

        /// <summary>
        /// Description of Preventive Maintenance entity
        /// </summary>
        [DisplayName("Costo")]
        public decimal Cost { get; set; }

        /// <summary>
        /// Cost with format
        /// </summary>
        [DisplayName("Costo")]
        [GridColumn(Title = "Costo", Width = "100px", SortEnabled = true)]
        public string CostStr {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(Cost, CurrencySymbol); }
        }  
    }
}
