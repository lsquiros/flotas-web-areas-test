﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models
{
    public class ExportableTransactionsFileReportModels
    {
        public string Proveedor { get; set; }

        public string Numero { get; set; }

        public string TipoDocumento { get; set; }

        public DateTime FechaDocumento { get; set; }

        public DateTime FechaRige { get; set; }

        public string Aplicacion { get; set; }

        public decimal Monto { get; set; }

        public decimal Subtotal { get; set; }

        public string Moneda { get; set; }

        public DateTime FechaVence { get; set; }

        public string TipoAsiento { get; set; }

        public string Paquete { get; set; }

        public string SubTipo { get; set; }
    }
}