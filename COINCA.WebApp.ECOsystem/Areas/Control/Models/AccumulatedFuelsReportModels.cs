﻿/************************************************************************************************************
*  File    : AccumulatedFuelsReportModels.cs
*  Summary : Accumulated Fuels Report Models
*  Author  : Berman Romero
*  Date    : 21/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Real Vs Budget Fuels Report Base
    /// </summary>
    public class AccumulatedFuelsReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public AccumulatedFuelsReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<dynamic>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// Month Count
        /// </summary>
        public int MonthCount { get; set; }

        /// <summary>
        /// Vehicle Count
        /// </summary>
        public int VehicleCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }


    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class AccumulatedFuelsReport
    {
        /// <summary>
        /// Year
        /// </summary>
        [DisplayName("Año")]
        [NotMappedColumn]
        public int Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        [DisplayName("# Mes")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int Month { get; set; }

        /// <summary>
        /// Month Name
        /// </summary>
        [DisplayName("Mes")]
        [NotMappedColumn]
        public string MonthName
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetMonthName(Month); }
        }

        /// <summary>
        /// Liters
        /// </summary>
        [DisplayName("Litros")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal Liters { get; set; }


        /// <summary>
        /// Liters as String
        /// </summary>
        [DisplayName("Litros")]
        [NotMappedColumn]
        public string LitersStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Liters, 3); }
        }

        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal CapacityUnitValue
        {
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [DisplayName("Unidad de Capacidad")]
        [NotMappedColumn]
        public string CapacityUnitValueStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(CapacityUnitValue); }
        }

        /// <summary>
        /// Liters
        /// </summary>
        [DisplayName("Litros Calculados")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal RealLiters { get; set; }


        /// <summary>
        /// Liters as String
        /// </summary>
        [DisplayName("Litros Calculados")]
        [NotMappedColumn]
        public string RealLitersStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(RealLiters, 3); }
        }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class AccumulatedFuelsByVehicleSubUnitReport : AccumulatedFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Centro de Costo")]
        [NotMappedColumn]
        public string CostCenterName { get; set; }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class AccumulatedFuelsByVehicleGroupReport : AccumulatedFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Grupo de Vehículos")]
        [NotMappedColumn]
        public string VehicleGroupName { get; set; }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class AccumulatedFuelsByVehicle : AccumulatedFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Matrícula")]
        [NotMappedColumn]
        public string PlateNumber { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        [ExcelMappedColumn("Nombre Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }

    }
}