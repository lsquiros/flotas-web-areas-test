﻿/************************************************************************************************************
*  File    : OdometerTraveledReportModels.cs
*  Summary : Real Vs Budget Fuels Report Models
*  Author  : Berman Romero
*  Date    : 21/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Real Vs Budget Fuels Report Base
    /// </summary>
    public class OdometerTraveledReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public OdometerTraveledReportBase()
        {
            Parameters = new OdometerTraveledParameters();
            List = new List<dynamic>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public OdometerTraveledParameters Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// Control Fuels Reports Base Model
    /// </summary>
    public class OdometerTraveledParameters
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public OdometerTraveledParameters()
        {
            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;
            ReportCriteriaId = (int?)ReportCriteria.Period;
            Key = null;
        }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date for Report when dates range is selected
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }


        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64 { get; set; }

        /// <summary>
        /// Key Filter
        /// </summary>
        public string Key { get; set; }

    }


    /// <summary>
    /// Real Vs Budget Fuels Report Model
    /// </summary>
    public class OdometerTraveledReport
    {

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Placa")]
        [NotMappedColumn]
        public string PlateId { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        [ExcelMappedColumn("Nombre Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }


        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha Inicial")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public DateTime? TrxMinDate { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha Inicial")]
        [NotMappedColumn]
        public string TrxMinDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(TrxMinDate); }
        }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha Final")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public DateTime? TrxMaxDate { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha Final")]
        [NotMappedColumn]
        public string TrxMaxDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(TrxMaxDate); }
        }


        /// <summary>
        /// TrxMinOdometer
        /// </summary>
        [DisplayName("Odómetro Inicial")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int TrxMinOdometer { get; set; }


        /// <summary>
        /// TrxMinOdometer
        /// </summary>
        [DisplayName("Odómetro Final")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int TrxMaxOdometer { get; set; }


        /// <summary>
        /// TrxMinOdometer
        /// </summary>
        [DisplayName("Odómetro Inicial")]
        [NotMappedColumn]
        public string TrxMinOdometerStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TrxMinOdometer); }
        }

        /// <summary>
        /// TrxMinOdometer
        /// </summary>
        [DisplayName("Odómetro Final")]
        [NotMappedColumn]
        public string TrxMaxOdometerStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TrxMaxOdometer); }
        }

        /// <summary>
        /// Difference
        /// </summary>
        [DisplayName("Diferencia")]
        [NotMappedColumn]
        public int Difference
        {
            get { return TrxMaxOdometer - TrxMinOdometer; }
        }

        /// <summary>
        /// TrxMinOdometer
        /// </summary>
        [DisplayName("Diferencia")]
        [NotMappedColumn]
        public string DifferenceStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Difference); }
        }
    }
}