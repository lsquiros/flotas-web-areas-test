﻿//************************************************************************************************************
//*  File    : PreventiveMaintenanceRecordByVehicleDetailModel.cs
//*  Summary : Preventive Maintenance Record ByVehicle Detail Model Models
//*  Author  : Danilo Hidalgo
//*  Date    : 01/15/2015
//* 
//*  Copyright 2014 COINCA, All rights reserved
//*  This software and documentation is the confidential and proprietary information of COINCA S.A.
//*  You shall not disclose such Confidential Information and shall use it only in accordance with the
//*  terms of the license agreement you entered into with this company.
//* 
//************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Operators Base Class for Views
    /// </summary>
    public class PreventiveMaintenanceRecordByVehicleDetailBase
    {
        /// <summary>
        /// PreventiveMaintenanceRecordByVehicleBase Constructor 
        /// </summary>
        public PreventiveMaintenanceRecordByVehicleDetailBase()
        {
            Data = new PreventiveMaintenanceRecordByVehicleDetail();
            List = new List<PreventiveMaintenanceRecordByVehicleDetail>();
            Head = new PreventiveMaintenanceRecordByVehicleDetailHead();
        }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public PreventiveMaintenanceRecordByVehicleDetail Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceRecordByVehicleDetail> List { get; set; }

        /// <summary>
        /// Head of Maintenance Preventive
        /// </summary>
        public PreventiveMaintenanceRecordByVehicleDetailHead Head { get; set; }

    }


    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance Record By Vehicle's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceRecordByVehicleDetail : ModelAncestor
    {
        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceRecordByVehicleDetailId { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceRecordByVehicleId { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceCostId { get; set; }

        /// <summary>
        /// Ddescription of Record
        /// </summary>
        [GridColumn(Title = "Descripción", Width = "150px")]
        public string Description { get; set; }

        /// <summary>
        /// Cost of Maintenance
        /// </summary>
        [GridColumn(Title = "Costo", Width = "150px")]
        public decimal Cost { get; set; }

        /// <summary>
        /// Record of Maintenance
        /// </summary>
        [GridColumn(Title = "Record", Width = "150px")]
        public decimal Record { get; set; }
    }


    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance Record By Vehicle's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceRecordByVehicleDetailHead : ModelAncestor
    {

        /// <summary>
        ///// Data property display the information of the entity 
        ///// </summary>
        //[NotMappedColumn]
        //public PreventiveMaintenanceRecordByVehicle Data { get; set; }

        ///// <summary>
        ///// The PRIMARY KEY property uniquely that identifies each record of this model
        ///// </summary>
        //[NotMappedColumn]
        //public int? PreventiveMaintenanceId { get; set; }

        ///// <summary>
        ///// Plate of Vehicle
        ///// </summary>
        //[GridColumn(Title = "Matrícula", Width = "150px")]
        //public string PlateId { get; set; }

        ///// <summary>
        ///// Name of Vehicle
        ///// </summary>
        //[GridColumn(Title = "Vehículo", Width = "150px")]
        //public string VehicleName { get; set; }
    }


}

