﻿/************************************************************************************************************
*  File    : RealVsBudgetFuelsReportModels.cs
*  Summary : Real Vs Budget Fuels Report Models
*  Author  : Berman Romero
*  Date    : 21/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Real Vs Budget Fuels Report Base
    /// </summary>
    public class RealVsBudgetFuelsReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public RealVsBudgetFuelsReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<dynamic>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// Real Vs Budget Fuels Report Model
    /// </summary>
    public class RealVsBudgetFuelsReport
    {

        /// <summary>
        /// Real Amount
        /// </summary>
        [DisplayName("Monto Consumo Real por Unidad")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal RealAmount { get; set; }

        /// <summary>
        /// Assigned Amount
        /// </summary>
        [DisplayName("Monto Asignado por Unidad")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal AssignedAmount { get; set; }

        /// <summary>
        /// Real Amount Pct
        /// </summary>
        [DisplayName("% Porcentaje")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal RealAmountPct { get; set; }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Moneda")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Assigned Amount as String
        /// </summary>
        [DisplayName("Monto Asignado por Unidad")]
        [ExcelMappedColumn("Monto Asignado por Unidad")]
        [NotMappedColumn]
        public string AssignedAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AssignedAmount, CurrencySymbol); }
        }

        /// <summary>
        /// Real Amount as String
        /// </summary>
        [DisplayName("Monto Consumo Real por Unidad")]
        [ExcelMappedColumn("Monto Consumo Real por Unidad")]
        [NotMappedColumn]
        public string RealAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(RealAmount, CurrencySymbol); }
        }

        /// <summary>
        /// Difference Amount 
        /// </summary>
        [DisplayName("Diferencia")]
        [ExcelMappedColumn("Diferencia")]
        [NotMappedColumn]
        public string DifferenceAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat((AssignedAmount - RealAmount), CurrencySymbol); }
        }


        /// <summary>
        /// Assigned Amount as String
        /// </summary>
        [DisplayName("% Porcentaje")]
        [ExcelMappedColumn("% Porcentaje")]
        [NotMappedColumn]
        public string RealAmountPctStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetPercentageFormat(RealAmountPct, 0); }
        }
        
    }

    /// <summary>
    /// Real Vs Budget Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class RealVsBudgetFuelsByVehicleSubUnitReport : RealVsBudgetFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Centro de Costo")]
        [NotMappedColumn]
        public string CostCenterName { get; set; }

    }

    /// <summary>
    /// Real Vs Budget Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class RealVsBudgetFuelsByVehicleGroupReport : RealVsBudgetFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Grupo de Vehículos")]
        [ExcelMappedColumn("Grupo de Vehículos")]
        [NotMappedColumn]
        public string VehicleGroupName { get; set; }

    }

    /// <summary>
    /// Real Vs Budget Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class RealVsBudgetFuelsByVehicle : RealVsBudgetFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Matrícula")]
        [ExcelMappedColumn("Matrícula")]
        [NotMappedColumn]
        public string PlateNumber { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        [ExcelMappedColumn("Nombre Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }

    }

}