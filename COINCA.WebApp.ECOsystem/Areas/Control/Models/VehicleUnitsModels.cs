﻿/************************************************************************************************************
*  File    : VehicleUnitsModels.cs
*  Summary : VehicleUnits Models
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// VehicleUnits Base
    /// </summary>
    public class VehicleUnitsBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public VehicleUnitsBase()
        {
            Data = new VehicleUnits();
            List = new List<VehicleUnits>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleUnits Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleUnits> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// VehicleUnits Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class VehicleUnits : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? UnitId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre de Unidad")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre de Unidad", Width = "300px", SortEnabled = true)]
        public string Name { get; set; }
        
        /// <summary>
        /// Cost Center Name Units
        /// </summary>
        [NotMappedColumn]
        public string CostCenterName { get; set; }

    }
}