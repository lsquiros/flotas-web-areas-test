﻿/************************************************************************************************************
*  File    : VehicleLowPerformanceReportModels.cs
*  Summary : Vehicle Low Performance Report Models
*  Author  : Andrés Oviedo
*  Date    : 26/01/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities;
using Newtonsoft.Json;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// PerformanceByVehicleReport Base
    /// </summary>
    public class VehicleLowPerformanceReportBase
    {
        /// <summary>
        /// Constructor 
        /// </summary>
        public VehicleLowPerformanceReportBase()
        {

            Parameters = new ControlFuelsReportsBase();
            Data = new VehicleLowPerformanceReports();
            List = new List<VehicleLowPerformanceReports>();
            Menus = new List<AccountMenus>();

            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;

            ReportCriteriaId = (int?)ReportCriteria.Period;
            ReportFuelTypeId = (int?)ReportFuelTypes.Vehicles;
            ReportComparativeId = (int?)ReportComparativeTypes.Vehicles;
            key = null;
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// Searched text
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }

        /// <summary>
        /// Report Fuel Type Id
        /// </summary>
        public int? ReportFuelTypeId { get; set; }


        /// <summary>
        /// Report compartive Id
        /// </summary>
        public int? ReportComparativeId { get; set; }

        /// <summary>
        /// Filterable Vehicle Id
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleLowPerformanceReports Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleLowPerformanceReports> List { get; set; }

        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64
        {
            get
            {
                Parameters.Year = Year;
                Parameters.Month = Month;
                Parameters.StartDate = StartDate;
                Parameters.EndDate = EndDate;
                Parameters.VehicleId = VehicleId;
                return ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(Parameters));
            }
        }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date for Report when dates range is selected
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(EndDate); }
            set { EndDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// ConsolidateLiquidationReport Model
    /// </summary>
    public class VehicleLowPerformanceReports : ModelAncestor
    {
        /// <summary>
        /// Identifier of the vehicle
        /// </summary>
        [ExcelMappedColumn("Número")]
        public int? VehicleId { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        [ExcelMappedColumn("Nombre Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }


        /// <summary>
        /// LicensePlate
        /// </summary>
        [ExcelMappedColumn("Placa")]
        public string PlateId { get; set; }
        
        /// <summary>
        /// Performace rate of the vehicle
        /// </summary>
        [ExcelMappedColumn("Rendimiento")]
        public decimal Performance { get; set; }

        /// <summary>
        /// Performace rate of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string PerformanceStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Performance); } }

        /// <summary>
        /// Type of vehicle
        /// </summary>
        [ExcelMappedColumn("Tipo")]
        public string Type { get; set; }

        /// <summary>
        /// Default Performace rate of the vehicle
        /// </summary>
        [ExcelMappedColumn("Rendimiento Esperado")]
        public decimal DefaultPerformance { get; set; }

        /// <summary>
        /// Default Performace rate of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string DefaultPerformanceStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(DefaultPerformance); } }

        /// <summary>
        /// Model of the vehicle
        /// </summary>
        [ExcelMappedColumn("Modelo")]
        public string VehicleModel { get; set; }

        /// <summary>
        /// Brand of the vehicle
        /// </summary>
        [ExcelMappedColumn("Marca")]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Brand of the vehicle
        /// </summary>
        [ExcelMappedColumn("Año")]
        public int Year { get; set; }
   }
}