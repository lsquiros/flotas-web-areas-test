﻿/************************************************************************************************************
*  File    : PerformanceByVehicleComparativeReportModels.cs
*  Summary : Performance By Vehicle Comparative Report Models
*  Author  : Andrés Oviedo
*  Date    : 05/03/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.Models.Account;
using ECOsystem.Utilities.Helpers;
using System;

using System.Collections.Generic;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// PerformanceByVehicleReport Base
    /// </summary>
    public class PerformanceByVehicleComparativeReportBase
    {
        /// <summary>
        /// Constructor 
        /// </summary>
        public PerformanceByVehicleComparativeReportBase()
        {
            Data = new PerformanceByVehicleComparativeReports();
            List = new List<PerformanceByVehicleComparativeReports>();
            GroupList = new List<PerformanceByVehicleGroupedComparativeReports>();
            Parameters = new PerformanceByVehicleComparativeReportParameters();
            Detail = new List<PerformanceByVehicleDetails>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Detail of Performance of the Vehicle
        /// </summary>
        public IEnumerable<PerformanceByVehicleDetails> Detail { get; set; }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public PerformanceByVehicleComparativeReportParameters Parameters { get; set; }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public PerformanceByVehicleComparativeReports Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PerformanceByVehicleComparativeReports> List { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PerformanceByVehicleGroupedComparativeReports> GroupList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }

    /// <summary>
    /// PerformanceByVehicleReport Base
    /// </summary>
    public class PerformanceByVehicleComparativeReportParameters
    {
        /// <summary>
        /// Constructor 
        /// </summary>
        public PerformanceByVehicleComparativeReportParameters()
        {
            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;

            ReportCriteriaId = (int?)ReportCriteria.Period;
            ReportFuelTypeId = (int?)ReportFuelTypes.Vehicles;
            ReportComparativeId = 100; //100=Vehicle, 200=Manufacturer, 300=VehicleModel, 400=VehicleType
            GroupBy = 2; // Month
        }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Agrupate by
        /// </summary>
        public int GroupBy { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }

        /// <summary>
        /// Report Fuel Type Id
        /// </summary>
        public int? ReportFuelTypeId { get; set; }

        /// <summary>
        /// Report Fuel Type Id
        /// </summary>
        public bool showZeros { get; set; }

        /// <summary>
        /// Report compartive Id
        /// </summary>
        public int ReportComparativeId { get; set; }

        /// <summary>
        /// Filterable Vehicle Id
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64 { get; set; }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date for Report when dates range is selected
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(EndDate); }
            set { EndDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Brand of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Model of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string VehicleModel { get; set; }

        /// <summary>
        /// Year of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public int? VehicleYear { get; set; }

        /// <summary>
        /// Type of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string VehicleType { get; set; }

        /// <summary>
        /// Medium of the results
        /// </summary>
        [ExcelNoMappedColumn]
        public int? CostCenterId { get; set; }

        /// <summary>
        /// Medium of the results
        /// </summary>
        [ExcelNoMappedColumn]
        public int? UnitId { get; set; }

        /// <summary>
        /// VehicleGroupId
        /// </summary>
        [ExcelNoMappedColumn]
        public int? VehicleGroupId { get; set; }
    }

    /// <summary>
    /// Performance By Vehicle Comparative Reports Model
    /// </summary>
    public class PerformanceByVehicleGroupedComparativeReports
    {
        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Type of the vehicle
        /// </summary>
        [ExcelMappedColumn("Agrupación")]
        public string GroupOption { get; set; }

        /// <summary>
        /// Group Value
        /// </summary>
        [ExcelNoMappedColumn]
        public int GroupValue { get; set; }


        /// <summary>
        /// Name of unit
        /// </summary>
        [ExcelMappedColumn("Unidad")]
        public string UnitName { get; set; }

        /// <summary>
        /// Cost Center Name
        /// </summary>
        [ExcelMappedColumn("Centro de Costo")]
        public string CostCenterName { get; set; }

        /// <summary>
        /// Cost Center Name
        /// </summary>
        [ExcelNoMappedColumn]
        public DateTime TrxDate { get; set; }

        /// <summary>
        /// Week Number
        /// </summary>
        [ExcelNoMappedColumn]
        public int TrxWeekNumber { get; set; }

        /// <summary>
        /// Tr xDispla yWeek Number
        /// </summary>
        [ExcelNoMappedColumn]
        public int TrxDisplayWeekNumber { get; set; }


        /// <summary>
        /// Total of Trx Reported Liters
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? TrxReportedLiters { get; set; }

        /// <summary>
        /// Total of Trx Traveled Odometer
        /// </summary>
        [ExcelMappedColumn("Total de Recorrido")]
        public decimal? TrxTraveledOdometer { get; set; }

        /// <summary>
        /// Trx Performance
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? TrxPerformance { get; set; }

        /// <summary>
        /// Trx Performance
        /// </summary>
        [ExcelMappedColumn("Total Recorrido Basado en GPS")]
        public decimal? GpsTraveledOdometer { get; set; }

        /// <summary>
        /// Gps Performance
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? GpsPerformance { get; set; }

        /// <summary>
        /// Gps Hours On
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? GpsHoursOn { get; set; }

        /// <summary>
        /// Agrupate by
        /// </summary>
        [ExcelNoMappedColumn]
        public int GroupBy { get; set; }

        /* Derived Properties */

        /// <summary>
        /// Trx Day
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxDay { get { return TrxDate.Year + "" + TrxDate.Month.ToString("00") + "" + TrxDate.Day.ToString("00"); } }
        /// <summary>
        /// Trx Day
        /// </summary>
        [ExcelMappedColumn("Fecha Transacción")]
        public string TrxDateFormatted { get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(TrxDate); } }
        /// <summary>
        /// Trx Reported Liters Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxReportedLitersFormatted { get { return TrxReportedLiters == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(TrxReportedLiters, 3); } }
        /// <summary>
        /// Trx Traveled Odometer Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxTraveledOdometerFormatted { get { return TrxTraveledOdometer == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TrxTraveledOdometer); } }
        /// <summary>
        /// Trx Performance Formatted
        /// </summary>
        [ExcelMappedColumn("Rendimiento por Transacción")]
        public string TrxPerformanceFormatted { get { return TrxPerformance == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TrxPerformance); } }
        /// <summary>
        /// Gps Traveled Odometer Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string GpsTraveledOdometerFormatted { get { return GpsTraveledOdometer == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(GpsTraveledOdometer); } }
        /// <summary>
        /// Gps Performance Formatted
        /// </summary>
        [ExcelMappedColumn("Rendimiento por GPS")]
        public string GpsPerformanceFormatted { get { return GpsPerformance == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(GpsPerformance); } }
        /// <summary>
        /// Gps Hours On Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string GpsHoursOnFormatted { get { return GpsHoursOn == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(GpsHoursOn); } }

        /// <summary>
        /// Gps Hours On Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string FormattedDateGroupBy
        {
            get
            {
                switch (GroupBy)
                {
                    case 1: //Year
                        return "" + TrxDate.Year;
                    case 2: //Month
                        return TrxDate.Year + "/" + ECOsystem.Utilities.Miscellaneous.GetMonthName(TrxDate.Month);
                    case 3: //Day
                        return ECOsystem.Utilities.Miscellaneous.GetUtcDateFormat(TrxDate);
                    case 4: //Week
                        return TrxDate.Year + "/" + ECOsystem.Utilities.Miscellaneous.GetMonthName(TrxDate.Month) + " - Semana " + TrxDisplayWeekNumber;
                }
                return "";
            }
        }

        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [ExcelMappedColumn("Total de ")]
        public decimal? CapacityUnitValue
        {
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), TrxReportedLiters); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        ///
        [ExcelNoMappedColumn]
        public string CapacityUnitValueStr
        {
            get
            {
                return CapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(CapacityUnitValue, 3);
            }

        }

        [ExcelNoMappedColumn]
        public decimal DefaultPerformance { get; set; }
    }

    /// <summary>
    /// Performance By Vehicle Comparative Reports Model
    /// </summary>
    public class PerformanceByVehicleComparativeReports
    {
        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Identifier of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// LicensePlate
        /// </summary>
        [ExcelMappedColumn("Placa")]
        public string PlateId { get; set; }

        /// <summary>
        /// Identifier of the vehicle
        /// </summary>
        [ExcelMappedColumn("Nombre del Vehículo")]
        public string VehicleName { get; set; }

        /// <summary>
        /// Identifier of the vehicle
        /// </summary>
        [ExcelMappedColumn("Marca")]
        public string Manufacturer { get; set; }


        /// <summary>
        /// Year of the vehicle
        /// </summary>
        [ExcelMappedColumn("Año")]
        public int? VehicleYear { get; set; }

        /// <summary>
        /// Model of the vehicle
        /// </summary>
        [ExcelMappedColumn("Modelo")]
        public string VehicleModel { get; set; }

        /// <summary>
        /// Type of the vehicle
        /// </summary>
        [ExcelMappedColumn("Tipo")]
        public string VehicleType { get; set; }

        /// <summary>
        /// Name of unit
        /// </summary>
        [ExcelMappedColumn("Unidad")]
        public string UnitName { get; set; }

        /// <summary>
        /// Cost Center Name
        /// </summary>
        [ExcelMappedColumn("Centro de Costo")]
        public string CostCenterName { get; set; }

        /// <summary>
        /// Rendimiento Esperado
        /// </summary>
        [ExcelMappedColumn("Rendimiento Esperado")]
        public decimal DefaultPerformance { get; set; }

         
        /// <summary>
        /// Cost Center Name
        /// </summary>
        [ExcelNoMappedColumn]
        public DateTime TrxDate { get; set; }

        /// <summary>
        /// Week Number
        /// </summary>
        [ExcelNoMappedColumn]
        public int TrxWeekNumber { get; set; }

        /// <summary>
        /// Tr xDispla yWeek Number
        /// </summary>
        [ExcelNoMappedColumn]
        public int TrxDisplayWeekNumber { get; set; }


        /// <summary>
        /// Total of Trx Reported Liters
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? TrxReportedLiters { get; set; }

        /// <summary>
        /// Total of Trx Traveled Odometer
        /// </summary>
        [ExcelMappedColumn("Total de Recorrido")]
        public decimal? TrxTraveledOdometer { get; set; }

        /// <summary>
        /// Trx Performance
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? TrxPerformance { get; set; }

        /// <summary>
        /// Trx Performance
        /// </summary>
        [ExcelMappedColumn("Total Recorrido Basado en GPS")]
        public decimal? GpsTraveledOdometer { get; set; }

        /// <summary>
        /// Gps Performance
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? GpsPerformance { get; set; }

        /// <summary>
        /// Gps Hours On
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? GpsHoursOn { get; set; }


        /// <summary>
        /// Agrupate by
        /// </summary>
        [ExcelNoMappedColumn]
        public int GroupBy { get; set; }

        /* Derived Properties */

        /// <summary>
        /// Trx Day
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxDay { get { return TrxDate.Year + "" + TrxDate.Month.ToString("00") + "" + TrxDate.Day.ToString("00"); } }

        /// <summary>
        /// Trx Day
        /// </summary>
        [ExcelMappedColumn("Fecha Transacción")]
        public string TrxDateFormatted { get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(TrxDate); } }

        /// <summary>
        /// Trx Reported Liters Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxReportedLitersFormatted { get { return TrxReportedLiters == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(TrxReportedLiters, 3); } }
        /// <summary>
        /// Trx Traveled Odometer Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxTraveledOdometerFormatted { get { return TrxTraveledOdometer == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TrxTraveledOdometer); } }
        /// <summary>
        /// Trx Performance Formatted
        /// </summary>
        [ExcelMappedColumn("Rendimiento por Transacción")]
        public string TrxPerformanceFormatted { get { return TrxPerformance == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TrxPerformance); } }
        /// <summary>
        /// Gps Traveled Odometer Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string GpsTraveledOdometerFormatted { get { return GpsTraveledOdometer == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(GpsTraveledOdometer); } }
        /// <summary>
        /// Gps Performance Formatted
        /// </summary>
        [ExcelMappedColumn("Rendimiento por GPS")]
        public string GpsPerformanceFormatted { get { return GpsPerformance == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(GpsPerformance); } }
        /// <summary>
        /// Gps Hours On Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string GpsHoursOnFormatted { get { return GpsHoursOn == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(GpsHoursOn); } }

        /// <summary>
        /// Gps Hours On Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string FormattedDateGroupBy
        {
            get
            {
                switch (GroupBy)
                {
                    case 1: //Year
                        return "" + TrxDate.Year;
                    case 2: //Month
                        return TrxDate.Year + "/" + ECOsystem.Utilities.Miscellaneous.GetMonthName(TrxDate.Month);
                    case 3: //Day
                        return ECOsystem.Utilities.Miscellaneous.GetUtcDateFormat(TrxDate);
                    case 4: //Week
                        return TrxDate.Year + "/" + ECOsystem.Utilities.Miscellaneous.GetMonthName(TrxDate.Month) + " - Semana " + TrxDisplayWeekNumber;
                }
                return "";
            }
        }

        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [ExcelMappedColumn("Total de ")]
        public decimal? CapacityUnitValue
        {
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), TrxReportedLiters); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [ExcelNoMappedColumn]
        public string CapacityUnitValueStr
        {
            get
            {
                return CapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(CapacityUnitValue, 3);
            }

        }
    }

    /// <summary>
    /// Performance By Vehicle Detail
    /// </summary>
    public class PerformanceByVehicleDetails
    {
        /// <summary>
        /// Identifier of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Cost Center Name
        /// </summary>
        [ExcelMappedColumn("Fecha de Transacción")]
        public DateTime TrxDate { get; set; }

        /// <summary>
        /// Trx Initial Odometer
        /// </summary>
        [ExcelMappedColumn("Odómetro en Transacción Anterior")]
        public decimal? TrxPreviousOdometer { get; set; }

        /// <summary>
        /// Trx Final Odometer
        /// </summary>
        [ExcelMappedColumn("Odómetro en Transacción")]
        public decimal? TrxReportedOdometer { get; set; }

        /// <summary>
        /// Total of Trx Reported Liters
        /// </summary>
        [ExcelMappedColumn("Total de Litros")]
        public decimal? TrxReportedLiters { get; set; }

        /// <summary>
        /// Trx Performance
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? TrxPerformance { get; set; }


        /// <summary>
        /// Gps Performance
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? GpsPerformance { get; set; }


        /// <summary>
        /// Trx Initial Odometer
        /// </summary>
        [ExcelMappedColumn("Odómetro GPS en Transacción Anterior")]
        public decimal? GpsPreviousOdometer { get; set; }

        /// <summary>
        /// Trx Final Odometer
        /// </summary>
        [ExcelMappedColumn("Odómetro GPS en Transacción")]
        public decimal? GpsReportedOdometer { get; set; }




        /* Derived Properties */

        /// <summary>
        /// Trx Reported Liters Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxReportedLitersFormatted { get { return TrxReportedLiters == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(TrxReportedLiters, 3); } }
        /// <summary>
        /// Trx Traveled Odometer Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxTraveledOdometerFormatted { get { return TrxPreviousOdometer == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TrxReportedOdometer - TrxPreviousOdometer); } }
        /// <summary>
        /// Trx Performance Formatted
        /// </summary>
        [ExcelMappedColumn("Rendimiento por Transacción")]
        public string TrxPerformanceFormatted { get { return TrxPerformance == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TrxPerformance); } }
        /// <summary>
        /// Gps Traveled Odometer Formatted
        /// </summary>
        [ExcelNoMappedColumn]
        public string GpsTraveledOdometerFormatted { get { return GpsPreviousOdometer == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(GpsReportedOdometer - GpsPreviousOdometer); } }
        /// <summary>
        /// Gps Performance Formatted
        /// </summary>
        [ExcelMappedColumn("Rendimiento por GPS")]
        public string GpsPerformanceFormatted { get { return GpsPerformance == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(GpsPerformance); } }
        /// <summary>
        /// Trx Final Odometer
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxPreviousOdometerFormatted { get { return TrxPreviousOdometer == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TrxPreviousOdometer); } }
        /// <summary>
        /// Trx Initial Odometer
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxReportedOdometerFormatted { get { return TrxReportedOdometer == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(TrxReportedOdometer); } }
        /// <summary>
        /// Gps Initial Odometer
        /// </summary>
        [ExcelNoMappedColumn]
        public string GpsPreviousOdometerFormatted { get { return GpsPreviousOdometer == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(GpsPreviousOdometer); } }
        /// <summary>
        /// Gps Initial Odometer
        /// </summary>
        [ExcelNoMappedColumn]
        public string GpsReportedOdometerFormatted { get { return GpsReportedOdometer == null ? "N/A" : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(GpsReportedOdometer); } }
        /// <summary>
        /// Gps Initial Odometer
        /// </summary>
        [ExcelNoMappedColumn]
        public string TrxDateFormatted { get { return ECOsystem.Utilities.Miscellaneous.GetUtcDateTimeFormat(TrxDate); } }
    }
}