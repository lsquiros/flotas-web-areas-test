﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models
{
    public class TransactionsSAPFileReport
    {
        public string ElectronicInvoice { get; set; }
        public string SAPProv { get; set; }

        public string LegalId { get; set; }

        public string ServiceStation { get; set; }

        public DateTime InvoiceDate { get; set; }

        public string Invoice { get; set; }

        public decimal Amount { get; set; }

        public string CostCenter { get; set; }

        public string Unit { get; set; }

        public string Vehicle { get; set; }

        public string PlateId { get; set; }
    }
}