﻿using  ECOsystem.Areas.Control.Business.Control;
using System;
using ECOsystem.Models.Account;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    public class ApprovalTransactionsBase 
    {     
        /// <summary>
        /// ApprovalTransaction Constructor
        /// </summary>
        public ApprovalTransactionsBase()
        {
            Parameters = new ApprovalTransactionParametersModel();
            Data = new ApprovalTransactions();
            List = new List<ApprovalTransactions>();
            Menus = new List<AccountMenus>();          
        }
    
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public ApprovalTransactions Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<ApprovalTransactions> List { get; set; }

        public ApprovalTransactionParametersModel Parameters { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    
    }

    public class ApprovalTransactions
    {
        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);

        /// <summary>
        /// Checked
        /// </summary>
        public bool Checked { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        public int TransactionId { get; set; }
        /// <summary>
        /// Date
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Vehicle
        /// </summary>
        public string VehicleName { get; set; }
        /// <summary>
        /// VehicleId
        /// </summary>
        public int VehicleId { get; set; }
        /// <summary>
        /// Plate
        /// </summary>
        public string PlateId { get; set; }

        /// <summary>
        /// PaymentInstrumentType
        /// </summary>
        public string PaymentInstrumentType { get; set; }

        /// <summary>
        /// CreditCard
        /// </summary>
        public string CreditCard { get; set; }

        /// <summary>
        /// PaymentInstrumentCode
        /// </summary>
        public string PaymentInstrumentCode { get; set; }
        
        /// <summary>
        /// CostCenterId
        /// </summary>
        public int CostCenterId { get; set; }
        /// <summary>
        /// CostCenter
        /// </summary>
        public string CostCenterName { get; set; }
        /// <summary>
        /// Identification
        /// </summary>
        public string DriverId { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string DriverName { get; set; }
        /// <summary>
        /// Manufacter
        /// </summary>
        public string Manufacter { get; set; }
        /// <summary>
        /// Fuel
        /// </summary>
        public decimal Liters { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// Type
        /// </summary>
        public string FuelName { get; set; }
        /// <summary>
        /// ServiceStation
        /// </summary>
        public string ServiceStationName { get; set; }
        /// <summary>
        /// BacId
        /// </summary>
        public string TerminalId { get; set; }
        /// <summary>
        /// AffiliateNumber
        /// </summary>
        public string AffiliateNumber { get; set; }
        /// <summary>
        /// Invoice
        /// </summary>
        public string Invoice { get; set; }
        /// <summary>
        /// LegalId
        /// </summary>
        public string LegalId { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public Guid? Status { get; set; }

        public string StatusName { get; set; }
        /// <summary>
        /// CountryName
        /// </summary>
        public string CountryName { get; set; }
        /// <summary>
        /// ExchangeValue
        /// </summary>
        public string ExchangeValue { get; set; }
        /// <summary>
        /// RealAmount
        /// </summary>
        public decimal RealAmount { get; set; }

        public int InvoiceCount { get; set; }

        public string DriverNameDecrypt { get {  return string.IsNullOrEmpty(DriverName) ? string.Empty : ECOsystem.Utilities.TripleDesEncryption.Decrypt(DriverName); } }

        public string DriverIdDecrypt { get { return DriverId == "N/A" ? DriverId : ECOsystem.Utilities.TripleDesEncryption.Decrypt(DriverId); } }

        public string CreditCardNumberWithMask { get { return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(ECOsystem.Utilities.TripleDesEncryption.Decrypt(CreditCard)); } }

        public string PaymentInstrument { get { if (PaymentInstrumentType == "Tarjeta" || PaymentInstrumentType == null)
                                                    return CreditCardNumberWithMask;
                                                else
                                                    return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(ECOsystem.Utilities.TripleDesEncryption.Decrypt(PaymentInstrumentCode));
                                            } }

        public int? ServiceStationId { get; set; }

        Guid _statusauth = new Guid("8BAFAEE7-0590-4441-B451-EA8AC9394143");

        /// <summary>
        /// RealAmount
        /// </summary>
        public string Color
        {
            get 
            {
                if (Status != _statusauth && string.IsNullOrEmpty(ServiceStationName) && (Change == null || !(bool)Change))
                {
                    return "ChangeNoServiceStationColor";
                }
                //else if (Status != _statusauth && string.IsNullOrEmpty(LegalId) && (Change == null || !(bool)Change))
                //{
                //    return "ChangeNoLegalId";
                //}
                else if (Status != _statusauth && InvoiceCount > 1)
                {
                    return "ChangeDuplicateColor";
                }
                else if (Change != null && (bool)Change)
                {
                    return "ChangeSelectColor";
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// RealAmount
        /// </summary>
        public bool? Change { get; set; }
        /// <summary>
        /// RealAmount
        /// </summary>
        public string Icon
        {
            get
            {
                if (Status != _statusauth && string.IsNullOrEmpty(ServiceStationName) && (Change == null || !(bool)Change))
                {
                    return "<i style=\"font-size: 24px;\" data-toggle=\"tooltip\" data-placement=\"top\"  title=\"La transacción no esta asociada a una estación de servicio.\" class=\"mdi mdi-alert\"></i>";
                }
                //else if (Status != _statusauth && string.IsNullOrEmpty(LegalId) && (Change == null || !(bool)Change))
                //{
                //    return "<i style=\"font-size: 24px;\" data-toggle=\"tooltip\" data-placement=\"top\"  title=\"La estación de servicio no cuenta con la cédula jurídica.\" class=\"mdi mdi-alert\"></i>";
                //}
                else if (Status != _statusauth && InvoiceCount > 1)
                {
                    return "<i style=\"font-size: 24px;\" data-toggle=\"tooltip\" data-placement=\"top\"  title=\"Existe una transacción con el mismo número de factura.\" class=\"mdi mdi-alert\"></i>";
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        public decimal? CapacityUnitValue
        {
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        public string CapacityUnitValueStr
        {
            get
            {
                return CapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(CapacityUnitValue, 3); 
            }

        }

        public int LineNumber { get; set; }

        public bool  AllowProcess { get; set; }

        public bool AlreadyApproved { get; set; }

        /// <summary>
        /// Validate if the MasterCard IsInternational
        /// </summary>
        public bool IsInternational { get; set; }

        public string DynamicColumnListXML { get; set; }

        public string DynamicColumnValuesXML { get; set; }

        private List<ApprovalTransactionsParameters> _DynamicColumnList;
        public List<ApprovalTransactionsParameters> DynamicColumnList
        {
            get
            {
                return string.IsNullOrEmpty(DynamicColumnListXML) ? new List<ApprovalTransactionsParameters>()
                    : ECOsystem.Utilities.Miscellaneous.GetXMLDeserialize<List<ApprovalTransactionsParameters>>(DynamicColumnListXML, typeof(ApprovalTransactionsParameters).Name);
            }
            set { _DynamicColumnList = value; }
        }

        private List<ApprovalTransactionsParameters> _DynamicColumnValues;
        public List<ApprovalTransactionsParameters> DynamicColumnValues
        {
            get
            {
                return string.IsNullOrEmpty(DynamicColumnValuesXML) ? new List<ApprovalTransactionsParameters>()
                    : ECOsystem.Utilities.Miscellaneous.GetXMLDeserialize<List<ApprovalTransactionsParameters>>(DynamicColumnValuesXML, typeof(ApprovalTransactionsParameters).Name);
            }
            set { _DynamicColumnValues = value; }
        }

        /// <summary>
        /// CustomerName
        /// </summary>
        public string CustomerName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(_customerName);
            }
            set
            {
                this._customerName = value;
            }
        }

        private string _customerName;

        /// <summary>nch
        /// CustomerBranchName
        /// </summary>
        public string CustomerBranchName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(_customerBranchName);
            }
            set
            {
                this._customerBranchName = value;
            }
        }

        private string _customerBranchName;

        /// <summary>nch
        /// IsCustomerBranch
        /// </summary>
        public int IsCustomerBranch { get; set; }

        public string ElectronicInvoice { get; set; }
    }

    public class ApprovalTransactionsImport
    {

        public string AuthorizationNumber { get; set; }

        public string Reference { get; set; }

        public string Date { get; set; }

        private DateTime _Date;
        public DateTime DateFormat { get { return ECOsystem.Utilities.Miscellaneous.ConvertDateFromApprovalToDateTime(Date); } set { _Date = value; } }

        public int Odometer { get; set; }

        public decimal Litters { get; set; }
        
        public string Message { get; set; }

        public decimal Amount { get; set; }

        public string AmountText { get; set; }

        public int LineNumber { get; set; }

        public bool IsException { get; set; }

        public string Description { get; set; }
    }

    public class ApprovalTransactionsParameters
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}