﻿using  ECOsystem.Areas.Control.Business;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models
{
    public class AccountFileReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public AccountFileReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<AccountFileReportModel>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<AccountFileReportModel> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    public class AccountFileReportModel
    {
        /// <summary>
        /// Identify
        /// </summary>
        public string Identify { get { return AccountFileReportBusiness.GetIdentifier(TransactionDate); } }

        /// <summary>
        /// PlateId
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// TransactionDate
        /// </summary>
        public DateTime TransactionDate { get; set; }

        /// <summary>
        /// InvoceDate
        /// </summary>
        public DateTime InvoceDate { get { return TransactionDate; } }

        /// <summary>
        /// ServiceStationId
        /// </summary>
        public string ServiceStationId { get; set; }

        /// <summary>
        /// Fuel
        /// </summary>
        public string Fuel { get; set; }

        /// <summary>
        /// Liters
        /// </summary>
        public decimal Liters { get; set; }

        /// <summary>
        /// Liters as String
        /// </summary>
        public string LitersStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Liters, 3); }
        }


        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Odometers
        /// </summary>
        public double Odometers { get; set; }

        /// <summary>
        /// CostCenter
        /// </summary>
        public string CostCenter { get; set; }
    }
  }
