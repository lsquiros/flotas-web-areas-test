﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceReportModels.cs
*  Summary : Preventive Maintenance Report Models
*  Author  : Napoleón Alvarado
*  Date    : 12/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;

using System.Collections.Generic;
using System.ComponentModel;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Preventive Maintenance Report Base
    /// </summary>
    public class PreventiveMaintenanceReportBase
    {
        /// <summary>
        /// Preventive Maintenance Reports Base Constructor
        /// </summary>
        public PreventiveMaintenanceReportBase()
        {
            Parameters = new ControlPreventiveMaintenanceReportBase();
            List = new List<dynamic>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlPreventiveMaintenanceReportBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// List of Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Preventive Maintenance Report Model
    /// </summary>
    public class PreventiveMaintenanceReport
    {

        /// <summary>
        /// FK Preventive Maintenance
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int PreventiveMaintenanceId { get; set; }

        /// <summary>
        /// FK Preventive Maintenance Catalog
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int PreventiveMaintenanceCatalogId { get; set; }

        /// <summary>
        /// Maintenance Name
        /// </summary>
        [DisplayName("Mantenimiento")]
        [GridColumn(Title = "Mantenimiento", Width = "90px", SortEnabled = true)]
        public string MaintenanceName { get; set; }
        
        /// <summary>
        /// FK Vehicle
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int VehicleId { get; set; }
        
        /// <summary>
        /// Vehicle Plate
        /// </summary>
        [DisplayName("Placa")]
        [GridColumn(Title = "Placa", Width = "90px", SortEnabled = true)]
        public string PlateId { get; set; }
        
        /// <summary>
        /// Vehicle Name
        /// </summary>
        [DisplayName("Nombre Vehículo")]
        [GridColumn(Title = "Nombre Vehículo", Width = "90px", SortEnabled = true)]
        public string VehicleName { get; set; }
        
        /// <summary>
        /// Vehicle Odometer
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double? ActualOdometer { get; set; }
        
        /// <summary>
        /// Actual Odometer Format
        /// </summary>
        [DisplayName("Kilometraje Actual")]
        [GridColumn(Title = "Kilometraje Actual", Width = "90px", SortEnabled = true)]
        public string ActualOdometerStr
        {
            //get;
            //set;
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Convert.ToDecimal(ActualOdometer)); }
        }
        
        /// <summary>
        /// Last Review Odometer
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double? LastReviewOdometer { get; set; }
        
        /// <summary>
        /// Next Review Odometer
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double? NextReviewOdometer { get; set; }
        
        /// <summary>
        /// Next Review Odometer Format
        /// </summary>
        [DisplayName("Kilometraje Próxima Revisión")]
        [GridColumn(Title = "Kilometraje Próxima Revisión", Width = "90px", SortEnabled = true)]
        public string NextReviewOdometerStr
        {
            //get;
            //set;
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(Convert.ToDecimal(NextReviewOdometer)); }
        }
        
        /// <summary>
        /// Last Review Date
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public DateTime LastReviewDate { get; set; }
        
        /// <summary>
        /// Next Review Date
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public DateTime NextReviewDate { get; set; }
        
        /// <summary>
        /// Next Review Date Format
        /// </summary>
        [DisplayName("Fecha Próxima Revisión")]
        [GridColumn(Title = "Fecha Próxima Revisión", Width = "90px", SortEnabled = true)]
        public string NextReviewDateStr
        {
            //get;
            //set;
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(NextReviewDate); }
            set { NextReviewDate = Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)); }
        }
        
        /// <summary>
        /// Frequency
        /// </summary>
        [DisplayName("Frecuencia")]
        [GridColumn(Title = "Frecuencia", Width = "90px", SortEnabled = true)]
        public string Frequency { get; set; }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Moneda")]
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Cost
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal Cost { get; set; }
        
        /// <summary>
        /// Cost Format
        /// </summary>
        [DisplayName("Presupuesto")]
        [GridColumn(Title = "Presupuesto", Width = "90px", SortEnabled = true)]
        public string CostStr
        {
            //get;
            //set;
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(Convert.ToDecimal(Cost), CurrencySymbol); }
        }
        
        /// <summary>
        /// Record
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal Record { get; set; }
        
        /// <summary>
        /// Record Format
        /// </summary>
        [DisplayName("Costo Real")]
        [GridColumn(Title = "Costo Real", Width = "90px", SortEnabled = true)]
        public string RecordStr
        {
            //get;
            //set;
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(Convert.ToDecimal(Record), CurrencySymbol); }
        }
        
        /// <summary>
        /// Record
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public bool Registred { get; set; }

        /// <summary>
        /// FilterName
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string FilterName { get; set; }



    }
}