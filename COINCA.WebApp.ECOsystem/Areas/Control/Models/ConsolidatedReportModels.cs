﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Models
{
    public class ConsolidatedReportModels
    {
        public string VehicleName { get; set; }
        public string PlateId { get; set; }
        public string Description { get; set; }
        public int TravelKilometers { get; set; }
        public decimal Liters { get; set; }
        public decimal Performance { get; set; }
        public decimal GallonsPerformance { get; set; }
        public decimal PreventiveMaintenanceTotal { get; set; }
        public decimal CorrectiveMaintenanceTotal { get; set; }
        public decimal TireCost { get; set; }
        public decimal BatteryCost { get; set; }
        public decimal Insurance { get; set; }
        public decimal Depreciation { get; set; }
        public decimal FuelAmount { get; set; }
        public decimal CostWithoutFuel { get; set; }
        public decimal TotalCost { get; set; }
        public decimal OperationCostByKm { get; set; }
        public decimal FinanceCostByKm { get; set; }
        public decimal FuelCostByKm { get; set; }
        public decimal TotalCostByKm { get; set; }
        [NotMapped]
        public string CustomerName { get; set; }
        public string DecryptCustomerName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(CustomerName); } }


    }

    public class ConsolidatedReportParameters
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public int CustomerId { get; set; }
        public int VehicleId { get; set; }
    }
}