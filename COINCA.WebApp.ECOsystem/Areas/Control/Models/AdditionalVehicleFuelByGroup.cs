﻿/************************************************************************************************************
*  File    : AdditionalFuelDistributions.cs
*  Summary : AdditionalVehicleFuelByGroup Models
*  Author  : Berman Romero
*  Date    : 09/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;

using System.Collections.Generic;
using System.Globalization;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// AdditionalVehicleFuelByGroup Base
    /// </summary>
    public class AdditionalVehicleFuelByGroupBase
    {

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public AdditionalVehicleFuelByGroup Data { get; set; }

        /// <summary>
        /// Grid Info property displays the main information of the entity 
        /// </summary>
        public AdditionalVehicleFuelByGroupGrid GridInfo { get; set; }

        
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CostCenterId { get; set; }


        public int? KeyId { get; set; }


        public int? VehicleId { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

        public int? AmountAlertPorcentage { get; set; }

        public int? DriverId { get; set; }

    }


    /// <summary>
    /// AdditionalVehicleFuelByGroup Model
    /// </summary>
    public class AdditionalVehicleFuelByGroup : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Value Type
        /// </summary>
        public string ValueType { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public decimal? Value { get; set; }

        /// <summary>
        /// Value as string
        /// </summary>
        public string ValueStr { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Rewrite Values
        /// </summary>
        public bool Rewrite { get; set; }
        public int? CostCenterId { get; set; }


        public int? Id { get; set; }


        public int? VehicleId { get; set; }

        public int? DriverId { get; set; }

    }

    /// <summary>
    /// Vehicle Fuel By Group to Load Grid
    /// </summary>
    public class AdditionalVehicleFuelByGroupGrid
    {
        /// <summary>
        /// CreditCardEmissionType
        /// </summary>
        public int? CreditCardEmissionType { get; set; }

        /// <summary>
        /// Is Result
        /// </summary>
        public bool IsResult { get; set; }

        /// <summary>
        /// Validate if is all or only one Driver
        /// </summary>
        public int? DriverId { get; set; }

        public int? ErrorCode { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IList<AdditionalFuelDistribution> List { get; set; }
    }

    /// <summary>
    /// AdditionalVehicleFuelByGroup Model
    /// </summary>
    [GridTable(PagingEnabled = false, PageSize = 30)]
    public class AdditionalFuelDistribution : ModelAncestor
    {

        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);


        /// <summary>
        /// LiterPrice 
        /// </summary>

        public decimal LiterPrice { get; set; }


        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        public decimal? ConvertLiterPrice
        {
            get
            {
                if (ECOsystem.Utilities.Session.GetCustomerCapacityUnitId() == 1)
                {
                    return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, LiterPrice), 3);
                }
                else
                {
                    return LiterPrice;
                }
            }
            //set { Liters = Convert.ToDecimal(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, value)); }
        }

        /// <summary>
        /// PlateId of vehicle
        /// </summary>
        [GridColumn(Title = "", Width = "80px")]
        public bool Checked { get; set; }


        /// <summary>
        /// PlateId of vehicle
        /// </summary>
        [GridColumn(Title = "Matrícula", Width = "80px")]
        public string PlateId { get; set; }

        /// <summary>
        /// Vehicle Name
        /// </summary>
        [GridColumn(Title = "Vehículo", Width = "100px")]
        public string VehicleName { get; set; }

        /// <summary>
        /// Vehicle Model
        /// </summary>
        [GridColumn(Title = "Modelo", Width = "100px")]
        public string VehicleModel { get; set; }

        /// <summary>
        /// Fuel Name
        /// </summary>
        [GridColumn(Title = "Combustible", Width = "100px")]
        public string FuelName { get; set; }

        private decimal? _liters;

        /// <summary>
        /// Liters of fuel
        /// </summary>
        [NotMappedColumn]
        public decimal? Liters
        {
            get { return _liters; }
            set { _liters = value; }
        }

        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [GridColumn(Title = "Unidad de Capacidad", Width = "70px")]
        public decimal? CapacityUnitValue
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters), 3); }
            set { Liters = Convert.ToDecimal(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, value)); }
        }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string CapacityUnitValueStr
        {
            //get { return CapacityUnitValue == null ? "" : ((decimal)CapacityUnitValue).ToString("N3", CultureInfo.CreateSpecificCulture("es-CR")); }
            get { return CapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(CapacityUnitValue, 3); }

        }

        /// <summary>
        /// Credit Amount
        /// </summary>
        [NotMappedColumn]
        public decimal? Amount { get; set; }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string AmountStr
        {
            get { return Amount == null ? "" : ((decimal)Amount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string RealAmountStr
        {
            get { return RealAmount == null ? "" : ((decimal)RealAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public decimal? ConvertAmount
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, Amount), 3); }
        }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string ConvertAmountStr
        {
            get { return ConvertAmount == null ? "" : ((decimal)ConvertAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Amount Str as Currency
        /// </summary>
        [GridColumn(Title = "Monto Asignado", Width = "100px")]
        public string AmountStrCurrency
        {
            get { return CurrencySymbol + ConvertAmountStr; }
        }


        /// <summary>
        /// Credit Amount
        /// </summary>
        [NotMappedColumn]
        public decimal? AvailableFuel { get; set; }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string AvailableFuelStr
        {
            get { return AvailableFuel == null ? "" : ((decimal)AvailableFuel).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        private decimal? _Additionalliters;

        /// <summary>
        /// Liters of fuel
        /// </summary>
        [NotMappedColumn]
        public decimal? AdditionalLiters
        {
            get { return _Additionalliters; }
            set { _Additionalliters = value; }
        }

        private int litersAdditionalCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [GridColumn(Title = "Unidad de Capacidad", Width = "70px")]
        public decimal? AdditionalCapacityUnitValue
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersAdditionalCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), AdditionalLiters), 2); }
            set { AdditionalLiters = Convert.ToDecimal(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersAdditionalCapacityUnitId, value)); }
        }

        private decimal? _BalanceLiters;

        /// <summary>
        /// Liters of fuel
        /// </summary>
        [NotMappedColumn]
        public decimal? BalanceLiters
        {
            get { return _BalanceLiters; }
            set { _BalanceLiters = value; }
        }
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [GridColumn(Title = "Unidad de Capacidad", Width = "70px")]
        public decimal? AdditionalBalanceCapacityUnitValue
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersAdditionalCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), BalanceLiters), 3); }
            set { BalanceLiters = Convert.ToDecimal(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersAdditionalCapacityUnitId, value)); }
        }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string AdditionalBalanceCapacityUnitValueStr
        {
            //get { return AdditionalBalanceCapacityUnitValue == null ? "" : ((decimal)AdditionalBalanceCapacityUnitValue).ToString("N3", CultureInfo.CreateSpecificCulture("es-CR")); }
            get { return AdditionalBalanceCapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(AdditionalBalanceCapacityUnitValue, 3, "es-CR", true); }
        }


        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string AdditionalCapacityUnitValueStr
        {
            //get { return AdditionalCapacityUnitValue == null ? "" : ((decimal)AdditionalCapacityUnitValue).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
            get { return AdditionalCapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(AdditionalCapacityUnitValue, 3, "es-CR", true); }
        }

        /// <summary>
        /// Credit Amount
        /// </summary>
        [NotMappedColumn]
        public decimal? AdditionalAmount { get; set; }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string AdditionalAmountStr
        {
            get { return AdditionalAmount == null ? "" : ((decimal)AdditionalAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public decimal? ConvertAdditionalAmount
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, AdditionalAmount), 3); }
        }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string ConvertAdditionalAmountStr
        {
            get { return ConvertAdditionalAmount == null ? "" : ((decimal)ConvertAdditionalAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Amount Str as Currency
        /// </summary>
        [GridColumn(Title = "Monto Adicional Asignado", Width = "100px")]
        public string AdditionalAmountStrCurrency
        {
            get { return CurrencySymbol + ConvertAdditionalAmountStr; }
        }

        /// <summary>
        /// Credit Amount
        /// </summary>
        [NotMappedColumn]
        public decimal? BalanceAmount { get; set; }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string BalanceAmountStr
        {
            get { return BalanceAmount == null ? "" : ((decimal)BalanceAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public decimal? ConvertBalanceAmount
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, BalanceAmount), 3); }
        }

        /// <summary>
        /// Assigned of credit as string
        /// </summary>
        [NotMappedColumn]
        public string ConvertBalanceAmountStr
        {
            get { return ConvertBalanceAmount == null ? "" : ((decimal)ConvertBalanceAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Amount Str as Currency
        /// </summary>
        [GridColumn(Title = "Monto Adicional Asignado", Width = "100px")]
        public string BalanceAmountStrCurrency
        {
            get { return CurrencySymbol + ConvertBalanceAmountStr; }
        }

        /// <summary>
        /// Amount Str as Currency
        /// </summary>
        [GridColumn(Title = "Monto real gastado", Width = "100px")]
        public decimal? RealAmount { get; set; }

        /// <summary>
        /// Amount Str as Currency
        /// </summary>
        [GridColumn(Title = "Monto real gastado", Width = "100px")]
        public string RealAmountStrCurrency { get { return CurrencySymbol + RealAmountStr; }  }


        /// <summary>
        /// Amount Str as Currency
        /// </summary>
        [GridColumn(Title = "Litros gastados", Width = "100px")]
        public decimal RealLiters { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? AdditionalId { get; set; }


        /// <summary>
        /// FK Vehicle Id from Users, each vehicle is associate with one user
        /// </summary>
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Fuel Id
        /// </summary>
        [NotMappedColumn]
        public int? DefaultFuelId { get; set; }


        #region Driver Fuel related fields
        /// <summary>
        /// The PRIMARY KEY property of the DriverFuel, uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? AdditionalDriverFuelId { get; set; }
        /// <summary>
        /// FK Driver Id from Users
        /// </summary>
        [NotMappedColumn]
        public int? DriverId { get; set; }
        /// <summary>
        /// Code of the driver
        /// </summary>        
        [NotMappedColumn]
        public string EncryptedCode { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [GridColumn(Title = "Código", Width = "250px", SortEnabled = true)]
        public string DecryptedCode
        {
            get { return TripleDesEncryption.Decrypt(EncryptedCode); }
            set { EncryptedCode = TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// Name of the driver
        /// </summary>
        [NotMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [GridColumn(Title = "Nombre", Width = "250px", SortEnabled = true)]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Identification of the driver
        /// </summary>
        [NotMappedColumn]
        public string EncryptedIdentification { get; set; }

        /// <summary>
        /// Decrypted Identification
        /// </summary>
        [GridColumn(Title = "Identificación", Width = "250px", SortEnabled = true)]
        public string DecryptedIdentification
        {
            get { return TripleDesEncryption.Decrypt(EncryptedIdentification); }
            set { EncryptedIdentification = TripleDesEncryption.Encrypt(value); }
        }

        #endregion

        /// <summary>
        /// Result after insert
        /// </summary>
        [NotMappedColumn]
        public int Result { get; set; }

        /// <summary>
        /// Result after insert
        /// </summary>
        [NotMappedColumn]
        public string ResultStr {
            get
            {
                switch (Result)
                {
                    case 0:
                        return "Actualizado exitosamente.";
                    case -1:
                        return "Dato existente, Acción: No reescribir valor.";
                    case -100:
                        return "Registro recientemente incluido, No se tomó ninguna acción.";
                    case 50001:
                        return "Error: Falló por control de Versión";
                    case 50002:
                        return "Error: No hay disponible crédito";
                    case 50003:
                        return "Error: Monto no puede ser menor a monto utilizado";                    
                    case 101:
                        return string.Empty;//"No se tomó ninguna acción sobre este valor.";
                    case -99:
                        return "No se tomó ninguna acción sobre este valor. El monto digitado o los litros establecidos no pueden ser menores a los que existen actualmente ya que es un acumulado. Tiene que ser un monto mayor a " + AdditionalAmountStr + " y en litros mayor a " + AdditionalLiters;
                    default:
                        return "Operación Inválida, Número error: " + Result;
                }
            }
            set { }
        }

        /// <summary>
        /// Result after insert
        /// </summary>
        [NotMappedColumn]
        public string ResultIcon
        {
            get
            {
                switch (Result)
                {
                    case 0:
                        return "<span style='font-size:24px;' class='text-success mdi mdi-checkbox-marked-circle' data-toggle='tooltip' data-placement='right' title='Registro actualizado exitosamente.'></span>&nbsp;";
                    case -1:
                        return "<span style='font-size:24px;' class='text-warning mdi mdi-block-helper' data-toggle='tooltip' data-placement='right' title='Dato existente, Acción: No reescribir valor.'></span>&nbsp;";
                    case -100:
                        return "<span style='font-size:24px;' class='text-info mdi mdi-information-outline' data-toggle='tooltip' data-placement='right' title='Registro recientemente incluido, No se tomó ninguna acción.'></span>&nbsp;";
                    case 50001:
                        return "<span style='font-size:24px;' class='text-danger mdi mdi-close-circle' data-toggle='tooltip' data-placement='right' title='Error: Falló por control de Versión'></span>&nbsp;";
                    case 50002:
                        return "<span style='font-size:24px;' class='text-danger mdi mdi-close-circle' data-toggle='tooltip' data-placement='right' title='Error: No hay disponible crédito'></span>&nbsp;";
                    case 50003:
                        return "<span style='font-size:24px;' class='text-danger mdi mdi-close-circle' data-toggle='tooltip' data-placement='right' title='Error: Monto no puede ser menor a monto utilizado'></span>&nbsp;";                    
                    case 101:
                        return string.Empty;//"<span2 class='text-info mdi mdi-information-outline' data-toggle='tooltip' data-placement='right' title='No se tomó ninguna acción sobre este valor.'></span2>";
                    case -99:
                        return "<span style='font-size:24px;' class='text-danger mdi mdi-close-circle' data-toggle='tooltip' data-placement='right' title='No se tomó ninguna acción sobre este valor. El monto digitado o los litros establecidos no pueden ser menores a los que existen actualmente ya que es un acumulado. Tiene que ser un monto mayor a " + AdditionalAmountStr + " y en litros mayor a " + AdditionalLiters + "'></span>&nbsp;";
                    default:
                        return "<span style='font-size:24px;' class='text-danger mdi mdi-thumb-down-outline' data-toggle='tooltip' data-placement='right' title='Operación Inválida, Número error: " + Result + "'></span>&nbsp;";
                }
            }
            set { }
        }

        public bool? Selected { get; set; }
    }

}