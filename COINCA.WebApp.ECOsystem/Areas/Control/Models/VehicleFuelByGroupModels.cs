﻿/************************************************************************************************************
*  File    : FuelDistributions.cs
*  Summary : VehicleFuelByGroup Models
*  Author  : Berman Romero
*  Date    : 09/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Configuration;
using System.Globalization;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;

namespace  ECOsystem.Areas.Control.Models.Control
{
    public class DistributionParameters
    {
        public int? VehicleGroupId { get; set; }
        
        public int? CostCenterId { get; set; }
        
        public int? VehicleId { get; set; }

        public int? UserId { get; set; }

        public bool? IsAdditional { get; set; }

        public int IssueForId { get; set; }

        public bool? IsSum { get; set; }
        public int? FuelId { get; set; }
    }

    public class VehicleFuelByGroup : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Value Type
        /// </summary>
        public string ValueType { get; set; }

        /// <summary>
        /// PlateId
        /// </summary>
        public string PlateId { get; set; }


        /// <summary>
        /// Value
        /// </summary>
        public decimal? Value { get; set; }

        /// <summary>
        /// Value as string
        /// </summary>
        public string ValueStr { get; set; }        

        /// <summary>
        /// Currency Symbol
        /// </summary>
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Rewrite Values
        /// </summary>
        public bool Rewrite { get; set; }
        public int? CostCenterId { get; set; }


        public int? Id { get; set; }


        public int? VehicleId { get; set; }

        public int? DriverId { get; set; }

        public bool IsImport { get; set; }

        public bool IsLiters { get; set; }


    }
        
    public class FuelDistribution
    {
        public override string ToString()
        {
            return $"PlateId: {PlateId}, LiterPrice {LiterPrice}, AvailableLiters: {AvailableLiters}, Assigned:{Assigned}";
        }
        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);

        public string PaymentInstrumentName { get; set; }

        public int? Id { get; set; }

        public decimal LiterPrice { get; set; }

        public int FuelId { get; set; }

        public string PlateId { get; set; }
        
        public string VehicleName { get; set; }
        
        public string VehicleModel { get; set; }
        
        public string FuelName { get; set; }

        public decimal? Liters { get; set; }
        
        public decimal? CapacityUnitValue
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, Session.GetCustomerCapacityUnitId(), Liters), 3); }
            set { Liters = Convert.ToDecimal(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, value)); }
        }
        
        public string CapacityUnitValueStr { get { return CapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(CapacityUnitValue, 3, "es-CR", true); } }
        
        public decimal? Amount { get; set; }       
        
        public string AmountStr { get { return Amount == null ? "" : ((decimal)Amount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); } }
        
        public decimal? ConvertAmount { get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, Amount), 3); } }
        
        public string ConvertAmountStr { get { return ConvertAmount == null ? "" : ((decimal)ConvertAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); } }
        
        public string AmountStrCurrency { get { return CurrencySymbol + AmountStr; } }
        
        public string CurrencySymbol { get; set; }

        public int? DriverFuelId { get; set; }
        
        public int? VehicleId { get; set; }
        
        public int? DriverId { get; set; }

        public int? CostCenterId { get; set; }

        public string EncryptedCode { get; set; }
        
        public string DecryptedCode { get { return TripleDesEncryption.Decrypt(EncryptedCode); } set { EncryptedCode = TripleDesEncryption.Encrypt(value); } }

        public string EncryptedName { get; set; }
                
        public string DecryptedName { get { return TripleDesEncryption.Decrypt(EncryptedName); } set { EncryptedName = TripleDesEncryption.Encrypt(value); } }
                
        public string EncryptedIdentification { get; set; }
                
        public string DecryptedIdentification { get { return TripleDesEncryption.Decrypt(EncryptedIdentification); } set { EncryptedIdentification = TripleDesEncryption.Encrypt(value); } }
        
        public int? DefaultFuelId { get; set; }
        
        public int? Result { get; set; }
              
        public bool? Selected { get; set; }

        public string ResultStr
        {
            get
            {
                switch (Result)
                {
                    case null:
                        return string.Empty;
                    case 0:
                        return "<span class='text-result'>Actualizado exitosamente.</span>";                    
                    case 50002:
                        return "<span class='text-result'>Error: No hay disponible crédito</span>";
                    case 50003:
                        return "<span class='text-result'>Error: Monto no puede ser menor a monto utilizado.</span>";                    
                    case 50004:
                        return "<span class='text-result'>Error: El disponible no puede ser menor a cero. </span>";
                    case 50005://50004 no esta disponible 
                        return "Error: No puedes disminuir la cantidad ingresada porque es mayor al presupuesto adicional actual";
                    default:
                        return "<span class='text-result'>Operación Inválida, Número error: " + Result + " </span>";
                }
            }
        }

        public string ResultIcon
        {
            get
            {
                switch (Result)
                {
                    case null:
                        return string.Empty;
                    case 0:
                        return "<span style='font-size:24px;' class='text-success mdi mdi-checkbox-marked-circle' data-toggle='tooltip' data-placement='right' title='Registro actualizado exitosamente.'></span>&nbsp;";
                    case 50002:
                        return "<span style='font-size:24px;' class='text-danger mdi mdi-close-circle' data-toggle='tooltip' data-placement='right' title='Error: No hay disponible crédito'></span>&nbsp;";
                    case 50003:
                        return "<span style='font-size:24px;' class='text-danger mdi mdi-close-circle' data-toggle='tooltip' data-placement='right' title='Error: Monto no puede ser menor a monto utilizado'></span>&nbsp;";                    
                    case 50004:
                        return "<span style='font-size:24px;' class='text-danger mdi mdi-close-circle' data-toggle='tooltip' data-placement='right' title='No se tomó ninguna acción sobre este valor. El monto digitado o los litros establecidos no pueden ser menores a los que existen actualmente ya que es un acumulado. Tiene que ser un monto mayor a " + AmountStr + " y en litros mayor a " + Liters + "'></span>&nbsp;";
                    case 50005:
                        return "<span style='font-size:24px;' class='text-danger mdi mdi-close-circle' data-toggle='tooltip' data-placement='right' title='Error: No puedes disminuir la cantidad ingresada porque es mayor al presupuesto adicional actual'></span>&nbsp;";
                    default:
                        return "<span style='font-size:24px;' class='text-danger mdi mdi-thumb-down-outline' data-toggle='tooltip' data-placement='right' title='Operación Inválida, Número error: " + Result + "'></span>&nbsp;";
                }
            }
        }

        #region AdditionalProperties
        public decimal? AdditionalLiters { get; set; }

        public decimal? AdditionalCapacityUnitValue
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, Session.GetCustomerCapacityUnitId(), AdditionalLiters), 3); }
            set { AdditionalLiters = Convert.ToDecimal(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, value)); }
        }

        public string AdditionalCapacityUnitValueStr { get { return CapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(AdditionalCapacityUnitValue, 3, "es-CR", true); } }

        public decimal? AdditionalAmount { get; set; }

        public string AdditionalAmountStr { get { return AdditionalAmount == null ? "" : ((decimal)AdditionalAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); } }

        public decimal? AdditionalConvertAmount { get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, AdditionalAmount), 3); } }

        public string AdditionalConvertAmountStr { get { return AdditionalConvertAmount == null ? "" : ((decimal)AdditionalConvertAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); } }

        public string AdditionalAmountStrCurrency { get { return CurrencySymbol + AdditionalConvertAmountStr; } }

        public decimal? Available { get; set; }

        public string AvailableStr { get { return Available == null ? "" : ((decimal)Available).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); } }

        public decimal? Assigned { get; set; }

        public string AssignedStr { get { return Assigned == null ? "" : ((decimal)Assigned).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); } }

        public decimal AvailableLiters { get; set; }

        public decimal AvailableLitersFormat { get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, Session.GetCustomerCapacityUnitId(), AvailableLiters), 3); } }

        public decimal? RealAmount { get; set; }

        public string RealAmountStr { get { return RealAmount == null ? "0" : ((decimal)RealAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); } }

        public decimal? TotalAssigned { get; set; }

        public decimal? Total { get; set; }

        public string Symbol { get; set; }
        #endregion
    }

    public class FuelDistributionEdition
    {
        public int? Id { get; set; }

        public decimal? Amount { get; set; }

        public decimal? Liters { get; set; }

        public decimal? AdditionalAmount { get; set; }

        public decimal? AdditionalLiters { get; set; }

        public int VehicleId { get; set; }

        public int FuelId { get; set; }

        public string Symbol { get; set; }
    }

    public class VehicleFuelErrors
    {
        public string Item { get; set; }
    }

}