﻿using ECO_Control.Business.Control;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Control.Models.Control
{
    public class ApprovalTransactionModel
    {
        /// <summary>
        /// Search Parameters
        /// </summary>
        public ApprovalTransactionParametersModel Parameters { get; set; }
        /// <summary>
        /// Transactions Search Result
        /// </summary>
        public IList<ApprovalTransactionDisplayModel> Result { get; set; }
        /// <summary>
        /// Changed Transactions
        /// </summary>
        public IList<ApprovalTransactionDisplayModel> Changed 
        { 
            get {
                var changed = new List<ApprovalTransactionDisplayModel>();
                foreach(var item in Result) {
                    if (item.IsChanged) {
                        changed.Add(item);
                    }
                }
                return changed;
            } 
        }
        public IEnumerable<VehicleCostCenters> Vehicles { get; set; }
        public ApprovalTransactionModel() {
            Menus = new List<AccountMenus>();
            Parameters = new ApprovalTransactionParametersModel();
            using (var business = new VehiclesBusiness())
            {
                Vehicles = business.RetrieveVehicleCostCenters(null);
            }
        }

        /// <summary>
        /// List of Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }
}