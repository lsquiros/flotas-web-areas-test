﻿/************************************************************************************************************
*  File    : CurrentFuelsReportModels.cs
*  Summary : Real Vs Budget Fuels Report Models
*  Author  : Berman Romero
*  Date    : 21/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Real Vs Budget Fuels Report Base
    /// </summary>
    public class CurrentFuelsReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public CurrentFuelsReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<dynamic>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

    }


    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class CurrentFuelsReport
    {    
        /// <summary>
        /// AssignedLiters
        /// </summary>
        [NotMappedColumn]
        public decimal AssignedLiters { get; set; }

        /// <summary>
        /// RealLiters
        /// </summary>
        [NotMappedColumn]
        public decimal RealLiters { get; set; }

        /// <summary>
        /// Liters as String
        /// </summary>
        [DisplayName("Litros Calculados")]
        [NotMappedColumn]
        public string RealLitersStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(RealLiters, 3); }
        }

        /// <summary>
        /// Liters
        /// </summary>
        [DisplayName("Litros")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal Liters { get; set; }


        /// <summary>
        /// Liters as String
        /// </summary>
        [DisplayName("Litros")]
        [NotMappedColumn]
        public string LitersStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Liters, 3); }
        }

        /// <summary>
        /// AditionalAmount
        /// </summary>
        [NotMappedColumn]
        public decimal AditionalAmount { get; set; }

        /// <summary>
        /// AditionalLiters
        /// </summary>
        [NotMappedColumn]
        public decimal AditionalLiters { get; set; }

        /// <summary>
        /// Real Amount
        /// </summary>
        [DisplayName("Monto Consumo Real por Unidad")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal RealAmount { get; set; }

        /// <summary>
        /// Assigned Amount
        /// </summary>
        [DisplayName("Monto Asignado por Unidad")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal AssignedAmount { get; set; }

        /// <summary>
        /// Real Amount Pct
        /// </summary>
        [DisplayName("% Porcentaje")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal RealAmountPct { get; set; }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Moneda")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Assigned Amount as String
        /// </summary>
        [DisplayName("Monto Asignado por Unidad")]
        [ExcelMappedColumn("Monto Asignado por Unidad")]
        [NotMappedColumn]
        public string AssignedAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AssignedAmount, CurrencySymbol); }
        }

        /// <summary>
        /// Real Amount as String
        /// </summary>
        [DisplayName("Monto Consumo Real por Unidad")]
        [ExcelMappedColumn("Monto Consumo Real por Unidad")]
        [NotMappedColumn]
        public string RealAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(RealAmount, CurrencySymbol); }
        }

        /// <summary>
        /// Assigned Amount as String
        /// </summary>
        [DisplayName("% Porcentaje")]
        [ExcelMappedColumn("% Porcentaje")]
        [NotMappedColumn]
        public string RealAmountPctStr
        {
          //  get { return ECOsystem.Utilities.Miscellaneous.GetPercentageFormat(RealAmountPct); }
           get { return ECOsystem.Utilities.Miscellaneous.GetPercentageFormat(RealAmountPct,0); }
       
        }
                 
        /// <summary>
        /// additional Amount as String
        /// </summary>  
        [NotMappedColumn]
        public string AditionalAmountStr
        { 
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AditionalAmount, CurrencySymbol); }

        }
         
        /// <summary>
        /// TotalAmount as String
        /// </summary>  
        [NotMappedColumn]
        public string TotalAmountStr
        { 
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AditionalAmount + AssignedAmount, CurrencySymbol); }

        }
         
        /// <summary>
        /// AssignedLiters as String
        /// </summary>  
        [NotMappedColumn]
        public string AssignedLitersStr
        { 
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(AssignedLiters, 3); }

        } 

        /// <summary>
        /// AditionalLitersStr as String
        /// </summary>  
        [NotMappedColumn]
        public string AditionalLitersStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(AditionalLiters, 3); }

        }
         
        /// <summary>
        /// TotalLitersStr as String
        /// </summary>  
        [NotMappedColumn]
        public string TotalLitersStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(AssignedLiters + AditionalLiters, 3); }

        }

        public decimal AvailableBalance { get; set; }

        /// <summary>
        /// AvailableBalance as String
        /// </summary>  
        [NotMappedColumn]
        public string AvailableBalanceFormat
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AvailableBalance, CurrencySymbol); }

        }

        /// <summary>
        /// AvailableLiters
        /// </summary>  
        [NotMappedColumn]
        public decimal AvailableLiters { get; set; }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 15)]
    public class CurrentFuelsByVehicleSubUnitReport : CurrentFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Centro de Costo")]
        [NotMappedColumn]
        public string CostCenterName { get; set; }


        /// <summary>
        /// Id of Sub Unit
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int? CostCenterId { get; set; }

        /// <summary>
        /// List of the vehicles
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumnAttribute]
        public List<dynamic> VehiclesDetail { get; set; }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 15)]
    public class CurrentFuelsByVehicleGroupReport : CurrentFuelsReport
    {
        /// <summary>
        /// Id of Sub Unit
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Grupo de Vehículos")]
        [ExcelMappedColumn("Grupo de Vehículos")]
        [NotMappedColumn]
        public string VehicleGroupName { get; set; }

        /// <summary>
        /// List of the vehicles
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumnAttribute]
        public List<dynamic> VehiclesDetail { get; set; }

    }

    /// <summary>
    /// Current Fuels By Vehicle Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 15)]
    public class CurrentFuelsByVehicleUnitsReport : CurrentFuelsReport
    {
        /// <summary>
        /// Id of Sub Unit
        /// </summary>
        [NotMappedColumn]
        public int? UnitId { get; set; }

        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Vehicle Unit Name")]
        [ExcelMappedColumn("Nombre Unidad")]
        [NotMappedColumn]
        public string VehicleUnitName { get; set; }

        /// <summary>
        /// List of the vehicles
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumnAttribute]
        public List<dynamic> VehiclesDetail { get; set; }

    }

    /// <summary>
    /// Current Fuels By Vehicle Sub Unit Report
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 15)]
    public class CurrentFuelsByVehicle : CurrentFuelsReport
    {
        /// <summary>
        /// Name of Sub Unit
        /// </summary>
        [DisplayName("Matrícula")]
        [ExcelMappedColumn("Matrícula")]
        [NotMappedColumn]
        public string PlateNumber { get; set; }

        /// <summary>
        /// Name of vehicle
        /// </summary>
        [DisplayName("Nombre Vehículo")]
        [ExcelMappedColumn("Nombre Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }

        /// <summary>
        /// Id of Sub Unit
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int? CostCenterId { get; set; }

        /// <summary>
        /// FuelId
        /// </summary>
        [NotMappedColumn]
        public string FuelName { get; set; }

        /// <summary>
        /// Id of Sub Unit
        /// </summary>
        [NotMappedColumn]
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Id of Sub Unit
        /// </summary>
        [NotMappedColumn]
        public int? UnitId { get; set; }

        [NotMappedColumn]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(VehicleName); }
            set { VehicleName = TripleDesEncryption.Encrypt(value); }
        }

        public int CustomerCapacityUnit { get; set; }

    }
}