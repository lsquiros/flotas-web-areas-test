﻿/************************************************************************************************************
*  File    : CustomerCreditsModels.cs
*  Summary : CustomerCredits Models
*  Author  : Berman Romero
*  Date    : 09/15/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Globalization;
using System.Linq;
using ECOsystem.Models.Miscellaneous;

namespace  ECOsystem.Areas.Control.Models.Control
{
    /// <summary>
    /// Customer Credit Model for API
    /// </summary>
    public class CustomerCredits : ModelAncestor
    {

        /// <summary>
        /// Customer Credit Id
        /// </summary>
        public int CustomerCreditId { get; set; }

        /// <summary>
        /// CostCenterId
        /// </summary>
        public int CostCenterId { get; set; }

        /// <summary>
        /// CustomerCreditByCostCenterId
        /// </summary>
        public int CustomerCreditByCostCenterId { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// Month Name
        /// </summary>
        public string MonthName
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetMonthName(Month); }
        }

        /// <summary>
        /// Credit Amount
        /// </summary>
        public decimal CreditAmount { get; set; }

        /// <summary>
        /// Credit Amount
        /// </summary>
        public string CreditAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditAmount, CurrencySymbol); }
        }

        /// <summary>
        /// Credit Assigned
        /// </summary>
        public decimal CreditAssigned { get; set; }

        /// <summary>
        /// Credit Assigned
        /// </summary>
        public string CreditAssignedStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditAssigned, CurrencySymbol); }
        }

        /// <summary>
        /// Credit Assigned as %
        /// </summary>
        public decimal CreditAssignedPct
        {
            get
            {
                return ((CreditAssigned / CreditAmount) * 100);
            }
        }

        /// <summary>
        /// Credit Assigned Pct % as string
        /// </summary>
        public string CreditAssignedPctStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetPercentageFormat(CreditAssignedPct); }
        }

        /// <summary>
        /// Credit Available
        /// </summary>
        public decimal CreditAvailable { get; set; }

        /// <summary>
        /// Credit Available
        /// </summary>
        public string CreditAvailableStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditAvailable, CurrencySymbol); }
        }

        /// <summary>
        /// Credit Available
        /// </summary>
        public decimal CreditAvailablePct
        {
            get
            {
                return ((CreditAvailable / CreditAmount) * 100);
            }
        }

        /// <summary>
        /// Credit Available Pct % as string
        /// </summary>
        public string CreditAvailablePctStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetPercentageFormat(CreditAvailablePct); }
        }


        /// <summary>
        /// Credit Type Id
        /// </summary>
        public int CreditTypeId { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        public string CurrencySymbol { get; set; }

    }


    /// <summary>
    /// Customer Credit Model for API
    /// </summary>
    public class CustomerCreditApi : ApiAncestor
    {

        /// <summary>
        /// Customer Account Number
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// Credit Amount
        /// </summary>
        public decimal CreditAmount { get; set; }

        /// <summary>
        /// Credit Type Id
        /// </summary>
        public int CreditTypeId { get; set; }

    }
}