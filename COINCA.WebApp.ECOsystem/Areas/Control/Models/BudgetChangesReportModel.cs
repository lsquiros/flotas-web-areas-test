﻿using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;

namespace  ECOsystem.Areas.Control.Models.Control
{
    public class BudgetChangesReportBase
    {
        public BudgetChangesReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<BudgetChanges>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<BudgetChanges> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    public class BudgetChanges
    {
        [NotMapped]
        public string UserName { get; set; }

        public string UserNameDecrypted { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(UserName); } }

        public string EncryptedDriver
        {
            get { return ""; }
            set { VehicleName = ECOsystem.Utilities.TripleDesEncryption.Decrypt(value); }
        }

        [NotMapped]
        public string VehicleName { get; set; }

        public string VehicleNameDecrypted { get { return ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 100 ? ECOsystem.Utilities.TripleDesEncryption.Decrypt(VehicleName) : VehicleName; } }

        public string PlateId { get; set; }

        public decimal Liters { get; set; }

        public decimal Amount { get; set; }

        public decimal? AdditionalLiters { get; set; }

        public string AdditionalLitersStr { get { return AdditionalLiters == null ? "" : ((decimal)AdditionalLiters).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); } }

        public decimal? AdditionalAmount { get; set; }

        public string AdditionalAmountStr { get { return AdditionalAmount == null ? "" : ((decimal)AdditionalAmount).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); } }

        public string AmountStrCurrency { get; set; }

        public string AdditionalAmountStrCurrency { get; set; }

        [NotMapped]
        public string EncryptCustomerName { get; set; }

        public string DecryptCustomerName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptCustomerName); } }

        public string Dates { get; set; }

        public int? VehicleGroupId { get; set; }

        public string VehicleGroupName { get; set; }

        public int? CostCenterId { get; set; }

        public string CostCenterName { get; set; }

        public DateTime InsertDate { get; set; }

        public string DistributionType { get { return ECOsystem.Utilities.Session.GetCustomerCapacityUnitName(); } }
    }    
}