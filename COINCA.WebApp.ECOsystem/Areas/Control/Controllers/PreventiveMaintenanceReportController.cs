﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceController.cs
*  Summary : Preventive Maintenance Controller Actions
*  Author  : Napoleón Alvarado
*  Date    : 12/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System.IO;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Models.Identity;
using System;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Preventive Maintenance Report Class
    /// </summary>
    public class PreventiveMaintenanceReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new PreventiveMaintenanceReportBusiness())
                {
                    return View(business.RetrievePreventiveMaintenanceReport(new ControlPreventiveMaintenanceReportBase()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index(PreventiveMaintenanceReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new PreventiveMaintenanceReportBusiness())
                {
                    if (model.Parameters.FilterType == 0)
                        model.Parameters.FilterType = null;
                    Session["FilterType"] = model.Parameters.FilterType;
                    return View(business.RetrievePreventiveMaintenanceReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                ControlPreventiveMaintenanceReportBase model = new ControlPreventiveMaintenanceReportBase();
                model.FilterType = string.IsNullOrEmpty(p) ? (int?)null : Convert.ToInt32(p);

                byte[] report;
                using (var business = new PreventiveMaintenanceReportBusiness())
                {
                    report = business.GeneratePreventiveMaintenanceReportExcel(model);
                }
                return new FileStreamResult(new MemoryStream(report), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "ExcelReport.xlsx"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public ActionResult PrintReport(string p)
        {
            //var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(Miscellaneous.Base64Decode(p));
            ControlPreventiveMaintenanceReportBase model = new ControlPreventiveMaintenanceReportBase();

            ViewBag.PrintView = true;
            using (var business = new PreventiveMaintenanceReportBusiness())
            {
                return View("Index", business.RetrievePreventiveMaintenanceReport(model));
            }
        }


        /// <summary>
        /// Load Counties
        /// </summary>
        /// <param name="stateId">The state Id related to the Counties to be loaded</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[System.Web.Mvc.Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public JsonResult LoadMaintenance(int? FilterType)
        {
            try
            {
                if (FilterType == null)
                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = false }
                    };

                using (var business = new PreventiveMaintenanceReportBusiness())
                {
                    var listTypes = business.RetrieveTypesJSON(FilterType);
                    if (listTypes == null)
                    {
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { result = false }
                        };
                    }

                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = true, listTypes }
                    };
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

    }
}