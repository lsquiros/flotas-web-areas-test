﻿using  ECOsystem.Areas.Control.Business;
using  ECOsystem.Areas.Control.Models;
using  ECOsystem.Areas.Control.Models.Control;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace  ECOsystem.Areas.Control.Controllers
{
    public class TransactionsExportSapReportController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                return View(new ControlFuelsReportsBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetReportData(ControlFuelsReportsBase model)
        {
            try
            {
                using (var bus = new TransactionsExportSapReportBusiness())
                {
                    var data = bus.ReportDataRetrieve(model);
                    if (data.Count() > 0)
                    {
                        Session["Reporte"] = data;
                    }
                    else
                        return Json("NoData", JsonRequestBehavior.AllowGet);

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Genera información para el Archivo para SAP - Dos Pinos, parámetros: {0}", JsonConvert.SerializeObject(model)));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Generar información para el Archivo para SAP - Dos Pinos, Error: {0}", e.Message));
                return Json(new ErrorModels() { Title = "Reporte de Facturación", TechnicalError = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DownloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    var list = (List<TransactionsExportSapReport>)Session["Reporte"];
                    using (var ms = new MemoryStream())
                    {
                        var sw = new StreamWriter(ms, System.Text.Encoding.GetEncoding(1252));

                        //Get the titles
                        sw.WriteLine("NumEquipo, Fecha, Hora, Responsable, Estación, Litros, Ruta, Kilómetros");
                        sw.Flush();

                        foreach (var item in list)
                        {
                            sw.WriteLine(item.VehicleSAP + "," + item.DateStr + "," + item.TimeStr + "," + item.ResponsableDecrypt + "," + item.ServiceStation + "," + item.Liters + "," + item.Route + "," + item.Odometer);
                            sw.Flush();
                        }
                        return new FileStreamResult(new MemoryStream(ms.ToArray()), "text/plain")
                        {
                            FileDownloadName = "Archivo para SAP.csv"
                        };
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}