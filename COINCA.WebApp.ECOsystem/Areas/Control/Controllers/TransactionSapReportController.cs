﻿using System.IO;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Models.Identity;
using System.Data;
using System;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using ECOsystem.Models.Miscellaneous;




namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// TransactionSapReportController
    /// </summary>
    public class TransactionSapReportController : Controller
    {
        /// <summary>
        /// Index 
        /// </summary>
        /// <returns></returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new TransactionSapReportBusiness())
                {
                    return View(business.RetrieveTransactionsSAPReport(new ControlFuelsReportsBase()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Load the transactions data based on the selection
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(TransactionsConsolideReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new TransactionSapReportBusiness())
                {
                    return View(business.RetrieveTransactionsSAPReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Set the data ready to download
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

                if (model != null)
                {
                    //model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                    model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
                }

                DataTable report = new DataTable();

                using (var business = new TransactionSapReportBusiness())
                {
                    report = business.GetReportDownloadData(model);
                    Session["Reporte"] = report;
                    using (var bus = new TransactionSapReportBusiness())
                    {
                        var modelr = bus.RetrieveTransactionsSAPReport(model);
                        modelr.Parameters = new ControlFuelsReportsBase();
                        return View("Index", modelr);
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Consolidado SAP");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "TransactionsSAPReport", this.ToString().Split('.')[2], "Reporte Consolidado SAP");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                using (var business = new TransactionSapReportBusiness())
                {
                    return View("Index", business.RetrieveTransactionsSAPReport(new ControlFuelsReportsBase()));
                }
            }
        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>        
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new TransactionSapReportBusiness())
            {
                return View("Index", business.RetrieveTransactionsSAPReport(model));
            }
        }

    }
}