﻿/*************************************************************************************************
*  File    : PreventiveMaintenanceByVehicleController.cs
*  Summary : Preventive Maintenance By Vehicle Controller Methods
*  Author  : Cristian Martínez
*  Date    : 08/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Control.Models.Identity;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Alarm By Odometer Controller
    /// </summary>
    public class AlarmByOdometerController : Controller
    {
        /// <summary>
        /// Alarm By Odometer Main View
        /// </summary>
        /// <returns></returns>
        //[Authorize(Roles = "CUSTOMER_CHANGEODOMETER")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new AlarmByOdometerBusiness())
                {
                    var model = new AlarmByOdometerBase
                    {
                        Data = new AlarmByOdometer(),
                        List = business.RetrieveAlarmByOdometer(null)
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Search Alarm By Odometer
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_CHANGEODOMETER")]
        public PartialViewResult SearchAlarmByOdometer(string key)
        {
            try
            {
                using (var business = new AlarmByOdometerBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveAlarmByOdometer(null, key));
                }
            }
            catch (Exception e)
            {

                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "AlarmByOdometer")
                });
            }
        }

        /// <summary>
        /// Load Alarm By Odometer
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_CHANGEODOMETER")]
        public PartialViewResult LoadAlarmByOdometer(int id)
        {
            try
            {
                using (var business = new AlarmByOdometerBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveAlarmByOdometer(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "AlarmByOdometer")
                });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Alarm By Odometers
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_CHANGEODOMETER")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditAlarmByOdometer(AlarmByOdometer model)
        {
            try
            {
                using (var business = new AlarmByOdometerBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.DisabledAlarmByOdometer(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveAlarmByOdometer(null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "AlarmByOdometer")
                });
            }
        }
    }
}