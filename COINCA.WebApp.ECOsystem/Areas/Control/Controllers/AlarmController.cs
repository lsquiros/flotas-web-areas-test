﻿using System;
/************************************************************************************************************
*  File    : AlarmController.cs
*  Summary : Alarm Controller Actions
*  Author  : Cristian Martínez 
*  Date    : 17/Nov/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using ECOsystem.Models.Identity;
using ECOsystem.Models.Account;
using ECOsystem.Business.Core;
using ECOsystem.Models.Core;

namespace ECO_Control.Controllers.Control
{
    /// <summary>
    /// Alarm Controller. Implementation of all action results from views
    /// </summary>
    public class AlarmController : Controller
    {
        /// <summary>
        /// Adds a new alarm to the database and return empty
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //[EcoAuthorize]
        //[HttpPost]
        //public PartialViewResult AddOrEditAlarm(AlarmsModels model) 
        //{
        //    try{
        //        using (var business = new AlarmBusiness()) {
                    
        //            model.Phone = (model.Phone == null) ? "0000-0000" : model.Phone;
        //            model.CustomerId = Convert.ToInt32(ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId); 

        //            if (ModelState.IsValid)
        //            {
        //                //business.AddOrEditAlarm(model);
        //            }
        //        }
        //        return null; 
        //    }
        //    catch (Exception e)
        //    {
        //        var user = ECOsystem.Utilities.Session.GetUserInfo();

        //        //Add log event
        //        AddLogEvent(
        //                exception: e,
        //                userId: (user != null) ? user.UserId : 0,
        //                customerId: (user != null) ? user.CustomerId : 0,
        //                partnerId: (user != null) ? user.PartnerId : 0,
        //                message: string.Empty,
        //                isError: true);

        //        return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
        //        {
        //            TechnicalError = e.Message + e.StackTrace,
        //            ReturnUlr = Url.Action("AlarmsList", "Alarm")
        //        });
        //    }
        //}

        /// <summary>
        /// Adds a new alarm to the database and return the grid
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditAlarms(AlarmsModels model)
        {
            try
            {
                using (var business = new AlarmBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditAlarm(model);
                    }
                    IEnumerable<AlarmsModels> alarms = business.RetrieveAlarm(null, null, null, null, null);
                    return PartialView("~/Views/Alarm/_partials/_Grid.cshtml", alarms);
                }
               
            }
            catch (Exception e)
            {

                var user = ECOsystem.Utilities.Session.GetUserInfo();

                //Add log event
                AddLogEvent(
                        exception: e,
                        userId: (user != null) ? user.UserId : 0,
                        customerId: (user != null) ? user.CustomerId : 0,
                        partnerId: (user != null) ? user.PartnerId : 0,
                        message: string.Empty,
                        isError: true);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("AlarmsList", "Alarm")
                });
            }
        }

        /// <summary>
        /// Loads the alarm
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alarmTriggerId"></param>
        /// <param name="entityTypeId"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        //[HttpPost]
        ////[EcoAuthorize]
        //public PartialViewResult LoadAlarm(int id, int alarmTriggerId, int entityTypeId, string title) 
        //{
        //    try
        //    {
        //        using (var business = new AlarmBusiness())
        //        {
        //            AlarmsModels alarm = business.RetrieveFirstAlarm(alarmTriggerId, ECOsystem.Utilities.Session.GetCustomerId(), entityTypeId, null, id);
        //            alarm.TitlePopUp = title;
        //            return PartialView("~/Views/Alarm/Index.cshtml", alarm);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        var user = ECOsystem.Utilities.Session.GetUserInfo();

        //        //Add log event
        //        AddLogEvent(
        //                exception: e,
        //                userId: (user != null) ? user.UserId : 0,
        //                customerId: (user != null) ? user.CustomerId : 0,
        //                partnerId: (user != null) ? user.PartnerId : 0,
        //                message: string.Empty,
        //                isError: true);

        //        return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
        //        {
        //            TechnicalError = e.Message + e.StackTrace,
        //            ReturnUlr = Url.Action("Index", "Drivers")
        //        });
        //    }
        //}

        /// <summary>
        /// Loads the detail alarms
        /// </summary>
        [HttpPost]
        //[EcoAuthorize]
        public PartialViewResult LoadAlarms(int id) 
        {
            try
            {
                using (var business = new AlarmBusiness())
                {
                    AlarmsModels alarm = business.RetrieveAlarm(null, null, null, id, null).ToList().FirstOrDefault();
                
                    return PartialView("~/Views/Alarm/_partials/_Detail.cshtml", alarm);
                }
            }
            catch (Exception e)
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                //Add log event
                AddLogEvent(
                        exception: e,
                        userId: (user != null) ? user.UserId : 0,
                        customerId: (user != null) ? user.CustomerId : 0,
                        partnerId: (user != null) ? user.PartnerId : 0,
                        message: string.Empty,
                        isError: true);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("AlarmsList", "Alarm")
                });
            }
        }
        
        /// <summary>
        /// Returns the alarms list view, change to Admin from AlarmList
        /// </summary>
        [EcoAuthorize]
        public ActionResult Admin()
        {
            using (var business = new AlarmBusiness())
            {
                IEnumerable<AlarmsModels> alarms = business.RetrieveAlarm(null, ECOsystem.Utilities.Session.GetCustomerId(), null, null, null);
                AlarmsModels alarm = new AlarmsModels();
                alarm.List = alarms;
                alarm.Menus = new List<AccountMenus>();

                return View("~/Views/Alarm/Admin.cshtml", alarm);
            }          
        }
        
        /// <summary>
        /// Returns the alarms list view
        /// </summary>
        public ActionResult AlarmsByVehicles()
        {
            using (var business = new AlarmBusiness())
            {
                List<AlarmsModels> alarms = new List<AlarmsModels>();
              
                AlarmsModels alarmModel = new AlarmsModels();
                alarmModel.List = alarms;

                return View("~/Views/Alarm/AlarmsByVehicles.cshtml", alarmModel);
            }
        }

        /// <summary>
        /// Returns the alarms list view
        /// </summary>
        public PartialViewResult ReturnAlarmsByVehicles(string StartDateStr,string EndDateStr)
        {
            
            using (var business = new AlarmBusiness())
            {
                if (!StartDateStr.Trim().Equals("") && !EndDateStr.Trim().Equals(""))
                {
                    DateTime startDateTime = DateTime.ParseExact(StartDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime endDateTime = DateTime.ParseExact(EndDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    return PartialView("~/Views/Alarm/_partials/_GridAlarmsByVehicles.cshtml", business.RetrieveAlarmsByVehicles(null, ECOsystem.Utilities.Session.GetCustomerId(), null, null, null, startDateTime, endDateTime));
                }
                else {
                    return null;
                }
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Alarms
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteAlarms(int id)
        {
            try
            {
                using (var business = new AlarmBusiness())
                {
                    business.DeleteAlarms(id);
                 
                    return PartialView("~/Views/Alarm/_partials/_Grid.cshtml", business.RetrieveAlarm(null, null, null, null, null));
                }
            }

            catch (Exception e)
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                //Add log event
                AddLogEvent(
                        exception: e,
                        userId: (user != null)? user.UserId : 0,
                        customerId: (user != null) ? user.CustomerId : 0,
                        partnerId: (user != null) ? user.PartnerId : 0,
                        message: string.Empty,
                        isError: true);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("AlarmsList", "Alarm")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("AlarmsList", "Alarm")
                    });
                }
            }

        }

        /// <summary>
        /// Prints the report
        /// </summary>
        public ActionResult PrintReport(string StartDateStr, string EndDateStr)
        {
                     
            using (var business = new AlarmBusiness())
            {
                DateTime startDateTime = DateTime.ParseExact(StartDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime endDateTime = DateTime.ParseExact(EndDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                IEnumerable<AlarmsModels> alarms = business.RetrieveAlarmsByVehicles(null, null, null, null, null, startDateTime, endDateTime);
                ViewBag.PrintView = true;
                AlarmsModels alarmModel = new AlarmsModels();
                alarmModel.List = alarms;

                return View("~/Views/Alarm/AlarmsByVehicles.cshtml", alarmModel);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        public ActionResult ExcelReportDownload(string StartDateStr, string EndDateStr)
        {
            try
            {
                byte[] report = new byte[10];
                using (var business = new AlarmBusiness())
                {
                    IEnumerable<AlarmsModels> alarms = new List<AlarmsModels>();
                    if (StartDateStr != string.Empty && EndDateStr != string.Empty)
                    {
                        DateTime startDateTime = DateTime.ParseExact(StartDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime endDateTime = DateTime.ParseExact(EndDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        alarms = business.RetrieveAlarmsByVehicles(null, null, null, null, null, startDateTime, endDateTime);
                    }
                    using (var excel = new ExcelReport())
                    {
                        report = excel.CreateSpreadsheetWorkbook("Consumo de Combustible Real del Período", alarms.ToList());
                    }
                }
                return new FileStreamResult(new MemoryStream(report), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "ExcelReport.xlsx"
                };
            }
            catch (Exception e)
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                //Add log event
                AddLogEvent(
                        exception:e,
                        userId: (user != null)? user.UserId : 0,
                        customerId: (user != null) ? user.CustomerId : 0,
                        partnerId: (user != null) ? user.PartnerId : 0,
                        message: string.Empty,
                        isError: true);

                return null;
            }
            
        } 
       

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}