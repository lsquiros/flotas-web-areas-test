﻿/************************************************************************************************************
*  File    : TransactionsReportController.cs
*  Summary : Transactions Report Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 12/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Models.Identity;
using System.Data;
using System;
using System.Globalization;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;




namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Transactions Report Class
    /// </summary>
    public class TransactionsReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new TransactionsReportBusiness())
                {
                    return View(business.RetrieveTransactionsReport(new ControlFuelsReportsBase()
                    {
                        ReportCriteriaId = (int)ReportCriteria.DateRange,
                        StartDate = DateTime.Today,
                        EndDate = DateTime.Today.AddDays(1).AddTicks(-1),
                        TransactionType = 1,
                        Month = null,
                        Year = null
                    }));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(TransactionsReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new TransactionsReportBusiness())
                {
                    if (model.Parameters.StartDate != null && model.Parameters.EndDate != null)
                    {
                        model.Parameters.Year = null;
                        model.Parameters.Month = null;
                    }
                    return View(business.RetrieveTransactionsReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult Admin()
        {
            try
            {
                using (var business = new TransactionsReportBusiness())
                {
                    return View(business.RetrieveTransactionsReportDeniedByPartner(new ControlFuelsReportsBase()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Admin(TransactionsReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Admin");

                using (var business = new TransactionsReportBusiness())
                {
                    return View(business.RetrieveTransactionsReportDeniedByPartner(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Load Users
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="trace"></param>
        /// <param name="filterType"></param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult LoadTrace(int? id, string trace, int filterType)
        {
            try
            {
                TransportDataModel traceModel;
                if (id != null)
                {
                    using (var business = new TransactionsReportBusiness())
                    {
                        var model = business.RetrieveTrace(id, filterType);

                        if (model.TransportData == null)
                        {
                            traceModel = new TransportDataModel();
                            traceModel.terminalId = model.TerminalId;
                            traceModel.responseCode = model.ResponseCode;
                            traceModel.responseDescription = model.ResponseCodeDescription;
                            traceModel.carTag = model.PlateId;
                            traceModel.units = model.Liters;
                            traceModel.amount = model.RealAmount;
                            traceModel.FilterType = filterType;

                            if (string.IsNullOrEmpty(model.CreditCardHolder))
                            {
                                traceModel.expirationDate = "";
                            }
                            else
                            {
                                var arr = model.CreditCardHolder.ToCharArray();
                                traceModel.expirationDate = arr.Length == 4 ? String.Format("{0}{1}/{2}{3}", arr[2], arr[3], arr[0], arr[1]) : "";
                            }
                        }
                        else
                        {
                            traceModel = model.TransportDataObject;
                            if (!string.IsNullOrEmpty(traceModel.expirationDate))
                            {
                                var arr = traceModel.expirationDate.ToCharArray();
                                traceModel.expirationDate = arr.Length == 4 ? String.Format("{0}{1}/{2}{3}", arr[2], arr[3], arr[0], arr[1]) : "";
                            }
                            traceModel.responseDescription = model.ResponseCodeDescription;
                            traceModel.responseCode = model.ResponseCode;
                            traceModel.FilterType = filterType;
                        }
                    }
                }
                else
                {
                    traceModel = JsonConvert.DeserializeObject<TransportDataModel>(trace);
                }
                return PartialView("_partials/_DetailTrace", traceModel);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CustomerUsers")
                });
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

                //model.StartDate = (model.StartDate != null)? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                //model.EndDate = (model.EndDate != null)? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;

                //model.StartDate = model.StartDate;
                model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;

                //If the download comes from main index, set the dates right
                if (model.StartDate > model.EndDate)
                {
                    model.StartDate = DateTime.Today;
                    model.EndDate = DateTime.Today.AddDays(1).AddTicks(-1);
                }

                if (model.Month != null && Convert.ToInt32(model.Month) > 0 && model.Year != null && Convert.ToInt32(model.Year) > 0)
                {
                    if (model.StartDate == null)
                    {
                        model.StartDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1, 0, 0, 0);
                    }

                    if (model.EndDate == null)
                    {
                        model.EndDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month)), 23, 59, 59);
                    }
                }
                DataTable report = new DataTable();

                using (var business = new TransactionsReportBusiness())
                {
                    report = business.GenerateDeniedReportData(model);

                    Session["Reporte"] = report;
                    Session["TransactionType"] = model.TransactionType;

                    if (model.StartDate == null)
                    {
                        int lastDay = DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month));
                        string MonthName = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1).ToString("MMM", CultureInfo.InvariantCulture);
                        Session["StartDate"] = MonthName + 1 + " /" + model.Year;
                        Session["EndDate"] = MonthName + lastDay + " /" + model.Year;
                    }
                    else
                    {
                        DateTime fecha_inicial = Convert.ToDateTime(model.StartDate);
                        int day_inicial = fecha_inicial.Day;
                        string MonthNameInicial = new DateTime(fecha_inicial.Year, fecha_inicial.Month, day_inicial).ToString("MMM", CultureInfo.InvariantCulture);
                        Session["StartDate"] = MonthNameInicial + day_inicial + " /" + fecha_inicial.Year;
                        DateTime fecha_final = Convert.ToDateTime(model.EndDate);
                        int day_final = fecha_final.Day;
                        string MonthNameFinal = new DateTime(fecha_final.Year, fecha_final.Month, day_final).ToString("MMM", CultureInfo.InvariantCulture);
                        Session["EndDate"] = MonthNameFinal + day_final + " /" + fecha_final.Year;
                    }


                    if (ECOsystem.Utilities.Session.GetCustomerId() != null)
                        return View("Index", business.RetrieveTransactionsReport(model));

                    return View("Admin", business.RetrieveTransactionsReportDeniedByPartner(model));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                string reportName = string.Empty;
                string downloadName = string.Empty;
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                if (Convert.ToUInt32(Session["TransactionType"]) == 6)
                {
                    reportName = "DeniedTransactionsReport";
                    downloadName = "Reporte de Transacciones Denegadas";
                }
                else if (Convert.ToUInt32(Session["TransactionType"]) == 3)
                {
                    reportName = "ReversedTransactionsReport";
                    downloadName = "Reporte de Transacciones Reversadas";
                }
                else
                {
                    reportName = "TransactionsReport";
                    downloadName = "Reporte de Transacciones Procesadas";
                }
                Session["TransactionType"] = null;
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de " + downloadName);
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report).Replace('|', '-'), reportName, this.ToString().Split('.')[2], downloadName);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                using (var business = new TransactionsReportBusiness())
                {
                    return View("Index", business.RetrieveTransactionsReport(new ControlFuelsReportsBase()));
                }
            }
        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new TransactionsReportBusiness())
            {
                return View("Index", business.RetrieveTransactionsReport(model));
            }
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}

