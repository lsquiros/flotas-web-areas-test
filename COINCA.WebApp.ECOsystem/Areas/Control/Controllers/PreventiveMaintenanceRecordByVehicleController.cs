﻿/*************************************************************************************************
*  File    : PreventiveMaintenanceRecordByVehicleController.cs
*  Summary : Preventive Maintenance Record By Vehicle Controller Methods
*  Author  : Danilo Hidalgo
*  Date    : 01/07/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Control.Models.Identity;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Preventive Maintenance Record By Vehicle Controller
    /// </summary>
    public class PreventiveMaintenanceRecordByVehicleController : Controller
    {
        /// <summary>
        /// Preventive Maintenance By Vehicule Main View
        /// </summary>
        /// <returns></returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new PreventiveMaintenanceRecordByVehicleBusiness())
                {
                    var model = new PreventiveMaintenanceVehiclesBase
                    {
                        Data = new PreventiveMaintenanceVehicles(),
                        List = business.RetrievePreventiveMaintenanceVehicles(customerId: ECOsystem.Utilities.Session.GetCustomerId(), preventiveMaintenanceCatalogId: 0),
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Search Preventive Maintenance By Type
        /// </summary>
        /// <param name="idSearch">Id of Preventive Maintenance Catalog</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchPreventiveMaintenanceRecordByVehicle(int idSearch, string key)
        {
            try
            {
                using (var business = new PreventiveMaintenanceRecordByVehicleBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrievePreventiveMaintenanceVehicles(customerId: ECOsystem.Utilities.Session.GetCustomerId(), preventiveMaintenanceCatalogId: idSearch, preventiveMaintenanceId: null, key: key));
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("Index", "PreventiveMaintenanceRecordByVehicle")
                });
            }
        }

        /// <summary>
        ///  Performs the maintenance Insert or Update of the entity Preventive Maintenance By Vehicle
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditPreventiveMaintenanceRecordByVehicle(PreventiveMaintenanceRecordByVehicle model)
        {
            try
            {
                using (var business = new PreventiveMaintenanceRecordByVehicleBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditPreventiveMaintenanceRecordByVehicle(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrievePreventiveMaintenanceVehicles(customerId: ECOsystem.Utilities.Session.GetCustomerId(), preventiveMaintenanceCatalogId: model.PreventiveMaintenanceCatalogIdSelect));
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("Index", "PreventiveMaintenanceRecordByVehicle")
                });
            }
        }

        /// <summary>
        /// Load PreventiveMaintenanceRecordByVehicle
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public ActionResult LoadPreventiveMaintenanceRecordByVehicle(int id, int idLoad)
        {
            try
            {
                using (var business = new PreventiveMaintenanceRecordByVehicleBusiness())
                {
                    var model = new PreventiveMaintenanceRecordByVehicleBase
                    {
                        Data = new PreventiveMaintenanceRecordByVehicle(),
                        List = business.RetrievePreventiveMaintenanceRecordByVehicle(customerId: ECOsystem.Utilities.Session.GetCustomerId(), preventiveMaintenanceCatalogId: idLoad, vehicleId: id),
                    };
                    return RedirectToAction("IndexDetail", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Load PreventiveMaintenanceRecordByVehicle
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        // [Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadPreventiveMaintenanceRecordByVehicleDetail(int id)
        {
            try
            {
                using (var business = new PreventiveMaintenanceRecordByVehicleBusiness())
                {
                    var model = new PreventiveMaintenanceRecordByVehicleBase
                    {
                        Data = new PreventiveMaintenanceRecordByVehicle(),
                        Detail = business.PreventiveMaintenanceRecordByVehicleDetail(id),
                    };
                    ViewBag.Symbol = ECOsystem.Utilities.Session.GetCustomerInfo().CurrencySymbol;
                    return PartialView("_partials/_Detail", model);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("IndexDetail", "PreventiveMaintenanceRecordByVehicle")
                });
            }

        }


        /// <summary>
        /// DeleteRecord
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public JsonResult DeleteRecord(string id)
        {
            try
            {
                using (var business = new PreventiveMaintenanceRecordByVehicleBusiness())
                {
                    business.DeletePreventiveMaintenanceRecordByVehicleDetail(Convert.ToInt16(id));
                }
                return Json(new { Result = "Ready" });
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json(new { Result = "Error" });
            }
        }


        /// <summary>
        /// SaveCostList
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPost]
        // [Authorize(Roles = "CUSTOMER_ADMIN")]
        public JsonResult SaveRecordList(List<string> list, int PreventiveMaintenanceRecordByVehicleId, int PreventiveMaintenanceId)
        {
            try
            {
                using (var business = new PreventiveMaintenanceRecordByVehicleBusiness())
                {
                    business.AddOrEditPreventiveMaintenanceRecordByVehicleDetail(list, PreventiveMaintenanceRecordByVehicleId, PreventiveMaintenanceId);
                }
                return Json(new { Result = "Ready" });
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json(new { Result = "Error" });
            }
        }

    }
}