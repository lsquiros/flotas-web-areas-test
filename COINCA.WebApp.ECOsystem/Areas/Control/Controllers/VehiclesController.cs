﻿/************************************************************************************************************
*  File    : VehiclesController.cs
*  Summary : Vehicles Controller Actions
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECO_Control.Business.Control;
using ECO_Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Business.Utilities;
using ECO_Control.Models.Identity;

namespace ECO_Control.Controllers.Control
{
    /// <summary>
    /// Vehicles Controller. Implementation of all action results from views
    /// </summary>
    public class VehiclesController : Controller
    {

        /// <summary>
        /// Vehicles Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new VehiclesBusiness())
            {
                var model = new VehiclesBase
                {
                    Data = new Vehicles(),
                    List = business.RetrieveVehicles(null)
                };
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
       //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehicles(Vehicles model)
        {
            try
            {
                using (var business = new VehiclesBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditVehicles(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null));
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                if (e.Message.Contains("IMEI_NO_CORRESPONDE"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Error al Asignar Número de Vehículo Intrack",
                        TechnicalError = "El Imei no corresponde al número de Vehículo Intrack digitado.",
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                }
                else if (e.Message.Contains("PLATE_IS_EXIST"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Validación de Placa",
                        TechnicalError = "La Placa utilizada para éste vehículo ya se encuentra registrada.",
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                    
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                }
            }
        }

        /// <summary>
        /// Performs the References Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult EditVehiclesReferences(VehiclesReferences model)
        {
            try
            {
                using (var business = new VehiclesBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.EditVehiclesReferences(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null));
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Vehicles")
                });
            }
        }

        /// <summary>
        /// Search Vehicles
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchVehicles(string key)
        {
            try
            {
                using (var business = new VehiclesBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null, key));
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Vehicles")
                });
            }
        }

        /// <summary>
        /// Load Vehicles
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadVehicles(int id)
        {
            try
            {
                if (id < 0)
                {
                    Vehicles v = new Vehicles();
                    v.FreightTemperature = 0;
                    v.FreightTemperatureThreshold1 = 0;
                    v.FreightTemperatureThreshold2 = 0;
                    v.AdministrativeSpeedLimit = 0;

                    return PartialView("_partials/_Detail", v);
                }
                else 
                {
                    using (var business = new VehiclesBusiness())
                    {
                        return PartialView("_partials/_Detail", business.RetrieveVehicles(id).FirstOrDefault());
                    }
                } 
                
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Vehicles")
                });
            }
        }

        /// <summary>
        /// Load VehiclesReferences
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult LoadVehiclesReferences(int id)
        {
            try
            {
                using (var business = new VehiclesBusiness())
                {
                    return PartialView("_partials/_DetailReferences", business.RetrieveVehiclesReferences(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Vehicles")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Vehicles
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult DeleteVehicles(int id)
        {
            try
            {
                using (var business = new VehiclesBusiness())
                {
                    business.DeleteVehicles(id);
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null));
                }
            }


            catch (Exception e)
            {
                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                }
            }
            
        }

    }
}