﻿/************************************************************************************************************
*  File    : FuelsByCreditController.cs
*  Summary : FuelsByCredit Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;

using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Fuels By Credit Controller
    /// </summary>
    public class FuelsByCreditController : Controller
    {
        /// <summary>
        /// FuelsByCredit Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new FuelsByCreditBusiness())
                {
                    var customer = ECOsystem.Utilities.Session.GetCustomerInfo();
                    if (customer.CostCenterFuelDistribution)
                    {
                        var result = new FuelsByCreditBase();
                        result.Menus = new List<AccountMenus>();
                        return View(result);
                    }
                    else
                    {
                        return View(business.RetrieveFuelsByCredit());
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadFuelsByCreditByCostCenter(int? CostCenterId)
        {
            try
            {
                using (var business = new FuelsByCreditBusiness())
                {
                    var customer = ECOsystem.Utilities.Session.GetCustomerInfo();
                    return PartialView("_partials/_FuelsByCreditPanel", business.RetrieveFuelsByCreditByCostCenter(customer.CustomerId, CostCenterId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// VehicleFuel Main View
        /// </summary>
        /// <param name="data">List of Fuels By Credit</param>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [ValidateInput(false)]
        //[EcoAuthorize]
        public ActionResult AddOrEditFuelsByCredit(IList<FuelsByCreditModel> data)
        {
            try
            {
                if (data == null || data.Count == 0) RedirectToAction("Index");

                using (var business = new FuelsByCreditBusiness())
                {
                    if (ECOsystem.Utilities.Session.GetCustomerInfo().CostCenterFuelDistribution)
                    {
                        business.AddOrEditFuelsByCreditByCostCenter(data);
                    }
                    else
                    {
                        business.AddOrEditFuelsByCredit(data);
                    }
                    
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetChanges()
        {
            return Json("");
        }
    }
}