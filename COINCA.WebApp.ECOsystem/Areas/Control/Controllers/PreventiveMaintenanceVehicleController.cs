﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceVehicleController.cs
*  Summary : PreventiveMaintenanceVehicle Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 02/20/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Control.Models.Identity;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    public class PreventiveMaintenanceVehicleController : Controller
    {
        /// <summary>
        /// PreventiveMaintenanceVehicle Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //public ActionResult Assign(int id, string PlateId)
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                var model = new PreventiveMaintenanceVehicleDetailBase
                {
                    Data = new PreventiveMaintenanceVehicleDetail(),
                    List = new List<PreventiveMaintenanceVehicleDetail>(),
                    PreventiveMaintenanceCatalogList = new List<SelectListItem>(),
                    Assign = new PreventiveMaintenanceVehicleAssign() { PreventiveMaintenanceCatalogList = new List<SelectListItem>() },
                    Record = new PreventiveMaintenanceRecordByVehicleBase()
                };

                model.Record.Alarm = new AlarmsModels();
                model.Record.Catalog = new PreventiveMaintenanceCatalog();
                model.Record.Data = new PreventiveMaintenanceRecordByVehicle();
                model.Record.Detail = new PreventiveMaintenanceRecordByVehicleCostDetail();
                model.Record.Detail.Cost = new System.Collections.Generic.List<PreventiveMaintenanceRecordByVehicleCost>();
                model.Record.Detail.Header = new PreventiveMaintenanceRecordByVehicleCostHeader();
                model.Record.PreventiveMaintenanceCatalogId = 0;
                return View(model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Search Vehicles
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult SearchVehicles(string key)
        {
            try
            {
                using (var business = new PreventiveMaintenanceVehicleBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "RealTime")
                });
            }
        }

        /// <summary>
        /// Load PreventiveMaintenanceVehicle
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadPreventiveMaintenanceVehicle(int id)
        {
            try
            {
                using (var business = new PreventiveMaintenanceVehicleBusiness())
                {
                    return PartialView("_partials/_AssignGrid", business.RetrievePreventiveMaintenanceByVehicle(id));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "RealTime")
                });
            }
        }

        /// <summary>
        ///  Performs the maintenance Insert or Update of the entity Preventive Maintenance By Vehicle
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditPreventiveMaintenanceVehicle(PreventiveMaintenanceVehicleAssign model)
        {
            try
            {
                using (var business = new PreventiveMaintenanceVehicleBusiness())
                {
                    business.AddOrEditPreventiveMaintenanceRecordVehicle(model);
                    return PartialView("_partials/_AssignGrid", business.RetrievePreventiveMaintenanceByVehicle(vehicleId: model.VehicleId));
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("Index", "PreventiveMaintenanceRecordByVehicle")
                });
            }
        }




        /// <summary>
        /// Performs the the operation of delete on the model 
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeletePreventiveMaintenanceVehicle(int vehicleId, int id)
        {
            try
            {
                using (var business = new PreventiveMaintenanceVehicleBusiness())
                {
                    business.DeletePreventiveMaintenanceVehicle(id);
                    return PartialView("_partials/_AssignGrid", business.RetrievePreventiveMaintenanceByVehicle(vehicleId: vehicleId));
                }
            }


            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Drivers")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Drivers")
                    });
                }
            }

        }


        /// <summary>
        /// Load PreventiveMaintenanceRecordByVehicle
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadPreventiveMaintenanceRecordByVehicleDetail(int id, int vehicleId)
        {
            try
            {
                using (var business = new PreventiveMaintenanceVehicleBusiness())
                {
                    var model = new PreventiveMaintenanceRecordByVehicleBase
                    {
                        Data = new PreventiveMaintenanceRecordByVehicle(),
                        Detail = business.PreventiveMaintenanceRecordByVehicleDetail(id),
                    };
                    model.Data.PreventiveMaintenanceId = id;
                    model.Data.VehicleId = vehicleId;
                    ViewBag.Symbol = ECOsystem.Utilities.Session.GetCustomerInfo().CurrencySymbol;
                    return PartialView("_partials/_DetailCost", model);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("IndexDetail", "PreventiveMaintenanceRecordByVehicle")
                });
            }

        }

        /// <summary>
        /// SaveCostList
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SaveRecordList(List<string> list, int PreventiveMaintenanceId, string Date, double Odometer, int Apply)
        {
            try
            {
                using (var business = new PreventiveMaintenanceVehicleBusiness())
                {
                    business.AddOrEditPreventiveMaintenanceRecordByVehicleDetail(list, PreventiveMaintenanceId, ECOsystem.Utilities.Miscellaneous.SetDate(Date), Odometer, Apply);
                }
                return Json(new { Result = "Ready" });
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json(new { Result = "Error" });
            }
        }

        /// <summary>
        /// RetrieveMaintenanceList
        /// </summary>
        /// <param name="filterType"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RetrieveMaintenanceList(int filterType)
        {
            using (var business = new PreventiveMaintenanceVehicleBusiness())
            {
                var model = business.RetrievePreventiveMaintenance(ECOsystem.Utilities.Session.GetCustomerId(), filterType, null, null);

                foreach (var item in model)
                {
                    item.Description = "{'Description':'" + item.Description + "'}";
                }

                return Json(model.AsEnumerable(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}

