﻿/************************************************************************************************************
*  File    : ConsolidateLiquidationReportController.cs
*  Summary : Consolidate Liquidation Report Controller Actions
*  Author  : Andrés Oviedo Brenes
*  Date    : 21/01/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Models.Account;
using System.Data;
using ECOsystem.Business.Utilities;
using System;




namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Consolidate Liquidation Report Class
    /// </summary>
    public class ConsolidateLiquidationReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new ConsolidateLiquidationReportBusiness())
                {
                    ConsolidateLiquidationReports modelI = new ConsolidateLiquidationReports();
                    var model = new ConsolidateLiquidationReportBase
                    {
                        Data = modelI,
                        List = business.RetrieveConsolidateLiquidationReport(new ConsolidateLiquidationReportBase()),
                        Menus = new List<AccountMenus>()
                    };
                    Session["ConsolidateLiquidation"] = model;
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index(ConsolidateLiquidationReportBase model)
        {
            try
            {
                if (model == null) RedirectToAction("Index");

                using (var business = new ConsolidateLiquidationReportBusiness())
                {
                    ConsolidateLiquidationReports modelI = new ConsolidateLiquidationReports();
                    var modelR = new ConsolidateLiquidationReportBase
                    {
                        Data = modelI,
                        List = business.RetrieveConsolidateLiquidationReport(model),
                        Month = model.Month,
                        Year = model.Year
                    };
                    Session["ConsolidateLiquidation"] = modelR;
                    return View(modelR);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Prints the report
        /// </summary>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public ActionResult PrintReport(string modelStr)
        {
            var model = JsonConvert.DeserializeObject<ConsolidateLiquidationReportBase>(modelStr);
            if (model == null) RedirectToAction("Index");

            using (var business = new ConsolidateLiquidationReportBusiness())
            {
                ConsolidateLiquidationReports modelI = new ConsolidateLiquidationReports();
                var modelR = new ConsolidateLiquidationReportBase
                {
                    Data = modelI,
                    List = business.RetrieveConsolidateLiquidationReport(model)
                };
                ViewBag.PrintView = true;
                return View("~/Views/ConsolidateLiquidationReport/Index.cshtml", modelR);
            }
        }

        /// <summary>
        /// Excel Report Download
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                DataTable report = new DataTable();

                var model = (ConsolidateLiquidationReportBase)Session["ConsolidateLiquidation"];

                using (var business = new ConsolidateLiquidationReportBusiness())
                {
                    report = business.GetReportDownloadData(model);
                    Session["Reporte"] = report;
                    return View("Index", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// DownloadFile
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte Liquidación Consolidada");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "ConsolidateLiquidationReport", this.ToString().Split('.')[2], "Reporte Liquidación Consolidada");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var model = (ConsolidateLiquidationReportBase)Session["ConsolidateLiquidation"];
                return View("Index", model);
            }
        }
    }
}

