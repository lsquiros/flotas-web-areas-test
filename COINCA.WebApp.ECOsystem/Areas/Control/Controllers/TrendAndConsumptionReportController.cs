﻿/************************************************************************************************************
*  File    : TrendAndConsumptionReportController.cs
*  Summary : Real Vs Budget Fuels Report Controller Actions
*  Author  : Berman Romero
*  Date    : 20/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Models.Identity;
using System;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Current Fuels Report Class
    /// </summary>
    public class TrendAndConsumptionReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new TrendAndConsumptionReportBusiness())
                {
                    return View(business.RetrieveTrendAndConsumptionReport(new TrendAndConsumptionParameters()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(TrendAndConsumptionReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new TrendAndConsumptionReportBusiness())
                {
                    return View(business.RetrieveTrendAndConsumptionReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<TrendAndConsumptionParameters>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

                byte[] report;
                using (var business = new TrendAndConsumptionReportBusiness())
                {
                    report = business.GenerateTrendAndConsumptionReportExcel(model);
                }
                return new FileStreamResult(new MemoryStream(report), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "ExcelReport.xlsx"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<TrendAndConsumptionParameters>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new TrendAndConsumptionReportBusiness())
            {
                return View("Index", business.RetrieveTrendAndConsumptionReport(model));
            }
        }
    }
}