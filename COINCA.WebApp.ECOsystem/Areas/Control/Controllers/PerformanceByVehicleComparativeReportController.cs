﻿/************************************************************************************************************
*  File    : PerformanceByVehicleReportController.cs
*  Summary : Performance By Vehicle Report Controller Actions
*  Author  : Andrés Oviedo Brenes
*  Date    : 23/01/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using System;
using System.Collections.Generic;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Models.Identity;
using System.Data;
using ECOsystem.Business.Utilities;




namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Performance By Vehicle Report Controller Class
    /// </summary>
    public class PerformanceByVehicleComparativeReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new PerformanceByVehicleComparativeReportBusiness())
                {

                    var modelR = business.RetrievePerformanceByVehicleComparativeReport(new PerformanceByVehicleComparativeReportParameters());
                    Session["VehiclePerformance"] = modelR;
                    return View(modelR);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(PerformanceByVehicleComparativeReportBase model)
        {
            try
            {
                if (model == null) RedirectToAction("Index");

                using (var business = new PerformanceByVehicleComparativeReportBusiness())
                {
                    if (model != null)
                    {
                        var result = business.RetrievePerformanceByVehicleComparativeReport(model.Parameters);
                        Session["VehiclePerformance"] = result;
                        return View(result);
                    }
                }
                return View(new PerformanceByVehicleComparativeReportBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Prints the report
        /// </summary>
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<PerformanceByVehicleComparativeReportParameters>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new PerformanceByVehicleComparativeReportBusiness())
            {
                return View("Index", business.RetrievePerformanceByVehicleComparativeReport(model));
            }
        }

        /// <summary>
        /// Load the Performance Details
        /// </summary>
        public PartialViewResult ShowDetails(int vehicleId, int? year, int? month, int? week, string day, int option)
        {
            try
            {
                using (var business = new PerformanceByVehicleComparativeReportBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrievePerformanceByVehicleComparativeDetail(vehicleId, year, month, week, day, option));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return null;
            }
        }

        /// <summary>
        /// Show the performance trend of the vehicle
        /// </summary>
        public JsonResult ShowChart(int? vehicleId, int? unitId, int? costCenterId, int groupValue, string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<PerformanceByVehicleComparativeReportParameters>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));
                if (model.ReportComparativeId == 100)
                {
                    model.VehicleId = vehicleId;
                }
                else
                {
                    model.UnitId = unitId;
                    model.CostCenterId = costCenterId;
                }
                using (var business = new PerformanceByVehicleComparativeReportBusiness())
                {
                    if (model.ReportComparativeId == 100)
                    {
                        var list = business.RetrievePerformanceByVehicleComparativeReport(model).List.Where(x => x.VehicleId == groupValue).ToList();
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new
                            {
                                labels = list.Select(x => x.FormattedDateGroupBy).ToArray(),
                                dataTrx = list.Select(x => decimal.Round(x.TrxPerformance == null ? new decimal(0) : (decimal)x.TrxPerformance, 2)).ToArray(),
                                dataGps = list.Select(x => decimal.Round(x.GpsPerformance == null ? new decimal(0) : (decimal)x.GpsPerformance, 2)).ToArray()
                            }
                        };
                    }
                    else
                    {
                        var list = business.RetrievePerformanceByVehicleComparativeReport(model).GroupList.Where(x => x.GroupValue == groupValue).ToList();
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new
                            {
                                labels = list.Select(x => x.FormattedDateGroupBy).ToArray(),
                                dataTrx = list.Select(x => decimal.Round(x.TrxPerformance == null ? new decimal(0) : (decimal)x.TrxPerformance, 2)).ToArray(),
                                dataGps = list.Select(x => decimal.Round(x.GpsPerformance == null ? new decimal(0) : (decimal)x.GpsPerformance, 2)).ToArray()
                            }
                        };
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Excel Report Download
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                DataTable report = new DataTable();

                var model = (PerformanceByVehicleComparativeReportBase)Session["VehiclePerformance"];

                using (var business = new PerformanceByVehicleComparativeReportBusiness())
                {
                    report = business.GetReportDownloadData(model);
                    Session["Reporte"] = report;
                    return View("Index", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// DownloadFile
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte Comparativo de Rendimiento");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "PerformanceByVehicleComparativeReport", this.ToString().Split('.')[2], "Reporte Comparativo de Rendimiento");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var model = (PerformanceByVehicleComparativeReportBase)Session["VehiclePerformance"];
                return View("Index", model);
            }
        }
    }
}

