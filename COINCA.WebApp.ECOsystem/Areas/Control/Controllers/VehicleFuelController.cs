﻿/************************************************************************************************************
*  File    : VehicleFuelController.cs
*  Summary : VehicleFuel Controller Actions
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using System.Collections.Generic;
using  ECOsystem.Areas.Control.Models.Identity;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// VehicleFuel Controller. Implementation of all action results from views
    /// </summary>
    public class VehicleFuelController : Controller
    {

        /// <summary>
        /// VehicleFuel Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                return View(new VehicleFuelBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// VehicleFuel Main View
        /// </summary>
        /// <param name="KeyId">VehicleId to load grid</param>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(int KeyId)
        {
            try
            {
                if (KeyId == 0) RedirectToAction("Index");

                using (var business = new VehicleFuelBusiness())
                {
                    return View(business.RetrieveVehicleFuelBase(KeyId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleFuel
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehicleFuel(VehicleFuel model)
        {
            try
            {
                using (var business = new VehicleFuelBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditVehicleFuel(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveVehicleFuel(null, model.VehicleId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("Code_50004")) // Error adding Fuels By Credit, Month and Year assigned to exist.
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Crédito ya asignado",
                        TechnicalError = "Ya tiene crédito asignado para el mes y año seleccionado.",
                        ReturnUlr = Url.Action("Index", "VehicleFuel")
                    });

                }
                else if (e.Message.Contains("Code_50002")) // Error updating Fuels By Credit, Assigned Amount greater than Total.
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Crédito No Disponible",
                        TechnicalError = "No hay crédito disponible para el mes seleccionado.",
                        ReturnUlr = Url.Action("Index", "VehicleFuel")
                    });

                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "VehicleFuel")
                    });
                }
            }
        }

        /// <summary>
        /// Search VehicleFuel
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <param name="vehicleId">Vehicle Id to load all information regarding fuel by month and year</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchVehicleFuel(string key, int vehicleId)
        {
            try
            {
                using (var business = new VehicleFuelBusiness())
                {
                    // return PartialView("_partials/_Grid", business.RetrieveVehicleFuel(null, vehicleId, key));

                    var filter = key;
                    key = String.Empty;

                    // var xxx = business.RetrieveVehicleFuel(null, vehicleId, key).ToList().Where(vehfuel => vehfuel.MonthName.ToLower().Contains(filter.ToLower())).ToList();
                    return PartialView("_partials/_Grid", business.RetrieveVehicleFuel(null, vehicleId, key).Where(vehfuel => vehfuel.FuelName.ToLower().Contains(filter.ToLower()) || vehfuel.CapacityUnitValueStr.ToLower().Contains(filter.ToLower()) || vehfuel.AmountStrCurrency.ToLower().Contains(filter.ToLower()) || vehfuel.MonthName.ToLower().Contains(filter.ToLower()) || vehfuel.Year.Equals(filter.ToLower())).ToList());

                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleFuel")
                });
            }
        }

        /// <summary>
        /// Load VehicleFuel
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="vehicleId">Vehicle Id to load all information regarding fuel by month and year</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadVehicleFuel(int id, int vehicleId)
        {
            try
            {
                if (id < 0)
                {
                    using (var business = new VehicleFuelBusiness())
                    {
                        var vehicleBase = business.RetrieveVehicleFuelBase(vehicleId);
                        ViewBag.CurrencySymbol = vehicleBase.CurrencySymbol;

                        return PartialView("_partials/_Detail", new VehicleFuel());
                    }
                }

                using (var business = new VehicleFuelBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveVehicleFuel(id, vehicleId).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleFuel")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleFuel
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="vehicleId">Vehicle Id to load all information regarding fuel by month and year</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteVehicleFuel(int id, int vehicleId)
        {
            try
            {
                using (var business = new VehicleFuelBusiness())
                {
                    business.DeleteVehicleFuel(id);
                    return PartialView("_partials/_Grid", business.RetrieveVehicleFuel(null, vehicleId));
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "VehicleFuel")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "VehicleFuel")
                    });
                }
            }

        }

    }
}