﻿/************************************************************************************************************
*  File    : CostCentersByCreditController.cs
*  Summary : CostCentersByCredit Controller Actions
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Models.Account;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// CostCentersByCredit Controller. Implementation of all action results from views
    /// </summary>
    public class CostCentersByCreditController : Controller
    {

        /// <summary>
        /// CostCentersByCredit Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new CustomerCreditsBusiness())
                {
                    return View(new CostCentersByCreditBase
                    {
                        Data = new CostCentersByCredit(),
                        GridInfo = new CostCentersByCreditGrid(),
                        CreditInfo = business.RetrieveCustomerCredits().FirstOrDefault(),
                        Menus = new List<AccountMenus>()
                    });
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// CostCentersByCredit by Group Main View to reload the page
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ActionResult with main View and Model</returns>
        [HttpPost]
        [ValidateInput(false)]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index(CostCentersByCreditBase model)
        {
            try
            {
                if (model == null) RedirectToAction("Index");

                using (var business = new CostCentersByCreditBusiness())
                {
                    return View(business.RetrieveCostCentersByCredit(model));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Performs the maintenance Insert or Update Vehicle Fuel and Credit to all vehicle related to a specific group
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult ApplyValueToAll(CostCentersByCreditBase model)
        {
            try
            {
                using (var business = new CostCentersByCreditBusiness())
                {
                    return PartialView("_partials/_Grid", business.AddOrEditCostCentersByCredit(model.Data));
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CostCentersByCredit")
                });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update Vehicle Fuel and Credit to all vehicle related to a specific group
        /// </summary>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <param name="list">CostCentersValues List</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult ApplyValuesToAll(CostCentersByCredit data, IList<CostCentersValues> list)
        {
            try
            {
                using (var business = new CostCentersByCreditBusiness())
                {
                    return PartialView("_partials/_Grid", business.AddOrEditCostCentersByCredit(data, list));
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CostCentersByCredit")
                });
            }
        }

    }
}