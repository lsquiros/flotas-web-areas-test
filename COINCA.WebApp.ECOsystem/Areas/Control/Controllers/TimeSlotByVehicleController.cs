﻿/************************************************************************************************************
*  File    : TimeSlotByVehicleController.cs
*  Summary : TimeSlotByVehicle Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 12/04/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Models.Account;
using System.Collections.Generic;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// TimeSlotByVehicle Controller. Implementation of all action results from views
    /// </summary>
    public class TimeSlotByVehicleController : Controller
    {
        /// <summary>
        /// TimeSlotByVehicle Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                var model = new TimeSlotByVehicleBase
                {
                    VehicleId = null,
                    Data = new TimeSlotByVehicle(),
                    Menus = new List<AccountMenus>()
                };
                return View(model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity TimeSlotByVehicle
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditTimeSlotByVehicle(TimeSlotByVehicle model)
        {
            try
            {
                using (var business = new TimeSlotByVehicleBusiness())
                {
                    business.AddOrEditTimeSlotByVehicle(model);
                    return PartialView("_partials/_Detail", business.RetrieveTimeSlotByVehicle(model.VehicleId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "TimeSlotByVehicle")
                });
            }
        }


        /// <summary>
        /// Load TimeSlotByVehicle
        /// </summary>
        /// <param name="id">The vehicle group Id to find all vehicles associated</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadTimeSlotByVehicle(int id)
        {
            try
            {
                using (var business = new TimeSlotByVehicleBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveTimeSlotByVehicle(id));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "TimeSlotByVehicle")
                });
            }
        }
    }
}
