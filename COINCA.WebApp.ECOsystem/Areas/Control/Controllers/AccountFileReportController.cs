﻿using  ECOsystem.Areas.Control.Business;
using  ECOsystem.Areas.Control.Models;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.Web.Mvc;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    public class AccountFileReportController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                var model = new AccountFileReportBase()
                {
                    Menus = new List<AccountMenus>(),
                    List = new List<AccountFileReportModel>()
                };

                return View(model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(AccountFileReportBase model)
        {
            try
            {

                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new AccountFileReportBusiness())
                {
                    return View(business.RetrieveAccountReportInfo(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Set the data ready to download
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));
                if (model != null)
                {
                    //model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                    model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
                }
                DataTable report = new DataTable();

                using (var business = new AccountFileReportBusiness())
                {
                    report = business.GetReportDownloadData(model);

                    Session["Reporte"] = report;

                    return View("Index", business.RetrieveAccountReportInfo(model));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de Archivo de Contabilidad");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "AccountFileReport", this.ToString().Split('.')[2], "Reporte de Archivo de Contabilidad");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var model = new AccountFileReportBase()
                {
                    Menus = new List<AccountMenus>(),
                    List = new List<AccountFileReportModel>()
                };
                return View("Index", model);
            }
        }
    }
}