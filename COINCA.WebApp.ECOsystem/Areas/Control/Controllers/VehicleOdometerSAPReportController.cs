﻿using  ECOsystem.Areas.Control.Business;
using  ECOsystem.Areas.Control.Models;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    public class VehicleOdometerSAPReportController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                return View(new VehicleOdometerSAPReportBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(int? CostCenterId)
        {
            try
            {
                CostCenterId = CostCenterId == 0 ? null : CostCenterId;
                using (var business = new VehicleOdometerSAPReportBusiness())
                {
                    var list = business.GetReportData(CostCenterId);
                    Session["Reporte"] = list;
                    return Json(list.Rows.Count, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de Odometro por Vehículo SAP");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "VehicleOdometerSAPReport", this.ToString().Split('.')[2], "Reporte de Odometro por Vehículo SAP");
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return Redirect("Index");
            }
        }
    }
}