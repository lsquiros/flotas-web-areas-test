﻿/************************************************************************************************************
*  File    : CurrentFuelsReportController.cs
*  Summary : Current Fuels Report Controller Actions
*  Author  : Berman Romero
*  Date    : 20/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Models.Identity;
using System;
using ECOsystem.Business.Utilities;
using System.Data;




namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Current Fuels Report Class
    /// </summary>
    public class CurrentFuelsReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        static ControlFuelsReportsBase report_Parameters;

        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new CurrentFuelsReportBusiness())
                {
                    return View(business.RetrieveCurrentFuelsReport(new ControlFuelsReportsBase()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index(CurrentFuelsReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new CurrentFuelsReportBusiness())
                {
                    return View(business.RetrieveCurrentFuelsReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                Object report;
                report_Parameters = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

                if (report_Parameters != null)
                {
                    //    report_Parameters.StartDate = (report_Parameters.StartDate != null) ? Convert.ToDateTime(report_Parameters.StartDate).AddDays(1) : report_Parameters.StartDate;
                    report_Parameters.EndDate = (report_Parameters.EndDate != null) ? Convert.ToDateTime(report_Parameters.EndDate).AddDays(-1) : report_Parameters.EndDate;
                }
                using (var business = new CurrentFuelsReportBusiness())
                {
                    if ((ReportFuelTypes)report_Parameters.ReportFuelTypeId != ReportFuelTypes.VehicleCostCenters &&
                        (ReportFuelTypes)report_Parameters.ReportFuelTypeId != ReportFuelTypes.VehicleGroups &&
                        (ReportFuelTypes)report_Parameters.ReportFuelTypeId != ReportFuelTypes.VehicleGroups)
                    {
                        report = new DataTable();
                        report = business.GetReportDownloadData(report_Parameters);
                    }
                    else
                    {
                        report = new DataSet();
                        report = business.GetReportDownloadDataSet(report_Parameters);
                    }
                    Session["Reporte"] = report;
                    return View("Index", business.RetrieveCurrentFuelsReport(report_Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {

                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Consumo Real del Período");
                    if ((ReportFuelTypes)report_Parameters.ReportFuelTypeId != ReportFuelTypes.VehicleCostCenters && (ReportFuelTypes)report_Parameters.ReportFuelTypeId != ReportFuelTypes.VehicleGroups)
                    {
                        DataTable report = new DataTable();
                        if (Session["Reporte"] != null)
                        {
                            report = (DataTable)Session["Reporte"];
                            Session["Reporte"] = null;
                        }

                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "CurrentFuelsReport_" + (ReportFuelTypes)report_Parameters.ReportFuelTypeId, this.ToString().Split('.')[2], "Consumo Real del Período");
                    }
                    else
                    {
                        DataSet report = new DataSet();
                        if (Session["Reporte"] != null)
                        {
                            report = (DataSet)Session["Reporte"];
                            Session["Reporte"] = null;
                        }

                        var reportName = "";
                        switch ((ReportFuelTypes)report_Parameters.ReportFuelTypeId)
                        {
                            case ReportFuelTypes.VehicleCostCenters:
                                reportName = "CurrentFuelsReport_VehicleCostCenters";
                                break;
                            case ReportFuelTypes.VehicleGroups:
                                reportName = "CurrentFuelsReport_VehicleGroups";
                                break;
                        }
                        return bus.GetReportDataSet(JsonConvert.SerializeObject(report), reportName, this.ToString().Split('.')[2], "Consumo Real del Período");
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return Redirect("Index");
            }
        }
    }
}