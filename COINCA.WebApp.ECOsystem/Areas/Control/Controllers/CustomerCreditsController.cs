﻿/************************************************************************************************************
*  File    : CustomerCreditsController.cs
*  Summary : CustomerCredits Controller Actions
*  Author  : Berman Romero
*  Date    : 09/30/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using System.Net.Http.Formatting;




namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Transaction Controller. Implementation of all methods for API
    /// </summary>
    public class CustomerCreditController : ApiController
    {
        /// <summary>
        /// POST api/customercredit in order to add an new customer credit information
        /// </summary>
        /// <param name="data">CustomersApi model with all information</param>
        /// <returns>Represents a HTTP response message including the status code and data.</returns>
        public HttpResponseMessage Post([FromBody] CustomerCreditApi data)
        {
            try
            {
                using (var business = new CustomerCreditsBusiness())
                {
                    var customerCreditId = business.AddCustomerCreditFromApi(data);
                    if (customerCreditId == 0)
                        throw new Exception("Creation fails when call Business.AddCustomerCreditFromApi method, with Account Number : '" + data.AccountNumber + "'");

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Message = "Transaction information, was successfully stored.",
                        CustomerCreditId = customerCreditId
                    });
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Request.CreateResponse(HttpStatusCode.BadRequest, new
                {
                    e.Message
                });
            }
        }
    }
}