﻿/************************************************************************************************************
*  File    : VehiclesByGroupController.cs
*  Summary : VehiclesByGroup Controller Actions
*  Author  : Berman Romero
*  Date    : 09/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web.Mvc;
using ECO_Control.Business.Control;
using ECO_Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using ECO_Control.Models.Identity;
using ECOsystem.Models.Account;
using System.Collections.Generic;

namespace ECO_Control.Controllers.Control
{
    /// <summary>
    /// VehiclesByGroup Controller. Implementation of all action results from views
    /// </summary>
    public class VehiclesByGroupController : Controller
    {

        /// <summary>
        /// VehiclesByGroup Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            var model = new VehiclesByGroupBase
                {
                    VehicleGroupId = null,
                    Data = new VehiclesByGroupData(),
                    Menus = new List<AccountMenus>()
                };
            return View(model);
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehiclesByGroup
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehiclesByGroup(VehiclesByGroupData model)
        {
            try
            {
                using (var business = new VehiclesByGroupBusiness())
                {
                    business.AddOrEditVehiclesByGroup(model);
                    return PartialView("_partials/_List", business.RetrieveVehiclesByGroup(model.VehicleGroupId) );
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehiclesByGroup")
                });
            }
        }


        /// <summary>
        /// Load VehiclesByGroup
        /// </summary>
        /// <param name="id">The vehicle group Id to find all vehicles associated</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadVehiclesByGroup(int id)
        {
            try
            {
                using (var business = new VehiclesByGroupBusiness())
                {
                    return PartialView("_partials/_List", business.RetrieveVehiclesByGroup(id));
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehiclesByGroup")
                });
            }
        }


    }
}