﻿/************************************************************************************************************
*  File    : VehicleFuelByGroupController.cs
*  Summary : VehicleFuel Controller Actions
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using System.Collections.Generic;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Models.Account;
using System.Data;
using Microsoft.VisualBasic.FileIO;
using ECOsystem.Business.Utilities;
using System.Runtime.InteropServices;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using System.Linq;

using Newtonsoft.Json;



namespace  ECOsystem.Areas.Control.Controllers.Control
{
    public class VehicleFuelByGroupController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                return View(new List<FuelDistribution>());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Index(DistributionParameters model)
        {
            try
            {
                var customer = ECOsystem.Utilities.Session.GetCustomerInfo();

                using (var bus = new VehicleFuelByGroupBusiness())
                {
                   var list = bus.RetrieveData(model, customer);
                    Session["FuelDistributionData"] = list;
                    return View(list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ApplyChanges(List<FuelDistributionEdition> list)
        {
            try
            {
                var data = (List<FuelDistribution>)Session["FuelDistributionData"];
                data.Where(x => list.Any(z => z.Id != null ? z.Id == x.Id : z.VehicleId == x.VehicleId && z.FuelId == x.FuelId)).ToList().ForEach(s =>
                {
                    s.Amount = list.FirstOrDefault().Amount ?? Convertion(s.LiterPrice, null, list.FirstOrDefault().Liters);
                    s.CapacityUnitValue = list.FirstOrDefault().Liters ?? Convertion(s.LiterPrice, list.FirstOrDefault().Amount, null);
                    s.Selected = true;
                });
                Session["FuelDistributionData"] = data;
                return PartialView("_partials/_Grid", data);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [EcoAuthorize]
        public ActionResult Save()
        {
            try
            {
                using (var dba = new VehicleFuelByGroupBusiness())
                {
                    var data = (List<FuelDistribution>)Session["FuelDistributionData"];
                    var result = dba.FuelDistributionAddOrEdit(data.Where(x => x.Selected == true).ToList());
                    //Update the result to null
                    data.ForEach(s => s.Result = null);
                    data = data.Except(data.Where(x => result.Any(z => z.Id == x.Id || (z.VehicleId == x.VehicleId && z.FuelId == x.FuelId))).ToList()).ToList();
                    data.AddRange(result);
                    foreach (var item in result)
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, JsonConvert.SerializeObject(item));
                    }
                    Session["FuelDistributionData"] = data.OrderBy(x => x.Id).ToList();
                    return PartialView("_partials/_Grid", data.OrderBy(x => x.Id));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                var error = string.Format("Error al modificar la distribución de combustible. Detalle técnico: {0}", e.Message);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, error);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TitleError = ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 101 ? "Distribución por Vehículos" : "Distribución por Conductores",
                    TechnicalError = error,
                    ReturnUlr = Url.Action("Index", "VehicleFuelByGroup")
                });
            }
        }

        private decimal? Convertion(decimal literPrice, decimal? amount, decimal? liters)
        {

            if (literPrice == 0)
                throw new Exception("Precio del Litro o Galón en nulo o en cero");

            if (liters == null)
            {
                return amount / literPrice;
            }
            else
            {
                return liters * literPrice;
            }
        }

        #region ImportMethods       
        [HttpPost]
        public ActionResult AddOrEditImportVehicleFuel()
        {
            DataTable csvData = new DataTable();
            var ErrorList = new List<string>();
            var List = new List<VehicleFuelErrors>();
            List<FuelDistribution> ListReturn = new List<FuelDistribution>();
            int ErrorCount = 0;
            int SuccessCount = 0;
            int NotFound = 0;
            int InvalidLines = 0;

            List<FuelDistribution> ListImport = new List<FuelDistribution>();
            try
            {
                HttpPostedFileBase file = Request.Files[0];
                // Verify that the user selected a file
                if (file != null && file.ContentLength > 0)
                {
                    // extract only the filename
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.GetTempPath() + fileName;
                    file.SaveAs(path);

                    // redirect back to the index action to show the form once again
                    int RowCount = 1;
                    using (TextFieldParser csvReader = new TextFieldParser(path))
                    {
                        csvReader.SetDelimiters(new string[] { "," });
                        csvReader.HasFieldsEnclosedInQuotes = true;
                        int countColumns = 0;
                        while (!csvReader.EndOfData)
                        {
                            VehicleFuelErrors ErrorModel = new VehicleFuelErrors();
                            FuelDistribution model = new FuelDistribution();
                            string[] colFields = csvReader.ReadFields();
                            string[] ColumnsToModel = new string[3];
                            if (colFields.Length <= 3)
                            {
                                foreach (string column in colFields)
                                {
                                    if (!column.Contains("!") && !column.Contains("¡") && column != "Placa" && column != "Monto" && column.Length < 10)
                                    {
                                        DataColumn datecolumn = new DataColumn(column);
                                        datecolumn.AllowDBNull = true;
                                        ColumnsToModel[countColumns] = column;
                                        countColumns++;
                                    }
                                    else
                                    {
                                        break;
                                    }

                                }

                                if (ColumnsToModel[0] != null)
                                {
                                    countColumns = 0;
                                    model.PlateId = ColumnsToModel[0];
                                    if (ColumnsToModel.Length > 2 && (ColumnsToModel[2] != "" && ColumnsToModel[2] != null))
                                    {
                                        model.CapacityUnitValue = Convert.ToDecimal(ColumnsToModel[2]);
                                    }
                                    else
                                    {

                                        model.Amount = Convert.ToDecimal(ColumnsToModel[1]);
                                    }

                                    ListImport.Add(model);
                                }
                            }
                            else
                            {
                                ErrorModel.Item = RowCount + " ) Formato de línea Inválido, se enconraron más columnas de las esperadas";
                                List.Add(ErrorModel);
                                ErrorCount++;
                                #region log
                                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(ErrorModel)));
                                #endregion
                            }
                            RowCount++;
                        }

                        using (var business = new VehicleFuelByGroupBusiness())
                        {
                            try
                            {
                                ListReturn = business.FuelDistributionAddOrEdit(ListImport);

                                NotFound = ListImport.Count - ListReturn.Count;

                                SuccessCount = ListReturn.Where(x => x.Result == 0).ToList().Count;

                                List<FuelDistribution> NotInsertedList = ListImport.Except(ListImport.Where(x => ListReturn.Any(z => z.PlateId == x.PlateId)).ToList()).ToList();

                                NotInsertedList.ForEach(x =>
                                {
                                    VehicleFuelErrors ErrorModel = new VehicleFuelErrors();
                                    ErrorModel.Item = RowCount + " ) Presupuesto para Vehículo con Placa: " + x.PlateId + " - Falló" + " - Mensaje de error: Placa del vehiculo no existe";
                                    List.Add(ErrorModel);
                                    ErrorCount++;
                                    RowCount++;
                                });

                                ListReturn.Where(x => x.Result != 0).ToList().ForEach(x =>
                                {
                                    VehicleFuelErrors ErrorModel = new VehicleFuelErrors();
                                    ErrorModel.Item = String.Format("{0} ) Presupuesto para Vehículo con Placa: {1} - Falló - Mensaje de error: {2}", RowCount, x.PlateId, x.ResultStr.Replace("<span class='text-result'>", string.Empty).Replace("</span>", string.Empty));
                                    //RowCount + " ) Presupuesto para Vehículo con Placa: " + x.PlateId + " - Falló" + " - Mensaje de error: "
                                    List.Add(ErrorModel);
                                    ErrorCount++;
                                    RowCount++;
                                });

                                InvalidLines = NotInsertedList.Count();
                            }
                            catch (Exception e)
                            {
                                #region log
                                new EventLogBusiness().AddLogEvent(LogState.INFO, e);
                                #endregion
                            }
                        }

                        using (var business = new VehicleFuelByGroupBusiness())
                        {
                            Session["SummaryFile"] = business.CreateTxtFormat(List, ErrorCount, SuccessCount);
                            Session["Path"] = path;
                        }
                    }
                }
                ViewBag.ResultData = string.Format("{0}|{1}|{2}|{3}|{4}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(SuccessCount),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(NotFound),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(InvalidLines),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(ListImport.Count()));

                return View("index", new List<FuelDistribution>());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DownloadReportFile()
        {
            try
            {
                byte[] SummaryFile = null;
                SummaryFile = (byte[])Session["SummaryFile"];
                Session["SummaryFile"] = null;
                string path = Session["Path"].ToString();
                Session["Path"] = null;
                System.IO.File.Delete(path);
                var CustomerInfo = ECOsystem.Utilities.Session.GetCustomerInfo();
                DateTime Today = DateTime.Today.Date;
                return new FileStreamResult(new MemoryStream(SummaryFile), "text/plain")
                {
                    FileDownloadName = CustomerInfo.DecryptedName + "- Reporte de Importación Presupuesto - " + Today + ".txt"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DonwloadExampleFile()
        {
            try
            {
                byte[] SummaryFile = System.IO.File.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data/Archivo_Ejemplo.csv")));

                return new FileStreamResult(new MemoryStream(SummaryFile), "text/csv")
                {
                    FileDownloadName = "Archivo_Ejemplo.csv"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}
