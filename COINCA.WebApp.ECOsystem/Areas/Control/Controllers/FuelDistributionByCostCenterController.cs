﻿using  ECOsystem.Areas.Control.Business;
using  ECOsystem.Areas.Control.Models;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace  ECOsystem.Areas.Control.Controllers
{
    public class FuelDistributionByCostCenterController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var bus = new FuelDistributionByCostCenterBusiness())
                {
                    var CostCenterList = new List<FuelDistributionByCostCenter>();
                    if (Session["CostCenterList"] == null)
                    {
                        CostCenterList = bus.FuelDistributionByCostCenterRetrieve(ECOsystem.Utilities.Session.GetCustomerId());
                        Session["CostCenterList"] = CostCenterList;
                    }
                    else
                    {
                        CostCenterList = (List<FuelDistributionByCostCenter>)Session["CostCenterList"];
                    }
                    return View(CostCenterList);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FuelDistributionChance(int? CostCenterId, decimal Amount)
        {
            var CostCenterList = (List<FuelDistributionByCostCenter>)Session["CostCenterList"];
            var obj = CostCenterList.Where(x => x.CostCenterId == CostCenterId).FirstOrDefault();
            var CurrentAmount = obj.CreditAmount;
            var CreditAssigned = obj.CreditAssigned;
            var totalCredit = obj.TotalCreditAmount;
            var availableAmount = obj.CreditCardLimit;
            var success = false;
            var totalAssigned = (totalCredit - CurrentAmount) + Amount;

            if (CurrentAmount != Amount)
            {
                if (totalAssigned <= availableAmount)
                {
                    if (Amount >= CreditAssigned)
                    {
                        CostCenterList.Where(x => x.CostCenterId == CostCenterId).ToList().ForEach(s => { s.CreditAmount = Amount; s.Selected = true; });
                        CostCenterList.ForEach(s => s.TotalCreditAmount = totalAssigned);
                        success = true;
                        Session["CostCenterList"] = CostCenterList;
                    }
                    else
                    {
                        Amount = CostCenterList.Where(x => x.CostCenterId == CostCenterId).FirstOrDefault().CreditAmount;
                        return Json(new { success, message = string.Format("El monto no puede ser menor a lo ya distribuido. Monto distribuido: {0}", ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(CreditAssigned, 2, "es-CR", false)), amount = ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Amount, 2, "es-CR", false) }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    Amount = CostCenterList.Where(x => x.CostCenterId == CostCenterId).FirstOrDefault().CreditAmount;
                    return Json(new { success, message = "El monto sobrepasa el disponible.", amount = ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Amount, 2, "es-CR", false) }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                success,
                amount = ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Amount, 2, "es-CR", false),
                totalAssigned,
                totalCreditAmount = availableAmount,
                totalAssignedStr = obj.CurrencySymbol + totalAssigned.ToString("N", CultureInfo.CreateSpecificCulture("es-CR"))
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FuelDistributionByCostCenterAddOrEdit()
        {
            try
            {
                var CostCenterList = (List<FuelDistributionByCostCenter>)Session["CostCenterList"];
                var xml = ECOsystem.Utilities.Miscellaneous.GetXML(CostCenterList.Where(x => x.Selected).ToList(), typeof(FuelDistributionByCostCenter).Name);
                using (var bus = new FuelDistributionByCostCenterBusiness())
                {
                    bus.FuelDistributionByCostCenterAddOrEdit(ECOsystem.Utilities.Session.GetCustomerId(), xml, ECOsystem.Utilities.Session.GetUserInfo().UserId);
                    CostCenterList = bus.FuelDistributionByCostCenterRetrieve(ECOsystem.Utilities.Session.GetCustomerId());
                    Session["CostCenterList"] = CostCenterList;

                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e, true);
                return RedirectToAction("Index");
            }
        }

        public ActionResult FuelDistributionByCostCenterSearch(string txtSearch)
        {
            var CostCenterList = new List<FuelDistributionByCostCenter>();
            CostCenterList = (List<FuelDistributionByCostCenter>)Session["CostCenterList"];
            return PartialView("_partials/_Grid", CostCenterList.Where(x => x.CostCenterName.ToLower().Contains(txtSearch.ToLower()) || x.UnitName.ToLower().Contains(txtSearch.ToLower())).ToList());
        }

        public ActionResult FuelDistributionByCostCenterCancel()
        {
            using (var bus = new FuelDistributionByCostCenterBusiness())
            {
                var CostCenterList = bus.FuelDistributionByCostCenterRetrieve(ECOsystem.Utilities.Session.GetCustomerId());
                Session["CostCenterList"] = CostCenterList;
                return RedirectToAction("Index");
            }
        }

        #region ImportData
        [HttpPost]
        public ActionResult ImportData()
        {
            try
            {
                HttpPostedFileBase file = Request.Files[0];
                var list = new List<FuelDistributionByCostCenter>();
                var fileName = (dynamic)null;

                #region Go through the file
                if (file != null && file.ContentLength > 0)
                {
                    fileName = Path.GetFileName(file.FileName);
                    var path = Path.GetTempPath() + fileName;
                    file.SaveAs(path);

                    using (TextFieldParser csvReader = new TextFieldParser(path, Encoding.GetEncoding("iso-8859-1")))
                    {
                        list = GetFileData(csvReader, ",");
                    }

                    if (list.Count == 0)
                    {
                        using (TextFieldParser csvReader = new TextFieldParser(path, Encoding.GetEncoding("iso-8859-1")))
                        {
                            list = GetFileData(csvReader, ";");
                        }
                    }
                }
                #endregion

                var xml = ECOsystem.Utilities.Miscellaneous.GetXML(list, typeof(FuelDistributionByCostCenter).Name);
                using (var bus = new FuelDistributionByCostCenterBusiness())
                {
                    var result = bus.FuelDistributionByCostCenterImportAddOrEdit(ECOsystem.Utilities.Session.GetCustomerId(), xml, ECOsystem.Utilities.Session.GetUserInfo().UserId);
                    
                    #region Event log
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Importación de Crédito por Centro de Costo");
                    #endregion

                    #region Results Data
                    TempData["DownloadResultFile"] = result.Count > 0 ? true : false;
                    TempData["ResultData"] = string.Format("{0}|{1}|{2}|{3}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(list.Count - result.Count),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(result.Count),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(list.Count));

                    Session["NoProcessData"] = result;
                    Session["FileNameToDownload"] = fileName;
                    Session["CostCenterList"] = null;
                    #endregion

                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                if (e.Message == "OverCreditLimit")
                {                    
                    TempData["InvalidImportAmount"] = true;
                    return RedirectToAction("Index");
                }
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e, true);
                return RedirectToAction("Index");
            }
        }

        private List<FuelDistributionByCostCenter> GetFileData(TextFieldParser csvReader, string delimiter)
        {
            var list = new List<FuelDistributionByCostCenter>();
            csvReader.SetDelimiters(delimiter);
            csvReader.HasFieldsEnclosedInQuotes = true;
            int lineNumber = 1;

            while (!csvReader.EndOfData)
            {
                string[] colFields = csvReader.ReadFields();
                try
                {
                    var item = new FuelDistributionByCostCenter();
                    item.UnitName = colFields[0];
                    item.CostCenterName = colFields[1];
                    item.CreditAmount = Convert.ToDecimal(colFields[2]);
                    item.LineNumber = lineNumber;
                    list.Add(item);
                }
                catch (Exception e)
                {
                    var item = new FuelDistributionByCostCenter();
                    item.UnitName = colFields[0];
                    item.CostCenterName = colFields[1];
                    item.LineNumber = lineNumber;
                    list.Add(item);
                }
                lineNumber++;
            }
            return list;
        }

        public ActionResult DonwloadExampleFile()
        {
            try
            {
                byte[] SummaryFile = System.IO.File.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data/Ejemplo_Credito_Centro_Costo.csv")));

                return new FileStreamResult(new MemoryStream(SummaryFile), "text/csv")
                {
                    FileDownloadName = "Ejemplo_Credito_Centro_Costo.csv"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DonwloadImportResults()
        {
            try
            {
                var list = (List<FuelDistributionByCostCenter>)Session["NoProcessData"];
                var fileName = Session["FileNameToDownload"].ToString();

                Session["NonApprovalList"] = null;
                Session["FileNameToDownload"] = null;
                using (var ms = new MemoryStream())
                {
                    var sw = new StreamWriter(ms, System.Text.Encoding.GetEncoding(1252));

                    #region HeaderReport
                    sw.WriteLine("Fecha: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                    sw.Flush();
                    sw.WriteLine("Archivo Origen: " + fileName);
                    sw.Flush();
                    sw.WriteLine("Las líneas no fueron procesadas debido a los siguientes motivos: ");
                    sw.Flush();
                    sw.WriteLine("1. El nombre de la Unidad o Centro de Costo no coincide con la información en la base de datos");
                    sw.Flush();
                    sw.WriteLine("2. El monto ingresado es nulo o en un formato no válido.");
                    sw.Flush();
                    sw.WriteLine("3. El monto ingresado es menor a lo ya distribuido.");
                    sw.Flush();
                    sw.WriteLine(string.Empty);
                    sw.Flush();
                    sw.WriteLine("# de Línea,Nombre Unidad,Nombre Centro de Costo,Monto");
                    sw.Flush();
                    #endregion

                    foreach (var item in list)
                    {
                        sw.WriteLine(string.Format("{0},{1},{2},{3}", item.LineNumber, item.UnitName, item.CostCenterName, item.CreditAmount));
                        sw.Flush();
                    }
                    return new FileStreamResult(new MemoryStream(ms.ToArray()), "text/plain")
                    {
                        FileDownloadName = "Resultado de Importación de Crédito por Centro de Costo.csv"
                    };
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}