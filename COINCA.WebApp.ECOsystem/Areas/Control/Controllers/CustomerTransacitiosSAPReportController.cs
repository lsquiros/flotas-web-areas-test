﻿using  ECOsystem.Areas.Control.Business.Control;
using ECOsystem.Models.Account;
using  ECOsystem.Areas.Control.Models.Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Controller to save the Parameters in the database 
    /// </summary>
    public class CustomerTransacitiosSAPReportController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        //[EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var bus = new TransactionSapReportBusiness())
                {
                    var model = new TransactionsConsolideReportBase()
                    {
                        Data = bus.SAPParametersRetrieve(),
                        Menus = new List<AccountMenus>(),
                        List = new List<TransactionsConsolideReport>()
                    };

                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// AddOrEditSAPParameters
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[EcoAuthorize]
        public ActionResult AddOrEditSAPParameters(TransactionsConsolideReportBase model)
        {
            try
            {
                using (var bus = new TransactionSapReportBusiness())
                {
                    bus.SAPParametersEdit(model.Data);

                    var modelIndex = new TransactionsConsolideReportBase()
                    {
                        Data = bus.SAPParametersRetrieve(),
                        Menus = new List<AccountMenus>(),
                        List = new List<TransactionsConsolideReport>()
                    };

                    return View("Index", modelIndex);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }
    }
}