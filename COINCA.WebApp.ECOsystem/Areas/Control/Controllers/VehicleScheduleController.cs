﻿/************************************************************************************************************
*  File    : VehicleScheduleController.cs
*  Summary : VehicleSchedule Controller Actions
*  Author  : Berman Romero
*  Date    : 09/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web.Mvc;
using ECO_Control.Business.Control;
using ECO_Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using ECO_Control.Models.Identity;
using System.Collections.Generic;
using ECOsystem.Models.Account;

namespace ECO_Control.Controllers.Control
{
    /// <summary>
    /// VehicleSchedule Controller. Implementation of all action results from views
    /// </summary>
    public class VehicleScheduleController : Controller
    {

        /// <summary>
        /// VehicleSchedule Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            var model = new VehicleScheduleBase
                {
                    VehicleId = null,
                    Data = new VehicleSchedule(),
                    Alarm = new AlarmsModels() { 
                        EntityTypeId = 400, 
                        AlarmTriggerId = 502,
                        TitlePopUp = "Alarma Fuera de Horario"
                    },
                    Menus = new List<AccountMenus>()
                };
            return View(model);
        }
        

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleSchedule
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehicleSchedule(VehicleSchedule model)
        {
            try
            {
                using (var business = new VehicleScheduleBusiness())
                {
                    business.AddOrEditVehicleSchedule(model);
                    return PartialView("_partials/_Detail", business.RetrieveVehicleSchedule(model.VehicleId) );
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleSchedule")
                });
            }
        }


        /// <summary>
        /// Load VehicleSchedule
        /// </summary>
        /// <param name="id">The vehicle group Id to find all vehicles associated</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadVehicleSchedule(int id)
        {
            try
            {
                using (var business = new VehicleScheduleBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveVehicleSchedule(id));
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleSchedule")
                });
            }
        }


    }
}