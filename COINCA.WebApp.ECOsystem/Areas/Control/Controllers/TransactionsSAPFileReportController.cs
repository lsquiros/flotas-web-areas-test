﻿using  ECOsystem.Areas.Control.Business;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Models.Identity;
using  ECOsystem.Areas.Control.Models.Control;




namespace  ECOsystem.Areas.Control.Controllers.Control
{
    public class TransactionsSAPFileReportController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                return View(new ControlFuelsReportsBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetReportData(ControlFuelsReportsBase model)
        {
            try
            {
                using (var bus = new TransactionsSAPFileReportBusiness())
                {
                    var data = bus.GetReportData(model);
                    if (data.Rows.Count > 0)
                    {
                        Session["Reporte"] = data;
                    }
                    else
                        return Json("NoData", JsonRequestBehavior.AllowGet);

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Genera información para el Archivo Transacciones para SAP, parámetros: {0}", JsonConvert.SerializeObject(model)));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Generar información para el Reporte de Facturación, Error: {0}", e.Message));
                return Json(new ErrorModels() { Title = "Reporte de Facturación", TechnicalError = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Archivo Transacciones para SAP");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "TransactionsSAPFileReport", this.ToString().Split('.')[2], "Archivo Transacciones para SAP");
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}