﻿using  ECOsystem.Areas.Control.Models.Control;
using  ECOsystem.Areas.Control.Models.Identity;
using  ECOsystem.Areas.Control.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using ECOsystem.Utilities;
using ECOsystem.Business.Utilities;
using Newtonsoft.Json;




namespace  ECOsystem.Areas.Control.Controllers.Control
{
    public class BudgetChangesReportController : Controller
    {
        // GET: BudgetChangesReport
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                return View(new ControlFuelsReportsBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(ControlFuelsReportsBase model)
        {
            try
            {
                using (var business = new BudgetChangesReportBusiness())
                {
                    Session["Reporte"] = business.GetReportDownloadData(model);
                    return Json(((DataTable)Session["Reporte"]).Rows.Count, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Movimientos de Presupuesto");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "BudgetChangesReport", this.ToString().Split('.')[2], "Reporte Movimientos de Presupuesto");
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return Redirect("Index");
            }
        }
    }
}