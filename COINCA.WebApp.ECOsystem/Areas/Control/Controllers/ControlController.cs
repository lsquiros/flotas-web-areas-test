﻿/************************************************************************************************************
*  File    : InformationController.cs
*  Summary : Information Controller Actions
*  Author  : Berman Romero
*  Date    : 06/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using System.Linq;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Models.Account;
using System.Collections.Generic;
using System;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Information Controller Class
    /// </summary>
    public class ControlController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new FuelsByCreditBusiness())
                {
                    var customerId = ECOsystem.Utilities.Session.GetCustomerId();
                    customerId = customerId ?? 0;

                    var model = business.RetrieveFuelsByCreditSummary();
                    model.CreditCardList = business.RetrieveCreditCardCloseToLimite(null).ToList();
                    model.DashboardIndicators = business.RetrieveDashboardIndicator((int)customerId);
                    model.Menus = new List<AccountMenus>();

                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}