﻿/************************************************************************************************************
*  File    : AdditionalVehicleFuelByGroupController.cs
*  Summary : AdditionalVehicleFuelByGroup Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 04/12/2016
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* AddOrEdit_VehicleFuelByGroup2
************************************************************************************************************/

using System;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using System.Collections.Generic;
using  ECOsystem.Areas.Control.Models.Identity;
using System.Linq;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using System.Data;
using Newtonsoft.Json;
using ECOsystem.Utilities;
using ECOsystem.Business.Utilities;
using System.Globalization;




namespace  ECOsystem.Areas.Control.Controllers.Control
{
    public class AdditionalVehicleFuelByGroupController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index(bool? Alert)
        {
            try
            {
                if (Alert != null && (bool)Alert)
                {
                    using (var bus = new VehicleFuelByGroupBusiness())
                    {
                        var list = bus.RetrieveAdditionalData(new DistributionParameters(), new List<FuelDistribution>(), Alert);
                        Session["FuelDistributionData"] = list;
                        Session["FuelDistributionAlert"] = Alert;
                        return View(list);
                    }
                }
                return View(new List<FuelDistribution>());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Index(DistributionParameters model)
       {
            try
            {
                using (var bus = new VehicleFuelByGroupBusiness())
                {
                    var list = bus.RetrieveAdditionalData(model, new List<FuelDistribution>());
                    Session["FuelDistributionData"] = list;
                    Session["AdditionalParameters"] = model;
                    return View(list.OrderBy(x => x.VehicleId).ToList());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ApplyChanges(List<FuelDistributionEdition> list)
        {
            try
            {
                var data = (List<FuelDistribution>)Session["FuelDistributionData"];
                data.Where(x => list.Any(z => z.Id != null ? z.Id == x.Id : z.VehicleId == x.VehicleId && z.FuelId == x.FuelId)).ToList().ForEach(s =>
                {
                    s.AdditionalAmount = list.FirstOrDefault().AdditionalAmount == null && list.FirstOrDefault().AdditionalLiters != null ? Convertion(s.LiterPrice, null, list.FirstOrDefault().AdditionalLiters) : list.FirstOrDefault().AdditionalAmount != null ? list.FirstOrDefault().AdditionalAmount : s.AdditionalAmount;
                    s.AdditionalCapacityUnitValue = list.FirstOrDefault().AdditionalLiters == null && list.FirstOrDefault().AdditionalAmount != null ? Convertion(s.LiterPrice, list.FirstOrDefault().AdditionalAmount, null) : list.FirstOrDefault().AdditionalLiters != null ? list.FirstOrDefault().AdditionalLiters : s.AdditionalCapacityUnitValue;
                    s.Selected = true;
                    s.Symbol = list.Count() == 1 ? list.FirstOrDefault().Symbol : s.Symbol;
                });
                Session["FuelDistributionData"] = data;
                return PartialView("_partials/_Grid", data.OrderBy(x => x.VehicleId));
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [EcoAuthorize]
        public ActionResult Save()
        {
            try
            {
                using (var bus = new VehicleFuelByGroupBusiness())
                {
                    var data = (List<FuelDistribution>)Session["FuelDistributionData"];
                    var result = bus.FuelDistributionAddOrEdit(data.Where(x => x.Selected == true).ToList());                    
                    var parameters = Session["AdditionalParameters"] != null ? (DistributionParameters)Session["AdditionalParameters"] : new DistributionParameters();
                    var IssueForId = ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId;
                    bool isAlert = Session["FuelDistributionAlert"] == null ? false : (bool)Session["FuelDistributionAlert"];

                    var list = bus.RetrieveAdditionalData(parameters, new List<FuelDistribution>(), isAlert);
                    list.Where(x => result.Any(z => z.Id == x.Id
                                               || (IssueForId == 101
                                                   && (z.VehicleId == x.VehicleId
                                                       && z.FuelId == x.FuelId)
                                                  )
                                               || (IssueForId == 100
                                                   && (z.DriverId == x.DriverId)
                                                  )
                                              )
                              ).ToList().ForEach(s => {
                                  s.Result = result.Where(x => x.Id == s.Id).ToList().Select(z => z.Result).FirstOrDefault();
                              }
                                                );
                    foreach (var item in result.Where(x => x.Result == 0))
                    {
                        item.AdditionalAmount = item.Symbol == "-" ? item.AdditionalAmount * -1 : item.AdditionalAmount;
                        item.AdditionalLiters = item.Symbol == "-" ? item.AdditionalLiters * -1 : item.AdditionalLiters;
                        new EventLogBusiness().AddLogEvent(LogState.INFO, JsonConvert.SerializeObject(item));
                    }
                    list = list.OrderBy(x => x.VehicleId).ToList();
                    Session["FuelDistributionData"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                var error = string.Format("Error al modificar la distribución adicional de combustible. Detalle técnico: {0}", e.Message);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, error);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TitleError = ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 101 ? "Distribución Adicional de Crédito por Vehículo" : "Distribución Adicional de Crédito por Conductor",
                    TechnicalError = error,
                    ReturnUlr = Url.Action("Index", "AdditionalVehicleFuelByGroup")
                });
            }
        }

        private decimal? Convertion(decimal literPrice, decimal? amount, decimal? liters)
        {
            if (literPrice == 0)
                throw new Exception("Precio del Litro o Galón en nulo o en cero");

            if (liters == null)
            {
                return amount / literPrice;
            }
            else
            {
                return liters * literPrice;
            }
        }

        public ActionResult ExcelReportDownload()
        {
            try
            {
                using (var bus = new VehicleFuelByGroupBusiness())
                {
                    var data = (List<FuelDistribution>)Session["FuelDistributionData"];

                    if (data.Count > 0)
                    {
                        Session["Reporte"] = bus.GetReportData(data);
                        return Json("success");
                    }
                    return Json("nodata");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                    using (var bus = new ReportsUtilities())
                    {
                        if (ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 101)
                        {
                            new EventLogBusiness().AddLogEvent(LogState.INFO, "Distribución Adicional de Crédito por Vehículo");
                            return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "AdditionalVehicleFuelByGroupReport", this.ToString().Split('.')[2], "Reporte Distribución Adicional de Crédito por Vehículo");
                        }
                        else
                        {
                            new EventLogBusiness().AddLogEvent(LogState.INFO, "Distribución Adicional de Crédito por Conductor");
                            return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "AdditionalDriverFuelByGroupReport", this.ToString().Split('.')[2], "Reporte Distribución Adicional de Crédito por Conductor");
                        }
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return Redirect("Index");
            }
        }
        /// <summary>
        /// This method is the task of recovering the detail of the additional budget for both vehicles and drivers.
        /// </summary>
        /// <param name="vehicleId">Receive the vehicle identifier, in the case that it is per driver receives the driver's identifier</param>
        /// <param name="fuelId">Fuel identifier, if it is per driver this data is received in null</param>
        /// <param name="isDriver">It is not used at the moment, it is for future implementations, in case the drivers are assigned FuelId</param>
        /// <returns>The list with the requested details is returned</returns>
        public JsonResult GetVehicleAdditionalDetail(int vehicleId, int? fuelId, int? isDriver) {

            List<BudgetChanges> detailList = null;

            try
            {
                Business.BudgetChangesReportBusiness methods = new Business.BudgetChangesReportBusiness();

                ControlFuelsReportsBase model = new ControlFuelsReportsBase();

                /*if (fuelId == null)
                {
                    model.VehicleId = vehicleId; //Esta linea hace referencia al driverId, porque se esta reutilizando la función 

                }
                else
                {
                    model.VehicleId = vehicleId;
                    model.ReportFuelTypeId = fuelId;

                    detailList = methods.RetrieveBudgetChangesReport(model);
                }*/
                model.VehicleId = vehicleId;
                model.ReportFuelTypeId = fuelId;

                detailList = methods.RetrieveBudgetChangesReport(model);

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
            }

            return Json(detailList);
        }
    }
}
