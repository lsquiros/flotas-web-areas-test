﻿using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    public class ApprovalTransactionController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var bus = new ApprovalTransactionBusiness())
                {
                    var status = bus.GetTypes(1).FirstOrDefault(t => t.Name.ToLower().Contains("pendiente")).Id;
                    var parameters = new ApprovalTransactionParametersModel()
                    {
                        Month = DateTime.Now.Month,
                        Year = DateTime.Now.Year,
                        Status = status
                    };
                    Session["ApprovalParameters"] = parameters;

                    var model = new ApprovalTransactionsBase()
                    {
                        Parameters = parameters,
                        List = bus.ApprovalTransctionsRetrieve(new ApprovalTransactionParametersModel()
                        {
                            Month = DateTime.Now.Month,
                            Year = DateTime.Now.Year,
                            Status = status
                        })
                    };
                    Session["ApprovalTransactionsData"] = model.List;
                    Session["FormName"] = "LoadApprovalTransactionForm";
                    GeneratePager(model.List.Count());
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Index(ApprovalTransactionParametersModel parameters)
        {
            try
            {
                using (var bus = new ApprovalTransactionBusiness())
                {
                    Session["FormName"] = "LoadApprovalTransactionForm";
                    Session["ApprovalParameters"] = parameters;

                    var model = new ApprovalTransactionsBase()
                    {
                        Parameters = parameters,
                        List = bus.ApprovalTransctionsRetrieve(parameters)
                    };

                    Session["ApprovalTransactionsData"] = model.List;
                    GeneratePager(model.List.Count());
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[Obtener Informacion - Conciliación de Transacciones] Parámetros: {0}", new JavaScriptSerializer().Serialize(parameters)));
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                {
                    TitleError = "Conciliación de Transacciones",
                    TechnicalError = e.Message,
                    ReturnUlr = Url.Action("Index", "ApprovalTransaction")
                });
            }
        }

        public ActionResult ApprovalTransactionsSearch(string key)
        {
            try
            {
                var model = (dynamic)null;
                if (string.IsNullOrEmpty(key))
                {
                    model = GetGridPaged((List<ApprovalTransactions>)Session["ApprovalTransactionsData"]);
                }
                else
                {
                    Session["ApprovalTransactionsData"] = ((List<ApprovalTransactions>)Session["ApprovalTransactionsData"]).Where(x => x.VehicleName.ToLower().Contains(key.ToLower()) || x.PlateId.ToLower().Contains(key.ToLower()) || x.CostCenterName.ToLower().Contains(key.ToLower()) || x.Invoice.Contains(key)).ToList();
                    model = Session["ApprovalTransactionsData"];
                }
                Session["SearchKey"] = key;
                return PartialView("_Partials/_Grid", model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ApprovalTransactionsChange(int transactionid, string invoice, int? costcenterid, int? servicestationid, string status, string columnvalues)
        {
            try
            {
                using (var bus = new ApprovalTransactionBusiness())
                {
                    var list = (List<ApprovalTransactions>)Session["ApprovalTransactionsData"];
                    var bacId = list.Where(x => x.TransactionId == transactionid).FirstOrDefault().AffiliateNumber;

                    if (!string.IsNullOrEmpty(invoice))
                    {
                        list.Where(x => x.TransactionId == transactionid).ToList().ForEach(s => s.Invoice = invoice);
                        int invoicecount = bus.ApprovalTransactionsDuplicateNumber(invoice, bacId, (int)ECOsystem.Utilities.Session.GetCustomerId());   //list.Where(x => x.Invoice == invoice).Count();

                        if (invoicecount > 1)
                        {
                            list.Where(x => x.Invoice == invoice).ToList().ForEach(s => s.InvoiceCount = invoicecount);
                        }
                        else
                        {
                            list.Where(x => x.TransactionId == transactionid).ToList().ForEach(s => { s.Change = true; s.InvoiceCount = 1; });
                        }
                    }
                    else if (list.Where(x => x.TransactionId == transactionid).FirstOrDefault().InvoiceCount == 1)
                    {
                        if (costcenterid != null)
                        {
                            list.Where(x => x.TransactionId == transactionid).ToList().ForEach(s => { s.CostCenterId = (int)costcenterid; s.Change = true; });
                        }
                        else if (servicestationid != null)
                        {
                            list.Where(x => x.TransactionId == transactionid).ToList().ForEach(s => { s.ServiceStationId = (int)servicestationid; s.Change = true; });
                        }
                        else if (!string.IsNullOrEmpty(status))
                        {
                            var statusname = bus.GetTypes(null).Where(x => x.Id == new Guid(status)).FirstOrDefault().Name;
                            list.Where(x => x.TransactionId == transactionid).ToList().ForEach(s => { s.Status = new Guid(status); s.StatusName = statusname; s.Change = true; });
                        }
                        else if (!string.IsNullOrEmpty(columnvalues))
                        {
                            var columnlist = new JavaScriptSerializer().Deserialize<List<ApprovalTransactionsParameters>>(columnvalues);
                            list.Where(x => x.TransactionId == transactionid).ToList().ForEach(s => { s.DynamicColumnValuesXML = ECOsystem.Utilities.Miscellaneous.GetXML(columnlist, typeof(ApprovalTransactionsParameters).Name); s.Change = true; });
                        }
                    }

                    Session["ApprovalTransactionsData"] = list;

                    return ShowResults(list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetCountsToSave()
        {
            try
            {
                var list = (List<ApprovalTransactions>)Session["ApprovalTransactionsData"];
                Guid statusauth = new Guid("8BAFAEE7-0590-4441-B451-EA8AC9394143");
                return Json(new { authCont = list.Where(x => x.Status == statusauth && (bool)x.Change).Count(), changeCount = list.Where(x => x.Status != statusauth && (x.Change != null && (bool)x.Change)).Count() });
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ApprovalTransactionsSave()
        {
            try
            {
                using (var bus = new ApprovalTransactionBusiness())
                {
                    var list = (List<ApprovalTransactions>)Session["ApprovalTransactionsData"];
                    var changes = list.Where(x => (x.Change != null && (bool)x.Change)).ToList();

                    bus.ApprovalTransctionsAddOrEdit(changes);
                    ViewBag.AjaxGrid = true;
                    ViewBag.FormName = "LoadApprovalTransactionForm";

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[Modificación de transacciones - Conciliación de Transacciones]{0}", new JavaScriptSerializer().Serialize(changes.Select(x => new { x.TransactionId, x.StatusName }).ToList())));

                    var returnList = bus.ApprovalTransctionsRetrieve((ApprovalTransactionParametersModel)Session["ApprovalParameters"]);
                    Session["ApprovalTransactionsData"] = returnList;
                    Session["SearchKey"] = null;
                    GeneratePager(returnList.Count());
                    return PartialView("_Partials/_Grid", GetGridPaged(returnList.ToList()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                {
                    TitleError = "Modificación de transacciones - Conciliación de Transacciones",
                    TechnicalError = e.Message,
                    ReturnUlr = Url.Action("Index", "ApprovalTransaction")
                });
            }
        }

        [HttpPost]
        public PartialViewResult LoadApprovalTrans(int? page)
        {
            try
            {
                var model = (List<ApprovalTransactions>)Session["ApprovalTransactionsData"];

                #region PagerInfo                        
                ViewBag.PageNum = page != null ? (int)page : 1;
                Session["PageNum"] = page;
                ViewBag.AjaxGrid = true;
                ViewBag.FormName = "LoadApprovalTransactionForm";

                GeneratePager(model.Count());
                #endregion

                if (page == null || page == 1)
                {
                    return PartialView("_partials/_Grid", model);
                }
                else
                {
                    var modelPage = model.Skip(((int)page - 1) * 50).Take(model.Count()).ToList();
                    return PartialView("_partials/_Grid", modelPage);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return null;
            }
        }

        List<ApprovalTransactions> GetGridPaged(List<ApprovalTransactions> list)
        {
            int page = Session["PageNum"] != null ? (int)Session["PageNum"] : 1;
            ViewBag.PageNum = page != null ? (int)page : 1;
            Session["PageNum"] = page;
            ViewBag.AjaxGrid = true;
            ViewBag.FormName = "LoadApprovalTransactionForm";
            GeneratePager(list.Count());

            return list.Skip(((int)page - 1) * 50).Take(list.Count()).ToList();
        }

        void GeneratePager(int count)
        {
            var pager = new GridMvc.Pagination.GridPager();
            pager.ItemsCount = count;
            pager.MaxDisplayedPages = 10;
            pager.PageSize = 50;
            ViewBag.PageModel = pager;
        }

        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            try
            {
                DataSet report = new DataSet();
                using (var business = new ApprovalTransactionBusiness())
                {
                    Session["Reporte"] = business.GenerateApprovalReportData((List<ApprovalTransactions>)Session["ApprovalTransactionsData"], (ApprovalTransactionParametersModel)Session["ApprovalParameters"]);
                    Session["TransactionType"] = 7;

                    return Json("success");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataSet report = new DataSet();
                if (Session["Reporte"] != null)
                {
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Conciliación de Transacciones");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "ApprovalTransactionsReport", ToString().Split('.')[2], "Reporte Conciliación de Transacciones");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                var list = (List<ApprovalTransactions>)Session["ApprovalTransactionsData"];
                ViewBag.ErrorReport = 1;
                Session["FormName"] = "LoadApprovalTransactionForm";
                GeneratePager(list.Count());
                using (var business = new ApprovalTransactionBusiness())
                {
                    var model = new ApprovalTransactionsBase()
                    {
                        Parameters = (ApprovalTransactionParametersModel)Session["ApprovalParameters"],
                        List = list
                    };
                    return View("Index", model);
                }
            }
        }

        [HttpPost]
        public ActionResult AddOrEditImportApprovalTransactions()
        {
            try
            {
                HttpPostedFileBase file = Request.Files[0];
                var list = new List<ApprovalTransactionsImport>();
                var fileName = (dynamic)null;

                #region Go through the file
                if (file != null && file.ContentLength > 0)
                {
                    fileName = Path.GetFileName(file.FileName);
                    var path = Path.GetTempPath() + fileName;
                    file.SaveAs(path);

                    using (TextFieldParser csvReader = new TextFieldParser(path))
                    {
                        list = GetFileData(csvReader, ",");
                    }

                    if (list.Count == 0)
                    {
                        using (TextFieldParser csvReader = new TextFieldParser(path))
                        {
                            list = GetFileData(csvReader, ";");
                        }
                    }
                }
                #endregion 

                var approvalList = new List<ApprovalTransactions>();
                var returnList = new List<ApprovalTransactions>();
                using (var bus = new ApprovalTransactionBusiness())
                {
                    approvalList = bus.GetAprovalFromImport(list.Where(x => x.IsException == false).ToList()).ToList();
                }

                var downloadList = list.Where(x => !approvalList.Any(y => y.LineNumber == x.LineNumber) || x.IsException).Select(x => x).ToList();
                //downloadList.Where(x => x.Description == null).ToList().ForEach(x => x.Description = "Transacción no encontrada");
                downloadList.Where(x => x.Description == null).ToList().ForEach(x => x.Description = "No se encontró el número de autorizacion");
                Session["NonApprovalList"] = downloadList;
                Session["FileNameToDownload"] = fileName;

                //Information to the view
                ViewBag.DownloadResultFile = downloadList.Count > 0 ? true : false;
                ViewBag.ResultData = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"),
                                                                          approvalList.Select(x => x.AllowProcess).FirstOrDefault() ? "Autómatico" : "Manual",
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(approvalList.Count), ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(downloadList.Count),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(approvalList.Count + downloadList.Count),
                                                                          approvalList.Select(x => x.AllowProcess).FirstOrDefault() ? "Autorizadas" : "Pendientes de Autorizar");

                returnList = approvalList.Where(x => !x.AlreadyApproved).GroupBy(x => x.TransactionId).Select(x => x.First()).ToList();

                if (!returnList.Select(x => x.AllowProcess).FirstOrDefault()) approvalList.ToList().ForEach(x => x.Change = true);

                Session["ApprovalTransactionsData"] = returnList;

                #region Event log
                new EventLogBusiness().AddLogEvent(LogState.INFO, "Importación de Conciliación de Transacciones");
                #endregion

                return View("Index", new ApprovalTransactionsBase()
                {
                    Parameters = (ApprovalTransactionParametersModel)Session["ApprovalParameters"],
                    List = returnList
                });
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e, true);
                return RedirectToAction("Index");
            }
        }

        private List<ApprovalTransactionsImport> GetFileData(TextFieldParser csvReader, string delimiter)
        {
            int lineNumber = 0;
            var list = new List<ApprovalTransactionsImport>();
            csvReader.SetDelimiters(delimiter);
            csvReader.HasFieldsEnclosedInQuotes = true;

            while (!csvReader.EndOfData)
            {
                string[] colFields = csvReader.ReadFields();
                foreach (var column in colFields.Where(x => x != ""))
                {
                    try
                    {
                        DateTime date;
                        Decimal number;
                        var item = new ApprovalTransactionsImport();

                        //Archivo no tiene el formato correcto
                        // Se cambia la regla para aceptar lineas con mas de 5 columnas, la unica restriccion es que vengan menos de 5 columnas
                        //if (colFields.Length > 5) if (colFields[5] != string.Empty || colFields[6] != string.Empty) break;
                        if (colFields.Length < 5 || colFields.Length > 5)
                        {
                            item.IsException = true;
                            item.AuthorizationNumber = colFields[0];
                            item.Reference = colFields[0];
                            item.Date = colFields.Length > 1 ? colFields[1] : "";
                            item.Odometer = 0;
                            item.Litters = 0;
                            item.Amount = 0;
                            item.AmountText = "";
                            item.Message = "";
                            item.LineNumber = lineNumber;
                            item.Description = "Número de columnas no cumple para poder ser procesada";
                            list.Add(item);
                            lineNumber++;
                            break;
                        }

                        if (!DateTime.TryParse(TryToGetDate(colFields[1]), out date))
                        {
                            item.IsException = true;
                            item.AuthorizationNumber = colFields[0];
                            item.Reference = colFields[0];
                            item.Date = colFields[1];
                            if (colFields[2].Contains("R:"))
                            {
                                item.Odometer = Convert.ToInt32(new string(colFields[2].Split(new string[] { "R:" }, StringSplitOptions.None)[1].Split(new string[] { "C:" }, StringSplitOptions.None)[0].Where(x => Char.IsDigit(x)).ToArray()));
                            }

                            if (colFields[3].Contains(".."))
                            {
                                item.Litters = Convert.ToDecimal(colFields[3].Replace("..", string.Empty));
                            }
                            else
                            {
                                if (!Decimal.TryParse(colFields[3], out number))
                                {
                                    item.Litters = 0;
                                }
                                else
                                {
                                    item.Litters = Convert.ToDecimal(colFields[3]);
                                }
                            }

                            item.Amount = Convert.ToDecimal(colFields[4]);
                            item.AmountText = colFields[4];
                            item.Message = colFields[2];
                            item.LineNumber = lineNumber;
                            item.Description = "Formato de fecha incorrecto";
                            list.Add(item);
                            lineNumber++;
                            break;
                        }

                        if (colFields[2] == "" || colFields[3] == "")
                        {
                            item.IsException = true;
                            item.AuthorizationNumber = colFields[0];
                            item.Reference = colFields[0];
                            item.Date = colFields.Length > 1 ? colFields[1] : "";
                            item.Odometer = 0;
                            item.Litters = 0;
                            item.Amount = 0;
                            item.AmountText = "";
                            item.Message = "";
                            item.LineNumber = lineNumber;
                            item.Description = "Columna de Kilometraje o Litros no debe venir vacía";
                            list.Add(item);
                            lineNumber++;
                            break;
                        }

                        item.AuthorizationNumber = colFields[0];
                        item.Reference = colFields[0];
                        item.Date = colFields[1];
                        if (string.IsNullOrEmpty(colFields[3])) break;
                        if (colFields[2].Contains("R:"))
                        {
                            item.Odometer = Convert.ToInt32(new string(colFields[2].Split(new string[] { "R:" }, StringSplitOptions.None)[1].Split(new string[] { "C:" }, StringSplitOptions.None)[0].Where(x => Char.IsDigit(x)).ToArray()));
                        }

                        if (colFields[3].Contains(".."))
                        {
                            item.Litters = Convert.ToDecimal(colFields[3].Replace("..", string.Empty));
                        }
                        else
                        {
                            if (!Decimal.TryParse(colFields[3], out number))
                            {
                                item.Litters = 0;
                            }
                            else
                            {
                                item.Litters = Convert.ToDecimal(colFields[3]);
                            }
                        }

                        item.Amount = Convert.ToDecimal(colFields[4]);
                        item.AmountText = colFields[4];
                        item.Message = colFields[2];
                        item.LineNumber = lineNumber;
                        item.IsException = false;
                        list.Add(item);
                        lineNumber++;
                        break;
                    }
                    catch (Exception e)
                    {
                        ECOsystem.Utilities.Session.SentrySupport(e);

                        break;
                    }
                }
            }
            return list;
        }

        [HttpPost]
        public ActionResult DonwloadImportResults()
        {
            try
            {
                var list = (List<ApprovalTransactionsImport>)Session["NonApprovalList"];
                var fileName = Session["FileNameToDownload"].ToString();

                Session["NonApprovalList"] = null;
                Session["FileNameToDownload"] = null;
                using (var ms = new MemoryStream())
                {
                    var sw = new StreamWriter(ms, System.Text.Encoding.GetEncoding(1252));

                    #region HeaderReport
                    sw.WriteLine("Fecha: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                    sw.Flush();
                    sw.WriteLine("Archivo Origen: " + fileName);
                    sw.Flush();
                    //sw.WriteLine("# de Línea,No. de Referencia,Fecha Transacción,Concepto,Litros,Monto,Descripción");
                    sw.WriteLine("# de Línea,No. de Autorización,Fecha Transacción,Concepto,Litros,Monto,Descripción");
                    sw.Flush();
                    #endregion

                    foreach (var item in list)
                    {
                        //sw.WriteLine(item.LineNumber + 1 + "," + item.Reference + "," + item.Date + ",R:" + item.Odometer + ",\"" + +item.Litters + "\",\"" + item.AmountText + "\"" + "," + item.Description);
                        sw.WriteLine(item.LineNumber + 1 + "," + item.AuthorizationNumber + "," + item.Date + ",R:" + item.Odometer + ",\"" + +item.Litters + "\",\"" + item.AmountText + "\"" + "," + item.Description);
                        sw.Flush();
                    }
                    return new FileStreamResult(new MemoryStream(ms.ToArray()), "text/plain")
                    {
                        FileDownloadName = "Resultado de Transacciones NO Conciliadas.csv"
                    };
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DonwloadExampleFile()
        {
            try
            {
                byte[] SummaryFile = System.IO.File.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data/Ejemplo_Transacciones_a_Conciliar.csv")));

                return new FileStreamResult(new MemoryStream(SummaryFile), "text/csv")
                {
                    FileDownloadName = "Ejemplo_Transacciones_a_Conciliar.csv"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        private string TryToGetDate(string date)
        {
            try
            {
                return ECOsystem.Utilities.Miscellaneous.ConvertDateFromApprovalToDateTime(date).ToString();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return date;
            }
        }

        /// <summary>
        /// Change status value to Authorized to list of transactions.
        /// It will change only transactions with Invoice count = 1 (no duplicated invoice)
        /// </summary>
        /// <param name="ChainValues">Pipe delimited list of transaction ids</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AuthorizeSelectedTransactions(string ChainValues)
        {
            try
            {
                var list = (List<ApprovalTransactions>)Session["ApprovalTransactionsData"];
                string autorizado_status = "autorizado";

                List<double> Ids = new List<double>();

                foreach (string x in ChainValues.Split('|'))
                {
                    Ids.Add(Convert.ToInt32(x));
                }

                using (var bus = new ApprovalTransactionBusiness())
                {
                    var status = bus.GetTypes(null).FirstOrDefault(t => t.Name.ToLower().Contains(autorizado_status) && (t.Name.ToLower().Trim().Length == autorizado_status.Length)).Id;
                    var statusname = bus.GetTypes(null).Where(x => x.Id == status).FirstOrDefault().Name;
                    list.Where(x => Ids.Contains(x.TransactionId)).ToList().ForEach(s =>
                    {
                        if (s.InvoiceCount == 1)
                        {
                            s.Status = status;
                            s.StatusName = statusname;
                            s.Change = true;
                        }
                    });
                }

                Session["ApprovalTransactionsData"] = list;


                return ShowResults(list);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                {
                    TitleError = "Modificación de transacciones - Conciliación de Transacciones",
                    TechnicalError = e.Message,
                    ReturnUlr = Url.Action("Index", "ApprovalTransaction")
                });
            }
        }

        /// <summary>
        /// Return list of transaction to view. It applies filter settings
        /// </summary>
        /// <param name="list">Transactions list</param>
        /// <returns>View of transactions </returns>
        private ActionResult ShowResults(List<ApprovalTransactions> list)
        {
            try
            {
                var returnlist = (dynamic)null;

                //Valida si existe un filtro de busqueda en la modificacion de transacciones
                if (Session["SearchKey"] != null)
                {
                    returnlist = list.Where(x => x.VehicleName.Contains(Session["SearchKey"].ToString()) || x.PlateId.Contains(Session["SearchKey"].ToString())
                                                                                                 || x.CostCenterName.Contains(Session["SearchKey"].ToString()) || x.Invoice.Contains(Session["SearchKey"].ToString())); ;
                }
                else
                {
                    returnlist = GetGridPaged(list);
                }

                return PartialView("_Partials/_Grid", returnlist);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
