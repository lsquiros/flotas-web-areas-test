﻿/************************************************************************************************************
*  File    : TransactionsDetailReportController.cs
*  Summary : Transactions Detail Report Controller Actions
*  Author  : Stefano Quiros
*  Date    : 02/16/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Models.Identity;
using System.Data;
using System;
using System.Globalization;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;




namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Transactions Report Class
    /// </summary>
    public class TransactionDetailReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new TransactionDetailReportBusiness())
                {
                    return View(new TransactionsReportBase());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(TransactionsReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new TransactionDetailReportBusiness())
                {
                    var modelV = business.RetrieveTransactionsReport(model.Parameters);

                    return View(modelV);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult Admin()
        {
            try
            {
                using (var business = new TransactionsReportBusiness())
                {
                    return View(business.RetrieveTransactionsReportDeniedByPartner(new ControlFuelsReportsBase()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));
                if (model != null)
                {
                    //model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                    model.StartDate = model.StartDate;
                    model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
                }
                if (model.Month != null && Convert.ToInt32(model.Month) > 0 && model.Year != null && Convert.ToInt32(model.Year) > 0)
                {
                    if (model.StartDate == null)
                    {
                        model.StartDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1, 0, 0, 0);
                    }

                    if (model.EndDate == null)
                    {
                        model.EndDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month)), 23, 59, 59);
                    }
                }

                //byte[] report;
                DataTable report = new DataTable();

                using (var business = new TransactionDetailReportBusiness())
                {
                    //report = business.GenerateTransactionsReportExcel(model);
                    report = business.GetReportDownloadData(model);

                    if (model.StartDate == null)
                    {
                        int lastDay = DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month));
                        string MonthName = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1).ToString("MMM", CultureInfo.InvariantCulture);
                        Session["StartDate"] = MonthName + 1 + " /" + model.Year;
                        Session["EndDate"] = MonthName + lastDay + " /" + model.Year;
                    }
                    else
                    {
                        DateTime fecha_inicial = Convert.ToDateTime(model.StartDate);
                        int day_inicial = fecha_inicial.Day;
                        string MonthNameInicial = new DateTime(fecha_inicial.Year, fecha_inicial.Month, day_inicial).ToString("MMM", CultureInfo.InvariantCulture);
                        Session["StartDate"] = MonthNameInicial + day_inicial + " /" + fecha_inicial.Year;
                        DateTime fecha_final = Convert.ToDateTime(model.EndDate);
                        int day_final = fecha_final.Day;
                        string MonthNameFinal = new DateTime(fecha_final.Year, fecha_final.Month, day_final).ToString("MMM", CultureInfo.InvariantCulture);
                        Session["EndDate"] = MonthNameFinal + day_final + " /" + fecha_final.Year;
                    }

                    Session["Reporte"] = report;
                    Session["FilterVehicle"] = model.FilterVehicle;

                    if (ECOsystem.Utilities.Session.GetCustomerId() != null)
                        return View("Index", business.RetrieveTransactionsReport(model));

                    return View("Admin", business.RetrieveTransactionsReport(model));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                string reportName = string.Empty;
                string username = ECOsystem.Utilities.Session.GetUserInfo().DecryptedName;
                if (username == null) username = ECOsystem.Utilities.Session.GetPartnerInfo().Name;
                string nameDownload = "Reporte Transaccional Detallado " + username + Session["StartDate"].ToString() + " - " + Session["EndDate"].ToString();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                if (Convert.ToUInt32(Session["FilterVehicle"]) == 1)
                {
                    reportName = "TransactionDetailReport";
                }
                else
                {
                    reportName = "TransactionDetailReportGV";
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Transaccional Detallado");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), reportName, this.ToString().Split('.')[2], nameDownload);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                using (var business = new TransactionDetailReportBusiness())
                {
                    return View("Index", new TransactionsReportBase());
                }
            }
        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new TransactionsReportBusiness())
            {
                return View("Index", business.RetrieveTransactionsReport(model));
            }
        }
    }
}
