﻿/************************************************************************************************************
*  File    : ComparativeEfficiencyFuelsReportController.cs
*  Summary : Efficiency Fuels Report Controller Actions
*  Author  : Berman Romero
*  Date    : 20/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Models.Identity;
using System;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Current Fuels Report Class
    /// </summary>
    public class ComparativeEfficiencyFuelsReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>        
        public ActionResult Index()
        {
            try
            {
                using (var business = new ComparativeEfficiencyFuelsReportBusiness())
                {
                    var modelF = business.RetrieveComparativeEfficiencyFuelsReport(new ControlFuelsReportsBase());

                    ScoreGlobalReportBase ScoreGlobalReportBase = new ScoreGlobalReportBase();
                    ScoreGlobalReportBase.Parameters.Year = modelF.Parameters.Year;
                    ScoreGlobalReportBase.Parameters.Month = modelF.Parameters.Month;
                    ScoreGlobalReportBase.Parameters.StartDate = modelF.Parameters.StartDate;
                    ScoreGlobalReportBase.Parameters.EndDate = modelF.Parameters.EndDate;
                    ScoreGlobalReportBase.Parameters.ReportType = "S";

                    modelF.ScoreGlobalReportBase = ScoreGlobalReportBase;

                    return View(modelF);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(ComparativeEfficiencyFuelsReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new ComparativeEfficiencyFuelsReportBusiness())
                {

                    var modelF = business.RetrieveComparativeEfficiencyFuelsReport(model.Parameters);

                    ScoreGlobalReportBase ScoreGlobalReportBase = new ScoreGlobalReportBase();
                    ScoreGlobalReportBase.Parameters.Year = model.Parameters.Year;
                    ScoreGlobalReportBase.Parameters.Month = model.Parameters.Month;
                    ScoreGlobalReportBase.Parameters.StartDate = model.Parameters.StartDate;
                    ScoreGlobalReportBase.Parameters.EndDate = model.Parameters.EndDate;
                    ScoreGlobalReportBase.Parameters.ReportType = "S";

                    modelF.ScoreGlobalReportBase = ScoreGlobalReportBase;

                    return View(modelF);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));
                if (model != null)
                {
                    model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                    model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
                }
                byte[] report;
                using (var business = new ComparativeEfficiencyFuelsReportBusiness())
                {
                    report = business.GenerateComparativeEfficiencyFuelsReportExcel(model);
                }
                return new FileStreamResult(new MemoryStream(report), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "ExcelReport.xlsx"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult RetrieveScoreGlobalReport(ScoreGlobalReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new ComparativeEfficiencyFuelsReportBusiness())
                {
                    model.Parameters.ReportType = "S";
                    return PartialView("_partials/_Detail", business.GetGlobalScoreSpeedByVehicle(model));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new ComparativeEfficiencyFuelsReportBusiness())
            {
                return View("Index", business.RetrieveComparativeEfficiencyFuelsReport(model));
            }
        }
    }
}