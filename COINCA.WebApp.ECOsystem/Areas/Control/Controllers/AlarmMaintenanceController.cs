﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem.Models.Core;
using ECOsystem.Models.Miscellaneous;



namespace  ECOsystem.Areas.Control.Controllers
{
    public class AlarmMaintenanceController : Controller
    {
        private const int cEntityTypeIdVehicle = 400;

        // GET: AlarmMaintenance
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Load Alarm
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alarmTriggerId"></param>
        /// <param name="entityTypeId"></param>
        /// <param name="title"></param>
        /// <param name="entityTypeIdLawSpeed"></param>
        /// <param name="entityTypeIdCustomerSpeed"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult LoadAlarmMaintenance(int id, string codView, ECOsystem.Models.Core.AlarmsModels alarm = null)
        {
            try
            {
                using (var business = new ECOsystem.Business.Core.AlarmBusiness())
                {
                    AlarmBase Model = new AlarmBase();
                    Model.ListAlarm = new List<ECOsystem.Models.Core.AlarmsModels>();
                    Model.ListAlarm = business.RetrieveAlarm(AlarmTriggerId: null, CustomerId: ECOsystem.Utilities.Session.GetCustomerId(),
                                                             EntityTypeId: cEntityTypeIdVehicle, AlarmId: null, EntityId: id, CodView: codView);

                    AlarmPropertiesModel propertiesAlarm = new AlarmPropertiesModel();
                    propertiesAlarm.CodView = codView;
                    Model.ListPropertiesAlarm = LoadPropertiesAlarm(propertiesAlarm);

                    return PartialView("~/Views/Shared/_AlarmMaintenance.cshtml", Model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "GeoFences")
                });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddOrEditAlarmMaintenance(List<AlarmModel> pListAlarm) // ECOsystem.Models.Core.AlarmsModels model)
        {
            try
            {
                foreach (AlarmModel alarm in pListAlarm)
                {
                    alarm.CustomerId = Convert.ToInt32(ECOsystem.Utilities.Session.GetCustomerId());
                    alarm.EntityTypeId = cEntityTypeIdVehicle;

                    if (alarm.Phone == null || alarm.Phone == "")
                        alarm.Phone = "";
                    else
                    {
                        alarm.SMS = true;
                        if (alarm.Email == null)
                            alarm.Email = "";
                    }

                    using (var business = new ECOsystem.Business.Core.AlarmBusiness())
                    {
                        business.AddOrEditAlarm(alarm);
                    }
                }

                return Json("success");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "GeoFences")
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //[HttpPost]
        public List<AlarmPropertiesModel> LoadPropertiesAlarm(AlarmPropertiesModel propertiesAlarm)
        {
            List<AlarmPropertiesModel> listPropertiesAlarm = new List<AlarmPropertiesModel>();
            try
            {
                using (var business = new ECOsystem.Business.Core.AlarmBusiness())
                {
                    listPropertiesAlarm = business.AlarmPropertiesRetrieve(propertiesAlarm);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
            }
            return listPropertiesAlarm;
        }
    }
}