﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Models;
using  ECOsystem.Areas.Control.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Business;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    public class ConsolidatedReportController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DownloadReport(ConsolidatedReportParameters parameters)
        {
            try
            {
                if (parameters.StartDate != null && parameters.EndDate != null)
                {
                    parameters.Year = null;
                    parameters.Month = null;
                }
                else
                {
                    parameters.StartDate = null;
                    parameters.EndDate = null;
                }

                using (var business = new ConsolidateReportBusiness())
                {
                    var ds = business.GetReportDownloadData(parameters);
                    Session["Reporte"] = ds;
                    return Json(ds.Tables[0].Rows.Count);
                }



            }
            catch (Exception e)
            {

            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataSet report = new DataSet();
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de Consolidado de Mantenimientos");
                        return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "ConsolidateReport", this.ToString().Split('.')[2], "Reporte Consolidado de Mantenimientos");
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return Redirect("Index");
            }
        }
    }
}