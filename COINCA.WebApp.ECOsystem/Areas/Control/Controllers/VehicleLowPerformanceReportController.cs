﻿/************************************************************************************************************
*  File    : VehicleLowPerformanceReportController.cs
*  Summary : Vehicle Low Performance Report Controller Actions
*  Author  : Andrés Oviedo Brenes
*  Date    : 26/01/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Web.Mvc;
using  ECOsystem.Areas.Control.Business.Control;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using  ECOsystem.Areas.Control.Models.Identity;
using System;



using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Controllers.Control
{
    /// <summary>
    /// Vehicle Low Performance Report Controller Class
    /// </summary>
    public class VehicleLowPerformanceReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new VehicleLowPerformanceReportBusiness())
                {
                    VehicleLowPerformanceReports modelI = new VehicleLowPerformanceReports();
                    var model = new VehicleLowPerformanceReportBase
                    {
                        Data = modelI,
                        List = business.RetrieveVehicleLowPerformanceReport(new VehicleLowPerformanceReportBase())
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(VehicleLowPerformanceReportBase model)
        {
            try
            {
                if (model == null) RedirectToAction("Index");

                using (var business = new VehicleLowPerformanceReportBusiness())
                {
                    if (model.ReportCriteriaId == 2000)
                    {
                        model.Month = null;
                        model.Year = null;
                    }

                    VehicleLowPerformanceReports modelI = new VehicleLowPerformanceReports();
                    var modelR = new VehicleLowPerformanceReportBase
                    {
                        Data = modelI,
                        List = business.RetrieveVehicleLowPerformanceReport(model)
                    };
                    modelR.StartDateStr = model.StartDateStr;
                    modelR.EndDateStr = model.EndDateStr;
                    modelR.Month = model.Month;
                    modelR.Year = model.Year;
                    modelR.ReportCriteriaId = model.ReportCriteriaId;
                    return View(modelR);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

                byte[] report = new byte[10];

                using (var business = new VehicleLowPerformanceReportBusiness())
                {
                    if (model.ReportCriteriaId == 2000)
                    {
                        model.Month = null;
                        model.Year = null;
                    }
                    using (var excel = new ExcelReport())
                    {
                        var model2 = new VehicleLowPerformanceReportBase();

                        model2.Year = model.Year;
                        model2.Month = model.Month;
                        model2.StartDate = model.StartDate;
                        model2.EndDate = model.EndDate;
                        model2.VehicleId = model.VehicleId;

                        report = excel.CreateSpreadsheetWorkbook("Vehículos con Bajo Rendimiento", business.RetrieveVehicleLowPerformanceReport(model2).ToList());
                    }
                }
                return new FileStreamResult(new MemoryStream(report), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "ExcelReport.xlsx"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Prints the report
        /// </summary>
        public ActionResult PrintReport(string modelStr)
        {
            var model = JsonConvert.DeserializeObject<VehicleLowPerformanceReportBase>(modelStr);
            if (model == null) RedirectToAction("Index");

            if (model.ReportCriteriaId == 2000)
            {
                model.Month = null;
                model.Year = null;
            }

            using (var business = new VehicleLowPerformanceReportBusiness())
            {
                VehicleLowPerformanceReports modelI = new VehicleLowPerformanceReports();
                var modelR = new VehicleLowPerformanceReportBase
                {
                    Data = modelI,
                    List = business.RetrieveVehicleLowPerformanceReport(model)
                };
                ViewBag.PrintView = true;
                modelR.StartDateStr = model.StartDateStr;
                modelR.EndDateStr = model.EndDateStr;
                modelR.Month = model.Month;
                modelR.Year = model.Year;

                modelR.ReportCriteriaId = model.ReportCriteriaId;
                return View("~/Views/VehicleLowPerformanceReport/Index.cshtml", modelR);
            }
        }
    }
}

