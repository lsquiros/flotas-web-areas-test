﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.Routing;
using System.Web.SessionState;
using ECOsystem.Business.Administration;
using  ECOsystem.Areas.Control.Utilities;
using ECOsystem.Business.Core;

namespace  ECOsystem.Areas.Control.Models.Identity
{
    /// <summary>
    /// Security
    /// </summary>
    public class EcoAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Control the access by on the user's permissions
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentException("httpContext");
            
            //Validates if the user is already logged
            var acc = new GetAccountUtility();

            acc.GetUserInformation(ref httpContext); 

            if (!httpContext.Request.IsAuthenticated)
                return false; 

            var controller = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString().ToUpper();
            var action = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString().ToUpper();
            var user = ECOsystem.Utilities.Session.GetUserInfo();
            var customerInfo = ECOsystem.Utilities.Session.GetCustomerInfo();

            if (customerInfo != null && !customerInfo.ModuleControl)
            {
                return false;
            }

            using (var context = new UserRolesPermissionsBusiness())
                return context.UserPermissionValidation(user.EncryptedUserName, controller, action, user.RoleId);

        }

        /// <summary>
        /// Validates the access if the user is not logged in
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Unauthorized" }, { "controller", "Account" } });

                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "UnauthorizedPartial" }, { "controller", "Account" } });
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Login" }, { "controller", "Account" } });

                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}

