﻿/************************************************************************************************************
*  File    : TransactionsReportUtility.cs
*  Summary : Sort the data for the reports which are going to use rdlc files
*  Author  : Henry Retana
*  Date    : 11/03/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using ECOsystem.Utilities.Helpers;
using System.Data;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Business.Administration;
using  ECOsystem.Areas.Control.Business.Control;
using System.Globalization;

namespace  ECOsystem.Areas.Control.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public class TransactionsReportUtility : IDisposable
    {
        /// <summary>
        /// Get the data table ready for the Denied Transactions Report by Customer
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable DeniedTransactionsReport(IEnumerable<TransactionDenyReportModel> list, DateTime startDate, DateTime endDate)
        {
        
            var CustomerName = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;

            using (var dt = new DataTable())
            {
                dt.Columns.Add("IsCustomerBranch");
                dt.Columns.Add("CustomerBranchName");
                dt.Columns.Add("PaymentInstrument");
                dt.Columns.Add("PaymentInstrumentType");
                dt.Columns.Add("ResponseCode");
                dt.Columns.Add("ResponseCodeDescription");
                dt.Columns.Add("Message");
                dt.Columns.Add("CreditCardHolder");
                dt.Columns.Add("InsertDate");
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("ServiceStationName");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("CountryName");
                dt.Columns.Add("ExchangeValue");
                dt.Columns.Add("RealAmountFormatted");
                dt.Columns.Add("CustomerName");

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["CustomerBranchName"] = item.CustomerBranchName;
                    dr["IsCustomerBranch"] = item.IsCustomerBranch;
                    dr["PaymentInstrument"] = item.PaymentInstrument;
                    dr["PaymentInstrumentType"] = item.PaymentInstrumentType;
                    dr["ResponseCode"] = item.ResponseCode;
                    dr["ResponseCodeDescription"] = item.ResponseCodeDescription;
                    dr["Message"] = item.Message;
                    dr["CreditCardHolder"] = item.CreditCardHolder;
                    dr["InsertDate"] = item.InsertDate;
                    dr["TerminalId"] = item.TerminalId;
                    dr["ServiceStationName"] = item.ServiceStationName;
                    dr["InitRangeDate"] = startDateStr;
                    dr["FiniRangeDate"] = endDateStr;
                    dr["CountryName"] = item.CountryName;
                    dr["ExchangeValue"] = item.ExchangeValue;
                    dr["RealAmountFormatted"] = item.RealAmount;
                    dr["CustomerName"] = item.CustomerName;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Get the datatable ready for the Transactions Report by Customer
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable TransactionsReportData(IEnumerable<TransactionsReport> list, DateTime startDate, DateTime endDate)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("IsCustomerBranch");
                dt.Columns.Add("CustomerBranchName");
                dt.Columns.Add("Invoice");
                dt.Columns.Add("ElectronicInvoice");
                dt.Columns.Add("MerchantDescription");
                dt.Columns.Add("SystemTraceCode");
                dt.Columns.Add("FuelName");
                dt.Columns.Add("State");
                dt.Columns.Add("CostCenterId");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("VehicleGroupId");
                dt.Columns.Add("VehicleGroupName");
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("HolderName");
                dt.Columns.Add("Date");
                dt.Columns.Add("CapacityUnitValue"); 
                dt.Columns.Add("CapacityUnitValueStr"); 
                dt.Columns.Add("FuelAmount");
                dt.Columns.Add("FuelAmountStr");
                dt.Columns.Add("Odometer");
                dt.Columns.Add("OdometerStr");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("PaymentInstrument");
                dt.Columns.Add("PaymentInstrumentType");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("CountryName");
                dt.Columns.Add("ExchangeValue");
                dt.Columns.Add("RealAmountFormatted");
                dt.Columns.Add("DriverName");
                dt.Columns.Add("IsInternational");
                dt.Columns.Add("ResponseCode");
                dt.Columns.Add("ResponseDescription");
                dt.Columns.Add("AuthorizationNumber");
                dt.Columns.Add("UnitOfCapacityId");                

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["CustomerName"] = item.CustomerName;
                    dr["IsCustomerBranch"] = item.IsCustomerBranch;
                    dr["CustomerBranchName"] = item.CustomerBranchName;
                    dr["Invoice"] = item.Invoice;
                    dr["ElectronicInvoice"] = item.ElectronicInvoice;
                    dr["MerchantDescription"] = item.MerchantDescription;
                    dr["SystemTraceCode"] = item.SystemTraceCode;
                    dr["FuelName"] = item.FuelName;
                    dr["State"] = item.State;
                    dr["CostCenterId"] = item.CostCenterId;
                    dr["CostCenterName"] = item.CostCenterName;
                    dr["VehicleGroupId"] = item.VehicleGroupId;
                    dr["VehicleGroupName"] = item.VehicleGroupName;
                    dr["TerminalId"] = item.TerminalId;
                    dr["HolderName"] = item.HolderNameStr;
                    dr["Date"] = item.DateToExport;
                    dr["CapacityUnitValue"] = item.CapacityUnitValue;
                    dr["CapacityUnitValueStr"] = item.CapacityUnitValueStr;
                    dr["FuelAmount"] = item.FuelAmount;
                    dr["FuelAmountStr"] = item.FuelAmountStr;
                    dr["Odometer"] = item.Odometer;
                    dr["OdometerStr"] = item.OdometerStr;
                    dr["VehicleName"] = item.VehicleName;
                    dr["PaymentInstrument"] = item.PaymentInstrument;
                    dr["PaymentInstrumentType"] = item.PaymentInstrumentType;

                    dr["InitRangeDate"] = startDateStr;
                    dr["FiniRangeDate"] = endDateStr;

                    dr["CountryName"] = item.CountryName;
                    dr["ExchangeValue"] = item.ExchangeValue;
                    dr["RealAmountFormatted"] = item.RealAmount;
                    dr["DriverName"] = item.DriverNameDescrypt;
                    dr["IsInternational"] = item.IsInternational;
                    dr["ResponseCode"] = item.ResponseCodeSplitted;
                    dr["ResponseDescription"] = item.ResponseDescriptionSplitted;
                    dr["AuthorizationNumber"] = item.AuthorizationNumber;
                    dr["UnitOfCapacityId"] = item.UnitOfCapacityId;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Get the information for the reports by Partner
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable TransactionsReportDataPartner(IEnumerable<TransactionsReportDeniedByPartner> list, DateTime startDate, DateTime endDate)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("IsCustomerBranch");
                dt.Columns.Add("CustomerBranchName");
                dt.Columns.Add("Invoice");
                dt.Columns.Add("MerchantDescription");
                dt.Columns.Add("SystemTraceCode");
                dt.Columns.Add("FuelName");
                dt.Columns.Add("State");
                dt.Columns.Add("CostCenterId");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("VehicleGroupId");
                dt.Columns.Add("VehicleGroupName");
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("HolderName");
                dt.Columns.Add("Date");
                dt.Columns.Add("CapacityUnitValue");
                dt.Columns.Add("CapacityUnitValueStr");
                dt.Columns.Add("FuelAmount");
                dt.Columns.Add("Odometer");
                dt.Columns.Add("PaymentInstrument");
                dt.Columns.Add("PaymentInstrumentType");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("CountryName");
                dt.Columns.Add("ExchangeValue");
                dt.Columns.Add("RealAmountFormatted");

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["CustomerName"] = item.CustomerName;
                    dr["IsCustomerBranch"] = item.IsCustomerBranch;
                    dr["CustomerBranchName"] = item.CustomerBranchName;
                    dr["Invoice"] = item.Invoice;
                    dr["MerchantDescription"] = item.MerchantDescription;
                    dr["SystemTraceCode"] = item.SystemTraceCode;
                    dr["FuelName"] = item.FuelName;
                    dr["State"] = item.State;
                    dr["CostCenterId"] = item.CostCenterId;
                    dr["CostCenterName"] = item.CostCenterName;
                    dr["VehicleGroupId"] = item.VehicleGroupId;
                    dr["VehicleGroupName"] = item.VehicleGroupName;
                    dr["TerminalId"] = item.TerminalId;
                    dr["HolderName"] = item.HolderNameStr;
                    dr["Date"] = item.DateStr;
                    dr["CapacityUnitValue"] = item.CapacityUnitValueStr;
                    dr["FuelAmount"] = item.FuelAmount;
                    dr["Odometer"] = item.OdometerStr;
                    dr["PaymentInstrument"] = item.PaymentInstrument;
                    dr["PaymentInstrumentType"] = item.PaymentInstrumentType;
                    dr["InitRangeDate"] = startDateStr;
                    dr["FiniRangeDate"] = endDateStr;
                    dr["CountryName"] = item.CountryName;
                    dr["ExchangeValue"] = item.ExchangeValue;
                    dr["RealAmountFormatted"] = item.RealAmount;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        public DataSet ApprovalTransactionsReport(List<ApprovalTransactions> list, DateTime startDate, DateTime endDate)
        {
            var customername = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
            var ds = new DataSet();

            using (var dt = new DataTable("ApprovalTransactionsReport"))
            {
                dt.Columns.Add("Date");
                dt.Columns.Add("Status");
                dt.Columns.Add("VoucherId");
                dt.Columns.Add("CostCenter");
                dt.Columns.Add("Vehicle");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("PaymentInstrument");
                dt.Columns.Add("PaymentInstrumentType");
                dt.Columns.Add("Identification");
                dt.Columns.Add("DriverName");
                dt.Columns.Add("Fuel");
                dt.Columns.Add("Amount");
                dt.Columns.Add("Type");
                dt.Columns.Add("ServiceStation");
                dt.Columns.Add("Terminal");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("CountryName");
                dt.Columns.Add("ExchangeValue");
                dt.Columns.Add("RealAmountFormatted");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("IsCustomerBranch");
                dt.Columns.Add("CustomerBranchName");
                dt.Columns.Add("IsInternational");
                dt.Columns.Add("DynamicColumns");
                dt.Columns.Add("TransactionId");
                dt.Columns.Add("ElectronicInvoice");

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var showDynamicColumns = string.IsNullOrEmpty(list.FirstOrDefault().DynamicColumnListXML) ? false : true;
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);


                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["Date"] = item.Date.ToString("dd/MM/yyyy hh:mm:ss"); //(DateTime)String.Format("dd/MM/yyyy hh:mm:ss",item.Date);
                    dr["Status"] = item.StatusName;
                    dr["VoucherId"] = item.Invoice;
                    dr["CostCenter"] = item.CostCenterName;
                    dr["Vehicle"] = item.VehicleName;
                    dr["PlateId"] = item.PlateId;
                    dr["PaymentInstrument"] = item.PaymentInstrument;
                    dr["PaymentInstrumentType"] = item.PaymentInstrumentType;
                    dr["Identification"] = item.DriverIdDecrypt;
                    dr["DriverName"] = item.DriverNameDecrypt;
                    dr["Fuel"] = item.CapacityUnitValueStr;
                    dr["Amount"] = item.Amount;
                    dr["Type"] = item.FuelName;
                    dr["ServiceStation"] = item.ServiceStationName;
                    dr["Terminal"] = item.TerminalId;
                    dr["InitRangeDate"] = startDateStr;
                    dr["FiniRangeDate"] = endDateStr;
                    dr["CountryName"] = item.CountryName;
                    dr["ExchangeValue"] = item.ExchangeValue;
                    dr["RealAmountFormatted"] = item.RealAmount;
                    dr["CustomerName"] = customername;
                    dr["IsCustomerBranch"] = item.IsCustomerBranch;
                    dr["CustomerBranchName"] = item.CustomerBranchName;
                    dr["IsInternational"] = item.IsInternational;
                    dr["DynamicColumns"] = showDynamicColumns;
                    dr["TransactionId"] = item.TransactionId;
                    dr["ElectronicInvoice"] = item.ElectronicInvoice;

                    dt.Rows.Add(dr);
                }
                ds.Tables.Add(dt);
            }

            if (!string.IsNullOrEmpty(list.FirstOrDefault().DynamicColumnListXML))
            {
                var id = 1;
                var columnnumbers = 0;
                
                using (var dt = new DataTable("ApprovalTransactionsDetail"))
                {
                    dt.Columns.Add("Titles");
                    dt.Columns.Add("Id");

                    foreach (var item in list.FirstOrDefault().DynamicColumnList)
                    {
                        DataRow dr = dt.NewRow();
                        dr["Titles"] = item.Value;
                        dr["Id"] = id;
                        dt.Rows.Add(dr);
                        id++;
                    }
                    columnnumbers = list.FirstOrDefault().DynamicColumnList.Count;
                    ds.Tables.Add(dt);
                }
                using (var dt = new DataTable("ApprovalTransactionsColumnValues"))
                {
                    id = 1;
                    dt.Columns.Add("Values");
                    dt.Columns.Add("TransactionId");
                    dt.Columns.Add("Id");
                    foreach (var item in list)
                    {
                        var count = 0;
                        if (item.DynamicColumnValues.Count > 0)
                        {
                            foreach (var val in item.DynamicColumnValues.Where(x => item.DynamicColumnList.Select(a => a.Id).Contains(x.Id)))
                            {
                                DataRow dr = dt.NewRow();
                                dr["Values"] = val.Value;
                                dr["TransactionId"] = item.TransactionId;
                                dr["Id"] = id;
                                dt.Rows.Add(dr);
                                id++;
                                count++;
                            }
                            //Validates if the number of columns is the same as the data 
                            if(columnnumbers > count)
                            {
                                for (int i = 0; i < columnnumbers - count; i++)
                                {
                                    DataRow dr = dt.NewRow();
                                    dr["Values"] = "";
                                    dr["TransactionId"] = item.TransactionId;
                                    dr["Id"] = id;
                                    dt.Rows.Add(dr);
                                    id++;
                                }
                            }
                        }
                        else
                        {
                            foreach (var val in item.DynamicColumnList)
                            {
                                DataRow dr = dt.NewRow();
                                dr["Values"] = "";
                                dr["TransactionId"] = item.TransactionId;
                                dr["Id"] = id;
                                dt.Rows.Add(dr);
                                id++;
                            }
                        }
                    }
                    ds.Tables.Add(dt);
                }
            }
            else
            {
                ds.Tables.Add(new DataTable("ApprovalTransactionsDetail"));
                ds.Tables.Add(new DataTable("ApprovalTransactionsColumnValues"));
            }
            return ds;
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
