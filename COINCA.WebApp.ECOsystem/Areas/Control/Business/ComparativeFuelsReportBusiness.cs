﻿/************************************************************************************************************
*  File    : ComparativeEfficiencyFuelsReportBusiness.cs
*  Summary : Efficiency Fuels Report Business Methods
*  Author  :  Alexander Aguero V
*  Date    : 09/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// ComparativeEfficiencyFuelsReport Class
    /// </summary>
    public class ComparativeEfficiencyFuelsReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Current Fuels Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of ComparativeEfficiencyFuelsReportBase in order to load the chart</returns>
        public ComparativeEfficiencyFuelsReportBase RetrieveComparativeEfficiencyFuelsReport(ControlFuelsReportsBase parameters)
        {
            var result = new ComparativeEfficiencyFuelsReportBase();

            GetReportData(parameters, result);

            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(ECOsystem.Utilities.Miscellaneous.GenerateColors().Take(result.MonthCount)));
            result.Parameters.AlphaColors = new HtmlString(JsonConvert.SerializeObject(ECOsystem.Utilities.Miscellaneous.GenerateAlphaColors().Take(result.MonthCount)));
            result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(ECOsystem.Utilities.Miscellaneous.GenerateHighlighColors().Take(result.MonthCount)));
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(ControlFuelsReportsBase parameters, ComparativeEfficiencyFuelsReportBase result)
        {
            if (parameters.ReportComparativeId == null) return;
            
            GetReportDataByVehicle(parameters, result);            
        }        

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicle(ControlFuelsReportsBase parameters, ComparativeEfficiencyFuelsReportBase result)
        {

            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<ComparativeEfficiencyFuelsByVehicle>("[Control].[Sp_ComparativeFuelsReport_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            IList<IList<decimal?>> data =
                result.List.Select(x => x.Year + "/" + x.Month)
                    .Distinct()
                    .ToList()
                    .Select(
                        yearMonth =>
                            result.List.Where(x => (x.Year + "/" + x.Month).Equals(yearMonth))
                                .Select(x => x.slope as decimal?)
                                .ToList())
                    .Cast<IList<decimal?>>()
                    .ToList();

            result.MonthCount = result.List.Select(x => x.Year + "/" + x.Month).Distinct().ToList().Count();
            result.VehicleCount = result.List.Select(x => x.PlateNumber).Distinct().ToList().Count();
            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.PlateNumber).Distinct().ToList()));
            result.Parameters.Titles = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => new { x.Month, x.MonthName, x.Year }).Distinct().OrderByDescending(x => x.Year).ThenByDescending(x => x.Month).Select(x => x.MonthName + "/" + x.Year).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(data));
        }


        /// <summary>
        /// Generate Real Vs Budget FuelsReport Excel
        /// </summary>
        /// <returns>A model of ComparativeEfficiencyFuelsReportBase in order to load the chart</returns>
        public byte[] GenerateComparativeEfficiencyFuelsReportExcel(ControlFuelsReportsBase parameters)
        {
            var result = new ComparativeEfficiencyFuelsReportBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Comparativo de Rendimiento Combustible", result.List.ToList());
            }
        }


        /// <summary>
        /// Get Get Score Speed By Vehicle Report
        /// </summary>
        /// <param name="result">Result model with parameters</param>
        public ScoreGlobalReportBase GetGlobalScoreSpeedByVehicle(ScoreGlobalReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<ScoreGlobalReport>("[Operation].[Sp_GetGlobalScoreByVehicle]",
                new
                {
                    CustomerId = Session.GetCustomerId(),
                    result.Parameters.Year,
                    result.Parameters.Month,
                    result.Parameters.StartDate,
                    result.Parameters.EndDate,
                    result.Parameters.ReportType,
                    result.Parameters.VehicleId
                });
            }

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => ECOsystem.Utilities.TripleDesEncryption.Decrypt(x.EncryptedIdentification)).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.Score, 0)).ToList()));
            
            return result;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}