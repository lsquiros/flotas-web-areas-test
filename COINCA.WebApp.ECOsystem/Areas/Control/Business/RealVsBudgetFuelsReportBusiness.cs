﻿/************************************************************************************************************
*  File    : RealVsBudgetFuelsReportBusiness.cs
*  Summary : RealVsBudgetFuelsReport Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// RealVsBudgetFuelsReport Class
    /// </summary>
    public class RealVsBudgetFuelsReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Real Vs Budget Fuels Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public RealVsBudgetFuelsReportBase RetrieveRealVsBudgetFuelsReport(ControlFuelsReportsBase parameters)
        {
            var result = new RealVsBudgetFuelsReportBase();

            GetReportData(parameters, result);
            FillColors(result);

            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(ControlFuelsReportsBase parameters, RealVsBudgetFuelsReportBase result)
        {
            if (parameters.ReportFuelTypeId == null) return;

            switch ((ReportFuelTypes)parameters.ReportFuelTypeId)
            {
                case ReportFuelTypes.Vehicles:
                    GetReportDataByVehicle(parameters, result);
                    break;
                case ReportFuelTypes.VehicleGroups:
                    GetReportDataByVehicleGroup(parameters, result);
                    break;
                case ReportFuelTypes.VehicleCostCenters:
                    GetReportDataByVehicleCostCenters(parameters, result);
                    break;

            }
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicleCostCenters(ControlFuelsReportsBase parameters, RealVsBudgetFuelsReportBase result)
        {

            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<RealVsBudgetFuelsByVehicleSubUnitReport>("[Control].[Sp_RealVsBudgetFuelsReportBySubUnit_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.CostCenterName).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round( x.RealAmountPct,0)).ToList()));
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicleGroup(ControlFuelsReportsBase parameters, RealVsBudgetFuelsReportBase result)
        {

            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                result.List = dba.ExecuteReader<RealVsBudgetFuelsByVehicleGroupReport>("[Control].[Sp_RealVsBudgetFuelsReportByVehicleGroup_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.VehicleGroupName).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round( x.RealAmountPct,0)).ToList()));
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicle(ControlFuelsReportsBase parameters, RealVsBudgetFuelsReportBase result)
        {

            using (var dba = new DataBaseAccess())
            {
                var customerId = Session.GetCustomerId();
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                result.List = dba.ExecuteReader<RealVsBudgetFuelsByVehicle>("[Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]",
                    new
                    {
                        CustomerId = (customerId != null)? customerId : 0,
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.PlateNumber).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.RealAmountPct, 0)).ToList()));
        }

        /// <summary>
        /// Fill Colors
        /// </summary>
        /// <param name="result"></param>
        private void FillColors(RealVsBudgetFuelsReportBase result)
        {
            IList<string> colors = new List<string>();
            IList<string> highlightColors = new List<string>();

            var ThesholdsBusi = RetrieveThresholds().OrderByDescending(o => o.CodingThresholdsId).FirstOrDefault();
            result.Parameters.RedHigher = ThesholdsBusi.RedHigher;
            result.Parameters.RedLower = ThesholdsBusi.RedLower;
            result.Parameters.YellowLower = ThesholdsBusi.YellowLower;
            result.Parameters.YellowHigher = ThesholdsBusi.YellowHigher;
            result.Parameters.GreenLower = ThesholdsBusi.GreenLower;
            result.Parameters.GreenHigher = ThesholdsBusi.GreenHigher;

            foreach (dynamic item in result.List)
            {
                // if (item.Score <= 69)
                if (item.RealAmountPct <= ThesholdsBusi.RedHigher)
                {
                    colors.Add("rgba(92, 184, 92,1)"); //green
                    highlightColors.Add("rgba(92, 184, 92,0.9)"); //green
                }
                else if (item.RealAmountPct >= ThesholdsBusi.YellowLower && item.RealAmountPct <= ThesholdsBusi.YellowHigher) //  if (item.Score >= 70 && item.Score <= 89)
                {
                    colors.Add("rgba(240, 173, 78,1)"); //yellow
                    highlightColors.Add("rgba(240, 173, 78,0.9)"); //yellow
                }
                else {
                    // if (item.Score > 89)
                    if (item.RealAmountPct >= ThesholdsBusi.GreenLower)
                    {
                        colors.Add("rgba(217, 83, 79,1)"); //red
                        highlightColors.Add("rgba(217, 83, 79,0.9)"); //red
                    }
                    else
                    {
                        colors.Add("rgba(240, 173, 78,1)"); //yellow
                        highlightColors.Add("rgba(240, 173, 78,0.9)"); //yellow
                    }
                }
            }
            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
            result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(highlightColors));
        }

        /// <summary>
        /// Generate Real Vs Budget FuelsReport Excel
        /// </summary>
        /// <returns> A model of RealVsBudgetFuelsReportBase in order to load the chart </returns>
        public byte[] GenerateRealVsBudgetFuelsReportExcel(ControlFuelsReportsBase parameters)
        {
            var result = new RealVsBudgetFuelsReportBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Consumo Real vs Presupuesto", result.List.ToList());
            }

        }

        /// <summary>
        /// Retrieve Parameters
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CustomerThresholds> RetrieveThresholds()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerThresholds>("[General].[Sp_CustomerThresholds_Retrieve]",
                new
                {
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                });
            }
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}