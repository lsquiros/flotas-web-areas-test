﻿/************************************************************************************************************
*  File    : PerformanceByVehicleBusiness.cs
*  Summary : Performance By Vehicle Business Methods
*  Author  : Cristian Martínez H.
*  Date    : 23/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using ECOsystem.DataAccess;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// Perfomance By Vehicle Business
    /// </summary>
    public class PerformanceByVehicleBusiness : IDisposable 
    {
        /// <summary>
        /// Performs the maintenance Insert of the entity Performance By Vehicle
        /// </summary>
        /// <param name="vehicleId">reference of vehicle</param>
        /// <param name="date">date in that calculate the  performance</param>
        /// <param name="performance">performance calculated of the vehicle </param>
        /// /// <param name="userId"> </param>
        public void AddPerformanceByVehicle(int? vehicleId, DateTime date, decimal performance, decimal totalOdometer, decimal totalLiters, int? userId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PerformanceByVehicle_Add]",
                    new
                    {
                         VehicleId = vehicleId, 
                         Date = date, 
                         Performance = performance,
                         TotalOdometer = totalOdometer,
                         TotalLiters = totalLiters,
                         LoggedUserId = userId
                    });
            }
        }

        /// <summary>
        /// Last Performance By Vehicle Retrieve
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public decimal? LastPerformanceByVehicle(int? vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<decimal?>("[General].[Sp_LastPerformanceByVehicle_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}