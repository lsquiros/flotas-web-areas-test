﻿/************************************************************************************************************
*  File    : VehicleLowPerformanceReportBusiness.cs
*  Summary : Vehicle Low Performance Report Business
*  Author  : Andres Oviedo
*  Date    : 26/01/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// VehicleLowPerformanceReport Class
    /// </summary>
    public class VehicleLowPerformanceReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Vehicle Low Performance Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public IEnumerable<VehicleLowPerformanceReports> RetrieveVehicleLowPerformanceReport(VehicleLowPerformanceReportBase parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<VehicleLowPerformanceReports>("[Control].[Sp_VehicleLowPerformanceReport_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        Year = parameters.Year,
                        Month = parameters.Month,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate,
                        VehicleId = parameters.VehicleId,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Generate Vehicle Low Performance Report Excel
        /// </summary>
        /// <returns>A model of VehicleLowPerformanceReportBase in order to load the chart</returns>
        public byte[] GenerateTransactionsReportExcel(VehicleLowPerformanceReportBase parameters)
        {
            var result = new VehicleLowPerformanceReportBase();

            result.List = RetrieveVehicleLowPerformanceReport(parameters);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Consumo de Combustible Real del Período", result.List.ToList());
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}