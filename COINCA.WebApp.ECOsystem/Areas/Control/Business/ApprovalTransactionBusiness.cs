﻿/*
 * Author:      Melvin Salas
 * Date:        March 14, 2016
 * Description: This business manage related if approval transaction report
 * */

using  ECOsystem.Areas.Control.Models.Control;
using  ECOsystem.Areas.Control.Utilities;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace  ECOsystem.Areas.Control.Business.Control
{
    public class ApprovalTransactionBusiness : IDisposable
    {
        public IEnumerable<ApprovalTransactions> ApprovalTransctionsRetrieve(ApprovalTransactionParametersModel parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ApprovalTransactions>("[Control].[Sp_ApprovalTransaction_Retrieve]",
                    new
                    {
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate,
                        Month = parameters.Month,
                        Year = parameters.Year,
                        Search = parameters.SearchText,
                        Status = parameters.Status,
                        UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        RetrieveType = parameters.RetrieveType,
                        CostCenterId = parameters.CostCenterId
                    });
            }
        }

        public void ApprovalTransctionsAddOrEdit(List<ApprovalTransactions> list)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_ApprovalTransaction_AddOrEdit]", new
                {
                    XmlData = getXmlApprovalTransactions(list)
                });
            }
        }

        string getXmlApprovalTransactions(List<ApprovalTransactions> list)
        {
            using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<ApprovalTransactions>));
                xmlSerializer.Serialize(stringWriter, list);
                return stringWriter.ToString().Substring(stringWriter.ToString().IndexOf('\n') + 1);
            }
        }

        /// <summary>
        /// GetTypes
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<ApprovalTransactionTypeModel> GetTypes(int? type = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return (dba.ExecuteReader<ApprovalTransactionTypeModel>("Control.Sp_ApprovalTransactionTypes_Retrieve",
                    new
                    {
                        RetrieveStates = type,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    }));
            }
        }

        /// <summary>
        /// GetEditableTypes
        /// </summary>
        /// <returns></returns>
        internal IList<ApprovalTransactionTypeModel> GetEditableTypes(bool Editable)
        {
            using (var dba = new DataBaseAccess())
            {
                return (dba.ExecuteReader<ApprovalTransactionTypeModel>("Control.Sp_ApprovalEditableTransactionTypes_Retrieve", new { Editable }));
            }
        }

        /// <summary>
        /// ApprovalTransactionsDuplicateNumber
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="bacId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public int ApprovalTransactionsDuplicateNumber(string invoice, string bacId, int customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Control].[Sp_ApprovalTransaction_Count]", new
                {
                    Invoice = invoice,
                    BacId = bacId,
                    CustomerId = customerId
                });
            }
        }

        /// <summary>
        /// GenerateApprovalReportData
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataSet GenerateApprovalReportData(List<ApprovalTransactions> list, ApprovalTransactionParametersModel parameters)
        {
            var startDate = new DateTime();
            var endDate = new DateTime();

            if (parameters.StartDate != null && parameters.EndDate != null)
            {
                startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
            }
            else if (parameters.Month != null && parameters.Year != null)
            {
                startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
            }

            using (var rep = new TransactionsReportUtility())
            {
                return rep.ApprovalTransactionsReport(list, startDate, endDate);
            }
        }

        /// <summary>
        /// Return a list of Service Stations
        /// </summary>
        public static SelectList GetServiceStations()
        {
            var dictionary = new Dictionary<int?, string>();

            using (var bus = new DataBaseAccess())
            {
                var result = bus.ExecuteReader< ECOsystem.Areas.Control.Models.Control.ServiceStations>("[General].[Sp_ServiceStations_Retrieve]",
                    new
                    {
                        UserId = ECOsystem.Utilities.Session.GetCustomerId(),
                        CountryId = ECOsystem.Utilities.Session.GetCustomerInfo().CountryId
                    });

                foreach (var item in result)
                {
                    dictionary.Add(item.ServiceStationId, "{'Name': '" + item.Name + "'}");
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public IEnumerable<ApprovalTransactions> GetAprovalFromImport(List<ApprovalTransactionsImport> list)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ApprovalTransactions>("[Control].[Sp_ApprovalTransactionsFromImport_Validate]", new
                {
                    XmlData = GetXMLList(list),
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                });
            }
        }

        string GetXMLList(List<ApprovalTransactionsImport> list)
        {
            using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<ApprovalTransactionsImport>));
                xmlSerializer.Serialize(stringWriter, list);
                return stringWriter.ToString().Substring(stringWriter.ToString().IndexOf('\n') + 1);
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}