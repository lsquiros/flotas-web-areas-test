﻿/************************************************************************************************************
*  File    : TrendAndConsumptionReportBusiness.cs
*  Summary : TrendAndConsumptionReport Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// TrendAndConsumptionReport Class
    /// </summary>
    public class TrendAndConsumptionReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Real Vs Budget Fuels Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of TrendAndConsumptionReportBase in order to load the chart</returns>
        public TrendAndConsumptionReportBase RetrieveTrendAndConsumptionReport(TrendAndConsumptionParameters parameters)
        {
            var result = new TrendAndConsumptionReportBase();

            GetReportDataByVehicle(parameters, result);
            
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }



        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicle(TrendAndConsumptionParameters parameters, TrendAndConsumptionReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<TrendAndConsumptionReport>("[Control].[Sp_TrendAndConsumptionReport_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        parameters.Key,
                        user.UserId
                    });
            }
        }


        /// <summary>
        /// Generate Real Vs Budget FuelsReport Excel
        /// </summary>
        /// <returns>A model of TrendAndConsumptionReportBase in order to load the chart</returns>
        public byte[] GenerateTrendAndConsumptionReportExcel(TrendAndConsumptionParameters parameters)
        {
            var result = new TrendAndConsumptionReportBase();

            GetReportDataByVehicle(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Reporte de Media de Consumo", result.List.ToList());
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}