﻿/************************************************************************************************************
*  File    : TransactionsReportBusiness.cs
*  Summary : TransactionsReport Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 12/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Data;
using  ECOsystem.Areas.Control.Utilities;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// TransactionsReport Class
    /// </summary>
    public class TransactionsReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Transactions Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of TransactionsReportBase in order to load the chart</returns>
        public TransactionsReportBase RetrieveTransactionsReport(ControlFuelsReportsBase parameters)
        {
            var result = new TransactionsReportBase();
            GetReportData(parameters, result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.TransactionType = parameters.TransactionType;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            return result;
        }

        /// <summary>
        /// Retrieve Transactions Report by Partner
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of TransactionsReportBase in order to load the chart</returns>
        public TransactionsReportDeniedByPartnerBase RetrieveTransactionsReportDeniedByPartner(ControlFuelsReportsBase parameters)
        {            
            var result = new TransactionsReportDeniedByPartnerBase();
            GetReportDeniedByPartnerData(parameters, result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.TransactionType = parameters.TransactionType;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDeniedByPartnerData(ControlFuelsReportsBase parameters, TransactionsReportDeniedByPartnerBase result)
        {
            GetReportDeniedByPartnerDataTransactions(parameters, result);
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDeniedByPartnerDataTransactions(ControlFuelsReportsBase parameters, TransactionsReportDeniedByPartnerBase result)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                if (parameters.CustomerId == -1)
                {
                    switch (parameters.TransactionType)
                    {
                        case 1:
                        case 3:
                            result.List = dba.ExecuteReader<TransactionsReportDeniedByPartner>(
                                "[Control].[Sp_TransactionsReportByPartner_Retrieve]",
                                new
                                {
                                    PartnerId = Session.GetPartnerId(),
                                    Status = parameters.TransactionType,
                                    Key = parameters.key,
                                    Year = parameters.Year,
                                    Month = parameters.Month,
                                    StartDate = parameters.StartDate,
                                    EndDate = parameters.EndDate,
                                    user.UserId
                                });
                            foreach (var item in result.List)
                            {
                                item.Invoice = (item.Invoice != null) ? ((item.Invoice == "NULL")? "-" : item.Invoice) : "-";
                                item.MerchantDescription = (item.MerchantDescription != null) ? ((item.MerchantDescription == "NULL")? "-" : item.MerchantDescription) : "-";
                                item.CreditCardNumber = ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(TripleDesEncryption.Decrypt(item.CreditCardNumber));
                            }
                            break;
                        case 6:
                            result.List = dba.ExecuteReader<TransactionsReportDeniedByPartner>(
                                "[Control].[Sp_TransactionsReportDeniedByPartner_Retrieve]",
                                 new
                                 {
                                     PartnerId = Session.GetPartnerId(),
                                     Year = parameters.Year,
                                     Month = parameters.Month,
                                     StartDate = parameters.StartDate,
                                     EndDate = parameters.EndDate,
                                     user.UserId
                                 });
                            foreach (var item in result.List)
                            {
                                var cardNumberStr = ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat((long)((item.TransportDataObject.cardNumber != null) ? item.TransportDataObject.cardNumber : 0));
                                var cardNumberHash = item.TransportDataObject != null ? TripleDesEncryption.Encrypt(cardNumberStr) : "";
                                var ccObj = GetCreditCard(cardNumberHash);

                                item.CreditCardHolder = (ccObj != null) ? ccObj.CreditCardHolder : "Ninguno";
                                item.CreditCardNumber = ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(cardNumberStr);
                            } break;
                    }
                }
                else
                {      
                    switch (parameters.TransactionType)
                    {
                        case 1:
                        case 3:
                            result.List = dba.ExecuteReader<TransactionsReportDeniedByPartner>("[Control].[Sp_TransactionsReport_Retrieve]",
                            new
                            {
                                CustomerId = parameters.CustomerId == 0 ? Session.GetCustomerId() : parameters.CustomerId,
                                Status = parameters.TransactionType,
                                Key = parameters.key,
                                Year = parameters.Year,
                                Month = parameters.Month,
                                StartDate = parameters.StartDate,
                                EndDate = parameters.EndDate,
                                user.UserId
                            });
                            foreach (var item in result.List)
                                item.CreditCardNumber = ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(TripleDesEncryption.Decrypt(item.CreditCardNumber));
                            break;
                        case 6:
                            result.List = dba.ExecuteReader<TransactionsReportDeniedByPartner>("[Control].[Sp_TransactionsReportDenied_Retrieve]",
                            new
                            {
                                CustomerId = parameters.CustomerId == 0 ? Session.GetCustomerId() : parameters.CustomerId,
                                Year = parameters.Year,
                                Month = parameters.Month,
                                StartDate = parameters.StartDate,
                                EndDate = parameters.EndDate,
                                user.UserId
                            });

                            foreach (var item in result.List)
                            {
                                var cardNumberStr = ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat((long)((item.TransportDataObject.cardNumber != null) ? item.TransportDataObject.cardNumber : 0));
                                var cardNumberHash = item.TransportDataObject != null ? TripleDesEncryption.Encrypt(cardNumberStr) : "";
                                var ccObj = GetCreditCard(cardNumberHash);

                                item.CreditCardHolder = (ccObj != null) ? ccObj.CreditCardHolder : "Ninguno";
                                item.CreditCardNumber = ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(cardNumberStr);

                                foreach (string tp in item.TransportData.Split(','))
                                {
                                    string[] t = tp.Split(':');
                                    if (t[0] == "\"terminalId\"")
                                    {
                                        item.TerminalId = t[1].Replace("\"", string.Empty).Trim();
                                    }
                                }

                            }
                            break;
                    }

                }
                result.List = result.List.OrderByDescending(x => x.Date);
            }
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }


        /// <summary>
        /// Get Report Data TXTSAP
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(ControlFuelsReportsBase parameters, TransactionsReportBase result)
        {
              GetReportDataTransactions(parameters, result);
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataTransactionsTXTSAP(ControlFuelsReportsBase parameters, TransactionsReportBase result)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                if (parameters.TransactionType != 6)
                {
                    int x = Convert.ToInt32(Session.GetCustomerId());

                    result.List = dba.ExecuteReader<TransactionsReport>("[Control].[SP_Transactions_SAPR_Retrieve]",
                    new
                    {
                        CustomerId = x,
                        Key = parameters.key,
                        Year = parameters.Year,
                        Month = parameters.Month,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate
                    });
                }
            }
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataTransactions(ControlFuelsReportsBase parameters, TransactionsReportBase result)
        {
            var DeniedList = new List<TransactionsReport>();
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                int cusId = 0;
                if (parameters.CustomerId == 0)
                    cusId = Convert.ToInt32(Session.GetCustomerId());
                else
                    cusId = parameters.CustomerId;
                
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                if (parameters.TransactionType == 6)
                {
                    result.List = dba.ExecuteReader<TransactionsReport>("[Control].[Sp_TransactionsReportDenied_Retrieve]",
                        new
                        {
                            CustomerId = cusId,
                            Year = parameters.Year,
                            Month = parameters.Month,
                            StartDate = parameters.StartDate,
                            EndDate = parameters.EndDate,
                            user.UserId
                        });
                    
                    foreach (var item in result.List)
                    {
                       var ccObj = GetCreditCard(item.PaymentInstrumentCode);

                        item.CreditCardHolder = (ccObj != null) ? ccObj.CreditCardHolder : "Ninguno";
                        item.CreditCardNumber = ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(item.PaymentInstrument);
                        item.VehicleName = (ccObj != null) ? ccObj.VehicleName : "";
                        if (parameters.key != null)
                        {
                            if (item.CreditCardHolder.Contains(parameters.key) || item.VehicleName.Contains(parameters.key))
                            {
                                
                                DeniedList.Add(item);
                            }                            
                        } 
                    }

                    if (parameters.key != null)
                    {
                        result.List = DeniedList;
                    }

                }
                else
                {                    
                    result.List = dba.ExecuteReader<TransactionsReport>("[Control].[Sp_TransactionsReport_Retrieve]",
                    new
                    {
                        CustomerId = cusId,
                        Status = parameters.TransactionType,
                        Key = parameters.key,
                        Year = parameters.Year,
                        Month = parameters.Month,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate,
                        //StartDate = "2019-11-01",
                        //EndDate = "2019-11-30",
                        user.UserId
                    });                    
                }
            }
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }

        /// <summary>
        /// Generate Transaction Report Excel
        /// </summary>
        /// <returns>A model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public byte[] GenerateTransactionsReportExcel(ControlFuelsReportsBase parameters)
        {
            var result = new TransactionsReportBase();
            var resultPartner = new TransactionsReportDeniedByPartnerBase();

            if (parameters.CustomerId > 0)
            {                
                GetReportData(parameters, result);
            }
            else
            {
                GetReportDeniedByPartnerDataTransactions(parameters, resultPartner);
            }

            using (var excel = new ExcelReport())
            {
                excel.HasCustomRows = false;
                //Customer Report
                if (parameters.CustomerId > 0)
                {
                    if (parameters.TransactionType == 6)
                    {
                        var resultDeny = new List<TransactionDenyReportModel>();

                        foreach (var item in result.List.ToList())
                        {
                            var e = new TransactionDenyReportModel();
                            e.CreditCardNumber = item.CreditCardNumber;
                            e.ResponseCode = item.ResponseCode;
                            e.ResponseCodeDescription = item.ResponseCodeDescription;
                            e.CreditCardHolder = item.CreditCardHolder;
                            e.Message = item.Message;
                            e.InsertDate = item.InsertDate;
                            e.Message = e.Message.Substring(0, 26);
                            e.TerminalId = item.TerminalId;
                            e.PaymentInstrumentType = item.PaymentInstrumentType;
                            resultDeny.Add(e);
                        }

                        return excel.CreateSpreadsheetWorkbook("Transacciones Denegadas", resultDeny);
                    }
                    else
                    {
                        return excel.CreateSpreadsheetWorkbook("Transacciones", result.List.ToList());
                    }
                }
                else // Partners reports 
                {
                    if (parameters.TransactionType == 6)
                    {
                        var resultDeny = new List<TransactionsReportDeniedByPartner>();

                        foreach (var item in resultPartner.List.ToList())
                        {
                            var e = new TransactionsReportDeniedByPartner();
                            e.CreditCardNumber = item.CreditCardNumber;
                            e.ResponseCode = item.ResponseCode;
                            e.ResponseCodeDescription = item.ResponseCodeDescription;
                            e.CreditCardHolder = item.CreditCardHolder;
                            e.Message = item.Message;
                            e.InsertDate = item.InsertDate;
                            e.Message = e.Message.Substring(0, 26);
                            e.TerminalId = item.TerminalId;
                            resultDeny.Add(e);
                        }

                        return excel.CreateSpreadsheetWorkbook("Transacciones Denegadas", resultDeny);
                    }
                    else
                    {
                        return excel.CreateSpreadsheetWorkbook("Transacciones", resultPartner.List.ToList());
                    }
                }
            }
        }

        /// <summary>
        /// Get the data in the datatable
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GenerateDeniedReportData(ControlFuelsReportsBase parameters)
        {
            var result = new TransactionsReportBase();
            var resultPartner = new TransactionsReportDeniedByPartnerBase();

            var startDate = new DateTime(2019,11,1);
            var endDate = new DateTime(2019,11,30);

            if (parameters.StartDate != null && parameters.EndDate != null)
            {
                startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
            }
            else if (parameters.Month != null && parameters.Year != null)
            {
                startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
            }

            if (parameters.CustomerId > 0 || Convert.ToUInt32(Session.GetCustomerId()) > 0)
            {
                GetReportData(parameters, result);
            }
            else
            {
                GetReportDeniedByPartnerDataTransactions(parameters, resultPartner);
            }

            var rep = new TransactionsReportUtility();

            //Customer Report
            if (parameters.CustomerId > 0 || Convert.ToUInt32(Session.GetCustomerId()) > 0)
            {
                if (parameters.TransactionType == 6)
                {
                    var resultDeny = new List<TransactionDenyReportModel>();

                    foreach (var item in result.List.ToList())
                    {
                        var e = new TransactionDenyReportModel();
                        e.CreditCardNumber = item.CreditCardNumber;
                        e.ResponseCode = item.ResponseCode;
                        e.ResponseCodeDescription = item.ResponseCodeDescription;
                        e.CreditCardHolder = item.CreditCardHolder;
                        e.Message = item.Message;
                        e.InsertDate = item.InsertDate;
                        e.Message = e.Message;
                        e.TerminalId = item.TerminalId;
                        e.ServiceStationName = item.ServiceStationName;
                        e.CountryName = item.CountryName;
                        e.ExchangeValue = item.ExchangeValue;
                        e.RealAmount = item.RealAmount;
                        e.CustomerBranchName = item.CustomerBranchName;
                        e.IsCustomerBranch = item.IsCustomerBranch;
                        e.CustomerName = item.CustomerName;
                        e.PaymentInstrumentType = item.PaymentInstrumentType;
                        resultDeny.Add(e);
                    }

                    return rep.DeniedTransactionsReport(resultDeny, startDate, endDate);
                }
                else
                {
                    return rep.TransactionsReportData(result.List, startDate, endDate);
                }
            }
            else
            {
                if (parameters.TransactionType == 6)
                {
                    var resultDeny = new List<TransactionDenyReportModel>();

                    foreach (var item in resultPartner.List.ToList())
                    {
                        var e = new TransactionDenyReportModel();
                        e.CreditCardNumber = item.CreditCardNumber;
                        e.ResponseCode = item.ResponseCode;
                        e.ResponseCodeDescription = item.ResponseCodeDescription;
                        e.CreditCardHolder = item.CreditCardHolder;
                        e.Message = item.Message;
                        e.InsertDate = item.InsertDate;
                        e.Message = e.Message;
                        //e.Message = e.Message.Substring(0, 26);
                        e.TerminalId = item.TerminalId;
                        e.ServiceStationName = item.ServiceStationName;
                        e.PaymentInstrumentType = item.PaymentInstrumentType;
                        resultDeny.Add(e);
                    }

                    return rep.DeniedTransactionsReport(resultDeny, startDate, endDate);
                }
                else
                {
                    return rep.TransactionsReportDataPartner(resultPartner.List, startDate, endDate);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="TerminalId"></param>
        /// <returns></returns>
        public string GetMerchantDescriptionByTerminalId(string TerminalId)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteScalar<string>("[dbo].[SP_TransactionsSAPServiceStationsTemp_Retrieve]",
                    new
                    {
                        TerminalId
                    });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        private CreditCardTransactions GetCreditCard(string cardNumber)
        {
            CreditCardTransactions creditCard = null;

            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                creditCard = dba.ExecuteReader<CreditCardTransactions>("[Control].[Sp_CreditCardByNumber_Retrieve]",
                new
                {
                    CreditCardNumber = cardNumber
                }).FirstOrDefault();
            }

            if (creditCard == null)
            {
                return null;
            }

            return creditCard;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        public TransactionsReport RetrieveTrace(int? id, int FilterType)
        {
            var model = new TransactionsReport();  
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return model = dba.ExecuteReader<TransactionsReport>("[Control].[Sp_TransportDataByTransactionId_Retrieve]",
                new
                {
                    TransactionId = id,
                    FilterType
                }).FirstOrDefault();
            }
        }
        

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}

internal class CreditCardTransactions
{
    public int? IssueForId { get; set; }

    public string EncryptedDriverName { get; set; }

    public string DecryptedDriverName
    {
        get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedDriverName); }
        set { EncryptedDriverName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
    }

    public string VehiclePlate { get; set; }

    public string VehicleName { get; set; }

    public string CreditCardHolder
    {
        get { return IssueForId == 100 ? DecryptedDriverName : IssueForId == 101 ? VehiclePlate : ""; }
        set
        {
            switch (IssueForId)
            {
                case 100:
                    DecryptedDriverName = value;
                    break;
                case 101:
                    VehiclePlate = value;
                    break;
            }
        }
    }
}
