﻿/************************************************************************************************************
*  File    : ConsolidateLiquidationReportBusiness.cs
*  Summary : ConsolidateLiquidationReport Business Methods
*  Author  : Andres Oviedo
*  Date    : 22/01/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Data;
using System.Globalization;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// RealVsBudgetFuelsReport Class
    /// </summary>
    public class ConsolidateLiquidationReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Consolidate Liquidation Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public IEnumerable<ConsolidateLiquidationReports> RetrieveConsolidateLiquidationReport(ConsolidateLiquidationReportBase parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<ConsolidateLiquidationReports>("[Control].[Sp_ConsolidateLiquidationReport_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        Year = parameters.Year,
                        Month = parameters.Month,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Generate  Excel
        /// </summary>
        /// <returns>A model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public byte[] GenerateTransactionsReportExcel(ConsolidateLiquidationReportBase parameters)
        {
            var result = new ConsolidateLiquidationReportBase();

            result.List = RetrieveConsolidateLiquidationReport(parameters);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Consumo de Combustible Real del Período", result.List.ToList());
            }

        }

        public DataTable GetReportDownloadData(ConsolidateLiquidationReportBase model)
        {
            try
            {
                var startDate = new DateTime();
                var endDate = new DateTime();

                if (model.Month != null && model.Year != null)
                {
                    startDate = new DateTime((int)model.Year, (int)model.Month, 1, 0, 0, 0);
                    endDate = new DateTime((int)model.Year, (int)model.Month, DateTime.DaysInMonth((int)model.Year, (int)model.Month), 23, 59, 59);
                }

                var Customer = Session.GetCustomerInfo().DecryptedName;
                using (var dt = new DataTable())
                {
                    dt.Columns.Add("Plate");
                    dt.Columns.Add("VehicleName");
                    dt.Columns.Add("TotalOdometer");
                    dt.Columns.Add("FuelName");
                    dt.Columns.Add("Unit");
                    dt.Columns.Add("Manufacturer");
                    dt.Columns.Add("VehicleModel");
                    dt.Columns.Add("VehicleYear");
                    dt.Columns.Add("Identification");
                    dt.Columns.Add("CapacityUnitValue");
                    dt.Columns.Add("TankCapacity");
                    dt.Columns.Add("DriverCode");
                    dt.Columns.Add("PaymentInstrument");
                    dt.Columns.Add("PaymentInstrumentType");
                    dt.Columns.Add("InitDate");
                    dt.Columns.Add("EndDate");
                    dt.Columns.Add("CapacityUnitName");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CostCenterId");
                    dt.Columns.Add("CostCenterName");
                    dt.Columns.Add("VehicleGroupId");
                    dt.Columns.Add("VehicleGroupName");

                    var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                    var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                    //Get array letters
                    char[] letters = startDateStr.ToCharArray();
                    // upper case the first char
                    letters[0] = char.ToUpper(letters[0]);
                    // return the array made of the new char array
                    startDateStr = new string(letters);
                    letters = endDateStr.ToCharArray();
                    // upper case the first char
                    letters[0] = char.ToUpper(letters[0]);
                    // return the array made of the new char array
                    endDateStr = new string(letters);

                    foreach (var item in model.List)
                    {
                        DataRow row = dt.NewRow();
                        row["Plate"] = item.PlateId;
                        row["VehicleName"] = item.VehicleName;
                        row["Manufacturer"] = item.Manufacturer;
                        row["VehicleYear"] = item.Year;
                        row["VehicleModel"] = item.VehicleModel;
                        row["FuelName"] = item.FuelName;
                        row["Unit"] = item.UnitName;
                        row["CapacityUnitValue"] = item.CapacityUnitValueStr;
                        row["TotalOdometer"] = item.TotalOdometerStr;
                        row["TankCapacity"] = item.TankCapacityStr;
                        row["DriverCode"] = item.CodeStr;
                        row["PaymentInstrument"] = item.PaymentInstrument;
                        row["PaymentInstrumentType"] = item.PaymentInstrumentType;
                        row["Identification"] = item.IdentificationStr;
                        row["InitDate"] = startDateStr;
                        row["EndDate"] = endDateStr;
                        row["CapacityUnitName"] = ECOsystem.Utilities.Session.GetCustomerCapacityUnitName();
                        row["CustomerName"] = Customer;
                        row["CostCenterId"] = item.CostCenterId;
                        row["CostCenterName"] = item.CostCenterName;
                        row["VehicleGroupId"] = item.VehicleGroupId;
                        row["VehicleGroupName"] = item.VehicleGroupName;

                        dt.Rows.Add(row);
                    }

                    return dt;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}