﻿using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Business
{
    public class BudgetChangesReportBusiness : IDisposable
    {

        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            return DataTableUtilities.ClassToDataTable(RetrieveBudgetChangesReport(parameters), string.Empty);
        }

        public List<BudgetChanges> RetrieveBudgetChangesReport(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<BudgetChanges>("[Control].[SP_BudgetChanges_Retrieve]",
                    new
                    {
                        parameters.StartDate,
                        parameters.EndDate,
                        parameters.Year,
                        parameters.Month,
                        CustomerId = Session.GetCustomerId(),
                        CostCenterId = parameters.CostCenterId == -1 ? null : parameters.CostCenterId,
                        parameters.VehicleId,
                        FuelId = parameters.ReportFuelTypeId
                    }).ToList();
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }    
}