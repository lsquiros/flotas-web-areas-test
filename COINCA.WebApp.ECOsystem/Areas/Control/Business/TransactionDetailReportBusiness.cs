﻿/************************************************************************************************************
*  File    : TransactionsReportBusiness.cs
*  Summary : TransactionsReport Business Methods
*  Author  : Stefano Quiros
*  Date    : 02/16/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Data;
using System.Globalization;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// TransactionsReport Class
    /// </summary>
    public class TransactionDetailReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Transactions Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of TransactionsReportBase in order to load the chart</returns>
        public TransactionsReportBase RetrieveTransactionsReport(ControlFuelsReportsBase parameters)
        {
            var result = new TransactionsReportBase();
            GetReportData(parameters, result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.TransactionType = parameters.TransactionType;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            return result;
        }

        /// <summary>
        /// Get Report Data TXTSAP
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(ControlFuelsReportsBase parameters, TransactionsReportBase result)
        {
            GetReportDataTransactions(parameters, result);
        }
               

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataTransactions(ControlFuelsReportsBase parameters, TransactionsReportBase result)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                int cusId = 0;
                if (parameters.CustomerId == 0)
                    cusId = Convert.ToInt32(Session.GetCustomerId());
                else
                    cusId = parameters.CustomerId;

                var user = ECOsystem.Utilities.Session.GetUserInfo();

                if (parameters.FilterVehicle == 1)
                {
                    result.List = dba.ExecuteReader<TransactionsReport>("[Control].[Sp_TransactionsDetailedReport_Retrieve_CC]",
                        new
                        {
                            CustomerId = cusId,
                            Year = parameters.Year,
                            Month = parameters.Month,
                            StartDate = parameters.StartDate,
                            EndDate = parameters.EndDate,
                            key = parameters.key,
                            user.UserId
                        });
                }
                else if (parameters.FilterVehicle == 2)                
                {
                    result.List = dba.ExecuteReader<TransactionsReport>("[Control].[Sp_TransactionsDetailedReport_Retrieve_GV]",
                    new
                    {
                        CustomerId = cusId,
                        Year = parameters.Year,
                        Month = parameters.Month,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate,
                        key = parameters.key,
                        user.UserId
                    });                    
                }           
            }
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            try
            {
                var result = new TransactionsReportBase();
                GetReportData(parameters, result);

                var startDate = new DateTime();
                var endDate = new DateTime();

                if (parameters.StartDate != null && parameters.EndDate != null)
                {
                    startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                    endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
                }
                else if (parameters.Month != null && parameters.Year != null)
                {
                    startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                    endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
                }


                return TransactionsReportGenerate(result.List, startDate, endDate);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable TransactionsReportGenerate(IEnumerable<TransactionsReport> list, DateTime startDate, DateTime endDate)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("FECHA");
                dt.Columns.Add("VEHICULO");
                dt.Columns.Add("PLACA");
                dt.Columns.Add("PaymentInstrument");
                dt.Columns.Add("PaymentInstrumentType");
                dt.Columns.Add("CENTRO_DE_COSTO");
                dt.Columns.Add("GRUPO_DE_VEHICULOS");
                dt.Columns.Add("UNIDAD");
                dt.Columns.Add("CONDUCTOR");
                dt.Columns.Add("ID_CONDUCTOR");
                dt.Columns.Add("FABRICANTE");
                dt.Columns.Add("ODOMETRO");
                dt.Columns.Add("ULTIMO_ODOMETRO");
                dt.Columns.Add("RECORRIDO");
                dt.Columns.Add("RENDIMIENTO");
                dt.Columns.Add("COSTOXKILOMETRO");
                dt.Columns.Add("LITROS");
                dt.Columns.Add("MONTO");
                dt.Columns.Add("COMBUSTIBLE");
                dt.Columns.Add("ESTACION_DE_SERVICIO");
                dt.Columns.Add("REFERENCIA");
                dt.Columns.Add("FACTURA");
                dt.Columns.Add("ElectronicInvoice");
                dt.Columns.Add("COUNTRYNAME");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("ExchangeValue");
                dt.Columns.Add("RealAmountFormatted");
                dt.Columns.Add("TIPO_UNIDAD_COMBUSTIBLE");
                dt.Columns.Add("IsInternational");
                dt.Columns.Add("CustomerName"); 
                dt.Columns.Add("CustomerBranchName");
                dt.Columns.Add("IsCustomerBranch");
                

                 var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);

                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();
                   
                    string text = string.Empty;
                    string CcDescrypt = TripleDesEncryption.Decrypt(item.CreditCardNumber);
                    string CcMask = ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(CcDescrypt);

                    row["FECHA"] = item.DateToExport;
                    row["VEHICULO"] = item.VehicleName;
                    row["PLACA"] = item.PlateId;
                    row["PaymentInstrument"] = item.PaymentInstrument;
                    row["PaymentInstrumentType"] = item.PaymentInstrumentType;
                    row["CENTRO_DE_COSTO"] = item.CostCenterName;
                    row["GRUPO_DE_VEHICULOS"] = item.VehicleGroupName;
                    row["UNIDAD"] = item.UnitName;
                    row["CONDUCTOR"] = item.DriverNameDescrypt;
                    row["ID_CONDUCTOR"] = item.DriverIdDescrypt;
                    row["FABRICANTE"] = item.Manufacturer;
                    row["ODOMETRO"] = item.Odometer;
                    row["ULTIMO_ODOMETRO"] = item.OdometerLast;
                    row["RECORRIDO"] = item.Travel;
                    row["RENDIMIENTO"] = item.Performance;
                    row["COSTOXKILOMETRO"] = item.KilometersCost;
                    row["LITROS"] = item.CapacityUnitValueStr;
                    row["MONTO"] = item.FuelAmount;
                    row["COMBUSTIBLE"] = item.FuelName;
                    row["ESTACION_DE_SERVICIO"] = item.MerchantDescription;
                    row["REFERENCIA"] = item.Reference;
                    row["FACTURA"] = item.Invoice;
                    row["ElectronicInvoice"] = item.ElectronicInvoice;
                    row["COUNTRYNAME"] = item.CountryName;
                    row["InitRangeDate"] = startDateStr;
                    row["FiniRangeDate"] = endDateStr;
                    row["ExchangeValue"] = item.ExchangeValue;
                    row["RealAmountFormatted"] = item.RealAmount;
                    row["TIPO_UNIDAD_COMBUSTIBLE"] = ECOsystem.Utilities.Session.GetCustomerCapacityUnitName();
                    row["IsInternational"] = item.IsInternational;
                    row["CustomerName"] = item.CustomerName;
                    row["CustomerBranchName"] = item.CustomerBranchName;
                    row["IsCustomerBranch"] = item.IsCustomerBranch;
                    dt.Rows.Add(row);                   
                }

                return dt;
            }
        }     

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
