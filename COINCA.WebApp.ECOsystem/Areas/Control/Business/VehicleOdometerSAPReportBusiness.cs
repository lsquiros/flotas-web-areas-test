﻿using  ECOsystem.Areas.Control.Models;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Business
{
    public class VehicleOdometerSAPReportBusiness : IDisposable
    {
        public IEnumerable<VehicleOdometerSAPReport> VehicleOdometerSAPReportRetrieve(int? CostCenterId)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<VehicleOdometerSAPReport>("[Control].[Sp_VehicleOdometerSAPReport_Retrieve]",
                new
                {
                    CustomerId = Convert.ToInt32(ECOsystem.Utilities.Session.GetCustomerId()),
                    CostCenterId,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public DataTable GetReportData(int? CostCenterId)
        {
            var list = VehicleOdometerSAPReportRetrieve(CostCenterId);
            using (var dt = new DataTable())
            {
                var customerName = Session.GetCustomerInfo().DecryptedName;                                
                dt.Columns.Add("EndDate");
                dt.Columns.Add("ExternalID");
                dt.Columns.Add("Odometer");
                dt.Columns.Add("CustomerName");                
                foreach (var item in list)
                {
                    var dr = dt.NewRow();
                    dr["EndDate"] = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm:ss tt");
                    dr["ExternalID"] = item.ExternalID;
                    dr["Odometer"] = item.Odometer;
                    dr["CustomerName"] = customerName;                   
                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}