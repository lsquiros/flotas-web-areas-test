﻿/************************************************************************************************************
*  File    : VehicleFuelBusiness.cs
*  Summary : VehicleFuel Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;

using System.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Control.Models;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// VehicleFuel Class
    /// </summary>
    public class CostCentersByCreditBusiness : IDisposable
    {

        /// <summary>
        /// Retrieve Vehicle Fuel By Group
        /// </summary>
        /// <param name="model"></param>
        /// <returns>VehicleFuelBase Model to render view</returns>
        public CostCentersByCreditBase RetrieveCostCentersByCredit(CostCentersByCreditBase model)
        {
            var date = DateTime.ParseExact("01/" + model.Month + "/" + model.Year, "dd/M/yyyy", CultureInfo.CreateSpecificCulture("es-CR"));

            using (var business = new FuelsByCreditBusiness())
            {
                var fuelsByCredit = business.RetrieveFuelsByCredit(date);
                model.CreditInfo = fuelsByCredit.CreditInfo;
                model.AssignedCredit = fuelsByCredit.AssignedCredit;
                model.PctAssignedCredit = fuelsByCredit.PctAssignedCredit;
            }
            if (model.CreditInfo == null)
            {
                model.Data = new CostCentersByCredit();
                model.GridInfo = new CostCentersByCreditGrid();
                return model;
            }
            model.Data = new CostCentersByCredit { CostCenterId = model.CostCenterId, Month = model.Month, Year = model.Year, CurrencySymbol = model.CreditInfo.CurrencySymbol };
            model.GridInfo = RetrieveCostCentersByCreditList(new CostCentersByCredit { CostCenterId = model.CostCenterId, Year = model.Year, Month = model.Month });
            model.GridInfo.IsResult = false;
            return model;
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <param name="list">CostCentersValues List</param>
        public CostCentersByCreditGrid AddOrEditCostCentersByCredit(CostCentersByCredit model, IList<CostCentersValues> list = null)
        {
            IList<FuelsCost> fuels;
            
            fuels = RetrieveFuelsCost();            

            var aux = list == null ? ExecuteAddOrEditCostCentersByCredit(model, fuels) : ExecuteAddOrEditCostCentersByCredit(model, list, fuels);

            var result = RetrieveCostCentersByCreditList(model);
            foreach (var item in result.List)
            {
                var temp = aux.FirstOrDefault(x => x.VehicleId == item.VehicleId);
                item.Result = temp != null ? temp.Result : -100;
            }
            result.IsResult = true;
            return result;
        }


        /// <summary>
        /// Execute Add Or Edit Vehicle Fuel By Group
        /// </summary>
        /// <param name="model">Vehicle Fuel By Group</param>
        /// <param name="fuels">List of Fuels By Credit </param>
        /// <returns>List of Vehicle Fuel By Group</returns>
        private IList<CostCentersByCreditModel> ExecuteAddOrEditCostCentersByCredit(CostCentersByCredit model, IList<FuelsCost> fuels)
        {
            var aux = RetrieveCostCentersByCreditList(model).List.ToList();
            foreach (var item in aux)
            {
                var literPrice = fuels.Where(x => x.FuelId == item.DefaultFuelId).Select(x => x.LiterPrice).FirstOrDefault();
                item.Amount = model.Value;
                item.Liters = (literPrice != null && literPrice > 0)? (item.Amount / literPrice) : 0;
                
                var sqlResult = AddOrEditCostCentersByCredit(new VehicleFuel
                {
                    Id = item.Id,
                    VehicleId = item.VehicleId,
                    Liters = item.Liters,
                    Amount = item.Amount,
                    Month = model.Month,
                    Year = model.Year,
                    RowVersion = item.RowVersion
                });
                item.Result = sqlResult != null ? sqlResult.ErrorNumber : 0;
            }
            return aux;
        }

        /// <summary>
        /// Execute Add Or Edit Vehicle Fuel By Group
        /// </summary>
        /// <param name="model">Vehicle Fuel By Group</param>
        /// /// <param name="list">CostCentersValues List</param>
        /// <param name="fuels">List of Fuels By Credit </param>
        /// <returns>List of Vehicle Fuel By Group</returns>
        private IList<CostCentersByCreditModel> ExecuteAddOrEditCostCentersByCredit(CostCentersByCredit model, IList<CostCentersValues> list, IList<FuelsCost> fuels)
        {
            var aux = RetrieveCostCentersByCreditList(model).List.ToList();
            foreach (var item in aux)
            {
                var literPrice = fuels.Where(x => x.FuelId == item.DefaultFuelId).Select(x => x.LiterPrice).FirstOrDefault();
                item.Amount = list.Where(x => x.VehicleId == item.VehicleId).Select(x => x.Value).FirstOrDefault();
                item.Liters = (item.Amount / literPrice);

                var sqlResult = AddOrEditCostCentersByCredit(new VehicleFuel
                {
                    Id = item.Id,
                    VehicleId = item.VehicleId,
                    Liters = item.Liters,
                    Amount = item.Amount,
                    Month = model.Month,
                    Year = model.Year,
                    RowVersion = item.RowVersion
                });
                item.Result = sqlResult != null ? sqlResult.ErrorNumber : 0;
            }
            return aux;
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleFuel
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public SqlError AddOrEditCostCentersByCredit(VehicleFuel model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<SqlError>("[Control].[Sp_CostCentersByCredit_AddOrEdit]",
                    new
                    {
                        model.Id,
                        model.VehicleId,
                        model.Liters,
                        model.Amount,
                        model.Month,
                        model.Year,
                        CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                        model.LoggedUserId,
                        model.RowVersion
                    }).FirstOrDefault();
            }

        }

        /// <summary>
        /// Retrieve List of Vehicle Fuel By Group
        /// </summary>
        /// <param name="model">Vehicle Fuel By Group model</param>
        /// <returns>CostCentersByCreditGrid Model to render view</returns>
        public CostCentersByCreditGrid RetrieveCostCentersByCreditList(CostCentersByCredit model)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = new CostCentersByCreditGrid
                {
                    List = dba.ExecuteReader<CostCentersByCreditModel>("[Control].[Sp_CostCentersByCredit_Retrieve]",
                        new
                        {
                            model.CostCenterId,
                            model.Year,
                            model.Month
                        })
                };
                return result;
            }
        }

        /// <summary>
        /// Retrieve Fuels Cost
        /// </summary>
        /// <returns></returns>
        public IList<FuelsCost> RetrieveFuelsCost()
        {
            IList<FuelsCost> fuels;
            using (var dba = new DataBaseAccess())
            {
                fuels = dba.ExecuteReader<FuelsCost>("[Control].[Sp_FuelsCost_Retrieve]",
                new
                {
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                });
            }
            return fuels;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
