﻿/************************************************************************************************************
*  File    : AccumulatedFuelsReportBusiness.cs
*  Summary : AccumulatedFuelsReport Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// AccumulatedFuelsReport Class
    /// </summary>
    public class AccumulatedFuelsReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Current Fuels Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of AccumulatedFuelsReportBase in order to load the chart</returns>
        public AccumulatedFuelsReportBase RetrieveAccumulatedFuelsReport(ControlFuelsReportsBase parameters)
        {
            var result = new AccumulatedFuelsReportBase();
            
            GetReportData(parameters, result);
            
            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(ECOsystem.Utilities.Miscellaneous.GenerateColors().Take(result.MonthCount)));
            result.Parameters.AlphaColors = new HtmlString(JsonConvert.SerializeObject(ECOsystem.Utilities.Miscellaneous.GenerateAlphaColors().Take(result.MonthCount)));
            result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(ECOsystem.Utilities.Miscellaneous.GenerateHighlighColors().Take(result.MonthCount)));
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            
            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(ControlFuelsReportsBase parameters, AccumulatedFuelsReportBase result)
        {
            if (parameters.ReportFuelTypeId == null) return;

            switch ((ReportFuelTypes)parameters.ReportFuelTypeId)
            {
                case ReportFuelTypes.Vehicles:
                    GetReportDataByVehicle(parameters, result);
                    break;
                case ReportFuelTypes.VehicleGroups:
                    GetReportDataByVehicleGroup(parameters, result);
                    break;
                case ReportFuelTypes.VehicleCostCenters:
                    GetReportDataByVehicleCostCenters(parameters, result);
                    break;
            }
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicleCostCenters(ControlFuelsReportsBase parameters, AccumulatedFuelsReportBase result)
        {

            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<AccumulatedFuelsByVehicleSubUnitReport>("[Control].[Sp_AccumulatedFuelsReportBySubUnit_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            IList<IList<decimal?>> data =
                result.List.Select(x => x.Year + "/" + x.Month)
                    .Distinct()
                    .ToList()
                    .Select(
                        yearMonth =>
                            result.List.Where(x => (x.Year + "/" + x.Month).Equals(yearMonth))
                                .Select(x => x.CapacityUnitValue as decimal?)
                                .ToList())
                    .Cast<IList<decimal?>>()
                    .ToList();

            result.MonthCount = result.List.Select(x => x.Year + "/" + x.Month).Distinct().ToList().Count();
            result.VehicleCount = result.List.Select(x => x.CostCenterName).Distinct().ToList().Count();
            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.CostCenterName).Distinct().ToList()));
            result.Parameters.Titles = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => new { x.Month, x.MonthName, x.Year }).Distinct().OrderByDescending(x => x.Year).ThenByDescending(x => x.Month).Select(x => x.MonthName + "/" + x.Year).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(data));
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicleGroup(ControlFuelsReportsBase parameters, AccumulatedFuelsReportBase result)
        {

            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<AccumulatedFuelsByVehicleGroupReport>("[Control].[Sp_AccumulatedFuelsReportByVehicleGroup_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            IList<IList<decimal?>> data =
                result.List.Select(x => x.Year + "/" + x.Month)
                    .Distinct()
                    .ToList()
                    .Select(
                        yearMonth =>
                            result.List.Where(x => (x.Year + "/" + x.Month).Equals(yearMonth))
                                .Select(x => x.CapacityUnitValue as decimal?)
                                .ToList())
                    .Cast<IList<decimal?>>()
                    .ToList();

            result.MonthCount = result.List.Select(x => x.Year + "/" + x.Month).Distinct().ToList().Count();
            result.VehicleCount = result.List.Select(x => x.VehicleGroupName).Distinct().ToList().Count();
            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.VehicleGroupName).Distinct().ToList()));
            result.Parameters.Titles = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => new { x.Month, x.MonthName, x.Year }).Distinct().OrderByDescending(x => x.Year).ThenByDescending(x => x.Month).Select(x => x.MonthName + "/" + x.Year).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(data));
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicle(ControlFuelsReportsBase parameters, AccumulatedFuelsReportBase result)
        {

            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<AccumulatedFuelsByVehicle>("[Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId,
                        Flag = 1
                    });
            }

            IList<IList<decimal?>> data =
                result.List.Select(x => x.Year + "/" + x.Month)
                    .Distinct()
                    .ToList()
                    .Select(
                        yearMonth =>
                            result.List.Where(x => (x.Year + "/" + x.Month).Equals(yearMonth))
                                .Select(x => x.CapacityUnitValue as decimal?)
                                .ToList())
                    .Cast<IList<decimal?>>()
                    .ToList();

            result.MonthCount = result.List.Select(x => x.Year + "/" + x.Month).Distinct().ToList().Count();
            result.VehicleCount = result.List.Select(x => x.PlateNumber).Distinct().ToList().Count();
            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.PlateNumber).Distinct().ToList()));
            result.Parameters.Titles = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => new {x.Month, x.MonthName ,x.Year}).Distinct().OrderByDescending(x=>x.Year).ThenByDescending(x=>x.Month) .Select(x => x.MonthName + "/" + x.Year).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(data));
        }


        /// <summary>
        /// Generate Real Vs Budget FuelsReport Excel
        /// </summary>
        /// <returns>A model of AccumulatedFuelsReportBase in order to load the chart</returns>
        public byte[] GenerateAccumulatedFuelsReportExcel(ControlFuelsReportsBase parameters)
        {
            var result = new AccumulatedFuelsReportBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Acumulado Mensual del Consumo en Litros", result.List.ToList());
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}