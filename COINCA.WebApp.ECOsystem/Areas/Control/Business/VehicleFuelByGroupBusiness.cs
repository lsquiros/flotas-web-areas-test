﻿/************************************************************************************************************
*  File    : VehicleFuelBusiness.cs
*  Summary : VehicleFuel Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;

using System.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using System.IO;
using ECOsystem.Utilities;
using System.Data;
using ECOsystem.Models.Core;

namespace  ECOsystem.Areas.Control.Business.Control
{
    public class VehicleFuelByGroupBusiness : IDisposable
    {
        #region [Normally Get Data]
        public List<FuelDistribution> RetrieveData(DistributionParameters model, Customers customer)
        {
            customer = customer == null ? Session.GetCustomerInfo() : customer;

            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<FuelDistribution>("[Control].[SP_FuelDistribution_Retrieve]",
                    new
                    {
                        model.VehicleId,
                        model.UserId,
                        model.VehicleGroupId,
                        model.CostCenterId,
                        customer.CustomerId,
                        LoggedUserId = Session.GetUserInfo().UserId,
                        model.IssueForId,
                        model.IsSum,
                        model.FuelId
                    }).ToList();
            }
        }

        private List<FuelDistribution> GetAdditionalDetail(DistributionParameters model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<FuelDistribution>("[Control].[SP_FuelDistributionAdditionalDetail_Retrieve]",
                    new
                    {
                        model.VehicleId,
                        model.UserId,
                        model.VehicleGroupId,
                        model.CostCenterId,
                        CustomerId = Session.GetCustomerId(),
                        LoggedUserId = Session.GetUserInfo().UserId,
                        model.IssueForId
                    }).ToList();
            }
        }
        #endregion

        #region [Get Data with Alert]
        private List<FuelDistribution> GetAdditionalAlertData()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<FuelDistribution>("[Control].[Sp_AdditionalVehicleFuelLowPorcentage_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        Session.GetUserInfo().UserId
                    }).ToList();
            }
        }

        /// <summary>
        /// Get a detailed list of vehicles with a budget alert
        /// </summary>
        /// <param name="model"></param>
        /// <returns>List<FuelDistribution></returns>
        private List<FuelDistribution> GetAdditionalDetailAlert(DistributionParameters model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<FuelDistribution>("[Control].[SP_FuelDistAddDetailAlert_Retrieve]",
                    new
                    {
                        model.VehicleId,
                        model.UserId,
                        model.VehicleGroupId,
                        model.CostCenterId,
                        CustomerId = Session.GetCustomerId(),
                        LoggedUserId = Session.GetUserInfo().UserId,
                        model.IssueForId
                    }).ToList();
            }
        }
        #endregion

        public List<FuelDistribution> RetrieveAdditionalData(DistributionParameters model, List<FuelDistribution> list, bool? alert = null)
        {
            if (alert != null && (bool)alert)
            {
                if (list.Count == 0) list = GetAdditionalAlertData();
                model.IssueForId = (int)Session.GetCustomerInfo().IssueForId;
            }
            else if (list.Count == 0) list = RetrieveData(model, null);

            var detail = (alert != null && (bool)alert) ? GetAdditionalDetailAlert(model) : GetAdditionalDetail(model);

            foreach (var item in detail) list.Where(x => item.VehicleId != null ? x.VehicleId == item.VehicleId : x.DriverId == item.DriverId).ToList()
                    .ForEach(s => { s.RealAmount = item.RealAmount; s.TotalAssigned = item.TotalAssigned; s.Total = item.Total; 
                        /*if (item.DriverId == null)*/ s.AdditionalAmount = 0; s.AdditionalLiters = 0; s.Symbol = "+"; });
            return list;
        }

        public List<FuelDistribution> FuelDistributionAddOrEdit(List<FuelDistribution> list)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<FuelDistribution>("[Control].[SP_FuelDistribution_AddOrEdit_Web]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        XmlData = ECOsystem.Utilities.Miscellaneous.GetXML(list),
                        Session.GetUserInfo().LoggedUserId
                    }).ToList();
            }
        }

        public byte[] CreateTxtFormat(IEnumerable<VehicleFuelErrors> modelList, int Error_Count, int Success_Count)
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    var sw = new StreamWriter(ms);
                    sw.WriteLine("Cantidad de Registros Agregados Satisfactoriamente: " + Success_Count);
                    sw.WriteLine("Cantidad de Registros Fallidos: " + Error_Count);
                    sw.WriteLine("");
                    foreach (var item in modelList)
                    {
                        sw.WriteLine(item.Item);
                        sw.Flush();
                    }

                    return ms.ToArray();
                }
            }
            catch (Exception)
            {
                throw new SystemException("System Exception when server tries to generate excel file");
            }
        }

        public DataTable GetReportData(List<FuelDistribution> list)
        {
            using (var dt = new DataTable())
            {
                if (Session.GetCustomerInfo().IssueForId == 101)
                {
                    dt.Columns.Add("Matricula");
                    dt.Columns.Add("Vehiculo");
                    dt.Columns.Add("Modelo");
                    dt.Columns.Add("Combustible");
                    dt.Columns.Add("Utilizado", typeof(decimal));
                    dt.Columns.Add("DisponibleLitros");
                    dt.Columns.Add("Disponible");
                    dt.Columns.Add("Porcentaje");
                    dt.Columns.Add("Resultado");

                    foreach (var item in list)
                    {
                        var dr = dt.NewRow();
                        dr["Matricula"] = item.PlateId;
                        dr["Vehiculo"] = item.VehicleName;
                        dr["Modelo"] = item.VehicleModel;
                        dr["Combustible"] = item.FuelName;
                        dr["Utilizado"] = item.Assigned;
                        dr["DisponibleLitros"] = item.AvailableLiters;
                        dr["Disponible"] = item.Available;
                        try
                        {
                            dr["Porcentaje"] = Convert.ToDouble(item.AvailableLiters / item.AdditionalLiters * 100).ToString("F", CultureInfo.InvariantCulture);
                        }
                        catch (Exception e)
                        { dr["Porcentaje"] = 1; }
                        dr["Resultado"] = item.ResultStr;

                        dt.Rows.Add(dr);
                    }
                }
                else
                {
                    dt.Columns.Add("Conductor");
                    dt.Columns.Add("Identificacion");
                    dt.Columns.Add("MontoAsignado", typeof(decimal));
                    dt.Columns.Add("Utilizado", typeof(decimal));
                    dt.Columns.Add("MontoAdicional", typeof(decimal));
                    dt.Columns.Add("Porcentaje", typeof(decimal));
                    dt.Columns.Add("Resultado");

                    foreach (var item in list)
                    {
                        var dr = dt.NewRow();
                        dr["Conductor"] = item.DecryptedName;
                        dr["Identificacion"] = item.DecryptedIdentification;
                        dr["MontoAsignado"] = item.Amount;
                        dr["Utilizado"] = item.Assigned;
                        dr["MontoAdicional"] = item.AdditionalAmount;
                        try
                        {
                            dr["Porcentaje"] = Convert.ToDouble(((item.Available + item.AdditionalAmount) * 100) / item.Amount).ToString("F", CultureInfo.InvariantCulture);
                        }
                        catch (Exception e)
                        { dr["Porcentaje"] = 1; }

                        dr["Resultado"] = item.ResultStr;
                        dt.Rows.Add(dr);
                    }
                }
                return dt;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
