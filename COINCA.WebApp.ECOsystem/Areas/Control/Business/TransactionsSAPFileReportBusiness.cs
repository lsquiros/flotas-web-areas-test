﻿using  ECOsystem.Areas.Control.Models;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Business
{
    public class TransactionsSAPFileReportBusiness : IDisposable
    {
        public IEnumerable<TransactionsSAPFileReport> ReportDataRetrieve(ControlFuelsReportsBase model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<TransactionsSAPFileReport>("[Control].[Sp_TransactionsSAPFileReport_Retrieve]", new
                {
                    model.Month,
                    model.Year, 
                    model.StartDate,
                    model.EndDate, 
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public DataTable GetReportData(ControlFuelsReportsBase model)
        {
            var data = ReportDataRetrieve(model);

            using (var dt = new DataTable())
            {
                dt.Columns.Add("SAPProv");
                dt.Columns.Add("LegalId");
                dt.Columns.Add("ServiceStation");
                dt.Columns.Add("InvoiceDate");
                dt.Columns.Add("Invoice");
                dt.Columns.Add("Amount");
                dt.Columns.Add("CostCenter");
                dt.Columns.Add("Unit");
                dt.Columns.Add("Vehicle");
                dt.Columns.Add("PlateId");
                                
                foreach (var item in data)
	            {
                    var dr = dt.NewRow();
                    dr["SAPProv"] = item.SAPProv;
                    dr["LegalId"] = item.LegalId;
                    dr["ServiceStation"] = item.ServiceStation;
                    dr["InvoiceDate"] = item.InvoiceDate.ToString("dd/MM/yyyy");
                    dr["Invoice"] = item.Invoice;
                    dr["Amount"] = item.Amount;
                    dr["CostCenter"] = item.CostCenter;
                    dr["Unit"] = item.Unit;
                    dr["Vehicle"] = item.Vehicle;
                    dr["PlateId"] = item.PlateId;
                    
                    dt.Rows.Add(dr);
	            }    
                return dt; 
            }
        }
        
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}