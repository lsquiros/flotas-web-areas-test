﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceReportBusiness.cs
*  Summary : Preventive Maintenance Report Business Methods
*  Author  : Napoleón Alvarado
*  Date    : 12/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// PreventiveMaintenanceReport Class
    /// </summary>
    public class PreventiveMaintenanceReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Preventive Maintenance Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of PreventiveMaintenanceReportBase in order to load the chart</returns>
        public PreventiveMaintenanceReportBase RetrievePreventiveMaintenanceReport(ControlPreventiveMaintenanceReportBase parameters)
        {
            var result = new PreventiveMaintenanceReportBase();
            GetReportData(parameters, result);
            result.Parameters.MaintenanceCatalogId = parameters.MaintenanceCatalogId;
            result.Parameters.Status = parameters.Status;
            result.Parameters.Date = parameters.Date;
            result.Parameters.DateStr = parameters.DateStr;
            result.Parameters.Odometer = parameters.Odometer;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.StartDateStr = parameters.StartDateStr;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.EndDateStr = parameters.EndDateStr;
            result.Parameters.CostCenter = parameters.CostCenter;
            result.Parameters.Key = parameters.Key;
            result.Menus = new List<AccountMenus>();
            //result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(ControlPreventiveMaintenanceReportBase parameters, PreventiveMaintenanceReportBase result)
        {
            GetReportDataPreventiveMaintenance(parameters, result);
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataPreventiveMaintenance(ControlPreventiveMaintenanceReportBase parameters, PreventiveMaintenanceReportBase result)
        {

            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<PreventiveMaintenanceReport>("[Control].[Sp_PreventiveMaintenanceByVehicleReport_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        MaintenanceCatalogId = parameters.MaintenanceCatalogId,
                        Status = parameters.Status,
                        Date = parameters.Date,
                        Odometer = parameters.Odometer,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate,
                        CostCenter = parameters.CostCenter,
                        Key = parameters.Key,
                        user.UserId,
                        FilterType = parameters.FilterType
                    });
            }
        }

        /// <summary>
        /// Generate Real Vs Budget FuelsReport Excel
        /// </summary>
        /// <returns>A model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public byte[] GeneratePreventiveMaintenanceReportExcel(ControlPreventiveMaintenanceReportBase parameters)
        {
            var result = new PreventiveMaintenanceReportBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Mantenimiento Preventivo", result.List.ToList());
            }

        }



        /// <summary>
        /// Retrieve Types JSON
        /// </summary>       
        public IEnumerable<dynamic> RetrieveTypesJSON(int? FilterType)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PreventiveMaintenanceReport>("[General].[Sp_PreventiveMaintenanceCatalog_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        FilterType,
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}