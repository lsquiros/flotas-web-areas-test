﻿/************************************************************************************************************
*  File    : PerformanceByVehicleReportBusiness.cs
*  Summary : PerformanceByVehicleReport Business Methods
*  Author  : Andres Oviedo
*  Date    : 23/01/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Data;
using System.Globalization;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// PerformanceByVehicleReport Class
    /// </summary>
    public class PerformanceByVehicleComparativeReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Performance By Vehicle Report
        /// </summary>
        /// <param name="p">parameters options</param>
        /// <returns>An model of PerformanceByVehicleReportBase in order to load the chart</returns>
        public PerformanceByVehicleComparativeReportBase RetrievePerformanceByVehicleComparativeReport(PerformanceByVehicleComparativeReportParameters p)
        {
            p.ParametersInBase64 = "";
            p.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(p));
            if (p.ReportComparativeId == 100) //100=Vehicle
            {
                return new PerformanceByVehicleComparativeReportBase
                {
                    List = GetPerformanceByVehicleComparativeData(GetPerformanceByVehicleComparativeReportRawData(p), p.GroupBy),
                    Parameters = p
                };
            }

            //else -> 200=Manufacturer, 300=VehicleModel, 400=VehicleType
            return new PerformanceByVehicleComparativeReportBase
            {
                GroupList = GetPerformanceByVehicleGroupedComparativeData(GetPerformanceByVehicleComparativeReportRawData(p), p.GroupBy, p.ReportComparativeId),
                Parameters = p
            };
        }

        /// <summary>
        /// Retrieve Performance By Vehicle Report
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <param name="week"></param>
        /// <param name="day"></param>
        /// <param name="option"></param>
        /// <returns>An model of PerformanceByVehicleReportBase in order to load the chart</returns>
        public IEnumerable<PerformanceByVehicleDetails> RetrievePerformanceByVehicleComparativeDetail(int vehicleId, int? year, int? month, int? week, string day, int option)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PerformanceByVehicleDetails>("[Control].[Sp_PerformanceByVehicleComparativeDetail_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId,
                        Year = year,
                        Month = month,
                        Week = week,
                        Day = day,
                        Option = option
                    });
            }
        }

        /// <summary>
        /// Get Performance By Vehicle Comparative Data
        /// </summary>
        /// <param name="list"></param>
        /// <param name="groupBy"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleComparativeReports> GetPerformanceByVehicleComparativeData(IEnumerable<PerformanceByVehicleComparativeReports> list, int groupBy)
        {
            switch (groupBy)
            {
                case 1: //Year
                    return GetPerformanceByVehicleComparativeReportGroupByYear(list);
                case 2: //Month
                    return GetPerformanceByVehicleComparativeReportGroupByMonth(list);
                case 3: //Day
                    return GetPerformanceByVehicleComparativeReportGroupByDay(list);
                case 4: //Week
                    return GetPerformanceByVehicleComparativeReportGroupByWeek(list);
            }
            return null;
        }

        /// <summary>
        /// Get Performance By Vehicle Comparative Data
        /// </summary>
        /// <param name="list"></param>
        /// <param name="groupBy"></param>
        /// <param name="comparativeId"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleGroupedComparativeReports> GetPerformanceByVehicleGroupedComparativeData(IEnumerable<PerformanceByVehicleComparativeReports> list, int groupBy, int comparativeId)
        {
            var groupList = GetPerformanceByVehicleProcessGroupedComparativeData(list, comparativeId);

            switch (groupBy)
            {
                case 1: //Year
                    return GetPerformanceByVehicleGroupedComparativeReportGroupByYear(groupList);
                case 2: //Month
                    return GetPerformanceByVehicleGroupedComparativeReportGroupByMonth(groupList);
                case 3: //Day
                    return GetPerformanceByVehicleGroupedComparativeReportGroupByDay(groupList);
                case 4: //Week
                    return GetPerformanceByVehicleGroupedComparativeReportGroupByWeek(groupList);
            }
            return null;
        }


        /// <summary>
        /// Get Performance By Vehicle Process Grouped ComparativeData
        /// </summary>
        /// <param name="list"></param>
        /// <param name="comparativeId"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleGroupedComparativeReports> GetPerformanceByVehicleProcessGroupedComparativeData(IEnumerable<PerformanceByVehicleComparativeReports> list, int comparativeId)
        {

            switch (comparativeId)
            {
                case 200: //200=Manufacturer
                    return list
                        .GroupBy(m => new
                                {
                                    m.Manufacturer,
                                    m.TrxDate,
                                    //m.UnitName,
                                    //m.CostCenterName,
                                    m.TrxWeekNumber
                                })
                        .Select(m => new PerformanceByVehicleGroupedComparativeReports
                        {
                            GroupOption = m.Key.Manufacturer,
                            GroupValue = (m.Key.Manufacturer /*+ "/" + m.Key.UnitName + "/" + m.Key.CostCenterName*/).GetHashCode(),
                            //UnitName = m.Key.UnitName,
                            //CostCenterName = m.Key.CostCenterName,
                            TrxDate = m.Key.TrxDate,
                            TrxWeekNumber = m.Key.TrxWeekNumber,
                            TrxReportedLiters = m.Sum(x => x.TrxReportedLiters),
                            TrxTraveledOdometer = m.Sum(x => x.TrxTraveledOdometer),
                            TrxPerformance = m.Average(x => x.TrxPerformance),
                            GpsTraveledOdometer = m.Sum(x => x.GpsTraveledOdometer),
                            GpsPerformance = m.Average(x => x.GpsPerformance),
                            GroupBy = 3 //Day
                        }).ToList();
                case 300:  //300=VehicleModel
                    return list
                        .GroupBy(m => new
                        {
                            VehicleModel = m.Manufacturer + "/" + m.VehicleModel,
                            m.TrxDate,
                            m.UnitName,
                            m.CostCenterName,
                            m.TrxWeekNumber
                        })
                        .Select(m => new PerformanceByVehicleGroupedComparativeReports
                        {
                            GroupOption = m.Key.VehicleModel,
                            GroupValue = (m.Key.VehicleModel + "/" + m.Key.UnitName + "/" + m.Key.CostCenterName).GetHashCode(),
                            UnitName = m.Key.UnitName,
                            CostCenterName = m.Key.CostCenterName,
                            TrxDate = m.Key.TrxDate,
                            TrxWeekNumber = m.Key.TrxWeekNumber,
                            TrxReportedLiters = m.Sum(x => x.TrxReportedLiters),
                            TrxTraveledOdometer = m.Sum(x => x.TrxTraveledOdometer),
                            TrxPerformance = m.Average(x => x.TrxPerformance),
                            GpsTraveledOdometer = m.Sum(x => x.GpsTraveledOdometer),
                            GpsPerformance = m.Average(x => x.GpsPerformance),
                            GroupBy = 3 //Day
                        }).ToList();
                case 400: //400=VehicleType
                    return list
                        .GroupBy(m => new
                        {
                            VehicleType = m.Manufacturer + "/" + m.VehicleModel + "/" + m.VehicleType,
                            m.TrxDate,
                            m.UnitName,
                            m.CostCenterName,
                            m.TrxWeekNumber
                        })
                        .Select(m => new PerformanceByVehicleGroupedComparativeReports
                        {
                            GroupOption = m.Key.VehicleType,
                            GroupValue = (m.Key.VehicleType + "/" + m.Key.UnitName + "/" + m.Key.CostCenterName).GetHashCode(),
                            UnitName = m.Key.UnitName,
                            CostCenterName = m.Key.CostCenterName,
                            TrxDate = m.Key.TrxDate,
                            TrxWeekNumber = m.Key.TrxWeekNumber,
                            TrxReportedLiters = m.Sum(x => x.TrxReportedLiters),
                            TrxTraveledOdometer = m.Sum(x => x.TrxTraveledOdometer),
                            TrxPerformance = m.Average(x => x.TrxPerformance),
                            GpsTraveledOdometer = m.Sum(x => x.GpsTraveledOdometer),
                            GpsPerformance = m.Average(x => x.GpsPerformance),
                            GroupBy = 3 //Day
                        }).ToList();
            }

            return null;
        }
        /// <summary>
        /// Get Performance By Vehicle Comparative Report Group By Month
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleComparativeReports> GetPerformanceByVehicleComparativeReportGroupByDay(IEnumerable<PerformanceByVehicleComparativeReports> list)
        {
            int? vehicleId = null;
            foreach (var item in list)
            {
                if (vehicleId != item.VehicleId)
                {
                    vehicleId = item.VehicleId;
                }
                else
                {
                    item.PlateId = "";
                    item.VehicleName = "";
                    item.Manufacturer = "";
                    item.VehicleModel = "";
                    item.VehicleType = "";
                    item.UnitName = "";
                    item.CostCenterName = "";
                    item.DefaultPerformance = 0;
                }
            }
            return list;
        }

        /// <summary>
        /// Get Performance By Vehicle Comparative Report Group By Month
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleComparativeReports> GetPerformanceByVehicleComparativeReportGroupByMonth(IEnumerable<PerformanceByVehicleComparativeReports> list)
        {
            var result = new List<PerformanceByVehicleComparativeReports>();
            int? vehicleId = null;

            foreach (var item in list
                .GroupBy(m => new { m.TrxDate.Month, m.TrxDate.Year, m.VehicleId, m.PlateId, m.VehicleName, m.Manufacturer, m.VehicleYear, m.VehicleModel, m.VehicleType, m.UnitName, m.CostCenterName, m.DefaultPerformance })
                .Select(m => new PerformanceByVehicleComparativeReports
                {
                    VehicleId = m.Key.VehicleId,
                    PlateId = m.Key.PlateId,
                    VehicleName = m.Key.VehicleName,
                    Manufacturer = m.Key.Manufacturer,
                    VehicleYear = m.Key.VehicleYear,
                    VehicleModel = m.Key.VehicleModel,
                    VehicleType = m.Key.VehicleType,
                    UnitName = m.Key.UnitName,
                    CostCenterName = m.Key.CostCenterName,
                    TrxDate = new DateTime(m.Key.Year, m.Key.Month, 1),
                    TrxReportedLiters = m.Sum(x => x.TrxReportedLiters),
                    TrxTraveledOdometer = m.Sum(x => x.TrxTraveledOdometer), 
                    TrxPerformance = m.Average(x => x.TrxPerformance),
                    GpsTraveledOdometer = m.FirstOrDefault().GpsTraveledOdometer,
                    GpsPerformance = m.Sum(x => x.TrxReportedLiters) > 0 ? ((m.FirstOrDefault().GpsTraveledOdometer / m.Sum(x => x.TrxReportedLiters))) : 0,
                    DefaultPerformance = m.Key.DefaultPerformance,
                    GroupBy = 2 //Month
                }).ToList())
            {
                if (vehicleId != item.VehicleId)
                {
                    vehicleId = item.VehicleId;
                }
                else
                {
                    item.PlateId = "";
                    item.VehicleName = "";
                    item.Manufacturer = "";
                    item.VehicleModel = "";
                    item.VehicleType = "";
                    item.UnitName = "";
                    item.CostCenterName = "";
                }
                result.Add(item);
            }
            return result;
        }

        /// <summary>
        /// Get Performance By Vehicle Comparative Report Group By Month
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleComparativeReports> GetPerformanceByVehicleComparativeReportGroupByWeek(IEnumerable<PerformanceByVehicleComparativeReports> list)
        {
            var result = new List<PerformanceByVehicleComparativeReports>();
            int? vehicleId = null;
            var count = 0;
            foreach (var item in list
                .GroupBy(m => new { m.TrxWeekNumber, m.TrxDate.Month, m.TrxDate.Year, m.VehicleId, m.PlateId, m.VehicleName, m.Manufacturer, m.VehicleYear, m.VehicleModel, m.VehicleType, m.UnitName, m.CostCenterName,m.DefaultPerformance  })
                .Select(m => new PerformanceByVehicleComparativeReports
                {
                    VehicleId = m.Key.VehicleId,
                    PlateId = m.Key.PlateId,
                    VehicleName = m.Key.VehicleName,
                    Manufacturer = m.Key.Manufacturer,
                    VehicleYear = m.Key.VehicleYear,
                    VehicleModel = m.Key.VehicleModel,
                    VehicleType = m.Key.VehicleType,
                    UnitName = m.Key.UnitName,
                    CostCenterName = m.Key.CostCenterName,
                    TrxDate = new DateTime(m.Key.Year, m.Key.Month, 1),
                    TrxWeekNumber = m.Key.TrxWeekNumber,
                    TrxReportedLiters = m.Sum(x => x.TrxReportedLiters),
                    TrxTraveledOdometer = m.Sum(x => x.TrxTraveledOdometer),
                    TrxPerformance = m.Average(x => x.TrxPerformance),
                    GpsTraveledOdometer = m.Sum(x => x.GpsTraveledOdometer),
                    GpsPerformance = m.Average(x => x.GpsPerformance),
                    DefaultPerformance = m.Key.DefaultPerformance,
                    GroupBy = 4 //Week
                }).ToList())
            {
                if (vehicleId != item.VehicleId)
                {
                    count = 1;
                    vehicleId = item.VehicleId;
                    item.TrxDisplayWeekNumber = count++;
                }
                else
                {
                    item.PlateId = "";
                    item.VehicleName = "";
                    item.Manufacturer = "";
                    item.VehicleModel = "";
                    item.VehicleType = "";
                    item.UnitName = "";
                    item.CostCenterName = "";
                    item.TrxDisplayWeekNumber = count++;
                }
                result.Add(item);
            }
            return result;
        }

        /// <summary>
        /// Get Performance By Vehicle Comparative Report Group By Year
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleComparativeReports> GetPerformanceByVehicleComparativeReportGroupByYear(IEnumerable<PerformanceByVehicleComparativeReports> list)
        {

            var result = new List<PerformanceByVehicleComparativeReports>();
            int? vehicleId = null;

            foreach (var item in list
                .GroupBy(m => new { m.TrxDate.Year, m.VehicleId, m.PlateId, m.VehicleName, m.Manufacturer, m.VehicleYear, m.VehicleModel, m.VehicleType, m.UnitName, m.CostCenterName,m.DefaultPerformance  })
                .Select(m => new PerformanceByVehicleComparativeReports
                {
                    VehicleId = m.Key.VehicleId,
                    PlateId = m.Key.PlateId,
                    VehicleName = m.Key.VehicleName,
                    Manufacturer = m.Key.Manufacturer,
                    VehicleYear = m.Key.VehicleYear,
                    VehicleModel = m.Key.VehicleModel,
                    VehicleType = m.Key.VehicleType,
                    UnitName = m.Key.UnitName,
                    CostCenterName = m.Key.CostCenterName,
                    TrxDate = new DateTime(m.Key.Year, 1, 1),
                    TrxReportedLiters = m.Sum(x => x.TrxReportedLiters),
                    TrxTraveledOdometer = m.Sum(x => x.TrxTraveledOdometer),
                    TrxPerformance = m.Average(x => x.TrxPerformance),
                    GpsTraveledOdometer = m.Sum(x => x.GpsTraveledOdometer),
                    GpsPerformance = m.Average(x => x.GpsPerformance),
                    DefaultPerformance = m.Key.DefaultPerformance,
                    GroupBy = 1 //Year
                }).ToList())
            {
                if (vehicleId != item.VehicleId)
                {
                    vehicleId = item.VehicleId;
                }
                else
                {
                    item.PlateId = "";
                    item.VehicleName = "";
                    item.Manufacturer = "";
                    item.VehicleModel = "";
                    item.VehicleType = "";
                    item.UnitName = "";
                    item.CostCenterName = "";
                }
                result.Add(item);
            }
            return result;
        }




        /// <summary>
        /// Get Performance By Vehicle Comparative Report Group By Month
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleGroupedComparativeReports> GetPerformanceByVehicleGroupedComparativeReportGroupByDay(IEnumerable<PerformanceByVehicleGroupedComparativeReports> list)
        {
            var groupOption = "";
            foreach (var item in list)
            {
                if (!groupOption.Equals(item.GroupOption + "/" + item.UnitName + "/" + item.CostCenterName))
                {
                    groupOption = item.GroupOption + "/" + item.UnitName + "/" + item.CostCenterName;
                }
                else
                {
                    item.GroupOption = "";
                    item.UnitName = "";
                    item.CostCenterName = "";
                }
            }
            return list;
        }

        /// <summary>
        /// Get Performance By Vehicle Comparative Report Group By Month
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleGroupedComparativeReports> GetPerformanceByVehicleGroupedComparativeReportGroupByMonth(IEnumerable<PerformanceByVehicleGroupedComparativeReports> list)
        {
            var result = new List<PerformanceByVehicleGroupedComparativeReports>();
            var groupOption = "";

            foreach (var item in list
                .GroupBy(m => new { m.TrxDate.Month, m.TrxDate.Year, m.GroupOption, m.GroupValue, m.UnitName, m.CostCenterName })
                .Select(m => new PerformanceByVehicleGroupedComparativeReports
                {
                    GroupOption = m.Key.GroupOption,
                    GroupValue = m.Key.GroupValue,
                    UnitName = m.Key.UnitName,
                    CostCenterName = m.Key.CostCenterName,
                    TrxDate = new DateTime(m.Key.Year, m.Key.Month, 1),
                    TrxReportedLiters = m.Sum(x => x.TrxReportedLiters),
                    TrxTraveledOdometer = m.Sum(x => x.TrxTraveledOdometer),
                    TrxPerformance = m.Average(x => x.TrxPerformance),
                    GpsTraveledOdometer = m.Sum(x => x.GpsTraveledOdometer),
                    GpsPerformance = m.Average(x => x.GpsPerformance),
                    GroupBy = 2 //Month
                }).ToList())
            {
                if (!groupOption.Equals(item.GroupOption + "/" + item.UnitName + "/" + item.CostCenterName))
                {
                    groupOption = item.GroupOption + "/" + item.UnitName + "/" + item.CostCenterName;
                }
                else
                {
                    item.GroupOption = "";
                    item.UnitName = "";
                    item.CostCenterName = "";
                }
                result.Add(item);
            }
            return result;
        }

        /// <summary>
        /// Get Performance By Vehicle Comparative Report Group By Month
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleGroupedComparativeReports> GetPerformanceByVehicleGroupedComparativeReportGroupByWeek(IEnumerable<PerformanceByVehicleGroupedComparativeReports> list)
        {
            var result = new List<PerformanceByVehicleGroupedComparativeReports>();
            var groupOption = "";
            var count = 0;
            foreach (var item in list
                .GroupBy(m => new { m.TrxWeekNumber, m.TrxDate.Month, m.TrxDate.Year, m.GroupOption, m.GroupValue, m.UnitName, m.CostCenterName })
                .Select(m => new PerformanceByVehicleGroupedComparativeReports
                {
                    GroupOption = m.Key.GroupOption,
                    GroupValue = m.Key.GroupValue,
                    UnitName = m.Key.UnitName,
                    CostCenterName = m.Key.CostCenterName,
                    TrxDate = new DateTime(m.Key.Year, m.Key.Month, 1),
                    TrxWeekNumber = m.Key.TrxWeekNumber,
                    TrxReportedLiters = m.Sum(x => x.TrxReportedLiters),
                    TrxTraveledOdometer = m.Sum(x => x.TrxTraveledOdometer),
                    TrxPerformance = m.Average(x => x.TrxPerformance),
                    GpsTraveledOdometer = m.Sum(x => x.GpsTraveledOdometer),
                    GpsPerformance = m.Average(x => x.GpsPerformance),
                    GroupBy = 4 //Week
                }).ToList())
            {
                if (!groupOption.Equals(item.GroupOption + "/" + item.UnitName + "/" + item.CostCenterName))
                {
                    groupOption = item.GroupOption + "/" + item.UnitName + "/" + item.CostCenterName;
                    count = 1;
                    item.TrxDisplayWeekNumber = count++;
                }
                else
                {
                    item.GroupOption = "";
                    item.UnitName = "";
                    item.CostCenterName = "";
                    item.TrxDisplayWeekNumber = count++;
                }
                result.Add(item);
            }
            return result;
        }

        /// <summary>
        /// Get Performance By Vehicle Comparative Report Group By Year
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleGroupedComparativeReports> GetPerformanceByVehicleGroupedComparativeReportGroupByYear(IEnumerable<PerformanceByVehicleGroupedComparativeReports> list)
        {

            var result = new List<PerformanceByVehicleGroupedComparativeReports>();
            var groupOption = "";

            foreach (var item in list
                .GroupBy(m => new { m.TrxDate.Year, m.GroupOption, m.GroupValue, m.UnitName, m.CostCenterName, m.DefaultPerformance  })
                .Select(m => new PerformanceByVehicleGroupedComparativeReports
                {
                    GroupOption = m.Key.GroupOption,
                    GroupValue = m.Key.GroupValue,
                    UnitName = m.Key.UnitName,
                    CostCenterName = m.Key.CostCenterName,
                    TrxDate = new DateTime(m.Key.Year, 1, 1),
                    TrxReportedLiters = m.Sum(x => x.TrxReportedLiters),
                    TrxTraveledOdometer = m.Sum(x => x.TrxTraveledOdometer),
                    TrxPerformance = m.Average(x => x.TrxPerformance),
                    GpsTraveledOdometer = m.Sum(x => x.GpsTraveledOdometer),
                    GpsPerformance = m.Average(x => x.GpsPerformance),
                    DefaultPerformance = m.Key.DefaultPerformance,
                    GroupBy = 1 //Year
                }).ToList())
            {
                if (!groupOption.Equals(item.GroupOption + "/" + item.UnitName + "/" + item.CostCenterName))
                {
                    groupOption = item.GroupOption + "/" + item.UnitName + "/" + item.CostCenterName;
                }
                else
                {
                    item.GroupOption = "";
                    item.UnitName = "";
                    item.CostCenterName = "";
                }
                result.Add(item);
            }
            return result;
        }



        /// <summary>
        /// Get Performance By Vehicle Comparative Report Raw Data
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private IEnumerable<PerformanceByVehicleComparativeReports> GetPerformanceByVehicleComparativeReportRawData(PerformanceByVehicleComparativeReportParameters p)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<PerformanceByVehicleComparativeReports>("[Control].[Sp_PerformanceByVehicleComparativeReport_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        p.Year,
                        p.Month,
                        p.StartDate,
                        p.EndDate,
                        p.VehicleId,
                        p.Manufacturer,
                        p.VehicleModel,
                        p.VehicleYear,
                        p.VehicleType,
                        p.GroupBy,
                        p.UnitId,
                        p.CostCenterId,
                        p.showZeros,
                        p.VehicleGroupId,
                        user.UserId
                    });
            }
        }




        /// <summary>
        /// Generate PerformanceByVehicleReport FuelsReport Excel
        /// </summary>
        /// <returns>A model of PerformanceByVehicleReportBase in order to load the chart</returns>
        public byte[] GeneratePerformanceByVehicleComparativeReportExcel(PerformanceByVehicleComparativeReportParameters parameters)
        {
            var result = RetrievePerformanceByVehicleComparativeReport(parameters);

            if (result == null) return null;
            using (var excel = new ExcelReport())
            {
                return result.Parameters.ReportComparativeId == 100 ? excel.CreateSpreadsheetWorkbook("Comparativo de Rendimiento", result.List.ToList()) : excel.CreateSpreadsheetWorkbook("Comparativo de Rendimiento", result.GroupList.ToList());
            }
        }

        public DataTable GetReportDownloadData(PerformanceByVehicleComparativeReportBase model) {
            try
            {
                var startDate = new DateTime();
                var endDate = new DateTime();

                if (model.Parameters.StartDate != null && model.Parameters.EndDate != null)
                {
                    startDate = model.Parameters.StartDate.HasValue ? model.Parameters.StartDate.Value : DateTime.Now;
                    endDate = model.Parameters.EndDate.HasValue ? model.Parameters.EndDate.Value : DateTime.Now;
                }
                else if (model.Parameters.Month != null && model.Parameters.Year != null)
                {
                    startDate = new DateTime((int)model.Parameters.Year, (int)model.Parameters.Month, 1, 0, 0, 0);
                    endDate = new DateTime((int)model.Parameters.Year, (int)model.Parameters.Month, DateTime.DaysInMonth((int)model.Parameters.Year, (int)model.Parameters.Month), 23, 59, 59);
                }

                using (var dt = new DataTable())
                {
                    dt.Columns.Add("Plate");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Manufacturer");
                    dt.Columns.Add("Year");
                    dt.Columns.Add("Model");
                    dt.Columns.Add("Type");
                    dt.Columns.Add("Unit");
                    dt.Columns.Add("CostCenter");
                    dt.Columns.Add("CapacityUnitValue");
                    dt.Columns.Add("Odometer");
                    dt.Columns.Add("TransmitionDate");
                    dt.Columns.Add("OdometerGPS");
                    dt.Columns.Add("TransactionPerformance");
                    dt.Columns.Add("GPSPerformance");
                    dt.Columns.Add("InitDate");
                    dt.Columns.Add("EndDate");
                    dt.Columns.Add("CapacityUnitName");
                    dt.Columns.Add("DefaultPerformance");
                    dt.Columns.Add("Company");
                    

                    var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                    var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                    //Get array letters
                    char[] letters = startDateStr.ToCharArray();
                    // upper case the first char
                    letters[0] = char.ToUpper(letters[0]);
                    // return the array made of the new char array
                    startDateStr = new string(letters);
                    letters = endDateStr.ToCharArray();
                    // upper case the first char
                    letters[0] = char.ToUpper(letters[0]);
                    // return the array made of the new char array
                    endDateStr = new string(letters);

                    foreach (var item in model.List)
                    {
                        DataRow row = dt.NewRow();
                        row["Plate"] = item.PlateId;
                        row["Name"] = item.VehicleName;
                        row["Manufacturer"] = item.Manufacturer;
                        row["Year"] = item.VehicleYear;
                        row["Model"] = item.VehicleModel;
                        row["Type"] = item.VehicleType ;
                        row["Unit"] = item.UnitName;
                        row["CostCenter"] = item.CostCenterName;
                        row["CapacityUnitValue"] = item.CapacityUnitValueStr;
                        row["Odometer"] = item.TrxTraveledOdometer;
                        row["TransmitionDate"] = item.TrxDateFormatted;
                        row["OdometerGPS"] = item.GpsTraveledOdometer;
                        row["TransactionPerformance"] = item.TrxPerformance;
                        row["GPSPerformance"] = item.GpsPerformance;
                        row["InitDate"] = startDateStr;
                        row["EndDate"] = endDateStr;
                        row["CapacityUnitName"] = ECOsystem.Utilities.Session.GetCustomerCapacityUnitName();
                        row["DefaultPerformance"] = item.DefaultPerformance;
                        row["Company"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                        dt.Rows.Add(row);
                    }

                    return dt;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}