﻿/************************************************************************************************************
*  File    : TransactionsBusiness.cs
*  Summary : Transactions Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using ECOsystem.Business.Administration;
using ECOsystem.Business.Settings;
using ECOsystem.DataAccess;
using ECO_Control.Models.Control;
using ECOsystem.Models.Administration;
using ECOsystem.Utilities;
using System.Web.Script.Serialization;
using ECOsystem.Business.Utilities;

namespace ECO_Control.Business.Control
{
    /// <summary>
    /// Transactions Class
    /// </summary>
    public class TransactionsBusiness : IDisposable
    {

        private static int GlobalCustomerId = 0; // Variable Statica para acceso del Customer desde la trama de Transaction
        private static string STATUS_ERROR = "[SYSTEM_ERROR]";

        #region Retrieve Process

        /// <summary>
        /// Retrieve Transactions 
        /// </summary>
        /// <param name="vehicleId">The Primary Key uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Transactions> RetrieveTransaction(int? vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Transactions>("[Control].[Sp_Transactions_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId
                    });
            }
        }

        /// <summary>
        /// Retrieve Master Credit Card
        /// </summary>
        /// <param name="fleetCard"></param>
        /// <returns></returns>
        public BaseMasterCreditCardInfo RetrieveMasterCreditCard(long fleetCard)
        {
            var fleetCardNumber = TripleDesEncryption.Encrypt(Miscellaneous.ConvertCreditCardToInternalFormat(fleetCard));
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<BaseMasterCreditCardInfo>("[General].[Sp_CustomerCreditCardByFleetCard_Retrieve]",
                    new
                    {
                        FleetCardNumber = fleetCardNumber
                    }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Retrieve Transaction Liter By Credit
        /// </summary>
        /// <param name="customerId">The Primary Key uniquely that identifies each record of customer</param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public decimal RetrieveTransactionLiterByCredit(int? customerId, decimal? amount)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<decimal>("[Control].[Sp_TransactionLitersByCredit_Retrieve]",
                    new
                    {
                        CustomerId = customerId,
                        Amount = amount
                    });
            }
        }

        /// <summary>
        /// Retrieve the Fuel's Price
        /// </summary>
        /// <param name="VehicleId"></param>
        /// <returns></returns>
        private decimal RetrieveFuelPrice(int VehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<decimal>("[General].[Sp_FuelPrice_Retrieve]",
                    new
                    {
                        VehicleId
                    });
            }
        }


        #endregion

        #region Transaction Process
        /// <summary>
        /// Performs the Insert of the entity transaction via API
        /// </summary>
        /// <param name="request">HttpRequestMessage</param>
        /// <param name="message">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>TransactionId created after insert</returns>
        public HttpResponseMessage AddTransactionFromApi<T>(T message, HttpRequestMessage request)
        {
            if (message is RequestPartnerTransactionApi)
            {
                var data = message as RequestPartnerTransactionApi;
                HttpResponseMessage result;
                if (!VerifyPartnerRequest(data, request, out result))
                {
                    return result;
                }

                switch (data.transactionType)
                {
                    case "SALE":
                        return InsertTransactionFromPartner(data, request);
                    case "PREAUTHORIZED_SALE":
                        return InsertTransactionFromPartner(data, request);
                    case "REVERSE":
                        return ReverseTransactionFromPartner(data, request);
                    case "BALANCE_INQUIRY":
                        return GetCreditCardBalance(data, request);
                    case "VOID":
                        return VoidTransactionFromPartner(data, request);
                }

            }

            return request.CreateResponse(HttpStatusCode.BadRequest, new
            {
                ErrorMessage = "Invalid arguments"
            });
        }

        /// <summary>
        /// Performs Verification process for Request
        /// </summary>
        /// <param name="request">HttpRequestMessage</param>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <param name="result">output HttpRequestMessage</param>
        /// <returns>TransactionId created after insert</returns>
        private bool VerifyPartnerRequest(RequestPartnerTransactionApi data, HttpRequestMessage request, out HttpResponseMessage result)
        {
            result = null;
            var erros = new List<string>();
            switch (data.transactionType)
            {
                case "SALE":
                    if (string.IsNullOrEmpty(data.terminalId)) erros.Add("terminalId: Es requerido");
                    if (data.systemTraceNumber == null) erros.Add("systemTraceNumber: Es requerido");
                    if (data.cardNumber == null) erros.Add("cardNumber: Es requerido");
                    //if (data.expirationDate == null) erros.Add("expirationDate: Es requerido");
                    if (data.amount == null) erros.Add("amount: Es requerido");
                    if (data.amount != null && data.amount <= 0) erros.Add("amount: Valor no permitido");
                    if (string.IsNullOrEmpty(data.carTag)) erros.Add("carTag: Es requerido");
                    if (data.kilometers == null) erros.Add("kilometers: Es requerido");
                    if (data.units == null) erros.Add("units: Es requerido");
                    if (data.units != null && data.units <= 0) erros.Add("units: Valor no permitido");
                    break;
                case "REVERSE": //VOID Process 
                    if (string.IsNullOrEmpty(data.terminalId)) erros.Add("terminalId: Es requerido");
                    if (data.systemTraceNumber == null) erros.Add("systemTraceNumber: Es requerido");
                    //if (data.cardNumber == null) erros.Add("cardNumber: Es requerido");
                    break;
                case "BALANCE_INQUIRY":
                    if (string.IsNullOrEmpty(data.terminalId)) erros.Add("terminalId: Es requerido");
                    if (data.cardNumber == null) erros.Add("cardNumber: Es requerido");
                    //if (data.expirationDate == null) erros.Add("expirationDate: Es requerido");
                    break;
                case "VOID":
                    if (string.IsNullOrEmpty(data.terminalId)) erros.Add("terminalId: Es requerido");
                    if (data.systemTraceNumber == null) erros.Add("systemTraceNumber: Es requerido");
                    if (data.authorizationNumber == null) erros.Add("authorizationNumber: Es requerido");
                    if (data.referenceNumber == null) erros.Add("referenceNumber: Es requerido");
                    break;
                default:
                    erros.Add("transactionType: Se espera: SALE|REVERSE|BALANCE_INQUIRY");
                    break;
            }

            var validationResult = !erros.Any();
            if (!validationResult)
            {
                result = request.CreateResponse(HttpStatusCode.BadRequest, new
                {
                    errorMessage = "Parámetros inválidos para procesar su solicitud",
                    errorList = erros.ToArray()
                });
            }
            return validationResult;
        }

        /// <summary>
        /// Original Transaction Check - Verifies previous Transaction information
        /// </summary>
        /// <param name="model"></param>
        private void OriginalTransactionCheck(TransactionsApi model)
        {
            ReverseTransactions tmp;
            using (var dba = new DataBaseAccess())
            {
                #region Validate SystemTrace / TransactionPOS format

                if (!string.IsNullOrEmpty(model.TransactionPOS))
                {
                    //Format: 000087
                    int formatLength = 6;
                    int stLength = model.TransactionPOS.Trim().Length;

                    if (formatLength > stLength)
                    {
                        string currentFormat = "";

                        for (int i = 1; i <= (formatLength - stLength); i++)
                        {
                            currentFormat += "0";
                        }

                        model.TransactionPOS = string.Format("{0}{1}", currentFormat, model.TransactionPOS.Trim());
                    }
                }

                #endregion

                tmp = dba.ExecuteReader<ReverseTransactions>("[Control].[Sp_TransactionsForReverse_Retrieve]",
                    new
                    {
                        model.TransactionPOS,
                        model.ProcessorId
                    }).FirstOrDefault();
            }

            if (tmp == null)
            {
                STATUS_ERROR = "[ORIGIN_TRANSC_ERROR]";
                throw new Exception("No Existe Transacción Origen para procesar Reverse | Void");
            }

            model.FuelAmount = model.FuelAmount ?? tmp.FuelAmount;
            model.PlateId = model.PlateId ?? tmp.PlateId;
            model.Odometer = model.Odometer ?? tmp.Odometer;
            model.Liters = model.Liters ?? tmp.Liters;
            model.CreditCardNumber = tmp.EncryptedCreditCardNumber;
            model.FuelId = model.FuelId ?? tmp.FuelId;

        }

        //private int ExecuteAddTransactions(TransactionsApi model)
        //{
        //    return ExecuteAddTransactions(model, 0).TransactionId;
        //}

        /// <summary>
        /// Execute Add Transactions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //private TransactionsAddApiResponse ExecuteAddTransactions(TransactionsApi model, int CustomerId)
        //{
        //    using (var dba = new DataBaseAccess())
        //    {

        //        #region Validate SystemTrace / TransactionPOS format

        //        if (!string.IsNullOrEmpty(model.TransactionPOS))
        //        {
        //            //Format: 000087
        //            int formatLength = 6;
        //            int stLength = model.TransactionPOS.Trim().Length;

        //            if (formatLength > stLength)
        //            {
        //                string currentFormat = "";

        //                for (int i = 1; i <= (formatLength - stLength); i++)
        //                {
        //                    currentFormat += "0";
        //                }

        //                model.TransactionPOS = string.Format("{0}{1}", currentFormat, model.TransactionPOS.Trim());
        //            }
        //        }

        //        #endregion
        //        //return 0;
        //        return dba.ExecuteReader<TransactionsAddApiResponse>("[Control].[Sp_Transactions_AddFromAPI]",
        //                new
        //                {
        //                    model.PlateId,
        //                    model.FuelId,
        //                    model.CreditCardNumber,
        //                    model.Date,
        //                    model.Odometer,
        //                    model.Liters,
        //                    model.FuelAmount,
        //                    model.Invoice,
        //                    model.TransactionPOS,
        //                    model.SchemePOS,
        //                    model.MerchantDescription,
        //                    model.ProcessorId,
        //                    model.IsFloating,
        //                    model.IsReversed,
        //                    model.IsDuplicated,
        //                    model.IsVoid,
        //                    model.LoggedUserId,
        //                    CustomerId,
        //                    DriverCode = model.DriverCodeEncrypt
        //                }).FirstOrDefault();
        //    }
        //}

        /// <summary>
        /// Execute Transaction Price By Liter Validation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private int ExecuteTransactionPriceByLiterValidation(int pVehicleId, decimal pLiters, decimal pFuelAmount)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Control].[Sp_TransactionPriceByLiterValidation_Retrieve]",
                        new
                        {
                            VehicleId = pVehicleId,
                            Liters = pLiters,
                            FuelAmount = pFuelAmount
                        });
            }
        }

        /// <summary>
        /// Execute Add Transactions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private OdometerAlarm ExecuteTransactionOdometerValidation(int pVehicleId, decimal pOdometer)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<OdometerAlarm>("[General].[Sp_TransactionOdometer_Alarm]",
                    new
                    {
                        VehicleId = pVehicleId,
                        Odometer = pOdometer
                    });
                return result.FirstOrDefault();
            }
        }

        /// <summary>
        /// Wrapper To Transactions Api
        /// </summary>
        /// <param name="data">Request Partner Transaction Api</param>
        /// <returns>Transactions Api Model</returns>
        private TransactionsApi WrapperRequestPartnerToTransactionsApi(RequestPartnerTransactionApi data)
        {
            var model = new TransactionsApi
            {
                PlateId = (data.carTag != null)? data.carTag.ToLower().Trim() : null,
                DriverCode = data.carTag,
                //FuelId = 0,
                TrxDate = DateTime.UtcNow.ToString("yyyyMMdd hh:mm:ss", CultureInfo.InvariantCulture),
                Odometer = (Int64?)data.kilometers,
                Liters = data.units,
                FuelAmount = data.amount,
                TransactionPOS = data.systemTraceNumber,
                SchemePOS = "SchemePOS",
                ProcessorId = data.terminalId,
                Invoice = data.invoice,
                MerchantDescription = data.merchantDescription
            };

            switch (data.transactionType)
            {
                case "SALE":
                    model.IsFloating = false;
                    model.IsReversed = false;
                    model.IsDuplicated = false;
                    model.IsVoid = false;
                    break;
                case "REVERSE": // PROCESS VOID
                    model.IsFloating = false;
                    model.IsReversed = true;
                    model.IsDuplicated = false;
                    model.IsVoid = true;
                    break;
                case "BALANCE_INQUIRY":
                    model.IsReversed = false;
                    model.IsVoid = false;
                    break;
                case "VOID":
                    model.IsFloating = false;
                    model.IsReversed = true;
                    model.IsDuplicated = false;
                    model.IsVoid = true;
                    break;
            }
            model.IsFloating = false;
            model.IsDuplicated = false;

            return model;
        }

        #endregion

        #region SALE | REVERSE PROCESS Validation

        /// <summary>
        /// Performs Get Credit Card Balance via API
        /// </summary>
        /// <param name="request">HttpRequestMessage</param>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>TransactionId created after insert</returns>
        private HttpResponseMessage GetCreditCardBalance(RequestPartnerTransactionApi data, HttpRequestMessage request)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    //Transport data
                    var jsonResponse = new JavaScriptSerializer().Serialize(data);

                    using (var businessCCB = new CreditCardBusiness())
                    {
                        var balance = business.RetrieveCreditCardBalance((long)data.cardNumber, data.expirationDate);

                        //Add to log
                        var result = businessCCB.AddLogTransactionsPOS(
                                      null,
                                      "00",
                                      "BALANCE_INQUIRY | Accepted",
                                      string.Format("Response = cardNumber:{0} | expirationDate: {1} | Amount: {2}", Miscellaneous.GetDisplayCreditCardMask(Miscellaneous.ConvertCreditCardToInternalFormat((long)data.cardNumber)), data.expirationDate, balance),
                                      jsonResponse,
                                      true, false);

                        //Return
                        return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                        {
                            responseCode = "00",
                            additionalMessage = null,
                            cardNumber = data.cardNumber,
                            expirationDate = data.expirationDate,
                            amount = balance
                        });
                    }
                }
            }
            catch (Exception e)
            {
                using (var businessCCB = new CreditCardBusiness())
                {
                    //Transport data
                    var jsonResponseE = new JavaScriptSerializer().Serialize(data);

                    //Add to log
                    var result = businessCCB.AddLogTransactionsPOS(
                              null,
                              "05",
                              "BALANCE_INQUIRY | Denied",
                              e.Message,
                              jsonResponseE,
                              false, true);

                    //Return
                    return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                    {
                        responseCode = "05",
                        additionalMessage = "", //"Error obteniendo información del servidor",
                        cardNumber = data.cardNumber,
                        expirationDate = data.expirationDate
                    });
                }
            }

        }

        /// <summary>
        /// Performs the reverse of the entity transaction via API
        /// </summary>
        /// <param name="request">HttpRequestMessage</param>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>TransactionId created after insert</returns>
        private HttpResponseMessage ReverseTransactionFromPartner(RequestPartnerTransactionApi data, HttpRequestMessage request)
        {
            try
            {
                using (var business = new TransactionsBusiness())
                {
                    //Transport data
                    var jsonResponse = new JavaScriptSerializer().Serialize(data);

                    //Business Log
                    using (var businessCCB = new CreditCardBusiness())
                    {
                        #region Inbound Transaction

                        //Add log to inbound data
                        var resultInbound = businessCCB.AddLogTransactionsPOS(
                                  null,
                                  "IN",
                                  "INBOUND REVERSE | DATA",
                                  "Transaction Reverse Entrante",
                                  jsonResponse,
                                  false, false, 0);

                        #endregion

                        long? cardNumber = null;//data.cardNumber;
                        int customer_id = 0; //businessCCB.RetrieveIDCostumerbyCreditCardNumber(((cardNumber != null) ? (long)cardNumber : 0));

                        var response = business.TryReverseTransactionFromPartner(data, ref cardNumber, ref customer_id);

                        #region Denied

                        if (response.TransactionId == 0)
                        {
                            //Add to log
                            var result = businessCCB.AddLogTransactionsPOS(
                                      null,
                                      "05",
                                      "REVERSE | Denied",
                                      "Error interno al insertar, intentar nuevamente. No se registro la Transacción | TrasanctionId == 0",
                                      jsonResponse,
                                      false, true, customer_id);

                            //Return
                            return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                            {
                                responseCode = "05", //denied
                                additionalMessage = ""//"Se proceso un error interno, intentar nuevamente"

                            });
                        }
                        #endregion

                        #region Accepted

                        var master = business.RetrieveMasterCreditCard((long)cardNumber); //data.cardNumber

                        //Add to log
                        var result2 = businessCCB.AddLogTransactionsPOS(
                                      response.TransactionId,
                                      "00",
                                      "REVERSE | Accepted",
                                      string.Format("Response = cardNumber:{0} | expirationDate: {1}", Miscellaneous.GetDisplayCreditCardMask(Miscellaneous.ConvertCreditCardToInternalFormat(master.DecryptedCreditCardNumber)), master.ExpirationDate),
                                      jsonResponse,
                                      true, false, customer_id);

                        //Return
                        return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                        {
                            responseCode = "00", //Accepted
                            additionalMessage = "",//"Transacción procesada exitosamente",
                            cardNumber = master.DecryptedCreditCardNumber,
                            expirationDate = master.ExpirationDate,
                            advice = response.IsInternal ? "F" : "T"
                        });

                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                using (var businessCCB = new CreditCardBusiness())
                {
                    //Transport data
                    var jsonResponseE = new JavaScriptSerializer().Serialize(data);

                    //Add to log
                    var result = businessCCB.AddLogTransactionsPOS(
                                null,
                                "05",
                                "REVERSE | Denied",
                                e.Message,
                                jsonResponseE,
                                false, true);

                    // Return 
                    return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                    {
                        responseCode = "05", //denied
                        additionalMessage = "" //e.Message,
                    });
                }
            }

        }

        /// <summary>
        /// Performs the VOID of the entity transaction via API
        /// </summary>
        /// <param name="request">HttpRequestMessage</param>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>TransactionId created after insert</returns>
        private HttpResponseMessage VoidTransactionFromPartner(RequestPartnerTransactionApi data, HttpRequestMessage request)
        {
            try
            {
                using (var business = new TransactionsBusiness())
                {
                    //Transport data
                    var jsonResponse = new JavaScriptSerializer().Serialize(data);

                    // Bussiness Log
                    using (var businessCCB = new CreditCardBusiness())
                    {
                        #region Inbound Transaction

                        //Add log to inbound data
                        var resultInbound = businessCCB.AddLogTransactionsPOS(
                                  null,
                                  "IN",
                                  "INBOUND VOID | DATA",
                                  "Transaction Void Entrante",
                                  jsonResponse,
                                  false, false, 0);

                        #endregion

                        long? cardNumber = null;

                        // Retrieve Customer Id
                        int customer_id = businessCCB.RetrieveIDCostumerbyCreditCardNumber(((cardNumber != null) ? (long)cardNumber : 0));

                        var transactionId = business.TryVoidTransactionFromPartner(data, ref cardNumber);


                        #region Denied

                        if (transactionId == 0)
                        {
                            //Add to log
                            var result = businessCCB.AddLogTransactionsPOS(
                                      null,
                                      "05",
                                      "VOID | Denied",
                                      "Error interno al insertar, intentar nuevamente. No se registro la Transacción | TrasanctionId == 0",
                                      jsonResponse,
                                      false, true, customer_id);

                            //Return
                            return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                            {
                                responseCode = "05", //denied
                                additionalMessage = ""//"Se proceso un error interno, intentar nuevamente"

                            });
                        }

                        #endregion

                        #region Accepted

                        var master = business.RetrieveMasterCreditCard((long)cardNumber);

                        //Add to log
                        var result2 = businessCCB.AddLogTransactionsPOS(
                                      transactionId,
                                      "00",
                                      "VOID | Accepted",
                                      string.Format("Response = cardNumber:{0} | expirationDate: {1}", Miscellaneous.GetDisplayCreditCardMask(Miscellaneous.ConvertCreditCardToInternalFormat(master.DecryptedCreditCardNumber)), master.ExpirationDate),
                                      jsonResponse,
                                      true, false, customer_id);

                        //Return
                        return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                        {
                            responseCode = "00", //Accepted
                            additionalMessage = "",//"Transacción procesada exitosamente",
                            cardNumber = master.DecryptedCreditCardNumber,
                            expirationDate = master.ExpirationDate
                        });

                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                using (var businessCCB = new CreditCardBusiness())
                {
                    //Transport data
                    var jsonResponseE = new JavaScriptSerializer().Serialize(data);

                    //Add to log
                    var result = businessCCB.AddLogTransactionsPOS(
                                null,
                                "05",
                                "VOID | Denied",
                                e.Message,
                                jsonResponseE,
                                false, true);

                    //Return
                    return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                    {
                        responseCode = "05", //denied
                        additionalMessage = "" //e.Message,
                    });
                }
            }

        }

        /// <summary>
        /// Performs the Insert of the entity transaction via API
        /// </summary>
        /// <param name="request">HttpRequestMessage</param>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>TransactionId created after insert</returns>
        private HttpResponseMessage InsertTransactionFromPartner(RequestPartnerTransactionApi data, HttpRequestMessage request)
        {
            try
            {
                using (var business = new TransactionsBusiness())
                {
                    //Transport data
                    var jsonResponse = new JavaScriptSerializer().Serialize(data);

                    //Business to log data
                    using (var businessCCB = new CreditCardBusiness())
                    {
                        #region Inbound Transaction

                        //Add log to inbound data
                        var resultInbound = businessCCB.AddLogTransactionsPOS(
                                  null,
                                  "IN",
                                  "INBOUND SALE | DATA",
                                  "Transaction Sale Entrante",
                                  jsonResponse,
                                  false, false, 0);

                        #endregion

                        // Retrieve Customer Id
                        GlobalCustomerId = businessCCB.RetrieveIDCostumerbyCreditCardNumber((long)data.cardNumber);

                        var response = business.TryInsertTransactionFromPartner(data);

                        #region Denied

                        if (response.TransactionId == 0) // No hubo inserción
                        {
                            //Add to log
                            var result = businessCCB.AddLogTransactionsPOS(
                                      null,
                                      "05",
                                      "SALE | Denied",
                                      "Error interno al insertar, intentar nuevamente. No se registro la Transacción | TrasanctionId == 0",
                                      jsonResponse,
                                      false, true, GlobalCustomerId);

                            //Return
                            return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                            {
                                responseCode = "05", //denied
                                additionalMessage = "" //e.Message,
                            });
                        }

                        #endregion

                        #region Accepted

                        var master = business.RetrieveMasterCreditCard((long)data.cardNumber);

                        //Add to log
                        var result2 = businessCCB.AddLogTransactionsPOS(
                                      response.TransactionId,
                                      "00",
                                      "SALE | Accepted",
                                      string.Format("Response = cardNumber:{0} | expirationDate: {1}", Miscellaneous.GetDisplayCreditCardMask(Miscellaneous.ConvertCreditCardToInternalFormat(master.DecryptedCreditCardNumber)), master.ExpirationDate),
                                      jsonResponse,
                                      true, false, GlobalCustomerId);

                        //Return
                        return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                        {
                            responseCode = "00", //Accepted
                            additionalMessage = "",//"Transacción procesada exitosamente",
                            cardNumber = master.DecryptedCreditCardNumber,
                            expirationDate = master.ExpirationDate,
                            advice = response.IsInternal ? "F" : "T"
                        });

                        #endregion
                    }
                }

            }
            catch (Exception e)
            {
                using (var businessCCB = new CreditCardBusiness())
                {
                    //Transport data
                    var jsonResponseE = new JavaScriptSerializer().Serialize(data);

                    //Retrieve customer Id
                    //int customer_id = businessCCB.RetrieveIDCostumerbyCreditCardNumber((long)data.cardNumber);
                    GlobalCustomerId = (!STATUS_ERROR.Contains("[SYSTEM_ERROR]")) ? GlobalCustomerId : 0;

                    //Add to log
                    var result = businessCCB.AddLogTransactionsPOS(
                              null,
                              "05",
                              "SALE | Denied",
                              e.Message,
                              jsonResponseE,
                              false, true, GlobalCustomerId);

                    // Return 
                    return request.CreateResponse(HttpStatusCode.OK, new ResponsePartnerTransactionApi
                    {
                        responseCode = "05", //denied
                        additionalMessage = "" //e.Message,
                    });
                }
            }

        }

        /// <summary>
        /// Performs the Insert of the entity transaction via API
        /// </summary>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>TransactionId created after insert</returns>
        //private TransactionsAddApiResponse TryInsertTransactionFromPartner(RequestPartnerTransactionApi data)
        //{
        //    using (var ccb = new CreditCardBusiness())
        //    {
        //        var listRules = ccb.RetrieveTransactionRules(GlobalCustomerId);
        //        var cardNumber = data.cardNumber != null ? TripleDesEncryption.Encrypt(Miscellaneous.ConvertCreditCardToInternalFormat((long)data.cardNumber)) : "";

        //        if (string.IsNullOrEmpty(cardNumber))
        //            throw new ArgumentException(string.Format("El Número de Tarjeta es Inválido. Tarjeta consultada: Dato Vacío / NULL."));

        //        var model = WrapperRequestPartnerToTransactionsApi(data);
        //        model.CreditCardNumber = cardNumber;

        //        if (listRules != null && listRules.Count() > 0)
        //        {
        //            //Verifies Card business rules
        //            CreditCardCheck(data, model, listRules.ToList());

        //            // Verifies vehicle business rules 
        //            VehicleCheck(model, listRules.ToList());
        //        }

        //        return ExecuteAddTransactions(model, GlobalCustomerId);
        //    }
        //}

        /// <summary>
        /// Performs the Reverse of the entity transaction via API
        /// </summary>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>TransactionId created after insert</returns>
        //private TransactionsAddApiResponse TryReverseTransactionFromPartner(RequestPartnerTransactionApi data, ref long? cardNumber, ref int customerId)
        //{
        //    var model = WrapperRequestPartnerToTransactionsApi(data);
        //    OriginalTransactionCheck(model); // Verifies previous Transaction information

        //    var cardNumberFormat = ECOsystem.Utilities.TripleDesEncryption.Decrypt(model.CreditCardNumber);
        //    cardNumber = Convert.ToInt64(cardNumberFormat.Replace("-", ""));

        //    using (var businessCCB2 = new CreditCardBusiness())
        //    {
        //        customerId = businessCCB2.RetrieveIDCostumerbyCreditCardNumber(((cardNumber != null) ? (long)cardNumber : 0));
        //    }

        //    return ExecuteAddTransactions(model, customerId);
        //}

        /// <summary>
        /// Performs the Reverse of the entity transaction via API
        /// </summary>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>TransactionId created after insert</returns>
        //private int TryVoidTransactionFromPartner(RequestPartnerTransactionApi data, ref long? cardNumber)
        //{
        //    var model = WrapperRequestPartnerToTransactionsApi(data);
        //    OriginalTransactionCheck(model); // Verifies previous Transaction information

        //    var cardNumberFormat = ECOsystem.Utilities.TripleDesEncryption.Decrypt(model.CreditCardNumber);
        //    cardNumber = Convert.ToInt64(cardNumberFormat.Replace("-", ""));

        //    return ExecuteAddTransactions(model);
        //}

        #endregion

        #region Rules Validation

        /// <summary>
        /// Vehicles Check And Get Transactions
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        private void VehicleCheck(TransactionsApi model, List<TransactionRulesModel> rules)
        {
            Vehicles vehicle = null;

            using (var business = new VehiclesBusiness())
            {
                vehicle = business.RetrieveVehicleByPlate(model.PlateId, GlobalCustomerId).FirstOrDefault();
                if (vehicle != null)
                {
                    //throw new Exception("Transaction Failed. There is not any registration for Vehicle Plate: '" + model.PlateId + "'");
                    //throw new Exception(string.Format("Vehículo con la matricula {0} no registrada en el sistema.", model.PlateId));

                    foreach (var rule in rules)
                    {
                        //if (rule.RuleName == "VehiclePlateValidate" && rule.RuleActive == true)
                        //{
                        //    vehicle = business.RetrieveVehicleByPlate(model.PlateId).FirstOrDefault();
                        //    if (vehicle == null)
                        //    {
                        //        //throw new Exception("Transaction Failed. There is not any registration for Vehicle Plate: '" + model.PlateId + "'");
                        //        throw new Exception(string.Format("Vehículo con la matricula {0} no registrada en el sistema.", model.PlateId));
                        //    }
                        //}

                        if (rule.RuleName == "LitersCapacityCreditCard" && rule.RuleActive == true)
                        {
                            //Convert units to Liters
                            var units = model.Liters;
                            int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
                            if (vehicle.PartnerCapacityUnitId != litersCapacityUnitId)
                            {
                                var liters = Miscellaneous.CapacityUnitConversion(vehicle.PartnerCapacityUnitId, litersCapacityUnitId, units);
                                model.Liters = liters;
                            }

                            if (model.Liters > vehicle.Liters)
                            {
                                STATUS_ERROR = "[LITERS_VALIDATE]";
                                //throw new Exception("Transaction Failed. Vehicle for plate '" + vehicle.PlateId + "' can not have transactions for the total of '" + vehicle.Liters + "' liters.");
                                throw new Exception(string.Format("Litros en la transacción supera la capacidad de litros del vehículo. Disponible: {0} | Litros Solicitados: {1}", vehicle.Liters, model.Liters));
                            }
                        }

                        if (rule.RuleName == "TimeSlotCreditCard" && rule.RuleActive == true)
                        {
                            using (var businessSlot = new TimeSlotByVehicleBusiness())
                            {
                                if (businessSlot.RetrieveValidateTimeSlotByVehicle(vehicle.VehicleId) == null)
                                {
                                    var TimeSlotRange = businessSlot.RetrieveRangeValidateTimeSlotByVehicle(vehicle.VehicleId);
                                    var DayTimeSlot = TimeSlotRange.Select(d => new { d.Day }).FirstOrDefault();

                                    string MinTime = null;
                                    string MaxTime = null;

                                    foreach (var t in TimeSlotRange)
                                    {
                                        if (MinTime == null)
                                            MinTime = t.Time;
                                        else
                                            MaxTime = t.Time;
                                    }

                                    if (DayTimeSlot.Day == (int)DateTime.Now.DayOfWeek)
                                    {  
                                        STATUS_ERROR = "[TIMESLOT_VALIDATE]";
                                        throw new Exception("Horario de Uso no permitido. Rango válido: " + DateTime.Now.ToString("dddd", CultureInfo.CreateSpecificCulture("ES")) + " - " + MinTime + " a " + MaxTime + ". Hora de la Transacción: " + DateTimeOffset.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                                    }                                    
                                    else
                                    {
                                        STATUS_ERROR = "[TIMESLOT_VALIDATE]";
                                        throw new Exception("Transacción Rechazada. Rango válido: No existe rango.");
                                    }
                                }
                            }
                        }

                        if (rule.RuleName == "PriceByLiterCreditCard" && rule.RuleActive == true)
                        {
                            //Valida Price By Liters con el monto asignado al día de hoy al combustible (Super, Regular, Diesel)
                            // TODO: Hacer un rango de validación con un porcentaje de error
                            if (ValidateTransactionPriceByLiter(Convert.ToInt16(vehicle.VehicleId), Convert.ToDecimal(model.Liters), Convert.ToDecimal(model.FuelAmount)) == false)
                            {
                                STATUS_ERROR = "[PRICE_LITERS_VALIDATE]";
                                throw new Exception("El monto de la transacción no coincide con la cantidad de combustible." +
                                                    " Monto: " + Convert.ToDecimal(model.FuelAmount).ToString("#,##0.00") + ", Cantidad de combustible: " +
                                                    Convert.ToDecimal(model.Liters).ToString("#,##0.00") + ", Precio del Litro: " + 
                                                    RetrieveFuelPrice(Convert.ToInt32(vehicle.VehicleId)).ToString("#,##0.00") + " Lt.");
                            }
                        }

                        if (rule.RuleName == "OdometerCreditCard" && rule.RuleActive == true)
                        {
                            OdometerAlarm result = ExecuteTransactionOdometerValidation(Convert.ToInt16(vehicle.VehicleId), Convert.ToDecimal(model.Odometer));
                            if (result.SetAlarm >= 1)
                            {
                                STATUS_ERROR = "[ODOMETER_VALIDATE]";
                                throw new Exception("Odómetro fuera de rango. Valor ingresado " + Convert.ToString(result.Odometer) +
                                                    ". Rango permitido de " + Convert.ToString(result.LastOdometer) + " kms a " + Convert.ToString(result.MaxPosibleOdometer) + " kms");
                            }
                        }

                        if (rule.RuleName == "DailyTransactionLimit" && rule.RuleActive == true)
                        {
                            STATUS_ERROR = "[DAILY_TRANSC_VALIDATE]";
                            IList<Transactions> transactions = RetrieveTransaction(vehicle.VehicleId).ToList();
                            var date = DateTime.UtcNow;
                            if (transactions.Count(x => x.Date.ToShortDateString().Equals(date.ToShortDateString())) > vehicle.DailyTransactionLimit)
                            {
                                //throw new Exception("Transaction Failed. User '" + vehicle.UserName + "'  has exceed the limit of daily transactions.");
                                throw new Exception(string.Format("Se excedió el limite diario de transacciones para el vehículo. El límite de transacciones es: {0}", vehicle.DailyTransactionLimit));
                            }
                        }

                        if (rule.RuleName == "LimitTankCapacity" && rule.RuleActive == true)
                        {
                            STATUS_ERROR = "[LIMIT_TANK_CAPACITY_VALIDATE]";

                            string litersVal = ValidateTankCapacity(model, vehicle);

                            if (litersVal != "")
                            {
                                throw new Exception("Transacción rechazada por litros disponibles en tanque " + litersVal.Split('|')[0] + ", cantidad ingresada " +
                                                     litersVal.Split('|')[1] + ", capacidad total del tanque " + litersVal.Split('|')[2] + ".");
                            }                            
                        }

                        if(rule.RuleName == "TimeSlotTransactions" && rule.RuleActive == true)
                        {
                            STATUS_ERROR = "[TIME_SLOT_TRANSACTIONS_VALIDATE]";

                            string dateVal = ValidateTimeSlotTransactions(model, vehicle);

                            if (dateVal != "")
                            {
                                throw new Exception("La transacción no se puede autorizar; la transacción se puede realizar a partir de las: " + dateVal);
                            }                            
                        }
                    }

                    model.FuelId = vehicle.DefaultFuelId;
                }
                else { //Vehículo no asignado a la tarjeta, o Emisión de tarjeta no válida
                    STATUS_ERROR = "[INVALID_VEHICLE]";
                    throw new Exception(string.Format("La transacción ha sido denegada, debido a que la tarjeta no está vinculada a la Placa: {0}, por favor validar el tipo de emisión de la tarjeta.", model.PlateId));
                }
            }

            //ApplyVehicleRules(transactions, vehicle, model);
        }

        /// <summary>
        /// Credit Card Check
        /// </summary>
        /// <param name="data"></param>
        /// <param name="cardNumber"></param>
        private void CreditCardCheck(RequestPartnerTransactionApi data, TransactionsApi model, List<TransactionRulesModel> rules)
        {
            var creditCard = ValidateCard(model.CreditCardNumber, Convert.ToInt16(data.expirationDate), rules);
            ValidateCardBalance(creditCard, data.amount, rules);
            ValidateCardExpirationDate(creditCard, data.expirationDate, rules);
            ValidateCardByPlateOrDriverCode(creditCard, ref model, rules);
        }

        /// <summary>
        /// Validate Card
        /// </summary>
        private CreditCard ValidateCard(string cardNumber, int ExpirationDate, List<TransactionRulesModel> rules)
        {
            try
            {
                int ExpirationYear = Convert.ToInt16(ExpirationDate.ToString().Substring(0, 2));
                int ExpirationMonth = Convert.ToInt16(ExpirationDate.ToString().Substring(2, 2));

                //pendiente danilo
                CreditCard creditCard = null;

                using (var business = new CreditCardBusiness())
                {
                    creditCard = business.RetrieveCreditCardByNumber(cardNumber).FirstOrDefault();

                    //credit card exists?
                    if (creditCard == null)
                    {
                        STATUS_ERROR = "[CARD_VALIDATE]";
                        throw new Exception(string.Format("Tarjeta no existe en el sistema. Card Number: {0}.", TripleDesEncryption.Decrypt(cardNumber)));
                    }
                }

                foreach (var rule in rules)
                {
                    //if (rule.RuleName == "InvalidCreditCard" && rule.RuleActive == true)
                    //{

                    //}

                    if (rule.RuleName == "StatusCreditCard" && rule.RuleActive == true)
                    {
                        //Status Validation, the credit card status must be Active (7)
                        if (creditCard.StatusId != 7)
                        {
                            STATUS_ERROR = "[STATUS_VALIDATE]";
                            throw new Exception("La tarjeta de crédito está: " + creditCard.StatusName);
                        }
                    }
                }

                return creditCard;
            }
            catch (Exception ex)
            {
                if (!STATUS_ERROR.Contains("STATUS_VALIDATE") && !STATUS_ERROR.Contains("CARD_VALIDATE"))
                {
                    STATUS_ERROR = "[FORMAT_VALIDATE]";
                    throw new Exception("Error en fecha expiración, formato válido es: YYMM");
                }

                throw ex;
            }
        }

        /// <summary>
        /// ValidateCardExpirationDate
        /// </summary>
        private void ValidateCardExpirationDate(CreditCard creditCard, int? expirationDate, List<TransactionRulesModel> rules)
        {
            try
            {
                var rule = rules.Where(w => w.RuleName == "ExpirationCreditCard").FirstOrDefault();
                if (rule != null && rule.RuleActive == true)
                {
                    var month = 0;
                    var year = 0;

                    var transactionDate = DateTime.UtcNow;

                    //Expiration Date Validation
                    if (expirationDate != null)
                    {
                        var expirationDateStr = expirationDate.ToString();
                        switch (expirationDateStr.Length)
                        {
                            case 4:
                                year = Convert.ToInt16(expirationDateStr.Substring(0, 2));
                                month = Convert.ToInt16(expirationDateStr.Substring(2, 2));
                                break;
                            case 3:
                                year = Convert.ToInt16(expirationDateStr.Substring(0, 1));
                                month = Convert.ToInt16(expirationDateStr.Substring(1, 2));
                                break;
                        }
                        year += 2000;

                        if (creditCard.ExpirationMonth != month || creditCard.ExpirationYear != year)
                        {
                            STATUS_ERROR = "[INVALID_EXP]";
                            throw new Exception("La tarjeta de crédito procesada tiene una fecha de expiración inválida. Fecha de expiración válida: " + Convert.ToInt16(creditCard.ExpirationYear).ToString("0000") + "/" + Convert.ToInt16(creditCard.ExpirationMonth).ToString("00") + " | entrante:" + year.ToString("0000") + "/" + month.ToString("00"));
                        }

                    }

                    //Get the expiration date from credit card data
                    year = Convert.ToInt16(creditCard.ExpirationYear);
                    month = Convert.ToInt16(creditCard.ExpirationMonth);

                    if (year < transactionDate.Year || (year == transactionDate.Year && month < transactionDate.Month))
                    {
                        STATUS_ERROR = "[DATE_EXP]";
                        throw new Exception("La tarjeta de crédito ya expiró, fecha de expiración es " + month.ToString("00") + "/" + year.ToString("0000"));
                    }
                }
            }
            catch (Exception ex)
            {
                if (!STATUS_ERROR.Contains("INVALID_EXP") && !STATUS_ERROR.Contains("DATE_EXP"))
                {
                    throw new Exception("Error verificando fecha Expiración de Tarjeta. Formato: YYMM");
                }
                else
                {
                    throw ex;
                }
            }

        }

        /// <summary>
        /// Validate Card Balance
        /// </summary>
        private void ValidateCardBalance(CreditCard creditCard, decimal? trxAmount, List<TransactionRulesModel> rules)
        {
            var rule = rules.Where(w => w.RuleName == "BalanceCreditCard").FirstOrDefault();
            if (rule != null && rule.RuleActive == true)
            {
                //Balance Validation
                if (creditCard.CreditAvailable < trxAmount)
                {
                    STATUS_ERROR = "[CARD_VALIDATE]";
                    throw new Exception(string.Format("El crédito disponible de la tarjeta es insuficiente. Disponible: {0} | Monto: {1}", creditCard.CreditAvailable, trxAmount));
                }
            }
        }

        /// <summary>
        /// Validate Card Plate Or Driver
        /// </summary>
        private void ValidateCardByPlateOrDriverCode(CreditCard creditCard, ref TransactionsApi model, List<TransactionRulesModel> rules)
        {
            var rule = rules.Where(w => w.RuleName == "PlateCreditCard").FirstOrDefault();
            var rule2 = rules.Where(w => w.RuleName == "DriverCodeCreditCard").FirstOrDefault();

            //bool isValidPlate = true;
            //bool isValidCode = true;
            string message = "";

            //Validate Driver Code
            if (rule2 != null && rule2.RuleActive == true)
            {
                using (var business = new VehiclesByDriverBusiness())
                {
                    var result = business.RetrieveVehicleByDriverCode(model.DriverCodeEncrypt, GlobalCustomerId, creditCard.TransactionPlate);
                    if (result == null)
                    {
                        STATUS_ERROR = "[CODE_VALIDATE]";
                        message += string.Format("La tarjeta no esta vinculada al Código de Conductor: {0}.", model.PlateId);
                        throw new Exception(message);
                    }
                    else
                    {
                        #region Validamos estado del Conductor
                            if (result.IsActive == false)
                            {
                                STATUS_ERROR = "[STATUS_USER]";
                                message += string.Format("La transacción ha sido denegada, el conductor con el código {0} está Inactivo en el sistema.", model.DriverCode);
                                throw new Exception(message);
                            }
                            if (result.IsDeleted == true)
                            {
                                STATUS_ERROR = "[STATUS_USER]";
                                message += string.Format("La transacción ha sido denegada, el conductor con el código {0} está Borrado del sistema.", model.DriverCode);
                                throw new Exception(message);
                            }
                        #endregion

                        //GSOLANO: Si el código de conductor es válido seteamos la PlacaId del modelo por si la Regla por Placa está habilitada.
                        model.PlateId = creditCard.TransactionPlate;
                    }
                }
            }


            //Validate Plate
            if (rule != null && rule.RuleActive == true)
            {
                if (creditCard.TransactionPlate != null && (creditCard.TransactionPlate.ToLower().Trim() != model.PlateId))
                {
                    STATUS_ERROR = "[PLATE_VALIDATE]";
                    message += string.Format("La tarjeta no esta vinculada al Vehículo Placa: {0}, actualmente esta vinculada a la Placa: {1}", model.PlateId, creditCard.VehiclePlate);
                    throw new Exception(message);
                }
            }

            //Si las reglas están apagadas asignamos por defecto la placa asignada a la tarjeta
            if ((rule == null || rule != null && rule.RuleActive == false) && (rule2 == null || rule2 != null && rule2.RuleActive == false))
            {
                model.PlateId = creditCard.TransactionPlate;
            }
        }

        /// <summary>
        /// Validate Card Balance
        /// </summary>
        private bool ValidateTransactionPriceByLiter(int pVehicleId, decimal pLiters, decimal pFuelAmount)
        {

            if (ExecuteTransactionPriceByLiterValidation(pVehicleId, pLiters, pFuelAmount) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Apply Vehicle Rules
        /// </summary>
        /// <param name="transactions">Transactions list</param>
        /// <param name="vehicle">vehicle</param>
        /// <param name="model">API model</param>
        private void ApplyVehicleRules(IList<Transactions> transactions, Vehicles vehicle, TransactionsApi model)
        {

            if (transactions.Count() >= 2)
            {
                GetAndSetPerformanceOfVehicle(transactions, vehicle, model);
            }
            else
            {
                OnlySetPerformanceOfVehicle(transactions, vehicle, model);
            }
        }

        /// <summary>
        /// Get And Set Performance Of Vehicle
        /// </summary>
        /// <param name="transactions">Transactions list</param>
        /// <param name="vehicle">vehicle</param>
        /// <param name="model">API model</param>
        private void GetAndSetPerformanceOfVehicle(IList<Transactions> transactions, Vehicles vehicle, TransactionsApi model)
        {
            var date = DateTime.UtcNow;

            decimal performance = 0;
            decimal totalOdometer = 0;
            decimal totalLiters = 0;

            for (var cont = 0; cont < transactions.Count() - 1; cont++)
            {
                var currentTransaction = transactions.ElementAt(cont);
                var nextTransaction = transactions.ElementAt(cont + 1);
                performance += ((currentTransaction.Odometer - nextTransaction.Odometer) / currentTransaction.Liters);
                totalLiters += currentTransaction.Liters;
                totalOdometer += (currentTransaction.Odometer - nextTransaction.Odometer);

            }
            performance /= transactions.Count() - 1;
            var odometerExpected = transactions.ElementAt(0).Odometer + (model.Liters * performance);
            using (var businessPerformance = new PerformanceByVehicleBusiness())
            {
                businessPerformance.AddPerformanceByVehicle(vehicle.VehicleId, date, performance, totalOdometer, totalLiters, model.LoggedUserId);
            }
            using (var businessParameters = new ParametersBusiness())
            {
                var parameters = businessParameters.RetrieveParameters().FirstOrDefault();
                using (var businessAlarmByOdometer = new AlarmByOdometerBusiness())
                {
                    string email = "";
                    using (var business = new UsersBusiness())
                    {
                        foreach (Users user in business.RetrieveUsers(null, null, null, vehicle.CustomerId).ToList())
                        {
                            if (("CUSTOMER_ADMIN").Equals(user.RoleName))
                            {
                                email = user.DecryptedEmail;
                            }
                        }
                    }

                    if (vehicle.AVL == null)
                    {
                        if (parameters != null && ((model.Odometer - odometerExpected) > parameters.POSOdometerErrorMargin)) return;
                        businessAlarmByOdometer.AddOrEditAlarmByOdometers(vehicle.VehicleId, date, odometerExpected, model.Odometer, false, model.LoggedUserId);
                        //throw new Exception("The entered value for odometer exceeds the threshold configured for this equipment.");
                        //Send email to confirm the email account

                        new EmailSender().SendEmail(email, "Notificación inTrack-ECOsystem - Odómetro Menor Al Esperado", "El odómetro real es menor al esperado para la transacción " + transactions[transactions.Count - 1].TransactionId + " y el vehículo " + vehicle.VehicleId);

                        throw new Exception("Odómetro excede umbral configurado para el vehículo");
                    }
                    if (parameters != null && ((model.Odometer - odometerExpected) > parameters.AVLOdometerErrorMargin)) return;
                    businessAlarmByOdometer.AddOrEditAlarmByOdometers(vehicle.VehicleId, date, odometerExpected, model.Odometer, false, model.LoggedUserId);
                    //throw new Exception("The entered value for odometer exceeds the threshold configured for this equipment.");

                    new EmailSender().SendEmail(email, "Notificación inTrack-ECOsystem - Odómetro Menor Al Esperado", "El odómetro real es menor al esperado para la transacción " + transactions[transactions.Count - 1].TransactionId + " y el vehículo " + vehicle.VehicleId);
                    throw new Exception("Odómetro excede umbral configurado para el vehículo");
                }
            }
        }

        /// <summary>
        /// Only Set Performance Of Vehicle
        /// </summary>
        /// <param name="transactions">Transactions list</param>
        /// <param name="vehicle">vehicle</param>
        /// <param name="model">API model</param>
        private void OnlySetPerformanceOfVehicle(IEnumerable<Transactions> transactions, Vehicles vehicle, TransactionsApi model)
        {
            var date = DateTime.UtcNow;

            using (var businessByVehicle = new PerformanceByVehicleBusiness())
            {
                var performance = businessByVehicle.LastPerformanceByVehicle(vehicle.VehicleId);
                if (performance == null) return;
                var odometerExpected = transactions.ElementAt(0).Odometer + (model.Liters * Convert.ToDecimal(performance));
                using (var businessParameters = new ParametersBusiness())
                {
                    var parameters = businessParameters.RetrieveParameters().FirstOrDefault();
                    using (var businessAlarmByOdometer = new AlarmByOdometerBusiness())
                    {
                        if (vehicle.AVL == null)
                        {
                            if (parameters != null && !((model.Odometer - odometerExpected) > parameters.POSOdometerErrorMargin)) return;
                            businessAlarmByOdometer.AddOrEditAlarmByOdometers(vehicle.VehicleId, date, odometerExpected, model.Odometer, false, model.LoggedUserId);
                            //throw new Exception("The entered value for odometer exceeds the threshold configured for this equipment.");
                            throw new Exception("Odómetro excede umbral configurado para el vehículo");
                        }
                        if (parameters != null && !((model.Odometer - odometerExpected) > parameters.AVLOdometerErrorMargin)) return;
                        businessAlarmByOdometer.AddOrEditAlarmByOdometers(vehicle.VehicleId, date, odometerExpected, model.Odometer, false, model.LoggedUserId);
                        //throw new Exception("The entered value for odometer exceeds the threshold configured for this equipment.");
                        throw new Exception("Odómetro excede umbral configurado para el vehículo");
                    }
                }
            }
        }
        
        /// <summary>
        /// Validate if the tank capacity can be proccess
        /// </summary>
        /// <param name="Odometer"></param>
        /// <param name="DefaultPerformance"></param>
        /// <param name="Liters"></param>
        /// <param name="TransactionLiters"></param>
        /// <returns></returns>
        private string ValidateTankCapacity(TransactionsApi model, Vehicles vehicle)
        {
            double LitersAvailable;

            double margin = 0;
            
            using (var business = new CreditCardBusiness()) 
            { 
                var parametersList = business.TRParametersRetrieve(vehicle.CustomerId);
                int OdometerLast = business.GetOdometerLastByVehicle(Convert.ToInt32(vehicle.VehicleId), model.Date);

                LitersAvailable = ((Convert.ToDouble(model.Odometer) - OdometerLast) * Convert.ToDouble(vehicle.DefaultPerformance)) - Convert.ToDouble(vehicle.Liters);

                //If the value is negative, convert the value to positive
                if (LitersAvailable < 0) LitersAvailable = Math.Abs(LitersAvailable);

                //Get the real number of liters available for the sale
                LitersAvailable = Convert.ToDouble(vehicle.Liters) - LitersAvailable;

                //Gets the margin value from the database
                foreach (var item in parametersList)
                {
                    if (item.ParameterName == "LimitTankCapacity")
                        margin = Convert.ToDouble(item.Value.Replace(',','.'));
                }

                //Times the LiterAvailable by the margin getting the result we need for the validation
                margin = LitersAvailable * margin / 100;

                //Validates if the liters in the transaction are within the margin
                if (!((LitersAvailable + margin) > Convert.ToDouble(model.Liters)))
                {
                    return LitersAvailable.ToString("#.00") + "|" + Convert.ToDouble(model.Liters).ToString("#.00") + "|" + Convert.ToDouble(vehicle.Liters).ToString("#.00");
                }                
            }

            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        private string ValidateTimeSlotTransactions(TransactionsApi model, Vehicles vehicle)
        {
            var transaction = RetrieveTransaction(vehicle.VehicleId).OrderByDescending(x => x.Date).FirstOrDefault();

            string TimeSlot = string.Empty;

            using (var business = new CreditCardBusiness())
            {
                var parametersList = business.TRParametersRetrieve(vehicle.CustomerId);
                
                //Gets the margin value from the database
                foreach (var item in parametersList)
                {
                    if (item.ParameterName == "TimeSlotTransactions")
                        TimeSlot = item.Value;
                }                
            }

            int seconds, minutes, hours;

            seconds = Convert.ToInt16(TimeSlot.Split(':')[2]);
            minutes = Convert.ToInt16(TimeSlot.Split(':')[1]);
            hours = Convert.ToInt16(TimeSlot.Split(':')[0]);

            if (transaction.Date.AddHours(hours).AddMinutes(minutes).AddSeconds(seconds) > model.Date)
            {
                return transaction.Date.AddHours(hours).AddMinutes(minutes).AddSeconds(seconds).ToString("hh:mm:ss tt");
            }
                
            return "";
        }

        #endregion

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
