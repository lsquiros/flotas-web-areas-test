﻿using  ECOsystem.Areas.Control.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Business
{
    public class ExpireMaintenanceBusiness : IDisposable
    {

        /// <summary>
        /// Get the expire Maintenance by Customer
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ExpiredPreventiveMaintenance> ExpireMaintenanceRetrieve(int customerId, int userId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ExpiredPreventiveMaintenance>("[General].[Sp_ExpiredMaintenance_Retrieve]",
                new
                {
                    CustomerId = customerId,
                    UserId = userId
                });
            }
        }

        public DataTable GetReportData(List<ExpiredPreventiveMaintenance> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("Plate");
                dt.Columns.Add("MaintenanceName");
                dt.Columns.Add("ExpiredDate");
                dt.Columns.Add("OdometerStr");
                dt.Columns.Add("LastReviewOdometerStr");
                dt.Columns.Add("ExpiredOdometerStr");
                dt.Columns.Add("ExpiredDays");
                dt.Columns.Add("CustomerName");               
                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();
                    dr["VehicleName"] = item.VehicleName;
                    dr["Plate"] = item.Plate;
                    dr["MaintenanceName"] = item.MaintenanceName;
                    dr["ExpiredDate"] = item.ExpiredDateStr;
                    dr["OdometerStr"] = item.Odometer;
                    dr["LastReviewOdometerStr"] = item.LastReviewOdometer;
                    dr["ExpiredOdometerStr"] = item.ExpiredOdometer;
                    dr["ExpiredDays"] = item.ExpiredDays;
                    dr["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }


        /// <summary>
        /// GC
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}