﻿using  ECOsystem.Areas.Control.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Business
{
    public class FuelDistributionByCostCenterBusiness : IDisposable
    {
        public List<FuelDistributionByCostCenter> FuelDistributionByCostCenterRetrieve(int? CustomerId, int? CostCenterId = null)
        {
            using (var db = new DataBaseAccess())
            {
                return db.ExecuteReader<FuelDistributionByCostCenter>("[Control].[Sp_CustomerCreditsByCostCenter_Retrieve]", new
                {
                    CustomerId,
                    CostCenterId
                }).ToList();
            }
        }

        public void FuelDistributionByCostCenterAddOrEdit(int? CustomerId, string XmlData, int? UserId)
        {
            using (var db = new DataBaseAccess())
            {
                db.ExecuteNonQuery("[Control].[Sp_CustomerCreditsByCostCenter_AddOrEdit]", new
                {                    
                    CustomerId,
                    XmlData,
                    UserId
                });
            }
        }

        public List<FuelDistributionByCostCenter> FuelDistributionByCostCenterImportAddOrEdit(int? CustomerId, string XmlData, int? UserId)
        {
            using (var db = new DataBaseAccess())
            {
                return db.ExecuteReader<FuelDistributionByCostCenter>("[Control].[Sp_CustomerCreditsByCostCenterImport_AddOrEdit]", new
                {
                    CustomerId,
                    XmlData,
                    UserId
                }).ToList();
            }
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}