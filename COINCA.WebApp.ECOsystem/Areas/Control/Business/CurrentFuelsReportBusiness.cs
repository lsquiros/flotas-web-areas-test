﻿/************************************************************************************************************
*  File    : CurrentFuelsReportBusiness.cs
*  Summary : CurrentFuelsReport Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Data;
using System.Globalization;
using System.Collections.Generic;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// CurrentFuelsReport Class
    /// </summary>
    public class CurrentFuelsReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Current Fuels Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of CurrentFuelsReportBase in order to load the chart</returns>
        public CurrentFuelsReportBase RetrieveCurrentFuelsReport(ControlFuelsReportsBase parameters)
        {
            var result = new CurrentFuelsReportBase();

            GetReportData(parameters, result);

            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(ECOsystem.Utilities.Miscellaneous.GenerateColors(result.List.Count())));
            result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(ECOsystem.Utilities.Miscellaneous.GenerateHighlighColors().Take(result.List.Count())));
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            ECOsystem.Business.Core.CustomerThresholdsBusiness objsholdsBusi = new ECOsystem.Business.Core.CustomerThresholdsBusiness();

            var ThesholdsBusi = objsholdsBusi.RetrieveThresholds().FirstOrDefault();
            result.Parameters.RedHigher = ThesholdsBusi.RedHigher;
            result.Parameters.RedLower = ThesholdsBusi.RedLower;
            result.Parameters.YellowLower = ThesholdsBusi.YellowLower;
            result.Parameters.YellowHigher = ThesholdsBusi.YellowHigher;
            result.Parameters.GreenLower = ThesholdsBusi.GreenLower;
            result.Parameters.GreenHigher = ThesholdsBusi.GreenHigher;


            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(ControlFuelsReportsBase parameters, CurrentFuelsReportBase result)
        {
            if (parameters.ReportFuelTypeId == null) return;

            switch ((ReportFuelTypes)parameters.ReportFuelTypeId)
            {
                case ReportFuelTypes.Vehicles:
                    GetReportDataByVehicle(parameters, result);
                    break;
                case ReportFuelTypes.VehicleGroups:
                    GetReportDataByVehicleGroup(parameters, result);
                    break;
                case ReportFuelTypes.VehicleCostCenters:
                    GetReportDataByVehicleCostCenters(parameters, result);
                    break;
                case ReportFuelTypes.VehicleUnits:
                    GetReportDataByVehicleUnits(parameters, result);
                    break;
            }
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicleCostCenters(ControlFuelsReportsBase parameters, CurrentFuelsReportBase result)
        {

            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<CurrentFuelsByVehicleSubUnitReport>("[Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.CostCenterName).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.RealAmountPct, 0)).ToList()));

            string sp = "[Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]";
            if (ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 100)
            {
                sp = "[Control].[Sp_CurrentFuelsReportByDriver_Retrieve]";
            }

            var model = new CurrentFuelsReportBase();
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                model.List = dba.ExecuteReader<CurrentFuelsByVehicle>(sp,
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            foreach (CurrentFuelsByVehicleSubUnitReport item in result.List)
            {
                item.VehiclesDetail = model.List.Where(x => item.CostCenterId.Equals(x.CostCenterId)).ToList();
            }
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicleUnits(ControlFuelsReportsBase parameters, CurrentFuelsReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<CurrentFuelsByVehicleUnitsReport>("[Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.VehicleUnitName).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.RealAmountPct, 0)).ToList()));

            string sp = "[Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]";
            if (ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 100)
            {
                sp = "[Control].[Sp_CurrentFuelsReportByDriver_Retrieve]";
            }

            var model = new CurrentFuelsReportBase();
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                model.List = dba.ExecuteReader<CurrentFuelsByVehicle>(sp,
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            foreach (CurrentFuelsByVehicleUnitsReport item in result.List)
            {
                item.VehiclesDetail = model.List.Where(x => item.UnitId.Equals(x.UnitId)).ToList();
            }
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicleGroup(ControlFuelsReportsBase parameters, CurrentFuelsReportBase result)
        {

            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<CurrentFuelsByVehicleGroupReport>("[Control].[Sp_CurrentFuelsReportByVehicleGroup_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.VehicleGroupName).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.RealAmountPct, 0)).ToList()));

            string sp = "[Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]";
            if (ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 100)
            {
                sp = "[Control].[Sp_CurrentFuelsReportByDriver_Retrieve]";
            }

            var model = new CurrentFuelsReportBase();
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                model.List = dba.ExecuteReader<CurrentFuelsByVehicle>(sp,
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            foreach (CurrentFuelsByVehicleGroupReport item in result.List)
            {
                item.VehiclesDetail = model.List.Where(x => item.VehicleGroupId.Equals(x.VehicleGroupId)).ToList();
            }
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataByVehicle(ControlFuelsReportsBase parameters, CurrentFuelsReportBase result)
        {
            string sp = "[Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]";
            if (ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 100)
            {
                sp = "[Control].[Sp_CurrentFuelsReportByDriver_Retrieve]";
            }

            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                result.List = dba.ExecuteReader<CurrentFuelsByVehicle>(sp,
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        user.UserId
                    });
            }

            if (ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 100)
            {
                result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.DecryptedName).ToList()));
            }
            else
            {
                result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.PlateNumber).ToList()));
            }

           
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.RealAmountPct, 0)).ToList()));
        }

        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            try
            {
                var result = new CurrentFuelsReportBase();
                GetReportData(parameters, result);

                var startDate = new DateTime();
                var endDate = new DateTime();


                if (parameters.Month != null && parameters.Year != null)
                {
                    startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                    endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
                }

                else
                if (parameters.StartDate != null && parameters.EndDate != null)
                {
                    startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                    endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
                }

                return CurrentFuelReportGenerate(result.List, startDate, endDate, parameters.ReportFuelTypeId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public DataSet GetReportDownloadDataSet(ControlFuelsReportsBase parameters)
        {
            try
            {
                var result = new CurrentFuelsReportBase();
                GetReportData(parameters, result);

                var startDate = new DateTime();
                var endDate = new DateTime();


                if (parameters.Month != null && parameters.Year != null)
                {
                    startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                    endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
                }

                else
                if (parameters.StartDate != null && parameters.EndDate != null)
                {
                    startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                    endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
                }

                return CurrentFuelReportGenerateDataSet(result.List, startDate, endDate, parameters.ReportFuelTypeId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public DataSet CurrentFuelReportGenerateDataSet(IEnumerable<dynamic> list, DateTime startDate, DateTime endDate, int? ReportType)
        {
            DataSet ds = new DataSet();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();

            var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
            var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
            //Get array letters
            char[] letters = startDateStr.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            startDateStr = new string(letters);
            letters = endDateStr.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            endDateStr = new string(letters);


            switch ((ReportFuelTypes)ReportType)
            {
                case ReportFuelTypes.VehicleCostCenters:
                    #region ReportFuelTypes.VehicleCostCenters
                    dt1 = new DataTable("CurrentFuelsReport_VehicleCostCenters");
                    dt2 = new DataTable("CurrentFuelsDetail_VehicleCostCenters");

                    dt1.Columns.Add("CostCenterName");
                    dt1.Columns.Add("CostCenterId");
                    dt1.Columns.Add("AssignedAmount", typeof(decimal));
                    dt1.Columns.Add("RealAmount", typeof(decimal));
                    dt1.Columns.Add("RealAmountPct", typeof(decimal));
                    dt1.Columns.Add("CustomerName");
                    dt1.Columns.Add("Dates");


                    ConfigureDataTable(dt2);
                    dt2.Columns.Add("CostCenterId");
                    dt2.Columns.Add("VehicleName");

                    foreach (var item in list)
                    {

                        DataRow row = dt1.NewRow();
                        row["CostCenterName"] = item.CostCenterName;
                        row["CostCenterId"] = item.CostCenterId;
                        row["AssignedAmount"] = item.AssignedAmount;
                        row["RealAmount"] = item.RealAmount;
                        row["RealAmountPct"] = item.RealAmountPct;
                        row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                        row["Dates"] = startDateStr + " al " + endDateStr;
                        dt1.Rows.Add(row);

                        foreach (var details in item.VehiclesDetail)
                        {
                            DataRow row2 = GetVehicleRow(dt2, startDateStr, endDateStr, details);
                            row2["CostCenterId"] = item.CostCenterId;                            
                            dt2.Rows.Add(row2);
                        }
                    }
                    #endregion
                    break;
                case ReportFuelTypes.VehicleGroups:
                    #region ReportFuelTypes.VehicleGroups
                    dt1 = new DataTable("CurrentFuelsReport_VehicleGroups");
                    dt2 = new DataTable("CurrentFuelsDetail_VehicleGroups");

                    dt1.Columns.Add("VehicleGroupName");
                    dt1.Columns.Add("VehicleGroupId");
                    dt1.Columns.Add("AssignedAmount", typeof(decimal));
                    dt1.Columns.Add("RealAmount", typeof(decimal));
                    dt1.Columns.Add("RealAmountPct", typeof(decimal));
                    dt1.Columns.Add("CustomerName");
                    dt1.Columns.Add("Dates");


                    ConfigureDataTable(dt2);
                    dt2.Columns.Add("VehicleGroupId");
                    dt2.Columns.Add("VehicleName");

                    foreach (var item in list)
                    {
                        DataRow row = dt1.NewRow();

                        row["VehicleGroupName"] = item.VehicleGroupName;
                        row["VehicleGroupId"] = item.VehicleGroupId;

                        row["AssignedAmount"] = item.AssignedAmount;
                        row["RealAmount"] = item.RealAmount;
                        row["RealAmountPct"] = item.RealAmountPct;
                        row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                        row["Dates"] = startDateStr + " al " + endDateStr;
                        dt1.Rows.Add(row);

                        foreach (var details in item.VehiclesDetail)
                        {
                            DataRow row2 = GetVehicleRow(dt2, startDateStr, endDateStr, details);
                            row2["VehicleGroupId"] = details.VehicleGroupId;

                            dt2.Rows.Add(row2);
                        }
                    }
                    #endregion
                    break;
                case ReportFuelTypes.VehicleUnits:
                    #region ReportFuelTypes.VehicleUnits
                    dt1 = new DataTable("CurrentFuelsReport_VehicleUnits");
                    dt2 = new DataTable("CurrentFuelsDetail_VehicleUnits");

                    dt1.Columns.Add("UnitId");
                    dt1.Columns.Add("VehicleUnitName");
                    dt1.Columns.Add("AssignedAmount", typeof(decimal));
                    dt1.Columns.Add("RealAmount", typeof(decimal));
                    dt1.Columns.Add("RealAmountPct", typeof(decimal));

                    ConfigureDataTable(dt2);
                    dt2.Columns.Add("UnitId");

                    foreach (var item in list)
                    {
                        DataRow row = dt1.NewRow();
                        row["UnitId"] = item.UnitId;
                        row["VehicleUnitName"] = item.VehicleUnitName;
                        row["AssignedAmount"] = item.AssignedAmount;
                        row["RealAmount"] = item.RealAmount;
                        row["RealAmountPct"] = item.RealAmountPct;
                        dt1.Rows.Add(row);

                        foreach (var details in item.VehiclesDetail)
                        {
                            DataRow row2 = GetVehicleRow(dt2, startDateStr, endDateStr, details);
                            row2["UnitId"] = details.VehicleGroupId;

                            dt2.Rows.Add(row2);
                        }
                    }                    
                    #endregion
                    break;
            }

            ds.Tables.Add(dt1);
            ds.Tables.Add(dt2);
            return ds;

        }

        private static void ConfigureDataTable(DataTable dt2)
        {
            dt2.Columns.Add("PlateNumber");
            dt2.Columns.Add("PresupuestoMensual", typeof(decimal));
            dt2.Columns.Add("PresupuestoAdicional", typeof(decimal));
            dt2.Columns.Add("PresupuestoTotal", typeof(decimal));
            dt2.Columns.Add("LitrosMensual", typeof(decimal));
            

            dt2.Columns.Add("LitrosAdicional", typeof(decimal));
            dt2.Columns.Add("LitrosTotal", typeof(decimal));
            dt2.Columns.Add("ConsumoMes", typeof(decimal));
            dt2.Columns.Add("LitrosMes", typeof(decimal));
            dt2.Columns.Add("LitrosRealesMes", typeof(decimal));
            dt2.Columns.Add("Porcentaje", typeof(decimal));

            dt2.Columns.Add("Disponible", typeof(decimal));
            dt2.Columns.Add("DisponibleLitros", typeof(decimal));
            dt2.Columns.Add("FuelName");

            dt2.Columns.Add("CustomerName");
            dt2.Columns.Add("Dates");
            dt2.Columns.Add("TypeReport");
        }

        private static DataRow GetVehicleRow(DataTable dt2, string startDateStr, string endDateStr, dynamic details)
        {
            DataRow row2 = dt2.NewRow();
            decimal price = 1;
            decimal disponible = 0;
            if (details.AssignedLiters == 0)
            {
                price = 1;
            }
            else
            {
                price = details.AssignedAmount / details.AssignedLiters;
            }

            disponible = (details.AditionalAmount + details.AssignedAmount) - details.RealAmount;

            row2["PlateNumber"] = details.PlateNumber;
            if (ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 100)
            {
                row2["PlateNumber"] = details.DecryptedName;
            }

			row2["VehicleName"] = details.VehicleName;
			row2["PresupuestoMensual"] = details.AssignedAmount;
            row2["PresupuestoAdicional"] = details.AditionalAmount;
            row2["PresupuestoTotal"] = details.AditionalAmount + details.AssignedAmount;
            row2["LitrosMensual"] = details.AssignedLiters;
            row2["LitrosAdicional"] = details.AditionalLiters;
            row2["LitrosTotal"] = details.AssignedLiters + details.AditionalLiters;
            row2["ConsumoMes"] = details.RealAmount;
            row2["LitrosMes"] = details.Liters; //price > 0 ? details.RealAmount / price : 0;
            row2["LitrosRealesMes"] = details.RealLiters;
            row2["Porcentaje"] = details.RealAmountPct;
            row2["Disponible"] = disponible;
            row2["DisponibleLitros"] = details.AvailableLiters;
            row2["FuelName"] = details.FuelName;
            row2["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
            row2["Dates"] = startDateStr + " al " + endDateStr;
            row2["TypeReport"] = ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId;
            row2["CustomerCapacityUnit"] = details.CustomerCapacityUnit;


            return row2;
        }

        public DataTable CurrentFuelReportGenerate(IEnumerable<dynamic> list, DateTime startDate, DateTime endDate, int? ReportType)
        {

            var dt = new DataTable();

            var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
            var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
            //Get array letters
            char[] letters = startDateStr.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            startDateStr = new string(letters);
            letters = endDateStr.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            endDateStr = new string(letters);

            switch ((ReportFuelTypes)ReportType)
            {
                case ReportFuelTypes.Vehicles:
                    dt.Columns.Add("PlateNumber");
					dt.Columns.Add("VehicleName");
					dt.Columns.Add("PresupuestoMensual", typeof(decimal));
                    dt.Columns.Add("PresupuestoAdicional", typeof(decimal));
                    dt.Columns.Add("PresupuestoTotal", typeof(decimal));
                    dt.Columns.Add("LitrosMensual", typeof(decimal));

                    dt.Columns.Add("LitrosAdicional", typeof(decimal));
                    dt.Columns.Add("LitrosTotal", typeof(decimal));
                    dt.Columns.Add("ConsumoMes", typeof(decimal));
                    dt.Columns.Add("LitrosMes", typeof(decimal));
                    dt.Columns.Add("LitrosRealesMes", typeof(decimal));
                    dt.Columns.Add("Porcentaje", typeof(decimal));

                    dt.Columns.Add("Disponible", typeof(decimal));
                    dt.Columns.Add("DisponibleLitros", typeof(decimal));
                    dt.Columns.Add("FuelName");

                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("Dates");
                    dt.Columns.Add("TypeReport");

                    dt.Columns.Add("CustomerCapacityUnit");
                    
                        
                    foreach (var item in list)
                    {
                        DataRow row = GetVehicleRow(dt, startDateStr, endDateStr, item);
                        dt.Rows.Add(row);
                    }

                    break;

                case ReportFuelTypes.VehicleGroups:
                    dt.Columns.Add("VehicleGroupName");

                    dt.Columns.Add("AssignedAmount", typeof(decimal));
                    dt.Columns.Add("RealAmount", typeof(decimal));
                    dt.Columns.Add("RealAmountPct", typeof(decimal));

                    foreach (var item in list)
                    {
                        DataRow row = dt.NewRow();

                        row["VehicleGroupName"] = item.VehicleGroupName;

                        row["AssignedAmount"] = item.AssignedAmount;
                        row["RealAmount"] = item.RealAmount;
                        row["RealAmountPct"] = item.RealAmountPct;
                        dt.Rows.Add(row);
                    }
                    break;

                case ReportFuelTypes.VehicleUnits:

                    dt.Columns.Add("VehicleUnitName");
                    dt.Columns.Add("AssignedAmount", typeof(decimal));
                    dt.Columns.Add("RealAmount", typeof(decimal));
                    dt.Columns.Add("RealAmountPct", typeof(decimal));

                    foreach (var item in list)
                    {
                        DataRow row = dt.NewRow();

                        row["VehicleUnitName"] = item.VehicleUnitName;
                        row["AssignedAmount"] = item.AssignedAmount;
                        row["RealAmount"] = item.RealAmount;
                        row["RealAmountPct"] = item.RealAmountPct;
                        dt.Rows.Add(row);
                    }

                    break;
            }

            return dt;

        }

        /// <summary>
        /// Generate Real Vs Budget FuelsReport Excel
        /// </summary>
        /// <returns>A model of CurrentFuelsReportBase in order to load the chart</returns>
        public byte[] GenerateCurrentFuelsReportExcel(ControlFuelsReportsBase parameters)
        {
            var result = new CurrentFuelsReportBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Consumo de Combustible Real del Período", result.List.ToList());
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}