﻿/************************************************************************************************************
*  File    : VehiclesBusiness.cs
*  Summary : Vehicles Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using System.Web.Script.Serialization;
using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// Vehicles Class
    /// </summary>
    public class VehiclesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Vehicles> RetrieveVehicles(int? vehicleId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<Vehicles>("[General].[Sp_Vehicles_Retrieve]",
                new
                {
                    VehicleId = vehicleId,
                    CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// Retrieve Vehicle by Plate
        /// </summary>
        /// <param name="plateId">The PlateId uniquely that identifies each vehicle</param>
        /// <returns>An IEnumerarble Vehicles Vehicles is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Vehicles> RetrieveVehicleByPlate(string plateId, int customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                int? UserId = null;

                try
                {
                    var user = ECOsystem.Utilities.Session.GetUserInfo();
                    UserId = user.UserId;
                }
                catch (Exception)
                {
                    UserId = null;
                } 

                return dba.ExecuteReader<Vehicles>("[General].[Sp_VehicleByPlate_Retrieve]",
                new {
                    PlateId = plateId,
                    CustomerId = customerId,
                    UserId
                }); 
            }
        }


        /// <summary>
        /// Retrieve Active Customers Count
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public int RetrieveRegisteredVehiclesCount(int? partnerId, int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteScalar<int>("[Insurance].[Sp_RegisteredVehiclesCount_Retrieve]",
                new
                {
                    PartnerId = partnerId,
                    CustomerId = customerId,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// Retrieve VehiclesReferences
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehiclesReferences> RetrieveVehiclesReferences(int? vehicleId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<VehiclesReferences>("[General].[Sp_VehiclesReferences_Retrieve]",
                new
                {
                    VehicleId = vehicleId,
                    Key = key,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicles(Vehicles model)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[General].[Sp_Vehicles_AddOrEdit]",
                    new
                    {
                        VehicleId = model.VehicleId,
                        PlateId = model.PlateId,
                        Name = model.Name,
                        CostCenterId = model.CostCenterId,
                        CustomerId = model.CustomerId ?? ECOsystem.Utilities.Session.GetCustomerId(),
                        UserId = model.UserId,
                        VehicleCategoryId = model.VehicleCategoryId,
                        Active = model.Active,
                        Colour = model.Colour,
                        Chassis = model.Chassis,
                        LastDallas = model.LastDallas,
                        FreightTemperature = model.FreightTemperature,
                        FreightTemperatureThreshold1 = model.FreightTemperatureThreshold1,
                        FreightTemperatureThreshold2 = model.FreightTemperatureThreshold2,
                        Predictive = model.Predictive,
                        AVL = model.AVL,
                        PhoneNumber = model.PhoneNumber,
                        Insurance = model.Insurance,
                        DateExpirationInsurance = model.DateExpirationInsurance,
                        CoverageType = model.CoverageType,
                        CabinPhone = model.CabinPhone,
                        NameEnterpriseInsurance = model.NameEnterpriseInsurance,
                        PeriodicityId = model.PeriodicityId,
                        Cost = model.Cost,
                        Odometer = model.Odometer,
                        LoggedUserId = model.LoggedUserId,
                        AdministrativeSpeedLimit = model.AdministrativeSpeedLimit,
                        IntrackReference = model.IntrackReference,
                        Imei = model.Imei,
                        RowVersion = model.RowVersion
                    });
                }
                String message = string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model));
                new EventLogBusiness().AddLogEvent(LogState.INFO, message);
            }
            catch (Exception exception)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, exception);
                throw;
            }
        }

        /// <summary>
        /// Performs the References Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void EditVehiclesReferences(VehiclesReferences model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehiclesReferences_Edit]",
                new
                {
                    model.VehicleId,
                    model.CustomerId,
                    model.AdministrativeSpeedLimit,
                    model.IntrackReference,
                    model.DeviceReference,
                    model.LoggedUserId,
                    model.RowVersion
                });
            }

        }
        /// <summary>
        /// Performs the the operation of delete on the model Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehicles(int vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Vehicles_Delete]",
                    new { VehicleId = vehicleId,
                          ModifyUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CostCenterId"></param>
        /// <returns></returns>
        public IEnumerable<Vehicles> RetrieveVehicleByCostCenter(int CostCenterId)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<Vehicles>("[General].[Sp_Vehicles_Retrieve]",
                new
                {
                    CostCenterId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),                        
                    user.UserId
                });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="costCenterId"></param>
        /// <param name="opc"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<VehicleCostCenters> RetrieveVehicleCostCenters(int? costCenterId, Boolean opc = false, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<VehicleCostCenters>("[General].[Sp_VehicleCostCenters_Retrieve]",
                new
                {
                    CostCenterId = costCenterId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key,
                    user.UserId
                });

            }
        }

        /// <summary>
        /// Retrieve VehicleGroups
        /// </summary>
        /// <param name="vehicleGroupId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleGroups> RetrieveVehicleGroups(int? vehicleGroupId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<VehicleGroups>("[General].[Sp_VehicleGroups_Retrieve]",
                new
                {
                    VehicleGroupId = vehicleGroupId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// Retrieve VehicleUnits
        /// </summary>
        /// <param name="unitId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleUnits> RetrieveVehicleUnits(int? unitId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<VehicleUnits>("[General].[Sp_VehicleUnits_Retrieve]",
                new
                {
                    UnitId = unitId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key
                });
            }
        }

        /// <summary>
        /// Retrieve VehicleCategories
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        ///   <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleCategories> RetrieveVehicleCategories(int? vehicleCategoryId, bool opc = false, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {

                return dba.ExecuteReader<VehicleCategories>("[General].[Sp_VehicleCategories_Retrieve]",
                new
                {
                    VehicleCategoryId = vehicleCategoryId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key
                });
            }
        }
        

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}