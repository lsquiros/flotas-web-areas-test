﻿﻿﻿/************************************************************************************************************
*  File    : FuelsByCreditBusiness.cs
*  Summary : FuelsByCredit Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Account;
using  ECOsystem.Areas.Control.Models;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// FuelsByCredit Class
    /// </summary>
    public class FuelsByCreditBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Dashboard Control Indicators
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public DashboardControlIndicators RetrieveDashboardIndicator(int CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<DashboardControlIndicators>("[Control].[Sp_DashboardControlIndicators_Retrieve]",
                    new
                    {
                        CustomerId
                    })
                    .FirstOrDefault();
            }
        }


        /// <summary>
        /// Retrieve Fuels By Credit information
        /// </summary>
        /// <returns>A FuelsByCreditBase Model</returns>
        public FuelsByCreditBase RetrieveFuelsByCredit(DateTime? date = null)
        {
            var result = new FuelsByCreditBase();

            result.Menus = new List<AccountMenus>();

            DateTime currentDate = date ?? DateTime.UtcNow;

            using (var business = new CustomerCreditsBusiness())
            {
                result.CreditInfo = business.RetrieveCustomerCredits(currentDate).FirstOrDefault();
            }
            if (result.CreditInfo == null)
            {
                result.List = new List<FuelsByCredit>();
                result.Data = new List<FuelsByCredit>();
                return result;
            }

            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                result.List = dba.ExecuteReader<FuelsByCredit>("[Control].[Sp_FuelsByCredit_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    });
            }

            IList<Fuels> fuels;

            fuels = RetrieveFuels(null, countryId: ECOsystem.Utilities.Session.GetCountryId()).ToList();

            using (var bus = new VehicleFuelByGroupBusiness())
            {
                result.VehiclesList = bus.RetrieveData(new DistributionParameters
                {
                    IssueForId = (int)ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId,
                    IsSum = true
                }, null);
            }

            result.Data = result.List.ToList();
            if (result.Data.Count() < fuels.Count())
            {
                foreach (var fuel in fuels.Where(fuel => result.Data.Count(x => x.FuelId == fuel.FuelId) == 0))
                    result.Data.Add(new FuelsByCredit { FuelId = fuel.FuelId, FuelName = fuel.Name, Year = result.CreditInfo.Year, Month = result.CreditInfo.Month, CurrencySymbol = result.CreditInfo.CurrencySymbol });
            }
            result.List = result.List.Except(result.List.Where(x => x.Year == currentDate.Year && x.Month == currentDate.Month).ToList()).ToList();
            result.AssignedCredit = result.Data.Sum(x => x.Total);
            result.AssignedLiters = result.Data.Sum(x => x.CapacityUnitValue);
            result.PctAssignedCredit = (result.AssignedCredit / result.CreditInfo.CreditAmount) * 100;


            return result;
        }

        /// <summary>
        /// Retrieve Fuels By Credit information
        /// </summary>
        /// <returns>A FuelsByCreditBase Model</returns>
        public FuelsByCreditBase RetrieveFuelsByCreditByCostCenter(int? CustomerId, int? CostCenterId)
        {
            var result = new FuelsByCreditBase();

            result.Menus = new List<AccountMenus>();

            DateTime currentDate = DateTime.UtcNow;

            using (var business = new CustomerCreditsBusiness())
            {
                result.CreditInfo = business.FuelDistributionByCostCenterRetrieve(CustomerId, CostCenterId).FirstOrDefault();
            }
            if (result.CreditInfo == null)
            {
                result.List = new List<FuelsByCredit>();
                result.Data = new List<FuelsByCredit>();
                return result;
            }

            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                result.List = dba.ExecuteReader<FuelsByCredit>("[Control].[Sp_FuelsByCreditByCostCenter_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                       ,CostCenterId
                    });
            }

            IList<Fuels> fuels;

            fuels = RetrieveFuels(null, countryId: ECOsystem.Utilities.Session.GetCountryId()).ToList();

            using (var bus = new VehicleFuelByGroupBusiness())
            {
                result.VehiclesList = bus.RetrieveData(new DistributionParameters
                {
                    IssueForId = (int)ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId,
                    IsSum = true,
                    CostCenterId = CostCenterId
                }, null);
            }

            result.Data = result.List.ToList();
            if (result.Data.Count() < fuels.Count())
            {
                foreach (var fuel in fuels.Where(fuel => result.Data.Count(x => x.FuelId == fuel.FuelId) == 0))
                    result.Data.Add(new FuelsByCredit { FuelId = fuel.FuelId, FuelName = fuel.Name, Year = result.CreditInfo.Year, Month = result.CreditInfo.Month, CurrencySymbol = result.CreditInfo.CurrencySymbol });
            }
            result.List = result.List.Except(result.List.Where(x => x.Year == currentDate.Year && x.Month == currentDate.Month).ToList()).ToList();
            result.AssignedCredit = result.Data.Sum(x => x.Total);
            result.AssignedLiters = result.Data.Sum(x => x.CapacityUnitValue);
            result.PctAssignedCredit = result.CreditInfo.CreditAmount > 0 ? (result.AssignedCredit / result.CreditInfo.CreditAmount) * 100 : 0;


            return result;
        }


        /// <summary>
        /// Retrieve Fuels By Credit summary information
        /// </summary>
        /// <returns>A FuelsByCreditSummary Model</returns>
        public FuelsByCreditSummary RetrieveFuelsByCreditSummary()
        {
            var result = new FuelsByCreditSummary();
            var currentDate = DateTimeOffset.Now;

            using (var business = new CustomerCreditsBusiness())
            {
                result.CreditInfo = business.RetrieveCustomerCredits().FirstOrDefault();
            }
            /*if (result.CreditInfo == null)
                throw new Exception("Invalid information for current date, Please check the credit information with your Partner");*/

            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<FuelsByCredit>("[Control].[Sp_FuelsByCredit_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    });
            }

            //using (var business = new VehicleFuelBusiness())
            //{
            //    var IsSum = 1;
            //    result.VehiclesList = business.RetrieveVehiclesFuelByCustomer(IsSum).ToList();
            //}

            using (var bus = new VehicleFuelByGroupBusiness())
            {
                result.VehiclesList = bus.RetrieveData(new DistributionParameters
                {
                    IssueForId = (int)ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId,
                    IsSum = true
                }, null);
            }

            result.AssignedCredit = result.List.Sum(x => x.Total);
            if ((result.AssignedCredit != null && result.AssignedCredit >= 0) && (result.CreditInfo != null && result.CreditInfo.CreditAmount > 0))
            {
                result.PctAssignedCredit = (result.AssignedCredit / result.CreditInfo.CreditAmount) * 100;
            }
            else
            {
                result.PctAssignedCredit = 0;
            }

            return result;
        }

        /// <summary>
        /// Retrieve Fuel Amounts By Credit
        /// </summary>
        /// <returns>IList of FuelsByCredit Model</returns>
        public IList<FuelsByCredit> RetrieveFuelAmountsByCredit()
        {
            using (var dba = new DataBaseAccess())
            {
                var currentDate = DateTime.UtcNow;
                return dba.ExecuteReader<FuelsByCredit>("[Control].[Sp_FuelsByCredit_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity FuelsByCredit
        /// </summary>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditFuelsByCredit(IList<FuelsByCreditModel> data)
        {
            using (var dba = new DataBaseAccess())
            {
                foreach (var item in data.Where(x => x.Total != null && x.Total != 0))
                {
                    dba.ExecuteNonQuery("[Control].[Sp_FuelsByCredit_AddOrEdit]", item);
                }
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity FuelsByCredit
        /// </summary>
        /// <param name="data">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditFuelsByCreditByCostCenter(IList<FuelsByCreditModel> data)
        {
            using (var dba = new DataBaseAccess())
            {
                foreach (var item in data.Where(x => x.Total != null && x.Total != 0))
                {
                    dba.ExecuteNonQuery("[Control].[Sp_FuelsByCredit_ByCostCenter_AddOrEdit]",
                        new
                        {
                            item.CustomerCreditByCostCenterId,
                            item.CustomerId,
                            item.CostCenterId,
                            item.FuelByCreditId,
                            item.FuelId,
                            item.Total,
                            item.LoggedUserId
                        });
                }
            }
        }

        /// <summary>
        /// Retrieve Fuels
        /// </summary>
        /// <param name="fuelId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="countryId">Optional country id to filter fuels by country</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Fuels> RetrieveFuels(int? fuelId, int? countryId = null, string key = null)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<Fuels>("[Control].[Sp_Fuels_Retrieve]",
                new
                {
                    FuelId = fuelId,
                    Key = key,
                    CountryId = countryId
                });
            }
        }


        /// <summary>
        /// Retrieve CreditCard that are close to the limit
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CreditCard> RetrieveCreditCardCloseToLimite(int? creditCardId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardCloseCreditLimit_Retrieve]",
                    new
                    {
                        CreditCardId = creditCardId,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId() ?? 0,
                        Key = string.IsNullOrEmpty(key) ? null : key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}

