﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceRecordByVehicleBusiness.cs
*  Summary : Preventive Maintenance Record By Vehicle Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 01/07/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// Preventive Maintenance By Vehicle Business class performs all related to operators business logic
    /// </summary>
    public class PreventiveMaintenanceRecordByVehicleBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Preventive Maintenance Vehicles
        /// </summary>
        /// <param name="customerId">The FK that identifies each row by Catalog of Users</param>
        /// <param name="preventiveMaintenanceCatalogId">The FK that identifies each row by Catalog of Preventive Type</param>
        /// <returns>IEnumerable collection of the PreventiveMaintenanceRecordByVehicle entity Model</returns>
        public IEnumerable<PreventiveMaintenanceVehicles> RetrievePreventiveMaintenanceVehicles(int? customerId, int? preventiveMaintenanceCatalogId, int? preventiveMaintenanceId = null, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PreventiveMaintenanceVehicles>("[General].[Sp_PreventiveMaintenanceVehicles_Retrieve]",
                new
                {
                    CustomerId = customerId,
                    PreventiveMaintenanceCatalogId = preventiveMaintenanceCatalogId,
                    Key = key
                });
            }
        }

        /// <summary>
        /// Retrieve Preventive Maintenance Record By Vehicle
        /// </summary>
        /// <param name="customerId">The FK that identifies each row by Catalog of Users</param>
        /// <param name="preventiveMaintenanceCatalogId">The FK that identifies each row by Catalog of Preventive Type</param>
        /// <returns>IEnumerable collection of the PreventiveMaintenanceRecordByVehicle entity Model</returns>
        public IEnumerable<PreventiveMaintenanceRecordByVehicle> RetrievePreventiveMaintenanceRecordByVehicle(int? customerId, int? preventiveMaintenanceCatalogId, int? preventiveMaintenanceId = null, int? vehicleId = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PreventiveMaintenanceRecordByVehicle>("[General].[Sp_PreventiveMaintenanceRecordByVehicle_Retrieve]",
                new
                {
                    CustomerId = customerId,
                    PreventiveMaintenanceId = preventiveMaintenanceId,
                    PreventiveMaintenanceCatalogId = preventiveMaintenanceCatalogId,
                    Vehicle = vehicleId
                });
            }
        }

        /// <summary>
        /// Retrieve Preventive Maintenance Record By Vehicle Detail
        /// </summary>
        /// <param name="customerId">The FK that identifies each row by Catalog of Users</param>
        /// <param name="preventiveMaintenanceCatalogId">The FK that identifies each row by Catalog of Preventive Type</param>
        /// <returns>IEnumerable collection of the PreventiveMaintenanceRecordByVehicle entity Model</returns>
        public PreventiveMaintenanceRecordByVehicleCostDetail PreventiveMaintenanceRecordByVehicleDetail(int? Id)
        {
            using (var dba = new DataBaseAccess())
            {
                PreventiveMaintenanceRecordByVehicleCostDetail model = new PreventiveMaintenanceRecordByVehicleCostDetail();
                var resultCost = dba.ExecuteReader<PreventiveMaintenanceRecordByVehicleCost>("[Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]",
                new
                {
                    Id = Id
                });
                var resultHeader = dba.ExecuteReader<PreventiveMaintenanceRecordByVehicleCostHeader>("[Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]",
                new
                {
                    Id = Id
                });
                model.Cost = (List<PreventiveMaintenanceRecordByVehicleCost>)resultCost;
                model.Header = (PreventiveMaintenanceRecordByVehicleCostHeader)resultHeader.FirstOrDefault();
                return model;
            }
        }
        
        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Preventive Maintenance By Vehicle
        /// </summary>
        /// <param name="model">Model or class passed as parameter which contains all properties to perform insert or update operations</param>
        public void AddOrEditPreventiveMaintenanceRecordByVehicle(PreventiveMaintenanceRecordByVehicle model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceRecordByVehicle_AddOrEdit]",
                    new
                    {
                        PreventiveMaintenanceId = model.PreventiveMaintenanceId,
                        PlateId = model.VehicleId,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        PreventiveMaintenanceCatalogIdSelect = model.PreventiveMaintenanceCatalogIdSelect,
                        LastReview = model.LastReview,
                        Day = model.Day,
                        Month = model.Month,
                        Year = model.Year,
                        LoggedUserId = model.LoggedUserId,
                        RowVersion = model.RowVersion
                    });
            }
        }

        /// <summary>
        ///  Performs the the operation of delete on the model PreventiveMaintenanceRecordByVehicleUnits
        /// </summary>
        /// <param name="preventiveMaintenanceByVehiculeId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeletePreventiveMaintenanceRecordByVehicleDetail(int id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]",
                    new { 
                        Id = id,
                        LoggedUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Operators
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditPreventiveMaintenanceRecordByVehicleDetail(List<string> model, int PreventiveMaintenanceRecordByVehicleId, int PreventiveMaintenanceId)
        {
            JavaScriptSerializer jsTool = new JavaScriptSerializer();
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            foreach (string item in model)
            {
                var record = jsTool.Deserialize<PreventiveMaintenanceRecordByVehicleCost>(item);
                root.Add(new XElement("PreventiveMaintenanceRecordByVehicleCostDetail", new XAttribute("PreventiveMaintenanceRecordByVehicleDetailId", record.PreventiveMaintenanceRecordByVehicleDetailId) , new XAttribute("PreventiveMaintenanceRecordByVehicleId", record.PreventiveMaintenanceRecordByVehicleId) , new XAttribute("PreventiveMaintenanceCostId", record.PreventiveMaintenanceCostId) , new XAttribute("Description", record.Description) , new XAttribute("Cost", record.Cost) , new XAttribute("Record", record.Record)));
            }
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]",
                    new
                    {
                        XmlData = doc.ToString(),
                        PreventiveMaintenanceRecordByVehicleId = PreventiveMaintenanceRecordByVehicleId,
                        PreventiveMaintenanceId = PreventiveMaintenanceId,
                        LoggedUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }

        }
        
        /// <summary>
        /// Performs the the operation of delete on the model Record
        /// </summary>
        /// <param name="preventiveMaintenanceCatalogId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeletePreventiveMaintenance(int preventiveMaintenanceCatalogId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceCatalog_Delete]",
                    new { PreventiveMaintenanceCatalogId = preventiveMaintenanceCatalogId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}