﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using  ECOsystem.Areas.Control.Models.Control;
using  ECOsystem.Areas.Control.Business.Control;
using ECOsystem.Business.Core;

namespace  ECOsystem.Areas.Control.Business
{    
    public class GeneralCollections
    {
        public static SelectList GetApprovalTransactionsTypes(int? type = null)
        {
            var dictionary = new Dictionary<Guid, string>();

            using (var business = new ApprovalTransactionBusiness())
            {
                var result = business.GetTypes(type).OrderBy(x => x.Name);
                foreach (var item in result)
                {
                    dictionary.Add(item.Id, item.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }
       
        public static SelectList GetEditableApprovalTransactionsTypes(bool Editable)
        {
            var dictionary = new Dictionary<Guid, string>();

            using (var business = new ApprovalTransactionBusiness())
            {
                var result = business.GetEditableTypes(Editable);
                foreach (var item in result)
                {
                    dictionary.Add(item.Id, item.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value");         
        }

        /// <summary>
        /// Return a list of Report Report Fuel Types in order to using it for populate DropDownListFor
        /// It's the same GetReportFuelTypes + VehicleUnits, Requirement only for CurrentFuelsReport
        /// When more fuel's reports required VehicleUnits delete this method
        /// </summary>
        public static SelectList GetReportFuelTypes2
        {
            get
            {
                return new SelectList(new Dictionary<int, string> {
                    {(int)ReportFuelTypes.Vehicles,"Vehículos"},
                    {(int)ReportFuelTypes.VehicleGroups,"Grupo de Vehículos"},
                    {(int)ReportFuelTypes.VehicleCostCenters,"Centro de Costos"},
                    //{(int)ReportFuelTypes.VehicleUnits,"Unidades"},
                }, "Key", "Value", (int)ReportFuelTypes.Vehicles);
            }
        }

        public static SelectList ApplyAllDistributionValues
        {
            get
            {
                if (ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 101)
                {
                    return new SelectList(new Dictionary<string, string> {
                                { "AMOUNT" , "Por Monto" },
                                { "CAPACITYUNIT" , "Por " + ECOsystem.Utilities.Session.GetCustomerCapacityUnitName() }
                            }, "Key", "Value");
                }

                return new SelectList(new Dictionary<string, string> {
                    { "AMOUNT" , "Por Monto" }                    
                }, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleGroups
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicleGroups(null);

                    foreach (var r in result)
                    {
                        dictionary.Add(r.VehicleGroupId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetVehicleGroupsAll
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicleGroups(null);

                    if (result.Count() > 0)
                    {
                        dictionary.Add(-1, "Todos");
                    }

                    foreach (var r in result)
                    {
                        dictionary.Add(r.VehicleGroupId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Sub Units in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleCostCenters
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var result = new VehiclesBusiness().RetrieveVehicleCostCenters(null, true);
                dictionary.Add(-1, "Todos");
                foreach (var r in result)
                {
                    dictionary.Add(r.CostCenterId, r.Name);
                }

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static List<VehicleCostCenters>  GetCostCenters()
        {
            return new VehiclesBusiness().RetrieveVehicleCostCenters(null, true).ToList();
        }

        /// <summary>
        /// Return a list of Vehicle Sub Units in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleCostCentersWithoutAll
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var result = new VehiclesBusiness().RetrieveVehicleCostCenters(null, true);
                foreach (var r in result)
                {
                    dictionary.Add(r.CostCenterId, r.Name);
                }

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetMonthNames
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a current Month Name
        /// </summary>
        public static SelectList GetCurrentMonthName
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.Where(w => w.Id == DateTime.Now.Month).ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a list of currencies in order to using it for populate DropDownList
        /// </summary>
        public static SelectList GetPreventiveMaintenanceCatalog
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new PreventiveMaintenanceVehicleBusiness())
                {
                    int FilterType = Convert.ToInt32(HttpContext.Current.Session["FilterType"]);
                    var result = business.RetrievePreventiveMaintenance(ECOsystem.Utilities.Session.GetCustomerId(), FilterType: FilterType, preventiveMaintenanceCatalogId: null, key: null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.PreventiveMaintenanceCatalogId, r.Description);
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year - 5; i < year + 9; i++)
                {
                    dictionary.Add(i, i);
                }
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        public static SelectList GetCardYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year; i < year + 15; i++)
                {   //ValidCreditCard for 15 years
                    dictionary.Add(i, i);
                }
                year = year + 1;
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterDetailTransacReport
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var year = DateTime.UtcNow.Year;
                dictionary.Add(1, "Centro de Costo");
                dictionary.Add(2, "Grupo de Vehículos");
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }


        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetReportCriteria
        {
            get
            {
                return new SelectList(new Dictionary<int, string> {
                    {(int)ReportCriteria.Period,"Por Período"},
                    {(int)ReportCriteria.DateRange,"Rango de Fechas"}
                }, "Key", "Value", "");
            }
        }

        /// <summary>
        /// Return a list of Preventive Maintenance Report Status
        /// </summary>
        public static SelectList GetReportPreventiveMaintenanceStatus
        {
            get
            {
                return new SelectList(new Dictionary<int, string> {
                    {(int)ReportPreventiveMaintenanceStatus.Pendientes,"Mantenimientos Pendientes"},
                    {(int)ReportPreventiveMaintenanceStatus.Realizado,"Mantenimientos Realizados"},
                    {(int)ReportPreventiveMaintenanceStatus.Vencidos,"Mantenimientos Vencidos"},
                }, "Key", "Value", (int)ReportPreventiveMaintenanceStatus.Pendientes);
            }
        }


        /// <summary>
        /// Return a list of Report Report Fuel Types in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetReportFuelTypes
        {
            get
            {
                return new SelectList(new Dictionary<int, string> {
                    {(int)ReportFuelTypes.Vehicles,"Vehículos"},
                    {(int)ReportFuelTypes.VehicleGroups,"Grupo de Vehículos"},
                    {(int)ReportFuelTypes.VehicleCostCenters,"Centro de Costos"},
                }, "Key", "Value", (int)ReportFuelTypes.Vehicles);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static SelectList GetReporCompartativeTypes
        {
            get
            {
                return new SelectList(new Dictionary<int, string> {
                    {(int) ReportComparativeTypes.Vehicles,"Vehiculo"},
                    {(int) ReportComparativeTypes.brand,"Marca"},
                    {(int) ReportComparativeTypes.model,"Modelo"},
                   {(int) ReportComparativeTypes.route,"Ruta"},
                   {(int) ReportComparativeTypes.cilinder,"Cilindrajes"},


                }, "Key", "Value", (int)ReportFuelTypes.Vehicles);
            }
        }

        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesByCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicles(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.VehicleId))
                        {
                            dictionary.Add(r.VehicleId, "{'PlateId':'" + r.PlateId + "', 'Name':'" + r.Name + "', 'CategoryType':'" + r.CategoryType + "', 'FuelName': '" + r.FuelName + "', 'CostCenterName': '" + r.CostCenterName + "'}");
                        }
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetVehiclesByCustomerDistribution
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicles(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.VehicleId))
                        {
                            dictionary.Add(r.VehicleId, string.Format("{0} - {1}", r.PlateId, r.Name));
                        }
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetVehiclesByCustomerDistributionAll
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicles(null);

                    if (result.Count() > 0)
                    {
                        dictionary.Add(-1, "Todos");
                    }

                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.VehicleId))
                        {
                            dictionary.Add(r.VehicleId, string.Format("{0} - {1}", r.PlateId, r.Name));
                        }
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Drivers in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetDriversByCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string> { { -1, "{'Name':'Todos','Code':'','Identification':'' }" } };
                using (var business = new UsersBusiness())
                {
                    var result = business.RetrieveDrivers(ECOsystem.Utilities.Session.GetCustomerId());
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.UserId))
                        {
                            dictionary.Add(r.UserId, "{'Name':'" + r.DecryptedName + "', 'Code':'" + r.DecryptedCode + "', 'Identification': '" + r.DecryptedIdentification + "'}");
                        }
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetDriversByCustomerDistribution
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new UsersBusiness())
                {
                    var result = business.RetrieveDrivers(ECOsystem.Utilities.Session.GetCustomerId());
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.UserId))
                        {
                            dictionary.Add(r.UserId, string.Format("{0} - {1}", r.DecryptedCode, r.DecryptedName));
                        }
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        #region VehicleCategories

        /// <summary>
        /// Return a list of Vehicle Manufacturers in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesManufacturers
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicleCategories(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.Manufacturer)) dictionary.Add(r.Manufacturer, r.Manufacturer);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Models in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesModels
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicleCategories(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.VehicleModel)) dictionary.Add(r.VehicleModel, r.VehicleModel);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Types in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesTypes
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicleCategories(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.Type)) dictionary.Add(r.Type, r.Type);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Years in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int?>();

                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicleCategories(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.Year)) dictionary.Add(r.Year, r.Year);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Units in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleUnits
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicleUnits(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.UnitId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }


        #endregion


        /// <summary>
        /// Return a list of currencies in order to using it for populate DropDownList
        /// </summary>
        public static SelectList GetPreventiveMaintenanceByCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new ECOsystem.Business.Core.PreventiveMaintenanceCatalogBusiness())
                {
                    var result = business.RetrievePreventiveMaintenance(ECOsystem.Utilities.Session.GetCustomerId(), preventiveMaintenanceCatalogId: null, key: null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.PreventiveMaintenanceCatalogId, "{'Description':'" + r.Description + "', 'FrequencyKm':'" + r.FrequencyKm + "', 'AlertBeforeKm':'" + r.FrequencyMonth + "', 'FrequencyMonth':'" + r.FrequencyMonth + "', 'AlertBeforeMonth':'" + r.AlertBeforeMonth + "', 'FrequencyDate':'" + r.FrequencyDate + "', 'AlertBeforeDate':'" + r.AlertBeforeDate + "'}");
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }


        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetDatesGroupBy
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {1, "Agrupación por: Año"},
                    {2, "Agrupación por: Mes"},
                    {4, "Agrupación por: Semana"},
                    {3, "Agrupación por: Día"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetComparativeGroupBy
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {100, "Comparación por: Vehículo"},
                    {200, "Comparación por: Marca"},
                    {300, "Comparación por: Modelo"},
                    {400, "Comparación por: Tipo"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static HtmlString GetMonthCardNames(int p)
        {
            if (p == DateTime.Now.Year)
            {

                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name))
                                       .SkipWhile(element => element.Key < DateTime.Now.Month).ToList();
                return new HtmlString(JsonConvert.SerializeObject(dictionary.ToList()));
            }
            else
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new HtmlString(JsonConvert.SerializeObject(dictionary.ToList()));
            }
        }

        /// <summary>
        ///Return a list of Periodicity Alarm For populate DropDownListFor
        /// </summary>
        public static SelectList GetPeriodicityAlarm
        {
            get
            {
                IEnumerable<Types> result;
                using (var bus = new TypesBusiness())
                {
                    result = bus.RetrieveTypes("CUSTOMERS_PERIODICITY_ALARM");
                };

                var dictionary = result.ToDictionary<Types, int?, string>(r => r.TypeId, r => r.Name);

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetFilterMaintenanceType
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {0, "Todos los Mantenimientos"},
                    {1, "Mantenimientos Preventivos"},
                    {2, "Mantenimientos Correctivos"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static IEnumerable<Vehicles> VehiclesListByCustomer
        {
            get
            {
                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicles(null);
                    return result;
                }
            }
        }
    }
}
