﻿/************************************************************************************************************
*  File    : VehiclesByGroupBusiness.cs
*  Summary : VehiclesByGroup Business Methods
*  Author  : Berman Romero
*  Date    : 09/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using ECO_Control.Models.Control;

namespace ECO_Control.Business.Control
{
    /// <summary>
    /// VehiclesByGroup Class
    /// </summary>
    public class VehiclesByGroupBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehiclesByGroup
        /// </summary>
        /// <param name="vehicleGroupId">The vehicle group Id to find all vehicles associated</param>
        /// <returns>An IEnumerable of T, T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public VehiclesByGroupData RetrieveVehiclesByGroup(int? vehicleGroupId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = new VehiclesByGroupData();
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                var aux = dba.ExecuteReader<VehiclesByGroup>("[General].[Sp_VehiclesByGroup_Retrieve]",
                new
                {
                    VehicleGroupId = vehicleGroupId,
                    CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                    user.UserId
                });
                
                result.AvailableVehiclesList = aux.Where(x => x.VehicleGroupId == null);
                result.ConnectedVehiclesList = aux.Where(x => x.VehicleGroupId != null);
                result.VehicleGroupId = vehicleGroupId;

                return result;
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehiclesByGroup
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehiclesByGroup(VehiclesByGroupData model)
        {
            
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            var i = 0;
            foreach (var item in model.ConnectedVehiclesList)
            {
                root.Add(new XElement("Vehicle", new XAttribute("VehicleId", item.VehicleId), new XAttribute("Index", ++i)));
            }

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehiclesByGroup_AddOrEdit]",
                new
                {
                    model.VehicleGroupId,
                    XmlData = doc.ToString(),
                    LoggedUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model VehiclesByGroup
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehiclesByGroup(int vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehiclesByGroup_Delete]",
                    new { VehicleId = vehicleId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}