﻿/************************************************************************************************************
*  File    : VehicleFuelBusiness.cs
*  Summary : VehicleFuel Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Business.Utilities;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// VehicleFuel Class
    /// </summary>
    public class VehicleFuelBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleFuel
        /// </summary>
        /// <param name="Id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="vehicleId">The FK from vehicles table</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleFuel> RetrieveVehicleFuel(int? Id, int? vehicleId, string key = null)
        {
            key = !string.IsNullOrEmpty(key) ? key : null;

            if (key != null)
            {
                var val = GeneralCollections.GetMonthNames.ToList().FirstOrDefault(x => x.Text.ToLower().Contains(key.ToLower()));
                if (val != null)
                    key = "" + val.Value;
            }

            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<VehicleFuel>("[Control].[Sp_VehicleFuel_Retrieve]",
                    new
                    {
                        Id = Id,
                        VehicleId = vehicleId,
                        Key = key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve Vehicle FuelBase
        /// </summary>
        /// <param name="keyId">VehicleId to load grid</param>
        /// <returns>VehicleFuelBase Model to render view</returns>
        public VehicleFuelBase RetrieveVehicleFuelBase(int keyId)
        {
            var model = new VehicleFuelBase
            {
                KeyId = keyId,
                Data = new VehicleFuel()
            };
            IList<FuelsByCredit> aux;
            using (var business = new FuelsByCreditBusiness())
            {
                aux = business.RetrieveFuelAmountsByCredit();
            }
            using (var business = new VehicleFuelBusiness())
            {
                model.List = business.RetrieveVehicleFuel(null, keyId);
            }

            int? fuelId;
            using (var business = new VehiclesBusiness())
            {
                var tmp = business.RetrieveVehicles(keyId).FirstOrDefault();
                fuelId = tmp == null ? -1 : tmp.DefaultFuelId;
            }

            var fuelAmountsByCredit = aux.FirstOrDefault(x => x.FuelId == fuelId);
            model.LiterPrice = (fuelAmountsByCredit != null && fuelAmountsByCredit.LiterPrice != null) ? fuelAmountsByCredit.LiterPrice : 0.00m;
            model.CurrencySymbol = fuelAmountsByCredit == null ? "" : fuelAmountsByCredit.CurrencySymbol;

            return model;
        }

        /// <summary>
        /// Retrieve VehicleFuel By Customer
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleByFuel> RetrieveVehiclesFuelByCustomer(int? IsSum)
        {
            
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                var currentDate = DateTime.UtcNow;                
                return dba.ExecuteReader<VehicleByFuel>("[Control].[Sp_VehicleFuel_Retrieve]",
                    new
                    {
                        CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                        currentDate.Year,
                        currentDate.Month,
                        user.UserId,
                        IsSum
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleFuel
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleFuel(VehicleFuel model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_VehicleFuel_AddOrEdit]",
                    new
                    {
                        model.Id,
                        model.VehicleId,
                        model.Liters,
                        model.Amount,
                        model.Month,
                        model.Year,
                        CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleFuel
        /// </summary>
        /// <param name="Id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehicleFuel(int Id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_VehicleFuel_Delete]",
                    new { Id = Id });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}