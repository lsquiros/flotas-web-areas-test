﻿using  ECOsystem.Areas.Control.Models;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Business
{
    public class AccountFileReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Transactions Sap
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public AccountFileReportBase RetrieveAccountReportInfo(ControlFuelsReportsBase parameters)
        {
            var result = new AccountFileReportBase();
            GetReportData(result, parameters);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.TransactionType = parameters.TransactionType;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            result.Menus = new List<AccountMenus>();
            return result;
        }

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="result"></param>
        /// <param name="parameters"></param>
        public void GetReportData(AccountFileReportBase result, ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                result.List = dba.ExecuteReader<AccountFileReportModel>("[Control].[Sp_AccountFileReport_Retrieve]",
                new
                {
                    CustomerId = Convert.ToInt32(ECOsystem.Utilities.Session.GetCustomerId()),
                    parameters.Year,
                    parameters.Month,
                    parameters.StartDate,
                    parameters.EndDate,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            try
            {
                var result = new AccountFileReportBase();
                GetReportData(result, parameters);

                return ReportGenerate(result.List);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable ReportGenerate(IEnumerable<AccountFileReportModel> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("Identify");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("TransactionDate");
                dt.Columns.Add("InvoceDate");
                dt.Columns.Add("ServiceStationId");
                dt.Columns.Add("Fuel");
                dt.Columns.Add("Liters");
                dt.Columns.Add("Amount");
                dt.Columns.Add("Odometers");
                dt.Columns.Add("CostCenter");               

                foreach (var item in list)
                {
                    DataRow drDebe = dt.NewRow();

                    drDebe["Identify"] = item.Identify;
                    drDebe["VehicleName"] = item.VehicleName;
                    drDebe["TransactionDate"] = item.TransactionDate.ToString("dd/MM/yyyy hh:mm:ss tt");
                    drDebe["InvoceDate"] = item.InvoceDate.ToString("dd/MM/yyyy hh:mm:ss tt");
                    drDebe["ServiceStationId"] = item.ServiceStationId;
                    drDebe["Fuel"] = item.Fuel.Replace("Gasolina ", "").ToUpper();
                    drDebe["Liters"] = item.LitersStr;
                    drDebe["Amount"] = item.Amount;
                    drDebe["Odometers"] = item.Odometers;
                    drDebe["CostCenter"] = item.CostCenter;

                    dt.Rows.Add(drDebe);
                }

                return dt;
            }
        }        


        public static string GetIdentifier(DateTime date)
        {
            string Ndate = date.ToString("yyyy/MM/dd HH:mm:ss");

            return Ndate.Replace("/", "").Replace(":", "").Replace(" ", "");
        }

        /// <summary>
        /// GC Method
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}