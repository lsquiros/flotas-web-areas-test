﻿/************************************************************************************************************
*  File    : VehicleFuelBusiness.cs
*  Summary : VehicleFuel Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;

using System.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Coinca.Models;
using System.Configuration;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// VehicleFuel Class
    /// </summary>
    public class AdditionalVehicleFuelByGroupBusiness : IDisposable
    {

        /// <summary>
        /// Retrieve Vehicle Fuel By Group
        /// </summary>
        /// <param name="model"></param>
        /// <returns>VehicleFuelBase Model to render view</returns>
        public AdditionalVehicleFuelByGroupBase RetrieveAdditionalVehicleFuelByGroup(AdditionalVehicleFuelByGroupBase model)
        {
            var date = DateTime.ParseExact("01/" + DateTime.Today.Month + "/" + DateTime.Today.Year, "dd/M/yyyy", CultureInfo.CreateSpecificCulture("es-CR"));

            Areas.Control.Models.Control.CustomerCredits creditInfo = null;
            using (var business = new CustomerCreditsBusiness())
            {
                creditInfo = business.RetrieveCustomerCredits(null).FirstOrDefault();
            }
            if (creditInfo == null)
            {
                model.Data = new AdditionalVehicleFuelByGroup();
                model.GridInfo = new AdditionalVehicleFuelByGroupGrid();
                return model;
            }
            model.Data = new AdditionalVehicleFuelByGroup
            {
                VehicleGroupId = model.VehicleGroupId,
                Month = DateTime.Today.Month,
                Year = DateTime.Today.Year,
                CurrencySymbol = creditInfo.CurrencySymbol, 
                CostCenterId = model.CostCenterId, Rewrite = true
            };
            if (ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 101)
            {
                model.GridInfo = RetrieveAdditionalVehicleFuelByGroupList(new AdditionalVehicleFuelByGroup
                {
                    VehicleGroupId = model.VehicleGroupId,
                    Year = DateTime.Today.Year,
                    Month = DateTime.Today.Month,
                    VehicleId = model.KeyId,
                    CostCenterId = model.CostCenterId,
                    Id = null
                });
            }
            else
            {
                model.GridInfo = RetrieveAdditionalDriverFuelByGroupList(new AdditionalVehicleFuelByGroup
                {
                    VehicleGroupId = model.VehicleGroupId,
                    Year = DateTime.Today.Year,
                    Month = DateTime.Today.Month,
                    VehicleId = model.KeyId,
                    CostCenterId = model.CostCenterId,
                    DriverId = model.DriverId,
                    Id = null
                });
            }

            model.GridInfo.IsResult = false;
            model.GridInfo.CreditCardEmissionType = ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId;
            return model;
        }

        private AdditionalVehicleFuelByGroupGrid RetrieveAdditionalDriverFuelByGroupList(AdditionalVehicleFuelByGroup model)
        {
            model.Year = DateTime.Today.Year;
            model.Month = DateTime.Today.Month;
            var user = ECOsystem.Utilities.Session.GetUserInfo();
            using (var dba = new DataBaseAccess())
            {
                if (model.DriverId != null || model.CostCenterId != null || model.DriverId == -1)
                {
                    var result = new AdditionalVehicleFuelByGroupGrid
                    {
                        List = dba.ExecuteReader<AdditionalFuelDistribution>("[Control].[Sp_AdditionalDriverFuel_Retrieve]",
                         new
                         {
                             model.Year,
                             model.Month,
                             CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                             user.UserId,
                             model.DriverId,
                             model.CostCenterId
                         })
                    };
                    return result;
                }
                else
                {
                    int porcent = 0;

                    porcent = Convert.ToInt32(RetrieveParameterAmountAlert().FirstOrDefault().AmountAlertPorcentage);

                    var result = new AdditionalVehicleFuelByGroupGrid
                    {
                        //List = dba.ExecuteReader<AdditionalFuelDistribution>("[Control].[Sp_AdditionalVehicleFuelLowPorcentage_Retrieve]",
                        List = dba.ExecuteReader<AdditionalFuelDistribution>("[Control].[Sp_AdditionalDriverFuel_Retrieve]",
                         new
                         {
                             Year = model.Year,
                             Month = model.Month,
                             Porcent = porcent,
                             CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                             UserId = user.UserId,
                             IsPorcent = true
                         })
                    };
                    return result;
                }
            }
        }

        /// <summary>
        /// Performs Alarm Credit Count
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public int AlarmCreditCountRetrieve(int pMinPct)
        {
            int Year = DateTime.Today.Year;
            int Month = DateTime.Today.Month;

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Control].[Sp_AlarmCreditCount_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(), 
                        Year = Year,
                        Month = Month,
                        MinPct = pMinPct,
                        UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public AdditionalVehicleFuelByGroupGrid AddOrEditAdditionalVehicleFuelByGroup(AdditionalVehicleFuelByGroup model, List<double> Ids, List<CostCentersValues> list)
        {
            model.Year = DateTime.Today.Year;

            IList<FuelsByCredit> fuels;
            using (var dba = new DataBaseAccess())
            {
                fuels = dba.ExecuteReader<FuelsByCredit>("[Control].[Sp_FuelsByCredit_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    });
            }
            var aux = ExecuteAddOrEditAdditionalVehicleFuelByGroup(model, fuels, Ids, null);
            var result = RetrieveAdditionalVehicleFuelByGroupList(model);
            foreach (var item in result.List)
            {
                var temp = aux.FirstOrDefault(x => x.VehicleId == item.VehicleId);
                item.Result = temp != null ? temp.Result : -100;
                if (Ids != null)
                {
                    if (!Ids.Contains((int)item.VehicleId))
                    {
                        item.Result = 101;
                    }
                }

            }
            result.IsResult = true;
            result.CreditCardEmissionType = ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId;
            return result;
        }


        /// <summary>
        /// Execute Add Or Edit Vehicle Fuel By Group
        /// </summary>
        /// <param name="model">Vehicle Fuel By Group</param>
        /// <param name="fuels">List of Fuels By Credit </param>
        /// <returns>List of Vehicle Fuel By Group</returns>
        private IList<AdditionalFuelDistribution> ExecuteAddOrEditAdditionalVehicleFuelByGroup(AdditionalVehicleFuelByGroup model, IList<FuelsByCredit> fuels, List<double> Ids, List<CostCentersValues> list)
        {
            int customerCapacityUnit = ECOsystem.Utilities.Session.GetCustomerCapacityUnitId();
            //var aux = ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 101? RetrieveAdditionalVehicleFuelByGroupList(model).List.ToList() : RetrieveAdditionalDriverFuelByGroupList(model).List.ToList();
            var aux = RetrieveAdditionalFuel(model).List;
            foreach (var item in aux)
            {
                if (Ids == null)
                {
                    model.Year = DateTime.Today.Year;
                    model.Month = DateTime.Today.Month;
                   
                    var literPrice = fuels.Where(x => x.FuelId == item.DefaultFuelId && x.Month == model.Month && x.Year == model.Year ).Select(x => x.LiterPrice).FirstOrDefault();
                    literPrice = (literPrice != null) ?
                                        literPrice : fuels.Where(x => x.FuelId == item.DefaultFuelId)
                                                          .OrderByDescending(o => o.Month)
                                                          .OrderByDescending(o => o.Year)
                                                          .Select(x => x.LiterPrice).FirstOrDefault();
                    if (list != null)
                    {
                        item.Amount = list.Where(x => x.VehicleId == item.VehicleId).Select(x => x.Value).FirstOrDefault();
                        item.Liters = (item.Amount / literPrice);
                    }
                    else
                    {
                        if (model.ValueType.Equals("CAPACITYUNIT"))
                        {
                            int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
                            item.Liters = ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(customerCapacityUnit, litersCapacityUnitId, model.Value);
                            item.Amount = item.Liters * literPrice;
                        }
                        else if (model.ValueType.Equals("AMOUNT"))
                        {
                            item.Amount = model.Value;
                            item.Liters = (item.Amount / literPrice);
                        }
                        else
                        {
                            item.Result = -1000;
                            continue;
                        }
                    }

                    var sqlResult = AddOrEditAdditionalVehicleFuelByGroup(new AdditionalVehicleFuel
                    {
                        AdditionalId = item.AdditionalId,
                        VehicleId = item.VehicleId,
                        Liters = item.Liters,
                        Amount = item.Amount,
                        Month = model.Month,
                        Year = model.Year,
                        RowVersion = item.RowVersion
                    });
                    item.Result = sqlResult != null ? sqlResult.ErrorNumber : 0;
                }
                else
                {

                    int count = Ids.Count();
                    for (int i = 0; i < Ids.Count(); i++)
                    {
                        if (i % 2 == 0)
                        {
                            if (item.VehicleId == Ids[i])
                            {
                                model.Year = DateTime.Today.Year;
                                model.Month = DateTime.Today.Month;
                                var literPrice = fuels.Where(x => x.FuelId == item.DefaultFuelId && x.Month == model.Month && x.Year == model.Year).Select(x => x.LiterPrice).FirstOrDefault();
                                literPrice = (literPrice != null) ?
                                                    literPrice : fuels.Where(x => x.FuelId == item.DefaultFuelId)
                                                                      .OrderByDescending(o => o.Month)
                                                                      .OrderByDescending(o => o.Year)
                                                                      .Select(x => x.LiterPrice).FirstOrDefault();

                                if (item.ConvertAdditionalAmount > Convert.ToInt32(Ids[i + 1]))
                                {
                                    item.Result = -99;
                                }
                                else
                                {                                
                                    if (list != null)
                                    {
                                        item.Amount = list.Where(x => x.VehicleId == item.VehicleId).Select(x => x.Value).FirstOrDefault();
                                        item.Liters = (item.Amount / literPrice);
                                    }
                                    else
                                    {
                                        int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
                                        item.Amount = ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, customerCapacityUnit, Convert.ToInt32(Ids[i + 1]));
                                        item.Liters = (item.Amount / literPrice);
                                    }
                                    var sqlResult = AddOrEditAdditionalVehicleFuelByGroup(new AdditionalVehicleFuel
                                    {
                                        AdditionalId = item.AdditionalId,
                                        VehicleId = item.VehicleId,
                                        Liters = item.Liters,
                                        Amount = item.Amount,
                                        Month = model.Month,
                                        Year = model.Year,
                                        RowVersion = item.RowVersion
                                    });
                                    item.Result = sqlResult != null ? sqlResult.ErrorNumber : 0;
                                }
                            }
                        }
                    }
                }
            }
            return aux;
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleFuel
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public SqlError AddOrEditAdditionalVehicleFuelByGroup(AdditionalVehicleFuel model)
        {
            using (var dba = new DataBaseAccess())
            {
                if (model.DriverId == null)
                {
                    return dba.ExecuteReader<SqlError>("[Control].[Sp_AdditionalVehicleFuelByGroup_AddOrEdit]",
                    new
                    {
                        AdditionalId = model.AdditionalId,
                        VehicleId = model.VehicleId,
                        Liters = model.Liters,
                        Amount = model.Amount,
                        Month = model.Month,
                        Year = model.Year,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        LoggedUserId = model.LoggedUserId,
                        RowVersion = model.RowVersion
                    }).FirstOrDefault();
                }
                else
                {
                    return dba.ExecuteReader<SqlError>("[Control].[Sp_AdditionalDriverFuel_AddOrEdit]",
                    new
                    {
                        AdditionalDriverFuelId = model.AdditionalDriverFuelId,
                        DriverId = model.DriverId,
                        Amount = model.Amount,                        
                        Month = model.Month,
                        Year = model.Year,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        LoggedUserId = model.LoggedUserId,
                    }).FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Retrieve List of Vehicle Fuel By Group
        /// </summary>
        /// <param name="model">Vehicle Fuel By Group model</param>
        /// <returns>AdditionalVehicleFuelByGroupGrid Model to render view</returns>
        public AdditionalVehicleFuelByGroupGrid RetrieveAdditionalVehicleFuelByGroupList(AdditionalVehicleFuelByGroup model)
        {
            model.Year = DateTime.Today.Year;
            model.Month = DateTime.Today.Month;
            var user = ECOsystem.Utilities.Session.GetUserInfo();
            using (var dba = new DataBaseAccess())
            {
                if (model.VehicleGroupId != null)
                {
                    var result = new AdditionalVehicleFuelByGroupGrid
                {
                    List = dba.ExecuteReader<AdditionalFuelDistribution>("[Control].[Sp_VehicleAdditionalFuelByGroup_Retrieve]",
                    new
                    {
                        model.VehicleGroupId,
                        model.Year,
                        model.Month,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        user.UserId
                    })
                };
                return result;
                }

                else if (model.CostCenterId != null)
                {
                    var result = new AdditionalVehicleFuelByGroupGrid
                    {

                        List = dba.ExecuteReader<AdditionalFuelDistribution>("[Control].[Sp_CostCentersAdditionalByCredit_Retrieve]",
                         new
                         {
                             model.CostCenterId,
                             model.Year,
                             model.Month,
                             CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                             user.UserId
                         })
                    };
                    return result;
                }

                else if (model.VehicleId != null)
                {
                    var result = new AdditionalVehicleFuelByGroupGrid
                    {
                        List = dba.ExecuteReader<AdditionalFuelDistribution>("[Control].[Sp_AdditionalVehicleFuel_Retrieve]",
                         new
                         {
                             VehicleId = model.VehicleId,
                             model.Year,
                             model.Month,
                             CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                             user.UserId
                         })
                    };
                    return result;
                }
                else 
                {
                    int porcent = 0;

                    porcent = Convert.ToInt32(RetrieveParameterAmountAlert().FirstOrDefault().AmountAlertPorcentage);
                    
                    var result = new AdditionalVehicleFuelByGroupGrid
                    {
                        List = dba.ExecuteReader<AdditionalFuelDistribution>("[Control].[Sp_AdditionalVehicleFuelLowPorcentage_Retrieve]",
                         new
                         {
                             Year = model.Year,
                             Month = model.Month,
                             Porcent = porcent,
                             CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                             UserId = user.UserId,
                         })
                    };
                    return result;                
                }
            }
        }

        /// <summary>
        /// Retrieve Parameters
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Parameters> RetrieveParameterAmountAlert()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Parameters>("[General].[Sp_Parameters_Retrieve]", null);
            }
        }

        public AdditionalVehicleFuelByGroupGrid AddOrEditAdditionalVehicleFuelByGroup(AdditionalVehicleFuelByGroup model, IList<AdditionalFuelDistribution> list)
        {
            model.Year = DateTime.Today.Year;

            IList<FuelsByCredit> fuels;
            using (var dba = new DataBaseAccess())
            {
                fuels = dba.ExecuteReader<FuelsByCredit>("[Control].[Sp_FuelsByCredit_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    });
            }
            var aux = ExecuteAddOrEditAdditionalVehicleFuelByGroup1(model, fuels, list, null);
            //var result = RetrieveAdditionalVehicleFuelByGroupList(model);
            var result = RetrieveAdditionalFuel(model);
            foreach (var item in result.List)
            {
                //var temp = aux.FirstOrDefault(x => x.VehicleId == item.VehicleId);
                //item.Result = temp != null ? temp.Result : -100;

                //int id = (int)item.VehicleId;
                //if (!list.Where(v => v.Selected == true && v.VehicleId == id).ToList().Any())
                //{
                //    item.Result = 101;
                //}

                var temp = item.VehicleId == null ? aux.FirstOrDefault(x => x.DriverId == item.DriverId) : aux.FirstOrDefault(x => x.VehicleId == item.VehicleId);
                item.Result = temp != null ? temp.Result : -100;

                int id = (item.VehicleId == null ? (int)item.DriverId : (int)item.VehicleId);
                if (!list.Where(v => v.Selected == true && (v.VehicleId == id || v.DriverId == id)).ToList().Any())
                {
                    item.Result = 101;
                }
            }
            result.IsResult = true;
            result.CreditCardEmissionType = ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId;
            return result;
        }

        /// <summary>
        /// Execute Add Or Edit Vehicle Fuel By Group
        /// </summary>
        /// <param name="model">Vehicle Fuel By Group</param>
        /// <param name="fuels">List of Fuels By Credit </param>
        /// <returns>List of Vehicle Fuel By Group</returns>
        private IList<AdditionalFuelDistribution> ExecuteAddOrEditAdditionalVehicleFuelByGroup1(AdditionalVehicleFuelByGroup model, IList<FuelsByCredit> fuels, IList<AdditionalFuelDistribution> vehicleList, List<CostCentersValues> list)
        {
            int customerCapacityUnit = ECOsystem.Utilities.Session.GetCustomerCapacityUnitId();
            IList<AdditionalFuelDistribution> aux;
            model.Year = DateTime.Today.Year;
            model.Month = DateTime.Today.Month;

            if (vehicleList == null)
            {
                //aux = RetrieveAdditionalVehicleFuelByGroupList(model).List;
                aux = RetrieveAdditionalFuel(model).List;

                aux.ToList().ForEach(vehicle =>
                {
                    decimal? literPrice = GetLiterPrice(model, fuels, vehicle);

                    if (list != null)
                    {
                        vehicle.Amount = list.Where(x => x.VehicleId == vehicle.VehicleId).Select(x => x.Value).FirstOrDefault();
                        vehicle.Liters = (vehicle.Amount / literPrice);
                    }
                    else
                    {
                        if (model.ValueType.Equals("CAPACITYUNIT"))
                        {
                            int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
                            vehicle.Liters = ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(customerCapacityUnit, litersCapacityUnitId, model.Value);
                            vehicle.Amount = vehicle.Liters * literPrice;
                        }
                        else if (model.ValueType.Equals("AMOUNT"))
                        {
                            vehicle.Amount = model.Value;
                            vehicle.Liters = (vehicle.Amount / literPrice);
                        }
                        else
                        {
                            vehicle.Result = -1000;
                        }
                    }

                    if (vehicle.Result != -1000)
                    {
                        var sqlResult = DoAddOrEditAdditionalVehicleFuelByGroup(model, vehicle);
                        vehicle.Result = sqlResult != null ? sqlResult.ErrorNumber : 0;
                    }
                });
            }
            else
            {
                aux = vehicleList;
                vehicleList.Where(v => v.Selected == true).ToList().ForEach(vehicle =>
                {
                    if (vehicle.VehicleId != null)
                    {
                        if (list != null)
                        {
                            decimal? literPrice = GetLiterPrice(model, fuels, vehicle);
                            vehicle.AdditionalAmount = list.Where(x => x.VehicleId == vehicle.VehicleId).Select(x => x.Value).FirstOrDefault();
                            vehicle.AdditionalLiters = (vehicle.AdditionalAmount / literPrice);
                        }
                    }

                    SqlError sqlResult = DoAddOrEditAdditionalVehicleFuelByGroup(model, vehicle);
                    vehicle.Result = sqlResult != null ? sqlResult.ErrorNumber : 0;
                });
            }
            return aux;
        }

        private AdditionalVehicleFuelByGroupGrid RetrieveAdditionalFuel(AdditionalVehicleFuelByGroup model)
        {
            return ECOsystem.Utilities.Session.GetCustomerInfo().IssueForId == 101 ? RetrieveAdditionalVehicleFuelByGroupList(model) : RetrieveAdditionalDriverFuelByGroupList(model);
        }

        private SqlError DoAddOrEditAdditionalVehicleFuelByGroup(AdditionalVehicleFuelByGroup model, AdditionalFuelDistribution vehicle)
        {
            return AddOrEditAdditionalVehicleFuelByGroup(new AdditionalVehicleFuel
            {
                AdditionalId = vehicle.AdditionalId,
                AdditionalDriverFuelId = vehicle.AdditionalDriverFuelId,
                VehicleId = vehicle.VehicleId,
                Liters = vehicle.AdditionalLiters,
                Amount = vehicle.AdditionalAmount,
                Month = model.Month,
                Year = model.Year,
                RowVersion = vehicle.RowVersion,
                DriverId = vehicle.DriverId
            });
        }

        private static decimal? GetLiterPrice(AdditionalVehicleFuelByGroup model, IList<FuelsByCredit> fuels, AdditionalFuelDistribution vehicle)
        {
            var literPrice = fuels.Where(x => x.FuelId == vehicle.DefaultFuelId && x.Month == model.Month && x.Year == model.Year).Select(x => x.LiterPrice).FirstOrDefault();
            literPrice = (literPrice != null) ?
                                literPrice : fuels.Where(x => x.FuelId == vehicle.DefaultFuelId)
                                                  .OrderByDescending(o => o.Month)
                                                  .OrderByDescending(o => o.Year)
                                                  .Select(x => x.LiterPrice).FirstOrDefault();
            return literPrice;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
