﻿/************************************************************************************************************
*  File    : TimeSlotByVehicleBusiness.cs
*  Summary : TimeSlotByVehicle Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 12/04/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using System.Collections.Generic;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// TimeSlotByVehicle Class
    /// </summary>
    public class TimeSlotByVehicleBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve TimeSlotByVehicle
        /// </summary>
        /// <param name="vehicleId">The vehicle Id to find all vehicles associated</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public TimeSlotByVehicle RetrieveTimeSlotByVehicle(int? vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<TimeSlotByVehicle>("[General].[Sp_TimeSlotByVehicle_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId
                    }).FirstOrDefault();

                return result ?? new TimeSlotByVehicle { VehicleId = vehicleId };
            }
        }

        /// <summary>
        /// Retrieve Validate TimeSlotByVehicle
        /// </summary>
        /// <param name="vehicleId">The vehicle Id to find all vehicles associated</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public ValidateTimeSlotByVehicle RetrieveValidateTimeSlotByVehicle(int? vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<ValidateTimeSlotByVehicle>("[General].[Sp_ValidateTimeSlotByVehicle_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId
                    }).FirstOrDefault();

                return result;
            }
        }

        /// <summary>
        /// RetrieveRangeValidateTimeSlotByVehicle
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public IEnumerable<ValidateTimeSlotByVehicle> RetrieveRangeValidateTimeSlotByVehicle(int? vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ValidateTimeSlotByVehicle>("[General].[Sp_ValidateRangeTimeSlotByVehicle_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity TimeSlotByVehicle
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditTimeSlotByVehicle(TimeSlotByVehicle model)
        {
            var js = new JavaScriptSerializer();
            var TimeSlot = js.Deserialize<TimeSlotByVehicleJson[]>(model.JsonTimeSlot);

            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            foreach (var item in TimeSlot)
            {
                root.Add(new XElement("TimeSlot", 
                    new XAttribute("VehicleId", "" + model.VehicleId),
                    new XAttribute("Time", item.id.Substring(0,2)+":"+item.id.Substring(2,2)),
                    new XAttribute("Day", item.id.Substring(4,2))));
            }
            model.XmlTimeSlot = doc.ToString();
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_TimeSlotByVehicle_AddOrEdit]",
                new
                {
                    model.VehicleId,
                    model.JsonTimeSlot,
                    model.XmlTimeSlot,
                    model.LoggedUserId,
                    model.RowVersion
                });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model TimeSlotByVehicle
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteTimeSlotByVehicle(int vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_TimeSlotByVehicle_Delete]",
                    new { VehicleId = vehicleId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}