﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  ECOsystem.Areas.Control.Models;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.DataAccess;
using System.Data;

namespace  ECOsystem.Areas.Control.Business
{
    public class ExportableTransactionsFileReportBusiness : IDisposable
    {
        public IEnumerable<ExportableTransactionsFileReportModels> ReportDataRetrieve(ControlFuelsReportsBase model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ExportableTransactionsFileReportModels>("[Control].[Sp_ExportableTransactionsFileReport_Retrieve]", new
                {
                    model.Month,
                    model.Year,
                    model.StartDate,
                    model.EndDate,
                    model.CostCenterId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public DataTable GetReportData(ControlFuelsReportsBase model)
        {
            var data = ReportDataRetrieve(model);

            using (var dt = new DataTable())
            {
                dt.Columns.Add("Proveedor");
                dt.Columns.Add("Numero");
                dt.Columns.Add("TipoDocumento");
                dt.Columns.Add("FechaDocumento");
                dt.Columns.Add("FechaRige");
                dt.Columns.Add("Aplicacion");
                dt.Columns.Add("Monto");
                dt.Columns.Add("Subtotal");
                dt.Columns.Add("Moneda");
                dt.Columns.Add("FechaVence");
                dt.Columns.Add("TipoAsiento");
                dt.Columns.Add("Paquete");
                dt.Columns.Add("SubTipo");

                foreach (var item in data)
                {
                    var dr = dt.NewRow();
                    dr["Proveedor"] = item.Proveedor;
                    dr["Numero"] = item.Numero;
                    dr["TipoDocumento"] = item.TipoDocumento;
                    dr["FechaDocumento"] = item.FechaDocumento.ToString("dd/MM/yyyy");
                    dr["FechaRige"] = item.FechaRige.ToString("dd/MM/yyyy"); 
                    dr["Aplicacion"] = item.Aplicacion;
                    dr["Monto"] = item.Monto;
                    dr["Subtotal"] = item.Subtotal;
                    dr["Moneda"] = item.Moneda;
                    dr["FechaVence"] = item.FechaVence.ToString("dd/MM/yyyy");
                    dr["TipoAsiento"] = item.TipoAsiento;
                    dr["Paquete"] = item.Paquete;
                    dr["SubTipo"] = item.SubTipo;

                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}