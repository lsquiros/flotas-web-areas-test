﻿using  ECOsystem.Areas.Control.Models;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Control.Business
{
    public class TransactionsExportSapReportBusiness : IDisposable
    {
        public IEnumerable<TransactionsExportSapReport> ReportDataRetrieve(ControlFuelsReportsBase model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<TransactionsExportSapReport>("[Control].[Sp_TransactionsExportSAPReport_Retrieve]", new
                {
                    model.Month,
                    model.Year,
                    model.StartDate,
                    model.EndDate,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }
        
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}