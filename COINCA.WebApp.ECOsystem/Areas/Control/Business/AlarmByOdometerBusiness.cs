﻿/************************************************************************************************************
*  File    : AlarmByOdometerBusiness.cs
*  Summary : Alarm By Odometer Business Methods
*  Author  : Cristian Martínez H.
*  Date    : 21/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Control.Models.Control;
using ECOsystem.Utilities;

namespace  ECOsystem.Areas.Control.Business.Control
{
    /// <summary>
    /// Alarm By Odometer Business
    /// </summary>
    public class AlarmByOdometerBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Alarm By Odometer
        /// </summary>
        /// <param name="alarmOdometerId">The Primary Key uniquely that identifies each alarm</param>
        /// <param name="key">Key is the search</param>
        /// <returns>IEnumerable collection of the AlarmByOdometer entity model</returns>
        public IEnumerable<AlarmByOdometer> RetrieveAlarmByOdometer(int? alarmOdometerId, string key = null)
        {
            using (var dba = new DataBaseAccess()) {
                return dba.ExecuteReader<AlarmByOdometer>("[Control].[Sp_AlarmByOdometer_Retrieve]", 
                    new {
                        CustomerId = Session.GetCustomerId(),
                        AlarmOdometerId = alarmOdometerId,
                        Key = key
                    });
            }
        }
        
        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Alarm By Odometers 
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="date"> Date of register</param>
        /// <param name="expectedOdometer"></param>
        /// <param name="reportedOdometer"></param>
        /// <param name="valFixed"></param>
        /// <param name="userId"></param>
        public void AddOrEditAlarmByOdometers(int? vehicleId, DateTime date, decimal? expectedOdometer, decimal? reportedOdometer, bool valFixed, int? userId)
        {
            using (var dba = new DataBaseAccess()) {
                dba.ExecuteNonQuery("[Control].[Sp_AlarmByOdometer_AddOrEdit]", 
                    new {
                        VehicleId = vehicleId, 
                        Date = date, 
                        ExpectedOdometer = expectedOdometer, 
                        ReportedOdometer = reportedOdometer,
                        Fixed = valFixed,
                        LoggedUserId = userId
                    });
            }
        }

        /// <summary>
        /// Changes the new value of odometer and disable the state of alarm by odometer
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform update the value of odometer</param>
        public void DisabledAlarmByOdometer(AlarmByOdometer model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_AlarmByOdometer_Disabled]", 
                    new { 
                         model.AlarmOdometerId,
                         model.VehicleId,
                         model.NewOdometer
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}