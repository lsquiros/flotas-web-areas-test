﻿var ECO_Control = ECO_Control || {};

ECO_Control.VehicleFuel = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        //Create Vehicle Fuel Modal
        onClickNew();

        var select2 = $("#KeyId").select2({
            formatResult: vehicleSelectFormat,
            formatSelection: vehicleSelectFormat,
            escapeMarkup: function (m) { return m; }
        }).data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectChange(data);
                    return fn.apply(this, arguments);
                }
            }
        })(select2.onSelect);


        var formContent = $('#detailContainer');
        formContent.off('click.btnSubmit', 'button[type="submit"]').on('click.btnSubmit', 'button[type="submit"]', function (e) {
            $('#detailContainer').find('#VehicleId').val($('#KeyId').val());
        });

        formContent.find('#Liters').numeric("integer");
        formContent.find('#CapacityUnitValue').numeric({
            allowMinus: false,
            maxDecimalPlaces: 5
        });
        formContent.find('#Year').numeric("integer");

        formContent.find('#AmountStr').numeric({
            allowMinus: false,
            maxDecimalPlaces: 2
        });

        formContent.off('blur.inputOnBlur', 'input[DataRefresh="true"]').on('blur.inputOnBlur', 'input[DataRefresh="true"]', inputOnBlur);

        formContent.off('blur.litersOnBlur', '#Liters').on('blur.litersOnBlur', '#Liters', function () {            
            var literPrice = toFloat($('#LiterPrice').val());
            var liters = toFloat($('#LiterPrice').val());
        });


     

    };

    /* On click BtnNew*/
    var onClickNew = function () {
        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });
    };

    /*format Number*/
    var formatNum = function (num) {
        return num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    /*Convert str to Float*/
    var toFloat = function (num) {
        num = ("" + num).replace(/,/g, '');
        return isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
    }

    /*Convert str to Number*/
    var toNum = function (num) {
        num = ("" + num).replace(/\./g, '').replace(/,/g, '.');
        return isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
    }

    /*Round Number */
    var round = function (value, decimals) {
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    }

    /* Vehicle Select Format*/
    var vehicleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-4">' + json.Name + '</div>' +
                '<div class="col-md-6">' + json.CostCenterName + '</div>' +
                '<div class="col-md-2">' + json.CategoryType + '</div>' +
                '</div>';
    };

    /*on Select Change*/
    var onSelectChange = function (obj) {
        if (obj.id === '') {
            $('#VehicleFuelDetail').html('');
        } else {
            $("#KeyId").val(obj.id);
            $('#ReloadForm').submit();
        }
    };

    /* input OnBlur */
    var inputOnBlur = function (e) {
     
        inputRefresh(this);
    }

    /* input OnBlur Validation */
    var inputRefresh = function (obj) {
  
        var value = 0.0;
        var conversionValue = 0;
        var literPrice = 0;

        if (obj.id === 'AmountStr') {
            try {
                value = toNum($(obj).val());
                literPrice = toFloat($('#LiterPrice').val());
              
                $('#Amount').val(value);
                $(obj).val(formatNum(value));
             
                if ($('#CapacityUnitId').val() == '0') {
                    var resultValue = isNaN((value / literPrice)) ? 0.0 : (value / literPrice);
                    var resultValueStr = formatNum(resultValue);

                    $('#CapacityUnitValue').val(resultValue);
                    $('#CapacityUnitValueStr').val(resultValueStr);
                } else {
                 
                    conversionValue = toFloat($('#ConversionValue').val());
                    var resultValue = isNaN(((value / literPrice) / conversionValue)) ? 0.0 : (value / literPrice) / conversionValue;
                    var resultValueStr = formatNum(resultValue);

                    $('#CapacityUnitValue').val(resultValue);
                    $('#CapacityUnitValueStr').val(resultValueStr);
                }
            } catch (e) {
                var err = e;
            }
            return;
        }

        if (obj.id === 'CapacityUnitValueStr') {
            try {
           
                value = toNum($(obj).val());
                literPrice = toFloat($('#LiterPrice').val());
                //$('#CapacityUnityValue').val(value);
                $('#CapacityUnitValue').val(value);
                $(obj).val(formatNum(value));
                //alert('literPrice ' + literPrice);
                if ($('#CapacityUnitId').val() == '0') {
                    $('#AmountStr').val(formatNum(value * literPrice));
                    $('#Amount').val(value * literPrice);
                } else {
                    conversionValue = toFloat($('#ConversionValue').val());
                    $('#AmountStr').val(formatNum((value * conversionValue) * literPrice));
                    $('#Amount').val((value * conversionValue) * literPrice);
                }
            } catch (e) {

            }
            return;
        }
    }
    /* Public methods */
    return {
        Init: initialize
    };
})();

