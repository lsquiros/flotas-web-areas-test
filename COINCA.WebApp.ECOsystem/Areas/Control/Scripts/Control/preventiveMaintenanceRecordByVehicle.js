﻿var ECO_Control = ECO_Control || {};

ECO_Control.PreventiveMaintenanceRecordByVehicle = (function () {
    var options = {};

    /*initialize function*/
    var initialize = function (opts) {
        $.extend(options, opts);

        var select1 = $("#PreventiveMaintenanceCatalogId").select2(
        { formatResult: maintenanceCatalogFormat, formatSelection: maintenanceCatalogFormat }).data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);        

        $("#LastReview").numeric("integer");
        resetAndInitCustomControls();
        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            $("#PreventiveMaintenanceCatalogIdSelect").val($("#PreventiveMaintenanceCatalogId").val());
            $('#PlateIdSelect').val($('#idSearch').val());
            $('#btnAddAlarm').addClass('hide');
            detailContainerSelector($('#FrequencyTypeId').val(), true);
        });

    };

    var addOrEditModalShow = function () {
        $('#btnAddAlarm').removeClass('hide');
        $("#addOrEditForm").find('select').not("[custom='true']").select2();
        $('#addOrEditModal').modal('show');
        $("#LastReview").numeric("integer");
        $("#PreventiveMaintenanceCatalogIdSelect").val($("#PreventiveMaintenanceCatalogId").val());
        detailContainerSelector($('#FrequencyTypeId').val(), false);
    }

    function detailContainerSelector(FrequencyTypeId, IsEdit){
        switch (FrequencyTypeId) {
            case '300':
                $('#FirstFrequencyType').removeClass('hide');
                $('#SecondFrequencyType').addClass('hide');
                $('#ThirdFrequencyType').addClass('hide');
                $('#Month').val(null);
                $('#Year').val(null);
                break;
            case '301':
                $('#SecondFrequencyType').removeClass('hide');
                $('#FirstFrequencyType').addClass('hide');
                $('#ThirdFrequencyType').addClass('hide');
                $("#dropdownYear").empty();
                var now = new Date();
                now.setMonth(now.getMonth() - $('#Frequency').val());
                for (var i = now.getFullYear() ; i <= (new Date()).getFullYear() ; i++) {
                    $("#dropdownYear").append("<option value=\'" + i + "\' selected>" + i + "</option>");
                }
                $("#dropdownYear").val($('#Year').val());
                var select3 = $("#dropdownYear").select2().data('select2');
                select3.onSelect = (function (fn) {
                    return function (data, opts) {
                        var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectChangeYear(data); return fn.apply(this, arguments); }
                    }
                })(select3.onSelect);
                var select4 = $("#dropdownMonth").select2().data('select2');
                select4.onSelect = (function (fn) {
                    return function (data, opts) {
                        var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectChangeMonth(data); return fn.apply(this, arguments); }
                    }
                })(select4.onSelect);
                break;
            case '302':
                $('#DatePreventiveMaintenance').val($('#Frequency').val());
                var dateComplete = $('#DatePreventiveMaintenance').val();
                var dateSlipt = dateComplete.split('/');
                $('#Day').val(dateSlipt[0]);
                $('#Month').val(dateSlipt[1]);
                $('#Year').val(dateSlipt[2]);
                $('#FirstFrequencyType').addClass('hide');
                $('#SecondFrequencyType').addClass('hide');
                $('#ThirdFrequencyType').removeClass('hide');
                break;
        }
        resetAndInitCustomControls();
    }


    /* Maintenance Catalog Select Format*/
    var maintenanceCatalogFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-8">' + json.Description + '</div>' +
                '<div class="col-md-4">' + json.Frequency + ' ' + json.FrequencyName + '</div>' +
                '</div>';
    };          

    var resetAndInitCustomControls = function () {
        $('#VehicleId').select2('data', {});
        $('#VehicleId').select2({
            formatResult: vehicleCategorySelectFormat,
            formatSelection: vehicleCategorySelectFormat,
            escapeMarkup: function (m) { return m; }
        });
    }

    /* vehicle Category Select Format */
    var vehicleCategorySelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-6">' + json.Name + '</div>' +
                '<div class="col-md-3">' + json.CategoryType + '</div>' +
                '<div class="col-md-3">' + json.FuelName + '</div>' +
                '</div>';
    };

    var onSelectChangeYear = function (obj) {
        $('#Year').val(obj.id);
    }

    var onSelectChangeMonth = function (obj) {
        $('#Month').val(obj.id);
    }

    var onSelectChange = function (obj) {
        if (obj.id === '') {
            $('#gridContainer').html('');
            $('#btnAdd').addClass('hide');
        } else {
            $('#idSearch').val(obj.id);
            $('#idLoad').val(obj.id);
            $('#idSearchDelete').val(obj.id);
            $('#idDelete').val(obj.id);
            $('#btnAdd').removeClass('hide');
            var json = null;
            try { json = JSON.parse(obj.text.replace(/\t/g, '').replace(/'/g, "\"")); } catch (e) { }
            if (json !== null) {
                $('#Frequency').val(json.Frequency);
                $('#FrequencyTypeId').val(json.FrequencyTypeId);
            }
            $('#searchForm').submit();            
        }
    };

    /*Public methods*/
    return {
        Init: initialize,
        AddOrEditModalShow: addOrEditModalShow
    };
})();