﻿var ECO_Control = ECO_Control || {};

ECO_Control.Vehicles = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        $('#Year').numeric("integer");
        $("#FreightTemperature").numeric("integer");
        $("#Cost").numeric("integer");
        $("#FreightTemperatureThreshold1").numeric("integer");
        $("#FreightTemperatureThreshold2").numeric("integer");
       
        $('#datetimepicker1').datepicker({ language: 'es', minDate: new Date(), format: 'dd/mm/yyyy', autoclose: true, todayHighlight: true });

        resetAndInitCustomControls();

        //Create Vehicle Modal
        onClickNew();

        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            resetAndInitCustomControls();
        });

        $('#gridContainer').off('click.ref_row', 'a[ref_row]').on('click.ref_row', 'a[ref_row]', function (e) {
            $('#loadRefForm').find('#id').val($(this).attr('id'));
            $('#loadRefForm').submit();
        });

    };

    /* Reset And Init Custom Select*/
    var resetAndInitCustomControls = function () {
        $('#UserId').select2('data', {});
        $('#UserId').select2({
            //minimumInputLength: 3,
            formatResult: driverSelectFormat,
            formatSelection: driverSelectedFormat,
            escapeMarkup: function (m) { return m; },
            matcher: driverSearch,
            allowClear: true
        });

        $('#VehicleCategoryId').select2('data', {});
        $('#VehicleCategoryId').select2({
            formatResult: vehicleCategorySelectFormat,
            formatSelection: vehicleCategorySelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        /*var hexColor = $('#Colour').val().length > 0 ? $('#Colour').val() : "#ff8800";
        $('#ColourBox').colpick({
            layout: 'hex',
            color: hexColor.replace(/#/, ''),
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).css('background-color', '#' + hex);
                $(el).colpickHide();
                $('#Colour').val('#' + hex);
            }
        }).css('background-color', hexColor);*/
    };
    
    /* On click BtnNew*/
    var onClickNew = function () {
        $('#btnAdd').off('click.btnAddNew').on('click.btnAddNew', function (e) {

            ECOsystem.Utilities.ShowLoader();

            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });
    };

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    /* On Success Reference Load after call edit*/
    var onSuccessReferenceLoad = function () {
        $("#referenceEditForm").find('select').not("[custom='true']").select2();
        $('#referenceEditModal').modal('show');
    };

    var referenceEditModalClose = function () {
        $('#referenceEditModal').modal('hide');
    };

    /* driver Select Format*/
    var vehicleCategorySelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-3">' + json.Manufacturer + '</div>' +
                '<div class="col-md-2">' + json.VehicleModel + '</div>' +
                '<div class="col-md-1">' + json.Year + '</div>' +
                '<div class="col-md-2">' + json.Type + '</div>' +
                '<div class="col-md-3">' + json.FuelName + '</div>' +
                '<div class="col-md-1">' + json.Liters + '</div>' +
                '</div>';
    };

    /* driver Select Format*/
    var driverSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                //'<div class="col-md-1"><img style="width: 48px; height: 48px;" src="' + json.Photo + '"/></div>' +
                '<div class="col-md-2">&nbsp;&nbsp;' + json.Code + '</div>' +
                '<div class="col-md-2">' + json.Identification + '</div>' +
                '<div class="col-md-7">' + json.Name + '</div>' +
                '</div>';
    };

    /* driver Selected Format*/
    var driverSelectedFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/'/g, "\""));
        return '<div class="row">' +
                //'<div class="col-md-1"><img style="width: 24px; height: 24px;" src="' + json.Photo + '"/></div>' +
                '<div class="col-md-2">&nbsp;&nbsp;' + json.Code + '</div>' +
                '<div class="col-md-2">' + json.Identification + '</div>' +
                '<div class="col-md-7">' + json.Name + '</div>' +
                '</div>';
    };

    /*driver search*/
    var driverSearch = function (term, text) {

        var obj = null;
        try { obj = JSON.parse(text.replace(/\t/g, '').replace(/'/g, "\"")); } catch (e) { }
        if (obj === null) { return false; }
        text = obj.Name + ' ' + obj.Code + ' ' + obj.Identification;
                
        var terms = term.split(" ");
        for (var i = 0; i < terms.length; i++) {
            var tester = new RegExp("\\b" + terms[i], 'i');
            if (tester.test(text) == false) {
                return (text === 'Other');
            }
        }
        return true;
    }

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        OnSuccessReferenceLoad: onSuccessReferenceLoad,
        ReferenceEditModalClose: referenceEditModalClose
    };
})();

