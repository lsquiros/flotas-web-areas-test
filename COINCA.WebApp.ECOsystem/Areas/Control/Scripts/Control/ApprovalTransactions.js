﻿var ECO_Control = ECO_Control || {};

ECO_Control.ApprovalTransactions = (function () {
    var options = {};
    var activeControl = null;

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);       
        initializeDropDownList();

        onClickbtnAppliesMassive();

        // MAKE DATEPICKER
        function makePicker(element) {
            element.datepicker({
                language: 'es',
                defaultDate: 'now',
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: false
            });
        }
        makePicker($('#StartDateStr'));
        makePicker($('#EndDateStr'));

        $('#btnsearch').click(function () {
            $('body').loader('show');
            $('#SearchForm').find('#key').val($("#Parameters_SearchText").val());
            $('#SearchForm').submit();
        });

        $('#Parameters_SearchText').keypress(function (e) {
            if (e.which == 13) {
                $('body').loader('show');
                $('#SearchForm').find('#key').val($("#Parameters_SearchText").val());
                $('#SearchForm').submit();
            }
        });

        $(".txtInvoice").on("change", function () {
            var field = $(this);  
            var id = $(this).attr("id").split("_")[1];
            var invoice = $(this).val();
            $('body').loader('show');

            setActiveControl($(this).attr("id"));

            $('#changeTransactionForm').find('#transactionid').val(id);
            $('#changeTransactionForm').find('#invoice').val(invoice);
            $('#changeTransactionForm').find('#status').val(null);
            $('#changeTransactionForm').find('#costcenterid').val(null);
            $('#changeTransactionForm').find('#servicestationid').val(null);
            $('#changeTransactionForm').submit();
        });

        $('.servicestationlist').on("change", function () {
            var field = $(this);
            var id = $(this).attr("id").split("_")[1];
            var servicestationid = $(this).val();
            $('body').loader('show');

            setActiveControl($(this).attr("id"));

            $('#changeTransactionForm').find('#transactionid').val(id);
            $('#changeTransactionForm').find('#status').val(null);
            $('#changeTransactionForm').find('#costcenterid').val(null);
            $('#changeTransactionForm').find('#servicestationid').val(servicestationid);
            $('#changeTransactionForm').find('#invoice').val(null);
            $('#changeTransactionForm').submit();
        });

        $('.costcenterlist').on("change", function () {
            var field = $(this);
            var id = $(this).attr("id").split("_")[1];
            var costcenterid = $(this).val();
            $('body').loader('show');

            setActiveControl($(this).attr("id"));

            $('#changeTransactionForm').find('#transactionid').val(id);
            $('#changeTransactionForm').find('#status').val(null);
            $('#changeTransactionForm').find('#costcenterid').val(costcenterid);
            $('#changeTransactionForm').find('#servicestationid').val(null);
            $('#changeTransactionForm').find('#invoice').val(null);
            $('#changeTransactionForm').submit();
        });

        $('.statuslist').on("change", function () {
            var field = $(this);
            var id = $(this).attr("id").split("_")[1];
            var status = $(this).val();
            $('body').loader('show');

            setActiveControl($(this).attr("id"));

            $('#changeTransactionForm').find('#transactionid').val(id);
            $('#changeTransactionForm').find('#status').val(status);
            $('#changeTransactionForm').find('#costcenterid').val(null);
            $('#changeTransactionForm').find('#servicestationid').val(null);
            $('#changeTransactionForm').find('#invoice').val(null);
            $('#changeTransactionForm').submit();
        });

        $(".txtDynamicColumn").on("change", function () {
            var field = $(this);
            var id = $(this).attr("transactionid");
            $('body').loader('show');
            var columnvalues = getColumnValues(id);

            setActiveControl($(this).attr("id"));

            $('#changeTransactionForm').find('#transactionid').val(id);
            $('#changeTransactionForm').find('#invoice').val(null);
            $('#changeTransactionForm').find('#status').val(null);
            $('#changeTransactionForm').find('#costcenterid').val(null);
            $('#changeTransactionForm').find('#servicestationid').val(null);
            $('#changeTransactionForm').find('#columnvalues').val(JSON.stringify(columnvalues));
            $('#changeTransactionForm').submit();
        });


        $('.confirmButton').click(function () {
            getChangesCount();
        });

        $(".PageNumber").click(function () {
            var form = $("#FormName").val();
            var val = $(this).attr("page_number")

            $('#' + form).find('#page').val(val);
            $('#' + form).submit();
        });

        $('#btngetreport').click(function () {
            showloader();
            if (checkAllSelects()) $('#ReloadForm').submit();
        });


        $('#gridContainer').off('change.checkall', '#checkall').on('change.checkall', '#checkall', function () {
            var checkboxes = $(this).closest('table').find('.gridchkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
                $(checkboxes).closest('tr').addClass('grid-row-selected_auth')
            } else {
                checkboxes.prop('checked', false);
                $(checkboxes).closest('tr').addClass('grid-row-selected_auth')
            }
        });

        $('#gridContainer').off('change.gridchkbox', '.gridchkbox').on('change.gridchkbox', '.gridchkbox', function () {
            if ($(this).is(':checked')) {             
                $(this).closest('tr').addClass('grid-row-selected_auth')
            } else {
                $(this).closest('tr').removeClass('grid-row-selected_auth')
            }
        });

        $('#gridContainer').off('click.autorizarTransacciones', '.autorizarTransaccionesBtn').on('click.autorizarTransacciones', '.autorizarTransaccionesBtn', function () {

            var selectedItemsCount = $('input[type=checkbox]:checked').length;
            if (selectedItemsCount > 0) {
                $("#confirmModalactualizar").modal('show');
            }
            else {
                $('#modalverification').modal('show');
                $('#confirmModalactualizar').modal('hide');
            }
        });
    }

    /* On click Btn Applies Massive*/
    var onClickbtnAppliesMassive = function () {        
        $('#btnAppliesMassive').off('click.btnAppliesMassive').on('click.btnAppliesMassive', function (e) {
            var _value = null;
            var rowSelected = $('#gridContainer .grid-row-selected_auth');
            $('#gridContainer .grid-row-selected_auth').each(function (i) {
                $.each(this.cells, function (i) {
                    $(this).find('div').each(function () {
                        var id = $(this).attr('transid');
                        if (id != undefined) {
                            if (_value == null)
                                _value = id;
                            else
                                _value = _value + '|' + id;
                            //return false;
                        }
                        //return false;
                    });
                    //return false;
                });
            });

            $('#applyAllForm2').find('#ChainValues').val(_value);
            $("#applyAllForm2").submit();
        });
    };

    var successAuth = function () {
        $('#confirmModalactualizar').modal('hide');
    }

    var setActiveControl = function (control) {
        activeControl = control;
    }

    var setControlFocus = function () {
        var index = $('.canFocus').index($('#' + activeControl)) + 1;

        if ($('.canFocus').eq(index).attr('id').split('_')[0] == 's2id') {
            $('.canFocus').eq(index).find('.select2-focusser').focus();
        }
        else {
            $('.canFocus').eq(index).focus();
            $('.canFocus').eq(index).select();
        }
    }

    var getColumnValues = function (transactionid) {
        var arr = [];

        $('.DynamicColumn_' + transactionid).each(function () {
            var ApprovalTransactionsParameters = {
                'Id': $(this).attr('ColumnId'),
                'Value': $(this).val()
            };
            arr.push(ApprovalTransactionsParameters);
        });
        return arr;
    }
  
    var initializeDropDownList = function () {
        var select1 = $("#Parameters_Criteria").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        var select4 = $("#Parameters_Status").select2().data('select2');
        select4.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectStatusChange(data); return fn.apply(this, arguments); }
            }
        })(select4.onSelect);
        
        var select5 = $("#Parameters_CostCenterId").select2().data('select2');
        select5.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectCostCenterChange(data); return fn.apply(this, arguments); }
            }
        })(select5.onSelect);        
    };

    var initdropsgrid = function () {
        $('#gridContainer .servicestationlist').each(function () {
            var id = $(this).attr('dropdow');
            var transactionId = $(this).attr('transactionId');
            
            $('#' + id + "_" + transactionId).select2('data', {});
            $('#' + id + "_" + transactionId).select2({
                formatResult: SStationSelectFormat,
                formatSelection: SStationSelectFormat,
                escapeMarkup: function (m) { return m; }
            });
        });

        $('#gridContainer .costcenterlist').each(function () {
            var id = $(this).attr('dropdow');
            var transactionId = $(this).attr('transactionId');

            $('#' + id + "_" + transactionId).select2('data', {});
            $('#' + id + "_" + transactionId).select2({
                escapeMarkup: function (m) { return m; }
            });
        });

        $('#gridContainer .statuslist').each(function () {
            var id = $(this).attr('dropdow');
            var transactionId = $(this).attr('transactionId');

            $('#' + id + "_" + transactionId).select2('data', {});
            $('#' + id + "_" + transactionId).select2({
                escapeMarkup: function (m) { return m; }
            });
        });
    };

    var SStationSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
               '</div>';
    };

    $('#saveButton').click(function () {
        $('body').loader('show');
        $('#tableData').modal('hide');
        $('#SaveEditForm').submit();
    });

    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Parameters_Month").select2('val', '');
            $("#Parameters_Year").select2('val', '');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');

            $('#Parameters_Criteria').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            //if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    var onSelectMonthChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Month').val(obj.id);
            //if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    var onSelectYearChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Year').val(obj.id);
            //if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    var onSelectStatusChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Status').val(obj.id);
            //if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    var onSelectCostCenterChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_CostCenterId').val(obj.id);
            //if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };
    
    var checkAllSelects = function () {
        if (($('#Parameters_ReportCriteria').val() === '')
                || ($('#Parameters_Month').val() === '')
                || ($('#Parameters_Status').val() === '')
                || ($('#Parameters_Year').val() === '')
                || ($('#Parameters_StartDateStr').val() === '')
                || ($('#Parameters_EndDateStr').val() === '')
            ) {
            if (($('#Parameters_Month').val() !== '') && ($('#Parameters_Year').val() !== '') && ($('#Parameters_Status').val() !== ''))
                return true;
            if (($('#Parameters_StartDateStr').val() !== '') && ($('#Parameters_EndDateStr').val() !== '') && ($('#Parameters_Status').val() !== ''))
                return true;
            invalidSelectOption();
            return false;
        }
        return true;
    };

    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                '<span aria-hidden="true">×</span>' +
                                            '</button>' +
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    var getChangesCount = function () {
        $.ajax({
            url: '/ApprovalTransaction/GetCountsToSave',
            type: "POST",
            contentType: "application/json",
            success: function (result) {
                if (result.authCont == 0 && result.changeCount == 0) {
                    $('#confirmationText').html('No hay datos para guardar.');
                    $('#saveButton').hide();
                } else {
                    var text = 'Datos a almacenar: <br/>';
                    if (result.authCont > 0) {
                        text = text + ' - Número de transacciones <strong>Autorizadas</strong>: ' + result.authCont + '<br/>';
                    }
                    if (result.changeCount > 0) {
                        text = text + ' - Número de transacciones <strong>Editadas</strong>: ' + result.changeCount + '<br/>';
                    }
                    $('#confirmationText').html(text + '<br/> ** Las transacciones autorizadas no se podrán modificar una vez guardadas.');
                    $('#saveButton').show();
                }
            }
        });
        $('#tableData').modal('show');
    };
    
    var successsearch = function () {
        initdropsgrid();
        initialize();
        $('body').loader('hide');
    };

    var successchange = function () {
        initdropsgrid();
        initialize();
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
        //$('body').loader('hide');
        $('body').loader('hide', setControlFocus);
    };

    var successsave = function () {
        initdropsgrid();
        initialize();
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
        $('body').loader('hide');
    };

    var successpaging = function () {
        initdropsgrid();
        initialize();
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
        $('body').loader('hide');
    };

    var showloader = function () {
        $('body').loader('show');
    };

    return {
        Init: initialize,
        InitDrop: initdropsgrid,
        SuccessSearch: successsearch,
        SuccessChange: successchange,
        SuccessSave: successsave,
        SuccessPaging: successpaging,
        ShowLoader: showloader,
        SuccessAuth: successAuth
    };
})();