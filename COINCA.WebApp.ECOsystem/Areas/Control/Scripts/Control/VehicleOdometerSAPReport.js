﻿var ECO_Control = ECO_Control || {};

ECO_Control.VehicleOdometerSAPReport = (function () {
    var options = {};
    
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
    }
        
    var initEvents = function () {        
        var select2 = $("#CostCenterId").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
            }
        })(select2.onSelect);
    }

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function (data) {
        $('body').loader('hide');
        if (data > 0){
            $('#DonwloadFileForm').submit();
            $('#shownorecords').addClass('hide');
        } else {
            $('#shownorecords').removeClass('hide');
        }
    };

    return {
        Init: initialize,
        HideLoader: hideLoader,
        ShowLoader: showLoader        
    };
})();