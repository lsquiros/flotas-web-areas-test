﻿var ECO_Control = ECO_Control || {};

ECO_Control.TransactionRules = (function () {
    var options = {};
    var jsonDataArray = [];

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initToggle();
        //ruleActiveChange();
        getParametersData();
        submitChanges();
        toggleChange();        

        //Validates if the value in the margin textbox is a number
        $("#margintxt").keypress(function (e)
        {                       
            var val = $("#margintxt").val();
            val = val + String.fromCharCode(e.charCode);
            val = val.replace(/\,/g, '.');            
            
            if (!$.isNumeric(val)) {
                return false;
            }            
        });

        //Gets the data from the modal and save the parameter in the database
        $('#TRSaveMarginParameter').click(function () {
            saveMaginParameterData();
        });        

        //Verifies if the rule is checked and if that is checked show the times
        $('input[type=checkbox]').each(function () {
            var check = $(this);
            var ruleId = $(this).attr("ruleId");

            if (ruleId == 13) {
                if (check.prop('checked')) {
                    $('#TimeSlotValue').removeClass('hideText');
                }
                else {
                    $('#TimeSlotValue').addClass('hideText');
                }
            }

            if (ruleId == 12) {
                if (check.prop('checked')) {
                    $('#MarginDivText').removeClass('hideText');
                }
                else {
                    $('#MarginDivText').addClass('hideText');
                }
            }
        });

        //Validate the texboxes to receive only numbers and that the max number allowed
        $('.number-only').keypress(function (e) {
            //Get the number in the text
            var hours = $("#TimeSlotValueHours").val();
            var minutes = $("#TimeSlotValueMinutes").val();
            var seconds = $("#TimeSlotValueSeconds").val();

            if (hours.length == 2) hours = '';
            if (minutes.length == 2) minutes = '';
            if (seconds.length == 2) seconds = '';

            if ($(this).attr('id') == 'TimeSlotValueHours') hours = hours + String.fromCharCode(e.charCode);
            if ($(this).attr('id') == 'TimeSlotValueMinutes') minutes = minutes + String.fromCharCode(e.charCode);
            if ($(this).attr('id') == 'TimeSlotValueSeconds') seconds = seconds + String.fromCharCode(e.charCode);

            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) {
                return false;
            }
            else {
                if (hours > 24) {
                    return false;
                }
                else if (minutes >= 60) {
                    return false;
                }
                else if (seconds >= 60) {
                    return false;
                }
            }
        });

        //Set the max length to the textboxes 
        $("#TimeSlotValueHours").attr('maxlength', '2');
        $("#TimeSlotValueMinutes").attr('maxlength', '2');
        $("#TimeSlotValueSeconds").attr('maxlength', '2');

        //*************************************************************************************************
        //Set the values as 2 digits when the inputs lost the focus and it has just one number on it
        $('#TimeSlotValue').off('blur.HoursInputOnBlur', '#TimeSlotValueHours').on('blur.HoursInputOnBlur', '#TimeSlotValueHours', function (e) {
            var obj = $('#TimeSlotValueHours');
            var val = $('#TimeSlotValueHours').val();
            if (val.length == 1) obj.val('0' + val); //if the value in the textbox is just one number, adds 0 at the left
            if (val == '') obj.val('00'); //if the value is nothing, add 00 so we avoid null numbers
        });

        $('#TimeSlotValue').off('blur.MinutesInputOnBlur', '#TimeSlotValueMinutes').on('blur.MinutesInputOnBlur', '#TimeSlotValueMinutes', function (e) {
            var obj = $('#TimeSlotValueMinutes');
            var val = $('#TimeSlotValueMinutes').val();
            if (val.length == 1) obj.val('0' + val); //if the value in the textbox is just one number, adds 0 at the left
            if (val == '') obj.val('00'); //if the value is nothing, add 00 so we avoid null numbers
        });

        $('#TimeSlotValue').off('blur.SecondsInputOnBlur', '#TimeSlotValueSeconds').on('blur.SecondsInputOnBlur', '#TimeSlotValueSeconds', function (e) {
            var obj = $('#TimeSlotValueSeconds');
            var val = $('#TimeSlotValueSeconds').val();
            if (val.length == 1) obj.val('0' + val); //if the value in the textbox is just one number, adds 0 at the left
            if (val == '') obj.val('00'); //if the value is nothing, add 00 so we avoid null numbers
        });
        //*************************************************************************************************
    };

    var initToggle = function () {       
        $('.toggleCheck').bootstrapToggle({
            on: 'Activo',
            off: 'Inactivo'
        });
    };

    var toggleChange = function ()
    {
        $('input[type=checkbox]').change(function () {
            
            var check = $(this);
            var ruleId = $(this).attr("ruleId");      
            var idValue1 = "#" + $("input[ruleId='6']").attr("id");  
            var idValue2 = "#" + $("input[ruleId='1']").attr("id");
                                               
            if (ruleId == 1) {
                if ($(this).prop('checked')) {
                    $(idValue1).bootstrapToggle('off');                
                }
            }
            else if (ruleId == 6)
            {
                if ($(this).prop('checked')) {
                    $(idValue2).bootstrapToggle('off');
                }
            }
            else if (ruleId == 12)
            {
                $.ajax({
                    url: '/TransactionRules/ValidateTankCapacityRule',
                    type: 'POST',
                    data: JSON.stringify({}),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        if (data != 0) {
                            if (check.prop('checked')) {
                                check.bootstrapToggle('off');
                                $('#InformationMessageModal').modal('show');
                                $('#MarginDivText').addClass('hideText');
                            }
                            else {
                                $('#MarginDivText').addClass('hideText');
                            }
                        }
                        else {
                            if (check.prop('checked')) {
                                $('#MarginDivText').removeClass('hideText');
                            }
                            else {
                                $('#MarginDivText').addClass('hideText');
                            }
                        }
                    }
                });
            }
            else if (ruleId == 13)
            {
                if (check.prop('checked'))
                {
                    $('#TimeSlotValue').removeClass('hideText');
                }
                else
                {
                    $('#TimeSlotValue').addClass('hideText');
                }

                DefaultTimeSlotValues();
            }
        });

    };    

    var getValidateResult = function () {
      
    };

    //var ruleActiveChange = function () {
    //    $('.toggleCheck').change(function () {
    //        ruleGetData(obj);
    //    });
    //};

    var ruleGetData = function (obj) {
       
        var ruleId = obj.attr("ruleId");
        var ruleCustomerId = obj.attr("ruleCustomerId");
        var customerId = obj.attr("customerId");
        var ruleActive = obj.prop('checked');

        var idValue = "#" + ruleId + "_" + ruleCustomerId + "_Value";
        var ruleValue = $(idValue).val();

        //var objArray = JSON.parse(jsonDataArray);

        if (findJsonObj(jsonDataArray, "RuleId", ruleId) > 0) {
            //alert(customerId + " > " + ruleId + " > " + ruleCustomerId + " > " + ruleActive);
            removeJsonObj(jsonDataArray, "RuleId", ruleId);
        }

        var data = {
            "CustomerId": customerId,
            "RuleId": ruleId,
            "RuleCustomerId": ruleCustomerId,
            "RuleActive": ruleActive,
            "RuleValue": ruleValue
        }

        jsonDataArray.push(data);
        //jsonDataArray = JSON.stringify(objArray);
    };

    var findJsonObj = function(array, property, value) {

        var find = 0;

        $.each(array, function (index, result) {
            if (result[property] == value) {
                find++;
            }
        });
        return find;
    }

    var removeJsonObj = function (array, property, value) {
        $.each(array, function (index, result) {
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
            }
        });
    }

    var submitChanges = function () {

        $('#confirmRulesModal').modal('hide');

        $("#confirmChanges").click(function () {
            $('#confirmRulesModal').modal('show');
        });

        $("#sendRules").click(function () {
            $('.toggleCheck').each(function () {
                ruleGetData($(this));
            });

            var uri = ECOsystem.Utilities.GetUrl() + '/CreditCard/TransactionRules';
            //Send data to backend
            $.ajax({
                type: "POST",
                url: uri,
                data: JSON.stringify(jsonDataArray),
                success: function (data) {
                    alert("Success!");
                },
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                async:false
                //traditional: true
            });

            SaveTimeSlot();
            saveMaginParameterData();
        });

    };

    var getParametersData = function () {

        $.ajax({
            url: '/TransactionRules/GetParametersData',
            type: 'POST',
            data: JSON.stringify({ }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                //If there is no data in the DB set the default values
                if (data.length == 0) {
                    DefaultTimeSlotValues();
                }
                else
                {
                    //goes through the results from the DB and set the values to each input in the TR
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].ParameterName == 'LimitTankCapacity') {
                            $('#margintxt').val(data[i].Value);
                        }
                        else if (data[i].ParameterName == 'TimeSlotTransactions') {
                            //If there is no value for the time slot transaction rule, set the default values
                            if (data[i].Value == null || data[i].Value == '') {
                                DefaultTimeSlotValues();
                            }
                            else {
                                $('#TimeSlotValueHours').val(data[i].Value.split(':')[0]);
                                $('#TimeSlotValueMinutes').val(data[i].Value.split(':')[1]);
                                $('#TimeSlotValueSeconds').val(data[i].Value.split(':')[2]);
                            }
                        }
                    }
                }
               
            }
        });
    };

    var saveMaginParameterData = function () {
        var val = $('#margintxt').val().replace(/\./g, ',');
        var valZero = val.replace(/\,/g, '.');

        if (val == '' || (valZero * 1) == 0) {
            val = '5,00'
        }

        $.ajax({
            url: '/TransactionRules/ParametersTRAddOrEdit',
            type: 'POST',
            data: JSON.stringify({ Value: val, TRId: 12 }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
            }
        });        
    };

    var SaveTimeSlot = function () {
        //Save the information for the timeslot tr   
        var val = $('#TimeSlotValueHours').val();        
        val = val + ':';        
        val = val + $('#TimeSlotValueMinutes').val();
        val = val + ':';
        val = val + $('#TimeSlotValueSeconds').val();

        if (val == '00:00:00')
        {
            val = '00:15:00'
        }

        $.ajax({
            url: '/TransactionRules/ParametersTRAddOrEdit',
            type: 'POST',
            data: JSON.stringify({ Value: val, TRId: 13 }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {               
            }
        });
    };

    var DefaultTimeSlotValues = function () {
        var hours = $('#TimeSlotValueHours');
        var minutes = $('#TimeSlotValueMinutes');
        var seconds = $('#TimeSlotValueSeconds');

        if (hours.val() == '' && minutes.val() == '' && seconds.val() == '')
        {
            hours.val('00');
            minutes.val('15');
            seconds.val('00');
        }

    };

    /* Public methods */
    return {
        Init: initialize
    };
})();

