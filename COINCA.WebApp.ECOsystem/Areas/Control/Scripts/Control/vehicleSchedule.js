﻿var ECO_Control = ECO_Control || {};

ECO_Control.VehicleSchedule = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        var select2 = $("#VehicleId").select2({
            formatResult: vehicleSelectFormat,
            formatSelection: vehicleSelectFormat,
            escapeMarkup: function (m) { return m; }
        }).data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectChange(data);
                    return fn.apply(this, arguments);
                }
            }
        })(select2.onSelect);


        $('#DetailContainer').off(('click.btnSubmit', 'button[type="submit"]')).on('click.btnSubmit', 'button[type="submit"]', function (e) {
            var array = [];

            $('#scheduleTable td.box.highlighted').each(function() {
                array.push({
                    id: $(this).attr('id')
                });
            });

            $('#JsonSchedule').val(JSON.stringify(array));
            $('#DetailContainer').find('#processing').removeClass('hide');
        });

        $('#DetailContainer').off(('click.btnCancel', 'a[atype="button"]')).on('click.btnCancel', 'a[atype="button"]', function (e) {
            initCanvas();
        });

        $('#clearModal').off(('click.btnClear', 'button[type="reset"]')).on('click.btnClear', 'button[type="reset"]', function (e) {
            clearData();
            $('#clearModal').modal('hide');
        });

        $('#DetailContainer').off('click.btnAddAlarm', '#btnAddAlarm').on('click.btnAddAlarm', '#btnAddAlarm', function (e) {
            $('#alarmForm').find('#id').val($('#VehicleId').val());
            $('#alarmForm').submit();
        });

        $('#DetailContainer').off(('mousedown.scheduleTable', '#scheduleTable td')).on('mousedown.scheduleTable', '#scheduleTable td', function (e) {
            isMouseDown = true;
            $(this).toggleClass("highlighted");
            return false; // prevent text selection
        });

        $('#DetailContainer').off(('mouseover.scheduleTable', '#scheduleTable td')).on('mouseover.scheduleTable', '#scheduleTable td', function (e) {
            if (isMouseDown) {
                $(this).toggleClass("highlighted");
            }
        });

        $(document)
          .mouseup(function () {
              isMouseDown = false;
          });
    };

    /* Vehicle Select Format*/
    var vehicleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-2">' + json.PlateId + '</div>' +
                '<div class="col-md-3">' + json.Name + '</div>' +
                '<div class="col-md-4">' + json.CostCenterName + '</div>' +
                '<div class="col-md-2">' + json.FuelName + '</div>' +
                '</div>';
    };

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        $('#DetailContainer').find('#processing').addClass('hide');
    };


    /* On Success Load after select Change*/
    var onSelectChange = function (obj) {
        $('#id').val(obj.id);
        //$("input:hidden[id='VehicleGroupId']").val(obj.id);

        if (obj.id === '') {
            $('#DetailContainer').html('');
        } else {
            $('#loadForm').submit();
        }
    };



    var isMouseDown;        //flag we use to keep track
    var boxes;              //boxes information
    var labels;             //labes details

    /*Init Canvas custom control*/
    var initCanvas = function () {
        boxes = [];
        labels = [];

        loadBoxes();
        loadLabels();
        drawTableBody();

        loadData();

    };

    /*Draw Table Body*/
    var drawTableBody = function () {

        var html = "";
        var k = 0; var t = 0; var i = 0; var j = 0;
        for (i = 0; i < 24; i++) {
            html += '<tr>';
            html += '<td class="time">' + labels[t++] + '</td>';
            for (j = 0; j < 7; j++) {
                if (boxes[k].checked)
                    html += '<td class="box highlighted" id=' + boxes[k++].id + '>&nbsp;</td>';
                else
                    html += '<td class="box" id=' + boxes[k++].id + '>&nbsp;</td>';
            }
            html += '<td class="time2">' + labels[t++] + '</td>';
            for (j = 0; j < 7; j++) {
                if (boxes[k].checked)
                    html += '<td class="box highlighted" id=' + boxes[k++].id + '>&nbsp;</td>';
                else
                    html += '<td class="box" id=' + boxes[k++].id + '>&nbsp;</td>';
            }
            html += '</tr>';
        }

        $('#scheduleBody').html(html);
    };

    /*Load data from DB*/
    var clearData = function () {
        for (var i = 0; i < boxes.length; i++) {
            boxes[i].checked = false;
        }
        drawTableBody();
    };

    /*Load data from DB*/
    var loadData = function () {
        if ($('#JsonSchedule').val().length == 0)
            return;
        try {
            var array = JSON.parse($('#JsonSchedule').val());
            for (var i = 0; i < array.length; i++) {
                for (var j = 0; j < boxes.length; j++) {
                    if (boxes[j].id == array[i].id) {
                        boxes[j].checked = true;
                        break;
                    }
                }
            }
            drawTableBody();
        } catch (e) {
            console.debug('Error loading json from DB... ' + e);
        }
    };


    /*Load Schedule Hours*/
    var loadLabels = function () {

        labels.push("12:00 AM");
        labels.push("12:00 PM");
        labels.push("12:30 AM");
        labels.push("12:30 PM");

        for (var i = 1; i < 12; ++i) {
            labels.push(pad2(i) + ":00 AM");
            labels.push(pad2(i) + ":00 PM");
            labels.push(pad2(i) + ":30 AM");
            labels.push(pad2(i) + ":30 PM");
        }
    };

    /*Load Schedule Boxes*/
    var loadBoxes = function () {

        var k = 0; var i = 0; var j = 0;
        for (i = 0; i < 24; i++) {
            for (j = 0; j < 7; j++) {
                boxes.push({

                    id: pad2(k) + (i % 2 == 0 ? '0' : '3') + '0' + pad2(j),
                    checked: false
                });
            }
            for (j = 0; j < 7; j++) {
                boxes.push({
                    id: pad2(k + 12) + (i % 2 == 0 ? '0' : '3') + '0' + pad2(j),
                    checked: false
                });
            }
            if (i % 2 > 0) k++;
        }
    };



    //Pad a number to two digits
    var pad2 = function (number) {
        return (number < 10 ? '0' : '') + number;
    };


    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        InitCanvas: initCanvas
    };
})();

