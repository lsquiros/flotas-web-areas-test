﻿var ECO_Control = ECO_Control || {};

ECO_Control.ExpiredMaintenance = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $('.apply_expired').click(function (e) {
            $('#ApplyMaintenanceForm').find('#id').val($(this).attr('idmaintenance'));
            $('#ApplyMaintenanceForm').find('#vehicleId').val($(this).attr('idvehicle'));
            $('#ApplyMaintenanceForm').submit();
        });

        if ($('table.grid-table').width() != undefined) {
            
            var GridW = $('table.grid-table').width();
            var CarddW = $('.card').width();

            if (GridW > CarddW) {
                $(".card").width(GridW + 200);
            }
            else {
                return;
            }
        }
        
    }  

    var showApplyModal = function() {
        $('#addOrEditModalCost').modal('show');
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var loaderHide = function () {
        $('body').loader('hide');
        $('#DonwloadFileForm').submit();        
    };

    var onClickDownloadReport = function () {
        $('#ExcelReportDownloadForm').submit();
    };
   
    return {
        Init: initialize,
        LoaderHide: loaderHide,
        ShowApplyModal: showApplyModal,
        ShowLoader : showLoader,
        OnClickDownloadReport : onClickDownloadReport
    };
})();