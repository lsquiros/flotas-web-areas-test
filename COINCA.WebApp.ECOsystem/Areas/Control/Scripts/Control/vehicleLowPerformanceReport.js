﻿var ECO_Control = ECO_Control || {};

ECO_Control.VehicleLowPerformanceReport = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        printEventsSetup();
        datePickersSetup();
        vehicleListSetup();
        initializeDropDownList();
    };

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#ddlMonth").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#ddlYear").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);
    };

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#ddlMonth").select2('val', '');
            $("#ddlYear").select2('val', '');
            $('#startDate').val('');
            $('#endDate').val('');

            $('#ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#ddlMonth').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#ddlYear').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        
        if (($('#ReportCriteriaId').val() === '')
                || ($('#ddlMonth').val() === '')
                || ($('#ddlYear').val() === '')
            ) {
            if (($('#ddlMonth').val() !== '') && ($('#ddlYear').val() !== ''))
                return true;            
            invalidSelectOption();
            return false;
        }
        return true;
    };

    var vehicleListSetup = function () {

        var select4 = $("#VehicleId").select2({
            formatResult: vehicleSelectFormat,
            formatSelection: vehicleSelectFormat,
            escapeMarkup: function (m) { return m; }
        }).data('select2');

    };

    /* Vehicle Select Format*/
    var vehicleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-2">' + json.PlateId + '</div>' +
                '<div class="col-md-6">' + json.Name + '</div>' +
                '<div class="col-sm-4">' + json.CostCenterName + '</div>' +
                '</div>';
    };   

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                '<span aria-hidden="true">×</span>' +
                                            '</button>' +
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    var printEventsSetup = function () {

        $('#page-content-wrapper').off('click.print', 'a[data-print-link]').on('click.print', 'a[data-print-link]', function (e) {

            var month = $('#ddlMonth').val();
            var year = $('#ddlYear').val();
            var startDate = $('#startDate').val();
            var endDate = $('#endDate').val();

            if ((month != '' && year != '') || (startDate != '' && endDate != '')) {

                options.Model.Year = year;
                options.Model.Month = month;
                options.Model.StartDateStr = startDate;
                options.Model.EndDateStr = endDate;
                options.Model.VehicleId = $('#VehicleId').val();

           
                ECOsystem.Utilities.ShowPopup('VehicleLowPerformanceReport/PrintReport' + '?modelStr=' + JSON.stringify(options.Model), 'Formato de impresión');
            }
        });

        //$('#page-content-wrapper').off('click.print', 'a[download]').on('click.print', 'a[download]', function (e) {

        //    var month = $('#ddlMonth').val();
        //    var year = $('#ddlYear').val();
        //    var startDate = $('#startDate').val();
        //    var endDate = $('#endDate').val();

        //    $(this).closest("#formExcel").append('<input type="hidden" name="Year" value="' + year + '" />');
        //    $(this).closest("#formExcel").append('<input type="hidden" name="Month" value="' + month + '" />');
        //    $(this).closest("#formExcel").append('<input type="hidden" name="StartDateStr" value="' + startDate + '" />');
        //    $(this).closest("#formExcel").append('<input type="hidden" name="EndDateStr" value="' + endDate + '" />');
        //    $(this).closest("#formExcel").append('<input type="hidden" name="EndDateStr" value="' + endDate + '" />');
        //    $(this).closest("#formExcel").append('<input type="hidden" name="VehicleId" value="' + $('#VehicleId').val() + '" />');
        //    $(this).closest("#formExcel").submit();
        //    e.stopPropagation();
        //});

        $('#btnPrint').off('click.btnPrint').on('click.btnPrint', function (e) {
            // $('#btnPrint').addClass('hide');
            window.print();
        });
    }

    var datePickersSetup = function () {
        if (!options.printMode) {
            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });
            $("#Parameters_StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });


            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });

            $("#Parameters_EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });
        }
    }

    /* Public methods */
    return {
        Init: initialize
    };
})();
