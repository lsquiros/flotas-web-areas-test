﻿var ECO_Admin = ECO_Admin || {};

try {
    $("#Month").select2().data('select2');
    $("#Year").select2().data('select2');
    $("#VehicleId").select2().data('select2');
    $("#dateRanges").select2().data('select2');

    $('#StartDate').datepicker({
        language: 'es',
        minDate: new Date(),
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    })

    $('#EndDate').datepicker({
        language: 'es',
        minDate: new Date(),
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    })
} catch (e) {

}


 ECOsystem.Areas.Admin.ConsolidatedReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        try {
            initializeDropDownList();
        }
        catch (exception) {
            console.log('excepcion',exception)
        }
    }

    /*init Events*/
    var initEvents = function () {

        $('#StartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        })

        $('#EndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        })

        $("#btnDownload").click(function (event) {
            event.preventDefault();

            if (document.getElementById('dateRanges').value == 0) {
                document.getElementById('StartDate').value = ''
                document.getElementById('EndDate').value = ''
                document.getElementById('mainForm').submit()
            }
            else {
                document.getElementById('mainForm').submit()
            }

        });
    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        $("#Month").select2().data('select2');
        $("#Year").select2().data('select2');
        $("#VehicleId").select2().data('select2');

    };

    var changeDateForm = function (val) {
        if (val == 1) {
            $('#datetimepickerStartDate').removeClass('hidden')
            $('#datetimepickerEndDate').removeClass('hidden')

            $('#Month').addClass('hidden')
            $('#Year').addClass('hidden')
        }
        else {
            $('#datetimepickerStartDate').addClass('hidden')
            $('#datetimepickerEndDate').addClass('hidden')

            $('#Month').removeClass('hidden')
            $('#Year').removeClass('hidden')
        }
    }

    // Loader Functions
    var showLoader = function () {
        debugger;
        $('body').loader('show');
    };

    /*Hide Loader*/
    var hideLoader = function (data) {
        debugger;
        if (data > 0) {
            $('body').loader('hide');
            notificationMessage('Datos descargados correctamente');
            $('#DownloadFileForm').submit();
        } else {
            $('body').loader('hide');
            notificationMessage('No hay datos encontrados con los criterios de busqueda.');
        }
    };

    /*invalid Select Option*/
    var notificationMessage = function (mensaje) {
        $('#InfoContainer').html('<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
            ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' + mensaje + '</div>');
    };


    /* Public methods */
    return {
        Init: initialize,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        ChangeDateForm: changeDateForm
    };
})();



