﻿var ECO_Control = ECO_Control || {};

ECO_Control.AccumulatedFuelsReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        disabledControls();
        initCharts();
        //refreshGraphicCanvas();
        initializeDropDownList();
    }

    /*init Events*/
    var initEvents = function () {
        $('#page-content-wrapper').off('click.print', 'a[data-print-link]').on('click.print', 'a[data-print-link]', function (e) {
            //ECOsystem.Utilities.ShowPopup(options.printURL, 'Formato de impresión');
        });
        $('#page-content-wrapper').off('click.goToPrint', 'button[data-print]').on('click.goToPrint', 'button[data-print]', function (e) {
            window.print();
        });

        if (!options.printMode) {
            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function(e) {
                if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });
            $("#Parameters_StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });


            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function(e) {
                if ($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });

            $("#Parameters_EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });
        }
    }

    /*disabled Controls when printMode = true*/
    var disabledControls = function () {
        if (options.printMode) {
            $('[data-filter]').attr('disabled', 'disabled');
        }
    }

    /*Init Charts*/
    var initCharts = function () {
        
        if (options.labels.length > 12) {
            options.labels = $.merge(options.labels, ["", "", "", "", "", "", "", "", "", "", "", ""]);
        }

        var j = 0;
        $("canvas").each(function () {

            var ctx = this;

            var datasets = [];
            for (var i = 0; i < options.data.length; i++) {
                datasets.push({
                    fillColor: options.alpha[i],
                    strokeColor: options.colors[i],
                    highlightFill: options.highligh[i],
                    highlightStroke: options.colors[i],
                    data: options.data[i].slice(j, j + 12),
                    title: options.titles[i],
                    pointColor: options.colors[i]
                });
            }

            new Chart($(this).get(0).getContext("2d")).Line({
                labels: options.labels.slice(j, j + 12),
                datasets: datasets
            },
            {
                scaleLabel: "<%=ECO_Control.AccumulatedFuelsReport.FormatLabel(value)%>",
                annotateLabel: "<%=ECO_Control.AccumulatedFuelsReport.FormatLabel(v3)%>",
                inGraphDataTmpl: "<%=ECO_Control.AccumulatedFuelsReport.FormatLabel(v3)%>",
                legendBlockSize: 20,
                legendColorIndicatorStrokeWidth: 7,
                drawXScaleLine: [{ position: "bottom", lineWidth: 1, lineColor: "black" }, { position: "0", lineWidth: 1, lineColor: "red" }],
                scaleLineWidth: 1,
                scaleLineColor: "black"

            });
            j += 13;
        });
        $("div[data-item]:not(:first)").removeClass("active");
    }
    
    /*format Label*/
    var formatLabel = function (v) {
        return ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToFloat(v));
    }


    /*initialize Drop Down List*/
    var initializeDropDownList = function () {
        var select1 = $("#Parameters_ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        try {
            var select4 = $("#Parameters_ReportFuelTypeId").select2().data('select2');
            select4.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportFuelTypeChange(data); return fn.apply(this, arguments); }
                }
            })(select4.onSelect);
        } catch (e) {}

    };

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Parameters_Month").select2('val', '');
            $("#Parameters_Year").select2('val', '');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');

            $('#Parameters_ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Month').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Year').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Report Fuel Type Change*/
    var onSelectReportFuelTypeChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_ReportFuelTypeId').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                '<span aria-hidden="true">×</span>' +
                                            '</button>' +
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function() {
        if (($('#Parameters_ReportCriteriaId').val() === '')
                || ($('#Parameters_Month').val() === '')
                || ($('#Parameters_Year').val() === '')
                || ($('#Parameters_ReportFuelTypeId').val() === '')
                || ($('#Parameters_StartDateStr').val() === '')
                || ($('#Parameters_EndDateStr').val() === '')
            ) {
            if (($('#Parameters_Month').val() !== '') && ($('#Parameters_Year').val() !== ''))
                return true;
            if (($('#Parameters_StartDateStr').val() !== '') && ($('#Parameters_EndDateStr').val() !== ''))
                return true;
            invalidSelectOption();
            return false;
        }
        return true;
    };

    var refreshGraphicCanvas = function () {
        $(window).resize(function () {
            $('#ReloadForm').submit();
        });
    };

/* Public methods */
return {
    Init: initialize,
    FormatLabel: formatLabel
};
})();

