﻿var ECO_Control = ECO_Control || {};

ECO_Control.FuelsInfo = (function () {
    var options = {};
    
    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

    };

    /* Public methods */
    return {
        Init: initialize
    };
})();


