﻿var ECO_Control = ECO_Control || {};

ECO_Control.AdditionalVehicleFuelByGroup = (function () {
    var options = {};

    $(document).ready(function () {
        setTimeout(() => {
            $(document).ready(function () {
                if (($("#VehicleGroupId").val() == '' || $("#VehicleGroupId").val() == undefined)
                    && ($("#CostCenterId").val() == '' || $("#CostCenterId").val() == undefined)
                    && ($("#VehicleId").val() == '' || $("#VehicleId").val() == undefined)
                    && ($("#UserId").val() == '' || $("#UserId").val() == undefined)) {

                    $("#CostCenterId").val('-1').change();
                    ECOsystem.Utilities.ShowLoader();
                    ShowMassiveChanges
                }
            });
        }, 500);
    })

    var initialize = function (opts) {
        $.extend(options, opts);

        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        initializeDropDownList();

        validateInputValues();

        initCurrency();

        $('#gridContainer').off('change.checkall', '#checkall').on('change.checkall', '#checkall', function () {
            var checked = $(this).is(':checked');
            $('#gridContainer .showCheck').each(function () {
                $(this).prop('checked', checked);
            });
        });

        $('#gridContainer').off('change.inputOnChange', '.calculateliters').on('change.inputOnChange', '.calculateliters', function () {
            
            var id = $(this).attr("rowId");
            var vehicleId = $(this).attr("vehicleId");
            var fuelId = $(this).attr("fuelId");
            var amount = toNum($(this).val().replace('.', '').replace(',', '.'));
            if (!isNaN(amount) && amount <= 9999999999) {
                showChanges(id, amount, null, vehicleId, fuelId);
            }
            else {
                if (id != "" && id != null && id != undefined) {
                    $(this).val($("#amount_" + id).val());
                }
                else {
                    $(this).val(0);
                }
            }
        });

        $('#gridContainer').off('change.inputOnChange', '.calculatemount').on('change.inputOnChange', '.calculatemount', function () {
            
            var id = $(this).attr("rowId");
            var vehicleId = $(this).attr("vehicleId");
            var fuelId = $(this).attr("fuelId");
            var liters = toNum($(this).val().replace(',', '.'));
            if (!isNaN(liters) && liters <= 9999999999) {
                showChanges(id, null, liters, vehicleId, fuelId);
            }
            else {
                if (id != "" && id != null && id != undefined) {
                    $(this).val($("#liters_" + id).val());
                }
                else {
                    $(this).val(0);
                }
            }
        });

        $('#ControlsContainer').off('blur.inputOnBlur', '#ValueAll').on('blur.inputOnBlur', '#ValueAll', function () {
            var value = toNum($(this).val());
            if ($('#ValueType').val() === 'AMOUNT') {
                $(this).val(formatNum(value));
            }
        });

        $('#ControlsContainer').off('focusin.inputFocusIn', '#ValueAll').on('focusin.inputFocusIn', '#ValueAll', function () {
            
            if ($('#ValueAll').val() === '') {
                if (($('#ValueType').select2('val').length == 0) || $('#ValueType').select2('val') == 'AMOUNT') {
                    $('#ValueType').select2('val', 'AMOUNT');
                    onSelectTypeChange({ id: 'AMOUNT' });
                } else if ($('#ValueType').select2('val') == 'LITERS') {
                    onSelectTypeChange({ id: 'LITERS' });
                } else {
                    onSelectTypeChange({ id: 'CAPACITYUNIT' });
                }
            }
        });

        $('#gridContainer').off('click.btnSaveData', '.btnSaveData').on('click.btnSaveData', '.btnSaveData', function () {
            
            var isValidValues = false;
            $('#gridContainer').find('.modifiedRowVehicle').each(function (i) {
                isValidValues = true;
            });
            if (isValidValues) {
                setAmountErrorMsj('', false, 'e2');
                $('#confirmModalUpdate').modal('show');
            } else {
                setAmountErrorMsj('No existen datos seleccionados para modificar', true, 'e2');
            }
        });

        $('#btnAppliesMassive').off('click.btnAppliesMassive').on('click.btnAppliesMassive', function (e) {
            $("#SaveForm").submit();
        });
    };

    var initializeDropDownList = function () {
        $("#VehicleGroupId").select2().data('select2');
        $("#CostCenterId").select2().data('select2');
        $("#VehicleId").select2().data('select2');
        $("#UserId").select2().data('select2');
        $("#ValueType").select2().data('select2');

        $("#VehicleGroupId").on('change', function () {
            if ($(this).val() !== '') {
                $("#CostCenterId").val('').change();
                $("#VehicleId").val('').change();
                $('#ReloadForm').submit();
            }
        });

        $("#CostCenterId").on('change', function () {
            if ($(this).val() !== '') {
                $("#VehicleGroupId").val('').change();
                $("#VehicleId").val('').change();
                $("#UserId").val('').change();
                $('#ReloadForm').submit();
            }
        });

        $("#VehicleId").on('change', function () {
            if ($(this).val() !== '') {
                $("#VehicleGroupId").val('').change();
                $("#CostCenterId").val('').change();
                $('#ReloadForm').submit();
            }
        });

        $("#UserId").on('change', function () {
            if ($(this).val() !== '') {
                $("#CostCenterId").val('').change();
                $('#ReloadForm').submit();
            }
        });

        $("#ValueType").on('change', function () {
            onSelectTypeChange($(this));
        });
    };

    var onSelectTypeChange = function (obj) {
        $('#ValueAll').val('');

        if (obj.id !== '') {
            if (obj.id === 'LITERS') {
                $('#ValueAll').numeric('integer');
            }
            if (obj.id === 'CAPACITYUNIT') {
                $('#ValueAll').numeric({
                    allowMinus: false,
                    maxDecimalPlaces: 3
                });
            }
            if (obj.id === 'AMOUNT') {
                $('#ValueAll').numeric({
                    allowMinus: false,
                    maxDecimalPlaces: 2
                });
            }
        }
    };

    var changeData = function (vehicleId, isLitersCalculation, amount) {
        $.ajax({
            url: '/AdditionalVehicleFuelByGroup/GetChanges',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ VehicleId: vehicleId, IsLitersCalculation: isLitersCalculation, Amount: amount }),
            success: function (result) {
                if (result.Success == "Success") {
                    refreshData(result);
                }
            }
        });
    }

    var showChanges = function (id, amount, liters, vehicleId, fuelId) {
        
        list = [];
        var obj = {};

        let symbol;

        try {
            symbol = id != undefined ? document.getElementById('symbol_' + id).innerHTML : "+";
        } catch (e) {
            symbol = "+";
        }

        obj.Id = id;
        obj.AdditionalAmount = amount;
        obj.AdditionalLiters = liters;
        obj.VehicleId = vehicleId;
        obj.FuelId = fuelId;
        obj.Symbol = symbol;
        list.push(obj);
        applyChanges(list);
    }

    var applyChanges = function (list) {
        $.ajax({
            url: '/AdditionalVehicleFuelByGroup/ApplyChanges',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ list: list }),
            success: function (result) {
                $('#gridContainer').html(result);
                $.fn.alphanum.setNumericSeparators({
                    thousandsSeparator: ".",
                    decimalSeparator: ","
                });
                validateInputValues();
                initCurrency();
            }
        });
        $('#confirmModal').modal('hide');
    }

    var setAppliesMassiveValidation = function () {
        $("#errormsj").html('');
        var value = $('#ValueAll').val();

        if (value.length == 0 || ECOsystem.Utilities.ToNum(value) == 0) {
            $("#errormsj").html('Monto requerido y debe ser mayor que 0.00');
        }
        else {
            var selectedItemsCount = $('input[type=checkbox]:checked').length;
            if (selectedItemsCount > 0) {
                $("#confirmModal").modal('show');
            }
            else {
                $('#modalverification').modal('show');
                $('#confirmModal').modal('hide');
            }
        }
    };

    var showMassiveChanges = function () {
        var amount = null;
        var liters = null;
        list = [];

        if ($('#ValueType').val() === 'AMOUNT') {
            amount = toNum($('#ValueAll').val());
        } else {
            liters = toNum($('#ValueAll').val());
        }

        $('input[type=checkbox]:checked').each(function () {
            if (this.id != 'checkall') {
                var obj = {};
                obj.Id = $(this).attr('id');
                obj.AdditionalAmount = amount;
                obj.AdditionalLiters = liters;
                list.push(obj);
            }
        });
        applyChanges(list);
    };

    var formatCurrency = function (num, s) {
        return s + '' + num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    var setAmountErrorMsj = function (msj, show, typeError) {
        typeError = typeError != null ? typeError : 'e1';
        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').show('slow');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').hide();
                }
                break;
            default:
                break;
        }
    }

    var Retrieveliters = function (value, literprice, vehicleid) {
        var result = value / literprice;

        $("#txtliters_" + vehicleid).val(formatNum(result));

        return result;
    }

    var RetrieveMount = function (liters, literprice, vehicleid) {
        var result = liters * literprice;

        $("#txtgridmount_" + vehicleid).val(formatNum(result));
        return result;
    }

    var toNum = function (num) {
        if (num.toString().indexOf(".") > -1 && num.toString().indexOf(",") > -1) {
            num = ("" + num).replace(/\./g, '').replace(/,/g, '.');
        }
        //SQuiros: No valida correctamente la función isNaN(num) || 
        return num === '' || num === null ? 0.00 : parseFloat(num);
    }

    var formatNum = function (num) {
        return num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
            '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
            ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' +
            'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    var validateInputValues = function () {
        
        $('#gridContainer').off('click.actualizarMontos', '.actualizarMontosBtn').on('click.actualizarMontos', '.actualizarMontosBtn', function () {
            var isValidValues = true;

            $('#gridContainer').find('input[data-form-value]').each(function (i) {
                var value = parseInt($(this).attr('value'));
                if (value < 0) {
                    isValidValues = false;
                }
            });

            $('#gridContainer').find('input[data-form-liters]').each(function (i) {
                var value = parseInt($(this).attr('value'));
                if (value < 0) {
                    isValidValues = false;
                }
            });

            if (isValidValues) {
                setAmountErrorMsj('', false, 'e2');
                $('#confirmModalactualizar').modal('show');
            } else {
                setAmountErrorMsj('* Ningún monto puede ser menor que 0.00', true, 'e2');
            }
        });
    };

    var initCurrency = function () {
        try {
            
            $('#wrapper').off('blur.currencyOnBlur', 'input[data-currency]').on('blur.currencyOnBlur', 'input[data-currency]', function (e) {
                
                var obj = $(this);
                obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
                obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));


            });
            $('#wrapper').find('input[data-currency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });

        } catch (e) {

        }
    };

    var onSuccessLoad = function () {
        $('#confirmModal').modal('hide');
        $('#confirmModalactualizar').modal('hide');
        $('#ControlsContainer').find('#processing').addClass('hide');
        $('#ControlsContainer').find('button[data-btnmodal=true]').removeAttr("disabled");
        $('#Data_Value').val('');
        $('#Data_ValueStr').val('');
        $('#Data_ValueType').select2('val', '');
        $('#gridContainer').find("span[data-toggle=tooltip]").tooltip('hide');

        $("#AlarmsDdl").load('/ASPMenus/Alarms');
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadExcelForm').submit();
    };

    var onClickAdditionalVehicleFuelByGroup = function () {
        $('#ExcelReportDownloadForm').submit();
    };

    var onSuccessSave = function () {
        $('#confirmModalUpdate').modal('hide');
        $('body').loader('hide');
    }

    var changeSymbol = function (element, id) {
        var symbol = document.getElementById('symbolAmount_' + id);
        element.innerHTML = (element.innerHTML.trim() == "+") ? "-" : "+"
        element.style.backgroundColor = (element.innerHTML.trim() == "+") ? "green" : "red"        
        symbol.style.backgroundColor = (element.innerHTML.trim() == "+") ? "green" : "red"
        symbol.innerHTML = (element.innerHTML.trim() == "+") ? "+" : "-"  

        list = [];
        var obj = {};
        obj.AdditionalAmount = $('#txtliters_' + id).val();
        obj.AdditionalLiters = $('#txtgridmount_' + id).val();
        obj.Id = id;
        obj.Symbol = element.innerHTML.trim();
        list.push(obj);
        applyChanges(list);
    }

    var changeSymbolAmount = function (element, id) {
        var symbolAmount = document.getElementById('symbol_' + id);
        element.innerHTML = (element.innerHTML == "+") ? "-" : "+"
        element.style.backgroundColor = (element.innerHTML == "+") ? "green" : "red"        
        symbolAmount.style.backgroundColor = (element.innerHTML == "+") ? "green" : "red"
        symbolAmount.innerHTML = (element.innerHTML == "+") ? "+" : "-"        

        list = [];
        var obj = {};
        obj.AdditionalAmount = $('#txtliters_' + id).val();
        obj.AdditionalLiters = $('#txtgridmount_' + id).val();
        obj.Id = id;
        obj.Symbol = element.innerHTML;
        list.push(obj);
        applyChanges(list);
    }

    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        ShowMassiveChanges: showMassiveChanges,
        SetAppliesMassiveValidation: setAppliesMassiveValidation,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnClickAdditionalVehicleFuelByGroup: onClickAdditionalVehicleFuelByGroup,
        OnSuccessSave: onSuccessSave,
        ChangeSymbol: changeSymbol,
        ChangeSymbolAmount: changeSymbolAmount
    };
})();
