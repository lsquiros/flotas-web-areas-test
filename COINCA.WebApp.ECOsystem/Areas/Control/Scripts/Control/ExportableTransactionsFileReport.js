﻿var ECO_Control = ECO_Control || {};

ECO_Control.ExportableTransactionsFileReport = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        var select1 = $("#ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        $("#Month").select2('data', {});
        $("#Month").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
        $("#Year").select2('data', {});
        $("#Year").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });



        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) { });

        $("#StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });


        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) { });

        $("#EndDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });
    }

    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Month").select2('val', '');
            $("#Year").select2('val', '');
            $('#StartDateStr').val('');
            $('#EndDateStr').val('');

            $('#ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
        }
    };

    var onEndGetData = function (data) {
        hideloader();
        if (data == "Success") {
            $('#DonwloadFileForm').submit();
            $('#InfoContainer').html('<br />' +
                '<div class="alert alert-success alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Seleccione la información necesaria para generar el reporte.' +
                '</div>');
        } else if (data == "NoData") {
            $('#InfoContainer').html('<br />' +
                '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;No existe información para descargar el reporte con las fechas seleccionadas.' +
                '</div>');
        } else {
            $('#InfoContainer').html('<br />' +
                '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                data.Title + '<hr /><strong> Mensaje técnico: </strong><br />' + data.TechnicalError + '<br /></div>');
        }
    }

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    return {
        Init: initialize,
        ShowLoader: showloader,
        OnEndGetData: onEndGetData
    };
})();