﻿var ECO_Control = ECO_Control || {};

 ECO_Control.ConsolidateLiquidationReport = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        printEventsSetup();

        var select1 = $("#ddlMonth").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#ddlYear").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#ddlMonth').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#ddlYear').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        
        if (($('#ddlMonth').val() === '') || ($('#ddlYear').val() === ''))
        {
            if (($('#ddlMonth').val() !== '') && ($('#ddlYear').val() !== ''))
                return true;           
            invalidSelectOption();
            return false;
        }
        return true;
    };

    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                '<span aria-hidden="true">×</span>' +
                                            '</button>' +
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    var printEventsSetup = function () {

        $('#page-content-wrapper').off('click.print', 'a[data-print-link]').on('click.print', 'a[data-print-link]', function (e) {
         
            var month = $('#ddlMonth').val();
            var year = $('#ddlYear').val();
            if (month != '' && year != '') {
               
                options.Model.Year = year;
                options.Model.Month = month;
                ECOsystem.Utilities.ShowPopup('ConsolidateLiquidationReport/PrintReport' + '?modelStr=' + JSON.stringify(options.Model), 'Formato de impresión');
            }
        });
       
        $('#page-content-wrapper').off('click.print', 'a[download]').on('click.print', 'a[download]', function (e) {

            var month = $('#ddlMonth').val();
            var year = $('#ddlYear').val();
     
            $(this).closest("#formExcel").append('<input type="hidden" name="Year" value="' + year + '" />');
            $(this).closest("#formExcel").append('<input type="hidden" name="Month" value="' + month + '" />');
            $(this).closest("#formExcel").submit();

        });

        $('#btnPrint').off('click.btnPrint').on('click.btnPrint', function (e) {
            // $('#btnPrint').addClass('hide');
            window.print();
        });
    }

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadFileForm').submit();
    };

    var onClickDownloadPartnerReport = function () {
        //$('body').loader('show');
        $('#ExcelReportDownloadForm').submit();
    };
      
    /* Public methods */
    return {
        Init: initialize,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnClickDownloadPartnerReport: onClickDownloadPartnerReport
    };
})();
