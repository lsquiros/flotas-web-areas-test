﻿var ECO_Control = ECO_Control || {};

ECO_Control.PerformanceByVehicleComparativeReport = (function () {
    var options = {};
    var chartData = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        printEventsSetup();
        datePickersSetup();
        initializeDropDownList();
        vehicleListSetup();
        onShowDetailClick();
        onShowChartClick();
        onChartShowModal();
    };

    /*Opens the detail modal*/
    var onShowDetailClick = function () {
        $('#gridContainer').off('click.details', 'a[data-detail]').on('click.details', 'a[data-detail]', function (e) {
            var detailsForm = $('#detailsForm');
            detailsForm.find('#detailId').val($(this).attr('data-id'));
            detailsForm.find('#detailMonth').val($(this).attr('data-month'));
            detailsForm.find('#detailYear').val($(this).attr('data-year'));
            detailsForm.find('#detailWeek').val($(this).attr('data-week'));
            detailsForm.find('#detailDay').val($(this).attr('data-day'));
            detailsForm.find('#detailOption').val($(this).attr('data-option'));
            detailsForm.submit();
        });
    }

    /*Opens the Chart modal*/
    var onShowChartClick = function () {
        $('#gridContainer').off('click.chart', 'a[data-chart]').on('click.chart', 'a[data-chart]', function (e) {
            var chartForm = $('#chartForm');
            chartForm.find('#chartVehicleId').val($(this).attr('data-id'));
            chartForm.find('#chartGroupValue').val($(this).attr('data-group-value'));
            chartForm.submit();
        });
    }

    /*detail Show Modal*/
    var detailShowModal = function () {
        $('#detailModal').modal('show');
    };

    /*detail Show Modal*/
    var chartShowModal = function (data) {
        chartData = data;
        $('#chartModal').modal('show');
    };

    /*On Show Chart Modal*/
    var onChartShowModal = function () {
        $('#chartModal').off('shown.bs.modal').on('shown.bs.modal', function (e) {
            new Chart($("#performanceChart").get(0).getContext("2d")).Line({
                labels: chartData.labels,
                datasets: [
                    {
                        strokeColor: "rgb(230, 14, 14)",
                        pointColor: "rgb(205, 151, 155)",
                        pointStrokeColor: "#f56691",
                        data: chartData.dataTrx,
                        title: "Rendimiento por Transacción"
                    },
                    {
                        fillColor: "rgba(151, 205, 172, 0.5)",
                        strokeColor: "rgb(87, 147, 106)",
                        pointColor: "rgb(151, 205, 172)",
                        pointStrokeColor: "#fff",
                        data: chartData.dataGps,
                        title: "Rendimiento GPS"
                    }
                ]
            }, {
                legend: true, inGraphDataShow: true, canvasBackgroundColor: 'rgba(255,255,255,1.00)', spaceLeft: 12, spaceRight: 12, spaceTop: 12, spaceBottom: 12, canvasBorders: false, canvasBordersWidth: 4, canvasBordersColor: "rgba(140,145,6,1)", yAxisMinimumInterval: 'none', scaleShowLabels: true, scaleShowLine: true, scaleLineWidth: 2, scaleLineColor: "rgba(15,15,15,0.37)", scaleOverlay: false, scaleOverride: false, scaleSteps: 10, scaleStepWidth: 10, scaleStartValue: 0, maxLegendCols: 5, legendBlockSize: 15, legendFillColor: 'rgba(255,255,255,0.00)', legendColorIndicatorStrokeWidth: 1, legendPosX: -2, legendPosY: 4, legendXPadding: 0, legendYPadding: 0, legendBorders: false, legendBordersWidth: 1, legendBordersColors: "rgba(102,102,102,1)", legendBordersSpaceBefore: 5, legendBordersSpaceLeft: 5, legendBordersSpaceRight: 5, legendBordersSpaceAfter: 5, legendSpaceBeforeText: 5, legendSpaceLeftText: 5, legendSpaceRightText: 5, legendSpaceAfterText: 5, legendSpaceBetweenBoxAndText: 5, legendSpaceBetweenTextHorizontal: 5, legendSpaceBetweenTextVertical: 5, legendFontColor: "rgba(59,59,59,1)", yAxisFontColor: "rgba(102,102,102,1)", showYAxisMin: false, rotateLabels: "smart", xAxisBottom: true, yAxisLeft: true, yAxisRight: false, graphTitleSpaceBefore: 5, graphTitleSpaceAfter: -3, scaleFontColor: "rgba(3,3,3,1)", pointLabelFontColor: "rgba(102,102,102,1)", pointLabelFontSize: 12, angleShowLineOut: true, angleLineWidth: 1, angleLineColor: "rgba(0,0,0,0.1)", percentageInnerCutout: 50, scaleShowGridLines: true, scaleGridLineWidth: 1, scaleGridLineColor: "rgba(140,145,6,0.13)", scaleXGridLinesStep: 1, scaleYGridLinesStep: 0, segmentShowStroke: true, segmentStrokeWidth: 2, segmentStrokeColor: "rgba(255,255,255,1.00)", datasetStroke: true, datasetFill: false, datasetStrokeWidth: 3, bezierCurve: false, bezierCurveTension: 0.4, pointDotStrokeWidth: 2, pointDotRadius: 5, pointDot: true, graphMin: 0, barShowStroke: false, barBorderRadius: 0, barStrokeWidth: 1, barValueSpacing: 15, barDatasetSpacing: 0, scaleShowLabelBackdrop: true, scaleBackdropColor: 'rgba(255,255,255,0.75)', scaleBackdropPaddingX: 2, scaleBackdropPaddingY: 2
            });
        });
    };

    /*vehicle List Setup*/
    var vehicleListSetup = function () {

        var select2 = $("#Parameters_VehicleId").select2({
            formatResult: vehicleSelectFormat,
            formatSelection: vehicleSelectFormat,
            escapeMarkup: function (m) { return m; }
        }).data('select2');

    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {
        $("#ReloadForm").find('select').not("[custom='true']").select2();
        var select1 = $("#Parameters_ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {

                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectReportCriteriaChange(data);
                    return fn.apply(this, arguments);
                }
            };
        })(select1.onSelect);
    };

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Parameters_Month").select2('val', '');
            $("#Parameters_Year").select2('val', '');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');

            $('#Parameters_ReportCriteriaId').val(obj.id);

            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            $('#ReloadForm').submit();
        }
    };

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                '<span aria-hidden="true">×</span>' +
                                            '</button>' +
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    /* Vehicle Select Format*/
    var vehicleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-2">' + json.PlateId + '</div>' +
                '<div class="col-md-3">' + json.Name + '</div>' +
                '<div class="col-md-4">' + json.CostCenterName + '</div>' +
                '<div class="col-md-2">' + json.FuelName + '</div>' +
                '</div>';
    }

    /*print Events Setup*/
    var printEventsSetup = function () {

        $('#page-content-wrapper').off('click.print', 'a[data-print-link]').on('click.print', 'a[data-print-link]', function (e) {
            ECOsystem.Utilities.ShowPopup(options.printURL, 'Formato de impresión');
        });
        $('#page-content-wrapper').off('click.goToPrint', 'button[data-print]').on('click.goToPrint', 'button[data-print]', function (e) {
            window.print();
        });
    }

    /*datePickersSetup*/
    var datePickersSetup = function () {
        if (!options.printMode) {
            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });
            $("#Parameters_StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });


            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });

            $("#Parameters_EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });
        }
    }

    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        if (($('#Parameters_ReportCriteriaId').val() === '')
                || ($('#Parameters_Month').val() === '')
                || ($('#Parameters_Year').val() === '')
                || ($('#Parameters_StartDateStr').val() === '')
                || ($('#Parameters_EndDateStr').val() === '')
            ) {
            if (($('#Parameters_Month').val() !== '') && ($('#Parameters_Year').val() !== ''))
                return true;

            if (($('#Parameters_StartDateStr').val() !== '') && ($('#Parameters_EndDateStr').val() !== ''))
                return true;
            invalidSelectOption();
            return false;
        }
        return true;
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadFileForm').submit();
    };

    var onClickDownloadPartnerReport = function () {
        //$('body').loader('show');
        $('#ExcelReportDownloadForm').submit();
    };

    /* Public methods */
    return {
        Init: initialize,
        DetailShowModal: detailShowModal,
        ChartShowModal: chartShowModal,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnClickDownloadPartnerReport: onClickDownloadPartnerReport
    };
})();
