﻿var ECO_Control = ECO_Control || {};

ECO_Control.AccountFileReport = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        initializeDropDownList();       
    }

    var initEvents = function () {

        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) {
            if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects())
                $('#ReloadForm').submit();
            else
                invalidSelectOption();
        });

        $("#Parameters_StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });


        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) {
            if ($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects())
                $('#ReloadForm').submit();
            else
                invalidSelectOption();
        });

        $("#Parameters_EndDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });
    }

    var initializeDropDownList = function () {

        var select1 = $("#Parameters_ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

    };

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Parameters_Month").select2('val', '');
            $("#Parameters_Year").select2('val', '');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');

            $('#Parameters_ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Month').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Year').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        if (($('#Parameters_ReportCriteriaId').val() === '')
                || ($('#Parameters_Month').val() === '')
                || ($('#Parameters_Year').val() === '')
                || ($('#Parameters_StartDateStr').val() === '')
                || ($('#Parameters_EndDateStr').val() === '')
            ) {
            if (($('#Parameters_Month').val() !== '') && ($('#Parameters_Year').val() !== ''))
            {
                showLoader();
                return true;
            }
                
            if (($('#Parameters_StartDateStr').val() !== '') && ($('#Parameters_EndDateStr').val() !== ''))
            {
                showLoader();
                return true;
            }
                
            invalidSelectOption();
            return false;
        }
        return true;
    };

    /*check All Selects before submit form*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                '<span aria-hidden="true">×</span>' +
                                            '</button>' +
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    /*check All Selects before submit form*/
    var showLoader = function () {
        $('body').loader('show');
    };

    /*check All Selects before submit form*/
    var onClickDownload = function () {
        $('body').loader('show');
        $('#ExcelReportDownloadForm').submit();
    };

    /*Hide Loader*/
    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadFileForm').submit();
    };

    return {
        Init: initialize,
        ShowLoader: showLoader,
        OnClickDownload: onClickDownload,
        HideLoader: hideLoader
    };
})();