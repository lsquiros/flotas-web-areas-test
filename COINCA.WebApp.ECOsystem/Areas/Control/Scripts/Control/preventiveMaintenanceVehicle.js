﻿var ECO_Control = ECO_Control || {};

ECO_Control.PreventiveMaintenanceVehicle = (function () {
    var options = {};

    /*initialize function*/
    var initialize = function (opts) {
        $.extend(options, opts);

        try {
            initializeDropDownList();
        }
        catch (exception) {
        }
        $('#datetimepicker1').datepicker({ language: 'es', minDate: new Date(), format: 'dd/mm/yyyy', autoclose: true, todayHighlight: true });
        sumatory();
    };

    var saveCost = function (apply) {
        var valid = true;
        var count = 0;
        var list = new Array();
        var PreventiveMaintenanceId = $('#PreventiveMaintenanceId').val();
        var Date = $('#Detail_Header_DateStr').val();
        var Odometer = $('#Detail_Header_Odometer').val();
        $('a[del_row_detail]').each(
            function (index) {
                var id = $(this).attr('id');
                if (parseFloat(ECOsystem.Utilities.ToNum($('#Record-' + id).val())) <= 0) { valid = false; }
                if ($.trim($('#Name-' + id).val()) == '') { valid = false; }
                var PreventiveMaintenanceRecordByVehicleDetailId = $('#Cost-' + id).attr("RecordByVehicleDetailId");
                var PreventiveMaintenanceCostId = $('#Cost-' + id).attr("costId").replace('Cost-', '');
                var Description = $('#Name-' + id).val();
                var Cost = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + id).val()));
                var Record = parseFloat(ECOsystem.Utilities.ToNum($('#Record-' + id).val()));
                list.push("{ PreventiveMaintenanceRecordByVehicleDetailId:" + parseInt(PreventiveMaintenanceRecordByVehicleDetailId) + ", PreventiveMaintenanceCostId:" + parseInt(PreventiveMaintenanceCostId) + ", Description:'" + Description + "', Cost:" + parseFloat(Cost) + ", Record:" + parseFloat(Record) + " }");
                count = count + 1;
            });
        if (Date == '' || Odometer == '') { valid = false; }

        if (valid == false || count == 0) {
            $('#validateAmountModal').modal('show');
        }
        else {
            var postData = { list: list, PreventiveMaintenanceId: parseInt(PreventiveMaintenanceId), Date: Date, Odometer: parseFloat(Odometer), Apply: parseInt(apply) };
            var uri = '/Control/PreventiveMaintenanceVehicle/SaveRecordList';
            
            $.ajax({
                type: "POST",
                url: uri,
                data: postData,
                success: function (data) {
                    $('#addOrEditModalCost').modal('hide');
                },
                error: function(data){
                    alert("Error al acceder al recurso.");
                },
                dataType: "json",
                traditional: true
            });
        }
    }

    var initializeCost = function (opts) {
        $.extend(options, opts);
        $('#Detail_Header_DateStr').datepicker({ language: 'es', minDate: new Date(), format: 'dd/mm/yyyy', autoclose: true, todayHighlight: true });

        $('#gridCostContainer').off('click.del', 'a[del_row_detail]').on('click.del', 'a[del_row_detail]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
        });

        $('#gridContainer').off('click.del', 'a[del_row_maint]').on('click.del', 'a[del_row_maint]', function (e) {
            $('#deleteModal').find('#id').val($(this).attr('id'));
            $('#deleteModal').find('#vehicleId').val($(this).attr('vehicleId'));
        });

        $('#gridContainer').off('click.edit', 'a[edit_row_maint]').on('click.edit', 'a[edit_row_maint]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').find('#vehicleId').val($(this).attr('vehicleid'));
            $('#loadForm').submit();
        });

        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {            
            var vehicleId = $('#VehicleListD').val();
            var vehicleName = $('#VehicleListD option:selected').text().split(',')[1].split(':')[1].replace('\'', '').replace('\'', '');

            if (vehicleId == null || vehicleId == ''){ return; }                                      
            
            $("#VehicleId").val(vehicleId);
            $('#PreventiveMaintenanceCatalogList').attr('disabled', true);
            $('#PreventiveMaintenanceCatalogList').html('');
            $('#PreventiveMaintenanceCatalogList').append('<option value="0">{"Description":"Mantenimiento Preventivo"}</option>');
            $('#VehicleInfo').html(vehicleName);
            
            addOrEditModalShow();
        });

        $("#btnSaveCost").click(function () {
            saveCost(0);
        });

        $("#btnApplyCost").click(function () {
            saveCost(1);
        });

        $("#btnAddCost").click(function () {
            $('#gridCostContainer').removeClass('hidden');
            $('#MessageAlert').addClass('hidden');

            var id = 0;
            $('a[del_row_detail]').each(
                function (index) {
                    var newId = parseFloat($(this).attr('id'));
                    if (newId > id) {
                        id = newId;
                    }
                }
            )
            id = id + 1;
            var row = '';
            row = row + '<tr class="grid-row ">';
            row = row + '<td class="grid-cell grid-col-name" data-name="Description">';
            row = row + '<div>';
            row = row + '<input type="text" class="form-control nav-justified" value="" id="Name-' + id + '" data-currency="true">';
            row = row + '</div>';
            row = row + '</td>';
            row = row + '<td class="grid-cell grid-col-center" data-name="Cost">';
            row = row + '<div>';
            row = row + '<div class="input-group">';
            row = row + '<span2 class="input-group-addon">';
            row = row + $('#CurrencySymbol').attr('value');
            row = row + '</span2>';
            row = row + '<input type="text" onblur="ECO_Control.PreventiveMaintenanceVehicle.Sumatory()" class="form-control text-right num-cost formatted" value="0.00" id="Cost-' + id + '" costid="Cost-0" RecordByVehicleDetailId="0" data-currency="true" disabled="disabled">';
            row = row + '</div>';
            row = row + '</div>';
            row = row + '</td>';
            row = row + '<td class="grid-cell grid-col-center" data-name="Record">';
            row = row + '<div>';
            row = row + '<div class="input-group">';
            row = row + '<span2 class="input-group-addon">';
            row = row + $('#CurrencySymbol').attr('value');
            row = row + '</span2>';
            row = row + '<input type="text" onblur="ECO_Control.PreventiveMaintenanceVehicle.Sumatory()" class="form-control text-right num-cost formatted" value="0.00" id="Record-' + id + '" recordbyvehicleid="Record-0" data-currency="true">';
            row = row + '</div>';
            row = row + '</div>';
            row = row + '</td>';
            row = row + '<td class="grid-cell grid-col-btn buttonColumn" data-name="">';
            row = row + '<span class="toolBar1">';
            row = row + '<a href="#" class="delete-color" data-toggle="modal" data-target="#deleteCostModal" id="' + id + '" title="Eliminar" del_row_detail="">';
            row = row + '<i class="mdi mdi-32px mdi-close-box">';
            row = row + '</i>';
            row = row + '</a>';
            row = row + '</span>';
            row = row + '</td>';
            row = row + '</tr>';
            $('#gridCostContainer table tbody').append(row);
            sumatory();
        });

        $('#gridCostContainer').off('click.del', 'a[del_row_detail]').on('click.del', 'a[del_row_detail]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
        });

        $('#btnDeleteCost').off('click.btnDeleteCost').on('click.btnDeleteCost', function (e) {
            var id = $('#loadForm').find('#id').val();
            var cost = $('#Cost-' + id).val();
            
            if ($('#Record-' + id).attr("recordbyvehicleid").replace('Record-', '') == '0') {
                var tr = $($('#Record-' + id)).closest("tr").hide();
                tr.fadeOut(400, function () { tr.remove(); });
                sumatory();
            }
            else {
                var postData = { id: id };
                $.ajax({
                    type: "POST",
                    url: '/PreventiveMaintenanceRecordByVehicle/DeleteRecord',
                    data: postData,
                    success: function (data) {
                        var tr = $($('#Record-' + id)).closest("tr").hide();
                        tr.fadeOut(400, function () { tr.remove(); });
                        sumatory();
                    },
                    dataType: "json",
                    traditional: true
                });
            }
        });

        $("#btnCancelCost").click(function () {            
            $('#addOrEditModalCost').modal('hide');
        });

        $('#VehicleListD').change(function () {
            var val = $(this).val();

            if (val != "")
            {
                $('#btnAdd').removeClass('disabled');
                $('#loadVehicleMaintenanceForm').find('#id').val(val);
                $('#loadVehicleMaintenanceForm').submit();
                return;
            }
            
            $('#btnAdd').addClass('disabled');
        });

        $('#ddlFilterType').change(function () {
            var val = $(this).val();            
            $('#PreventiveMaintenanceCatalogList').html('');
            $('#select2-chosen-3').html("Mantenimiento");
            
            if(val != 0)
            {
                $.ajax({
                    url: '/PreventiveMaintenanceVehicle/RetrieveMaintenanceList',
                    type: 'POST',
                    data: JSON.stringify({ filterType: val }),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {                       
                        for (var i = 0; i < data.length; i++) {
                            $('#PreventiveMaintenanceCatalogList').append('<option value="' + data[i].PreventiveMaintenanceCatalogId + '">'
                                    + data[i].Description +
                                    '</option>');
                        }
                        $('#PreventiveMaintenanceCatalogList').attr('disabled', false);
                    }
                });                
            }

        }); 

        $('#PreventiveMaintenanceCatalogList').change(function () {
            if($(this).val() >  0)
            {
                $('#MaintenanceId').addClass('hidden');
                return;
            }
            $('#MaintenanceId').removeClass('hidden');
        });

        $('#btnAddMaintenance').click(function () {
            var maint = $('#PreventiveMaintenanceCatalogList').val();

            if (maint > 0) {
                $('#addOrEditForm').submit();
                return;
            }
            
            $('#MaintenanceId').removeClass('hidden');
            
        });
    }

    var initializeDropDownList = function () {

       var select2 = $("#VehicleListD").select2(
           { formatResult: VehicleListFormat, formatSelection: VehicleListFormat }).data('select2');
            select2.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

       var select4 = $("#PreventiveMaintenanceCatalogList").select2(
        { formatResult: CatalogListFormat, formatSelection: CatalogListFormat }).data('select2');
            select4.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
        }
        })(select4.onSelect);

    };

    var VehicleListFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-4">' + json.PlateId + '</div>' +
                '<div class="col-md-6">' + json.Name + '</div>' +
                '</div>';
    };

    var CatalogListFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Description + '</div>' +
                '</div>';
    };

    var addOrEditModalClose = function () {
        //ECOsystem.Utilities.InitCardResize();
        $('#addOrEditModal').modal('hide');
        //location.reload();
    };

    var deleteModalClose = function () {
        $('#deleteModal').modal('hide');
        //location.reload();
    };

    var addOrEditModalShow = function () {
        $('#addOrEditModal').modal('show');
    }

    var addOrEditModalCostShow = function () {
        sumatory();
        $('#addOrEditModalCost').modal('show');
    }

    var sumatory = function () {
        var totalCost = 0;
        var totalRecord = 0;
        $('a[del_row_detail]').each(
            function (index) {
                var numCost = 0;
                var numRecord = 0;
                try {
                    if ($('#Cost-' + $(this).attr('id')).hasClass("formatted") == true) {
                        numCost = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()));
                    }
                    else {
                        numCost = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()) / 100);
                        $('#Cost-' + $(this).attr('id')).addClass("formatted");
                    }
                }
                catch (err) {
                    var numCost = 0;
                }
                try {
                    if ($('#Record-' + $(this).attr('id')).hasClass("formatted") == true) {
                        numRecord = parseFloat(ECOsystem.Utilities.ToNum($('#Record-' + $(this).attr('id')).val()));
                    }
                    else {
                        numRecord = parseFloat(ECOsystem.Utilities.ToNum($('#Record-' + $(this).attr('id')).val()) / 100);
                        $('#Record-' + $(this).attr('id')).addClass("formatted");
                    }
                }
                catch (err) {
                    var numRecord = 0;
                }
                $('#Cost-' + $(this).attr('id')).val(ECOsystem.Utilities.FormatNum(numCost));
                $('#Record-' + $(this).attr('id')).val(ECOsystem.Utilities.FormatNum(numRecord));
                totalCost = totalCost + numCost;
                totalRecord = totalRecord + numRecord;
                $('#Cost-total').val(ECOsystem.Utilities.FormatNum(totalCost));
                $('#Record-total').val(ECOsystem.Utilities.FormatNum(totalRecord));
            }
        )
    }

    /*Public methods*/
    return {
        Init: initialize,
        AddOrEditModalShow: addOrEditModalShow,
        AddOrEditModalClose: addOrEditModalClose,
        DeleteModalClose: deleteModalClose,
        AddOrEditModalCostShow: addOrEditModalCostShow,
        Sumatory: sumatory,
        InitCost: initializeCost,
        

    };
})();


