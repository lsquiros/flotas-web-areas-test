﻿var ECO_Control = ECO_Control || {};

ECO_Control.VehicleFuelByGroup = (function () {
    var options = {};
    var list = [];

    var initialize = function (opts) {
        $.extend(options, opts);

        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        initializeDropDownList();

        initCurrency();


        downloadFile();

        result();

        $('#gridContainer').off('change.checkall', '#checkall').on('change.checkall', '#checkall', function () {
            var checked = $(this).is(':checked');
            $('#gridContainer .showCheck').each(function () {
                $(this).prop('checked', checked);
            });
        });

        $('#gridContainer').off('change.inputOnChange', '.calculateliters').on('change.inputOnChange', '.calculateliters', function () {
            var id = $(this).attr("rowId");
            var vehicleId = $(this).attr("vehicleId");
            var fuelId = $(this).attr("fuelId");
            var amount = toNum($(this).val().replace('.', '').replace(',', '.'));
            if (!isNaN(amount) && amount <= 9999999999) {
                showChanges(id, amount, null, vehicleId, fuelId);
            }
            else {
                if (id != "" && id != null && id != undefined) {
                    $(this).val($("#amount_" + id).val());
                }
                else {
                    $(this).val(0);
                }                
            }
        });

        $('#gridContainer').off('change.inputOnChange', '.calculatemount').on('change.inputOnChange', '.calculatemount', function () {
            var id = $(this).attr("rowId");
            var vehicleId = $(this).attr("vehicleId");
            var fuelId = $(this).attr("fuelId");
            var liters = toNum($(this).val().replace(',', '.'));
            if (!isNaN(liters) && liters <= 9999999999) {
                showChanges(id, null, liters, vehicleId, fuelId);
            }
            else {
                if (id != "" && id != null && id != undefined) {
                    $(this).val($("#liters_" + id).val());
                }
                else {
                    $(this).val(0);
                }   
            }
        });

        $('#ControlsContainer').off('blur.inputOnBlur', '#ValueAll').on('blur.inputOnBlur', '#ValueAll', function () {
            var value = toNum($(this).val());
            if ($('#ValueType').val() === 'AMOUNT') {
                $(this).val(formatNum(value));
            }
        });

        $('#ControlsContainer').off('focusin.inputFocusIn', '#ValueAll').on('focusin.inputFocusIn', '#ValueAll', function () {
            
            if ($('#ValueAll').val() === '') {
                if (($('#ValueType').select2('val').length == 0) || $('#ValueType').select2('val') == 'AMOUNT') {
                    $('#ValueType').select2('val', 'AMOUNT');
                    onSelectTypeChange({ id: 'AMOUNT' });
                } else if ($('#ValueType').select2('val') == 'LITERS') {
                    onSelectTypeChange({ id: 'LITERS' });
                } else {
                    onSelectTypeChange({ id: 'CAPACITYUNIT' });
                }
                
            }
        });

        $('#gridContainer').off('click.btnSaveData', '.btnSaveData').on('click.btnSaveData', '.btnSaveData', function () {
            var isValidValues = true;
            $('#gridContainer').find('input[data-form-value]').each(function (i) {
                var value = parseInt($(this).attr('value'));
                if (value <= 0) {
                    isValidValues = true;
                }
            });
            if (isValidValues) {
                setAmountErrorMsj('', false, 'e2');
                $('#confirmModalaUpdate').modal('show');
            } else {
                setAmountErrorMsj('* Ningún monto puede ser igual que 0.00', true, 'e2');
            }
        });

        $('#btnAppliesMassive').off('click.btnAppliesMassive').on('click.btnAppliesMassive', function (e) {
            $("#SaveForm").submit();
        });


        //$('#btnDownloadFileResults').off('click.btnDownloadFileResults').on('click.btnDownloadFileResults', function (e) {
        //    downloadFile();
        //});

    };


    var onSelectCostCenterChange = function () {
        var costCenter = $("#CostCenterId").val();
        if (costCenter) {
            $('body').loader('show');
            $('#ReloadForm').submit();
        } else {
            $("#CostCenterId").select2('open');
        }
    };

    var initializeDropDownList = function () {
        $("#VehicleGroupId").select2().data('select2');
        $("#CostCenterId").select2().data('select2');
        $("#VehicleId").select2().data('select2');
        $("#UserId").select2().data('select2');
        $("#ValueType").select2().data('select2');
        $("#CostCenterIdDistributionType").select2().data('select2');
        $("#FuelId").select2().data('select2');

        $("#VehicleGroupId").on('change', function () {
            if ($(this).val() !== '') {
                $('#ReloadForm').submit();
            }
        });

        $("#CostCenterId").on('change', function () {
            var button = $('#btnSearchDistibution');
            if (button.length === 0) {
                if ($(this).val() !== '') {
                    $('#ReloadForm').submit();
                }
            }
        });

        $("#VehicleId").on('change', function () {
            if ($(this).val() !== '') {
                $('#ReloadForm').submit();
            }
        });

        $("#UserId").on('change', function () {
            if ($(this).val() !== '') {
                $('#ReloadForm').submit();
            }
        });

        $("#ValueType").on('change', function () {
            onSelectTypeChange($(this));
        });

        $("#FuelId").on('change', function () {
            if ($(this).val() !== '') {
                $('#ReloadForm').submit();
            }
        });

        
        $(document).ready(function () {
            if (($("#VehicleGroupId").val() == '' || $("#VehicleGroupId").val() == undefined)
                && ($("#CostCenterId").val() == '' || $("#CostCenterId").val() == undefined)
                && ($("#VehicleId").val() == '' || $("#VehicleId").val() == undefined)
                && ($("#UserId").val() == '' || $("#UserId").val() == undefined)) {
                if ($("#CostCenterFuelDistribution").val() == "False") {
                    $("#CostCenterId").val('-1').change();
                    ECOsystem.Utilities.ShowLoader();
                }
                
            }
        });
    };

    var onSelectTypeChange = function (obj) {
        $('#ValueAll').val('');

        if (obj.id !== '') {
            if (obj.id === 'LITERS') {
                $('#ValueAll').numeric('integer');
            }
            if (obj.id === 'CAPACITYUNIT') {
                $('#ValueAll').numeric({
                    allowMinus: false,
                    maxDecimalPlaces: 3
                });
            }
            if (obj.id === 'AMOUNT') {
                $('#ValueAll').numeric({
                    allowMinus: false,
                    maxDecimalPlaces: 2
                });
            }
        }
    };

    var showMassiveChanges = function () {
        var amount = null;
        var liters = null;
        list = [];

        if ($('#ValueType').val() === 'AMOUNT') {
            amount = toNum($('#ValueAll').val());
        } else {
            liters = toNum($('#ValueAll').val());
        }

        $('input[type=checkbox]:checked').each(function () {
            if (this.id != 'checkall') {
                var obj = {};

                obj.Id = $(this).attr('id');
                obj.amount = amount;
                obj.Liters = liters;
                list.push(obj);
            }
        });
        applyChanges(list);
    };

    var showChanges = function (id, amount, liters, vehicleId, fuelId) {
        list = [];
        var obj = {};

        obj.Id = id;
        obj.amount = amount;
        obj.Liters = liters;
        obj.VehicleId = vehicleId;
        obj.FuelId = fuelId;
        list.push(obj);

        applyChanges(list);
    }

    var applyChanges = function (list) {
        $.ajax({
            url: '/VehicleFuelByGroup/ApplyChanges',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ list: list }),
            success: function (result) {
                $('#gridContainer').html(result);
            }
        });
        $('#confirmModal').modal('hide');
    }

    var setAppliesMassiveValidation = function () {
        $("#errormsj").html('');
        var value = $('#ValueAll').val();

        if (value.length == 0 || ECOsystem.Utilities.ToNum(value) == 0 || ECOsystem.Utilities.ToNum(value) > 9999999999) {
            $("#errormsj").html('Monto requerido debe ser mayor que 0.00 y menor a 9,999,999,999.00');
        }
        else {
            var selectedItemsCount = $('input[type=checkbox]:checked').length;
            if (selectedItemsCount > 0) {
                $("#confirmModal").modal('show');
            }
            else {
                $('#modalverification').modal('show');
                $('#confirmModal').modal('hide');
            }
        }
    };

    var toNum = function (num) {
        if (num.toString().indexOf(".") > -1 && num.toString().indexOf(",") > -1) {
            num = ("" + num).replace(/\./g, '').replace(/,/g, '.');
        }
        return num === '' || num === null ? 0.00 : parseFloat(num);
    }

    var setAmountErrorMsj = function (msj, show, typeError) {
        typeError = typeError != null ? typeError : 'e1';
        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').removeClass('hide');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').addClass('hide');
                }
                break;
            default:
                break;
        }
    }

    var onClickImport = function () {
        $('#ImportModal').modal('show');
    };

    var onClickImport2 = function () {
        $('#ImportXLS').trigger('click');
        $("#txtFile").val("");
    };

    var onClickImport3 = function () {
        $('body').loader('show');
        $('#ImportForm').submit();
    };

    var downloadFile = function () {
        $('#DonwloadFileForm1').submit();
    };

    var getFilename = function (input) {
        if (ValidateFormat(input)) {
            if (input.files && input.files[0]) {
                var value = $("#ImportXLS").val();
                var NameOfFile = input.files[0].name;
                $("#txtFile").val(NameOfFile);
                $("#btnImport").removeClass("hide");
                setErrorFormatMsj('', false);
            }
        }
        else {
            $("#txtFile").val("");
            if (!$("#btnImport").hasClass("hide")) {
                $("#btnImport").addClass("hide");
            }
            setErrorFormatMsj('* El formato de archivo es inválido, favor seleccionar un archivo de formato CSV.', true);
        }
    }

    var setErrorFormatMsj = function (msj, show) {
        if (show) {
            $('#errorFormatMsj').html(msj);
            $('#validationFormatErrors').removeClass('hide');
        } else {
            $('#errorFormatMsj').html('');
            $('#validationFormatErrors').addClass('hide');
        }

    }

    var ValidateFormat = function (input) {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var files = input.files;
            for (var i = 0, f; f = files[i]; i++) {
                // Only process csv files.
                if (!f.name.match('\.csv') || (f.size > 46030)) {
                    return false;
                }
                else {
                    return true;
                }
            }
        } else {

            alert('La opción de subir imágenes no es soportada por este navegador.', true);
        }
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
    };

    var onSuccessSave = function (data) {
        $('#confirmModalaUpdate').modal('hide');
        hideLoader();

        $('#gridContainer').html(data);
    };

    //--------------------------------------------

    var result = function () {
        var resultdata = $('#ResultData').val();
        if (resultdata != '' && resultdata != undefined && resultdata != null) showresultmodal(resultdata);
    }

    var showresultmodal = function (resultdata) {
        $('#resultimportdate').html(resultdata.split('|')[0]);
        $('#resultupdated').html(resultdata.split('|')[1]);
        $('#resultnoprocess').html(resultdata.split('|')[2]);
        $('#resultregisterror').html(resultdata.split('|')[3]);
        $('#resulttotal').html(resultdata.split('|')[4]);
        $('#SummaryResultModal').modal('show');
    }

    var Retrieveliters = function (value, literprice, vehicleid) {
        var result = 0.0;
        if (literprice > 0.00000001)
            result = value / literprice;

        $("#txtliters_" + vehicleid).val(formatNum(result));

        return result;
    }

    var RetrieveMount = function (liters, literprice, vehicleid) {

        var result = liters * literprice;

        $("#txtgridmount_" + vehicleid).val(formatNum(result));
        return result;
    }

    var formatNum = function (num) {
        return num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
            '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
            ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' +
            'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    var initCurrency = function () {
        try {
            $('#wrapper').off('blur.currencyOnBlur', 'input[data-currency]').on('blur.currencyOnBlur', 'input[data-currency]', function (e) {
                var obj = $(this);

                obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
                obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));


            });
            $('#wrapper').find('input[data-currency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });

        } catch (e) {

        }
    }

    var onSuccessLoad = function () {
        $('#confirmModal').modal('hide');
        $('#confirmModalactualizar').modal('hide');
        $('#ControlsContainer').find('#processing').addClass('hide');
        $('#ControlsContainer').find('button[data-btnmodal=true]').removeAttr("disabled");

        $('#Data_Value').val('');
        $('#Data_ValueStr').val('');
        $('#Data_ValueType').select2('val', '');

        $('#gridContainer').find("span[data-toggle=tooltip]").tooltip('hide');
    };

    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        ShowMassiveChanges: showMassiveChanges,
        SetAppliesMassiveValidation: setAppliesMassiveValidation,
        OnClickImport: onClickImport,
        OnClickImport2: onClickImport2,
        OnClickImport3: onClickImport3,
        GetFilename: getFilename,
        DownloadFile: downloadFile,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnSuccessSave: onSuccessSave,
        OnSelectCostCenterChange: onSelectCostCenterChange
    };
})();
