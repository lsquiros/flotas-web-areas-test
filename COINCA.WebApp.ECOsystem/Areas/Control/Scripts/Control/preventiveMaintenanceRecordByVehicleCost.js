﻿var ECO_Control = ECO_Control || {};

ECO_Control.preventiveMaintenanceRecordByVehicleCost = (function () {
    var options = {};

    /*initialize function*/
    //var initialize = function (opts) {
        $.extend(options, opts);
        sumatory();

        $('#gridCostContainer').off('click.del', 'a[del_row_detail]').on('click.del', 'a[del_row_detail]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
        });

        $('#btnDeleteCost').off('click.btnDeleteCost').on('click.btnDeleteCost', function (e) {
            var id = $('#loadForm').find('#id').val();
            var cost = $('#Cost-' + id).val();

            if ($('#Record-' + id).attr("recordbyvehicleid").replace('Record-', '') == '0') {
                var tr = $($('#Record-' + id)).closest("tr").hide();
                tr.fadeOut(400, function () { tr.remove(); });
                sumatory();
            }
            else {
                var postData = { id: id };
                $.ajax({
                    type: "POST",
                    url: ECOsystem.Utilities.GetUrl() + '/PreventiveMaintenanceRecordByVehicle/DeleteRecord',
                    data: postData,
                    success: function (data) {
                        var tr = $($('#Record-' + id)).closest("tr").hide();
                        tr.fadeOut(400, function () { tr.remove(); });
                        sumatory();
                    },
                    dataType: "json",
                    traditional: true
                });
            }
        });



        $("#btnCancelCost").click(function () {
            location.reload();
        });

        $("#btnSaveCost").click(function () {
            var valid = true;
            var count = 0;
            var list = new Array();
            var PreventiveMaintenanceRecordByVehicleId = $('#PreventiveMaintenanceRecordByVehicleId').val();
            var PreventiveMaintenanceId = $('#PreventiveMaintenanceId').val();
            $('a[del_row_detail]').each(
            function (index) {
                var id = $(this).attr('id');
                if (parseFloat(ECOsystem.Utilities.ToNum($('#Record-' + id).val())) <= 0) { valid = false; }
                if ($.trim($('#Name-' + id).val()) == '') { valid = false; }
                var PreventiveMaintenanceRecordByVehicleDetailId = $('#Cost-' + id).attr("RecordByVehicleDetailId");
                var PreventiveMaintenanceCostId = $('#Cost-' + id).attr("costId").replace('Cost-', '');
                var Description = $('#Name-' + id).val();
                var Cost = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + id).val()));
                var Record = parseFloat(ECOsystem.Utilities.ToNum($('#Record-' + id).val()));
                list.push("{ PreventiveMaintenanceRecordByVehicleDetailId:" + parseInt(PreventiveMaintenanceRecordByVehicleDetailId) + ", PreventiveMaintenanceRecordByVehicleId:" + parseInt(PreventiveMaintenanceRecordByVehicleId) + ", PreventiveMaintenanceCostId:" + parseInt(PreventiveMaintenanceCostId) + ", Description:'" + Description + "', Cost:" + parseFloat(Cost) + ", Record:" + parseFloat(Record) + " }");
                count = count + 1;
            })
            if (valid == false || count == 0) {
                alert('error de validación');
            }
            else {
                var postData = { list: list, PreventiveMaintenanceRecordByVehicleId: parseInt(PreventiveMaintenanceRecordByVehicleId), PreventiveMaintenanceId: parseInt(PreventiveMaintenanceId) };
                $.ajax({
                    type: "POST",
                    url: ECOsystem.Utilities.GetUrl() + '/PreventiveMaintenanceRecordByVehicle/SaveRecordList',
                    data: postData,
                    success: function (data) {
                        location.reload();
                    },
                    dataType: "json",
                    traditional: true
                });
            }
        });

        $("#btnAddCost").click(function () {
            var id = 0;
            $('a[del_row_detail]').each(
                function (index) {
                    var newId = parseFloat($(this).attr('id'));
                    if (newId > id) {
                        id = newId;
                    }
                }
            )
            id = id + 1;
            var row = '';
            row = row + '<tr class="grid-row ">';
            row = row + '<td class="grid-cell grid-col-name" data-name="Description">';
            row = row + '<div>';
            row = row + '<input type="text" class="form-control nav-justified" value="" id="Name-' + id + '" data-currency="true">';
            row = row + '</div>';
            row = row + '</td>';
            row = row + '<td class="grid-cell grid-col-center" data-name="Cost">';
            row = row + '<div>';
            row = row + '<div class="input-group">';
            row = row + '<span2 class="input-group-addon">';
            row = row + $('#CurrencySymbol').attr('value');
            row = row + '</span2>';
            row = row + '<input type="text" onblur="ECO_Control.preventiveMaintenanceRecordByVehicleCost.Sumatory()" class="form-control text-right num-cost formatted" value="0.00" id="Cost-' + id + '" costid="Cost-0" RecordByVehicleDetailId="0" data-currency="true" disabled="disabled">';
            row = row + '</div>';
            row = row + '</div>';
            row = row + '</td>';
            row = row + '<td class="grid-cell grid-col-center" data-name="Record">';
            row = row + '<div>';
            row = row + '<div class="input-group">';
            row = row + '<span2 class="input-group-addon">';
            row = row + $('#CurrencySymbol').attr('value');
            row = row + '</span2>';
            row = row + '<input type="text" onblur="ECO_Control.preventiveMaintenanceRecordByVehicleCost.Sumatory()" class="form-control text-right num-cost formatted" value="0.00" id="Record-' + id + '" recordbyvehicleid="Record-0" data-currency="true">';
            row = row + '</div>';
            row = row + '</div>';
            row = row + '</td>';
            row = row + '<td class="grid-cell grid-col-btn" data-name="">';
            row = row + '<span class="toolBar1">';
            row = row + '<a href="#" class="delete-color" data-toggle="modal" data-target="#deleteCostModal" id="' + id + '" title="Eliminar" del_row_detail="">';
            row = row + '<i class="glyphicon glyphicon-remove">';
            row = row + '</i>';
            row = row + '</a>';
            row = row + '</span>';
            row = row + '</td>';
            row = row + '</tr>';
            $('#gridCostContainer table tbody').append(row);
            sumatory();
        });

        var sumatory = function () {
            var totalCost = 0;
            var totalRecord = 0;
            $('a[del_row_detail]').each(
                function (index) {
                    var numCost = 0;
                    var numRecord = 0;
                    try {
                        if ($('#Cost-' + $(this).attr('id')).hasClass("formatted") == true) {
                            numCost = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()));
                        }
                        else {
                            numCost = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()) / 100);
                            $('#Cost-' + $(this).attr('id')).addClass("formatted");
                        }
                    }
                    catch (err) {
                        var numCost = 0;
                    }
                    try {
                        if ($('#Record-' + $(this).attr('id')).hasClass("formatted") == true) {
                            numRecord = parseFloat(ECOsystem.Utilities.ToNum($('#Record-' + $(this).attr('id')).val()));
                        }
                        else {
                            numRecord = parseFloat(ECOsystem.Utilities.ToNum($('#Record-' + $(this).attr('id')).val()) / 100);
                            $('#Record-' + $(this).attr('id')).addClass("formatted");
                        }
                    }
                    catch (err) {
                        var numRecord = 0;
                    }   
                    $('#Cost-' + $(this).attr('id')).val(ECOsystem.Utilities.FormatNum(numCost));
                    $('#Record-' + $(this).attr('id')).val(ECOsystem.Utilities.FormatNum(numRecord));
                    totalCost = totalCost + numCost;
                    totalRecord = totalRecord + numRecord;
                    $('#Cost-total').val(ECOsystem.Utilities.FormatNum(totalCost));
                    $('#Record-total').val(ECOsystem.Utilities.FormatNum(totalRecord));
                }
            )   
        }

        return {
            Init: initialize,
            Sumatory: sumatory//,
            //AddOrEditModalShow: addOrEditModalShow
        };

    })();