﻿var ECO_Control = ECO_Control || {};

ECO_Control.BudgetChangesReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();       
        initializeDropDownList();
    }

    /*init Events*/
    var initEvents = function () {

            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                $('#datetimepickerEndDate').datepicker('remove');

                var dateV = $('#StartDateStr').val();

                $('#datetimepickerEndDate').datepicker({
                    language: 'es',
                    startDate: dateV,
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    todayHighlight: true
                });

                $("#datetimepickerEndDate").datepicker('update');
                $('#EndDateStr').val();
            });

            $("#StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });           

            $("#EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });

            initDatePickerTime();
    }  

    var initDatePickerTime = function () {
        $('#datetimepickerStartDate').datepicker("setDate", new Date());
        $('#datetimepickerEndDate').datepicker("setDate", new Date());
    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        
        var select4 = $("#CostCenterId").select2().data('select2');
        select4.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
            }
        })(select4.onSelect);
    };

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Month").select2('val', '');
            $("#Year").select2('val', '');
            $('#StartDateStr').val('');
            $('#EndDateStr').val('');

            $('#ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");           
        }
    }; 

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
            '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
            ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' +
            'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        if (($('#ReportCriteriaId').val() === '')
            || ($('#Month').val() === '')
            || ($('#Year').val() === '')
            || ($('#StartDateStr').val() === '')
            || ($('#EndDateStr').val() === '')
            || ($('#CostCenterId').val() === '')
        ) {
            if (($('#Month').val() !== '') && ($('#Year').val() !== '') && ($('#CostCenterId').val() !== ''))
                return true;
            if (($('#StartDateStr').val() !== '') && ($('#EndDateStr').val() !== '') && ($('#CostCenterId').val() !== ''))
                return true;
            invalidSelectOption();
            return false;
        }
        return true;
    };

    /*Excecute the excel download*/
    var onClickDownloadReport = function () {
        if (checkAllSelects()) $('#ExcelReportDownloadForm').submit();
    };  

    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    /*Hide Loader*/
    var hideLoader = function (data) {
        if (isNaN(data)) {
            $('body').loader('hide');
            $('#ControlsContainer').html('<br />' +
                '<div class="alert alert-danger alert-dismissible fade in" role="alert">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Ocurrió un error al procesar el reporte, por favor intentelo de nuevo. <hr />' +
                '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
        } else if (data > 0) {
            $('body').loader('hide');
            $('#DownloadFileForm').submit();
            $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;El reporte muestra los movimientos de presupuesto.</div>');
        } else {
            $('body').loader('hide');
            $('#ControlsContainer').html('<div class="alert alert-warning alert-dismissible fade in" role="alert">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;No datos encontrados con los criterios de busqueda.</div>');
        }
    };
    

    /* Public methods */
    return {
        Init: initialize,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnClickDownloadReport : onClickDownloadReport
    };
})();

