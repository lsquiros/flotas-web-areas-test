﻿var ECO_Control = ECO_Control || {};

ECO_Control.CostCentersByCredit = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        initializeDropDownList();

        initCurrency();

        onClickbtnAppliesMassive();

        setAppliesMassiveValidation();

        validateInputValues();

        try {
            $("#applyAllForm").data("validator").settings.submitHandler = function (e) {
                $('#ControlsContainer').find('#processing').removeClass('hide');
                $('#ControlsContainer').find('button[data-btnmodal=true]').attr("disabled", "disabled");
            };
        } catch (e) {

        }

    };

    /* Show or hide errors messages*/
    var setAmountErrorMsj = function (msj, show, typeError) {

        typeError = typeError != null ? typeError : 'e1';

        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').removeClass('hide');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').addClass('hide');
                }
                break;
            default:
                break;
        }
    
        
    }

    var validateInputValues = function () {

        $('#gridContainer').off('click.ApplyAmount', '#actualizarMontos').on('click.ApplyAmount', '#actualizarMontos', function () {

            var isValidValues = true;

            $('#gridContainer').find('input[data-form-value]').each(function (i) {
                var value = parseInt($(this).attr('value'));
                if (value <= 0) {
                    isValidValues = false;
                }
            });

            if (isValidValues) {
                setAmountErrorMsj('', true, 'e2');
                $('#confirmModal2').modal('show');
            } else {
                setAmountErrorMsj('Los Montos establecidos son requeridos y debe ser mayor que 0.00', true, 'e2');
            }
        });
    };

    /*Set Applies Massive Validation*/
    var setAppliesMassiveValidation = function() {

        $('#applyAllForm').off('click.btnSetAppliesMassive', '#btnSetAppliesMassive').on('click.btnSetAppliesMassive', '#btnSetAppliesMassive', function (e) {
            setAmountErrorMsj('', false);
            var value = $('#Data_ValueStr').val();
            if (value.length == 0 || ECOsystem.Utilities.ToNum(value) == 0)
            {
                setAmountErrorMsj('El Monto es requerido y debe ser mayor que 0.00', true);
                return false;
            }
            return true;
        });
    }

    /* On click Btn Applies Massive*/
    var onClickbtnAppliesMassive = function () {
        $('#btnAppliesMassive').off('click.btnAppliesMassive').on('click.btnAppliesMassive', function (e) {
           
            $('#gridContainer').find('input[data-form-value]').each(function (i) {
                $(this).attr('name', 'list[' + i + '].Value');
            });
            $('#gridContainer').find('input[data-form-vehicle]').each(function (i) {
                $(this).attr('name', 'list[' + i + '].VehicleId');
            });

            return true;
        });
    };

    var initCurrency = function () {
        try {
            $('#wrapper').off('blur.currencyOnBlur', 'input[data-currency]').on('blur.currencyOnBlur', 'input[data-currency]', function (e) {
                var obj = $(this);
                obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
                obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));
            });
            $('#wrapper').find('input[data-currency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });

        } catch (e) {

        }
    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#CostCenterId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectGroupChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);
        

    };

    /*on Select Group Change*/
    var onSelectGroupChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#CostCenterId').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Month').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Year').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function() {
        if (($('#Year').val() === '') || ($('#Month').val() === '') || ($('#CostCenterId').val() === '')) {
            invalidSelectOption();
            return false;
        }
        return true;
    };

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                '<span aria-hidden="true">×</span>' +
                                            '</button>' +
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };


    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        $('#confirmModal').modal('hide');
        $('#confirmModal2').modal('hide');

        $('#ControlsContainer').find('#processing').addClass('hide');
        $('#ControlsContainer').find('button[data-btnmodal=true]').removeAttr("disabled");

        $('#Data_Value').val('');
        $('#Data_ValueStr').val('');
        
        $('#gridContainer').find("span[data-toggle=tooltip]").tooltip('hide');
    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad
    };
})();
