﻿var ECO_Control = ECO_Control || {};

ECO_Control.FuelsByCredit = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        initControls();
    };

    var initControls = function () {
        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        $('#btnSubmit').off('click.btnSubmit').on('click.btnSubmit', function (e) {
            var result = true;
            $('#FormContent').find('input[IsAmount="true"]').each(function () {
                result = result && inputValidation(this);
            });
            if (result) $('#SubmitForm').submit();

            $('#confirmModal').modal('hide');
        });

        $("#SubmitForm").off('submit.SubmitForm').on('submit.SubmitForm', function () {
            $('body').loader('show');
            $(this).find('#btnSubmit').attr("disabled", "disabled");
            $(this).find('#btnCancel').attr("disabled", "disabled");
        });

        var formContent = $('#FormContent');
        formContent.off('blur.inputOnBlur', 'input[IsAmount="true"]').on('blur.inputOnBlur', 'input[IsAmount="true"]', inputOnBlur);
        formContent.find('input[IsAmount="true"]').numeric({
            allowMinus: false,
            maxDecimalPlaces: 2
        });

        formContent.off('blur.inputOnBlur', 'input[IsAmount="false"]').on('blur.inputOnBlur', 'input[IsAmount="false"]', inputOnBlurLitters);
        formContent.find('input[IsAmount="false"]').numeric({
            allowMinus: false,
            maxDecimalPlaces: 2
        });

        $("#CostCenterId").select2().data('select2');
    };

    var onSelectCostCenterChange = function () {
        var costCenterId = $("#CostCenterId").val();
        $('body').loader('show');
        $.ajax({
            url: '/FuelsByCredit/LoadFuelsByCreditByCostCenter',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ CostCenterId: costCenterId }),
            success: function (result) {
                $('#FuelsByCreditsPanel').html(result);
                initControls();  
                $('body').loader('hide');
            }
        });
    };


    /*Convert str to Number*/
    var toNum = function (num) {
        num = ("" + num).replace(/\./g, '').replace(/,/g, '.');
        return isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
    }

    /*Convert str to Float*/
    var toFloat = function (num) {
        num = ("" + num).replace(/,/g, '');
        return isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
    }

    /*format Number*/
    var formatNum = function (num) {
        return num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    /*format Currency with Symbol*/
    var formatCurrency = function (num, s) {
        return s + '' + num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    /* input OnBlur */
    var inputOnBlur = function (e) {
        inputValidation(this);
    }

    /* input OnBlur */
    var inputOnBlurLitters = function (e) {
        var id = this.id.substr(0, this.id.lastIndexOf("_") + 1);
        if (toNum(formatNum(toFloat($('#' + id + 'Liters').val()))) === toNum($(this).val()))
            return;

        var value = toNum($(this).val());
        var amountInput = $('#' + id + 'TotalStr');

        var value = toNum($(this).val());
        var LiterPrice = $('#' + id + 'LiterPrice').val();
        var ConversionValue = $('#ConversionValue').val();
        LiterPrice = (LiterPrice == "" || isNaN(LiterPrice) || LiterPrice == null) ? "0" : LiterPrice;
        ConversionValue = (ConversionValue == "" || isNaN(ConversionValue) || ConversionValue == null) ? "0" : ConversionValue;
        amountInput.val(formatNum(convertToAmoutOrLiters(value, true, LiterPrice, ConversionValue, $('#CapacityUnitId').val())));

        if (!inputValidation(amountInput[0])) {
            $(this).val(formatNum(toFloat($('#' + id + 'Liters').val())));
            $(this).focus();
            $(this).select();

        } else {
            $('#' + id + 'Liters').val(value);

        }
    }


    /* input OnBlur Validation */
    var inputValidation = function (obj) {

        var id = obj.id.substr(0, obj.id.lastIndexOf("_") + 1);
        if (toNum(formatNum(toFloat($('#' + id + 'Total').val()))) === toNum($(obj).val()))
            return true;

        var value = toNum($(obj).val());
        var totalAssigned = 0;
        $('input[IsAmount="true"]').each(function () {
            totalAssigned += toNum($(this).val());
        });

        var validation = $(obj).parents('.line').next('.error');
        $(validation).addClass('hide');

        var LiterPrice = $('#' + id + 'LiterPrice').val();
        var ConversionValue = $('#ConversionValue').val();
        //LiterPrice = (LiterPrice == "" || isNaN(LiterPrice) || LiterPrice == null) ? "0" : LiterPrice;
        ConversionValue = (ConversionValue == "" || isNaN(ConversionValue) || ConversionValue == null) ? "0" : ConversionValue;

        var FuelName = $('#' + id + 'FuelName').val();
        $(obj).val(formatNum(toNum($('#' + id + 'Total').val())));

        if (LiterPrice == "" || isNaN(LiterPrice) || LiterPrice == null) {
            $(validation).removeClass('hide');
            $(validation).find('span').html('El precio de este combustible se encuentra vencido, favor actualizar el precio para poder distribuir presupuesto.');
            $(obj).val(formatNum(toFloat($('#' + id + 'Total').val())));
            $(obj).focus();
            $(obj).select();
            return false;
        }

        var totalCreditAmount = $('#TotalCreditAmount').val();
        var currencySymbol = $('#CurrencySymbol').val();

        if (value < FuelName) {

            $(validation).removeClass('hide');
            $(validation).find('span').html('El valor digitado no puede ser menor a lo ya distribuido para este combustible: ' + formatCurrency(toFloat(FuelName), currencySymbol) +
                ' o a su equivalente de ' + formatNum(convertToAmoutOrLiters(toFloat(FuelName), false, LiterPrice, ConversionValue, $('#CapacityUnitId').val())) + ' Litros' +
                '. Debe de digitar un valor mayor a la sumatoria de lo ya distribuído en los vehículos');
            $(obj).val(formatNum(toFloat($('#' + id + 'Total').val())));
            $(obj).focus();
            $(obj).select();
            return false;
        }


        if (totalAssigned > totalCreditAmount) {
            totalAssigned = totalAssigned - value;
            value = totalCreditAmount - totalAssigned;
            totalAssigned = totalCreditAmount;
            $(validation).removeClass('hide');
            $(validation).find('span').html('El valor digitado sobrepasa el disponible, Favor indicar un valor menor o igual a: ' + formatCurrency(value, currencySymbol) +
                ' o a su equivalente de ' + formatNum(convertToAmoutOrLiters(value, false, LiterPrice, ConversionValue, $('#CapacityUnitId').val())) + ' Litros');
            $(obj).val(formatNum(toFloat($('#' + id + 'Total').val())));
            $(obj).focus();
            $(obj).select();
            return false;
        }

        $('#' + id + 'Total').val(value);
        $('#' + id + 'CapacityUnitValue').val(formatNum(convertToAmoutOrLiters(value, false, LiterPrice, ConversionValue, $('#CapacityUnitId').val())));


        $(obj).val(formatNum(value));
        var pctnum = parseInt(((totalAssigned / totalCreditAmount) * 100), 10);
        var pct = pctnum + '%';
        var pctstr = "(" + pct + ")";
        var pctclass = pctnum > 80 ? "danger" : pctnum > 40 ? "warning" : "success";

        $('#AssignedCreditPct').width(pct);
        $('#AssignedCreditPct').removeClass("progress-bar-danger");
        $('#AssignedCreditPct').removeClass("progress-bar-warning");
        $('#AssignedCreditPct').removeClass("progress-bar-success");
        $('#AssignedCreditPct').addClass("progress-bar-" + pctclass);
        $('#AssignedCreditAmt').width(pct);
        $('#AssignedCreditAmt').attr('data-val', formatCurrency(totalAssigned, currencySymbol) + pctstr);

        return true;
    }

    var convertToAmoutOrLiters = function (value, toAmount, literPrice, conversionValue, capacityUnitId) {
        var result;
        var factor = 1;
        var factorConvertion = 1;
        if (toAmount) {
            factor = 1 * toFloat(literPrice);
            factorConvertion = 1 * toFloat(conversionValue);
        }
        else {
            factor = 1 / toFloat(literPrice);
            factorConvertion = 1 / toFloat(conversionValue);
        }

        if (capacityUnitId == '0') {
            result = value * factor;
            result = (result == Infinity || isNaN(result)) ? 0 : result;

        } else {
            result = (value * factor) * factorConvertion;
            result = (result == Infinity || isNaN(result)) ? 0 : result;
        }
        return result;
    }


    /* Public methods */
    return {
        Init: initialize,
        OnSelectCostCenterChange: onSelectCostCenterChange
    };
})();

