﻿var ECO_Control = ECO_Control || {};

ECO_Control.FuelDistributionByCostCenter = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        initEditControls();

        $('#btnImport').click(function () {
            $('body').loader('show');
            $('#ImportModal').modal('hide');
        });

        var downloadfile = $('#downloadFile').val();
        if (downloadfile) $('#DonwloadResultFileForm').submit();

        var resultdata = $('#resultData').val();
        if (resultdata != '' && resultdata != undefined && resultdata != null) showresultmodal(resultdata);

        var invalidImportAmount = $('#invalidImportAmount').val();
        if (invalidImportAmount != '' && invalidImportAmount != undefined && invalidImportAmount != null) invalidImportAmountModal();
    }

    var showresultmodal = function (resultdata) {
        $('#resultimportdate').html(resultdata.split('|')[0]);
        $('#resultapproval').html(resultdata.split('|')[1]);
        $('#resultnoapproval').html(resultdata.split('|')[2]);
        $('#resulttotal').html(resultdata.split('|')[3]);
        if (resultdata.split('|')[2] >= 1) {
            $('#msgNoProcess').show();
        }
        $('#SummaryResultModal').modal('show');
    }

    var invalidImportAmountModal = function () {
        $('#invalidImportAmountModal').modal('show');
    }

    var onClickImport = function () {
        $('#ImportCSV').trigger('click');
        $("#txtFile").val("");
    };

    var getFilename = function (input) {
        if (ValidateFormat(input)) {
            if (input.files && input.files[0]) {
                var value = $("#ImportCSV").val();
                var NameOfFile = input.files[0].name;
                $("#txtFile").val(NameOfFile);
                setErrorFormatMsj('', false);
            }
        }
        else {
            $("#txtFile").val("");
            setErrorFormatMsj('* El formato de archivo es inválido, favor seleccionar un archivo de formato CSV.', true);
        }
    }

    var setErrorFormatMsj = function (msj, show) {
        if (show) {
            $('#errorFormatMsj').html(msj);
            $('#validationFormatErrors').removeClass('hide');
        } else {
            $('#errorFormatMsj').html('');
            $('#validationFormatErrors').addClass('hide');
        }
    }

    var ValidateFormat = function (input) {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var files = input.files;
            for (var i = 0, f; f = files[i]; i++) {
                if (!f.name.match('\.csv')) {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    };

    var initEditControls = function () {
        $('.costcenteramount').off('blur.costcenteramount').on('blur.costcenteramount', function () {
            amountChange($(this));
        });

        $('#btnSaveData').click(function () {
            $('#confirmModal').modal('show');
        });

        $('#btnSubmitSaveData').click(function () {
            $('body').loader('show');
            $('#confirmModal').modal('hide');
            $('#FuelDistributionByCostCenterAddOrEditForm').submit();
        });

        $('#txtSearchCostCenter').keypress(function (e) {
            if (e.which == 13) {
                $('#FuelDistributionByCostCenterSearchForm').find('#txtSearch').val($(this).val());
                $('#FuelDistributionByCostCenterSearchForm').submit();
            }
        });

        $('#btnSearch').click(function () {
            $('#FuelDistributionByCostCenterSearchForm').find('#txtSearch').val($('#txtSearchCostCenter').val());
            $('#FuelDistributionByCostCenterSearchForm').submit();
        });

        $('#txtSearchCostCenter').off('blur.txtSearchCostCenter').on('blur.txtSearchCostCenter', function (e) {
            $('#searchControls').hide();
        });

        $('#btnSubmitCancelData').click(function () {
            $('body').loader('show');
            $('#confirmCancelModal').modal('hide');
            $('#FuelDistributionByCostCenterCancelForm').submit();
        });
    }

    var amountChange = function (obj) {
        var costcenterid = obj.attr('costcenterid');
        var amount = setAmountFormat(obj.val());
        $('.errormsg').hide();
        $('.errordiv').hide();  
        enableControlButtons(true);
        $.ajax({
            url: '/FuelDistributionByCostCenter/FuelDistributionChance',
            type: 'POST',
            data: JSON.stringify({ CostCenterId: costcenterid, Amount: amount }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {
                obj.val(result.amount);
                enableControlButtons(false);
                if (result.success) {
                    obj.closest('tr').addClass('SelectColor');
                    validateFormat(result);
                    $('#errormsg-' + costcenterid).hide();
                    $('#errordiv-' + costcenterid).hide();
                } else if (result.message) {
                    $('#errormsg-' + costcenterid).html(result.message);
                    $('#errormsg-' + costcenterid).show();
                    $('#errordiv-' + costcenterid).show();
                }
            }
        });
    }

    var enableControlButtons = function (disable) {
        $('#controlButtonContainer :button').attr('disabled', disable);
    }

    var validateFormat = function (result) {
        var pctnum = parseInt(((result.totalAssigned / result.totalCreditAmount) * 100), 10);
        var pct = pctnum + '%';
        var pctstr = "(" + pct + ")";
        var pctclass = pctnum > 80 ? "danger" : pctnum > 40 ? "warning" : "success";

        $('#AssignedCreditPct').width(pct);
        $('#AssignedCreditPct').removeClass("progress-bar-danger");
        $('#AssignedCreditPct').removeClass("progress-bar-warning");
        $('#AssignedCreditPct').removeClass("progress-bar-success");
        $('#AssignedCreditPct').addClass("progress-bar-" + pctclass);
        $('#AssignedCreditAmt').width(pct);
        $('#AssignedCreditAmt').attr('data-val', result.totalAssignedStr + pctstr);
    }

    var setAmountFormat = function (amount) {
        var format = amount.replace('.', '');
        format = format.replace(',', '.');
        format = format.replace(/\s+/g, '');;
        return format;
    }

    var showSearchControls = function () {
        $('#searchControls').show();
        $('#txtSearchCostCenter').focus();
    }

    var onSuccessSearch = function (result) {
        $('#gridContainer').html(result);
        initEditControls();
        $('#txtSearchCostCenter').focus();
    }

    return {
        Init: initialize,
        OnClickImport: onClickImport,
        GetFilename: getFilename,
        ShowSearchControls: showSearchControls,
        OnSuccessSearch: onSuccessSearch
    };
})();