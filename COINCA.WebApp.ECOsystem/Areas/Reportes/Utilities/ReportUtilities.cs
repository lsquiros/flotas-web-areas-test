﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;

namespace COINCA.WebAPI.ECOSystemReports.Business
{
    public class ReportUtilities : IDisposable
    {
        /// <summary>
        /// Create Report DataTable
        /// </summary>
        /// <param name="Datos"></param>
        /// <param name="Report_Name"></param>
        /// <param name="username"></param>
        /// <param name="productInfo"></param>
        /// <param name="OdometerType"></param>
        /// <returns></returns>
        public HttpResponseMessage CreateReportDataTable(DataTable Datos, string Report_Name, string username, string productInfo, string nameSpace, int? OdoType)
        {
         
            string path = HttpContext.Current.Server.MapPath("~/Areas/Reportes/ReportsData/" + nameSpace + "/" + Report_Name + "/");
            string OdometerType = OdoType == null ? "" : (Convert.ToInt16(OdoType) == 1 ? "Erróneos" : "Historial");
            string OperationName = string.Empty;
            string dataSetName = Report_Name;
            string logopath = "file:///" + HttpContext.Current.Server.MapPath("~/Content/Images/BAC/Flota.png");
            string Site = "flotas.baccredomatic.com";
            string URL = "https://flotas.baccredomatic.com/";
            switch (productInfo)
            {
                case "IntrackECO":
                    logopath = "file:///" + HttpContext.Current.Server.MapPath("~/Content/Images/logo.png");
                    Site = "www.intrack-eco.com";
                    URL = "https://www.intrack-eco.com/ECOSystem/";
                    break;
                case "LineVitaECO":
                    logopath = "file:///" + HttpContext.Current.Server.MapPath("~/Content/Images/LineVita/logo_report.png");
                    Site = "www.line-vita.com";
                    URL = "http://www.line-vita.com/";
                    break;
            }
            #region ReportWithOtherDatasetNames
            switch (Report_Name)
            {
                case "TransactionDetailReportGV":
                    dataSetName = "TransactionDetailReport";
                    path = HttpContext.Current.Server.MapPath("~/ReportsData/" + nameSpace + "/" + dataSetName + "/");
                    break;
                case "ScoreSpeedRouteReport":
                    OperationName = Report_Name;
                    dataSetName = "ScoreSpeedReport";
                    Report_Name = "ScoreSpeedReport";
                    path = HttpContext.Current.Server.MapPath("~/ReportsData/" + nameSpace + "/" + dataSetName + "/");
                    break;
                case "ScoreSpeedAdminReport":
                    OperationName = Report_Name;
                    dataSetName = "ScoreSpeedReport";
                    Report_Name = "ScoreSpeedReport";
                    path = HttpContext.Current.Server.MapPath("~/ReportsData/" + nameSpace + "/" + dataSetName + "/");
                    break;
                case "ScoreRouteGlobalReport":
                    OperationName = Report_Name;
                    dataSetName = "ScoreGlobalReport";
                    Report_Name = "ScoreGlobalReport";
                    path = HttpContext.Current.Server.MapPath("~/ReportsData/" + nameSpace + "/" + dataSetName + "/");
                    break;
                case "ScoreAdminGlobalReport":
                    OperationName = Report_Name;
                    dataSetName = "ScoreGlobalReport";
                    Report_Name = "ScoreGlobalReport";
                    path = HttpContext.Current.Server.MapPath("~/ReportsData/" + nameSpace + "/" + dataSetName + "/");
                    break;
                case "CreditCardsReportByClient":
                    dataSetName = "dsCreditCardReportByClient";
                    break;
                case "CreditCardsReportDetailByClient":
                    dataSetName = "dsCreditCardReportByClient";
                    path = HttpContext.Current.Server.MapPath("~/ReportsData/" + nameSpace + "/CreditCardsReportByClient/");
                    break;
            }
            #endregion            
            ReportViewer ReportViewer1 = new ReportViewer();
            ReportViewer1.Reset();
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = path + Report_Name + ".rdlc";
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(dataSetName, Datos));
            ReportViewer1.LocalReport.EnableHyperlinks = true;
            ReportViewer1.LocalReport.EnableExternalImages = true;
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("UserName", username, false));
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("URL", URL, false));
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("Site", Site, false));
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("PictureLogo", logopath, true));
            if (Report_Name == "SettingOdometerReport")
            {
                ReportViewer1.LocalReport.SetParameters(new ReportParameter("OdometerType", OdometerType, true));
            }
            else if (Report_Name == "MonthlyBillReport" || Report_Name == "VPosTransactionBill")
            {
                var baclogo = "file:///" + HttpContext.Current.Server.MapPath("~/Content/Images/BAC/Logo_BAC_Credomatic_Horizontal.png");
                ReportViewer1.LocalReport.SetParameters(new ReportParameter("BACImage", baclogo, true));
            }
             switch (OperationName)
            {
                case "ScoreSpeedRouteReport":
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter("ReportName", "Calificación Hábitos de Conducción según Velocidad de Ley por Conductor", false));
                    break;
                case "ScoreSpeedAdminReport":
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter("ReportName", "Calificación Hábitos de Conducción según Velocidad Delimitada por la Empresa por Conductor", false));
                    break;
                case "ScoreRouteGlobalReport":
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter("ReportName", "Índice Ponderado de Hábitos de Conducción según Velocidad de Ley", false));
                    break; 
                case "ScoreAdminGlobalReport":
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter("ReportName", "Índice Ponderado de Hábitos de Conducción según Velocidad Delimitada por la Empresa", false));
                    break; 
            }                        
            ReportViewer1.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);            
            result.Content = new ByteArrayContent(bytes);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");           
            return result; 
        }

        /// <summary>
        /// Create Report DataSet
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="Report_Name"></param>
        /// <param name="username"></param>
        /// <param name="productInfo"></param>
        /// <returns></returns>
        public HttpResponseMessage CreateReportDataSet(DataSet ds, string Report_Name, string username, string productInfo, string nameSpace)
        {
            HttpContext.Current.Session["nombreSubReporte"] = Report_Name.Replace("Report", "Detail");
            HttpContext.Current.Session["datosReporte"] = ds;

            string path = HttpContext.Current.Server.MapPath("~/ReportsData/" + nameSpace + "/" + Report_Name + "/");
            string dataSetName = Report_Name;           
            string logopath = "file:///" + HttpContext.Current.Server.MapPath("~/Content/Images/BAC/Flota.png");
            string Site = "flotas.baccredomatic.com";
            string URL = "https://flotas.baccredomatic.com/";
            switch (productInfo)
            {
                case "IntrackECO":
                    logopath = "file:///" + HttpContext.Current.Server.MapPath("~/Content/Images/logo.png");
                    Site = "www.intrack-eco.com";
                    URL = "http://www.intrack-eco.com:8081/intrack/";
                    break;
                case "LineVitaECO":
                    logopath = "file:///" + HttpContext.Current.Server.MapPath("~/Content/Images/LineVita/logo_report.png");
                    Site = "www.line-vita.com";
                    URL = "http://www.line-vita.com/";
                    break;
            }
            ReportViewer ReportViewer1 = new ReportViewer();
            ReportViewer1.Reset();
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = path + Report_Name + ".rdlc";
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(dataSetName, ds.Tables[dataSetName]));
            ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSource);
            if (Report_Name == "ApprovalTransactionsReport" || Report_Name == "ConsolidateExpensesReport" || Report_Name == "CustomReport" || Report_Name == "BillingReport" || Report_Name == "ProviderBillingReport" ||
                Report_Name == "ConsolidatedReport")
            {
                HttpContext.Current.Session["nombreSubSReporte"] = Report_Name.Replace("Report", "ColumnValues");
                ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubSDataSource);

                //Se agrega validacion para un tercer sub-reporte que lleva el control de las sumatorias de las columnas
                if(Report_Name == "ProviderBillingReport")
                {
                    HttpContext.Current.Session["nombreSubSSReporte"] = Report_Name.Replace("Report", "SumColumnValues");
                    ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubSSDataSource);
                }
            }
            ReportViewer1.LocalReport.EnableHyperlinks = true;
            ReportViewer1.LocalReport.EnableExternalImages = true;
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("UserName", username, true));
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("URL", URL, false));
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("Site", Site, false));
            ReportViewer1.LocalReport.SetParameters(new ReportParameter("PictureLogo", logopath, true));            
            ReportViewer1.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);            
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(bytes);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            return result;
        }

        /// <summary>
        /// Set Sub Data Source
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetSubDataSource(object sender, SubreportProcessingEventArgs e)
        {
            string ReportDetail = HttpContext.Current.Session["nombreSubReporte"].ToString();
            DataSet ds = (DataSet)HttpContext.Current.Session["datosReporte"];
            e.DataSources.Add(new ReportDataSource(ReportDetail, ds.Tables[ReportDetail]));
        }

        /// <summary>
        /// Set Sub Data Source
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetSubSDataSource(object sender, SubreportProcessingEventArgs e)
        {
            string ReportDetail = HttpContext.Current.Session["nombreSubSReporte"].ToString();
            DataSet ds = (DataSet)HttpContext.Current.Session["datosReporte"];
            e.DataSources.Add(new ReportDataSource(ReportDetail, ds.Tables[ReportDetail]));
        }

        /// <summary>
        /// Set Sub Data Source
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetSubSSDataSource(object sender, SubreportProcessingEventArgs e)
        {
            string ReportDetail = HttpContext.Current.Session["nombreSubSSReporte"].ToString();
            DataSet ds = (DataSet)HttpContext.Current.Session["datosReporte"];
       
            var dtCopy = ds.Tables[ReportDetail.Replace("SumColumnValues", "ColumnValues")].Copy();           
            dtCopy.TableName = ReportDetail;
          
            e.DataSources.Add(new ReportDataSource(ReportDetail, dtCopy));
        }

        /// <summary>
        /// GC
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}