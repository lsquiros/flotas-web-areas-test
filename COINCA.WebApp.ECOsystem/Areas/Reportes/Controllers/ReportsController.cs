﻿using COINCA.WebAPI.ECOSystemReports.Business;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;



using SharpRaven;
using SharpRaven.Data;
using System.Configuration;

namespace COINCA.WebAPI.ECOSystemReports.Controllers
{
    public class ReportsController : ApiController
    {
        [Route("reports/reportdatatable")]
        public HttpResponseMessage ReportDataTable([FromBody]string Datos)
        {
            try
            {
                int? OdoType = Datos.Split('|').Length > 5 ? Convert.ToInt16(Datos.Split('|')[5]) : (int?)null;
                using (var bus = new ReportUtilities())
                {
                    return bus.CreateReportDataTable(JsonConvert.DeserializeObject<DataTable>(Datos.Split('|')[0]), Datos.Split('|')[1], Datos.Split('|')[2], Datos.Split('|')[3], Datos.Split('|')[4], OdoType);
                }
            }
            catch (Exception ex)
            {
                var ravenClient = new RavenClient(ConfigurationManager.AppSettings["SentryDSN"]);
                ravenClient.Environment = ConfigurationManager.AppSettings["SentryEnv"];

                ravenClient.Capture(new SentryEvent(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Error = ex.Message }, "application/json");
            }
        }

        [Route("reports/reportdataset")]
        public HttpResponseMessage ReportDataset([FromBody]string Datos)
        {
            try
            {
                using (var bus = new ReportUtilities())
                {
					JsonSerializerSettings setting = new JsonSerializerSettings();
					setting.DateParseHandling = DateParseHandling.None;
					return bus.CreateReportDataSet(JsonConvert.DeserializeObject<DataSet>(Datos.Split('|')[0],setting), Datos.Split('|')[1], Datos.Split('|')[2], Datos.Split('|')[3], Datos.Split('|')[4]);
                }            
            }
            catch (Exception ex)
            {
                var ravenClient = new RavenClient(ConfigurationManager.AppSettings["SentryDSN"]);
                ravenClient.Environment = ConfigurationManager.AppSettings["SentryEnv"];

                ravenClient.Capture(new SentryEvent(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Error = ex.Message }, "application/json");
            }
        }
    }
}
