﻿using ECO_ServiceStations.Business;
using ECO_ServiceStations.Models;
using ECO_ServiceStations.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_ServiceStations.Controllers.ServiceStations
{
    public class StationTransactionsReportController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                return View(new StationTransactionsReportBase()
                {
                    List = new List<StationTransactionsReport>()
                });
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DownloadReport(ControlFuelsReportsBase parameters)
        {
            try
            {
                using (var bus = new StationTransactionsReportBusiness())
                {
                    var ds = bus.StationTransactionsReportRetrieve(parameters);

                    if (ds.Tables[1].Rows.Count == 0)
                    {
                        return Json("No Data");
                    }
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Generar el reporte de transacciones por estaciones de servicio. Parámetros: " +
                                                                                    "CustomerId: {0}, ServiceStationsId: {1}, Mes: {2}, Año: {3}, FechaInicio: {4}, FechaFinal: {5}, Usuario: {5}.",
                                                                                    parameters.CustomerId, parameters.ServiceStationId, parameters.Month, parameters.Year, parameters.StartDate,
                                                                                    parameters.EndDate, ECOsystem.Utilities.Session.GetUserInfo().UserId));
                    Session["Report"] = ds;
                    return Json("Success");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al generar el reporte de transacciones por estaciones de servicio. Error: {0}", e.Message));
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataSet report = new DataSet();
                if (Session["Report"] != null)
                {
                    report = (DataSet)Session["Report"];
                    Session["Report"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte transacciones por estaciones de servicio");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "StationTransactionsReport", this.ToString().Split('.')[2], "Reporte de Transacciones por Estación de Servicio");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al descargar el reporte de transacciones por estaciones de servicio. Error: {0}", e.Message));
                ViewBag.ErrorReport = 1;
                return View("Index", new StationTransactionsReportBase() { List = new List<StationTransactionsReport>() });
            }
        }
    }
}