﻿using ECO_ServiceStations.Business;
using ECO_ServiceStations.Models;
using ECO_ServiceStations.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;


using System;
using System.Collections.Generic;

using System.Web.Mvc;

namespace ECO_ServiceStations.Controllers
{
    public class ServiceStationsHomeController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new ServiceStationsBusiness())
                {
                    var model = business.StationTransactionsResumeRetrieve();
                    model.Menus = new List<AccountMenus>();
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
