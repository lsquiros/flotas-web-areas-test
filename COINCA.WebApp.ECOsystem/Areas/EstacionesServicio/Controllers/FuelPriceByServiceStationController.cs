﻿using ECO_ServiceStations.Models;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECO_ServiceStations.Business;
using ECOsystem.Business.Utilities;
using System.Web.Script.Serialization;
using ECO_ServiceStations.Models.Identity;
using System.IO;
using ECOsystem.Utilities;
using System.Data;
using System.Data.OleDb;
using System.Globalization;




namespace ECO_ServiceStations.Controllers
{
    public class FuelPriceByServiceStationController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var bus = new FuelPriceByServiceStationsBusiness())
                {
                    var model = new FuelPriceByServiceStationBase()
                    {
                        List = bus.FuelPriceByServiceStationRetrieve(),
                        Menus = new List<AccountMenus>()
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult FuelPriceByServiceStationAddOrEdit(List<FuelPriceByServiceStation> list)
        {
            try
            {
                using (var bus = new FuelPriceByServiceStationsBusiness())
                {
                    bus.FuelPriceByServiceStationAddOrEdit(list);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificación de los precios de combustible para estaciones de servicio. Valores: {0}", new JavaScriptSerializer().Serialize(list)));

                    return Json("Success");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al modificar los precios de combustible. Error {0}", e.Message));
                return Json("Error");
            }
        }

        public ActionResult ShowProgramFuels(int? serviceStationId)
        {
            try
            {
                using (var bus = new FuelPriceByServiceStationsBusiness())
                {
                    return PartialView("_partials/_ProgramFuelPrice", bus.ProgramFuelPriceByServiceStationRetrieve(serviceStationId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ProgramFuelPriceByServiceStationAddOrEdit(List<FuelPriceByServiceStation> list)
        {
            try
            {
                using (var bus = new FuelPriceByServiceStationsBusiness())
                {
                    bus.ProgramFuelPriceByServiceStationAddOrEdit(list);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificación de los precios de combustible para estaciones de servicio. Valores: {0}", new JavaScriptSerializer().Serialize(list)));

                    return Json("Success");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al modificar los precios de combustible. Error {0}", e.Message));
                return Json("Error");
            }
        }

        /// <summary>
        /// Template used for fuel prices import 
        /// </summary>
        /// <returns>Template file</returns>
        [EcoAuthorize]
        public ActionResult DownloadTemplate()
        {
            try
            {
                byte[] report;
                using (var bus = new FuelPriceByServiceStationsBusiness())
                {
                    List<FuelPriceByServiceStationExcel> List = bus.FuelPriceByServiceStationRetrieve_Simple();
                    using (var excel = new ExcelReport())
                    {
                        excel.HasCustomRows = true;
                        excel.IndexHeaderCustomRow = 1;
                        excel.IndexBodyCustomRow = 2;
                        report = excel.CreateSpreadsheetWorkbook(null, List);
                        return new FileStreamResult(new MemoryStream(report), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                        {
                            FileDownloadName = "Plantilla de Importación de Precio de Combustible.xlsx"
                        };
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Import services station prices
        /// </summary>
        /// <param name="txtStartDateImport">Start date</param>
        /// <param name="txtStartHourImport">Start hour</param>
        /// <returns>Return the status: success or failed (message)</returns>
        [EcoAuthorize]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult ImportFile(string txtStartDateImport, string txtStartHourImport)
        {
            if (Request.HttpMethod == "GET")
            {
                return Redirect("/EstacionesServicio/FuelPriceByServiceStation/Index");
            }

            DataTable excelData = new DataTable();
            var list = new List<FuelPriceByServiceStation>();
            var ErrorList = new List<string>();
            int ProcessCount = 0;
            int NoProcessCount = 0;

            try
            {
                HttpPostedFileBase file = Request.Files[0];
                // Verify that the user selected a file
                if (file != null && file.ContentLength > 0)
                {
                    // extract only the filename
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.GetTempPath() + fileName;
                    file.SaveAs(path);

                    using (var excel = new ExcelReport())
                    {
                        bool isRead = true;
                        try
                        {
                            excelData = excel.ReadExcel(path);
                        }
                        catch
                        {
                            isRead = false;
                        }

                        if (isRead)
                        {
                            if (excelData.Rows.Count > 0)
                            {
                                if (excelData.Columns.Count == 3)
                                {

                                    #region Validate date

                                    DateTime? date = null;

                                    try
                                    {
                                        int year = 0;
                                        int month = 0;
                                        int day = 0;
                                        int hour = 0;
                                        int minute = 0;

                                        if (!string.IsNullOrEmpty(txtStartDateImport))
                                        {
                                            var arrDate = txtStartDateImport.Split('/');
                                            if (arrDate.Length == 3)
                                            {
                                                year = Convert.ToInt32(arrDate[2]);
                                                month = Convert.ToInt32(arrDate[1]);
                                                day = Convert.ToInt32(arrDate[0]);


                                                if (!string.IsNullOrEmpty(txtStartHourImport))
                                                {
                                                    var arrTime = txtStartHourImport.Split(' ');
                                                    if (arrTime.Length == 2)
                                                    {
                                                        if (arrTime[1].ToString().ToLower().Equals("pm"))
                                                            hour = 12;

                                                        arrTime = arrTime[0].Split(':');
                                                        hour = hour + Convert.ToInt32(arrTime[0]);
                                                        minute = Convert.ToInt32(arrTime[1]);

                                                        date = new DateTime(year, month, day, hour, minute, 0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        ErrorList.Add("La fecha ingresada no tiene un formato válido. Se utilizó la fecha de hoy para programar el combustible.");
                                    }
                                    #endregion Validate date

                                    #region Validate File
                                    var excelList = excelData.AsEnumerable().ToList();

                                    int emptyRows = 0;
                                    int line = 1;

                                    foreach (DataRow row in excelList)
                                    {
                                        line++;
                                        try
                                        {
                                            //Service Station
                                            if (string.IsNullOrEmpty(row[0].ToString()))
                                            {
                                                if (string.IsNullOrEmpty(row[1].ToString()) && string.IsNullOrEmpty(row[2].ToString()))
                                                {
                                                    emptyRows++;
                                                }
                                                else
                                                {
                                                    ErrorList.Add(string.Format("Línea {0}. La estación de servicio es inválida.", line));
                                                }
                                                throw new Exception(string.Format("Línea {0}. La estación de servicio es inválida.", line));
                                            }

                                            //Fuel
                                            if (string.IsNullOrEmpty(row[1].ToString()))
                                            {
                                                ErrorList.Add(string.Format("Línea {0}. El tipo de combustible es inválido.", line));
                                                throw new Exception(string.Format("Línea {0}. El tipo de combustible es inválido.", line));
                                            }

                                            //Price
                                            decimal num = 0;
                                            try
                                            {
                                                num = decimal.Parse(row[2].ToString().Replace('.', ','), CultureInfo.GetCultureInfo("es-CR"));
                                            }
                                            catch
                                            {
                                                ErrorList.Add(string.Format("Línea {0}. El precio ingresado para el combustible {1} en la estación de servicio {2} es inválido.", line, row[1].ToString(), row[0].ToString()));
                                                throw new Exception(string.Format("Línea {0}. El precio ingresado para el combustible {1} en la estación de servicio {2} es inválido.", line, row[1].ToString(), row[0].ToString()));
                                            }

                                            FuelPriceByServiceStation elementPrice = new FuelPriceByServiceStation
                                            {
                                                ServiceStationName = row[0].ToString(),
                                                FuelName = row[1].ToString(),
                                                Price = num
                                            };
                                            ProcessCount++;
                                            list.Add(elementPrice);
                                        }
                                        catch (Exception ex)
                                        {
                                            NoProcessCount++;
                                        }
                                    }

                                    #endregion Validate File

                                    using (var bus = new FuelPriceByServiceStationsBusiness())
                                    {
                                        bus.ProgramFuelPriceByServiceStationImport(list, date);
                                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificación de los precios de combustible para estaciones de servicio. Valores: {0}", new JavaScriptSerializer().Serialize(list)));
                                    }

                                    ErrorList.Add(string.Format("El archivo incluye {0} filas sin datos.", emptyRows));
                                }
                                else
                                {
                                    ErrorList.Add("El archivo no fue importado. Las columnas del archivo no son válidas.");
                                }
                            }
                            else
                            {
                                ErrorList.Add("El archivo no fue importado. No posee datos.");
                            }
                        }
                        else
                        {
                            ErrorList.Add("El archivo no fue importado. Verifique que el nombre de las columnas ingresadas no se repita.");
                        }

                        ErrorList.Add(string.Format("Fecha: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                        ErrorList.Add(string.Format("Cantidad de líneas Procesadas: {0}", ProcessCount));
                        ErrorList.Add(string.Format("Cantidad de líneas No Procesadas: {0}", NoProcessCount));
                        ErrorList.Add(string.Format("Cantidad de líneas leídas: {0}", (ProcessCount + NoProcessCount)));

                        using (var ms = new MemoryStream())
                        {
                            var sw = new StreamWriter(ms);
                            foreach (var item in ErrorList)
                            {
                                sw.WriteLine(item);
                                sw.Flush();
                            }
                            Session["SummaryFile"] = ms.ToArray();
                            Session["Path"] = path;
                        }
                        Session["ResultData"] = string.Format("{0}|{1}|{2}|{3}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"),
                                ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(ProcessCount),
                                ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(NoProcessCount),
                                ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(ProcessCount + NoProcessCount));
                    }
                }
                return Redirect("/EstacionesServicio/FuelPriceByServiceStation/Index");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                using (var bus = new FuelPriceByServiceStationsBusiness())
                {
                    var model = new FuelPriceByServiceStationBase()
                    {
                        List = bus.FuelPriceByServiceStationRetrieve(),
                        Menus = new List<AccountMenus>()
                    };
                    return View("Index", model);
                }
            }
        }

        public ActionResult DownloadReportFile()
        {
            try
            {
                byte[] SummaryFile = (byte[])Session["SummaryFile"];
                string path = Session["Path"].ToString();
                System.IO.File.Delete(path);

                Session["SummaryFile"] = null;
                Session["Path"] = null;
                Session["ResultData"] = null;

                var CustomerInfo = ECOsystem.Utilities.Session.GetCustomerInfo();
                DateTime Today = DateTime.Now;
                return new FileStreamResult(new MemoryStream(SummaryFile), "text/plain")
                {
                    FileDownloadName = "Detalle de la Importación de Combustibles - " + Today + ".txt"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}