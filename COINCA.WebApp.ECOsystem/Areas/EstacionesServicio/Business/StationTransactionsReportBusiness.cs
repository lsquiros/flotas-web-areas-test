﻿using ECO_ServiceStations.Models;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using System.Xml.Linq;
using System.Data;
using System.Globalization;
using ECOsystem.Utilities;
using System.Configuration;

namespace ECO_ServiceStations.Business
{
    public class StationTransactionsReportBusiness : IDisposable
    {
        public DataSet StationTransactionsReportRetrieve(ControlFuelsReportsBase parameters)
        {            
            string xml = null;
            DataSet ds = new DataSet();

            if (parameters.ServiceStationId == null || parameters.ServiceStationId == -1)
            {
                parameters.ServiceStationId = null;
                var doc = new XDocument(new XElement("xmldata"));
                var root = doc.Root;
                foreach (var item in ECOsystem.Utilities.Session.GetServiceStattionsInfo())
                {
                    root.Add(new XElement("ServiceStations", new XAttribute("ServiceStationId", item.ServiceStationId)));
                }
                xml = doc.ToString();
            }
            var TransactionModel = StationTransactionsReportRetrieve(parameters, xml);

            using (var bus = new UtilitiesBusiness())
            {
                var CustomerModel = bus.CustomerByServiceStationRetrieve().Where(x => TransactionModel.ToList().Select(y => y.CustomerId).Contains(x.CustomerId)).ToList();
                ds.Tables.Add(GetCustomersForReport(parameters, CustomerModel));
                ds.Tables.Add(GetTransactionsForReport(parameters, TransactionModel));
                return ds;
            }
        }

        private DataTable GetCustomersForReport(ControlFuelsReportsBase parameters, IEnumerable<CustomerByServiceStations> model)
        {    
            using (var dt = new DataTable("StationTransactionsReport"))
            {
                dt.Columns.Add("CustomerId");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("StartDate");
                dt.Columns.Add("EndDate");

                var startDateStr = FormatDates(parameters.StartDate, parameters.Month, parameters.Year, false);
                var endDateStr = FormatDates(parameters.EndDate, parameters.Month, parameters.Year, true);

                foreach (var item in model)
                {
                    DataRow row = dt.NewRow();
                    row["CustomerId"] = item.CustomerId;
                    row["CustomerName"] = item.DecryptName;
                    row["StartDate"] = startDateStr;
                    row["EndDate"] = endDateStr;
                    dt.Rows.Add(row);
                }
                return dt;
            }
        }

        private DataTable GetTransactionsForReport(ControlFuelsReportsBase parameters, IEnumerable<StationTransactionsReport> model)
        {      
            using (var dt = new DataTable("StationTransactionsDetail"))
            {
                dt.Columns.Add("CustomerId");
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("Date");
                dt.Columns.Add("Vehicle");
                dt.Columns.Add("PlateId");                
                dt.Columns.Add("Driver");
                dt.Columns.Add("DriverId");                
                dt.Columns.Add("Liters");
                dt.Columns.Add("Amount");
                dt.Columns.Add("Fuel");
                dt.Columns.Add("ServiceStationName");
                dt.Columns.Add("Invoice");
                dt.Columns.Add("Gallons");                
                dt.Columns.Add("FuelPrice");
                                
                foreach (var item in model)
                {
                    DataRow row = dt.NewRow();

                    string CcDescrypt = TripleDesEncryption.Decrypt(item.CreditCardNumber);
                    string CcMask = Miscellaneous.GetDisplayCreditCardMask(CcDescrypt);
                    decimal gallons = decimal.Round(item.Liters / Convert.ToDecimal(ConfigurationManager.AppSettings["GALLONSTOLITERSEQUIVALENCE"]), 2, MidpointRounding.AwayFromZero);
                
                    row["CustomerId"] = item.CustomerId;
                    row["TerminalId"] = item.TerminalId;
                    row["Date"] = item.DateToExport;
                    row["Vehicle"] = item.VehicleName;
                    row["PlateId"] = item.PlateId;                    
                    row["Driver"] = item.DriverNameDescrypt;
                    row["DriverId"] = item.DriverIdDescrypt;                    
                    row["Liters"] = item.Liters;
                    row["Amount"] = item.FuelAmount;
                    row["Fuel"] = item.FuelName;
                    row["ServiceStationName"] = item.ServiceStationName;
                    row["Invoice"] = item.Invoice;
                    row["Gallons"] = gallons;                    
                    row["FuelPrice"] = item.FuelPrice;                    
                    dt.Rows.Add(row);
                }
                return dt;
            }
        }

        private IEnumerable<StationTransactionsReport> StationTransactionsReportRetrieve(ControlFuelsReportsBase parameters, string xml)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<StationTransactionsReport>("[Stations].[Sp_StationTransactionsReport_Retrieve]",
                      new
                      {
                          CustomerId = parameters.CustomerId,
                          ServiceStationId = parameters.ServiceStationId,
                          XMLServiceStationIds = xml,
                          Year = parameters.Year,
                          Month = parameters.Month,
                          StartDate = parameters.StartDate,
                          EndDate = parameters.EndDate
                      });
            }
        }

        private IEnumerable<CustomerStationTransactionsReport> CustomersStationTransactionsReportRetrieve(ControlFuelsReportsBase parameters, string xml)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerStationTransactionsReport>("[Stations].[Sp_Customers_StationTransactionsReport_Retrieve]",
                      new
                      {
                          CustomerId = parameters.CustomerId,
                          //ServiceStationId = parameters.ServiceStationId,
                          //XMLServiceStationIds = xml,
                          Year = parameters.Year,
                          Month = parameters.Month,
                          StartDate = parameters.StartDate,
                          EndDate = parameters.EndDate
                      });
            }
        }

        private string FormatDates(DateTime? date, int? month, int? year, bool end)
        {
            DateTime finaldate = new DateTime();

            if(date == null)
            {
                if(end)
                {
                    var day = DateTime.DaysInMonth((int)year, (int)month);
                    DateTime dateTime = DateTime.Parse(month + "/" + day + "/" + year);
                    finaldate = dateTime.AddHours(23).AddMinutes(59).AddSeconds(59);
                }
                else
                {
                    finaldate = DateTime.Parse(month + "/01/" + year);
                }
            }
            else
            {
                finaldate = DateTime.Parse(date.ToString());
            }
            return finaldate.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.CurrentCulture); 
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}