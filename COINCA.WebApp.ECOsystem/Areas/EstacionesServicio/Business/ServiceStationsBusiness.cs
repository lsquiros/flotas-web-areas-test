﻿using ECO_ServiceStations.Models;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using System.Xml.Linq;
using System.Data;
using System.Globalization;
using ECOsystem.Utilities;


namespace ECO_ServiceStations.Business
{
    public class ServiceStationsBusiness : IDisposable
    {
        public DashboardServiceStations StationTransactionsResumeRetrieve()
        {            
            string xml = null;
            DashboardServiceStations model;
            
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            //var timezone = ECOsystem.Utilities.Session.GetCustomerInfo().TimeZone;
            var dateCustomer = DateTime.Now;

            foreach (var item in ECOsystem.Utilities.Session.GetServiceStattionsInfo().ToList().Select(q => q.ServiceStationId).Distinct())
            {
                root.Add(new XElement("ServiceStations", new XAttribute("ServiceStationId", item)));
            }
            xml = doc.ToString();

            using (var dba = new DataBaseAccess())
            {
                model = dba.ExecuteReader<DashboardServiceStations>("[Stations].[Sp_StationTransactionsResume_Retrieve]",
                    new
                    {
                        XMLServiceStationIds = xml,
                        StartDate = dateCustomer.ToString("yyyy-MM-dd"),
                        EndDate = dateCustomer.ToString("yyyy-MM-dd HH:mm:ss")
                    }).FirstOrDefault();

                if (model != null)
                {
                    return model;
                }
                else {
                    return new DashboardServiceStations();
                }

            } 
            
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}