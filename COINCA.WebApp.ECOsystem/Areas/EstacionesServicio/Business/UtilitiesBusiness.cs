﻿using ECO_ServiceStations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using System.Xml.Linq;

namespace ECO_ServiceStations.Business
{
    public class UtilitiesBusiness : IDisposable
    {
        public IEnumerable<CustomerByServiceStations> CustomerByServiceStationRetrieve()
        {
            string XmlData = null;
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            foreach (var item in ECOsystem.Utilities.Session.GetServiceStattionsInfo().Select(x => x.PartnerId).Distinct())
            {
                root.Add(new XElement("ServiceStations", new XAttribute("PartnerId", item)));
            }
            XmlData = doc.ToString();

            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<CustomerByServiceStations>("[Stations].[Sp_CustomersByServiceStations_Retrieve]", new { XmlData  });  
            }
        }

        public IEnumerable<ServiceStationsByUser> ServiceStationsByUserRetrieve()
        {
            string XmlData = null;
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            foreach (var item in ECOsystem.Utilities.Session.GetServiceStattionsInfo().Select(x => x.ServiceStationId).Distinct())
            {
                root.Add(new XElement("ServiceStations", new XAttribute("ServiceStationId", item)));
            }
            XmlData = doc.ToString();

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ServiceStationsByUser>("[Stations].[Sp_ServiceStationsByUser_Retrieve]", new { XmlData });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}