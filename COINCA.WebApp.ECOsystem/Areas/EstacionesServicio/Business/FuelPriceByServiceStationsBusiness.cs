﻿using ECO_ServiceStations.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ECO_ServiceStations.Business
{
    public class FuelPriceByServiceStationsBusiness : IDisposable
    {
        public IEnumerable<FuelPriceByServiceStationShow> FuelPriceByServiceStationRetrieve()
        {
            string XMLServiceStationIds = null;
            
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            foreach (var item in ECOsystem.Utilities.Session.GetServiceStattionsInfo())
            {
                root.Add(new XElement("ServiceStations", new XAttribute("ServiceStationId", item.ServiceStationId)));
            }
            XMLServiceStationIds = doc.ToString();

            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<FuelPriceByServiceStation>("[Stations].[Sp_FuelPriceByServiceStation_Retrieve]", new { XMLServiceStationIds });
                var list = new List<FuelPriceByServiceStationShow>();
                foreach (var item in result.Select(x => new { x.ServiceStationName, x.ServiceStationId }).Distinct())
                {
                    var model = new FuelPriceByServiceStationShow();
                    model.ServiceStationId = item.ServiceStationId;
                    model.ServiceStationName = item.ServiceStationName;
                    model.FuelList = result.Where(x => x.ServiceStationId == item.ServiceStationId).ToList();
                    list.Add(model);
                }
                return list;
            }
        }

        public void FuelPriceByServiceStationAddOrEdit(List<FuelPriceByServiceStation> list)
        {
            var XMLData = string.Empty;

            using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<FuelPriceByServiceStation>));
                xmlSerializer.Serialize(stringWriter, list);
                XMLData = stringWriter.ToString().Substring(stringWriter.ToString().IndexOf('\n') + 1);
            }

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Stations].[Sp_FuelPriceByServiceStation_AddOrEdit]", 
                    new 
                    {
                        XMLData
                       ,ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
                
            }
        }
                
        public List<FuelPriceByServiceStation> ProgramFuelPriceByServiceStationRetrieve(int? ServiceStationId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<FuelPriceByServiceStation>("[Stations].[Sp_ProgramFuelPriceByServiceStation_Retrieve]", new { ServiceStationId }).ToList();                
            }
        }

        public void ProgramFuelPriceByServiceStationAddOrEdit(List<FuelPriceByServiceStation> list)
        {
            var XMLData = string.Empty;

            using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<FuelPriceByServiceStation>));
                xmlSerializer.Serialize(stringWriter, list.Where(x => x.StartDate != null || x.Price != null).ToList());
                XMLData = stringWriter.ToString().Substring(stringWriter.ToString().IndexOf('\n') + 1);
            }
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Stations].[Sp_ProgramFuelPriceByServiceStation_AddOrEdit]", 
                    new
                    {
                        XMLData,
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }


        /// <summary>
        /// Retrieve fuel prices
        /// </summary>
        /// <returns>List of FuelPriceByServiceStationExcel</returns>
        public List<FuelPriceByServiceStationExcel> FuelPriceByServiceStationRetrieve_Simple()
        {
            string XMLServiceStationIds = null;

            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            foreach (var item in ECOsystem.Utilities.Session.GetServiceStattionsInfo())
            {
                root.Add(new XElement("ServiceStations", new XAttribute("ServiceStationId", item.ServiceStationId)));
            }
            XMLServiceStationIds = doc.ToString();

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<FuelPriceByServiceStationExcel>("[Stations].[Sp_FuelPriceByServiceStation_Retrieve]", new { XMLServiceStationIds }).ToList();
            }
        }

        /// <summary>
        /// Import services station prices
        /// </summary>
        /// <param name="List">New prices list</param>
        /// <param name="txtStartDate">Start date and time</param>
        public void ProgramFuelPriceByServiceStationImport(List<FuelPriceByServiceStation> List, DateTime? txtStartDate = null)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Stations].[Sp_ProgramFuelPriceByServiceStation_Import]",
                    new
                    {
                        XMLData = ECOsystem.Utilities.Miscellaneous.GetXML(List),
                        ECOsystem.Utilities.Session.GetUserInfo().UserId,
                        StartDate = txtStartDate
                    });
            }
        }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}