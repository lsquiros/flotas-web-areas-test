﻿var ECO_ServiceStations = ECO_ServiceStations || {};

ECO_ServiceStations.FuelPriceByServiceStations = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        downloadFile();

        $('.btnUpdateSelectedValues').click(function () {
            if ($('#gridContainer input[type=checkbox]:checked').not("#chkCheckAll").length > 0) {
                if ($('.pricetext').filter(function () { return this.value != '' && this.value != undefined }).length) {
                    applyChanges();
                } else {
                    ECOsystem.Utilities.SetMessageShow('Debe completar los espacios para precios nuevos.', 'ERROR');
                }
            } else {
                ECOsystem.Utilities.SetMessageShow('Debe seleccionar una o varias estaciones de servicio de la lista.', 'ERROR');
            }
        });

        $('#btnSaveChanges').click(function () {
            saveChanges();
        });

        $('#btnProgram').click(function () {
            makePicker($('#txtProgramAll'));
            $('#ProgramAllEditModal').modal('show');
        });

        $('#btnProgramAllChanges').click(function () {
            saveAllProgramPrices();
        });

        $('#programfuelcontainer').off('click.btnSaveProgramChanges', '#btnSaveProgramChanges').on('click.btnSaveProgramChanges', '#btnSaveProgramChanges', function () {
            saveProgramPrices($(this).attr('servicestaionid'));
        });

        $('#gridContainer').off('click.chkCheckAll', '#chkCheckAll').on('click.chkCheckAll', '#chkCheckAll', function () {
            var check = $(this);
            $('#gridContainer').find('input[type=checkbox]').each(function () {
                if (check.is(':checked')) {
                    $(this).prop('checked', true);
                } else {
                    $(this).prop('checked', false);
                }
            });
        });

        $('#gridContainer').off('change.txtFuels', '.txtFuels').on('change.txtFuels', '.txtFuels', function () {
            $(this).closest('tr').addClass('selectRow');
        });

        $('#gridContainer').off('click.program', 'a[program_row]').on('click.program', 'a[program_row]', function () {
            var id = $(this).attr('servicestationid');
            showProgramFuels(id);
        });

        $('.txtFuels').keypress(function (e) {
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) {
                return false;
            }
        });

        $('.pricetext').keypress(function (e) {
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) {
                return false;
            }
        });

        $('#txtHourProgramAll').each(function () {
            makeTimePicker($(this));
        });

        $('#timepickericon').click(function () {
            $('#txtHourProgramAll').select();
        });

        $('#ImportFuelPricesContainer .datetimepickerStartDate').each(function () {
            makePicker($(this));
        });


        $('#ImportFuelPricesContainer .timepicker').each(function () {
            makeTimePicker($(this));
        });
    }

    var showProgramFuels = function (id) {
        $.ajax({
            url: '/FuelPriceByServiceStation/ShowProgramFuels',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ serviceStationId: id }),
            success: function (result) {
                $('#programfuelcontainer').html(result);
                initializeDatePickers();
                initializeTimePickers();
                $('#ProgramFuelPriceModal').modal('show');
            }
        });
    }

    var initializeDatePickers = function () {
        $('#ProgramFuelPriceModal .datetimepickerStartDate').each(function () {
            makePicker($(this));
        });
    }

    var makePicker = function (element) {
        element.datepicker({
            language: 'es',
            defaultDate: 'now',
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
            beforeShowDay: function (date) {
                return date > new Date();
            }
        });
    }

    var initializeTimePickers = function () {
        $('#ProgramFuelPriceModal .timepicker').each(function () {
            makeTimePicker($(this));
        });
    }

    var makeTimePicker = function (element) {
        element.timepicker({
            timeFormat: 'hh:mm a',
            interval: 30,
            minTime: '0',
            maxTime: '11:00pm',
            startTime: '1:00',
            dropdown: true,
            scrollbar: true,
            disableTextInput :true
        });
        element.timepicker({
            template: 'modal'
        });
    }


    var applyChanges = function () {
        var values = [];

        $('.pricetext').each(function (index) {
            var item = {};
            item.fuelId = $(this).attr('fuelid');
            item.amount = $(this).val();
            values.push(item);

            if ($('.pricetext').length - 1 == index) {

                $('#gridContainer input[type=checkbox]').each(function (i) {
                    var station = this;
                    if ($(station).is(':checked') && $(station).attr("id") != 'chkCheckAll') {
                        var serviceStationId = $(station).attr("id");

                        $('.service-station-' + serviceStationId).each(function () {
                            for (var i = 0; i < values.length; i++) {
                                if ($(this).attr('fuelPriceId') == values[i].fuelId) {
                                    if (values[i].amount != '') {
                                        $(this).val(values[i].amount);
                                        $(this).closest('tr').addClass('selectRow');
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });
    };

    var saveChanges = function () {
        $('#ConfirmationEditModal').modal('hide');
        $('body').loader('show');

        var list = [];

        $('.selectRow').each(function () {
            var serviceStationId = $(this).attr("id");
            $('.service-station-' + serviceStationId).each(function () {
                var item = {};
                item.ServiceStationId = serviceStationId;
                item.FuelId = $(this).attr("fuelPriceId");

                if ($(this).val() == '') {
                    item.Price = 0;
                } else {
                    item.Price = $(this).val();
                }

                list.push(item);
            });
        });

        $.ajax({
            url: '/FuelPriceByServiceStation/FuelPriceByServiceStationAddOrEdit',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ list: list }),
            success: function (result) {
                if (result == 'Success') window.location.reload();
            }
        });
    };

    var saveProgramPrices = function (servicestationid) {
        var list = [];
        $('#ProgramFuelPriceModal').modal('hide');
        utiLoader(true);

        $('#ProgramFuelPriceModal #gridProgramContainer tbody tr').each(function () {
            var fuelid = $(this).attr('id');
            var item = {};
            var date = $('#txtStartDate_' + fuelid).val();
            var hour = $('#txtStartHour_' + fuelid).val();

            item.Id = $(this).attr('rowid');
            item.ServiceStationId = servicestationid;
            item.FuelId = fuelid;

            item.StartDate = date.split('/')[2] + '/' + date.split('/')[1] + '/' + date.split('/')[0] + ' ' + hour;

            item.Price = $('#txtPrice_' + fuelid).val();

            list.push(item);
        });
        applyChangesWithList(list);
    }

    var saveAllProgramPrices = function () {
        var list = [];
        var values = [];
        var date = $('#txtProgramAll').val();
        var hour = $('#txtHourProgramAll').val();
        $('#ProgramAllEditModal').modal('hide');
        utiLoader(true);

        $('.pricetext').each(function () {
            var item = {};
            item.fuelId = $(this).attr('fuelid');
            item.amount = $(this).val();
            values.push(item);
        });

        for (var i = 0; i < values.length; i++) {
            if (!$.isNumeric(values[i].amount)) {
                $('#modalverificationMessage').text('Debe digitar precios para los combustibles');
                $('#modalverification').modal('show');
                utiLoader(false);
                return;
            }
        }

        $('#gridContainer input[type=checkbox]').each(function (i) {
            if ($(this).is(':checked')) {
                if ($(this).attr("id") != 'chkCheckAll') {
                    var id = $(this).attr("id");
                    for (var i = 0; i < values.length; i++) {
                        var item = {};
                        item.ServiceStationId = id;
                        item.FuelId = values[i].fuelId;
                        item.StartDate = date.split('/')[2] + '/' + date.split('/')[1] + '/' + date.split('/')[0] + ' ' + hour;
                        item.Price = values[i].amount;
                        list.push(item);
                    }
                }
            }
        });



        if (list.length > 0) applyChangesWithList(list);
        else {
            $('#modalverificationMessage').text('Seleccione estaciones de servicios para aplicar cambio.');
            $('#modalverification').modal('show');
            utiLoader(false);
        }
    }

    var applyChangesWithList = function (list) {
        $.ajax({
            url: '/FuelPriceByServiceStation/ProgramFuelPriceByServiceStationAddOrEdit',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ list: list }),
            success: function (result) {
                utiLoader(false);
                if (result == 'Success') window.location.reload();
            }
        });
    }

    var utiLoader = function (show) {
        if (show) {
            $('body').loader('show');
        } else {
            $('body').loader('hide');
        }
    }

    var downloadFile = function () {
        var resultdata = $('#ResultData').val();
        if (resultdata != '' && resultdata != undefined && resultdata != null) showresultmodal(resultdata);

        $('#DownloadFileForm').submit();
    };

    var showresultmodal = function (resultdata) {
        $('#resultimportdate').html(resultdata.split('|')[0]);
        $('#resultadded').html(resultdata.split('|')[1]);
        $('#resultnoprocess').html(resultdata.split('|')[2]);
        $('#resulttotal').html(resultdata.split('|')[3]);
        $('#SummaryResultModal').modal('show');
    }

    var selectFile = function () {
        $('#ImportCSV').trigger('click');
        $("#txtFile").val("");
    };

    var getFilename = function (input) {
        if (ValidateFormat(input)) {
            if (input.files && input.files[0]) {
                //var value = $("#ImportCSV").val();
                var NameOfFile = input.files[0].name;
                $("#txtFile").val(NameOfFile);
                setErrorFormatMsj('', false);
            }
        } else {
            $("#txtFile").val("");
            setErrorFormatMsj('* El formato de archivo es inválido o el tamaño del archivo sobrepasa lo permitido, favor seleccionar un archivo de formato .xlsx con un tamaño menor a 45KB (Equivalente a 500 líneas). Se sugiere dividir el archivo para poder subirlo por partes.', true);
        }
    }

    var ValidateFormat = function (input) {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var files = input.files;
            for (var i = 0, f; f = files[i]; i++) {
                // Only process csv files.
                if (!f.name.match('\.xlsx') || (f.size > 46030)) {
                    return false;
                }
                else {
                    return true;
                }
            }
        } else {
            ECOsystem.Utilities.SetMessageShow('La opción de subir imágenes no es soportada por este navegador.', 'ERROR');
        }
    };

    /* Show or hide errors messages*/
    var setErrorFormatMsj = function (msj, show) {
        if (show) {
            $('#errorFormatMsj').html(msj);
            $('#validationFormatErrors').removeClass('hide');
        } else {
            $('#errorFormatMsj').html('');
            $('#validationFormatErrors').addClass('hide');
        }
    }
    
    var onClickImport = function () {
        if ($('#ImportForm').valid()) {
            var temp = $("#txtStartDateImport").val().split('/');
            var date = new Date();
            date.setDate(temp[0]);
            date.setMonth(temp[1] - 1);
            date.setFullYear(temp[2]);

            if (date > new Date()) {
                $('body').loader('show');
                $('#ImportForm').submit();
            } else {
                ECOsystem.Utilities.SetMessageShow('La fecha de importación es incorrecta.', 'ERROR');
                $("#txtStartDateImport").css("border", "2px solid red");
            }
        }
    };

    return {
        Init: initialize,
        DownloadFile: downloadFile,
        SelectFile: selectFile,
        GetFilename: getFilename,
        OnClickImport: onClickImport
    };
})();