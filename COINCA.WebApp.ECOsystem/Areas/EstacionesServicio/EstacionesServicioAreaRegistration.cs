﻿using System.Web.Mvc;

namespace ECOsystem.Areas.EstacionesServicio
{
    public class EstacionesServicioAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "EstacionesServicio";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "EstacionesServicio_default",
                "EstacionesServicio/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}