﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Models.Account;
using System.ComponentModel;

using System.Globalization;
using ECOsystem.Models.Core;

namespace ECO_ServiceStations.Models
{

    public class DashboardServiceStationsReportBase
    {
        public DashboardServiceStationsReportBase()
        {
            List = new List<DashboardServiceStations>();
            Menus = new List<AccountMenus>();
            Data = new DashboardServiceStations();
        }

        public List<DashboardServiceStations> List { get; set; }

        public List<AccountMenus> Menus { get; set; }

        public DashboardServiceStations Data { get; set; }
    }

    public class DashboardServiceStations
    {
        public DashboardServiceStations() {
            Customers = 0;
            Quantity = 0;
            Amount = 0;
        }

        /// <summary>
        /// Quantity of customers
        /// </summary>
        public int Customers { get; set; }

        /// <summary>
        /// Quantity of transactions
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Total amount of transactions
        /// </summary>        
        public decimal Amount { get; set; }

        public string AmountStr {
            get { return CurrencySymbol + Amount.ToString("N", CultureInfo.CreateSpecificCulture("es-CR")); }
        }

        /// <summary>
        /// Currency Symbol
        /// </summary>        
        public string CurrencySymbol { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

    }      
}