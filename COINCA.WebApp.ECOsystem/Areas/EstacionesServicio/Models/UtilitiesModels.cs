﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_ServiceStations.Models
{
    public class CustomerByServiceStations
    {
        public int CustomerId { get; set; }

        public string EncryptName { get; set; }

        public string DecryptName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptName); } }
    }

    public class ServiceStationsByUser
    {
        public int ServiceStationId { get; set; }

        public string Name { get; set; }
    }
}