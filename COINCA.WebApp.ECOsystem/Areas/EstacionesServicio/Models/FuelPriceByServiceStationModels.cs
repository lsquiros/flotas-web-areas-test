﻿using ECOsystem.Models.Account;
using ECOsystem.Utilities.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ECO_ServiceStations.Models
{
    public class FuelPriceByServiceStationBase
    {
        public FuelPriceByServiceStationBase()
        {
            Data = new FuelPriceByServiceStation();
            List = new List<FuelPriceByServiceStationShow>();            
            Menus = new List<AccountMenus>();
        }

        public FuelPriceByServiceStation Data { get; set; }
        public IEnumerable<FuelPriceByServiceStationShow> List { get; set; }        
        public List<AccountMenus> Menus { get; set; }
    }

    public class FuelPriceByServiceStation
    {
        public int Id { get; set; }
        public int FuelId { get; set; }
        public int? ServiceStationId { get; set; }
        public string ServiceStationName { get; set; }
        public string FuelName { get; set; }
        public decimal? Price { get; set; }
        public DateTime? StartDate { get; set; }
    }
    public class FuelPriceByServiceStationExcel
    {

        [DisplayName("ESTACIÓN DE SERVICIO")]
        public string ServiceStationName { get; set; }

        [DisplayName("TIPO DE COMBUSTIBLE")]
        public string FuelName { get; set; }

        [DisplayName("PRECIO")]
        public decimal? Price { get; set; }
    }

    public class FuelPriceByServiceStationShow
    {
        public int? ServiceStationId { get; set; }

        public string ServiceStationName { get; set; }

        public List<FuelPriceByServiceStation> FuelList { get; set; }
    }
}