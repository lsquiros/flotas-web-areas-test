﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Models.Account;
using System.ComponentModel;

using ECOsystem.Models.Core;
using System.Configuration;

namespace ECO_ServiceStations.Models
{

    public class StationTransactionsReportBase
    {
        public StationTransactionsReportBase()
        {
            List = new List<StationTransactionsReport>();
            Menus = new List<AccountMenus>();
            Parameters = new ControlFuelsReportsBase();
        }

        public List<StationTransactionsReport> List { get; set; }

        public List<AccountMenus> Menus { get; set; }

        public ControlFuelsReportsBase Parameters { get; set; }
    }
    
    public class StationTransactionsReport
    {
        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Transacción Id")]
        public int TransactionId { get; set; }

        /// <summary>
        /// FilterType
        /// </summary>        
        public int FilterType { get; set; }

        /// <summary>
        /// CreditCard With Format Number and Mask
        /// </summary>        
        [DisplayName("Tarjeta")]        
        public string CreditCardNumberWithMask { get { return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(CreditCardNumberDescrypt); } }

        /// <summary>
        /// Credit Card Number
        /// </summary>
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// CreditCard With Format Number
        /// </summary>
        public string CreditCardNumberDescrypt { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(CreditCardNumber); } }

        /// <summary>
        /// HolderName
        /// </summary>
        [DisplayName("Titular")]        
        public string HolderName { get; set; }

        /// <summary>
        /// HolderNameStr
        /// </summary>
        [DisplayName("Titular")]        
        public string HolderNameStr { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(HolderName); } }

        /// <summary>
        /// Invoice - # Factura
        /// </summary>
        [DisplayName("#Factura")]
        public string Invoice { get; set; }


        /// <summary>
        /// MerchantDescription - Gasolinera
        /// </summary>
        [DisplayName("Gasolinera")]
        public string ServiceStationName { get; set; }

        /// <summary>
        /// FuelName
        /// </summary>
        [DisplayName("Combustible")]
        public string FuelName { get; set; }

        /// <summary>
        /// Liters
        /// </summary>
        [DisplayName("Litros")]
        public decimal Liters { get; set; }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [DisplayName("Litros")]
        public string LitersStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(Liters, 3); }
        }

        /// <summary>
        /// LitersLast
        /// </summary>
        [DisplayName("LitersLast")]        
        public decimal LitersLast { get; set; }


        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        public decimal? CapacityUnitValue
        {
            get { return ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        public string CapacityUnitValueStr
        {
            get
            {
                return CapacityUnitValue == null ? "" : ECOsystem.Utilities.Miscellaneous.GetNumberFormatWithDecimal(CapacityUnitValue, 3);//.Replace(",","."); 
            }

        }

        /// <summary>
        /// Odometer
        /// </summary>
        public double Odometer { get; set; }

        /// <summary>
        /// Odometer format
        /// </summary>
        [DisplayName("Odómetro")]
        public string OdometerStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Convert.ToDecimal(Odometer)); } }

        /// <summary>
        /// Odometer format
        /// </summary>
        [DisplayName("Odómetro")]
        public string OdometerStrCR { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(Convert.ToDecimal(Odometer)); } }

        /// <summary>
        /// OdometerLast
        /// </summary>
        public double OdometerLast { get; set; }

        /// <summary>
        /// Odometer format
        /// </summary>
        [DisplayName("Ultimo Odómetro")]
        public string OdometerLastStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(Convert.ToDecimal(OdometerLast)); } }

        /// <summary>
        /// Recorrido
        /// </summary>
        [DisplayName("Travel")]
        public decimal Travel { get { return (Convert.ToDecimal(Odometer - OdometerLast)); } }


        /// <summary>
        /// Rendimiento
        /// </summary>
        [DisplayName("Travel")]
        public string TravelStrCR
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(Travel); }
        }


        /// <summary>
        /// Rendimiento
        /// </summary>
        [DisplayName("Performance")]
        public decimal Performance { get { return ((Liters > 0) ? Decimal.Round(Travel / Liters, 2) : 0); } }

        /// <summary>
        /// Rendimiento
        /// </summary>
        [DisplayName("Performance")]
        public string PerformanceStrCR
        {
           get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(Performance); }
        }
        
        /// <summary>
        /// KilometersCost
        /// </summary>
        [DisplayName("KilometersCost")]
        public decimal KilometersCost { get { return ((Travel > 0) ? Decimal.Round(RealAmount / Travel, 2) : 0); } }

        /// <summary>
        /// KilometersCost
        /// </summary>
        [DisplayName("KilometersCost")]
        public string KilometersCostStrCR
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(KilometersCost); }
        }
        
        /// <summary>
        /// FuelAmount
        /// </summary>
        [DisplayName("Monto")]
        public decimal FuelAmount { get; set; }

        /// <summary>
        /// Fuel Amount as String
        /// </summary>
        [DisplayName("Monto")]        
        public string FuelAmountStr
        {            
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCurrencyCR(FuelAmount, CurrencySymbol); }
        }

        /// <summary>
        /// Fuel Amount as String
        /// </summary>
        [DisplayName("Monto")]
        public string FuelAmountStrCR
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormatCR(FuelAmount); }
        }
        
        /// <summary>
        /// Reference
        /// </summary>
        [DisplayName("Reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Transaction State
        /// </summary>
        [DisplayName("Estado Transacción")]
        public string State { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Date To Report Export
        /// </summary>
        [DisplayName("Fecha Emisión")]        
        public string DateToExport
        {
            get
            {
                return ((Date != null) ? Convert.ToDateTime(Date).ToString("dd/MM/yyyy hh:mm:ss tt") : string.Empty);
            }
        }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha")]
        public string DateStr
        {
            get
            {
                return ECOsystem.Utilities.Miscellaneous.GetDateFormat(Date);
            }
        }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha")]
        public string DateTimeStr { get { return ECOsystem.Utilities.Miscellaneous.GetDateTimeFormat(Date); } }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Moneda")]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        [DisplayName("Mensaje")]
        public string Message { get; set; }

        /// <summary>
        /// CreditCardHolder
        /// </summary>
        public string CreditCardHolder { get; set; }

        /// <summary>
        /// Transport Data
        /// </summary>
        public string TransportData { get; set; }
        
        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Fecha Creada")]
        public DateTime? InsertDate { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Codigo de Respuesta")]
        public string ResponseCode { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("descripcion Respuesta")]
        public string ResponseCodeDescription { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Customer ID")]
        public int CustomerId { get; set; }

        /// <summary>
        /// CostCenter Name
        /// </summary>
        [DisplayName("CostCenter Name")]
        public string CostCenterName { get; set; }


        /// <summary>
        /// VehicleGroupName
        /// </summary>
        [DisplayName("VehicleGroupName")]
        public string VehicleGroupName { get; set; }

        /// <summary>
        /// DriverId
        /// </summary>
        [DisplayName("DriverId")]
        public string DriverId { get; set; }


        /// <summary>
        /// DriverId Desencriptado
        /// </summary>        
        public string DriverIdDescrypt { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(DriverId); } }

        /// <summary>
        /// DriverName
        /// </summary>
        [DisplayName("DriverName")]
        public string DriverName { get; set; }


        /// <summary>
        /// DriverName Desencriptado
        /// </summary>        
        public string DriverNameDescrypt { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(DriverName); } }

        /// <summary>
        /// UnitName
        /// </summary>
        [DisplayName("UnitName")]
        public string UnitName { get; set; }


        /// <summary>
        /// Manufacturer
        /// </summary>
        [DisplayName("Manufacturer")]
        public string Manufacturer { get; set; }


        /// <summary>
        /// MCC
        /// </summary>
        public int MCC { get; set; }

        /// <summary>
        /// Identification
        /// </summary>
        public string HolderId { get; set; }

        /// <summary>
        /// Identification
        /// </summary>
        public string HolderIdDescrypt { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(HolderId); } }

        /// <summary>
        /// Terminal Id
        /// </summary>
        public string TerminalId { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// VehiclePlate
        /// </summary>
        public string PlateId { get; set; }

        /// <summary>
        /// CustomerName
        /// </summary>
        [DisplayName("Customer Name")]
        public string CustomerName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(_customerName);
            }
            set
            {
                this._customerName = value;
            }
        }

        private string _customerName;

        /// <summary>
        /// Country Name
        /// </summary>
        [DisplayName("País")]
        public string CountryName { get; set; }

        /// <summary>
        /// Total of Trx Traveled Odometer
        /// </summary>
        [DisplayName("Tipo Cambio")]
        public string ExchangeValue { get; set; }

        /// <summary>
        /// Real Amount of Trx 
        /// </summary>        
        public decimal RealAmount { get; set; }

        /// <summary>
        /// Liter Price
        /// </summary>
        public decimal FuelPrice { get; set; }

        /// <summary>
        /// Real Amount String
        /// </summary>
        [DisplayName("Monto Real")]
        public string RealAmountFormatted
        {
            get { return RealAmount == null ? "N/A" : ((decimal)RealAmount).ToString("N", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); }
        }
    }

    public class CustomerStationTransactionsReport
    {
        public int CustomerId { get; set; }

        public string EncryptName { get; set; }

        public string DecryptName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptName); } }
    }
}