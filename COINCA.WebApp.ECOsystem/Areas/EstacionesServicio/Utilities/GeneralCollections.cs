﻿using ECO_ServiceStations.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_ServiceStations.Utilities
{
    public class GeneralCollections
    {        
        public static SelectList GetCustomersByServiceStations
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var bus = new UtilitiesBusiness())
                {
                    var model = bus.CustomerByServiceStationRetrieve();

                    if (model.Any())
                    {
                        dictionary.Add(-1, "Todos");

                        foreach (var item in model)
                        {
                            dictionary.Add(item.CustomerId, item.DecryptName);
                        }
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }                
            }
        }

        public static SelectList GetServiceStationsByUser
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var bus = new UtilitiesBusiness())
                {
                    var model = bus.ServiceStationsByUserRetrieve();
                    
                    if(model.Any())
                    {
                        dictionary.Add(-1, "Todos");
                        foreach (var item in model)
                        {
                            dictionary.Add(item.ServiceStationId, item.Name);
                        }
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }
    }
}