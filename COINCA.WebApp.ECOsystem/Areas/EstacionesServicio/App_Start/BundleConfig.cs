﻿using System.Web.Optimization;

namespace ECO_ServiceStations
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/bootstrap_css").Include(
                      // FONTS
                      "~/Content/Styles/materialdesignicons.css",
                      // THEME
                      "~/Content/Styles/bootstrap.css",
                      //"~/Content/Styles/bootstrap-theme.css",
                      "~/Content/Styles/material.css",
                      // SELECT 2
                      "~/Content/Styles/select2/select2.css",
                      "~/Content/Styles/select2/select2-bootstrap.css",
                      // SIDEBAR
                      "~/Content/Styles/simple-sidebar.css",
                      // OTHER
                      "~/Content/Styles/datepicker3.css",
                      "~/Content/Styles/jquery.loader.css",
                      "~/Content/Styles/bootstrap_toggle/bootstrap-toggle.css",
                      "~/Content/Styles/material.css"
                      ));

            bundles.Add(new StyleBundle("~/bundles/menu_css").Include(
                      "~/Content/Styles/plugins/metisMenu/metisMenu.min.css"));

            bundles.Add(new StyleBundle("~/bundles/bootstrap_default_css").Include(
                     "~/Content/Styles/bootstrap-theme.css"));

            bundles.Add(new StyleBundle("~/bundles/bootstrap_bac_css").Include(
                      "~/Content/Styles/bootstrap-theme-bac.css"));

            bundles.Add(new StyleBundle("~/bundles/bootstrap_linevita_css").Include(
                      "~/Content/Styles/bootstrap-theme-linevita.css"));

            bundles.Add(new StyleBundle("~/bundles/gridmvc_css").Include(
                      "~/Content/Styles/Gridmvc.css"));

            bundles.Add(new StyleBundle("~/bundles/gridmvc_css_bacflota").Include(
                      "~/Content/Styles/Gridmvc-BacFlota.css"));

            bundles.Add(new StyleBundle("~/bundles/gridmvc_css_linevita").Include(
                      "~/Content/Styles/Gridmvc-LineVita.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.min.js",
                        "~/Content/Scripts/jquery.validate*",
                        "~/Content/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Content/Scripts/jquery.alphanum.js",
                        "~/Content/Scripts/material.js",
                        "~/Content/Scripts/select2.min.js",
                        "~/Content/Scripts/Select2-locales/select2_locale_es.js",
                        "~/Content/Scripts/mathFunctions.js",
                        "~/Content/Scripts/jquery.sortable.min.js",
                        "~/Content/Scripts/plugins/jquery.loader.js",
                        "~/Content/Scripts/Store/store.min.js"
                        , "~/Content/Scripts/materialize.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap_js").Include(
                      "~/Content/Scripts/bootstrap.js",
                      "~/Content/Scripts/respond.js",
                      "~/Content/Scripts/bootstrap-datepicker.js",
                      "~/Content/Scripts/bootstrap-filestyle.min.js",
                      "~/Content/Scripts/bootstrap_toggle/bootstrap-toggle.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/menu_js").Include(
                      "~/Content/Scripts/plugins/metisMenu/metisMenu.min.js",
                      "~/Content/Scripts/plugins/sb-admin-2.js"));

            bundles.Add(new ScriptBundle("~/bundles/gridmvc_js").Include(
                      "~/Content/Scripts/gridmvc.min.js",
                      "~/Content/Scripts/gridmvc.lang.es.js"));

            bundles.Add(new ScriptBundle("~/bundles/ChartGoogleJS").Include(
                    "~/Content/Scripts/googleCharts/loader.js",
                    "~/Content/Scripts/chartJS/Chart.min.js",
                    "~/Scripts/Utilities/chartUtilities.js",
                    "~/Scripts/Utilities/cardUtilities.js"
                ));

            BundleTable.EnableOptimizations = true;
        }
    }
}
