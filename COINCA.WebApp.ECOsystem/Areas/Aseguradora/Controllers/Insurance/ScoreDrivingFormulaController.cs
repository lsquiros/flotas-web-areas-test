﻿/************************************************************************************************************
*  File    : ScoreDrivingFormulaController.cs
*  Summary : Score Driving Formula Controller Actions
*  Author  : Andrés Oviedo
*  Date    : 23/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECO_Insurance.Business.Insurance;
using ECO_Insurance.Models.Identity;
using ECO_Insurance.Models.Insurance;

namespace ECO_Insurance.Controllers.Administration
{
    /// <summary>
    /// ScoreDrivingFormula Controller. Implementation of all action results from views
    /// </summary>
    public class ScoreDrivingFormulaController : Controller
    {
        /// <summary>
        /// ScoreDrivingFormula Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using(var business=new ScoreDrivingFormulaBusiness())
            {
                ScoreDrivingFormula parameters=new ScoreDrivingFormula();
                parameters.CustomerId=ECOsystem.Utilities.Session.GetCustomerId();

                var model = new ScoreDrivingFormulaBase
                {
                    Data=new ScoreDrivingFormula(), 
                    List =business.GetScoreDrivingFormulas(parameters)
                };
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity ScoreDrivingFormula
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
       // [Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult AddOrEditScoreDrivingFormula(int? ScoreDrivingFormulaId, string Name,string ScoreDrivingFormulaStr)
        {
            try
            {
                using (var business = new ScoreDrivingFormulaBusiness())
                {
                    ScoreDrivingFormula model = new ScoreDrivingFormula();
                    model.ScoreDrivingFormulaId = ScoreDrivingFormulaId;
                    model.Name = Name;
                   
                    model.CustomerId = ECOsystem.Utilities.Session.GetCustomerId();
                    model.ScoreDrivingFormulaStr = ScoreDrivingFormulaStr.Replace("<b>", "").Replace("</b>", "").Replace("<font size=\"4\">", "").Replace("</font>", "").Replace(",", "").Replace("√", "sqrt");
                    business.AddOrEditScoreDrivingFormula(model);
                    model.ScoreDrivingFormulaId = null;
                    return PartialView("_partials/_Grid", business.GetScoreDrivingFormulas(model));
                }
              
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ScoreDrivingFormula")
                });
            }
            
        }


        /// <summary>
        /// Load ScoreDrivingFormula
        /// </summary>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadScoreDrivingFormula(int id)
        {
            try
            {
                using (var business = new ScoreDrivingFormulaBusiness())
                {
                    ScoreDrivingFormula model = new ScoreDrivingFormula();
                    model.ScoreDrivingFormulaId = id;
                    model.CustomerId = ECOsystem.Utilities.Session.GetCustomerId();

                    return PartialView("_partials/_FormulaKeyBoard", business.GetScoreDrivingFormulas(model).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ScoreDrivingFormula")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model ScoreDrivingFormula
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult DeleteScoreDrivingFormula(int id)
        {
            try
            {
                using (var business = new ScoreDrivingFormulaBusiness())
                {
                    business.DeleteScoreDrivingFormula(id);
                    ScoreDrivingFormula model = new ScoreDrivingFormula();
                    model.CustomerId = ECOsystem.Utilities.Session.GetCustomerId();
                    return PartialView("_partials/_Grid", business.GetScoreDrivingFormulas(model));
                }
            }

            catch (Exception e)
            {
                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "ScoreDrivingFormula")
                    });
                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ScoreDrivingFormula")
                });
            }

        }

        /// <summary>
        /// Search VehicleGroups
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchScoreDrivingFormula(string key)
        {
            try
            {
                using (var business = new ScoreDrivingFormulaBusiness())
                {
                    ScoreDrivingFormula model = new ScoreDrivingFormula();
                    model.CustomerId = ECOsystem.Utilities.Session.GetCustomerId();
                    return PartialView("_partials/_Grid", business.GetScoreDrivingFormulas(model, key));
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleGroups")
                });
            }
        }
    }
}
