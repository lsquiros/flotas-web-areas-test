﻿/************************************************************************************************************
*  File    : EventRebuildingController.cs
*  Summary : Event Rebuilding Controller Actions
*  Author  : Andres Oviedo
*  Date    : 26/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECO_Insurance.Business.Insurance;
using ECO_Insurance.Models.Identity;
using ECO_Insurance.Models.Insurance;

namespace ECO_Insurance.Controllers.Insurance
{
    /// <summary>
    /// Event Rebuilding Controller
    /// </summary>
    public class EventRebuildingController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "INSURANCE_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new EventRebuildingBusiness())
            {
                return View(business.GetEventRebuildingReport(new OperationReportsBase()));
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        //[Authorize(Roles = "INSURANCE_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index(EventRebuildingBase model)
        {
            if (model == null || model.Parameters == null) RedirectToAction("Index");

            using (var business = new EventRebuildingBusiness())
            {
                model.Parameters.ReportType = "S";
                return View(business.GetEventRebuildingReport(model.Parameters));
            }
        }

        /// <summary>
        /// Load Rebuilding
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>

        //[Authorize(Roles = "INSURANCE_ADMIN")]
        [HttpPost]
        public ActionResult ShowRebuilding(int id, DateTime? startDate, DateTime? endDate)
        {
            try
            {
                using (var business = new EventRebuildingBusiness())
                {
                    Reconstructing re= business.RetrieveIntrackReference(id).FirstOrDefault();

                    EventRebuilding model=new EventRebuilding();
                    model.CountryCode = re.CountryCode;
                    model.IntrackReference = re.IntrackReference;
                    model.UserTimeZone = re.UserTimeZone;
                    model.StartDate = startDate.ToString();
                    model.EndDate = endDate.ToString();

                    return PartialView("_partials/_Detail", model);
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "EventRebuilding")
                });
            }
        }
       
    }
}