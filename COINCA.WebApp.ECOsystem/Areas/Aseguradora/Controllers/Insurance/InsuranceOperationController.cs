﻿using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Business.Core;
using ECOsystem.Models.Core;
using ECO_Insurance.Business.Insurance;
using ECO_Insurance.Models.Identity;
using ECO_Insurance.Models.Insurance;

namespace ECO_Insurance.Controllers.Insurance
{
    /// <summary>
    /// Insurance Operation Controller Class
    /// </summary>
    public class InsuranceOperationController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "INSURANCE_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new ScoreGlobalReportBusiness())
            {
                using (var businessThresholds = new CustomerThresholdsBusiness())
                {
                    CustomerThresholds customerThresholds = businessThresholds.RetrieveThresholds().ToList().FirstOrDefault();
                    ScoreGlobalReportBase model = business.RetrieveScoreGlobalSumary(new OperationReportsBase());
                    var list = model.List.ToList();

                    int GlobalScoreListCount = list.Count();

                    if (GlobalScoreListCount > 0)
                    {
                        model.LowScoreCount = Convert.ToDecimal((from score in list
                                                                 where score.Score <= customerThresholds.RedHigher && score.Score >= customerThresholds.RedLower
                                                                 select score).Count());// / Convert.ToDecimal(GlobalScoreListCount) * 100;

                        model.RegularScoreCount = Convert.ToDecimal((from score in list
                                                                     where score.Score <= customerThresholds.YellowHigher && score.Score >= customerThresholds.YellowLower
                                                                     select score).Count());// / Convert.ToDecimal(GlobalScoreListCount) * 100;

                        model.SuperiorScoreCount = Convert.ToDecimal((from score in list
                                                                      where score.Score <= customerThresholds.GreenHigher && score.Score >= customerThresholds.GreenLower
                                                                      select score).Count());// / Convert.ToDecimal(GlobalScoreListCount) * 100;
                    }

                    return View(new ScoreGlobalReportBase());

                }
            }
        }
    }
}
