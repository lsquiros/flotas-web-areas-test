﻿using System.IO;
using System.Web.Mvc;
using ECOsystem.Utilities;
using ECO_Insurance.Business.Insurance;
using ECO_Insurance.Models.Identity;
using ECO_Insurance.Models.Insurance;
using Newtonsoft.Json;

namespace ECO_Insurance.Controllers.Insurance
{
    public class DrivingScoreFleetAdminScoreReportController : Controller
    {

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "INSURANCE_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            DrivingScoreFleetDataSourceReportBase model = new DrivingScoreFleetDataSourceReportBase(); 
                using (var business = new DrivingScoreFleetDataSourceReportBusiness())
                {
                    model.Parameters.ReportType = "S";
                    model.Parameters.ScoreOpc = "SPADM";
                    return View(business.RetrieveDrivingScoreFleetDataSourceReport(model.Parameters));
                }
            
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        //[Authorize(Roles = "INSURANCE_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index(DrivingScoreFleetDataSourceReportBase model)
        {
            if (model == null || model.Parameters == null) RedirectToAction("Index");

            using (var business = new DrivingScoreFleetDataSourceReportBusiness())
            {
                model.Parameters.ReportType = "S";
                model.Parameters.ScoreOpc = "SPADM";
                return View(business.RetrieveDrivingScoreFleetDataSourceReport(model.Parameters));
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        //[Authorize(Roles = "INSURANCE_ADMIN")]
        public ActionResult ExcelReportDownload(string p)
        {
            var model = JsonConvert.DeserializeObject<DrivingScoreFleetDataSourceReportBase>(Miscellaneous.Base64Decode(p));

            byte[] report;
            using (var business = new DrivingScoreFleetDataSourceReportBusiness())
            {
                model.Parameters.ScoreOpc = "SPADM";
                report = business.GenerateDrivingScoreFleetDataSourceReportExcel(model.Parameters);
            }
            return new FileStreamResult(new MemoryStream(report), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = "Reporte_VelocidadAdmin.xlsx"
            };        
        
        }


        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "INSURANCE_ADMIN")]
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<DrivingScoreFleetDataSourceReportBase>(Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new DrivingScoreFleetDataSourceReportBusiness())
            {
                model.Parameters.ScoreOpc = "SPADM";
                return View("Index", business.RetrieveDrivingScoreFleetDataSourceReport(model.Parameters));
            }
        }
    }
}
