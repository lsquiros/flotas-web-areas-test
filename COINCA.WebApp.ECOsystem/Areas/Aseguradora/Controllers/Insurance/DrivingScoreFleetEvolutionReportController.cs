﻿/************************************************************************************************************
*  File    : DrivingScoreGlobalReportController.cs
*  Summary : Driving Score Global ReportController Actions
*  Author  : Andres Oviedo
*  Date    : 12/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Web.Mvc;
using ECOsystem.Utilities;
using ECO_Insurance.Business.Insurance;
using ECO_Insurance.Models.Identity;
using ECO_Insurance.Models.Insurance;
using Newtonsoft.Json;

namespace ECO_Insurance.Controllers.Insurance
{
    /// <summary>
    /// Driving Score Fleet Evolution Report Class
    /// </summary>
    public class DrivingScoreFleetEvolutionReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "INSURANCE_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new DrivingScoreFleetEvolutionReportBusiness())
            {

                //  return View(business.RetrieveCurrentFuelsReport(new ControlFuelsReportsBase()));
               DrivingScoreFleetEvolutionReportBase model = new DrivingScoreFleetEvolutionReportBase();
                model.Parameters.ReportType = "R";
                return View(business.RetrieveDrivingScoreFleetEvolutionReport(model.Parameters));

            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        //[Authorize(Roles = "INSURANCE_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index(DrivingScoreFleetEvolutionReportBase model)
        {
            if (model == null || model.Parameters == null) RedirectToAction("Index");

            using (var business = new DrivingScoreFleetEvolutionReportBusiness())
            {
                model.Parameters.ReportType = "R";
                return View(business.RetrieveDrivingScoreFleetEvolutionReport(model.Parameters));
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        //[Authorize(Roles = "INSURANCE_ADMIN")]
        public ActionResult ExcelReportDownload(string p)
        {
            var model = JsonConvert.DeserializeObject<DrivingScoreFleetEvolutionReportBase>(Miscellaneous.Base64Decode(p));

            byte[] report;
            using (var business = new DrivingScoreFleetEvolutionReportBusiness())
            {
                report = business.GenerateDrivingScoreFleetEvolutionReportExcel(model.Parameters);
            }
            return new FileStreamResult(new MemoryStream(report), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = "ExcelReport.xlsx"
            };
        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
       // [Authorize(Roles = "INSURANCE_ADMIN")]
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<DrivingScoreFleetEvolutionReportBase>(Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new DrivingScoreFleetEvolutionReportBusiness())
            {
                return View("Index", business.RetrieveDrivingScoreFleetEvolutionReport(model.Parameters));
            }
        }
    }
}