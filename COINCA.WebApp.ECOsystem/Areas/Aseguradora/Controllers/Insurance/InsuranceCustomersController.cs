﻿/************************************************************************************************************
*  File    : InsuranceCustomersController.cs
*  Summary : InsuranceCustomers Controller Actions
*  Author  : Andrés Oviedo
*  Date    : 19/01/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem;
using ECOsystem.Business.Core;
using ECOsystem.Models.Core;
using ECOsystem.Models.Miscellaneous;
using ECO_Insurance.Models.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ECO_Insurance.Controllers.Insurance
{
    /// <summary>
    /// Customers Controller. Implementation of all action results from views
    /// </summary>
    public class InsuranceCustomersController : Controller
    {

        private ApplicationUserManager _userManager;

        /// <summary>
        /// Asp Net User Manager
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? (_userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// Insurance Customers Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[System.Web.Mvc.Authorize(Roles = "INSURANCE_ADMIN, INSURANCE_USER")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new CustomersBusiness())
            {
                Customers data = new Customers();
                //data.InsurancePartnerId = Utilities.Session.GetPartnerId();
                var model = new CustomersBase
                {
                    Data = data,
                    List = business.RetrieveCustomers(null,ECOsystem.Utilities.Session.GetCountryId(),null)
                };
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert of the entity Customers
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[System.Web.Mvc.Authorize(Roles = "INSURANCE_ADMIN, INSURANCE_USER")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditCustomers(Customers model)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    if (model.CustomerId == null)
                        model.IsActive = true;
                    //model.InsurancePartnerId = Utilities.Session.GetPartnerId();    
       
                    business.AddOrEditCustomers(model, UserManager, Url, Request);

                    return PartialView("_partials/_Grid", business.RetrieveCustomers(null, ECOsystem.Utilities.Session.GetCountryId(), null));
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "InsuranceCustomers")
                });
            }
        }
        
        /// <summary>
        /// Search Customers
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <param name="CountryId">Country id (FK)</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[System.Web.Mvc.Authorize(Roles = "INSURANCE_ADMIN, INSURANCE_USER")]
        public PartialViewResult SearchCustomers(string key, int? CountryId = null)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveCustomers(null, ECOsystem.Utilities.Session.GetCountryId(), key));
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "InsuranceCustomers")
                });
            }
        }

        /// <summary>
        /// Load Customers
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[System.Web.Mvc.Authorize(Roles = "INSURANCE_ADMIN, INSURANCE_USER")]
        public PartialViewResult LoadCustomers(int id)
        {
            using (var business = new CustomersBusiness())
            {
                return PartialView("_partials/_Detail", business.RetrieveCustomers(id, ECOsystem.Utilities.Session.GetCountryId(), null).ToList().FirstOrDefault());
            }
        }


        /// <summary>
        /// Performs the the operation of delete on the model Customers
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="CountryId">CountryId FK</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[System.Web.Mvc.Authorize(Roles = "INSURANCE_ADMIN, INSURANCE_USER")]
        [EcoAuthorize]
        public PartialViewResult DeleteCustomers(int id)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    business.DeleteCustomers(id);
                    return PartialView("_partials/_Grid", business.RetrieveCustomers(null, ECOsystem.Utilities.Session.GetCountryId(), null));
                }
            }

            catch (Exception e)
            {
                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "InsuranceCustomers")
                    });
                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "InsuranceCustomers")
                });
            }
        }

    }

}