﻿using System.Collections.Generic;
using System.Web.Mvc;
using ECOsystem.Models.Account;
using ECO_Insurance.Models.Identity;

namespace ECO_Insurance.Controllers.Insurance
{
    /// <summary>
    /// Administration Controller Class
    /// </summary>
    public class InsuranceController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index(string title, string header)
        {
            ViewBag.Information = title;
            ViewBag.Header = header;

            var model = new AccountMenusBase { Menus = new List<AccountMenus>() };
            return View(model);
        }
	}
}