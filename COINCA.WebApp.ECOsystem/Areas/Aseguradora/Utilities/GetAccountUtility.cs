﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;

namespace ECO_Insurance.Utilities
{
    public class GetAccountUtility
    {

        public void GetUserInformation(ref HttpContextBase context) //, int? userId)
        {
            HttpCookie aCookie = context.Request.Cookies["ECOCFG"];
            Users user = null;

            if (aCookie != null)
            {
                aCookie.Expires = DateTime.Now.AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["ECOCFG_TIME"]));
                //context.Response.Cookies.Add(aCookie);
                string text = aCookie.Value;
                TokenByUserModel userActive = AccountManagement.TokenUserRetrieve(text);

                if (!context.Request.IsAuthenticated)
                {

                    using (var business = new UsersBusiness())
                    {
                        user = business.RetrieveUsers(null, userName: userActive.UserId).FirstOrDefault();
                    }

                    context.Session.Add("LOGGED_USER_INFORMATION", user);                    
                }
                else 
                {
                    using (var business = new UsersBusiness())
                    {
                        user = business.RetrieveUsers(null, userName: userActive.UserId).FirstOrDefault();
                    }

                    context.Session.Add("LOGGED_USER_INFORMATION", user);
                }                
            }          

        }

    }
}