﻿var ECO_Admin = ECO_Admin || {};

ECO_Admin.ImportData = (function () {
    var options = {};
    var errors = 0;
    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        setButtonStyle();
        initCsv();
        defaultInformation();
    }

    /*set Button Style*/
    var setButtonStyle = function () {
        $('#wrapper').find(":file").filestyle({
            buttonText: "&nbsp;Seleccionar Archivo...",
            buttonName: "btn-warning",
            buttonBefore: true
        });
    };

    /* initCsv */
    var initCsv = function () {
        $('#wrapper').off('change.csvfile', '#csvFileInput').on('change.csvfile', '#csvFileInput', function (e) {
            handleFiles(this.files);
        });

        $('#wrapper').off('click.csvfile', 'label[for="csvFileInput"]').on('click.csvfile', 'label[for="csvFileInput"]', function (e) {
            defaultInformation();
        });
    }

    /** File Upload **/
    function handleFiles(files) {
        if (window.FileReader) {
            getAsText(files[0]);
        } else {
            alert('FileReader are not supported in this browser.');
        }
    }

    /*get As Text*/
    function getAsText(fileToRead) {
        var reader = new FileReader();
        reader.onload = loadHandler;
        reader.onerror = errorHandler;
        reader.readAsText(fileToRead);
    }

    /*loadHandler*/
    function loadHandler(event) {
        var csv = event.target.result;
        processData(csv);
    }

    /*errorHandler*/
    function errorHandler(evt) {
        if (evt.target.error.name == "NotReadableError") {
            alert("Canno't read file !");
        }
    }

    /*process Data*/
    function processData(csv) {
        var allTextLines = csv.split(/\r\n|\n/);
        var lines = [];
        while (allTextLines.length) {
            lines.push(allTextLines.shift().split(','));
        }
        drawOutput(lines);
    }

    /*draw Output*/
    function drawOutput(lines) {
        //Clear previous data
        $('#csvOutput').html('');
        $('#wrapper').find('button:submit').attr('disabled', 'true');
        errors = 0;

        var result = '<table class="table table-striped">';
        result += getOutputHeader();
        result += "<tbody>";
        for (var i = 1; i < lines.length; i++) {
            result += getOutputRow(lines[i], i);
        }
        if (lines.length < 2) {
            result += "<tr><td colspan='" + (options.validation.length + 1) + "' class='text-danger'>Error procesando información</td></tr>";
        }
        result += '</tbody></table>';
        $('#csvOutput').html(result);
        if (errors == 0) {
            $('#wrapper').find('button:submit').removeAttr('disabled');
        }
    }

    /*get Output Header*/
    function getOutputHeader() {
        var result = '<thead><tr>';
        for (var i = 0; i < options.validation.length; i++) {
            var item = options.validation[i];
            result += '<th class="grid-header" nowrap><div class="grid-header-title ' + item.align + '"><span>' + item.colname + '</span></div></th>';
        }
        result += '<th class="grid-header" nowrap>&nbsp;</th>' + '</tr></thead>';
        return result;
    }

    /*get Output Row*/
    function getOutputRow(obj, currentLine) {
        try {
            var result = "<tr>";
            //text-success  text-danger
            var validation = validateData(obj);
            if (!validation.result) {
                return "<tr><td colspan='" + options.validation.length + "' class='text-danger'><b>Falló proceso de validación!</b> -- linea: " + currentLine + "</br>" + validation.error + "</td><td class='grid-cell text-danger'><i class='glyphicon glyphicon-remove'></i></td></tr>";
            } else {

                for (var i = 0; i < options.validation.length; i++) {
                    var item = options.validation[i];
                    result += "<td class='grid-cell " + item.align + "' " + (item.nowrap == true ? "nowrap" : "") + ">" + formatValue(obj[i], i) +
                                    "<input type='hidden' name='list[" + (currentLine - 1) + "]." + item.propname + "' value='" + getValue(obj[i], i) + "'>" +
                              "</td>";
                }
                result += "<td class='grid-cell'><i class='text-success glyphicon glyphicon-ok'></i></td></tr>";
                return result;
            };
        } catch (e) {
            return "<tr><td colspan='" + (options.validation.length + 1) + "' class='text-danger'>Error procesando información</td></tr>";
        }

    }

    /*get Value*/
    var getValue = function (val, i) {
        switch (options.validation[i].type) {
            case 'Boolean':
                return val.length == 0 ? '' : val == '1'?'True':'False';
            default:
                return val;
        }
    };

    /*validate Data*/
    var validateData = function (obj) {
        if (obj.length == options.validation.length) {
            var validation = { 'result': true, 'error': '' };
            for (var i = 0; i < options.validation.length; i++) {
                var item = options.validation[i];

                if (item.required && obj[i].length == 0) {
                    validation.result = false;
                    validation.error += '&#10148;' + item.colname + ': Es requerido</br>';
                }
                if (item.type == 'Integer' && !(!isNaN(parseInt(obj[i])) && (parseInt(obj[i]) % 1 === 0))) {
                    if (item.required == false && obj[i].length == 0)
                        continue;
                    validation.result = false;
                    validation.error += '&#10148;' + item.colname + ': Debe ser un valor entero</br>';
                }
                if (item.type == 'Decimal' && !(!isNaN(parseFloat(obj[i])) && isFinite(obj[i]))) {
                    if (item.required == false && obj[i].length == 0)
                        continue;
                    validation.result = false;
                    validation.error += '&#10148;' + item.colname + ': Debe ser un valor decimal</br>';
                }
            }

        } else {
            validation = { 'result': false, 'error': '&#10148;Número de columnas en el archivo diferente al esperado' };
        }
        if (validation.result == false) {
            errors++;
        }
        return validation;
    };

    /*default Information*/
    var defaultInformation = function () {

        $('#wrapper').find('button:submit').attr('disabled', 'true');
        
        var result = "";
        result += "<div class='alert alert-warning' role='alert'>" +
            "<div class='row'>" +
            "	<div class='col-sm-12'>" +
            "		<strong>Información para cargar archivo</strong>" +
            "	</div>" +
            "	<div class='col-sm-12 top20'>" +
            "		<p>" +
            "	El archivo debe contener las siguientes columnas:<br />" +
            "			<ul>";

        for (var i = 0; i < options.validation.length; i++) {
            var item = options.validation[i];
            result += "			<li>" +
                            "       Columna " + (i + 1) + " - " + item.colname + " (Valor " + item.type + ") " + (item.required == true ? " - Requerido" : "") +
                            "   </li>";
        }
        result += "			</ul>" +
            "		</p>" +
            "	</div>" +
            "</div>" +
            "</div>";

        $('#csvOutput').html(result);
    };

    /*printAjaxResult*/
    var printAjaxResult = function(result, message, rows) {
        var html = '<div class="alert alert-' + result + ' alert-dismissible" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '<span>' + message + '</span><br/><br/>' +
            '<span>Registros insertados: ' + rows + '</span>' +
            '</div>';
        $('#csvOutput').html(html);
        $('#wrapper').find('button:submit').attr('disabled','true');
    };

    /*format value*/
    var formatValue = function (val, i) {
        switch (options.validation[i].type) {
            case 'Decimal':
                return val.length == 0 ? "" : ECOsystem.Utilities.FormatNum(parseFloat(val));
            default:
                return val;
        }
    };

    /*success Import*/
    var successImport = function(data) {
        printAjaxResult((data.result == true ? "success" : "danger"), data.message, data.rows);
    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessImport: successImport
    };
})();