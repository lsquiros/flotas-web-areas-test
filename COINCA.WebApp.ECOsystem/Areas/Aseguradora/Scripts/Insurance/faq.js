﻿var ECO_Insurance = ECO_Insurance || {};

ECO_Insurance.Faq = (function () {
    var options = {};
    var line = 0;

    var initialize = function (opts) {
        $.extend(options, opts);

        $("#faqFileInput").filestyle({
            buttonText: "&nbsp;Seleccionar archivo...",
            buttonBefore: true
        });

        $("#btnSubmitUpload").off("click").on("click", function () {
            //alert("Hola")
            $("#frmUploadFAQ").submit();
        });

        //GUI Messages
        showMessage();
    }

    var showMessage = function ()
    {
        var currentMessage = $("#FAQHiddenMessage").val();
        var typeMessage = $("#TypeHiddenMessage").val();
        if (currentMessage != null && currentMessage != "")
        {
            switch (typeMessage) {
                case "ERROR":
                    ECO_Insurance.Utilities.SetMessageShow(currentMessage, typeMessage);
                    break;
                case "ALERT":
                    ECO_Insurance.Utilities.SetMessageShow(currentMessage, typeMessage);
                    break;
                case "INFO":
                    ECO_Insurance.Utilities.SetMessageShow(currentMessage, typeMessage);
                    break;
                case "SUCCESS":
                    ECO_Insurance.Utilities.SetMessageShow(currentMessage, typeMessage);
                    break;
                default:
                    break;
            }
        }
    }

    return {
        Init: initialize
    }
})();