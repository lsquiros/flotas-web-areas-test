﻿var ECO_Insurance = ECO_Insurance || {};

ECO_Insurance.EventRebuilding = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
           $.extend(options, opts);
        onShowDetailClick();
    }
    //Opens the detail modal
    var onShowDetailClick = function () {
    
        $(document).on("click", "#showRebuilding", function (e) {

            var vehicleId = $(this).attr('data-id');
            var startDate = $(this).attr('data-startDate');
            var endDate = $(this).attr('data-endDate');
               $.ajax({
                   url: options.ShowRebuildingUrl,
                    type: 'POST',
                    contentType: 'application/json;',
                    data: JSON.stringify({ id: vehicleId, startDate: startDate, endDate: endDate }),
                    success: function (partialView) {
                     
                       $('#detailContainer').html(partialView);
                       
                        return false;
                    }
                });
            //});

        });
    }
    /* Public methods */
    return {
        Init: initialize
    };
})();

