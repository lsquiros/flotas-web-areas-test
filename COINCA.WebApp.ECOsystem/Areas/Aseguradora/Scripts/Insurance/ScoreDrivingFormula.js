﻿var ECO_Insurance = ECO_Insurance || {};

ECO_Insurance.ScoreDrivingFormula = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        array= [];
        setupCalculator();
        initGrid();
        onSaveClick();
    }

    var onSaveClick = function () {
        $('#btnSave').click(function () {
        var mo = options.Model;
       

        mo.ScoreDrivingFormulaId = $('#ScoreDrivingFormulaId').val();
        mo.Name = $('#FormulaName').val();

          
            mo.ScoreDrivingFormulaStr = array.toString();
        
          
            var opened = countCharsInString(mo.ScoreDrivingFormulaStr, '(');
            var closed = countCharsInString(mo.ScoreDrivingFormulaStr, ')');

            if (opened > closed) {
                $('#lblError').text(opened - closed + 'cierre(s) de paréntesis faltante');
                return;
            } if (closed > opened) {
                $('#lblError').text(closed - opened + 'apertura(s) de paréntesis faltante');
                return;
            }
            if (!mo.Name || mo.Name == '' || !mo.ScoreDrivingFormulaStr || mo.ScoreDrivingFormulaStr=='')
            {
                $('#lblError').text('Por favor rellene todos los campos');
                return;
            }
            $('#lblError').text('');
            $.ajax({
                url: ECO_Insurance.Utilities.GetUrl() + '/ScoreDrivingFormula/AddOrEditScoreDrivingFormula',
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ ScoreDrivingFormulaId: mo.ScoreDrivingFormulaId, Name: mo.Name, ScoreDrivingFormulaStr: mo.ScoreDrivingFormulaStr }),
                success: function (ViewResult) {
                    $('#addOrEditModal').modal('hide');
                    $('#gridContainer').html(ViewResult);
                    return true;
                }
            });
        });
    }

    var CharArray = ['<font size="4"><b>+</b></font>', '<font size="4"><b>-</b></font>', '<font size="4"><b>*</b></font>', '<font size="4"><b>÷</b></font>', '<font size="4"><b>%</b></font>', '<font size="4"><b>(</b></font>', '<font size="4"><b>)</b></font>', '<font size="4"><b>√</b></font>', '<font size="4"><b>xⁿ</b></font>', 'ΣDT', 'ΣDEC', 'IRC', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'VEC', 'nEC', 'x̄VC', 'S', '.'];
    var splitFormula = function (formula) {
        array = [];
      
        var char = "";
        for (i = 0; i < formula.length; i++) {
            char = char + formula.charAt(i);
          
            for (j = 0; j < CharArray.length; j++) {
                if (char == CharArray[j]) {
                    array.push(char);
                   
                    char = "";
                    
                }
            }
        }
        
    }

    var escapeRegExp = function (string) {
        return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    }
    var replaceAll = function (string, find, replace) {
        return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
    }
    var boldCharArray = ['+', '-', '*', '÷', '%', '(', ')', '√', 'xⁿ'];
    var replaceToBold = function (formula) {

        for (i = 0; i < boldCharArray.length; i++) {
         
            formula = replaceAll(formula, boldCharArray[i], '<font size="4"><b>' + boldCharArray[i] + '</b></font>');//formula.replace(re, '<font size="4"><b>' + boldCharArray[i] + '</b></font>');
        }
      
        return formula;
    }
    var loadFormula = function () {
        var formula = $('#ScoreDrivingFormulaStr').val();
      
        formula=replaceToBold(formula);
        splitFormula(formula);
        $('#txtFormula').html(formula);
    }

    /* Init events for delete */
    var initGrid = function () {

        $('#gridContainer').off('click.del', 'a[del_row]').on('click.del', 'a[del_row]', function (e) {
            $('#deleteModal').find('#id').val($(this).attr('id'));
        });

        $('#gridContainer').off('click.edit', 'a[edit_row]').on('click.edit', 'a[edit_row]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();
        });

        $('#detailContainer').off('hidden.bs.modal', '#addOrEditModal').on('hidden.bs.modal', '#addOrEditModal', function (e) {
            modalClear();
        });

        $('#detailContainer').off('click.checkbox', 'input:checkbox').on('click.checkbox', 'input:checkbox', function (e) {
            var check = $(this).is(':checked') ? "true" : "false";
            $(this).siblings('input:hidden').val(check);
        });

        try {
            $("#addOrEditForm").data("validator").settings.submitHandler = function (e) {
                $('#detailContainer').find('#processing').removeClass('hide');
                $('#detailContainer').find('button').attr("disabled", "disabled");
            };
        }
        catch (e) {
        }
        $("#addOrEditForm").find('select').not("[custom='true']").select2();
        $("#addOrEditAlarmForm").find('select').not("[custom='true']").select2();

    };

    var modalClear = function () {
        var addOrEditModal = $('#addOrEditModal');
        addOrEditModal.find('input:text, input[type=number], input:hidden, textarea').val('');
        addOrEditModal.find('select').attr('selectedIndex', '-1').find("option:selected").removeAttr('selected');

        addOrEditModal.find('img').attr('src', '');
        addOrEditModal.find('img').attr('title', '');
        addOrEditModal.find('.field-validation-error').html('');
        addOrEditModal.find('.label-clear').html('');

        addOrEditModal.find('input:checkbox').val('true');
        addOrEditModal.find('input:checkbox').removeAttr("checked");
        addOrEditModal.find('input:checkbox').siblings('input:hidden').val('false');
        addOrEditModal.find('#validationErrors').addClass('hide');

        addOrEditModal.find('#processing').addClass('hide');
        addOrEditModal.find('button').removeAttr("disabled");
        addOrEditModal.find('select').not("[custom='true']").select2();
        type = 0;
        $(".element-name").html('');

    };
    var countCharsInString = function (value,toCount) {
        var count = 0;
        var char="";
        for (var i = 0; i < value.length; i++) {
            char = value.charAt(i);

            if (char == toCount)
                count++;
        }
        return count;
    }
    var setupCalculator = function () {
    
        $('#btnΣDT').click(function () {
            addToFormula($('#btnΣDT').val());
        });

        $('#btnΣDEC').click(function () {
            addToFormula($('#btnΣDEC').val());
        });
        
        $('#btnIRC').click(function () {
            addToFormula($('#btnIRC').val());
        });

        $('#btn7').click(function () {
            addToFormula($('#btn7').val());
        });

        $('#btn8').click(function () {
            addToFormula($('#btn8').val());
        });

        $('#btn9').click(function () {
            addToFormula($('#btn9').val());
        });

        $('#btnVEC').click(function () {
            addToFormula($('#btnVEC').val());
        });

        $('#btnnEC').click(function () {
            addToFormula($('#btnnEC').val());
        });

        $('#btnx̄VC').click(function () {
            addToFormula($('#btnx̄VC').val());
        });

        $('#btn4').click(function () {
            addToFormula($('#btn4').val());
        });

        $('#btn5').click(function () {
            addToFormula($('#btn5').val());
        });

        $('#btn6').click(function () {
            addToFormula($('#btn6').val());
        });

        $('#btnS').click(function () {
            addToFormula($('#btnS').val());
        });

        $('#btnPlus').click(function () {
            addToFormula($('#btnPlus').val(), true);
        });
        $('#btnSqrt').click(function () {
            addToFormula($('#btnSqrt').val(), true);
        });
        
        $('#btn-').click(function () {
            addToFormula($('#btn-').val(), true);
        });

        $('#btn1').click(function () {
            addToFormula($('#btn1').val());
        });

        $('#btn2').click(function () {
            addToFormula($('#btn2').val());
        });

        $('#btn3').click(function () {
            addToFormula($('#btn3').val());
        });

        $('#btnx').click(function () {
            addToFormula($('#btnx').val(), true);
        });

        $('#btnDiv').click(function () {
            addToFormula($('#btnDiv').val(), true);
        });

        $('#btnDot').click(function () {
            addToFormula($('#btnDot').val());
        });

        $('#btn0').click(function () {
            addToFormula($('#btn0').val());
        });

        $('#btnPer').click(function () {
            addToFormula($('#btnPer').val(), true);
        });

        $('#btnOp').click(function () {
            addToFormula($('#btnOp').val(), true);
        });

        $('#btnCp').click(function () {
            addToFormula($('#btnCp').val(), true);
        });

        $('#btnPo').click(function () {
            addToFormula($('#btnPo').val(), true);
        });

        $('#btnC').click(function () {
            $('#txtFormula').html('');
            array = [];
        });

        $('#btnD').click(function () {
            array.pop();
            var newFormula = array.join("");
            $('#txtFormula').html(newFormula);
        });
    }
    var array;
    var addToFormula = function (v,b) {
        if (b) v = '<font size="4"><b>' + v + '</b></font>';
        $('#txtFormula').append(v);
        array.push(v);
    };
    var addOrEditModalShow = function () {
        $("#addOrEditForm").find('select').not("[custom='true']").select2();
        $('#addOrEditModal').modal('show');
        loadFormula();
        setupCalculator();
        onSaveClick();
    };
   
    /* Public methods */
    return {
        Init: initialize,
        AddOrEditModalShow: addOrEditModalShow
      
    };
})();

