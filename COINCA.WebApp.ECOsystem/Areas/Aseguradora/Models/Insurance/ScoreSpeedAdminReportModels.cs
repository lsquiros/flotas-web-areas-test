﻿/************************************************************************************************************
*  File    : ScoreSpeedAdminReportModels.cs
*  Summary : ScoreSpeedAdminReport  Models
*  Author  : Alexander Aguero V
*  Date    : 21/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// Real Vs Budget Fuels Report Base
    /// </summary>
    public class ScoreSpeeAdminReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public ScoreSpeeAdminReportBase()
        {
            Parameters = new OperationReportsBase();
            List = new List<dynamic>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public OperationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }

    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class ScoreSpeeAdminReport
    {
        /// <summary>
        /// DriverScoreId
        /// </summary>
        [ExcelNoMappedColumn]
        public int DriverScoreId { get; set; }

        /// <summary>
        /// UserId
        /// </summary>
        [DisplayName("UserId")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int UserId { get; set; }

        /// <summary>
        /// Identification
        /// </summary>
        [DisplayName("Identificación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string EncryptedIdentification { get; set; }

        /// <summary>
        /// DecryptedIdentification
        /// </summary>
        [ExcelMappedColumn("Indentificación")]
        public string DecryptedIdentification
        {
            get { return TripleDesEncryption.Decrypt(EncryptedIdentification); }
            set { EncryptedIdentification = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        [ExcelMappedColumn("Nombre")]
        [NotMappedColumn]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double Score { get; set; }

        /// <summary>
        /// Scorestr
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelMappedColumn("% Puntuación")]
        public string Scorestr
        {
            get { return Miscellaneous.GetPercentageFormat((decimal)Score, 0); }
        }
    }
}