﻿/************************************************************************************************************
*  File    : WeightedIndexDrivingScoreFleetReportModels.cs
*  Summary : Weighted Index Driving Score Fleet Report Models
*  Author  : Andrés Oviedo Brenes
*  Date    : 16/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// 
    /// </summary>
    public class WeightedIndexDrivingScoreFleetReportBase
    {
        /// <summary>
        /// Weighted Index Driving Score Fleet Report Base Constructor
        /// </summary>
        public WeightedIndexDrivingScoreFleetReportBase()
        {
            Parameters = new OperationReportsBase();
            List = new List<WeightedIndexDrivingScoreFleet>();
            Menus = new List<AccountMenus>();
        }
        /// <summary>
        /// Report Parameters
        /// </summary>
        public OperationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<WeightedIndexDrivingScoreFleet> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }
    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class WeightedIndexDrivingScoreFleet
    {
        /// <summary>
        /// DriverScoreId
        /// </summary>
        [ExcelNoMappedColumn]
        public int DriverScoreId { get; set; }
        /// <summary>
        ///  CustomerId
        /// </summary>
        [DisplayName("CustomerId")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Flota")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        [ExcelMappedColumn("Flota")]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Score of the fleet
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelMappedColumn("Puntuación")]
        public double AverageScore { get; set; }
        
        /// <summary>
        /// Score of the fleet formatted
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string AverageScoreStr
        {
            get { return Miscellaneous.GetPercentageFormat((decimal)AverageScore, 0); }
        }

        /// <summary>
        /// Kms traveled by the fleet
        /// </summary>
        [DisplayName("Kilómetros Recorridos")]
        [NotMappedColumn]
        [ExcelMappedColumn("Kilómetros Recorridos")]
        public int KmTraveled { get; set; }

        /// <summary>
        /// Kms traveled by the fleet
        /// </summary>
        [DisplayName("Kilómetros Recorridos")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string KmTraveledStr
        {
            get { return Miscellaneous.GetNumberFormat(KmTraveled); }
        }

        /// <summary>
        /// Wighted Index of the fleet
        /// </summary>
        [DisplayName("Índice Ponderado")]
        [NotMappedColumn]
        [ExcelMappedColumn("Índice Ponderado")]
        public double WightedIndex { get; set; }

        /// <summary>
        /// Wighted Index of the fleet
        /// </summary>
        [DisplayName("Índice Ponderado")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string WightedIndexStr
        {
            get { return Miscellaneous.GetNumberFormat((decimal?)WightedIndex); }
        }
        
    }
}


