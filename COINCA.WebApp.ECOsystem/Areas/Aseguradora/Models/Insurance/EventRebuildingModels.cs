﻿/************************************************************************************************************
*  File    : EventRebuildingModels.cs
*  Summary : Event Rebuilding Models
*  Author  : Andrés Oviedo Brenes
*  Date    : 26/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using GridMvc.DataAnnotations;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// Base of the Event Rebuilding Model
    /// </summary>
    public class EventRebuildingBase
    {
        /// <summary>
        /// EventRebuildingBase Constructor
        /// </summary>
        public EventRebuildingBase()
        {
            Parameters = new OperationReportsBase();
            List = new List<EventRebuilding>();
            Data = new EventRebuilding();
        }
        /// <summary>
        /// OperationReportsBase
        /// </summary>
        public OperationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<EventRebuilding> List { get; set; }


        /// <summary>
        /// Model
        /// </summary>
        public EventRebuilding Data { get; set; }
    }

    public class EventRebuilding : ModelAncestor
    {

        /// <summary>
        /// DriverScoreId
        /// </summary>
        [NotMappedColumn]
        public int DriverScoreId { get; set; }

        /// <summary>
        /// Identity of the user
        /// </summary>
        [DisplayName("UserId")]
        [NotMappedColumn]
        public int UserId { get; set; }

        /// <summary>
        /// Name of the driver
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name of the driver
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Identification of the driver
        /// </summary>
        [DisplayName("Identificación")]
        [NotMappedColumn]
        public string EncryptedIdentification { get; set; }

        
        /// <summary>
        /// Identification of the driver
        /// </summary>
        [DisplayName("Identificación")]
        [NotMappedColumn]
        public string DecryptedIdentification
        {
            get { return TripleDesEncryption.Decrypt(EncryptedIdentification); }
            set { EncryptedIdentification = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Driving Score of the driver
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        public double Score { get; set; }

        /// <summary>
        /// Driving Score of the driver formatted
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        public string Scorestr
        {
            get { return Miscellaneous.GetPercentageFormat((decimal)Score, 0); }
        }

        /// <summary>
        /// Evaluated date
        /// </summary>
        [NotMappedColumn]
        public DateTime Date { get; set; }

        /// <summary>
        /// Evaluated date
        /// </summary>
        [DisplayName("Fecha")]
        [NotMappedColumn]
        public string DateStr { get { return Miscellaneous.GetDateFormat(Date); } }

        /// <summary>
        /// Identifier of the vehicle
        /// </summary>
        [DisplayName("Vehículo")]
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Identifier of the customer
        /// </summary>
        [NotMappedColumn]
        public int? CustomerId { get; set; }

        /// <summary>
        /// License Plate of the vehicle
        /// </summary>
        [NotMappedColumn]
        public string PlateId { get; set; }

        /// <summary>
        /// Reference of the vehicle in the Intrack database
        /// </summary>
        [NotMappedColumn]
        public int IntrackReference { get; set; }

        /// <summary>
        /// Country Code 
        /// </summary>
        [NotMappedColumn]
        public string CountryCode { get; set; }

        /// <summary>
        /// Time Zone of the country
        /// </summary>
        [NotMappedColumn]
        public int UserTimeZone { get { return 0.ToTimeOffSet(); } set { 0.ToTimeOffSet(); } }

        /// <summary>
        /// Initial date to search
        /// </summary>
        [NotMappedColumn]
        public string StartDate { get; set; }

        /// <summary>
        /// Final date to search
        /// </summary>
        [NotMappedColumn]
        public string EndDate { get; set; }

    }
}


