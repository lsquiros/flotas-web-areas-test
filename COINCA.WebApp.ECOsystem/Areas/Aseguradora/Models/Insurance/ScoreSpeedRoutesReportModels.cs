﻿/************************************************************************************************************
*  File    : ScoreSpeedRoutesReportModels.cs
*  Summary : Score Speed Routes Report Models Models
*  Author  : Alexander Aguero V
*  Date    : 21/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// Real Vs Budget Fuels Report Base
    /// </summary>
    public class ScoreSpeeRoutesReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public ScoreSpeeRoutesReportBase()
        {
            Parameters = new OperationReportsBase();
            List = new List<dynamic>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public OperationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }
    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class ScoreSpeeRoutesReport
    {
        /// <summary>
        /// DriverScoreId
        /// </summary>
        [ExcelNoMappedColumn]
        public int DriverScoreId { get; set; }

        /// <summary>
        /// UserId
        /// </summary>
        [DisplayName("UserId")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int UserId { get; set; }

        /// <summary>
        /// Identification
        /// </summary>
        [DisplayName("Identificación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string EncryptedIdentification { get; set; }

        [ExcelMappedColumn("Identificación")]
        public string DecryptedIdentification
        {
            get { return TripleDesEncryption.Decrypt(EncryptedIdentification); }
            set { EncryptedIdentification = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double Score { get; set; }

        /// <summary>
        /// Scorestr
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelMappedColumn("% Puntuación")]
        public string Scorestr
        {
            get { return Miscellaneous.GetPercentageFormat((decimal)Score, 0); }
        }


    }

}



