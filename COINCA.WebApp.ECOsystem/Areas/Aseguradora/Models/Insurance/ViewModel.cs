﻿/************************************************************************************************************
*  File    : ViewModel.cs
*  Summary : ViewModel
*  Author  : Manuel Azofeifa Hidalgo 
*  Date    : 17/Nov/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using ECOsystem.Models.Core;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// View Model
    /// </summary>
    public class ViewModelInsurance
    {
        /// <summary>
        /// rssModels
        /// </summary>
        public IEnumerable<PartnerNews> rssModels { get; set; }

        /// <summary>
        /// caIndicator
        /// </summary>
        public CAIndicators caIndicator { get; set; }

        /// <summary>
        /// realVsBudgetFuelsReportBase
        /// </summary>
        public RealVsBudgetFuelsReportBase realVsBudgetFuelsReportBase { get; set; }

        /// <summary>
        /// Score Speed Routes Report 
        /// </summary>
        public ScoreSpeeRoutesReportBase scoreSpeedRoutesReportBase { get; set; }

        /// <summary>
        /// Score Spee Admi nReport Base
        /// </summary>
        public ScoreSpeeAdminReportBase scoreSpeeAdminReportBase { get; set; }

        /// <summary>
        /// Score Spee Admi nReport Base
        /// </summary>
        public ScoreGlobalReportBase scoreGlobalReportBase { get; set; }

        /// <summary>
        /// Driving Score Fleet Report Base
        /// </summary>
        public DrivingScoreFleetReportBase DrivingScoreFleetReport { get; set; }

        /// <summary>
        /// Driving Score Fleet Evolution Report Base
        /// </summary>
        public DrivingScoreFleetEvolutionReportBase DrivingScoreFleetEvolutionReport { get; set; }

        /// <summary>
        /// Low Driving Score Fleet Evolution Report Base
        /// </summary>
        public LowDrivingScoreFleetEvolutionReportBase LowDrivingScoreFleetEvolutionReport { get; set; }


        /// <summary>
        /// Weighted Index Driving Score Fleet Report Base
        /// </summary>
        public WeightedIndexDrivingScoreFleetReportBase WeightedIndexDrivingScoreFleetReport { get; set; }

        /// <summary>
        /// Dasboad Modules
        /// </summary>
        public IEnumerable<DashboardModule> dasboadModules { get; set; }


        /// <summary>
        /// CustomerByPartner
        /// </summary>
        public CustomerByPartnerData Data { get; set; }
    }
}