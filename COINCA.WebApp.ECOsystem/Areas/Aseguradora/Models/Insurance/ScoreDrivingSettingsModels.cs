﻿/************************************************************************************************************
*  File    : ScoreDrivingSettingsModels.cs
*  Summary : Score Driving Settings Models
*  Author  : Andrés Oviedo Brenes
*  Date    : 11/03/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// Score Driving Settings
    /// </summary>
    public class ScoreDrivingSettings
    {

        /// <summary>
        /// Are the driving scores visible to the partner
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public bool PartnerVisibility { get; set; }

    }

   
}


