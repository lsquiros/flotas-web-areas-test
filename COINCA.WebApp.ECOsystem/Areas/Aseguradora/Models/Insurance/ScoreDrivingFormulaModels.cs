﻿/************************************************************************************************************
*  File    : ScoreDrivingFormulaModels.cs
*  Summary : Score Driving Formula  Models
*  Author  : Andrés Oviedo Brenes
*  Date    : 23/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// Score Driving Formula Base
    /// </summary>
    public class ScoreDrivingFormulaBase
    {
        /// <summary>
        /// Score Driving Formula Base Constructor
        /// </summary>
        public ScoreDrivingFormulaBase()
        {
            List = new List<ScoreDrivingFormula>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public ScoreDrivingFormula Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<ScoreDrivingFormula> List { get; set; }

    }

    /// <summary>
    /// Score Driving Formula
    /// </summary>
    public class ScoreDrivingFormula:ModelAncestor
    {
        /// <summary>
        /// Identifier of the Score Driver Formula
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int? ScoreDrivingFormulaId { get; set; }

        /// <summary>
        /// Name of the Score Driver Formula
        /// </summary>
        [DisplayName("Nombre")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Nombre", Width = "150px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// Score Driver Formula
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string ScoreDrivingFormulaStr { get; set; }

        /// <summary>
        /// Score Driver Formula
        /// </summary>
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Fórmula", Width = "150px", SortEnabled = true)]
        public string ScoreDrivingFormulaStrFormatted { get
        {
            if (ScoreDrivingFormulaStr != null)return ScoreDrivingFormulaStr.Replace("SDEC", "ΣDEC").Replace("SDT", "ΣDT").Replace("x¯VC", "x̄VC").Replace("xn", "xⁿ").Replace("sqrt", "√");
            return "";
        } }

        /// <summary>
        /// Store Procedure of the Score Driver Formula
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string ScoreDrivingFormulaSP { get; set; }

        /// <summary>
        /// Is the Formula Active
        /// </summary>
        [ExcelNoMappedColumn]
        [DisplayName("Predeterminada")]
        [NotMappedColumn]
        public bool IsActive { get; set; }

        /// <summary>
        /// Is the Formula Active
        /// </summary>
        [ExcelNoMappedColumn]
        [DisplayName("Predeterminada")]
        [NotMappedColumn]
        public bool IsPending { get; set; }

        /// <summary>
        /// Is the Formula Active
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public bool IsApproved { get; set; }
        

        /// <summary>
        /// Customer asociated to the formula
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int? CustomerId { get; set; }
    }
}


