﻿/************************************************************************************************************
*  File    : ScoreSpeedAdminReportModels.cs
*  Summary : ScoreSpeedAdminReport  Models
*  Author  : Andrés Oviedo Brenes
*  Date    : 17/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// 
    /// </summary>
    public class DrivingScoreFleetDataSourceReportBase
    {
        /// <summary>
        /// Driving Score Fleet Data Source Report Base Constructor
        /// </summary>
        public DrivingScoreFleetDataSourceReportBase()
        {
            Parameters = new OperationReportsBase();
            List = new List<DrivingScoreFleetDataSource>();
            Menus = new List<AccountMenus>();
        }
        /// <summary>
        /// Report Parameters
        /// </summary>
        public OperationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<DrivingScoreFleetDataSource> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }
    /// <summary>
    /// Driving Score Fleet Data Source
    /// </summary>
    public class DrivingScoreFleetDataSource
    {
   

        /// <summary>
        /// ScoreTypescr for Excel
        /// </summary>
        [ExcelMappedColumn("Tipo de Velocidad")]
        public string ScoreTypeStr
        {
            get {  return ScoreType.Equals("SPADM") ? "Administrativa":"Carretera";  }
        }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        [ExcelMappedColumn("Nombre del Conductor")]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Identification of the driver
        /// </summary>
        [ExcelMappedColumn("Identificación")]
        public string DecryptedIdentification { get { return TripleDesEncryption.Decrypt(EncryptedIdentification); } }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Flota")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string EncryptedIdentification { get; set; }

        /// <summary>
        /// Over Speed Amount of the vehicle
        /// </summary>
        [ExcelMappedColumn("NEC: Cantidad de Exceso de Velocidad")]
        public double OverSpeedAmount { get; set; }

        /// <summary>
        /// Over Speed Distance of the vehicle
        /// </summary>
        [ExcelNoMappedColumn] //("Distancia en Exceso de Velocidad")
        public double OverSpeedDistance { get; set; }

        /// <summary>
        /// Over Speed Distance Percentage of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public double OverSpeedDistancePercentage { get; set; }

        /// <summary>
        /// Over Speed Average of the vehicle
        /// </summary>
        [ExcelMappedColumn("x̄VC Promedio de Exceso de Velocidad")]
        public double OverSpeedAverage { get; set; }

        /// <summary>
        /// Over Speed Weihgted Average of the vehicle
        /// </summary>
        [ExcelNoMappedColumn] //("Promedio Ponderado de Exceso de Velocidad")
        public double OverSpeedWeihgtedAverage { get; set; }

        /// <summary>
        /// Standard Deviation of the vehicle
        /// </summary>
        [ExcelMappedColumn("S: Desviación Estándar")]
        public double StandardDeviation { get; set; }

        /// <summary>
        /// Variance of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public double Variance { get; set; }

        /// <summary>
        /// Variation Coefficient of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public double VariationCoefficient { get; set; }

        /// <summary>
        /// Kms Traveled by the vehicle
        /// </summary>
        [ExcelMappedColumn("ΣDT Sumatoria Distancia Total")]
        public string KmTraveledStr { get { return Miscellaneous.GetNumberFormat((decimal?)KmTraveled); } }

        /// <summary>
        /// Over Speed of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string OverSpeedStr { get { return  Miscellaneous.GetNumberFormat((decimal?)OverSpeed); } }

        /// <summary>
        /// Over Speed Amount of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string OverSpeedAmountStr { get { return Miscellaneous.GetNumberFormat((decimal?)OverSpeedAmount); } }

        /// <summary>
        /// Over Speed Distance of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string OverSpeedDistanceStr { get { return Miscellaneous.GetNumberFormat((decimal?)OverSpeedDistance); } }

        /// <summary>
        /// Over Speed Distance Percentage of the vehicle
        /// </summary>
        [ExcelMappedColumn("IRC: Porcentaje en Distancia Total de Exceso de Velocidad")]
        public string OverSpeedDistancePercentageStr { get { return Miscellaneous.GetPercentageFormat((decimal?)OverSpeedDistancePercentage * 100); } }

        /// <summary>
        /// Over Speed Average of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string OverSpeedAverageStr { get { return Miscellaneous.GetNumberFormat((decimal?)OverSpeedAverage); } }

        /// <summary>
        /// Over Speed Weihgted Average of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string OverSpeedWeihgtedAverageStr { get { return Miscellaneous.GetNumberFormat((decimal?)OverSpeedWeihgtedAverage); } }

        /// <summary>
        /// Standard Deviation of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string StandardDeviationStr { get { return Miscellaneous.GetNumberFormat((decimal?)StandardDeviation); } }

        /// <summary>
        /// Variance of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string VarianceStr { get { return Miscellaneous.GetNumberFormat((decimal?)Variance); } }

        /// <summary>
        /// Variation Coefficient of the vehicle
        /// </summary>
        [ExcelNoMappedColumn]
        public string VariationCoefficientStr { get { return Miscellaneous.GetNumberFormat((decimal?)VariationCoefficient); } }

        [ExcelNoMappedColumn]
        public string AverageScoreStr { get { return Miscellaneous.GetNumberFormat((decimal?)AverageScore); } }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [ExcelMappedColumn("CS: Calificación")]
        public double AverageScore { get; set; }

        /// <summary>
        /// Type of the score
        /// </summary>
        [ExcelNoMappedColumn]
        public string ScoreType { get; set; }

        /// <summary>
        /// Identifier of the score
        /// </summary>
        [ExcelNoMappedColumn]
        public int DriverScoreId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Flota")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string EncryptedName { get; set; }
     

        /// <summary>
        /// Identifier of the driver user
        /// </summary>
        [ExcelNoMappedColumn]
        public int? UserId { get; set; }

        /// <summary>
        /// Identifier of the customer
        /// </summary>
        [DisplayName("CustomerId")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// Name of the customer
        /// </summary>
        [DisplayName("Cliente")]
        [NotMappedColumn]
        [ExcelNoMappedColumn] //("Flota")
        public string CustomerName { get { return TripleDesEncryption.Decrypt(EncryptedCustomerName); } }

        /// <summary>
        /// Name of the customer
        /// </summary>
        [DisplayName("Cliente")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string EncryptedCustomerName { get; set; }

        /// <summary>
        /// Kms Traveled by the vehicle
        /// </summary>
        [ExcelNoMappedColumn] //("Kilómetros Recorridos")
        public double KmTraveled { get; set; }

        /// <summary>
        /// Over Speed of the vehicle
        /// </summary>
        [ExcelNoMappedColumn] //("Exceso de Velocidad")
        public double OverSpeed { get; set; }



    }
}

