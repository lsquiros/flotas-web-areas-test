﻿/************************************************************************************************************
*  File    : ScoreSpeedAdminReportModels.cs
*  Summary : ScoreSpeedAdminReport  Models
*  Author  : Alexander Aguero V
*  Date    : 21/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// 
    /// </summary>
    public class ScoreGlobalReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public ScoreGlobalReportBase()
        {
            Parameters = new OperationReportsBase();
            List = new List<ScoreGlobalReport>();
            Menus = new List<AccountMenus>();
            LowScoreCount = 0;
            RegularScoreCount= 0;
            SuperiorScoreCount = 0;
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public OperationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<ScoreGlobalReport> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Low Driver Score Count 
        /// </summary>
        public decimal LowScoreCount { get; set; }

        /// <summary>
        /// Regular Driver Score Percentage
        /// </summary>
        public decimal RegularScoreCount { get; set; }

        /// <summary>
        /// Superior Driver Score Percentage
        /// </summary>
        public decimal SuperiorScoreCount { get; set; }               
    }
    
    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class ScoreGlobalReport
    {
        /// <summary>
        /// DriverScoreId
        /// </summary>
        [ExcelNoMappedColumn]
        public int DriverScoreId { get; set; }
        
        /// <summary>
        /// UserId
        /// </summary>
        [DisplayName("Usuario")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int UserId { get; set; }

        /// <summary>
        /// VehicleId
        /// </summary>        
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Identification
        /// </summary>
        [DisplayName("Identificación")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string EncryptedIdentification { get; set; }

        [DisplayName("Identificación")]
        public string DecryptedIdentification
        {
            get { return TripleDesEncryption.Decrypt(EncryptedIdentification); }
            set { EncryptedIdentification = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// TraveledDistance
        /// </summary>
        [DisplayName("Distancia Recorrida")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double TraveledDistance { get; set; }

        /// <summary>
        /// TraveledDistanceStr
        /// </summary>
        [DisplayName("Distancia Recorrida")]
        [NotMappedColumn]
        [ExcelMappedColumn("ΣDT")]
        public decimal TraveledDistanceStr
        {
            get { return Math.Round(decimal.Parse(TraveledDistance.ToString()), 0); }
        }
        
        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double Score { get; set; }

        /// <summary>
        /// ScoreType
        /// </summary>
        [DisplayName("Tipo Puntuación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string ScoreType { get; set; }

        /// <summary>
        /// Photo
        /// </summary>
        [DisplayName("Foto")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string Photo { get; set; }

        /// <summary>
        /// Scorestr
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string Scorestr
        {
            get { return Miscellaneous.GetPercentageFormat((decimal)Score, 0); }
        }

        /// <summary>
        /// ScoreSpeedAdmin
        /// </summary>
        [DisplayName("Puntuación Administrativa")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double ScoreSpeedAdmin { get; set; }

        /// <summary>
        /// ScoreSpeedAdminstr
        /// </summary>
        [DisplayName("Puntuación Administrativa")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string ScoreSpeedAdminstr
        {
            get { return Miscellaneous.GetPercentageFormat((decimal)ScoreSpeedAdmin, 0); }
        }

        /// <summary>
        /// ScoreSpeedRoute
        /// </summary>
        [DisplayName("Puntuación Carretera")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public double ScoreSpeedRoute { get; set; }

        /// <summary>
        /// ScoreSpeedRouteStr
        /// </summary>
        [DisplayName("Puntuación Carretera")]
        [NotMappedColumn]
        [ExcelMappedColumn("CS")]
        public string ScoreSpeedRouteStr
        {
           get { return Miscellaneous.GetPercentageFormat((decimal)ScoreSpeedRoute, 0); }
        }

        /// <summary>
        /// WeightedIndexByRoute
        /// </summary>
        [DisplayName("Indice Ponderado por Ley")]
        [NotMappedColumn]
        [ExcelMappedColumn("IPC[C]")]
        public decimal WeightedIndexByRoute
        {
            get
            {
                if (ScoreSpeedRoute != 0)
                    return decimal.Parse(Math.Round((TraveledDistance/ScoreSpeedRoute),2).ToString());
                return 0;
            }
        }

        /// <summary>
        /// WeightedIndexByAdmin
        /// </summary>
        [DisplayName("Indice Ponderado por Empresa")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal WeightedIndexByAdmin
        {
            get
            {
                if (ScoreSpeedAdmin != 0)
                    return decimal.Parse(Math.Round((TraveledDistance / ScoreSpeedAdmin),2).ToString()) ;
                return 0;
            }
        }
    }
}