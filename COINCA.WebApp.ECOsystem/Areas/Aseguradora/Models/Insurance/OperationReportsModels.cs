﻿/************************************************************************************************************
*  File    : ControlReportsModels.cs
*  Summary : Control Reports Models
*  Author  : Berman Romero
*  Date    : 21/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web;
using ECOsystem.Utilities;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// Report Fuel Types Enum
    /// </summary>
    public enum ReportFuelTypes
    {
        /// <summary>
        /// Vehicles
        /// </summary>
        Vehicles = 100,
        /// <summary>
        /// Vehicle Groups
        /// </summary>
        VehicleGroups = 200,
        /// <summary>
        /// Vehicle Sub Units
        /// </summary>
        VehicleCostCenters = 300,
        /// <summary>
        /// Cost Center
        /// </summary>
        CostCenter = 400
    }

    /// <summary>
    /// Report Fuel Types Enum
    /// </summary>
    public enum ReportCriteria
    {
        /// <summary>
        /// Period
        /// </summary>
        Period = 1000,
        /// <summary>
        /// Date Range
        /// </summary>
        DateRange = 2000,
        /// <summary>
        /// Date Range
        /// </summary>
        DayDate = 3000

    }

    /// <summary>
    /// Control Fuels Reports Base Model
    /// </summary>
    public class OperationReportsBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public OperationReportsBase()
        {
            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;   //  !!!!!!!!!!!!!   CAMBIAR !!!!!!!!!!!!!!!!!!
            Month =  currentDate.Month;     // !!!!!!!!!!!!!   CAMBIAR !!!!!!!!!!!!!!!!!!

            ReportCriteriaId = (int?)ReportCriteria.Period;
            ReportFuelTypeId = (int?)ReportFuelTypes.Vehicles;
        }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart titles
        /// </summary>
        public HtmlString Titles { get; set; }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart titles
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart labels
        /// </summary>
        public HtmlString Labels { get; set; }

        /// <summary>
        /// Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString Colors { get; set; }

        /// <summary>
        /// Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString Colors2{ get; set; }

        /// <summary>
        /// Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString Colors3 { get; set; }

        /// <summary>
        /// Alpha Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString AlphaColors { get; set; }
        
        /// <summary>
        /// Highlight Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString HighlightColors { get; set; }
        
        /// <summary>
        /// Highlight Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString HighlightColors2 { get; set; }
        
        /// <summary>
        /// Highlight Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString HighlightColors3 { get; set; }

        /// <summary>
        /// Data formatted as JSON for second chart serie
        /// </summary>
        public HtmlString Data { get; set; }

        /// <summary>
        /// Data formatted as JSON for second chart serie
        /// </summary>
        public HtmlString Data2 { get; set; }

        /// <summary>
        /// Data formatted as JSON for second chart serie
        /// </summary>
        public HtmlString Data3 { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// MonthName
        /// </summary>
        public string MonthName
        {
            get { return Miscellaneous.GetMonthName(Month); }
        }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string StartDateStr
        {
            get { return Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date for Report when dates range is selected
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string EndDateStr
        {
            get { return Miscellaneous.GetDateFormat(EndDate); }
            set { EndDate = Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// ScoreType  type the  score SPADM>> Administrative  SPROU>> Road
        /// </summary>
        public string ScoreOpc { get; set; }
        public string ScoreType { get; set; } // R: Road, A: Administrative
        public string ReportType { get; set; } //S:Summarized, D:Detailed
        public string InvocationType { get; set; } //R: Report, J:Job

        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }

        /// <summary>
        /// Report Fuel Type Id
        /// </summary>
        public int? ReportFuelTypeId { get; set; }

        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64 { get; set; }

        public int? RedLower { get; set; }

        /// <summary>
        /// Error margin of odometer with POS
        /// </summary>
        public int? RedHigher { get; set; }

        /// <summary>
        /// Tolerance time to get arrival to point
        /// </summary>
        public int? YellowLower { get; set; }

        /// <summary>
        /// Error Margin of geopoint
        /// </summary>
        public int? YellowHigher { get; set; }

        public int? GreenLower { get; set; }

        /// <summary>
        /// Passwords to Validate Repetition
        /// </summary>
        public int? GreenHigher { get; set; }

        /// <summary>
        /// Indentifier of the vehicle
        /// </summary>
        public int? VehicleId { get; set; }

       
        public DateTime? DayDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string DayDateStr
        {
            get { return Miscellaneous.GetDateFormat(DayDate); }
            set { DayDate = Miscellaneous.SetDate(value); }
        }
    }
}