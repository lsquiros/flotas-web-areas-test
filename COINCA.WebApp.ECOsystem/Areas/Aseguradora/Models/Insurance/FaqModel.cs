﻿using System.Collections.Generic;
using ECOsystem.Models.Account;

namespace ECO_Insurance.Models.Insurance
{
    public class FaqModelBase
    {
        public FaqModelBase()
        {
            Data = new List<FaqModel>();
            Menus = new List<AccountMenus>();
        }
       
        public IEnumerable<FaqModel> Data { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    public class FaqModel
    {
        private int index;
        private string question;
        private string answer;

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public string Question
        {
            get { return question; }
            set { question = value; }
        }

        public string Answer
        {
            get { return answer; }
            set { answer = value; }
        }

    }
}