﻿/************************************************************************************************************
*  File    : LowScoreDrivingFleetEvolutionReportModels.cs
*  Summary : Low Score Driving Fleet Evolution Report Models
*  Author  : Andrés Oviedo Brenes
*  Date    : 13/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECO_Insurance.Models.Insurance
{
    /// <summary>
    /// 
    /// </summary>
    public class LowDrivingScoreFleetEvolutionReportBase
    {
        /// <summary>
        /// LowScoreDrivingFleetEvolutionReportBase Constructor
        /// </summary>
        public LowDrivingScoreFleetEvolutionReportBase()
        {
            Parameters = new OperationReportsBase();
            List = new List<LowScoreDrivingFleetEvolution>();
            Menus = new List<AccountMenus>();
        }
        public List<string> Titles { get; set; }

        public int ColumnCount { get; set; }
        /// <summary>
        /// Report Parameters
        /// </summary>
        public OperationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<LowScoreDrivingFleetEvolution> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

    }
    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class LowScoreDrivingFleetEvolution
    {
        /// <summary>
        /// DriverScoreId
        /// </summary>
        [ExcelNoMappedColumn]
        public int DriverScoreId { get; set; }
        /// <summary>
        ///  CustomerId
        /// </summary>
        [DisplayName("CustomerId")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int CustomerId { get; set; }
        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Flota")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        [ExcelMappedColumn("Flota")]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelMappedColumn("Puntuación")]
        public double AverageScore { get; set; }
      
        /// <summary>
        /// AverageScorestr
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string AverageScorestr
        {
            get { return Miscellaneous.GetPercentageFormat((decimal)AverageScore, 0); }
        }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [ExcelNoMappedColumn]
        public int Month { get; set; }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Mes")]
        [NotMappedColumn]
        [ExcelMappedColumn("Mes")]
        public string MonthStr { get { return Miscellaneous.GetMonthName(Month); } }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Año")]
        [NotMappedColumn]
        [ExcelMappedColumn("Año")]
        public int Year { get; set; }
    }
}


