﻿/************************************************************************************************************
*  File    : TypesBusiness.cs
*  Summary : Types Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECO_Insurance.Models.Insurance;
//using COINCA.Library.DataAccess;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// Parameters Business Class
    /// </summary>
    public class TypesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Parameters
        /// </summary>
        /// <param name="usage"></param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Types> RetrieveTypes(string usage)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Types>("[General].[Sp_Types_Retrieve]", new
                {
                    Usage = usage
                });
            }
        }

        
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}