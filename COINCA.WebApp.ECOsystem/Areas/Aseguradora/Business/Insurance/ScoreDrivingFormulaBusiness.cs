﻿/************************************************************************************************************
*  File    : ScoreDrivingFormulaBusiness.cs
*  Summary : Score Driving Formula Business Methods
*  Author  : Andrés Oviedo
*  Date    : 23/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.Business.Core;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using ECO_Insurance.Models.Insurance;
//using COINCA.Library.DataAccess;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// Score Driving Formula Business
    /// </summary>
    public class ScoreDrivingFormulaBusiness : IDisposable
    {

        /// <summary>
        /// Get Score Driving Formula for the customer
        /// </summary>
        public IEnumerable<ScoreDrivingFormula> GetScoreDrivingFormulas(ScoreDrivingFormula model,string key=null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ScoreDrivingFormula>("[Insurance].[Sp_GetScoreDrivingFormulas]",
                    new
                    {
                        model.ScoreDrivingFormulaId, model.CustomerId,
                        Key = key
                    });
            }
          
        }

        /// <summary>
        /// Add or Edit Driving Formula for the customer
        /// </summary>
        public void AddOrEditScoreDrivingFormula(ScoreDrivingFormula model)
        {
            int? ScoreDrivingFormulaId=null;
            using (var dba = new DataBaseAccess())
            {
                ScoreDrivingFormulaId=dba.ExecuteScalar<int?>("[Insurance].[Sp_ScoreDrivingFormula_AddOrEdit]",
                    new
                    {
                        model.ScoreDrivingFormulaId,
                        model.Name,
                        model.ScoreDrivingFormulaStr,
                        model.ScoreDrivingFormulaSP,
                      //  model.IsActive,
                        model.CustomerId,
                        model.RowVersion,
                        model.LoggedUserId
                    });
            }
                
                using (var business = new UsersBusiness())
                {
                    foreach (Users user in business.RetrieveUsers(null, null, null, model.CustomerId).ToList())
                    {
                            if (("SUPER_ADMIN").Equals(user.RoleName))
                            {
                                string email = user.DecryptedEmail;

                                using (var customersBusiness = new CustomersBusiness())
                                {
                                    Customers customer=customersBusiness.RetrieveCustomers(model.CustomerId).FirstOrDefault();
                                    new EmailSender().SendEmail(email, "Notificación Flotas BAC Credomatic - Solicitud de Fórmula de Hábitos de Conducción", "El cliente " + customer.DecryptedName + " ha solicitado la fórmula con el índice " + ScoreDrivingFormulaId);
                                }
                            }
                    }
                }
       
        }

        /// <summary>
        /// Performs the the operation of delete on the model ScoreDrivingFormula
        /// </summary>
        /// <param name="scoreDrivingFormulaId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteScoreDrivingFormula(int scoreDrivingFormulaId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Insurance].[Sp_ScoreDrivingFormula_Delete]",
                    new { ScoreDrivingFormulaId = scoreDrivingFormulaId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}