﻿/************************************************************************************************************
*  File    : DrivingScoreFleetEvolutionReportBusiness.cs
*  Summary : Driving Score Fleet EvolutionReport Business Methods
*  Author  : Andrés Oviedo
*  Date    : 13/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Business.Core;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Insurance.Models.Insurance;
using Newtonsoft.Json;

//using CurrentFuelsByVehicle = ECOsystem.Models.Operation.CurrentFuelsByVehicle;
//using CurrentFuelsByVehicleGroupReport = ECOsystem.Models.Operation.CurrentFuelsByVehicleGroupReport;
//using CurrentFuelsByVehicleSubUnitReport = ECOsystem.Models.Operation.CurrentFuelsByVehicleSubUnitReport;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// Driving Score Fleet Evolution Report
    /// </summary>
    public class DrivingScoreFleetEvolutionReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Driving Score Fleet Evolution Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of DrivingScoreFleetEvolutionReportBase in order to load the chart</returns>
        public DrivingScoreFleetEvolutionReportBase RetrieveScoreGlobalSumary(OperationReportsBase parameters)
        {
            var result = new DrivingScoreFleetEvolutionReportBase();
            if (string.IsNullOrEmpty(parameters.ReportType))
                parameters.ReportType = "R"; //S:Summarized, D:Detailed R:OnlyRouteScore 
            GetScoreDrivingFleetEvolution(parameters, result);
            FillColors(result);
            result.List = result.List.ToList();

            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DrivingScoreFleetEvolutionReportBase RetrieveDrivingScoreFleetEvolutionReport(OperationReportsBase parameters)
        {
            var result = new DrivingScoreFleetEvolutionReportBase();
            if (string.IsNullOrEmpty(parameters.ReportType))
            {
                //   result.Parameters.Data = new HtmlString("[]") ;
                //  result.Parameters.Labels = new HtmlString("[]");
                parameters.ReportType = "S"; //S:Summarized, D:Detailed
                GetReportData(parameters, result);

            }
            else
                GetReportData(parameters, result);

            // FillColors(result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }

        /// <summary>
        /// Fill colors to used in chart,using the list of customers related to the partner(At the moment only take one customer)
        /// </summary>
        /// <param name="result"></param>
        private void FillColorsByCustomers(DrivingScoreFleetEvolutionReportBase result)
        {
            IList<string> colors = new List<string>();
            IList<string> highlightColors = new List<string>();
            IList<string> colors2 = new List<string>();
            IList<string> highlightColors2 = new List<string>();
            IList<string> colors3 = new List<string>();
            IList<string> highlightColors3 = new List<string>();
            using (var objsholdsBusi = new CustomerThresholdsBusiness())
            {
                int? customerId = Session.GetCustomersIdByPartner().FirstOrDefault();

                var ThesholdsBusi = objsholdsBusi.RetrieveThresholdByCustomer(customerId).FirstOrDefault();
                               
                foreach (DrivingScoreFleetEvolution item in result.List)
                {
                    // if (item.Score <= 69)
                    if (item.AverageScore <= ThesholdsBusi.RedHigher && item.AverageScore >= ThesholdsBusi.RedLower)
                    {
                        if (item.Month == GetPreviousDate(result.Parameters.Month - 2, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 2, result.Parameters.Year)[1])
                        {
                            colors.Add("rgba(217, 83, 79,1)"); //red
                            highlightColors.Add("rgba(217, 83, 79,0.9)"); //red
                        }
                        else if (item.Month == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[1])
                        {
                            colors2.Add("rgba(217, 83, 79,1)"); //red
                            highlightColors2.Add("rgba(217, 83, 79,0.9)"); //red
                        }
                        else if (item.Month == result.Parameters.Month && item.Year == result.Parameters.Year)
                        {
                            colors3.Add("rgba(217, 83, 79,1)"); //red
                            highlightColors3.Add("rgba(217, 83, 79,0.9)"); //red
                        }
                    }
                    else
                        //  if (item.Score >= 70 && item.Score <= 89)
                        if (item.AverageScore <= ThesholdsBusi.YellowHigher && item.AverageScore >= ThesholdsBusi.YellowLower)
                        {
                            if (item.Month == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[1])
                            {
                                colors.Add("rgba(240, 173, 78,1)"); //yellow
                                highlightColors.Add("rgba(240, 173, 78,0.9)"); //yellow
                            }
                            else if (item.Month == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[1])
                            {
                                colors2.Add("rgba(240, 173, 78,1)"); //yellow
                                highlightColors2.Add("rgba(240, 173, 78,0.9)"); //yellow
                            }
                            else if (item.Month == result.Parameters.Month && item.Year == result.Parameters.Year)
                            {
                                colors3.Add("rgba(240, 173, 78,1)"); //yellow
                                highlightColors3.Add("rgba(240, 173, 78,0.9)"); //yellow
                            }
                        }
                        else
                            // if (item.Score > 89)
                            if (item.AverageScore <= ThesholdsBusi.GreenHigher && item.AverageScore >= ThesholdsBusi.GreenLower)
                            {
                                if (item.Month == result.Parameters.Month  && item.Year == result.Parameters.Month)
                                {
                                    colors.Add("rgba(92, 184, 92,1)"); //green
                                    highlightColors.Add("rgba(92, 184, 92,0.9)"); //green
                                }
                                else if (item.Month == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[1])
                                {
                                    colors2.Add("rgba(92, 184, 92,1)"); //green
                                    highlightColors2.Add("rgba(92, 184, 92,0.9)"); //green
                                }
                                else if (item.Month == result.Parameters.Month && item.Year == result.Parameters.Year)
                                {
                                    colors3.Add("rgba(92, 184, 92,1)"); //green
                                    highlightColors3.Add("rgba(92, 184, 92,0.9)"); //green
                                }
                            }
                }
                result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
                result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(highlightColors));

                result.Parameters.Colors2 = new HtmlString(JsonConvert.SerializeObject(colors2));
                result.Parameters.HighlightColors2 = new HtmlString(JsonConvert.SerializeObject(highlightColors2));

                result.Parameters.Colors3 = new HtmlString(JsonConvert.SerializeObject(colors3));
                result.Parameters.HighlightColors3 = new HtmlString(JsonConvert.SerializeObject(highlightColors3));
            }

        }

        /// <summary>
        /// Fill colors By Month
        /// </summary>
        /// <param name="result"></param>
        private void FillColorsByMonth(DrivingScoreFleetEvolutionReportBase result)
        {
            IList<string> colors = new List<string>();
            IList<string> highlightColors = new List<string>();
            IList<string> colors2 = new List<string>();
            IList<string> highlightColors2 = new List<string>();
            IList<string> colors3 = new List<string>();
            IList<string> highlightColors3 = new List<string>();
            using (var objsholdsBusi = new CustomerThresholdsBusiness())
            {
                int? customerId = Session.GetCustomersIdByPartner().FirstOrDefault();

                var ThesholdsBusi = objsholdsBusi.RetrieveThresholdByCustomer(customerId).FirstOrDefault();

                foreach (DrivingScoreFleetEvolution item in result.List)
                {
                    if (item.Month == GetPreviousDate(result.Parameters.Month - 2, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 2, result.Parameters.Year)[1])
                    {
                        colors.Add("rgba(129,91,58,1)"); //  "#815b3a",//
                        highlightColors.Add("rgba(129,91,58,0.9)"); //
                    }
                    else if (item.Month == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[1])
                    {
                        colors2.Add("rgba(59, 127, 196,1)"); //  "#D38E1F",//
                        highlightColors2.Add("rgba(59, 127, 196,0.9)"); //
                    }
                    else if (item.Month == result.Parameters.Month && item.Year == result.Parameters.Year)
                    {
                        colors3.Add("rgba(241,92,117,1)"); // "#2C662D",//
                        highlightColors3.Add("rgba(241,92,117,0.9)"); //
                    }
                }

                result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
                result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(highlightColors));

                result.Parameters.Colors2 = new HtmlString(JsonConvert.SerializeObject(colors2));
                result.Parameters.HighlightColors2 = new HtmlString(JsonConvert.SerializeObject(highlightColors2));

                result.Parameters.Colors3 = new HtmlString(JsonConvert.SerializeObject(colors3));
                result.Parameters.HighlightColors3 = new HtmlString(JsonConvert.SerializeObject(highlightColors3));
            }

        }

        private void FillColors(DrivingScoreFleetEvolutionReportBase result)
        {
            IList<string> colors = new List<string>();
            IList<string> highlightColors = new List<string>();
            CustomerThresholdsBusiness objsholdsBusi = new CustomerThresholdsBusiness();

            var ThesholdsBusi = objsholdsBusi.RetrieveThresholds().FirstOrDefault();
            foreach (DrivingScoreFleetEvolution item in result.List)
            {
                // if (item.Score <= 69)
                if (item.AverageScore <= ThesholdsBusi.RedHigher && item.AverageScore >= ThesholdsBusi.RedLower)
                {
                    colors.Add("rgba(217, 83, 79,1)"); //red
                    highlightColors.Add("rgba(217, 83, 79,0.9)"); //red
                }
                else
                    //  if (item.Score >= 70 && item.Score <= 89)
                    if (item.AverageScore <= ThesholdsBusi.YellowHigher && item.AverageScore >= ThesholdsBusi.YellowLower)
                    {
                        colors.Add("rgba(240, 173, 78,1)"); //yellow
                        highlightColors.Add("rgba(240, 173, 78,0.9)"); //yellow
                    }
                    else
                        // if (item.Score > 89)
                        if (item.AverageScore <= ThesholdsBusi.GreenHigher && item.AverageScore >= ThesholdsBusi.GreenLower)
                        {
                            colors.Add("rgba(92, 184, 92,1)"); //green
                            highlightColors.Add("rgba(92, 184, 92,0.9)"); //green
                        }
            }
            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
            result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(highlightColors));
        }

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="result"></param>
        private void GetReportData(OperationReportsBase parameters, DrivingScoreFleetEvolutionReportBase result)
        {

            GetScoreDrivingFleetEvolution(parameters, result);


        }
       
        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetScoreDrivingFleetEvolution(OperationReportsBase parameters, DrivingScoreFleetEvolutionReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<DrivingScoreFleetEvolution>("[Insurance].[Sp_GetScoreDrivingFleetEvolution]",
                    new
                    {
                        PartnerId = Session.GetPartnerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        parameters.ReportType,
                        MonthsQuantity=3,
                        parameters.CustomerId
                    });
            }
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;

            List<string> titles =new List<string>();
            titles.Add(Miscellaneous.GetMonthName(GetPreviousDate(parameters.Month - 2, parameters.Year)[0]) + "/" + GetPreviousDate(parameters.Month - 2, parameters.Year)[1]);
            titles.Add(Miscellaneous.GetMonthName(GetPreviousDate(parameters.Month - 1, parameters.Year)[0]) + "/" + GetPreviousDate(parameters.Month - 1, parameters.Year)[1]);
            titles.Add(Miscellaneous.GetMonthName(GetPreviousDate(parameters.Month, parameters.Year)[0]) + "/" +  parameters.Year);
            
            result.Titles = titles;
            result.ColumnCount = result.List.Select(x => TripleDesEncryption.Decrypt(x.EncryptedName)).ToList().Distinct().Count();
            result.Parameters.Titles = new HtmlString(JsonConvert.SerializeObject(titles));

            //Seleccionamos los labels y sacamos una sumatoria de cada modelo segun su average filtrado por label, para ordenarlo de menor a mayor
            Dictionary<string, double> namesWithSum = new Dictionary<string, double>();
            foreach (var item in result.List.Select(x => x.DecryptedName).Distinct())
            {
                namesWithSum.Add(item, result.List.Where(w => w.DecryptedName == item).Sum(s => s.AverageScore));
            }

            //Ordenamiento de los nombres conforme de menor a mayor porcentaje en total de cada Cliente
            var nameOrdered = namesWithSum.OrderBy(o => o.Value).Select(s => s.Key).ToList();

            //Listas para ordenar los modelos conforme al orden de los nombres de namedOrdered
            var data3List = new List<DrivingScoreFleetEvolution>(); 
            var data2List = new List<DrivingScoreFleetEvolution>();
            var data1List = new List<DrivingScoreFleetEvolution>();

            foreach (var itemName in nameOrdered)
            {
                foreach (var itemModel3 in result.List.Where(a=>a.Year==parameters.Year && a.Month==parameters.Month && a.DecryptedName == itemName))
                {
                    data3List.Add(itemModel3);
                }

                foreach (var itemModel2 in result.List.Where(a => a.Year == GetPreviousDate(parameters.Month - 1, parameters.Year)[1] && a.Month == GetPreviousDate(parameters.Month - 1, parameters.Year)[0] && a.DecryptedName == itemName))
                {
                    data2List.Add(itemModel2);
                }

                foreach (var itemModel1 in result.List.Where(a => a.Year == GetPreviousDate(parameters.Month - 2, parameters.Year)[1] && a.Month == GetPreviousDate(parameters.Month - 2, parameters.Year)[0] && a.DecryptedName == itemName))
                {
                    data1List.Add(itemModel1);
                }
            }

            //result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.DecryptedName).ToList().Distinct()));
            /*result.Parameters.Data3 = new HtmlString(JsonConvert.SerializeObject(result.List.Where(a=>a.Year==parameters.Year && a.Month==parameters.Month).Select(x => Math.Round(x.AverageScore, 0)).ToList()));
            result.Parameters.Data2 = new HtmlString(JsonConvert.SerializeObject(result.List.Where(a => a.Year == GetPreviousDate(parameters.Month - 1, parameters.Year)[1] && a.Month == GetPreviousDate(parameters.Month - 1, parameters.Year)[0]).Select(x => Math.Round(x.AverageScore, 0)).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Where(a => a.Year == GetPreviousDate(parameters.Month - 2, parameters.Year)[1] && a.Month == GetPreviousDate(parameters.Month - 2, parameters.Year)[0]).Select(x => Math.Round(x.AverageScore, 0)).ToList()));*/

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(nameOrdered));
            result.Parameters.Data3 = new HtmlString(JsonConvert.SerializeObject(data3List.Select(x => Math.Round(x.AverageScore, 0)).ToList()));
            result.Parameters.Data2 = new HtmlString(JsonConvert.SerializeObject(data2List.Select(x => Math.Round(x.AverageScore, 0)).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(data1List.Select(x => Math.Round(x.AverageScore, 0)).ToList()));
            FillColorsByMonth(result);
        }

        private List<int?> GetPreviousDate(int? month,int? year) {
            List<int?> date = new List<int?>();

            if (month < 1)
            {
                date.Add(12+month);
                date.Add(year - 1);
                return date;
            }
            date.Add(month);
            date.Add(year);
            return date;
        }
       
        /// <summary>
        /// Generate ScoreGlobal Repor To Excel
        /// </summary>
        /// <returns>A model of DrivingScoreFleetEvolutionReportBase in order to load the chart</returns>
        public byte[] GenerateDrivingScoreFleetEvolutionReportExcel(OperationReportsBase parameters)
        {
            var result = new DrivingScoreFleetEvolutionReportBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Tendencia de Habitos de Conducción.", result.List.ToList());
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}