﻿/************************************************************************************************************
*  File    : ScoreGlobalReportBusiness.cs
*  Summary : ScoreGlobalReport Business Methods
*  Author  : Alexander AGuero
*  Date    : 26/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Business.Core;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Insurance.Models.Insurance;
using Newtonsoft.Json;

//using CurrentFuelsByVehicle = ECOsystem.Models.Operation.CurrentFuelsByVehicle;
//using CurrentFuelsByVehicleGroupReport = ECOsystem.Models.Operation.CurrentFuelsByVehicleGroupReport;
//using CurrentFuelsByVehicleSubUnitReport = ECOsystem.Models.Operation.CurrentFuelsByVehicleSubUnitReport;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// ScoreGlobalReport Class
    /// </summary>
    public class DrivingScoreFleetReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Current Fuels Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of DrivingScoreFleetReportBase in order to load the chart</returns>

        public DrivingScoreFleetReportBase RetrieveScoreGlobalSumary(OperationReportsBase parameters)
        {
            var result = new DrivingScoreFleetReportBase();
            if (string.IsNullOrEmpty(parameters.ReportType))
                parameters.ReportType = "S"; //S:Summarized, D:Detailed
            GetDrivingScoreFleet(parameters, result);
                FillColors(result);
                result.List = result.List.ToList(); 
            
            return result; 
            

        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DrivingScoreFleetReportBase RetrieveDrivingScoreFleetReport(OperationReportsBase parameters)
        {
            var result = new DrivingScoreFleetReportBase();
            if (string.IsNullOrEmpty(parameters.ReportType))
            {
             //   result.Parameters.Data = new HtmlString("[]") ;
              //  result.Parameters.Labels = new HtmlString("[]");
                parameters.ReportType = "S"; //S:Summarized, D:Detailed
                GetReportData(parameters, result);
            
            }
            else
                GetReportData(parameters, result);
          
           // FillColors(result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            
            return result;
        }

        /// <summary>
        /// Fill colors to used in chart,using the list of customers related to the partner(At the moment only take one customer)
        /// </summary>
         /// <param name="result"></param>
        private void FillColorsByCustomers(DrivingScoreFleetReportBase result)
        {
            IList<string> colors = new List<string>();
            IList<string> highlightColors = new List<string>();
            using(var objsholdsBusi=new CustomerThresholdsBusiness())
            {
             int? customerId= Session.GetCustomersIdByPartner().FirstOrDefault();

             var ThesholdsBusi = objsholdsBusi.RetrieveThresholdByCustomer(customerId).FirstOrDefault();
                 foreach (DrivingScoreFleet item in result.List.OrderBy(o => o.AverageScore))
                 {
                     // if (item.Score <= 69)
                     if (item.AverageScore <= ThesholdsBusi.RedHigher && item.AverageScore >= ThesholdsBusi.RedLower)
                     {
                         colors.Add("rgba(217, 83, 79,1)"); //red
                         highlightColors.Add("rgba(217, 83, 79,0.9)"); //red
                     }
                     else
                         //  if (item.Score >= 70 && item.Score <= 89)
                         if (item.AverageScore <= ThesholdsBusi.YellowHigher && item.AverageScore >= ThesholdsBusi.YellowLower)
                         {
                             colors.Add("rgba(240, 173, 78,1)"); //yellow
                             highlightColors.Add("rgba(240, 173, 78,0.9)"); //yellow
                         }
                         else
                             // if (item.Score > 89)
                             if (item.AverageScore <= ThesholdsBusi.GreenHigher && item.AverageScore >= ThesholdsBusi.GreenLower)
                             {
                                 colors.Add("rgba(92, 184, 92,1)"); //green
                                 highlightColors.Add("rgba(92, 184, 92,0.9)"); //green
                             }
                 }
                 result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
                 result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(highlightColors));
             }
           
        }


        private void FillColors(DrivingScoreFleetReportBase result)
        {
            IList<string> colors = new List<string>();
            IList<string> highlightColors = new List<string>();
            CustomerThresholdsBusiness objsholdsBusi = new CustomerThresholdsBusiness();
            
            var ThesholdsBusi = objsholdsBusi.RetrieveThresholds().FirstOrDefault();
            foreach (DrivingScoreFleet item in result.List)
            {
               // if (item.Score <= 69)
                if (item.AverageScore <= ThesholdsBusi.RedHigher && item.AverageScore >= ThesholdsBusi.RedLower)
                {
                    colors.Add("rgba(217, 83, 79,1)"); //red
                    highlightColors.Add("rgba(217, 83, 79,0.9)"); //red
                }
                else
                  //  if (item.Score >= 70 && item.Score <= 89)
                    if (item.AverageScore <= ThesholdsBusi.YellowHigher && item.AverageScore >= ThesholdsBusi.YellowLower)    
                {
                        colors.Add("rgba(240, 173, 78,1)"); //yellow
                        highlightColors.Add("rgba(240, 173, 78,0.9)"); //yellow
                    }
                    else
                       // if (item.Score > 89)
                        if (item.AverageScore <= ThesholdsBusi.GreenHigher && item.AverageScore >= ThesholdsBusi.GreenLower)    
                        {
                            colors.Add("rgba(92, 184, 92,1)"); //green
                            highlightColors.Add("rgba(92, 184, 92,0.9)"); //green
                        }
            }
            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
            result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(highlightColors));
        }
        
        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="result"></param>
        private void GetReportData(OperationReportsBase parameters, DrivingScoreFleetReportBase result)
        {

            GetScoreDrivingFleet(parameters, result);
                 
            
        }
        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetDrivingScoreFleet(OperationReportsBase parameters, DrivingScoreFleetReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<DrivingScoreFleet>("[Operation].[Sp_GetGlobalScoreAllDriver]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        parameters.ReportType
                    });
            }

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => TripleDesEncryption.Decrypt(x.EncryptedName)).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.AverageScore,0)).ToList()));
                         
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetScoreDrivingFleet(OperationReportsBase parameters, DrivingScoreFleetReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<DrivingScoreFleet>("[Insurance].[Sp_GetScoreDrivingFleet]",
                    new
                    {
                        PartnerId = Session.GetPartnerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        parameters.ReportType,
                        parameters.CustomerId
                    });
            }

            var orderedList = result.List.OrderBy(o => o.AverageScore); 
            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(orderedList.Select(x => x.DecryptedName).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(orderedList.Select(x => Math.Round(x.AverageScore, 0)).ToList()));
            FillColorsByCustomers(result);

        }
       
        /// <summary>
        /// Generate ScoreGlobal Repor To Excel
        /// </summary>
        /// <returns>A model of DrivingScoreFleetReportBase in order to load the chart</returns>
        public byte[] GenerateDrivingScoreFleetReportExcel(OperationReportsBase parameters)
        {
            var result = new DrivingScoreFleetReportBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Calificación habitos de conducción.", result.List.ToList());
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}