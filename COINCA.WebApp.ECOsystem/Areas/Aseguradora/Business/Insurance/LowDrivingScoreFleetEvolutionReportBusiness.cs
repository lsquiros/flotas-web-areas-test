﻿/************************************************************************************************************
*  File    : Low DrivingScoreFleetEvolutionReportBusiness.cs
*  Summary : Low Driving Score Fleet EvolutionReport Business Methods
*  Author  : Andrés Oviedo
*  Date    : 13/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Business.Core;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Insurance.Models.Insurance;
using Newtonsoft.Json;
//using COINCA.Library.DataAccess;

//using CurrentFuelsByVehicle = ECOsystem.Models.Operation.CurrentFuelsByVehicle;
//using CurrentFuelsByVehicleGroupReport = ECOsystem.Models.Operation.CurrentFuelsByVehicleGroupReport;
//using CurrentFuelsByVehicleSubUnitReport = ECOsystem.Models.Operation.CurrentFuelsByVehicleSubUnitReport;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// Low Driving Score Fleet Evolution Report
    /// </summary>
    public class LowDrivingScoreFleetEvolutionReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Low Driving Score Fleet Evolution Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of DrivingScoreFleetEvolutionReportBase in order to load the chart</returns>
        public LowDrivingScoreFleetEvolutionReportBase RetrieveScoreGlobalSumary(OperationReportsBase parameters)
        {
            var result = new LowDrivingScoreFleetEvolutionReportBase();
            if (string.IsNullOrEmpty(parameters.ReportType))
                parameters.ReportType = "S"; //S:Summarized, D:Detailed
            GetLowScoreDrivingFleetEvolution(parameters, result);
            FillColors(result);
            result.List = result.List.ToList();

            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public LowDrivingScoreFleetEvolutionReportBase RetrieveLowDrivingScoreFleetEvolutionReport(OperationReportsBase parameters)
        {
            var result = new LowDrivingScoreFleetEvolutionReportBase();
            if (string.IsNullOrEmpty(parameters.ReportType))
            {
                //   result.Parameters.Data = new HtmlString("[]") ;
                //  result.Parameters.Labels = new HtmlString("[]");
                parameters.ReportType = "S"; //S:Summarized, D:Detailed
                GetReportData(parameters, result);

            }
            else
                GetReportData(parameters, result);

            // FillColors(result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }

        /// <summary>
        /// Fill colors to used in chart,using the list of customers related to the partner(At the moment only take one customer)
        /// </summary>
        /// <param name="result"></param>
        private void FillColorsByCustomers(LowDrivingScoreFleetEvolutionReportBase result)
        {
            IList<string> colors = new List<string>();
            IList<string> highlightColors = new List<string>();
            IList<string> colors2 = new List<string>();
            IList<string> highlightColors2 = new List<string>();
            IList<string> colors3 = new List<string>();
            IList<string> highlightColors3 = new List<string>();
            using (var objsholdsBusi = new CustomerThresholdsBusiness())
            {
                int? customerId = Session.GetCustomersIdByPartner().FirstOrDefault();

                var ThesholdsBusi = objsholdsBusi.RetrieveThresholdByCustomer(customerId).FirstOrDefault();

                foreach (LowScoreDrivingFleetEvolution item in result.List)
                {
                    // if (item.Score <= 69)
                    if (item.AverageScore <= ThesholdsBusi.RedHigher && item.AverageScore >= ThesholdsBusi.RedLower)
                    {
                        if (item.Month == GetPreviousDate(result.Parameters.Month - 2, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 2, result.Parameters.Year)[1])
                        {
                            colors.Add("rgba(129,91,58,1)"); // #815b3a
                            highlightColors.Add("rgba(129,91,58,0.9)"); // #815b3a
                        }
                        else if (item.Month == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[1])
                        {
                            colors2.Add("rgba(59, 127, 196,1)"); // #3b7fc4
                            highlightColors2.Add("rgba(59, 127, 196,0.9)"); // #3b7fc4
                        }
                        else if (item.Month == result.Parameters.Month && item.Year == result.Parameters.Year)
                        {
                            colors3.Add("rgba(241,92,117,1)"); // #f15c75
                            highlightColors3.Add("rgba(241,92,117,0.9)"); // #f15c75
                        }
                    }
                    else
                        //  if (item.Score >= 70 && item.Score <= 89)
                        if (item.AverageScore <= ThesholdsBusi.YellowHigher && item.AverageScore >= ThesholdsBusi.YellowLower)
                        {
                            if (item.Month == GetPreviousDate(result.Parameters.Month - 2, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 2, result.Parameters.Year)[1])
                            {
                                colors.Add("rgba(129,91,58,1)"); // #815b3a
                                highlightColors.Add("rgba(129,91,58,0.9)"); // #815b3a
                            }
                            else if (item.Month == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[1])
                            {
                                colors2.Add("rgba(59, 127, 196,1)"); // #3b7fc4
                                highlightColors2.Add("rgba(59, 127, 196,0.9)"); // #3b7fc4
                            }
                            else if (item.Month == result.Parameters.Month && item.Year == result.Parameters.Year)
                            {
                                colors3.Add("rgba(241,92,117,1)"); // #f15c75
                                highlightColors3.Add("rgba(241,92,117,0.9)"); // #f15c75
                            }
                        }
                        else
                            // if (item.Score > 89)
                            if (item.AverageScore <= ThesholdsBusi.GreenHigher && item.AverageScore >= ThesholdsBusi.GreenLower)
                            {
                                if (item.Month == GetPreviousDate(result.Parameters.Month - 2, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 2, result.Parameters.Year)[1])
                                {
                                    colors.Add("rgba(129,91,58,1)"); // #815b3a
                                    highlightColors.Add("rgba(129,91,58,0.9)"); // #815b3a
                                }
                                else if (item.Month == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[0] && item.Year == GetPreviousDate(result.Parameters.Month - 1, result.Parameters.Year)[1])
                                {
                                    colors2.Add("rgba(59, 127, 196,1)"); // #3b7fc4
                                    highlightColors2.Add("rgba(59, 127, 196,0.9)"); // #3b7fc4
                                }
                                else if (item.Month == result.Parameters.Month && item.Year == result.Parameters.Year)
                                {
                                    colors3.Add("rgba(241,92,117,1)"); // #f15c75
                                    highlightColors3.Add("rgba(241,92,117,0.9)"); // #f15c75
                                }
                            }
                }
                result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
                result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(highlightColors));

                result.Parameters.Colors2 = new HtmlString(JsonConvert.SerializeObject(colors2));
                result.Parameters.HighlightColors2 = new HtmlString(JsonConvert.SerializeObject(highlightColors2));

                result.Parameters.Colors3 = new HtmlString(JsonConvert.SerializeObject(colors3));
                result.Parameters.HighlightColors3 = new HtmlString(JsonConvert.SerializeObject(highlightColors3));
            }

        }


        private void FillColors(LowDrivingScoreFleetEvolutionReportBase result)
        {
            IList<string> colors = new List<string>();
            IList<string> highlightColors = new List<string>();
            CustomerThresholdsBusiness objsholdsBusi = new CustomerThresholdsBusiness();

            var ThesholdsBusi = objsholdsBusi.RetrieveThresholds().FirstOrDefault();
            foreach (LowScoreDrivingFleetEvolution item in result.List)
            {
                // if (item.Score <= 69)
                if (item.AverageScore <= ThesholdsBusi.RedHigher && item.AverageScore >= ThesholdsBusi.RedLower)
                {
                    colors.Add("rgba(217, 83, 79,1)"); //red
                    highlightColors.Add("rgba(217, 83, 79,0.9)"); //red
                }
                else
                    //  if (item.Score >= 70 && item.Score <= 89)
                    if (item.AverageScore <= ThesholdsBusi.YellowHigher && item.AverageScore >= ThesholdsBusi.YellowLower)
                    {
                        colors.Add("rgba(240, 173, 78,1)"); //yellow
                        highlightColors.Add("rgba(240, 173, 78,0.9)"); //yellow
                    }
                    else
                        // if (item.Score > 89)
                        if (item.AverageScore <= ThesholdsBusi.GreenHigher && item.AverageScore >= ThesholdsBusi.GreenLower)
                        {
                            colors.Add("rgba(92, 184, 92,1)"); //green
                            highlightColors.Add("rgba(92, 184, 92,0.9)"); //green
                        }
            }
            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
            result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(highlightColors));
        }

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="result"></param>
        private void GetReportData(OperationReportsBase parameters, LowDrivingScoreFleetEvolutionReportBase result)
        {

            GetLowScoreDrivingFleetEvolution(parameters, result);


        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetLowScoreDrivingFleetEvolution(OperationReportsBase parameters,LowDrivingScoreFleetEvolutionReportBase result)
        {

            int? lowScoreForValidate = 0;

            using (var dba = new DataBaseAccess())
            {
                using (var objsholdsBusi = new CustomerThresholdsBusiness())
                {
                    int? customerId = Session.GetCustomersIdByPartner().FirstOrDefault();

                    var ThesholdsBusi = objsholdsBusi.RetrieveThresholdByCustomer(customerId).FirstOrDefault();
                    //int? rh=ThesholdsBusi.RedHigher;
                    lowScoreForValidate = ThesholdsBusi.RedHigher;

                    result.List = dba.ExecuteReader<LowScoreDrivingFleetEvolution>("[Insurance].[Sp_GetLowScoreDrivingFleetEvolution]",
                        new
                        {
                            PartnerId = Session.GetPartnerId(),
                            parameters.Year,
                            parameters.Month,
                            parameters.StartDate,
                            parameters.EndDate,
                            parameters.ReportType,
                            MonthsQuantity = 3,
                            LowScore = ThesholdsBusi.RedHigher,
                            parameters.CustomerId
                        }).Select(x=>x).Where(x=>x.AverageScore>0);
                }
            }
        
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;

            List<string> titles = new List<string>();
            titles.Add(Miscellaneous.GetMonthName(GetPreviousDate(parameters.Month - 2, parameters.Year)[0]) + "/" + GetPreviousDate(parameters.Month - 2, parameters.Year)[1]);
            titles.Add(Miscellaneous.GetMonthName(GetPreviousDate(parameters.Month - 1, parameters.Year)[0]) + "/" + GetPreviousDate(parameters.Month - 1, parameters.Year)[1]);
            titles.Add(Miscellaneous.GetMonthName(GetPreviousDate(parameters.Month, parameters.Year)[0]) + "/" + parameters.Year);
            result.Titles = titles;
            result.ColumnCount = result.List.Select(x => TripleDesEncryption.Decrypt(x.EncryptedName)).ToList().Distinct().Count();
            result.Parameters.Titles = new HtmlString(JsonConvert.SerializeObject(titles));

            //Validamos que se obtengan solo los que el mes actual esten bajo el índice de conducción
            var listFilter = result.List.Where(a => a.Year == parameters.Year && a.Month == parameters.Month && a.AverageScore <= lowScoreForValidate).ToList();
            
            // Lista para almacenar los meses anteriores
            var listPreviousMonth = new List<LowScoreDrivingFleetEvolution>();

            //Extraemos los meses anteriores de los meses actuales segun la lista principal del Retrieve
            foreach (var item in listFilter)
            {
                var monthMinus1 = result.List.Where(a => a.Year == parameters.Year && a.Month == parameters.Month - 1 && a.CustomerId == item.CustomerId).FirstOrDefault();
                if(monthMinus1 != null)
                    listPreviousMonth.Add(monthMinus1);

                var monthMinus2 = result.List.Where(a => a.Year == parameters.Year && a.Month == parameters.Month - 2 && a.CustomerId == item.CustomerId).FirstOrDefault();
                if (monthMinus2 != null)
                    listPreviousMonth.Add(monthMinus2);
            }
            
            //Validamos si tenemos meses anteriores para combinarla con la lista de los meses actuales
            if (listPreviousMonth.Count > 0)
            {
                listFilter.AddRange(listPreviousMonth.ToList());
            }

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(listFilter.Select(x => x.DecryptedName).ToList().Distinct()));

            List<double> data3 = listFilter.Where(a => a.Year == parameters.Year && a.Month == parameters.Month).Select(x => Math.Round(x.AverageScore, 0)).ToList();
            if (data3.Count > 0) result.Parameters.Data3 = new HtmlString(JsonConvert.SerializeObject(data3));
            else result.Parameters.Data3 = new HtmlString(JsonConvert.SerializeObject("{[0]}"));

            List<double> data2 = listFilter.Where(a => a.Year == GetPreviousDate(parameters.Month - 1, parameters.Year)[1] && a.Month == GetPreviousDate(parameters.Month - 1, parameters.Year)[0]).Select(x => Math.Round(x.AverageScore, 0)).ToList();
            if (data2.Count > 0) result.Parameters.Data2 = new HtmlString(JsonConvert.SerializeObject(data2));
            else result.Parameters.Data2 = new HtmlString(JsonConvert.SerializeObject("{[0]}"));

            List<double> data = listFilter.Where(a => a.Year == GetPreviousDate(parameters.Month - 2, parameters.Year)[1] && a.Month == GetPreviousDate(parameters.Month - 2, parameters.Year)[0]).Select(x => Math.Round(x.AverageScore, 0)).ToList();
            if (data.Count > 0) result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(data));
            else result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject("{[0]}"));

            result.List = listFilter;
            FillColorsByCustomers(result);
        }

        private List<int?> GetPreviousDate(int? month, int? year)
        {
            List<int?> date = new List<int?>();

            if (month < 1)
            {
                date.Add(12 + month);
                date.Add(year - 1);
                return date;
            }
            date.Add(month);
            date.Add(year);
            return date;
        }

        /// <summary>
        /// Generate ScoreGlobal Repor To Excel
        /// </summary>
        /// <returns>A model of DrivingScoreFleetEvolutionReportBase in order to load the chart</returns>
        public byte[] GenerateLowDrivingScoreFleetEvolutionReportExcel(OperationReportsBase parameters)
        {
            var result = new LowDrivingScoreFleetEvolutionReportBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Calificación Hábitos de Conducción Agregada", result.List.ToList());
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}