﻿/************************************************************************************************************
*  File    : WeightedIndexDrivingScoreFleetReportBusiness.cs
*  Summary : WeightedIndexDrivingScoreFleetReportBusiness Methods
*  Author  : Andrés Oviedo Brenes
*  Date    : 16/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Insurance.Models.Insurance;
using Newtonsoft.Json;
//using COINCA.Library.DataAccess;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// Weighted Index Driving Score Fleet Report Business Class
    /// </summary>
    public class WeightedIndexDrivingScoreFleetReportBusiness : IDisposable
    {
     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public WeightedIndexDrivingScoreFleetReportBase RetrieveWeightedIndexDrivingScoreFleetReport(OperationReportsBase parameters)
        {
            var result = new WeightedIndexDrivingScoreFleetReportBase();
            if (string.IsNullOrEmpty(parameters.ReportType))
            {
                parameters.ReportType = "S"; //S:Summarized, D:Detailed
                GetReportData(parameters, result);
            }
            else
                GetReportData(parameters, result);

            // FillColors(result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }
       
        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="result"></param>
        private void GetReportData(OperationReportsBase parameters, WeightedIndexDrivingScoreFleetReportBase result)
        {
            GetWeightedIndexDrivingScoreFleet(parameters, result);
        }
     
        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetWeightedIndexDrivingScoreFleet(OperationReportsBase parameters, WeightedIndexDrivingScoreFleetReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<WeightedIndexDrivingScoreFleet>("[Insurance].[Sp_GetWightedIndexScoreDrivingFleet]",
                    new
                    {
                        PartnerId = Session.GetPartnerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        parameters.ReportType,
                        parameters.CustomerId
                    });
            }

            Dictionary<string, double> dataList = new Dictionary<string, double>();
            foreach (var item in result.List)
            {
                var label = item.DecryptedName;
                var value = Math.Round(item.WightedIndex, 2);
                dataList.Add(label, value);
            }

            var dataOrderedList = from data in dataList orderby data.Value descending select data;

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(dataOrderedList.Select(s => s.Key).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(dataOrderedList.Select(s => s.Value).ToList()));
            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(Miscellaneous.GenerateColors().Take(result.List.Count())));
            result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(Miscellaneous.GenerateHighlighColors().Take(result.List.Count())));

        }

        /// <summary>
        /// Generate ScoreGlobal Repor To Excel
        /// </summary>
        /// <returns>A model of DrivingScoreFleetReportBase in order to load the chart</returns>
        public byte[] GenerateWeightedIndexDrivingScoreFleetExcel(OperationReportsBase parameters)
        {
            var result = new WeightedIndexDrivingScoreFleetReportBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Información de Acronimos", result.List.ToList());
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}