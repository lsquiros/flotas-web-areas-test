﻿/************************************************************************************************************
*  File    : ScoreGlobalReportBusiness.cs
*  Summary : ScoreGlobalReport Business Methods
*  Author  : Alexander AGuero
*  Date    : 26/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Business.Core;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Insurance.Models.Insurance;
using Newtonsoft.Json;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// ScoreGlobalReport Class
    /// </summary>
    public class ScoreGlobalReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Current Fuels Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of ScoreGlobalReportBase in order to load the chart</returns>
        public ScoreGlobalReportBase RetrieveScoreGlobalSumary(OperationReportsBase parameters)
        {
            var result = new ScoreGlobalReportBase();
            if (string.IsNullOrEmpty(parameters.ReportType))
                parameters.ReportType = "S"; //S:Summarized, D:Detailed
            GetScoreSpeedAllDriver(parameters, result);
            FillColors(result);
            result.List = result.List.ToList();

            return result;


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public ScoreGlobalReportBase RetrieveScoreGlobalReport(OperationReportsBase parameters)
        {
            var result = new ScoreGlobalReportBase();
            if (string.IsNullOrEmpty(parameters.ReportType))
            {
                parameters.ReportType = "S"; //S:Summarized, D:Detailed
                GetReportData(parameters, result);
            }
            else
                GetReportData(parameters, result);

            FillColors(result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>

        private void FillColors(ScoreGlobalReportBase result)
        {
            IList<string> colors = new List<string>();
            IList<string> highlightColors = new List<string>();
            CustomerThresholdsBusiness objsholdsBusi = new CustomerThresholdsBusiness();

            var ThesholdsBusi = objsholdsBusi.RetrieveThresholds().FirstOrDefault();
            result.Parameters.RedHigher = ThesholdsBusi.RedHigher;
            result.Parameters.RedLower = ThesholdsBusi.RedLower;
            result.Parameters.YellowLower = ThesholdsBusi.YellowLower;
            result.Parameters.YellowHigher = ThesholdsBusi.YellowHigher;
            result.Parameters.GreenLower = ThesholdsBusi.GreenLower;
            result.Parameters.GreenHigher = ThesholdsBusi.GreenHigher;

            foreach (ScoreGlobalReport item in result.List)
            {
                // if (item.Score <= 69)


                if (item.Score <= ThesholdsBusi.RedHigher && item.Score >= ThesholdsBusi.RedLower)
                {
                    colors.Add("rgba(217, 83, 79,1)"); //red
                    highlightColors.Add("rgba(217, 83, 79,1)"); //red
                }
                else
                    //  if (item.Score >= 70 && item.Score <= 89)
                    if (item.Score <= ThesholdsBusi.YellowHigher && item.Score >= ThesholdsBusi.YellowLower)
                    {
                        colors.Add("rgba(240, 173, 78,1)"); //yellow
                        highlightColors.Add("rgba(240, 173, 78,1)"); //yellow
                    }
                    else
                        if (item.Score <= ThesholdsBusi.GreenHigher && item.Score >= ThesholdsBusi.GreenLower)
                        {
                            colors.Add("rgba(92, 184, 92,1)"); //green
                            highlightColors.Add("rgba(92, 184, 92,1)"); //green
                        }
                        else
                            if (item.Score <= ThesholdsBusi.RedLower)
                            {
                                colors.Add("rgba(217, 83, 79,1)"); //red
                                highlightColors.Add("rgba(217, 83, 79,1)"); //red

                            }
                            else if (item.Score > ThesholdsBusi.GreenHigher)
                            {
                                colors.Add("rgba(92, 184, 92,1)"); //green
                                highlightColors.Add("rgba(92, 184, 92,1)"); //green

                            }

            }
            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
            result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(highlightColors));
        }

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="result"></param>
        private void GetReportData(OperationReportsBase parameters, ScoreGlobalReportBase result)
        {
            GetScoreSpeedAllDriver(parameters, result);
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetScoreSpeedAllDriver(OperationReportsBase parameters, ScoreGlobalReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                var customerId = Session.GetCustomerId();
                                 
                 result.List = dba.ExecuteReader<ScoreGlobalReport>("[Operation].[Sp_GetGlobalScoresByMonthOrDays]",
                new
                {
                    CustomerId = (customerId != null)? customerId : 0,
                    parameters.Year,
                    parameters.Month,
                    parameters.StartDate,
                    parameters.EndDate,
                    parameters.ReportType
                });
            }

            foreach (var item in result.List)
            {
                item.Score = Math.Round(item.Score, 0);
                item.ScoreSpeedAdmin = Math.Round(item.ScoreSpeedAdmin, 0);
                item.ScoreSpeedRoute = Math.Round(item.ScoreSpeedRoute, 0);
            }
            
            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => TripleDesEncryption.Decrypt(x.EncryptedIdentification)).ToList()));
                result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.Score, 0)).ToList()));
          
        }

        /// <summary>
        /// Get Get Score Speed By Vehicle Report
        /// </summary>
        /// <param name="result">Result model with parameters</param>
        public ScoreGlobalReportBase GetGlobalScoreSpeedByVehicle(ScoreGlobalReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<ScoreGlobalReport>("[Operation].[Sp_GetGlobalScoreByVehicle]",
                new
                {
                    CustomerId = Session.GetCustomerId(),
                    result.Parameters.Year,
                    result.Parameters.Month,
                    result.Parameters.StartDate,
                    result.Parameters.EndDate,
                    result.Parameters.ReportType,
                    result.Parameters.VehicleId
                });
            }

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => TripleDesEncryption.Decrypt(x.EncryptedIdentification)).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.Score, 0)).ToList()));

            FillColors(result);

            return result;
        }

        /// <summary>
        /// Generate ScoreGlobal Repor To Excel
        /// </summary>
        /// <returns>A model of ScoreGlobalReportBase in order to load the chart</returns>
        public byte[] GenerateScoreGlobalReportExcel(OperationReportsBase parameters)
        {
            var result = new ScoreGlobalReportBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Índice Ponderado de Hábitos de Conducción según Velocidad de Ley", result.List.ToList());
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}