﻿/************************************************************************************************************
*  File    : DriversBusiness.cs
*  Summary : Drivers Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using ECOsystem.DataAccess;
//using COINCA.Library.DataAccess;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// Drivers Business class performs all related to Drivers business logic
    /// </summary>
    public class DriversBusiness : IDisposable
    {        
        /// <summary>
        /// Retrieve Active Customers Count
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public int RetrieveRegisteredDriversCount(int? partnerId, int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Insurance].[Sp_RegisteredDriversCount_Retrieve]",
                    new
                    {
                        PartnerId = partnerId,
                        CustomerId = customerId

                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}