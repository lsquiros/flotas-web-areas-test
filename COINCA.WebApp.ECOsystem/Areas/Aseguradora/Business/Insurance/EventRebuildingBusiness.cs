﻿/************************************************************************************************************
*  File    : EventRebuildingBusiness.cs
*  Summary : Event Rebuilding Business Methods
*  Author  : Andrés Oviedo
*  Date    : 26/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECO_Insurance.Models.Insurance;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// Event Rebuilding Business
    /// </summary>
    public class EventRebuildingBusiness : IDisposable
    {
        /// <summary>
        /// Event Rebuilding Business
        /// </summary>
        /// <param name="parameters">parameters options</param>
        public EventRebuildingBase GetEventRebuildingReport(OperationReportsBase parameters)
        {          
            using (var dba = new DataBaseAccess())
            {
                EventRebuildingBase result = new EventRebuildingBase();
                if (parameters.CustomerId != null)
                {
                    result.List = dba.ExecuteReader<EventRebuilding>("[Insurance].[Sp_GetEventRebuildingReport]",
                    new
                        {
                            parameters.CustomerId,
                            parameters.Year,
                            parameters.Month,
                            parameters.StartDate,
                            parameters.EndDate,
                            parameters.ReportType

                        });
                }
                return result;
            }
        }


        /// <summary>
        /// Retrieve Intrack References of the vehicle
        /// </summary>
        /// <param name="pVeicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Reconstructing> RetrieveIntrackReference(int pVeicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Reconstructing>("[Efficiency].[Sp_IntrackReference_Retrieve]",
                    new
                    {
                        VeicleId = pVeicleId
                    });
            }
        }
               
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}