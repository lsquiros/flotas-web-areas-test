﻿/************************************************************************************************************
*  File    : DrivingScoreFleetDataSourceReportBusiness.cs
*  Summary : Driving Score Fleet Data Source Report Business Methods
*  Author  : Andrés Oviedo
*  Date    : 17/02/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Business.Core;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Insurance.Models.Insurance;
using Newtonsoft.Json;
//using COINCA.Library.DataAccess;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// Driving Score Fleet Data Source Report
    /// </summary>
    public class DrivingScoreFleetDataSourceReportBusiness : IDisposable
    {

        /// <summary>
        /// Retrieve Driving Score Fleet Data Source Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of DrivingScoreFleetDataSourceReportBase</returns>
        public DrivingScoreFleetDataSourceReportBase RetrieveDrivingScoreFleetDataSourceReport(OperationReportsBase parameters)
        {
            var result = new DrivingScoreFleetDataSourceReportBase();
            if (string.IsNullOrEmpty(parameters.ReportType))
            {
                parameters.ReportType = "S"; //S:Summarized, D:Detailed
                GetDrivingScoreFleetDataSourceReport(parameters, result);
            }
            else
                GetDrivingScoreFleetDataSourceReport(parameters, result);

            // FillColors(result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }

        /// <summary>
        /// Fill colors to used in chart,using the list of customers related to the partner(At the moment only take one customer)
        /// </summary>
        /// <param name="result"></param>
        private void FillColorsByCustomers(DrivingScoreFleetDataSourceReportBase result)
        {
            IList<string> colors = new List<string>();
            IList<string> highlightColors = new List<string>();

            using (var objsholdsBusi = new CustomerThresholdsBusiness())
            {
                int? customerId = Session.GetCustomersIdByPartner().FirstOrDefault();

                var ThesholdsBusi = objsholdsBusi.RetrieveThresholdByCustomer(customerId).FirstOrDefault();

                foreach (DrivingScoreFleetDataSource item in result.List)
                {
                    // if (item.Score <= 69)
                    if (item.AverageScore <= ThesholdsBusi.RedHigher && item.AverageScore >= ThesholdsBusi.RedLower)
                    {
                        colors.Add("rgba(217, 83, 79,1)"); //red
                        highlightColors.Add("rgba(217, 83, 79,0.9)"); //red
                    }
                    else
                        //  if (item.Score >= 70 && item.Score <= 89)
                        if (item.AverageScore <= ThesholdsBusi.YellowHigher && item.AverageScore >= ThesholdsBusi.YellowLower)
                        {
                            colors.Add("rgba(240, 173, 78,1)"); //yellow
                            highlightColors.Add("rgba(240, 173, 78,0.9)"); //yellow
                        }
                        else
                        {
                            // if (item.Score > 89)
                            if (item.AverageScore <= ThesholdsBusi.GreenHigher && item.AverageScore >= ThesholdsBusi.GreenLower)
                            {
                                colors.Add("rgba(92, 184, 92,1)"); //green
                                highlightColors.Add("rgba(92, 184, 92,0.9)"); //green
                            }
                            else {
                                colors.Add("rgba(217, 83, 79,1)"); //red
                                highlightColors.Add("rgba(217, 83, 79,0.9)"); //red
                            }
                        }
                }
                result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
                result.Parameters.HighlightColors = new HtmlString(JsonConvert.SerializeObject(highlightColors));
            }
        }
       
        /// <summary>
        /// Get Driving Score Fleet DataSource Report Data
        /// </summary>
        /// <param name="parameters">Operation Reports Base and Driving Score Fleet Evolution Reports Base</param>
        private void GetDrivingScoreFleetDataSourceReport(OperationReportsBase parameters, DrivingScoreFleetDataSourceReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<DrivingScoreFleetDataSource>("[Insurance].[Sp_GetDrivingScoreFleetDataSourceReport]",
                    new
                    {
                        PartnerId = Session.GetPartnerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        parameters.ReportType,
                        parameters.CustomerId,
                        parameters.ScoreOpc
                    });
            }

            foreach (var item in result.List)
            {
                item.AverageScore = Math.Round(item.AverageScore, 0);
            }

            List<DrivingScoreFleetDataSource> newList = new List<DrivingScoreFleetDataSource>();
            Dictionary<string, List<DrivingScoreFleetDataSource>> dataFiltered = new Dictionary<string, List<DrivingScoreFleetDataSource>>();
            foreach (var item in result.List)
            {
                if (!dataFiltered.ContainsKey(item.DecryptedName.Trim()))
                {
                    dataFiltered.Add(item.DecryptedName.Trim(), new List<DrivingScoreFleetDataSource>());
                    dataFiltered[item.DecryptedName.Trim()].Add(item);
                }
                else {
                    dataFiltered[item.DecryptedName.Trim()].Add(item);
                }
            }

            foreach (var itemDict in dataFiltered)
            {
                var sprouData = itemDict.Value.Where(w => w.ScoreType.Contains("SPROU")).FirstOrDefault();
                // SPROU / SPADM = 2 --> Siempre van a ver dos tipos, pero cuando se acumulan más para ambos tipos se filtra por OverSpeedAverage y AverageScore
                if (itemDict.Value.Count > 2)
                { 
                    sprouData = itemDict.Value.Where(w => w.ScoreType.Contains("SPROU") && w.OverSpeedAverage > 0).OrderByDescending(o => o.AverageScore).FirstOrDefault();
                }
                if (sprouData != null)
                    newList.Add(sprouData);


                var spadmData = itemDict.Value.Where(w => w.ScoreType.Contains("SPADM")).FirstOrDefault();
                // SPROU / SPADM = 2 --> Siempre van a ver dos tipos, pero cuando se acumulan más para ambos tipos se filtra por OverSpeedAverage y AverageScore
                if (itemDict.Value.Count > 2) 
                { 
                    spadmData = itemDict.Value.Where(w => w.ScoreType.Contains("SPADM") && w.OverSpeedAverage > 0).OrderByDescending(o => o.AverageScore).FirstOrDefault();
                }
                if (spadmData != null)
                    newList.Add(spadmData);
            }

            result.List = newList.OrderBy(o => o.AverageScore);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x =>  x.ScoreType+" - "+TripleDesEncryption.Decrypt(x.EncryptedName)).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.AverageScore).ToList()));
          
            FillColorsByCustomers(result);
        }


        /// <summary>
        /// Generate ScoreGlobal Repor To Excel
        /// </summary>
        /// <returns>A model of DrivingScoreFleetEvolutionReportBase in order to load the chart</returns>
        public byte[] GenerateDrivingScoreFleetDataSourceReportExcel(OperationReportsBase parameters)
        {
            var result = new DrivingScoreFleetDataSourceReportBase();

            GetDrivingScoreFleetDataSourceReport(parameters, result);

            using (var excel = new ExcelReport())
            {
                excel.HasCustomRows = true;
                excel.IndexHeaderCustomRow = 7;
                excel.IndexBodyCustomRow = 8;
                return excel.CreateSpreadsheetWorkbook("Hábitos de Conducción",result.List.OrderBy(c => c.AverageScore).ToList());
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}