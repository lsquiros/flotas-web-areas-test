﻿/************************************************************************************************************
*  File    : VehiclesBusiness.cs
*  Summary : Vehicles Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;

namespace ECO_Insurance.Business.Insurance
{
    /// <summary>
    /// Vehicles Class
    /// </summary>
    public class VehiclesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Active Customers Count
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public int RetrieveRegisteredVehiclesCount(int? partnerId, int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = Session.GetUserInfo();
                return dba.ExecuteScalar<int>("[Insurance].[Sp_RegisteredVehiclesCount_Retrieve]",
                new
                {
                    PartnerId = partnerId,
                    CustomerId = customerId,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}