﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Business.Core;
using ECOsystem.Utilities;
using ECO_Insurance.Business.Insurance;
using ECO_Insurance.Models.Insurance;

namespace ECO_Insurance.Business.Utilities
{
    /// <summary>
    /// General Collections Class Contains all methods for load Drop Down List
    /// </summary>
    public class GeneralCollections
    {
        /// <summary>
        /// Return a list of Countries for current user in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersByPartner
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new CustomersBusiness())
                {
                    var result = business.RetrieveCustomersCollectionByPartners(Session.GetPartnerId());
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CustomerId, r.DecryptedName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetReportCriteriaForTrendReports
        {
            get
            {
                return new SelectList(new Dictionary<int, string> {
                    {(int)ReportCriteria.Period,"Por Período"}
                }, "Key", "Value", (int)ReportCriteria.Period);
            }
        }

        /// <summary>
        /// Return a list of active Customers
        /// </summary>
        public static int GetActiveCustomersCount(int? partnerId, int? customerId, int? countryId)
        {
            using (var business = new CustomersBusiness())
            {
                return business.RetrieveActiveCustomersCount(partnerId, customerId, countryId);

            }
        }

        /// <summary>
        /// Return a list of not active Customers
        /// </summary>
        public static int GetNotActiveCustomersCount(int? partnerId, int? customerId, int? countryId)
        {
            using (var business = new CustomersBusiness())
            {
                return business.RetrieveNotActiveCustomersCount(partnerId, customerId, countryId);

            }
        }

        /// <summary>
        /// Return a list of Registered Vehicles
        /// </summary>
        public static int GetRegisteredVehiclesCount(int? partnerId, int? customerId)
        {
            using (var business = new VehiclesBusiness())
            {
                return business.RetrieveRegisteredVehiclesCount(partnerId, customerId);

            }
        }

        /// <summary>
        /// Return a list of Registered Vehicles Drivers
        /// </summary>
        public static int GetRegisteredDriversCount(int? partnerId, int? customerId)
        {
            using (var business = new DriversBusiness())
            {
                return business.RetrieveRegisteredDriversCount(partnerId, customerId);

            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetMonthNames
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a current Month Name
        /// </summary>
        public static SelectList GetCurrentMonthName
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.Where(w => w.Id == DateTime.Now.Month).ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year - 5; i < year + 9; i++)
                {
                    dictionary.Add(i, i);
                }
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Card Years in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCardYears
        {
            get
            { 
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year ; i < year + 15; i++)
                {   //ValidCreditCard for 15 years
                    dictionary.Add(i, i);
                }
                year = year + 1;
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterDetailTransacReport
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var year = DateTime.UtcNow.Year;
                dictionary.Add(1, "Centro de Costo");
                dictionary.Add(2, "Grupo de Vehículos");
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterSettingOdometers
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();                
                dictionary.Add(1, "Odómetros Erróneos");
                dictionary.Add(2, "Historial de Odómetros");
                return new SelectList(dictionary, "Key", "Value", 1);
            }
        }

        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterPartnerCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var year = DateTime.UtcNow.Year;
                dictionary.Add(1, "Socio");
                dictionary.Add(2, "Clientes");
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetReportCriteria
        {
            get
            {
                return new SelectList(new Dictionary<int, string> { 
                    {(int)ReportCriteria.Period,"Por Período"},
                    {(int)ReportCriteria.DateRange,"Rango de Fechas"}
                }, "Key", "Value", "");
            }
        }

        /// <summary>
        /// Return a list of currencies in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCurrencies
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new CurrenciesBusiness())
                {
                    var result = business.RetrieveCurrencies(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CurrencyId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Customers Units Of Capacity for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersUnitsOfCapacity
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("CUSTOMERS_CAPACITY_UNITS");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
    }
}