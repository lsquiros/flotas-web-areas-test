﻿using ECO_Partner.Models;
using ECOsystem.DataAccess;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Linq;
using System.Web;
using System.Runtime.InteropServices;
using ECOsystem.Utilities;

namespace ECO_Partner.Business
{
    public class CollectionZonesBusiness : IDisposable
    {
        public IEnumerable<TypeZone> RetrieveZones(TypeZone model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<TypeZone>("[General].[Sp_CollectionsZones_Retrieve]",
          new
          {
              PartnerId = Session.GetPartnerId()
          });
            }
        }
        public void AddOrEditZone(int? ZoneId, string Name, string Description)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_CollectionsZones_AddOrEdit]",
                    new
                    {
                        Name = Name,
                        Description = Description,
                        ZoneId = ZoneId,
                        UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId,
                        PartnerId = Session.GetPartnerId()
                    });
            }
        }
        public void DeleteZone(int Id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_CollectionZone_Delete]",
                    new
                    {
                        ZoneId = Id,
                    });
            }
        }
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }


    }
}