﻿using ECO_Partner.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Business
{
    public class AvailableBudgetReportBusiness : IDisposable
    {
        public IEnumerable<AvailableBudgetReport> GetReportData(int? CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<AvailableBudgetReport>("[General].[Sp_AvailableBudgetReport_Retrieve]", new 
                { 
                    CustomerId,
                    ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId
                });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}