﻿using ECO_Partner.Models;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ECO_Partner.Business
{
    public class BillingReportBusiness : IDisposable
    {
        public DataSet RetrieveReport(AdministrationReportsBase model)
        {
            var ds = new DataSet();
            var dates = HttpContext.Current.Session["RangoFechas"].ToString();
            var PartnerName = Session.GetPartnerInfo().Name;
            var list = GetPrincipalData(model);
            var DetailList = GettDetailsData(model);

            if (list.Count() != 0)
            {
                using (var dt = new DataTable("BillingReport"))
                {
                    dt.Columns.Add("OrderId");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("PlateId");
                    dt.Columns.Add("CloseOrderDate");
                    dt.Columns.Add("Dates");
                    dt.Columns.Add("PartnerName");

                    foreach (var item in list)
                    {
                        var row = dt.NewRow();
                        row["OrderId"] = item.OrderId;
                        row["CustomerName"] = item.DecryptedCustomerName;
                        row["PlateId"] = item.PlateId;
                        row["CloseOrderDate"] = Miscellaneous.GetDateFormat(item.CloseOrderDate);
                        row["Dates"] = dates;
                        row["PartnerName"] = PartnerName;
                        dt.Rows.Add(row);
                    }

                    ds.Tables.Add(dt);
                }

                using (var dt = new DataTable("BillingDetail"))
                {
                    dt.Columns.Add("Title");
                    dt.Columns.Add("CatalogId");

                    foreach (var item in DetailList.Select(x => new { x.Description, x.CatalogId }).Distinct())
                    {
                        var row = dt.NewRow();
                        row["Title"] = item.Description;
                        row["CatalogId"] = item.CatalogId;
                        dt.Rows.Add(row);
                    }
                    ds.Tables.Add(dt);
                }

                using (var dt = new DataTable("BillingColumnValues"))
                {
                    dt.Columns.Add("OrderId");
                    dt.Columns.Add("CostAmount");
                    dt.Columns.Add("CatalogId");

                    var obj = DetailList.Select(x => new { x.Description, x.CatalogId }).Distinct();
                    var orderId = DetailList.OrderBy(x => x.OrderId).FirstOrDefault().OrderId;

                    foreach (var item in DetailList.OrderBy(x => x.OrderId).ToList())
                    {
                        if (orderId != item.OrderId)
                        {
                            foreach (var objBlank in obj.Where(y => !DetailList.Where(x => x.OrderId == orderId).Select(a => a.CatalogId).ToList().Contains(y.CatalogId)))
                            {
                                var row = dt.NewRow();
                                row["OrderId"] = orderId;
                                row["CostAmount"] = 0;
                                row["CatalogId"] = objBlank.CatalogId;
                                dt.Rows.Add(row);
                            }

                            var row1 = dt.NewRow();
                            row1["OrderId"] = item.OrderId;
                            row1["CostAmount"] = item.CostAmount;
                            row1["CatalogId"] = item.CatalogId;
                            dt.Rows.Add(row1);
                            orderId = item.OrderId;
                        }
                        else
                        {
                            var row = dt.NewRow();
                            row["OrderId"] = item.OrderId;
                            row["CostAmount"] = item.CostAmount;
                            row["CatalogId"] = item.CatalogId;
                            dt.Rows.Add(row);
                        }
                    }
                    foreach (var objBlank in obj.Where(y => !DetailList.Where(x => x.OrderId == orderId).Select(a => a.CatalogId).ToList().Contains(y.CatalogId)))
                    {
                        var row = dt.NewRow();
                        row["OrderId"] = orderId;
                        row["CostAmount"] = 0;
                        row["CatalogId"] = objBlank.CatalogId;
                        dt.Rows.Add(row);
                    }
                    ds.Tables.Add(dt);
                }

                return ds;
            }
            else
            {
                return null;
            }

            
        }

        List<BillingReport> GetPrincipalData(AdministrationReportsBase model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<BillingReport>("[General].[SP_BillingReport_Retrieve]",
                    new
                    {
                        ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId,
                        model.StartDate,
                        model.EndDate,
                        model.Year,
                        model.Month
                    }).ToList();
            }
        }

        List<BillingReportDetails> GettDetailsData(AdministrationReportsBase model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<BillingReportDetails>("[General].[SP_BillingReportDetails_Retrieve]",
                    new
                    {
                        ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId,
                        model.StartDate,
                        model.EndDate,
                        model.Year,
                        model.Month
                    }).ToList();
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}