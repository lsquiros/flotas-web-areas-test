﻿using ECO_Partner.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ECO_Partner.Business
{
    public class AmountByServiceStationsReportBusiness : IDisposable
    {

        private IEnumerable<AmountByServiceStationsReport> RetrieveReportData(string EndDate, bool HasTerminals)
        {
            using (var dba = new DataBaseAccess())
            {
                var endDate = Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(EndDate)).AddHours(23).AddMinutes(59).AddSeconds(59);
                return dba.ExecuteReader<AmountByServiceStationsReport>("[General].[Sp_AmountByServiceStationsReport_Retrieve]",
                    new
                    {
                        EndDate = endDate,
                        HasTerminals,
                        ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId
                    });
            }
        }

        public DataTable GetReportDataTable(string EndDate, bool HasTerminals)
        {
            var model = RetrieveReportData(EndDate, HasTerminals);
            var monthname = Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(EndDate)).ToString("MMMM, yyyy", new CultureInfo("es-ES")).ToUpper();
            var Partner = ECOsystem.Utilities.Session.GetPartnerInfo().Name;
            using (var dt = new DataTable())
            {
                dt.Columns.Add("BacId");
                dt.Columns.Add("ServiceStationName");
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("CountryName");
                dt.Columns.Add("MonthlyAmount",typeof(decimal));
                dt.Columns.Add("MonthName");
                dt.Columns.Add("TopMonetaryLimit",typeof(decimal));
                dt.Columns.Add("AvailableAmount",typeof(decimal));
                dt.Columns.Add("Partner");

                foreach (var item in model)
                {
                    DataRow row = dt.NewRow();

                    row["BacId"] = item.BacId;
                    row["ServiceStationName"] = item.ServiceStationName;
                    row["TerminalId"] = item.TerminalId;
                    row["CountryName"] = item.CountryName;
                    row["MonthlyAmount"] = item.MonthlyAmount;
                    row["MonthName"] = monthname;
                    row["TopMonetaryLimit"] = item.TopMonetaryLimit;
                    row["AvailableAmount"] = item.AvailableAmount;
                    row["Partner"] = Partner;

                    dt.Rows.Add(row);
                }
                return dt;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}