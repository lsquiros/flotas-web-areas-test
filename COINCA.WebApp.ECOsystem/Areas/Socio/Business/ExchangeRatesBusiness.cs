﻿using ECO_Partner.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Business
{
    public class ExchangeRatesBusiness : IDisposable
    {
        public ExchangeRates ExchangeRatesRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ExchangeRates>("[General].[Sp_ExchangeRates_Retrieve]",
                    new
                    {                       
                        CountryName = ECOsystem.Utilities.Session.GetPartnerInfo().CountryName
                    }).FirstOrDefault();
            }
        }

        public void ExchangeRatesAddOrEdit(ExchangeRates model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_ExchangeRate_Add]",
                    new
                    {
                        CountryName = ECOsystem.Utilities.Session.GetPartnerInfo().CountryName,
                        Value = model.NewAmount
                    });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}