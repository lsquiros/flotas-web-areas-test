﻿using ECOsystem.DataAccess;
using ECOsystem.Models;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Business
{
    public class PartnerNotificationsBusiness : IDisposable
    {
        public List<PartnerNotifications> PartnerNotificationsRetrieve(int? Id, string Key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                 return dba.ExecuteReader<PartnerNotifications>("[General].[Sp_PartnerNotifications_Retrieve]",
                    new
                    {
                        Id,
                        Key,
                        PartnerId = ECOsystem.Utilities.Session.GetPartnerId()
                    }).ToList();
            }
        }

        public void PartnerNotificationsAddOrEdit(PartnerNotifications model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PartnerNotifications_AddOrEdit]",
                    new
                    {
                        model.Id,
                        model.Title,
                        model.Message,
                        model.Xml,
                        model.SendDate,
                        model.Active,
                        PartnerId = ECOsystem.Utilities.Session.GetPartnerId(),
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        public void PartnerNotificationsDelete(int Id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PartnerNotifications_Delete]",
                    new
                    {
                        Id,
                        PartnerId = ECOsystem.Utilities.Session.GetPartnerId(),
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        public PartnerNotifications PartnerNotificationsValidate(PartnerNotifications model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerNotifications>("[General].[Sp_PartnerNotifications_Validate]",
                   new
                   {
                       model.Xml,
                       PartnerId = ECOsystem.Utilities.Session.GetPartnerId()
                   }).FirstOrDefault();
            }
        }

        public List<Vehicles> VehiclesForNotificationsRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Vehicles>("[General].[Sp_VehiclesByPartner_Retrieve]",
                   new
                   {
                       PartnerId = ECOsystem.Utilities.Session.GetPartnerId()
                   }).ToList();
            }
        }

        public List<Users> DriversForNotificationsRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Users>("[General].[Sp_DriversByPartner_Retrieve]",
                   new
                   {
                       PartnerId = ECOsystem.Utilities.Session.GetPartnerId()
                   }).ToList();
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}