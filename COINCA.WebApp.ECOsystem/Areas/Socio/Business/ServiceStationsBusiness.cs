﻿using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using System.Web.Mvc;
using System.IO;
using ECO_Partner.Models;
using System.Data;
using System.Text;
using System.Xml.Serialization;
using ECOsystem.Models.Core;

namespace ECO_Partner.Business
{
    /// <summary>
    /// Service Station Business
    /// </summary>
    public class ServiceStationsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve the name of the Canton
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetCantonNameById(int id)
        {
            using (var business = new ECOsystem.Business.Core.GeographicDivisionBusiness())
            {
                var model = business.RetrieveCounties(id, null, null).FirstOrDefault();
                return model.Name;
            }
        }

        /// <summary>
        ///  Retrieve the name of the Province
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetProvinceNameById(int id)
        {
            using (var business = new ECOsystem.Business.Core.GeographicDivisionBusiness())
            {
                var model = business.RetrieveStates(id, null).FirstOrDefault();
                return model.Name;
            }
        }

        /// <summary>
        /// Retrieve the States based on the StatesId
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static IEnumerable<ECOsystem.Models.Core.States> GetStates(int? countryId)
        {
            return new ECOsystem.Business.Core.GeographicDivisionBusiness().RetrieveStates(null, countryId);
        }

        /// <summary>
        /// Retrieve the States based on the CountiesId
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static IEnumerable<ECOsystem.Models.Core.Counties> GetCounties(int? stateId, int? countryId)
        {
            return new ECOsystem.Business.Core.GeographicDivisionBusiness().RetrieveCounties(null, stateId, countryId);
        }

        /// <summary>
        /// Load Counties for the Edit Service Statios
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static SelectList GetCountiesEditServiceStatios(int? stateId, int? countryId)
        {
            var dictionary = new Dictionary<int?, string>();
            if (stateId == null) return new SelectList(dictionary, "Key", "Value");
            using (var business = new ECOsystem.Business.Core.GeographicDivisionBusiness())
            {
                var result = business.RetrieveCounties(null, stateId, countryId);
                foreach (var r in result)
                    dictionary.Add(r.CountyId, r.Name);
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        /// <summary>
        /// Add or edit a Service Station, only a model is sent
        /// </summary>
        /// <param name="model">ServiceStations</param>
        public void ServiceStationsAddOrEdit(ServiceStations model)
        {
            var users = Session.GetUserInfo();
            //var country = ECOsystem.Utilities.Session.GetUserInfo(); //Session.GetPartnerInfo();
            var customerId = Session.GetCustomerId();

            using (var dba = new DataBaseAccess())
            {
                model.ServiceStationId = dba.ExecuteScalar<int>("[General].[Sp_ServiceStations_AddOrEdit]",
                new
                {
                    model.ServiceStationId,
                    model.BacId,
                    model.Name,
                    model.Address,
                    model.CantonId,
                    model.ProvinceId,
                    model.Latitude,
                    model.Longitude,
                    users.UserId,
                    users.CountryId,
                    model.SAPProv,
                    model.Legal_Id,
                    model.Terminal,
                    model.Number_Provider,
                    model.TopMonetaryLimit,
                    CustomerId = customerId,
                    PartnerId = users.PartnerId,
                });

                dba.ExecuteNonQuery("[General].[Sp_TerminalsByServiceStations_Add]",
                new
                {
                    model.ServiceStationId,
                    Xml = ECOsystem.Utilities.Miscellaneous.GetXML<Terminals>(model.TerminalList),
                    UserId = users.UserId
                });
            }
        }

        /// <summary>
        /// Overloaded method for send Collection of service stations, to update in a massive way.
        /// </summary>
        /// <param name="model">IEnumerable<ServiceStations></param>
        public IEnumerable<ServiceStations> ServiceStationsAddOrEdit(IEnumerable<ServiceStations> model)
        {
            var users = Session.GetUserInfo();
            var customerId = Session.GetCustomerId();
            var Xml = Miscellaneous.GetXML(model.ToList());

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ServiceStations>("[General].[Sp_ByServiceStations_Import]",
                new
                {
                    users.CountryId,
                    users.UserId,
                    users.PartnerId,
                    Xml
                });
            }
        }

        /// <summary>
        /// Service Statios AddOrEdit
        /// </summary>
        /// <param name="model"></param>
        public string AddOrEditImportTerminalsServiceStation(ServiceStations model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_ServiceStationsTerminals_AddOrEdit]", new
                {
                    model.BacId,
                    model.Terminal,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        /// <summary>
        /// Retrieve Service Stations
        /// </summary>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static IEnumerable<ServiceStations> ServiceStationRetrieve(int? id, int? countryId, int? PartnerId, int? customerid, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ServiceStations>("[General].[Sp_ServiceStations_Retrieve]", new
                {
                    ServiceStationId = id,
                    CountryId = countryId,
                    PartnerId,
                    Key = key,
                    CustomerId = customerid
                });
            }
        }

        /// <summary>
        /// Delete Service Stations
        /// </summary>
        /// <param name="SSId"></param>
        public void DeleteServiceStations(int SSId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ServiceStations_Delete]",
                    new { ServiceStationId = SSId });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelList"></param>
        /// <returns></returns>
        public byte[] CreateTxtFormat(IEnumerable<ServiceStationErrors> modelList, int Error_Count, int Success_Count)
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    var sw = new StreamWriter(ms);
                    sw.WriteLine("Cantidad de registros agregados satisfactoriamente: " + Success_Count);
                    sw.WriteLine("Cantidad de registros fallidos: " + Error_Count);
                    sw.WriteLine("");
                    foreach (var item in modelList)
                    {
                        sw.WriteLine(item.Item);
                        sw.Flush();
                    }

                    return ms.ToArray();
                }
            }
            catch (Exception)
            {
                throw new SystemException("System Exception when server tries to generate excel file");
            }
        }

        public List<Terminals> GetTerminalsByServiceStation(int? ServiceStationId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Terminals>("[General].[Sp_TerminalByServiceStations_Retrieve]",
                new
                {
                    ServiceStationId
                }).ToList();
            }
        }

        public List<Terminals> GetTerminalsByServiceStationList(List<ServiceStations> list)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Terminals>("[General].[Sp_TerminalByServiceStationsList_Retrieve]",
                new
                {
                    XmlData = ECOsystem.Utilities.Miscellaneous.GetXML<string>(list.Select(s => s.ServiceStationId.ToString()).ToList())
                }).ToList();
            }
        }

        public bool ValidateTerminal(string TerminalId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<bool>("[General].[Sp_TerminalByServiceStations_Validate]",
                    new
                    {
                        TerminalId,
                        Session.GetUserInfo().PartnerId
                    });
            }
        }

        /// <summary>
        /// Service Stations Generate
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataSet ReportData(List<ServiceStations> list)
        {
            //var xml = GetXML<string>(list.Select(s => s.ServiceStationId.ToString()).ToList());
            List<Terminals> detailList = GetTerminalsByServiceStationList(list);

            DataSet ds = new DataSet();

            using (var dt = new DataTable("ServiceStationsReport"))
            {
                dt.Columns.Add("ServiceStationId");
                dt.Columns.Add("BacId");
                dt.Columns.Add("Name");
                dt.Columns.Add("Address");
                dt.Columns.Add("CantonString");
                dt.Columns.Add("ProvinceString");
                dt.Columns.Add("Legal_Id");
                dt.Columns.Add("CustomerName");
                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();
                    row["ServiceStationId"] = item.ServiceStationId;
                    row["BacId"] = item.BacId;
                    row["Name"] = item.Name;
                    row["Address"] = item.Address;
                    row["CantonString"] = item.CantonString;
                    row["ProvinceString"] = item.ProvinceString;
                    row["Legal_Id"] = item.Legal_Id;
                    //row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    dt.Rows.Add(row);
                }
                ds.Tables.Add(dt);
            }

            using (var dt = new DataTable("ServiceStationsDetail"))
            {
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("ServiceStationId");
                foreach (var item in detailList)
                {
                    DataRow row = dt.NewRow();
                    row["TerminalId"] = item.TerminalId;
                    row["ServiceStationId"] = item.ServiceStationId;
                    dt.Rows.Add(row);
                }
                ds.Tables.Add(dt);
            }

            return ds;
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
