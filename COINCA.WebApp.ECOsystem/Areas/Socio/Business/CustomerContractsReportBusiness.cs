﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ECO_Partner.Models;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;

namespace ECO_Partner.Business
{
    public class CustomerContractsReportBusiness : IDisposable
    {
        public DataTable CustomerContractsReportDataTable(CustomerContractsReportBase Parameters)
        {
            return DataTableUtilities.ClassToDataTable(CustomerContractsRetrieve(Parameters), "CustomerContractsReport");
        }

        public List<CustomerContractsReport> CustomerContractsRetrieve(CustomerContractsReportBase Parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader <CustomerContractsReport>("[General].[Sp_CustomersContractsReport_Retrieve]", 
                new
                {
                    Parameters.Year
                   ,Parameters.Month
                   ,Parameters.StartDate
                   ,Parameters.EndDate
                   ,Parameters.Classification
                   ,PartnerId = Session.GetPartnerInfo().PartnerId
                }).ToList();
            }
        }

        /// <summary>
        /// GC
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    
}