﻿/************************************************************************************************************
*  File    : TransactionsReportBusiness.cs
*  Summary : TransactionsReport Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 12/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Models.Administration;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using ECO_Partner.Models;
using System.Data;
using ECO_Partner.Utilities;
using System.Web;

namespace ECO_Partner.Business
{
    /// <summary>
    /// TransactionsReport Class
    /// </summary>
    public class TransactionsReportBusiness : IDisposable
    {


        public TransactionsReportBase RetrieveTransactionsReport(ControlFuelsReportsBase parameters)
        {
            var result = new TransactionsReportBase();
            GetReportData(parameters, result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.TransactionType = parameters.TransactionType;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            return result;
        }

        /// <summary>
        /// Retrieve Transactions Report by Partner
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of TransactionsReportBase in order to load the chart</returns>
        public TransactionsReportDeniedByPartnerBase RetrieveTransactionsReportDeniedByPartner(ControlFuelsReportsBase parameters)
        {            
            var result = new TransactionsReportDeniedByPartnerBase();
            GetReportDeniedByPartnerData(parameters, result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.TransactionType = parameters.TransactionType;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDeniedByPartnerData(ControlFuelsReportsBase parameters, TransactionsReportDeniedByPartnerBase result)
        {
            GetReportDeniedByPartnerDataTransactions(parameters, result);
        }

        private void GetReportData(ControlFuelsReportsBase parameters, TransactionsReportBase result)
        {
            GetReportDataTransactions(parameters, result);
        }


        private void GetReportDataTransactions(ControlFuelsReportsBase parameters, TransactionsReportBase result)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                int cusId = 0;
                if (parameters.CustomerId == 0)
                    cusId = Convert.ToInt32(Session.GetCustomerId());
                else
                    cusId = parameters.CustomerId;

                var user = ECOsystem.Utilities.Session.GetUserInfo();

                if (parameters.TransactionType == 6)
                {
                    result.List = dba.ExecuteReader<TransactionsReport>("[Control].[Sp_TransactionsReportDenied_Retrieve]",
                        new
                        {
                            CustomerId = cusId,
                            Year = parameters.Year,
                            Month = parameters.Month,
                            StartDate = parameters.StartDate,
                            EndDate = parameters.EndDate,
                            user.UserId
                        });


                    foreach (var item in result.List)
                    {                        
                        var ccObj = GetCreditCard(item.CreditCardNumber);

                        item.CreditCardHolder = (ccObj != null) ? ccObj.CreditCardHolder : "Ninguno";
                        item.CreditCardNumber = Miscellaneous.GetDisplayCreditCardMask(item.CreditCardNumberDescrypt);                        
                    }

                }
                else
                {

                    result.List = dba.ExecuteReader<TransactionsReport>("[Control].[Sp_TransactionsReport_Retrieve]",
                    new
                    {
                        CustomerId = cusId,
                        Status = parameters.TransactionType,
                        Key = parameters.key,
                        Year = parameters.Year,
                        Month = parameters.Month,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate,
                        user.UserId
                    });
                }
            }
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }


        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDeniedByPartnerDataTransactions(ControlFuelsReportsBase parameters, TransactionsReportDeniedByPartnerBase result)
        {
            var DeniedList = new List<TransactionsReportDeniedByPartner>();
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var user = Session.GetUserInfo();

                if (parameters.TransactionType == 8) //Transactions with no service stations
                {
                    result.List = dba.ExecuteReader<TransactionsReportDeniedByPartner>("[Control].[Sp_TransactionsReportNoServiceStationsByPartner_Retrieve]",
                    new
                    {
                        PartnerId = Session.GetPartnerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        parameters.CustomerId,
                        user.UserId
                    });
                    foreach (var item in result.List)
                    {
                        item.Invoice = (item.Invoice != null) ? ((item.Invoice == "NULL") ? "-" : item.Invoice) : "-";
                        item.MerchantDescription = (item.MerchantDescription != null) ? ((item.MerchantDescription == "NULL") ? "-" : item.MerchantDescription) : "-";
                        item.CreditCardNumber = Miscellaneous.GetDisplayCreditCardMask(TripleDesEncryption.Decrypt(item.CreditCardNumber));
                    }
                }
                else
                {
                    if (parameters.CustomerId == -1)
                    {
                        switch (parameters.TransactionType)
                        {
                            case 1:
                            case 2:
                            case 3:
                            case 7:
                            case 9:
                                result.List = dba.ExecuteReader<TransactionsReportDeniedByPartner>("[Control].[Sp_TransactionsReportByPartner_Retrieve]",
                                new
                                {
                                    PartnerId = Session.GetPartnerId(),
                                    Status = parameters.TransactionType,
                                    Key = parameters.key,
                                    parameters.Year,
                                    parameters.Month,
                                    parameters.StartDate,
                                    parameters.EndDate,
                                    user.UserId
                                });
                                foreach (var item in result.List)
                                {
                                    item.Invoice = (item.Invoice != null) ? ((item.Invoice == "NULL") ? "-" : item.Invoice) : "-";
                                    item.MerchantDescription = (item.MerchantDescription != null) ? ((item.MerchantDescription == "NULL") ? "-" : item.MerchantDescription) : "-";
                                    item.CreditCardNumber = Miscellaneous.GetDisplayCreditCardMask(TripleDesEncryption.Decrypt(item.CreditCardNumber));
                                }
                                break;
                            case 6:
                                result.List = dba.ExecuteReader<TransactionsReportDeniedByPartner>("[Control].[Sp_TransactionsReportDeniedByPartner_Retrieve]",
                                new
                                {
                                    PartnerId = Session.GetPartnerId(),
                                    parameters.Year,
                                    parameters.Month,
                                    parameters.StartDate,
                                    parameters.EndDate,
                                    user.UserId
                                });
                                foreach (var item in result.List)
                                {
                                    
                                    var ccObj = GetCreditCard(item.CreditCardNumber);

                                    item.CreditCardHolder = (ccObj != null) ? ccObj.CreditCardHolder : "Ninguno";
                                    item.CreditCardNumber = Miscellaneous.GetDisplayCreditCardMask(item.CreditCardNumberDescrypt);
                                    item.VehicleName = (ccObj != null) ? ccObj.VehicleName : "";
                                    if (parameters.key != null)
                                    {
                                        if (item.CreditCardHolder.Contains(parameters.key) || item.VehicleName.Contains(parameters.key))
                                        {

                                            DeniedList.Add(item);
                                        }
                                    }                                   
                                }

                                if (parameters.key != null)
                                {
                                    result.List = DeniedList;
                                }
                                break;
                        }
                    }
                    else
                    {
                        switch (parameters.TransactionType)
                        {
                            case 1:
                            case 2:
                            case 3:
                            case 7:
                            case 9:
                                result.List = dba.ExecuteReader<TransactionsReportDeniedByPartner>("[Control].[Sp_TransactionsReport_Retrieve]",
                                new
                                {
                                    CustomerId = parameters.CustomerId == 0 ? Session.GetCustomerId() : parameters.CustomerId,
                                    Status = parameters.TransactionType,
                                    Key = parameters.key,
                                    parameters.Year,
                                    parameters.Month,
                                    parameters.StartDate,
                                    parameters.EndDate,
                                    user.UserId
                                });
                                foreach (var item in result.List)
                                    item.CreditCardNumber = Miscellaneous.GetDisplayCreditCardMask(TripleDesEncryption.Decrypt(item.CreditCardNumber));
                                break;
                            case 6:
                                result.List = dba.ExecuteReader<TransactionsReportDeniedByPartner>("[Control].[Sp_TransactionsReportDenied_Retrieve]",
                                new
                                {
                                    CustomerId = parameters.CustomerId == 0 ? Session.GetCustomerId() : parameters.CustomerId,
                                    parameters.Year,
                                    parameters.Month,
                                    parameters.StartDate,
                                    parameters.EndDate,
                                    user.UserId
                                });

                                foreach (var item in result.List)
                                {                                    
                                    var ccObj = GetCreditCard(item.CreditCardNumber);

                                    item.CreditCardHolder = (ccObj != null) ? ccObj.CreditCardHolder : "Ninguno";
                                    item.CreditCardNumber = Miscellaneous.GetDisplayCreditCardMask(item.CreditCardNumberDescrypt);
                                    item.VehicleName = (ccObj != null) ? ccObj.VehicleName : "";

                                    if (parameters.key != null)
                                    {
                                        if (item.CreditCardHolder.Contains(parameters.key) || item.VehicleName.Contains(parameters.key))
                                        {

                                            DeniedList.Add(item);
                                        }
                                    }                                    
                                }

                                if (parameters.key != null)
                                {
                                    result.List = DeniedList;
                                }
                                break;
                        }
                    }
                } 
                HttpContext.Current.Session["ReportData"] = result.List;
                result.List = result.List.OrderByDescending(x => x.Date);
            }
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }

        

        /// <summary>
        /// Generate Transaction Report Excel
        /// </summary>
        /// <returns>A model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public byte[] GenerateTransactionsReportExcel(ControlFuelsReportsBase parameters)
        {
            var result = new TransactionsReportBase();
            var resultPartner = new TransactionsReportDeniedByPartnerBase();

            if (parameters.CustomerId > 0)
            {                
               // GetReportData(parameters, result);
            }
            else
            {
                GetReportDeniedByPartnerDataTransactions(parameters, resultPartner);
            }

            using (var excel = new ExcelReport())
            {
                excel.HasCustomRows = false;
                //Customer Report
                if (parameters.CustomerId > 0)
                {
                    if (parameters.TransactionType == 6)
                    {
                        var resultDeny = new List<TransactionDenyReportModel>();

                        foreach (var item in result.List.ToList())
                        {
                            var e = new TransactionDenyReportModel
                            {
                                CreditCardNumber = item.CreditCardNumber,
                                ResponseCode = item.ResponseCode,
                                ResponseCodeDescription = item.ResponseCodeDescription,
                                CreditCardHolder = item.CreditCardHolder,
                                Message = item.Message,
                                InsertDate = item.InsertDate
                            };
                            e.Message = e.Message.Substring(0, 26);
                            e.TerminalId = item.TerminalId;
                            resultDeny.Add(e);
                        }

                        return excel.CreateSpreadsheetWorkbook("Transacciones Denegadas", resultDeny);
                    }
                    else
                    {
                        return excel.CreateSpreadsheetWorkbook("Transacciones", result.List.ToList());
                    }
                }
                else // Partners reports 
                {
                    if (parameters.TransactionType == 6)
                    {
                        var resultDeny = new List<TransactionsReportDeniedByPartner>();

                        foreach (var item in resultPartner.List.ToList())
                        {
                            var e = new TransactionsReportDeniedByPartner
                            {
                                CreditCardNumber = item.CreditCardNumber,
                                ResponseCode = item.ResponseCode,
                                ResponseCodeDescription = item.ResponseCodeDescription,
                                CreditCardHolder = item.CreditCardHolder,
                                Message = item.Message,
                                InsertDate = item.InsertDate
                            };
                            e.Message = e.Message.Substring(0, 26);
                            e.TerminalId = item.TerminalId;
                            resultDeny.Add(e);
                        }

                        return excel.CreateSpreadsheetWorkbook("Transacciones Denegadas", resultDeny);
                    }
                    else
                    {
                        return excel.CreateSpreadsheetWorkbook("Transacciones", resultPartner.List.ToList());
                    }
                }
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public string GetMerchantDescriptionByTerminalId(string terminalId)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteScalar<string>("[dbo].[SP_TransactionsSAPServiceStationsTemp_Retrieve]",
                    new
                    {
                        TerminalId = terminalId
                    });
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        private static CreditCard GetCreditCard(string cardNumber)
        {
            CreditCard creditCard;

            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                creditCard = dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardByNumber_Retrieve]",
                    new
                    {
                        CreditCardNumber = cardNumber
                    }).FirstOrDefault();
            }                     

            //credit card exists?
            return creditCard;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        public DataTable GenerateDeniedReportData(ControlFuelsReportsBase parameters)
        {
            var result = new TransactionsReportDeniedByPartnerBase();
            var resultPartner = new TransactionsReportDeniedByPartnerBase();

            var startDate = new DateTime();
            var endDate = new DateTime();

            if (parameters.StartDate != null && parameters.EndDate != null)
            {
                startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
            }
            else if (parameters.Month != null && parameters.Year != null)
            {
                startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
            }          

            resultPartner.List = (List<TransactionsReportDeniedByPartner>)HttpContext.Current.Session["ReportData"];
            var rep = new TransactionsReportUtility();

            if (parameters.TransactionType == 6)
            {
                var resultDeny = new List<TransactionDenyReportModel>();

                foreach (var item in resultPartner.List.ToList())
                {
                    var e = new TransactionDenyReportModel
                    {
                        CreditCardNumber = item.CreditCardNumber,
                        ResponseCode = item.ResponseCode,
                        ResponseCodeDescription = item.ResponseCodeDescription,
                        CreditCardHolder = item.CreditCardHolder,
                        Message = item.Message,
                        InsertDate = item.InsertDate
                    };
                    e.Message = e.Message;
                    e.TerminalId = item.TerminalId;
                    resultDeny.Add(e);
                }

                return rep.DeniedTransactionsReport(resultDeny, startDate, endDate);
            }
            else
            {
                return rep.TransactionsReportDataPartner(resultPartner.List, startDate, endDate, parameters.TransactionType);
            }
        }

        /// <summary>
        /// Generate Real Vs Budget FuelsReport Excel
        /// </summary>
        /// <returns>A model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public byte[] GenerateApplicantsReportExcel(string NameDownload)
        {
            var result = (List<TransactionsReportDeniedByPartner>)HttpContext.Current.Session["ReportData"];

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook(NameDownload, result.ToList());
            }

        }
    }
}
