﻿using ECO_Partner.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ECO_Partner.Business
{
    public class SMSBillReportBusiness : IDisposable
    {
        public DataTable RetrieveReport(AdministrationReportsBase model)
        {
            var list = GetData(model);

            using (var dt = new DataTable())
            {
                dt.Columns.Add("Customer");
                dt.Columns.Add("SMSTotal");
                dt.Columns.Add("UnitAmount");
                dt.Columns.Add("TotalAmount");
                dt.Columns.Add("Partner");
                dt.Columns.Add("EndDate");

                foreach (var item in list)
                {
                    var row = dt.NewRow();
                    row["Customer"] = item.DecryptCustomerName;                    
                    row["SMSTotal"] = item.SMSTotal;
                    row["UnitAmount"] = item.UnitAmount;
                    row["TotalAmount"] = item.TotalAmount;
                    row["Partner"] = ECOsystem.Utilities.Session.GetPartnerInfo().Name;
                    row["EndDate"] = ECOsystem.Utilities.Miscellaneous.GetDateFormat(item.EndDate);

                    dt.Rows.Add(row);
                }
                return dt;
            }
        }

        List<SMSBillReport> GetData(AdministrationReportsBase model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<SMSBillReport>("[General].[SP_SMSBillReport_Retrieve]",
                    new
                    {
                        ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId,
                        model.StartDate,
                        model.EndDate,
                        model.Year,
                        model.Month
                    }).ToList();
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}