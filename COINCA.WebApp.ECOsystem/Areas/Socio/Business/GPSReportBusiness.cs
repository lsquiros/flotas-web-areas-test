﻿/************************************************************************************************************
*  File    : GPSReportBusiness.cs
*  Summary : GPS Business Methods
*  Author  : Stefano Quirós
*  Date    : 20/04/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECO_Partner.Models;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Core;
using AdministrationReportsBase = ECO_Partner.Models.AdministrationReportsBase;
using System.Data;
using System.Data.SqlTypes;

namespace ECO_Partner.Business
{
    public class GPSReportBusiness : IDisposable
    {

        /// <summary>
        /// Retrieve Applicants Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of CreditCardsReportBase in order to load the chart</returns>
        public GPSReportBase RetrieveGpsReport(AdministrationReportsBase parameters)
        {
            var result = new GPSReportBase();

            if (parameters.CustomerId == -1) parameters.CustomerId = null;
            GetReportData(parameters, result);
            result.Parameters = parameters;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            var custList = RetrieveCustomersByCountry(result.Parameters.CountryId).ToList();
            var newCustList = new List<Customers>();
            var custTypeAll = custList.FirstOrDefault(w => w.DecryptedName == "Todos");

            newCustList.Add(custTypeAll);
            newCustList.AddRange(custList.Where(w => w.DecryptedName != "Todos").ToList());
            result.UsersList = new SelectList(newCustList, "CustomerId", "DecryptedName", result.Parameters.UserId);

            return result;
        }

        /// <summary>
        /// Vehicles Generate
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        internal static object DownloadData(IEnumerable<GpsReport> list, AdministrationReportsBase param)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("ClientName");
                dt.Columns.Add("isDemo");
                dt.Columns.Add("PartnerName");
                dt.Columns.Add("Installations", typeof(int));
                dt.Columns.Add("InstallationsThisMonth", typeof(int));
                dt.Columns.Add("Fee", typeof(decimal));
                dt.Columns.Add("Cost", typeof(decimal));
                dt.Columns.Add("MembershipFee", typeof(decimal));
                dt.Columns.Add("MembershipCost", typeof(decimal));
                dt.Columns.Add("TotalCost", typeof(decimal));
                dt.Columns.Add("EndDate", typeof(DateTime));
                dt.Columns.Add("PrevEndDate", typeof(DateTime));
                dt.Columns.Add("EndDateStr", typeof(string));
                dt.Columns.Add("PrevEndDateStr", typeof(string));
                dt.Columns.Add("Active");
                dt.Columns.Add("Modality");
                dt.Columns.Add("ReportModality");
                dt.Columns.Add("TotalGPS", typeof(decimal));
				dt.Columns.Add("TotalOrdersAmount", typeof(decimal));
                dt.Columns.Add("TotalOrders", typeof(int));
                foreach (var item in list)
                {
                    var row = dt.NewRow();
                    row["ClientName"] = item.ClientName;
                    row["isDemo"] = item.isDemo;
                    row["PartnerName"] = ECOsystem.Utilities.Session.GetPartnerInfo().Name;
                    row["Installations"] = item.Installations;
                    row["InstallationsThisMonth"] = item.InstallationsThisMonth;
                    row["Fee"] = item.Fee;
                    row["Cost"] = item.Cost;
                    row["MembershipFee"] = item.MembershipFee;
                    row["MembershipCost"] = item.MembershipCost;
                    row["TotalCost"] = item.TotalCost;
                    row["EndDate"] = item.EndDate;
                    row["PrevEndDate"] = item.PrevEndDate;
                    row["EndDateStr"] = item.EndDateStr;
                    row["PrevEndDateStr"] = item.PrevEndDateStr;
                    row["TotalGPS"] = item.TotalGPS;
                    row["Active"] = 1;
                    //row["Modality"] = param.GpsModality;
                    row["Modality"] = item.GPSModalityId;
					row["ReportModality"] = param.GpsModality;
                    row["TotalOrdersAmount"] = item.TotalOrdersAmount;
                    row["TotalOrders"] = item.TotalOrders;
					dt.Rows.Add(row);
                }
                return dt;
            }
        }
        
        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(AdministrationReportsBase parameters, GPSReportBase result)
        {
            GetReportDataApplicants(parameters, result);
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private static void GetReportDataApplicants(AdministrationReportsBase parameters, GPSReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                if (parameters.EndDate == null || parameters.EndDate < (DateTime)SqlDateTime.MinValue)
                    parameters.EndDate = DateTime.Now;
                result.List = dba.ExecuteReader<GpsReport>("[Control].[Sp_GPSReport_Retrieve]",
                    new
                    {
                        PartnerId = parameters.PartnerId,
                        CustomerId = parameters.CustomerId,
                        ReportCriteria = parameters.ReportCriteriaId,
                        Year = parameters.Year,
                        Month = parameters.Month,
                        EndDate = parameters.EndDate,
                        GpsModalityId = parameters.GpsModalityId
                    });
            }
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }

        /// <summary>
        /// Return a list of Countries for current user in order to using it for populate DropDownListFor
        /// </summary>
        private IEnumerable<Customers> RetrieveCustomersByCountry(int? countryId)
        {           

            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<Customers>("[General].[Sp_CustomersByCountry_Retrieve]",
                    new
                    {
                        CountryId = countryId,
                        PartnerId = Session.GetPartnerId(),
                    });

                //Order by Customer Name A-Z
                result = result.OrderBy(o => o.DecryptedName).ToList();

                return result;
            }
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}