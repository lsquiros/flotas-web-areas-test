﻿/************************************************************************************************************
*  File    : MasterCardsReportBusiness.cs
*  Summary : MasterCardsReport Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 01/20/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using ECO_Partner.Models;
using Newtonsoft.Json;
using AdministrationReportsBase = ECO_Partner.Models.AdministrationReportsBase;

namespace ECO_Partner.Business
{
    /// <summary>
    /// MasterCardsReport Class
    /// </summary>
    public class MasterCardsReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Applicants Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of MasterCardsReportBase in order to load the chart</returns>
        public MasterCardsReportBase RetrieveMasterCardsReport(AdministrationReportsBase parameters)
        {
            var result = new MasterCardsReportBase();
            GetReportData(parameters, result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            result.Parameters.CountryId = parameters.CountryId;
            result.Parameters.UserId = parameters.UserId;
            result.UsersList = new SelectList(RetrieveCustomersByCountry(result.Parameters.CountryId), "CustomerId", "DecryptedName", result.Parameters.UserId);
            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(AdministrationReportsBase parameters, MasterCardsReportBase result)
        {
            GetReportDataMasterCards(parameters, result);
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataMasterCards(AdministrationReportsBase parameters, MasterCardsReportBase result)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                result.List = dba.ExecuteReader<MasterCardsReport>("[Control].[Sp_MasterCardsReport_Retrieve]",
                    new
                    {
                        parameters.CustomerId,
                        parameters.CountryId,
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                    });
            }
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }

        /// <summary>
        /// Generate Real Vs Budget FuelsReport Excel
        /// </summary>
        /// <returns>A model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public byte[] GenerateMasterCardsReportExcel(AdministrationReportsBase parameters)
        {
            var result = new MasterCardsReportBase();

            GetReportDataMasterCards(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Reporte de Tarjetas Maestras", result.List.ToList());
            }

        }

        /// <summary>
        /// Return a list of Countries for current user in order to using it for populate DropDownListFor
        /// </summary>
        public IEnumerable<Customers> RetrieveCustomersByCountry(int? countryId)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var result = dba.ExecuteReader<Customers>("[General].[Sp_CustomersByCountry_Retrieve]",
                    new
                    {
                        CountryId = countryId,
                        PartnerId = Session.GetPartnerId(),
                    });
                return result;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
