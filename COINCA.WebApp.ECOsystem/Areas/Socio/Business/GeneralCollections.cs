﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using ECOsystem.DataAccess;
using ECOsystem.Business.Core;
using ECOsystem.Models.Core;
using ECO_Partner.Models;
using ReportCriteria = ECO_Partner.Models.ReportCriteria;

namespace ECO_Partner.Business 
{
    /// <summary>
    /// General Collections Class Contains all methods for load Drop Down List
    /// </summary>
    public class GeneralCollections
    {
        /// <summary>
        /// Performs Alarm Credit Count
        /// </summary>
        /// <param name="pMinPct"></param>
        public static int AlarmCreditCountRetrieve(int pMinPct)
        {
            var today = DateTime.Today;
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Control].[Sp_AlarmCreditCount_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        today.Year,
                        today.Month,
                        MinPct = pMinPct,
                        Session.GetUserInfo().UserId
                    });
            }
        }



        /// <summary>
        /// Return a list of Counties in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCounties(int? stateId, int? countryId)
        {
            var dictionary = new Dictionary<int?, string>();
            if (stateId != null)
            {
                using (var business = new GeographicDivisionBusiness())
                {
                    var result = business.RetrieveCounties(null, stateId, countryId);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CountyId, r.Code + " " + r.Name);
                    }
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }


        /// <summary>
        /// Return a list of Cities in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCities(int? countyId, int? stateId, int? countryId)
        {
            var dictionary = new Dictionary<int?, string>();
            if (countyId != null)
            {
                using (var business = new GeographicDivisionBusiness())
                {
                    var result = business.RetrieveCities(null, countyId, stateId, countryId);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CityId, r.Code + " " + r.Name);
                    }
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }


        /// <summary>
        /// Return a list of Fuels in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetFuels
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new FuelsBusiness())
                {
                    var result = business.RetrieveFuels(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.FuelId, r.Name + " - " + r.CountryName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetDatesGroupBy
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {1, "Agrupación por: Año"},
                    {2, "Agrupación por: Mes"},
                    {4, "Agrupación por: Semana"},
                    {3, "Agrupación por: Día"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetComparativeGroupBy
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {100, "Comparación por: Vehículo"},
                    {200, "Comparación por: Marca"},
                    {300, "Comparación por: Modelo"},
                    {400, "Comparación por: Tipo"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Countries for current user in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCountriesCurrentUser
        {
            get
            {
                var result = RetrieveCountriesCurrentUser(null);
                var dictionary = result.ToDictionary(r => r.CountryId, r => r.Name);

                return new SelectList(dictionary, "Key", "Value");
            }
        }


         public static IEnumerable<Countries> RetrieveCountriesCurrentUser(int? countryId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Countries>("[General].[Sp_CountriesByUser_Retrieve]",
                    new
                    {
                        CountryId = countryId,
                        LoggedUserId = Session.GetUserInfo().UserId,
                        Key = key
                    });
            }
        }


        /// <summary>
        /// Return a list of Countries for current user in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersByPartnerWithAll
        {
            get
            {
                var dictionary = new Dictionary<int?, string> {{-1, "Todos"}};
                using (var business = new CustomersBusiness())
                {
                    var result = business.RetrieveCustomersCollectionByPartners(Session.GetPartnerId());
                    foreach (var r in result)
                        dictionary.Add(r.CustomerId, r.DecryptedName);
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetCustomersChargesByPartner
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new CustomersBusiness())
                {
                    var result = business.RetrieveCustomersCollectionByPartners(Session.GetPartnerId());
                    dictionary.Add(default(int), string.Format("Ninguno"));
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CustomerId, r.DecryptedName);
                    }                        
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetPartnersLinkedToUser
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var bus = new PartnersBusiness())
                {
                    var result = bus.RetrievePartnersLinkedToUser();
                    foreach (var r in result)
                    {
                        dictionary.Add(r.PartnerId, r.Name);
                    }   
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of States in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetStates(int? countryId)
        {
            var dictionary = new Dictionary<int?, string>();

            using (var business = new GeographicDivisionBusiness())
            {
                var result = business.RetrieveStates(null, countryId);
                foreach (var r in result)
                {
                    dictionary.Add(r.StateId, r.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        public static SelectList GetZones()
        {
            var dictionary = new Dictionary<int?, string>();

            using (var business = new CollectionZonesBusiness())
            {
                var result = business.RetrieveZones(null);
                foreach (var r in result)
                {
                    dictionary.Add(r.ZoneId, r.Name);
                }                    
            }
            return new SelectList(dictionary, "Key", "Value");
        }


        /// <summary>
        /// Return a list of Acces Countries for selected user in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCountriesAccess
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                var user = Session.GetUserInfo();
                if (!user.IsPartnerUser) return new SelectList(dictionary, "Key", "Value");

                foreach (var r in user.PartnerUser.CountriesAccessList.Where(x => x.IsCountryChecked))
                {
                    dictionary.Add(r.CountryId, r.CountryName);
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of currencies in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCurrencies
        {
            get
            {
                var result = RetrieveCurrencies(null);
                var dictionary = result.ToDictionary(r => r.CurrencyId, r => string.Format("{0} ({1})", r.Name, r.Code));
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static IEnumerable<Currencies> RetrieveCurrencies(int? currencyId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Currencies>("[Control].[Sp_Currencies_Retrieve]",
                    new
                    {
                        CurrencyId = currencyId,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Devuelve una lista de Encargados para ponerlos en 
        /// </summary>
        public static SelectList GetCustomerManager
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                var partnerId = Session.GetPartnerId();

                using (var business = new PartnersBusiness())
                {
                    var result = business.RetrieveCustomerManager(partnerId);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.UserId, r.DecryptedName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Partners in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetPartners
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new PartnersBusiness())
                {
                    var result = business.RetrievePartners(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.PartnerId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
       
        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetPartnerCardStatus
        {
            get
            {
                var dictionary = new Dictionary<int, string>();

                using (var business = new StatusBusiness())
                {
                    var result = business.RetrieveStatus("PARTNER_MASTER_CARD");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.StatusId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetPartnerCardStatusById(int? StatusId)
        {           
            var dictionary = new Dictionary<int, string>();

            using (var business = new StatusBusiness())
            {
                var result = business.RetrieveStatus("PARTNER_MASTER_CARD");
                foreach (var r in result)
                {
                    dictionary.Add(r.StatusId, r.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value", StatusId);           
        }

        /// <summary>
        /// Return a list of Customers Units Of Capacity for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersUnitsOfCapacity
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("CUSTOMERS_CAPACITY_UNITS");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

      

        /// <summary>
        /// Return a list of Customers Credit Card Issue For populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersCreditCardIssueForId
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("CUSTOMERS_CREDIT_CARDS_ISSUE_FOR");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Customers Credit Card Types For populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersCreditCardTypes
        {
            get
            {
                var dictionary = new Dictionary<string, string> { { "C", "Crédito" }, { "D", "Débito" } };
                return new SelectList(dictionary, "Key", "Value", "C");
            }
        }

        

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetMonthNames
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a current Month Name
        /// </summary>
        public static SelectList GetCurrentMonthName
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.Where(w => w.Id == DateTime.Now.Month).ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year - 5; i < year + 9; i++)
                {
                    dictionary.Add(i, i);
                }
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        public static SelectList GetCardYears
        {
            get
            { 
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year ; i < year + 15; i++)
                {   //ValidCreditCard for 15 years
                    dictionary.Add(i, i);
                }
                year = year + 1;
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }


        /// <summary>
        /// Return a list of Transactions Types it for populate DropDownListFor
        /// </summary>
        public static SelectList GetTransactionTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var year = DateTime.UtcNow.Year;
                dictionary.Add(1,"Procesadas");
                //dictionary.Add(2,"Flotantes");
                dictionary.Add(3,"Reversadas");
                //dictionary.Add(4,"Duplicadas");
                //dictionary.Add(5,"Ajustes");
                dictionary.Add(6,"Rechazadas");
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }
        

        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterDetailTransacReport
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var year = DateTime.UtcNow.Year;
                dictionary.Add(1, "Centro de Costo");
                dictionary.Add(2, "Grupo de Vehículos");
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterSettingOdometers
        {
            get
            {
                var dictionary = new Dictionary<int?, string> {{1, "Odómetros Erróneos"}, {2, "Historial de Odómetros"}};
                return new SelectList(dictionary, "Key", "Value", 1);
            }
        }


        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterPartnerCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var year = DateTime.UtcNow.Year;
                dictionary.Add(1, "Socio");
                dictionary.Add(2, "Clientes");
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetReportCriteria
        {
            get
            {
                return new SelectList(new Dictionary<int, string> { 
                    {(int)ReportCriteria.Period,"Por Período"},
                    {(int)ReportCriteria.DateRange,"Rango de Fechas"}
                }, "Key", "Value", "");
            }
        }

        /// <summary>
        /// Return the modality of the GPS
        /// </summary>
        public static SelectList GetGPSModality
        {
            get
            {
                return new SelectList(new Dictionary<int, string> { 
                    {1,"Alquilado"},
                    {2,"Propio"}
                }, "Key", "Value", "");
            }
        }

   

        /// <summary>
        /// Return a list of Icon of vehicle in order to using for populate DropDownList 
        /// </summary>
        public static SelectList GetIconVehicleCategory
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                string path = HttpContext.Current.Server.MapPath("~/Content/Images/Vehicles/");
                if (Directory.Exists(path))
                {
                    List<String> listImage = Directory.GetFiles(path, "*.png", SearchOption.AllDirectories).ToList();
                    foreach (String itemImage in listImage)
                    {
                        dictionary.Add(Path.GetFileName(itemImage), "{'image':'" + Path.GetFileName(itemImage) + "'}");
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

      

        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomerRoles
        {
            get
            {
                return new SelectList(new Dictionary<string, string> { 
                    {"CUSTOMER_ADMIN","Administrador"},
                    {"CUSTOMER_USER","Usuario Regular"},
                }, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetDriverRoles
        {
            get
            {
                return new SelectList(new Dictionary<string, string> { 
                    {"CUSTOMER_DRIVER","Conductor"}
                }, "Key", "Value");
            }
        }
    

        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetSuperRoles
        {
            get
            {
                return new SelectList(new Dictionary<string, string> { 
                    {"SUPER_ADMIN","Administrador"},
                    {"SUPER_USER","Usuario Regular"},
                }, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetPartnerRoles
        {
            get
            {
                if (Session.GetPartnerInfo().PartnerTypeId != 801)
                {
                    return new SelectList(new Dictionary<string, string> { 
                    {"PARTNER_ADMIN","Administrador"},
                    {"PARTNER_USER","Usuario Regular"},
                    }, "Key", "Value");
                }
                return new SelectList(new Dictionary<string, string> { 
                    {"INSURANCE_ADMIN","Administrador"},
                    {"INSURANCE_USER","Usuario Regular"},
                }, "Key", "Value");
            }
        }

        public static SelectList GetRoles
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new ScoreTypesBusiness())
                {
                    var result = business.RetrieveScoreTypes(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.ScoreTypeId, r.Code + " " + r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Partner Types to populate DropDownList
        /// </summary>
        public static SelectList GetPartnerTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("PARTNER_TYPE");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetMonthCardNames(int p)
        {     
              if (p == DateTime.Now.Year)
            {
                 
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name))
                                       .SkipWhile(element => element.Key < DateTime.Now.Month).ToList();
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
                //return new HtmlString(JsonConvert.SerializeObject(dictionary.ToList()));
            }
            else
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
                //return new HtmlString(JsonConvert.SerializeObject(dictionary.ToList()));
            }
       }

        public static SelectList GetVehiclesForNotifications
        {
            get
            {
                var dictionary = new Dictionary<string, string>();
                using (var bus = new PartnerNotificationsBusiness())
                {
                    var result = bus.VehiclesForNotificationsRetrieve().OrderBy(q => q.PlateId).ToList();
                    foreach (var r in result)
                    {
                        dictionary.Add(r.PlateId, r.PlateId);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetDriversForNotifications
        {
            get
            {
                var dictionary = new Dictionary<string, string>();
                using (var bus = new PartnerNotificationsBusiness())
                {
                    var result = bus.DriversForNotificationsRetrieve().OrderBy(q => q.DecryptedEmail).ToList();
                    foreach (var r in result)
                    {
                        dictionary.Add(r.DecryptedEmail, r.DecryptedEmail);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Get Classification Types
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int?, string> GetClassificationTypes()
        {
            var dictionary = new Dictionary<int?, string>();

            using (var business = new TypesBusiness())
            {
                var result = business.RetrieveClassificationTypes();
                foreach (var r in result)
                {
                    dictionary.Add(r.Id, r.Name);
                }
            }
            return dictionary;            
        }
    }
}