﻿using ECO_Partner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;

namespace ECO_Partner.Business
{
    public class FTPParametersBusiness : IDisposable
    {
        public FTPParameters FTPParametersRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<FTPParameters>("[General].[Sp_FTPParameters_Retrieve]", new
                {
                    PartnerId = ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId
                }).FirstOrDefault();
            }
        }

        public void FTPParametersAddOrEdit(FTPParameters model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_FTPParameters_AddOrEdit]", new
                {
                    model.Id,
                    model.HostName,
                    model.Key,
                    model.UserName,
                    model.Password,
                    model.HostFolder,
                    model.IsProtocolSFTP,
                    model.Active,
                    PartnerId = ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}