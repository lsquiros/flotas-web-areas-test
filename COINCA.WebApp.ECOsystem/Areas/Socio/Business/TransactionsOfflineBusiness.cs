﻿using ECO_Partner.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ECO_Partner.Business
{
    public class TransactionsOfflineBusiness : IDisposable
    {
        public TransactionsOffline RetrieveTransactionsOfflineParameters(int PartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<TransactionsOffline>("[Partners].[Sp_TransactionsOfflineParameters_Retrieve]",
                new
                {
                    PartnerId
                }).FirstOrDefault();
            }       
        }

        public IEnumerable<CustomersTransactionsOffline> CustomerTransactionsOfflineRetrieve(int PartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomersTransactionsOffline>("[Partners].[Customers_TransactionsOffline_Retrieve]",
                new
                {
                    PartnerId
                });
            }
        }

        public void AddOrEditTransactionsOffline(TransactionsOffline model, List<int> customers)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Partners].[Sp_TransactionsOffline_AddOrEdit]",
                new
                {
                    model.Id,
                    ECOsystem.Utilities.Session.GetUserInfo().PartnerId,
                    model.StartDate,
                    model.EndDate,
                    model.Hours,
                    model.Minutes,
                    model.Active,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId,
                    model.Emails
                });
                AddOrEditCustomersTransactionsOffline(customers ?? new List<int>(), model.Active);
            }        
        }

        private void AddOrEditCustomersTransactionsOffline(List<int> customers, bool active)
        {
            string XMLCustomersIds = null;
            
            if(customers.Any() && active)
            {
                var doc = new XDocument(new XElement("xmldata"));
                var root = doc.Root;
                foreach (var item in customers)
                {
                    root.Add(new XElement("Customers", new XAttribute("CustomerId", item)));
                }
                XMLCustomersIds = doc.ToString();
            }

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Partners].[Sp_CustomersTransactionsOfflineActive_AddOrEdit]",
                new
                {
                    XMLCustomersIds,
                    ECOsystem.Utilities.Session.GetUserInfo().PartnerId
                });
            }       
        }

        public void SendTransactionsOffline()
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Partners].[Sp_TransactionsOffline_Send]", new {                    
                    ECOsystem.Utilities.Session.GetUserInfo().PartnerId
                });                
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}