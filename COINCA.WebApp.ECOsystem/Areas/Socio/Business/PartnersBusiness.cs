﻿/************************************************************************************************************
*  File    : PartnersBusiness.cs
*  Summary : Partners Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.Business.Core;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECO_Partner.Models;

namespace ECO_Partner.Business
{
    /// <summary>
    /// Partners Business Class
    /// </summary>
    public class PartnersBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Partners
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Partners> RetrievePartners(int? partnerId, string key = null)
        {
            IList<Partners> result;
            using (var dba = new DataBaseAccess())
            {
                result = dba.ExecuteReader<Partners>("[General].[Sp_Partners_Retrieve]",
                    new
                    {
                        PartnerId = partnerId,
                        Key = key
                    });
            }
            if (partnerId == null || result.FirstOrDefault() == null) return result;

            using (var business = new UsersBusiness())
            {
                var aux = result.FirstOrDefault();
                aux.TempUsersList = business.RetrieveUsers(null, partnerId: partnerId).Select(x => new PartnerTempUser { UserFullName = x.DecryptedName, UserEmail = x.DecryptedEmail, RoleName = x.RoleName }).ToList();
                return new List<Partners> { aux };
            }
        }

        /// <summary>
        /// Retrieve Partners
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Partners> RetrievePartnersByLoggedUser()
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                if (user.IsPartnerUser)
                {
                    return dba.ExecuteReader<Partners>("[General].[Sp_Partners_Retrieve]",
                    new
                    {
                        user.PartnerGroupId,
                        user.PartnerId
                    });
                }
                return new List<Partners>();
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Partners
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public int AddOrEditPartners(Partners model)
        {
            int? returnValue;
            using (var dba = new DataBaseAccess())
            {
                returnValue = dba.ExecuteScalar<int>("[General].[Sp_Partners_AddOrEdit]",
                    new
                    {
                        model.PartnerId,
                        model.Name,
                        model.CountryId,
                        model.Logo,
                        model.ApiUserName,
                        model.ApiPassword,
                        model.CapacityUnitId,
                        model.PartnerTypeId,
                        model.LoggedUserId,
                        model.FuelErrorPercent,
                        model.RowVersion
                    });
            }
            return returnValue ?? 0;
        }


        /// <summary>
        /// Performs the the operation of delete on the model Partners
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeletePartners(int partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Partners_Delete]",
                    new { PartnerId = partnerId });
            }
        }        

        /// <summary>
        /// Obtain the Product Type according to PartnerId associate the Costumer
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public static int GetProductTypeInfo(int CustomerId)
        {
            int? iPartnerId = null;
            int? iProductTypeId = null;
            int iPId = 0;
            int iProdId = 0;

            using (var dba = new DataBaseAccess())
            {
                string strSPUserRetrive = "[General].[Sp_ProductTypeByCustomer]";

                var returnValue = dba.ExecuteReader<ProductTypeByCustomer>(strSPUserRetrive,
                    new
                    {
                       CustomerId
                    });

                if (returnValue != null && returnValue.Count > 0)
                {
                    iPartnerId = returnValue[0].PartnerId;
                    iProductTypeId = returnValue[0].ProductTypesId;
                }

                if (iPartnerId != null)
                    iPId = Convert.ToInt32(iPartnerId);
                if (iProductTypeId != null)
                    iProdId = Convert.ToInt32(iProductTypeId);
                //Retornar el Product
                return iProdId;

            }
        }

        public List<Partners> RetrievePartnersLinkedToUser()
        {            
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Partners>("[General].[Sp_PartnersLinkedToUser_Retrieve]",
                    new
                    {
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    }).ToList();
            }         
        }

        /// <summary>
        /// Devuelve una lista de Encargados por Socio.
        /// </summary>
        public  IEnumerable<Users> RetrieveCustomerManager(int? partner)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Users>("[General].[Sp_CustomerManager_Retrieve]",
                    new
                    {
                        PartnerId = partner
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}