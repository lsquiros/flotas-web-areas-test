﻿using ECO_Partner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using System.Web;

namespace ECO_Partner.Business
{
    public class GeneralParametersBusiness : IDisposable
    {

        public GeneralParameters GeneralParametersRetrieve(int PartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<GeneralParameters>("[General].[Sp_GeneralParameterByPartner_Retrieve]", new { PartnerId }).FirstOrDefault();
                if (result != null)
                {
                    result.CreditCardNumber = string.IsNullOrEmpty(result.CreditCardNumber) ? string.Empty : ECOsystem.Utilities.TripleDesEncryption.Decrypt(result.CreditCardNumber);
                    return result;
                }
                return new GeneralParameters();                
            }
        }

        public void GeneralParametersAddOrEdit(GeneralParameters model, int PartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_GeneralParametersByPartner_AddOrEdit]",
                    new
                    {
                        model.Id,
                        model.AlarmDays,
                        model.RTN,
                        model.PhoneNumber, 
                        model.Address,
                        model.BacService,
                        model.BacName,
                        PartnerId,
                        model.StationTolerance,
                        CreditCardNumber = string.IsNullOrEmpty(ECOsystem.Utilities.TripleDesEncryption.Encrypt(model.CreditCardNumber)) ? null : ECOsystem.Utilities.TripleDesEncryption.Encrypt(model.CreditCardNumber),
                        model.Amount,
                        model.Active,
                        model.CardAlarmEmail,
                        model.PreauthorizedDays,
                        model.PreauthorizedEmail,
                        ECOsystem.Utilities.Session.GetUserInfo().UserId,
                        model.SMSPriceXML,
                        model.SMSInternalModem,
                        model.ContractYears,
                        model.SelectAllVehicles,
                        model.ContractAlertDays,
                        model.PilotModeNotificationEmail,
                        model.CancelCreditCardNumber,
                        model.CancelCreditCardEmails,
                        model.CancelCreditCardActive,
                        model.AlertBefore,
                        model.Periodicity,
                        model.NameDay,
                        model.NameDate,
                        model.Repeat,
                        model.ExpireCreditCardEmails,
                        model.ExpireCreditCardActive,
                        model.MasterCardEmails,
                        model.MasterCardAlertActive,
                        model.MasterCardPercent,
                        model.MasterCardAlertPeriodicity,
                        model.MasterCardAlertNameDay,
                        model.MasterCardAlertNameDate,
                        model.MasterCardAlertRepeat

                    });
            }
        }

        /// <summary>
        /// GC
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}