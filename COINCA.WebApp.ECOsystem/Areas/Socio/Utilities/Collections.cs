﻿using ECOsystem.Business.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Partner.Utilities
{
    public class Collections
    {
        public static SelectList GetCustomers
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var bus = new PartnerSurveyBusiness())
                {
                    var result = bus.PartnerSurveyReportCustomers(ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CustomerId, r.DecryptedName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetSurveyNames
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var bus = new PartnerSurveyBusiness())
                {
                    var result = bus.PartnerSurveyNameForReport(ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.Id, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
    }
}