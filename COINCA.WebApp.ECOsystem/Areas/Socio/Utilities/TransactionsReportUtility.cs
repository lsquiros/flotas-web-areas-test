﻿/************************************************************************************************************
*  File    : TransactionsReportUtility.cs
*  Summary : Sort the data for the reports which are going to use rdlc files
*  Author  : Henry Retana
*  Date    : 11/03/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using ECO_Partner.Models;
using System.Globalization;


namespace ECO_Partner.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public class TransactionsReportUtility : IDisposable
    {
        /// <summary>
        /// Get the data table ready for the Denied Transactions Report by Customer
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable DeniedTransactionsReport(IEnumerable<TransactionDenyReportModel> list, DateTime startDate, DateTime endDate)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("PaymentInstrumentType");
                dt.Columns.Add("PaymentInstrument");
                dt.Columns.Add("ResponseCode");
                dt.Columns.Add("ResponseCodeDescription");
                dt.Columns.Add("Message");
                dt.Columns.Add("CreditCardHolder");
                dt.Columns.Add("InsertDate");
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("CountryName");
                dt.Columns.Add("ExchangeValue");
                dt.Columns.Add("RealAmountFormatted");
                dt.Columns.Add("IsInternational");

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);
                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["PaymentInstrument"] = item.PaymentInstrument;
                    dr["PaymentInstrumentType"] = item.PaymentInstrumentType;
                    dr["ResponseCode"] = item.ResponseCode;
                    dr["ResponseCodeDescription"] = item.ResponseCodeDescription;
                    dr["Message"] = item.Message;
                    dr["CreditCardHolder"] = item.CreditCardHolder;
                    dr["InsertDate"] = item.InsertDate;
                    dr["TerminalId"] = item.TerminalId;
                    dr["InitRangeDate"] = startDateStr;
                    dr["FiniRangeDate"] = endDateStr;
                    dr["CountryName"] = item.CountryName;
                    dr["ExchangeValue"] = item.ExchangeValue;
                    dr["RealAmountFormatted"] = item.RealAmount;
                    dr["IsInternational"] = item.IsInternational;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Get the datatable ready for the Transactions Report by Customer
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable TransactionsReportData(IEnumerable<TransactionsReport> list, DateTime startDate, DateTime endDate)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Invoice");
                dt.Columns.Add("MerchantDescription");
                dt.Columns.Add("SystemTraceCode");
                dt.Columns.Add("FuelName");
                dt.Columns.Add("State");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("HolderNameStr");
                dt.Columns.Add("DateStr");
                dt.Columns.Add("CapacityUnitValueStr");
                dt.Columns.Add("FuelAmountStr");
                dt.Columns.Add("OdometerStr");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("PaymentInstrument");
                dt.Columns.Add("PaymentInstrumentType");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("CountryName");
                dt.Columns.Add("ExchangeValue");
                dt.Columns.Add("RealAmountFormatted");

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["CustomerName"] = item.CustomerName;
                    dr["Invoice"] = item.Invoice;
                    dr["MerchantDescription"] = item.MerchantDescription;
                    dr["SystemTraceCode"] = item.SystemTraceCode;
                    dr["FuelName"] = item.FuelName;
                    dr["State"] = item.State;
                    dr["CostCenterName"] = item.CostCenterName;
                    dr["TerminalId"] = item.TerminalId;
                    dr["HolderNameStr"] = item.HolderNameStr;
                    dr["DateStr"] = item.DateToExport;
                    dr["CapacityUnitValueStr"] = item.CapacityUnitValue;
                    dr["FuelAmountStr"] = item.FuelAmount;
                    dr["OdometerStr"] = item.Odometer;
                    dr["VehicleName"] = item.VehicleName;
                    dr["PaymentInstrument"] = item.PaymentInstrument;
                    dr["PaymentInstrumentType"] = item.PaymentInstrumentType;

                    dr["InitRangeDate"] = startDateStr;
                    dr["FiniRangeDate"] = endDateStr;

                    dr["CountryName"] = item.CountryName;
                    dr["ExchangeValue"] = item.ExchangeValue;
                    dr["RealAmountFormatted"] = item.RealAmount;
                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Get the information for the reports by Partner
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable TransactionsReportDataPartner(IEnumerable<TransactionsReportDeniedByPartner> list, DateTime startDate, DateTime endDate, int transactionType)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Invoice");
                dt.Columns.Add("MerchantDescription");
                dt.Columns.Add("SystemTraceCode");
                dt.Columns.Add("FuelName");
                dt.Columns.Add("State");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("TerminalId");
                dt.Columns.Add("HolderNameStr");
                dt.Columns.Add("DateStr");
                dt.Columns.Add("CapacityUnitValueStr");
                dt.Columns.Add("FuelAmountStr");
                dt.Columns.Add("OdometerStr");
                dt.Columns.Add("PaymentInstrumentType");
                dt.Columns.Add("PaymentInstrument");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("CountryName");
                dt.Columns.Add("ExchangeValue");
                dt.Columns.Add("RealAmountFormatted");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("ReportName");
                dt.Columns.Add("IsInternational");

                string reportName = string.Empty;

                switch (transactionType)
                {
                    case 6:
                        reportName = "Reporte de Transacciones Denegadas";
                        break;
                    case 3:
                        reportName = "Reporte de Transacciones Reversadas";
                        break;
                    case 2:
                        reportName = "Reporte de Transacciones Flotantes";
                        break;
                    case 1:
                        reportName = "Reporte de Transacciones Procesadas";
                        break;
                    case 8:
                        reportName = "Reporte de Transacciones sin Estación de Servicio";
                        break;
                }

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);

                foreach (var item in list)
                {
                    DataRow dr = dt.NewRow();

                    dr["CustomerName"] = item.CustomerName;
                    dr["Invoice"] = item.Invoice;
                    dr["MerchantDescription"] = item.MerchantDescription;
                    dr["SystemTraceCode"] = item.SystemTraceCode;
                    dr["FuelName"] = item.FuelName;
                    dr["State"] = item.State;
                    dr["CostCenterName"] = item.CostCenterName;
                    dr["TerminalId"] = item.TerminalId;
                    dr["HolderNameStr"] = item.HolderNameStr;
                    dr["DateStr"] = item.DateStr;
                    dr["CapacityUnitValueStr"] = item.CapacityUnitValueStr;
                    dr["FuelAmountStr"] = item.FuelAmount;
                    dr["OdometerStr"] = item.OdometerStr;
                    dr["PaymentInstrumentType"] = item.PaymentInstrumentType;
                    dr["PaymentInstrument"] = item.PaymentInstrument;
                    dr["InitRangeDate"] = startDateStr;
                    dr["FiniRangeDate"] = endDateStr;
                    dr["CountryName"] = item.CountryName;
                    dr["ExchangeValue"] = item.ExchangeValue;
                    dr["RealAmountFormatted"] = item.RealAmount;
                    dr["VehicleName"] = item.VehicleName;
                    dr["ReportName"] = reportName;
                    dr["IsInternational"] = item.IsInternational;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            throw new NotImplementedException();
        }
  
    }
}
