﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ECO_Partner.Utilities
{
    /// <summary>
    /// Security
    /// </summary>
    public class EcoAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Control the access by on the user's permissions
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentException("httpContext");
            
            //Validates if the user is already logged
            var acc = new GetAccountUtility();

            acc.GetUserInformation(ref httpContext); 

            if (!httpContext.Request.IsAuthenticated)
                return false; 

            var controller = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString().ToUpper();
            var action = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString().ToUpper();
            var user = ECOsystem.Utilities.Session.GetUserInfo();
            
            using (var context = new ECOsystem.Business.Core.UserRolesPermissionsBusiness())
                return context.UserPermissionValidation(user.EncryptedUserName, controller, action, user.RoleId);

        }

        /// <summary>
        /// Validates the access if the user is not logged in
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Unauthorized" }, { "controller", "Account" } });

                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "UnauthorizedPartial" }, { "controller", "Account" } });
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Login" }, { "controller", "Account" } });

                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}

