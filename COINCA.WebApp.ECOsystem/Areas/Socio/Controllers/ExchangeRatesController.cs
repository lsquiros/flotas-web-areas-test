﻿using ECO_Partner.Models;
using ECO_Partner.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECO_Partner.Business;
using ECOsystem.Business.Utilities;
using System.Web.Script.Serialization;
using ECOsystem.Models.Core;




namespace ECO_Partner.Controllers
{
    public class ExchangeRatesController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var bus = new ExchangeRatesBusiness())
                {
                    return View(bus.ExchangeRatesRetrieve());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExchangeRatesAddOrEdit(ExchangeRates model)
        {
            try
            {
                using (var bus = new ExchangeRatesBusiness())
                {
                    bus.ExchangeRatesAddOrEdit(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Agrega/Edita tipos de cambio con la siguiente información = {0}", new JavaScriptSerializer().Serialize(model)));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                var message = "Error al agregar o editar tipos de cambio.";
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format(message + " Detalle del error = {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = message, TechnicalError = e.Message });
            }
        }
    }
}