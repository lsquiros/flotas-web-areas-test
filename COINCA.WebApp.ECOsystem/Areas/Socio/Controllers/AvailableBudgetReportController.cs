﻿using ECO_Partner.Business;
using ECOsystem.Business.Utilities;


using System;
using System.Collections.Generic;

using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COINCA.WebApp.ECO_Partner.Controllers
{
    public class AvailableBudgetReportController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DonwloadFile(int? CustomerId)
        {
            try
            {
                using (var bus = new AvailableBudgetReportBusiness())
                {
                    using (var ms = new MemoryStream())
                    {
                        var sw = new StreamWriter(ms);
                        var data = bus.GetReportData(CustomerId);
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Genera Archivo de Disponible por Tarjeta, parámetros: Cliente = {0}", CustomerId == -1 ? "Todos" : CustomerId.ToString()));
                        if (data.Count() > 0)
                        {
                            foreach (var item in data)
                            {
                                sw.WriteLine(item.DecryptPhoneNumber + ";" + item.MessageFixed);
                                sw.Flush();
                            }
                            return new FileStreamResult(new MemoryStream(ms.ToArray()), "text/plain")
                            {
                                FileDownloadName = "Archivo de Disponible por Tarjeta.csv"
                            };
                        }
                        return View("Index");
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Generar Archivo de Disponible por Tarjeta, Error: {0}", e.Message));
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}