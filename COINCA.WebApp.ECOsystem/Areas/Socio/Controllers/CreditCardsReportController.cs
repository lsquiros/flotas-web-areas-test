﻿/************************************************************************************************************
*  File    : CreditCardsReportController.cs
*  Summary : CreditCards Report Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 01/20/2015
*  Mod. By : Gerald Solano
*  Mod.Date: 03/30/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/


using System.Web.Mvc;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Data;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECO_Partner.Business;
using ECO_Partner.Utilities;
using ECOsystem.Models.Miscellaneous;




namespace ECO_Partner.Controllers.Partner
{
    /// <summary>
    /// Applicants Report Class
    /// </summary>
    public class CreditCardsReportController : Controller
    {
        /// <summary>
        /// Renderizado inicial del Módulo de Reporte de Tarjetas
        /// </summary>
        /// <returns></returns>
        /// Modificado por: Gerald Solano
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new CreditCardsReportBusiness())
                {
                    return View(business.RetrieveCreditCardsReport(new AdministrationReportsBase()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Credit Card Report Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(CreditCardsReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new CreditCardsReportBusiness())
                {
                    // If get all customers
                    if (model.Parameters.CustomerId == -1 || model.Parameters.CustomerId == null)
                        model.Parameters.PartnerId = ECOsystem.Utilities.Session.GetPartnerId();

                    var creditCards = business.RetrieveCreditCardsReport(model.Parameters);
                    Session["CreditCardsReport"] = creditCards;
                    return View(creditCards);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return RedirectToAction("Index");
            }

        }

        /// <summary>
        /// Renderizado inicial del Módulo de Reporte de Tarjetas Detallado
        /// </summary>
        /// <returns></returns>
        /// Modificado por: Gerald Solano
        [EcoAuthorize]
        public ActionResult Admin()
        {
            try
            {
                using (var business = new CreditCardsReportBusiness())
                {
                    return View(business.RetrieveCreditCardsReportDetail(new AdministrationReportsBase()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Credit Card Report Detail Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Admin(CreditCardsReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new CreditCardsReportBusiness())
                {
                    return View(business.RetrieveCreditCardsReportDetail(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return RedirectToAction("Admin");
            }

        }

        /// <summary>
        /// Excel Report Download
        /// </summary>
        /// <returns>File Stream Result</returns>
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<AdministrationReportsBase>(Miscellaneous.Base64Decode(p));
                if (model != null)
                {
                    model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                    model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
                }
                if (model.Month != null && Convert.ToInt32(model.Month) > 0 && model.Year != null && Convert.ToInt32(model.Year) > 0)
                {
                    if (model.StartDate == null)
                    {
                        model.StartDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1, 0, 0, 0);
                    }

                    if (model.EndDate == null)
                    {
                        model.EndDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month)), 23, 59, 59);
                    }
                }

                using (var business = new CreditCardsReportBusiness())
                {
                    var creditCardsReport = (CreditCardsReportBase)Session["CreditCardsReport"];
                    if (creditCardsReport == null)
                    {
                        creditCardsReport = business.RetrieveCreditCardsReport(model);
                    }
                    var report = business.GenerateCreditCardsReportExcel(model, creditCardsReport);
                    Session["Reporte"] = report;

                    return View("Index", creditCardsReport);
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return null;
            }

        }

        /// <summary>
        /// Excel Report Detail Download
        /// </summary>
        /// <returns>File Stream Result</returns>
        public ActionResult ExcelReportDetailDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<AdministrationReportsBase>(Miscellaneous.Base64Decode(p));
                if (model != null)
                {
                    model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                    model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
                }
                using (var business = new CreditCardsReportBusiness())
                {
                    var report = business.GenerateCreditCardsReportDetailExcel(model);
                    Session["Reporte"] = report;

                    return View("Admin", business.RetrieveCreditCardsReportDetail(model));
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return null;
            }

        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        public ActionResult PrintReport(string p, string returnView)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<AdministrationReportsBase>(Miscellaneous.Base64Decode(p));

                ViewBag.PrintView = true;
                using (var business = new CreditCardsReportBusiness())
                {
                    if (returnView == "Admin")
                    {
                        return View(returnView, business.RetrieveCreditCardsReportDetail(model));
                    }
                    var creditCards = (CreditCardsReportBase)Session["CreditCards"];

                    if (creditCards == null)
                    {
                        creditCards = business.RetrieveCreditCardsReport(model);
                    }
                    return View(returnView, creditCards);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return View(returnView);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Conciliación de Transacciones", true);
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), Request.Form["reportName"], this.ToString().Split('.')[2], "Reporte de Tarjetas de Crédito");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e, true);
                ViewBag.ErrorReport = 1;
                using (var business = new CreditCardsReportBusiness())
                {
                    return View("Index", business.RetrieveCreditCardsReport(new AdministrationReportsBase()));
                }
            }
        }
    }
}
