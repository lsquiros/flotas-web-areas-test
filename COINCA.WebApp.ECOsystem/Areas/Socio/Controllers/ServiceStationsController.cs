﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECO_Partner.Business;
using ECO_Partner.Utilities;
using Microsoft.VisualBasic.FileIO;
using System.Data;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Newtonsoft.Json;




namespace ECO_Partner.Controllers.Partner
{
    /// <summary>
    ///  ServiceStationsController
    /// </summary>
    public class ServiceStationsController : Controller
    {
        private readonly PartnerNewsBusiness _business = new PartnerNewsBusiness();
        ///<Summary>
        /// Get the Index
        ///</Summary>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                return View(RetriveData(null));
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [EcoAuthorize]
        public PartialViewResult AddOrEditServiceStations(ServiceStations model)
        {
            try
            {
                using (var bus = new ServiceStationsBusiness())
                {
                    model.TerminalList = Session["CurrentServiceStation"] != null ? ((ServiceStations)Session["CurrentServiceStation"]).TerminalList : new List<Terminals>();
                    bus.ServiceStationsAddOrEdit(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model)));
                    return PartialView("_partials/_Grid", RetriveData(null).List);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ServiceStations")
                });
            }
        }

        /// <summary>
        /// Edit Service Stations by Excel.
        /// Receive an excel in json format modeled with 
        /// the fields to update in the database, 
        /// it becomes a collection for the SP
        /// to update it massively
        /// </summary>
        /// <param name="model">JSON Stringify ServiceStations</param>
        /// <returns></returns>
        public JsonResult AddOrEditServiceStationsImport(string data)
        {
            try
            {
                using (var bus = new ServiceStationsBusiness())
                {
                    IEnumerable<ServiceStations> serviceStations = JsonConvert.DeserializeObject<IEnumerable<ServiceStations>>(data);
                    var serviceStationsResponse = bus.ServiceStationsAddOrEdit(serviceStations);

                    return Json(new
                    {
                        data = serviceStationsResponse,
                        status = "success",
                        message = "Registros actualizados con éxito"
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(new
                {
                    data = string.Empty,
                    status = "error",
                    message = e.Message,
                }, JsonRequestBehavior.AllowGet);

            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [EcoAuthorize]
        public PartialViewResult LoadServiceStations(int id)
        {
            try
            {
                if (id < 0)
                {
                    var modal = new ServiceStations();
                    Session["CurrentServiceStation"] = null;
                    return PartialView("_partials/_AddOrEdit", modal);
                }
                var nmodal = RetriveData(id).List.FirstOrDefault();
                nmodal.TerminalList = GetTerminalsByServiceStation(nmodal.ServiceStationId);

                Session["CurrentServiceStation"] = nmodal;

                return PartialView("_partials/_AddOrEdit", nmodal);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ServiceStations")
                });
            }
        }

        private List<Terminals> GetTerminalsByServiceStation(int? ServiceStationId)
        {
            try
            {
                using (var bus = new ServiceStationsBusiness())
                {
                    return bus.GetTerminalsByServiceStation(ServiceStationId);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult SearchServiceStations(string key)
        {
            try
            {
                return PartialView("_partials/_Grid", RetriveData(null, key).List);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ServiceStations")
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadStates(int countryId)
        {
            try
            {
                var list = ServiceStationsBusiness.GetStates(countryId);

                if (list == null) return Json(null);

                var statesList = list.ToList();

                return Json(statesList.AsEnumerable(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadCantons(int stateId, int countryId)
        {
            try
            {
                var list = ServiceStationsBusiness.GetCounties(stateId, countryId);

                if (list == null) return Json(null);

                List<ECOsystem.Models.Core.Counties> statesList = list.ToList();

                return Json(statesList.AsEnumerable(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(null);
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Vehicles
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteServiceStatios(int id)
        {
            try
            {
                var business = new ServiceStationsBusiness();

                business.DeleteServiceStations(id);

                string message = "[Delete] ServiceStationId:" + id.ToString();
                new EventLogBusiness().AddLogEvent(LogState.INFO, message);

                return PartialView("_partials/_Grid", RetriveData(null).List);

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index")
                    });
                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ServiceStations")
                });
            }
        }

        private ServiceStationsBase RetriveData(int? id, string key = null)
        {
            try
            {
                int? countryId = null;
                int? customerid = null;
                int? pPartnerId = ECOsystem.Utilities.Session.GetPartnerId();

                if (ECOsystem.Utilities.Session.GetCustomerInfo() != null)
                {
                    var cus = ECOsystem.Utilities.Session.GetCustomerInfo();
                    customerid = ECOsystem.Utilities.Session.GetCustomerId();
                    countryId = cus.CountryId;
                }

                var model = new ServiceStationsBase
                {
                    Data = new ServiceStations(),
                    List = ServiceStationsBusiness.ServiceStationRetrieve(id, countryId, pPartnerId, customerid, key),
                    Menus = new List<AccountMenus>()
                };

                Session["ServiceStationsInfo"] = model.List;

                return model;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bacAfiliado"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetServiceStationName(string bacAfiliado)
        {
            try
            {
                var service = RetriveData(null, bacAfiliado).List.FirstOrDefault();

                return Json(service != null ? service.Name : null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        // [EcoAuthorize]
        [HttpPost]
        public ActionResult AddOrEditImportServiceStation()
        {
            DataTable csvData = new DataTable();
            var ErrorList = new List<string>();
            var List = new List<ServiceStationErrors>();
            int ErrorCount = 0;
            int SuccessCount = 0;
            try
            {
                HttpPostedFileBase file = Request.Files[0];
                // Verify that the user selected a file
                if (file != null && file.ContentLength > 0)
                {
                    // extract only the filename
                    var fileName = Path.GetFileName(file.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    //var path = Path.Combine(Server.MapPath ("~/App_Data"), fileName);
                    var path = Path.GetTempPath() + fileName;
                    file.SaveAs(path);
                    // redirect back to the index action to show the form once again
                    int RowCount = 1;
                    using (TextFieldParser csvReader = new TextFieldParser(path))
                    {
                        csvReader.SetDelimiters(new string[] { "," });
                        csvReader.HasFieldsEnclosedInQuotes = true;
                        int countColumns = 0;
                        int AfiliadoColumn = 0;
                        int Terminalcolumn = 0;
                        while (!csvReader.EndOfData)
                        {

                            ServiceStations model = new ServiceStations();
                            string[] colFields = csvReader.ReadFields();
                            string[] ColumnsToModel = new string[colFields.Count()];
                            foreach (string column in colFields)
                            {
                                if (RowCount == 1)
                                {
                                    if (column.ToLower() == "afiliado")
                                    {
                                        AfiliadoColumn = countColumns;
                                    }
                                    if (column.ToLower() == "terminal" || column.ToLower() == "terminales")
                                    {
                                        Terminalcolumn = countColumns;
                                    }
                                }

                                else
                                {
                                    DataColumn datecolumn = new DataColumn(column);
                                    datecolumn.AllowDBNull = true;
                                    ColumnsToModel[countColumns] = column;
                                }
                                countColumns++;
                            }
                            countColumns = 0;
                            if (RowCount > 1)
                            {
                                model.BacId = ColumnsToModel[AfiliadoColumn];
                                model.Terminal = ColumnsToModel[Terminalcolumn];

                                using (var business = new ServiceStationsBusiness())
                                {
                                    ServiceStationErrors ErrorModel = new ServiceStationErrors();
                                    try
                                    {
                                        if (model.BacId != null || model.Terminal != null)
                                        {
                                            var Response = business.AddOrEditImportTerminalsServiceStation(model);

                                            if (Convert.ToInt32(Response.Split('-')[2]) == 0)
                                            {
                                                ErrorModel.Item = RowCount - 1 + " ) " + Response.Split('-')[0] + Response.Split('-')[1];
                                                ErrorCount++;
                                            }
                                            else
                                            {
                                                ErrorModel.Item = RowCount - 1 + " ) " + Response.Split('-')[0] + Response.Split('-')[1];
                                                SuccessCount++;

                                            }

                                        }
                                        else
                                        {
                                            ErrorModel.Item = RowCount - 1 + " ) " + model.Terminal + " - Falló" + " - Mensaje de error: El número de Afiliado o Terminal no pueden venir nulos'";
                                            ErrorCount++;
                                        }
                                        List.Add(ErrorModel);
                                        #region log
                                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model.Terminal + model.BacId)));
                                        #endregion
                                    }
                                    catch (Exception e)
                                    {
                                        ErrorModel.Item = RowCount + " ) " + model.Terminal + " - Falló" + " - Mensaje de error:'" + e.Message + "'";
                                        List.Add(ErrorModel);
                                        ErrorCount++;
                                        #region log
                                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(ErrorModel)));
                                        #endregion
                                    }
                                }
                            }
                            RowCount++;
                        }
                        using (var business = new ServiceStationsBusiness())
                        {
                            Session["SummaryFile"] = business.CreateTxtFormat(List, ErrorCount, SuccessCount);
                            Session["Path"] = path;
                        }
                    }
                }

                return View("Index", RetriveData(null));
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public ActionResult DonwloadReportFile()
        {
            try
            {
                byte[] SummaryFile = null;
                SummaryFile = (byte[])Session["SummaryFile"];
                Session["SummaryFile"] = null;
                string path = Session["Path"].ToString();
                Session["Path"] = null;
                System.IO.File.Delete(path);
                var UserInfo = ECOsystem.Utilities.Session.GetUserInfo();
                DateTime Today = DateTime.Today.Date;
                return new FileStreamResult(new MemoryStream(SummaryFile), "text/plain")
                {
                    FileDownloadName = UserInfo.DecryptedName + "- Reporte de Importación Terminales - " + Today + ".txt"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ValidateTerminal(string TerminalId)
        {
            try
            {
                using (var bus = new ServiceStationsBusiness())
                {
                    var model = new ServiceStations();

                    if (Session["CurrentServiceStation"] != null)
                    {
                        model = (ServiceStations)Session["CurrentServiceStation"];
                    }
                    else
                    {
                        model = new ServiceStations() { TerminalList = new List<Terminals>() };
                    }

                    if (model.TerminalList.Where(x => x.TerminalId == TerminalId).Any()) return Json("Exists", JsonRequestBehavior.AllowGet);

                    if (!bus.ValidateTerminal(TerminalId))
                    {
                        model.TerminalList.Add(new Terminals() { TerminalId = TerminalId });
                        Session["CurrentServiceStation"] = model;
                        return Json(model.TerminalList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Exists", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RemoveTerminal(string TerminalId)
        {
            try
            {
                var model = (ServiceStations)Session["CurrentServiceStation"];
                model.TerminalList.Remove(model.TerminalList.Where(x => x.TerminalId == TerminalId).Select(x => x).FirstOrDefault());
                Session["CurrentServiceStation"] = model;
                return Json(model.TerminalList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        #region MapsServiceStations

        /// <summary>
        /// Show the partial view with the maps
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ShowMapsServiceStations(int? id)
        {
            try
            {
                String img = _business.RetrieveImageStation(RetrievePartnerId());
                if (img != null)
                    ViewBag.imageS = img;

                return PartialView("_partials/_Maps");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        ///<Summary>
        /// Get the Index
        ///</Summary>
        [HttpPost]
        public ActionResult Maps(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength <= 0) return RedirectToAction("Index");
                var ms = new MemoryStream();
                var img = Image.FromStream(file.InputStream);


                float origWidth = img.Width;
                float origHeight = img.Height;
                var sngRatio = origWidth / origHeight;
                const int newWidth = 400;
                var newHeight = newWidth / sngRatio;
                var newBmp = new Bitmap(img, Convert.ToInt32(newWidth), Convert.ToInt32(newHeight));
                var oGraphics = Graphics.FromImage(newBmp);
                oGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                oGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                oGraphics.DrawImage(img, 0, 0, newWidth, newHeight);

                img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                var imageArray = ms.ToArray();

                _business.AddOrEditImageStation(RetrievePartnerId(), imageArray);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        ///<Summary>
        /// Holder   
        ///</Summary>      

        public Image ByteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                var ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length) { Position = 0 };
                return Image.FromStream(ms, true);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        public int? RetrievePartnerId()
        {
            try
            {
                var partnerId = ECOsystem.Utilities.Session.GetPartnerId();
                if (partnerId != null)
                    return partnerId;
                var id = ECOsystem.Utilities.Session.GetUserInfo().LoggedUserId ?? 0;
                var partnerOfUserId = _business.RetrievePartnerId(id);
                return partnerOfUserId;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }
        #endregion


        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            try
            {
                using (var business = new ServiceStationsBusiness())
                {
                    Session["Reporte"] = business.ReportData((List<ServiceStations>)Session["ServiceStationsInfo"]);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataSet report = new DataSet();
                if (Session["Reporte"] != null)
                {
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte Estaciones de Servicio");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "ServiceStationsReport", this.ToString().Split('.')[2], "Reporte Estaciones de Servicio");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                ViewBag.ErrorReport = 1;
                var model = new ServiceStationsBase
                {
                    Data = new ServiceStations(),
                    List = (List<ServiceStations>)Session["ServiceStationsInfo"]
                };
                return View("Index", model);
            }
        }

        public ActionResult DonwloadExampleFile()
        {
            try
            {
                byte[] SummaryFile = System.IO.File.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data/Ejemplo_Importacion_de_Terminales.csv")));

                return new FileStreamResult(new MemoryStream(SummaryFile), "text/csv")
                {
                    FileDownloadName = "Ejemplo_Importacion_de_Terminales.csv"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download xlsx example file
        /// </summary>
        /// <returns>Excel File</returns>
        public ActionResult DonwloadExampleFileExcel()
        {
            try
            {
                byte[] SummaryFile = System.IO.File.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data/Ejemplo_Estaciones_Servicio.xlsx")));

                return new FileStreamResult(new MemoryStream(SummaryFile), "text/csv")
                {
                    FileDownloadName = "Ejemplo_Estaciones_Servicio.xlsx"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
