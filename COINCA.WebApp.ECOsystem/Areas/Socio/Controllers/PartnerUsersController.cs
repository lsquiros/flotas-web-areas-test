﻿/************************************************************************************************************
*  File    : CustomerUsersController.cs
*  Summary : Users Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using ECOsystem.Business.Core;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using ECOsystem;
using ECO_Partner.Business;
using ECO_Partner.Utilities;
using ECO_Partner.Models;




namespace ECO_Partner.Controllers
{
    /// <summary>
    /// Users Controller. Implementation of all action results from views
    /// </summary>
    public class PartnerUsersController : Controller
    {

        private ApplicationUserManager _userManager;

        /// <summary>
        /// Asp Net User Manager
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? (_userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());
            }

        }


        /// <summary>
        /// Users Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "PARTNER_ADMIN,INSURANCE_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                #region Filters
                string page = Request.QueryString["grid-page"];
                string key = null;
                int? statusId = 2;

                if (page != null)
                {
                    if (Session["filterKey"] != null)
                        key = Session["filterKey"].ToString();

                    if (Session["filterStatusId"] != null)
                        statusId = (int)Session["filterStatusId"];
                }
                else
                {
                    Session.Remove("filterKey");
                    Session.Remove("filterStatusId");
                }
                #endregion Filters

                using (var business = new UsersBusiness())
                {
                    var model = new ECOsystem.Models.Core.UsersBase
                    {
                        Data = new ECOsystem.Models.Core.Users(),
                        List = business.RetrieveUsers(null, key, partnerId: ECOsystem.Utilities.Session.GetPartnerId(), UserStatus: statusId) //ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId
                    };
                    model.Parameters.Key = key;
                    model.Parameters.Status = statusId;

                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Users
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditUsers(ECOsystem.Models.Core.Users model)
        {
            try
            {
                var partner = ECOsystem.Utilities.Session.GetPartnerInfo();

                using (var business = new UsersBusiness())
                {
                    if (model.PartnerUser == null) { model.PartnerUser = new ECOsystem.Models.Core.PartnerUsers(); }
                    model.PartnerUser.CountriesAccessList = business.RetrievePartnerUsersByCountry(Convert.ToInt16(model.PartnerUserId));

                    //GSOLANO: Se establece por defecto setear el país actual cuando se crea un usuario Socio nuevo.
                    if (model != null && model.PartnerUser.CountriesAccessList == null)
                    {
                        if (partner != null)
                        {
                            var countries = business.RetrievePartnerUsersByCountry(Convert.ToInt16(partner.PartnerId));//Convert.ToInt16(model.PartnerUser.PartnerUserId));
                            var currentCountry = countries.FirstOrDefault(w => w.CountryId == partner.CountryId);
                            if (currentCountry != null)
                            {
                                currentCountry.IsCountryChecked = true;
                            }
                            model.PartnerUser.CountriesAccessList = countries;
                        }
                    }

                    business.AddOrEditUsers(model, UserManager, Url, Request);

                    #region Event log

                    var user = ECOsystem.Utilities.Session.GetUserInfo();

                    //Add log event
                    AddLogEvent(
                            message: string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model)),
                            userId: user != null ? user.UserId : 0,
                            customerId: user != null ? user.CustomerId : 0,
                            partnerId: user != null ? user.PartnerId : 0,
                            exception: null,
                            isInfo: true);
                    #endregion

                    return PartialView("_partials/_Grid", business.RetrieveUsers(null, partnerId: ECOsystem.Utilities.Session.GetPartnerId()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);


                if (e.Message.Contains("ERROR EXIST_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Usuario ya existe",
                        TechnicalError = "El usuario con el correo especificado ya existe en el sistema.",
                        ReturnUlr = Url.Action("Index", "PartnerUsers")
                    });

                }
                else if (e.Message.Contains("ERROR UPDT_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Error de Actualización",
                        TechnicalError = "El usuario que se desea actualizar presenta inconsistencias, por favor intente de nuevo el proceso de actualización.",
                        ReturnUlr = Url.Action("Index", "PartnerUsers")
                    });

                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "PartnerUsers")
                    });
                }
            }
        }

        /// <summary>
        /// Search Users
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "PARTNER_ADMIN,INSURANCE_ADMIN")]
        public PartialViewResult SearchUsers(string key, int statusId = 2)
        {
            Session["filterKey"] = key;
            Session["filterStatusId"] = statusId;
            try
            {
                using (var business = new UsersBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveUsers(null, key, partnerId: ECOsystem.Utilities.Session.GetPartnerId(), UserStatus: statusId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "PartnerUsers")
                });
            }
        }

        /// <summary>
        /// Load Users
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadUsers(int id)
        {
            Session["EditUserId"] = id;
            if (id < 0)
            {
                var user = new ECOsystem.Models.Core.Users { PartnerUser = new ECOsystem.Models.Core.PartnerUsers(), IsPartnerUser = true };
                using (var business = new UsersBusiness())
                {
                    user.PartnerUser.CountriesAccessList = business.RetrievePartnerUsersByCountry();
                }
                return PartialView("_partials/_Detail", user);
            }
            try
            {
                using (var business = new UsersBusiness())
                {
                    var model = business.RetrieveUsers(id, partnerId: ECOsystem.Utilities.Session.GetPartnerId(), bindObtPhoto: true, LoadSingleUser: true).FirstOrDefault();
                    model.List = CustomerByPartnerBusiness.CustomersByPartner_Retrieve(id);
                    return PartialView("_partials/_Detail", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "PartnerUsers")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model 
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteUsers(int id)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    business.DeleteUsers(id);

                    #region Event log

                    var user = ECOsystem.Utilities.Session.GetUserInfo();

                    //Add log event
                    AddLogEvent(
                            message: string.Format("[DELETE_USR] Cuenta de Usuario ha sido eliminada. Usuario afectado es UserId = {0}", id),
                            userId: user != null ? user.UserId : 0,
                            customerId: user != null ? user.CustomerId : 0,
                            partnerId: user != null ? user.PartnerId : 0,
                            exception: null,
                            isInfo: true);

                    #endregion

                    return PartialView("_partials/_Grid", business.RetrieveUsers(null, partnerId: ECOsystem.Utilities.Session.GetPartnerId()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "PartnerUsers")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "PartnerUsers")
                    });
                }
            }
        }

        /// <summary>
        /// Use to fill the dropdown menu with the options to select in the 
        /// Authorization modal for the partner User
        /// </summary>
        /// <returns></returns>
        public static SelectList GetSelecctionList(int? typeId)
        {
            try
            {
                Dictionary<string, string> dictionary;
                if (typeId == 3)
                {
                    dictionary = new Dictionary<string, string> { { "1", "Cliente" } };
                }
                else
                {
                    dictionary = new Dictionary<string, string> { { "0", "Socio" }, { "1", "Cliente" } };
                }

                return new SelectList(dictionary, "key", "value");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }


        /// <summary>
        /// Roles for Permissions
        /// </summary>     
        /// <returns></returns>
        public static SelectList RetrievePartnersbyPartners()
        {
            try
            {
                var dictionary = new Dictionary<int, string>();
                using (var bus = new PartnersBusiness())
                {
                    var partner = bus.RetrievePartners(null);
                    if (partner == null) throw new ArgumentNullException("Partner");

                    foreach (var p in partner)
                        dictionary.Add(Convert.ToInt32(p.PartnerId), p.Name);


                    return new SelectList(dictionary, "key", "value");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        /// <summary>
        /// Roles for Permissions
        /// </summary>     
        /// <returns></returns>
        public static SelectList RetrievePartnersbyPartners(int? partnerId, int? typeId)
        {
            try
            {
                var dictionary = new Dictionary<int, string>();
                using (var bus = new PartnersBusiness())
                {
                    var partner = bus.RetrievePartners(null);
                    if (partner == null) throw new ArgumentNullException("Partner");

                    if (typeId == 3)
                    {
                        var validPartner = partner.Where(x => x.PartnerId == partnerId).FirstOrDefault();
                        if (validPartner == null) throw new ArgumentNullException("Partner");
                        dictionary.Add(Convert.ToInt32(validPartner.PartnerId), validPartner.Name);
                    }
                    else
                    {
                        foreach (var p in partner)
                            dictionary.Add(Convert.ToInt32(p.PartnerId), p.Name);
                    }

                    return new SelectList(dictionary, "key", "value");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }


        /// <summary>
        /// GetTreeData
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="selectType"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetTreeData(int userId, int selectType, int PartnerId)
        {
            try
            {
                var resultList = new List<object>();

                switch (selectType)
                {
                    case 0:

                        using (var bus = new PartnersBusiness())
                        {
                            var model = bus.RetrievePartners(null).Select(x => new { x.PartnerId, x.Name, x.PartnerTypeId }).OrderBy(x => x.Name);
                            var pChecked = CustomerByPartnerBusiness.PartnerAuthByPartnerRetrieve(userId).Select(x => new { x.PartnerId }).ToList();

                            foreach (var item in model)
                            {
                                if (pChecked.FirstOrDefault(x => x.PartnerId == item.PartnerId) != null)
                                    resultList.Add(new { text = item.Name, id = item.PartnerId, state = new { @checked = true } });
                                else
                                    resultList.Add(new { text = item.Name, id = item.PartnerId });
                            }
                        }
                        break;
                    case 1:
                        using (var bus = new CustomersBusiness())
                        {
                            var pId = PartnerId;
                            var model = bus.RetrieveCustomersForTreeView(pId).Where(x => x.IsActive == true).Select(x => new { x.CustomerId, x.DecryptedName }).OrderBy(x => x.DecryptedName);
                            var cChecked = CustomerByPartnerBusiness.CustomersByPartner_Retrieve(userId).Select(x => new { x.CustomerId }).ToList();

                            foreach (var item in model)
                            {
                                if (cChecked.FirstOrDefault(x => x.CustomerId == item.CustomerId) != null)
                                    resultList.Add(new { text = item.DecryptedName, id = item.CustomerId, state = new { @checked = true } });
                                else
                                    resultList.Add(new { text = item.DecryptedName, id = item.CustomerId });
                            }
                        }
                        break;
                    case 2:
                        var info = ECOsystem.Utilities.Session.GetUserInfo();
                        var mlist = Business.ServiceStationsBusiness.ServiceStationRetrieve(null, info.CountryId, info.PartnerId, null).ToList();
                        var sChecked = CustomerByPartnerBusiness.ServiceStationsByPartner_Retrieve(userId).Select(x => new { x.ServiceStationId }).ToList();
                        foreach (var item in mlist)
                        {
                            if (sChecked.FirstOrDefault(x => x.ServiceStationId == item.ServiceStationId) != null)
                                resultList.Add(new { text = item.Name, id = item.ServiceStationId, state = new { @checked = true } });
                            else
                                resultList.Add(new { text = item.Name, id = item.ServiceStationId });
                        }
                        break;
                }

                return Json(resultList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Save the information from the TreeView
        /// </summary>
        /// <param name="list"></param>
        /// <param name="userId"></param>
        /// <param name="roleId"></param>
        /// <param name="selectType"></param>
        /// <returns></returns>
        public ActionResult SaveTreeViewData(List<int> list, int userId, string roleId, int selectType, int PartnerId)
        {
            try
            {
                switch (selectType)
                {
                    case 0:
                        using (var bus = new CustomerByPartnerBusiness())
                        {
                            bus.PartnersByPartner_AddOrEdit(list, userId, roleId);
                            new EventLogBusiness().AddLogEvent(LogState.INFO, "Almacenamiento de Socio para un Socio");
                        }
                        break;
                    case 1:
                        using (var bus = new CustomerByPartnerBusiness())
                        {
                            bus.CustomersByPartner_AddOrEdit(list, userId, roleId, PartnerId);
                            new EventLogBusiness().AddLogEvent(LogState.INFO, "Almacenamiento de Clientes para un Socio");
                        }
                        break;
                    case 2:

                        using (var bus = new CustomerByPartnerBusiness())
                        {

                            bus.ServiceStationsByPartner_AddOrEdit(list, userId, PartnerId);
                            new EventLogBusiness().AddLogEvent(LogState.INFO, "Almacenamiento de Estaciones de Servicio para un Socio de Estaciones");
                        }
                        break;
                }
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult IsValidEmail(string email)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    ECOsystem.Models.Core.Users model = new ECOsystem.Models.Core.Users();
                    model.DecryptedEmail = email;

                    var result = business.IsValidEmail(model, UserManager);

                    if (result == 0)
                        Response.StatusCode = 400;

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        /// <param name="exception"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = exception != null ? string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace) : message;

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = ControllerContext.RouteData.Values["controller"].ToString(),
                Action = ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion

    }
}
