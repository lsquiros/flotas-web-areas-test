﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECO_Partner.Business;
using ECO_Partner.Models;
using ECO_Partner.Utilities;
using ECOsystem.Audit;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;



namespace ECO_Partner.Controllers
{
    public class CollectionZonesController : Controller
    {
        // GET: CollectionZones
        public ActionResult Index()
        {
            try
            {
                using (var business = new CollectionZonesBusiness())
                {
                    var x = new CollectionZonesModels
                    {
                        Data = new TypeZone(),
                        List = business.RetrieveZones(null),
                        Menus = new List<AccountMenus>()
                    };
                    Session["CollectionZones"] = x.List;
                    return View(x);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AddOrEditZones(int? ZoneId, string Name, string Description)
        {
            try
            {
                using (var business = new CollectionZonesBusiness())
                {
                    business.AddOrEditZone(ZoneId, Name, Description);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, String.Format("Agrega un nueva zona o Modifica una nueva Zona. Id:{0}, Nombre:{1},Descripción:{2}", ZoneId, Name, Description));
                    var list = business.RetrieveZones(null);
                    Session["CollectionZones"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index", new TypeZone());
            }
        }
        public ActionResult CollectionZoneLoad(int? Id)
        {
            try
            {
                if (Id == null)
                {
                    return PartialView("_partials/_DetailZone", new TypeZone());
                }
                var list = (List<TypeZone>)Session["CollectionZones"];
                return PartialView("_partials/_DetailZone", list.Where(x => x.ZoneId == Id).FirstOrDefault());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index", new TypeZone());
            }
        }
        public ActionResult CollectionZoneSearch(string key)
        {
            try
            {
                var list = (List<TypeZone>)Session["CollectionZones"];
                return PartialView("_partials/_Grid", list.Where(x => x.Name.ToLower().Contains(key.ToLower())).ToList());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index", new TypeZone());
            }
        }
        public ActionResult CollectionZoneDelete(int Id)
        {
            try
            {
                using (var business = new CollectionZonesBusiness())
                {
                    business.DeleteZone(Id);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Elimina la Zonas");
                    var list = business.RetrieveZones(null);
                    Session["CollectionZones"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "La Zona se encuentra asociado a un Cliente, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "CollectionZones")
                    });
                }
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index", new TypeZone());
            }
        }
    }
}
