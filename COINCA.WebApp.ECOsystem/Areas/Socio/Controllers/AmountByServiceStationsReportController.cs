﻿using ECO_Partner.Business;
using ECO_Partner.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Partner.Controllers.Partner
{
    public class AmountByServiceStationsReportController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                return View(new AmountByServiceStationsReportBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DownloadReport(string EndDate, bool HasTerminals)
        {
            try
            {
                using (var bus = new AmountByServiceStationsReportBusiness())
                {
                    var dt = bus.GetReportDataTable(EndDate, HasTerminals);

                    if (dt.Rows.Count == 0)
                    {
                        return Json("No Data");
                    }
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Generar el reporte de montos por estaciones de servicio. Parámetros: Socio: {0}, FechaFinal: {1}, Terminales: {2}.", ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId, EndDate, HasTerminals));
                    Session["Report"] = dt;
                    Session["HasTerminals"] = HasTerminals;
                    return Json("Success");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al generar el reporte de montos por estaciones de servicio. Error: {0}", e.Message));
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Report"] != null)
                {
                    report = (DataTable)Session["Report"];
                    Session["Report"] = null;
                }

                bool hasterminals = Session["HasTerminals"] != null ? (bool)Session["HasTerminals"] : false;

                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte montos por estaciones de servicio");
                    if (hasterminals)
                    {
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "AmountByServiceStationsReportTerminals", this.ToString().Split('.')[2], "Reporte de montos por Estación de Servicio");
                    }
                    else
                    {
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "AmountByServiceStationsReport", this.ToString().Split('.')[2], "Reporte de montos por Estación de Servicio");
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al descargar el reporte de montos por estaciones de servicio. Error: {0}", e.Message));
                ViewBag.ErrorReport = 1;
                return View("Index", new AmountByServiceStationsReportBase());
            }
        }
    }
}