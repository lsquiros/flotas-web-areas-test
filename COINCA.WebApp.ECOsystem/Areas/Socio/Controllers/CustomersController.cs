﻿/************************************************************************************************************
/************************************************************************************************************
*  File    : CustomersController.cs
*  Summary : Customers Controller Actions
*  Author  : Berman Romero
*  Date    : 09/15/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using Microsoft.AspNet.Identity.Owin;
using ECOsystem.Models.Core;
using ECOsystem.Business.Core;
using System.Web.Script.Serialization;
using ECOsystem;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using ECOsystem.Models.Account;
using ECO_Partner.Business;
using ECO_Partner.Utilities;
using Newtonsoft.Json;
using System.Data;
using System.IO;
using ECOsystem.Models;
using ECOsystem.Utilities;
using ECOsystem.ServiceOrderUtilities.Models;
using ECO_Partner.Models;




namespace ECO_Partner.Controllers.Partner
{
    public class CustomersController : Controller
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? (_userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());
            }
        }

        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                var productType = ECOsystem.Utilities.Session.GetProductInfoByPartner((int)ECOsystem.Utilities.Session.GetPartnerId());

                int currentPage = 0;

                Int32.TryParse(((Request.QueryString.Count > 0) ? Request.QueryString[0] : "0"), out currentPage);

                int? gridPager = currentPage;

                if (productType == ProductInfo.BACFlota && gridPager < 2)
                {
                    return View(IndexRetrieve());
                }
                else
                {
                    using (var business = new CustomersBusiness())
                    {
                        var currentCustomerStatus = (Session["CustomersStatus"] != null) ? Convert.ToInt32(Session["CustomersStatus"].ToString()) : 1;
                        var currentCustomersFilter = (Session["CustomersFilter"] != null) ? Convert.ToInt32(Session["CustomersFilter"].ToString()) : 1;
                        var currentPageSize = (gridPager != null && gridPager > 0) ? ((Session["CurrentTotalCustomers"] != null) ? Convert.ToInt32(Session["CurrentTotalCustomers"].ToString()) : 0) : 0;
                        var currentSearchInput = (gridPager != null && gridPager > 0) ? ((Session["CurrentSearchByNameOrEmail"] != null) ? Session["CurrentSearchByNameOrEmail"].ToString() : string.Empty) : string.Empty;

                        var model = new CustomersBase
                        {
                            Data = new Customers(),
                            List = business.SearchCustomersFromPartner(null, currentPageSize, currentSearchInput, statusId: currentCustomerStatus, filterType: currentCustomersFilter),
                            Menus = new List<AccountMenus>()
                        };

                        var customerIni = model.List.FirstOrDefault();
                        var useAllItems = (gridPager != null && gridPager > 0) ? true : false;

                        Session["PartnerList"] = model.List;
                        Session["CurrentTotalCustomers"] = customerIni != null ? customerIni.TotalRows : 0;

                        ViewBag.PageNum = (gridPager != null && gridPager > 0) ? gridPager : 1;
                        Session["PageNum"] = (gridPager != null && gridPager > 0) ? gridPager : 1;
                        ViewBag.AjaxGrid = true;
                        ViewBag.FormName = "searchForm";

                        if (useAllItems)
                            ViewBag.AjaxGrid = null; Session["AjaxGrid"] = null;

                        GeneratePager(((customerIni != null) ? (
                                        (productType == ProductInfo.LineVitaECO) ?
                                            ((useAllItems) ? model.List.Count() : customerIni.TotalRows)
                                        :
                                            ((useAllItems) ? customerIni.TotalRows : customerIni.TotalRows)

                                    ) : 0), 10, 20, useAllItems);

                        return View(model);
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);


                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TitleError = "Error no controlado",
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public ActionResult AddCustomers(CustomersEditBase model)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    var productType = ECOsystem.Utilities.Session.GetProductInfoByPartner((int)ECOsystem.Utilities.Session.GetPartnerId());
                    model.Data.IsActive = model.Data.CustomerId == null ? true : model.Data.IsActive;
                    business.AddOrEditCustomers(model.Data, UserManager, Url, Request);

                    if (model.Data.CustomerId != null)
                    {
                        if (productType == ProductInfo.BACFlota)
                        {
                            //Customer Budget
                            model.CustomerBudget.NameDay = model.CustomerBudget.Periodicity == 7 ? model.CustomerBudget.NameDay : model.CustomerBudget.NameDate;
                            business.AddOrEditBudgetClosingPeriodicity(model.CustomerBudget);

                            //Prices
                            model.Data.ManagementPricesXml = Session["ManagementAgentPrices"] != null ? ECOsystem.Utilities.Miscellaneous.GetXML((List<CustomerPricesModel>)Session["ManagementAgentPrices"], typeof(CustomerPricesModel).Name) : string.Empty;
                            model.Data.SMSPricesXml = Session["SMSPrices"] != null ? ECOsystem.Utilities.Miscellaneous.GetXML((List<CustomerPricesModel>)Session["SMSPrices"], typeof(CustomerPricesModel).Name) : string.Empty;
                            business.EditCustomersPrices(model.Data);

                            //Branch Customers
                            business.BranchByCustomerAddOrEdit(model.Data.CustomerId, model.BranchCustomerList.ToList());
                        }

                        //Contract 
                        if (model.ContractInformation != null) business.CustomerContractAddOrEdit(model.ContractInformation);
                    }

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model)));

                    return Json(string.Format("Success|{0}", model.Data.CustomerId), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (!string.IsNullOrEmpty(e.Message) && e.Message.Contains("VALIDATE_USER_FROM_PARTNER"))
                {
                    var uriToRedirect = e.Message.Split('|');
                    var strProduct = string.Empty;

                    if (uriToRedirect.Count() > 1)
                    {
                        using (var business = new CustomersBusiness())
                        {
                            var customerIdReference = 0;
                            Int32.TryParse(uriToRedirect[1], out customerIdReference);
                            var currentCustomer = business.RetrieveCustomers(customerIdReference, LoadSingleCustomer: true, queryWithOutPartner: true).FirstOrDefault();
                            var productType = ECOsystem.Utilities.Session.GetProductInfoByPartner(((currentCustomer != null) ? currentCustomer.PartnerId : 0));
                            strProduct = (productType == ProductInfo.LineVitaECO) ? "ControlCar" : "Flotas";
                        }
                    }

                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessageWithDismiss.cshtml", new CustomError()
                    {
                        TitleError = "Validación de usuario",
                        TechnicalError = string.Format("Ya existe un Usuario en plataforma ligado al producto {0} con el mismo correo, favor utilizar otro correo que no esté en uso.", strProduct),
                        RedirectUrl = ((uriToRedirect.Count() > 1) ? "VALIDATE_USER_FROM_PARTNER -" + uriToRedirect[1] : string.Empty),
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
            }
        }

        /// <summary>
        /// Search Customers
        /// </summary>
        /// <param name="pageNum"></param>
        /// <param name="pageSize"></param>
        /// <param name="key"></param>
        /// <param name="statusId"></param>
        /// <param name="filterType"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult SearchCustomers(int? pageNum, int? pageSize, string key = null, int statusId = 1, int filterType = 0)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    // Si el filtro es por nombre de cliente o correo se solicita obtener todos los clientes, 
                    // debido a que la busqueda se realiza por código (debido a la encriptación)
                    if (filterType == 1 || filterType == 2)
                    {
                        pageSize = (Session["CurrentTotalCustomers"] != null) ? Convert.ToInt32(Session["CurrentTotalCustomers"].ToString()) : pageSize;
                        pageNum = 1;
                    }

                    IEnumerable<Customers> cms = business.SearchCustomersFromPartner(pageNum, pageSize, key, statusId: statusId, filterType: filterType);
                    var customerIni = cms.FirstOrDefault();
                    bool useAllItems = false;

                    //Restauramos la cantidad total de registros para cuando la busqueda se valida desde codigo
                    if (filterType == 1 || filterType == 2)
                    {
                        useAllItems = true;
                        Session["CurrentTotalCustomers"] = (customerIni != null) ? customerIni.TotalRows : pageSize;
                        Session["CurrentSearchByNameOrEmail"] = (!string.IsNullOrEmpty(key)) ? key : string.Empty;
                    }

                    Session["CustomersStatus"] = statusId;
                    Session["CustomersFilter"] = filterType;
                    Session["PartnerList"] = cms;

                    ViewBag.PageNum = pageNum != null ? (int)pageNum : 1;
                    Session["PageNum"] = pageNum;
                    ViewBag.AjaxGrid = true;
                    ViewBag.FormName = "searchForm";

                    if (useAllItems)
                        ViewBag.AjaxGrid = null; Session["AjaxGrid"] = null;

                    GeneratePager(((customerIni != null) ? ((useAllItems) ? cms.Count() : customerIni.TotalRows) : 0), 10, 20, useAllItems);

                    return PartialView("_partials/_Grid", cms);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpGet]
        [EcoAuthorize]
        public ActionResult LoadCustomers(int? id)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    var model = business.RetrieveCustomersEdit(id);
                    Session["VehicleList"] = model.Vehicles;
                    using (var bus = new APITokenBusiness())
                    {
                        var token = bus.RetrieveAPIToken(id);
                        ViewBag.CustomerToken = token;
                        model.ContractsInformation.ForEach(m => m.APIToken = token);
                    }

                    #region ContractInfo                    
                    model.ContractInformation.Vehicles = model.Vehicles;
                    Session["ContractList"] = model.ContractsInformation;
                    #endregion

                    #region GetPricesData                
                    Session["ManagementAgentPrices"] = model.Data.ManagmentPricesList;
                    Session["SMSPrices"] = model.Data.SMSPricesList;
                    Session["ManagementPriceId"] = model.Data.ManagementPriceId;
                    Session["SMSPriceId"] = model.Data.SMSPriceId;
                    #endregion
                    return View("Admin", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadUsers(int? id, int? userid)
        {
            try
            {
                var user = new Users { CustomerUser = new CustomerUsers(), IsCustomerUser = true };

                using (var business = new UsersBusiness())
                {
                    if (id != null && userid == null)
                        user.CustomerId = id;

                    if (userid != null)
                        user = business.RetrieveUsers(userId: userid, bindObtPhoto: true).FirstOrDefault();

                    return PartialView("_partials/_DetailUsers", user);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        /// <summary>
        /// Add Or Edit Users
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditUsers(Users model)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    business.AddOrEditUsers(model, UserManager, Url, Request);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model)));
                    return PartialView("_partials/_GridUsers", business.RetrieveUsers(null, customerId: model.CustomerId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
                if (e.Message.Contains("ERROR EXIST_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Usuario ya existe",
                        TechnicalError = "El usuario con el correo especificado ya existe en el sistema.",
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
                if (e.Message.Contains("ERROR UPDT_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Error de Actualización",
                        TechnicalError = "El usuario que se desea actualizar presenta inconsistencias, por favor intente de nuevo el proceso de actualización.",
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        /// <summary>
        /// Delete Users
        /// </summary>
        /// <param name="id"></param>
        /// <param name="custid"></param>
        /// <returns></returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteUsers(int id, int custid)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    business.DeleteUsers(id);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DELETE_USR] Cuenta de Usuario ha sido eliminada. Usuario afectado es UserId = {0}", id));
                    return PartialView("_partials/_GridUsers", business.RetrieveUsers(null, customerId: custid, isList: true));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index")
                });
            }
        }


        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadCustomerCard(int custId, int id, bool international)
        {
            try
            {
                if (id < 0)
                {
                    using (var business = new CustomersBusiness())
                    {
                        var customer = business.RetrieveCustomers(custId).FirstOrDefault();
                        return PartialView("_partials/_DetailCard", new CustomerCreditCards()
                        {
                            CurrencyName = customer.CurrencyName,
                            CurrencySymbol = customer.CurrencySymbolDollar,
                            InternationalCard = international
                        });
                    }
                }
                using (var business = new CustomersBusiness())
                {
                    var model = business.RetrieveCustomerCreditCards(custId, id).FirstOrDefault();
                    model.InternationalCard = international;
                    return PartialView("_partials/_DetailCard", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpPost]
        public JsonResult LoadPreviousCustomerCardRequest(int custId, int id)
        {
            try
            {
                if (id != -100)
                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = false }
                    };
                using (var business = new CustomersBusiness())
                {
                    var tmp = business.RetrieveCustomerCardRequest(custId, null).FirstOrDefault();
                    if (tmp == null)
                    {
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { result = false }
                        };
                    }
                    var cardRequest = new CardRequest
                    {
                        StateId = tmp.StateId,
                        CountyId = tmp.CountyId,
                        CityId = tmp.CityId,
                        AddressLine1 = tmp.AddressLine1,
                        AddressLine2 = tmp.AddressLine2,
                        AuthorizedPerson = tmp.AuthorizedPerson,
                        ContactPhone = tmp.ContactPhone,
                        EstimatedDeliveryStr = tmp.EstimatedDeliveryStr
                    };
                    using (var gdBusiness = new GeographicDivisionBusiness())
                    {
                        var listCounties = gdBusiness.RetrieveCountiesJSON(null, tmp.StateId, null);
                        var listCities = gdBusiness.RetrieveCitiesJSON(null, tmp.CountyId, null, null);
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { result = true, cardRequest, listCounties, listCities }
                        };
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadCounties(int? stateId)
        {
            try
            {
                if (stateId == null)
                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = false }
                    };

                using (var business = new GeographicDivisionBusiness())
                {
                    var listCounties = business.RetrieveCountiesJSON(null, stateId, null);
                    if (listCounties == null)
                    {
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { result = false }
                        };
                    }

                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = true, listCounties }
                    };
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadCities(int? countyId)
        {
            try
            {
                if (countyId == null)
                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = false }
                    };

                using (var business = new GeographicDivisionBusiness())
                {
                    var listCities = business.RetrieveCitiesJSON(null, countyId, null, null);
                    if (listCities == null)
                    {
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { result = false }
                        };
                    }

                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = true, listCities }
                    };
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadCustomerCardRequest(int custId, int id)
        {
            try
            {
                if (id < 0)
                {
                    using (var business = new CustomersBusiness())
                    {
                        var customer = business.RetrieveCustomers(custId).FirstOrDefault();
                        if (customer == null)
                        {
                            return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                            {
                                TechnicalError = "Missing Customer Id",
                                ReturnUlr = Url.Action("Index", "Customers")
                            });
                        }
                        return PartialView("_partials/_DetailCardRequest", new CardRequest()
                        {
                            IssueForId = customer.IssueForId,
                            CountryId = customer.CountryId
                        });
                    }

                }
                using (var business = new CustomersBusiness())
                {
                    return PartialView("_partials/_DetailCardRequest", business.RetrieveCustomerCardRequest(custId, id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpPost]
        public PartialViewResult LoadBinnacle(int CustomerBinnacleId, int? BinnacleLoadId)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    var list = business.AddOrRetrieveCustomerBinnacle(new CustomerBinnacle() { CustomerId = (int)CustomerBinnacleId });
                    return PartialView("_partials/_BinnacleGrid", list);
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public ActionResult AddorEditTerminal(CustomerTerminals model)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditCustomerTerminal(model);
                        String message = string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model));
                        new EventLogBusiness().AddLogEvent(LogState.INFO, message);
                    }
                    return PartialView("_partials/_GridTerminal", business.RetrieveCustomerTerminals(Convert.ToInt32(Session["CustomerIDTerminal"])));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                var message = e.Message;

                if (e.Message.Contains("TERMINAL_EXISTS"))
                {
                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = false }
                    };
                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadUploadRequestModal()
        {
            try
            {

                return PartialView("_partials/_UploadRequest", new CardRequestCsv() { AddressLine1 = "ssss" });

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpPost]
        public PartialViewResult AddOrEditCard(CustomerCreditCards model)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    business.AddOrEditCustomerCreditCards(model);
                    String message = string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, message);
                    var result = business.RetrieveCustomerCreditCards(model.CustomerId, null).ToList();
                    if (ECOsystem.Utilities.Session.GetPartnerInfo().HasPayments && result.Count() > 0)
                    {
                        result.Where(x => !x.InternationalCard).FirstOrDefault().Payments = business.RetrieveCustomerPayments(new Filters(), model.CustomerId).ToList();
                    }
                    return PartialView("_partials/_GridCards", result);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        public ActionResult CancelCustomerAddOrEdit(int? customerId)
        {
            using (var business = new CustomersBusiness())
            {
                var result = new List<CustomerCreditCards>();
                if (!customerId.IsNull())
                {
                    result = business.RetrieveCustomerCreditCards(customerId, null).ToList();
                    if (ECOsystem.Utilities.Session.GetPartnerInfo().HasPayments && result.Count() > 0)
                    {
                        result.Where(x => !x.InternationalCard).FirstOrDefault().Payments = business.RetrieveCustomerPayments(new Filters(), customerId).ToList();
                    }
                    if (result.Count() == 0)
                    {
                        result = new List<CustomerCreditCards>();
                    }
                }
                return PartialView("_partials/_GridCards", result);
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditCardRequest(CardRequest model)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    model.PaymentReference = model.PaymentReference ?? string.Empty;

                    business.AddOrEditCardRequest(model);
                    String message = string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, message);
                    return PartialView("_partials/_GridCardRequest", business.RetrieveCustomerCardRequest(model.CustomerId, null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message.Equals("El número de placa seleccionado ya posee una tarjeta asociada") ? e.Message : (e.Message + e.StackTrace),
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpPost]
        public PartialViewResult AjaxCancelEditCustomers(int id)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    return PartialView("_partials/_DetailInfoRO", business.RetrieveCustomers(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteCustomers(int id) // , int CountryId)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    business.DeleteCustomers(id);
                    String message = string.Format("[Delete] CustomerId: {0}", id);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, message);
                    return PartialView("_partials/_Grid", business.RetrieveCustomers(null));
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteCustomerInfo(int id, int custId, string entity)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    String message;
                    switch (entity)
                    {
                        case "CARD_REQUEST":
                            business.DeleteCustomerCardRequest(id);
                            message = string.Format("[Delete] CardRequestId: {0}", id);
                            new EventLogBusiness().AddLogEvent(LogState.INFO, message);
                            return PartialView("_partials/_GridCardRequest", business.RetrieveCustomerCardRequest(custId, null));
                        case "CUSTOMER_CARD":
                            business.DeleteCustomerCard(id);
                            message = string.Format("[Delete] CreditCardId: {0}", id);
                            new EventLogBusiness().AddLogEvent(LogState.INFO, message);
                            return PartialView("_partials/_GridCards", business.RetrieveCustomerCreditCards(custId, null));
                    }
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = "Invalid parameters to perform delete operation!",
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteCustomerTerminal(string id)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    business.DeleteCustomerTerminal(id);
                    String message = string.Format("[Delete] TerminalId:", new JavaScriptSerializer().Serialize(id));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, message);
                    return PartialView("_partials/_GridTerminal", business.RetrieveCustomerTerminals(Convert.ToInt32(Session["CustomerIDTerminal"])));
                }

            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [System.Web.Http.HttpPost]
        public PartialViewResult ProcessCsvFile(int custId, IList<CardRequestCsv> list)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    foreach (var item in list)
                    {
                        business.AddOrEditCardRequest(new CardRequest(item, custId));
                        String message = string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(item));
                        new EventLogBusiness().AddLogEvent(LogState.INFO, message);
                    }
                    return PartialView("_partials/_GridCardRequest", business.RetrieveCustomerCardRequest(custId, null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = "Processing csv file failed!" + e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        [HttpPost]
        public ActionResult GetTreeData(int userId)
        {
            try
            {
                var resultList = new List<object>();

                using (var bus = new CustomersBusiness())
                {
                    var pId = bus.GetPartnerIdByUserId(userId);
                    var model = bus.RetrieveCustomersForTreeView(pId).Select(x => new { x.CustomerId, x.DecryptedName });
                    var cChecked = bus.CustomerByCustomerRetrieve(userId).Select(x => new { x.CustomerId }).ToList();

                    foreach (var item in model)
                    {
                        if (cChecked.FirstOrDefault(x => x.CustomerId == item.CustomerId) != null)
                            resultList.Add(new { text = item.DecryptedName, id = item.CustomerId, state = new { @checked = true } });
                        else
                            resultList.Add(new { text = item.DecryptedName, id = item.CustomerId });
                    }

                    return Json(resultList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetTreeDataBranchCustomers(int CustomerId)
        {
            try
            {
                var resultList = new List<object>();

                using (var bus = new CustomersBusiness())
                {
                    var PartnerId = ECOsystem.Utilities.Session.GetPartnerId();
                    var model = bus.RetrieveCustomersForTreeView(PartnerId).Select(x => new { x.CustomerId, x.DecryptedName }).Where(x => x.CustomerId != CustomerId).OrderBy(x => x.DecryptedName);
                    var cChecked = bus.BranchByCustomerRetrieve(CustomerId).Select(x => new { x.CustomerId }).ToList();

                    foreach (var item in model)
                    {
                        if (cChecked.FirstOrDefault(x => x.CustomerId == item.CustomerId) != null)
                            resultList.Add(new { text = item.DecryptedName, id = item.CustomerId, state = new { @checked = true } });
                        else
                            resultList.Add(new { text = item.DecryptedName, id = item.CustomerId });
                    }

                    return Json(resultList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveTreeViewData(List<int> list, int userId, string roleId)
        {
            try
            {
                using (var bus = new CustomersBusiness())
                {
                    bus.CustomerByCustomerAddOrEdit(list, userId, roleId);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Almacenamiento de Clientes para un Cliente");

                    return Json(1, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(0, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetRoleForCustomerSelected(int userId, int? selectType)
        {
            try
            {
                using (var bus = new CustomersBusiness())
                {
                    var roleId = bus.GetRoleForCustomerByCustomer(userId, selectType) ?? "";
                    return Json(roleId, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            try
            {
                var report = GetReportDownloadData();
                Session["Reporte"] = report;
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Get Report Download Data
        /// </summary>
        /// <returns></returns>
        public DataTable GetReportDownloadData()
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    using (var dt = new DataTable())
                    {
                        dt.TableName = "CustomerReport";
                        dt.Columns.Add("Name");
                        dt.Columns.Add("Currency");
                        dt.Columns.Add("DateCreated");
                        dt.Columns.Add("IsActive");
                        dt.Columns.Add("Vehicles");
                        dt.Columns.Add("Cards");
                        dt.Columns.Add("GPS");
                        dt.Columns.Add("ContactName");
                        dt.Columns.Add("ContactEmail");
                        dt.Columns.Add("FinancialContactName");
                        dt.Columns.Add("FinancialContactEmail");

                        var CustomerList = business.RetrieveCustomers(null);

                        foreach (var item in CustomerList)
                        {
                            DataRow dr = dt.NewRow();

                            dr["Name"] = item.DecryptedName;
                            dr["Currency"] = item.CurrencyName;
                            dr["DateCreated"] = item.InsertDateStr;
                            dr["IsActive"] = ((item.IsActive) ? "SI" : "NO");
                            dr["Vehicles"] = item.VehicleCant;
                            dr["Cards"] = item.CreditCardCant;
                            dr["GPS"] = item.GPSCant;
                            dr["ContactName"] = item.ContactName;
                            dr["ContactEmail"] = item.Email;
                            dr["FinancialContactName"] = item.FinancialContact;
                            dr["FinancialContactEmail"] = item.FinancialEmail;
                            dt.Rows.Add(dr);
                        }

                        return dt;
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        [HttpPost]
        public ActionResult DownloadReportFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ECOsystem.Utilities.ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte Clientes");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "CustomerReport", "Partner", "Reporte de Clientes");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                ViewBag.ErrorReport = 1;
                using (var business = new GPSReportBusiness())
                {
                    return RedirectToAction("Index");
                }
            }
        }

        [HttpPost]
        public ActionResult SearhVehicles(string key)
        {
            try
            {
                var list = (List<Vehicles>)Session["VehicleList"];

                return PartialView("_partials/_VehicleGrid", list.Where(x => (!string.IsNullOrEmpty(x.PlateId) && x.PlateId.ToLower().Contains(key.ToLower()))
                                                                            || (!string.IsNullOrEmpty(x.Name) && x.Name.ToLower().Contains(key.ToLower()))
                                                                            || (!string.IsNullOrEmpty(x.Chassis) && x.Chassis.ToLower().Contains(key.ToLower()))).ToList());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Search Contracts
        /// </summary>
        /// <param name="key">Plate</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchContracts(string key)
        {
            try
            {
                var list = (List<CustomerContractInformation>)Session["ContractList"];

                return PartialView("_partials/_ContractGrid", list.Where(x => (!string.IsNullOrEmpty(x.SelectedVehiclesStr) && x.SelectedVehiclesStr.ToLower().Contains(key.ToLower()))).ToList());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        #region ManagementPrices 
        [HttpPost]
        public ActionResult SavePricesForCustomers(CustomerPricesModel model, int priceId)
        {
            try
            {
                var list = new List<CustomerPricesModel>();

                if (priceId == (int)Session["ManagementPriceId"])
                {
                    list = Session["ManagementAgentPrices"] != null ? (List<CustomerPricesModel>)Session["ManagementAgentPrices"] : new List<CustomerPricesModel>();
                }
                else if (priceId == (int)Session["SMSPriceId"])
                {
                    list = Session["SMSPrices"] != null ? (List<CustomerPricesModel>)Session["SMSPrices"] : new List<CustomerPricesModel>();
                }

                if (model.Id == null)
                {
                    model.Id = list.Max(x => x.Id) != null ? (int)list.Max(x => x.Id) + 1 : 1;
                    list.Add(model);
                }
                else
                {
                    list.Where(x => x.Id == model.Id).ToList().ForEach(s => { s.From = model.From; s.To = model.To; s.Price = model.Price; });
                }
                //Validates 
                var r = ValidateRange(list.OrderBy(x => x.From).ToList(), model);
                if (!r.WithinRange)
                {
                    if (priceId == (int)Session["ManagementPriceId"])
                    {
                        Session["ManagementAgentPrices"] = list.OrderBy(x => x.From).ToList();
                    }
                    else if (priceId == (int)Session["SMSPriceId"])
                    {
                        Session["SMSPrices"] = list.OrderBy(x => x.From).ToList();
                    }
                }
                else
                {
                    list.RemoveAll(x => x.Id == model.Id);
                }

                return Json(new { partial = RenderPartialViewToString("_partials/_PricesGrid", new DynamicPartialModel() { list = list.OrderBy(x => x.From).ToList(), priceId = priceId }), withinRange = r.WithinRange, showwarning = r.NotInRange, maxfrom = r.NotInRange != null ? r.NotInRange : list.Max(x => x.To) + 1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeletePricesForCustomers(int id, int priceId)
        {
            try
            {
                var list = new List<CustomerPricesModel>();
                if (priceId == (int)Session["ManagementPriceId"])
                {
                    list = Session["ManagementAgentPrices"] != null ? (List<CustomerPricesModel>)Session["ManagementAgentPrices"] : new List<CustomerPricesModel>();
                }
                else if (priceId == (int)Session["SMSPriceId"])
                {
                    list = Session["SMSPrices"] != null ? (List<CustomerPricesModel>)Session["SMSPrices"] : new List<CustomerPricesModel>();
                }

                list.RemoveAll(x => x.Id == id);

                if (priceId == (int)Session["ManagementPriceId"])
                {
                    Session["ManagementAgentPrices"] = list;
                }
                else if (priceId == (int)Session["SMSPriceId"])
                {
                    Session["SMSPrices"] = list;
                }

                var r = ValidateRange(list.OrderBy(x => x.From).ToList(), null);

                return Json(new { partial = RenderPartialViewToString("_partials/_PricesGrid", new DynamicPartialModel() { list = list.OrderBy(x => x.From).ToList(), priceId = priceId }), maxfrom = r.NotInRange != null ? r.NotInRange : list.Count == 0 ? 1 : list.Max(x => x.To) + 1, showwarning = r.NotInRange }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        CustomerValidatePrices ValidateRange(List<CustomerPricesModel> list, CustomerPricesModel model)
        {
            try
            {
                var i = 1;
                var result = new CustomerValidatePrices();
                var nextVal = 0;

                foreach (var item in list)
                {
                    if (list.Skip(i).FirstOrDefault() != null)
                    {
                        if (item.To + 1 != list.Skip(i).FirstOrDefault().From)
                        {
                            result.NotInRange = item.To + 1;
                            nextVal = list.Skip(i).FirstOrDefault().From;
                        }
                        i++;
                    }
                }
                //Validates if the range is lower than one already in the list 
                if (model != null)
                {
                    list.RemoveAll(x => x.Id == model.Id); //remove the item already in the list for the analisis 
                    if (list.Count > 0 && !list.Select(x => x.From).ToList().Contains(model.From))
                    {
                        result.WithinRange = model.To > nextVal && model.To < result.NotInRange ? true : false;
                    }
                    list.Add(model);
                }
                return result;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return null;
            }
        }

        public string RenderPartialViewToString(string viewName, object model)
        {
            try
            {
                ViewData.Model = model;
                using (var sw = new StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }
        #endregion

        public ActionResult GetContractReportData(int CustomerReportId, int ContractReportId)
        {
            try
            {
                using (var bus = new CustomersBusiness())
                {
                    var data = bus.GetReportData(CustomerReportId, ContractReportId);
                    if (data != null && data.Rows.Count > 0)
                    {
                        Session["ReportData"] = data;
                        return Json("Success");
                    }
                    return Json("NoData");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DonwloadContractFile()
        {
            try
            {
                var report = new DataTable();
                if (Session["ReportData"] != null)
                {
                    report = (DataTable)Session["ReportData"];
                    Session["ReportData"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Facturación");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "CustomerContractReport", this.ToString().Split('.')[2], "Reporte de Historial de Contratos");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                ViewBag.ErrorReport = 1;
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Allow to cancel a contract usind the contract id and customer id.
        /// </summary>
        /// <param name="CustomerId">Customer id</param>
        /// <param name="ContractId">Contract id</param>
        /// <returns></returns>
        public ActionResult CustomerContractCancel(int CustomerId, int ContractId)
        {
            try
            {
                using (var bus = new CustomersBusiness())
                {
                    var ordersList = new List<OrdersModel>();

                    //Get Vehicles in Contract
                    var vehiclesInContract = bus.RetrieveContractByContractId(ContractId, CustomerId);

                    //Apply the cancelation in contracts
                    bus.CustomerContractCancel(CustomerId, ContractId);

                    //Process Orders of Uninstallation
                    foreach (var item in vehiclesInContract)
                    {
                        ordersList.AddRange(ECOsystem.ServiceOrderUtilities.Utilities.OrdersUtilities.SendOrder((int)ECOsystem.ServiceOrderUtilities.Models.OrdersEnum.Uninstall, CustomerId, item.VehicleId, true));
                    }

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Cancelación del contrato para el cliente {0}, detalle de ordenes de desinstalación: {1}", CustomerId, new JavaScriptSerializer().Serialize(ordersList)));

                    return PartialView("_partials/_ContractCancelOrderResult", ordersList);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TitleError = "Cancelación de Contrato",
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Customers")
                });
            }
        }

        //Index Information
        private CustomersBase IndexRetrieve()
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    var currentCustomerStatus = (Session["CustomersStatus"] != null) ? Convert.ToInt32(Session["CustomersStatus"].ToString()) : 1;

                    var model = new CustomersBase
                    {
                        Data = new Customers(),
                        List = business.RetrieveCustomers(null, statusId: currentCustomerStatus),
                        Menus = new List<AccountMenus>()
                    };
                    Session["PartnerList"] = model.List;
                    Session["CurrentTotalCustomers"] = (model != null && model.List != null) ? model.List.Count() : 0;
                    return model;
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        /// <summary>
        /// Load Vehicle Order
        /// </summary>
        /// <param name="VehicleId"></param>
        /// <returns></returns>
        public ActionResult LoadVehicleOrder(int VehicleId)
        {
            try
            {
                return PartialView("_partials/_OrderDetail", ECOsystem.ServiceOrderUtilities.Utilities.OrdersUtilities.GetOrderInformation(VehicleId));
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("NOT_EXIST"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessageWithDismiss.cshtml", new CustomError()
                    {
                        TitleError = "Validación de Orden de Trabajo",
                        TechnicalError = "No se puede seguir con el proceso, el Id del vehículo no está homologado en la plataforma Intrack",
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TitleError = "Cancelación de Contrato",
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
            }
        }

        /// <summary>
        /// Add Or Edit Vehicle Order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddOrEditVehicleOrder(VehicleOrder model)
        {
            try
            {
                var result = ECOsystem.ServiceOrderUtilities.Utilities.OrdersUtilities.SendOrder(model.OrderType, null, model.VehicleId, true).FirstOrDefault();
                if (string.IsNullOrEmpty(result.ErrorMessage))
                {
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                throw new Exception(new JavaScriptSerializer().Deserialize<VehicleOrderResult>(result.ErrorMessage).ResponseMessage);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                var message = e.Message;

                if (e.Message.Contains("WITH_GPS"))
                {
                    message = "No se puede seguir con el proceso, el vehículo procesado ya tiene un GPS instalado.";
                }
                else if (e.Message.Contains("ORDER_CODE"))
                {
                    message = "Error en el proceso creación, el código temporal para la creación de la orden no se ha generado correctamente.";
                }
                else if (e.Message.Contains("OPEN_ORDER"))
                {
                    message = "Ya existe una orden de trabajo abierta para el vehículo procesado.";
                }
                else if (e.Message.Contains("NOT_EXIST"))
                {
                    message = "No se puede seguir con el proceso, el Id del vehículo no está homologado en la plataforma Intrack.";
                }

                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get Vehicle By Client
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult GetVehicleByClient(int? vehicleId, int customerId)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    return Json(business.RetrieveVehicleByVehicleId(vehicleId, customerId), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Add Or Edit Vehicle By Client
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddOrEditVehicleByClient(Vehicles model)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    business.AddOrEditVehicleByCustomer(model);

                    // Get product type
                    var productType = ECOsystem.Utilities.Session.GetProductInfoByPartner((int)ECOsystem.Utilities.Session.GetPartnerId());

                    //Validate ProductType
                    if (productType == ProductInfo.LineVitaECO)
                    {
                        using (var businessUsers = new UsersBusiness())
                        {
                            businessUsers.AddVehicleReferencesToCustomerUsers((int)model.CustomerId);
                        }
                    }

                    var message = string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, message);

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Add Or Edit Contract By Client
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddOrEditContractByClient(CustomerContractInformation model)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    business.CustomerContractAddOrEdit(model);

                    // Get product type
                    var productType = ECOsystem.Utilities.Session.GetProductInfoByPartner((int)ECOsystem.Utilities.Session.GetPartnerId());

                    //Validate ProductType
                    if (productType == ProductInfo.LineVitaECO)
                    {
                        using (var businessUsers = new UsersBusiness())
                        {
                            businessUsers.AddVehicleReferencesToCustomerUsers((int)model.CustomerId);
                        }
                    }

                    var message = string.Format("[DataChanges] CustomerId: {0} | Data: {1}", model.CustomerId, new JavaScriptSerializer().Serialize(model));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, message);

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get Contract By Client
        /// </summary>
        /// <param name="contractId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult GetContractByClient(int? contractId, int customerId)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    return Json(business.RetrieveContractByContractId(contractId, customerId), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Add Customer Binnacle
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddOrEditCustomerBinnacle(CustomerBinnacle model)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    model.DecryptedUserName = ECOsystem.Utilities.Session.GetUserInfo().DecryptedEmail;

                    var result = business.AddOrRetrieveCustomerBinnacle(model);

                    var message = string.Format("[AddCustomerBinnacle]{0}", new JavaScriptSerializer().Serialize(model));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, message);

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Get Clients To Trasladate
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="currentCustomerId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetClientsToTrasladate(int vehicleId, int currentCustomerId)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    var totalClients = (Session["CurrentTotalCustomers"] != null) ? Convert.ToInt32(Session["CurrentTotalCustomers"].ToString()) : 1000;
                    var vehicle = business.RetrieveVehicleByVehicleId(vehicleId, currentCustomerId);
                    var clientList = business.SearchCustomersFromPartner(null, totalClients, null);

                    return Json(new
                    {
                        VehicleInfo = string.Format("{0} - {1}", vehicle.PlateId, vehicle.Name),
                        CustomerList = clientList.Select(s => new { CustomerId = s.CustomerId, CustomerName = s.DecryptedName }).ToList()
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(string.Format("Error |{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Trasladate Vehicle To Client
        /// </summary>
        /// <param name="currentVehicleId"></param>
        /// <param name="currentCustomerId"></param>
        /// <param name="newCustomerId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TrasladateVehicleToClient(int currentVehicleId, int currentCustomerId, int newCustomerId)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    business.VehicleTrasladatedBetweenCustomer(currentVehicleId, currentCustomerId, newCustomerId);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;

                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(string.Format("Error |{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get Partners To Trasladate
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="currentCustomerId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetPartnersToTrasladate(int customerId)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    var client = business.RetrieveCustomers(customerId).FirstOrDefault();
                    var partnerList = business.GetPartnersForTrasladateCustomers();

                    return Json(new
                    {
                        ClientInfo = string.Format("{0} - {1}", client.CustomerId, client.DecryptedName),
                        PartnerList = partnerList.Select(s => new { PartnerId = s.PartnerId, PartnerName = s.Name }).ToList()
                    }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(string.Format("Error |{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Trasladate Customer To Partner
        /// </summary>
        /// <param name="currentCustomerId"></param>
        /// <param name="currentPartnerId"></param>
        /// <param name="newPartnerId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TrasladateCustomerToPartner(int currentCustomerId, int newPartnerId)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    business.CustomerTrasladatedBetweenPartners(currentCustomerId, newPartnerId);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;

                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(string.Format("Error |{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Generador de paginación
        /// </summary>
        /// <param name="count"></param>
        void GeneratePager(int itemsCount, int maxDisplayedPages, int pageSize, bool useAllItems = false)
        {
            try
            {
                var pager = new GridMvc.Pagination.GridPager();
                pager.ItemsCount = itemsCount;
                pager.MaxDisplayedPages = maxDisplayedPages;
                pager.PageSize = pageSize;

                ViewBag.PageModel = (useAllItems) ? null : pager;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
            }
        }

        public ActionResult IsValidCardPlate(string plateCard)
        {
            try
            {
                using (var business = new CustomersBusiness())
                {
                    return Json(business.ValidatePlate_CreditCard(plateCard), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateAPIToken(int? customerId)
        {
            try
            {
                using (var bus = new APITokenBusiness())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Update token for customer: {0}", customerId));
                    return Json(bus.GenerateAPIToken(customerId), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SearchPayments(Filters Parameters)
        {
            try
            {
                using (var bus = new CustomersBusiness())
                {
                    return PartialView("_partials/_CustomerPayments", bus.RetrieveCustomerPayments(Parameters, null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);

            }

        }


        public ActionResult AddOrEditPayment(decimal Payment, int CustomerId)
        {
            try
            {
                using (var bus = new CustomersBusiness())
                {
                    bus.AddOrEditCustomerPayments(Payment, CustomerId);
                    return PartialView("_partials/_CustomerPayments", bus.RetrieveCustomerPayments(new Filters(), CustomerId));
                }
            }
            catch (Exception e)
            {

                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetFuelsMargin(int CustomerId) {
            List<ParametersByCustomer> fuelsMarginObject = null;
            try
            {
                using (var db = new ECOsystem.DataAccess.DataBaseAccess())
                {
                    fuelsMarginObject = db.ExecuteReader<ParametersByCustomer>("[General].[Sp_Retrieve_FuelsMargin]", new { CustomerId }).ToList();
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
            }

            return Json(fuelsMarginObject);
        }

        public JsonResult SaveRanges(CardRange model)
        {
            model.UserIdCreate = ECOsystem.Utilities.Session.GetUserInfo().UserId;
            model.UserIdUpdate = model.UserIdCreate;

            try
            {
                using (var db = new ECOsystem.DataAccess.DataBaseAccess())
                {
                    var x = db.ExecuteNonQuery("[General].[SP_SAVE_CARD_RANGE]", new {
                        RangeId = model.RangeId,
                        CustomerId = model.CustomerId,
                        UserId = model.UserIdCreate,
                        Quantity = model.Quantity,
                        Price = model.Price,
                        IsDeleted = model.IsDeleted
                    });
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
            }

            return Json($"Q:{model.Quantity}, P:{model.Price}, C:{model.CustomerId}, U:{model.UserIdCreate}");
        }

        public JsonResult LoadRanges(CardRange model)
        {
            //comentario de prueba Jorge
            List<CardRange> CardRangeList = null;
            try
            {
                using (var db = new ECOsystem.DataAccess.DataBaseAccess())
                {
                    CardRangeList = db.ExecuteReader<CardRange>("[General].[SP_RETRIEVE_CARD_RANGE]", 
                        new {
                            CustomerId = model.CustomerId
                        }).ToList();
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
            }

            return Json(CardRangeList);
        }
    }
}