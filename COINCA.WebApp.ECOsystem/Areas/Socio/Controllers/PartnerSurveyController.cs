﻿using ECO_Partner.Utilities;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ECO_Partner.Controllers
{
    public class PartnerSurveyController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var bus = new PartnerSurveyBusiness())
            {
                var list = bus.PartnerSurveyRetrieve(ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId);
                Session["QuestionTypes"] = bus.PartnerSurveyQuestionTypesRetrieve();
                Session["SurveyList"] = list;
                return View(list);
            }
        }

        #region Survey
        public ActionResult LoadPartnerSurvey(int? id)
        {
            var list = Session["SurveyList"] != null ? (List<PartnerSurvey>)Session["SurveyList"] : new List<PartnerSurvey>();
            var survey = id != null ? list.Where(x => x.Id == id).FirstOrDefault() : new PartnerSurvey();
            survey.PartnerId = (int)ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId;
            Session["Survey"] = survey;
            return PartialView("_partials/_Detail", survey);
        }

        public ActionResult AddOrEditPartnerSurvey(string obj, string order)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<PartnerSurvey>(obj);
                using (var bus = new PartnerSurveyBusiness())
                {
                    var survey = (PartnerSurvey)Session["Survey"];
                    survey.Name = model.Name;
                    survey.Description = model.Description;
                    survey.TargetId = model.TargetId;
                    survey.Active = model.Active;
                    survey.Mandatory = model.Mandatory;
                    ValidateQuestionOrder(survey, order);
                    bus.PartnerSurveyAddOrEdit(survey, ECOsystem.Utilities.Session.GetUserInfo().UserId);

                    var list = bus.PartnerSurveyRetrieve(ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId);
                    Session["SurveyList"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Mantenimiento de Formularios", TechnicalError = e.Message });
            }
        }

        public ActionResult DeletePartnerSurvey(int id)
        {
            try
            {
                using (var bus = new PartnerSurveyBusiness())
                {
                    bus.PartnerSurveyAddOrEdit(new PartnerSurvey() { Id = id, Delete = true }, ECOsystem.Utilities.Session.GetUserInfo().UserId);

                    var list = bus.PartnerSurveyRetrieve(ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId);
                    Session["SurveyList"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Questions
        public ActionResult LoadPartnerSurveyQuestion(int? id, int? questionTypeId, string order)
        {
            var survey = (PartnerSurvey)Session["Survey"];
            var questionTypes = (List<PartnerSurveyQuestionTypes>)Session["QuestionTypes"];

            ValidateQuestionOrder(survey, order);
            var questions = survey.NewQuestions != null && survey.NewQuestions.Count > 0 ? survey.NewQuestions : survey.Questions;
            var question = !id.IsNull() ? questions.Where(x => x.Id == id).FirstOrDefault() : new PartnerSurveyQuestion();

            question.Order = !id.IsNull() ? question.Order : Convert.ToInt32(questions.Max(x => x.Order)) + 1;
            question.QuestionTypeId = questionTypeId;
            question.ShowOptions = question.QuestionTypeId != null ? questionTypes.Where(x => x.Id == question.QuestionTypeId).FirstOrDefault().ShowOptions.IsNullToFalse() : false;
            question.Id = question.Id.IsNull() ? Convert.ToInt32(questions.Max(x => x.Id)) + 1 : question.Id;

            Session["QuestionDetail"] = question;
            return PartialView("_partials/_QuestionDetail", question);
        }

        public ActionResult AddOrEditPartnerSurveyQuestion(string obj, string text, int? questionTypeId)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<PartnerSurvey>(obj);
                var survey = (PartnerSurvey)Session["Survey"];
                var question = (PartnerSurveyQuestion)Session["QuestionDetail"];
                var questionTypes = (List<PartnerSurveyQuestionTypes>)Session["QuestionTypes"];
                var questions = survey.NewQuestions ?? survey.Questions;

                question.Text = text;
                question.QuestionTypeId = questionTypeId;
                question.QuestionTypeText = questionTypes.Where(x => x.Id == questionTypeId).FirstOrDefault().Name;
                if (questions.Where(x => x.Id == question.Id).Any())
                {
                    questions[questions.FindIndex(x => x.Id == question.Id)] = question;
                }
                else
                {
                    questions.Add(question);
                }

                SetAnswerList(questions);
                ReplaceSurvey(survey, model);
                survey.NewQuestions = questions;

                Session["Survey"] = survey;
                Session["QuestionDetail"] = null;
                return PartialView("_partials/_Detail", survey);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult DeletePartnerSurveyQuestion(string obj, int? id)
        {
            var model = JsonConvert.DeserializeObject<PartnerSurvey>(obj);
            var survey = (PartnerSurvey)Session["Survey"];
            var questions = survey.NewQuestions ?? survey.Questions;

            //Remove questions
            questions.Where(x => x.Id == id).ToList().ForEach(a => a.Delete = true);

            ReplaceSurvey(survey, model);
            survey.NewQuestions = questions;

            Session["Survey"] = survey;
            return PartialView("_partials/_Detail", survey);
        }

        public ActionResult CancelAddOrEditPartnerSurveyQuestion()
        {
            var question = (PartnerSurveyQuestion)Session["QuestionDetail"];
            question.NewAnswers = new List<PartnerSurveyAnswer>();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowOrHideOptions(int questionTypeId)
        {
            var question = (PartnerSurveyQuestion)Session["QuestionDetail"];
            var questionTypes = (List<PartnerSurveyQuestionTypes>)Session["QuestionTypes"];
            var showOrHide = questionTypes.Where(x => x.Id == questionTypeId).FirstOrDefault().ShowOptions.IsNullToFalse();

            if (showOrHide)
            {
                return PartialView("_partials/_AnswerDetail", question.Answers);
            }
            else
            {
                return Json(ECOsystem.Resources.PartnerSurvey.NoOptionsForQuestion, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Answers
        public ActionResult LoadPartnerSurveyAnswer(int? id, int? questionTypeId)
        {
            var question = (PartnerSurveyQuestion)Session["QuestionDetail"];
            var answer = !id.IsNull() ? question.NewAnswers != null && question.NewAnswers.Count > 0 ? question.NewAnswers.Where(x => x.Id == id).FirstOrDefault() : question.Answers.Where(x => x.Id == id).FirstOrDefault()
                                    : new PartnerSurveyAnswer();

            //Set properties 
            answer.Id = answer.Id.IsNull() ? question.NewAnswers != null && question.NewAnswers.Count > 0 ? Convert.ToInt32(question.NewAnswers.Max(x => x.Id)) + 1 : Convert.ToInt32(question.Answers.Max(x => x.Id)) + 1 : answer.Id;
            answer.PartnerSurveyQuestionId = questionTypeId;
            answer.ShowNextQuestion = ShowNextQuestion(questionTypeId);
            answer.QuestionsList = GetNextQuestions(question.Order).ToList();

            ViewBag.ShowOtherOption = id.IsNull(); 

            return PartialView("_partials/_OptionDetail", answer);
        }

        public ActionResult AddOrEditPartnerSurveyAnswer(string questionText, int questionTypeId, string obj)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<PartnerSurveyAnswer>(obj);
                var question = (PartnerSurveyQuestion)Session["QuestionDetail"];
                var answers = question.NewAnswers != null && question.NewAnswers.Count > 0 ? question.NewAnswers : question.Answers;

                if (answers.Where(x => x.Id == model.Id).Any())
                {
                    answers[answers.FindIndex(x => x.Id == model.Id)] = model;
                }
                else
                {
                    answers.Add(model);
                }

                question.Text = questionText;
                question.QuestionTypeId = questionTypeId;
                question.NewAnswers = answers;
                question.ShowOptions = true;
                Session["QuestionDetail"] = question;
                return PartialView("_partials/_QuestionDetail", question);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult DeletePartnerSurveyAnswer(string questionText, int questionTypeId, int? id)
        {
            var question = (PartnerSurveyQuestion)Session["QuestionDetail"];
            var answers = question.NewAnswers != null && question.NewAnswers.Count > 0 ? question.NewAnswers : question.Answers;

            answers.Where(x => x.Id == id).ToList().ForEach(a => a.Delete = true);

            question.Text = questionText;
            question.QuestionTypeId = questionTypeId;
            question.NewAnswers = answers;
            question.ShowOptions = true;
            Session["QuestionDetail"] = question;
            return PartialView("_partials/_QuestionDetail", question);
        }
        #endregion

        #region Util
        bool ShowNextQuestion(int? id)
        {
            var list = (List<PartnerSurveyQuestionTypes>)Session["QuestionTypes"];
            var result = list.Where(x => x.Id == id).FirstOrDefault();
            return result != null ? result.OneOption.IsNullToFalse() : false;
        }

        SelectList GetNextQuestions(int? order)
        {
            var survey = (PartnerSurvey)Session["Survey"];
            return GeneralCollections.GetPartnerQuestionsNext(survey.Questions.Where(x => x.Order > order).ToList());
        }

        void SetAnswerList(List<PartnerSurveyQuestion> list)
        {
            var questionTypes = (List<PartnerSurveyQuestionTypes>)Session["QuestionTypes"];
            foreach (var item in list)
            {
                var hasOptions = questionTypes.Where(x => x.Id == item.QuestionTypeId).FirstOrDefault().ShowOptions.IsNullToFalse();
                if (hasOptions && item.NewAnswers.Count > 0) item.AnswersStr = Miscellaneous.GetXML(item.NewAnswers, typeof(PartnerSurveyAnswer).Name);
            }
        }

        void ValidateQuestionOrder(PartnerSurvey model, string order)
        {
            var orderList = JsonConvert.DeserializeObject<List<OrdenItems>>(order);
            bool changeOrder = false;
            var questions = model.NewQuestions != null && model.NewQuestions.Count > 0 ? model.NewQuestions : model.Questions;

            if (!string.IsNullOrEmpty(order))
            {
                foreach (var item in questions.Where(x => !x.Delete))
                {
                    changeOrder = changeOrder ? true : item.Order != orderList.Where(x => x.Id == item.Id).FirstOrDefault().Order ? true : false;
                    item.Order = orderList.Where(x => x.Id == item.Id).FirstOrDefault().Order;
                }
                if (changeOrder)
                {
                    foreach (var item in questions)
                    {
                        var options = item.Answers;
                        options.ForEach(x => x.NextSurveyQuestionId = null);
                        item.AnswersStr = Miscellaneous.GetXML(options, typeof(PartnerSurveyAnswer).Name);
                    }
                }
            }
            model.QuestionsStr = Miscellaneous.GetXML(questions, typeof(PartnerSurveyQuestion).Name);
        }

        void ReplaceSurvey(PartnerSurvey survey, PartnerSurvey model)
        {
            survey.Name = model.Name;
            survey.TargetId = model.TargetId;
            survey.Description = model.Description;
            survey.Active = model.Active;
            survey.Mandatory = model.Mandatory;
        }
        #endregion
    }
}