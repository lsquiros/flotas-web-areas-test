﻿/************************************************************************************************************
*  File    : GPSReportController.cs
*  Summary : GPS Report Controller Actions
*  Author  : Stefano Quirós
*  Date    : 20/04/2016
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web.Mvc;
using ECO_Partner.Models;
using ECO_Partner.Business;
using System.Data;
using ECO_Partner.Utilities;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;
using ECOsystem.Utilities;
using ECOsystem.Business.Utilities;




namespace ECO_Partner.Controllers.Partner
{
    public class GPSReportController : Controller
    {
        /// <summary>
        /// Renderizado inicial del Módulo de Reporte de GPS
        /// </summary>
        /// <returns></returns>       
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                var model = new GPSReportBase();
                model.List = new List<GpsReport>() { new GpsReport() };
                return View(model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(GPSReportBase model)
        {
            try
            {
                return View(new GPSReportBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return RedirectToAction("Index");
            }

        }


        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(int ReportCriteriaId, int ParametersMonth, int ParametersYear, string ParametersEndDateStr, string ParametersGpsModality, int ParametersCustomerId)
        {
            try
            {
                var model = new GPSReportBase();
                model.Parameters.GpsModality = ParametersGpsModality;
                model.Parameters.EndDateStr = ParametersEndDateStr;
                model.Parameters.CustomerId = ParametersCustomerId;
                model.Parameters.Year = ParametersYear;
                model.Parameters.Month = ParametersMonth;
                model.Parameters.ReportCriteriaId = ReportCriteriaId;


                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new GPSReportBusiness())
                {
                    // If get all customers
                    if (model.Parameters.CustomerId == -1)
                        model.Parameters.PartnerId = ECOsystem.Utilities.Session.GetPartnerId();
                    model.Parameters.ActiveStr = "1";
                    var x = business.RetrieveGpsReport(model.Parameters);
                    Session["GPSReport"] = x.List;
                    Session["GPSReportParams"] = model.Parameters;
                    Session["Reporte"] = GPSReportBusiness.DownloadData((List<GpsReport>)Session["GPSReport"], (AdministrationReportsBase)Session["GPSReportParams"]);
                    //return View(x);
                    //return (this.ExcelReportDownload());

                    //Session["Reporte"] = GPSReportBusiness.DownloadData((List<GpsReport>)Session["GPSReport"], (AdministrationReportsBase)Session["GPSReportParams"]);
                    return View("Index", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte GPS");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "GPSReport", this.ToString().Split('.')[2], "Reporte de GPS");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                ViewBag.ErrorReport = 1;
                using (var business = new GPSReportBusiness())
                {
                    var model = business.RetrieveGpsReport(new AdministrationReportsBase());
                    return View("Index", model);
                }
            }
        }
    }
}