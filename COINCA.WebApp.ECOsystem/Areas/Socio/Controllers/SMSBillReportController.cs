﻿using ECO_Partner.Business;
using ECO_Partner.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Partner.Controllers.Partner
{
    public class SMSBillReportController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                return View(new AdministrationReportsBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExcelReportDownload(AdministrationReportsBase model)
        {
            try
            {
                using (var bus = new SMSBillReportBusiness())
                {
                    var report = bus.RetrieveReport(model);
                    if (report.Rows.Count > 0)
                    {
                        Session["ReportData"] = report;
                        return Json("Success");
                    }
                    return Json("NoData");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["ReportData"] != null)
                {
                    report = (DataTable)Session["ReportData"];
                    Session["ReportData"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Facturación de Mensajes SMS");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "SMSBillReport", this.ToString().Split('.')[2], "Reporte de Facturación de Mensajes SMS");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                ViewBag.ErrorReport = 1;
                return View("Index", new AdministrationReportsBase());
            }
        }
    }
}