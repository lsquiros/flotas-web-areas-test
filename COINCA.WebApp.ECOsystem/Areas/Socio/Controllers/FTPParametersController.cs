﻿using ECO_Partner.Business;
using ECO_Partner.Models;
using ECO_Partner.Utilities;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;


using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace COINCA.WebApp.ECO_Partner.Controllers
{
    public class FTPParametersController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var bus = new FTPParametersBusiness())
                {
                    var model = bus.FTPParametersRetrieve();
                    return View(model == null ? new FTPParameters() : model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FTPParametersAddOrEdit(FTPParameters model)
        {
            try
            {
                using (var bus = new FTPParametersBusiness())
                {
                    bus.FTPParametersAddOrEdit(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format((model.Id == null ? "Agrega" : "Edita") + " parámetros ftp con la siguiente información = {0}", new JavaScriptSerializer().Serialize(model)));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                var message = "Error al agregar o editar comercio.";
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format(message + " Detalle del error = {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = message, TechnicalError = e.Message });
            }
        }
    }
}