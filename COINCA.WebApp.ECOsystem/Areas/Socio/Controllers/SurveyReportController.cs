﻿using ECO_Partner.Models;
using ECO_Partner.Utilities;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.Mvc;

namespace ECO_Partner.Controllers.Partner
{
    public class SurveyReportController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [EcoAuthorize]
        public ActionResult ExcelReportDownload(int? surveyId, int? customerId, string startDate, string endDate)
        {
            try
            {
                using (var bus = new PartnerSurveyBusiness())
                {
                    var StartDate = DateTime.ParseExact(startDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var EndDate = DateTime.ParseExact(endDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var report = bus.PartnerSurveyReportDataSet(ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId, surveyId, customerId, StartDate, EndDate);
                    if (report.Tables.Count > 0)
                    {
                        Session["ReportData"] = report;
                        return Json("Success");
                    }
                    return Json("NoData");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public ActionResult DonwloadFile()
        {
            try
            {
                var report = new DataSet();
                if (Session["ReportData"] != null)
                {
                    report = (DataSet)Session["ReportData"];
                    Session["ReportData"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Reporte de Encuestas");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "PartnerSurveyReport", this.ToString().Split('.')[2], "Reporte de Encuestas");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}
