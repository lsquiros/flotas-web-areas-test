﻿using ECO_Partner.Business;
using ECO_Partner.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Partner.Controllers.Partner
{
    public class BillingReportController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                return View(new AdministrationReportsBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExcelReportDownload(AdministrationReportsBase model)
        {
            try
            {
                Session["RangoFechas"] = Convert.ToDateTime(model.StartDate).ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + Convert.ToDateTime(model.EndDate).ToString("dd/MM/yyyy hh:mm:ss tt");

                if (Session["RangoFechas"] == null)
                {
                    Session["RangoFechas"] = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1, 0, 0, 0) + " - " + new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month)), 23, 59, 59);
                }

                using (var bus = new BillingReportBusiness())
                {
                    var report = bus.RetrieveReport(model);
                    if (report != null)
                    {
                        Session["ReportData"] = report;
                        return Json("Success");
                    }
                    return Json("NoData");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataSet report = new DataSet();
                if (Session["ReportData"] != null)
                {
                    report = (DataSet)Session["ReportData"];
                    Session["ReportData"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Facturación");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "BillingReport", this.ToString().Split('.')[2], "Reporte de Facturación");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index", new AdministrationReportsBase());
            }
        }
    }
}
