﻿/************************************************************************************************************
*  File    : TransactionsReportController.cs
*  Summary : Transactions Report Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 12/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Data;
using System;
using ECOsystem.Utilities;
using ECO_Partner.Business;
using ECO_Partner.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Models.Account;
using System.Collections.Generic;
using System.Threading.Tasks;




namespace ECO_Partner.Controllers.Partner
{
    /// <summary>
    /// Transactions Report Class
    /// </summary>
    public class TransactionsReportController : Controller
    {

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult Admin()
        {
            try
            {
                using (var business = new TransactionsReportBusiness())
                {
                    return View(business.RetrieveTransactionsReportDeniedByPartner(new ControlFuelsReportsBase()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Admin(TransactionsReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Admin");

                using (var business = new TransactionsReportBusiness())
                {
                    return View(business.RetrieveTransactionsReportDeniedByPartner(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>       
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(Miscellaneous.Base64Decode(p));

                model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;

                if (model.Month != null && Convert.ToInt32(model.Month) > 0 && model.Year != null && Convert.ToInt32(model.Year) > 0)
                {
                    if (model.StartDate == null)
                    {
                        model.StartDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1, 0, 0, 0);
                    }

                    if (model.EndDate == null)
                    {
                        model.EndDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month)), 23, 59, 59);
                    }
                }
                DataTable report = new DataTable();
                using (var business = new TransactionsReportBusiness())
                {
                    report = business.GenerateDeniedReportData(model);
                    Session["Reporte"] = report;
                    Session["TransactionType"] = model.TransactionType;
                    return View("Admin", new TransactionsReportDeniedByPartnerBase() { Menus = new List<AccountMenus>() });

                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                string ReportName = string.Empty;
                string NameDownload = string.Empty;
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                switch (Convert.ToUInt32(Session["TransactionType"]))
                {
                    case 6:
                        ReportName = "DeniedTransactionsReport";
                        NameDownload = "Reporte de Transacciones Denegadas";
                        break;
                    case 3:
                        ReportName = "ReversedTransactionsReport";
                        NameDownload = "Reporte de Transacciones Reversadas";
                        break;
                    case 2:
                        ReportName = "TransactionsReport";
                        NameDownload = "Reporte de Transacciones Flotantes";
                        break;
                    case 1:
                        ReportName = "TransactionsReport";
                        NameDownload = "Reporte de Transacciones Procesadas";
                        break;
                    case 8:
                        ReportName = "TransactionsReport";
                        NameDownload = "Reporte de Transacciones sin Estación de Servicio";
                        break;
                }

                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Conciliación de Transacciones");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report).Replace('|', '-'), ReportName, this.ToString().Split('.')[2], NameDownload);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                ViewBag.ErrorReport = 1;
                using (var business = new TransactionsReportBusiness())
                {
                    return View("Admin", business.RetrieveTransactionsReportDeniedByPartner(new ControlFuelsReportsBase()));
                }
            }
        }

        /// <summary>
        /// Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new TransactionsReportBusiness())
            {
                return View("Admin", business.RetrieveTransactionsReportDeniedByPartner(model));
            }
        }


        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion

    }
}