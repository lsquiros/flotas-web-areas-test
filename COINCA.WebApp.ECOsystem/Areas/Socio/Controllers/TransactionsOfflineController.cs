﻿using ECO_Partner.Business;
using ECO_Partner.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;


using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ECO_Partner.Controllers
{
    public class TransactionsOfflineController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                using (var bus = new TransactionsOfflineBusiness())
                {
                    var PartnerId = ECOsystem.Utilities.Session.GetUserInfo().PartnerId;
                    var model = new TransactionsOfflineBase()
                    {
                        Data = bus.RetrieveTransactionsOfflineParameters(PartnerId) ?? new TransactionsOffline(),
                        CustomerList = bus.CustomerTransactionsOfflineRetrieve(PartnerId) ?? new List<CustomersTransactionsOffline>(),
                        Menus = new List<AccountMenus>()
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddOrEditOfflineTransactions(int hours, int minutes, bool active, int? idTransactionOffline, string emails, List<int> customers)
        {
            try
            {
                using (var business = new TransactionsOfflineBusiness())
                {
                    var model = new TransactionsOffline()
                    {
                        Id = idTransactionOffline,
                        Hours = hours,
                        Minutes = minutes,
                        StartDate = DateTime.Now,
                        EndDate = DateTime.Now.AddHours(hours).AddMinutes(minutes),
                        Active = active,
                        Emails = emails
                    };
                    business.AddOrEditTransactionsOffline(model, customers);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificación de los parámetros para las transacciones offline. Valores: Parámetros = {0}, Clientes = {1}",
                                                       new JavaScriptSerializer().Serialize(model), new JavaScriptSerializer().Serialize(customers)));
                    return Json("Success");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al modificar parámetros para las transacciones offline. Error técnico = {0}", e.Message));
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SendTransactionsOffline()
        {
            try
            {
                using (var bus = new TransactionsOfflineBusiness())
                {
                    bus.SendTransactionsOffline();
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificación de parámetro de envio de transacciones offline"));
                    return Json("Success");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al modificar parámetro de envio de transacciones offline. Error técnico = {0}", e.Message));
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}