﻿using ECO_Partner.Business;
using ECO_Partner.Models;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;
using ECOsystem.Models.Miscellaneous;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Partner.Controllers
{
    public class GeneralParametersController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                using (var bus = new GeneralParametersBusiness())
                {
                    var partnerId = ECOsystem.Utilities.Session.GetUserInfo().PartnerId;
                    var model = new GeneralParametersBase()
                    {
                        Data = bus.GeneralParametersRetrieve(partnerId),
                        List = new List<GeneralParameters>(),
                        Menus = new List<AccountMenus>()
                    };
                    //Get Token info 
                    using (var bustoken = new APITokenBusiness())
                    {
                        model.Data.APIToken = bustoken.RetrieveAPIToken(PartnerId: partnerId);
                    }
                    Session["SMSPrices"] = model.Data.SMSPricesList;
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddOrEditGeneralParameters(GeneralParametersBase model)
        {
            try
            {
                using (var bus = new GeneralParametersBusiness())
                {
                    var partnerId = ECOsystem.Utilities.Session.GetUserInfo().PartnerId;
                    var token = string.Empty;
                    model.Data.SMSPriceXML = ECOsystem.Utilities.Miscellaneous.GetXML((List<CustomerPricesModel>)Session["SMSPrices"], typeof(CustomerPricesModel).Name);
                    bus.GeneralParametersAddOrEdit(model.Data, partnerId);

                    #region API Service TOken
                    if (model.Data.GenerateToken)
                    {
                        using (var bustoken = new APITokenBusiness())
                        {
                            token = bustoken.GenerateAPIToken(PartnerId: partnerId);
                        }
                    }
                    #endregion

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edición de Parámetros Globales.Detalles: {0}", JsonConvert.SerializeObject(model)));
                    return Json(token, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, "Error al actualizar Parámetros Globales " + e);
                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        #region SMSPrices 
        [HttpPost]
        public ActionResult SavePricesForSMS(CustomerPricesModel model)
        {
            try
            {
                var list = Session["SMSPrices"] != null ? (List<CustomerPricesModel>)Session["SMSPrices"] : new List<CustomerPricesModel>();

                if (model.Id == null)
                {
                    model.Id = list.Max(x => x.Id) != null ? (int)list.Max(x => x.Id) + 1 : 1;
                    list.Add(model);
                }
                else
                {
                    list.Where(x => x.Id == model.Id).ToList().ForEach(s => { s.From = model.From; s.To = model.To; s.Price = model.Price; });
                }
                //Validates 
                var r = ValidateRange(list.OrderBy(x => x.From).ToList(), model);
                if (!r.WithinRange)
                {
                    Session["SMSPrices"] = list.OrderBy(x => x.From).ToList();
                }
                else
                {
                    list.RemoveAll(x => x.Id == model.Id);
                }

                return Json(new { partial = RenderPartialViewToString("_partials/_PricesGrid", list.OrderBy(x => x.From).ToList()), withinRange = r.WithinRange, showwarning = r.NotInRange, maxfrom = r.NotInRange != null ? r.NotInRange : list.Max(x => x.To) + 1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeletePricesForSMS(int id)
        {
            try
            {
                var list = new List<CustomerPricesModel>();
                list = Session["SMSPrices"] != null ? (List<CustomerPricesModel>)Session["SMSPrices"] : new List<CustomerPricesModel>();
                list.RemoveAll(x => x.Id == id);
                Session["SMSPrices"] = list;

                var r = ValidateRange(list.OrderBy(x => x.From).ToList(), null);

                return Json(new { partial = RenderPartialViewToString("_partials/_PricesGrid", list.OrderBy(x => x.From).ToList()), maxfrom = r.NotInRange != null ? r.NotInRange : list.Count == 0 ? 1 : list.Max(x => x.To) + 1, showwarning = r.NotInRange }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        CustomerValidatePrices ValidateRange(List<CustomerPricesModel> list, CustomerPricesModel model)
        {
            try
            {
                var i = 1;
                var result = new CustomerValidatePrices();
                var nextVal = 0;

                foreach (var item in list)
                {
                    if (list.Skip(i).FirstOrDefault() != null)
                    {
                        if (item.To + 1 != list.Skip(i).FirstOrDefault().From)
                        {
                            result.NotInRange = item.To + 1;
                            nextVal = list.Skip(i).FirstOrDefault().From;
                        }
                        i++;
                    }
                }
                //Validates if the range is lower than one already in the list 
                if (model != null)
                {
                    list.RemoveAll(x => x.Id == model.Id); //remove the item already in the list for the analisis 
                    if (list.Count > 0 && !list.Select(x => x.From).ToList().Contains(model.From))
                    {
                        result.WithinRange = model.To > nextVal && model.To < result.NotInRange ? true : false;
                    }
                    list.Add(model);
                }
                return result;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }

        public string RenderPartialViewToString(string viewName, object model)
        {
            try
            {
                ViewData.Model = model;
                using (var sw = new StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return null;
            }
        }
        #endregion
    }
}