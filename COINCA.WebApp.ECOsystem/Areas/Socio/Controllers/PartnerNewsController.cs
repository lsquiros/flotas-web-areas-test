﻿using ECOsystem.Audit;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Core;
using ECO_Partner.Business;
using ECO_Partner.Utilities;
using System.Web.Script.Serialization;




namespace ECO_Partner.Controllers
{
    public class PartnerNewsController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var bus = new PartnerNotificationsBusiness())
                {
                    using (var business = new PartnerNewsBusiness())
                    {
                        var model = new PartnersNewsBase
                        {
                            Data = new PartnerNews(),
                            List = business.RetrieveNewsByPartners(ECOsystem.Utilities.Session.GetPartnerId(), null),
                            ListNotifications = bus.PartnerNotificationsRetrieve(null),
                            Menus = new List<AccountMenus>()
                        };
                        return View(model);
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        #region News
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditPartnerNews(PartnerNews model)
        {
            try
            {
                using (var business = new PartnerNewsBusiness())
                {
                    var partnerId = ECOsystem.Utilities.Session.GetPartnerId();
                    model.PartnerId = (int)partnerId;
                    business.AddOrEditNewsByPartners(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edición / Creación de Noticia, datos: {0}", new JavaScriptSerializer().Serialize(model)));
                    return PartialView("_partials/_Grid", business.RetrieveNewsByPartners(partnerId, null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index")
                });
            }
        }

        [HttpPost]
        public PartialViewResult SearchPartnerNews(string key)
        {
            try
            {
                using (var business = new PartnerNewsBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveNewsByPartners(ECOsystem.Utilities.Session.GetPartnerId(), null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index")
                });
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadPartnerNews(int? id)
        {
            try
            {
                using (var business = new PartnerNewsBusiness())
                {
                    return PartialView("_partials/_Detail", id == null ? new PartnerNews() : business.RetrieveNewsByPartners(ECOsystem.Utilities.Session.GetPartnerId(), id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index")
                });
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeletePartnerNews(int id)
        {
            try
            {
                using (var business = new PartnerNewsBusiness())
                {
                    business.DeleteNewsByPartners(id);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se elimina Noticia, datos: {0}", id));
                    return PartialView("_partials/_Grid", business.RetrieveNewsByPartners(ECOsystem.Utilities.Session.GetPartnerId(), null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index")
                    });
                }
            }
        }
        #endregion

        #region Notifications           
        public ActionResult AddOrEditNotifications(PartnerNotifications model)
        {
            try
            {
                using (var bus = new PartnerNotificationsBusiness())
                {
                    bus.PartnerNotificationsAddOrEdit(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edición / Creación de Notificación, datos: {0}", new JavaScriptSerializer().Serialize(model)));
                    return PartialView("_partials/_GridNotifications", bus.PartnerNotificationsRetrieve(null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index")
                });
            }
        }

        public ActionResult SearchNotifications(string key)
        {
            try
            {
                using (var bus = new PartnerNotificationsBusiness())
                {
                    return PartialView("_partials/_GridNotifications", bus.PartnerNotificationsRetrieve(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index")
                });
            }
        }

        public ActionResult LoadNotifications(int? id)
        {
            try
            {
                using (var bus = new PartnerNotificationsBusiness())
                {
                    if (id == null)
                    {
                        return PartialView("_partials/_DetailNotification", new PartnerNotifications());
                    }
                    return PartialView("_partials/_DetailNotification", bus.PartnerNotificationsRetrieve(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index")
                });
            }
        }

        public ActionResult DeleteNotifications(int id)
        {
            try
            {
                using (var bus = new PartnerNotificationsBusiness())
                {
                    bus.PartnerNotificationsDelete(id);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se elimina la notificación, datos: {0}", id));
                    return PartialView("_partials/_GridNotifications", bus.PartnerNotificationsRetrieve(null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                {
                    TitleError = "Eliminación de Registro",
                    TechnicalError = e.Message,
                    ReturnUlr = Url.Action("Index")
                });
            }
        }

        public ActionResult ValidateNotifications(string xml)
        {
            try
            {
                using (var bus = new PartnerNotificationsBusiness())
                {
                    var model = new PartnerNotifications() { Content = xml };
                    var result = bus.PartnerNotificationsValidate(model).Content;
                    if (string.IsNullOrEmpty(result))
                    {
                        return Json(new { message = "NoData", list = new List<string>(), error = string.Empty }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { message = "Success", data = result.Split(';').ToList(), error = string.Empty }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(new { message = "Error", list = new List<string>(), error = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}
