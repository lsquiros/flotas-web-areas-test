﻿using System;

using System.Web.Mvc;
using ECO_Partner.Utilities;
using ECOsystem.Business.Utilities;



namespace ECO_Partner.Controllers
{
    /// <summary>
    /// PartnerReportsController
    /// </summary>
    public class PartnerReportsController : Controller
    {
        //
        // GET: /PartnerReports/
        //[Authorize(Roles = "PARTNER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}