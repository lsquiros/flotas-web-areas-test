﻿/************************************************************************************************************
*  File    : ApplicantsReportController.cs
*  Summary : Applicants Report Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 12/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Web.Mvc;
using Newtonsoft.Json;
using System;
using ECOsystem.Audit;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;
using ECO_Partner.Business;
using ECO_Partner.Models;
using ECO_Partner.Utilities;




namespace ECO_Partner.Controllers
{
    /// <summary>
    /// Applicants Report Class
    /// </summary>
    public class ApplicantsReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        /// 
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new ApplicantsReportBusiness())
                {
                    ViewBag.Users = new SelectList(business.RetrieveAdminPartnerUsersByCountry(0), "Key", "Value");
                    return View(business.RetrieveApplicantsReport(new AdministrationReportsBase()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(ApplicantsReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new ApplicantsReportBusiness())
                {
                    return View(business.RetrieveApplicantsReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                // Return response
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ApplicantsReport")
                });
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<AdministrationReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));
                if (model != null)
                {
                    model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                    model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
                }
                if (model.Month != null && Convert.ToInt32(model.Month) > 0 && model.Year != null && Convert.ToInt32(model.Year) > 0)
                {
                    if (model.StartDate == null)
                    {
                        model.StartDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1, 0, 0, 0);
                    }

                    if (model.EndDate == null)
                    {
                        model.EndDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month)), 23, 59, 59);
                    }
                }

                byte[] report;
                using (var business = new ApplicantsReportBusiness())
                    report = business.GenerateApplicantsReportExcel(model);
                return new FileStreamResult(new MemoryStream(report), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "ExcelReport.xlsx"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        public ActionResult PrintReport(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<AdministrationReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

                ViewBag.PrintView = true;
                using (var business = new ApplicantsReportBusiness())
                {
                    return View("Index", business.RetrieveApplicantsReport(model));
                }
            }
            catch (Exception e)
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                //Add log event
                AddLogEvent(
                        e,
                        userId: user != null ? user.UserId : 0,
                        customerId: user != null ? user.CustomerId : 0,
                        partnerId: user != null ? user.PartnerId : 0,
                        message: string.Empty,
                        isError: true);

                // Return response
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ApplicantsReport")
                });
            }
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = exception != null ? string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace) : message;

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = ControllerContext.RouteData.Values["controller"].ToString(),
                Action = ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}
