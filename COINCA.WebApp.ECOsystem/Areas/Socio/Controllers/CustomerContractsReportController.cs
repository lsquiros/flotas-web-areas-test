﻿using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Mvc;
using ECO_Partner.Business;
using ECO_Partner.Models;

namespace ECO_Partner.Controllers
{
    public class CustomerContractsReportController : Controller
    {
        public ActionResult Index()
        {
            return View(new CustomerContractsReportBase());
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(CustomerContractsReportBase model)
        {
            try
            {
                using (var business = new CustomerContractsReportBusiness())
                {
                    var report = business.CustomerContractsReportDataTable(model);
                    var count = report.Columns.Count;
                    if (count > 0)
                    {
                        Session["ReportData"] = report;
                        return Json(count, JsonRequestBehavior.AllowGet);
                    }
                    return Json(count, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DownloadReport()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["ReportData"] != null)
                {
                    report = (DataTable)Session["ReportData"];
                    Session["ReportData"] = null;
                }
                using (var bus = new ECOsystem.Utilities.ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Contratos");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "CustomerContractsReport", "Partner", "Reporte de Contratos");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                ViewBag.ErrorReport = 1;
                using (var business = new GPSReportBusiness())
                {
                    return RedirectToAction("Index");
                }
            }
        }
    }
}
