﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    public class BillingReport
    {
        public int OrderId { get; set; }

        public string CustomerName { get; set; }

        public string DecryptedCustomerName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(CustomerName); } }        

        public string PlateId { get; set; }

        public DateTime CloseOrderDate { get; set; }       

    }

    public class BillingReportDetails
    {
        public int OrderId { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public decimal BaseAmount { get; set; }

        public decimal CostAmount { get; set; }

        public decimal PaymentAmount { get; set; }

        public int CatalogId { get; set; }

    }
}