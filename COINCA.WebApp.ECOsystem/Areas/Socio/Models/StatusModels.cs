﻿/************************************************************************************************************
*  File    : StatusModels.cs
*  Summary : Status Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.Models.Miscellaneous;

namespace ECO_Partner.Models
{
    
    /// <summary>
    /// Partner Models
    /// </summary>
    public class Status : ModelAncestor
    {
        /// <summary>
        /// Status Id property
        /// </summary>
        public int StatusId { get; set; }

        /// <summary>
        /// Status property
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Code property
        /// </summary>
        public string Code { get; set; }

    }
}