﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    public class ManagementBillReport
    {
        public string EncryptCustomerName { get; set; }

        public string DecryptCustomerName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptCustomerName); } }

        public int DependentCustomers { get; set; }

        public int Agents { get; set; }

        public decimal UnitAmount { get; set; }

        public decimal TotalAmount { get; set; }

        public DateTime EndDate { get; set; }
    }
}