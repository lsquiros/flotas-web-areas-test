﻿/************************************************************************************************************
*  File    : GPSReportModels.cs
*  Summary : GPS Report Models
*  Author  : Stefano Quirós
*  Date    : 20/04/2016
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Utilities;
using System.Globalization;
using System.ComponentModel.DataAnnotations;

namespace ECO_Partner.Models
{
    /// <summary>
    /// Applicants Report Base
    /// </summary>
    public class GPSReportBase
    {
        /// <summary>
        /// Applicants Reports Base Constructor
        /// </summary>
        public GPSReportBase()
        {
            Parameters = new AdministrationReportsBase();
            List = new List<dynamic>();
            UsersList = new List<SelectListItem>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public AdministrationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<dynamic> List { get; set; }

        /// <summary>
        /// List of users
        /// </summary>
        public IEnumerable<SelectListItem> UsersList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    public class GpsReport {
        // Client Data
        public string ClientId;
        private string _clientName;
        public string ClientName 
        { 
            get 
            {
                return _clientName;
            }
            set 
            {
                _clientName = ECOsystem.Utilities.TripleDesEncryption.Decrypt(value);
            }
        }

        public string isDemo { get; set; }
        public int TotalGPS { get; set; }
        public int Installations { get; set; }
        public int InstallationsThisMonth { get; set; }
        public decimal Fee { get; set; }
        public decimal MembershipFee { get; set; }
        [Display(Name ="Modalidad")]
        public int GPSModalityId { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime PrevEndDate { get; set; }
        public string ReportModality { get; set; }

        public string EndDateStr { get { return EndDate.ToString("MMMM yyyy", new CultureInfo("es-ES")); } }
        public string PrevEndDateStr { get { return PrevEndDate.ToString("MMMM yyyy", new CultureInfo("es-ES")); } }

        public decimal Cost
        {
            get
            {
                return Fee * Installations;
            }
        }
        public decimal MembershipCost
        {
            get
            {
                return MembershipFee * Installations;
            }
        }
        public decimal TotalCost
        {
            get
            {
                return Cost + MembershipCost + TotalOrders;
            }
        }

        public string FeeStr { get { return Miscellaneous.GetNumberFormatCR(Fee); } }
        public string CostStr { get { return Miscellaneous.GetNumberFormatCR(Cost); } }
        public string MembershipFeeStr { get { return Miscellaneous.GetNumberFormatCR(MembershipFee); } }
        public string MembershipCostStr { get { return Miscellaneous.GetNumberFormatCR(MembershipCost); } }
        public string TotalCostStr { get { return Miscellaneous.GetNumberFormatCR(TotalCost); } }
        public string Get(string x) {
            return x;
        }

		public decimal TotalOrdersAmount { get; set; }

        public int TotalOrders { get; set; }
    }

    /// <summary>
    /// Credit Cards Report By Client
    /// </summary>
    public class GPSReportByClient
    {
        /// <summary>
        /// Encrypted Client Name
        /// </summary>
        [NotMappedColumn]
        public string ClientName { get; set; }

        /// <summary>
        /// Decryped Client Name
        /// </summary>
        [DisplayName("Nombre Cliente")]
        [GridColumn(Title = "Nombre Cliente", Width = "120px", SortEnabled = true)]
        public string DecrypedClientName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(ClientName); }
            set { ClientName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Encrypted AccountNumber
        /// </summary>
        [NotMappedColumn]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Decryped AccountNumber
        /// </summary>
        [NotMappedColumn]
        [GridColumn(Title = "Número Cuenta", Width = "120px", SortEnabled = true)]
        public string DecrypedAccountNumber
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(AccountNumber); }
            set { AccountNumber = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Account Number with restrict display
        /// </summary>
        [DisplayName("Número de Cuenta")]
        [GridColumn(Title = "Número de Cuenta", Width = "120px", SortEnabled = true)]
        public string RestrictAccountNumber { get { return ECOsystem.Utilities.Miscellaneous.GetDisplayAccountNumberMask(DecrypedAccountNumber); } }

        /// <summary>
        /// StatusName
        /// </summary>
        [DisplayName("Estado")]
        [GridColumn(Title = "Estado", Width = "120px", SortEnabled = true)]
        public string StatusName { get; set; }

        /// <summary>
        /// RequestDate Date
        /// </summary>
        [NotMappedColumn]
        [GridColumn(Title = "Creado", Width = "120px", SortEnabled = true)]
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Create Date Str
        /// </summary>
        [DisplayName("Creado")]
        [GridColumn(Title = "Creado", Width = "120px", SortEnabled = true)]
        public string CreateDateStr { get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(CreateDate); } }

        /// <summary>
        /// Activo
        /// </summary>
        [DisplayName("Activo")]
        public bool Active { get; set; }

        /// <summary>
        /// Total de Tarjetas Administrativas
        /// </summary>
        [DisplayName("Administración")]
        public int TotalCantGPS { get; set; }      
        

        /// <summary>
        /// Costo de Administración
        /// </summary>
        [DisplayName("Costo Administración")]
        public decimal CostAdmin { get; set; }

        /// <summary>
        /// Total de GPS's que se estan monitoreando
        /// </summary>
        [DisplayName("Monitoreo")]
        public int TotalCantMonitor { get; set; }
       

        /// <summary>
        /// Costo de Monitoreo
        /// </summary>
        [DisplayName("Costo Monitoreo")]
        public decimal CostMonitor { get; set; }

        /// <summary>
        /// Total de equipos vendidos
        /// </summary>
        [DisplayName("Equipos Vendidos")]
        public int CantSoldEquip { get; set; }

        /// <summary>
        /// Costo de Equipo Vendido
        /// </summary>
        [DisplayName("Costo Equipo Vendido")]
        public decimal CostSoldEquip { get; set; }


        /// <summary>
        /// Total de Tarjetas Bloqueadas
        /// </summary>
        [DisplayName("Equipos Alquilados")]
        public int CantGPSRented { get; set; }

        /// <summary>
        /// Costo de Equipo Vendido
        /// </summary>
        [DisplayName("Costo Equipo Alquilado")]
        public decimal CostRentedGPS { get; set; }
    }
}
