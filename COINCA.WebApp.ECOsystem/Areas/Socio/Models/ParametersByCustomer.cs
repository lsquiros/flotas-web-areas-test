﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    public class ParametersByCustomer
    {
        /// <summary>
        /// Indentifier of Model
        /// </summary>
        public int? ParameterByCustomerId { get; set; }

        /// <summary>
        /// Indentifier of Model
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Name of the parameter
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Value of the Parameter
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Key of the Model
        /// </summary>
        public string ResuourceKey { get; set; }
    }
}