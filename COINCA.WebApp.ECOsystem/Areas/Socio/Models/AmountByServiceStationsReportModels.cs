﻿using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    public class AmountByServiceStationsReportBase
    {
        public AmountByServiceStationsReportBase()
        {
            Menus = new List<AccountMenus>();
        }

        [DisplayName("Fecha final")]
        [Required(ErrorMessage = "{0} es requerida")]
        public string EndDate { get; set; }

        public List<AccountMenus> Menus { get; set; }
    }

    public class AmountByServiceStationsReport
    {
        public string BacId { get; set; }

        public string ServiceStationName { get; set; }

        public string TerminalId { get; set; }

        public string CountryName { get; set; }

        public decimal MonthlyAmount { get; set; }

        public decimal TopMonetaryLimit { get; set; }

        public decimal AvailableAmount { get; set; }
    }
}