﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    public class AvailableBudgetReport
    {
        public int UserId { get; set; }

        public string PhoneNumber { get; set; }

        public string DecryptPhoneNumber { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(PhoneNumber); } }

        public string Message { get; set; }

        public string MessageFixed
        {
            get
            {
                Message = Message.Replace("%CREDITCARD%", CreditCardFormat);
                Message = Message.Replace("%AMOUNT%", AvailableAmount.ToString());

                return Message;
            }
        }

        public string CreditCard { get; set; }

        public string CreditCardFormat { get { return string.IsNullOrEmpty(CreditCard) ? string.Empty : ECOsystem.Utilities.TripleDesEncryption.Decrypt(CreditCard).Split('-')[3]; } }

        public decimal AvailableAmount { get; set; }

        public decimal AvailableLiters { get; set; }
    }
}