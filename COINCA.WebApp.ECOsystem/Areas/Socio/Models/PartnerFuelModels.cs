﻿/************************************************************************************************************
*  File    : PartnerFuelModels.cs
*  Summary : PartnerFuel Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;

namespace ECO_Partner.Models
{
    /// <summary>
    /// PartnerFuel Base
    /// </summary>
    public class PartnerFuelBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public PartnerFuelBase()
        {
            KeyId = 0;
            Data = new PartnerFuel();
            List = new List<PartnerFuel>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public PartnerFuel Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PartnerFuel> List { get; set; }

        /// <summary>
        /// KeyId for load the information
        /// </summary>
        public int KeyId { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// PartnerFuel Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PartnerFuel : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PartnerFuelId { get; set; }


        /// <summary>
        /// FK Vehicle Id from Users, each vehicle is associate with one user
        /// </summary>
        [NotMappedColumn]
        public int? PartnerId { get; set; }
        
        /// <summary>
        /// Liters of fuel
        /// </summary>
        [DisplayName("Combustible")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? FuelId { get; set; }

        /// <summary>
        /// Fuel Name
        /// </summary>
        [GridColumn(Title = "Combustible", Width = "150px", SortEnabled = true)]
        public string FuelName { get; set; }

        /// <summary>
        /// Liter Price
        /// </summary>
        [DisplayName("Precio por Litro")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Precio", Width = "100px", SortEnabled = true, Format = "{0:N}")]
        public decimal? LiterPrice { get; set; }
        
        /// <summary>
        /// Start Date
        /// </summary>
        [GridColumn(Title = "Desde", Width = "150px", SortEnabled = true)]
        public string StartDate { get; set; }

        /// <summary>
        /// Start Date
        /// </summary>
        [GridColumn(Title = "Hasta", Width = "150px", SortEnabled = true)]
        public string EndDate { get; set; }

    }
}