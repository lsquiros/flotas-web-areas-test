﻿/************************************************************************************************************
*  File    : ControlReportsModels.cs
*  Summary : Control Reports Models
*  Author  : Danilo Hidalgo
*  Date    : 12/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;

namespace ECO_Partner.Models
{
    /// <summary>
    /// Report Fuel Types Enum
    /// </summary>
    public enum ReportCriteria
    {
        /// <summary>
        /// Period
        /// </summary>
        Period = 1000,
        /// <summary>
        /// Date Range
        /// </summary>
        DateRange = 2000
    };

    /// <summary>
    /// Control Fuels Reports Base Model
    /// </summary>
    public class AdministrationReportsBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public AdministrationReportsBase()
        {
            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;
            ReportCriteriaId = (int?)ReportCriteria.Period;
            UserId = 0;
            CountryId = 0;
            CustomerId = 0;
        }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        public String ActiveStr { get; set; }
        public String GpsModality { get; set; }

        public Boolean IsActive { get { return ActiveStr == "1"; } }
        public int GpsModalityId 
        { 
            get 
            { 
                return Convert.ToInt32(GpsModality); 
            } 
        }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date for Report when dates range is selected
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = value != null ? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(null); }
        }

        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }

        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64 { get; set; }

        /// <summary>
        /// Report User Id
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Report Transaction Type
        /// </summary>
        public int? CountryId { get; set; }

        /// <summary>
        /// Report CustomerId 
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Report PartnerId 
        /// </summary>
        public int? PartnerId { get; set; }

    }

}
