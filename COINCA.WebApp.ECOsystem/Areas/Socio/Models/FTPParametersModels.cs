﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    public class FTPParameters
    {
        public int? Id { get; set; }

        public bool IsProtocolSFTP { get; set; }

        public string HostName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Key { get; set; }

        public string HostFolder { get; set; }

        public bool Active { get; set; }
    }
}