﻿using ECOsystem.Models.Account;
using GridMvc.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    /// <summary>
    /// Transactions Report Base
    /// </summary>
    public class TransactionsOfflineBase
    {
        /// <summary>
        /// COffline Transactions Base Constructor
        /// </summary>
        public TransactionsOfflineBase()
        {
           Menus = new List<AccountMenus>();
           Data = new TransactionsOffline();
           CustomerList = new List<CustomersTransactionsOffline>();
        }

        public IEnumerable<AccountMenus> Menus { get; set; }

        public TransactionsOffline Data { get; set; }

        public IEnumerable<CustomersTransactionsOffline> CustomerList { get; set; }
    }

    /// <summary>
    /// Offline Transactions Model
    /// </summary>
    public class TransactionsOffline
    {

        /// <summary>
        /// TransactionOfflineParameter
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Email for send of the alarm
        /// </summary>
        [DisplayName("Correos:")]
        [NotMappedColumn]
        public string Emails { get; set; }

        /// <summary>
        /// TransactionOfflineBeginDate
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// TransactionOfflineFinalDate
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Hours
        /// </summary>
        public int? Hours { get; set; }

        /// <summary>
        /// Minutes
        /// </summary>
        public int? Minutes { get; set; }
        
        /// <summary>
        /// Active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// PartnerId
        /// </summary>
        public int PartnerId { get; set; }

        /// <summary>
        /// AlarmId
        /// </summary>
        public int? AlarmId { get; set; }

    }

    public class CustomersTransactionsOffline
    {
        public int CustomerId { get; set; }

        public string EncryptName { get; set; }

        public string DecryptName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptName);  } }

        public bool TransactionsOfflineActive { get; set; }
    }
}