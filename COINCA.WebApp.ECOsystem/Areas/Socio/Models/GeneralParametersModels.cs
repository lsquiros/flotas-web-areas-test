﻿using ECOsystem.Models.Account;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    /// <summary>
    /// General Parameters Base
    /// </summary>
    public class GeneralParametersBase
    {
        public GeneralParametersBase()
        {
            Data = new GeneralParameters();
            List = new List<GeneralParameters>();
            Menus = new List<AccountMenus>();
        }

        public GeneralParameters Data { get; set; }

        public IEnumerable<GeneralParameters> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// General Parameters Model
    /// </summary>
    public class GeneralParameters
    {
        public int? Id { get; set; }

        [DisplayName("Alertar")]        
        [Range(typeof(Decimal), "1", "9999", ErrorMessage = "El valor debe de ser un número entre 1 y 9999")]
        public int? AlarmDays { get; set; }

        [DisplayName("RTN")]                
        public string RTN { get; set; }

        [DisplayName("Teléfono")]
        public string PhoneNumber { get; set; }

        [DisplayName("Dirección")]
        public string Address { get; set; }

        [DisplayName("Nombre del Servicio")]
        public string BacService { get; set; }

        [DisplayName("Nombre de la Entidad")]
        public string BacName { get; set; }

        [DisplayName("Tolerancia Montos por Estación de Servicio")]        
        [Range(typeof(Decimal), "1", "99", ErrorMessage = "El valor debe de ser un número entre 1 y 99")]
        public int? StationTolerance { get; set; }

        [DisplayName("Número de Tarjeta")]        
        public string CreditCardNumber { get; set; }

        [DisplayName("Monto en la Transacción")]        
        public decimal? Amount { get; set; }

        [DisplayName("Activar Validación")]
        public bool Active { get; set; }

        [DisplayName("Correo para notificar la expiración de tarjetas")]
        public string CardAlarmEmail { get; set; }

        [DisplayName("Número de días")]
        public int? PreauthorizedDays { get; set; }

        [DisplayName("Correo Notificación")]
        public string PreauthorizedEmail { get; set; }

        public string SMSPriceXML { get; set; }

        public List<CustomerPricesModel> SMSPricesList
        {
            get { return string.IsNullOrEmpty(SMSPriceXML) ? new List<CustomerPricesModel>() : ECOsystem.Utilities.Miscellaneous.GetXMLDeserialize<List<CustomerPricesModel>>(SMSPriceXML, typeof(CustomerPricesModel).Name); }
        }

        public bool SMSInternalModem { get; set; }

        public string APIToken { get; set; }

        public bool GenerateToken { get; set; }

        [DisplayName("Notificación Modo Piloto")]
        public string PilotModeNotificationEmail { get; set; }

        #region ContractProperties
        public int? ContractYears { get; set; }

        public bool SelectAllVehicles { get; set; }

        public int? ContractAlertDays { get; set; }
        #endregion

        #region CancelCreditCardAlert
        public int? CancelCreditCardNumber { get; set; }
        public string CancelCreditCardEmails { get; set; }
        public bool CancelCreditCardActive { get; set; }
        #endregion

        #region
        public int Periodicity { get; set; }

        public int NameDay { get; set; }

        public int NameDate { get; set; }

        public int Repeat { get; set; }

        public string ExpireCreditCardEmails { get; set; }

        public bool ExpireCreditCardActive { get; set; }

        public int AlertBefore { get; set; }

        public string MasterCardEmails { get; set; }

        public bool MasterCardAlertActive { get; set; }

        public int MasterCardPercent { get; set; }

        public int MasterCardAlertPeriodicity { get; set; }

        public int MasterCardAlertNameDay { get; set; }

        public int MasterCardAlertNameDate { get; set; }

        public int MasterCardAlertRepeat { get; set; }

        #endregion
    }
}