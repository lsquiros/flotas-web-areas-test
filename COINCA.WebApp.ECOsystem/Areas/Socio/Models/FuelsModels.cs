﻿/************************************************************************************************************
*  File    : FuelsModels.cs
*  Summary : Fuels Models
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;

namespace ECO_Partner.Models
{
    /// <summary>
    /// Fuels Base
    /// </summary>
    public class FuelsBase
    {
        /// <summary>
        /// FuelsBase constructor
        /// </summary>
        public FuelsBase()
        {
            Data = new Fuels();
            List = new List<Fuels>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Fuels Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Fuels> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }


    }


    /// <summary>
    /// Fuels Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Fuels : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? FuelId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Combustible")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre del  Combustible", Width = "200px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        [DisplayName("País")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int CountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("País")]
        [GridColumn(Title = "País", Width = "200px", SortEnabled = true)]
        public string CountryName { get; set; }
    }


    /// <summary>
    /// Fuels By Credit Model
    /// </summary>
    public class FuelsCost
    {

        /// <summary>
        /// The FuelId FK property identifies fuels model
        /// </summary>
        public int? FuelId { get; set; }
        
        /// <summary>
        /// Liter Price by current FuelId
        /// </summary>
        public decimal? LiterPrice { get; set; }


    }
}