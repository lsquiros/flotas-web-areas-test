﻿/************************************************************************************************************
*  File    : CurrentFuelsReportModels.cs
*  Summary : Real Vs Budget Fuels Report Models
*  Author  : Berman Romero
*  Date    : 21/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;
using System;

using System.Web;
using Newtonsoft.Json;
using System.Configuration;

namespace ECO_Partner.Models
{

     public enum ReportFuelTypes
    {
        /// <summary>
        /// Vehicles
        /// </summary>
        Vehicles = 100,
        /// <summary>
        /// Vehicle Groups
        /// </summary>
        VehicleGroups = 200,
        /// <summary>
        /// Vehicle Cost Centers
        /// </summary>
        VehicleCostCenters = 300,
        /// <summary>
        /// Vehicle Units
        /// </summary>
        VehicleUnits = 400
    };

    public enum ReportComparativeTypes
    {
        /// <summary>
        /// Vehicles
        /// </summary>
        Vehicles = 100,
        /// <summary>
        ///Ruta
        /// </summary>
        route = 200,
        /// <summary>
        /// Modelo
        /// </summary>
        model = 300,
        /// <summary>
        /// Marca
        /// </summary>
        brand = 400,
        /// <summary>
        /// Cilindraje
        /// </summary>
        cilinder = 500
    };


      public class TransactionsReportDeniedByPartnerBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public TransactionsReportDeniedByPartnerBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<TransactionsReportDeniedByPartner>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<TransactionsReportDeniedByPartner> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Transactions Report Model
    /// </summary>
    public class TransactionsReportDeniedByPartner
    {
        private string _customerName;

        /// <summary>
        /// CustomerName
        /// </summary>
        [DisplayName("Customer Name")]
        [ExcelMappedColumn("Nombre de Cliente")]
        [NotMappedColumn]
        public string CustomerName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(_customerName);
            }
            set
            {
                this._customerName = value;
            }
        }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("Transacción Id")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int TransactionId { get; set; }

        /// <summary>
        /// System Trace Code
        /// </summary>
        [DisplayName("Código Transacción")]
        public string SystemTraceCode { get; set; }

        /// <summary>
        /// Invoice - # Factura
        /// </summary>
        [DisplayName("#Factura")]
        public string Invoice { get; set; }

        /// <summary>
        /// MerchantDescription - Gasolinera
        /// </summary>
        [DisplayName("Gasolinera")]
        public string MerchantDescription { get; set; }

        /// <summary>
        /// Transaction State
        /// </summary>
        [DisplayName("Estado Transacción")]
        public string State { get; set; }

        /// <summary>
        /// HolderName
        /// </summary>
        [DisplayName("Titular")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Titular", Width = "120px", SortEnabled = true)]
        public string HolderName { get; set; }

        /// <summary>
        /// HolderNameStr
        /// </summary>
        [DisplayName("Titular")]
        [GridColumn(Title = "Titular", Width = "120px", SortEnabled = true)]
        public string HolderNameStr { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(HolderName); } }


        /// <summary>
        /// Date
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha")]
        [GridColumn(Title = "Fecha", Width = "120px", SortEnabled = true)]
        public string DateStr { get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(Date); } }

        /// <summary>
        /// FuelName
        /// </summary>
        [DisplayName("Combustible")]
        [GridColumn(Title = "Combustible", Width = "120px", SortEnabled = true)]
        public string FuelName { get; set; }

        /// <summary>
        /// Liters
        /// </summary>
        [DisplayName("Litros")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Litros", Width = "120px", SortEnabled = true)]
        public decimal Liters { get; set; }

        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
        /// <summary>
        /// Capacity Unit Value
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal CapacityUnitValue
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, ECOsystem.Utilities.Session.GetCustomerCapacityUnitId(), Liters), 3); }
        }

        /// <summary>
        /// Capacity Unit Value as String
        /// </summary>
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Unidad de Capacidad", Width = "120px", SortEnabled = true)]
        public string CapacityUnitValueStr
        {
            get { return Math.Round(CapacityUnitValue, 3).ToString(); }
        }

        /// <summary>
        /// FuelAmount
        /// </summary>
        [DisplayName("Monto")]
        [ExcelNoMappedColumn]
        [GridColumn(Title = "Monto", Width = "120px", SortEnabled = true)]
        public decimal FuelAmount { get; set; }

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Moneda")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string CurrencySymbol { get; set; }


        /// <summary>
        /// Fuel Amount as String
        /// </summary>
        [DisplayName("Monto")]
        [NotMappedColumn]
        public string FuelAmountStr
        {
            //get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(FuelAmount); }
            //get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AssignedAmount, CurrencySymbol); }
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(FuelAmount, CurrencySymbol); }
        }

        /// <summary>
        /// Odometer
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double Odometer { get; set; }

        /// <summary>
        /// Odometer format
        /// </summary>
        [DisplayName("Odómetro")]
        [GridColumn(Title = "Odómetro", Width = "120px", SortEnabled = true)]
        public string OdometerStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Convert.ToDecimal(Odometer)); } }


        /// <summary>
        /// Message
        /// </summary>
        [DisplayName("Mensaje")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string Message { get; set; }

        /// <summary>
        /// CreditCardHolder
        /// </summary>
        /// 
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardHolder { get; set; }

        /// <summary>
        /// CreditCard Format Number
        /// </summary>
        /// 
        [ExcelMappedColumn("#Tarjeta")]
        [NotMappedColumn]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// CreditCard With Format Number
        /// </summary>        
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardNumberDescrypt { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(CreditCardNumber); } }

        /// <summary>
        /// Card Number from Transaction Data
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public long CreditCardNumberTransaction { 
            get {
                return ((this.TransportDataObject != null && this.TransportDataObject.cardNumber != null) ? (long)this.TransportDataObject.cardNumber : 0);
            } 
        }

        /// <summary>
        /// Card Number from Transaction Data With Format
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardNumberTransactionWithFormat
        {
            get
            {
                return ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat(CreditCardNumberTransaction);
            }
        }

        /// <summary>
        /// Card Number from Transaction Data With MASK
        /// </summary>        
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardNumberTransactionWithMask
        {
            get
            {
                return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(CreditCardNumberTransactionWithFormat);
            }
        }

        /// <summary>
        /// Card Number from Transaction Data Encrypted
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CreditCardNumberTransactionEncrypted
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Encrypt(CreditCardNumberTransactionWithFormat);
            }
        }

        /// <summary>
        /// PaymentInstrumentCode
        /// </summary>
        public string PaymentInstrumentCode { get; set; }

        /// <summary>
        /// PaymentInstrumentType
        /// </summary>
        public string PaymentInstrumentType { get; set; }

        /// <summary>
        /// PaymentInstrument
        /// </summary>
        public string PaymentInstrument
        {
            get
            {
                if (PaymentInstrumentType != "Tarjeta")
                    return CreditCardNumber;
                else
                    return PaymentInstrumentCode;
            }
        }

        /// <summary>
        /// Transport Data
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string TransportData { get; set; }

        /// <summary>
        ///  Transport Data Obj
        /// </summary>
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public TransportDataModel TransportDataObject
        {
            get
            {
                var trans = this.TransportData == null ? "" : this.TransportData;
                return JsonConvert.DeserializeObject<TransportDataModel>(trans);
            }
        }

        /// <summary>
        /// InsertDate
        /// </summary>
        [DisplayName("Fecha Creada")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public DateTime? InsertDate { get; set; }

        /// <summary>
        /// ResponseCodeDescription
        /// </summary>
        [DisplayName("Codigo de Respuesta")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string ResponseCode { get; set; }

        /// <summary>
        /// TransactionId
        /// </summary>
        [DisplayName("descripcion Respuesta")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string ResponseCodeDescription { get; set; }

        /// <summary>
        /// CustomerId
        /// </summary>
        [DisplayName("Customer ID")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// CostCenterName
        /// </summary>
        [DisplayName("CostCenter Name")]
        [ExcelMappedColumn("Centro de Costo")]
        [NotMappedColumn]
        public string CostCenterName { get; set; }

        /// <summary>
        /// Terminal Id
        /// </summary>
        [ExcelMappedColumn("Terminal ID")]
        [NotMappedColumn]
        public string TerminalId { get; set; }

        /// <summary>
        /// DateToExport
        /// </summary>
        [DisplayName("Fecha Emision")]
        //[ExcelMappedColumn("Fecha Emision")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string DateToExport
        {
            get
            {
                return ((InsertDate != null) ? Convert.ToDateTime(InsertDate).ToString("dd/MM/yyyy hh:mm:ss tt") : string.Empty);
            }
        }

        /// <summary>
        /// Country Name
        /// </summary>
        [DisplayName("País")]
        [ExcelMappedColumn("País")]
        [GridColumn(Title = "País", Width = "120px", SortEnabled = true)]
        public string CountryName { get; set; }

        /// <summary>
        /// Total of Trx Traveled Odometer
        /// </summary>
        //[ExcelNoMappedColumn]
        [ExcelMappedColumn("Tipo de Cambio")]
        public string ExchangeValue { get; set; }
              
        /// <summary>
        /// Real Amount of Trx 
        /// </summary>
        [ExcelNoMappedColumn]
        public decimal? RealAmount { get; set; }

        /// <summary>
        /// Real Amount String
        /// </summary>
        [DisplayName("Monto Real")]
        [ExcelMappedColumn("Monto Real")]
        [GridColumn(Title = "Monto Real", Width = "120px", SortEnabled = true)]
        public string RealAmountFormatted
        {
            get { return RealAmount == null ? "N/A" : ((decimal)RealAmount).ToString("N", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); }
        }

        /// <summary>
        /// VehicleName
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// Validate if the MasterCard IsInternational
        /// </summary>
        public bool IsInternational { get; set; }
    }

    /// <summary>
    /// Real Vs Budget Fuels Report Base
    /// </summary>
    public class CurrentFuelsReportBase
    {    
        public CurrentFuelsReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<dynamic>();
            Menus = new List<AccountMenus>();
        }
     
        public ControlFuelsReportsBase Parameters { get; set; }

        public IEnumerable<dynamic> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

    }

     public enum ReportPreventiveMaintenanceStatus
    {
        /// <summary>
        /// Pendiente
        /// </summary>
        Pendientes = 1,
        /// <summary>
        ///Vencidos
        /// </summary>
        Vencidos = 2,
        /// <summary>
        /// Realizados
        /// </summary>
        Realizado = 3,
    };


    public class ControlFuelsReportsBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public ControlFuelsReportsBase()
        {
            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;
            StartDate = currentDate.Date;
            EndDate = currentDate.Date;
            CustomerId = -1;
            TransactionType = 1;
            ReportCriteriaId = (int?)ReportCriteria.DateRange;
            ReportFuelTypeId = (int?)ReportFuelTypes.Vehicles;
            ReportComparativeId = (int?)ReportComparativeTypes.Vehicles;
            ReportStatus = (int?)ReportPreventiveMaintenanceStatus.Pendientes;
            key = null;
        }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart titles
        /// </summary>
        public HtmlString Titles { get; set; }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart labels
        /// </summary>
        public HtmlString Labels { get; set; }

        /// <summary>
        /// Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString Colors { get; set; }

        /// <summary>
        /// Alpha Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString AlphaColors { get; set; }


        /// <summary>
        /// Highlight Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString HighlightColors { get; set; }

        /// <summary>
        /// Data formatted as JSON for second chart serie
        /// </summary>
        public HtmlString Data { get; set; }


        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date for Report when dates range is selected
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null)? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null)? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }

        /// <summary>
        /// Report Fuel Type Id
        /// </summary>
        public int? ReportFuelTypeId { get; set; }

        /// <summary>
        /// Report compartive Id
        /// </summary>
        public int? ReportComparativeId { get; set; }

        /// <summary>
        /// Report Status
        /// </summary>
        public int? ReportStatus { get; set; }

        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64 { get; set; }

        /// <summary>
        /// Key Filter Vehicle
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// Report Transaction Type
        /// </summary>
        public int TransactionType { get; set; }

        /// <summary>
        /// Report Vehicle filter
        /// </summary>
        public int FilterVehicle { get; set; }

        /// <summary>
        /// Setting Odometers Filter
        /// </summary>
        public int OdometerType { get; set; }

        /// <summary>
        /// Report Frequency Type Id
        /// </summary>
        public int? ReportFrequencyTypeId { get; set; }

        public int? RedLower { get; set; }

        /// <summary>
        /// Error margin of odometer with POS
        /// </summary>
        public int? RedHigher { get; set; }

        /// <summary>
        /// Tolerance time to get arrival to point
        /// </summary>
        public int? YellowLower { get; set; }

        /// <summary>
        /// Error Margin of geopoint
        /// </summary>
        public int? YellowHigher { get; set; }

        public int? GreenLower { get; set; }

        /// <summary>
        /// Passwords to Validate Repetition
        /// </summary>
        public int? GreenHigher { get; set; }

    }


    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class CurrentFuelsReport
    {
       
        [DisplayName("Monto Consumo Real por Unidad")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal RealAmount { get; set; }
      
        [DisplayName("Monto Asignado por Unidad")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal AssignedAmount { get; set; }

        [DisplayName("% Porcentaje")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal RealAmountPct { get; set; }

        [DisplayName("Moneda")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        [DisplayName("Monto Asignado por Unidad")]
        [ExcelMappedColumn("Monto Asignado por Unidad")]
        [NotMappedColumn]
        public string AssignedAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(AssignedAmount, CurrencySymbol); }
        }

        [DisplayName("Monto Consumo Real por Unidad")]
        [ExcelMappedColumn("Monto Consumo Real por Unidad")]
        [NotMappedColumn]
        public string RealAmountStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(RealAmount, CurrencySymbol); }
        }

        [DisplayName("% Porcentaje")]
        [ExcelMappedColumn("% Porcentaje")]
        [NotMappedColumn]
        public string RealAmountPctStr
        {         
           get { return ECOsystem.Utilities.Miscellaneous.GetPercentageFormat(RealAmountPct,0); }      
        }

    }

    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class CurrentFuelsByVehicleSubUnitReport : CurrentFuelsReport
    {
      
        [DisplayName("Centro de Costo")]
        [NotMappedColumn]
        public string CostCenterName { get; set; }

        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int? CostCenterId { get; set; }

        [NotMappedColumn]
        [ExcelNoMappedColumnAttribute]
        public List<dynamic> VehiclesDetail { get; set; }

    }

    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class CurrentFuelsByVehicleGroupReport : CurrentFuelsReport
    {

        [DisplayName("Grupo de Vehículos")]
        [ExcelMappedColumn("Grupo de Vehículos")]
        [NotMappedColumn]
        public string VehicleGroupName { get; set; }

    }

    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class CurrentFuelsByVehicleUnitsReport : CurrentFuelsReport
    {
        [DisplayName("Vehicle Unit Name")]
        [ExcelMappedColumn("Nombre Unidad")]
        [NotMappedColumn]
        public string VehicleUnitName { get; set; }

    }

    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class CurrentFuelsByVehicle : CurrentFuelsReport
    {
        [DisplayName("Matrícula")]
        [ExcelMappedColumn("Matrícula")]
        [NotMappedColumn]
        public string PlateNumber { get; set; }

        [DisplayName("Nombre Vehículo")]
        [ExcelMappedColumn("Nombre Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }

        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int? CostCenterId { get; set; }

    }
}