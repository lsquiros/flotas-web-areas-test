﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    public class CardRange
    {
        public int RangeId { get; set; }
        public int CustomerId { get; set; }
        public int? UserIdCreate { get; set; }
        public int? UserIdUpdate { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public bool IsDeleted { get; set; }
    }
}