﻿using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    /// <summary>
    /// Control Fuels Reports Base Model
    /// </summary>
    public class CustomerContractsReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public CustomerContractsReportBase()
        {
            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;
            ReportCriteriaId = (int?)ReportCriteria.Period;
            Menus = new List<AccountMenus>();
        }

        public IEnumerable<AccountMenus> Menus { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }
        public DateTime? EndDate { get; set; }
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }
       
        public int? ReportCriteriaId { get; set; }
        public int Classification { get; set; }
        public List<CustomerContractsReport> CustomerContractsList { get; set; }
    }


    public class CustomerContractsReport
    {
        public string PartnerName { get; set; }
        public string CustomerName { get; set; }
        public string DecryptedCustomerName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(CustomerName); } }
        public string Email { get; set; }
        public string DecryptedEmail { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(Email); } }
        public string LegalDocument { get; set; }
        public string MainPhone { get; set; }
        public string SecundaryPhone { get; set; }
        public string PlateId { get; set; }
        public string AVL { get; set; }
        public string CellPhone { get; set; }
        public string Chassis { get; set; }
        public string CreateDate { get; set; }
        public string Classiffication { get; set; }
        public string Since { get; set; }
        public string To { get; set; }
        public string ContractStatus { get; set; }
        public string HasGPS { get; set; }
        public int Renovations { get; set; }
        public string LastRenovation { get; set; }
        public string LastCancelation { get; set; }
        public string VehicleContractStatus { get; set; }
        public string SupportTicket { get; set; }
        public string OperationBAC { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }
        public DateTime? EndDate { get; set; }
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }
    }
}