﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    public class SMSBillReport
    {
        public string EncryptCustomerName { get; set; }

        public string DecryptCustomerName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptCustomerName); } }

        public int SMSTotal { get; set; }

        public decimal UnitAmount { get; set; }
                
        public decimal TotalAmount { get; set; }

        public DateTime EndDate { get; set; }
    }
}