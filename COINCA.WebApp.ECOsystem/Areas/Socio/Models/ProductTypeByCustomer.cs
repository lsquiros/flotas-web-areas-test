﻿
/************************************************************************************************************
*  File    : ProductTypeByCustomer.cs
*  Summary : ProductTypeByCustomer Models
*  Author  : Cindy Vargas
*  Date    : 27/07/2016
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ECOsystem.Models.Miscellaneous;

namespace ECO_Partner.Models
{
    public class ProductTypeByCustomer : ModelAncestor
    {
        /// <summary>
        /// PartnerId: Partner that is associated to Customer 
        /// </summary>
        public int? PartnerId { get; set; }

        /// <summary>
        /// ProductTypesId: Product Type associated to Partner
        /// </summary>
        public int? ProductTypesId { get; set; }
    }
}