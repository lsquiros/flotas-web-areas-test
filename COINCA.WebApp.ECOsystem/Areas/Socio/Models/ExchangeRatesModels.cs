﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    public class ExchangeRates
    {
        public decimal CurrentAmount { get; set; }

        public decimal NewAmount { get; set; }
    }
}