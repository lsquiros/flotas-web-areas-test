﻿using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ECO_Partner.Models
{
    public class CollectionZonesModels
    {
        /// <summary>
        /// Constructor CollectionZones
        /// </summary>
        public CollectionZonesModels()
        {
            Data = new TypeZone();
            List = new List<TypeZone>();
            Menus = new List<AccountMenus>();
        }
        /// <summary>
        /// Service statios model
        /// </summary>
        public TypeZone Data { get; set; }

        /// <summary>
        /// CollectionZonesList
        /// </summary>
        public IEnumerable<TypeZone> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        public int ZoneId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int? PartnerId{ get; set; }
    }

    public class TypeZone
    {
        public int? ZoneId { get; set; }

        [DisplayName("Nombre")]
        public string Name { get; set; }

        [DisplayName("Descripción")]
        public string Description { get; set; }

        public int? PartnerId { get; set; }
    }

}