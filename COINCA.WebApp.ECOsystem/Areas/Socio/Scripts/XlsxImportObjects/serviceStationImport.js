/**
 * Define Class for ServiceStation
 * and return obj for el controller. 
 * */
class ServiceStation {
    constructor(bacId, name, address, Legal_Id, TerminalId) {
        this.BacId = bacId;
        this.Name = name;
        this.Address = address;
        this.Legal_Id = Legal_Id;
        this.TerminalId = TerminalId;
    }

    mapServiceStation(csv) {
        var rows = csv.Sheet;
        ////rows.shift();
        return rows.map(
            r => new ServiceStation(r[0], r[1], r[2], r[3], r[4])
        );
    }
}