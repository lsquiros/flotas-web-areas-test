const type_xls = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" 
const titulo = "Estaciones de Servicio"
var txtFilexlsx = document.getElementById("txtFileXlsx")
var X = XLSX;
var XW = {
    /* worker message */
    msg: 'xlsx',
    /* worker scripts */
    worker: '../../../Scripts/XlsxImport/xlsxworker.js'
};

var global_wb;

(function () {
    let xlf = document.getElementById('xlf');

    if (!xlf.addEventListener)
        return;

    function handleFile(e) {
        do_file(e.target.files);
    }

    xlf.addEventListener('change', handleFile, false);

})();

const do_file = (function () {
    let rABS = true

    let xw = function xw(data, cb) {
        let worker = new Worker(XW.worker);
        worker.onmessage = function (e) {
            switch (e.data.t) {
                case 'ready': break;
                case 'e': console.error(e.data.d); break;
                case XW.msg: cb(JSON.parse(e.data.d)); break;
            }
        };
        worker.postMessage({ d: data, b: rABS ? 'binary' : 'array' });
    };

    return function do_file(files) {

        if (files[0].type != type_xls) {
            swal(titulo, "Favor de cargar un archivo de Excel valido", "warning")
            $("#txtFileXlsx").val("");
            return
        }

        rABS = true //domrabs.checked;
        use_worker = true //domwork.checked;
        let f = files[0];
        txtFilexlsx.value = files[0].name
        let reader = new FileReader();
        reader.onload = function (e) {
            if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
            let data = e.target.result;
            if (!rABS) data = new Uint8Array(data);
            if (use_worker) xw(data, process_wb);
            else process_wb(X.read(data, { type: rABS ? 'binary' : 'array' }));
        };
        if (rABS) reader.readAsBinaryString(f);
        else reader.readAsArrayBuffer(f);
    };
})();

var process_wb = (function () {
    let to_json = function to_json(workbook) {
        let result = {};
        workbook.SheetNames.forEach(function (sheetName) {
            let roa = X.utils.sheet_to_json(workbook.Sheets[sheetName],
                {
                    header: 1
                });
            if (roa.length) {
                roa.shift(); //delete first rows
                result["Sheet"] = roa
            }
        });

        sendController(result)

    };

    return function process_wb(wb) {
        global_wb = wb;
        let output = to_json(wb); //break;        
        return output //console.log(output)
        if (typeof console !== 'undefined') console.log("output", new Date());
    };

})();


async function sendController(csv) {
    try {

        if (csv.Sheet.length == 0) {
            await swal(titulo, "El archvio no tiene registros. Favor Validarlo", "warning")
            return
        }

        await ECOsystem.Utilities.ShowLoader();

        const st = new ServiceStation();
        const result = await st.mapServiceStation(csv);
        const result_str = JSON.stringify(result)

        const respuesta = await fetch("../AddOrEditServiceStationsImport", {
            headers: { "accept": "application/json", "content-type": "application/json" },
            method: "POST",
            body: JSON.stringify({ data: result_str })
        })

        const response = await respuesta.json()

        if (response.data.length > 0) {
            let objResponse = []
            for (let i = 0; i < response.data.length; i++) {
                const element = response.data[i];
                objResponse.push(new ServiceStation(element.BacId, element.Name, element.Address, element.Legal_Id, element.TerminalId))
            }

            jsonToCsvConvertor(objResponse, "EstacionesServicio", "Estaciones de Servicio NO registradas")
            response.status = "warning"
            response.message = "Se descarga un archivo con las Estaciones NO REGISTRADAS en el sistema para su validacion"
        }

        if (response.status == "error") {
            response.message = "Ah ocurrido un error inesperado, favor intentarlo nuevamente."
        }

        await swal(titulo, response.message, response.status)

    } catch (e) {
        await swal(titulo, "Existe un error en la carga del archivo", "error")
    } finally {
        await $('#ImportModal2').modal('hide');
        await ECOsystem.Utilities.HideLoader()
        $("#txtFileXlsx").val("");
    }

}

