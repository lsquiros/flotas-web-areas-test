﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.Customers = (function () {
    var options = {};
    var line = 0;

    var initialize = function (opts) {
        $.extend(options, opts);
        initButtons();
        fileApiUpload();
        initCheckUsername();
        initCurrencyInputs();
        initDates();
        initCsv();
        InitDropDownRoles();
        initDropDownList();
        initContract();
        initVehiclesClient();
        initCustomerBinnacle();
        initTrasladateVehicle();
        initTrasladateCustomer();
        generateAPIToken();

        $("#AddorEditTerminalRequestForm").validate();

        $('.withoutdecimal').on('keypress keyup blur', function (e) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((e.which < 48 || e.which > 57)) {
                e.preventDefault();
            }
        });

        $('.toggleCheck').bootstrapToggle({
            on: 'Activo',
            off: 'Inactivo'
        });

        $('#NameDate').keydown(function (e) {
            var currentValue = e.key;
            var finalValue = $(this).val() + currentValue;
            if (finalValue > 31) {
                e.preventDefault();
            }
        });

        //Init users
        try {
            $("#addOrEditUsersForm").data("validator").settings.submitHandler = function (e) {
                $('#detailUsersContainer').find('#processing').removeClass('hide');
                $('#detailUsersContainer').find('button').attr("disabled", "disabled");
            };
        } catch (e) { }


        //Validate checkbox to create client without email
        if ($("#chbxEnableClientWithoutEmail").attr("id") != undefined) {
            $("#chbxEnableClientWithoutEmail").change(function () {
                if (this.checked) {
                    $('#Email').attr("readonly", "readonly");
                } else {
                    $('#Email').removeAttr("readonly");
                }
            });
        }

        //Validate radio button for type year or type month in contracts
        if ($('input:radio[name=rdMonthOrYear]').length > 0) {
            $('input:radio[name=rdMonthOrYear]').change(function () {
                if (this.id == 'rdTypeMonth') {
                    $("#ContractYears").val("12");
                    $("#ContractYears").attr("min", "12");
                }
                else {
                    $("#ContractYears").val("1");
                    $("#ContractYears").attr("min", "1");
                }
            });
        }

        $('#ContractYears').keyup(function () {
            var minNumber = $("#ContractYears").attr("min");
            var currentNumber = $("#ContractYears").val();
            try {
                if (parseInt(minNumber) < parseInt(currentNumber)) {
                    $("#ContractYears").val(min)
                }
            } catch (e) {
                console.error(e);
            }
        });

        $('#CustomersByCustomersModal').on('hidden.bs.modal', function () {
            $('#treeview-checkable').html('');
        });

        $('#dbxRoleList').change(function () {
            $('#errorMessage').html('');
        });

        $("#NameDate").bind('keyup mouseup', function () {
            $("#NameDate").attr('value', $("#NameDate").val());
        });

        $('#Email').keyup(function () {
            var lowercase = $(this).val().toLowerCase();
            $(this).val(lowercase.replace(/\s/g, ''));
        });

        $('#DisplayAccountNumber').keypress(function (e) {
            var key = window.Event ? e.which : e.keyCode
            var val = $(this).val();
            if ((key >= 48 && key <= 57)) {
                val = val + e.key;
                if (val.length < 9 || val.length > 26) {
                    disableMasterCardButtons(true);
                    $('#accountNumberWarning').show();
                    $(this).focus();
                } else {
                    disableMasterCardButtons(false);
                    $('#accountNumberWarning').hide();
                }
                return true;
            } else {
                return false;
            }
        });

        $('#DisplayAccountNumber').off('blur.DisplayAccountNumber').on('blur.DisplayAccountNumber', function () {
            var val = $(this).val();
            if (val.length < 9 || val.length > 26) {
                disableMasterCardButtons(true);
                $('#accountNumberWarning').show();
                $(this).focus();
            } else {
                disableMasterCardButtons(false);
                $('#accountNumberWarning').hide();
            }
        });


        if ($("#isDemo").prop('checked')) {
            $("#datetimepickerIsDemoAlert").show();
            $("#lblDateDemo").show();
            $("#lbInfoDateDemo").css('display', 'inline-block');
        } else {
            $("#datetimepickerIsDemoAlert").hide();
            $("#lblDateDemo").hide();
            $("#lbInfoDateDemo").hide();
            $("#lbInfoDateDemo").css('display', 'none');
        }
    }

    var initCustomerCards = function () {
        initMasterCardDrops();

        $('.masterCard').off('blur.masterCard').on('blur.masterCard', function () {
            checkMasterCard($(this));
        });

        $('.btnCancelMasterCard').click(function () {
            var customerId = $(this).attr('customerId');
            $.ajax({
                url: '/Customers/CancelCustomerAddOrEdit',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify({ customerId: customerId }),
                success: function (result) {
                    $('#masterCardGrid').html(result);
                    initMasterCardDrops();
                    disableMasterCardButtons(false);
                }
            });
        });
    }

    var checkMasterCard = function (obj) {
        var current = obj.val();
        var val = current.split('-').join('');
        var showMsj = false;

        if (!isNaN(val)) {
            if (val.length == 16) {
                val = val.replace(/(\d{4})(\d{4})(\d{4})(\d{4})/, "$1-$2-$3-$4");
            } else if (val.length == 15) {
                val = val.replace(/(\d{4})(\d{6})(\d{5})/, "$1-$2-$3");
            } else {
                showMsj = true;
                val = current;
            }
        } else {
            showMsj = true;
            val = current;
        }
        obj.val(val);
        if (showMsj) {
            $('.mastercardformat').show();
            obj.focus();
            disableMasterCardButtons(true);
            return false;
        } else {
            $('.mastercardformat').hide();
            disableMasterCardButtons(false);
            return true;
        }
    }

    var disableMasterCardButtons = function (disabled) {
        $('#btnSaveCustomer2').attr('disabled', disabled);
        $('#btnSaveCustomer1').attr('disabled', disabled);
        $('#btnMasterCardLocal').attr('disabled', disabled);
        $('#btnMasterCardInternational').attr('disabled', disabled);
    }

    var initDropDownList = function () {
        var select1 = $("#StateId").select2().data('select2');
        if (select1 != undefined) {
            select1.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onStateChange(data); return fn.apply(this, arguments); }
                }
            })(select1.onSelect);
        }

        var select2 = $("#CountyId").select2().data('select2');
        if (select2 != undefined) {
            select2.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onCountyChange(data); return fn.apply(this, arguments); }
                }
            })(select2.onSelect);
        }

        var select3 = $("#CityId").select2().data('select2');
        if (select3 != undefined) {
            select3.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
                }
            })(select3.onSelect);
        }

        $('#ZoneId').select2();
        $('#CreditCardType').select2();
        $('#CurrencyId').select2();
        $('#IssueForId').select2().on('change', function () {
            var issueForId = $(this).val();
            if (issueForId === '100') {
                $('#costCenterDistributionContainer').hide();
                $('#CostCenterFuelDistribution').prop('checked', false);
            } else {
                $('#costCenterDistributionContainer').show();
            }
        });
        $('#CustomerCharges').select2();
        $('#GPSModality').select2();
        $('#CustomerCharges').select2();
        $('#GPSModality').select2();
        $('#CustomerManagerId').select2();
        $('#ContractYear').select2();
        $('#UnitOfCapacityId').select2();
        $('#CostCenterFuelDistribution').on('change', function () {
            var active = $(this).prop('checked');
            $('#distributionTypeLbl').html(active ? 'Centro de Costo' : 'Combustible');
            $('#FuelDistributionCostCenterConfirmation').modal('show');
        });
        $('#CancelCostCenterDistribution').click(function () {
            var active = $('#CostCenterFuelDistribution').prop('checked');

            $('#CostCenterFuelDistribution').bootstrapToggle(active ? 'off' : 'on');
            $('#CostCenterFuelDistribution').prop('checked', !active);
            $('#FuelDistributionCostCenterConfirmation').modal('hide');
        });
        if ($('#ClassificationId').attr("id") != undefined && $('#ClassificationId').attr("type") != "hidden")
            $('#ClassificationId').select2();

        //Additional Information data - Enable URL input
        enableURLInput((($("#EnableInputURL").val() == "1") ? true : false));
    };

    var initMasterCardDrops = function () {
        var select7 = $("#notInternational_Parameters_ReportCriteriaId").select2().data('select2');
        if (select7 != undefined) {
            select7.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
                }
            })(select7.onSelect);
        }

        var select8 = $("#notInternational_Parameters_Month").select2().data('select2');
        if (select8 != undefined) {
            select8.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
                }
            })(select8.onSelect);
        }

        var select9 = $("#notInternational_Parameters_Year").select2().data('select2');
        if (select9 != undefined) {
            select9.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
                }
            })(select9.onSelect);
        }

        $("#notInternational_StatusId").select2();
        $("#notInternational_ExpirationMonth").select2();
        $("#notInternational_ExpirationYear").select2();

        $("#international_StatusId").select2();
        $("#international_ExpirationMonth").select2();
        $("#international_ExpirationYear").select2();
    }

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");

            $('#notInternational_Parameters_ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
        }
    };

    var initContract = function () {

        $.validator.addMethod("askVehicules", function (value, element) {
            if ($("#isProductLineVitaECO").val() == "True") { //if it's an line vita user include the rule
                if ($('#ContractVehiclesContainer').find('input[type=checkbox]:checked').length > 0) {
                    $('#contractAlert').hide();
                    return true;
                } else {
                    $('#dangerContractMessage').hide();
                    $('#infoContractMessage').show();
                    $('#msgContractAlertInfo').html("Debe seleccionar al menos un vehículo.");
                    $('#contractAlert').show();
                    return false;
                }
            } else {
                return true; //if it's an flotas user omit the rule
            }
        }, "Debe seleccionar al menos un vehículo para el contrato.");

        //Initialize form $("#addOrEditContractClientForm").validate() rules
        try {
            $("#SinceStr").rules("add", {
                askVehicules: true
            });
            $("#ContractYears").rules("add", {
                askVehicules: true
            });
        } catch (e) { }

        //Hide state for the alert message
        $('#contractAlert').hide();

        //Add button trigger
        $("#btnAddContractByClient").click(function () {
            if ($("#countVehicles").val() > 0) { //
                $('#ContractId').val('');
                $('#ContractYears').val('1');
                $('#ContractYears').removeAttr('disabled');
                $('#SinceStr').val('');
                $('#SinceStr').removeAttr('disabled');
                $('#btnSendFormContractAddOrEdit').removeAttr('disabled');

                $('#contractText').html('crear');
                $('#RenewContractModal').modal('show');
            } else { //No vehicles for the client.
                $('#dangerContractMessage').hide();
                $('#infoContractMessage').show();
                $('#msgContractAlertInfo').html("Debe ingresar al menos un vehículo para ingresar contratos nuevos.");
                $('#contractAlert').show();
            }
        });

        $('#btnAddOrEditContract').click(function () {
            $('#addOrEditContractContainer').show('slow');
            $('#btnAddContractByClient').attr('disabled', 'disabled');
            $('#ContractGridContainer').addClass('removetop');
            $('.input-clean').val(''); //Clean form values
            $('.text-danger > span').html('') // Clean form messages

            $('#ContractControlsContainer').show('slow');
            $('#AddOrEditContract').val(true);
            $('#RenewContractModal').modal('hide');

            var contractId = $('#ContractId').val();
            var customerId = $('#CustomerId').val();
            showLoader();
            $.ajax({
                url: '/Customers/GetContractByClient',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify({ contractId: contractId, customerId: customerId }),
                success: function (result) {
                    hideLoader();

                    $(".ckeckVehicleId").prop("checked", false);

                    //Add
                    if (contractId == undefined || contractId == '') {
                        $(".ckeckVehicleId").removeAttr("disabled");
                        $("#chkCheckAll").removeAttr("disabled");

                        //If exist only 1 vehicle
                        if ($("#countVehicles").val() == 1) {
                            $("#chkCheckAll").prop("checked", true);
                            $(".ckeckVehicleId").prop("checked", true);
                        }

                        result.forEach(function (element) {
                            if (element.Disabled) {
                                $("#VehicleId-" + element.VehicleId).attr('disabled', 'disabled');
                                $("#chkCheckAll").attr('disabled', 'disabled');
                                //If exist only 1 vehicle
                                if ($("#countVehicles").val() == 1) {
                                    $("#chkCheckAll").prop("checked", false);
                                    $(".ckeckVehicleId").prop("checked", false);
                                }
                            }
                        });
                    } else {
                        //Renewed
                        $("#chkCheckAll").attr('disabled', 'disabled');
                        $(".ckeckVehicleId").attr('disabled', 'disabled');

                        result.forEach(function (element) {
                            if (element.Selected) {
                                $("#VehicleId-" + element.VehicleId).prop("checked", true);
                            }
                        });
                    }
                }
            });

        });

        //Cancel button trigger
        $('#btnCancelContractAddOrEdit').click(function () {
            $('#addOrEditContractContainer').hide('slow');
            $('#btnAddContractByClient').removeAttr('disabled');
            $('#ContractGridContainer').removeClass('removetop');
            $('#infoContractMessage').hide();
        });

        $('#btnSendFormContractAddOrEdit').click(function () {
            //Validate form
            if ($('#addOrEditContractClientForm').valid()) {
                $('#ConfirmAddOrEditContractCustomerChanges').modal('show'); //Confirm Modal
            }
        });

        //Confirm button trigger
        $('#btnConfirmAddOrEditContract').click(function () {
            addOrEditContractData();
        });

        //Confirm cancel/finish the contract
        $('#btnCancelCustomerContract').click(function () {
            showLoader();
            $('#CustomerContractCancelForm').submit();
            $('#CancelContractConfimationModal').modal('hide');
        });

        $('#ContractVehiclesContainer').off('click.chkCheckAll', '#chkCheckAll').on('click.chkCheckAll', '#chkCheckAll', function () {
            var check = $(this);
            $('#ContractVehiclesContainer').find('input[type=checkbox]').each(function () {
                if (check.is(':checked')) {
                    $(this).prop('checked', true);
                } else {
                    $(this).prop('checked', false);
                }
            });
        });

        $('#showContractInfo').click(function () {
            $('#ContractControlsContainer').show('slow');
        });



        $('#VehiclesGridContainer').off('click.order-vehicle-view', 'a[order-vehicle-view]').on('click.order-vehicle-view', 'a[order-vehicle-view]', function (e) {
            $('#LoadVehicleOrderForm').find('#VehicleId').val($(this).attr('id'));
            $('#LoadVehicleOrderForm').submit();
        });

        $('#ContractYears').keypress(function (e) {
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '45') {
                return false;
            }
        });
    }

    // Add Or Edit Vehicle: Send data for backend //
    var addOrEditContractData = function () {
        var modelData = {
            'ContractId': $('#ContractId').val(),
            'SinceStr': $('#SinceStr').val(),
            'ContractYears': $('#ContractYears').val(),
            'UseMonths': (($('#rdTypeYear').is(":checked") == true) ? false : true),
            'CustomerId': $('#CustomerId').val(),
            'Vehicles': getContractVehicles()
        }

        showLoader();
        $('#ConfirmAddOrEditContractCustomerChanges').modal('hide');

        $.ajax({
            url: '/Customers/AddOrEditContractByClient',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ model: modelData }),
            success: function (result) {
                if (result.indexOf('Success') > -1) {
                    window.location.reload();
                } else {
                    hideLoader();
                    if (result.indexOf('PLATE_IS_EXIST') > -1) {
                        $('#dangerContractMessage').hide();
                        $('#infoContractMessage').show();
                        $('#msgContractAlertInfo').html("Ya existe en plataforma un vehículo con la Placa digitada.");

                    } else if (result.indexOf('CHASIS_IS_EXIST') > -1) {
                        $('#dangerContractMessage').hide();
                        $('#infoContractMessage').show();
                        $('#msgContractAlertInfo').html("Ya existe en plataforma un vehículo con el Chasis digitado.");

                    } else {
                        $('#infoContractMessage').hide();
                        $('#dangerContractMessage').show();
                        $('#msgContractAlert').html(result);
                    }

                    $('#btnAddContractByClient').removeAttr('disabled'); //Enable button
                    $('#ConfirmAddOrEditContractCustomerChanges').modal('hide');
                    $('#contractAlert').show();
                }
            }
        });

    }

    // Get contract data
    var getContractData = function (contractId, operation, to, years) {

        if (operation === 'read') {
            $('.input-clean').val(''); //Clean form values

            showLoader();

            var customerId = $('#CustomerId').val();
            $('#ContractYears').val(years);
            $('#ContractYears').attr('disabled', 'disabled');
            $('#SinceStr').val(to.toString().split(' ')[0]);
            $('#SinceStr').attr('disabled', 'disabled');
            $('#btnSendFormContractAddOrEdit').attr('disabled', 'disabled');

            $.ajax({
                url: '/Customers/GetContractByClient',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify({ contractId: contractId, customerId: customerId }),
                success: function (result) {
                    hideLoader();
                    $('.text-danger > span').html('') // Clean form messages

                    $('#addOrEditContractContainer').show('slow');
                    $('#btnAddContractByClient').attr('disabled', 'disabled');
                    $('#ContractGridContainer').addClass('removetop');
                    $('.input-clean').val(''); //Clean form values
                    $('.text-danger > span').html('') // Clean form messages
                    $('#ContractControlsContainer').show('slow');

                    console.log(result);
                    $(".ckeckVehicleId").prop("checked", false);
                    $(".ckeckVehicleId").attr('disabled', 'disabled');
                    $("#chkCheckAll").attr('disabled', 'disabled');

                    result.forEach(function (element) {
                        if (element.Selected) {
                            $("#VehicleId-" + element.VehicleId).prop("checked", true);
                        }
                    });
                }
            });
        }

        if (operation === 'download') {
            $('#ContractReportId').val(contractId);
            $('#GetReportDataForm').submit();
        }
    };

    var editContractData = function (contractId, to, years) {
        $('#ContractId').val(contractId);
        $('#ContractYears').val(years);
        $('#ContractYears').removeAttr('disabled');
        $('#SinceStr').val(to.toString().split(' ')[0]);
        $('#SinceStr').removeAttr('disabled');
        $('#btnSendFormContractAddOrEdit').removeAttr('disabled');

        $('#contractText').html('renovar');
        $('#RenewContractModal').modal('show');
    }

    var InitDropDownRoles = function () {
        $('#dbxRoleList').select2('data', {});
        $('#dbxRoleList').select2({
            formatResult: RoleIdSelectFormat,
            formatSelection: RoleIdSelectFormat,
            escapeMarkup: function (m) { return m; }
        });
    };

    //Add or Edit Vehicles from Partner profile
    var initVehiclesClient = function () {
        //Hide state for the alert message
        $('#vehicleAlert').hide();

        //Add button trigger
        $("#btnAddVehicleByClient").click(function () {
            $('#addOrEditVehicleContainer').show('slow');
            $('#btnAddVehicleByClient').attr('disabled', 'disabled');
            $('#VehiclesGridContainer').addClass('removetop');
            $('.input-clean').val(''); //Clean form values
            $('#Odometer').val('0');
            $('.text-danger > span').html('') // Clean form messages
        });

        //Cancel button trigger
        $('#btnCancelVehicleAddOrEdit').click(function () {
            $('#addOrEditVehicleContainer').hide('slow');
            $('#btnAddVehicleByClient').removeAttr('disabled');
            $('#VehiclesGridContainer').removeClass('removetop');
            $('#infoVehicleMessage').hide();
        });

        $('#btnSendFormVehicleAddOrEdit').click(function () {

            let operation = document.getElementsByClassName('operation')

            if (operation.length == 0) {
                //Validate form
                if ($('#addOrEditVehicleClientForm').valid()) {

                    //Confirm Modal
                    $('#ConfirmAddOrEditVehicleCustomerChanges').modal('show');

                    //Add button state
                    $('#btnAddVehicleByClient').removeAttr('disabled');
                }
            }
            else {
                if (operation[0].value == "" && ($('#ClassificationId').val() == "1" || $('#ClassificationId').val() == "2")) {
                    alert('No puede dejar el Número de Operación BAC en Blanco')
                }
                else {
                    if ($('#addOrEditVehicleClientForm').valid()) {

                        //Confirm Modal
                        $('#ConfirmAddOrEditVehicleCustomerChanges').modal('show');

                        //Add button state
                        $('#btnAddVehicleByClient').removeAttr('disabled');
                    }
                }
            }
        });

        //Confirm button trigger
        $('#btnConfirmAddOrEditVehicle').click(function () {
            addOrEditVehicleData();
        });

        //Initialize validate plugin for the form
        $('#addOrEditVehicleClientForm').validate();

        //Initialize to Upper and Capital case text trigger
        $('.data-uppercase').keyup(function () {
            $(this).val($(this).val().toUpperCase());
        });

        $('.data-capitalcase').keyup(function () {
            var text = $(this).val();
            text = text.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                return letter.toUpperCase();
            });
            $(this).val(text);
        });
    }

    //Trasladate Vehicle between Customers
    var initTrasladateVehicle = function () {
        //Cancel button trigger
        $('#btnCancelTrasladate').click(function () {
            $('#trasladateVehicleClientContainer').hide('slow');
            $('#btnAddVehicleByClient').removeAttr('disabled');
            $('#VehiclesGridContainer').removeClass('removetop');
            $('#infoVehicleMessage').hide();
        });

        //Validate form
        $('#btnSendVehicleToCustomer').click(function () {
            //Validate form
            if ($("#customerList").val() > 0) {

                var selectedInfo = $("#customerList option:selected").val() + " - " + $("#customerList option:selected").text();

                //Confirm info client
                $("#clientInfoConfirm").html(selectedInfo);

                //Confirm Modal
                $('#ConfirmTrasladateVehicleToCustomer').modal('show');

                //Add button state
                $('#btnAddVehicleByClient').removeAttr('disabled');
            } else {
                $("#msgVehicleAlertInfo").html("Por favor selecciona el cliente a donde se desea trasladar el vehículo.");
            }
        });


        //Confirm button trigger
        $('#btnConfirmTrasladateVehicle').click(function () {
            $("#newCustomerId").val($("#customerList option:selected").val());

            $("#trasladateVehicleClientForm").submit();
        });
    }

    //Trasladate Customer between Partners
    var initTrasladateCustomer = function () {
        //Cancel button trigger
        $('#btnCancelTrasladate').click(function () {
            $('#trasladateClientPartnerContainer').hide('slow');
        });

        //Validate form
        $('#btnSendCustomerToPartner').click(function () {
            //Validate form
            if ($("#partnerList").val() > 0) {

                var selectedInfo = $("#partnerList option:selected").val() + " - " + $("#partnerList option:selected").text();

                //Confirm info partner
                $("#partnerInfoConfirm").html(selectedInfo);

                //Confirm Modal
                $('#ConfirmTrasladateCustomerToPartner').modal('show');
            } else {
                ECOsystem.Utilities.SetMessageShow("Por favor selecciona el socio a donde se desea trasladar el cliente.", "INFO");
            }
        });


        //Confirm button trigger
        $('#btnConfirmTrasladateCustomer').click(function () {
            $("#newPartnerId").val($("#partnerList option:selected").val());

            $("#trasladateClientPartnerForm").submit();
        });
    }

    //Add Or Edit Vehicle: Send data for backend //
    var addOrEditVehicleData = function () {
        var modelData = {
            'CustomerId': $('#CustomerId').val(),
            'VehicleId': $('#VehicleId').val(),
            'PlateId': $('#PlateId').val(),
            'Chassis': $('#Chassis').val(),
            'Name': $('#Name').val(),
            'Manufacturer': $('#Manufacturer').val(),
            'VehicleModel': $('#VehicleModel').val(),
            'VehicleYear': $('#VehicleYear').val(),
            'Colour': $('#Colour').val(),
            'AVL': $('#AVL').val(),
            'Odometer': $('#Odometer').val(),
            'ClassificationId': (($("#ClassificationId").attr("id") != undefined) ? (($("#ClassificationId").val().length == 0) ? $("#ClassificationId option:contains(Ninguno)").val() : $("#ClassificationId").val()) : $("#ClassificationId option:contains(Ninguno)").val()),
            'SupportTicket': $('#SupportTicket').val(),
            'SupportTicketURL': $('#SupportTicketURL').val(),
            'OperationBAC': $('#OperationBAC').val(),
            //'FleetioId' : $('#FleetioId').val()
        }

        showLoader();
        $('#ConfirmAddOrEditVehicleCustomerChanges').modal('hide');

        $.ajax({
            url: '/Customers/AddOrEditVehicleByClient',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ model: modelData }),
            success: function (result) {
                if (result.indexOf('Success') > -1) {
                    window.location.reload();
                } else {
                    hideLoader();
                    if (result.indexOf('PLATE_IS_EXIST') > -1) {
                        $('#dangerVehicleMessage').hide();
                        $('#infoVehicleMessage').show();
                        $('#msgVehicleAlertInfo').html("Ya existe en plataforma un vehículo con la Placa digitada.");

                    } else if (result.indexOf('CHASIS_IS_EXIST') > -1) {
                        $('#dangerVehicleMessage').hide();
                        $('#infoVehicleMessage').show();
                        $('#msgVehicleAlertInfo').html("Ya existe en plataforma un vehículo con el Chasis digitado.");

                    } else {
                        $('#infoVehicleMessage').hide();
                        $('#dangerVehicleMessage').show();
                        $('#msgVehicleAlert').html(result);
                    }

                    $('#btnAddVehicleByClient').attr('disabled', 'disabled');
                    $('#ConfirmAddOrEditVehicleCustomerChanges').modal('hide');
                    $('#vehicleAlert').show();
                }
            }
        });

    }

    // Get Vehicle 
    var getVehicleData = function (vehicleId) {

        $('.input-clean').val(''); //Clean form values

        showLoader();

        var customerId = $('#CustomerId').val();

        $.ajax({
            url: '/Customers/GetVehicleByClient',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ vehicleId: vehicleId, customerId: customerId }),
            success: function (result) {
                hideLoader();
                $('.text-danger > span').html('') // Clean form messages

                if (result.VehicleId != undefined && result.VehicleId > 0) {
                    $('#VehicleId').val(vehicleId);
                    $('#PlateId').val(result.PlateId);
                    $('#Chassis').val(result.Chassis);
                    $('#Name').val(result.Name);
                    $('#Manufacturer').val(result.Manufacturer);
                    $('#VehicleModel').val(result.VehicleModel);
                    $('#VehicleYear').val(result.VehicleYear);
                    $('#Colour').val(result.Colour);
                    $('#AVL').val(result.AVL);
                    $('#Odometer').val(result.OdometerVehicle);
                    $('#SupportTicket').val(result.SupportTicket);
                    $('#SupportTicketURL').val(result.SupportTicketURL);
                    $('#OperationBAC').val(result.OperationBAC)
                    //$('#FleetioId').val(result.FleetioId)

                    if (result.SupportTicket != null && result.SupportTicket.length > 0) {
                        $("#EnableInputURL").val("0");
                        $('#UriSupportLink').attr('href', result.SupportTicketURL);
                        $('#UriSupportLink').html(result.SupportTicket);
                    }

                    if ($('#ClassificationId').attr("id") != undefined && $('#ClassificationId').attr("type") != "hidden")
                        $('#ClassificationId').val(result.ClassificationId).trigger("change");

                    //Show form
                    $('#addOrEditVehicleContainer').show('slow');
                    $('#btnAddVehicleByClient').attr('disabled', 'disabled');
                    $('#VehiclesGridContainer').addClass('removetop');

                    //Enable / Disable URI Input
                    enableURLInput((($("#EnableInputURL").val() == "1") ? true : false))
                }
                else {
                    $('#msgVehicleAlert').html(result);
                    $('#vehicleAlert').show();
                }
            }
        });
    };

    // Trasladate Between Customers
    var trasladateBetweenCustomers = function (vehicleId) {
        $('.input-clean').val(''); //Clean form values
        var customerId = $('#CustomerId').val();

        showLoader();

        $.ajax({
            url: '/Customers/GetClientsToTrasladate',
            type: 'GET',
            contentType: "application/json",
            data: { vehicleId: vehicleId, currentCustomerId: customerId },
            success: function (result) {
                hideLoader();
                $('.text-danger > span').html('') // Clean form messages
                

                if (result.VehicleInfo != undefined && result.VehicleInfo.length > 0) {
                    $("#currentVehicleId").val(vehicleId);
                    $("#currentCustomerId").val(customerId);
                    $('#VehiclePlate').val(result.VehicleInfo);

                    if (result.CustomerList != undefined) {
                        $('#customerList').empty();

                        $.each(result.CustomerList, function (i, val) {
                            var option = new Option(val.CustomerName, val.CustomerId);
                            $('#customerList').append(option);
                        });
                    }

                    //Show form
                    $('#trasladateVehicleClientContainer').show('slow');
                    $('#btnAddVehicleByClient').attr('disabled', 'disabled');
                    $('#VehiclesGridContainer').addClass('removetop');
                }
                else {
                    $('#msgVehicleAlert').html(result);
                    $('#vehicleAlert').show();
                }

                //Apply Select2 to ComboBox
                $('#customerList').select2({
                    sorter: function (data) {
                        return data.sort(function (a, b) {
                            return a.CustomerName < b.CustomerName ? -1 : a.CustomerName > b.CustomerName ? 1 : 0;
                        });
                    }
                });
            }
        });
    };

    // Trasladate Between Partners
    var trasladateBetweenPartners = function (customerId) {
        $('.input-clean').val(''); //Clean form values

        showLoader();

        $.ajax({
            url: '/Customers/GetPartnersToTrasladate',
            type: 'GET',
            contentType: "application/json",
            data: { customerId: customerId },
            success: function (result) {
                hideLoader();

                

                if (result.ClientInfo != undefined && result.ClientInfo.length > 0) {
                    $("#currentCustomerId").val(customerId);
                    $('#CustomerInfo').val(result.ClientInfo);

                    if (result.PartnerList != undefined) {
                        $('#partnerList').empty();

                        $.each(result.PartnerList, function (i, val) {
                            var option = new Option(val.PartnerName, val.PartnerId);
                            $('#partnerList').append(option);
                        });
                    }

                    //Show form
                    $('#trasladateClientPartnerContainer').show('slow');
                }
                else {
                    ECOsystem.Utilities.SetMessageShow("Se encontró un error no contralado en el proceso actual. Por favor informar del evento a soporte.", "ERROR");
                }

                //Apply Select2 to ComboBox
                $('#partnerList').select2({
                    sorter: function (data) {
                        return data.sort(function (a, b) {
                            return a.PartnerName < b.PartnerName ? -1 : a.PartnerName > b.PartnerName ? 1 : 0;
                        });
                    }
                });

                //Stop reload page
                window.stop();
            }
        });
    };

    //Add Binnacle for Customer
    var initCustomerBinnacle = function () {
        $('#btnCustomerBinnacleSubmit').click(function () {
            //Validate form
            if ($('#addCustomerBinnacleForm').valid()) {

                //Confirm Modal
                $('#ConfirmAddCustomerBinnacle').modal('show');
            }
        });

        //Confirm button trigger
        $('#btnConfirmAddCustomerBinnacle').click(function () {
            addOrEditCustomerBinnacleData();
        });

        //Initialize validate plugin for the form
        $('#addCustomerBinnacleForm').validate();

        //Confirm button trigger
        $('#btnConfirmDeleteCustomerBinnacle').click(function () {
            addOrEditCustomerBinnacleData();
        });
    }

    // Add Customer Binnacle: Send data for backend //
    var addOrEditCustomerBinnacleData = function () {

        var id = $('#BinnacleId').val();
        var deleted = $("#BinnacleDelete").val();

        var modelData = {
            'Comment': $('#txtCommentBinnacle').val(),
            'CustomerId': $('#CustomerId').val(),
        }

        if (id != undefined && id != "") {
            modelData.Id = id;
            if (deleted != undefined && deleted != "" && deleted != "0") {
                modelData.Deleted = 1;
            }
        }

        showLoader();
        $('#ConfirmAddCustomerBinnacle').modal('hide');

        $.ajax({
            url: '/Customers/AddOrEditCustomerBinnacle',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ model: modelData }),
            success: function (result) {
                if (result.indexOf('Success') > -1) {
                    if (deleted != undefined && deleted != "" && deleted != "0") {
                        $('#ConfirmDeleteCustomerBinnacle').modal('hide');
                        hideLoader();
                    } else if (id != undefined && id != "" && id != "0") {
                        $('#BinnacleContainer').removeClass('hidden');
                        hideLoader();
                    }
                    $('#CustomerBinnacleId').val($('#CustomerId').val());
                    $('#BinnacleLoadId').val("0"); //Reload all

                    //Clean variables
                    $('#BinnacleId').val("0");
                    $("#BinnacleDelete").val("0");
                    $('#txtCommentBinnacle').val("");

                    $('#loadBinnacleForm').submit();
                } else {
                    hideLoader();
                    $('#dangerBinnacleMessage').show();
                    $('#msgBinnacleAlert').html(result);
                }
            }
        });

    }

    var editBinnacle = function (id) {
        var comment = $("#BinnacleInGrid-" + id).find("input[type=hidden]").val();
        $("#BinnacleId").val(id);
        $("#txtCommentBinnacle").val(comment);
        $("#BinnacleDelete").val(0);
        $('#BinnacleContainer').addClass('hidden');
    }

    var deleteBinnacle = function (id) {
        $("#BinnacleId").val(id);
        $("#BinnacleDelete").val(1);
        $('#ConfirmDeleteCustomerBinnacle').modal('show');
    }

    //Role format dropdown list
    var RoleIdSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-12">' + json.Name + '</div>' +
            '</div>';
    };

    var initCsv = function () {
        $('#UploadRequestContainer').off('hidden.bs.modal', '#importModal').on('hidden.bs.modal', '#importModal', function (e) {
            $('#UploadRequestContainer').html('');
        });

        $('#UploadRequestContainer').off('change.csvfile', '#csvFileInput').on('change.csvfile', '#csvFileInput', function (e) {
            handleFiles(this.files);
        });
    }

    var initDates = function () {
        $('#PaymentReference').keypress(function (e) {
            var a = [];
            var k = e.which;
            for (i = 48; i < 58; i++)
                a.push(i);
            if (!(a.indexOf(k) >= 0))
                e.preventDefault();
        })

        $('#datePickerEstimatedDelivery').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $("#EstimatedDeliveryStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $('#datetimepickerSince').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    }

    var initCurrencyInputs = function () {
        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        try {

            $('body').off('blur.currencyOnBlur', 'input[data-currency], input[datacurrency]').on('blur.currencyOnBlur', 'input[data-currency], input[datacurrency]', function (e) {
                var obj = $(this);
                obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
                obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));
            });
            $('body').find('input[data-currency], input[datacurrency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });

        } catch (e) {

        }
    };

    var initCheckUsername = function () {
    }

    /* Email validation to prevent an invalid submission*/
    var validateEmail = function () {
        setErrorMsj('', false);
        if ($('#TxtNewUserEmailAdd').val().length == 0) {
            return false;
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#TxtNewUserEmailAdd').val().match(email)) {
            return true;
        } else {
            setErrorMsj('Correo electrónico invalido', true);
            return false;
        }
    };

    /* Show or hide errors messages*/
    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj').html(msj);
            $('#validationErrors').removeClass('hide');
        } else {
            $('#errorMsj').html('');
            $('#validationErrors').addClass('hide');
        }
    }

    /*init Buttons*/
    var initButtons = function () {
        try {
            //trigger click on hidden file input
            $('#nInformacionAdicional').off('click.btnUpload', '#btnUpload').on('click.btnUpload', '#btnUpload', function (e) {
                $('#fileHidden').trigger('click');
                e.stopPropagation();
            });

        } catch (e) { }

        try {
            $('#gridCardRequestContainer').off('click.editCardRequest', 'a[edit-card-request]').on('click.editCardRequest', 'a[edit-card-request]', function (e) {
                $('#loadCustomerCardRequestForm').find('#CardRequestId').val($(this).attr('data-id'));
                $('#loadCustomerCardRequestForm').submit();
            });

            $('#gridCardRequestContainer').off('click.delCardRequest', 'a[del-card-request]').on('click.delCardRequest', 'a[del-card-request]', function (e) {
                var deleteForm = $('#deleteForm');
                deleteForm.find('#deleteId').val($(this).attr('data-id'));
                deleteForm.find('#deleteEntity').val("CARD_REQUEST");
                deleteForm.attr('data-ajax-update', '#gridCardRequestContainer');
            });

            //Edit user
            $('#tabscontainer').off('click.tab', 'a[clickable_tabs]').on('click.tab', 'a[clickable_tabs]', function (e) {
                GetCustomerBranchTreeData($('#CustomerId').val());
            });

            $('#gridUsersContainer').off('click.SetUser', 'a[set-user-row]').on('click.SetUser', 'a[set-user-row]', function (e) {
                $('#CustomersByCustomersModal').find('#CUserId').val($(this).attr('id'));
                $('#CustomersByCustomersModal').find('#CustomerName').html($(this).attr('data-CustomerName'));

                ECOsystem.Utilities.ShowLoader();
                GetRoleSelected();
                GetUserPermissions();
            });

            $('#gridUsersContainer').off('click.delUser', 'a[del-user-row]').on('click.delUser', 'a[del-user-row]', function (e) {
                $('#deleteModalCustomerUsers').find('#id').val($(this).attr('id'));
            });

            //Edit user
            $('#gridUsersContainer').off('click.editUser', 'a[edit-user-row]').on('click.editUser', 'a[edit-user-row]', function (e) {
                $('#loadUsersForm').find('#CustomerUserId').val($(this).attr('id'));
                $('#loadUsersForm').submit();
                showLoader();
            });

            $('#addOrEditForm').off('click.edit', '#btnEdit').on('click.edit', '#btnEdit', function (e) {
                $('#loadForm').submit();
            });
            $('#addOrEditForm').off('click.cancel-edit', '#btnCancel').on('click.cancel-edit', '#btnCancel', function (e) {
                $('#cancelForm').submit();
            });

            $('#page-content-wrapper').off('click.loadCustomerCardForm', '#btnAddCard').on('click.loadCustomerCardForm', '#btnAddCard', function (e) {
                $('#loadCustomerCardForm').find('#CustomerCardId').val("-1");
                $('#loadCustomerCardForm').submit();
            });

            $('#page-content-wrapper').off('click.btnAddCardRequest', '#btnAddCardRequest').on('click.btnAddCardRequest', '#btnAddCardRequest', function (e) {
                if (!$("#btnAddCardRequest").is(':disabled')) {
                    $("#btnAddCardRequest").attr("disabled", "disabled");
                    ECOsystem.Utilities.ShowLoader();
                    $('#loadCustomerCardRequestForm').find('#CardRequestId').val("-1");
                    $('#loadCustomerCardRequestForm').submit();

                    ECOsystem.Utilities.ApplyNumericInputs(); //Apply numeric inputs
                }
            });

            //Add New User
            $('#page-content-wrapper').off('click.loadUsersForm', '#btnAddUsers').on('click.loadUsersForm', '#btnAddUsers', function (e) {
                $('#loadUsersForm').find('#CustomerUserCustId').val();
                $('#loadUsersForm').find('#CustomerUserId').val('');
                $('#loadUsersForm').submit();
                showLoader();
            });

            $('#page-content-wrapper').off('click.loadTerminalRequestForm', '#btnCreateTerminal').on('click.loadTerminalRequestForm', '#btnCreateTerminal', function (e) {
                $('#loadTerminalRequestForm').submit();
            });

            $('#page-content-wrapper').off('click.btnAddCardRequestFull', '#btnAddCardRequestFull').on('click.btnAddCardRequestFull', '#btnAddCardRequestFull', function (e) {
                $('#LoadUploadModalForm').submit();
            });
        } catch (e) { }

        $('#isDemo').on('change', function () {
            if ($(this).prop('checked')) {
                $('#IsDemoAlarmPanel').removeClass('hidden');
                var d = new Date();
                var datestring = ("0" + d.getDate()).slice(-2) + "/" + ("0" + (d.getMonth() + 2)).slice(-2) + "/" + d.getFullYear();
                $('#StartDateStr').val(datestring);
                $('#StartDateStr').removeAttr('disabled');
                $("#datetimepickerIsDemoAlert").show();
                $("#lblDateDemo").show();
                $("#lbInfoDateDemo").css('display', 'inline-block');
            } else {
                $('#IsDemoAlarmPanel').addClass('hidden');
                $('#StartDateStr').val('');
                $("#StartDateStr").attr('disabled', 'disabled');
                $("#datetimepickerIsDemoAlert").hide();
                $("#lblDateDemo").hide();
                $("#lbInfoDateDemo").css('display', 'none');
            }
        });

        $('#datetimepickerIsDemoAlert').datepicker({
            language: 'es',
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
            beforeShowDay: function (date) {
                return date > new Date();
            }
        });

        $("#StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $('#StartDateStr').blur(function (e) {
            var dayArray = $('#StartDateStr').val().split('/');
            var day = new Date(dayArray[1] + '/' + dayArray[0] + '/' + dayArray[2]);
            if (day < new Date()) {
                $('#StartDateStr').val('');
            }
        });

        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $("#notInternational_Parameters_StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });


        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $("#notInternational_Parameters_EndDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $('#VehiclesTab').find('#btnVehicleSearch').click(function () {
            var key = $('#txtVehicleSearch').val();
            searchVehicles(key);
        });

        $('#VehiclesTab').find('#txtVehicleSearch').keypress(function (e) {
            if (e.which == 13) {
                var key = $(this).val();
                searchVehicles(key);
            }
        })

        $('#contractTabs').find('#btnContractSearch').click(function () {
            var key = $('#txtContractSearch').val();
            searchContracts(key);
        });

        $('#contractTabs').find('#txtContractSearch').keypress(function (e) {
            if (e.which == 13) {
                var key = $(this).val();
                searchContracts(key);
            }
        });

        //Copy token to clipboard
        $('#APIToken').dblclick(function () {
            $(this).select();
            document.execCommand("copy");
        });

        $("#txtSearchCustomerBranch").keypress(function (obj) {
            if (obj.keyCode != 13) {
                var text = $(this).val() + obj.key;
                $(".node-treeview-checkable").each(function () {
                    var treeview = this;
                    var customerName = $(treeview).attr("customername");

                    if (typeof customerName !== 'undefined') {

                        if (customerName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                            $(treeview).show("slow");
                            $(treeview).find('.node-treeview-checkable').show('slow');
                            $(this).find('.generateAllCardsBranch').show("slow");
                        } else {
                            var flag = 0;
                            var Name = $(this).attr("customername");

                            if (typeof Name !== 'undefined') {

                                if (Name.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    $(this).find('.generateAllCardsBranch').show("slow");
                                    flag = 1;
                                }
                                else {
                                    $(this).hide("slow");
                                    $(this).find('.generateAllCardsBranch').hide("slow");
                                }
                            }
                        }
                    }
                });
            }
        });


        $("#txtSearchCustomerBranch").keydown(function (obj) {
            if (obj.keyCode == 8) {
                var text = $(this).val().substring(0, $(this).val().length - 1);
                $(".node-treeview-checkable").each(function () {
                    var treeview = this;
                    var customerName = $(treeview).attr("customername");

                    if (typeof customerName !== 'undefined') {

                        if (customerName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                            $(treeview).show("slow");
                            $(treeview).find('.node-treeview-checkable').show('slow');
                            $(this).find('.generateAllCardsBranch').show("slow");
                        } else {
                            var flag = 0;
                            var Name = $(this).attr("customername");

                            if (typeof Name !== 'undefined') {

                                if (Name.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    $(this).find('.generateAllCardsBranch').show("slow");
                                    flag = 1;
                                }
                                else {
                                    $(this).hide("slow");
                                    $(this).find('.generateAllCardsBranch').hide("slow");
                                }
                            }
                        }
                    }
                });
            }
        });
    }

    var validateInputValues = function (emails) {
        if (emails != "") {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            var emailList = emails.split(';');

            for (i = 0; i < emailList.length; i++) {
                if (!re.test(emailList[i].trim())) {
                    return false;
                }
            }
        }
        return true;
    };

    /*on Select Report Criteria Change*/
    var onSearchSelectChange = function (obj) {
        if (obj.id === '') {
            $('input[name="CountryId"]').removeAttr('value');
            $('#searchForm').submit();
        } else {
            $('input[name="CountryId"]').val(obj.id);
            $('#searchForm').submit();
        }
    };

    var subdelterminal = function () {
        $('#deleteTForm').submit();
    };

    /* Standard Error Function for File API */

    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };

    var fileApiUpdateProgress = function (e) {
        if (e.lengthComputable) {
            var percentLoaded = Math.round((e.loaded / e.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                $('.percent').css('width', percentLoaded + '%').html(percentLoaded + '%');
            }
        }
    };

    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            //trigger click on hidden file input

            $('#nInformacionAdicional').off('change.fileHidden', '#fileHidden').on('change.fileHidden', '#fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onprogress = fileApiUpdateProgress;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {
                    $('#imgLogo').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        alert("La extensión del archivo es inválida");
                        continue;
                    }
                    // Closure to capture the file information.
                    reader.onload = (function (F) {
                        $('.percent').html('100%').css('width', '100%');
                        setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgLogo').removeClass('imgloading');", 2000);
                        return function (e) {
                            var lsrc = e.target.result;
                            $('#imgLogo').attr({
                                src: lsrc,
                                title: escape(F.name)
                            });
                            $('#Logo').val(lsrc);
                        };
                    })(f);
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
            });
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.');
        }
    };

    var onSuccessLoad = function () {
        initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    var onSuccessLoadEdit = function () {
        $('#addOrEditForm').find('button[data-read-only-info]').addClass('hide');
        $('#addOrEditForm').find('button[data-read-write-info]').removeClass('hide');
        $('#addOrEditForm').find('select').not("[custom='true']").select2();
        hideLoader();
    };

    var onSuccessEdit = function (data) {
        if (data = "Success") {
            window.location.replace('/Socio/Customers/Index');
            ECOsystem.Utilities.HideLoader();
        }
    };

    /**/
    var onSuccessLoadCard = function () {
        $("#addOrEditCardModal").find('select').not("[custom='true']").select2();

        $('#addOrEditCardModal').find('#DisplayCreditCardNumber').alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '-',
            maxLength: 19
        });

        initCurrencyInputs();

        $('#addOrEditCardModal').modal('show');
    };

    //Partial Add Users
    var onSuccessLoadUsers = function () {
        $('#addOrEditUsersModal').modal('show');
        hideLoader();
    };

    var onSuccessAddEditUsers = function () {
        $('#addOrEditUsersModal').modal('hide');
        ECOsystem.Utilities.HideLoader();
    };

    var onSuccessUpdateCard = function (data) {
        $('#CustomerBudgetTab').find('#masterCardGrid').html(data);
    };

    /*onSuccessLoadCardRequest*/
    var onSuccessLoadCardRequest = function () {
        $("#addOrEditCardRequestModal").find('select').not("[custom='true']").select2();
        initDates();

        $('#addOrEditCardRequestModal').off('click.btnPaste', '#btnPaste').on('click.btnPaste', '#btnPaste', function (e) {
            $('#loadPreviousCardRequestForm').submit();
        });

        initDropDownList();

        $('#addOrEditCardRequestModal').removeData('validator');
        $.validator.unobtrusive.parse($('#addOrEditCardRequestModal'));

        $('#addOrEditCardRequestModal').modal('show');

        ECOsystem.Utilities.HideLoader();
        $('#btnAddCardRequest').removeAttr('disabled');
    };

    var onSuccessTerminalRequest = function (data) {
        if (data.result == false) {
            ECOsystem.Utilities.SetMessageShow("Ya existe esta terminal.", "ERROR");
        } else {
            $("#gridContainerT").html(data);
            $("#txtTerminal").val('');
            $("#txtDescripcionTerminal").val('');
        }

    };

    var onBeginTerminalRequest = function () {
        if ($("#txtTerminal").val().trim() == '') {
            if ($('#validationMessage').length > 0)
                return false;
            var validationMessage = '<span id="validationMessage" class="text-danger field-validation-error" data-valmsg-for="Terminal" data-valmsg-replace="true">';
            validationMessage += '<span for="Terminal" class="">ID de la Terminal es requerido.</span>';
            validationMessage += '</span>';
            $(validationMessage).insertAfter($("#txtTerminal"));
            return false;
        }
        else
            $('#validationMessage').remove();

        return true;
    };

    var onSuccessAddorEditTerminal = function () {
        $('#addOrEditTerminalModal').modal('hide');
    };

    /*on Success Load Upload Request*/
    var onSuccessLoadUploadRequest = function () {
        $('#importModal').find(":file").filestyle({
            buttonText: "&nbsp;Seleccionar Archivo...",
            buttonName: "btn-warning",
            buttonBefore: true,
        });
        $('#importModal').modal('show');
    };

    var importModalClose = function () {
        $('#importModal').modal('hide');
    }

    /*on Success Upload Check*/
    var onSuccessUploadCheck = function (data) {
        alert(data.message);
    };

    /*onSuccessUpdateCardRequest*/
    var onSuccessUpdateCardRequest = function () {
        ECOsystem.Utilities.HideLoader();
        $('#addOrEditCardRequestModal').modal('hide');
    };

    var onSuccessLoadPreviousCardRequest = function (data) {
        if (data.result) {
            if (data.cardRequest) {
                $('#StateId').val(data.cardRequest.StateId);
                $('#StateId').select2('val', data.cardRequest.StateId);
                if (data.listCounties != null) {
                    loadCounties(data.listCounties);
                }
                $('#CountyId').val(data.cardRequest.CountyId);
                $('#CountyId').select2('val', data.cardRequest.CountyId);
                if (data.listCities != null) {
                    loadCities(data.listCities);
                }
                $('#CityId').val(data.cardRequest.CityId);
                $('#CityId').select2('val', data.cardRequest.CityId);
                $('#AddressLine1').val(data.cardRequest.AddressLine1);
                $('#AddressLine2').val(data.cardRequest.AddressLine2);
                $('#AuthorizedPerson').val(data.cardRequest.AuthorizedPerson);
                $('#EstimatedDeliveryStr').val(data.cardRequest.EstimatedDeliveryStr);
                $('#ContactPhone').val(data.cardRequest.ContactPhone);
            }
        }
    }

    var onStateChange = function (obj) {
        $('#CityId').select2('val', null);
        $('#CountyId').select2('val', null);
        $('#loadCountiesForm').find("input[name=stateId]").val(obj.id);
        $('#loadCountiesForm').submit();
    };

    var onCountyChange = function (obj) {
        $('#CityId').select2('val', null);
        $('#loadCitiesForm').find("input[name=countyId]").val(obj.id);
        $('#loadCitiesForm').submit();
    };

    var onSuccessLoadCounties = function (data) {
        if (data.result) {
            loadCounties(data.listCounties);
        }
    }

    var onSuccessLoadCities = function (data) {
        if (data.result) {
            loadCities(data.listCities);
        }
    }

    var loadCounties = function (options) {

        $('#CountyId').html("");
        $('#CountyId')
            .append($("<option></option>")
                .text("Seleccione Cantón"));
        $.each(options, function (index, item) {
            $('#CountyId')
                .append($("<option></option>")
                    .attr("value", item.id)
                    .text(item.text));
        });
    }

    var loadCities = function (options) {

        $('#CityId').html("");
        $('#CityId')
            .append($("<option></option>")
                .text("Seleccione Distrito"));
        $.each(options, function (index, item) {
            $('#CityId')
                .append($("<option></option>")
                    .attr("value", item.id)
                    .text(item.text));
        });
    }

    function handleFiles(files) {
        // Check for the various File API support.
        if (window.FileReader) {
            // FileReader are supported.
            getAsText(files[0]);
        } else {
            alert('FileReader are not supported in this browser.');
        }
    }

    function getAsText(fileToRead) {
        var reader = new FileReader();
        // Handle errors load
        reader.onload = loadHandler;
        reader.onerror = errorHandler;
        // Read file into memory as UTF-8      
        reader.readAsText(fileToRead);
    }

    function loadHandler(event) {
        var csv = event.target.result;
        processData(csv);
    }

    function processData(csv) {
        var allTextLines = csv.split(/\r\n|\n/);
        var lines = [];
        while (allTextLines.length) {
            lines.push(allTextLines.shift().split(','));
        }
        drawOutput(lines);
    }

    function errorHandler(evt) {
        if (evt.target.error.name == "NotReadableError") {
            alert("Canno't read file !");
        }
    }

    function drawOutput(lines) {
        //Clear previous data
        $('#csvOutput').html('');

        var result = '<table class="table table-striped">';

        result += getOutputHeader();
        result += "<tbody>";
        line = 0;
        for (var i = 1; i < lines.length; i++) {

            result += getOutputRow(lines[i], i);
        }
        if (lines.length < 2) {
            result += "<tr><td colspan='10' class='text-danger'>Error procesando información</td></tr>";
        }

        result += '</tbody></table>';

        $('#csvOutput').html(result);

        $('#importModal').find('button:submit').removeAttr('disabled');
    }

    function getOutputHeader() {
        return '<thead><tr>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Placa</span></div></th>' +
            '<th class="grid-header" ><div class="grid-header-title"><span>Conductor</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Identificación</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Provincia</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Cantón</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Distrito</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Dirección de Entrega</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Dirección Adicional</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Ref. Pago</span></div></th>' +
            '<th class="grid-header" ><div class="grid-header-title"><span>Autorizado</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Teléfono</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Entrega</span></div></th>' +
            '<th class="grid-header" nowrap></th>' +
            '</tr></thead>';
    }

    function getOutputRow(lines, currentLine) {
        try {
            //text-success  text-danger
            var result = "";
            var tipoDeEmision = $("#CurrentTipoEmision").val();

            if (lines.length != 12) {
                result = "<span class='text-danger'><i class='mdi mdi-close mdi-18px'></i>&nbsp;&nbsp;Número de columnas en el archivo diferente al esperado</span>";
            } else if (parseDate(lines[11]) == undefined) {
                result = "<span class='text-danger'><i class='mdi mdi-close mdi-18px'></i>&nbsp;&nbsp;Formato de fecha incorrecto</span>";
            }

            if (result == "") {

                if (tipoDeEmision == "100" && lines[1] == "") { //Emision es por Conductor

                    return "<tr><td colspan='13' class='text-danger'><i class='mdi mdi-close mdi-18px'></i>&nbsp;&nbsp;Nombre Conductor es requerido, el Socio emite tarjetas por Conductor -- Número de linea: " + currentLine + "</td></tr>"

                } else if (tipoDeEmision == "101" && lines[0] == "") { //Emision es por Vehiculo

                    return "<tr><td colspan='13' class='text-danger'><i class='mdi mdi-close mdi-18px'></i>&nbsp;&nbsp;Placa de Vehiculo es requerido, el Socio emite tarjetas por Vehículo -- Número de linea: " + currentLine + "</td></tr>"
                }

                return '<tr>' +
                    '<td class="grid-cell" nowrap>' + lines[0] + '</td>' +
                    '<td class="grid-cell">' + lines[1] + '</td>' +
                    '<td class="grid-cell">' + lines[2] + '</td>' +
                    '<td class="grid-cell">' + lines[3] + '</td>' +
                    '<td class="grid-cell">' + lines[4] + '</td>' +
                    '<td class="grid-cell">' + lines[5] + '</td>' +
                    '<td class="grid-cell">' + lines[6] + '</td>' +
                    '<td class="grid-cell">' + lines[7] + '</td>' +
                    '<td class="grid-cell">' + lines[8] + '</td>' +
                    '<td class="grid-cell">' + lines[9] + '</td>' +
                    '<td class="grid-cell">' + lines[10] + '</td>' +
                    '<td class="grid-cell">' + lines[11] + '</td>' +
                    '<td class="grid-cell"><i class="text-success glyphicon glyphicon-ok"></i></td>' +
                    '</tr>' +
                    '<input type="hidden" name="list[' + line + '].PlateId" value="' + lines[0] + '">' +
                    '<input type="hidden" name="list[' + line + '].DriverName" value="' + lines[1] + '">' +
                    '<input type="hidden" name="list[' + line + '].DriverIdentification" value="' + lines[2] + '">' +
                    '<input type="hidden" name="list[' + line + '].DeliveryState" value="' + lines[3] + '">' +
                    '<input type="hidden" name="list[' + line + '].DeliveryCounty" value="' + lines[4] + '">' +
                    '<input type="hidden" name="list[' + line + '].DeliveryCity" value="' + lines[5] + '">' +
                    '<input type="hidden" name="list[' + line + '].AddressLine1" value="' + lines[6] + '">' +
                    '<input type="hidden" name="list[' + line + '].AddressLine2" value="' + lines[7] + '">' +
                    '<input type="hidden" name="list[' + line + '].PaymentReference" value="' + lines[8] + '">' +
                    '<input type="hidden" name="list[' + line + '].AuthorizedPerson" value="' + lines[9] + '">' +
                    '<input type="hidden" name="list[' + line + '].ContactPhone" value="' + lines[10] + '">' +
                    '<input type="hidden" name="list[' + (line++) + '].EstimatedDelivery" value="' + lines[11] + '">';
            } else {
                return "<tr><td colspan='13' class='text-danger'>" + result + " -- Número de linea: " + line + "</td></tr>";
            }
        } catch (e) {
            return "<tr><td colspan='13' class='text-danger'>Error procesando información</td></tr>";
        }

    }

    function parseDate(str) {
        try {
            // validate year as 4 digits, month as 01-12, and day as 01-31 
            if ((str = str.match(/^(\d{4})(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01])$/))) {
                // make a date
                str[0] = new Date(+str[1], +str[2] - 1, +str[3]);
                // check if month stayed the same (ie that day number is valid)
                if (str[0].getMonth() === +str[2] - 1)
                    return str[0];
            }
            return undefined;
        } catch (e) {
            return undefined;
        }
    }

    function saveCustomersByCustomer() {
        ECOsystem.Utilities.ShowLoader();

        var checked = $('#CustomersByCustomersModal').find('input[type=checkbox][id!=-1]:checked');
        var userId = $('#CUserId').val();
        var role = $('#dbxRoleList').val();
        var list = [];

        if (role != "" && role != null) {
            $('#errorMessage').html('');

            if (checked.length == 0) {
                savePermissions(list, userId, role);
            }

            $.each(checked, function (index) {
                list.push(this.id);

                if (index == checked.length - 1) {
                    savePermissions(list, userId, role);
                }
            })
        } else {
            $('#errorMessage').html('Por favor seleccione un rol');
            $('#ConfirmationSaveModalCustomerByCustomers').modal('hide');
            ECOsystem.Utilities.HideLoader();
        }
    }

    var savePermissions = function (list, userId, role) {
        $.ajax({
            url: '/Customers/SaveTreeViewData',
            type: 'POST',
            data: JSON.stringify({ List: list, UserId: userId, RoleId: role }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data == 1) {
                    $('#ConfirmationSaveModalCustomerByCustomers').modal('hide');
                    $('#InformationMessageModal').modal('show');
                    location.reload();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $('#treeview-checkable').html('');
                    $('#CustomersByCustomersModal').modal('hide');
                }
            },
            complete: function () {
                ECOsystem.Utilities.HideLoader();
            }
        });
    }

    var readMaintenancePrices = function () {
        $("button[data-read-only-prices]").show();
        $("button[data-write-prices]").hide();
        $("#fieldSetPrices").attr("disabled", "disabled");
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DownloadFileForm').submit();
    };

    var onClickDownloadPartnerReport = function () {

        $('#ExcelReportDownloadForm').submit();
    };

    var addPricesToCustomer = function (priceid) {
        var to = $('#customerPrices').find('#txtTo_' + priceid).val();
        var price = $('#customerPrices').find('#txtPrice_' + priceid).val();
        var maxfrom = parseInt($('#customerPrices').find('#maxfrom_' + priceid).val());

        if (to == '' || to == null) {
            $('#customerPrices').find('#warningTo_' + priceid).html('Valor requerido.');
            $('#customerPrices').find('#warningTo_' + priceid).show();
        } else if (price == '' || price == null) {
            $('#customerPrices').find('#warningTo_' + priceid).hide();
            $('#customerPrices').find('#warningPrice_' + priceid).show();
        } else {
            $('#customerPrices').find('#warningTo_' + priceid).hide();
            $('#customerPrices').find('#warningPrice_' + priceid).hide();

            //Validates the margins
            if (to < maxfrom) {
                $('#customerPrices').find('#warningTo_' + priceid).html('El valor no se encuentra en el rango permitido.');
                $('#customerPrices').find('#warningTo_' + priceid).show();
            }
            else {
                savePrices(to, price, priceid);
            }
        }
    }

    var savePrices = function (to, price, priceid) {
        var model = {
            'Id': $('#customerPrices').find('#PricesId_' + priceid).val(),
            'From': $('#customerPrices').find('#txtFrom_' + priceid).val(),
            'To': to,
            'Price': parseFloat(price)
        };

        $.ajax({
            url: '/Customers/SavePricesForCustomers',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ model: model, priceId: priceid }),
            success: function (result) {
                if (result.withinRange) {
                    $('#customerPrices').find('#txtTo_' + priceid).focus();
                    $('#customerPrices').find('#warningTo_' + priceid).html('El valor no se encuentra en el rango permitido.');
                    $('#customerPrices').find('#warningTo_' + priceid).show();
                } else {
                    $('#customerPrices').find('#warningTo_' + priceid).hide();

                    $('#pricesGridContainer_' + priceid).html(result.partial);
                    $('#customerPrices').find('#maxfrom_' + priceid).val(result.maxfrom);
                    $('#customerPrices').find('#txtFrom_' + priceid).val(result.maxfrom);
                    $('#customerPrices').find('#txtTo_' + priceid).val('');
                    $('#customerPrices').find('#txtPrice_' + priceid).val('');

                    if (result.showwarning != null) {
                        $('#customerPrices').find('#rangevalidationwarning_' + priceid).show('slow');
                    } else {
                        $('#customerPrices').find('#rangevalidationwarning_' + priceid).hide('slow');
                    }
                    $('#customerPrices').find('#PricesId_' + priceid).val('');
                }
            }
        });
    }

    var onDelete = function (id, priceid) {
        $.ajax({
            url: '/Customers/DeletePricesForCustomers',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ id: id, priceId: priceid }),
            success: function (result) {
                $('#pricesGridContainer_' + priceid).html(result.partial);
                $('#customerPrices').find('#maxfrom_' + priceid).val(result.maxfrom);
                $('#customerPrices').find('#txtFrom_' + priceid).val(result.maxfrom);

                if (result.showwarning != null) {
                    $('#customerPrices').find('#rangevalidationwarning_' + priceid).show('slow');
                } else {
                    $('#customerPrices').find('#rangevalidationwarning_' + priceid).hide('slow');
                }
            }
        });
    }

    var onEdit = function (id, from, to, price, priceid) {
        $('#customerPrices').find('#PricesId_' + priceid).val(id);
        $('#customerPrices').find('#txtFrom_' + priceid).val(from);
        $('#customerPrices').find('#txtTo_' + priceid).val(to);
        $('#customerPrices').find('#txtPrice_' + priceid).val(price);
        $('#customerPrices').find('#maxfrom_' + priceid).val(from);

        $('#customerPrices').find('#txtTo_' + priceid).focus();
    }

    var addOrEditMasterCards = function (id, international) {
        $('#loadCustomerCardForm').find('#CustomerCardId').val(id);
        $('#loadCustomerCardForm').find('#International').val(international);
        if (international) {
            $('#MasterCardDetailInter').show('slow');
            $('#MasterCardDetailLocal').hide('slow');
        }
        else {
            $('#MasterCardDetailLocal').show('slow');
            $('#MasterCardDetailInter').hide('slow');
        }
        initCustomerCards();
    }

    /**
     * Check the validations for a master card / Limit, inactivity
     */
    var checkInactiveCards = function (isinternational) {
        var obj = isinternational ? $('#international_DisplayCreditCardNumber') : $('#notInternational_DisplayCreditCardNumber');

        if (checkMasterCard(obj)) {
            var limit = $("#notInternational_CreditLimitStr").val();
            var assigned = parseInt($("#notInternational_AssignedCredit").val().split('.')[0], 10);
            if ($("#existInternacionalCard").val() == "Activa" && $("#notInternational_StatusId").val() != "100") {
                ECOsystem.Utilities.SetMessageShow("No es posible cambiar el estado de tarjeta maestra local, mientras la tarjeta maestra internacional se encuentre activa.", "ERROR");
            } else if (limit < assigned) {
                ECOsystem.Utilities.SetMessageShow("El monto límite elegido no es permitido. El cliente ya asignó un monto mayor a los combustibles en el módulo Control.", "ERROR");
            } else if (limit > 99999999999999) {
                ECOsystem.Utilities.SetMessageShow("El monto límite elegido no es permitido. Indique un monto inferior.", "ERROR");
            } else {
                showLoader();
                $.ajax({
                    url: '/Customers/AddOrEditCard',
                    type: 'POST',
                    contentType: "application/json",
                    data: JSON.stringify({ model: getMasterCardInformation(isinternational) }),
                    success: function (result) {
                        $('#masterCardGrid').html(result);
                        ECOsystem.Utilities.SetMessageShow("La información fue almacenada correctamente.", "SUCCESS");
                        $('#MasterCardDetailInter').hide('slow');
                        $('#MasterCardDetailLocal').hide('slow');
                        hideLoader();
                    }
                });
            }
        }
    }

    var getMasterCardInformation = function (isinternational) {
        var model = {
            'CustomerCreditCardId': isinternational ? $('#international_CustomerCreditCardId').val() : $('#notInternational_CustomerCreditCardId').val(),
            'CustomerId': $("#CustomerId").val(),
            'CreditCardHolder': isinternational ? $('#international_CreditCardHolder').val() : $('#notInternational_CreditCardHolder').val(),
            'DisplayCreditCardNumber': isinternational ? $('#international_DisplayCreditCardNumber').val() : $('#notInternational_DisplayCreditCardNumber').val(),
            'ExpirationYear': isinternational ? $('#international_ExpirationYear').val() : $('#notInternational_ExpirationYear').val(),
            'ExpirationMonth': isinternational ? $('#international_ExpirationMonth').val() : $('#notInternational_ExpirationMonth').val(),
            'CreditLimitStr': isinternational ? $('#international_CreditLimitStr').val() : $('#notInternational_CreditLimitStr').val(),
            'StatusId': isinternational ? $('#international_StatusId').val() : $('#notInternational_StatusId').val(),
            'InternationalCard': isinternational
        }
        return model;
    }

    var submitButtonClick = function () {

        $("#DecryptedName").unbind('keyup');
        $("#ContactName").unbind('keyup');
        $("#Email").unbind('keyup');
        $("#MainPhone").unbind('keyup');
        if (validateFields()) {
            customerInformationAddOrEdit();
        }
    };

    var customerInformationAddOrEdit = function () {
        showLoader();
        $.ajax({
            url: '/Customers/AddCustomers',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ model: getCustomerInfoForEdit() }),
            success: function (result) {
                if (result.split('|')[0] == 'Success') {
                    ECOsystem.Utilities.SetMessageShow("La información fue almacenada correctamente.", "SUCCESS");
                    if (result.split('|')[1] == null || result.split('|')[1] == '' || result.split('|')[1] == undefined) {
                        window.location.href = '/Socio/Customers/Index';
                    } else {
                        window.location.reload();
                    }
                } else {
                    $('body').loader('hide');
                    $('#AddOrEditMessage').html(result);
                    $('#errorModal').modal('show');
                }
            }
        });
    }

    var getBranchCustomersToAdd = function () {

        var checked = $('#treeview-checkable-branch').find('input[type=checkbox][id!=-1]:checked');
        var BranchCustomerList = [];
        if (checked.length > 0) {
            $.each(checked, function () {
                BranchCustomerList.push({ 'CustomerId': this.id });
            })
        }
        return BranchCustomerList;
    }


    var getCustomerInfoForEdit = function () {
        
        var customerId = $('#CustomerId').val();
        var contract = $('#AddOrEditContract').val();
        var customerContract = null;
        var BranchCustomerList = getBranchCustomersToAdd();
        
        var customer = {
            'CustomerId': customerId,
            'DecryptedName': $('#DecryptedName').val(),
            'CurrencyId': $('#CurrencyId').val(),
            'IsActive': $('#IsActive').prop('checked'),
            'UnitOfCapacityId': $('#UnitOfCapacityId').val(),
            'IssueForId': $('#IssueForId').val(),
            'CreditCardType': $('#CreditCardType').val(),
            'RowVersion': $('#RowVersion').val(),
            'TransactionsOffline': $('#TransactionsOffline').prop('checked'),
            'isDemo': $('#isDemo').prop('checked'),
            'IsDemoAlertStr': $('#StartDateStr').val(),
            'InChargeEmails': $('#InChargeEmails').val(),
            'SMSAlarms': $('#SMSAlarms').prop('checked'),
            'CustomerCharges': $('#CustomerCharges').val(),
            'ModuleEfficiency': $('#ModuleEfficiency').prop('checked'),
            'ModuleControl': $('#chkControl').prop('checked'),
            'ModuleOperation': $('#ModuleOperation').prop('checked'),
            'ModuleManagement': $('#ModuleManagement').prop('checked'),
            'CostCardAdminStr': $('#CostCardAdminStr').val(),
            'CostCardEmisionStr': $('#CostCardEmisionStr').val(),
            'CostModuleEOStr': $('#CostModuleEOStr').val(),
            'GPSModality': $('#GPSModality').val(),
            'CostRentedGPSStr': $('#CostRentedGPSStr').val(),
            'CostRentedBACStr': $('#CostRentedBACStr').val(),
            'MembershipFeeStr': $('#MembershipFeeStr').val(),
            'CostCardRepositionStr': $('#CostCardRepositionStr').val(),
            'CustomerManagerId': $('#CustomerManagerId').val(),
            'AdditionalId': $('#AdditionalId').val(),
            'Address': $('#Address').val(),
            'BusinessName': $('#BusinessName').val(),
            'CityId': $('#CityId').val(),
            'CountyId': $('#CountyId').val(),
            'StateId': $('#StateId').val(),
            'ContactName': $('#ContactName').val(),
            'Email': $('#Email').val(),
            'SecundaryPhone': $('#SecundaryPhone').val(),
            'MainPhone': $('#MainPhone').val(),
            'Latitude': $('#Latitude').val(),
            'Longitude': $('#Longitude').val(),
            'ZoneId': $('#ZoneId').val(),
            'LegalDocument': $('#LegalDocument').val(),
            'Logo': $('#Logo').val(),
            'ApplicantClient': $('#ApplicantClient').val(),
            'SapCode': $('#SapCode').val(),
            'SupportTicket': $('#SupportTicket').val(),
            'SupportTicketURL': $('#SupportTicketURL').val(),
            'RegisteredOwner': $('#RegisteredOwner').val(),
            'FinancialContact': $('#FinancialContact').val(),
            'FinancialEmail': $('#FinancialEmail').val(),
            'CostCenterFuelDistribution': $('#CostCenterFuelDistribution').prop('checked'),
            'DisplayAccountNumber': $('#DisplayAccountNumber').val(),
            'FuelPermit': $('#Data_FuelPermit').prop('checked'),
            'InitialMargin': $('#initialMargin').val(),
            'FinalMargin': $('#finalMargin').val(),
            'HasFleetio': document.getElementById('HasFleetio').checked
        }
        var customerBudget = {
            'CustomerId': customerId,
            'Periodicity': $('#Periodicity:checked').val(),
            'NameDay': $('#NameDay:checked').val(),
            'NameDate': $('#NameDate').val(),
            'Repeat': $('#Repeat').val()
        }

        var model = {
            'Data': customer,
            'CustomerBudget': customerBudget,
            'ContractInformation': customerContract,
            'BranchCustomerList': BranchCustomerList
        }
        return model;
    }

    var getContractVehicles = function () {
        var arr = [];
        $('#ContractVehiclesContainer').find('input[type=checkbox]').each(function () {
            if ($(this).attr('id') !== 'chkCheckAll') {
                if ($(this).is(':checked')) {
                    var obj = { 'VehicleId': $(this).attr('id').split('-')[1] }
                    arr.push(obj)
                }
            }
        });
        return arr;
    }

    var searchVehicles = function (key) {
        showLoader();
        $.ajax({
            url: '/Customers/SearhVehicles',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ key: key }),
            success: function (result) {
                $('#VehiclesTab').find('#VehiclesGridContainer').html(result);
                $('body').loader('hide');
            }
        });
    };

    var searchContracts = function (key) {
        showLoader();
        $.ajax({
            url: '/Customers/SearchContracts',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ key: key }),
            success: function (result) {
                $('#contractTabs').find('#ContractGridContainer').html(result);
                $('body').loader('hide');
            }
        });
    };

    var validateFields = function () {
        var priceId = null;
        var formIsValid = true;
        var validateEmail = true;

        if ($("#chbxEnableClientWithoutEmail").attr("id") != undefined) {
            $("#chbxEnableClientWithoutEmail").change(function () {
                if (this.checked) {
                    validateEmail = false;
                    $('#Email').attr("readonly", "readonly");
                } else {
                    $('#Email').removeAttr("readonly");
                }
            });
        }

        $("#DecryptedName").keyup(function () {
            if ($(this).val() != '') $(this).css("border", "");
            else $(this).css("border", "2px solid red");
        });
        $("#ContactName").keyup(function () {
            if ($(this).val() != '') $(this).css("border", "");
            else $(this).css("border", "2px solid red");
        });
        $("#Email").keyup(function () {
            if ($(this).val() != '') $(this).css("border", "");
            else $(this).css("border", "2px solid red");
        });
        $("#FinancialEmail").keyup(function () {
            $(this).css("border", "");
        });
        $("#MainPhone").keyup(function () {
            if ($(this).val() != '') $(this).css("border", "");
            else $(this).css("border", "2px solid red");
        });
        $("#SecundaryPhone").keyup(function () {
            $(this).css("border", "");
        });

        $('#ValidationMessage').html('');
        $("input.form-control").css("border", "1px solid #ccc");

        if ($("#chbxEnableClientWithoutEmail").attr("id") != undefined) {
            if ($("#chbxEnableClientWithoutEmail").is(":checked")) {
                validateEmail = false;
                $('#Email').attr("readonly", "readonly");
            } else {
                $('#Email').removeAttr("readonly");
            }
        }

        $('.rangevalidationwarning').each(function () {
            if ($(this).css('display') == 'block') {
                priceId = $(this).attr('priceId');
                return priceId;
            }
        });
        if (priceId != null) {
            $('#tabCustomerPrices').click();
            $('#ValidatePricesModal').modal();
            setTimeout(function () {
                $('html,body').animate({ scrollTop: $('#pricesGridContainer_' + priceId).offset().top }, 'slow');
            }, 1000);
            formIsValid = false;
        }

        var customerid = $('#CustomerId').val();
        if (customerid != "") {
            if (parseInt($('#Repeat').val()) < 0) {
                ECOsystem.Utilities.SetMessageShow("Por favor indique la periocidad del cliente.", "ERROR");
                formIsValid = false;
            }
        }

        if ($('#DecryptedName').val() == '') {
            $("#DecryptedName").css("border", "1px solid red");
            $('#DecryptedName').focus();
            ECOsystem.Utilities.SetMessageShow("Nombre del cliente es requerido.", "ERROR");
            formIsValid = false;
        }
        if ($('#ContactName').val() == '') {
            $("#ContactName").css("border", "1px solid red");
            $('#ContactName').focus();

            if (formIsValid)
                ECOsystem.Utilities.SetMessageShow("Contacto del cliente es requerido.", "ERROR");

            formIsValid = false;
        }
        if ($('#Email').val() == '' && validateEmail == true) {
            $("#Email").css("border", "1px solid red");
            $('#Email').focus();

            if (formIsValid)
                ECOsystem.Utilities.SetMessageShow("Correo del cliente es requerido.", "ERROR");

            formIsValid = false;
        }
        if (!verifyFormat($('#Email').val(), true) && validateEmail == true) {
            $("#Email").css("border", "1px solid red");
            $('#Email').focus();

            if (formIsValid)
                ECOsystem.Utilities.SetMessageShow("Por favor verifique el formato del correo.", "ERROR");

            formIsValid = false;
        }
        if (!$('#FinancialEmail').val() == '' && !verifyFormat($('#FinancialEmail').val(), true)) {
            $("#FinancialEmail").css("border", "1px solid red");
            $('#FinancialEmail').focus();

            if (formIsValid)
                ECOsystem.Utilities.SetMessageShow("Por favor verifique el formato del correo del contacto financiero.", "ERROR");

            formIsValid = false;
        }
        if ($('#MainPhone').val() == '') {
            $("#MainPhone").css("border", "1px solid red");
            $('#MainPhone').focus();

            if (formIsValid)
                ECOsystem.Utilities.SetMessageShow("Por favor indique el Teléfono del cliente", "ERROR");

            formIsValid = false;
        }
        if (!verifyFormat($('#MainPhone').val(), false)) {
            $("#MainPhone").css("border", "1px solid red");
            $('#MainPhone').focus();

            if (formIsValid)
                ECOsystem.Utilities.SetMessageShow("Por favor verifique el formato de los Teléfonos.", "ERROR");

            formIsValid = false;
        }
        if ($('#SecundaryPhone').val() != '' && !verifyFormat($('#SecundaryPhone').val(), false)) {
            $("#SecundaryPhone").css("border", "1px solid red");
            $('#SecundaryPhone').focus();

            if (formIsValid)
                ECOsystem.Utilities.SetMessageShow("Por favor verifique el formato de los Teléfonos.", "ERROR");

            formIsValid = false;
        }
        if ($('#SinceStr').val() != undefined && $('#SinceStr').val().length === 0) {

            if ($("#ContractId").val() != "") {
                $("#SinceStr").css("border", "1px solid red");
                $('#SinceStr').focus();

                if (formIsValid)
                    ECOsystem.Utilities.SetMessageShow("Desde es requerido en el apartado de Contratos.", "ERROR");

                formIsValid = false;
            }
        }

        return formIsValid;
    }

    var verifyFormat = function (data, email) {
        var regex = email ? /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/ : /^([0-9\+\s\+\-])+$/; //email or phone

        return regex.test(data);
    }

    var donwloadContractReport = function (data) {
        $('body').loader('hide');
        if (data == 'Success') {
            $('#DownloadContractReportForm').submit();
        } else if (data == 'NoData') {
            $('#msjresult').html('No existen datos que mostrar.')
            $('#ValidatePricesModal').modal('show');
        } else {
            $('#AddOrEditMessage').html(data);
            $('#errorModal').modal('show');
        }
    }

    var contractCancelResult = function () {
        $('body').loader('hide');
        $('#CancelContractOrdersResultModal').modal('show');
    }

    var showOrderDetail = function () {
        $('body').loader('hide');
        $('#OrderDetailModal').modal('show');
    }

    var validateOrderResult = function (data) {
        $('body').loader('hide');
        if (data == 'Success') {
            ECOsystem.Utilities.SetMessageShow("La información fue almacenada correctamente.", "SUCCESS");
            $('#OrderDetailModal').modal('hide');
        } else {
            $('#orderdata').html('<div class="col-sm-12 center"> <b>Error al procesar la orden. <br /> Detalle técnico: <br /><b>' + data + '</div>');
            $('#VehicleOrderContainer').find('#btnOrderDetailAddOrEdit').hide();
        }
    }

    //Maintenance Clients - Begin Search
    var beginSearchClients = function () {
        showLoader();

        if ($("#messageSearchInfo")) {
            $("#messageSearchInfo").remove();
        }

        var messageSearchInfo = '<div id="messageSearchInfo" class="col-lg-12"><div class="alert alert-info alert-dismissible fade in"  role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
            ' <i class="mdi mdi-information" style="font-size:18px;"></i>' +
            ' La búsqueda puede coincidir con varios criterios, es posible visualizar un resultado o varios.' +
            '</div></div>';

        $("#gridContainer").before(messageSearchInfo);
    }

    //Maintenance Clients - Status Criteria
    var searchByStatus = function (statusType) {
        showLoader();
        setStatusName(statusType);
        $('#searchForm').submit();
    }

    var setStatusName = function (statusType) {

        switch (statusType) {
            case 1:
                $('#btnStatusName').html('Activos <span class="caret"></span>');
                break;
            case 0:
                $('#btnStatusName').html('Inactivos <span class="caret"></span>');
                break;
            default:
                $('#btnStatusName').html('Todos <span class="caret"></span>');
        }

        $('#searchFormStatusId').val(statusType);
    }

    //Maintenance Clients - Search Criteria
    var searchByFilter = function (filterType) {
        setFilterName(filterType);
    }

    var setFilterName = function (filterType) {

        switch (filterType) {
            case 1:
                $('#btnFilterName').html('Nombre Cliente <span class="caret"></span>');
                break;
            case 2:
                $('#btnFilterName').html('Correo <span class="caret"></span>');
                break;
            case 3:
                $('#btnFilterName').html('Cédula Física / Jurídica <span class="caret"></span>');
                break;
            case 4:
                $('#btnFilterName').html('Placa <span class="caret"></span>');
                break;
            case 5:
                $('#btnFilterName').html('Chasis <span class="caret"></span>');
                break;
            case 6:
                $('#btnFilterName').html('Razón Social <span class="caret"></span>');
                break;
            case 7:
                $('#btnFilterName').html('Contacto Principal <span class="caret"></span>');
                break;
            default:
                if ($("#HiddenProductTypeId").val() == "LineVitaECO") {
                    $('#btnFilterName').html('Placa <span class="caret"></span>');
                } else {
                    $('#btnFilterName').html('Nombre Cliente <span class="caret"></span>');
                }

        }

        $('#searchFormFilter').val(filterType);
    }

    //Additional Information - EnableURL Methods
    var enableURLInput = function (enable) {
        if (enable) {
            $(".editable-supporturl").show();
            $(".link-supporturl").hide();
        } else {
            $(".editable-supporturl").hide();
            $(".link-supporturl").show();
        }

    }

    var checkValidUserName = function () {
        ECOsystem.Utilities.ShowLoader();
        if ($('#UserId').val() == '' || $('#UserId').val() == undefined) {
            if (verifyFormat($('#TxtNewUserEmailAdd').val(), true)) {
                $('#checkForm').find('#email').val($('#TxtNewUserEmailAdd').val());
                $('#checkForm').submit();
            } else {
                $("#confirmation").html("¡Algunos campos son requeridos o el formato no es correcto!")
                $("#addOrEditUsersForm").submit(); //Do submit will show the errors.
                ECOsystem.Utilities.HideLoader();
            }
        } else {
            if ($("#addOrEditUsersForm").valid()) {
                $("#addOrEditUsersForm").submit(); //Si es una edición no lo válida.
            } else {
                ECOsystem.Utilities.HideLoader();
            }
        }
    }

    /*on Success Check User*/
    var onSuccessCheckUser = function (data) {
        if (!data) {
            ECOsystem.Utilities.SetMessageShow("El correo electrónico ya es utilizado.", "ERROR");
            ECOsystem.Utilities.HideLoader();
        } else {
            $("#confirmation").html("¡Algunos campos son requeridos o el formato no es correcto!")
            $("#addOrEditUsersForm").submit();
        }
    }

    var onFailureCheckUser = function () {
        ECOsystem.Utilities.SetMessageShow("Ya existe un Usuario en plataforma con el mismo correo, favor utilizar otro correo que no esté en uso.", "ERROR");
        ECOsystem.Utilities.HideLoader();
    }

    var onSuccessCheckPlateCard = function (data) {
        if (!data) {
            ECOsystem.Utilities.SetMessageShow("El número de placa seleccionado ya posee una tarjeta asociada.", "ERROR");
            ECOsystem.Utilities.HideLoader();
        } else {
            $("#confirmation").html("¡Algunos campos son requeridos o el formato no es correcto!")
            ECOsystem.Utilities.HideLoader();
            $("#addOrEditCarRequestdForm").submit();
        }
    }

    var cleanFilters = function () {
        /*** Reset active filter ***/
        $('#btnFilterName').html('Todos <span class="caret"></span>');
        $('#btnStatusName').html('Todos <span class="caret"></span>');
        $('#searchFormStatusId').val(1);
        $('#searchFormFilter').val(0);
        $('#page').val(1);
        /*** Reset textbox filter ***/
        $('#txtSearch').val('');
        /*** Reset grid ***/
        $('#searchForm').submit();
    }

    var confirmModalClose = function () {
        //Close current open modals
        
        $('.modal').each(function (e) {
            if ($(this).is(':visible')) {
                $(this).modal('hide');
            }
        });
        window.location.reload();
    };

    var failureProcess = function () {
        hideLoader();
        ECOsystem.Utilities.SetMessageShow("Se encontró un error no contralado en el proceso actual. Por favor informar del evento a soporte.", "ERROR");
    };

    var checkValidCardPlate = function () {
        var lastPlate = $("#addOrEditCarRequestdForm").find("#hdnLastPlate").val();
        var newPlate = $("#addOrEditCarRequestdForm").find('#PlateId').val();
        if (lastPlate != newPlate) {
            if ($("#addOrEditCarRequestdForm").valid()) {
                ECOsystem.Utilities.ShowLoader();
                $('#checkPlateCardForm').find('#plateCard').val(newPlate);
                $('#checkPlateCardForm').submit();
            } else {
                $("#addOrEditCarRequestdForm").submit(); //Si es una edición no lo válida.
            }
        } else {
            if ($("#addOrEditCarRequestdForm").valid()) {
                ECOsystem.Utilities.ShowLoader();
            }
            $("#addOrEditCarRequestdForm").submit();
        }
    };

    var GetRoleSelected = function () {
        var userId = $('#CUserId').val();

        $.ajax({
            url: '/Customers/GetRoleForCustomerSelected',
            type: 'POST',
            data: JSON.stringify({ UserId: userId, SelectType: null }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                $('#dbxRoleList').val(data).change();
            }
        });
    };

    var GetCustomerBranchTreeData = function (obj) {
        if ($('#CheckAllBranch').hasClass('hidden')) {
            showLoader();
            $.ajax({
                url: '/Customers/GetTreeDataBranchCustomers',
                type: 'POST',
                data: JSON.stringify({ CustomerId: obj }),
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    var json = data;
                    if (json == '[]') {
                        $('#treeview-checkable-branch').html('');
                        $('#treeview-checkable-branch').prepend('<p class="top20 text-success">No se encontraron resultados</p>');
                        $('#CheckAllBranch').addClass('hidden');
                        $('.SelectAll').prop('checked', false);
                    }
                    else {
                        $('#CheckAllBranch').removeClass('hidden');
                        $('#txtSearchCustomerBranch').removeClass('hidden');

                        for (var i = 0; i < data.length; i += 1) {
                            if (data[i].state != undefined && data[i].state.checked == true) {
                                $('#treeview-checkable-branch').append('<li class="list-group-item node-treeview-checkable" style="padding-left:30px" customername="' + data[i].text + '"><div class="checkbox"><label><input type ="checkbox" class="generateAllCardsBranch" id="' + data[i].id + '" value="' + data[i].text + '" checked>' + data[i].text + '</label></div></li>');
                            } else {
                                $('#treeview-checkable-branch').append('<li class="list-group-item node-treeview-checkable" style="padding-left:30px" customername="' + data[i].text + '"><div class="checkbox"><label><input type ="checkbox" class="generateAllCardsBranch" id="' + data[i].id + '" value="' + data[i].text + '">' + data[i].text + '</label></div></li>');
                            }

                            if (i == data.length - 1) {
                                if ($('#BranchOfficeInfoContainer').find('input[type=checkbox][id!=-1]').length ==
                                    $('#BranchOfficeInfoContainer').find('input[type=checkbox][id!=-1]:checked').length) {
                                    $('.SelectAll').prop('checked', true);
                                }


                                $('#BranchOfficeInfoContainer').off('click', '.generateAllCardsBranch').on('click', '.generateAllCardsBranch', function (e) {
                                    var id = $(this).attr('id');
                                    var ischecked = $(this).prop("checked");

                                    if (id == -1) {
                                        $('#BranchOfficeInfoContainer').find('input:checkbox').each(function () {
                                            if ($(this).css('display') != 'none') {
                                                $(this).prop('checked', ischecked);
                                            }                                            
                                        });
                                    } else {
                                        var allCheckboxes = $('#BranchOfficeInfoContainer').find('input[type=checkbox][id!=-1]').length;
                                        var selectedCheckboxes = $('#BranchOfficeInfoContainer').find('input[type=checkbox][id!=-1]:checked').length;

                                        if (allCheckboxes == selectedCheckboxes) {
                                            $('.SelectAll').prop('checked', true);
                                        } else {
                                            $('.SelectAll').prop('checked', false);
                                        }
                                    }
                                });
                            }
                        }
                    }
                    ECOsystem.Utilities.HideLoader();
                }
            });
        }
    }

    var GetUserPermissions = function () {
        var userId = $('#CUserId').val();

        $.ajax({
            url: '/Customers/GetTreeData',
            type: 'POST',
            data: JSON.stringify({ UserId: userId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                var json = data;
                if (json == '[]') {
                    $('#treeview-checkable').html('');
                    $('#treeview-checkable').prepend('<p class="top20 text-success">No se encontraron resultados</p>');
                    $('#CheckAll').addClass('hidden');
                    $('#-1').prop('checked', false);
                }
                else {
                    $('#CheckAll').removeClass('hidden');

                    for (var i = 0; i < data.length; i += 1) {
                        if (data[i].state != undefined && data[i].state.checked == true) {
                            $('#treeview-checkable').append('<li class="list-group-item node-treeview-checkable"><div class="checkbox"><label><input type ="checkbox" class="generateAllCards" id="' + data[i].id + '" value="' + data[i].text + '" checked>' + data[i].text + '</label></div></li>');
                        } else {
                            $('#treeview-checkable').append('<li class="list-group-item node-treeview-checkable"><div class="checkbox"><label><input type ="checkbox" class="generateAllCards" id="' + data[i].id + '" value="' + data[i].text + '">' + data[i].text + '</label></div></li>');
                        }

                        if (i == data.length - 1) {
                            if ($('#CustomersByCustomersModal').find('input[type=checkbox][id!=-1]').length ==
                                $('#CustomersByCustomersModal').find('input[type=checkbox][id!=-1]:checked').length) {
                                $('#-1').prop('checked', true);
                            }


                            $('#CustomersByCustomersModal').off('click', '.generateAllCards').on('click', '.generateAllCards', function (e) {
                                var id = $(this).attr('id');
                                var ischecked = $(this).prop("checked");

                                if (id == -1) {
                                    $('#CustomersByCustomersModal').find('input:checkbox').each(function () {
                                        $(this).prop('checked', ischecked);
                                    });
                                } else {
                                    var allCheckboxes = $('#CustomersByCustomersModal').find('input[type=checkbox][id!=-1]').length;
                                    var selectedCheckboxes = $('#CustomersByCustomersModal').find('input[type=checkbox][id!=-1]:checked').length;

                                    if (allCheckboxes == selectedCheckboxes) {
                                        $('#-1').prop('checked', true);
                                    } else {
                                        $('#-1').prop('checked', false);
                                    }
                                }
                            });
                        }
                    }
                }
                ECOsystem.Utilities.HideLoader();

                $('#CustomersByCustomersModal').modal('show');
            }
        });
    };

    var generateAPIToken = function () {
        $('#btnGenerateAPIToken').click(function () {
            $('#ConfirmAddOrEditAPIToken').modal('show');
        });

        $('#btnConfirmAddOrEditAPIToken').click(function () {
            $('#ConfirmAddOrEditAPIToken').modal('hide');
            ECOsystem.Utilities.ShowLoader();
            var customerId = $('#CustomerId').val();

            $.ajax({
                url: '/Customers/GenerateAPIToken',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify({ customerId: customerId }),
                success: function (result) {
                    if (result.split('|')[0] === 'Error') {
                        $('#AddOrEditAPITokenError').find('#errormsg').show();
                        $('#AddOrEditAPITokenError').find('#confirmsg').hide();
                        $('#AddOrEditAPITokenError').find('#error').html('<b>' + result.split('|')[1] + '</b>');
                        $('#AddOrEditAPITokenError').find('#error').show();
                        $('#AddOrEditAPITokenError').find('#errortitle').show();
                    } else {
                        $('#AddOrEditAPITokenError').find('#errormsg').hide();
                        $('#AddOrEditAPITokenError').find('#error').hide();
                        $('#AddOrEditAPITokenError').find('#errortitle').hide();
                        $('#AddOrEditAPITokenError').find('#confirmsg').show();
                        $('#APIToken').val(result);
                    }
                    $('#AddOrEditAPITokenError').modal('show');
                    $('body').loader('hide');
                }
            });
        });
    }

    var onClickSearch = function () {
        $('#PaymentPanel').hide('slow');
        $('body').loader('show');
        if ($('#notInternational_Parameters_ReportCriteriaId').val() == 1000) {
            var month = $('#notInternational_Parameters_Month').val();
            var year = $('#notInternational_Parameters_Year').val();
        }
        else {
            var startDate = $('#notInternational_Parameters_StartDateStr').val();
            var endDate = $('#notInternational_Parameters_EndDateStr').val();
        }

        var Parameters = {
            'CustomerId': $('#CustomerId').val(),
            'Month': month,
            'Year': year,
            'StartDateStr': startDate,
            'EndDateStr': endDate,
            'ReportCriteriaId': $('#notInternational_Parameters_ReportCriteriaId').val()
        }

        $.ajax({
            url: '/Customers/SearchPayments',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ Parameters }),
            success: function (result) {
                $('body').loader('hide');
                $('#PaymentGrid').html(result);
            }
        });
    };

    var onClickSavePayment = function () {
        var payment = $('#txtPayment').val();
        var customerid = $('#CustomerId').val();
        var regex = /^[0-9]\d{0,4}(\.\d{1,3})?%?(\.\d{1,3})?%?(\.\d{1,3})?%?(\,\d{1,3})?%?$/;
        var validateRegex = regex.test(payment);
        if (validateRegex) {
            $('body').loader('show');
            $.ajax({
                url: '/Customers/AddOrEditPayment',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify({ Payment: ECOsystem.Utilities.ToNum(payment), CustomerId: customerid }),
                success: function (result) {
                    $('body').loader('hide');
                    $('#PaymentPanel').hide('slow');
                    $('#PaymentGrid').html(result);
                    $('#txtPayment').val("");
                }
            });
        }
        else {
            $('#txtPayment').css("border", "1px solid red");
            ECOsystem.Utilities.SetMessageShow("Favor digite un monto válido al Pago, el formato puede estar incorrecto", "ERROR");
        }

    };

    var onClickAddPayment = function () {
        $('#PaymentPanel').show('slow');
    };

    var onClickbtnCancelPayment = function () {
        $('#PaymentPanel').hide('slow');
        $('#txtPayment').val("");

    };

    function validateAmount(e) {
        var regex = /^[0-9]\d{0,4}(\.\d{1,3})?%?(\.\d{1,3})?%?(\.\d{1,3})?%?(\,\d{1,3})?%?$/;
        if (regex.test(e.val())) {
            $('#txtPayment').css("border", "");
        }
        else {
            $('#txtPayment').css("border", "1px solid red");
            ECOsystem.Utilities.SetMessageShow("Favor digite un monto válido al Pago, el formato puede estar incorrecto", "ERROR");
        }
    }





    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        OnSuccessCheckUser: onSuccessCheckUser,
        OnSuccessCheckPlateCard: onSuccessCheckPlateCard,
        OnSuccessLoadEdit: onSuccessLoadEdit,
        OnSuccessEdit: onSuccessEdit,
        OnSuccessLoadCard: onSuccessLoadCard,
        OnSuccessUpdateCard: onSuccessUpdateCard,
        OnSuccessLoadCardRequest: onSuccessLoadCardRequest,
        OnBeginTerminalRequest: onBeginTerminalRequest,
        OnSuccessTerminalRequest: onSuccessTerminalRequest,
        OnSuccessAddorEditTerminal: onSuccessAddorEditTerminal,
        OnSuccessUpdateCardRequest: onSuccessUpdateCardRequest,
        OnSuccessLoadPreviousCardRequest: onSuccessLoadPreviousCardRequest,
        OnSuccessLoadUploadRequest: onSuccessLoadUploadRequest,
        OnSuccessLoadCounties: onSuccessLoadCounties,
        OnSuccessLoadCities: onSuccessLoadCities,
        OnSuccessImport: importModalClose,
        Subdelterminal: subdelterminal,
        OnSuccessLoadUsers: onSuccessLoadUsers,
        OnSuccessAddEditUsers: onSuccessAddEditUsers,
        SaveCustomersByCustomer: saveCustomersByCustomer,
        ReadPrices: readMaintenancePrices,
        SubmitButtonClick: submitButtonClick,
        OnClickDownloadPartnerReport: onClickDownloadPartnerReport,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        AddPricesToCustomer: addPricesToCustomer,
        OnDeletePrices: onDelete,
        OnEditPrices: onEdit,
        AddOrEditMasterCards: addOrEditMasterCards,
        CheckInactiveCards: checkInactiveCards,
        DonwloadContractReport: donwloadContractReport,
        ContractCancelResult: contractCancelResult,
        ShowOrderDetail: showOrderDetail,
        ValidateOrderResult: validateOrderResult,
        EnableURLInput: enableURLInput,
        GetContractData: getContractData,
        EditContractData: editContractData,
        GetVehicleData: getVehicleData,
        BeginSearchClients: beginSearchClients,
        SearchByStatus: searchByStatus,
        SetStatusName: setStatusName,
        SearchByFilter: searchByFilter,
        SetFilterName: setFilterName,
        CheckValidUserName: checkValidUserName,
        CleanFilters: cleanFilters,
        EditBinnacle: editBinnacle,
        DeleteBinnacle: deleteBinnacle,
        CheckValidCardPlate: checkValidCardPlate,
        TrasladateBetweenCustomers: trasladateBetweenCustomers,
        TrasladateBetweenPartners: trasladateBetweenPartners,
        ConfirmModalClose: confirmModalClose,
        FailureProcess: failureProcess,
        OnFailureCheckUser: onFailureCheckUser,
        OnClickSearch: onClickSearch,
        OnClickSavePayment: onClickSavePayment,
        OnClickAddPayment: onClickAddPayment,
        OnClickbtnCancelPayment: onClickbtnCancelPayment,
        ValidateAmount: validateAmount
        //LoadCustomer: loadCustomer
    };

})();