﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.CollectionZones = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        showLoader();
        hideLoader();

        $('#gridContainer').off('click.del_row', 'a[del_row]').on('click.del_row', 'a[del_row]', function (e) {
            $('#deleteForm').find('#Id').val($(this).attr('Id'));
        });

        $('#btnAddForm').click(function () {
            $('#LoadForm').find('#Id').val(null);
            $('#LoadForm').submit();
        })
        $('#DetailContainer').on('click', '#btnCancelZone', function (e) {
            $('#DetailContainer').hide('slow')
            $('#txtName').val('');
            $('#txtDescription').val('');
        });
    }

    var onAddOrEditZone = function () {
        var Name = $('#txtName').val();
        var Description = $('#txtDescription').val();
        var ZoneId = $('#Id').val();

        if (Name == "" || Description == "") {
            $('#TextEmptyMessageModal').modal('show');
        } else {
            $.ajax({
                url: '/CollectionZones/AddOrEditZones',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify({ Name: Name, Description: Description, ZoneId: ZoneId }),
                success: function (data) {
                    $('#DetailContainer').hide('slow')
                    $('#txtName').val('');
                    $('#txtDescription').val('');
                    $('#InformationMessageModal').modal('show');
                    $('#gridContainer').html(data);
                }
            });
        }
    }

    var onLoad = function (Id) {
        $('#LoadForm').find('#Id').val(Id);
        $('#LoadForm').submit();
    }

    var onZoneDelete = function () {
        var ZoneId = $('#ZoneId').val();
        $.ajax({
            url: '/CollectionZones/CollectionZoneDelete',
            type: 'post',
            contentType: "application/json",
            data: JSON.stringify({ ZoneId: ZoneId }),
            success: function (data) {       
            }
        });
    }

    var deleteModalClose = function (data) { 
        $('#ConfirmationDelete').modal('hide');
        hideLoader();
    }

    var showLoader = function () {
        $('body').loader('show');
        hideLoader();
    };

    var hideLoader = function () {
        $('body').loader('hide');
    };

    var editModalShow = function () {
        $('#DetailContainer').show('slow');
    }

    return {
        Init: initialize,
        OnAddOrEditZone: onAddOrEditZone,
        OnLoad: onLoad,
        OnZoneDelete: onZoneDelete,
        DeleteModalClose: deleteModalClose,
        EditModalShow: editModalShow
    };
})();

