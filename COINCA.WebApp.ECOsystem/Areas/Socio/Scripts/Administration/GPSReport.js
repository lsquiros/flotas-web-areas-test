﻿var ECOsystem = ECOsystem || {};

ECOsystem.GPSReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        disabledControls();
        initCharts();
        initCurrencyInputs();
        try {
            initializeDropDownList();
        }
        catch (exception) {
        }
    }

    /*init Events*/
    var initEvents = function () {
        $('#page-wrapper').off('click.print', 'a[data-print-link]').on('click.print', 'a[data-print-link]', function (e) {
            ECOsystem.Utilities.ShowPopup(options.printURL, 'Formato de impresión');
        });
        $('#page-wrapper').off('click.goToPrint', 'button[data-print]').on('click.goToPrint', 'button[data-print]', function (e) {
            window.print();
        });

        if (!options.printMode) {
            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });
            $("#Parameters_StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });


            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if (!($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects()))
                    invalidSelectOption();
            });

            $("#Parameters_EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });
        }

        $('#btnGenerate').off('click.btnGenerate').on('click.btnGenerate', function (e) {
            if (checkAllSelects()) {
                showLoader();
                $('#Parameters_CustomerId').val('-1')
                $('#ReportCriteriaId').val($('#Parameters_ReportCriteriaId').val());
                $('#ParametersMonth').val($('#Parameters_Month').val());
                $('#ParametersYear').val($('#Parameters_Year').val());
                $('#ParametersEndDateStr').val($('#Parameters_EndDateStr').val());
                $('#ParametersGpsModality').val($('#Parameters_GpsModality').val());
                $('#ParametersCustomerId').val($('#Parameters_CustomerId').val())
                $('#ExcelReportDownloadForm').submit();
            }
        });
    }

    /*disabled Controls when printMode = true*/
    var disabledControls = function () {
        if (options.printMode) {
            $('[data-filter]').attr('disabled', 'disabled');
        }
    }

    /*Init Charts*/
    var initCharts = function () { }

    /*format Label*/
    var formatLabel = function (v) {
        return ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToFloat(v)) + "%";
    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {
        var select1 = $("#Parameters_ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        var select4 = $("#Parameters_GpsModality").select2().data('select2');
        select4.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onModalityChange(data); return fn.apply(this, arguments); }
            }
        })(select4.onSelect);
    };

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $('#Parameters_EndDateStr').val('');
            $('#Parameters_GpsModality').select2('val', '');

            $('#Parameters_ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            if (checkAllSelects())
            {
                $('#Parameters_CustomerId').val('-1')
            }
        }
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Month').val(obj.id);
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Year').val(obj.id);
        }
    };

    /*on Select User Change*/
    var onModalityChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_GpsModality').val(obj.id);
            if (checkAllSelects())
            {
                $('#Parameters_CustomerId').val('-1')
                //$('#ReloadForm').submit();
            }
        }
    };

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<div class="text-danger top30">Por favor seleccionar las opciones correspondientes para cargar la información.</div>');
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        if (
                ($('#Parameters_EndDateStr').val() === '' && $('#Parameters_ReportCriteriaId').val() === '2000') 
                || ($('#Parameters_ReportCriteriaId').val() === '1000' && ($('#Parameters_Month').val() === '' || $('#Parameters_Year').val() === ''))
                || ($('#Parameters_GpsModality').val() === '')) {
            invalidSelectOption();
            return false;
        }
        return true;
    };

    /*init Currency Inputs*/
    var initCurrencyInputs = function () {
        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        try {

            $('body').off('blur.currencyOnBlur', 'input[data-currency], input[datacurrency]').on('blur.currencyOnBlur', 'input[data-currency], input[datacurrency]', function (e) {
                var obj = $(this);
                obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
                obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));
            });
            $('body').find('input[data-currency], input[datacurrency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });

        } catch (e) {

        }
    };

    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DownloadExcelForm').submit();
    };

    var onClickDownloadCCReport = function () {
        $('#ExcelReportDownloadForm').submit();
    };

    /* Public methods */
    return {
        Init: initialize,
        FormatLabel: formatLabel,
        OnClickDownloadCCReport: onClickDownloadCCReport,
        ShowLoader: showLoader,
        HideLoader: hideLoader
    };
})();

