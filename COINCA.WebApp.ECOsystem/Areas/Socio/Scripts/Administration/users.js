﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.Users = (function () {
    var options = {};

    
    /* initialize function */
    
    var initialize = function (opts) {
        $.extend(options, opts);

        $('#driverLicenseExpirationId').datepicker({ language: 'es', minDate: new Date(), format: 'dd/mm/yyyy', autoclose: true, todayHighlight: true });
        $("#DriverLicenseExpirationStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace : false,         
            allow: '/',
            maxLength: 10
        });

        

        //trigger click on hidden file input
        $('#btnUpload').off('click.btnUpload').on('click.btnUpload', function (e) {
            $('#fileHidden').trigger('click');
            e.stopPropagation();
        });
        //trigger click on hidden file input
        $('#AddAlarmId').off('click.AddAlarmId').on('click.AddAlarmId', function (e) {
            $('#alarmForm').find('#id').val($('#UserId').val());
            $('#alarmForm').submit();
        });
        fileApiUpload();
        formValidation();
        onClickNew();
        onClickCheckboxChangePassword();
        
    }


    
    /* Standard Error Function for File API */
    
    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };

    
    /* Update progress for File API */
    
    var fileApiUpdateProgress = function (e) {
        if (e.lengthComputable) {
            var percentLoaded = Math.round((e.loaded / e.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                $('.percent').css('width', percentLoaded + '%').html(percentLoaded + '%');
            }
        }
    };

    
    /* File Upload implementation */
    
    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            $('#fileHidden').off('change.fileHidden').on('change.fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onprogress = fileApiUpdateProgress;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {
                    $('#imgPhoto').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        alert("La extensión del archivo es inválida");
                        continue;
                    }
                    // Closure to capture the file information.
                    reader.onload = (function (F) {
                        $('.percent').html('100%').css('width', '100%');
                        setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgPhoto').removeClass('imgloading');", 2000);
                        return function (e) {
                            var lsrc = e.target.result;
                            $('#imgPhoto').attr({
                                src: lsrc,
                                title: escape(F.name)
                            });
                            $('#Photo').val(lsrc);
                        };
                    })(f);
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
            });
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.');
        }
    };

    
    /* Form validation to prevent an invalid submission*/
    
    var formValidation = function () {

        $('#detailContainer').off('click.btnSaveSubmit', 'button[type=submit]').on('click.btnSaveSubmit', 'button[type=submit]', function (e) {
            setErrorMsj('', false);
            if (validatePassword() && validateEmail() && validatePasswordTemporal()) {
                return true;
            } else {
                return false;
            }
        });
    };

    
    /* Email validation to prevent an invalid submission*/
    
    var validateEmail = function () {
        if ($('#Email').val().length == 0) {
            return true;
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#Email').val().match(email)) {
            return true;
        } else {
            setErrorMsj('Correo electrónico invalido', true);
            return false;
        }
    };

    
    /* Password validation to prevent an invalid submission*/
    
    var validatePassword = function () {
        if ($('#detailContainer').find('#Password').length > 0) {
            if (($('#Password').val().length == 0) && ($('#PasswordConfirm').val().length == 0)) {
                return true;
            }

            if ($('#Password').val() == $('#PasswordConfirm').val()) {
                var pass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
                if ($('#Password').val().match(pass)) {
                    return true;
                } else {
                    setErrorMsj('La contraseña debe contener al menos entre 8 y 15 caracteres, una letra minúscula, una letra mayúscula, un dígito numérico y un carácter especial', true);
                    return false;
                }

            } else {
                setErrorMsj('Las contraseñas deben ser iguales', true);
                return false;
            }
        }
        return true;
    };

    
    /* Password Temporar validation to prevent an invalid submission*/
    
    var validatePasswordTemporal = function () {
        if ($('#detailContainer').find('#PasswordTemporal').length > 0) {
            if ($('#PasswordTemporal').val().length == 0) {
                return true;
            }
            var pass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
            if ($('#PasswordTemporal').val().match(pass)) {
                return true;
            } else {
                setErrorMsj('La contraseña debe contener al menos entre 8 y 15 caracteres, una letra minúscula, una letra mayúscula, un dígito numérico y un carácter especial', true);
                return false;
            }
        }
        return true;
    };

    
    /* Show or hide errors messages*/
    
    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj').html(msj);
            $('#validationErrors').removeClass('hide');
        } else {
            $('#errorMsj').html('');
            $('#validationErrors').addClass('hide');
        }

    }

    
    /* On Success Load after call edit*/
    
    var onSuccessLoad = function () {
        //initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
    };
    
    /* On click BtnNew*/
    
    var onClickNew = function () {
        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });
    };

    
    /* On click change pass*/
    
    var onClickCheckboxChangePassword = function () {
        $('#detailContainer').off('click.checkboxChangePassword', '#ChangePassword').on('click.checkboxChangePassword', '#ChangePassword', function (e) {
            if ($(this).is(':checked')) {
                $('#_PasswordTemporal').removeClass('hide').val('');
            } else {
                $('#_PasswordTemporal').addClass('hide').val('');
            }

        });
    };


    
    /* Public methods */
    
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad
    };
})();

