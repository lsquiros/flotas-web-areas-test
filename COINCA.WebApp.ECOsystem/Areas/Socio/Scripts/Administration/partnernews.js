﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.PartnerNews = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);
        
        $('#gridContainer').off('click.edit', 'a[edit_row_new]').on('click.edit', 'a[edit_row_new]', function (e) {
            $('#loadNewsForm').find('#id').val($(this).attr('id'));
            $('#loadNewsForm').submit();
        });

        $('#btnAdd').click(function () {
            $('#loadNewsForm').find('#id').val(null);
            $('#loadNewsForm').submit();
        });

        $('#btnAddNotifications').click(function () {
            $('#loadNotificationsForm').find('#id').val(null);
            $('#loadNotificationsForm').submit();
        });

        //Edit Partner Notifications
        $('#gridContainerNotifications').off('click.edit', 'a[edit_row_notifications]').on('click.edit', 'a[edit_row_notifications]', function (e) {
            $('#loadNotificationsForm').find('#id').val($(this).attr('id'));
            $('#loadNotificationsForm').submit();
        });

        //Delete Partner Notifications
        $('#gridContainerNotifications').off('click.delete', 'a[del_row_notifications]').on('click.delete', 'a[del_row_notifications]', function (e) {
            $('#deleteNotificationsForm').find('#id').val($(this).attr('id'));
            $('#deleteNotificationModal').modal('show');
        });   
             
        $('#btnSubmitSaveData').click(function (e) {            
            $('#validateNotificationsModal').modal('hide');
            $('#addOrEditNotificationsForm').submit();
        });     
    }

    var addContact = function () {
        var plateId = $('#ddlVehicles').val();
        var email = $('#ddlDrivers').val();
        var content = $('textarea#Content')[1].value
        var notEmpty = content.split(';').filter(function (n) { return n.trim() !== '' });

        content = notEmpty.join(';');

        if (plateId !== '') {
            content = content === '' ? content + plateId : content + ';' + plateId;
        }
        if (email !== '') {
            content = content === '' ? content + email : content + ';' + email;
        }
        $('#txtContent').val(content);
        $('textarea#Content').val(content);
    };

    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };

    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            $('#fileHidden').off('change.fileHidden').on('change.fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {
                    $('#imgLogo').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        alert("La extensión del archivo es inválida");
                        continue;
                    }
                    // Closure to capture the file information.
                    reader.onload = (function (F) {
                        $('.percent').html('100%').css('width', '100%');
                        setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgLogo').removeClass('imgloading');", 2000);
                        return function (e) {
                            var lsrc = e.target.result;
                            $('#imgLogo').attr({
                                src: lsrc,
                                title: escape(F.name)
                            });
                            $('#Image').val(lsrc);
                        };
                    })(f);
                    // Read in the image file as a data URL.                    
                    reader.readAsDataURL(f);
                }
            });
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.');
        }
    };

    var loadNotificationsSuccess = function (e) {        
        //$('#ddlVehicles').select2().on('change', function (e) {
        //    $('#ddlDrivers').select2('val', null);
        //});

        //$('#ddlDrivers').select2().on('change', function (e) {
        //    $('#ddlVehicles').select2('val', null);
        //});

        $('#btnAddContact').click(function () {
            addContact();
        });

        $('#btnSaveNotitications').click(function () {
            validateNotifications();
        });           

        $('#detailContainerNotifications').show('slow');

        $('#datetimepickerSendDate').datepicker({
            language: 'es',
            startDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $("#SendDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        }); 
        e.stopPropagation();
    };

    var loadNewsSuccess = function () {
        $('#btnUpload').off('click.btnUpload').on('click.btnUpload', function (e) {
            $('#fileHidden').trigger('click');
            e.stopPropagation();
        });
        fileApiUpload();
    }

    var validateNotifications = function () {
        var xml = $('textarea#Content')[1].value.trim();

        $.ajax({
            url: '/PartnerNews/ValidateNotifications',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ xml: xml }),
            success: function (result) {                
                switch (result.message) {
                    case 'Success':
                        $('#modalValidateText').html('<p><b>Las siguientes Placas o Usuarios no existen en la base de datos:</b></p>');
                        $('#btnSubmitSaveData').hide();
                        $('#btnCancelNotitications').html('<i class="mdi mdi-check mdi-18px"></i>&nbsp;Aceptar');
                        $('#modalValidateText').append('<ul>');
                        for (var i = 0; i < result.data.length; i++) {
                            $('#modalValidateText').append('<li>' + result.data[i] + '</li>');
                        }
                        $('#modalValidateText').append('</ul>');
                        break;
                    case 'Error':
                        $('#modalValidateText').html('<p>' + result.error + '</p>');
                        $('#btnSubmitSaveData').hide();
                        $('#btnCancelNotitications').html('<i class="mdi mdi-check mdi-18px"></i>&nbsp;Aceptar');
                        break;
                    default:    
                        $('#modalValidateText').html('<p>¿Desea almacenar los datos?</p>');
                        $('#btnSubmitSaveData').show();
                        $('#btnCancelNotitications').html('<i class="mdi mdi-close mdi-18px"></i>&nbsp;Cancelar');
                        break;
                }           
                $('#validateNotificationsModal').modal('show');
            }
        });
    };

    var addOrEditNotificationSuccess = function () {
        $('#detailContainerNotifications').hide('slow');
        $('body').loader('hide');
    };

return {
    Init: initialize,
    LoadNotificationsSuccess: loadNotificationsSuccess,
    AddOrEditNotificationSuccess: addOrEditNotificationSuccess,
    LoadNewsSuccess: loadNewsSuccess
};
}) ();

