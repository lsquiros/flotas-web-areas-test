﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.ServiceStations = (function () {
    var options = {};

    /* initialize function */    
    var initialize = function (opts)
    {
        $.extend(options, opts);

        $('#gridContainer').off('click.del_row', 'a[del_row]').on('click.del_row', 'a[del_row]', function (e) {
            $('#deleteForm').find('#id').val($(this).attr('id'));            
        });
        
        initControls()

        downloadFile();
    }
       
    var initControls = function () {
        $('#btnAddTerminal').unbind('click');
        $('#terminalsTableContainer').find('.btnDeleteTerminal').unbind('click');
        $('#deleteTerminalModal').find('#btnDeleteTerminal').unbind('click');
        $('#btnValidateServiceStations').unbind('click');
        $('#confirmationaddoreditmodal').find('#btnAddOrEditServiceStations').unbind('click');

        $('#btnAddTerminal').click(function () {
            addTerminals();
            e.preventDefault();
        });

        $('#terminalsTableContainer').find('.btnDeleteTerminal').click(function () {
            var terminalId = $(this).attr('TerminalId');
            $('#deleteTerminalModal').find('#btnDeleteTerminal').attr('TerminalId', terminalId);
            $('#deleteTerminalModal').modal('show');            
        });

        $('#deleteTerminalModal').find('#btnDeleteTerminal').click(function (e) {
            var terminalId = $(this).attr('TerminalId');
            removeTerminal(terminalId);
            e.preventDefault();
        });

        $('#TopMonetaryLimitStr').keypress(function (event) {
            return isNumber(event);
        });

        $('#btnValidateServiceStations').click(function () {
            var valueStr = $('#TopMonetaryLimitStr').val();
            var valueNum = toNum(valueStr);

            $('#TopMonetaryLimit').val(valueNum);
            if ($('#addOrServiceStationsEditForm').valid()) {
                $('#confirmationaddoreditmodal').modal('show');
            }
        });

        $('#confirmationaddoreditmodal').find('#btnAddOrEditServiceStations').click(function () {
            $('#addOrServiceStationsEditForm').submit();
            $('#confirmationaddoreditmodal').modal('hide');
        });

        $('#btnCancelAddOrEdit').click(function () {
            $('#detailContainer').hide('slow');
            $('#btnAdd').removeAttr('disabled');
            $('#gridContainer').removeClass('removetop');
        });

        $.validator.unobtrusive.parse("#detailContainer");
    }

    /*Convert str to Number*/
    var toNum = function (num) {
        num = ("" + num).replace(/\./g, '').replace(/,/g, '.');
        return isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ((charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }

    var removeTerminal = function (terminalId) {
        $.ajax({
            url: 'ServiceStations/RemoveTerminal',
            type: 'POST',
            data: JSON.stringify({ TerminalId: terminalId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data != 'Error') {
                    $('#terminalsTableContainer').html('');
                    if (data.length == 0)
                    {
                        $('#terminalsTableContainer').html('<br />' +
                                                           '<div class="alert alert-info alert-dismissible fade in" role="alert">' +
                                                               '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;No existen terminales asociadas a la estación de servicio.' +
                                                           '</div>');
                        $('#deleteTerminalModal').modal('hide');
                        return;
                    }

                    var tablerow = '<br /><table class="table table-striped">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<th class="tableHeader center" colspan="6">Terminales</th>' +
                                            '</tr>' +
                                        '</thead>' +
                                    '<tbody>';
                    for (var i = 0; i < data.length; i = i + 3){
                        tablerow = tablerow + '<tr><th>' + data[i].TerminalId + '</th>' +
                                              '<th style="border-right: 1px solid #ccc;" class="text-right">' +
                                                  '<a class="delete-color btnDeleteTerminal" terminalid="' + data[i].TerminalId + '" title="Eliminar"><i class="mdi mdi-32px mdi-close-box"></i></a>' +
                                              '</th>';
                        if (i < data.length - 2) {
                            tablerow = tablerow + '<th>' + data[i + 1].TerminalId + '</th>' +
                                                  '<th style="border-right: 1px solid #ccc;" class="text-right">' +
                                                       '<a class="delete-color btnDeleteTerminal" terminalid="' + data[i + 1].TerminalId + '" title="Eliminar"><i class="mdi mdi-32px mdi-close-box"></i></a>' +
                                                  '</th>' +
                                                  '<th>' + data[i + 2].TerminalId + '</th>' +
                                                  '<th class="text-right">' +
                                                       '<a class="delete-color btnDeleteTerminal" terminalid="' + data[i + 2].TerminalId + '" title="Eliminar"><i class="mdi mdi-32px mdi-close-box"></i></a>' +
                                                  '</th>';
                        } else if (i < data.length - 1) {
                            tablerow = tablerow + '<th>' + data[i + 1].TerminalId + '</th>' +
                                                  '<th class="text-right">' +
                                                       '<a class="delete-color btnDeleteTerminal" terminalid="' + data[i + 1].TerminalId + '" title="Eliminar"><i class="mdi mdi-32px mdi-close-box"></i></a>' +
                                                  '</th>';
                        }                        
                        tablerow = tablerow + '</tr>';
                    }
                    $('#terminalsTableContainer').html(tablerow + '</tbody></table>');
                } else {
                    $('#terminalsTableContainer').html('<br />' +
                                                       '<div class="alert alert-danger alert-dismissible fade in" role="alert">' +
                                                           '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Ocurrió un error durante el almacenamiento de terminales. Por favor intentelo de nuevo.' +
                                                       '</div>');
                }
                initControls();
                $('#deleteTerminalModal').modal('hide');
            }
        });
    }

    var addTerminals = function () {
        var terminalId = $('#txtAddTerminal').val();        
        if (terminalId == null || terminalId == '' || terminalId == undefined) return;
        $.ajax({
            url: 'ServiceStations/ValidateTerminal',
            type: 'POST',
            data: JSON.stringify({ TerminalId: terminalId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data != 'Error' && data != 'Exists') {
                    $('#terminalsTableContainer').html('');
                    var tablerow = '<br /><table class="table table-striped">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<th class="tableHeader center" colspan="6">Terminales</th>' +
                                            '</tr>' +
                                        '</thead>' +
                                    '<tbody>';
                    for (var i = 0; i < data.length; i = i + 3) {
                        tablerow = tablerow + '<tr><th>' + data[i].TerminalId + '</th>' +
                                              '<th style="border-right: 1px solid #ccc;" class="text-right">' +
                                                  '<a class="delete-color btnDeleteTerminal" terminalid="' + data[i].TerminalId + '" title="Eliminar"><i class="mdi mdi-32px mdi-close-box"></i></a>' +
                                              '</th>';
                        if (i < data.length - 2) {
                            tablerow = tablerow + '<th>' + data[i + 1].TerminalId + '</th>' +
                                                  '<th style="border-right: 1px solid #ccc;" class="text-right">' +
                                                       '<a class="delete-color btnDeleteTerminal" terminalid="' + data[i + 1].TerminalId + '" title="Eliminar"><i class="mdi mdi-32px mdi-close-box"></i></a>' +
                                                  '</th>' +
                                                  '<th>' + data[i + 2].TerminalId + '</th>' +
                                                  '<th class="text-right">' +
                                                       '<a class="delete-color btnDeleteTerminal" terminalid="' + data[i + 2].TerminalId + '" title="Eliminar"><i class="mdi mdi-32px mdi-close-box"></i></a>' +
                                                  '</th>';
                        } else if (i < data.length - 1) {
                            tablerow = tablerow + '<th>' + data[i + 1].TerminalId + '</th>' +
                                                  '<th class="text-right">' +
                                                       '<a class="delete-color btnDeleteTerminal" terminalid="' + data[i + 1].TerminalId + '" title="Eliminar"><i class="mdi mdi-32px mdi-close-box"></i></a>' +
                                                  '</th>';
                        }                        
                        tablerow = tablerow + '</tr>';
                    }
                    $('#terminalsTableContainer').html(tablerow + '</tbody></table>');
                } else if (data == 'Error' && data != 'Exists'){
                    $('#terminalsTableContainer').html('<br />' +
                                                       '<div class="alert alert-danger alert-dismissible fade in" role="alert">' +
                                                           '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Ocurrió un error durante el almacenamiento de terminales. Por favor intentelo de nuevo.' +
                                                       '</div>');
                } else if (data != 'Error' && data == 'Exists'){
                    $('#addTerminalValidation').html('<br />' +
                                                       '<div class="alert alert-warning alert-dismissible fade in" role="alert">' +
                                                           '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Ya existe esta terminal.' +
                                                       '</div>');
                    $('#addTerminalValidation').show('slow');
                    setTimeout(function () { $('#addTerminalValidation').hide('slow'); }, 3000);
                }
                initControls();
            }
        });
    }

    var initDropsDown = function () {
        $('#ddlCantonsId').select2('data', {});
        $('#ddlCantonsId').select2({
            escapeMarkup: function (m) { return m; }
        });

        $('#ddlProvincesId').select2('data', {});
        $('#ddlProvincesId').select2({
            escapeMarkup: function (m) { return m; }
        });
    }

    var addServiceStationModal = function () {   
        $('#loadForm').find('#id').val('-1');
        $('#loadForm').submit();               
    };

    var onSuccessLoad = function () {        
        $('#detailContainer').show('slow');
        $('#btnAdd').attr('disabled', 'disabled');
        $('#gridContainer').addClass('removetop');
        initDropsDown();
        initControls();
        $(window).scrollTop(0);
    };

    var onCountryChange = function () {
        var vCountryId = $("#ddlPais").val();
        $('#ddlProvincesId').empty();
        $('#ddlProvincesId').append('<option value>Seleccione una Provincia</option>');

        $.ajax({
            url: 'ServiceStations/LoadStates',
            type: 'POST',
            data: JSON.stringify({ countryId: vCountryId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    $('#ddlProvincesId').append('<option value="' + data[i].StateId + '">'
                            + data[i].Name +
                            '</option>');
                }
            }
        });
    };

    var onProvinceChange = function () {       
        var vCountryId = $("#CountryId").val();
        var vProvinceId = $('#ddlProvincesId').val();
        $('#ddlCantonsId').empty();
        $('#ddlCantonsId').append('<option value>Seleccione un Canton</option>');

        $.ajax({
            url: '/Socio/ServiceStations/LoadCantons',
            type: 'POST',
            data: JSON.stringify({ stateId: vProvinceId, countryId: vCountryId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {

                for (var i = 0; i < data.length; i++) {
                    $('#ddlCantonsId').append('<option value="' + data[i].CountyId + '">'
                            + data[i].Name +
                            '</option>');
                }
            }
        });
    };

    var addOrEditModalClose = function () {
        $('#detailContainer').hide('slow');
        $('#btnAdd').removeAttr('disabled');
        $('#gridContainer').removeClass('removetop');
        $('body').loader('hide');
        ECOsystem.Utilities.setConfirmation();
    };

    var successSearch = function () {
        $('#txtSearchS').val('');
    };

    var deleteModalClose = function () {
        $('#deleteModalStacionesServicio').modal('hide');
        hideLoader();
    }

    var onClickImport = function () {
        $('#ImportModal').modal('show');
    };

    var onClickImport2 = function () {
        $('#ImportCSV').trigger('click');
        $("#txtFile").val("");
    };

    var onClickImport3 = function () {
        $('body').loader('show');
        $('#ImportForm').submit();
    };

    var onClickImport4 = function () {
        $('#ImportModal2').modal('show');
    };

    var onClickImportXlsx = function () {
        $('#xlf').trigger('click');
        $("#txtFileXlsx").val("");
    }

    var downloadFile = function () {        
        $('#DownloadFileForm').submit();
    };

    $('#btnGetDownloadData').click(function (e) {
        $('#GetDataToDownloadForm').submit();
        e.preventDefault();
    });

    var ValidateFormat = function (input) {
        
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var files = input.files;
            for (var i = 0, f; f = files[i]; i++) {
                // Only process csv files.
                if (!f.name.match('\.csv') || (f.size > 46030)) {
                    return false;
                }
                else {
                    return true;
                }
            }           
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.', true);
        }
    };
    
    var getFilename = function (input) {
        
        if (ValidateFormat(input)) {
            if (input.files && input.files[0]) {
                var value = $("#ImportCSV").val();
                var NameOfFile = input.files[0].name;
                $("#txtFile").val(NameOfFile);
                setErrorFormatMsj('', false);
                $("#btnImport").removeClass('hidden');
                
            }
        }
        else {
            $("#txtFile").val("");
            setErrorFormatMsj('* El formato de archivo es inválido o el tamaño del archivo sobrepasa lo permitido, favor seleccionar un archivo de formato CSV con un tamaño menor a 45KB (Equivalente a 500 líneas). Se sugiere dividir el archivo para poder subirlo por partes.', true);
            $("#btnImport").addClass('hidden');
        }

    }

    var setErrorFormatMsj = function (msj, show) {
        if (show) {
            $('#errorFormatMsj').html(msj);
            $('#validationFormatErrors').removeClass('hide');
        } else {
            $('#errorFormatMsj').html('');
            $('#validationFormatErrors').addClass('hide');
        }
    }

    var showLoader = function () {        
	    $('#confirmationaddoreditmodal').modal('hide');
        $('body').loader('show');
    };

    var onSuccessReportData = function (data) {
        hideLoader();
        if (data == "Success") {
            $('#DonwloadExcelForm').submit();
        }
    }

    var hideLoader = function () {
        $('body').loader('hide');
    };

    return {
        Init: initialize,
        AddServiceStationModal: addServiceStationModal,
        OnSuccessLoad: onSuccessLoad,
        OnCountryChange: onCountryChange,
        OnProvinceChange: onProvinceChange,
        AddOrEditModalClose: addOrEditModalClose,
        SuccessSearch: successSearch,        
        DeleteModalClose: deleteModalClose,
        OnClickImport: onClickImport,
        OnClickImport2: onClickImport2,
        OnClickImport3: onClickImport3,
        OnClickImport4: onClickImport4,
        OnClickImportXlsx: onClickImportXlsx,
        DownloadFile: downloadFile,
        GetFilename: getFilename,
	    ShowLoader: showLoader,
        OnSuccessReportData: onSuccessReportData
       };
})();
