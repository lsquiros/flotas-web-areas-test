﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.PartnerUsers = (function () {
    var options = {};

    
    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        $('#PartnerAuthorizedLoginModal').on('hidden.bs.modal', function () {
            $('#ddlSelectionList').val('');
            $('#ddlSelectionList').removeClass('hide');

            $('#ddlPartnerList').val('');
            $('#lblPartnerList').addClass('hide');
            $('#ddlPartnerList').addClass('hide');
            $('#treeview-checkable').html('');
            $('#CheckAll').addClass('hidden');

            $('#dataContainer').addClass('hide');
            $('#SearchDiv').addClass('hide');
            $('#lbl_rol').addClass('hide');
            $('#ddlRolePartnerList').addClass('hide');
            $('#ddlRoleCustomerList').addClass('hide');

            $('#divsave').addClass('hide');
            $('#modalBodyId').animate({ height: '70px' }, "slow");
        });

        $('#ddlPartnerList').change(function () {
            //Clean the container and load the information again
            $('#treeview-checkable').html('');
            $('#CheckAll').addClass('hidden');

            var val = $('#ddlPartnerList').val();
            if (val == '') {
                $('#dataContainer').addClass('hide');
                $('#treeview-checkable').html('');
                $('#CheckAll').addClass('hidden');
                $('#divsave').addClass('hide');
            } else {
                $('#dataContainer').removeClass('hide');
                $('#divsave').removeClass('hide');
                $('#lbl_rol').removeClass('hide');
                $('#ddlRoleCustomerList').removeClass('hide');
                $('#ddlRolePartnerList').addClass('hide');
                $('#modalBodyId').animate({ height: '450px' }, "slow");
                GetRoleSelected();
                ShowCustomersTreeData();
            }
        });

        $('#ddlSelectionList').change(function () {

            //Clean the container and load the information again
            $('#ddlPartnerList').val('');
            $('#lblPartnerList').addClass('hide');
            $('#ddlPartnerList').addClass('hide');
            $('#treeview-checkable').html('');
            $('#CheckAll').addClass('hidden');

            var val = $('#ddlSelectionList').val();
            switch (val) {
                case '0':
                    $('#dataContainer').removeClass('hide');
                    $('#SearchDiv').removeClass('hide');
                    $('#lbl_rol').addClass('hide');
                    $('#ddlRolePartnerList').removeClass('hide');
                    $('#ddlRoleCustomerList').addClass('hide');

                    $('#divsave').removeClass('hide');
                    $('#modalBodyId').animate({ height: '400px' }, "slow");

                    GetRoleSelected();
                    ShowCustomersTreeData(0);
                    break;
                case '1':
                    $('#dataContainer').addClass('hide');
                    $('#SearchDiv').removeClass('hide');
                    $('#lbl_rol').removeClass('hide');
                    $('#ddlRolePartnerList').addClass('hide');
                    $('#ddlRoleCustomerList').removeClass('hide');

                    $('#lblPartnerList').removeClass('hide');
                    $('#ddlPartnerList').removeClass('hide');

                    $('#divsave').addClass('hide');
                    $('#modalBodyId').animate({ height: '150px' }, "slow");

                    break;
                case '2':
                    $('#dataContainer').removeClass('hide');
                    $('#SearchDiv').addClass('hide');

                    $('#divsave').removeClass('hide');
                    $('#modalBodyId').animate({ height: '400px' }, "slow");

                    ECOsystem.Utilities.ShowLoader();
                    ShowCustomersTreeData(2);
                    break;
                default:
                    $('#dataContainer').addClass('hide');
                    $('#lbl_rol').addClass('hide');
                    $('#SearchDiv').addClass('hide');

                    $('#divsave').addClass('hide');
                    $('#modalBodyId').animate({ height: '70px' }, "slow");
                    break;
            }
        });

        $('#ddlRoleCustomerList').change(function () {
            $('#errorMessage').html('');
        });

        $('#ddlRolePartnerList').change(function () {
            $('#errorMessage').html('');
        });

        $('#gridContainer').on('click.edit', 'a[edit_row]', function (e) {
            ECOsystem.Utilities.ShowLoader();
        });

        $('#ddlRoleCustomerList').select2('data', {});
        $('#ddlRoleCustomerList').select2({
            formatResult: RoleCustomerSelectFormat,
            formatSelection: RoleCustomerSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('#RoleId').select2('data', {});
        $('#RoleId').select2({
            formatResult: RoleIdSelectFormat,
            formatSelection: RoleIdSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('#ddlRolePartnerList').select2('data', {});
        $('#ddlRolePartnerList').select2({
            formatResult: RoleCustomerSelectFormat,
            formatSelection: RoleCustomerSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        //trigger click on hidden file input
        $('#btnUpload').off('click.btnUpload').on('click.btnUpload', function (e) {
            $('#fileHidden').trigger('click');
            e.stopPropagation();
        });
        fileApiUpload();
        formValidation();
        onClickNew();
        onClickAssignClient();

        $('#gridContainerClient').off('click.del', 'a[del_row]').on('click.del', 'a[del_row]', function (e) {
            $('#deleteCustomerbyPartner').find('#id').val($(this).attr('id'));            
        });

        $('#gridContainer').off('click.SetUser', 'a[set-user-row]').on('click.SetUser', 'a[set-user-row]', function (e) {
            $('#PartnerAuthorizedLoginModal').find('#PUserId').val($(this).attr('id'));
            $('#PartnerAuthorizedLoginModal').find('#PUserName').html($(this).attr('data-username'));
            $('#PartnerAuthorizedLoginModal').find('#PRole').val($(this).attr('data-role'))

            ECOsystem.Utilities.ShowLoader();
            GetUserPermissions();
        });     

        $('#IsSupportUser').off('click.IsSupportUser').on('click.IsSupportUser', function () {
            if ($(this).prop('checked')) {
                $('#showRoles').hide('slow');
            } else {
                $('#showRoles').show('slow');
            }          
        });
        
        $("#txtSearch").on('keydown', function () {
            var key = event.keyCode || event.charCode;

            if (key == 8 || key == 46) {
                if ($(this).val().length == 0) {
                    $("#btnSearch").trigger("click");
                }
            }            
        });
    }
    
    var RoleCustomerSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +             
                '</div>';
    };

    var RoleIdSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
                '</div>';
    };

    /* Standard Error Function for File API */
    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };
        
    /* Update progress for File API */
    var fileApiUpdateProgress = function (e) {
        if (e.lengthComputable) {
            var percentLoaded = Math.round((e.loaded / e.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                $('.percent').css('width', percentLoaded + '%').html(percentLoaded + '%');
            }
        }
    };

    
    /* File Upload implementation */
    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            $('#fileHidden').off('change.fileHidden').on('change.fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onprogress = fileApiUpdateProgress;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {
                    $('#imgPhoto').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        alert("La extensión del archivo es inválida");
                        continue;
                    }
                    // Closure to capture the file information.
                    reader.onload = (function (F) {
                        $('.percent').html('100%').css('width', '100%');
                        setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgPhoto').removeClass('imgloading');", 2000);
                        return function (e) {
                            var lsrc = e.target.result;
                            $('#imgPhoto').attr({
                                src: lsrc,
                                title: escape(F.name)
                            });
                            $('#Photo').val(lsrc);
                        };
                    })(f);
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
            });
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.');
        }
    };
        
    /* Form validation to prevent an invalid submission*/
    var formValidation = function () {
        $('#detailContainer').off('click.btnSaveSubmit', 'button[type=submit]').on('click.btnSaveSubmit', 'button[type=submit]', function (e) {
            setErrorMsj('', false);
            if(validateEmail()/* && validateCountry()*/) {
                return true;
            } else {
                return false;
            }
        });
    };
        
    /* Email validation to prevent an invalid submission*/
    var validateEmail = function () {
        if ($('#DecryptedEmail').val().length == 0) {
            return true;
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#DecryptedEmail').val().match(email)) {
            return true;
        } else {
            setErrorMsj('Correo electrónico invalido', true);
            return false;
        }
    };

    ///*validate at least 1 Country selected*/
    //var validateCountry = function () {
    //    var numberOfChecked = $('input:checkbox[data-checkbox-index]:checked').length;
    //    if (numberOfChecked < 1) {
    //        setErrorMsj('Se debe seleccionar al menos un país', true);
    //        return false;
    //    }

    //    $('input:checkbox[data-checkbox-index]').each(function() {
    //        var index = $(this).attr('data-checkbox-index');
    //        if (this.checked) {
    //            $('#PartnerUser_CountriesAccessList_' + index + '__IsCountryChecked').val('True');
    //        } else {
    //            $('#PartnerUser_CountriesAccessList_' + index + '__IsCountryChecked').val('False');
    //        }
    //    });
    //    return true;
    //}
    
    /* Show or hide errors messages*/
    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj').html(msj);
            $('#validationErrors').removeClass('hide');
        } else {
            $('#errorMsj').html('');
            $('#validationErrors').addClass('hide');
        }
    }
    
    /* On Success Load after call edit*/    
    var onSuccessLoad = function () {
        initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
        ECOsystem.Utilities.HideLoader();
    };

    /* Save the information from the treeview*/
    var saveCustomersAuthorized = function () {
        ECOsystem.Utilities.ShowLoader();

        var val = $('#ddlSelectionList').val();
        var checked = $('#PartnerAuthorizedLoginModal').find('input[type=checkbox][id!=-1]:checked');
        var userId = $('#PUserId').val();
        var partnerId = $('#ddlPartnerList').val() == null || $('#ddlPartnerList').val() == false ? 0 : $('#ddlPartnerList').val();
        var list = [];

        var role = $('#ddlRoleCustomerList').val();
        if (role == null || role == "") role = $('#ddlRolePartnerList').val();

        if ((role != "") || val == null) {
            $('#errorMessage').html('');

            if ($('#PRole').val() == 'SERVICESTATION_ADMIN') {
                val = 2;
            }

            if (checked.length == 0) {
                savePermissions(list, userId, role, val, partnerId);
            }

            $.each(checked, function (index) {
                list.push(this.id);

                if (index == checked.length - 1) {
                    savePermissions(list, userId, role, val, partnerId);
                }
            })
        } else {
            $('#errorMessage').html('Por favor seleccione un rol');
            $('#ConfirmationSaveModalAuthorization').modal('hide');
            ECOsystem.Utilities.HideLoader();
        }
    };

    var savePermissions = function (list, userId, role, val, partnerId) {
        $.ajax({
            url: '/PartnerUsers/SaveTreeViewData',
            type: 'POST',
            data: JSON.stringify({ List: list, UserId: userId, RoleId: role, selectType: val, PartnerId: partnerId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data == 1 || data == 2) {
                    if (val == 0) $('#SpanTitle').html('Asignación de socios a un usuario')

                    $('#ConfirmationSaveModalAuthorization').modal('hide');
                    $('#InformationMessageModal').modal('show');
                    location.reload();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $('#treeview-checkable').html('');
                    $('#PartnerAuthorizedLoginModal').modal('hide');
                }
            },
            complete: function () {
                ECOsystem.Utilities.HideLoader();
            }
        });
    }
      
    /* On click BtnNew*/    
    var onClickNew = function () {
        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });
    };
    
    /* On click BtnNew*/
    var onClickAssignClient = function () {
        $('#btnAssignClient').off('click.btnAssignClient').on('click.btnAssignClient', function (e) {         
            var custid = $('#ddlAssignCustomer').val();
            var UserId = $('#UserIdAddCustomer').val();
            $('#saveFormCustomerbyPartner').find('#Custid').val(custid);
            $('#saveFormCustomerbyPartner').find('#UserId').val(UserId);
            $('#saveFormCustomerbyPartner').submit();
        });
    };
    
    /* Close modal for delete after that information has been deleted*/
    var deleteModalClose = function () {
        $('#deleteCustomerbyPartner').modal('hide');       
    };

    var searchByStatus = function (statusType) {
        setStatusName(statusType);
        $('#searchForm').submit();
    }

    var setStatusName = function (statusType) {
        switch (statusType) {
            case 1:
                $('#btnStatusName').html('Activos <span class="caret"></span>');
                break;
            case 0:
                $('#btnStatusName').html('Inactivos <span class="caret"></span>');
                break;
            default:
                $('#btnStatusName').html('Todos <span class="caret"></span>');
        }

        $('#searchFormStatusId').val(statusType);
    }

    var cleanFilters = function () {
        /*** Reset active filter ***/
        $('#btnStatusName').html('Todos <span class="caret"></span>');
        $('#searchFormStatusId').val(2);
        /*** Reset textbox filter ***/
        $('#txtSearch').val('');
        /*** Reset grid ***/
        $('#searchForm').submit();
    }

    var validEmail = function () {
        $('.valid').removeAttr("style");

        if ($('#UserId').val() == '' || $('#UserId').val() == undefined) {
            if ($("#addOrEditForm").valid()) {
                $.ajax({
                    url: '/PartnerUsers/IsValidEmail',
                    type: 'POST',
                    data: JSON.stringify({ email: $("#DecryptedEmail").val() }),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        $("#addOrEditForm").submit();
                    },
                    error: function (data) {
                        ECOsystem.Utilities.SetMessageShow("El correo electrónico ya es utilizado.", "ERROR");
                    }
                });
            } else {
                $("#addOrEditForm").submit(); //If has error and we use submit the user can see the errors.
            }
        } else {
            $("#addOrEditForm").submit();
        }
    };

    var ShowCustomersTreeData = function (value) {
        var userId = $('#PUserId').val();
        var partnerId = $('#ddlPartnerList').val() == null || $('#ddlPartnerList').val() == false ? 0 : $('#ddlPartnerList').val();
        var selecttype = value != null ? value : $('#ddlSelectionList').val();

        $.ajax({
            url: '/Socio/PartnerUsers/GetTreeData',
            type: 'POST',
            data: JSON.stringify({ UserId: userId, selectType: selecttype, PartnerId: partnerId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                $('#-1').prop('checked', false);

                var json = data;
                if (json == '[]') {
                    $('#treeview-checkable').html('');
                    $('#treeview-checkable').prepend('<p class="top20 text-success">No se encontraron resultados</p>');
                    $('#CheckAll').addClass('hidden');
                    $('#divsave').addClass('hide');
                } else {
                    $('#CheckAll').removeClass('hidden');

                    for (var i = 0; i < data.length; i += 1) {
                        if (data[i].state != undefined && data[i].state.checked == true) {
                            $('#treeview-checkable').append('<li class="list-group-item node-treeview-checkable"><div class="checkbox"><label><input type ="checkbox" class="generateAllCards" id="' + data[i].id + '" value="' + data[i].text + '" checked>' + data[i].text + '</label></div></li>');
                        } else {
                            $('#treeview-checkable').append('<li class="list-group-item node-treeview-checkable"><div class="checkbox"><label><input type ="checkbox" class="generateAllCards" id="' + data[i].id + '" value="' + data[i].text + '">' + data[i].text + '</label></div></li>');
                        }

                        if (i == data.length - 1) {
                            if ($('#PartnerAuthorizedLoginModal').find('input[type=checkbox][id!=-1]').length ==
                                $('#PartnerAuthorizedLoginModal').find('input[type=checkbox][id!=-1]:checked').length) {
                                $('#-1').prop('checked', true);
                            }

                            $('#PartnerAuthorizedLoginModal').off('click', '.generateAllCards').on('click', '.generateAllCards', function (e) {
                                var id = $(this).attr('id');
                                var ischecked = $(this).prop("checked");

                                if (id == -1) {
                                    $('#PartnerAuthorizedLoginModal').find('input:checkbox').each(function () {
                                        $(this).prop('checked', ischecked);
                                    });
                                } else {
                                    var allCheckboxes = $('#PartnerAuthorizedLoginModal').find('input[type=checkbox][id!=-1]').length;
                                    var selectedCheckboxes = $('#PartnerAuthorizedLoginModal').find('input[type=checkbox][id!=-1]:checked').length;

                                    if (allCheckboxes == selectedCheckboxes) {
                                        $('#-1').prop('checked', true);
                                    } else {
                                        $('#-1').prop('checked', false);
                                    }
                                }
                            });
                        }
                    }
                }

                ECOsystem.Utilities.HideLoader();
                $('#PartnerAuthorizedLoginModal').modal('show');
            }
        });
    };

    var GetRoleSelected = function () {
        ECOsystem.Utilities.ShowLoader();
        var userId = $('#PUserId').val();
        var val = $('#ddlSelectionList').val();

        $.ajax({
            url: '/Socio/Customers/GetRoleForCustomerSelected',
            type: 'POST',
            data: JSON.stringify({ UserId: userId, SelectType: val }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {

                if (data == '') {
                    $('#ddlRoleCustomerList').val('').change();
                    $('#ddlRolePartnerList').val('').change();
                }
                else {
                    $('#ddlRoleCustomerList').val(data).change();
                    $('#ddlRolePartnerList').val(data).change();
                }
            }
        });
    }

    var GetUserPermissions = function () {
        $('#PartnerAuthorizedLoginModal').modal('show');

        if ($('#PRole').val() == 'SERVICESTATION_ADMIN') {

            $('#labeldrescription').html('Seleccione las estaciones a asignar:');
            $('#lbl_rol').addClass('hide');
            $('#ddlSelectionList').val('2');
            $('#ddlSelectionList').addClass('hide');

            $('#treeview-checkable').html('');
            $('#CheckAll').addClass('hidden');

            $('#divsave').removeClass('hide');
            $('#ddlPartnerList').val('');
            $('#modalBodyId').animate({ height: '400px' }, "slow");
            $('#ddlRolePartnerList').addClass('hide');
            $('#lbl_rol').addClass('hide');
            $('#ddlRoleCustomerList').addClass('hide');
            $('#ddlPartnerList').addClass('hide');
            $('#lblPartnerList').addClass('hide');

            $('#dataContainer').removeClass('hide');

            ShowCustomersTreeData(2);
        } else {
            ECOsystem.Utilities.HideLoader();
        }
    }

    /* Public methods */    
    return {
        Init: initialize,
        DeleteModalClose: deleteModalClose,
        OnSuccessLoad: onSuccessLoad,
        SaveCustomersAuthorized: saveCustomersAuthorized,
        SearchByStatus: searchByStatus,
        SetStatusName: setStatusName,
        CleanFilters: cleanFilters,
        ValidEmail: validEmail
    };
})();

