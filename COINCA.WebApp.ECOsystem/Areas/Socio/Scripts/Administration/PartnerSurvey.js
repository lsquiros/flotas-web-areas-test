﻿var ECOsystem = ECOsystem || {};

ECOsystem.PartnerSurvey = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $('#gridContainer').off('click.edit', 'a[edit_row]').on('click.edit', 'a[edit_row]', function (e) {
            var id = $(this).attr('id');
            $('#LoadPartnerSurveyForm').find('#id').val(id);
            $('#LoadPartnerSurveyForm').submit();
            $('#btnAdd').attr('disabled', true);
            showLoader();
        });

        $('#gridContainer').off('click.del', 'a[del_row]').on('click.del', 'a[del_row]', function (e) {
            var id = $(this).attr('id');
            $('#deletePartnerSurveyForm').find('#id').val(id);
        });

        $('#btnAdd').click(function () {
            $('#LoadPartnerSurveyForm').find('#id').val(null);
            $('#LoadPartnerSurveyForm').submit();
            showLoader();
        });

        $('#showDetails').click(function () {
            var elem = $('#surveyDetails');

            if (elem.css('display') == 'block') {
                $('#surveyDetails').hide('slow');
            } else {
                $('#surveyDetails').show('slow');
            }
        });
    }

    /* Initialize and Disable controls */
    var initQuestionControls = function () {
        $('#QuestionTypeId').select2()
            .on('change', function () {
                var val = $(this).val();
                if (val) {
                    $('#currentQuestionTypeId').val(val);
                    showOrHideOptions(val);
                }
            });

        $('.editOption').click(function () {
            $('#loadAnswerForm').find('#id').val($(this).attr('answerId'));
            $('#loadAnswerForm').find('#questionTypeId').val($('#QuestionTypeId').val());
            $('#loadAnswerForm').submit();
        });

        $('.removeOption').click(function () {
            $('#deleteAnswerModal').find('#questionText').val($('#questionContainer').find('#Text').val());
            $('#deleteAnswerModal').find('#questionTypeId').val($('#detailContainer').find('#currentQuestionTypeId').val());
            $('#deleteAnswerModal').find('#id').val($(this).attr('answerId'));
            $('#deleteAnswerModal').modal('show');
        });        

        $('#btnAddOrEditQuestion').click(function () {
            var survey = getSurveyInfo()
            var text = $('#detailContainer').find('#Text').val();
            var questionType = $('#detailContainer').find('#QuestionTypeId').val();
            if (text && questionType) {
                $('#QuestionRequireMessage').hide();
                $('#addOrEditQuestionForm').find('#obj').val(JSON.stringify(survey));
                $('#addOrEditQuestionForm').find('#text').val(text);
                $('#addOrEditQuestionForm').find('#questionTypeId').val(questionType);
                $('#addOrEditQuestionForm').submit();
            } else {
                $('#detailContainer').find('#Text').focus()
                $('#QuestionRequireMessage').show();
            }            
        });

        $('#btnCancelAddOrEditQuestion').click(function () {
            $.ajax({
                url: '/PartnerSurvey/CancelAddOrEditPartnerSurveyQuestion',
                type: 'POST',
                success: function () {
                    $('#detailContainer').find('#questionContainer').hide('slow');
                    $('#detailContainer').find('#questionContainer').html('');
                    $('#detailContainer').find('#questionTableContainer').show('slow');
                    disableControls(false);
                }
            });
        });

        initNewOption();
    }

    var initOptionControls = function () {
        $('#optionDetailContainer').find('#btnCloseOption').click(function () {
            $('#splitQuestionContainer').toggleClass('col-sm-6');
            $('#splitQuestionContainer').addClass('col-sm-12');
            disableControls(false, true);
            $('#optionSplitDetailContainer').hide();
            initQuestionControls();            
        });

        $('#optionDetailContainer').find('#btnAddOrEditOption').click(function () {
            var model = {
                Id: $('#optionDetailContainer').find('#Id').val(),
                Order: $('#optionDetailContainer').find('#Order').val(),                
                Text: $('#optionDetailContainer').find('#Text').val(),
            }
            var text = $('#optionDetailContainer').find('#Text').val();
            if (text) {
                $('#OptionRequireMessage').hide();
                $('#addOrEditAnswerForm').find('#questionText').val($('#questionContainer').find('#Text').val()); 
                $('#addOrEditAnswerForm').find('#questionTypeId').val($('#detailContainer').find('#currentQuestionTypeId').val());
                $('#addOrEditAnswerForm').find('#obj').val(JSON.stringify(model));
                $('#addOrEditAnswerForm').submit();
            } else {
                $('#optionDetailContainer').find('#Text').focus();
                $('#OptionRequireMessage').show();
            }
        });

        $('#optionDetailContainer').find('#NextSurveyQuestionId').select2();
    }

    var initControls = function () {
        $('#TargetId').select2();

        $('.editQuestion').click(function () {
            removeShadow();
            var order = getItemsOrder();

            $('#loadQuestionForm').find('#id').val($(this).attr('id'));
            $('#loadQuestionForm').find('#questionTypeId').val($(this).attr('questionTypeId'));
            $('#loadQuestionForm').find('#order').val(JSON.stringify(order));
            $(this).closest('.sortQuestion').addClass('boxShadowSelect');
            disableControls(true);
            $('#loadQuestionForm').submit();
        });

        $('.removeQuestion').click(function () {
            var survey = getSurveyInfo();
            $('#deleteQuestionModal').find('#id').val($(this).attr('id'))
            $('#deleteQuestionModal').find('#obj').val(JSON.stringify(survey));
            $('#deleteQuestionModal').modal('show');
        });        

        $('#btnCloseSurvey').click(function () {
            onFinishSurveyAddOrEdit();
        });

        $('#btnSaveSurvey').click(function () {
            var survey = getSurveyInfo();
            var order = getItemsOrder();
            if (survey.Name && survey.TargetId) {
                if (order.length > 0) {
                    $('#SurveyRequireMessage').hide();
                    $('#SurveyRequireQuestionMessage').hide();
                    $('#AddOrEditPartnerSurveyForm').find('#obj').val(JSON.stringify(survey));
                    $('#AddOrEditPartnerSurveyForm').find('#order').val(JSON.stringify(order));
                    $('#AddOrEditPartnerSurveyForm').submit();
                } else {
                    $('#SurveyRequireQuestionMessage').show();
                }
            } else {
                $('#Name').focus();
                $('#SurveyRequireMessage').show();
            }
        });

        $('#btnAddNewQuestion').click(function () {
            removeShadow();
            disableControls(true);
            var order = getItemsOrder();

            $('#loadQuestionForm').find('#id').val(null);
            $('#loadQuestionForm').find('#questionTypeId').val(null);
            $('#loadQuestionForm').find('#order').val(JSON.stringify(order));
            $('#loadQuestionForm').submit();
        });
    }

    var disableControls = function (val, isOption) {
        if (!isOption) {
            $('#btnAddNewQuestion').attr('disabled', val);
            $('#btnSaveSurvey').attr('disabled', val);
        } else {
            $("#splitQuestionContainer *").prop('disabled', val);
        }
    }
    /* ****************************** */

    var initNewOption = function () {
        $('#btnAddNewOption').click(function () {
            $('#loadAnswerForm').find('#id').val(null);
            $('#loadAnswerForm').find('#questionTypeId').val($('#QuestionTypeId').val());
            $('#loadAnswerForm').submit();
        });
    };

    var showOrHideOptions = function (questionTypeId) {
        $.ajax({
            url: '/PartnerSurvey/ShowOrHideOptions',
            type: 'POST',
            data: { questionTypeId: questionTypeId },
            success: function (data) {
                $('#OptionContainer').html(data);
                initNewOption();
            }
        });
    }

    var getSurveyInfo = function () {
        var model = {
            Name: $('#Name').val(),
            TargetId: $('#TargetId').val(),
            Active: $('#Active').is(':checked'),
            Description: $('#Description').val(),
            Mandatory: $('#Mandatory').is(':checked')
        };
        return model;
    }

    var onLoadOptionSuccess = function () {
        $('#splitQuestionContainer').toggleClass('col-sm-12');
        $('#splitQuestionContainer').addClass('col-sm-6');
        disableControls(true, true);
        $('#optionSplitDetailContainer').show();
        initOptionControls();
    }

    var onLoadSuccess = function () {
        initControls();
        hideLoader();
        $('#detailContainer').show('slow');
        $("#sortableQuestion").sortable();
    }

    var onSuccessLoadQuestion = function () {
        $('#detailContainer').find('#questionContainer').show('slow');
        $('#detailContainer').find('#questionTableContainer').hide('slow');
        disableControls(true);
        initQuestionControls();
    }

    var onFinishSurveyAddOrEdit = function () {
        $('#detailContainer').hide('hide');
        $('#detailContainer').html('');
        $('#btnAdd').attr('disabled', false);
        $(window).scrollTop(0);
        hideLoader();
    }

    var onSuccessAddOrEditAnswer = function () {
        var showOther = $('#optionDetailContainer').find('#AddNewAnswer').is(':checked');

        if (!showOther) {
            $('#splitQuestionContainer').removeClass('col-sm-6');
            $('#splitQuestionContainer').addClass('col-sm-12');
            disableControls(false, true);
            $('#optionSplitDetailContainer').hide();
            initQuestionControls();            
        } else {            
            $('#loadAnswerForm').find('#id').val(null);
            $('#loadAnswerForm').find('#questionTypeId').val($('#QuestionTypeId').val());
            $('#loadAnswerForm').submit();
        }
        hideLoader();
    }

    var onSuccessAddOrEditQuestion = function () {
        $('#detailContainer').find('#questionContainer').hide('slow');
        $('#detailContainer').find('#questionTableContainer').show('slow');
        disableControls(false);
        initQuestionControls();
    };

    var onDeleteSuccess = function () {
        $('#deleteModal').modal('hide');
        hideLoader();
    }

    var getItemsOrder = function () {
        var list = [];
        var order = 1;

        $('#sortableQuestion').find('.sortQuestion').each(function () {
            var item = {
                'Id': $(this).attr('id'),
                'Order': order                
            };
            list.push(item);
            order++;
        });
        return list;
    }
    
    /* Utilities */
    var removeShadow = function () {
        $('.sortQuestion').each(function () {
            $(this).removeClass('boxShadowSelect');
        });
    }

    var showLoader = function () {
        $('body').loader('show');
    }

    var hideLoader = function () {
        $('body').loader('hide');
    }
    /* ****************************** */

    return {
        Init: initialize,
        OnLoadSuccess: onLoadSuccess,
        OnSuccessLoadQuestion: onSuccessLoadQuestion,
        OnFinishSurveyAddOrEdit: onFinishSurveyAddOrEdit,
        OnLoadOptionSuccess: onLoadOptionSuccess,
        OnSuccessAddOrEditAnswer: onSuccessAddOrEditAnswer,
        OnSuccessAddOrEditQuestion: onSuccessAddOrEditQuestion,
        OnDeleteSuccess: onDeleteSuccess,
        ShowLoader: showLoader,
        HideLoader: hideLoader
    };
})();