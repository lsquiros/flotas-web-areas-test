﻿var ECOsystem = ECOsystem || {};

ECOsystem.SurveyReport = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $('#customerId').select2();
        $('#surveyId').select2();
        
        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $('#btnDownload').click(function () {
            var startDate = $('#startDate').val();
            var endDate = $('#startDate').val();

            if (startDate && endDate) {
                if (new Date(startDate) > new Date(endDate)) {
                    $('#warning').html('La fecha inicial debe ser menor a la final');
                    $('#showWarning').show();
                } else {
                    $('#showWarning').hide();
                    $('#ExcelReportDownloadForm').submit();
                }                
            } else {
                $('#warning').html('Por favor ingrese la fecha inicial y fecha final');
                $('#showWarning').show();
            }
        });
    }

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function (data) {
        $('body').loader('hide');

        if (data == 'Success') {
            $('#DownloadFileForm').submit();
        } else if (data == 'NoData') {
            ECOsystem.Utilities.SetMessageShow('No existe información para generar el reporte.', 'ALERT');
        } else {
            ECOsystem.Utilities.SetMessageShow('Error al generar el reporte. Detalle técnico: ' + data, 'ERROR');
        }
    };

    return {
        Init: initialize,
        ShowLoader: showLoader,
        HideLoader: hideLoader
    };
})();