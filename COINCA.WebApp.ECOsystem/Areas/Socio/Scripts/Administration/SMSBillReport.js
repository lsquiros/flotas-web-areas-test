﻿var ECOsystem = ECOsystem || {};

ECOsystem.SMSBillReport = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        initializeDropDownList();
    }

    var initEvents = function () {
        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $("#StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });
        
        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $("#EndDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });
    }

    var initializeDropDownList = function () {
        var select1 = $("#ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);
    };

    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Month").select2('val', '');
            $("#Year").select2('val', '');
            $('#StartDateStr').val('');
            $('#EndDateStr').val('');

            $('#ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
        }
    };

    var onSelectMonthChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Month').val(obj.id);
        }
    };

    var onSelectYearChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Year').val(obj.id);
        }
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function (data) {
        $('body').loader('hide');

        if (data == 'Success') {
            $('#DownloadFileForm').submit();
        } else if (data == 'NoData') {
            $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;No existe información para generar el reporte.</div>');
        } else {
            $('#ControlsContainer').html('<div class="alert alert-danger alert-dismissible fade in" role="alert">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Error al generar el reporte. <br /> <b>Detalle técnico: </b> <br />' + data + '</div>');
        }
    };

    var onClickDownloadReport = function () {
        $('#ExcelReportDownloadForm').submit();
    }

    return {
        Init: initialize,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnClickDownloadReport: onClickDownloadReport
    };
})();

