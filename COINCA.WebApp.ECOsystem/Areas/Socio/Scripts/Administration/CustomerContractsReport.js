﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.CustomerContractsReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        try {
            initializeDropDownList();
        }
        catch (exception) {
        }
    }

    /*init Events*/
    var initEvents = function () {

            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#StartDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });
            $("#StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });


            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#EndDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });

            $("#EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });
        }
    

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        try {
            var select4 = $("#Classification").select2().data('select2');
            select4.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportClassifficationChange(data); return fn.apply(this, arguments); }
                }
            })(select4.onSelect);
        } catch (e) {

        }       

    };

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Month").select2('val', '');
            $("#Year").select2('val', '');
            $('#StartDateStr').val('');
            $('#EndDateStr').val('');

            $('#ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
        }
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Month').val(obj.id);
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Year').val(obj.id);
        }
    };

    /*on Select Report Fuel Type Change*/
    var onSelectReportClassifficationChange = function (obj) {
        $('#Classification').val(obj.id);
    };   
        
    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
            '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
            ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' +
            'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        if (($('#ReportCriteriaId').val() === '')
            || ($('#Month').val() === '')
            || ($('#Year').val() === '')
            || ($('#TransactionType').val() === '')
            || ($('#CustomerId').val() === '')
            || ($('#StartDateStr').val() === '')
            || ($('#EndDateStr').val() === '')
        ) {
            if (($('#Month').val() !== '') && ($('#Year').val() !== ''))
                return true;
            if (($('#StartDateStr').val() !== '') && ($('#EndDateStr').val() !== ''))
                return true;
            invalidSelectOption();
            return false;
        }
        return true;
    };

    var onClickDownloadReport = function () {
        $('#ReloadForm').submit();
    };

    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    /*Hide Loader*/
    var hideLoader = function (data) {
        if (data > 0) {
            $('body').loader('hide');
            $('#DownloadFileForm').submit();
        } else {
            $('body').loader('hide');
            invalidSelectOption('No datos encontrados con los criterios de busqueda.');
        }
    };

    /* Public methods */
    return {
        Init: initialize,
        OnClickDownloadReport: onClickDownloadReport,       
        ShowLoader: showLoader,
        HideLoader: hideLoader
    };
})();

