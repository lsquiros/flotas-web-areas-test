﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.AmountByServiceStationsReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
    }

    /*init Events*/
    var initEvents = function () {
        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $("#EndDate").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });
    }
 
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function (data) {
        $('body').loader('hide');
        if (data == 'Success') {
            $('#DonwloadFileForm').submit();
        } else if (data == 'No Data') {
            $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                        'No existen datos con los parámetros seleccionados. </div>');
        } else {
            $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           'Ocurrió un error al procesar el reporte, por favor intentelo de nuevo. <hr />' +
                                           '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
        }
    };

    /* Public methods */
    return {
        Init: initialize,
        ShowLoader: showLoader,
        HideLoader: hideLoader
    };
})();