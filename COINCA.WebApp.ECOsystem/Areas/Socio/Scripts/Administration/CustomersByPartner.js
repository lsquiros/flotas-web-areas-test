﻿var ECOsystem = ECOsystem || {};

ECOsystem.CustomersByPartner = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        var select2 = $("#PartnerId").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectChange(data);
                    return fn.apply(this, arguments);
                }
            }
        })(select2.onSelect);

        $('.connected').sortable({
            connectWith: '.connected'
        });

        $('#btnSubmit').off('click.btnSubmit').on('click.btnSubmit', function (e) {
          
            $('#ConnectedCustomers').find('input:hidden').each(function (i) {
               
                $(this).attr('name', 'ConnectedCustomersList[' + i + '].CustomerId');
            });
            return true;
        });        

    };

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
        $('.lightgray').attr('draggable',false);
    };

    var onSelectChange = function (obj) {
        $('#id').val(obj.id);
        $("input:hidden[id='PartnerId']").val(obj.id);

        if (obj.id === '') {
            $('#DragAndDropContainer').html('');
        } else {
            $('#loadForm').submit();
         
        }
    };

    var onSelectCustomer = function (customer) {
        $('body').loader('show');

        $.ajax({
            url: '/Account/SetCustomerRole',
            type: 'POST',
            data: JSON.stringify({ customer: customer }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                window.location = '/Home/IndexCustomer/';
            }
        });
    };

    var onClickModuleOption = function (obj) {
        
        var _value = 0;
        url = $(obj).attr("url");
        var verification = $('#SavePermissionsVerification').val();
        $('#gridContainer tr').each(function (i) {
            if (($(this).css('background-color') === 'rgb(255, 255, 115)')) {
                _value++
            }
        });
        if (verification == '1') {
            _value++;
            $('#confirmationMessage').text('¿Está seguro que desea navegar hacia otra opción del menú y perder los cambios?')
        }
        if (_value > 0) {
            $("#confirmModalSearch").modal('show');
        } else {
            window.location.replace(url);
        }

    };

    var onClickAcceptChangesLost = function () {
        
        window.location.replace(url);
    };

    var onClickWeekly = function () {
        //toggleDisabledDays(id, true);
        $('#divDays').removeClass('hide');
        $('#divDayOfTheMonth').addClass('hide');
    };

    var onClickQuincenal = function () {
        //toggleDisabledDays(id, true);
        $('#divDays').removeClass('hide');
        $('#divDayOfTheMonth').addClass('hide');
    };

    var onClickTrisemanal = function () {
        //toggleDisabledDays(id, true);
        $('#divDays').removeClass('hide');
        $('#divDayOfTheMonth').addClass('hide');
    };

    var onClickMonthly = function () {
        //toggleDisabledDays(id, true);
        $('#divDays').addClass('hide');
        $('#divDayOfTheMonth').removeClass('hide');
    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        OnSelectCustomer: onSelectCustomer,
        OnClickModuleOption: onClickModuleOption,
        OnClickAcceptChangesLost: onClickAcceptChangesLost,
        OnClickWeekly: onClickWeekly,
        OnClickQuincenal: onClickQuincenal,
        OnClickTrisemanal: onClickTrisemanal,
        OnClickMonthly: onClickMonthly
    };
})();

