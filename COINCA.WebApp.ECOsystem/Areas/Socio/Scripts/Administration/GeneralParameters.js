﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.GeneralParameters = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $('#Data_CreditCardNumber').off('blur.Data_CreditCardNumber').on('blur.Data_CreditCardNumber', function (e) {
            ValidateCreditCard($(this).val());
        });

        $('#btnAccept').click(function () {
            validateFields();
        });

        $('#Data_Amount').keypress(function (event) {
            return isNumber(event);
        });

        $('#Data_PreauthorizedDays').keypress(function (event) {
            return isNumber(event);
        });

        $('#btnAddEditNewPrice').click(function () {
            addPricesToCustomer();
        });

        $('.withoutdecimal').on('keypress keyup blur', function (e) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((e.which < 48 || e.which > 57)) {
                e.preventDefault();
            }
        });

        $('.contractText').keypress(function (e) {
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '45') {
                return false;
            }
        });

        $('#Data_CancelCreditCardNumber').keypress(function (e) {
            var val = e.key;
            if (/[^\d]/.test(val)) {
                return false;
            }
        });

        $('#Data_CancelCreditCardEmails').on('blur', function () {
            var val = $(this).val();
            if (!validateInputValues(val)) {
                $(this).focus();
                $('#valmsjcce').show();
                return false;
            }
            $('#valmsjcce').hide();
            return true;
        });
    }

    var addPricesToCustomer = function () {
        var to = $('#txtTo').val();
        var price = $('#txtPrice').val();
        var maxfrom = parseInt($('#maxfrom').val());

        if (to == '' || to == null) {
            $('#warningTo').html('Valor requerido.');
            $('#warningTo').show();
        } else if (price == '' || price == null) {
            $('#warningTo').hide();
            $('#warningPrice').show();
        } else {
            $('#warningTo').hide();
            $('#warningPrice').hide();

            //Validates the margins
            if (to < maxfrom) {
                $('#warningTo').html('El valor no se encuentra en el rango permitido.');
                $('#warningTo').show();
            }
            else {
                savePrices(to, price);
            }
        }
    }

    var savePrices = function (to, price) {
        var model = {
            'Id': $('#PricesId').val(),
            'From': $('#txtFrom').val(),
            'To': to,
            'Price': parseFloat(price)
        };

        $.ajax({
            url: '/GeneralParameters/SavePricesForSMS',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ model: model }),
            success: function (result) {
                if (result.withinRange) {
                    $('#txtTo').focus();
                    $('#warningTo').html('El valor no se encuentra en el rango permitido.');
                    $('#warningTo').show();
                } else {
                    $('#warningTo').hide();

                    $("#pricesGridContainer").html(result.partial);
                    $('#maxfrom').val(result.maxfrom);
                    $('#txtFrom').val(result.maxfrom);
                    $('#txtTo').val('');
                    $('#txtPrice').val('');

                    if (result.showwarning != null) {
                        $('#rangevalidationwarning').show('slow');
                    } else {
                        $('#rangevalidationwarning').hide('slow');
                    }
                    $('#PricesId').val(null);
                }
            }
        });
    }

    var onDelete = function (id) {
        $.ajax({
            url: '/GeneralParameters/DeletePricesForSMS',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ id: id }),
            success: function (result) {
                $("#pricesGridContainer").html(result.partial);
                $('#maxfrom').val(result.maxfrom);
                $('#txtFrom').val(result.maxfrom);

                if (result.showwarning != null) {
                    $('#rangevalidationwarning').show('slow');
                } else {
                    $('#rangevalidationwarning').hide('slow');
                }
            }
        });
    }

    var onEdit = function (id, from, to, price) {
        $('#PricesId').val(id);
        $('#txtFrom').val(from);
        $('#txtTo').val(to);
        $('#txtPrice').val(price);
        $('#maxfrom').val(from);

        $('#txtTo').focus();
    }

    var validateFields = function () {
        var val = $('#Data_CreditCardNumber').val();
        var amount = $('#Data_Amount').val();

        $('#Data_Amount').val(amount.replace(',', '.'));

        if (amount != "" && val == "") {
            $('#valmsj').html('Por favor digite el número de tarjeta.').show('slow');
            return;
        } else if (amount == "" && val != "") {
            $('#valmsj').html('Por favor digite el monto.').show('slow');
            return;
        } else if ($('#rangevalidationwarning').css('display') == 'block') {
            $('#msjresult').html('Por favor validar los rangos para los precios de SMS.');
            $('#ConfirmationModal').modal('show');
            return;
        } else if (val == "" && amount == "") {
            if (validateEmails() && validateCardAlarmEmail() && validateCreditCardEmail() && validatePilotEmails()) {
                $('#addOrEditForm').submit();
                $('#valmsj').hide('slow');
            }
        } else if (val.length < 19 || val.length > 19) {
            $('#valmsj').html('El número de tarjeta no es válido.').show('slow');
            return;
        } else {
            if (validateEmails() && validateCardAlarmEmail() && validateCreditCardEmail() && validatePilotEmails()) {
                $('#addOrEditForm').submit();
                $('#valmsj').hide('slow');
            }
        }
    }

    var validateEmails = function () {
        var val = $('#Data_PreauthorizedEmail').val();
        if (!validateInputValues(val)) {
            $('#Data_PreauthorizedEmail').focus();
            $('#valmsjPE').html('El formato del correo no es el correcto');
            $('#valmsjPE').show();
            return false;
        }
        $('#valmsjPE').hide();
        return true;
    }

    var validateCreditCardEmail = function () {
        var val = $('#Data_CancelCreditCardEmails').val();
        if (!validateInputValues(val)) {
            $('#Data_CancelCreditCardEmails').focus();
            $('#valmsjcce').show();
            return false;
        }
        $('#valmsjcce').hide();
        return true;
    }

    var validateCardAlarmEmail = function () {
        var val = $('#Data_CardAlarmEmail').val();
        if (!validateInputValues(val)) {
            $('#Data_CardAlarmEmail').focus();
            $('#valmsjCAE').html('El formato del correo no es el correcto');
            $('#valmsjCAE').show();
            return false;
        }
        $('#valmsjCAE').hide();
        return true;
    }

    var validatePilotEmails = function () {
        var val = $('#Data_PilotModeNotificationEmail').val();
        if (!validateInputValues(val)) {            
            $('#Data_PilotModeNotificationEmail').focus();
            $('#valpilotemail').show();
            return false;
        }
        $('#valpilotemail').hide();
        return true;
    }

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    var showAffirmationModal = function (data) {
        hideloader();
        if (data.split('|')[0] === "Error") {
            $('#msjresult').html(data.split('.')[0] + '<hr /> Error técnico: ' + data.replace(data.split('.')[0], ''));
        } else {
            $('#Data_APIToken').val(data);
            $('#msjresult').html('Datos almacenados correctamente')
        }
        $('#ConfirmationModal').modal('show');
    };

    var ValidateCreditCard = function (ccnumber) {
        ccnumber = ccnumber.replace(/\-/g, '');

        var finalText = '';
        var c = 1;

        for (var i = 0; i < ccnumber.length; i++) {
            if (c == 4 && i < 13) {
                finalText = finalText + ccnumber.split('')[i] + '-';
                c = 1;
            }
            else {
                finalText = finalText + ccnumber.split('')[i];
                c++;
            }
        }
        $('#Data_CreditCardNumber').val(finalText);
    }

    var isNumber = function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ((charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }

    var validateInputValues = function (emails) {
        if (emails != "" && emails != undefined) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            var emailList = emails.split(';');

            for (i = 0; i < emailList.length; i++) {
                if (!re.test(emailList[i].trim())) {
                    return false;
                }
            }
        }
        return true;
    };

    return {
        Init: initialize,
        ShowLoader: showloader,
        ShowAffirmationModal: showAffirmationModal,
        OnDeletePrices: onDelete,
        OnEditPrices: onEdit
    };
})();