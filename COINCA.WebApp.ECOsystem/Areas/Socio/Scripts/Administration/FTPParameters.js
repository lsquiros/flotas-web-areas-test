﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.FTPParameters = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);        
    }

    var onSaveData = function (data) {
        hideloader();
        if (data == "Success") {
            ECOsystem.Utilities.setConfirmation();
            window.location.reload();
        } else {
            $('#ControlsContainer').html(data);
        }
    }

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    return {
        Init: initialize,
        ShowLoader: showloader,
        OnSaveData: onSaveData
    };
})();