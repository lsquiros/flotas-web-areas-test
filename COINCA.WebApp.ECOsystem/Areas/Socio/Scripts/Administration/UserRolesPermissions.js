﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.UserRolesPermissions = (function () {
    var options = {};
    
    var initialize = function (opts) {
        $.extend(options, opts);

        $('#gridContainer').off('click.edit_row', 'a[edit_row]').on('click.edit_row', 'a[edit_row]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();
        });

        $('#gridContainer').off('click.del', 'a[del_row]').on('click.del', 'a[del_row]', function (e) {
            $('#deleteModal').find('#id').val($(this).attr('id'));
            type = 1;
            setTitle($(this));
        });

        $('#detailContainer').off('click.click', '#btnSaveRoles').on('click.click', '#btnSaveRoles', function () {
            validateRole();
        });

        //$('#detailContainer').off('blur.inputOnBlur', '#txtRoleName').on('blur.inputOnBlur', '#txtRoleName', function () {
        //    var roleName = $('#txtRoleName').val();
        //    $('#NewRoleName').val(roleName);
        //    if (roleName == "" || roleName == null)
        //    {
        //        $('#RoleNameValidation').html('El nombre del rol es requerido');
        //    }
        //    else
        //    {
        //        $.ajax({
        //            url: '/UserRolesPermission/CheckValidRoleName',
        //            type: 'POST',
        //            data: JSON.stringify({ RoleName: roleName, Parent : '' }),
        //            dataType: 'json',
        //            contentType: 'application/json',
        //            success: function (data) {
                        
        //                if (data == 0)
        //                {
        //                    $('#RoleNameValidation').html('El rol ya existe en la base de datos');
        //                    $('#btnSaveRoles').addClass('disabled');
        //                }
        //                else
        //                {
        //                    $('#btnSaveRoles').removeClass('disabled');
        //                }
        //            }
        //        });
        //    }
        //});

        $('#detailContainer').off('focusin.inputOnBlur', '#txtRoleName').on('focusin.inputOnBlur', '#txtRoleName', function () {
            $('#RoleNameValidation').html('');                      
        });
        
        $('#btnSavePermissions').click(function () {
            $('#SavePermissionsVerification').val(1);
            $('#ConfirmationSaveModal').modal('show');
        });

        //Init dropDown 
        $("#RolList").select2('data', {});
        $("#RolList").select2({
            formatResult: roleSelectFormat,
            formatSelection: roleSelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
        
        $("#ModuleList").select2('data', {});
        $("#ModuleList").select2({
            formatResult: moduleSelectFormat,
            formatSelection: moduleSelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
    };

    ///* Select Format*/
    //var roleSelectFormat = function (item) {
    //    if (!item.id) return item.text;
    //    var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
    //    return '<div class="row">' +
    //            '<div class="col-md-6">' + json.Name + '</div>' +
    //            '</div>';
    //};

    var initRoleControls = function () {
        $("#ParentPA").select2('data', {});
        $("#ParentPA").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        $("#PartnerID").select2('data', {});
        $("#PartnerID").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        }).on('change', function () {
            getCustomersData($(this).val());
        });

        $("#CustomerID").select2('data', {});
        $("#CustomerID").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        $("#btnAddCustomer").click(function () {
            var customerID = $("#CustomerID").val();
            var roleId = $("#RoleId").val();
            var partnerId = $("#PartnerID").val();
            $.ajax({
                url: '/UserRolesPermission/AddRoleCustomer',
                type: 'POST',
                data: JSON.stringify({ customerId: customerID, roleId: roleId, partnerId: partnerId }),    
                contentType: 'application/json',
                success: function (data) {                    
                    if (data.split('|')[0] != 'Error') {
                        $('#detailContainer').find('#RolesByCustomer').html(data);
                    }
                }
            });
        });

        $('#detailContainer').find('#RolesByCustomer').off('click.delcusrole_row', 'a[delcusrole_row]').on('click.delcusrole_row', 'a[delcusrole_row]', function (e) {
            $('#deleteCustomerRoleModal').find('#CustomerId').val($(this).attr('id'));
            $('#deleteCustomerRoleModal').find('#PartnerId').val($(this).attr('partnerid'));
        });

        $('#deleteCustomerRoleModal').find('#btnCustRoleDelete').click(function () {
            deleteCustomerRole();
        });        
    }

    var deleteCustomerRole = function () {
        var customerId = $('#deleteCustomerRoleModal').find('#CustomerId').val();
        var partnerId = $('#deleteCustomerRoleModal').find('#PartnerId').val();
        var roleId = $('#RoleId').val();

        $.ajax({
            url: '/UserRolesPermission/DeleteCustomerRole',
            type: 'POST',
            data: JSON.stringify({ customerId: customerId, partnerId: partnerId, roleId: roleId }),
            contentType: 'application/json',
            success: function (data) {
                if (data.split('|')[0] != 'Error') {
                    $('#detailContainer').find('#RolesByCustomer').html(data);
                    $('#deleteCustomerRoleModal').modal('hide');
                }
            }
        });
    }

    var getCustomersData = function (partnerId) {        
        $.ajax({
            url: '/UserRolesPermission/GetCustomersByPartner',
            type: 'POST',
            data: JSON.stringify({ PartnerId : partnerId }),
            dataType: 'json',
            contentType: 'application/json',
            complete: function (data) {
                $("#CustomerID").empty();
                if (data.responseJSON.length > 0) {
                    $("#CustomerID").attr('disabled', false);
                    for (var i = 0; i < data.responseJSON.length; i++) {
                        $("#CustomerID").append($('<option></option>').val(data.responseJSON[i].Value).html(data.responseJSON[i].Text));
                    }
                } else {
                    $("#CustomerID").attr('disabled', true);
                }
            }
        });
    }

    var validateRole = function () {
        var roleName = $('#txtRoleName').val();
        if (roleName == "" || roleName == null) {
            $('#RoleNameValidation').html('El nombre del rol es requerido');
        }
        else {
            var roleId = $('#RoleId').val();

            if (roleId == "" || roleId == null) {
                $.ajax({
                    url: '/UserRolesPermission/CheckValidRoleName',
                    type: 'POST',
                    data: JSON.stringify({ RoleName: roleName, Parent: '' }),
                    dataType: 'json',
                    contentType: 'application/json',
                    complete: function (data) {
                        if (data == 0) {
                            $('#RoleNameValidation').html('El rol ya existe en la base de datos');
                            $('#txtRoleName').focus();
                        }
                        else {
                            $('#addOrEditForm').submit();
                        }
                    }
                });
            } else {
                $('#addOrEditForm').submit();
            }
        }
    }

    var onSuccessLoad = function () {
        $('#addOrEditModal').modal('show');
        $('#btnSaveRoles').addClass('disabled');
    };

    var addModulesModal = function () {
        $('#loadForm').find('#id').val('-1');
        $('#loadForm').submit();
    };

    var onRolesChangeMenu = function () {
        $('#ModuleList').attr('disabled', true);
        var vRole = $("#RolList").val();
        $('#treeViewContainer').addClass('hide');

        $('#ModuleList').empty();
        $('#ModuleList').append('<option value>Seleccione un Módulo...</option>');

        if ($("#RolList option:selected").text() == "Seleccione un rol...") { return; }

        $.ajax({
            url: '/UserRolesPermission/RetrieveModulesPermissions',
            type: 'POST',
            data: JSON.stringify({ roleId: vRole }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    $('#ModuleList').append('<option value="' + data[i].ModuleId + '">'
                            + data[i].Name +
                            '</option>');
                }
                $('#ModuleList').attr('disabled', false);
            }
        });
    }

    var onRolesAddMenus = function () {
        $('#RolList').empty();
        $('#RolList').append('<option value>Seleccione un Rol...</option>');
        $.ajax({
            url: '/UserRolesPermission/RetrieveRolesPermissions',
            type: 'POST',
            data: JSON.stringify({ x: 1 }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {

                for (var i = 0; i < data.length; i++) {
                    $('#RolList').append('<option value="' + data[i].RoleId + '">'
                            + data[i].Name +
                            '</option>');
                }
            }
        });

        $('#addOrEditModal').modal('hide');
    }
    
    var onRolesCancel = function () {        
        $('#detailContainer').hide('slow');
        //$.ajax({
        //    url: '/UserRolesPermission/CancelSaveAsPermissions',
        //    type: 'POST',
        //    data: JSON.stringify({ x: 1 }),
        //    dataType: 'json',
        //    contentType: 'application/jon',
        //    success: function (data) {
        //        $('#addOrEditModal').modal('hide');
        //    }
        //});        
    }

    var onConfirmationModalOn = function () {
        $('#ConfirmationModalForm').submit();
    }

    var onConfirmationModalShow = function () {
        $('#ConfirmationModal').modal('show');
    }

    /* Select Format*/
    var roleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-6">' + json.Name + '</div>' +
                '</div>';
    };

    /* Select Format*/
    var moduleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-6">' + json.Name + '</div>' +
                '</div>';
    };

	var addOrEditStart = function () {
        $('#NewRoleId').val($('#RoleId').val());        
    };

	var addOrEditModalHide = function () {
		$('#ConfirmationModal').modal('show');
        $('#addOrEditModal').modal('hide');
        var newrole = $('#NewRoleId').val();
        if(newrole == '')
        {
            $('#AssingPermissionsModal').modal('show');
		}
		$('#detailContainer').hide('slow');
    };

    var redirectToPermissions = function () {
        var newRoleName = $('#NewRoleName').val();
        var url = '/Socio/UserRolesPermission/Index?newRoleName=' + newRoleName; 
        window.location.href = url;
    };

    var showRoleDetail = function () {
        $('#detailContainer').show('slow');
        initRoleControls();
    }

    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        AddModulesModal: addModulesModal,
        OnRolesChangeMenu: onRolesChangeMenu,
        OnRolesAddMenus: onRolesAddMenus,
        OnRolesCancel: onRolesCancel,
        //OnConfirmationModalOn: onConfirmationModalOn,
        OnConfirmationModalShow: onConfirmationModalShow,
	    AddOrEditStart: addOrEditStart,
        AddOrEditModalHide: addOrEditModalHide,
        RedirectToPermissions: redirectToPermissions,
        ShowRoleDetail: showRoleDetail
    };
})();