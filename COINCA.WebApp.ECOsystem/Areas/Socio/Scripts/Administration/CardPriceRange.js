﻿const addRange = () => {
    let quantity = $('#cardQuantity').val()
    let price = $('#cardPrice').val()

    if (quantity > 0 && price > 0) {

        $.post(urls.SaveRanges, {
            CustomerId: $('#CustomerId').val(),
            Quantity: quantity,
            Price: price,
            IsDeleted: 0
        }, function (data) {
            $('#cardQuantity').val('')
            $('#cardPrice').val('')
            LoadRanges()
        })
    }
    else {
        swal("Mensaje del Sistema", "No puede dejar espacios en blanco ni valores en cero.", "warning")
    }
}

function LoadRanges() {
    $.post(urls.LoadRanges, {
        CustomerId: $('#CustomerId').val()
    }, function (data) {
        if (data.length > 0) {
            let html = ""
            for (var i = 0; i < data.length; i++) {
                html += `<tr>
                                <td>>${data[i].Quantity}</td>
                                <td>$${data[i].Price}</td>
                                <td style="cursor:pointer" title="Eliminar" class="text-center" onclick="DeleteRange(${data[i].RangeId})">
                                    <i class="mdi mdi-delete"></i>
                                </td>
                            </tr>`
            }
            $('#rangeTable').html(html)
            $('#tableDiv').prop('hidden', false)
        }
        else {
            $('#rangeTable').html('')
            $('#tableDiv').prop('hidden', true)
        }
    })
}

setTimeout(() => {
    LoadRanges()
}, 1000);

const DeleteRange = RangeId => {

    swal({
        title: "Mensaje del Sistema",
        text: "¿Desea eliminar este registro?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.post(urls.SaveRanges, {
                    RangeId: RangeId
                }, function (data) {
                    LoadRanges()
                })
            }
        });
}