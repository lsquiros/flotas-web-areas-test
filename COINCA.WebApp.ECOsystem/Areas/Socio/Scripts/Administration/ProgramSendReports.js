﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.ProgramSendReports = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);
        initToggle();

        $('.datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $(".txtStartDate").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $('.datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $(".txtEndDate").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $(".rdbWeekly").click(function () {
            var id = $(this).attr("reportId");
            toggleDisabledDays(id, false);
            $('#divDays-' + id).removeClass('hide');
            $('#divDayOfTheMonth-' + id).addClass('hide');            
        });

        $(".rdbMonthly").click(function () {
            var id = $(this).attr("reportId");
            toggleDisabledDays(id, true);
            $('#divDays-' + id).addClass('hide');
            $('#divDayOfTheMonth-' + id).removeClass('hide');
        });

        $(".rdbDaily").click(function () {
            var id = $(this).attr("reportId");
            toggleDisabledDays(id, true);
            $('#divDays-' + id).addClass('hide');
            $('#divDayOfTheMonth-' + id).addClass('hide');
        });

        $('.dbVehicles').select2('data', {});
        $('.dbVehicles').select2({
            formatResult: vehicleSelectFormat,
            formatSelection: vehicleSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('.dbCostCenter').select2('data', {});
        $('.dbCostCenter').select2({
            formatResult: CostCenterSelectFormat,
            formatSelection: CostCenterSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('dbVehicleUnit').select2('data', {});
        $('dbVehicleUnit').select2({
            formatResult: UnitIdSelectFormat,
            formatSelection: UnitIdSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('.dbDriver').select2('data', {});
        $('.dbDriver').select2({
            formatResult: DriverSelectFormat,
            formatSelection: DriverSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('.dbVehicleGroup').select2('data', {});
        $('.dbVehicleGroup').select2({            
            escapeMarkup: function (m) { return m; }
        });
           
        $('.dbCustomers').select2('data', {});
        $('.dbCustomers').select2({
            escapeMarkup: function (m) { return m; }
        });

        $("#addCostCenter").click(function () {
            $('#ValidationMessage').html('');
            $('[data-toggle="tooltip"]').tooltip('disable');

            var name = $('#CostCenterId option:selected').text().split(':')[1].replace(/'/g, '').replace('}', '');
            var emails = $('#txtEmailsCostCenter').val();
            var reportid = $("#addCostCenter").attr('reportidattr');

            if (name == '' || emails == '' || !validateInputValues(emails)) {
                $('#ValidationMessage').html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                          'El formato de los correos no es el correcto ó no se ha seleccionado un Centro de Costo</div>');
                return;
            }

            addCostCenterToTable($('#CostCenterId').val(), name, emails, reportid);
            $('#txtEmailsCostCenter').val('')
            $('#CostCenterId').val('').change();
        });
        
        $('.DayOfTheMonth').keypress(function (e) {
            //Only numbers
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;

            //Not higher than 31
            if (parseInt(this.value + "" + String.fromCharCode(e.charCode)) > 31 || parseInt(this.value + "" + String.fromCharCode(e.charCode)) < 1) return false;
        });

        $('.btnSendReportNow').click(function () {
            var reportId = $(this).attr('reportid');
            $.ajax({
                url: 'ProgramSendReports/SendTestEmail',
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify({ ReportId: reportId }),
                contentType: 'application/json',
                success: function (data) {
                    if (data == 'Success') ECOsystem.Utilities.setConfirmation();
                }
            });
        });
    };

    var getData = function (obj) {
        var id = obj.id;

        $.ajax({
            url: 'ProgramSendReports/GetData',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({ Id: id }),
            contentType: 'application/json',
            success: function (data) {
                showData(data);
            }
        });
    };

    var toggleDisabledDays = function (id, change) {
        $(".Days_" + id).each(function () {
            $(this).attr("disabled", change);
            if (change) $(this).prop("checked", !change);
        });
    };

    var vehicleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-2">' + json.PlateId + '</div>' +
                '<div class="col-md-4">' + json.Name + '</div>' +
                '</div>';
    };

    var CostCenterSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
               '</div>';
    };

    var UnitIdSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
                '</div>';
    };

    var DriverSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
               '</div>';
    };

    var initToggle = function () {
        $('.toggleCheck').bootstrapToggle({
            on: 'Enviar',
            off: 'No Enviar'
        });
    };

    var saveChanges = function (obj) {
        $('#confirmModal').modal('hide');
        
        if (validateAllEmails(obj.getAttribute("reportId"))) {
            showLoader();
            $('#ControlsContainerPanicBotton').html('');

            var list = obtainData(obj);

            for (var i = 0; i < list.length; i++) {
                var costCenterList = obtainCostCenterEmailsData(list[i].ReportId);

                if (list[i].Elapse == 0) {
                    $('#ValidationMessage_' + list[i].ReportId).html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Por favor seleccione la periodicidad con la que desea recibir el reporte programado.</div>');
                    hideLoader();
                    return;
                }

                if ((list[i].Elapse == 7 || list[i].Elapse == 30) && list[i].Days == "") {
                    $('#ValidationMessage_' + list[i].ReportId).html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Por favor ingrese el o los días que desea recibir el reporte programado.</div>');
                    hideLoader();
                    return;
                }

                if (costCenterList.length == 0 && (list[i].Emails == "" || list[i].Emails == undefined)) {
                    $('#ValidationMessage_' + list[i].ReportId).html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Por favor ingrese un correo.</div>');
                    hideLoader();
                    return;
                }
                list[i].CostCenterList = costCenterList;
            }
            
            $.ajax({
                url: 'ProgramSendReports/SendProgramReportAddOrEdit',
                type: 'POST',
                data: { List: list },
                dataType: 'html',
                success: function (data) {
                    hideLoader();
                    $("#ReportParameters-" + ReportId).addClass("hidden");
                    $('#GridList-' + ReportId).html(data);

                }
            });
        } else {
            $('#ControlsContainer_' + obj.getAttribute("reportId")).html('<br />' +
                                         '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                         'Por favor verifique que los correos cuentan con el formato correcto. </div>');
            hideLoader();
        }
        
    };
    
    var obtainData = function (obj) {

        var list = [];
        var data = {}

        ReportId = obj.getAttribute("reportId");
        data.ReportId = ReportId;
        data.Id = $("#ProgramReportId_" + ReportId).val();
        var check = $("#ReportActive_" + ReportId);

        if (check.prop("checked")) data.Active = 'True'; else data.Active = 'False';

        data.Emails = $("#txtEmails_" + ReportId).val();
        data.StartDate = parseDate($("#StartDate_" + ReportId).val());
        data.EndDate = parseDate($("#EndDate_" + ReportId).val());
        data.VehicleId = $("#VehicleId_" + ReportId).val();
        data.Days = '';
        data.DriverId = $("#DriverId_" + ReportId).val();

        if ($("#Monthly-2-" + ReportId).prop("checked")) {
            data.Elapse = 30;
            data.Days = $('#DayOfTheMonth_' + ReportId).val();
        } else if ($("#weekly-2-" + ReportId).prop("checked")) {
            data.Elapse = 7;
            if ($("#Sunday_" + ReportId).prop("checked")) data.Days = data.Days + '1';
            if ($("#Monday_" + ReportId).prop("checked")) data.Days = data.Days + ',2';
            if ($("#Tuesday_" + ReportId).prop("checked")) data.Days = data.Days + ',3';
            if ($("#Wednesday_" + ReportId).prop("checked")) data.Days = data.Days + ',4';
            if ($("#Thursday_" + ReportId).prop("checked")) data.Days = data.Days + ',5';
            if ($("#Friday_" + ReportId).prop("checked")) data.Days = data.Days + ',6';
            if ($("#Saturday_" + ReportId).prop("checked")) data.Days = data.Days + ',7';
        } else if ($("#Daily-2-" + ReportId).prop("checked")) {
            data.Elapse = 1;
        } else {
            data.Elapse = 0;
        }

        list.push(data);
        return list;
    };

    var obtainCostCenterEmailsData = function (reportId) {
        
        var list = [];
        $('#listCostCenters_' + reportId + ' tr').each(function () {
            var val = $(this).closest('tr').text();
            if (val.split('|')[1] != undefined) {
                var arr = {};

                arr.CostCenterId = val.split('|')[0];
                arr.CostCenterEmails = val.split('|')[2];
                arr.Id = val.split('|')[3];
                list.push(arr);
            }
        });
        return list;
    }

    var validateAllEmails = function (id) {
        var val = true;
        var check = $(this);
        var emails = $("#txtEmails_" + id).val();

        if (emails == undefined) return val;

        if (validateInputValues(emails) && emails != "") {
            if (!val) return val;
        } else {
            val = false;
        }
        return val;
    };

    var parseDate = function (datestr) {
        if (datestr == undefined) return null;

        var newDate = datestr.split("/");
        return new Date(newDate[1] + "/" + newDate[0] + "/" + newDate[2]);
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');      
    };

    var discardChanges = function (obj) {
        $("#ReportParameters-" + obj.getAttribute("reportid")).addClass("hidden");
    };

    var showData = function (data) {
        $('#ControlsContainer_' + data[0].ReportId).empty();
        for (var i = 0; i < data.length; i++) {
            var reportid = data[i].ReportId;
            var id = data[i].Id;

            $("#txtEmails_" + reportid).val("");
            $("#StartDate_" + reportid).val("");
            $("#EndDate_" + reportid).val("");
            $("#VehicleId_" + reportid).val("");
            $("#ProgramReportId_" + reportid).val("");

            $("#Monthly-2-" + reportid).prop("checked", false);
            $('#DayOfTheMonth_' + reportid).val("");
            $("#weekly-2-" + reportid).prop("checked", false);
            $("#Sunday_" + reportid).prop("checked", false);
            $("#Monday_" + reportid).prop("checked", false);
            $("#Tuesday_" + reportid).prop("checked", false);
            $("#Wednesday_" + reportid).prop("checked", false);
            $("#Thursday_" + reportid).prop("checked", false);
            $("#Friday_" + reportid).prop("checked", false);
            $("#Saturday_" + reportid).prop("checked", false);
            $("#Daily-2-" + reportid).prop("checked", false);

            $('#listCostCenters_' + reportid + ' tbody').empty();

            $("#ReportParameters-" + reportid).removeClass("hidden");

            $("#txtEmails_" + reportid).val(data[i].Emails);
            $("#StartDate_" + reportid).val(data[i].StartDateStr);
            $("#EndDate_" + reportid).val(data[i].EndDateStr);
            $("#VehicleId_" + reportid).val(data[i].VehicleId);
            $("#ProgramReportId_" + reportid).val(data[i].Id);

            if (data[i].CostCenterList != null || data[i].CostCenterList != undefined) {
                for (var x = 0; x < data[i].CostCenterList.length; x++) {
                    addCostCenterToTable(data[i].CostCenterList[x].Id, data[i].CostCenterList[x].CostCenterId, data[i].CostCenterList[x].CostCenterName, data[i].CostCenterList[x].CostCenterEmails, reportid);
                }
            }

            if (data[i].Elapse == 30) {
                $("#Monthly-2-" + reportid).prop("checked", true);
                $('#DayOfTheMonth_' + reportid).val(data[i].Days);
                $('#divDays-' + reportid).addClass('hide');
                $('#divDayOfTheMonth-' + reportid).removeClass('hide');
            } else if (data[i].Elapse == 7) {
                $("#weekly-2-" + reportid).prop("checked", true);
                toggleDisabledDays(id, false);
                for (var y = 0; y < data[i].Days.split(",").length; y++) {
                    switch (data[i].Days.split(",")[y]) {
                        case '1':
                            $("#Sunday_" + reportid).prop("checked", true);
                            break;
                        case '2':
                            $("#Monday_" + reportid).prop("checked", true);
                            break;
                        case '3':
                            $("#Tuesday_" + reportid).prop("checked", true);
                            break;
                        case '4':
                            $("#Wednesday_" + reportid).prop("checked", true);
                            break;
                        case '5':
                            $("#Thursday_" + reportid).prop("checked", true);
                            break;
                        case '6':
                            $("#Friday_" + reportid).prop("checked", true);
                            break;
                        case '7':
                            $("#Saturday_" + reportid).prop("checked", true);
                            break;
                    }
                }
                $('#divDays-' + reportid).removeClass('hide');
                $('#divDayOfTheMonth-' + reportid).addClass('hide');
            } else if (data[i].Elapse == 1) {
                $("#Daily-2-" + reportid).prop("checked", true);
                $('#divDays-' + reportid).addClass('hide');
                $('#divDayOfTheMonth-' + reportid).addClass('hide');
            }
        }
    };

    var showParameters = function (obj) {

        ReportId = obj.getAttribute("ReportId");

        $("#txtEmails_" + ReportId).val("");
        $("#StartDate_" + ReportId).val("");
        $("#EndDate_" + ReportId).val("");
        $("#VehicleId_" + ReportId).val("");
        $("#ProgramReportId_" + ReportId).val("");

        $("#Monthly-2-" + ReportId).prop("checked", false);
        $('#DayOfTheMonth_' + ReportId).val("");
        $("#weekly-2-" + ReportId).prop("checked", false);
        $("#Sunday_" + ReportId).prop("checked", false);
        $("#Monday_" + ReportId).prop("checked", false);
        $("#Tuesday_" + ReportId).prop("checked", false);
        $("#Wednesday_" + ReportId).prop("checked", false);
        $("#Thursday_" + ReportId).prop("checked", false);
        $("#Friday_" + ReportId).prop("checked", false);
        $("#Saturday_" + ReportId).prop("checked", false);
        $("#Daily-2-" + ReportId).prop("checked", false);

        $('#listCostCenters_' + ReportId + ' tbody').empty();

        $("#ReportParameters-" + ReportId).removeClass("hidden");

        $('#ControlsContainer_' + ReportId).empty();
    };

    var validateInputValues = function (emails) {
        if (emails != "") {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            var emailList = emails.split(';');

            for (i = 0; i < emailList.length; i++) {
                if (!re.test(emailList[i].trim())) {
                    return false;
                }
            }
        }        
        return true;
    };


    var addCostCenterToTable = function (id, costcenterid, costcentername, costcenteremails, reportid) {
        
        if (id == 0) {
            ContId = ContId + 1;
        }
        else {
            ContId = id;
        }

        $('#listCostCenters_' + reportid + ' tbody').append('<tr>' +
            '<th class="hide">' + costcenterid + '|</th>' +
            '<th>' + costcentername + '</th>' +
            '<th class="hide">|</th>' +
            '<th>' + costcenteremails + '</th>' +
            '<th class="hide">|</th>' +
            '<th class="hide">' + ContId + '</th> ' +
            '<th  style="text-align:right;"><a href="#" id="RowId_' + ContId + '" class="delete-color" title="Eliminar" onclick="ECO_Partner.ProgramSendReports.ShowConfirmationModal(this)"><i class="mdi mdi-32px mdi-close-box"></i></a></th>' +
            '</tr>');
    }    

    var showConfirmationModal = function (obj) {
        
        $('#ReportId').val(obj.getAttribute("reportid"));
        $('#deleteModal').modal('show');
        id = obj.id;

        if (id.includes("RowId")) {
            $('#IsEmailRow').val(true);
            $('#RowId').val(id);
        }
        else {
            $('#IsEmailRow').val(false);
            $('#RowId').val(obj.getAttribute("id"));
        }
    }

    var deleteRow = function () {
        
        var Validation = $('#IsEmailRow').val();
        var ReportId = $('#ReportId').val();
        var id = $('#RowId').val();
        $.ajax({
            url: 'ProgramSendReports/DeleteProgramReport',
            type: 'POST',
            dataType: 'html',
            data: {
                Validation,
                Id: id,
                ReportId: ReportId
            },
            success: function (data) {
                if (Validation == "true") {
                    $('#' + id).closest('tr').remove();
                }
                else {
                    $('#GridList-' + ReportId).html(data);
                }
            }
        });
        $('#deleteModal').modal('hide');
    }

    return {
        Init: initialize,
        GetData: getData,
        DeleteRow: deleteRow,
        ShowParameters: showParameters,
        SaveChanges: saveChanges,
        ShowConfirmationModal: showConfirmationModal,
        DiscardChanges: discardChanges
    };
})();

