﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.TransactionsOffline = (function () {
    var options = {};
    var jsonDataArray = [];

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initToggle();
        toggleChange();

        $("body").tooltip({ selector: '[data-toggle=tooltip]' });

        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        /*onClickGenerateCardFile*/
        $("body").on("click", "#btnGuardar", function (e) {
            
            var active = $('#checkTransactionsOffline').prop('checked');
            if (active == true) {
                if (validateInputValues()) {
                    setAmountErrorMsj('', false, 'e2');
                    $('#ConfirmationEditModal').modal('show');
                }
                else {
                    setAmountErrorMsj('* Debe indicar los correos a los cuales desea notificar en un formato correcto, seleccionar al menos un cliente y el lapso de tiempo no puede ser 0.', true, 'e2');
                }

            }
            else {
                setAmountErrorMsj('', false, 'e2');
                $('#ConfirmationEditModal').modal('show');
            }
         });


        //Validate the texboxes to receive only numbers and that the max number allowed
        $('.number-only').keypress(function (e) {
            //Get the number in the text
            var hours = $("#TimeSlotValueHours").val();
            var minutes = $("#TimeSlotValueMinutes").val();
            
            if (hours.length == 2) hours = '';
            if (minutes.length == 2) minutes = '';
            
            if ($(this).attr('id') == 'TimeSlotValueHours') hours = hours + String.fromCharCode(e.charCode);
            if ($(this).attr('id') == 'TimeSlotValueMinutes') minutes = minutes + String.fromCharCode(e.charCode);
            
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) {
                return false;
            }
            else {
                if (hours > 99) {
                    return false;
                }
                else if (minutes >= 60) {
                    return false;
                }            
            }
        });       

        //Set the max length to the textboxes 
        $("#TimeSlotValueHours").attr('maxlength', '2');
        $("#TimeSlotValueMinutes").attr('maxlength', '2');
                
        //Set the values as 2 digits when the inputs lost the focus and it has just one number on it
        $('#TimeSlotValue').off('blur.HoursInputOnBlur', '#TimeSlotValueHours').on('blur.HoursInputOnBlur', '#TimeSlotValueHours', function (e) {
            var obj = $('#TimeSlotValueHours');
            var val = $('#TimeSlotValueHours').val();
            if (val.length == 1) obj.val('0' + val); //if the value in the textbox is just one number, adds 0 at the left
            if (val == '') obj.val('00'); //if the value is nothing, add 00 so we avoid null numbers
        });

        $('#TimeSlotValue').off('blur.MinutesInputOnBlur', '#TimeSlotValueMinutes').on('blur.MinutesInputOnBlur', '#TimeSlotValueMinutes', function (e) {
            var obj = $('#TimeSlotValueMinutes');
            var val = $('#TimeSlotValueMinutes').val();
            if (val.length == 1) obj.val('0' + val); //if the value in the textbox is just one number, adds 0 at the left
            if (val == '') obj.val('00'); //if the value is nothing, add 00 so we avoid null numbers
        });        
                
        showControls($('#checkTransactionsOffline').prop('checked'));

        $('#confirmChanges').click(function () {
            transactionsOfflineSave();
        });

        $('#confirmSendChanges').click(function () {
            sendTransactionsOffline();
        });        

        $('#GridContainer').off('click.chkCheckAllCustomers', '#chkCheckAllCustomers').on('click.chkCheckAllCustomers', '#chkCheckAllCustomers', function () {
            var check = $(this);
            $('#GridContainer').find('input[type=checkbox]').each(function () {
                if (check.is(':checked')) {
                    $(this).prop('checked', true);
                } else {
                    $(this).prop('checked', false);
                }
            });
        });
    };

    var initToggle = function () {
        $('.toggleCheck').bootstrapToggle({
            on: 'Activo',
            off: 'Inactivo'
        });
    };

    var toggleChange = function () {
        $('#checkTransactionsOffline').change(function () {            
            var check = $(this);
            showControls(check.prop('checked'));
        });
    };

    var showControls = function(check) {
        if (check) {
            $('.TimeSlotValue').removeClass('hideText');
            $('#GridContainer').removeClass('hide');
            $('#btnSendTransactions').addClass('hide');
            $('#EmailsId').removeClass('hide');
        }
        else {
            $('.TimeSlotValue').addClass('hideText');
            $('#GridContainer').addClass('hide');
            $('#btnSendTransactions').removeClass('hide');
            $('#EmailsId').addClass('hide');
        }
    }

    var validateInputValues = function () {
        var customers = getCustomersChecked();
        var emails = $('#Data_Emails').val();
        var hours = $("#TimeSlotValueHours").val() == '' ? 0 : $("#TimeSlotValueHours").val();
        var minutes = $("#TimeSlotValueMinutes").val() == '' ? 0 : $("#TimeSlotValueMinutes").val();

        if (customers.length == 0 || !validateEmails(emails) || (hours == 0 && minutes == 0)) {
            return false;
        } else {
            return true;
        }
       
    };

    var validateEmails = function (emails) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i; 
        var emailList = emails.split(';');

        for (i = 0; i < emailList.length; i++) {
            if (!re.test(emailList[i].trim())) {
                return false;
            }
        }
        return true;
    }

    var transactionsOfflineSave = function () {
        $('#ConfirmationEditModal').modal('hide');
        $('body').loader('show');
        var hours = $("#TimeSlotValueHours").val() == '' ? 0 : $("#TimeSlotValueHours").val();
        var minutes = $("#TimeSlotValueMinutes").val() == '' ? 0 : $("#TimeSlotValueMinutes").val();
        var active = $('#checkTransactionsOffline').prop('checked');
        var customers = getCustomersChecked();
        var idTransactionOffline = $('#checkTransactionsOffline').attr('idTrasactionOffline') == undefined ? null : $('#checkTransactionsOffline').attr('idTrasactionOffline');
        var emails = $('#Data_Emails').val();
        $.ajax({
            url: '/TransactionsOffline/AddOrEditOfflineTransactions',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ hours: hours, minutes: minutes, active: active, idTransactionOffline: idTransactionOffline, customers: customers, emails: emails }),
            success: function (result) {
                if (result == 'Success') {
                    window.location.reload();
                }
                else {
                    $('#ControlsContainer').show('slow');
                    $('#ControlsContainer').html('<br /><div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                                 'Ocurrió un error al procesar la información, por favor intentelo de nuevo. <hr />' +
                                                 '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                }
            }
        });
    };
    
    var getCustomersChecked = function () {
        var arr = [];
        $('#GridContainer input[type=checkbox]').each(function (i) {
            if ($(this).is(':checked')) {
                if ($(this).attr("id") != 'chkCheckAllCustomers') {
                    arr.push($(this).attr("id"));
                }
            }
        });
        return arr;
    };

    var sendTransactionsOffline = function () {
        $('#ConfirmationSendModal').modal('hide');
        $('body').loader('show');

        $.ajax({
            url: '/TransactionsOffline/SendTransactionsOffline',
            type: "POST",
            contentType: "application/json",            
            success: function (result) {
                $('body').loader('hide');
                if (result == 'Success') {
                    $('#ControlsContainer').html('<br /><div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                                 'Cambios almacenados correctamente </div>');
                    $('#ControlsContainer').show('slow');
                    setTimeout(function () { $("#ControlsContainer").hide('slow'); }, 2000);                    
                }
                else {
                    $('#ControlsContainer').html('<br /><div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                                 'Ocurrió un error al procesar la información, por favor intentelo de nuevo. <hr />' +
                                                 '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                    $('#ControlsContainer').show('slow');
                }
            }
        });
    };

    /* Show or hide errors messages*/
    var setAmountErrorMsj = function (msj, show, typeError) {

        typeError = typeError != null ? typeError : 'e1';

        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').removeClass('hide');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').addClass('hide');
                }
                break;
            default:
                break;
        }


    }

    return {
        Init: initialize
    };
})();

