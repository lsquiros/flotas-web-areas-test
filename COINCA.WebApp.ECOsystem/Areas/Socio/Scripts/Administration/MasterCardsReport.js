﻿var ECO_Partner = ECO_Partner || {};

ECO_Partner.MasterCardsReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        disabledControls();
        initCharts();
        try {
            initializeDropDownList();
        }
        catch (exception) {
        }
    }

    /*init Events*/
    var initEvents = function () {
        $('#page-content-wrapper').off('click.print', 'a[data-print-link]').on('click.print', 'a[data-print-link]', function (e) {
            ECOsystem.Utilities.ShowPopup(options.printURL, 'Formato de impresión');
        });
        $('#page-content-wrapper').off('click.goToPrint', 'button[data-print]').on('click.goToPrint', 'button[data-print]', function (e) {
            window.print();
        });

        if (!options.printMode) {
            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });
            $("#Parameters_StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });


            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });

            $("#Parameters_EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });
        }
    }

    /*disabled Controls when printMode = true*/
    var disabledControls = function () {
        if (options.printMode) {
            $('[data-filter]').attr('disabled', 'disabled');
        }
    }

    /*Init Charts*/
    var initCharts = function () { }
    
    /*format Label*/
    var formatLabel = function (v) {
        return ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToFloat(v)) + "%";
    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#Parameters_ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        var select4 = $("#Parameters_CountryId").select2().data('select2');
        select4.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectCountryChange(data); return fn.apply(this, arguments); }
            }
        })(select4.onSelect);

        var select5 = $("#Parameters_CustomerId").select2().data('select2');
        select5.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectUserChange(data); return fn.apply(this, arguments); }
            }
        })(select5.onSelect);

    };
    
    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Parameters_Month").select2('val', '');
            $("#Parameters_Year").select2('val', '');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');
            $('#Parameters_CountryId').select2('val', '');
            $('#Parameters_CustomerId').select2('val', '');

            $('#Parameters_ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Month').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Year').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select Country Change*/
    var onSelectCountryChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_CountryId').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Select User Change*/
    var onSelectUserChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_CustomerId').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                '<span aria-hidden="true">×</span>' +
                                            '</button>' +
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        if (($('#Parameters_ReportCriteriaId').val() === '')
                || ($('#Parameters_Month').val() === '')
                || ($('#Parameters_Year').val() === '')
                || ($('#Parameters_StartDateStr').val() === '')
                || ($('#Parameters_EndDateStr').val() === '')
                || ($('#Parameters_CountryId').val() === '')
                || ($('#Parameters_CustomerId').val() === '')
            ) {
            if (($('#Parameters_Month').val() !== '') && ($('#Parameters_Year').val() !== ''))
                return true;
            if (($('#Parameters_StartDateStr').val() !== '') && ($('#Parameters_EndDateStr').val() !== ''))
                return true;
            invalidSelectOption();
            return false;
        }
        return true;
    };

    /* Public methods */
    return {
        Init: initialize,
        FormatLabel: formatLabel
    };
})();

