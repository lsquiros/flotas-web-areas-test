﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace ECOsystem.Eco_Maps_Service {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="RoutingSoap", Namespace="http://tempuri.org/")]
    public partial class Routing : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback RoutingByStreetOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetLocationsByLatLonOperationCompleted;
        
        private System.Threading.SendOrPostCallback SaveRoutesFromImportDataOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetProvinciasFromMapsOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetCantonesFromMapsOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public Routing() {
            this.Url = global::ECOsystem.Properties.Settings.Default.ecosystem_Eco_Maps_Service_Routing;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event RoutingByStreetCompletedEventHandler RoutingByStreetCompleted;
        
        /// <remarks/>
        public event GetLocationsByLatLonCompletedEventHandler GetLocationsByLatLonCompleted;
        
        /// <remarks/>
        public event SaveRoutesFromImportDataCompletedEventHandler SaveRoutesFromImportDataCompleted;
        
        /// <remarks/>
        public event GetProvinciasFromMapsCompletedEventHandler GetProvinciasFromMapsCompleted;
        
        /// <remarks/>
        public event GetCantonesFromMapsCompletedEventHandler GetCantonesFromMapsCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/RoutingByStreet", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public double RoutingByStreet(string InitialCountry, string EndCountry, decimal InitialLatitudePoint, decimal InitialLongitudePoint, decimal EndLatitudePoint, decimal EndLongitudePoint) {
            object[] results = this.Invoke("RoutingByStreet", new object[] {
                        InitialCountry,
                        EndCountry,
                        InitialLatitudePoint,
                        InitialLongitudePoint,
                        EndLatitudePoint,
                        EndLongitudePoint});
            return ((double)(results[0]));
        }
        
        /// <remarks/>
        public void RoutingByStreetAsync(string InitialCountry, string EndCountry, decimal InitialLatitudePoint, decimal InitialLongitudePoint, decimal EndLatitudePoint, decimal EndLongitudePoint) {
            this.RoutingByStreetAsync(InitialCountry, EndCountry, InitialLatitudePoint, InitialLongitudePoint, EndLatitudePoint, EndLongitudePoint, null);
        }
        
        /// <remarks/>
        public void RoutingByStreetAsync(string InitialCountry, string EndCountry, decimal InitialLatitudePoint, decimal InitialLongitudePoint, decimal EndLatitudePoint, decimal EndLongitudePoint, object userState) {
            if ((this.RoutingByStreetOperationCompleted == null)) {
                this.RoutingByStreetOperationCompleted = new System.Threading.SendOrPostCallback(this.OnRoutingByStreetOperationCompleted);
            }
            this.InvokeAsync("RoutingByStreet", new object[] {
                        InitialCountry,
                        EndCountry,
                        InitialLatitudePoint,
                        InitialLongitudePoint,
                        EndLatitudePoint,
                        EndLongitudePoint}, this.RoutingByStreetOperationCompleted, userState);
        }
        
        private void OnRoutingByStreetOperationCompleted(object arg) {
            if ((this.RoutingByStreetCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.RoutingByStreetCompleted(this, new RoutingByStreetCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetLocationsByLatLon", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string[] GetLocationsByLatLon(string[] LocationList, string CountryCode) {
            object[] results = this.Invoke("GetLocationsByLatLon", new object[] {
                        LocationList,
                        CountryCode});
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void GetLocationsByLatLonAsync(string[] LocationList, string CountryCode) {
            this.GetLocationsByLatLonAsync(LocationList, CountryCode, null);
        }
        
        /// <remarks/>
        public void GetLocationsByLatLonAsync(string[] LocationList, string CountryCode, object userState) {
            if ((this.GetLocationsByLatLonOperationCompleted == null)) {
                this.GetLocationsByLatLonOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetLocationsByLatLonOperationCompleted);
            }
            this.InvokeAsync("GetLocationsByLatLon", new object[] {
                        LocationList,
                        CountryCode}, this.GetLocationsByLatLonOperationCompleted, userState);
        }
        
        private void OnGetLocationsByLatLonOperationCompleted(object arg) {
            if ((this.GetLocationsByLatLonCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetLocationsByLatLonCompleted(this, new GetLocationsByLatLonCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/SaveRoutesFromImportData", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool SaveRoutesFromImportData(string PointList, string CountryCode, int routeId, int UserId) {
            object[] results = this.Invoke("SaveRoutesFromImportData", new object[] {
                        PointList,
                        CountryCode,
                        routeId,
                        UserId});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void SaveRoutesFromImportDataAsync(string PointList, string CountryCode, int routeId, int UserId) {
            this.SaveRoutesFromImportDataAsync(PointList, CountryCode, routeId, UserId, null);
        }
        
        /// <remarks/>
        public void SaveRoutesFromImportDataAsync(string PointList, string CountryCode, int routeId, int UserId, object userState) {
            if ((this.SaveRoutesFromImportDataOperationCompleted == null)) {
                this.SaveRoutesFromImportDataOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSaveRoutesFromImportDataOperationCompleted);
            }
            this.InvokeAsync("SaveRoutesFromImportData", new object[] {
                        PointList,
                        CountryCode,
                        routeId,
                        UserId}, this.SaveRoutesFromImportDataOperationCompleted, userState);
        }
        
        private void OnSaveRoutesFromImportDataOperationCompleted(object arg) {
            if ((this.SaveRoutesFromImportDataCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SaveRoutesFromImportDataCompleted(this, new SaveRoutesFromImportDataCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetProvinciasFromMaps", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string[] GetProvinciasFromMaps(string CountryCode) {
            object[] results = this.Invoke("GetProvinciasFromMaps", new object[] {
                        CountryCode});
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void GetProvinciasFromMapsAsync(string CountryCode) {
            this.GetProvinciasFromMapsAsync(CountryCode, null);
        }
        
        /// <remarks/>
        public void GetProvinciasFromMapsAsync(string CountryCode, object userState) {
            if ((this.GetProvinciasFromMapsOperationCompleted == null)) {
                this.GetProvinciasFromMapsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetProvinciasFromMapsOperationCompleted);
            }
            this.InvokeAsync("GetProvinciasFromMaps", new object[] {
                        CountryCode}, this.GetProvinciasFromMapsOperationCompleted, userState);
        }
        
        private void OnGetProvinciasFromMapsOperationCompleted(object arg) {
            if ((this.GetProvinciasFromMapsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetProvinciasFromMapsCompleted(this, new GetProvinciasFromMapsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetCantonesFromMaps", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string[] GetCantonesFromMaps(string CountryCode, string Provincia) {
            object[] results = this.Invoke("GetCantonesFromMaps", new object[] {
                        CountryCode,
                        Provincia});
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void GetCantonesFromMapsAsync(string CountryCode, string Provincia) {
            this.GetCantonesFromMapsAsync(CountryCode, Provincia, null);
        }
        
        /// <remarks/>
        public void GetCantonesFromMapsAsync(string CountryCode, string Provincia, object userState) {
            if ((this.GetCantonesFromMapsOperationCompleted == null)) {
                this.GetCantonesFromMapsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetCantonesFromMapsOperationCompleted);
            }
            this.InvokeAsync("GetCantonesFromMaps", new object[] {
                        CountryCode,
                        Provincia}, this.GetCantonesFromMapsOperationCompleted, userState);
        }
        
        private void OnGetCantonesFromMapsOperationCompleted(object arg) {
            if ((this.GetCantonesFromMapsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetCantonesFromMapsCompleted(this, new GetCantonesFromMapsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void RoutingByStreetCompletedEventHandler(object sender, RoutingByStreetCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class RoutingByStreetCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal RoutingByStreetCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public double Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((double)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void GetLocationsByLatLonCompletedEventHandler(object sender, GetLocationsByLatLonCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetLocationsByLatLonCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetLocationsByLatLonCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void SaveRoutesFromImportDataCompletedEventHandler(object sender, SaveRoutesFromImportDataCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class SaveRoutesFromImportDataCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal SaveRoutesFromImportDataCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void GetProvinciasFromMapsCompletedEventHandler(object sender, GetProvinciasFromMapsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetProvinciasFromMapsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetProvinciasFromMapsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void GetCantonesFromMapsCompletedEventHandler(object sender, GetCantonesFromMapsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetCantonesFromMapsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetCantonesFromMapsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591