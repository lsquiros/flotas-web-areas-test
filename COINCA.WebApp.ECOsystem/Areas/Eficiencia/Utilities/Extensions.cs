﻿/************************************************************************************************************
*  File    :Extensions.cs
*  Summary : Extensions has common Methods for Extens
*  Author  : Berman Romero
*  Date    : 08/01/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web;

namespace ECO_Efficiency.Utilities
{
    /// <summary>
    /// Extensions Class
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Convert the passed datetime into client timezone.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime ToClientTime(this DateTime dt)
        {
            var timeOffSet = HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"];  // read the value from session
            if (timeOffSet == null) return dt.ToLocalTime(); // if there is no offset in session return the datetime in server timezone
            var offset = int.Parse(timeOffSet.ToString());
            dt = dt.AddMinutes(offset);
            return dt;
        }

        /// <summary>
        /// Convert the passed datetime into client timezone.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static TimeSpan ToClientTime(this TimeSpan dt)
        {
            var timeOffSet = HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"];  // read the value from session
            if (timeOffSet == null) return dt; // if there is no offset in session return the datetime in server timezone
            var offset = int.Parse(timeOffSet.ToString());
            return dt.Add(TimeSpan.FromMinutes(offset));
        }

        /// <summary>
        /// Convert the passed datetime into server timezone.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime ToServerTime(this DateTime dt)
        {
            var timeOffSet = HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"];  // read the value from session
            if (timeOffSet == null) return dt.ToLocalTime(); // if there is no offset in session return the datetime in server timezone
            var offset = int.Parse(timeOffSet.ToString());
            dt = dt.AddMinutes(offset * -1);
            return dt;
        }

        /// <summary>
        /// Convert the passed datetime into client timezone.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static TimeSpan ToServerTime(this TimeSpan dt)
        {
            var timeOffSet = HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"];  // read the value from session
            if (timeOffSet == null) return dt; // if there is no offset in session return the datetime in server timezone
            var offset = int.Parse(timeOffSet.ToString());
            return dt.Add(TimeSpan.FromMinutes(offset * -1));
        }
        
        /// <summary>
        /// Convert the passed datetime into client timezone.
        /// </summary>
        /// <returns></returns>
        public static int ToTimeOffSet(this int i)
        {
            var timeOffSet = HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"];  // read the value from session

            if (timeOffSet == null) return 0; // if there is no offset in session return the datetime in server timezone

            i = int.Parse(timeOffSet.ToString());

            return i;
        }
    }
}