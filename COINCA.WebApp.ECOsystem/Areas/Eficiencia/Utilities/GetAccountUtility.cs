﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;

namespace ECO_Efficiency.Utilities
{
    public class GetAccountUtility
    {

        public void GetUserInformation(ref HttpContextBase context) //, int? userId)
        {
            HttpCookie aCookie = context.Request.Cookies["ECOCFG"];
            Users user = null;

            if (aCookie != null)
            {
                aCookie.Expires = DateTime.Now.AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["ECOCFG_TIME"]));
                //context.Response.Cookies.Add(aCookie);
                string text = aCookie.Value;
                TokenByUserModel userActive = AccountManagement.TokenUserRetrieve(text);

                if (!context.Request.IsAuthenticated)
                {
                    //if the customer has info it will log with the customer information
                    if (userActive.SerializeUserInfo != null)
                    {
                        context.Session.Add("LOGGED_USER_INFORMATION", userActive.User);
                    }
                    else
                    {
                        using (var business = new UsersBusiness())
                        {
                            user = business.RetrieveUsers(null, userName: userActive.UserId).FirstOrDefault();
                            user.DecryptedPassword = userActive.Context;
                        }
                        context.Session.Add("LOGGED_USER_INFORMATION", user);  
                    }                
                }
                else 
                {
                    //if the customer has info it will log with the customer information
                    if (userActive.SerializeUserInfo != null)
                    {
                        context.Session.Add("LOGGED_USER_INFORMATION", userActive.User);
                    }
                    else
                    {
                        using (var business = new UsersBusiness())
                        {
                            user = business.RetrieveUsers(null, userName: userActive.UserId).FirstOrDefault();
                            user.DecryptedPassword = userActive.Context;
                        }
                        context.Session.Add("LOGGED_USER_INFORMATION", user);
                    }                    
                }                
            } 
        }
    }
}