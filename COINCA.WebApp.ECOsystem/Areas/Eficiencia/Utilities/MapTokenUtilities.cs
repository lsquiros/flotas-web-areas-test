﻿/************************************************************************************************************
*  File    : Maps.cs
*  Summary : Maps methods related to maps authentication
*  Author  : María de los Ángeles Jiménez 
*  Descrip : User auth.
*  
*  Copyright 2019 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECO_Efficiency.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;

namespace ECO_Efficiency.Utilities
{
    public static class MapTokenUtilities
    {

        public static bool GenerateToken()
        {
            var user = ECOsystem.Utilities.Session.GetUserInfo();
            var cont = 0;
            while (string.IsNullOrEmpty(user.MapsToken) && cont < 5)
            {
                if (!string.IsNullOrEmpty(ApiLoginAsync(user))) return true;
                cont++;
            }
            return false;
        }
        
        public static string ApiLoginAsync(Users user)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    //Security
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var url = string.Format("{0}{1}", ConfigurationManager.AppSettings["UrlApiToken"], "login");
                    var response = client.PostAsJsonAsync(url, JsonConvert.SerializeObject(new Dictionary<string, string> {
                                { "user", user.DecryptedEmail },
                                { "password", user.DecryptedPassword },
                                { "domain", "controlcar-app" },
                                { "customerId", string.Format("{0}", ECOsystem.Utilities.Session.GetCustomerId()) },
                                { "expiretime", string.Format("{0}", 480) } //8 hours
                                //{ "expiretime", string.Format("{0}", user.MapsExpirateTimeToken) }
                            })).Result;

                    if (!response.IsSuccessStatusCode) throw new Exception(response.Content.ReadAsStringAsync().Result);

                    var respMessage = JsonConvert.DeserializeObject<MapTokenModels>(response.Content.ReadAsStringAsync().Result);

                    user.MapsToken = respMessage.token;
                    user.MapsRefreshToken = respMessage.refreshToken;

                    HttpContext.Current.Session.Remove("LOGGED_USER_INFORMATION");
                    HttpContext.Current.Session.Add("LOGGED_USER_INFORMATION", user);

                    return user.MapsToken;

                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return string.Empty;
            }
        }
    }
}