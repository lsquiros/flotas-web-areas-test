﻿/************************************************************************************************************
*  File    : VehicleByRouteBusiness.cs
*  Summary : VehicleByRoute Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 11/20/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// VehicleByRoute Business
    /// </summary>
    public class VehicleByRouteBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleByRoute> RetrieveVehicles(int? vehicleId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = Session.GetUserInfo();
                return dba.ExecuteReader<VehicleByRoute>("[General].[Sp_Vehicles_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId,
                        CustomerId = Session.GetCustomerId(),
                        Key = key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve VehicleByRoute
        /// </summary>
        /// <param name="geoFenceId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerableT T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public VehicleByRoute RetrieveVehicleByRoute(int? VehicleId, int? RouteId)
        {
            using (var dba = new DataBaseAccess())
            {
                VehicleByRoute result = new VehicleByRoute();
                IEnumerable<VehicleByRouteDetail> list = dba.ExecuteReader<VehicleByRouteDetail>("[Efficiency].[Sp_VehicleByRoute_Retrieve]",
                    new
                    {
                        VehicleId, RouteId
                    });

                foreach (var x in list) 
                {
                    if (x.Days.Contains("D")) { x.Sunday = true; } else { x.Sunday = false; }
                    if (x.Days.Contains("L")) { x.Monday = true; } else { x.Monday = false; }
                    if (x.Days.Contains("K")) { x.Tuesday = true; } else { x.Tuesday = false; }
                    if (x.Days.Contains("M")) { x.Wednesday = true; } else { x.Wednesday = false; }
                    if (x.Days.Contains("J")) { x.Thursday = true; } else { x.Thursday = false; }
                    if (x.Days.Contains("V")) { x.Friday = true; } else { x.Friday = false; }
                    if (x.Days.Contains("S")) { x.Saturday = true; } else { x.Saturday = false; }
                }
                result.DetailList = list;
                return result;
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleByRoute
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleByRoute(List<string> model)
        {
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            //var i = 0;
            int VehicleId = 0;
            foreach (string item in model)
            {
                string[] split = item.Split('-');
                VehicleId = Convert.ToInt32(split[0]); //Convert.ToInt16(split[0]);
                root.Add(new XElement("VehicleByRoute", new XAttribute("RouteId", split[1]), new XAttribute("Days", split[2])));
            }
            
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_VehicleByRoute_AddOrEdit]",
                    new
                    {
                        VehicleId,
                        XmlData = doc.ToString(),
                        LoggedUserId = Session.GetUserInfo().UserId
                    });
            }

        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity GeoFence
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleByRoute(GeoFences model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_GeoFences_AddOrEdit]",
                    new
                    {
                        model.GeoFenceId,
                        model.Name,
                        model.Active,
                        CustomerId = Session.GetCustomerId(),
                        model.Alarm,
                        model.AlarmId,
                        model.Description,
                        model.Polygon,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model GeoFence
        /// </summary>
        /// <param name="GeoFenceId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteGeoFences(int GeoFenceId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_GeoFences_Delete]",
                    new {GeoFenceId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}

