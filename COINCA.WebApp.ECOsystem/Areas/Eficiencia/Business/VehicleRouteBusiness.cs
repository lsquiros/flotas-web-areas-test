﻿/************************************************************************************************************
*  File    : VehicleRouteBusiness.cs
*  Summary : VehicleRoute Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 11/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Linq;
using Newtonsoft.Json;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// VehicleRoute Business Class
    /// </summary>
    public class VehicleRouteBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleRoute> RetrieveVehicles(int? vehicleId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = Session.GetUserInfo();
                return dba.ExecuteReader<VehicleRoute>("[General].[Sp_Vehicles_Retrieve]",
                    new
                    {
                       
                        VehicleId = vehicleId,
                        CustomerId= Session.GetCustomerId(),
                        Key = key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve VehicleRoute
        /// </summary>
        /// <param name="pVeicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleRoute> RetrieveIntrackReference(int pVeicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<VehicleRoute>("[Efficiency].[Sp_IntrackReference_Retrieve]",
                    new
                    {
                        VeicleId = pVeicleId
                    });

            }
        }

        /// <summary>
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public DataSet ReportGenerate(int vehicleID,string Placa)
        {
            RouteReportsJourney dtable;
            DataSet ds = new DataSet();

            List<RouteReportsJourney> Listreport1 = new List<RouteReportsJourney>();
            List<RouteReportsJourney> Listreport2 = new List<RouteReportsJourney>();

            using (var dba = new DataBaseAccess())
            {
                dtable = dba.ExecuteReader<RouteReportsJourney>("[Efficiency].[Sp_RoutesReport_Retrieve]",
                    new
                    {
                        VehicleId = vehicleID
                    }).FirstOrDefault();
            }

            Listreport1 = JsonConvert.DeserializeObject<List<RouteReportsJourney>>(dtable.Valor1);
            Listreport2 = JsonConvert.DeserializeObject<List<RouteReportsJourney>>(dtable.Valor2);


            using (var dt = new DataTable("VehicleRouteReport"))
            {
                dt.Columns.Add("Point");
                dt.Columns.Add("Schedule");
                dt.Columns.Add("RealTime");
                dt.Columns.Add("Difference");
                dt.Columns.Add("Type");
                dt.Columns.Add("VehiclePlate");
                dt.Columns.Add("Fecha");
				dt.Columns.Add("CustomerName");

				foreach (RouteReportsJourney item in Listreport1)
                {
                    DataRow drRoute = dt.NewRow(); 
                    drRoute["Point"] = item.Nombre;
                    drRoute["Schedule"] = item.Valor1;
                    if (string.IsNullOrEmpty(item.Valor2))
                    {
                        item.Valor2 = "No Llegó";
                    }

                    drRoute["RealTime"] = item.Valor2;
                    if (item.Valor3 == "No Visitado")
                    {
                        item.Valor3 = "0";
                    }
                    drRoute["Difference"] = item.Valor3;
                    drRoute["Type"] = item.Color;
                    drRoute["VehiclePlate"] = Placa;
                    drRoute["Fecha"] = item.Fecha;
					drRoute["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
					dt.Rows.Add(drRoute);
                }
                ds.Tables.Add(dt);
            }


            using (var dt1 = new DataTable("VehicleRouteDetail"))
            {
                dt1.Columns.Add("Point");
                dt1.Columns.Add("Schedule");
                dt1.Columns.Add("RealTime");
                dt1.Columns.Add("Difference");
                dt1.Columns.Add("Type");
                dt1.Columns.Add("Fecha");
               
                foreach (RouteReportsJourney item in Listreport2)
                {
                    DataRow drRoute1 = dt1.NewRow();
                    drRoute1["Point"] = item.Nombre;
                    if (item.Valor1 == ".00")
                    {
                        item.Valor1 = "0.00";
                    }
                    drRoute1["Schedule"] = item.Valor1;

                    if (item.Valor2 == ".00")
                    {
                        item.Valor2 = "0.00";
                    }
                    drRoute1["RealTime"] = item.Valor2;
                    if (item.Valor3 == ".00%")
                    {
                        item.Valor3 = "0.00%";
                    }
                    drRoute1["Difference"] = item.Valor3;
                    drRoute1["Type"] = item.Color ;
                    drRoute1["Fecha"] = item.Fecha;
                    dt1.Rows.Add(drRoute1);
                }
                ds.Tables.Add(dt1);
            }

            return ds;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}
