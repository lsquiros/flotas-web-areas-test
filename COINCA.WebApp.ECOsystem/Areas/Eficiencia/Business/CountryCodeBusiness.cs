﻿/************************************************************************************************************
*  File    : GeoFencesBusiness.cs
*  Summary : GeoFences Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 10/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// CountryCode Business Class
    /// </summary>
    public class CountryCodeBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve CountryCode
        /// </summary>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CountryCodeModels> RetrieveCountryCode()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CountryCodeModels>("[Efficiency].[Sp_CountryCode_Retrieve]",
                    new
                    {
                        CustomerId= Session.GetCustomerId()
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}
