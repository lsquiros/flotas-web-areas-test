﻿/************************************************************************************************************
*  File    : PassControlVehicleBusiness.cs
*  Summary : PassControlVehicle Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 11/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;
using System.Data;
using System.Web;

namespace ECO_Efficiency.Business
{
    public class PassControlVehicleBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PassControlVehicle> RetrieveVehicles(int? vehicleId, string key = null)
        {            
            using (var dba = new DataBaseAccess())
            {
                var user = Session.GetUserInfo();
                return dba.ExecuteReader<PassControlVehicle>("[General].[Sp_Vehicles_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId,
                        CustomerId= Session.GetCustomerId(),
                        Key = key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve PassControlVehicle
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PassControlVehicle> RetrieveIntrackReference(int pVeicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PassControlVehicle>("[Efficiency].[Sp_IntrackReference_Retrieve]",
                    new
                    {
                        VeicleId = pVeicleId
                    });

            }
        }

        public DataTable GetPassControlVehicleReportDownloadGenerateData(PassControlReport[] pReportData)
        {
            try
            {
                var dt = new DataTable();

                dt.Columns.Add("Id");
                dt.Columns.Add("InDateTime");
                dt.Columns.Add("OutDateTime");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("SubUnitName");
                dt.Columns.Add("GroupName");
                dt.Columns.Add("DriverName");
                dt.Columns.Add("Dallas");
                dt.Columns.Add("Longitude");//, typeof(decimal));
                dt.Columns.Add("Latitude");//, typeof(decimal));
                dt.Columns.Add("Dir");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Dates");

                foreach (var item in pReportData)
                {
                    DataRow row = GetInOutRow(dt, item);
                    dt.Rows.Add(row);
                }

                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static DataRow GetInOutRow(DataTable dt2, dynamic details)
        {
            var customerName = Session.GetCustomerInfo().DecryptedName;
            var dates = HttpContext.Current.Session["RangoFechas"].ToString();
            DataRow row2 = dt2.NewRow();

            row2["Id"] = details.Id;
            row2["InDateTime"] = details.InDateTime;
            row2["OutDateTime"] = details.OutDateTime;
            row2["PlateId"] = details.PlateId;
            row2["VehicleName"] = details.VehicleName;
            row2["SubUnitName"] = details.SubUnitName;
            row2["GroupName"] = details.GroupName;
            row2["DriverName"] = details.DriverName;
            row2["Dallas"] = details.Dallas;
            row2["Longitude"] = details.Longitude;
            row2["Latitude"] = details.Latitude;
            row2["Dir"] = details.Dir;
            row2["CustomerName"] = customerName;
            row2["Dates"] = dates;

            return row2;
        }

        public DataSet GetPassControlVehicleReportDownloadGenerateDataSet(PassControlReport[] pReportData)
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dt1 = new DataTable();
                DataTable dt2 = new DataTable();

                dt1 = new DataTable("PassControlVehicleReport");
                dt2 = new DataTable("PassControlVehicleDetail");

                dt1.Columns.Add("Id");
                dt1.Columns.Add("InDateTime");
                dt1.Columns.Add("OutDateTime");
                dt1.Columns.Add("PlateId");
                dt1.Columns.Add("VehicleName");
                dt1.Columns.Add("SubUnitName");
                dt1.Columns.Add("GroupName");
                dt1.Columns.Add("DriverName");
                dt1.Columns.Add("Dallas");
                dt1.Columns.Add("Longitude");//, typeof(decimal));
                dt1.Columns.Add("Latitude");//, typeof(decimal));
                dt1.Columns.Add("Dir");
                dt1.Columns.Add("CustomerName");
                dt1.Columns.Add("Dates");

                dt2.Columns.Add("Fecha");
                dt2.Columns.Add("Evento");
                dt2.Columns.Add("Longitude");//, typeof(decimal));
                dt2.Columns.Add("Latitude");//, typeof(decimal));
                dt2.Columns.Add("Dir");
                dt2.Columns.Add("ParentID");

                foreach (var item in pReportData)
                {
                    if (item.ParentID == 0)
                    {
                        DataRow row = GetInOutRow(dt1, item);
                        dt1.Rows.Add(row);
                    }
                    else
                    {
                        DataRow row2 = GetOnOffRow(dt2, item);
                        dt2.Rows.Add(row2);
                    }
                }

                ds.Tables.Add(dt1);
                ds.Tables.Add(dt2);
                return ds;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static DataRow GetOnOffRow(DataTable dt2, dynamic details)
        {
            DataRow row2 = dt2.NewRow();

            row2["Fecha"] = details.InDateTime;
            row2["Evento"] = details.EventOnOff;
            row2["Longitude"] = details.Longitude;
            row2["Latitude"] = details.Latitude;
            row2["Dir"] = details.Dir;
            row2["ParentID"] = details.ParentID;

            return row2;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
