﻿using ECO_Efficiency.Models;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ECO_Efficiency.Business
{
    public class VehicleGPSSummaryBusiness : IDisposable
    {
        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="result"></param>
        /// <param name="parameters"></param>
        public IEnumerable<VehicleGPSSummary> GetReportData(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<VehicleGPSSummary>("[Efficiency].[Sp_VehicleGPSSummaryWeekly_Retrieve]", 
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.StartDate,
                        parameters.EndDate,
                        Session.GetUserInfo().UserId,
                        CostCenterId = parameters.CostCenterId == -1 ? null : parameters.CostCenterId
                    });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            var result = new VehicleGPSSummaryBase { List = GetReportData(parameters) };
            return ReportGenerate(result.List);
        }


        /// <summary>
        /// ReportGenerate
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private DataTable ReportGenerate(IEnumerable<VehicleGPSSummary> list)
        {
            using (var dt = new DataTable())
            {
                var customerName = Session.GetCustomerInfo().DecryptedName;
                var dates = HttpContext.Current.Session["RangoFechas"].ToString();
                
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("CostCenter");              
                dt.Columns.Add("MaxSpeed");
                dt.Columns.Add("AvgSpeed");
                dt.Columns.Add("OverSpeedSummary");
                dt.Columns.Add("OnCount");
                dt.Columns.Add("Stops");
                dt.Columns.Add("AbruptTurns");
                dt.Columns.Add("SuddenStops");
                dt.Columns.Add("QuickAccelerations");
                dt.Columns.Add("OnLapseTime");
                dt.Columns.Add("OffLapseTime");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Dates");
                dt.Columns.Add("AdminSpeed");
                dt.Columns.Add("AuthorizedTime");
                dt.Columns.Add("UnAuthorizedTime");
                dt.Columns.Add("DriverName");
                dt.Columns.Add("Kilometers");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("OverSpeedCount");
                dt.Columns.Add("TemperatureAVG");
                dt.Columns.Add("VehicleGroupName");

                foreach (var item in list)
                {
                    var drGPSSum = dt.NewRow();
                    
                    drGPSSum["VehicleName"] = item.VehicleName;
                    drGPSSum["PlateId"] = item.PlateId;
                    drGPSSum["CostCenter"] = item.CostCenter;               
                    drGPSSum["MaxSpeed"] = item.MaxSpeed;
                    drGPSSum["AvgSpeed"] = item.AvgSpeed;
                    drGPSSum["OverSpeedSummary"] = item.OverSpeedSummary;
                    drGPSSum["OnCount"] = item.OnCount;
                    drGPSSum["Stops"] = item.Stops;
                    drGPSSum["AbruptTurns"] = item.AbruptTurns;
                    drGPSSum["SuddenStops"] = item.SuddenStops;
                    drGPSSum["QuickAccelerations"] = item.QuickAccelerations;
                    drGPSSum["OnLapseTime"] = item.LapseOn;
                    drGPSSum["OffLapseTime"] = item.LapseOff;
                    drGPSSum["CustomerName"] = customerName;
                    drGPSSum["Dates"] = dates;
                    drGPSSum["AdminSpeed"] = item.AdminSpeed;
                    drGPSSum["AuthorizedTime"] = item.AuthorizedTime;
                    drGPSSum["UnAuthorizedTime"] = item.UnAuthorizedTime;
                    drGPSSum["DriverName"] = item.DecryptDriverName;
                    drGPSSum["Kilometers"] = item.Kilometers;
                    drGPSSum["CostCenterName"] = item.CostCenterName;
                    drGPSSum["OverSpeedCount"] = item.OverSpeedCount;
                    drGPSSum["TemperatureAVG"] = item.TemperatureAVG;
                    drGPSSum["VehicleGroupName"] = item.VehicleGroupName;
                    dt.Rows.Add(drGPSSum);
                }
                return dt;
            }
        }


        public IEnumerable<VehicleCostCenters> RetrieveVehicleCostCenters(int? costCenterId, Boolean opc = false, string key = null)
        {
            using (DataBaseAccess dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<VehicleCostCenters>("[General].[Sp_VehicleCostCenters_Retrieve]",
                new
                {
                    CostCenterId = costCenterId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key,
                    user.UserId
                });

            }
        }


        /// <summary>
        /// GC Method
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}