﻿/************************************************************************************************************
*  File    : PassControlSubUnitBusiness.cs
*  Summary : PassControlSubUnit Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 11/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// PassControlSubUnit Class
    /// </summary>
    public class PassControlSubUnitBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleCostCenters
        /// </summary>
        /// <param name="costCenterId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PassControlSubUnit> RetrieveVehicleCostCenters(int? costCenterId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = Session.GetUserInfo();

                return dba.ExecuteReader<PassControlSubUnit>("[General].[Sp_VehicleCostCenters_Retrieve]",
                    new
                    {
                        CostCenterId = costCenterId,
                        CustomerId= Session.GetCustomerId(),
                        Key = key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve SubUnit
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PassControlSubUnit> RetrieveIntrackReference(int pId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PassControlSubUnit>("[Efficiency].[Sp_SubUnitReference_Retrieve]",
                    new
                    {
                        Id = pId
                    });

            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }


    }
}


