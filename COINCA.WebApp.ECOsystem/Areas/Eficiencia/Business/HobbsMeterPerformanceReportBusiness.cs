﻿/************************************************************************************************************
*  File    : HobbsMeterPerformanceReportBusiness.cs
*  Summary : HobbsMeterPerformanceReport Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;
using Newtonsoft.Json;
using System.Data;
using System.Collections.Generic;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// HobbsMeterPerformanceReport Class
    /// </summary>
    public class HobbsMeterPerformanceReportBusiness : IDisposable
    {
        /// <summary>
        /// Get report data like a DataTable object
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// /// <returns>A DataTable object</returns>
        public DataTable GetReportDownloadData(HobbsMeterPerformanceParameters parameters)
        {
            return DataTableUtilities.ClassToDataTable(GetReportDataByVehicle(parameters), string.Empty);
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// /// <returns>An model of HobbsMeterPerformanceReport in order to load the chart</returns>
        private List<HobbsMeterPerformanceReport> GetReportDataByVehicle(HobbsMeterPerformanceParameters parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = Session.GetUserInfo();
                return dba.ExecuteReader<HobbsMeterPerformanceReport>("[Control].[Sp_HobbsMeterPerformanceReportByVehicle_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        parameters.Key,
                        user.UserId,
                        parameters.Dates
                    }).ToList();
            }
        }

        /// <summary>
        /// Retrieve Real Vs Budget Fuels Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of HobbsMeterPerformanceReportBase in order to load the chart</returns>
        public HobbsMeterPerformanceReportBase RetrieveHobbsMeterPerformanceReport(HobbsMeterPerformanceParameters parameters)
        {
            var result = new HobbsMeterPerformanceReportBase();

            result.List =  GetReportDataByVehicle(parameters);
            
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}