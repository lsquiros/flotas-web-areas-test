﻿/************************************************************************************************************
*  File    : RoutesBusiness.cs
*  Summary : Routes Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 10/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;
using ECO_Maps.Business.Routes;
using Newtonsoft.Json;
using ECOsystem.Business.Utilities;
using System.Linq;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// Routes Business Class
    /// </summary>
    public class RoutesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Routes
        /// </summary>
        /// <param name="routeId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Routes> RetrieveRoutes(int? routeId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Routes>("[Efficiency].[Sp_Routes_Retrieve]",
                    new
                    {
                        RouteId = routeId,
                        CustomerId= Session.GetCustomerId(),
                        Key = key
                    });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelList"></param>
        /// <returns></returns>
        public byte[] CreateTxtFormat(IEnumerable<RoutesErrors> modelList)
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    var sw = new StreamWriter(ms);
                    foreach (var item in modelList)
                    {
                        sw.WriteLine(item.Item);
                        sw.Flush();
                    }
                    return ms.ToArray();
                }
            }
            catch (Exception)
            {
                throw new SystemException("System Exception when server tries to generate excel file");
            }
        }

        /// <summary>
        /// Retrieve RoutesDetail
        /// </summary>
        /// <param name="routeId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public RoutesSegmentsBase RetrieveRoutesDetail(int? routeId)
        {
            using (var dba = new DataBaseAccess())
            {
                RoutesSegmentsBase result = new RoutesSegmentsBase();
                List<RoutesPoints> pointsList =
                    (List<RoutesPoints>)dba.ExecuteReader<RoutesPoints>("[Efficiency].[Sp_RoutesPoints_Retrieve]",
                    new
                    {
                        RouteId = routeId
                    });
                List<RoutesSegments> segmentsList = 
                    (List<RoutesSegments>)dba.ExecuteReader<RoutesSegments>("[Efficiency].[Sp_RoutesSegments_Retrieve]",
                    new
                    {
                        RouteId = routeId
                    });
                result.Points = pointsList;
                result.Segments = segmentsList;
                return result;
            }
        }


        public RoutesPoints RetrievePointDetail(int? routeId, int? pointId)
        {
 
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<RoutesPoints>("[Efficiency].[Sp_RoutesPoints_Retrieve]",
                   new
                   {
                       RouteId = routeId,
                       PointId = pointId
                   }).FirstOrDefault();
                  
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Route
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public int AddOrEditRoutes(Routes model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Efficiency].[Sp_Routes_AddOrEdit]",
                    new
                    {
                        model.RouteId,
                        model.Name,
                        model.Active,
                        CustomerId= Session.GetCustomerId(),
                        model.Description,
                        model.LoggedUserId
                    });
            }
        }
                
        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Route
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <param name="pRouteId"></param>
        public void AddOrEditRoutesDetail(RoutesSegmentsBase model, int pRouteId)
        {
            XmlSerializer xsSegments = new XmlSerializer(typeof(List<RoutesSegments>));
            StringWriter swwSegments = new StringWriter();
            XmlWriter writerSegments = XmlWriter.Create(swwSegments);
            xsSegments.Serialize(writerSegments, model.Segments);
            string xmlSegments = swwSegments.ToString();

            XmlSerializer xsPoints = new XmlSerializer(typeof(List<RoutesPoints>));
            StringWriter swwPoints = new StringWriter();
            XmlWriter writerPoints = XmlWriter.Create(swwPoints);
            xsPoints.Serialize(writerPoints, model.Points);
            string xmlPoints = swwPoints.ToString();


            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_RoutesDetail_AddOrEdit]",
                    new
                    {

                        RouteId = pRouteId, Session.GetUserInfo().LoggedUserId,
                        XmlPoints = xmlPoints,
                        XmlSegments = xmlSegments
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Route
        /// </summary>
        /// <param name="RouteId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteRoutes(int RouteId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_Routes_Delete]",
                    new {RouteId });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PointList"></param>
        /// <param name="routeName"></param>
        /// <returns></returns>
        public int AddRoutesFromImport(List<RoutesPoints> points, string routeName)
        {
            int? routeId = GetLastRouteId(routeName);

            if(routeId == 0)
            {
                //Save the route in the database
                Routes model = new Routes();
                model.Name = routeName;
                model.Active = true;
                model.Description = routeName;

                AddOrEditRoutes(model);
                routeId = GetLastRouteId(routeName);
            }            

            string PointList = JsonConvert.SerializeObject(points);
            
            if (SaveRoutesImport(PointList, Session.GetCustomerInfo().CountryCode, Convert.ToInt32(routeId), Convert.ToInt32(Session.GetUserInfo().UserId)))
            {
                return Convert.ToInt32(routeId); 
            }

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public int? GetLastRouteId(string Name)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int?>("[Efficiency].[Sp_LastRouteId_Retrieve]", 
                    new 
                    { 
                        Name,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    });               
            }

        }

        /// <summary>
        /// Save Routes
        /// </summary>
        /// <param name="PointList"></param>
        /// <param name="CountryCode"></param>
        /// <param name="routeId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        private bool SaveRoutesImport(string PointList, string CountryCode, int routeId, int UserId)
        {
            try
            {
                using (var bus = new ECOsystem.Eco_Maps_Service.Routing())
                {
                    if (bus.SaveRoutesFromImportData(PointList, CountryCode, routeId, UserId))
                    {
                        return true;
                    }
                    return false;
                }                 
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return false;   
            }
            
        }

        public void PointsAddOrEdit(RoutesPoints model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_RoutePoints_AddOrEdit]",
                new
                {
                    model.PointId,
                    model.RouteId, 
                    model.PointReference,
                    model.Order, 
                    model.Name, 
                    model.Time, 
                    model.StopTime, 
                    model.CommerceId,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="PointList"></param>
        /// <param name="routeName"></param>
        /// <returns></returns>
        public int AddRoutesFromReconstructing(List<RoutesPoints> points, string routeName, string routeDescription)
        {
            int? routeId = GetLastRouteId(routeName);

            if (routeId == 0)
            {
                //Save the route in the database
                Routes model = new Routes();
                model.Name = routeName;
                model.Active = true;
                model.Description = routeDescription;

                AddOrEditRoutes(model);
                routeId = GetLastRouteId(routeName);
            }

            string PointList = JsonConvert.SerializeObject(points);

            if (SaveRoutesImport(PointList, Session.GetCustomerInfo().CountryCode, Convert.ToInt32(routeId), Convert.ToInt32(Session.GetUserInfo().UserId)))
            {
                return Convert.ToInt32(routeId);
            }

            return 0;
        }

        public string GetSerializedRouteModel(RoutesPoints model)
        {
            var mapmodel = new ECO_Maps.Models.PointsList() 
            { 
                Id  = model.PointId,
                Order = model.Order,
                Name = model.Name, 
                Hour = Convert.ToInt32(model.TimeStr24.Split(':')[0]),
                Minute = Convert.ToInt32(model.TimeStr24.Split(':')[1]),
                StopTime = model.StopTime,
                x = Convert.ToDouble(model.Longitude),
                y = Convert.ToDouble(model.Latitude),
                NewPoint = model.NewPoint
            };
            return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(mapmodel);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }


    
    }
}


