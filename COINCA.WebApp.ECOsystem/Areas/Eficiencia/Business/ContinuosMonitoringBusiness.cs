﻿using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;
using System.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Data;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// ContinuosMonitoringBusiness
    /// </summary>
    public class ContinuosMonitoringBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve the information about the vehicles which are being monitoring
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VehiclesMonitoring> RetrieveVehiclesMonitoring()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<VehiclesMonitoring>("[Efficiency].[Sp_VehiclesMonitoring_Retrieve]",
                new
                {
                    CustomerId = Session.GetCustomerId(), Session.GetUserInfo().UserId
                });
            }
        }

        public ContinuosMonitoringModel LoadVehicleTemperature(int VehicleId, DateTime? Date)
        {
            //int LabelsCount = 0;

            var model = new ContinuosMonitoringModel();
            model.VehicleTemperatureList = (List<VehicleTemperatureResume>)HttpContext.Current.Session["VehicleTemperatureList"];

            if (((model.VehicleTemperatureList == null || model.VehicleTemperatureList.Count == 0) || model.VehicleTemperatureList.FirstOrDefault().VehicleId != VehicleId) || (DateTime.Parse(Date == null ? DateTime.Now.ToString() : Date.ToString()).ToString("MM/dd/yyyy") != DateTime.Today.ToString("MM/dd/yyyy")))
            {
                using (var dba = new DataBaseAccess())
                {
                    model.VehicleTemperatureList = dba.ExecuteReader<VehicleTemperatureResume>("[Efficiency].[Sp_VehiclesTemperature_Retrieve]",
                       new
                       {
                           VehicleId,
                           Date
                       }).ToList();
                }
            }           
            
            HttpContext.Current.Session["VehicleTemperatureList"] = model.VehicleTemperatureList;

            IList<IList<decimal?>> data = model.VehicleTemperatureList.Select(x => x.SensorId).Distinct().ToList()
                                               .Select(vehicle => model.VehicleTemperatureList.Where(x => (x.SensorId).Equals(vehicle))
                                                                  .Select(x => x.Temperature as decimal?).ToList()).Cast<IList<decimal?>>().ToList();

            IList<IList<string>> count = model.VehicleTemperatureList.Select(x => x.SensorId).Distinct().ToList()
                                              .Select(vehicle => model.VehicleTemperatureList.Where(x => (x.SensorId).Equals(vehicle))
                                                                .Select(x => x.Date.ToString().Split(' ')[0] + ' ' + x.Date.ToString().Split(' ')[1] + ' ' + (x.Date.ToString().Split(' ')[3] == "" ? x.Date.ToString().Split(' ')[4] 
                                                                                                                                                                                                    : x.Date.ToString().Split(' ')[3]) as string)
                                              .ToList()).Cast<IList<string>>().ToList();

            List<string> labels = new List<string>();
            model.SensorCount = model.VehicleTemperatureList.Select(x => x.SensorId).Distinct().ToList().Count();
            for (int i = 0; i < count.Count(); i++)
            {
                if (labels.Count() < count[i].Count())
                {
                    labels = count[i].ToList();
                }
            }

            model.HourCount = labels.Count();
            model.Labels = new HtmlString(JsonConvert.SerializeObject(labels));
            model.Titles = new HtmlString(JsonConvert.SerializeObject(model.VehicleTemperatureList.Select(x => new { x.SensorId }).Distinct().OrderByDescending(x => x.SensorId).Select(x => "Sensor " + x.SensorId).ToList()));
            model.Data = new HtmlString(JsonConvert.SerializeObject(data));

            model.Colors = new HtmlString(JsonConvert.SerializeObject(Miscellaneous.GenerateColors().Take(model.SensorCount)));
            model.AlphaColors = new HtmlString(JsonConvert.SerializeObject(Miscellaneous.GenerateAlphaColors().Take(model.SensorCount)));
            model.HighlightColors = new HtmlString(JsonConvert.SerializeObject(Miscellaneous.GenerateHighlighColors().Take(model.SensorCount)));

            return model;        
        }

        /// <summary>
        /// ReportGenerate
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public DataTable ReportGenerate(List<VehicleTemperatureResume> list)
        {
            var startDate = HttpContext.Current.Session["StartDate"] == null ? DateTime.Now.ToString("dd/MM/yyyy") : DateTime.Parse(HttpContext.Current.Session["StartDate"].ToString()).ToString("dd/MM/yyyy");
            var customerName = Session.GetCustomerInfo().DecryptedName;

            using (var dt = new DataTable("Temperaturas"))
            {
                dt.Columns.Add("Matricula");
                dt.Columns.Add("Fecha");
                dt.Columns.Add("SensorId");
                dt.Columns.Add("Id");
                dt.Columns.Add("ReportId");
                dt.Columns.Add("Temperature");
				dt.Columns.Add("MinTemperature");
				dt.Columns.Add("MaxTemperature");
				dt.Columns.Add("CustomerName");
                dt.Columns.Add("StartDate");
				dt.Columns.Add("Name");

				foreach (var item in list.OrderBy(x => x.Id))
                {
                    var dr = dt.NewRow();
                    dr["Matricula"] = item.Plate;
                    dr["Fecha"] =  item.Date;
                    dr["SensorId"] = item.SensorId;
                    dr["Id"] = item.Id;
                    dr["ReportId"] = item.ReportId;
                    dr["Temperature"] = item.Temperature;
					dr["MinTemperature"] = item.Temperature;
					dr["MaxTemperature"] = item.Temperature;
					dr["CustomerName"] = customerName;
                    dr["StartDate"] = startDate;
					dr["Name"] = item.Name;

					dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
