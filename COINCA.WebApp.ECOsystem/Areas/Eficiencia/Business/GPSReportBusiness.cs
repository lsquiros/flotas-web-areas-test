﻿using ECO_Efficiency.Models;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

namespace ECO_Efficiency.Business
{
    public class GPSReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Transactions Sap
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public GPSReportBase RetrieveGPSReport()
        {
            var result = new GPSReportBase();
            result.List = (List<GPSReportModel>)HttpContext.Current.Session["GPSData"];            
            result.Menus = new List<AccountMenus>();
            return result;
        }

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="result"></param>
        /// <param name="parameters"></param>
        public IEnumerable<GPSReportModel> GetReportData(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var list = dba.ExecuteReader<GPSReportModel>("[Control].[SP_GPS_Report]",
                new
                {
                    VehicleId = parameters.FilterVehicle,                    
                    parameters.StartDate,
                    parameters.EndDate                    
                });
                HttpContext.Current.Session["GPSData"] = list;
                return list;
            }
        }

        private IEnumerable<GPSReportModel> GetReportStopsDetail(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<GPSReportModel>("[Control].[SP_GPS_ReportStops]",
                new
                {
                    VehicleId = parameters.FilterVehicle,
                    parameters.StartDate,
                    parameters.EndDate,
                    parameters.StopsDetail,
                    parameters.SuddenStops,
                    parameters.AbruptTurns,
                    parameters.QuickAccelerations,
                    parameters.HighTemperature,
                    parameters.LowTemperature, 
                    parameters.PanicButton 
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            var result = new GPSReportBase {List = GetReportData(parameters)};
            return ReportGenerate(result.List, "");
        }

        public DataSet GetReportStopsDownloadData(ControlFuelsReportsBase parameters)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt = ReportGenerate(GetReportData(parameters), "GPSStopsReport");
            ds.Tables.Add(dt);

            //Get the stops
            dt = new DataTable();
            dt = ReportGenerate(GetReportStopsDetail(parameters), "GPSStopsDetail");
            ds.Tables.Add(dt);
            return ds;
        }

        /// <summary>
        /// ReportGenerate
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private DataTable ReportGenerate(IEnumerable<GPSReportModel> list, string tableName)
        {
            using (var dt = new DataTable(tableName))
            {
                var customerName = Session.GetCustomerInfo().DecryptedName;
                var dates = HttpContext.Current.Session["RangoFechas"].ToString();
                var plateId = HttpContext.Current.Session["PlateId"].ToString();
                dt.Columns.Add("START_DATE");
                dt.Columns.Add("END_DATE");
                dt.Columns.Add("LATITUD");
                dt.Columns.Add("LONGITUD");
                dt.Columns.Add("STATUS");
                dt.Columns.Add("KILOMETERS");
                dt.Columns.Add("MAXSPEED");
                dt.Columns.Add("LOCATION");
                dt.Columns.Add("STOPS");
                dt.Columns.Add("SuddenStops");
                dt.Columns.Add("AbruptTurns");
                dt.Columns.Add("QuickAccelerations");
                dt.Columns.Add("ELAPSETIME");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Dates");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("GPSId");
                foreach (var item in list)
                {
                    var drGPS = dt.NewRow();
                    var datediff = string.Empty;
                    if (item.EndDate != null) datediff = (item.EndDate - item.StartDate).ToString();
                    drGPS["START_DATE"] = Convert.ToDateTime(item.StartDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                    drGPS["END_DATE"] = item.EndDate != null ? Convert.ToDateTime(item.EndDate).ToString("dd/MM/yyyy hh:mm:ss tt") : "";
                    drGPS["LATITUD"] = item.Latitud;
                    drGPS["LONGITUD"] = item.Longitud;
                    drGPS["STATUS"] = item.Status;
                    drGPS["KILOMETERS"] = item.Kilometers.ToString("0.00");
                    drGPS["MAXSPEED"] = Convert.ToInt32(item.MaxSpeed);
                    drGPS["LOCATION"] = item.Location;
                    drGPS["STOPS"] = item.Stops;
                    drGPS["SuddenStops"] = item.SuddenStops;
                    drGPS["AbruptTurns"] = item.AbruptTurns;
                    drGPS["QuickAccelerations"] = item.QuickAccelerations;
                    drGPS["ELAPSETIME"] = datediff;
                    drGPS["CustomerName"] = customerName;
                    drGPS["Dates"] = dates;
                    drGPS["PlateId"] = plateId;
                    drGPS["GPSId"] = item.GPSId;
                    dt.Rows.Add(drGPS);
                }

                return dt;
            }
        }

        /// <summary>
        /// GC Method
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}