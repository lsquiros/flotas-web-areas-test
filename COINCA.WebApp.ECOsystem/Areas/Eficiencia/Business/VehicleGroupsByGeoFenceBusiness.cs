﻿/************************************************************************************************************
*  File    : VehicleGroupsByGeoFenceBusiness.cs
*  Summary : VehicleGroupsByGeoFence Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 11/17/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// VehicleGroupsByGeoFence Class
    /// </summary>
    public class VehicleGroupsByGeoFenceBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleGroupsByGeoFence
        /// </summary>
        /// <param name="vehicleGeoFenceId">The vehicle geoFence Id to find all vehicleGroups associated</param>
        /// <returns>An IEnumerable of T, T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public VehicleGroupsByGeoFenceData RetrieveVehicleGroupsByGeoFence(int? geoFenceId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = new VehicleGroupsByGeoFenceData();
                //pendiente sp
                var aux = dba.ExecuteReader<VehicleGroupsByGeoFence>("[Efficiency].[Sp_VehicleGroupsByGeoFence_Retrieve]",
                    new
                    {
                        GeoFenceId = geoFenceId,
                        CustomerId= Session.GetCustomerId()
                    });

                result.AvailableVehicleGroupsList = aux.Where(x => x.GeoFenceId == null);
                result.ConnectedVehicleGroupsList = aux.Where(x => x.GeoFenceId != null);
                result.GeoFenceId = geoFenceId;
                return result;
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleGroupsByGeoFence
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleGroupsByGeoFence(VehicleGroupsByGeoFenceData model)
        {
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            var i = 0;
            foreach (var item in model.ConnectedVehicleGroupsList)
            {
                root.Add(new XElement("VehicleGroup", new XAttribute("VehicleGroupId", item.VehicleGroupId), new XAttribute("Index", ++i)));
            }

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_VehicleGroupsByGeoFence_AddOrEdit]",
                    new
                    {
                        model.GeoFenceId,
                        XmlData = doc.ToString(),
                        LoggedUserId = Session.GetUserInfo().UserId
                    });
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}
