﻿using ECO_Efficiency.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace COINCA.WebApp.ECO_Efficiency.Business
{
    public class VehicleCommandsReportBusiness : IDisposable
    {
        private int GetVehicleDevice(int? VehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_DeviceReferenceByVehicle_Retrieve]", new
                {
                    VehicleId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                });
            }
        }

        private List<ECOsystem.Models.Core.VehicleCommandReport> GetReportData(ControlFuelsReportsBase parameters)
        {
            using (var cmd = new LineVitaCommandsBusiness())
            {
                return cmd.GetCommandLog(GetVehicleDevice(parameters.FilterVehicle), parameters.StartDate, parameters.EndDate, cmd.LineVitaCommandURL());
            }
        }

        public DataTable ReportRetrieve(ControlFuelsReportsBase parameters)
        {
            var list = GetReportData(parameters);
            using (var dt = new DataTable())
            {
                dt.Columns.Add("StartDate");
                dt.Columns.Add("EndDate");
                dt.Columns.Add("Date");
                dt.Columns.Add("Command");
                dt.Columns.Add("Source");
                dt.Columns.Add("ExecutedBy");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("CustomerName");
                foreach (var item in list)
                {
                    var drGPS = dt.NewRow();
                    drGPS["StartDate"] = Convert.ToDateTime(parameters.StartDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                    drGPS["EndDate"] = Convert.ToDateTime(parameters.EndDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                    drGPS["Date"] = Convert.ToDateTime(item.Fecha_Ejecucion).ToString("dd/MM/yyyy hh:mm:ss tt");
                    drGPS["Command"] = item.Comando_Ejecutado;
                    drGPS["Source"] = item.Origen;
                    drGPS["ExecutedBy"] = item.Usuario;
                    drGPS["VehicleName"] = item.Placa;
                    drGPS["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    dt.Rows.Add(drGPS);
                }
                return dt;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}