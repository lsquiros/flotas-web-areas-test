﻿using ECO_Efficiency.Models;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ECO_Efficiency.Business
{
    public class VehicleGPSSummaryWeeklyBusiness : IDisposable
    {
        private List<VehicleGPSSummary> GetReportData(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<VehicleGPSSummary>("[Efficiency].[Sp_DailyActivityGPSReport_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.StartDate,
                        parameters.EndDate,
                        VehicleId = parameters.FilterVehicle,
                        CostCenterId = parameters.CostCenterId == -1 ? null : parameters.CostCenterId,
                        Session.GetUserInfo().UserId
                    }).ToList();
            }
        }

        public DataTable ReportGenerate(ControlFuelsReportsBase parameters)
        {
            using (var dt = new DataTable())
            {
                var list = GetReportData(parameters);
                var customerName = Session.GetCustomerInfo().DecryptedName;
                var dates = string.Format("{0} - {1}", Convert.ToDateTime(parameters.StartDate).ToString("dd/MM/yyyy hh:mm:ss"), Convert.ToDateTime(parameters.EndDate).ToString("dd/MM/yyyy hh:mm:ss"));
                var days = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).DayNames.ToList().Select(x => new { value = x.ToUpperInvariant() }).ToList();

                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("MaxSpeed");
                dt.Columns.Add("OnLapseTime");
                dt.Columns.Add("Kilometers");
                dt.Columns.Add("StartOn");
                dt.Columns.Add("EndOff");
                dt.Columns.Add("DayOW");
                dt.Columns.Add("DayName");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Dates");
                dt.Columns.Add("AdminSpeed");
                dt.Columns.Add("TotalLapse");
                dt.Columns.Add("SlowMotion");
                dt.Columns.Add("TotalSlowMotion");
                dt.Columns.Add("ShortDate");
                dt.Columns.Add("VehicleGroupName");

                foreach (var item in list)
                {
                    var drGPSSum = dt.NewRow();
                    var day = days[item.DayOW].value;
                    drGPSSum["CostCenterName"] = item.CostCenterName;
                    drGPSSum["VehicleName"] = item.VehicleName;
                    drGPSSum["PlateId"] = item.PlateId;
                    drGPSSum["MaxSpeed"] = item.MaxSpeed;
                    drGPSSum["OnLapseTime"] = item.LapseOn;
                    drGPSSum["Kilometers"] = item.Kilometers;
                    drGPSSum["StartOn"] = item.StartOn;
                    drGPSSum["EndOff"] = item.EndOff;
                    drGPSSum["DayOW"] = item.DayOW;
                    drGPSSum["DayName"] = day;
                    drGPSSum["CustomerName"] = customerName;
                    drGPSSum["Dates"] = dates;
                    drGPSSum["AdminSpeed"] = item.AdminSpeed;
                    drGPSSum["TotalLapse"] = TotalLapse(list, item.VehicleId, false);
                    drGPSSum["SlowMotion"] = item.SlowMotion;
                    drGPSSum["TotalSlowMotion"] = TotalLapse(list, item.VehicleId, true);
                    drGPSSum["ShortDate"] = item.StartDate.ToShortDateString();
                    drGPSSum["VehicleGroupName"] = item.VehicleGroupName;

                    dt.Rows.Add(drGPSSum);
                }
                return dt;
            }
        }

        string TotalLapse(List<VehicleGPSSummary> list, int vehicleId, bool slowMotion)
        {
            TimeSpan value = new TimeSpan();
            string time = string.Empty; 
            foreach (var item in list.Where(x => x.VehicleId == vehicleId).ToList())
            {
                if ((!slowMotion && !string.IsNullOrEmpty(item.LapseOn)) ||
                    (slowMotion && !string.IsNullOrEmpty(item.SlowMotion)))
                {
                    time = slowMotion ? item.SlowMotion : item.LapseOn;
                    value = value + new TimeSpan(Convert.ToInt32(time.Split(':')[0]), Convert.ToInt32(time.Split(':')[1]), Convert.ToInt32(time.Split(':')[2]));
                }
            }

            if (!string.IsNullOrEmpty(time))
            {
                return string.Format("{0}:{1}:{2}", (int)value.TotalHours,
                value.Minutes.ToString().Length == 1 ? string.Format("0{0}", value.Minutes.ToString()) : value.Minutes.ToString(),
                value.Seconds.ToString().Length == 1 ? string.Format("0{0}", value.Seconds.ToString()) : value.Seconds.ToString());
            }
            return "00:00:00";
        }

        public void Dispose()
        {        
            GC.SuppressFinalize(this);
        }
    }
}