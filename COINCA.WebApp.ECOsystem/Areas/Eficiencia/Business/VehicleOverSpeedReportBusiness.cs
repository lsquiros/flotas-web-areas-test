﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;
using ECO_Maps.Business.Intrack;
using Newtonsoft.Json;

namespace ECO_Efficiency.Business
{
    public class VehicleOverSpeedReportBusiness : IDisposable
    {

        /// <summary>
        /// Retrieve Transactions Sap
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public VehicleOverSpeedReportModelBase RetrieveInformationReport(ControlFuelsReportsBase parameters)
        {
            var result = new VehicleOverSpeedReportModelBase();

            GetReportData(result, parameters);           
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            result.Menus = new List<AccountMenus>();
            return result;
        }

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="result"></param>
        /// <param name="parameters"></param>
        public void GetReportData(VehicleOverSpeedReportModelBase result, ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                result.List = dba.ExecuteReader<VehicleOverSpeedReportModel>("[Efficiency].[Sp_VehicleOverSpeed_Retrieve]",
                new
                {
                    VehicleId = parameters.FilterVehicle, parameters.StartDate, parameters.EndDate,
                    FilterType = parameters.ReportCriteriaId
                });

                HttpContext.Current.Session["ReporteDatos"] = result.List;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            try
            {
                var result = new VehicleOverSpeedReportModelBase();

                if (HttpContext.Current.Session["ReporteDatos"] != null) result.List = (List<VehicleOverSpeedReportModel>)HttpContext.Current.Session["ReporteDatos"];                
                HttpContext.Current.Session["ReporteDatos"] = null;
                return ReportGenerate(result.List);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private DataTable ReportGenerate(IEnumerable<VehicleOverSpeedReportModel> list)
        {
            using (var dt = new DataTable())
            {
                string CustomerName = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                string Dates = HttpContext.Current.Session["RangoFechas"].ToString();
                string PlateId = HttpContext.Current.Session["PlateId"].ToString();
                dt.Columns.Add("DATE");
                dt.Columns.Add("ELAPSED");
                dt.Columns.Add("MAXSPEED");
                dt.Columns.Add("AVGSPEED");
                dt.Columns.Add("LOCATION");
                dt.Columns.Add("KILOMETERS");
                dt.Columns.Add("DRIVER");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Dates");
                dt.Columns.Add("PlateId");
                foreach (var item in list)
                {
                    DataRow drGPS = dt.NewRow();
                    drGPS["DATE"] = Convert.ToDateTime(item.GPSDateTime).ToString("dd/MM/yyyy hh:mm:ss tt");
                    drGPS["ELAPSED"] = item.ElapsedTime;
                    drGPS["MAXSPEED"] = item.MAXSpeed.ToString("0");
                    drGPS["AVGSPEED"] = item.AVGSpeed.ToString("0");
                    drGPS["LOCATION"] = item.Location;
                    drGPS["KILOMETERS"] = item.Odometer.ToString("0.00");
                    drGPS["DRIVER"] = item.DecryptedDriverName;
                    drGPS["CustomerName"] = CustomerName;
                    drGPS["Dates"] = Dates;
                    drGPS["PlateId"] = PlateId;
                    dt.Rows.Add(drGPS);
                }

                return dt;
            }
        }

        /// <summary>
        /// Get the location for the lat and lon
        /// </summary>
        /// <param name="model"></param>
        public void GetLocations(ref VehicleOverSpeedReportModelBase model)
        {
            List<string> list = new List<string>();
            int i = 0;

            foreach (var item in model.List)
            {
                list.Add(item.Latitude.ToString() + '|' + item.Longitude);
                i++;
            }

            var bus = new GetLocationsBusiness();

            string[] locations = bus.GetAddress(list, Session.GetCustomerInfo().CountryCode);

            i = 0;
            foreach (var item in model.List)
            {
                item.Location = locations[i];
                i++;
            }
        }

        /// <summary>
        /// GC Method
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}