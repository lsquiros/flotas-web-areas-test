﻿/************************************************************************************************************
*  File    : VehiclesByGeoFenceBusiness.cs
*  Summary : VehiclesByGeoFence Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 11/17/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// VehiclesByGeoFence Class
    /// </summary>
    public class VehiclesByGeoFenceBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehiclesByGeoFence
        /// </summary>
        /// <param name="vehicleGeoFenceId">The vehicle geoFence Id to find all vehicles associated</param>
        /// <returns>An IEnumerable of T, T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public VehiclesByGeoFenceData RetrieveVehiclesByGeoFence(int? geoFenceId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = new VehiclesByGeoFenceData();              
                var user = Session.GetUserInfo();

                var aux = dba.ExecuteReader<VehiclesByGeoFence>("[Efficiency].[Sp_VehiclesByGeoFence_Retrieve]",
                    new
                    {
                        GeoFenceId = geoFenceId,
                        CustomerId= Session.GetCustomerId(),
                        user.UserId
                    });

                result.AvailableVehiclesList = aux.Where(x => x.GeoFenceId == null);
                result.ConnectedVehiclesList = aux.Where(x => x.GeoFenceId != null);
                result.GeoFenceId = geoFenceId;
                return result;
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehiclesByGeoFence
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehiclesByGeoFence(VehiclesByGeoFenceData model)
        {
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            var i = 0;
            foreach (var item in model.ConnectedVehiclesList)
            {
                root.Add(new XElement("Vehicle", new XAttribute("VehicleId", item.VehicleId), new XAttribute("Index", ++i)));
            }

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_VehiclesByGeoFence_AddOrEdit]",
                    new
                    {
                        model.GeoFenceId,
                        XmlData = doc.ToString(),
                        LoggedUserId = Session.GetUserInfo().UserId
                    });
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}
