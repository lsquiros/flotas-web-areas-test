﻿/************************************************************************************************************
*  File    : GeoFencesBusiness.cs
*  Summary : GeoFences Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 10/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;
//using COINCA.Library.DataAccess;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// GeoFences Business
    /// </summary>
    public class GeoFencesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve GeoFences
        /// </summary>
        /// <param name="geoFenceId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerableT T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<GeoFences> RetrieveGeoFences(int? geoFenceId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<GeoFences>("[Efficiency].[Sp_GeoFences_Retrieve]",
                    new
                    {
                        GeoFenceId = geoFenceId,
                        CustomerId= Session.GetCustomerId(),
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity GeoFence
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditGeoFences(GeoFences model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_GeoFences_AddOrEdit]",
                    new
                    {
                        model.GeoFenceId, model.Name, model.Active,
                        CustomerId = Session.GetCustomerId(), model.Alarm, model.AlarmId, model.Description, model.Polygon, model.LoggedUserId, model.In, model.Out, model.RowVersion, model.GeoFenceTypeId, model.DriverId
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model GeoFence
        /// </summary>
        /// <param name="GeoFenceId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteGeoFences(int GeoFenceId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_GeoFences_Delete]",
                    new {GeoFenceId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}

