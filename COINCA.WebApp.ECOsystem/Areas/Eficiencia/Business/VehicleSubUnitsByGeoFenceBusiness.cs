﻿/************************************************************************************************************
*  File    : VehicleCostCentersByGeoFenceBusiness.cs
*  Summary : VehicleCostCentersByGeoFence Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 11/17/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// VehicleCostCentersByGeoFence Class
    /// </summary>
    public class VehicleCostCentersByGeoFenceBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleCostCentersByGeoFence
        /// </summary>
        /// <param name="vehicleGeoFenceId">The vehicle geoFence Id to find all VehicleCostCenters associated</param>
        /// <returns>An IEnumerable of T, T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public VehicleCostCentersByGeoFenceData RetrieveVehicleCostCentersByGeoFence(int? geoFenceId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = new VehicleCostCentersByGeoFenceData();
                //pendiente sp
                var aux = dba.ExecuteReader<VehicleCostCentersByGeoFence>("[Efficiency].[Sp_VehicleCostCentersByGeoFence_Retrieve]",
                    new
                    {
                        GeoFenceId = geoFenceId,
                        CustomerId= Session.GetCustomerId()
                    });

                result.AvailableVehicleCostCentersList = aux.Where(x => x.GeoFenceId == null);
                result.ConnectedVehicleCostCentersList = aux.Where(x => x.GeoFenceId != null);
                result.GeoFenceId = geoFenceId;
                return result;
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleCostCentersByGeoFence
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleCostCentersByGeoFence(VehicleCostCentersByGeoFenceData model)
        {
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            var i = 0;
            foreach (var item in model.ConnectedVehicleCostCentersList)
            {
                root.Add(new XElement("VehicleCostCenter", new XAttribute("CostCenterId", item.CostCenterId), new XAttribute("Index", ++i)));
            }

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_VehicleCostCentersByGeoFence_AddOrEdit]",
                    new
                    {
                        model.GeoFenceId,
                        XmlData = doc.ToString(),
                        LoggedUserId = Session.GetUserInfo().UserId
                    });
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}

