﻿/************************************************************************************************************
*  File    : RealTimeGroupBusiness.cs
*  Summary : RealTimeGroup Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 01/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// RealTimeGroup Class
    /// </summary>
    public class RealTimeGroupBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve RealTimeGroup
        /// </summary>
        /// <param name="vehicleGroupId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<RealTimeGroup> RetrieveRealTimeGroup(int? vehicleGroupId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = Session.GetUserInfo();

                return dba.ExecuteReader<RealTimeGroup>("[General].[Sp_VehicleGroups_Retrieve]",
                    new
                    {
                        VehicleGroupId = vehicleGroupId,
                        CustomerId= Session.GetCustomerId(),
                        Key = key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}