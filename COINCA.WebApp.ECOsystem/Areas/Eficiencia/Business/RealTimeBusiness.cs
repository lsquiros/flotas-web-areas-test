﻿/************************************************************************************************************
*  File    : RealTimeBusiness.cs
*  Summary : RealTime Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 11/03/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// RealTime Business Class
    /// </summary>
    public class RealTimeBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<RealTime> RetrieveVehicles(int? vehicleId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = Session.GetUserInfo();
                return dba.ExecuteReader<RealTime>("[General].[Sp_Vehicles_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId,
                        CustomerId= Session.GetCustomerId(),
                        Key = key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve RealTime
        /// </summary>
        /// <param name="pVeicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<RealTime> RetrieveIntrackReference(int pVeicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                IEnumerable<RealTime> result = dba.ExecuteReader<RealTime>("[Efficiency].[Sp_IntrackReference_Retrieve]",
                    new
                    {
                        VeicleId = pVeicleId
                    });
                return result;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}
