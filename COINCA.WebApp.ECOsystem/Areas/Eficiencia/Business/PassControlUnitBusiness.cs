﻿/************************************************************************************************************
*  File    : PassControlUnitBusiness.cs
*  Summary : PassControlUnit Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 13/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;

namespace ECO_Efficiency.Business
{
    /// <summary>
    /// PassControlUnit Class
    /// </summary>
    public class PassControlUnitBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleUnits
        /// </summary>
        /// <param name="unitId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PassControlUnit> RetrieveVehicleUnits(int? unitId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PassControlUnit>("[General].[Sp_VehicleUnits_Retrieve]",
                    new
                    {
                        UnitId = unitId,
                        CustomerId= Session.GetCustomerId(),
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Retrieve SubUnit
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PassControlUnit> RetrieveIntrackReference(int pId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PassControlUnit>("[Efficiency].[Sp_UnitReference_Retrieve]",
                    new
                    {
                        Id = pId
                    });

            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }



    }
}
