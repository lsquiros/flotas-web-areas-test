﻿using ECO_Efficiency.Models;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ECO_Efficiency.Business
{
    public class GeofencesReportBusiness : IDisposable
    {
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            return DataTableUtilities.ClassToDataTable(RetrieveGeofenceAlarmsReport(parameters), string.Empty);
        }

        private List<GeofencesReport> RetrieveGeofenceAlarmsReport(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<GeofencesReport>("[Efficiency].[SP_GeofencesAlarmsReport_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.StartDate,
                        parameters.EndDate,
                        CostCenterId = parameters.CostCenterId == -1 ? null : parameters.CostCenterId,
                        GeofencesId = parameters.GeofencesId == -1 ? null : parameters.GeofencesId,
                        parameters.Dates
                    }).ToList();
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}