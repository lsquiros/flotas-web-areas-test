﻿using ECO_Efficiency.Models;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ECO_Efficiency.Business
{
    public class LastVehicleTransReportBusiness : IDisposable
    {
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            return DataTableUtilities.ClassToDataTable(RetrieveLastVehicleTransReport(parameters), string.Empty);
        }

        private List<LastVehicleTransReportModels> RetrieveLastVehicleTransReport(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<LastVehicleTransReportModels>("[Efficiency].[SP_LastVehicleTransmission_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        parameters.StartDate,
                        CostCenterId = parameters.CostCenterId == -1 ? null : parameters.CostCenterId,
                        parameters.Dates
                    }).ToList();
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}