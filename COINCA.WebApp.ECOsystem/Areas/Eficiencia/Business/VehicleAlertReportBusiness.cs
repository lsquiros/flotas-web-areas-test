﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;
using System.Data;

namespace ECO_Efficiency.Business
{
    public class VehicleAlertReportBusiness : IDisposable
    {

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="result"></param>
        /// <param name="parameters"></param>
        public IEnumerable<VehicleAlertReport> GetReportData(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<VehicleAlertReport>("[Efficiency].[Sp_VehicleGPSAlertReport_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        VehicleId = parameters.FilterVehicle == null? 0 : parameters.FilterVehicle,
                        parameters.CostCenterId,
                        parameters.StartDate,
                        parameters.EndDate,
                        Temperature = parameters.HighTemperature,
                        parameters.PanicButton,
                        parameters.StopsDetail,
                        Session.GetUserInfo().UserId
                    });
            }
        }


        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="result"></param>
        /// <param name="parameters"></param>
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            var result = new VehicleAlertReportModelBase { List = GetReportData(parameters) };
            return ReportGenerate(result.List);
        }

        /// <summary>
        /// ReportGenerate
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private DataTable ReportGenerate(IEnumerable<VehicleAlertReport> list)
        {
            using (var dt = new DataTable())
            {
                var customerName = Session.GetCustomerInfo().DecryptedName;
                var dates = HttpContext.Current.Session["RangoFechas"].ToString();

                dt.Columns.Add("Name");
                dt.Columns.Add("Date", typeof(DateTime));
                dt.Columns.Add("Address");
                dt.Columns.Add("ReportId");
                dt.Columns.Add("Description");
                dt.Columns.Add("Latitud", typeof(double));
                dt.Columns.Add("Longitud", typeof(double));
                dt.Columns.Add("DecryptDriverName");
                dt.Columns.Add("CostCenterId");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("VehicleGroupName");

                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Dates");

                foreach (var item in list)
                {
                    var dataRow = dt.NewRow();

                    dataRow["Name"] = item.Name;
                    dataRow["Date"] = item.Date;
                    dataRow["Address"] = item.Address;
                    dataRow["ReportId"] = item.ReportId;
                    dataRow["Description"] = item.Description;
                    dataRow["Latitud"] = item.Latitude;
                    dataRow["Longitud"] = item.Longitude;
                    dataRow["DecryptDriverName"] = item.DecryptDriverName;
                    dataRow["CostCenterId"] = item.CostCenterId;
                    dataRow["CostCenterName"] = item.CostCenterName;
                    dataRow["VehicleGroupName"] = item.VehicleGroupName;

                    dataRow["CustomerName"] = customerName;
                    dataRow["Dates"] = dates;

                    dt.Rows.Add(dataRow);
                }
                return dt;
            }
        }


        /// <summary>
        /// GC Method
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }

}
