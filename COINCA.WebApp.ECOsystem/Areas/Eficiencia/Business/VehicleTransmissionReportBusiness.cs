﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Web;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;
using ECO_Maps.Business.Intrack;
using Newtonsoft.Json;

namespace ECO_Efficiency.Business
{
    public class VehicleTransmissionReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Transactions Sap
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public VehicleTransmissionReportBase RetrieveVehicleTransmissionReport()
        {
            var result = new VehicleTransmissionReportBase();
            result.List = (List<VehicleTransmissionReportModel>)HttpContext.Current.Session["VehicleTransmissionData"];
            result.Menus = new List<AccountMenus>();
            return result;
        }

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="result"></param>
        /// <param name="parameters"></param>
        public IEnumerable<VehicleTransmissionReportModel> GetReportData(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var list = dba.ExecuteReader<VehicleTransmissionReportModel>("[Efficiency].[SP_VehicleTransmission_Report]",
                new
                {
                    VehicleId = parameters.FilterVehicle,
                    parameters.StartDate,
                    parameters.EndDate
                });
                HttpContext.Current.Session["VehicleTransmissionData"] = list;
                return list;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            var result = new VehicleTransmissionReportBase { List = GetReportData(parameters) };
            return ReportGenerate(result.List, "");
        }

        /// <summary>
        /// ReportGenerate
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private DataTable ReportGenerate(IEnumerable<VehicleTransmissionReportModel> list, string tableName)
        {
            using (var dt = new DataTable(tableName))
            {
                var customerName = Session.GetCustomerInfo().DecryptedName;
                var dates = HttpContext.Current.Session["RangoFechas"].ToString();
                var plateId = HttpContext.Current.Session["PlateId"].ToString();
                dt.Columns.Add("ReportDate");
                dt.Columns.Add("Latitude");
                dt.Columns.Add("Longitude");
                dt.Columns.Add("IsVehicleOn");
                dt.Columns.Add("ReportSpeed");
                dt.Columns.Add("Location");
                dt.Columns.Add("Distance");
                dt.Columns.Add("EventDetail");
                dt.Columns.Add("BatteryStatus");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Dates");
                dt.Columns.Add("PlateId");
                foreach (var item in list)
                {
                    var drGPS = dt.NewRow();
                            
                    drGPS["ReportDate"] = Convert.ToDateTime(item.ReportDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                    drGPS["Latitude"] = item.Latitude;
                    drGPS["Longitude"] = item.Longitude;
                    drGPS["IsVehicleOn"] = item.IsVehicleOn;
                    drGPS["Distance"] = item.ReportDistance.ToString("0.00");
                    drGPS["ReportSpeed"] = Convert.ToInt32(item.ReportSpeed);
                    drGPS["Location"] = item.Location;
                    drGPS["EventDetail"] = item.EventDetail;
                    drGPS["BatteryStatus"] = item.BatteryStatus;

                    drGPS["CustomerName"] = customerName;
                    drGPS["Dates"] = dates;
                    drGPS["PlateId"] = plateId;
                    dt.Rows.Add(drGPS);
                }

                return dt;
            }
        }

        /// <summary>
        /// GC Method
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}