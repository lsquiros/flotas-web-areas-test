﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECO_Efficiency.Models;
using ECOsystem.Models.Core;
using System.Xml;
using System.Xml.Linq;

namespace ECO_Efficiency.Business.Utilities
{
    /// <summary>
    /// General Collections Class Contains all methods for load Drop Down List
    /// </summary>
    public class GeneralCollections
    {

        /// <summary>
        /// Return a list of GeoFences in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetGeoFences
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new GeoFencesBusiness())
                {
                    var result = business.RetrieveGeoFences(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.GeoFenceId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Routes in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetRoutesByVehicle
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new RoutesBusiness())
                {
                    var result = business.RetrieveRoutes(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.RouteId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetMonthNames
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year - 5; i < year + 9; i++)
                {
                    dictionary.Add(i, i);
                }
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetReportCriteria
        {
            get
            {
                return new SelectList(new Dictionary<int, string> { 
                    {(int)ECO_Efficiency.Models.ReportCriteria.Period,"Por Período"},
                    {(int)ECO_Efficiency.Models.ReportCriteria.DateRange,"Rango de Fechas"}
                }, "Key", "Value", "");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesByCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                IEnumerable<Vehicles> result = RetrieveVehicles(null);
                foreach (var r in result)
                {
                    if (!dictionary.ContainsKey(r.VehicleId))
                    {
                        dictionary.Add(r.VehicleId, "{'PlateId':'" + r.PlateId + "', 'Name':'" + r.Name + "'}");
                    }
                }

                return new SelectList(dictionary, "Key", "Value");
            }
        }


        public static SelectList GetVehicleCostCenters
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var result = new VehicleGPSSummaryBusiness().RetrieveVehicleCostCenters(null, true);
                dictionary.Add(-1, "Todos");
                foreach (var r in result)
                {
                    dictionary.Add(r.CostCenterId, r.Name);
                }

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of filters for the OverSpeed Report
        /// </summary>
        public static SelectList GetFilterOverSpeedTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                dictionary.Add(1, "{'Name':'Velocidad Delimitada por la empresa'}");
                dictionary.Add(2, "{'Name':'Velocidad de Ley'}");

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static IEnumerable<Vehicles> RetrieveVehicles(int? vehicleId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = Session.GetUserInfo();

                return dba.ExecuteReader<Vehicles>("[General].[Sp_Vehicles_Retrieve]",
                new
                {
                    VehicleId = vehicleId,
                    CustomerId = Session.GetCustomerId(),
                    Key = key,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetSeeRouteType
        {
            get
            {
                return new SelectList(new Dictionary<int, string> {                     
                    {1, "Último Hora"},
                    {2, "Último Día"},
                    {3, "Última Semana"},
                    {4, "Últimas 2 Semanas"},
                    {5, "Último Mes"}
                }, "Key", "Value", "");
            }
        }

        public static SelectList GetGeoFenceTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var dba = new DataBaseAccess())
                {
                    var user = Session.GetUserInfo();

                    var model = dba.ExecuteReader<GeoFenceTypes>("[General].[Sp_GeoFenceTypes_Retrieve]", new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    });

                    foreach (GeoFenceTypes type in model)
                    {
                        dictionary.Add(type.Id, type.Name);
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }

        public static SelectList GetGeoFence
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var dba = new DataBaseAccess())
                {
                    var user = Session.GetUserInfo();

                    var model = dba.ExecuteReader<GeoFences>("[Efficiency].[Sp_GeoFences_Retrieve]", new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    });
                    
                    dictionary.Add(-1, "Todos");
                    foreach (GeoFences item in model)
                    {
                        dictionary.Add(item.GeoFenceId, item.Name);
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }

        public static SelectList GetDrivers
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var dba = new DataBaseAccess())
                {

                    var result2 = dba.ExecuteReader<Users>("[General].[Sp_Drivers_Retrieve]",
                        new
                        {
                            //UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId,
                            CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                        });

                    foreach (Users user in result2)
                    {
                        dictionary.Add(user.DriversUserId, user.DecryptedName);
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }

        public static string SetTemperatureAlert(int? ReportId)
        {
            switch (ReportId)
            {
                case 31:
                    return "HighTemperature";
                case 32:
                    return "LowTemperature";
                default:
                    return string.Empty;
            }
        }

        public static List<string> GetNodesXMLSensors(string xmlSensors)
        {
            var xml = XDocument.Parse(string.Format("<root>{0}</root>", xmlSensors));
            var rows = from node in xml.Descendants("row")
                       select new
                       {
                           Temperature = node.Attribute("Temperature").Value
                       };
            var list = new List<string>();
            foreach (var item in rows)
            {
                list.Add(item.Temperature);
            }
            return list;
        }
    }
}