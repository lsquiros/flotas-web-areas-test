﻿/************************************************************************************************************
*  File    : HobbsMeterPerformanceReportController.cs
*  Summary : Real Vs Budget Fuels Report Controller Actions
*  Author  : Berman Romero
*  Date    : 20/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Mvc;

namespace ECO_Control.Controllers.Control
{
    /// <summary>
    /// Current Fuels Report Class
    /// </summary>
    public class HobbsMeterPerformanceReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new HobbsMeterPerformanceReportBusiness())
                {
                    return View(business.RetrieveHobbsMeterPerformanceReport(new HobbsMeterPerformanceParameters()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(HobbsMeterPerformanceReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new HobbsMeterPerformanceReportBusiness())
                {
                    return View(business.RetrieveHobbsMeterPerformanceReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<HobbsMeterPerformanceParameters>(Miscellaneous.Base64Decode(p));

                model.Dates = Convert.ToDateTime(model.StartDate).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(model.EndDate).ToString("dd/MM/yyyy");

                using (var business = new HobbsMeterPerformanceReportBusiness())
                {
                    DataTable report = business.GetReportDownloadData(model);

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Horímetros (GPS)");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "HobbsMeterPerformanceReport", "Efficiency", "Reporte Horímetros (GPS)");
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<HobbsMeterPerformanceParameters>(Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new HobbsMeterPerformanceReportBusiness())
            {
                return View("Index", business.RetrieveHobbsMeterPerformanceReport(model));
            }
        }
    }
}