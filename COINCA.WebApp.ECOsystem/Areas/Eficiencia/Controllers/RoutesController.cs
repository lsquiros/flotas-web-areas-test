﻿/************************************************************************************************************
*  File    : RoutesController.cs
*  Summary : Routes Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 10/27/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;
using Microsoft.VisualBasic.FileIO;
using System.Web.Script.Serialization;




namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// Routes Controller. Implementation of all action results from views
    /// </summary>
    public class RoutesController : Controller
    {
        /// <summary>
        /// Routes Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index(int? routeId)
        {
            try
            {
                using (var business = new RoutesBusiness())
                {
                    var model = new RouteBase
                    {
                        Data = new Routes() { Points = new List<RoutesPoints>() },
                        List = business.RetrieveRoutes(null)
                    };

                    ViewBag.ErrorDuringProcess = TempData["ErrorDuringProcess"] ?? 0;
                    ViewBag.ErrorMessage = Session["ErrorMessage"] ?? string.Empty;
                    ViewBag.ResultData = Session["ResultData"] ?? string.Empty;
                    if (routeId != null && routeId > 0)
                        model.JavascriptToRun = string.Format("ECO_Efficiency.Routes.LoadMapRouteFromReconstructing({0})", routeId);
                    else
                        model.JavascriptToRun = string.Empty;
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity GeoFences
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult AddOrEditRoutes(Routes model)
        {
            try
            {
                using (var business = new RoutesBusiness())
                {
                    var routeId = business.AddOrEditRoutes(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se almacena/modifica la ruta: {0}. Detalle: {1}", routeId, new JavaScriptSerializer().Serialize(model)));
                    var route = business.RetrieveRoutes(null).Where(x => x.RouteId == routeId).FirstOrDefault();
                    route.MapUrl = MapUrl(routeId.ToString());
                    return Json(route, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, "Error al almacenar ruta:" + e.Message);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Routes")
                });
            }
        }

        /// <summary>
        /// Clean the data for the right Panel 
        /// </summary>
        /// <returns></returns>
        public PartialViewResult CleanData()
        {
            var model = new Routes();
            return PartialView("_partials/_Detail", model);
        }

        /// <summary>
        /// Search GeoFences
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchRoutes(string key)
        {
            try
            {
                using (var business = new RoutesBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveRoutes(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Routes")
                });
            }
        }

        /// <summary>
        /// Load GeoFences
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadRoutes(int id)
        {
            try
            {
                if (id < 0)
                {
                    return PartialView("_partials/_RouteDetail", new Routes() { Points = new List<RoutesPoints>() });
                }
                using (var business = new RoutesBusiness())
                {
                    var result = business.RetrieveRoutes(id).FirstOrDefault();
                    result.Points = business.RetrieveRoutesDetail(id).Points;
                    return PartialView("_partials/_RouteDetail", result);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "GeoFences")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model GeoFences
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteRoutes(int id)
        {
            try
            {
                using (var business = new RoutesBusiness())
                {
                    business.DeleteRoutes(id);
                    return PartialView("_partials/_Grid", business.RetrieveRoutes(null));
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Routes")
                    });
                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Routes")
                });
            }

        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Routes
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pSegmentsList"></param>
        /// <returns></returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult SaveRouteDetail(string pId, RoutesSegmentsBase pSegmentsList)
        {
            try
            {
                using (var business = new RoutesBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditRoutesDetail(pSegmentsList, Convert.ToInt16(pId));
                    }
                    return PartialView("_partials/_Grid", business.RetrieveRoutes(null));
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Routes")
                });
            }
            //Models.Maps.RoutesSegmentsBase SegmentsList = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Models.Maps.RoutesSegmentsBase>(pSegmentsList);
        }

        /// <summary>
        /// Get the URL 
        /// </summary>
        /// <param name="pRouteId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetMapUrl(string pRouteId)
        {
            try
            {
                return Json(MapUrl(pRouteId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        string MapUrl(string pRouteId)
        {
            try
            {
                string countryCode;
                using (var business = new CountryCodeBusiness())
                {
                    countryCode = business.RetrieveCountryCode().FirstOrDefault().countryCode;
                }
                return "Params=" + countryCode + "|" + pRouteId + "|" + ECOsystem.Utilities.Session.GetUserInfo().UserId + "|" + ECOsystem.Utilities.Session.GetCustomerId() + "|" + 0.ToTimeOffSet();
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return null;
            }
        }

        /// <summary>
        /// Download a Copy of the example file for the routes import
        /// </summary>
        /// <returns></returns>
        public ActionResult DonwloadExampleFile()
        {
            try
            {
                byte[] SummaryFile = System.IO.File.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data/Importacion_Rutas_Ejemplo.csv")));

                return new FileStreamResult(new MemoryStream(SummaryFile), "text/csv")
                {
                    FileDownloadName = "Importacion_Rutas_Ejemplo.csv"
                };
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        //[EcoAuthorize]
        [HttpPost]
        public ActionResult AddOrEditImportRoutes(bool chkOverride)
        {
            int routeId = 0;
            int resultCode = 2;
            int RouteExist = 0;
            int RouteFail = 0;
            int RouteSuccess = 0;
            int ErrorCount = 0;

            var List = new List<RoutesErrors>();

            try
            {
                HttpPostedFileBase file = Request.Files[0];
                // Verify that the user selected a file
                if (file != null && file.ContentLength > 0)
                {
                    // extract only the filename
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.GetTempPath() + fileName;
                    file.SaveAs(path);


                    using (TextFieldParser csvReader = new TextFieldParser(path))
                    {
                        #region Read the file
                        csvReader.SetDelimiters(",");
                        csvReader.HasFieldsEnclosedInQuotes = true;
                        int countColumns = 0;
                        int order = 1;
                        int lineNumber = 1;

                        RoutesSegmentsBase pList = new RoutesSegmentsBase();
                        List<RoutesSegmentsBase> pointList = new List<RoutesSegmentsBase>();

                        string tempRouteName = null;

                        //Go line by line
                        while (!csvReader.EndOfData)
                        {
                            RoutesPoints p = new RoutesPoints();

                            string[] colFields = csvReader.ReadFields();
                            string[] columnsToModel = new string[12];

                            //Go through all the column in each line   
                            foreach (var column in colFields.Where(x => x != ""))
                            {
                                if (column != "RUTA" && !column.Contains("Fecha") && column != "Conductor" && column != "M�vil")
                                {
                                    columnsToModel[countColumns] = column;
                                    countColumns++;
                                }
                                else
                                {
                                    break;
                                }
                            }

                            //Verify if the route is different to the route saving before
                            if (tempRouteName != columnsToModel[0] && tempRouteName != null)
                            {
                                pointList.Add(pList);
                                pList = new RoutesSegmentsBase();
                                order = 1;
                            }

                            //Add the results from the row in the model
                            if (columnsToModel[0] != null)
                            {
                                countColumns = 0;
                                p.Time = "00:00";
                                pList.RouteName = columnsToModel[0];
                                p.Order = order;
                                p.PointReference = order; //Convert.ToInt32(ColumnsToModel[1]);
                                p.Latitude = columnsToModel[9];
                                p.Longitude = columnsToModel[10];
                                p.StopTime = 0;
                                p.Name = columnsToModel[4];
                                p.LineNumber = lineNumber;

                                pList.Points.Add(p);
                                order++;
                            }

                            tempRouteName = pList.RouteName;
                            lineNumber++;
                        }

                        //Add the last route got from the file
                        pointList.Add(pList);
                        #endregion

                        //Will be use to show where there are error in the file 
                        #region Save the routes
                        using (var business = new RoutesBusiness())
                        {
                            //Go for each one of the routes saved
                            foreach (var item in pointList)
                            {
                                var lineNumbers = string.Empty;

                                try
                                {
                                    //Verifies if the route already exists in the database
                                    int? overId = business.GetLastRouteId(item.RouteName);

                                    //If the override is false and the route already exists, it will raise an error 
                                    if (!chkOverride)
                                    {
                                        if (overId != 0)
                                        {
                                            throw new Exception("1");
                                        }
                                    }

                                    //Check if there are lat or long in blank
                                    if (item.Points.Any(x => x.Latitude == null || x.Longitude == null))
                                    {
                                        //If there are get the line where they are
                                        foreach (var ln in item.Points.Where(x => x.Latitude == null || x.Longitude == null).Select(x => new { x.LineNumber }).ToList())
                                        {
                                            lineNumbers = lineNumbers == string.Empty ? ln.LineNumber.ToString() : lineNumbers + " - " + ln.LineNumber;
                                        }

                                        throw new Exception("2");
                                    }

                                    //Save the routes in the database
                                    routeId = business.AddRoutesFromImport(item.Points, item.RouteName);

                                    //If the route Id is 0 returns an error
                                    if (routeId == 0)
                                    {
                                        throw new Exception("2");
                                    }

                                    RouteSuccess++;
                                    Session["ErrorMessage"] = Session["ErrorMessage"] == null ? "Ruta: " + item.RouteName + " - Exitoso - " + (overId == 0 ? "Nueva" : "Actualizada") :
                                                                Session["ErrorMessage"] + "|Ruta: " + item.RouteName + " - Exitoso - " + (overId == 0 ? "Nueva" : "Actualizada");
                                }
                                catch (Exception e)
                                {
                                    RoutesErrors ErrorModel = new RoutesErrors();
                                    ErrorCount++;

                                    if (e.Message == "1")
                                    {
                                        resultCode = 3;
                                        RouteExist++;
                                        Session["ErrorMessage"] = Session["ErrorMessage"] == null
                                            ? "Ruta: " + item.RouteName +
                                              " - Fallido - Ruta ya Existe (sobreescribir no esta seleccionado)"
                                            : Session["ErrorMessage"] + "|Ruta: " + item.RouteName +
                                              " - Fallido - Ruta ya Existe (sobreescribir no esta seleccionado)";

                                        ErrorModel.Item = ErrorCount + " ) " + "Ruta: " + item.RouteName + " - Fallido - Ruta ya Existe (sobreescribir no esta seleccionado)";
                                    }
                                    else if (e.Message == "2")
                                    {
                                        resultCode = 3;
                                        RouteFail++;
                                        Session["ErrorMessage"] = Session["ErrorMessage"] == null
                                            ? "Ruta: " + item.RouteName + " - Fallido - Linea: " + lineNumbers
                                            : Session["ErrorMessage"] + "|Ruta: " + item.RouteName +
                                              " - Fallido - Linea: " + lineNumbers;
                                        ErrorModel.Item = ErrorCount + " ) " + "Ruta: " + item.RouteName + " - No se agregó porque el formato del registro está incorrecto - Linea: " + lineNumbers;

                                    }
                                    List.Add(ErrorModel);
                                }
                            }
                        }//
                        using (var business = new RoutesBusiness())
                        {
                            Session["SummaryFile"] = business.CreateTxtFormat(List);
                            Session["Path"] = path;
                        }
                        #endregion
                    }
                }

                #region Event log
                new EventLogBusiness().AddLogEvent(LogState.INFO, "Importación de ruta: " + routeId + ". Usuario: " + ECOsystem.Utilities.Session.GetUserInfo().UserId);
                #endregion

                TempData["ErrorDuringProcess"] = resultCode;
                Session["ResultData"] = string.Format("{0}|{1}|{2}|{3}|{4}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(RouteSuccess),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(RouteExist),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(RouteFail),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(RouteSuccess + RouteExist + RouteFail));

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e, true);

                Session["ErrorMessage"] = "Sucedió un error durante la Importación de Rutas, por favor inténtelo nuevamente.";

                TempData["ErrorDuringProcess"] = 1;

                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public ActionResult DonwloadReportFile()
        {
            try
            {
                byte[] SummaryFile = null;
                SummaryFile = (byte[])Session["SummaryFile"];
                Session["SummaryFile"] = null;
                string path = Session["Path"].ToString();
                Session["Path"] = null;
                System.IO.File.Delete(path);
                var CustomerInfo = ECOsystem.Utilities.Session.GetCustomerInfo();
                DateTime Today = DateTime.Today.Date;
                return new FileStreamResult(new MemoryStream(SummaryFile), "text/plain")
                {
                    FileDownloadName = CustomerInfo.DecryptedName + "- Reporte de Importación Rutas - " + Today + ".txt"
                };
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowPointsAddOrEdit(RoutesPoints model)
        {
            try
            {
                using (var bus = new RoutesBusiness())
                {
                    var nMd = bus.RetrievePointDetail(null, model.PointId);
                    if (nMd == null)
                    {
                        model.IsCommerce = 0;
                        model.CommerceName = string.Empty;
                        model.CommerceId = 0;
                    }
                    else
                    {
                        model.IsCommerce = nMd.IsCommerce;
                        model.CommerceName = nMd.CommerceName;
                        model.CommerceId = nMd.CommerceId;
                    }
                }
                return PartialView("_partials/_PointsAddOrEdit", model);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditPointRoutes(RoutesPoints model)
        {
            try
            {
                using (var bus = new RoutesBusiness())
                {
                    var ser = bus.GetSerializedRouteModel(model);
                    model.SerializedObject = ser;
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Erro al almacena/modifica el punto de la ruta: {0}. Detalle: {1}", model.RouteId, e.Message));
                return View("Index");
            }
        }
    }
}


