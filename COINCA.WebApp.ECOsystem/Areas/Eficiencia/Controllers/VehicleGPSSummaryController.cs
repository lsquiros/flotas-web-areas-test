﻿using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

/************************************************************************************************************
*  File    : VehicleGPSSummaryController.cs
*  Summary : VehicleGPSSummary Actions
*  Author  : Stefano Quirós 
*  Date    : 28/Febrero/2017
* 
*  Copyright 2017 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company. 
************************************************************************************************************/

namespace ECO_Efficiency.Controllers.Efficiency
{
    public class VehicleGPSSummaryController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                return View(new VehicleGPSSummaryBase() { Menus = new List<AccountMenus>() });
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(VehicleGPSSummaryBase model)
        {
            try
            {
                Session["RangoFechas"] = Convert.ToDateTime(model.Parameters.StartDate).ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + Convert.ToDateTime(model.Parameters.EndDate).ToString("dd/MM/yyyy hh:mm:ss tt");

                using (var business = new VehicleGPSSummaryBusiness())
                {
                    Session["Reporte"] = business.GetReportDownloadData(model.Parameters);
                    return Json(((DataTable)Session["Reporte"]).Rows.Count, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Resumen de Eventos de GPS");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "VehicleGPSSummaryReport", this.ToString().Split('.')[2], "Reporte Resumen de Eventos de GPS");
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return Redirect("Index");
            }
        }
    }
}