﻿/************************************************************************************************************
*  File    : RealTimeCostCenterController.cs
*  Summary : RealTimeCostCenter Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 01/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;



using ECOsystem.Business.Utilities;

namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// RealTimeCostCenter Controller. Implementation of all action results from views
    /// </summary>
    public class RealTimeCostCenterController : Controller
    {

        /// <summary>
        /// RealTimeCostCenter Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new RealTimeCostCenterBusiness())
                {
                    var model = new RealTimeCostCenterBase
                    {
                        Data = new RealTimeCostCenter(),
                        List = business.RetrieveRealTimeCostCenter(null)
                    };
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Search RealTimeCostCenter
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchRealTimeCostCenter(string key)
        {
            try
            {
                using (var business = new RealTimeCostCenterBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveRealTimeCostCenter(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "RealTimeCostCenter")
                });
            }
        }

        /// <summary>
        /// Load RealTimeCostCenter
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadRealTimeCostCenter(int id)
        {
            try
            {
                using (var business = new RealTimeCostCenterBusiness())
                {
                    RealTimeCostCenter model = business.RetrieveRealTimeCostCenter(id).FirstOrDefault();
                    if (model != null)
                    {
                        using (var businessParameters = new ParametersBusiness())
                        {
                            model.TransmissionTime = Convert.ToInt32(businessParameters.RetrieveParameters().FirstOrDefault().TransmissionTime) * -1;
                        }
                    }
                    return PartialView("_partials/_Detail", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "RealTimeCostCenter")
                });
            }
        }

    }
}