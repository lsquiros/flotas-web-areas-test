﻿using ECO_Efficiency.Models;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ECO_Efficiency.Business;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using System.Data;
using Newtonsoft.Json;




namespace ECO_Efficiency.Controllers.Efficiency
{
    public class VehicleAlertReportController : Controller
    {
        // GET: VehicleAlarmReport
        public ActionResult Index()
        {
            try
            {
                var model = new VehicleAlertReportModelBase
                {
                    Menus = new List<AccountMenus>()
                };

                return View(model);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(VehicleAlertReportModelBase model)
        {
            try
            {
                Session["RangoFechas"] = Convert.ToDateTime(model.Parameters.StartDate).ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + Convert.ToDateTime(model.Parameters.EndDate).ToString("dd/MM/yyyy hh:mm:ss tt");

                using (var business = new VehicleAlertReportBusiness())
                {
                    Session["Reporte"] = business.GetReportDownloadData(model.Parameters);
                    return Json(((DataTable)Session["Reporte"]).Rows.Count, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Resumen Eventos de GPS");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "VehicleAlertReport", this.ToString().Split('.')[2], "Reporte Alerta de GPS");
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return Redirect("Index");
            }
        }
    }
}