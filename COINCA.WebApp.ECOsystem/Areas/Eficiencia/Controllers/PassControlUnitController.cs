﻿/************************************************************************************************************
*  File    : PassControlUnitController.cs
*  Summary : PassControlUnit Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 11/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;



using ECOsystem.Business.Utilities;

namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// PassControlUnit Controller. Implementation of all action results from views
    /// </summary>
    public class PassControlUnitController : Controller
    {
        /// <summary>
        /// PassControlVehicle Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new PassControlUnitBusiness())
                {
                    var model = new PassControlUnitBase
                    {
                        Data = new PassControlUnit(),
                        List = business.RetrieveVehicleUnits(null)
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Search Vehicles
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchUnits(string key)
        {
            try
            {
                using (var business = new PassControlUnitBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveVehicleUnits(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "PassControlUnit")
                });
            }

        }




        /// <summary>
        /// Load PassControlVehicle
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadPassControlUnits(int id)
        {
            try
            {
                using (var business = new PassControlUnitBusiness())
                {
                    PassControlUnit model = business.RetrieveIntrackReference(id).FirstOrDefault();
                    if (model != null)
                    {
                        model.UnitId = id;
                        model.CustomerId = Convert.ToInt16(ECOsystem.Utilities.Session.GetCustomerId());

                        return PartialView("_partials/_Detail", model);
                    }
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError
                    {
                        TitleError = "Unidad sin Vehículos Asignados",
                        TechnicalError = "Esta unidad no tiene vehículos asignados.",
                        ReturnUlr = Url.Action("Index", "PassControlUnit")
                    });
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "PassControlUnit")
                });
            }
        }
    }
}