﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;
using Newtonsoft.Json;
using ECOsystem.Utilities;
using System.Web;
using System.Data;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECO_Efficiency.Utilities;




namespace ECO_Efficiency.Controllers.Efficiency
{

    /// <summary>
    /// Continuos Monitoring Class
    /// </summary>
    public class ContinuosMonitoringController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
				#region Validates Token Info
                var user = ECOsystem.Utilities.Session.GetUserInfo();
				if (string.IsNullOrEmpty(user.MapsToken)) MapTokenUtilities.ApiLoginAsync(user);
				#endregion

				return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Index Post
        /// </summary>
        /// <returns></returns>
        //public ActionResult VehicleDetailsPanel(string VehicleIds)
        public ActionResult VehicleDetailsPanel(string VehicleIds)
        {
            try
            {
                var model = IndexRetrieve();

                model.VehicleIdsString = VehicleIds;

                var list = new List<VehiclesMonitoring>();

                foreach (var item in VehicleIds.Split(','))
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        list.Add(model.VehicleList.Where(x => x.VehicleId == Convert.ToInt32(item)).FirstOrDefault());
                    }
                }
                return PartialView("_Partials/_VehicleDetailsPanel", list);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// 
        /// </summary>
        /// <returns></returns>
        private ContinuosMonitoringModel IndexRetrieve()
        {
            try
            {
                var model = new ContinuosMonitoringModel();

                using (var bus = new ContinuosMonitoringBusiness())
                {
                    model.VehicleList = bus.RetrieveVehiclesMonitoring();
                }

                using (var bus = new CountryCodeBusiness())
                {
                    var country = bus.RetrieveCountryCode().FirstOrDefault();
                    model.UserTimeZone = country.TimeZone;
                    model.CountryCode = country.countryCode;
                }

                using (var business = new ParametersBusiness())
                {
                    model.TransmissionTime = Convert.ToInt32(business.RetrieveParameters().FirstOrDefault().TransmissionTime);
                }

                model.List = new List<SelectListItem>();
                model.VehicleTemperatureList = new List<VehicleTemperatureResume>();

                return model;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="VehicleId"></param>
        /// <returns></returns>
        public ActionResult LoadTemperaturePartial(string PositionList, DateTime StartDate, int VehicleId)
        {
            try
            {
                var model = new ContinuosMonitoringModel();
                var list = new List<VehicleTemperatureResume>();
                var position = 0;
                Session["StartDate"] = StartDate;


                if (StartDate.ToString("MM/dd/yyyy") == DateTime.Today.ToString("MM/dd/yyyy"))
                {
                    model.VehicleTemperatureList = (List<VehicleTemperatureResume>)Session["VehicleTemperatureList"];
                }
                else
                {
                    using (var bus = new ContinuosMonitoringBusiness())
                    {
                        model.VehicleTemperatureList = bus.LoadVehicleTemperature(VehicleId, DateTime.Parse(StartDate.ToString("MM/dd/yyyy"))).VehicleTemperatureList;
                    }
                }

                if (PositionList == "")
                {
                    model.SensorCount = model.VehicleTemperatureList.Select(x => x.SensorId).Distinct().ToList().Count();

                    for (int i = 1; i <= model.SensorCount; i++)
                    {
                        if (PositionList == "")
                        {
                            PositionList += i;
                        }
                        else
                        {
                            PositionList += "|" + i;
                        }

                    }
                }

                if (PositionList != "")
                {
                    for (int i = 0; i < PositionList.Split('|').Count(); i++)
                    {
                        position = Convert.ToInt32(PositionList.Split('|')[i]);
                        list.AddRange(model.VehicleTemperatureList.Where(x => x.SensorId == position));//.ToList();                 
                    }

                    IList<IList<decimal?>> data = list.Select(x => x.SensorId).Distinct().ToList().Select(v => list.Where(x => (x.SensorId).Equals(v)).Select(x => x.Temperature as decimal?).ToList()).Cast<IList<decimal?>>().ToList();
                    IList<IList<string>> count = list.Select(x => x.SensorId).Distinct().ToList().Select(v => model.VehicleTemperatureList.Where(x => (x.SensorId).Equals(v))
                                                                                                 .Select(x => x.Date).ToList()).Cast<IList<string>>().ToList();

                    List<string> labels = new List<string>();
                    model.SensorCount = list.Select(x => x.SensorId).Distinct().ToList().Count();
                    for (int i = 0; i < count.Count(); i++)
                    {
                        if (labels.Count() < count[i].Count())
                        {
                            labels = count[i].ToList();
                        }
                    }

                    model.HourCount = labels.Count();
                    model.Labels = new HtmlString(JsonConvert.SerializeObject(labels));
                    model.Titles = new HtmlString(JsonConvert.SerializeObject(list.Select(x => new { x.SensorId }).Distinct().OrderByDescending(x => x.SensorId).Select(x => "Sensor " + x.SensorId).ToList()));
                    model.Data = new HtmlString(JsonConvert.SerializeObject(data));

                    model.Colors = new HtmlString(JsonConvert.SerializeObject(Miscellaneous.GenerateColors().Take(model.SensorCount)));
                    model.AlphaColors = new HtmlString(JsonConvert.SerializeObject(Miscellaneous.GenerateAlphaColors().Take(model.SensorCount)));
                    model.HighlightColors = new HtmlString(JsonConvert.SerializeObject(Miscellaneous.GenerateHighlighColors().Take(model.SensorCount)));
                    model.VehicleTemperatureList = list;

                    return PartialView("_Partials/_TemperatureChart", model);
                }
                else
                {
                    return PartialView("_Partials/_TemperatureChart", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult LoadTemperatureGrid()
        {
            try
            {
                return PartialView("_partials/_TemperatureGrid", (List<VehicleTemperatureResume>)Session["VehicleTemperatureList"]);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="VehicleId"></param>
        /// <returns></returns>
        public ActionResult LoadTemperatureChart(int VehicleId, DateTime? Date)
        {
            try
            {
                var model = new ContinuosMonitoringModel();
                using (var business = new ContinuosMonitoringBusiness())
                {
                    model = business.LoadVehicleTemperature(VehicleId, Date);
                }
                model.VehicleIdTemp = VehicleId;
                // return to the client the url to redirect to
                return View("TemperatureChart", model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            try
            {
                DataTable report = new DataTable();

                using (var business = new ContinuosMonitoringBusiness())
                {
                    List<VehicleTemperatureResume> dataContext = (List<VehicleTemperatureResume>)Session["VehicleTemperatureList"];
                    report = business.ReportGenerate(dataContext);
                    Session["Reporte"] = report;
                }
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Reporte de Sensores de Temperatura");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "GPSTemperature", this.ToString().Split('.')[2], "Reporte de Sensores de Temperatura");
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }

        [HttpPost]
        public bool submit()
        {
            return true;
        }

        public ActionResult GetListFromXML(string xml)
        {
            try
            {
                return Json(string.IsNullOrEmpty(xml) ? new List<string>() : ECO_Efficiency.Business.Utilities.GeneralCollections.GetNodesXMLSensors(xml), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LogAlarmPanicButton(int VehicleId)
        {
            try
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se ha aceptado la arleta de botón de pánico, para el vehículo: {0}", VehicleId));
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        #region Commands
        public ActionResult GetAvailableCommands(int VehicleId)
        {
            try
            {
                using (var bus = new LineVitaCommandsBusiness())
                {
                    return PartialView("_Partials/_CommandsDetail", GetCommandForVehicle(VehicleId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al obtener los comandos para el vehículo, detalle: {0}, error: {1}", VehicleId, e.Message));
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SendCommand(int VehicleId, string command)
        {
            try
            {
                using (var bus = new LineVitaCommandsBusiness())
                {
                    Session["VehicleId"] = VehicleId;
                    Session["CommandSent"] = command;
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se envia comando al vehículo: {0}, comando: {1}", VehicleId, command));
                    var result = bus.PostCommand(VehicleId, Session["LineVitaUrl"].ToString(), command);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al ejecutar comando. Vehículo: {0}, comando: {1}, error: {2}", VehicleId, command, e.Message));
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetCommandStatus()
        {
            try
            {
                var vehicleId = Session["VehicleId"] != null ? Convert.ToInt16(Session["VehicleId"].ToString()) : 0;
                var command = Session["CommandSent"] != null ? Session["CommandSent"].ToString() : string.Empty;
                var model = GetCommandForVehicle(vehicleId);

                if (model != null && model.Status != "BLOCKED") new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Comando ejecutado. Vehículo: {0}, comando: {1}", vehicleId, command));

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        private VehicleCommands GetCommandForVehicle(int VehicleId)
        {
            try
            {
                using (var bus = new LineVitaCommandsBusiness())
                {
                    Session["LineVitaUrl"] = bus.LineVitaCommandURL();
                    return bus.GetCommandList(VehicleId, Session["LineVitaUrl"].ToString());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return null;
            }
        }
        #endregion
    }
}
