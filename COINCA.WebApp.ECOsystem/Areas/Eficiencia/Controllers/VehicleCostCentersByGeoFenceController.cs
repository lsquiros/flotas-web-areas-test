﻿/************************************************************************************************************
*  File    : VehicleCostCentersByGeoFenceController.cs
*  Summary : VehicleCostCentersByGeoFence Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 11/18/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;



using ECOsystem.Business.Utilities;

namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// VehicleCostCentersByGeoFence Controller. Implementation of all action results from views
    /// </summary>
    public class VehicleCostCentersByGeoFenceController : Controller
    {
        /// <summary>
        /// VehicleCostCentersByGeoFence Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                var model = new VehicleCostCentersByGeoFenceBase
                {
                    GeoFenceId = null,
                    Data = new VehicleCostCentersByGeoFenceData(),
                    Menus = new List<AccountMenus>()
                };
                return View(model);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleCostCentersByGroup
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehicleCostCentersByGeoFence(VehicleCostCentersByGeoFenceData model)
        {
            try
            {
                using (var business = new VehicleCostCentersByGeoFenceBusiness())
                {
                    business.AddOrEditVehicleCostCentersByGeoFence(model);
                    return PartialView("_partials/_List", business.RetrieveVehicleCostCentersByGeoFence(model.GeoFenceId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCostCentersByGeoFence")
                });
            }
        }


        /// <summary>
        /// Load VehicleCostCentersByGroup
        /// </summary>
        /// <param name="id">The vehicle group Id to find all VehicleCostCenters associated</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadVehicleCostCentersByGeoFence(int id)
        {
            try
            {
                using (var business = new VehicleCostCentersByGeoFenceBusiness())
                {
                    return PartialView("_partials/_List", business.RetrieveVehicleCostCentersByGeoFence(id));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCostCentersByGeoFence")
                });
            }
        }
    }
}

