﻿/************************************************************************************************************
*  File    : PassControlVehicleController.cs
*  Summary : PassControlVehicle Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 11/11/2014
*  Modif.By: Gerald Solano
*  Modif.Da: 18/08/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;
using ECO_Maps.Utilities;
using Newtonsoft.Json;
using ECOsystem.Utilities;
using System.Data;
using ECOsystem.Models.Core;
using ECO_Maps.Business.PassControl;
using System.IO;




namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// PassControlVehicle Controller. Implementation of all action results from views
    /// </summary>
    public class PassControlVehicleController : Controller
    {
        static PassControlVehicle report_Parameters;

        /// <summary>
        /// PassControlVehicle Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new PassControlVehicleBusiness())
                {
                    var model = new PassControlVehicleBase
                    {
                        Data = new PassControlVehicle(),
                        List = business.RetrieveVehicles(null)
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Search Vehicles
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchVehicles(string key)
        {
            try
            {
                using (var business = new PassControlVehicleBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "RealTime")
                });
            }
        }

        /// <summary>
        /// Load PassControl by Vehicle
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult LoadPassControlVehicle(int id)
        {

            try
            {
                using (var business = new PassControlVehicleBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveIntrackReference(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "RealTime")
                });
            }
        }

        /// <summary>
        /// Load PassControl by Group
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[EcoAuthorize]
        public PartialViewResult LoadPassControlGroup(int id)
        {
            try
            {
                using (var business = new PassControlGroupBusiness())
                {
                    return PartialView("_partials/_DetailGroup", business.RetrieveIntrackReference(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "RealTime")
                });
            }
        }

        /// <summary>
        /// Load PassControl by CostCenter
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[EcoAuthorize]
        public PartialViewResult LoadPassControlCostCenter(int id)
        {
            try
            {
                using (var business = new PassControlSubUnitBusiness())
                {
                    var model = business.RetrieveIntrackReference(id).FirstOrDefault();
                    model.CostCenterId = id;
                    return PartialView("_partials/_DetailCostCenter", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "RealTime")
                });
            }
        }

        /// <summary>
        /// Get Provincias from Mapas
        /// </summary>
        /// <param name="CountryCode"></param>
        /// <returns></returns>
        public ActionResult GetProvinciasFromMaps(string CountryCode)
        {
            try
            {
                List<SelectListItem> provinces = new List<SelectListItem>();
                using (var bus = new Business.Administration.GeographicDivisionBusiness())
                {
                    var custInfo = ECOsystem.Utilities.Session.GetCustomerInfo();
                    var results = bus.RetrieveStates(null, custInfo.CountryId);
                    foreach (var item in results)
                    {
                        SelectListItem prov = new SelectListItem
                        {
                            Text = item.Name,
                            Value = item.Name
                        };
                        provinces.Add(prov);
                    }
                    return Json(provinces, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Get Cantones from Mapas
        /// </summary>
        /// <param name="CountryCode"></param>
        /// <param name="Provincia"></param>
        /// <returns></returns>
        public ActionResult GetCantonesFromMaps(string CountryCode, string Provincia)
        {
            try
            {
                List<SelectListItem> cantons = new List<SelectListItem>();

                using (var bus = new Business.Administration.GeographicDivisionBusiness())
                {
                    var custInfo = ECOsystem.Utilities.Session.GetCustomerInfo();
                    var results = bus.RetrieveCounties(null, null, custInfo.CountryId);
                    foreach (var item in results)
                    {
                        SelectListItem prov = new SelectListItem
                        {
                            Text = item.Name,
                            Value = item.Name
                        };
                        cantons.Add(prov);
                    }
                    return Json(cantons, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [HttpPost]
        public ActionResult ExcelReportDownload(PassControlReport[] pReportData, bool pEventOn, bool pEventOff, string pStartDate, string pEndDate) //PassControlReportModel
        {
            try
            {
                Object report = null;
                Session["RangoFechas"] = string.Format("{0} - {1}", pStartDate, pEndDate);

                if (pEventOn || pEventOff)
                    Session["ActiveEvent"] = "1";
                else
                    Session["ActiveEvent"] = "0";

                using (var business = new PassControlVehicleBusiness())
                {
                    if (pEventOn || pEventOff)
                    {
                        report = new DataSet();
                        report = business.GetPassControlVehicleReportDownloadGenerateDataSet(pReportData);
                        Session["Reporte"] = report;
                        return Json(((DataSet)report).Tables[0].Rows.Count, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        report = new DataTable();
                        report = business.GetPassControlVehicleReportDownloadGenerateData(pReportData);
                        Session["Reporte"] = report;
                        return Json(((DataTable)report).Rows.Count, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Control de Paso Vehicular");

                    if (Session["ActiveEvent"].ToString() == "0")
                    {
                        DataTable report = new DataTable();
                        if (Session["Reporte"] != null)
                        {
                            report = (DataTable)Session["Reporte"];
                            Session["Reporte"] = null;
                        }

                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "PassControlVehicleResumeReport", this.ToString().Split('.')[2], "Control de Paso Vehicular");
                    }
                    else
                    {
                        DataSet report = new DataSet();
                        if (Session["Reporte"] != null)
                        {
                            report = (DataSet)Session["Reporte"];
                            Session["Reporte"] = null;
                        }

                        return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "PassControlVehicleReport", this.ToString().Split('.')[2], "Control de Paso Vehicular");
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Redirect("Index");

            }
        }
    }
}
