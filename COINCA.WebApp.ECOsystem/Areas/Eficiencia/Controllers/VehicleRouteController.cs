﻿/************************************************************************************************************
*  File    : VehicleRouteController.cs
*  Summary : VehicleRoute Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 11/03/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;
using Newtonsoft.Json;
using System.Data;
using ECOsystem.Utilities;
using ECOsystem.Business.Utilities;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;




namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// VehicleRoute Controller. Implementation of all action results from views
    /// </summary>
    public class VehicleRouteController : Controller
    {
        /// <summary>
        /// VehicleRoute Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new VehicleRouteBusiness())
                {
                    var model = new VehicleRouteBase
                    {
                        Data = new VehicleRoute(),
                        List = business.RetrieveVehicles(null).Where(x => x.HasGPS)
                    };
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Search Vehicles
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchVehicles(string key)
        {
            try
            {
                using (var business = new VehicleRouteBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "RealTime")
                });
            }
        }

        /// <summary>
        /// Load VehicleRoute
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadVehicleRoute(int id)
        {
            try
            {
                using (var business = new VehicleRouteBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveIntrackReference(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleRoute")
                });
            }
        }



        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(int vehicleId, string placa)
        {
            try
            {
                using (var business = new VehicleRouteBusiness())
                {
                    Session["Reporte"] = null;
                    Session["Reporte"] = business.ReportGenerate(vehicleId, placa);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataSet report = new DataSet();
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;
                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Reporte Cumplimiento de Ruta");
                        return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "VehicleRouteReport", this.ToString().Split('.')[2], "Reporte Cumplimiento de Ruta");
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                ViewBag.ErrorReport = 1;
                //return View("Index");
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "RealTime")
                });
            }
        }
    }
}

