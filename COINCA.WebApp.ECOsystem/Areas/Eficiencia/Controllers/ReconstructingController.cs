﻿/************************************************************************************************************
*  File    : ReconstructingController.cs
*  Summary : Reconstructing Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 11/04/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;
using System.Collections.Generic;
using System.Device.Location;
using ECO_Efficiency.Utilities;



using ECOsystem.Business.Utilities;

namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// Reconstructing Controller
    /// </summary>
    public class ReconstructingController : Controller
    {
        /// <summary>
        /// Reconstructing Controller. Implementation of all action results from views
        /// </summary>
        /// <summary>
        /// Reconstructing Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index(int? VehicleId)
        {
            try
            {
                using (var business = new ReconstructingBusiness())
                {
                    var model = new ReconstructingBase
                    {
                        Data = new Reconstructing(),
                        List = business.RetrieveVehicles(null)
                    };
                    using (var parambusiness = new ParametersBusiness())
                    {
                        model.TransmissionTime = Convert.ToInt32(parambusiness.RetrieveParameters().FirstOrDefault().TransmissionTime);
                    }
                    //#region Validates Token Info
                    if (!MapTokenUtilities.GenerateToken()) ViewBag.NoToken = true;
                    ViewBag.VehicleId = VehicleId;
                    //#endregion
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Search Vehicles
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchVehicles(string key)
        {
            try
            {
                using (var business = new ReconstructingBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Reconstructing")
                });
            }
        }

        /// <summary>
        /// Load Reconstructing
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadReconstructing(int id)
        {
            try
            {
                using (var business = new ReconstructingBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveIntrackReference(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Reconstructing")
                });
            }
        }


        /// <summary>
        /// Index Post
        /// </summary>
        /// <returns></returns>
        //public ActionResult VehicleDetailsPanel(string VehicleIds)
        public ActionResult VehicleDetailsPanel(int vehicleId, DateTime startdate, DateTime enddate)
        {
            try
            {
                var searchDateHour = startdate;
                startdate = new DateTime(startdate.Year, startdate.Month, startdate.Day);

                using (var business = new ReconstructingBusiness())
                {
                    var model = business.RetrieveIntrackReference(vehicleId).FirstOrDefault();
                    model.FragmentList = business.VehicleReconstructingRetrieve(vehicleId, startdate, enddate);

                    if (model.FragmentList.Count() > 0 && (searchDateHour.Hour + searchDateHour.Minute) > 0)
                    {
                        var selectedFragment = model.FragmentList.Where(x => x.StartDate >= searchDateHour).FirstOrDefault();
                        if (selectedFragment != null)
                            selectedFragment.Selected = true;
                    }
                    Session["Data"] = model;

                    return PartialView("_Partials/_VehicleDetailPanel", model);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public PartialViewResult LoadVehicleRoute()
        {
            try
            {
                var reconstructingModel = (Reconstructing)Session["Data"];
                int index = 0;
                var model = new ReconstructingRouteModel();
                var minimumStopTime = reconstructingModel.FragmentList.Count() < 1 ? 0 : reconstructingModel.FragmentList.FirstOrDefault().MinimumStopTime;
                var minimumDistanceBetweenPoints = reconstructingModel.FragmentList.Count() < 1 ? 0 : reconstructingModel.FragmentList.FirstOrDefault().MinimumDistanceBetweenPoints;
                reconstructingModel.FragmentList.Where(v => v.Status == "Apagado").ToList().ForEach(
                    f =>
                    {
                        index++;
                        var vehicleStop = new VehicleStop();
                        vehicleStop.Id = f.GPSId;
                        vehicleStop.Index = index;
                        vehicleStop.StartDate = f.StartDate;
                        vehicleStop.StopDuration = f.EndDate - f.StartDate;
                        vehicleStop.Description = f.Location; //string.Format("Punto {0}",index);
                        vehicleStop.Latitud = f.Latitud;
                        vehicleStop.Longitud = f.Longitud;

                        model.VehicleStops.Add(vehicleStop);
                    }
                    );

                model = ValidateStopDuration(ValidateDistance(model, minimumDistanceBetweenPoints), minimumStopTime);
                index = 0;
                model.VehicleStops.ForEach(vs =>
                {
                    index++;
                    vs.Index = index;
                });
                Session["ReconstructingRouteData"] = model;
                return PartialView("_partials/_RouteDetail", model);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return null;
            }
        }

        /// <summary>
        /// Filter the point list according to parameter by customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private static ReconstructingRouteModel ValidateDistance(ReconstructingRouteModel model, decimal minimumDistance)
        {
            try
            {
                VehicleStop stop1 = null;
                VehicleStop stop2 = null;
                var modelFiltered = new ReconstructingRouteModel();
                for (int i = 0; i < model.VehicleStops.Count; i++)
                {
                    stop2 = model.VehicleStops[i];

                    if (stop1 != null)
                    {
                        var sCoord = new GeoCoordinate(double.Parse(stop1.Latitud), double.Parse(stop1.Longitud));
                        var eCoord = new GeoCoordinate(double.Parse(stop2.Latitud), double.Parse(stop2.Longitud));

                        decimal distanceMeters = (decimal)sCoord.GetDistanceTo(eCoord);
                        if (distanceMeters > minimumDistance)
                            modelFiltered.VehicleStops.Add(stop2);
                    }
                    else
                        modelFiltered.VehicleStops.Add(stop2);
                    stop1 = stop2;
                }

                return modelFiltered;
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return null;
            }
        }
        /// <summary>
        /// Filter the point list according to parameter by customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private static ReconstructingRouteModel ValidateStopDuration(ReconstructingRouteModel model, decimal minimumStopDuration)
        {
            try
            {
                var modelFiltered = new ReconstructingRouteModel();
                foreach (VehicleStop stop in model.VehicleStops)
                {
                    if (stop.StopDuration.TotalMinutes > (double)minimumStopDuration)
                    {
                        modelFiltered.VehicleStops.Add(stop);
                    }
                }
                return modelFiltered;
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return null;
            }
        }

        [HttpPost]
        public ActionResult LoadCreateRoute(string Name, string Description, bool doRedirect)
        {
            try
            {
                var model = (ReconstructingRouteModel)Session["ReconstructingRouteData"];
                model.Name = Name;
                model.Description = Description;

                List<RoutesPoints> routePoints = new List<RoutesPoints>();
                model.VehicleStops.ForEach(vs =>
                {
                    RoutesPoints point = new RoutesPoints();

                    point.Latitude = vs.Latitud;
                    point.Longitude = vs.Longitud;
                    point.Name = vs.Description;
                    point.Order = vs.Index;
                    point.LineNumber = vs.Index;
                    point.PointReference = vs.Index;
                    point.StopTime = (int)Math.Truncate(vs.StopDuration.TotalMinutes);
                    point.Time = vs.StartDate.ToString(@"HH:mm:ss");

                    routePoints.Add(point);
                });

                int RouteId;
                using (var business = new RoutesBusiness())
                {
                    RouteId = business.AddRoutesFromReconstructing(routePoints, model.Name, model.Description);
                }

                model.routeId = 0;

                if (doRedirect && RouteId != 0)
                {
                    model.routeId = RouteId;
                }

                return PartialView("_partials/_RouteDetail", model);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public PartialViewResult DeleteStop(int idstop)
        {
            try
            {
                var model = (ReconstructingRouteModel)Session["ReconstructingRouteData"];
                VehicleStop stop = model.VehicleStops.FirstOrDefault(v => v.Id == idstop);

                if (stop != null)
                    model.VehicleStops.Remove(stop);
                int index = 1;
                model.VehicleStops.ForEach(s => { s.Index = index++; });
                Session["ReconstructingRouteData"] = model;
                return PartialView("_partials/_RouteGrid", model.VehicleStops);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return null;
            }
        }

        public ActionResult GetListFromXML(string xml)
        {
            try
            {
                return Json(string.IsNullOrEmpty(xml) ? new List<string>() : ECO_Efficiency.Business.Utilities.GeneralCollections.GetNodesXMLSensors(xml), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}


