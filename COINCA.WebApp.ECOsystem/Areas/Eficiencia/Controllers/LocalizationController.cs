﻿using System;

using System.Web.Mvc;
using ECO_Efficiency.Models.Identity;
using ECOsystem.Business.Utilities;



namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// 
    /// </summary>
    public class LocalizationController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
