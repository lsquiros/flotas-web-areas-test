﻿using COINCA.WebApp.ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Efficiency.Controllers.Efficiency
{
    public class VehicleCommandsReportController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExcelReportDownload(ControlFuelsReportsBase parameters)
        {
            try
            {
                using (var bus = new VehicleCommandsReportBusiness())
                {
                    var data = bus.ReportRetrieve(parameters);
                    Session["Reporte"] = data;
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Descarga del reporte de comandos, detalle: {0}", JsonConvert.SerializeObject(parameters)));
                    return Json(data.Rows.Count, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al descargar, detalle: {0}", e.Message));
                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                    using (var bus = new ReportsUtilities())
                    {
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "VehicleCommandsReport", this.ToString().Split('.')[2], "Reporte de Comandos");
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}