﻿using COINCA.WebApp.ECO_Efficiency.Business;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

/************************************************************************************************************
*  File    : GeofencesReportController.cs
*  Summary : Geofences Report
*  Author  : Álvaro Zamora 
*  Date    : 08/Agosto/2018
* 
*  Copyright 2017 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company. 
************************************************************************************************************/

namespace ECO_Efficiency.Controllers.Efficiency
{
    public class GeofencesReportController : Controller
    {

        public ActionResult Index()
        {
            try
            {
                return View(new ControlFuelsReportsBase());
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(ControlFuelsReportsBase model)
        {
            try
            {
                model.Dates = Convert.ToDateTime(model.StartDate).ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + Convert.ToDateTime(model.EndDate).ToString("dd/MM/yyyy hh:mm:ss tt");

                using (var business = new GeofencesReportBusiness())
                {
                    Session["Reporte"] = business.GetReportDownloadData(model);
                    return Json(((DataTable)Session["Reporte"]).Rows.Count, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Alarmas de Geocercas");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "GeofencesAlarmsReport", this.ToString().Split('.')[2], "Reporte Alarmas de Geocercas");
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return Redirect("Index");
            }
        }
    }
}