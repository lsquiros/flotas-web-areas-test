﻿/************************************************************************************************************
*  File    : VehicleByrouteController.cs
*  Summary : VehicleByroute Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 11/20/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;



using ECOsystem.Business.Utilities;

namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// VehicleByRoute Controller. Implementation of all action results from views
    /// </summary>
    public class VehicleByRouteController : Controller
    {
        /// <summary>
        /// VehicleByRoute Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new VehicleByRouteBusiness())
                {
                    var model = new VehicleByRouteBase
                    {
                        Data = new VehicleByRoute(),
                        List = business.RetrieveVehicles(null).Where(x => x.HasGPS)
                    };
                    model.Data.DetailList = new List<VehicleByRouteDetail>();
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity GeoFences
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehicleByRoute(Routes model)
        {
            try
            {
                using (var business = new RoutesBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditRoutes(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveRoutes(null));
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Routes")
                });
            }
        }

        /// <summary>
        /// Load GeoFences
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadVehicleByRoute(int id)
        {
            try
            {
                using (var business = new VehicleByRouteBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveVehicleByRoute(id, null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleByRoute")
                });
            }
        }

        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public JsonResult SaveList(List<String> values)
        {
            try
            {
                using (var business = new VehicleByRouteBusiness())
                {
                    business.AddOrEditVehicleByRoute(values);
                }
                return Json(new { Result = "Ready" });
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(new { Result = "Error" });
            }

        }

        /// <summary>
        /// Search GeoFences
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchVehicles(string key)
        {
            try
            {
                using (var business = new VehicleByRouteBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "GeoFences")
                });
            }
        }

        /// <summary>
        /// Performs the operation for get Country Code
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public ActionResult GetCountryCode()
        {
            try
            {
                using (var business = new CountryCodeBusiness())
                {
                    return Json(business.RetrieveCountryCode().FirstOrDefault().countryCode, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
