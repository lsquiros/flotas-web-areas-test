﻿/************************************************************************************************************
*  File    : MeasuringController.cs
*  Summary : Measuring Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 11/17/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Account;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;
using System;



using ECOsystem.Business.Utilities;

namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// Measuring Controller. Implementation of all action results from views
    /// </summary>
    public class MeasuringController : Controller
    {
        /// <summary>
        /// Measuring Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new CountryCodeBusiness())
                {
                    CountryCodeModels result = business.RetrieveCountryCode().FirstOrDefault();
                    result.customerId = ECOsystem.Utilities.Session.GetCustomerId().Value;

                    var model = new CountryCodeModelsBase
                    {
                        Data = result,
                        Menus = new List<AccountMenus>()
                    };

                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
