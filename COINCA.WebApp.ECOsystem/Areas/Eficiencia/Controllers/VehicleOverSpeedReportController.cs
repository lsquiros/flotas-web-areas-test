﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using Newtonsoft.Json;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;




namespace ECO_Efficiency.Controllers.Efficiency
{
    public class VehicleOverSpeedReportController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                var model = new VehicleOverSpeedReportModelBase
                {
                    Menus = new List<AccountMenus>()
                };

                return View(model);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Load the data based on the selection
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(VehicleOverSpeedReportModelBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");
                Session["Download"] = null;
                Session["PlateId"] = model.Parameters.key.Replace("'", "");
                Session["RangoFechas"] = Convert.ToDateTime(model.Parameters.StartDate).ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + Convert.ToDateTime(model.Parameters.EndDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                Session["OverSpeedParameters"] = model.Parameters;
                using (var business = new VehicleOverSpeedReportBusiness())
                {
                    var vModel = business.RetrieveInformationReport(model.Parameters);
                    return View(vModel);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = (ControlFuelsReportsBase)Session["OverSpeedParameters"];  //JsonConvert.DeserializeObject<ControlFuelsReportsBase>(Miscellaneous.Base64Decode(p));

                model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
                DataTable report = new DataTable();
                using (var business = new VehicleOverSpeedReportBusiness())
                {
                    report = business.GetReportDownloadData(model);
                    Session["Reporte"] = null;
                    Session["Reporte"] = report;
                    if (report.Rows.Count > 0)
                    {
                        Session["Download"] = "Download";
                        Session["ReporteDatos"] = null;
                    }

                    var vmodel = new VehicleOverSpeedReportModelBase
                    {
                        Menus = new List<AccountMenus>(),
                        List = new List<VehicleOverSpeedReportModel>(),
                        Parameters = new ControlFuelsReportsBase()
                    };
                    return View("Index", vmodel);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                if (report.Rows.Count > 0)
                {
                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de Excesos de Velocidad");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "VehicleOverSpeedReport", this.ToString().Split('.')[2], "Reporte de Excesos de Velocidad");
                    }
                }
                Session["Download"] = null;
                var model = new VehicleOverSpeedReportModelBase
                {
                    Menus = new List<AccountMenus>()
                };
                return View("Index", model);//RedirectToAction("Index", "VehicleOverSpeedReport");              
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var vmodel = new VehicleOverSpeedReportModelBase
                {
                    Menus = new List<AccountMenus>(),
                    List = new List<VehicleOverSpeedReportModel>(),
                    Parameters = new ControlFuelsReportsBase()
                };
                return View("Index", vmodel);
            }
        }
    }
}