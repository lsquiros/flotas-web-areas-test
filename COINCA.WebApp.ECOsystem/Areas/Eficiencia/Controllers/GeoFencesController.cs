﻿/************************************************************************************************************
*  File    : GeoFencesController.cs
*  Summary : GeoFences Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 10/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;



using ECOsystem.Business.Utilities;
using ECO_Efficiency.Utilities;

namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// GeoFences Controller. Implementation of all action results from views
    /// </summary>
    public class GeoFencesController : Controller
    {
        /// <summary>
        /// GeoFences Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new GeoFencesBusiness())
                {
                    var model = new GeoFencesBase
                    {
                        Data = new GeoFences(),
                        List = business.RetrieveGeoFences(null)
                    };

                    #region Validates Token Info
                    if (!MapTokenUtilities.GenerateToken()) ViewBag.NoToken = true;
                    #endregion

                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity GeoFences
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditGeoFences(GeoFences model)
        {
            try
            {
                using (var business = new GeoFencesBusiness())
                {
                    business.AddOrEditGeoFences(model);
                    //return RedirectToAction("Index");
                    return PartialView("_partials/_GeoFenceList", business.RetrieveGeoFences(null));
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "GeoFences")
                });
            }
        }

        /// <summary>
        /// Search GeoFences
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchGeoFences(string key)
        {
            try
            {
                using (var business = new GeoFencesBusiness())
                {
                    return PartialView("_partials/_GeoFenceList", business.RetrieveGeoFences(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "GeoFences")
                });
            }
        }

        /// <summary>
        /// Load GeoFences
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadGeoFences(int id)
        {
            try
            {
                using (var business = new GeoFencesBusiness())
                {
                    if (id == 0)
                        return PartialView("_partials/_Detail", new GeoFences());

                    return PartialView("_partials/_Detail", business.RetrieveGeoFences(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "GeoFences")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model GeoFences
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteGeoFences(int id)
        {
            try
            {
                using (var business = new GeoFencesBusiness())
                {
                    business.DeleteGeoFences(id);
                    return PartialView("_partials/_GeoFenceList", business.RetrieveGeoFences(null));
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "GeoFences")
                    });
                }
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "GeoFences")
                });
            }

        }

        /// <summary>
        /// Performs the operation for get Country Code
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCountryCode()
        {
            try
            {
                using (var business = new CountryCodeBusiness())
                {
                    return Json(business.RetrieveCountryCode().FirstOrDefault().countryCode + "&CustomerId=" + ECOsystem.Utilities.Session.GetCustomerId(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

            /// <summary>
            /// Clean the data for the right Panel 
            /// </summary>
            /// <returns></returns>
            public PartialViewResult CleanData()
            {
                var model = new GeoFences();
                return PartialView("_partials/_Detail", model);
            }    
        }
    }
