﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using System.Linq;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using Newtonsoft.Json;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;
using System.Threading.Tasks;




namespace ECO_Efficiency.Controllers.Efficiency
{
    public class GPSReportController : Controller
    {
        // GET: GPSReport
        public ActionResult Index()
        {
            try
            {
                var model = new GPSReportBase
                {
                    Menus = new List<AccountMenus>()
                };
                return View(model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(GPSReportBase model)
        {
            try
            {
                Session["PlateId"] = model.Parameters.key.Replace("'", "");
                Session["RangoFechas"] = Convert.ToDateTime(model.Parameters.StartDate).ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + Convert.ToDateTime(model.Parameters.EndDate).ToString("dd/MM/yyyy hh:mm:ss tt");

                using (var business = new GPSReportBusiness())
                {
                    if (model.Parameters.StopsDetail || model.Parameters.QuickAccelerations || model.Parameters.SuddenStops || model.Parameters.AbruptTurns ||
                        model.Parameters.LowTemperature || model.Parameters.HighTemperature || model.Parameters.PanicButton)
                    {
                        Session["ReporteDetails"] = business.GetReportStopsDownloadData(model.Parameters);
                    }
                    else
                    {
                        Session["Reporte"] = business.GetReportDownloadData(model.Parameters);
                    }
                    return Json(business.RetrieveGPSReport().List.Count(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de GPS");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "GPSReport", this.ToString().Split('.')[2], "Reporte de GPS");
                    }
                }
                else if (Session["ReporteDetails"] != null)
                {
                    DataSet report = new DataSet();
                    report = (DataSet)Session["ReporteDetails"];
                    Session["ReporteDetails"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de GPS");
                        return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "GPSStopsReport", this.ToString().Split('.')[2], "Reporte de GPS");
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                using (var business = new GPSReportBusiness())
                {
                    return View("Index", business.RetrieveGPSReport());
                }
            }
        }
    }
}