﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using System.Linq;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using Newtonsoft.Json;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;
using System.Threading.Tasks;




namespace ECO_Efficiency.Controllers.Efficiency
{
    public class VehicleTransmissionReportController : Controller
    {
        // GET: VehicleTransmissionReport
        public ActionResult Index()
        {
            try
            {
                var model = new VehicleTransmissionReportBase
                {
                    Menus = new List<AccountMenus>()
                };
                return View(model);
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(VehicleTransmissionReportBase model)
        {
            try
            {
                Session["PlateId"] = model.Parameters.key.Replace("'", "");
                Session["RangoFechas"] = Convert.ToDateTime(model.Parameters.StartDate).ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + Convert.ToDateTime(model.Parameters.EndDate).ToString("dd/MM/yyyy hh:mm:ss tt");

                using (var business = new VehicleTransmissionReportBusiness())
                {
                    Session["Reporte"] = business.GetReportDownloadData(model.Parameters);
                    return Json(business.RetrieveVehicleTransmissionReport().List.Count(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de Transmisiones de Vehiculo");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "VehicleTransmissionReport", this.ToString().Split('.')[2], "Reporte de Transmisiones de Vehiculos");
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                using (var business = new VehicleTransmissionReportBusiness())
                {
                    return View("Index", business.RetrieveVehicleTransmissionReport());
                }
            }
        }
    }
}