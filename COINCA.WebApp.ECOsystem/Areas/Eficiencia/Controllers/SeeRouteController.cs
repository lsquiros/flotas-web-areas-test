﻿/************************************************************************************************************
*  File    : SeeRouteController.cs
*  Summary : SeeRoute Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 11/03/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECO_Efficiency.Business;
using ECO_Efficiency.Models;
using ECO_Efficiency.Models.Identity;
using ECO_Maps.Utilities;



using ECOsystem.Business.Utilities;

namespace ECO_Efficiency.Controllers.Efficiency
{
    /// <summary>
    /// SeeRoute Controller. Implementation of all action results from views
    /// </summary>
    public class SeeRouteController : Controller
    {

        /// <summary>
        /// SeeRoute Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new SeeRouteBusiness())
                {
                    var model = new SeeRouteBase
                    {
                        Data = new SeeRoute(),
                        List = business.RetrieveVehicles(null)
                    };
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Search Vehicles
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchVehicles(string key)
        {
            try
            {

                using (var business = new SeeRouteBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "SeeRoute")
                });
            }
        }

        /// <summary>
        /// Load SeeRoute
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadSeeRoute(int id)
        {
            try
            {
                using (var business = new SeeRouteBusiness())
                {
                    ViewBag.vehicleid = id;
                    return PartialView("_partials/_Detail", business.RetrieveIntrackReference(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "SeeRoute")
                });
            }
        }

        /// <summary>
        /// Index Post
        /// </summary>
        /// <returns></returns>
        //public ActionResult VehicleDetailsPanel(string VehicleIds)
        public ActionResult VehicleDetailsPanel(int VehicleId)
        {
            try
            {
                Maps MapsFunctions = new Maps();
                using (var business = new SeeRouteBusiness())
                {
                    var modelSeeRoute = business.RetrieveIntrackReference(VehicleId).FirstOrDefault();
                    using (var business2 = new ParametersBusiness())
                    {
                        modelSeeRoute.TransmissionTime = Convert.ToInt32(business2.RetrieveParameters().FirstOrDefault().TransmissionTime);
                    }
                    var modelContinuousMonitoring = MapsFunctions.RetrieveContinuousMonitoring(VehicleId.ToString(), DateTime.Now.ToString("MM/dd/yyyy"), Convert.ToInt32(Session["LOGGED_USER_TIMEZONE_OFF_SET"]) * 1).FirstOrDefault();
                    DateTime timeZone = modelContinuousMonitoring.GPSDateTime;
                    modelSeeRoute.VSSSpeed = modelContinuousMonitoring.VSSSpeed;
                    modelSeeRoute.Status = MapsFunctions.getCarStatusId(modelContinuousMonitoring.Status, modelContinuousMonitoring.ActualDate, modelContinuousMonitoring.GPSDateTime, modelSeeRoute.TransmissionTime, modelContinuousMonitoring.VSSSpeed);
                    modelSeeRoute.Distance = modelContinuousMonitoring.Distance;
                    modelSeeRoute.Latitude = modelContinuousMonitoring.Latitude;
                    modelSeeRoute.Longitude = modelContinuousMonitoring.Longitude;
                    modelSeeRoute.Date = timeZone.ToString("dd/MM/yyyy");
                    modelSeeRoute.Time = timeZone.ToString("HH:mm:ss");

                    //geoEvent.ImageUrl = MapsFunctions.getCarImage(x.Status, x.ActualDate, x.GPSDateTime, inactive, x.VSSSpeed, true);  
                    return PartialView("_Partials/_VehicleDetailsPanel", modelSeeRoute);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Retrieve Route lines on the map
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RouteDetailsRetrieve(int SearchType, int id)
        {
            try
            {
                using (var business = new SeeRouteBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveIntrackReference(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "SeeRoute")
                });
            }
        }
    }
}
