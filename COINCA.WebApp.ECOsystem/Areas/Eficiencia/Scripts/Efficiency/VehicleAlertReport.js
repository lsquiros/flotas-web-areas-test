﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.VehicleAlertReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initializeDropDownList();
        initEvents();        
    }

    var initEvents = function () {
        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) {
            $('#datetimepickerEndDate').datepicker('remove');

            var dateV = $('#Parameters_StartDateStr').val();
            var params = dateV.split("/");
            //var daysotm = new Date(params[2], params[1], 1, -1).getDate();
            //var days = parseInt(params[0]) + (parseInt(daysotm) - 1);
            var days = parseInt(params[0]) + 6;
            var maxDateV = days + "/" + params[1] + "/" + params[2];

            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: dateV,
                startDate: dateV,
                endDate: maxDateV,
                format: 'dd/mm/yyyy',
                autoclose: true
            });
            $("#datetimepickerEndDate").datepicker('update');
            $('#Parameters_EndDateStr').val();
        });

        $("#Parameters_StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $("#Parameters_EndDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        initDatePickerTime();
    }

    var initDatePickerTime = function () {
        $('#datetimepickerStartDate').datepicker("setDate", new Date());
        $('#datetimepickerEndDate').datepicker("setDate", new Date());
    } 

    var initializeDropDownList = function () {
        try {
            var select3 = $("#Parameters_CostCenterId").select2().data('select2');
            select3.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectCostCenterChange(data); return fn.apply(this, arguments); }
                }
            })(select3.onSelect);

            var select4 = $("#Parameters_FilterVehicle").select2({ formatResult: vehiclesSelectFormat, formatSelection: vehiclesSelectFormat, escapeMarkup: function (m) { return m; } }).data('select2');
            select4.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectVehiclesChange(data); return fn.apply(this, arguments); }
                }
            })(select4.onSelect);
        }
        catch (e) { }
    };

    var onSelectCostCenterChange = function (obj) {
        if (obj.id === '') {
            //invalidSelectOption();
        } else {
            //$('#Parameters_FilterVehicle option:selected').removeAttr('selected');
            $('#Parameters_FilterVehicle').val('').change();
        }
    }
    /*on Select Status Change*/
    var onSelectVehiclesChange = function (obj) {
        if (obj.id === '') {
            //invalidSelectOption();
        } else {
            //$('#Parameters_FilterVehicle').val(obj.id);
            //if (checkAllSelects()) $('#ReloadForm').submit();
            //$("#Parameters_CostCenterId").val('');
            //$('#Parameters_CostCenterId option[value=-2]').prop('selected', 'selected');
            $('#Parameters_CostCenterId').val('').change();
        }
    };
    /*Format for the vehicle dropdown*/
    var vehiclesSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.PlateId + '</div>' +
                '</div>';
    };
    /*Excecute the excel download*/
    var onClickDownload = function () {
        if (checkAllSelects()) $('#ExcelReportDownloadForm').submit();
    };
    /*Loader*/
    var showLoader = function () {
        $('body').loader('show');
    };
    /*Hide Loader*/
    var hideLoader = function (data) {
        if (isNaN(data)) {
            $('body').loader('hide');
            $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           'Ocurrió un error al procesar el reporte, por favor intentelo de nuevo. <hr />' +
                                           '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
        } else if (data > 0) {
            $('body').loader('hide');
            $('#DownloadFileForm').submit();
        } else {
            $('body').loader('hide');
            $('#ControlsContainer').html('<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                          'No datos encontrados con los criterios de busqueda.</div>');
        }
    };

    var onClickFirstDate = function () {
        $('#Parameters_EndDateStr').val('');
    };
    /*Check the dropdown selection*/
    var checkAllSelects = function () {
        if (($('#Parameters_StartDateStr').val() !== '') && ($('#Parameters_EndDateStr').val() !== '')) {
            showLoader();
            $('#ControlsContainer').html('');
            return true;
        }
        else {
            invalidSelectOption('Por favor seleccionar las opciones correspondientes para cargar la información.');
            return false;
        }
        return true;
    };

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                '<span aria-hidden="true">×</span>' +
                                            '</button>' +
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };


    /* Public methods */
    return {
        Init: initialize,
        OnClickDownload: onClickDownload,
        HideLoader: hideLoader,
        ShowLoader: showLoader,
        OnClickFirstDate: onClickFirstDate
    };
})();