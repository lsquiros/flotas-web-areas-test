﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.PassControl = (function () {
    var options = {};
    var objReportData;

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initializeDropDownList();
        initEvents();
    }

    /*init Events*/
    var initEvents = function () {       

        // LevelType Init --> 1: Vehicle, 2:Group, 4: CostCenter
        var mapURI = $("#MapURI").val();
        mapURI = mapURI.replace("[LevelType]", "1");
        $("#srcMapURI").val(mapURI);

        // DatePicker for Control de Paso Report
        $('#datetimepickerStartDate, #datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $("#menu-left-toggle").click(function () {
            $("#menu-left").toggleClass("menu-hide");
            $("#map").toggleClass("map-menu-left-hide");
            $("#menu-left-toggle").toggleClass("map-menu-left-hide");

            if ($("#menu-left").hasClass("menu-hide")) {
                $("#left_icon").addClass("mdi-arrow-right-bold");
                $("#left_icon").removeClass("mdi-arrow-left-bold");
            } else {
                $("#left_icon").removeClass("mdi-arrow-right-bold");
                $("#left_icon").addClass("mdi-arrow-left-bold");
            }
        });


        $("#menu-right-toggle").click(function () {
            $("#menu-right").toggleClass("menu-hide");
            $("#menu-right-toggle").toggleClass("menu-right-hide");

            if ($("#menu-right").hasClass("menu-hide")) {
                $("#right_icon").addClass("mdi-arrow-left-bold");
                $("#right_icon").removeClass("mdi-arrow-right-bold");
            } else {
                $("#right_icon").removeClass("mdi-arrow-left-bold");
                $("#right_icon").addClass("mdi-arrow-right-bold");
            }
        });


        $('#btnSearchVehicle').click(function () {
            SearchVehicleLeftPanel();
        });

        $('#btnSearchVehicleGroup').click(function () {
            SearchVehicleGroupPanel();
        });

        // Expand Left Cards
        $("#btnExpandLeft").click(function (e) {
            
            var expand = $(this).attr('expand');
            if (typeof expand !== typeof undefined && expand !== false) {
                $(".accordin-left").collapse("hide");
                $(this).removeAttr("expand");
                ChangeExpandIcon($("#btnExpandLeft i"), true);
            } else {
                $(".accordin-left").collapse("show");
                $(this).attr("expand", "expand");
                ChangeExpandIcon($("#btnExpandLeft i"), false);
            }
        });

        $('.accordionIconEffect').click(function () {
            if (!$('.accordin-left').hasClass('in')) {
                $(this).find('i').removeClass('mdi-plus');
                $(this).find('i').addClass('mdi-minus');
            }
            else {
                $(this).find('i').removeClass('mdi-minus');
                $(this).find('i').addClass('mdi-plus');
            }
        });

        $('.card-PassControl').off('click.edit_vh', 'a[edit_row_vh]').on('click.edit_vh', 'a[edit_row_vh]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();           
        });

        $('.card-PassControl').off('click.view_group', 'a[view_row_group]').on('click.view_group', 'a[view_row_group]', function (e) {          
            $('#loadFormGroup').find('#id').val($(this).attr('id'));
            $('#loadFormGroup').submit();          
        });

        $('.card-PassControl').off('click.edit_cc', 'a[edit_row_cc]').on('click.edit_cc', 'a[edit_row_cc]', function (e) {
            $('#loadFormCostCenter').find('#id').val($(this).attr('id'));
            $('#loadFormCostCenter').submit();         
        });

        $('.card').off('click.restoreCss', '.force-close-card-reveal').on('click.restoreCss', '.force-close-card-reveal', function () {
            $("#main-card").addClass("card-PassControl");
        });

        $("body").on("click", "#btnSearchFilter", SendSearch);
        $("body").on("click", "#btnGenerarReport", SendGenerateReport);

        // Validate if has data for Report
        //setInterval(GetReportDataFromIframe, 500);
        window.addEventListener("message", GetReportDataFromIframe, false);
    }
    
    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        $('#cbProvincias').attr("disabled", "disabled");
        $('#cbCantones').attr("disabled", "disabled");

        //Populate Province
        $.ajax({
            url: '/PassControlVehicle/GetProvinciasFromMaps',
            type: "GET",
            data: { CountryCode: $("#CountryCode").val() },
            success: function (result) {
                var provDropDownList = $('#cbProvincias');
                provDropDownList.empty();
                $.each(result, function () {
                    if (this.Value != "") {
                        provDropDownList.append(
                            $('<option/>', {
                                value: this.Value,
                                text: this.Text
                            })
                        );
                    }
                });
                $('#cbProvincias').removeAttr("disabled");
                $("#cbProvincias").trigger("change");
            }
        });

        //Populate Cantons
        $('#cbProvincias').change(function () {
            $.ajax({
                url: '/PassControlVehicle/GetCantonesFromMaps',
                type: "GET",
                data: { CountryCode: $("#CountryCode").val(), Provincia: $(this).val() },
                success: function (result) {
                    var cantDropDownList = $('#cbCantones');
                    cantDropDownList.empty();
                    $.each(result, function () {
                        if (this.Value != "") {
                            cantDropDownList.append(
                                $('<option/>', {
                                    value: this.Value,
                                    text: this.Text
                                })
                            );
                        }
                    });
                    $('#cbCantones').removeAttr("disabled");
                }
            });
        });
    };

    // Change Expand Button Icons
    function ChangeExpandIcon(icon, result) {
        if (result) {
            icon.removeClass("mdi-arrow-compress-all");
            icon.addClass("mdi-arrow-expand-all");
        } else {
            icon.removeClass("mdi-arrow-expand-all");
            icon.addClass("mdi-arrow-compress-all");
        }
    }

    // Function that looks for the filter in the left panels
    function SearchVehicleLeftPanel() {
        var flag = 0;
        var text = $('#txtSearch').val();
        $(".checkboxGV").show("slow");

        $(".VehiclePanelRight").each(function (index) {
            if (text == '') {
                var gvname = $(this).attr("gvname");
                if (typeof gvname !== 'undefined') {
                    $(this).show("slow");
                }
            }
            else {
                var gvname = $(this).attr("gvname");

                if (typeof gvname !== 'undefined') {

                    if (gvname.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                        $(this).show("slow");
                    }
                    else {

                        $(this).find('.checkboxGV').each(function () {
                            var VHPlate = $(this).attr("VHPlate");
                            var VHName = $(this).attr("VHName");

                            if (typeof VHName !== 'undefined' && typeof VHPlate !== 'undefined') {

                                if (VHName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else if (VHPlate.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else {
                                    $(this).hide("slow");
                                }
                            }
                        });

                        //If the vehicle was found let the cost center visible
                        if (flag != 1) {
                            $(this).hide("slow");
                        }
                        else {
                            flag = 0;
                        }
                    }
                }
            }
        });
    }

    function SearchVehicleGroupPanel() {
        var flag = 0;
        var text = $('#txtSearch-Group').val();
        $(".checkboxGV").show("slow");

        $(".GroupVehiclePanelRight").each(function (index) {
            if (text == '') {
                var gvname = $(this).attr("gvname");
                if (typeof gvname !== 'undefined') {
                    $(this).show("slow");
                }
            }
            else {
                var gvname = $(this).attr("gvname");

                if (typeof gvname !== 'undefined') {

                    if (gvname.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                        $(this).show("slow");
                    }
                    else {

                        $(this).find('.checkboxGV').each(function () {
                            var VHPlate = $(this).attr("VHPlate");
                            var VHName = $(this).attr("VHName");

                            if (typeof VHName !== 'undefined' && typeof VHPlate !== 'undefined') {

                                if (VHName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else if (VHPlate.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else {
                                    $(this).hide("slow");
                                }
                            }
                        });

                        //If the vehicle was found let the cost center visible
                        if (flag != 1) {
                            $(this).hide("slow");
                        }
                        else {
                            flag = 0;
                        }
                    }
                }
            }
        });
    }

    // Send search with filters
    var SendSearch = function () {
        var mapURI = $("#srcMapURI").val();
        var countryCode = $("#CountryCode").val();
        var province = $("#cbProvincias").val();
        var canton = $("#cbCantones").val();
        var textSearch = $("#txtSearchFilter").val();
      
        mapURI += "&ExternalSearch=true";
        mapURI += "&CountryCode=" + countryCode;
        mapURI += "&Province=" + province;
        mapURI += "&Canton=" + canton;
        mapURI += "&Search=" + textSearch;

        $('#mapFrame').attr('src', mapURI);
    }

    // Send Generate Report
    var SendGenerateReport = function () {
        
        var mapURI = $("#MapURI").val();
        var startDate = $("#StartDateStr").val();
        var endDate = $("#EndDateStr").val();
        var distance = $("#DistanceReport").val();
        var latitude = $("#LatitudeReport").val();
        var longitude = $("#LongitudeReport").val();
        var eventOn = ($('#chkEventosEncendido').prop('checked')) ? 1 : 0;
        var eventOff = ($('#chkEventosApagado').prop('checked')) ? 1 : 0;

        mapURI += "&ExternalReport=true";
        mapURI += "&StartDate=" + startDate;
        mapURI += "&EndDate=" + endDate;
        mapURI += "&Distance=" + distance;
        mapURI += "&Latitude=" + latitude;
        mapURI += "&Longitude=" + longitude;
        mapURI += "&EventOn=" + eventOn;
        mapURI += "&EventOff=" + eventOff;


        showLoader();
        $('#mapFrame').attr('src', mapURI);
    };

    // Get Report from iframe Map
    var GetReportDataFromIframe = function (event) {
        
        if (event.data.includes("[Points]")) {
            var data = event.data.replace("[Points]", "");
            var arrData = data.split('|');
            $("#DistanceReport").val(arrData[0]);
            $("#LatitudeReport").val(arrData[1]);
            $("#LongitudeReport").val(arrData[2]);
            setErrorMsj('', false);

        } else if (event.data != null && event.data != "") {
            objReportData = jQuery.parseJSON(event.data); //event.data;
            var obj = jQuery.parseJSON(event.data);
            var elem = 0;
            var tblGrid = $("#tblReportPassControl tbody");
            var strTemplate = "<tr class=\"grid-row\">" +
                                "<td class=\"grid-cell grid-col-left\">#Driver</td>" +
                                "<td class=\"grid-cell grid-col-center\">#Dallas</td>" +
                                "<td class=\"grid-cell grid-col-center\">#Plate</td>" +
                                "<td class=\"grid-cell grid-col-center\">#Grupo</td>" +
                                "<td class=\"grid-cell grid-col-center\">#CCosto</td>" +
                                "<td class=\"grid-cell grid-col-center\">#FechaIn</td>" +
                                "<td class=\"grid-cell grid-col-center\">#FechaOut</td>" +
                              "</tr>"

            tblGrid.empty(); //Clear old data

            $.each(obj, function (i, item) {
                elem++;
                var row = strTemplate;
                row = row.replace("#Plate", item.PlateId);
                row = row.replace("#FechaIn", item.InDateTime);
                row = row.replace("#FechaOut", item.OutDateTime);
                row = row.replace("#Grupo", item.SubUnitName);
                row = row.replace("#CCosto", item.SubUnitName);
                row = row.replace("#Driver", item.DriverName);
                row = row.replace("#Dallas", item.Dallas);

                tblGrid.append(row);
            });

            if (elem > 0) {
                setErrorMsj('', false);
                $("#main-card").removeClass("card-PassControl");
                $("#btnRevealDetail").trigger("click");
                var mapURI = $("#MapURI").val();
                $('#mapFrame').attr('src', mapURI);
            }           
        }
        else if (event.data == "") {

            var elem = 0;
            var tblGrid = $("#tblReportPassControl tbody");
            var strTemplate = "<tr class=\"grid-row\">" +
                                "<td class=\"grid-cell grid-col-left\">N/A</td>" +
                                "<td class=\"grid-cell grid-col-center\">N/A</td>" +
                                "<td class=\"grid-cell grid-col-center\">N/A</td>" +
                                "<td class=\"grid-cell grid-col-center\">N/A</td>" +
                                "<td class=\"grid-cell grid-col-center\">N/A</td>" +
                                "<td class=\"grid-cell grid-col-center\">N/A</td>" +
                                "<td class=\"grid-cell grid-col-center\">N/A</td>" +
                              "</tr>"
            tblGrid.empty(); //Clear old data

            var row = strTemplate;
            tblGrid.append(row);

            $("#main-card").removeClass("card-PassControl");
            $("#btnRevealDetail").trigger("click");
            var mapURI = $("#MapURI").val();
            $('#mapFrame').attr('src', mapURI);
            setErrorMsj('<i class="mdi mdi-alert" style="font-size:18px;"></i>&nbsp;No hay datos para mostrar con la combinación de criterios utilizada', true);
        }

        hideLoader();
    };

    /*Loader*/
    var showLoader = function () {
        $('body').loader('show');
    };

    /*Hide Loader*/
    var hideLoader = function () {
        $('body').loader('hide');
    };

    /* Show or hide errors messages*/
    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj2').html(msj);
            $('#validationErrors2').removeClass('hide');
        } else {
            $('#errorMsj2').html('');
            $('#validationErrors2').addClass('hide');
        }
    };

    var onClickDownloadPartnerReport = function () {
        $('body').loader('show');
        ///$('#ExcelReportDownloadForm').submit();
        var startDate = $("#StartDateStr").val();
        var endDate = $("#EndDateStr").val();
        var eventOn = ($('#chkEventosEncendido').prop('checked')) ? 1 : 0;
        var eventOff = ($('#chkEventosApagado').prop('checked')) ? 1 : 0;

        $.ajax({
            url: '/Eficiencia/PassControlVehicle/ExcelReportDownload',
            type: 'POST',
            data: JSON.stringify({ pReportData: objReportData, pEventOn: eventOn, pEventOff: eventOff, pStartDate: startDate, pEndDate: endDate }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data > 0) {
                    $('body').loader('hide');
                    $('#DownloadFilePassControl').submit();
                } else {
                    $('body').loader('hide');
                    invalidSelectOption('No datos encontrados con los criterios de busqueda.');
                } 
            }
        });
    };

    /*invalid Select Option*/
    var invalidSelectOption = function (mensaje) {
        $('#ControlsContainer').html('<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
            ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' + mensaje + '</div>');
    };

    /* Public methods */
    return {
        Init: initialize,
        HideLoader: hideLoader,
        ShowLoader: showLoader,
        InitDropDown: initializeDropDownList,
        OnClickDownloadPartnerReport: onClickDownloadPartnerReport
    };
})();

/*$('body').loader('show');
        ///$('#ExcelReportDownloadForm').submit();
        var jsonObject = {
            "Name": "Puntos",
            "PuntosControl": objReportData
        };

        $.ajax({
            url: '/Eficiencia/PassControlVehicle/ExcelReportDownload',
            type: 'POST',
            data: JSON.stringify(jsonObject),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                //window.location = '/Home/IndexCustomer/';
                $('body').loader('hide');
            }
        });*/