﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.TemperatureChart = (function () {
    var options = {};
    var newInfo = false; 

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts)
        initCharts();
        initDatePicker();

        $('#btnReloadChart').click(function () {            
            newInfo = true;
            reconstructionChart();
        });
    }

    var initDatePicker = function () {
        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    }

    /*Init Charts*/
    var initCharts = function () {
        if (options.labels.length > 30) {
            options.labels = $.merge(options.labels, ["", "", "", "", "", "", "", "", "", "", "", ""]);
        }

        var j = 0;
        $("canvas").each(function () {

            var ctx = this;

            var datasets = [];
            for (var i = 0; i < options.data.length; i++) {
                datasets.push({
                    fillColor: options.alpha[i],
                    strokeColor: options.colors[i],
                    highlightFill: options.highligh[i],
                    highlightStroke: options.colors[i],
                    data: options.data[i].slice(j, j + 30),
                    title: options.titles[i],
                    pointColor: options.colors[i]
                });
            }

            new Chart($(this).get(0).getContext("2d")).Line({
                labels: options.labels.slice(j, j + 30),
                datasets: datasets,
            },
                {
                    scaleLabel: "<%=ECO_Efficiency.TemperatureChart.FormatLabel(value)%>",
                    annotateLabel: "<%=ECO_Efficiency.TemperatureChart.FormatLabel(v3)%>",
                    inGraphDataTmpl: "<%=ECO_Efficiency.TemperatureChart.FormatLabel(v3)%>",
                    legendBlockSize: 20,
                    legendColorIndicatorStrokeWidth: 7,
                    drawXScaleLine: [{ position: "bottom", lineWidth: 1, lineColor: "black" }, { position: "0", lineWidth: 1, lineColor: "red" }],
                    scaleLineWidth: 1,
                    scaleLineColor: "black"

                });
            j += 31;
        });
        $("div[data-item]:not(:first)").removeClass("active");
    }

    var reconstructionChart = function (obj) {        
        var date = $('#StartDate').val();
        var vehicleId = $('#VehicleIdTemp').val();

        if (date == '') {
            date = new Date();
        } else {
            date = date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2];
        }
        
        var positionList = "";

        $('input[type=checkbox]').each(function () {
            if (this.checked == true) {
                if ($(this).attr('position') != undefined) {
                    if (positionList == "")
                        positionList = $(this).attr('position');
                    else
                        positionList = positionList + '|' + $(this).attr('position');
                }
            }
        });

            $('body').loader('show');
            $.ajax({
                url: '/Eficiencia/ContinuosMonitoring/LoadTemperaturePartial',
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ PositionList: positionList, StartDate: date, VehicleId: vehicleId })
            }).success(function (partialViewResult) {
                $("#chartcontainer").html(partialViewResult);
                if ($("#Data").val() != "") {

                    $("#card_title").attr("hidden", false);
                    $("#sensorsdiv").attr("hidden", false);
                var datasets = [];
                var data = JSON.parse($("#Data").val());
                var alpha = JSON.parse($("#AlphaColors").val());
                var highligh = JSON.parse($("#HighlightColors").val());
                var colors = JSON.parse($("#Colors").val());
                var titles = JSON.parse($("#Titles").val());
                var labels = JSON.parse($("#Labels").val())

                if (labels.length > 30) {
                    labels = $.merge(labels, ["", "", "", "", "", "", "", "", "", "", "", ""]);
                }

                var j = 0;
                $("canvas").each(function () {

                    var ctx = this;

                    var datasets = [];
                    for (var i = 0; i < data.length; i++) {
                        datasets.push({
                            fillColor: alpha[i],
                            strokeColor: colors[i],
                            highlightFill: highligh[i],
                            highlightStroke: colors[i],
                            data: data[i].slice(j, j + 30),
                            title: titles[i],
                            pointColor: colors[i]
                        });
                    }

                    new Chart($(this).get(0).getContext("2d")).Line({
                        labels: labels.slice(j, j + 30),
                        datasets: datasets
                    },
                        {
                            scaleLabel: "<%=ECO_Efficiency.TemperatureChart.FormatLabel(value)%>",
                            annotateLabel: "<%=ECO_Efficiency.TemperatureChart.FormatLabel(v3)%>",
                            inGraphDataTmpl: "<%=ECO_Efficiency.TemperatureChart.FormatLabel(v3)%>",
                            legendBlockSize: 20,
                            legendColorIndicatorStrokeWidth: 7,
                            drawXScaleLine: [{ position: "bottom", lineWidth: 1, lineColor: "black" }, { position: "0", lineWidth: 1, lineColor: "red" }],
                            scaleLineWidth: 1,
                            scaleLineColor: "black"

                        });
                    j += 31;
                });
                $("div[data-item]:not(:first)").removeClass("active");;

                if (newInfo) {
                    showInfoChart();
                }               

                }
                else {
                    $("#card_title").attr("hidden", true);
                    $("#sensorsdiv").attr("hidden", true);
                }
                $('body').loader('hide');
            });
        
    }

    var showInfoChart = function () {
        $.ajax({
            url: '/Eficiencia/ContinuosMonitoring/LoadTemperatureGrid',
            type: "POST"
        }).success(function (partialViewResult) {
            $('#TemperatureDetailContainer').html(partialViewResult);
            newInfo = false;
        });
    };

    /*format Label*/
    var formatLabel = function (v) {
        return formatNum(toFloat(v));
    }

    /*Convert str to Float*/
    var toFloat = function (num) {
        num = ("" + num).replace(/,/g, '');
        return isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
    }

    /*format Number*/
    var formatNum = function (num) {
        return num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    /*Loader*/
    var showLoader = function () {
        $('body').loader('show');
    };

    /*Hide Loader*/
    var hideLoader = function (data) {
        $('body').loader('hide');
        $('#DownloadFileForm').submit();
    };

    var onClickDownloadReport = function () {
        $('#ExcelReportDownloadForm').submit();
    };

    var onClickFirstDate = function () {
        $('#Parameters_EndDateStr').val('');
    };
    
    /* Public methods */
    return {
        Init: initialize,
        FormatLabel: formatLabel,
        ReconstructionChart: reconstructionChart,
        HideLoader: hideLoader,
        ShowLoader: showLoader,
        OnClickDownloadReport: onClickDownloadReport,
        OnClickFirstDate: onClickFirstDate
    };
})();