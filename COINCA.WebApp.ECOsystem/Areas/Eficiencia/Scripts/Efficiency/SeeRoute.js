﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.SeeRoute = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        $('.accordionIconEffect').click(function () {
            if (!$('.accordin-left').hasClass('in')) {
                $(this).find('i').removeClass('mdi-plus');
                $(this).find('i').addClass('mdi-minus');
            }
            else {
                $(this).find('i').removeClass('mdi-minus');
                $(this).find('i').addClass('mdi-plus');
            }
        });

        $(".btnView").click(function (e) {
            e.stopPropagation();
            var vehicleId = $(this).attr("vehicleId");
            var Url = $("#mapFrame").attr("OriginalSrc") + vehicleId + "|" + vehiclesArray.join();
            $("#mapFrame").attr("src", Url);
        });       


    };

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
    };

    /*on Select Group Change*/
    var onSelectSearchType = function (obj) {
        if (obj.id === '') {
            //invalidSelectOption();
        } else {
            $('#SearchType').val(obj.id);
            var t = $('#SearchType').val();
            $('#RetrieveDetails').submit();
        }
    };

    // Change Expand Button Icons
    function ChangeExpandIcon(icon, result) {
        if (result) {
            icon.removeClass("mdi-arrow-compress-all");
            icon.addClass("mdi-arrow-expand-all");
        } else {
            icon.removeClass("mdi-arrow-expand-all");
            icon.addClass("mdi-arrow-compress-all");
        }
    }

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad
    };
})();

