﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.VehicleCommandsReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initializeDropDownList();
        initEvents();
    }

    /*init Events*/
    var initEvents = function () {
        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) {
            $('#datetimepickerEndDate').datepicker('remove');

            var dateV = $('#StartDateStr').val();
            var params = dateV.split("/");
            var daysotm = new Date(params[2], params[1], 1, -1).getDate();
            var days = parseInt(params[0]) + (parseInt(daysotm) - 1);
            var maxDateV = days + "/" + params[1] + "/" + params[2];
            //Get the date in the format to display
            var fechaFinal = (days > daysotm) ? ((days - daysotm).toString().length == 1 ? "0" + (days - daysotm) : (days - daysotm)) +
                "/" + ((parseInt(params[1]) + 1).toString().length == 1 ? "0" + (parseInt(params[1]) + 1) : params[1] == 12 ? "01" : (parseInt(params[1]) + 1)) +
                "/" + (params[1] == 12 ? (parseInt(params[2])) + 1 : params[2])
                : days + "/" + params[1] + "/" + params[2];

            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: dateV,
                startDate: dateV,
                endDate: maxDateV,
                format: 'dd/mm/yyyy',
                autoclose: true
            });
            $("#datetimepickerEndDate").datepicker('update');
            $('#EndDateStr').val(fechaFinal);
        });

        $("#StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $("#EndDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });
    }

    /*initialize Drop Down List*/

    var initializeDropDownList = function () {
        try {
            var select4 = $("#FilterVehicle").select2({ formatResult: vehiclesSelectFormat, formatSelection: vehiclesSelectFormat, escapeMarkup: function (m) { return m; } }).data('select2');
            select4.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
                }
            })(select4.onSelect);
        }
        catch (e) { }
    };

    var vehiclesSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-12">' + json.PlateId + '</div>' +
            '</div>';
    };

    /*Check the dropdown selection*/
    var checkAllSelects = function () {
        if (($('#StartDateStr').val() !== '') && ($('#EndDateStr').val() !== '') && ($('#FilterVehicle').val() !== '')) {
            var plateid = $("#FilterVehicle option:selected").text();
            var val = plateid.split(',')[0].split(':')[1];
            $('#ExcelReportDownloadForm').find('#key').val(val);
            showLoader();
            $('#ControlsContainer').html('');
            return true;
        }
        else {
            invalidSelectOption('Por favor seleccionar las opciones correspondientes para cargar la información.');
            return false;
        }
        return true;
    };   
    
    var invalidSelectOption = function (mensaje) {
        $('#ControlsContainer').html('<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
            ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' + mensaje + '</div>');
    };

    var onClickDownloadPartnerReport = function () {
        if (checkAllSelects()) $('#ExcelReportDownloadForm').submit();
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function (data) {
        if (data > 0) {
            $('body').loader('hide');
            $('#DonwloadFileForm').submit();
        } else {
            $('body').loader('hide');
            invalidSelectOption('No datos encontrados con los criterios de busqueda.');
        }
    };

    var onClickFirstDate = function () {
        $('#EndDateStr').val('');
    };

    /* Public methods */
    return {
        Init: initialize,
        OnClickDownloadPartnerReport: onClickDownloadPartnerReport,
        HideLoader: hideLoader,
        ShowLoader: showLoader,
        OnClickFirstDate: onClickFirstDate
    };
})();