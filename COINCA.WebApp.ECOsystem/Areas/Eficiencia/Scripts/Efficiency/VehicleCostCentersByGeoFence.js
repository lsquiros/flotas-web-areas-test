﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.VehicleCostCentersByGeoFence = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        var select2 = $("#GeoFenceId").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectChange(data);
                    return fn.apply(this, arguments);
                }
            }
        })(select2.onSelect);

        $('.connected').sortable({
            connectWith: '.connected'
        });

        $('#btnSubmit').off('click.btnSubmit').on('click.btnSubmit', function (e) {
            $('#ConnectedVehicleCostCenters').find('input:hidden').each(function (i) {
                $(this).attr('name', 'ConnectedVehicleCostCentersList[' + i + '].CostCenterId');
            });
            return true;
        });

        //$("#saveForm").data("validator").settings.submitHandler = function (e) {
        //    $('#DragAndDropContainer').find('#processing').removeClass('hide');
        //    $('#DragAndDropContainer').find('button').attr("disabled", "disabled");
        //    $('#DragAndDropContainer').find('a[atype="button"]').attr("disabled", "disabled");
        //};
    };

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
    };

    var onSelectChange = function (obj) {
        $('#id').val(obj.id);
        $("input:hidden[id='GeoFenceId']").val(obj.id);

        if (obj.id === '') {
            $('#DragAndDropContainer').html('');
        } else {
            $('#loadForm').submit();
        }
    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad
    };
})();

