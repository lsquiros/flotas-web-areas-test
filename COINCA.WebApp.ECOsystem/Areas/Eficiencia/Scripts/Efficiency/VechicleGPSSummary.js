﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.VehicleGPSSummary = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initializeDropDownList();
        initEvents();
    }


    /*init Events*/
    var initEvents = function () {
        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) {
            $('#datetimepickerEndDate').datepicker('remove');

            var dateV = $('#Parameters_StartDateStr').val();
            var params = dateV.split("/");
            var days = parseInt(params[0]) + 6;
            var maxDateV = days + "/" + params[1] + "/" + params[2];

            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                //minDate: dateV,
                startDate: dateV,
                //endDate: maxDateV,
                format: 'dd/mm/yyyy',
                autoclose: true
            });
            $("#datetimepickerEndDate").datepicker('update');
            $('#Parameters_EndDateStr').val();
        });

        $("#Parameters_StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $("#Parameters_EndDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        initDatePickerTime();
    }

    var initDatePickerTime = function () {
        $('#datetimepickerStartDate').datepicker("setDate", new Date());
        $('#datetimepickerEndDate').datepicker("setDate", new Date());
    } 

    var initializeDropDownList = function () {
        try {    
            var select3 = $("#Parameters_CostCenterId").select2().data('select2');
            select3.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
                }
            })(select3.onSelect);           
        }
        catch (e) { }
    };

    var vehiclesSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.PlateId + '</div>' +
                '</div>';
    };

    /*Check the dropdown selection*/
    var checkAllSelects = function () {
        if (($('#Parameters_StartDateStr').val() !== '') && ($('#Parameters_EndDateStr').val() !== '')) {
            showLoader();
            $('#ControlsContainer').html('');
            return true;
        }
        else {
            invalidSelectOption('Por favor seleccionar las opciones correspondientes para cargar la información.');
            return false;
        }
        return true;
    };
    
    /*Excecute the excel download*/
    var onClickDownloadReport = function () {
        if (checkAllSelects()) $('#ExcelReportDownloadForm').submit();
    };

    /*Loader*/
    var showLoader = function () {
        $('body').loader('show');
    };

    /*Hide Loader*/
    var hideLoader = function (data) {
        if (isNaN(data)) {
            $('body').loader('hide');
            $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           'Ocurrió un error al procesar el reporte, por favor intentelo de nuevo. <hr />' +
                                           '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
        } else if (data > 0) {
            $('body').loader('hide');
            $('#DownloadFileForm').submit();
        } else {
            $('body').loader('hide');
            $('#ControlsContainer').html('<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                          'No datos encontrados con los criterios de busqueda.</div>');            
        }
    };

    var onClickFirstDate = function () {
        $('#Parameters_EndDateStr').val('');
    };

    /* Public methods */
    return {
        Init: initialize,
        OnClickDownloadReport: onClickDownloadReport,
        HideLoader: hideLoader,
        ShowLoader: showLoader,
        OnClickFirstDate: onClickFirstDate
    };
})();