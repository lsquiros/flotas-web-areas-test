﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.LastVehicleTransReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initializeDropDownList();
        initEvents();
    }
    
    /*init Events*/
    var initEvents = function () {
        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }); 

        $("#datetimepickerStartDate").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });
        
        initDatePickerTime();
    }

    var initDatePickerTime = function () {
        $('#datetimepickerStartDate').datepicker("setDate", new Date());
    }

    var initializeDropDownList = function () {
        try {
            var select3 = $("#CostCenterId").select2().data('select2');
            select3.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
                }
            })(select3.onSelect);
        }
        catch (e) { }
    };

    /*Check the dropdown selection*/
    var checkAllSelects = function () {
        if ($('#StartDateStr').val() !== '') {
            showLoader();
            $('#ControlsContainer').html('');
            return true;
        }
        else {
            $('#ControlsContainer').html('<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                'Por favor seleccionar las opciones correspondientes para cargar la información.</div>');
            return false;
        }
        return true;
    };

    /*Excecute the excel download*/
    var onClickDownloadReport = function () {
        if (checkAllSelects()) $('#ExcelReportDownloadForm').submit();
    };

    /*Loader*/
    var showLoader = function () {
        $('body').loader('show');
    };

    /*Hide Loader*/
    var hideLoader = function (data) {
        if (isNaN(data)) {
            $('body').loader('hide');
            $('#ControlsContainer').html('<br />' +
                '<div class="alert alert-danger alert-dismissible fade in" role="alert">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Ocurrió un error al procesar el reporte, por favor intentelo de nuevo. <hr />' +
                '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
        } else if (data > 0) {
            $('body').loader('hide');
            $('#DownloadFileForm').submit();
            $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;El reporte muestra la última ubicación de un vehículo, en un día en específico.</div>');
        } else {
            $('body').loader('hide');
            $('#ControlsContainer').html('<div class="alert alert-warning alert-dismissible fade in" role="alert">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;No datos encontrados con los criterios de busqueda.</div>');
        }
    };

    /* Public methods */
    return {
        Init: initialize,
        OnClickDownloadReport: onClickDownloadReport,
        HideLoader: hideLoader,
        ShowLoader: showLoader
    };
})();