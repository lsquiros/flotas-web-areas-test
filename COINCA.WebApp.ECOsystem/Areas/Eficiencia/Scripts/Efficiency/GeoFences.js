﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.GeoFence = (function () {
    var i = 0;
    window.addEventListener("message", function (event) {
        $('#Polygon').val(event.data);
    });

    $('#detailContainer').off('click.btnAddAlarm', '#btnAddAlarm').on('click.btnAddAlarm', '#btnAddAlarm', function (e) {
        $('#alarmForm').find('#id').val($('#id').val());
        $('#alarmForm').submit();
        $(this).attr('disabled', 'disabled');
    });

    $('#detailContainer').off('click.btnguardar', '#btnguardar').on('click.btnguardar', '#btnguardar', function (e) {
        setErrorMsj('', false);
        if ($('#Name').val() == '' || $('#Description').val() == '' || $('#Polygon').val() == '') {
            if ($('#Polygon').val() == '') {
                setErrorMsj('<i class="mdi mdi-alert" style="font-size:18px;"></i>&nbsp;No se seleccionó el polígono', true);
            }
            else {
                setErrorMsj('<i class="mdi mdi-alert" style="font-size:18px;"></i>&nbsp;Nombre y Descripción son requeridos', true);
            }
        }
        else {
            setErrorMsj('', false);
            $('body').loader('show');
            $("#addOrEditForm").submit();
        }
    });

    function changehover() {
        if (i < 6) { 
            $('#menu-left-toggle').toggleClass('hover');
            i++;
        }
    };

    $(document).ready(function () {
        $("#LoadMenuForm").submit();
        window.setInterval(changehover, 1500);
    });

    //var AlarmSaveBtn = function () { $('#addOrEditAlarmForm').submit(); };

    $("#menu-left-toggle").click(function () {
        $("#menu-left").toggleClass("menu-hide");
        $("#map").toggleClass("map-menu-left-hide");
        $("#menu-left-toggle").toggleClass("map-menu-left-hide");

        if ($("#menu-left").hasClass("menu-hide")) {
            $("#left_icon").addClass("mdi-arrow-right-bold");
            $("#left_icon").removeClass("mdi-arrow-left-bold");
        } else {
            $("#left_icon").removeClass("mdi-arrow-right-bold");
            $("#left_icon").addClass("mdi-arrow-left-bold");
        }
    });


    $("#menu-right-toggle").click(function () {
        $("#menu-right").toggleClass("menu-hide");
        $("#menu-right-toggle").toggleClass("menu-right-hide");

        if ($("#menu-right").hasClass("menu-hide")) {
            $("#right_icon").addClass("mdi-arrow-left-bold");
            $("#right_icon").removeClass("mdi-arrow-right-bold");
        } else {
            $("#right_icon").removeClass("mdi-arrow-left-bold");
            $("#right_icon").addClass("mdi-arrow-right-bold");
        }
    });

    $('#btnSearchVehicleGroup').click(function () {
        SearchVehicleLeftPanel();
    });

    //Function that looks for the filter in the left panels
    function SearchVehicleLeftPanel() {
        var flag = 0;
        var text = $('#txtSearch').val();
        $(".checkboxGV").show("slow");

        $(".VehiclePanelRight").each(function (index) {
            if (text == '') {
                var gvname = $(this).attr("gvname");
                if (typeof gvname !== 'undefined') {
                    $(this).show("slow");
                }
            }
            else {
                var gvname = $(this).attr("gvname");

                if (typeof gvname !== 'undefined') {

                    if (gvname.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                        $(this).show("slow");
                    }
                    else {

                        $(this).find('.checkboxGV').each(function () {
                            var VHPlate = $(this).attr("VHPlate");
                            var VHName = $(this).attr("VHName");

                            if (typeof VHName !== 'undefined' && typeof VHPlate !== 'undefined') {

                                if (VHName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else if (VHPlate.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else {
                                    $(this).hide("slow");
                                }
                            }
                        });

                        //If the vehicle was found let the cost center visible
                        if (flag != 1) {
                            $(this).hide("slow");
                        }
                        else {
                            flag = 0;
                        }
                    }
                }
            }
        });
    };

    // Expand Left Cards
    $("#btnExpandLeft").click(function (e) {
        var expand = $(this).attr('expand');
        if (typeof expand !== typeof undefined && expand !== false) {
            $(".accordin-left").collapse("hide");
            $(this).removeAttr("expand");
            ChangeExpandIcon($("#btnExpandLeft i"), true);
        } else {
            $(".accordin-left").collapse("show");
            $(this).attr("expand", "expand");
            ChangeExpandIcon($("#btnExpandLeft i"), false);
        }
    });

    // Change Expand Button Icons
    function ChangeExpandIcon(icon, result) {
        if (result) {
            icon.removeClass("mdi-arrow-compress-all");
            icon.addClass("mdi-arrow-expand-all");
        } else {
            icon.removeClass("mdi-arrow-expand-all");
            icon.addClass("mdi-arrow-compress-all");
        }
    }

    function removeProcesingBar() {
        $("#processingLeftPanel").addClass("hide");
        $("#detailContainer").load("/GeoFences/CleanData");

    }

    function clean() {
        $("#detailContainer").load("/GeoFences/CleanData");
    }

    /* Show or hide errors messages*/
    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj').html(msj);
            $('#validationErrors').removeClass('hide');
        } else {
            $('#errorMsj').html('');
            $('#validationErrors').addClass('hide');
        }
    };

    var alarmMaintenanceModalShow = function () {
        $("#alarmForm").find('select').not("[custom='true']").select2();

        $('#alarmMaintenanceModal').on('shown.bs.modal', function (e) {

            $('.rangeValue').click(function () {
                //$("#colRango").find(":nth-child(2)").remove();
                var val = $(this).val();
                var id = $(this).attr('alarmtipoid');
                $('#demo_' + id).html(val);
                //$('#ldlCantVeces_' + id).html('Veces: ' + val);
                //$(this).removeClass('active');
            });

            $('.validateRepeat').click(function () {
                var id = $(this).attr('alarmtipoid');
                if ($('#Alarm_' + id).find('#txtAlarmPhone').val() == null ||
                    $('#Alarm_' + id).find('#txtAlarmPhone').val() == '' ||
                    $('#Alarm_' + id).find('#txtAlarmPhone').val() == '0000-0000') {
                    $(this).removeAttr('checked');
                    setAmountErrorMsj('El servicio para el envío de mensajes recurrentes, es por medio de SMS, por lo cual se debe indicar un número de celular para su activación.', true, 'e2');

                } else {
                    setAmountErrorMsj('', true, 'e2');
                    if ($(this).is(":checked")) {
                        $('#rngAlarmRepeatTimes_' + id).removeAttr('disabled');
                        $('#txtAlarmIntervalRepeat_' + id).removeAttr('disabled');
                    } else {
                        $('#rngAlarmRepeatTimes_' + id).attr('disabled', 'disabled');
                        $('#txtAlarmIntervalRepeat_' + id).attr('disabled', 'disabled');
                    }
                }
            });

            $('#btnSendAlarm').click(function () {
                var message = '';
                var error = false;
                var ids = $(this).attr('allIds');
                var items = ids.substring(1, ids.length - 1).split(",")
                var list = [];
                if (ids != null) {
                    for (var i = 0; i < items.length; i++) {
                        message += ValidateInputs(items[i]);
                        if (message != '') {
                            error = true;
                        }
                        list.push(GetValues(items[i]));
                    }
                    setAmountErrorMsj(message, true, 'e2');
                    if (!error) {
                        AddOrEditAlarmMaintenance(list);
                    }
                }
            });
        }).modal('show');

        $('#btnCloseAlarm').click(function () {
            alarmMaintenanceModalClose();
        });

        $('#btnCancelAlarm').click(function () {
            alarmMaintenanceModalClose();
        });
    };

    var ValidateInputs = function (alarmTriggerId) {
        var message = '';
        //if ($('#Alarm_' + alarmTriggerId).find('#chkAlarmActive').is(":checked")) {
        if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val() == null ||
            $('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val() == '') {

            if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() == null ||
                $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() == '') {
                message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Verifique que agregó al menos un correo o un número de celular. <br/>';
            }
            else if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val(), false)) {
                message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888<br/>';
            }
        }
        else if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val(), true)) {
            message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El correo debe cumplir el siguiente formato: ejemplo1@dominio.com. <br/>';
        } else {
            if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() != undefined &&
                $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() != null &&
                $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() != '') {
                if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val(), false)) {
                    message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888<br/>';
                }
            }
        }
        //}
        return message;
    }

    var verificarFormato = function (data, email) {
        var result = false;
        var list = data.replace(" ", "").split(";");
        var regex = email ? /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/ : /^([0-9\+\s\+\-])+$/;

        for (var item = 0; item < list.length; item++) {

            if (regex.test(list[item])) {
                result = true;
            }
            else {
                result = false;
                break;
            }
        }
        return result;
    }

    /* Show or hide errors messages*/
    var setAmountErrorMsj = function (msj, show, typeError) {

        typeError = typeError != null ? typeError : 'e1';

        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').removeClass('hide');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').addClass('hide');
                }
                break;
            default:
                break;
        }


    }

    var GetValues = function (alarmTriggerId) {
        var alarma = {};
        alarma.AlarmId = $('#Alarm_' + alarmTriggerId).attr('alarmid');
        alarma.AlarmTriggerId = alarmTriggerId;
        alarma.EntityId = $('#id').val();
        alarma.Email = $('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val();
        alarma.Phone = $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val();
        alarma.RepeatTimes = $('#Alarm_' + alarmTriggerId).find('#rngAlarmRepeatTimes_' + alarmTriggerId).val();
        alarma.IntervalRepeat = $('#Alarm_' + alarmTriggerId).find('#txtAlarmIntervalRepeat_' + alarmTriggerId).val();
        alarma.Active = ($('#Alarm_' + alarmTriggerId).find('#chkAlarmActive').is(":checked")) ? true : false;

        if ((alarma.Email == null || alarma.Email == "") && (alarma.Phone == null || alarma.Phone == "")) {
            alarma.Active = false;
        }

        if (!$('#Alarm_' + alarmTriggerId).find('#chkAlarmRepeat').is(":checked") || (alarma.Phone == null || alarma.Phone == "")) {
            alarma.RepeatTimes = 0;
            alarma.IntervalRepeat = 0;
        }

        return alarma;
    }

    var AddOrEditAlarmMaintenance = function (listAlarm) {
        //$('body').loader('show');
        $.ajax({
            url: '/Administration/AlarmMaintenance/AddOrEditAlarmMaintenance',
            type: 'POST',
            data: JSON.stringify({ pListAlarm: listAlarm }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                alarmMaintenanceModalClose();
            }
        });
    };

    var alarmMaintenanceModalClose = function () {
        $('#alarmMaintenanceModal').on('hide.bs.modal', function (e) {
            $('#btnAddAlarm').removeAttr('disabled');
        }).modal('hide');
    };

    /* Public methods */
    return {
        RemoveProcesingBar: removeProcesingBar,
        AlarmMaintenanceModalShow: alarmMaintenanceModalShow,
        AlarmMaintenanceModalClose: alarmMaintenanceModalClose
        //LoadMapRoute: loadMapRoute
    };
})();