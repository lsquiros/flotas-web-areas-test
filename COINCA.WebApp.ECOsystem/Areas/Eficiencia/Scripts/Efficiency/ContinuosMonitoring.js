﻿var ECO_Efficiency = ECO_Efficiency || {};
var vehiclesArray = [];
ECO_Efficiency.ContinousMonitoring = (function () {
    var options = {};
    var interval = null;

    window.addEventListener("message", receiveMessage, false);

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    function receiveMessage(event) {
        if (event.data.substring(0, 3) == '***') {
            var arr = event.data.replace('***', '').split('|');
            $('#txtSearcRightPanel').val(arr[0]);
            SearchRightPanel();
            $('#accordion-' + arr[1]).addClass("in");
            if ($("#wrapperLeft").hasClass("toggled") == true) {
                $("#wrapperLeft").toggleClass("toggled");
            }
        }
        else if (event.data.substring(0, 3) == '*Re') {
            window.location.href = $(location).attr('href').replace('/ContinuosMonitoring/Index/', '/Account/IndexControl?title=Eficiencia&header=1');
        }
        else {
            var obj = jQuery.parseJSON(event.data);
            var direccion;
            $.each(obj, function (i, item) {
                if (item.Direction == "" || item.Direction == null) { direccion = "N/A"; } else { direccion = item.Direction; }
                $("#speed-" + item.VehicleId).html(item.VSSSpeed + " km/h");
                $("#latlong-" + item.VehicleId).html(item.Latitude + ", " + item.Longitude);
                $("#travel-" + item.VehicleId).html(item.Distance + " km");
                $("#dateTime-" + item.VehicleId).html(item.Date + " " + item.Time);
                $("#location-" + item.VehicleId).html(item.location);
                $("#direction-" + item.VehicleId).html(direccion);
                if (item.HasCooler) {
                    $('#div-temperature-' + item.VehicleId).removeAttr('class');
                    $('#div-temperature-' + item.VehicleId).addClass(setTemperatureAlert(item.ReportId));
                    $("#temperature-" + item.VehicleId).html(item.TemperatureStr);
                    getXMLTemperature(item.SensorsXML, item.VehicleId);
                }

                $('#status-' + item.VehicleId).html(item.StatusStr);
                //if (item.Status == 1) {
                //    $('#status-' + item.VehicleId).html('<span class="label label-danger">Apagado</span>');
                //} else if (item.Status == 2) {
                //    $('#status-' + item.VehicleId).html('<span class="label label-default">Sin Transmitir</span>');
                //} else if (item.Status == 3) {
                //    $('#status-' + item.VehicleId).html('<span class="label label-success">Encendido</span>');
                //} else if (item.Status == 4) {
                //    $('#status-' + item.VehicleId).html('<span class="label label-warning">Detenido</span>');
                //}                
                if (item.ReportId == 10) {
                    $('#divBattery-' + item.VehicleId).removeClass('hide');
                    $('#divGps-' + item.VehicleId).addClass('hide');
                    $('#lowbattery-' + item.VehicleId).html(item.ReportIdDescription);
                }
                else if (item.ReportId == 12) {
                    $('#divGps-' + item.VehicleId).removeClass('hide');
                    $('#divBattery-' + item.VehicleId).addClass('hide');
                    $('#nogps-' + item.VehicleId).html(item.ReportIdDescription);
                } else if (item.ReportId == 200) {
                    $.title = "Nueva Alarma";
                    $('#panicbuttonplateid').html(item.PlateId + ' - ' + item.Name);
                    $('#PanicAlarmModal').modal('show');
                    $('#VehicleIdAlarm').val(item.VehicleId);
                }else{
                    $('#divBattery-' + item.VehicleId).addClass('hide');
                    $('#divGps-' + item.VehicleId).addClass('hide');
                }
            });
        }
    }

    var getXMLTemperature = function (xmlstring, vehicleid) {
        $.ajax({
            url: '/Eficiencia/ContinuosMonitoring/GetListFromXML',
            type: 'POST',
            data: JSON.stringify({ xml: xmlstring }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                var rows = '<table class="table table-striped"><tbody>';
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i = i + 3) {
                        var x = i + 1;
                        var y = 0;
                        rows = rows + '<tr><th><i class="mdi mdi-thermometer" data-toggle="tooltip" data-placement="right" title="Sensor #' + x + '"></i>(' + x + ')&nbsp;<span style="font-weight:normal;">' + data[i] + '°</span></th>';
                        if (i < data.length - 2) {
                            x = i + 2;
                            y = i + 3;
                            rows = rows + '<th><i class="mdi mdi-thermometer" data-toggle="tooltip" data-placement="right" title="Sensor #' + x + '"></i>(' + x + ')&nbsp;<span style="font-weight:normal;">' + data[i + 1] + '°</span></th>';
                            rows = rows + '<th><i class="mdi mdi-thermometer" data-toggle="tooltip" data-placement="right" title="Sensor #' + y + '"></i>(' + y + ')&nbsp;<span style="font-weight:normal;">' + data[i + 2] + '°</span></th>';
                        }
                        else if (i < data.length - 1) {
                            x = i + 2;
                            rows = rows + '<th><i class="mdi mdi-thermometer" data-toggle="tooltip" data-placement="right" title="Sensor #' + x + '"></i>(' + x + ')&nbsp;<span style="font-weight:normal;">' + data[i + 1] + '°</span></th>';
                        }
                        rows = rows + '</tr>';
                    }
                } else {
                    rows = rows + '<tr><th>No existen datos</th></tr>';
                }
                rows = rows + '</tbody></table>';
                $('#sensorstabledetail-' + vehicleid).html(rows);
            }
        });
    }

    var setTemperatureAlert = function (reportId) {
        switch (reportId) {
            case 31:
                return "HighTemperature";
            case 32:
                return "LowTemperature";
            default:
                return "";
        }
    }

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        $(window).resize(function () {
            resize();
        });

        $("#menu-left-toggle").click(function () {
            $("#menu-left").toggleClass("menu-hide");
            $("#map").toggleClass("map-menu-left-hide");
            $("#menu-left-toggle").toggleClass("map-menu-left-hide");

            if ($("#menu-left").hasClass("menu-hide")) {
                $("#left_icon").addClass("mdi-arrow-right-bold");
                $("#left_icon").removeClass("mdi-arrow-left-bold");
            } else {
                $("#left_icon").removeClass("mdi-arrow-right-bold");
                $("#left_icon").addClass("mdi-arrow-left-bold");
            }
        });

        $("#menu-right-toggle").click(function () {
            $("#menu-right").toggleClass("menu-hide");
            $("#menu-right-toggle").toggleClass("menu-right-hide");

            if ($("#menu-right").hasClass("menu-hide")) {
                $("#right_icon").addClass("mdi-arrow-left-bold");
                $("#right_icon").removeClass("mdi-arrow-right-bold");
            } else {
                $("#right_icon").removeClass("mdi-arrow-left-bold");
                $("#right_icon").addClass("mdi-arrow-right-bold");
            }
        });

        function resize() {
            var height = $(window).height() * -1;
            $('#divCCScroll').css('height', $(window).height() - 250);
            $('#divGVScroll').css('height', $(window).height() - 250);
        }

        $("#chkAllCC").click(function (e) {
            $(".btnCheckCC").prop('checked', $(this).is(":checked"));
            $(".checkboxCostCenter").prop('checked', $(this).is(":checked"));
        });

        $("#chkAllGrp").click(function (e) {
            $(".checkboxGroup").prop('checked', $(this).is(":checked"));
            $(".btnCheckGroup").prop('checked', $(this).is(":checked"));
        });

        // Expand Left Cards
        $("#btnExpandLeft").click(function (e) {
            var expand = $(this).attr('expand');
            if (typeof expand !== typeof undefined && expand !== false) {
                $(".accordin-left").collapse("hide");
                $(this).removeAttr("expand");
                ChangeExpandIcon($("#btnExpandLeft i"), true);
            } else {
                $(".accordin-left").collapse("show");
                $(this).attr("expand", "expand");
                ChangeExpandIcon($("#btnExpandLeft i"), false);
            }
        });

        // Expand Right Cards
        $("#btnExpandRight").click(function (e) {
            var expand = $(this).attr('expand');
            if (typeof expand !== typeof undefined && expand !== false) {
                $(".accordin-right").collapse("hide");
                $(this).removeAttr("expand");
                ChangeExpandIcon($("#btnExpandRight i"), true);
            } else {
                $(".accordin-right").collapse("show");
                $(this).attr("expand", "expand");
                ChangeExpandIcon($("#btnExpandRight i"), false);
            }
        });

        // Change Expand Button Icons
        function ChangeExpandIcon(icon, result) {
            if (result) {
                icon.removeClass("mdi-arrow-compress-all");
                icon.addClass("mdi-arrow-expand-all");
            } else {
                icon.removeClass("mdi-arrow-expand-all");
                icon.addClass("mdi-arrow-compress-all");
            }
        }

        $('#txtSearcRightPanel').keyup(function (event) {
            if (event.keyCode == '13') {
                SearchRightPanel();
            }
        });

        $('#txtSearcCostCenter').keyup(function (event) {
            if (event.keyCode == '13') {
                SearchCostCentersLeftPanel();
            }
        });

        $('#txtSearcGroup').keyup(function (event) {
            if (event.keyCode == '13') {
                SearchVehicleGruopsLeftPanel();
            }
        });

        $('#btnSearchCostCenter').click(function () {
            SearchCostCentersLeftPanel();
        });

        $('#btnSearchRightPanel').click(function () {
            SearchRightPanel();
        });

        $("#tabCostCenter").click(function () {
            $("input:checkbox").prop('checked', false);
        });

        $("#tabGroup").click(function () {
            $("input:checkbox").prop('checked', false);
        });

        $("#btnRefresh").click(function () {
            Search();
        });

        $('#btnSearchVehicleGroup').click(function () {
            SearchVehicleGruopsLeftPanel();
        });

        $('#btnAcceptAlarm').click(function () {
            var vehicleId = $('#VehicleIdAlarm').val();
            $.ajax({
                url: '/Eficiencia/ContinuosMonitoring/LogAlarmPanicButton',
                type: 'POST',
                data: JSON.stringify({ VehicleId: vehicleId }),
                contentType: 'application/json',
                success: function (data) {
                    $('#PanicAlarmModal').modal('hide');
                }
            });
        });
    };

    /*Init Charts*/
    var initCharts = function () {

        var labels = JSON.parse($("#Labels").val());
        if (labels.length > 12) {
            labels = $.merge(labels, ["", "", "", "", "", "", "", "", "", "", "", ""]);
        }

        var j = 0;
        $("canvas").each(function () {

            var ctx = this;

            var datasets = [];
            var data = JSON.parse($("#Data").val());
            var alpha = JSON.parse($("#AlphaColors").val());
            var highligh = JSON.parse($("#HighlightColors").val());
            var colors = JSON.parse($("#Colors").val());
            var titles = JSON.parse($("#Titles").val());

            for (var i = 0; i < data.length; i++) {
                datasets.push({
                    fillColor: alpha[i],
                    strokeColor: colors[i],
                    highlightFill: highligh[i],
                    highlightStroke: colors[i],
                    data: data[i].slice(j, j + 12),
                    title: titles[i],
                    pointColor: colors[i]
                });
            }

            new Chart($(this).get(0).getContext("2d")).Line({
                labels: labels.slice(j, j + 12),
                datasets: datasets
            },
                {
                    scaleLabel: "<%=ECO_Efficiency.ContinousMonitoring.FormatLabel(value)%>",
                    annotateLabel: "<%=ECO_Efficiency.ContinousMonitoring.FormatLabel(v3)%>",
                    inGraphDataTmpl: "<%=ECO_Efficiency.ContinousMonitoring.FormatLabel(v3)%>",
                    legendBlockSize: 20,
                    legendColorIndicatorStrokeWidth: 7,
                    drawXScaleLine: [{ position: "bottom", lineWidth: 1, lineColor: "black" }, { position: "0", lineWidth: 1, lineColor: "red" }],
                    scaleLineWidth: 1,
                    scaleLineColor: "black"

                });
            j += 13;
        });
        $("div[data-item]:not(:first)").removeClass("active");
    }

    /*format Label*/
    var formatLabel = function (v) {
        return ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToFloat(v));
    }

    function onSuccessLoadTemperatures(result) {

        $('#chartModal').modal('show');
        hideLoader();
        //initialize();
        //initCharts();
    }

    // Update Vehicles Array and Refresh Search
    function Search() {
        UpdateVehiclesArray();
        RefreshSearch();
    }

    // Update Vehicles Array to Search
    function UpdateVehiclesArray() {
        vehiclesArray = [];

        $('.checkboxGroup').each(function (i, obj) {
            if ($(this).is(":checked")) {
                vehiclesArray.push($(this).val());
            }
        });

        $('.checkboxCostCenter').each(function (i, obj) {
            if ($(this).is(":checked")) {
                vehiclesArray.push($(this).val());
            }
        });
    }

    // Refresh Search about last vehicle list
    function RefreshSearch() {
        var url = $("#mapFrame").attr("OriginalSrc") + "0|" + vehiclesArray.join();
        $("#mapFrame").attr("src", url);

        $("#divVehicleList").html("");
        showLoader();

        $.ajax({
            url: '/Eficiencia/ContinuosMonitoring/VehicleDetailsPanel',
            type: "GET",
            data: { VehicleIds: vehiclesArray.join() }
        }).done(function (partialViewResult) {
            $("#divVehicleList").html(partialViewResult);
            hideLoader();
        });;
    }

    $(".btnCheckCC").click(function (e) {
        e.stopPropagation();
        var checkBox = $(this);
        $('div#Centro-' + checkBox.attr("cc") + ' input[type=checkbox]').each(function () {
            $(this).prop('checked', checkBox.is(':checked'));
        });
    });

    $(".btnCheckGroup").click(function (e) {
        e.stopPropagation();
        var checkBox = $(this);
        $('div#Group-' + checkBox.attr("cc") + ' input[type=checkbox]').each(function () {
            $(this).prop('checked', checkBox.is(':checked'));
        });
    });

    //Function that looks for the information in the right panel
    function SearchRightPanel() {
        var text = $('#txtSearcRightPanel').val();
        if (text == '') {
            RefreshSearch();
        } else {
            $(".vehicleDetail").each(function (index) {
                var plate = $(this).attr("plate");
                (typeof plate !== 'undefined' && plate.toUpperCase().indexOf(text.toUpperCase()) < 0) ? $(this).hide("") : $(this).show("");
            });
        }
    }

    function SearchCostCentersLeftPanel() {
        var flag = 0;
        var text = $('#txtSearcCostCenter').val();
        $(".checkboxCC").show("slow");

        $(".CostCenterPanelRight").each(function (index) {
            if (text == '') {
                var CCCode = $(this).attr("CCCode");
                if (typeof CCCode !== 'undefined') {
                    $(this).show("slow");
                }
            }
            else {
                var CCCode = $(this).attr("CCCode");
                var CCName = $(this).attr("CCName");

                if (typeof CCCode !== 'undefined' && typeof CCName !== 'undefined') {

                    if (CCCode.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                        $(this).show("slow");
                    }
                    else if (CCName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                        $(this).show("slow");
                    }
                    else {

                        $(this).find('.checkboxCC').each(function () {
                            var VHPlate = $(this).attr("VHPlate");
                            var VHName = $(this).attr("VHName");

                            if (typeof VHName !== 'undefined' && typeof VHPlate !== 'undefined') {

                                if (VHName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else if (VHPlate.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else {
                                    $(this).hide("slow");
                                }
                            }
                        });

                        //If the vehicle was found let the cost center visible 
                        if (flag != 1) {
                            $(this).hide("slow");
                        }
                        else {
                            flag = 0;
                        }
                    }
                }
            }
        });
    }

    function SearchVehicleGruopsLeftPanel() {
        var flag = 0;
        var text = $('#txtSearcGroup').val();
        $('.checkboxGV').show("slow");
        $('.GroupVehiclePanelRight').each(function (index) {
            if (text == '') {
                var gvname = $(this).attr('gvname');
                if (typeof gvname !== 'undefined') {
                    $(this).show("slow");
                }
            }
            else {
                var gvname = $(this).attr("gvname");
                if (typeof gvname !== 'undefined') {
                    if (gvname.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                        $(this).show("slow");
                    }
                    else {
                        $(this).find('.checkboxGV').each(function () {
                            var VHPlate = $(this).attr("VHPlate");
                            var VHName = $(this).attr("VHName");

                            if (typeof VHName !== 'undefined' && typeof VHPlate !== 'undefined') {
                                if (VHName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else if (VHPlate.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else {
                                    $(this).hide("slow");
                                }
                            }
                        });

                        //If the vehicle was found let the cost center visible 
                        if (flag != 1) {
                            $(this).hide("slow");
                        }
                        else {
                            flag = 0;
                        }
                    }
                }
            }
        });
    }

    function refresh() {
        var timeout = 50000;
        var action = function () {
            $.ajax({
                url: '/Eficiencia/ContinuosMonitoring/submit',
                type: 'POST',
                success: function (data) { }
            });
        };
        setInterval(action, timeout);
    }

    /*Loader*/
    var showLoader = function () {
        $('body').loader('show');
    };

    /*Hide Loader*/
    var hideLoader = function () {
        $('body').loader('hide');
    };

    var showAvailableCommands = function (vehicleId) {
        if ($('#CommandContainer-' + vehicleId).css('display') == 'none') {
            $.ajax({
                url: '/Eficiencia/ContinuosMonitoring/GetAvailableCommands',
                type: 'POST',
                data: JSON.stringify({ VehicleId: vehicleId }),
                contentType: 'application/json',
                success: function (data) {
                    if (data == 'Error') {
                        showToast('Error al obtener los comandos para el vehículo.');
                    } else {
                        $('#CommandContainer-' + vehicleId).html(data);
                        $('#CommandContainer-' + vehicleId).show('slow');
                    }
                }
            });
        } else {
            $('#CommandContainer-' + vehicleId).hide('slow');
        }
    }

    var sendCommand = function (vehicleId, command) {
        $.ajax({
            url: '/Eficiencia/ContinuosMonitoring/SendCommand',
            type: 'POST',
            data: JSON.stringify({ VehicleId: vehicleId, Command: command }),
            contentType: 'application/json',
            success: function (data) {
                var msj = "";
                if (data) {
                    msj = "Comando enviado correctamente";
                    refreshCommandStatus();
                } else {
                    msj = "Error al ejecutar el comando";
                }
                showToast(msj);
                $('#CommandContainer-' + vehicleId).hide('slow');
            }
        });
    }

    var showToast = function (msj) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "500",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "slideDown",
            "hideMethod": "fadeOut"
        };
        toastr["info"](msj);
    }

    var refreshCommandStatus = function () {
        var timeout = 15000;
        interval = setInterval(function () {
            getCommandStatus();
        }, timeout);
    };

    var getCommandStatus = function () {
        $.ajax({
            url: '/Eficiencia/ContinuosMonitoring/GetCommandStatus',
            type: 'POST',
            contentType: 'application/json',
            success: function (data) {
                if (data.Status != "BLOCKED") {
                    showToast("El comando ha sido ejecutado");
                    clearInterval(interval);
                } else {
                    showToast("Comando en ejecución");
                }
            }
        });
    }

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoadTemperatures: onSuccessLoadTemperatures,
        FormatLabel: formatLabel,
        ShowAvailableCommands: showAvailableCommands,
        SendCommand: sendCommand
    };
})();
