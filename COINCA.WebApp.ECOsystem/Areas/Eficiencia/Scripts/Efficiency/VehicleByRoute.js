﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.VehicleByRoute = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        var select2 = $("#RouteId").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectChange(data);
                    return fn.apply(this, arguments);
                }
            }
        })(select2.onSelect);

        $('.connected').sortable({
            connectWith: '.connected'
        });

        $('#btnSubmit').off('click.btnSubmit').on('click.btnSubmit', function (e) {
   
            $('#ConnectedVehicles').find('input:hidden').each(function (i) {
                $(this).attr('name', 'ConnectedVehiclesList[' + i + '].VehicleId');
            });
            return true;
        });
    };

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
    };

    var onSelectChange = function (obj) {
 
        $('#id').val(obj.id);
        $("input:hidden[id='RouteId']").val(obj.id);

        if (obj.id === '') {
            $('#DragAndDropContainer').html('');
        } else {
            $('#loadForm').submit();
        }
    };

    var onClickBtnSave = function () {
        //alert("Se ha actualizado la asignación de días")
        var count = 0;
        var stringArray = [];
        $('#gridDetailContainer table > tbody  > tr > td').each(function () {
            if (this.className.indexOf('data-Id') > -1) {
                var value = '';
                if ($('#Sunday-' + this.textContent).is(':checked')) {
                    value = value + 'D';
                }
                if ($('#Monday-' + this.textContent).is(':checked')) {
                    value = value + 'L';
                }
                if ($('#Tuesday-' + this.textContent).is(':checked')) {
                    value = value + 'K';
                }
                if ($('#Wednesday-' + this.textContent).is(':checked')) {
                    value = value + 'M';
                }
                if ($('#Thursday-' + this.textContent).is(':checked')) {
                    value = value + 'J';
                }
                if ($('#Friday-' + this.textContent).is(':checked')) {
                    value = value + 'V';
                }
                if ($('#Saturday-' + this.textContent).is(':checked')) {
                    value = value + 'S';
                }
                value = $('#id').val() + '-' + this.textContent + "-" + value;
                stringArray[count] = value;
                count = count + 1;
            }
        });
        var postData = { values: stringArray };
        var uri = '/Eficiencia/VehicleByRoute/SaveList';
        //var uri = '/VehicleByRoute/SaveList';
        $.ajax({
            type: "POST",
            url: uri,
            //url: Efficiency.Utilities.GetUrl() + '/VehicleByRoute/SaveList',
            //url: '/VehicleByRoute/SaveList',
            data: postData,
            success: function (data) {
                $('#addOrEditModal').modal('hide');
            },
            dataType: "json",
            traditional: true
        });
    };

    var onClickBtnAdd = function () {
        var id = $('#s2id_RouteId').select2('data').id;
        var name = $('#s2id_RouteId').select2('data').text;

        if (id != '') {
            var inGrid = false;
            $('#gridDetailContainer table > tbody  > tr > td').each(function () {
                if (this.className.indexOf('data-Id') > -1) {
                    if (this.textContent == id) {
                        inGrid = tr;
                    }
                }
            });
            if (inGrid == false) {
                $('#gridDetailContainer table').find('.grid-empty-text').remove();
                var row = '';
                row = row + '<tr class="grid-row ">';
                row = row + '<td class="grid-cell" data-name="RouteName">' + name + '</td>';
                row = row + '<td class="grid-cell grid-col-hide data-Id" data-name="RouteId">' + id + '</td>';
                row = row + '<td class="grid-cell grid-col-center" data-name="Sunday"><input id="Sunday-' + id + '" name="Sunday-' + id + '" type="checkbox"></td>';
                row = row + '<td class="grid-cell grid-col-center" data-name="Monday"><input id="Monday-' + id + '" name="Monday-' + id + '" type="checkbox"></td>';
                row = row + '<td class="grid-cell grid-col-center" data-name="Tuesday"><input id="Tuesday-' + id + '" name="Tuesday-' + id + '" type="checkbox"></td>';
                row = row + '<td class="grid-cell grid-col-center" data-name="Wednesday"><input id="Wednesday-' + id + '" name="Wednesday-' + id + '" type="checkbox"></td>';
                row = row + '<td class="grid-cell grid-col-center" data-name="Thursday"><input id="Thursday-' + id + '" name="Thursday-' + id + '" type="checkbox"></td>';
                row = row + '<td class="grid-cell grid-col-center" data-name="Friday"><input id="Friday-' + id + '" name="Friday-' + id + '" type="checkbox"></td>';
                row = row + '<td class="grid-cell grid-col-center" data-name="Saturday"><input id="Saturday-' + id + '" name="Saturday-' + id + '" type="checkbox"></td>';
                row = row + '</tr>';
                $('#gridDetailContainer table').append(row);
            }
        }
    };

    var onClickDownloadReport = function () { 
       
        if (($("#mapFrame").contents().find("#summaryTable").length > 0))
        {
            $('#ExcelReportDownloadForm').submit(); 
        }
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadFileForm').submit();
    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad, 
        OnClickDownloadReport: onClickDownloadReport,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnClickBtnAdd: onClickBtnAdd,
        OnClickBtnSave: onClickBtnSave
    };
})();

