﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.VehicleReconstructing = (function () {
    var options = {};
    var fragmentPlaying = 0;
    var playingAll = '';
    var resetFragment = '';
    var cardPlaying = 0;

    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
    }

    var initEvents = function () {

        $('.accordionIconEffect').click(function () {
            if (!$('.accordin-left').hasClass('in')) {
                $(this).find('i').removeClass('mdi-plus');
                $(this).find('i').addClass('mdi-minus');
            }
            else {
                $(this).find('i').removeClass('mdi-minus');
                $(this).find('i').addClass('mdi-plus');
            }
        });

        $("#menu-left-toggle").click(function () {
            $("#menu-left").toggleClass("menu-hide");
            $("#map").toggleClass("map-menu-left-hide");
            $("#menu-left-toggle").toggleClass("map-menu-left-hide");

            if ($("#menu-left").hasClass("menu-hide")) {
                $("#left_icon").addClass("mdi-arrow-right-bold");
                $("#left_icon").removeClass("mdi-arrow-left-bold");
            } else {
                $("#left_icon").removeClass("mdi-arrow-right-bold");
                $("#left_icon").addClass("mdi-arrow-left-bold");
            }
        });


        $("#menu-right-toggle").click(function () {
            $("#menu-right").toggleClass("menu-hide");
            $("#menu-right-toggle").toggleClass("menu-right-hide");

            if ($("#menu-right").hasClass("menu-hide")) {
                $("#right_icon").addClass("mdi-arrow-left-bold");
                $("#right_icon").removeClass("mdi-arrow-right-bold");
            } else {
                $("#right_icon").removeClass("mdi-arrow-left-bold");
                $("#right_icon").addClass("mdi-arrow-right-bold");
            }
        });

        $("#txtSearchCostCenter").on('keydown', function (event) {
            var key = event.keyCode || event.charCode;

            if (key == 8 || key == 46) {
                if ($(this).val().length == 0) {
                    SearchCostCentersLeftPanel();
                }
            }
            if (key == 13) {
                SearchCostCentersLeftPanel();
            }
        });

        $("#txtSearchRightPanel").on('keydown', function (event) {
            var key = event.keyCode || event.charCode;

            if (key == 8 || key == 46) {
                if ($(this).val().length == 0) {
                    SearchVehicleGruopsLeftPanel();
                }
            }
            if (key == 13) {
                SearchVehicleGruopsLeftPanel();
            }
        });

        $('#btnSearchCostCenter').click(function () {
            SearchCostCentersLeftPanel();
        });


        $('#btnSearchVehicleGroup').click(function () {
            SearchVehicleGruopsLeftPanel();
        });


        // Expand Left Cards
        $("#btnExpandLeft").click(function () {
            var expand = $(this).attr('expand');
            if (typeof expand !== typeof undefined && expand !== false) {
                $(".accordin-left").collapse("hide");
                $(this).removeAttr("expand");
                ChangeExpandIcon($("#btnExpandLeft i"), true);
            } else {
                $(".accordin-left").collapse("show");
                $(this).attr("expand", "expand");
                ChangeExpandIcon($("#btnExpandLeft i"), false);
            }
        });

        $('#gridContainer').off('click.edit', 'a[car_detail]').on('click.edit', 'a[car_detail]', function (e) {
            var id = $(this).attr('id');
            var intrackref = $(this).attr('intrackref');

            $('#PlayingIntrackref').val(intrackref);
            $('#playAllIcon').removeClass('mdi-play');
            $('#playAllIcon').addClass('mdi-pause');
            $('#btnPlayAllStart').attr('play-pause', '0');

            showLoader();
            enableControls();
            RefreshSearch(id, intrackref);
        });

        $('#btnPlayAllStart').click(function () {
            ResetControls();
            var playpause = $(this).attr('play-pause');
            if (playingAll == '') {
                $('#ConfirmationPlayModal').modal('show');
            } else {
                ChangePlayAllBt(playpause);
                if (resetFragment == 'RS') {
                    var date = $('#StartDateStr').val();
                    var startDate = GetDates(true, new Date(date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2]), false);
                    var endDate = GetDates(true, new Date(date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2]), true);

                    returnResult(playpause, startDate, endDate, resetFragment);
                    resetFragment = '';
                } else {
                    returnResult(playpause, null, null, resetFragment);
                }
            }
        });

        //esm
        $('#btnCreateRoute').off('click.btnCreateRoute').on('click.btnCreateRoute', function () {
            $.ajax({
                url: '/Eficiencia/Reconstructing/LoadVehicleRoute',
                type: "POST",
                data: {}
            }).done(function (partialViewResult) {
                $("#ConfirmationPlayModal2").html(partialViewResult);
                $('#ConfirmationPlayModal2').modal('show');
            });
        });

        $('#btnglobalplay').click(function () {
            showLoader();
            var date = $('#StartDateStr').val();
            var playpause = $('#btnPlayAllStart').attr('play-pause');
            var intrackref = $('#PlayingIntrackref').val();
            playingAll = 'Playing';

            $('#playAllIcon').removeClass('mdi-play');
            $('#playAllIcon').addClass('mdi-pause');
            $('#btnPlayAllStart').attr('play-pause', '0');
            $('#ConfirmationPlayModal').modal('hide');

            var sdate = GetDates(true, new Date(date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2]), false);
            var edate = GetDates(true, new Date(date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2]), true);

            /*if ($('#chkMostrarRutaCompleta').prop('checked')) 
                var url = $("#mapFrame").attr("OriginalSrc") + intrackref + '|' + sdate + '|' + edate + '|PA|1';
            else */

            var url = $("#mapFrame").attr("OriginalSrc") + intrackref + '|' + sdate + '|' + edate + '|PA|0';
            
            $("#mapFrame").attr("src", url);
        });

        $('#btnGenRecons').click(function () {
            var date = $('#StartDateStr').val().split(' ')[0];
            var hour = $('#HourStr').val();
            $('#msjhour').hide();

            if (date == '') {
                $('#StartDateStr').focus();
                return;
            }

            var playpause = $('#btnPlayAllStart').attr('play-pause');
            $('#playAllIcon').addClass('mdi-play');
            $('#playAllIcon').removeClass('mdi-pause');
            $('#btnPlayAllStart').attr('play-pause', '0');
            showLoader();
            playingAll = '';
            var startdate = date.split('/');
            var sdate = startdate[1] + '/' + startdate[0] + '/' + startdate[2];

            //Hours 
            if (hour != '') sdate = validatesHour(sdate, hour);
            if (sdate == '') {
                $('#msjhour').show('slow');
                hideLoader();
                return;
            }

            var vehicleId = $('#PlayingVehicleId').val();
            var intrackref = $('#PlayingIntrackref').val();

            var sdate = GetDates(true, new Date(date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2]), false);
            var edate = GetDates(true, new Date(date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2]), true);

            var url = $("#mapFrame").attr("OriginalSrc") + intrackref + '|' + sdate + '|' + edate + '|LR|1';
            
            refreshRightPanel(vehicleId, sdate, GetDates(true, new Date(sdate), true), url, true);
        });

        InitPanelButtons();
    }

    var validatesHour = function (date, hour) {
        //Validates format
        var split = hour.split(':');
        var showmsj = true;

        if (split.length == 2) {
            if (!isNaN(split[0]) && split[0] <= 23) {
                if (!isNaN(split[1]) && split[0] <= 59) {
                    showmsj = false;
                }
            } 
        }

        if (showmsj) {
            return ''; 
        }
        return date + ' ' + hour;
    }

    var temperaturesChart =  function (obj) {        
        //ECOsystem.Utilities.ShowLoader();
        $('#loadTemperatureForm').find('#vehicleId').val($(obj).attr('vehicleid'));
        $('#loadTemperatureForm').submit();
    }

    function addOrEditDetailModalShow() {
        $('#ConfirmationPlayModal2').modal('show');
    };

    function closeRoutesModal() {
        $('#ConfirmationPlayModal2').modal('hide');
        if ($("#RedirectToRoutes").length) {
            var url = $("#RedirectToRoutes").val();
            location.href = url;
        }
    };

    function deleteModalClose() {
        $('#deleteStopModal').modal('hide');
    }

    function InitPanelButtons() {
        $('.btnPlayPause').click(function () {
            playingAll = '';
            var id = $(this).attr('id');
            var playpause = $(this).attr('play-pause');
            var startDate = null;
            var endDate = null;
            var intrackref = $('#PlayingIntrackref').val();
            var flag = false;
            cardPlaying = id;

            if (fragmentPlaying == 0 || fragmentPlaying != id) {
                showLoader();
                startDate = $('#' + id + '_StartDate').val();
                endDate = $('#' + id + '_EndDate').val();
                fragmentPlaying = id;
                ResetControls();
                $('#card_' + id).addClass('playingShadowCard');
                flag = true;
            }

            if (playpause == 0) {
                $(this).removeClass('mdi-play-circle');
                $(this).addClass('mdi-pause-circle');
                $(this).attr('play-pause', '1');
            } else if (playpause == 1) {
                $(this).addClass('mdi-play-circle');
                $(this).removeClass('mdi-pause-circle');
                $(this).attr('play-pause', '0');
            }

            if (flag) {
                var url = $("#mapFrame").attr("OriginalSrc") + intrackref + '|' + startDate + '|' + endDate + '|' + id + '|0';
                $("#mapFrame").attr("src", url);
            } else {
                if (resetFragment == 'RS') {
                    $('#card_' + id).addClass('playingShadowCard');
                    returnResult(playpause, startDate, endDate, resetFragment);
                    resetFragment = '';
                } else {
                    returnResult(playpause, startDate, endDate, resetFragment);
                }
            }
        });
    };

    function ChangePlayAllBt(playpause) {
        if (playpause == 0) {
            $('#playAllIcon').removeClass('mdi-play');
            $('#playAllIcon').addClass('mdi-pause');
            $('#btnPlayAllStart').attr('play-pause', '1');
        } else if (playpause == 1) {
            $('#playAllIcon').addClass('mdi-play');
            $('#playAllIcon').removeClass('mdi-pause');
            $('#btnPlayAllStart').attr('play-pause', '0');
        }
    };

    function ResetControls() {
        $('.mdi-pause-circle').each(function () {
            $(this).addClass('mdi-play-circle');
            $(this).removeClass('mdi-pause-circle');
            $(this).attr('play-pause', '0');
        });
        $('.playingShadowCard').each(function () {
            $(this).removeClass('playingShadowCard');
        });
    };

    function SearchCostCentersLeftPanel() {
        var text = $('#txtSearchCostCenter').val();
        if (text == '') {
            $(".CostCenterPanelRight").show("slow");
            $(".checkboxCC").show("slow");
        } else {
            $(".CostCenterPanelRight").each(function (index) {
                var panel = this;
                var CCName = $(panel).attr("CCName");

                if (typeof CCName !== 'undefined') {

                    if (CCName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                        $(panel).show("slow");
                        $(panel).find('.checkboxCC').show('slow');
                    } else {
                        var flag = 0;

                        $(panel).find('.checkboxCC').each(function (index2) {
                            var VHPlate = $(this).attr("VHPlate");
                            var VHName = $(this).attr("VHName");

                            if (typeof VHName !== 'undefined' && typeof VHPlate !== 'undefined') {

                                if (VHName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }
                                else if (VHPlate.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                }else {
                                    $(this).hide("slow");
                                }
                            }

                            if ($(panel).find('.checkboxCC').length - 1 == index2) {
                                //If the vehicle was found let the cost center visible
                                if (flag != 1) {
                                    $(panel).hide("slow");
                                } else {
                                    $(panel).show("slow");
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    function SearchVehicleGruopsLeftPanel() {
        var text = $('#txtSearchRightPanel').val();
        if (text == '') {
            $(".GroupVehiclePanelRight").show("slow");
            $(".checkboxGV").show("slow");
        } else {
            $('.GroupVehiclePanelRight').each(function (index) {
                var panel = this;
                var gvname = $(panel).attr("gvname");

                if (typeof gvname !== 'undefined') {
                    if (gvname.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                        $(panel).show("slow");
                        $(panel).find('.checkboxGV').show('slow');
                    } else {
                        var flag = 0;

                        $(panel).find('.checkboxGV').each(function (index2) {
                            var VHPlate = $(this).attr("VHPlate");
                            var VHName = $(this).attr("VHName");

                            if (typeof VHName !== 'undefined' && typeof VHPlate !== 'undefined') {

                                if (VHName.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                } else if (VHPlate.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                                    $(this).show("slow");
                                    flag = 1;
                                } else {
                                    $(this).hide("slow");
                                }
                            }

                            if ($(panel).find('.checkboxGV').length - 1 == index2) {
                                //If the vehicle was found let the cost center visible
                                if (flag != 1) {
                                    $(panel).hide("slow");
                                } else {
                                    $(panel).show("slow");
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    function ChangeExpandIcon(icon, result) {
        if (result) {
            icon.removeClass("mdi-arrow-compress-all");
            icon.addClass("mdi-arrow-expand-all");
        } else {
            icon.removeClass("mdi-arrow-expand-all");
            icon.addClass("mdi-arrow-compress-all");
        }
    }

    function RefreshSearch(id, intrackref) {
        var date = GetDates(false, new Date(), false);
        var mid = GetDates(true, new Date(), false);
        var url = $("#mapFrame").attr("OriginalSrc") + intrackref + '|' + mid + '|' + date + '|LR|1';
        $('#StartDateStr').val(mid.split('/')[1] + '/' + mid.split('/')[0] + '/' + mid.split('/')[2]);
        $('#PlayingVehicleId').val(id);
        $("#mapFrame").attr("src", url);
        
        refreshRightPanel(id, mid, date, '', false)
    }

    function refreshRightPanel(id, sdate, edate, url, useUrl) {
        $.ajax({
            url: '/Eficiencia/Reconstructing/VehicleDetailsPanel',
            type: "GET",
            data: { VehicleId: id, startdate: sdate, enddate: edate }
        }).done(function (partialViewResult) {
            $("#divVehicleList").html(partialViewResult);
            InitPanelButtons();
            if (useUrl) $("#mapFrame").attr("src", url);

            var container = $('#fragmentList');
            var scrollTo = $('.selectedFrag');
            if (scrollTo.offset() != undefined) {
                container.animate({
                    scrollTop: scrollTo.offset().top - 10 - container.offset().top + container.scrollTop()
                });
            }
            hideLoader();
        });
    };

    function receiveMessage(event) {
        var obj = jQuery.parseJSON(event.data);
        var direccion;
        if (obj.IsFinish && cardPlaying == obj.GPSId) {
            ChangePlayAllBt(1);
            ResetControls();
            resetFragment = 'RS';
        }
        if (obj.Direction == "" || obj.Direction == null) { direccion = "N/A"; } else { direccion = obj.Direction; }

        $("#speed-Vehicle").html(obj.VSSSpeed + " km/h");
        $("#dateTime-Vehicle").html(obj.Date + " " + obj.Time);
        $("#direction-Vehicle").html(direccion);
        $("#latlon").val(obj.Latitude + ', '+ obj.Longitude);

        if (obj.HasCooler) {
            $('#div-temperature-Vehicle').removeAttr('class');
            $('#div-temperature-Vehicle').addClass(ECOsystem.Utilities.SetTemperatureAlert(obj.ReportId));
            $("#temperature-Vehicle").html(obj.TemperatureStr);
            getXMLTemperature(obj.SensorsXML);
        }
        if (obj.Status == 1) {
            $('#status-Vehicle').html('<span class="label label-danger">Apagado</span>');
        } else if (obj.Status == 2) {
            $('#status-Vehicle').html('<span class="label label-default">Sin Transmitir</span>');
        } else if (obj.Status == 3) {
            $('#status-Vehicle').html('<span class="label label-success">Encendido</span>');
        } else if (obj.Status == 4) {
            $('#status-Vehicle').html('<span class="label label-warning">Detenido</span>');
        }

        if (obj.ReportId == 10) {
            $('#divBattery-vehicle').removeClass('hide');
            $('#divGPS-vehicle').addClass('hide');
            $('#lowbattery-vehicle').html(obj.ReportIdDescription);
        }
        else if (obj.ReportId == 12) {
            $('#divGPS-vehicle').removeClass('hide');
            $('#divBattery-vehicle').addClass('hide');
            $('#nogps-vehicle').html(obj.ReportIdDescription);
        } else {
            $('#divBattery-vehicle').addClass('hide');
            $('#divGPS-vehicle').addClass('hide');
        }
        hideLoader();
    }

    function getXMLTemperature(xmlstring) {
        $.ajax({
            url: '/Eficiencia/Reconstructing/GetListFromXML',
            type: 'POST',
            data: JSON.stringify({ xml: xmlstring }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                var rows = '<table class="table table-striped"><tbody>';
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i = i + 3) {
                        var x = i + 1;
                        var y = 0;
                        rows = rows + '<tr><th><i class="mdi mdi-thermometer" data-toggle="tooltip" data-placement="right" title="Sensor #' + x + '"></i>(' + x + ')&nbsp;&nbsp;<span style="font-weight:normal;">' + data[i] + '°</span></th>';
                        if (i < data.length - 2) {
                            x = i + 2;
                            y = i + 3;
                            rows = rows + '<th><i class="mdi mdi-thermometer" data-toggle="tooltip" data-placement="right" title="Sensor #' + x + '"></i>(' + x + ')&nbsp;&nbsp;<span style="font-weight:normal;">' + data[i + 1] + '°</span></th>';
                            rows = rows + '<th><i class="mdi mdi-thermometer" data-toggle="tooltip" data-placement="right" title="Sensor #' + y + '"></i>(' + y + ')&nbsp;&nbsp;<span style="font-weight:normal;">' + data[i + 2] + '°</span></th>';
                        }
                        else if (i < data.length - 1) {
                            x = i + 2;
                            rows = rows + '<th><i class="mdi mdi-thermometer" data-toggle="tooltip" data-placement="right" title="Sensor #' + x + '"></i>(' + x + ')&nbsp;&nbsp;<span style="font-weight:normal;">' + data[i + 1] + '°</span></th>';
                        }
                        rows = rows + '</tr>';
                    }
                } else {
                    rows = rows + '<tr><th>No existen datos</th></tr>';
                }
                rows = rows + '</tbody></table>';
                $('#sensorstabledetail').html(rows);
            }
        });
    }

    function returnResult(playpause, startdate, enddate, resetFragment) {
        var result = playpause + '|' + startdate + '|' + enddate + '|' + resetFragment;
        var targetframe = $('#mapFrame')[0].contentWindow;
        targetframe.postMessage(result, '*');
    }

    function GetDates(mid, date, plus) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        var hour = '00';
        var min = '00';
        var sec = '00';

        if (!mid) {
            hour = date.getHours();
            min = date.getMinutes();
            sec = date.getSeconds();
            if (hour < 10) { hour = '0' + hour }
            if (min < 10) { min = '0' + min }
            if (sec < 10) { sec = '0' + sec }
        }
        if (plus) {
            var lastDay = new Date(yyyy, mm, 0).toString().split(' ')[2];
            if (lastDay == dd) {
                mm = mm == 12 ? 1 : mm + 1;
                dd = "1";
                yyyy = mm == 1 ? yyyy + 1 : yyyy;
            } else {
                dd = dd + 1;
            }
        }
        if (parseInt(dd) < 10) { dd = '0' + dd }
        if (parseInt(mm) < 10) { mm = '0' + mm }
        date = mm + '/' + dd + '/' + yyyy + ' ' + hour + ':' + min + ':' + sec;
        return date;
    }

    function showLoader() {
        $('body').loader('show');
    }

    function hideLoader() {
        $('body').loader('hide');
    }

    function enableControls() {
        $('#StartDateStr').attr('disabled', false);
        $('#HourStr').attr('disabled', false);
        $('#btnGenRecons').attr('disabled', false);
        $('#btnPlayAllStart').attr('disabled', false);
        $('#btnCreateRoute').attr('disabled', false);

        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        //    .on('changeDate', function (e) {
        //    loadRoute(e);                           
        //});    
    }

    //function loadRoute(e) {
    //    var date = $('#StartDateStr').val();
    //    var playpause = $('#btnPlayAllStart').attr('play-pause');
    //    ChangePlayAllBt(playpause);
    //    showLoader();
    //    playingAll = '';
    //    var startdate = date.split('/');
    //    var sdate = startdate[1] + '/' + startdate[0] + '/' + startdate[2];
    //    var vehicleId = $('#PlayingVehicleId').val();
    //    var intrackref = $('#PlayingIntrackref').val();
    //    var url = $("#mapFrame").attr("OriginalSrc") + intrackref + '|0|0|LR';
    //    refreshRightPanel(vehicleId, sdate, GetDates(true, new Date(sdate), true), url, true);
    //    e.preventDefault();
    //};

    function initCreateRouteControls() {
        $('a[del_row]').click(function () {
            var $input = $(this);
            if ($input.hasClass('disabled'))
                return;

            var id = $input.attr('idstop');
            $('#deleteForm #idstop').val(id);
            $('#deleteStopModal').modal('show');
        });

        $.validator.unobtrusive.parse("#ConfirmationPlayModal2");

        $('#createRouteForm').submit(function (event) {
            if ($("#createRouteForm").valid()) {
                $('.delete-color').addClass('disabled');
                $('#modalFooter button').prop('disabled', true);
                $('#processing').removeClass('hide');                
            }
        });

        $('#saveRoute').off('click.saveRoute').on('click.saveRoute', function () {
            if ($("#createRouteForm").valid()) {
                $('#SaveStopModal').modal('show');
            }
        });

        $('#btnSaveRoute').off('click.btnSaveRoute').on('click.btnSaveRoute', function () {
            $('#doRedirect').val($('#showRoute:checked').length == 1);
            $('#SaveStopModal').modal('hide');
            $('#createRouteForm').trigger('submit');
        });
    }

    return {
        Init: initialize,
        ReceiveMessage: receiveMessage,
        AddOrEditDetailModalShow: addOrEditDetailModalShow,
        CloseRoutesModal: closeRoutesModal,
        DeleteModalClose: deleteModalClose,
        InitCreateRouteControls: initCreateRouteControls,
        TemperaturesChart : temperaturesChart
    };

})();