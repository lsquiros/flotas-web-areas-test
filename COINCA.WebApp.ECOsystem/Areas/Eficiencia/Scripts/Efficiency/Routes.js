﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.Routes = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        result();
        downloadFile();
    }


    var initEvents = function () {
        $('#ImportModal').on('hidden.bs.modal', function () {
            setErrorFormatMsj('', false);
        });

        $("#btnDownloadFile").click(function () {
            $("#DonwloadFileForm").submit();
            $('#InformationMessageModal').modal('hide');
        });

        $("#menu-left-toggle").click(function () {
            $("#menu-left").toggleClass("menu-hide");
            $("#map").toggleClass("map-menu-left-hide");
            $("#menu-left-toggle").toggleClass("map-menu-left-hide");

            if ($("#menu-left").hasClass("menu-hide")) {
                $("#left_icon").addClass("mdi-arrow-right-bold");
                $("#left_icon").removeClass("mdi-arrow-left-bold");
            } else {
                $("#left_icon").removeClass("mdi-arrow-right-bold");
                $("#left_icon").addClass("mdi-arrow-left-bold");
            }
        });


        $("#menu-right-toggle").click(function () {
            $("#menu-right").toggleClass("menu-hide");
            $("#menu-right-toggle").toggleClass("menu-right-hide");

            if ($("#menu-right").hasClass("menu-hide")) {
                $("#right_icon").addClass("mdi-arrow-left-bold");
                $("#right_icon").removeClass("mdi-arrow-right-bold");
            } else {
                $("#right_icon").removeClass("mdi-arrow-left-bold");
                $("#right_icon").addClass("mdi-arrow-right-bold");
            }
        });


        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function () {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });

        $('#btnImport').click(function () {
            $('#ImportModal').modal('show');
        });

        $('#btnDeletePoint').click(function () {
            var id = $(this).attr('pointid');
            var removeid = $(this).attr('removeid');
            removepoint(id, removeid);
            orderRows();
        });


        $('#btnSaveRoute').click(function () {
            $('#addOrEditRoutesForm').submit();
            $('#SaveNotification').modal('hide');
        });
    }

    var leftPanelInit = function () {
        //If user use cancel button is necesary unbind last click event.
        $('#detailContainer').find('.editpoint').unbind("click");
        $('#detailContainer').find('.btnDeletePoint').unbind("click");
        $('#detailContainer').find('#btnCancelSave').unbind("click");
        $('#detailContainer').find('#PointDetailContainer').find('#btnCancelPoint').unbind("click");
        $('#detailContainer').find('#PointDetailContainer').find('#btnPointAddOrEdit').unbind("click");

        $('#detailContainer').find('.editpoint').click(function () {
            showPointAddOrEdit($(this));
        });

        $('#detailContainer').find('.btnDeletePoint').click(function () {
            $('#deletePointModal').find('#btnDeletePoint').attr('pointid', $(this).attr('id'));
            $('#deletePointModal').find('#btnDeletePoint').attr('removeid', $(this).attr('removeid'));
        });

        $('#detailContainer').find('#btnCancelSave').click(function () {
            showLoader();
            window.location.reload();
        });

        $('#detailContainer').find('#PointDetailContainer').find('#btnCancelPoint').click(function () {
            $('#PointDetailContainer').hide('slow');
        });

        $('#detailContainer').find('#PointDetailContainer').find('#btnPointAddOrEdit').click(function () {
            pointsAddOrEdit();
        });

        $('#CommerceId').select2('data', {});  
        $('#CommerceId').select2({
            escapeMarkup: function (m) { return m; }
        });
    }

    var orderRows = function () {
        var num = 1;
        $('#detailContainer').find('#PointsContainer').find('.orderrow').each(function () {
            $(this).html(num);
            num++;
        });
    };

    var removepoint = function (id, removeid) {
        returnResult('d|' + id);
        $('#detailContainer').find('#PointsContainer').find('#pointsGrid').find('tr#' + removeid).remove();
        $('#deletePointModal').modal('hide');
    }

    //Results from Import 
    var result = function () {

        var resultdata = $('#ResultData').val();
        if (resultdata != '' && resultdata != undefined && resultdata != null) showresultmodal(resultdata);
    }


    var showresultmodal = function (resultdata) {
        $('#resultimportdate').html(resultdata.split('|')[0]);
        $('#resultadded').html(resultdata.split('|')[1]);
        $('#resultexist').html(resultdata.split('|')[2]);
        $('#resultfail').html(resultdata.split('|')[3]);
        $('#resulttotal').html(resultdata.split('|')[4]);
        $('#SummaryResultModal').modal('show');
    }
    var returnResult = function (data) {
        var targetframe = $('#mapFrame')[0].contentWindow;
        targetframe.postMessage(data, '*');
    }

    var updateDistance = function (routeDistance) {
        if (routeDistance != undefined) {
            $('#routeDistance').html('&nbsp;' + routeDistance + '&nbsp;Kilómetros')
        }
	}

	var updateTime = function (routeTime) {
		if (routeDistance != undefined) {
			$('#routeTime').html('&nbsp;' + routeTime + '&nbsp;Minutos')
		}
	}

    var receiveMessage = function (event) {
        var obj = event.data;
        if (event.data.Order != null && event.data.Order != '' && event.data.Order != undefined) {
            appendNewRow(obj);
        }

        if (event.data.distance != null) {
            updateDistance(event.data.distance);
		}

		if (event.data.time != null) {
			updateTime(event.data.time);
		}
    }

    var appendNewRow = function (obj) {
        $('#detailContainer').
            find('#PointsContainer').
            find('#pointsGrid').
            append('<tr class="grid-row" id="' + obj.Id + '" order="' + obj.Order + '" ><td class="grid-cell orderrow" data-name="Order">' + obj.Order + '</td>' +
            '<td class="grid-cell" style="cursor:pointer" onclick="openTooltip(' + obj.PointReference + ',' + obj.Order + ')"  data-name="Name" id="name-' + obj.Id + '">' + obj.Name + '</td>' +
            '<td class="grid-cell grid-col-btn" style="min-width:85px"  data-name="" id="' + obj.Id + '">' +
            '<span>' +
            '<i class="mdi mdi-32px mdi-checkbox-blank" style="position: absolute; color: white;"></i>' +
            '<a class="delete-color" style= "cursor:pointer" ' +
            ' onclick= "ECO_Efficiency.Routes.LoadMapRouteCenter(' + $('#RouteId').val() + ',' + obj.y + ',' + obj.x + ',' + obj.Id + ',' + obj.Order +
            ')" title= "Centrar" > <i class="mdi mdi-32px mdi-image-filter-center-focus"></i></a> ' +

            '<a href= "#" class="edit-color editpoint" id= "' + obj.Id + '" routeid= "' + obj.RouteId + '" iscommerce= "' + obj.IsCommerce + '" pname = "' + obj.Name + '" stoptime= "' + obj.StopTime + '" time= "' + obj.Hour + ':' + obj.Minute + '" ' +
            'order="' + obj.Order + '" newpoint="' + obj.NewPoint + '" pointreference="' + obj.PointReference + '" title="Editar"><i class="mdi mdi-32px mdi-pencil-box"></i></a>' +
            '<a href="#" class="delete-color btnDeletePoint" data-toggle="modal" data-target="#deletePointModal" id="' + obj.PointReference + '" removeId="' + obj.Id + '" title="Eliminar" newpoint="' + obj.NewPoint + '">' +
            '<i class="mdi mdi-32px mdi-close-box"></i></a></span></td></tr>');
        leftPanelInit();
        $('#PointsContainer')[0].scrollTop = $('#PointsContainer')[0].scrollHeight;
    }

    var pointsAddOrEdit = function () {
        var model = getPointDataToEdit();
        showLoader();
        $.ajax({
            type: "POST",
            url: '/Routes/EditPointRoutes',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ model: model }),
        }).success(function (data) {
            updaterow(data); //update right panel
            if (data.NewPoint != null && data.NewPoint != '' && data.NewPoint != undefined) {
                returnResult('a|' + data.SerializedObject);
            } else {
                //$('#PointsContainer').html(data);
                returnResult('r|' + data.SerializedObject);
            }
            $('#PointDetailContainer').hide('slow');
            //loadMapRouteData(model.RouteId, null, 0);
            leftPanelInit();
            hideLoader();
        });
    }   

    var updaterow = function (data) {
        var obj = $('#detailContainer').find('#PointsContainer').find('#pointsGrid').find('span').find('#' + data.PointId);
        obj.attr('pname', data.Name);
        obj.attr('stoptime', data.StopTime);
        obj.attr('time', data.Time);
        obj = $('#detailContainer').find('#PointsContainer').find('#pointsGrid').find('#name-' + data.PointId);
        obj.html(data.Name);

    }

    var getPointDataToEdit = function () {

        var model = {
            PointId: $('#detailContainer').find('#PointDetailContainer').find('#PointId').val(),
            RouteId: $('#detailContainer').find('#PointDetailContainer').find('#RouteId').val(),
            Name: $('#detailContainer').find('#PointDetailContainer').find('#Name').val(),
            StopTimeStr: $('#detailContainer').find('#PointDetailContainer').find('#StopTimeStr').val(),
            TimeStr: getTime24($('#detailContainer').find('#PointDetailContainer').find('#TimeStr').val()),
            PointReference: $('#detailContainer').find('#PointDetailContainer').find('#PointReference').val(),
            Order: $('#detailContainer').find('#PointDetailContainer').find('#Order').val(),
            NewPoint: $('#detailContainer').find('#PointDetailContainer').find('#NewPoint').val(),
            CommerceId: $('#detailContainer').find('#PointDetailContainer').find('#CommerceId').val()
        }
        return model;
    }

    var getTime24 = function (time) {
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        if (time.match(/\s(.*)$/) != null) {
            var AMPM = time.match(/\s(.*)$/)[1];
            if (AMPM == "PM" && hours < 12) hours = hours + 12;
            if (AMPM == "AM" && hours == 12) hours = hours - 12;
        }
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        return sHours + ":" + sMinutes;
    }

	var showPointAddOrEdit = function (obj) {
		if ($('#PointDetailContainer').is(":visible")) {
			return;
		}
        var model = {
            PointId: obj.attr('id'),
            RouteId: obj.attr('routeid'),
            Name: obj.attr('pname'),
            StopTime: obj.attr('stoptime'),
            Time: obj.attr('time'),
            Order: obj.attr('order'),
            NewPoint: obj.attr('newpoint'),
            PointReference: obj.attr('pointreference'),
            IsCommerce: obj.attr('iscommerce')
        }

        $.ajax({
            type: "POST",
            url: '/Routes/ShowPointsAddOrEdit',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ model: model }),
        }).success(function (data) {
            $('#PointDetailContainer').html(data);
            $('#PointDetailContainer').show('slow');
            leftPanelInit();
        });
    }

    var resetSrc = function () {
        var src = $("#mapFrame").attr('originalsrc') + '';
        $("#mapFrame").attr("src", src);
    }

    var deleteModalClose2 = function () {
        //$("#mapFrame").attr("src", "");
        resetSrc();

        $("#MsgNotification").removeClass("hide");
        $('#mapFrame').hide();
        ECOsystem.Utilities.DeleteModalClose();
    }

    var loadMapRouteCenter = function (route, latitude, longitude, pointid, order) {
        $.ajax({
            type: "POST",
            url: '/Routes/GetMapUrl',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ pRouteId: route }),
        }).success(function (data) {
            //data = data + "&latDirection=" + latitude + "&PageCenterPoint=1&lonDirection=" + longitude+"&IsRouteLoad=0";
            data = data + "&latDirection=" + latitude + "&PageCenterPoint=1&lonDirection=" + longitude + "&IsRouteLoad=0";
            showMapFrame(data);
            hideLoader();
        });
    }

    var loadMapRoute = function (val) {
        showLoader();
        $('#loadForm').find('#id').val(val);
        $('#loadForm').submit();

        $('.panel-group.card.route').removeClass("active");
        $('#route-' + val).addClass("active");


        var onFinal = function () {
            hideLoader();
            $("#menu-left-toggle").click();
        }

        loadMapRouteData(val, onFinal);

        //$.ajax({
        //    type: "POST",
        //    url: '/Routes/GetMapUrl',
        //    type: "POST",
        //    contentType: "application/json",
        //    data: JSON.stringify({ pRouteId: val }),
        //}).success(function (data) {
        //    showMapFrame(data);
        //    hideLoader();
        //    $("#menu-left-toggle").click();
        //});
    };

    var loadMapRouteData = function (val, onFinal, doRouteLoad) {
        $.ajax({
            type: "POST",
            url: '/Routes/GetMapUrl',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ pRouteId: val }),
        }).success(function (data) {
            if (doRouteLoad == undefined)
                doRouteLoad = 1
            data = data + "&IsRouteLoad=" + doRouteLoad;
            showMapFrame(data);
            if (onFinal != undefined)
                onFinal();
        });
    }

    var loadMapRouteFromReconstructing = function (val) {
        loadMapRoute(val);

        // search list
        $('#route-' + val).focus();
        $('#route-' + val).select();

        var container = $('#gridContainer'),
            scrollTo = $('#route-' + val);

        container.animate({
            scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
        });
    }

    var clearRoutesData = function () {
        $("#MsgNotification").removeClass("hide");
        $('#mapFrame').hide();
        $("#detailContainer").load("/Routes/CleanData");
    };

    var addOrEditDetailShow = function () {
        var id = $('#loadForm').find('#id').val();

        if (id < 0) {
            $('#mapFrame').hide();
            $("#MsgNotification").removeClass("hide");
        }
        leftPanelInit();
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
        $('#menu-right').removeClass('hide');
        $('#menu-right-toggle').removeClass('hide');

        if ($("#menu-right-toggle").hasClass("menu-right-hide")) $("#menu-right-toggle").toggleClass("menu-right-hide");
        if ($('#menu-right').hasClass("menu-hide")) $('#menu-right').toggleClass("menu-hide");
    };

    var updateRoutes = function (data) {
        hideLoader();
        var id = $('#loadForm').find('#id').val();

        ///////////////////////////////////////////
        //Update route
        if (id > 0) {
            $('#gridContainer').find("#route-" + data.RouteId).find("strong").html(data.Name);
            returnResult('s|' + data.RouteId);
            return true;
        }

        ///////////////////////////////////////////
        //Insert route
        //$('#RouteId').val(data.RouteId);
        
        $('#gridContainer').append('<div class="panel-group card route active" id="route-' + data.RouteId + '" style="cursor:pointer">' +
            '<div class="row" style="margin-top:-5px;margin-bottom:-10px;">' +
            '<div class="col-sm-8"  onclick="ECO_Efficiency.Routes.LoadMapRoute(' + data.RouteId + ')">' +
            '<strong>' + data.Name + '</strong>' +
            '</div>' +
            '<div class="col-sm-4">' +
            '<a href="#" class="pull-right" data-toggle="modal" data-target="#deleteModal" id="@item.RouteId" del_row>' +
            '<i class="mdi mdi-close-box mdi-24px" style="color:red;"></i>' +
            '</a></div></div>');

        ECO_Efficiency.Routes.LoadMapRoute(data.RouteId);
        //$('#loadForm').find('#id').val(data.RouteId)
        //$('#loadForm').submit();
        //showMapFrame(data.MapUrl);
    };

    var showMapFrame = function (data) {
        var src = $("#mapFrame").attr('originalsrc') + data;
        $("#mapFrame").attr("src", src);
        $("#MsgNotification").addClass("hide");
        $('#mapFrame').show();
    }

    var onClickImport2 = function () {
        $('#ImportCSV').trigger('click');
        $("#txtFile").val("");
    };

    var onClickImport3 = function () {
        var val = $('#ImportCSV').val();

        if (val == "") {
            setErrorFormatMsj('Por favor seleccione un archivo', true);
            return;
        }
        $('body').loader('show');
        $('#ImportForm').submit();
    };

    var downloadFile = function () {
        $('#DonwloadFileForm').submit();
    };

    var getFilename = function (input) {
        if (ValidateFormat(input)) {
            if (input.files && input.files[0]) {
                var value = $("#ImportCSV").val();
                var NameOfFile = input.files[0].name;
                $("#txtFile").val(NameOfFile);
                setErrorFormatMsj('', false);
            }
        }
        else {
            $("#txtFile").val("");
            setErrorFormatMsj('* El formato de archivo es inválido o el tamaño del archivo sobrepasa lo permitido, favor seleccionar un archivo de formato CSV con un tamaño menor a 45KB (Equivalente a 500 líneas). Se sugiere dividir el archivo para poder subirlo por partes.', true);
        }
    }

    var ValidateFormat = function (input) {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var files = input.files;
            for (var i = 0, f; f = files[i]; i++) {
                if (!f.name.match('\.csv') || (f.size > 46030)) {
                    return false;
                }
                else {
                    return true;
                }
            }
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.', true);
        }
    };

    var setErrorFormatMsj = function (msj, show) {
        if (show) {
            $('#errorFormatMsj').html(msj);
            $('#validationFormatErrors').removeClass('hide');
        } else {
            $('#errorFormatMsj').html('');
            $('#validationFormatErrors').addClass('hide');
        }
    };

    var ImportModalHide = function () {
        $('#ImportModal').modal('hide');
        AddOrEditDetailModalShow();
        hideLoader();
    };

    var showLoader = function () {
        $('body').loader('show');
    }

    var hideLoader = function () {
        $('body').loader('hide');
    }

    return {
        Init: initialize,
        DeleteModalClose2: deleteModalClose2,
        LoadMapRoute: loadMapRoute,
        ClearRoutesData: clearRoutesData,
        AddOrEditDetailShow: addOrEditDetailShow,
        UpdateRoutes: updateRoutes,
        OnClickImport2: onClickImport2,
        OnClickImport3: onClickImport3,
        DownloadFile: downloadFile,
        GetFilename: getFilename,
        DownloadFile: downloadFile,
        ShowLoader: showLoader,
        ReceiveMessage: receiveMessage,
        LoadMapRouteFromReconstructing: loadMapRouteFromReconstructing,
        LoadMapRouteCenter: loadMapRouteCenter
    };
})();
