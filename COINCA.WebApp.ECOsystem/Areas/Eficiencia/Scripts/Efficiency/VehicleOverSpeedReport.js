﻿var ECO_Efficiency = ECO_Efficiency || {};

ECO_Efficiency.VehicleOverSpeedReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initializeDropDownList();
        initEvents();        
    }

    /*init Events*/
    var initEvents = function () {
        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) {

            $('#datetimepickerEndDate').datepicker('remove');

            var dateV = $('#Parameters_StartDateStr').val();
            var params = dateV.split("/");
            var days = parseInt(params[0]) + 2;
            var maxDateV = days + "/" + params[1] + "/" + params[2];

            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: dateV,
                startDate: dateV,
                endDate: maxDateV,
                format: 'dd/mm/yyyy',
                defaultViewDate: dateV,
                autoclose: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });

            $("#datetimepickerEndDate").datepicker('update');

            if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects())
                $('#ReloadForm').submit();
            else
                invalidSelectOption();
        });

        $("#Parameters_StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $("#Parameters_EndDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        //Init Dates based on the results
        var cList = $('#cList').val();
        if (cList == 0) initDatePickerTime();
    }

    var initDatePickerTime = function () {
        $('#datetimepickerStartDate').datepicker("setDate", new Date());
        $('#datetimepickerEndDate').datepicker("setDate", new Date());
    } 

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select4 = $("#Parameters_FilterVehicle").select2({ formatResult: vehiclesSelectFormat, formatSelection: vehiclesSelectFormat, escapeMarkup: function (m) { return m; } }).data('select2');
        select4.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectVehiclesChange(data); return fn.apply(this, arguments); }
            }
        })(select4.onSelect);

        var select5 = $("#Parameters_ReportCriteriaId").select2({ formatResult: FilterSelectFormat, formatSelection: FilterSelectFormat, escapeMarkup: function (m) { return m; } }).data('select2');
        select5.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectFilterChange(data); return fn.apply(this, arguments); }
            }
        })(select5.onSelect);

    };

    /*on Select Status Change*/
    var onSelectVehiclesChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_FilterVehicle').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*on Filter Change*/
    var onSelectFilterChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_ReportCriteriaId').val(obj.id);
            if (checkAllSelects()) $('#ReloadForm').submit();
        }
    };

    /*Check the dropdown selection*/
    var checkAllSelects = function () {
        if (($('#Parameters_StartDateStr').val() !== '')
            && ($('#Parameters_EndDateStr').val() !== '')
            && ($('#Parameters_FilterVehicle').val() !== '')
            && ($('#Parameters_ReportCriteriaId').val() !== ''))
        {
            var plateid = $("#Parameters_FilterVehicle option:selected").text();
            var val = plateid.split(',')[0].split(':')[1];
            $('#ReloadForm').find('#Parameters_key').val(val);
            showLoader();
            return true;
        }
        else {
            invalidSelectOption();
            return false;
        } 

        return true;
    };

    /*Format for the vehicle dropdown*/
    var vehiclesSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.PlateId + '</div>' +
                '</div>';
    };

    /*Format for the filter dropdown*/
    var FilterSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
                '</div>';
    };

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                           ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                '<span aria-hidden="true">×</span>' +
                                            '</button>' +
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    /*Excecute the excel download*/
    var onClickDownload = function () {
        showLoader();
        $('#ExcelReportDownloadForm').submit();
    };

    /*Loader*/
    var showLoader = function () {
        $('body').loader('show');
    };

    /*Hide Loader*/
    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadFileForm').submit();        
    };
    
    var onClickFirstDate = function () {        
        $('#Parameters_EndDateStr').val('');
    };

    /* Public methods */
    return {
        Init: initialize,
        OnClickDownload: onClickDownload,
        HideLoader: hideLoader,
        ShowLoader: showLoader,
        OnClickFirstDate: onClickFirstDate
    };
})();