﻿/************************************************************************************************************
*  File    : VehicleByRouteDetailModels.cs
*  Summary : VehicleByRouteDetail Models
*  Author  : Danilo Hidalgo
*  Date    : 11/20/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// VehicleByRouteDetail Base
    /// </summary>
    public class VehicleByRouteDetailBase
    {
        /// <summary>
        /// VehicleByRouteDetail Constructor
        /// </summary>
        public VehicleByRouteDetailBase()
        {
            Data = new VehicleByRouteDetail();
            List = new List<VehicleByRouteDetail>();
        }
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleByRouteDetail Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleByRouteDetail> List { get; set; }
    }

    /// <summary>
    /// VehicleByRouteDetail Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class VehicleByRouteDetail : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? RouteId { get; set; }

        /// <summary>
        /// Days asigned to route
        /// </summary>
        [NotMappedColumn]
        public string Days { get; set; }

        /// <summary>
        /// Name of the route
        /// </summary>
        [DisplayName("Ruta")]
        [GridColumn(Title = "Ruta", Width = "180px", SortEnabled = true)]
        public string RouteName { get; set; }

        /// <summary>
        /// Sunday day
        /// </summary>
        [DisplayName("dom")]
        [NotMappedColumn]
        public bool Sunday { get; set; }

        /// <summary>
        /// Monday day
        /// </summary>
        [DisplayName("lun")]
        [NotMappedColumn]
        public bool Monday { get; set; }

        /// <summary>
        /// Tuesday day
        /// </summary>
        [DisplayName("mar")]
        [NotMappedColumn]
        public bool Tuesday { get; set; }

        /// <summary>
        /// Wednesday day
        /// </summary>
        [DisplayName("mie")]
        [NotMappedColumn]
        public bool Wednesday { get; set; }

        /// <summary>
        /// Thursday day
        /// </summary>
        [DisplayName("jue")]
        [NotMappedColumn]
        public bool Thursday { get; set; }

        /// <summary>
        /// Friday day
        /// </summary>
        [DisplayName("vie")]
        [NotMappedColumn]
        public bool Friday { get; set; }

        /// <summary>
        /// Saturday day
        /// </summary>
        [DisplayName("sab")]
        [NotMappedColumn]
        public bool Saturday { get; set; }

    }

}
