﻿/************************************************************************************************************
*  File    : ReconstructingModels.cs
*  Summary : Reconstructing Models
*  Author  : Danilo Hidalgo
*  Date    : 11/04/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using GridMvc.DataAnnotations;
using System;
using System.Linq;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// RealTime Base
    /// </summary>
    public class ReconstructingBase
    {
        /// <summary>
        /// ReconstructingBase Constructor
        /// </summary>
        public ReconstructingBase()
        {
            Data = new Reconstructing();
            List = new List<Reconstructing>();
            Menus = new List<AccountMenus>();
        }
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Reconstructing Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Reconstructing> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }        

        /// <summary>
        /// 
        /// </summary>
        public int TransmissionTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime StartDate { get; set; }
    }

    /// <summary>
    /// RealTime Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Reconstructing : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int IntrackReference { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string CountryCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int UserTimeZone { get { return 0.ToTimeOffSet(); } set { 0.ToTimeOffSet(); } }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Vehículo", Width = "120px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// Plate Id or number for the Vehicle
        /// </summary>
        [DisplayName("Matrícula")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Matrícula", Width = "50px", SortEnabled = true)]
        public string PlateId { get; set; }

        /// <summary>
        /// FK CostCenterId from SubUnit
        /// </summary>
        [DisplayName("Centro de Costo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int CostCenterId { get; set; }

        /// <summary>
        /// SubUnit Name
        /// </summary>
        [DisplayName("Centro de Costo")]
        [GridColumn(Title = "Centro de Costo", Width = "50px", SortEnabled = true)]
        public string CostCenterName { get; set; }


        /// <summary>
        /// FK User Id from Users, each vehicle is associate with one user
        /// </summary>
        [DisplayName("Conductor")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int UserId { get; set; }

        /// <summary>
        /// UserName for UserId
        /// </summary>
        [DisplayName("Conductor Asignado")]
        [GridColumn(Title = "Conductor Asignado", Width = "100px", SortEnabled = true)]
        public string UserNameDecrypt { get { return TripleDesEncryption.Decrypt(UserName); } }

        [NotMappedColumn]
        public string UserName { get; set; }

        /// <summary>
        /// FK VehicleCategoryId from VehicleCategories each vehicle is associate with one vehicle category
        /// </summary>
        [DisplayName("Clase de Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int VehicleCategoryId { get; set; }

        /// <summary>
        /// Category type/description
        /// </summary>
        [DisplayName("Clase")]
        [GridColumn(Title = "Clase", Width = "50px", SortEnabled = true)]
        public string CategoryType { get; set; }


        /// <summary>
        /// Fuel Name for the vehicle
        /// </summary>
        [DisplayName("Combustible")]
        [NotMappedColumn]
        public string FuelName { get; set; }

        /// <summary>
        /// Is Active?
        /// </summary>
        [DisplayName("Activo")]
        [NotMappedColumn]
        public bool Active { get; set; }

        /// <summary>
        /// Vehicle Colour
        /// </summary>
        [DisplayName("Color")]
        [NotMappedColumn]
        public string Colour { get; set; }

        /// <summary>
        /// Vehicle year
        /// </summary>
        [DisplayName("Año")]
        [NotMappedColumn]
        public int? Year { get; set; }

        /// <summary>
        /// Vehicle chassis
        /// </summary>
        [DisplayName("Chasis")]
        [NotMappedColumn]
        public string Chassis { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int VehicleGroupId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string VehicleGroupName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double? Score { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ScoreStr
        {
            get { return Score == null ? "N/A" : Miscellaneous.GetNumberFormat((decimal)Score); }
        }

        /// <summary>
        /// Name of current entity
        /// </summary>         
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }

        public double VSSSpeed { get; set; }

        public double Distance { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int Status { get; set; }
          
        public string StatusStr
        {
            get
            {
                switch (Status)
                {
                    case 1:
                        string text1 = "<span class=/'label label-danger/'>Apagado</span>";
                        string htmlEncoded1 = HttpContext.Current.Server.HtmlEncode(text1);
                        string original1 = HttpContext.Current.Server.HtmlDecode(htmlEncoded1);
                        return original1;
                    case 2:
                        string text2 = "<span class='label label-default'>Sin Transmitir</span>";
                        string htmlEncoded2 = HttpContext.Current.Server.HtmlEncode(text2);
                        string original2 = HttpContext.Current.Server.HtmlDecode(htmlEncoded2);
                        return original2;
                    case 3:
                        string text3 = "<span class='label label-success'>Encendido</span>";
                        string htmlEncoded3 = HttpContext.Current.Server.HtmlEncode(text3);
                        string original3 = HttpContext.Current.Server.HtmlDecode(htmlEncoded3);
                        return original3;
                    case 4:
                        string text4 = "<span class='label label-warning'>Detenido</span>";
                        string htmlEncoded4 = HttpContext.Current.Server.HtmlEncode(text4);
                        string original4 = HttpContext.Current.Server.HtmlDecode(htmlEncoded4);
                        return original4;
                    default:
                        return "";
                }
            }
        }

        public string Date { get; set; }

        public string Time { get; set; }

        public string DateTime
        {
            get
            {
                return Date + " " + Time;

            }
        }

        /// <summary>
        /// Type of search in combo SeeRoutes    
        /// </summary>
        [DisplayName("Trayecto del Vehículo")]
        [NotMappedColumn]
        public int SearchType { get; set; }

        /// <summary>
        /// VehicleName 
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// FragmentList
        /// </summary>
        public IEnumerable<VehicleReconstructing> FragmentList { get; set; }

        public string Temperature { get; set; }

        public string TemperatureStr { get { return HasCooler ? Temperature == "No Disponible" ? Temperature : string.Format("{0}°", Temperature) : Temperature; } }

        public bool HasCooler { get; set; }

        public int? ReportId { get; set; }

        public string SensorsXML { get; set; }

        public string ReportIdImage { get; set; }

        public string ReportIdDescription { get; set; }
    }

    public class VehicleReconstructing
    {
        public int GPSId { get; set; }

        public int VehicleId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string OverSpeedSummary { get; set; }

        public List<string> OverSpeedSummaryFixed
        {
            get
            {
                var list = OverSpeedSummary.Split(',').ToList();
                if (string.IsNullOrEmpty(OverSpeedSummary)) list.Add("G");
                return list;
            }
        }

        public string Status { get; set; }

        public int MaxSpeed { get; set; }

        public double Odometer { get; set; }

        public int Stops { get; set; }

        public string Location { get; set; }

        public string Latitud { get; set; }

        public string Longitud { get; set; }

        public decimal MinimumDistanceBetweenPoints { get; set; }

        public decimal MinimumStopTime { get; set; }

        public bool Selected { get; set; }
    }
}


