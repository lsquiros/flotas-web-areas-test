﻿using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ECO_Efficiency.Models
{
    
    public class GeofencesReport
    {
        [NotMapped]
        public int VehicleId { get; set; }

        public string VehicleName { get; set; }

        public string PlateId { get; set; }

        [NotMapped]
        public string EncryptDriverName { get; set; }

        public string DecryptDriverName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptDriverName); } }

        public string EventType { get; set; }

        [NotMapped]
        public DateTime? Date {get; set; }

        public string DateFormatted
        {
            get { return Miscellaneous.GetUtcDateTimeFormat(Date); }
            set { Date = Miscellaneous.SetDate(value); }
        }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public string Location { get; set; }

        public string GeofenceName { get; set; }

        [NotMapped]
        public string EncryptCustomerName { get; set; }

        public string DecryptCustomerName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptCustomerName); } }

        public string Dates { get; set; }

        public string CostCenterName { get; set; }

        public string VehicleGroupName { get; set; }
    }
}