﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ECO_Efficiency.Models
{
    public class ReconstructingRouteModel
    {
        public ReconstructingRouteModel()
        {
            VehicleStops = new List<VehicleStop>(); 
        }
        [Required(ErrorMessage = "{0} es requerido")]
        public string Name { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public string Description { get; set; }

        public List<VehicleStop> VehicleStops { get; set; }

        public int routeId { get; set; }
    }

    public class VehicleStop
    {
        public int Id { get; set; }
        public int Index { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public TimeSpan StopDuration { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }

        public string StopDurationInHours
        {

            get { return StopDuration.ToString(@"hh\:mm"); }
        } 

        public string HoursFromStartDate
        {
            get { return Convert.ToDateTime(StartDate).ToString(@"HH\:mm tt"); }
        }
    }
}