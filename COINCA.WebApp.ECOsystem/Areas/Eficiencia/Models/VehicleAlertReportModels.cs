﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// VehicleAlertReportModelBase
    /// </summary>
    public class VehicleAlertReportModelBase
    {
        public VehicleAlertReportModelBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<VehicleAlertReport>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleAlertReport> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    public class VehicleAlertReport 
    {
        public string Name { get; set; }

        public DateTime? Date { get; set; }

        public string Address { get; set; }

        public int ReportId { get; set; }

        public string Description { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string EncryptDriverName { get; set; }

        public string DecryptDriverName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptDriverName); } }

        public int CostCenterId { get; set; }

        public string CostCenterName { get; set; }

        public string VehicleGroupName { get; set; }
    }
}