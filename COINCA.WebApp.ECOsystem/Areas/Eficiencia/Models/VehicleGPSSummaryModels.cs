﻿using ECO_Efficiency.Models;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Efficiency.Models
{
    public class VehicleGPSSummaryBase
    {
        public VehicleGPSSummaryBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<VehicleGPSSummary>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleGPSSummary> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    public class VehicleGPSSummary
    {
        public int CustomerId { get; set; }

        public int VehicleId { get; set; }

        public string VehicleName { get; set; }

        public string PlateId { get; set; }

        public string CostCenter { get; set; }

        public int MaxSpeed { get; set; }

        public decimal AvgSpeed { get; set; }

        public string OverSpeedSummary { get; set; }
                
        public int OnCount { get; set; }

        public int Stops { get; set; }

        public int AbruptTurns { get; set; }

        public int SuddenStops { get; set; }

        public int QuickAccelerations { get; set; }

        public decimal? AdminSpeed { get; set; }

        public string LapseOn { get; set; }

        public string LapseOff { get; set; }

        public string AuthorizedTime { get; set; }

        public string UnAuthorizedTime { get; set; }

        public string EncryptDriverName { get; set; }

        public string DecryptDriverName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptDriverName);  } }

        public decimal Kilometers { get; set; }

        public string CostCenterName { get; set; }

        public string VehicleGroupName { get; set; }

        public int OverSpeedCount { get; set; }

        public int DayOW { get; set; }

        public string StartOn { get; set; }

        public string EndOff { get; set; }

        public string SlowMotion { get; set; }

        public decimal TemperatureAVG { get; set; }

        public DateTime StartDate { get; set; }

    }
}