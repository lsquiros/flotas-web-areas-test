﻿/************************************************************************************************************
*  File    : RoutesModels.cs
*  Summary : Routes Models
*  Author  : Danilo Hidalgo
*  Date    : 10/27/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// Routes Base
    /// </summary>
    public class RouteBase
    {
        /// <summary>
        /// RouteBase Constructor
        /// </summary>
        public RouteBase()
        {
            Data = new Routes();
            List = new List<Routes>();
            Menus = new List<AccountMenus>();
        }
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Routes Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Routes> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

        public string JavascriptToRun { get; set; }
    }

    public class RouteReportsJourney
    {
        public string Nombre { get; set; }
        public string Valor1 { get; set; }
        public string Valor2 { get; set; }
        public string Valor3 { get; set; }
        public string Valor4 { get; set; }
        public string Color { get; set; }
        public string Fecha { get; set; }
    }

    /// <summary>
    /// Routes Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Routes : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? RouteId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre", Width = "100px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Activo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public bool Active { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? CustomerId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string CountryCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int UserTimeZone { get { return 0.ToTimeOffSet(); } set { 0.ToTimeOffSet(); } }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Descripción")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Descripción", Width = "200px", SortEnabled = true)]
        public string Description { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Tiempo de Duración")]
        [GridColumn(Title = "Tiempo (Min)", Width = "100px", SortEnabled = true)]
        public int Time { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Distancia Aproximada")]
        [GridColumn(Title = "Distancia (Km)", Width = "100px", SortEnabled = true)]
        public double Distance { get; set; }

        public List<RoutesPoints> Points { get; set; }

        public string MapUrl { get; set; }
    }
}
