﻿/************************************************************************************************************
*  File    : HobbsMeterPerformanceReportModels.cs
*  Summary : Real Vs Budget Fuels Report Models
*  Author  : Berman Romero
*  Date    : 21/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// Real Vs Budget Fuels Report Base
    /// </summary>
    public class HobbsMeterPerformanceReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public HobbsMeterPerformanceReportBase()
        {
            Parameters = new HobbsMeterPerformanceParameters();
            List = new List<HobbsMeterPerformanceReport>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>ss
        /// Report Parameters
        /// </summary>
        public HobbsMeterPerformanceParameters Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<HobbsMeterPerformanceReport> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// Control Fuels Reports Base Model
    /// </summary>
    public class HobbsMeterPerformanceParameters
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public HobbsMeterPerformanceParameters()
        {
            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;
            ReportCriteriaId = (int?)ReportCriteria.Period;
            Key = null;
            Dates = "";
        }

        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string StartDateStr
        {
            get { return Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date for Report when dates range is selected
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string EndDateStr
        {
            get { return Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }


        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64 { get; set; }

        /// <summary>
        /// Key Filter
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Date filter
        /// </summary>
        public string Dates { get; set; }

    }


    /// <summary>
    /// Real Vs Budget Fuels Report Model
    /// </summary>
    public class HobbsMeterPerformanceReport
    {

        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Placa")]
        [NotMappedColumn]
        public string PlateId { get; set; }


        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Nombre Vehículo")]
        [NotMappedColumn]
        public string VehicleName { get; set; }


        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha Inicial")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public DateTime? MinProcessedDate { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha Inicial")]
        [NotMappedColumn]
        public string MinProcessedDateStr
        {
            get { return Miscellaneous.GetDateFormat(MinProcessedDate); }
        }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha Final")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public DateTime? MaxProcessedDate { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [DisplayName("Fecha Final")]
        [NotMappedColumn]
        public string MaxProcessedDateStr
        {
            get { return Miscellaneous.GetDateFormat(MaxProcessedDate); }
        }


        /// <summary>
        /// TrxMinOdometer
        /// </summary>
        [DisplayName("Odómetro Inicial")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal GpsStartOdometer { get; set; }


        /// <summary>
        /// TrxMinOdometer
        /// </summary>
        [DisplayName("Odómetro Final")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public decimal GpsEndOdometer { get; set; }


        /// <summary>
        /// TrxMinOdometer
        /// </summary>
        [DisplayName("Odómetro Inicial")]
        [NotMappedColumn]
        public string GpsStartOdometerStr
        {
            get { return Miscellaneous.GetNumberFormat(GpsStartOdometer); }
        }

        /// <summary>
        /// TrxMinOdometer
        /// </summary>
        [DisplayName("Odómetro Final")]
        [NotMappedColumn]
        public string GpsEndOdometerStr
        {
            get { return Miscellaneous.GetNumberFormat(GpsEndOdometer); }
        }

        /// <summary>
        /// TrxMinOdometer
        /// </summary>
        [DisplayName("Tiempo Encendido")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string GpsHoursOnStr { get; set; }

        [ExcelNoMappedColumn]
        public string CostCenterName { get; set; }

        [ExcelNoMappedColumn]
        public string VehicleGroupName { get; set; }

        public string EncryptCustomerName { get; set; }

        public string Dates { get; set; }

        public string CustomerName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptCustomerName); } }


    }
}