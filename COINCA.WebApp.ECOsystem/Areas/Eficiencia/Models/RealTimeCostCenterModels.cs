﻿/************************************************************************************************************
*  File    : RealTimeCostCenterModels.cs
*  Summary : RealTimeCostCenter Models
*  Author  : Danilo Hidalgo
*  Date    : 01/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// RealTimeCostCenter Base
    /// </summary>
    public class RealTimeCostCenterBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public RealTimeCostCenterBase()
        {
            Data = new RealTimeCostCenter();
            List = new List<RealTimeCostCenter>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public RealTimeCostCenter Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<RealTimeCostCenter> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// RealTimeCostCenter Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class RealTimeCostCenter : ModelAncestor
    {        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? CostCenterId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre Centro de Costo", Width = "250px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// Cost Center Code
        /// </summary>
        [DisplayName("Código")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Código", Width = "50px", SortEnabled = true)]
        public string Code { get; set; }

        /// <summary>
        /// FK UnitId from VehicleUnits
        /// </summary>
        [DisplayName("Unidad")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int UnitId { get; set; }

        /// <summary>
        /// Unit Name
        /// </summary>
        [DisplayName("Unidad")]
        [GridColumn(Title = "Unidad", Width = "100px", SortEnabled = true)]
        public string UnitName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string CountryCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int UserTimeZone { get { return 0.ToTimeOffSet(); } set { 0.ToTimeOffSet(); } }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int TransmissionTime { get; set; }

    }
}