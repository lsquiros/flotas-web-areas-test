﻿/************************************************************************************************************
*  File    : CountryCode.cs
*  Summary : CountryCode Models
*  Author  : Danilo Hidalgo
*  Date    : 10/28/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;

namespace ECO_Efficiency.Models
{

    public class CountryCodeModelsBase
    {
        public CountryCodeModelsBase()
        {
            Data = new CountryCodeModels();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// 
        /// </summary>
        public CountryCodeModels Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Country Code
    /// </summary>
    public class CountryCodeModels : ModelAncestor
    {
        /// <summary>
        /// Property
        /// </summary>
        public string countryCode { get; set; }
        /// <summary>
        /// Property
        /// </summary>
        public int customerId { get; set; }

        /// <summary>
        /// TimeZone
        /// </summary>
        public int TimeZone { get; set; }
    }
}

