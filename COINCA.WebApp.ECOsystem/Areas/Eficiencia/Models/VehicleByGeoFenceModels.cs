﻿/************************************************************************************************************
*  File    : VehiclesByGeoFenceModels.cs
*  Summary : VehiclesByGeoFence Models
*  Author  : Danilo Hidalgo
*  Date    : 11/17/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// VehiclesByGeoFence Base
    /// </summary>
    public class VehicleByGeoFenceBase
    {
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehiclesByGeoFenceData Data { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        [DisplayName("GeoCerca")]
        public int? GeoFenceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// VehiclesGeoFence Model
    /// </summary>
    public class VehiclesByGeoFence : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Vehicles model
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Name for the Vehicle
        /// </summary>
        [DisplayName("Vehículo")]
        public string VehicleName { get; set; }

        /// <summary>
        /// Plate Id or number for the Vehicle
        /// </summary>
        [DisplayName("Matrícula")]
        public string PlateId { get; set; }

        /// <summary>
        /// Vehicle Type
        /// </summary>
        [DisplayName("Tipo")]
        public string VehicleType { get; set; }


        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of GeoFence model
        /// </summary>
        public int? GeoFenceId { get; set; }


    }

    /// <summary>
    /// Vehicles By GeoFences Data
    /// </summary>
    public class VehiclesByGeoFenceData
    {
        /// <summary>
        /// Vehicles By GeoFences Data default Constructor
        /// </summary>
        public VehiclesByGeoFenceData()
        {
            AvailableVehiclesList = new List<VehiclesByGeoFence>();
            ConnectedVehiclesList = new List<VehiclesByGeoFence>();
        }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        public int? GeoFenceId { get; set; }

        /// <summary>
        /// Available Vehicles List
        /// </summary>
        public IEnumerable<VehiclesByGeoFence> AvailableVehiclesList { get; set; }

        /// <summary>
        /// Connected Vehicles List
        /// </summary>
        public IEnumerable<VehiclesByGeoFence> ConnectedVehiclesList { get; set; }
    }


}

