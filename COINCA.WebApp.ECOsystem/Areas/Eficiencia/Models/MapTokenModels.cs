﻿/************************************************************************************************************
*  File    : MapTokenModels.cs
*  Summary : MapTokenModels Models
*  Author  : María de los Ángeles Jiménez
*  Date    : APR/01/2019
* 
*  Copyright 2019 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// Map token data
    /// </summary>
    public class MapTokenModels
    {
        public string token { get; set; }

        public string refreshToken { get; set; }
    }
}