﻿/************************************************************************************************************
*  File    : VehicleGroupsByGeoFenceModels.cs
*  Summary : VehicleGroupsByGeoFence Models
*  Author  : Danilo Hidalgo
*  Date    : 11/18/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// VehicleGroupsByGeoFence Base
    /// </summary>
    public class VehicleGroupsByGeoFenceBase
    {
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleGroupsByGeoFenceData Data { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        [DisplayName("GeoCerca")]
        public int? GeoFenceId { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// VehicleGroupsByGeoFence Model
    /// </summary>
    public class VehicleGroupsByGeoFence : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Name for the VehicleGroup
        /// </summary>
        [DisplayName("Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Description for the VehicleGroup
        /// </summary>
        [DisplayName("Descripción")]
        public string Description { get; set; }


        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of GeoFence model
        /// </summary>
        public int? GeoFenceId { get; set; }


    }

    /// <summary>
    /// VehicleGroups By GeoFences Data
    /// </summary>
    public class VehicleGroupsByGeoFenceData
    {
        /// <summary>
        /// VehicleGroup By GeoFences Data default Constructor
        /// </summary>
        public VehicleGroupsByGeoFenceData()
        {
            AvailableVehicleGroupsList = new List<VehicleGroupsByGeoFence>();
            ConnectedVehicleGroupsList = new List<VehicleGroupsByGeoFence>();
        }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        public int? GeoFenceId { get; set; }

        /// <summary>
        /// Available VehicleGroup List
        /// </summary>
        public IEnumerable<VehicleGroupsByGeoFence> AvailableVehicleGroupsList { get; set; }

        /// <summary>
        /// Connected VehicleGroup List
        /// </summary>
        public IEnumerable<VehicleGroupsByGeoFence> ConnectedVehicleGroupsList { get; set; }
    }

}
