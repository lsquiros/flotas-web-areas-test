﻿using System;
using System.Collections.Generic;
using System.Web;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// GPS Report Base 
    /// </summary>
    public class GPSReportBase
    {
        public GPSReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<GPSReportModel>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<GPSReportModel> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }
    
    /// <summary>
    /// GPS Report Model 
    /// </summary>
    public class GPSReportModel
    {
        /// <summary>
        /// GPSId
        /// </summary>
        public int GPSId { get; set; }
        /// <summary>
        /// StarDate
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date String
        /// </summary>
        public string StartDateStr
        {
            get { return Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date String
        /// </summary>
        public String EndDateStr
        {
            get { return Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Latitud
        /// </summary>
        public double Latitud { get; set; }

        /// <summary>
        /// Longitud
        /// </summary>
        public double Longitud { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Max Speed
        /// </summary>
        public double MaxSpeed { get; set; }        

        /// <summary>
        /// Location
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Kilometers
        /// </summary>
        public double Kilometers { get; set; }

        /// <summary>
        /// Stops
        /// </summary>
        public int Stops { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// StopsDetail 
        /// </summary>
        public int StopsDetail { get; set; }

        /// <summary>
        /// Abrupt turns
        /// </summary>
        public int AbruptTurns { get; set; }

        /// <summary>
        /// Sudden Stops
        /// </summary>
        public int SuddenStops { get; set; }

        /// <summary>
        /// Quick Accelerations
        /// </summary>
        public int QuickAccelerations { get; set; }

         /// <summary>
        /// LowTemperature
        /// </summary>
        public int LowTemperature { get; set; }

         /// <summary>
        /// HighTemperature
        /// </summary>
        public int HighTemperature { get; set; }

        /// <summary>
        /// PanicButton
        /// </summary>
        public int PanicButton { get; set; }

     
    }

    /// <summary>
    /// Control Fuels Reports Base Model
    /// </summary>
    public class ControlFuelsReportsBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public ControlFuelsReportsBase()
        {
            var currentDate = DateTimeOffset.Now;
            Year = currentDate.Year;
            Month = currentDate.Month;
            ReportCriteriaId = (int?)ReportCriteria.Period;            
            key = null;
        }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart titles
        /// </summary>
        public HtmlString Titles { get; set; }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart labels
        /// </summary>
        public HtmlString Labels { get; set; }

        /// <summary>
        /// Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString Colors { get; set; }

        /// <summary>
        /// Alpha Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString AlphaColors { get; set; }


        /// <summary>
        /// Highlight Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString HighlightColors { get; set; }

        /// <summary>
        /// Data formatted as JSON for second chart serie
        /// </summary>
        public HtmlString Data { get; set; }
        
        /// <summary>
        /// Year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Month
        /// </summary>
        public int? Month { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Start Date for Report when dates range is selected
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string StartDateStr
        {
            get { return Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date for Report when dates range is selected
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date as string for datepicker for Report when dates range is selected
        /// </summary>
        public string EndDateStr
        {
            get { return Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Report Criteria Id
        /// </summary>
        public int? ReportCriteriaId { get; set; }

        /// <summary>
        /// Report Fuel Type Id
        /// </summary>
        public int? ReportFuelTypeId { get; set; }

        /// <summary>
        /// Report compartive Id
        /// </summary>
        public int? ReportComparativeId { get; set; }

        /// <summary>
        /// Report Status
        /// </summary>
        public int? ReportStatus { get; set; }

        /// <summary>
        /// Parameters In Base64
        /// </summary>
        public string ParametersInBase64 { get; set; }

        /// <summary>
        /// Key Filter Vehicle
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// Report Transaction Type
        /// </summary>
        public int TransactionType { get; set; }

        /// <summary>
        /// Report Vehicle filter
        /// </summary>
        public int? FilterVehicle { get; set; }

        /// <summary>
        /// Setting Odometers Filter
        /// </summary>
        public int OdometerType { get; set; }

        /// <summary>
        /// Report Frequency Type Id
        /// </summary>
        public int? ReportFrequencyTypeId { get; set; }

        public int? RedLower { get; set; }

        /// <summary>
        /// Error margin of odometer with POS
        /// </summary>
        public int? RedHigher { get; set; }

        /// <summary>
        /// Tolerance time to get arrival to point
        /// </summary>
        public int? YellowLower { get; set; }

        /// <summary>
        /// Error Margin of geopoint
        /// </summary>
        public int? YellowHigher { get; set; }

        public int? GreenLower { get; set; }

        /// <summary>
        /// Passwords to Validate Repetition
        /// </summary>
        public int? GreenHigher { get; set; }

        /// <summary>
        /// StopsDetail 
        /// </summary>
        public bool StopsDetail { get; set; }

        /// <summary>
        /// Abrupt turns
        /// </summary>
        public bool AbruptTurns { get; set; }

        /// <summary>
        /// Sudden Stops
        /// </summary>
        public bool SuddenStops { get; set; }

        /// <summary>
        /// Quick Accelerations
        /// </summary>
        public bool QuickAccelerations { get; set; }

        /// <summary>
        /// CostCenterId
        /// </summary>
        public int? CostCenterId { get; set; }

        /// <summary>
        /// GeofencesId
        /// </summary>
        public int? GeofencesId { get; set; }

        /// <summary>
        /// Dates
        /// </summary>
        public string Dates { get; set; }

        /// <summary>
        /// HigthTemperature    
        /// </summary>
        public bool HighTemperature { get; set; }

        /// <summary>
        /// HigthTemperature    
        /// </summary>
        public bool LowTemperature { get; set; }

        /// <summary>
        /// PanicButton
        /// </summary>
        public bool PanicButton { get; set; }


    }

    /// <summary>
    /// Report Fuel Types Enum
    /// </summary>
    public enum ReportCriteria
    {
        /// <summary>
        /// Period
        /// </summary>
        Period = 1000,
        /// <summary>
        /// Date Range
        /// </summary>
        DateRange = 2000,
        /// <summary>
        /// Date Range
        /// </summary>
        DayDate = 3000
    }
}
