﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using System;
using System.Web;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ContinuosMonitoringModel : ModelAncestor
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public ContinuosMonitoringModel()
        {
            VehicleTemperatureList = new List<VehicleTemperatureResume>();
            //VehicleTemperatureDetailList = new List<VehicleTemperatureResume>();
        }

        /// <summary>
        /// 
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int UserTimeZone { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TransmissionTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string VehicleIdsString { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<VehiclesMonitoring> VehicleList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<VehicleTemperatureResume> VehicleTemperatureList { get; set; }

        public List<VehicleTemperatureResume> VehicleTemperatureDetailList { get; set; }

        /// <summary>
        /// Month Count
        /// </summary>
        public int SensorCount { get; set; }

        /// <summary>
        /// Vehicle Count
        /// </summary>
        public int HourCount { get; set; }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart titles
        /// </summary>
        public HtmlString Titles { get; set; }

        /// <summary>
        /// Labes formatted as JSON in order to display each chart labels
        /// </summary>
        public HtmlString Labels { get; set; }

        /// <summary>
        /// Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString Colors { get; set; }

        /// <summary>
        /// Alpha Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString AlphaColors { get; set; }


        /// <summary>
        /// Highlight Colors array formatted as JSON for first chart serie
        /// </summary>
        public HtmlString HighlightColors { get; set; }

        /// <summary>
        /// Data formatted as JSON for second chart serie
        /// </summary>
        public HtmlString Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int FilterId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<SelectListItem> List { get; set; }

        public int VehicleIdTemp { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class VehiclesMonitoring
    {
        /// <summary>
        /// 
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PlateId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CostCenterId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CostCenterName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CostCenterCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int VehicleGroupId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string VehicleGroupName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>         
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double? Score { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ScoreStr
        {
            get { return Score == null ? "N/A" : Miscellaneous.GetNumberFormat((decimal)Score); }
        }

        public bool HasCooler { get; set; }

        public string Temperature { get; set; }

        public string TemperatureStr { get { return HasCooler ? Temperature == "No Disponible" ? Temperature : string.Format("{0}°", Temperature) : Temperature; } }

        public int? ReportId { get; set; }

        public string SensorsXML { get; set; }

        public string ReportIdImage { get; set; }

        public string ReportIdDescription { get; set; }

        public bool? HasCommands { get; set; }
    }

    public class VehicleTemperatureResume
    {
        public int Id { get; set; }

        public Int64 ReportId { get; set; }

		public string Name { get; set; }

		public string Plate { get; set; }
        
        public string Date { get; set; }
        
        public int SensorId { get; set; }
        
        public int Hour { get; set; }
        
        public int VehicleId { get; set; }
                
        public int Year { get; set; }
        
        public int Month { get; set; }
        
        public decimal? Temperature { get; set; }

		public decimal? MinTemperature { get; set; }

		public decimal? MaxTemperature { get; set; }

		public string MonthName
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetMonthName(Month); }
        }

    }
}