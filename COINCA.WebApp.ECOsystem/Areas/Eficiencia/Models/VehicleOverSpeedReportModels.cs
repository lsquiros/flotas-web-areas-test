﻿using System;
using System.Collections.Generic;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// VehicleOverSpeedReportModelBase
    /// </summary>
    public class VehicleOverSpeedReportModelBase
    {
        public VehicleOverSpeedReportModelBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<VehicleOverSpeedReportModel>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleOverSpeedReportModel> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// VehicleOverSpeedReportModel
    /// </summary>
    public class VehicleOverSpeedReportModel
    {
        /// <summary>
        /// GPSDateTime
        /// </summary>
        public DateTime GPSDateTime { get; set; }

        /// <summary>
        /// ElapsedTime
        /// </summary>
        public string ElapsedTime { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        public decimal Longitude { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        public decimal Latitude { get; set; }

        /// <summary>
        /// MINSpeed
        /// </summary>
        public decimal MINSpeed { get; set; }

        /// <summary>
        /// MAXSpeed
        /// </summary>
        public decimal MAXSpeed { get; set; }

        /// <summary>
        /// AVGSpeed
        /// </summary>
        public decimal AVGSpeed { get; set; }
        
        /// <summary>
        /// Odometer
        /// </summary>
        public decimal Odometer { get; set; }

        /// <summary>
        /// DriverName
        /// </summary>
        public string DriverName { get; set; }
        
        /// <summary>
        /// DecryptedDriverName
        /// </summary>
        public string DecryptedDriverName
        {
            get { return TripleDesEncryption.Decrypt(DriverName); }
        }

        /// <summary>
        /// Address
        /// </summary>
        public string Location { get; set; }
    }
}