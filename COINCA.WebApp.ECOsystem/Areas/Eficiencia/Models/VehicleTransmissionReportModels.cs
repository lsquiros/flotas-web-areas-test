﻿using System;
using System.Collections.Generic;
using System.Web;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// GPS Report Base 
    /// </summary>
    public class VehicleTransmissionReportBase
    {
        public VehicleTransmissionReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            List = new List<VehicleTransmissionReportModel>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleTransmissionReportModel> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// GPS Report Model 
    /// </summary>
    public class VehicleTransmissionReportModel
    {
        ///// <summary>
        ///// StarDate
        ///// </summary>
        //public DateTime? StartDate { get; set; }

        ///// <summary>
        ///// Start Date String
        ///// </summary>
        //public string StartDateStr
        //{
        //    get { return Miscellaneous.GetDateFormat(StartDate); }
        //    set { StartDate = Miscellaneous.SetDate(value); }
        //}

        ///// <summary>
        ///// End Date
        ///// </summary>
        //public DateTime? EndDate { get; set; }

        ///// <summary>
        ///// End Date String
        ///// </summary>
        //public String EndDateStr
        //{
        //    get { return Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
        //    set { EndDate = (value != null) ? Convert.ToDateTime(Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : Miscellaneous.SetDate(value); }
        //}

        /// <summary>
        /// ReportDate
        /// </summary>
        public DateTime? ReportDate { get; set; }

        /// <summary>
        /// Start Date String
        /// </summary>
        public string ReportDateStr
        {
            get { return Miscellaneous.GetDateFormat(ReportDate); }
            set { ReportDate = Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Latitud
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Longitud
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public string IsVehicleOn { get; set; }

        /// <summary>
        /// Max Speed
        /// </summary>
        public double ReportSpeed { get; set; }

        /// <summary>
        /// Location
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Distance in Kilometers
        /// </summary>
        public decimal ReportDistance { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// EventDetail
        /// </summary>
        public string EventDetail { get; set; }

        /// <summary>
        /// BatteryStatus
        /// </summary>
        public string BatteryStatus { get; set; }
    }
}