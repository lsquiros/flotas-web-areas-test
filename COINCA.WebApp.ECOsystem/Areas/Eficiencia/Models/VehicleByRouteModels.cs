﻿/************************************************************************************************************
*  File    : VehicleByRouteModels.cs
*  Summary : VehicleByRoute Models
*  Author  : Danilo Hidalgo
*  Date    : 11/20/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// VehicleByRoute Base
    /// </summary>
    public class VehicleByRouteBase
    {
        /// <summary>
        /// VehicleByRoute Constructor
        /// </summary>
        public VehicleByRouteBase()
        {
            Data = new VehicleByRoute();
            List = new List<VehicleByRoute>();
            Menus = new List<AccountMenus>();
        }
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleByRoute Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleByRoute> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// VehicleByRoute Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class VehicleByRoute : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? RouteId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Vehículo", Width = "120px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// Plate Id or number for the Vehicle
        /// </summary>
        [DisplayName("Matrícula")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Matrícula", Width = "50px", SortEnabled = true)]
        public string PlateId { get; set; }

        /// <summary>
        /// FK CostCenterId from SubUnit
        /// </summary>
        [DisplayName("Centro de Costo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int CostCenterId { get; set; }

        /// <summary>
        /// SubUnit Name
        /// </summary>
        [DisplayName("Centro de Costo")]
        [GridColumn(Title = "Centro de Costo", Width = "50px", SortEnabled = true)]
        public string CostCenterName { get; set; }


        /// <summary>
        /// FK User Id from Users, each vehicle is associate with one user
        /// </summary>
        [NotMappedColumn]
        public int UserId { get; set; }

        /// <summary>
        /// UserName for UserId
        /// </summary>
        [DisplayName("Conductor Asignado")]
        [GridColumn(Title = "Conductor Asignado", Width = "100px", SortEnabled = true)]
        public string UserNameDecrypt { get { return TripleDesEncryption.Decrypt(UserName); } }

        [NotMappedColumn]
        public string UserName { get; set; }

        /// <summary>
        /// FK VehicleCategoryId from VehicleCategories each vehicle is associate with one vehicle category
        /// </summary>
        [DisplayName("Clase de Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int VehicleCategoryId { get; set; }

        /// <summary>
        /// Category type/description
        /// </summary>
        [DisplayName("Clase de Vehículo")]
        [GridColumn(Title = "Clase de Vehículo", Width = "50px", SortEnabled = true)]
        public string CategoryType { get; set; }


        /// <summary>
        /// Fuel Name for the vehicle
        /// </summary>
        [DisplayName("Combustible")]
        [NotMappedColumn]
        public string FuelName { get; set; }

        /// <summary>
        /// Is Active?
        /// </summary>
        [DisplayName("Activo")]
        [NotMappedColumn]
        public bool Active { get; set; }

        /// <summary>
        /// Vehicle Colour
        /// </summary>
        [DisplayName("Color")]
        [NotMappedColumn]
        public string Colour { get; set; }

        /// <summary>
        /// Vehicle year
        /// </summary>
        [DisplayName("Año")]
        [NotMappedColumn]
        public int? Year { get; set; }

        /// <summary>
        /// Vehicle chassis
        /// </summary>
        [DisplayName("Chasis")]
        [NotMappedColumn]
        public string Chassis { get; set; }

        [NotMappedColumn]
        public VehicleByRouteDetail Detail { get; set; }


        [NotMappedColumn]
        public IEnumerable<VehicleByRouteDetail> DetailList { get; set; }

		/// <summary>
		/// Has GPS?
		/// </summary>
		[NotMappedColumn]
		public bool HasGPS { get; set; }
	}

}
