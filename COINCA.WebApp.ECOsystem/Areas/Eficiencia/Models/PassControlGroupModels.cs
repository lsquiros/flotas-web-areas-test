﻿/************************************************************************************************************
*  File    : PassControlGroupModels.cs
*  Summary : PassControlGroup Models
*  Author  : Danilo Hidalgo
*  Date    : 11/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// PassControlGroup Base
    /// </summary>
    public class PassControlGroupBase
    {
        public PassControlGroupBase()
        {
            Data = new PassControlGroup();
            List = new List<PassControlGroup>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public PassControlGroup Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PassControlGroup> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// VehicleGroups Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PassControlGroup : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string CountryCode { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int UserTimeZone { get { return 0.ToTimeOffSet(); } set { 0.ToTimeOffSet(); } }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre del Grupo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre del Grupo", Width = "150px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// Description of the group
        /// </summary>
        [DisplayName("Descripción")]
        [GridColumn(Title = "Descripción", Width = "220px", SortEnabled = true)]
        public string Description { get; set; }
    }
}

