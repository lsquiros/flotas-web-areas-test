﻿/************************************************************************************************************
*  File    : GeoFencesModels.cs
*  Summary : GeoFences Models
*  Author  : Danilo Hidalgo
*  Date    : 10/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// GeoFences Base
    /// </summary>
    public class GeoFencesBase
    {
        /// <summary>
        /// GeoFencesBase Constructor
        /// </summary>
        public GeoFencesBase()
        {
            Data = new GeoFences();
            List = new List<GeoFences>();
            Menus = new List<AccountMenus>();
            CodView = "GeoFen";
        }   
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public GeoFences Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<GeoFences> List { get; set; }


        /// <summary>
        /// Information of the Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// For get Alarm of Vehicle
        /// </summary>
        [NotMappedColumn]
        public string CodView { get; set; }

    }

    /// <summary>
    /// GeoFences Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class GeoFences : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? GeoFenceId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre", Width = "100px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Activo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public bool Active { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? CustomerId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool? Alarm { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? AlarmId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string CountryCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Descripción")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Descripción", Width = "200px", SortEnabled = true)]
        public string Description { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Polígono")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string Polygon { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Geocerca de Entrada")]
        [NotMappedColumn]
        public bool In { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Geocerca de Salida")]
        [NotMappedColumn]
        public bool Out { get; set; }

        [DisplayName("Tipo de Geocerca")]
        [NotMappedColumn]
        public int? GeoFenceTypeId { get; set; }

        [DisplayName("Conductor relacionado a la geocerca")]
        [NotMappedColumn]
        public int? DriverId { get; set; }

    }

    public class GeoFenceTypes : ModelAncestor
    {
        /// <summary>
        /// Status Id property
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Status property
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description property
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// IsHome property
        /// </summary>
        public bool IsHome { get; set; }

        /// <summary>
        /// Tolerance property
        /// </summary>
        public double Tolerance { get; set; }

    }
}

