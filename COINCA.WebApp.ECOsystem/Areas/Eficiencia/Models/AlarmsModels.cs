﻿/************************************************************************************************************
*  File    : AlarmsModels.cs
*  Summary : Alarms Models
*  Author  : Cristian Martínez Hernández
*  Date    : 17/Nov/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// Alarms Models
    /// </summary>
    public class AlarmsModels : ModelAncestor
    {
        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<AlarmsModels> List { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model 
        /// </summary>
        [NotMappedColumn]
        public int? AlarmId { get; set; }

        /// <summary>
        /// Fk User Id From User
        /// </summary>
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// Phones for send of the alarm
        /// </summary>
        [DisplayName("SMS (Teléfonos):")]
        //[Required(ErrorMessage="{0} es requerido.")]
        [NotMappedColumn]
        public string Phone { get; set; }

        /// <summary>
        /// Email for send of the alarm
        /// </summary>
        [DisplayName("Correos:")]
        [Required(ErrorMessage="{0} es requerido.")]
        [NotMappedColumn]
        public string Email { get; set; }

        /// <summary>
        /// FK AlarmTriggerId from Types
        /// </summary>
        [NotMappedColumn]
        public int AlarmTriggerId { get; set; }

        /// <summary>
        /// FK Periodicity Type from Types
        /// </summary>
        [DisplayName("Periodicidad:")]
        [RequiredIf("AlarmTriggerId == 300", ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? PeriodicityTypeId { get; set; }

        /// <summary>
        /// FK Entity Type Id From Types
        /// </summary>
        [NotMappedColumn]
        public int EntityTypeId { get; set; }

        /// <summary>
        /// FK EntityId from Drives, Vehicles, Group, SUbUnit, Unit
        /// </summary>
        [NotMappedColumn]
        public int EntityId { get; set; }

        /// <summary>
        /// Active Alarm
        /// </summary>
        [DisplayName("Habilitada")]
        [NotMappedColumn]
        public bool Active { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string TitlePopUp { get; set; }

        /// <summary>
        /// Property for indicate that have periodicity
        /// </summary>
        [NotMappedColumn]
        public bool HasPeriodicity { get; set; }

        /// <summary>
        /// Type of the alarm
        /// </summary>
        [DisplayName("Tipo de Alarma:")]
        [NotMappedColumn]
        public string AlarmType { get; set; }

        /// <summary>
        /// Name of the entity of the alarm
        /// </summary>
        [NotMappedColumn]
        public string Entity { get; set; }

        /// <summary>
        /// Type of tht entity
        /// </summary>
        [DisplayName("Tipo de Entidad:")]
        [NotMappedColumn]
        public string EntityType { get; set; }

        /// <summary>
        /// Periodiciy of the alarm
        /// </summary>
        [DisplayName("Periodicidad:")]
        [NotMappedColumn]
        public string Periodicity { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public DateTime? NextAlarm { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Siguiente Alarma:")]
        [NotMappedColumn]
        public string NextAlarmStr { get{return Miscellaneous.GetDateFormat(NextAlarm);}}
    
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool IsEncrypted { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Entidad:")]
        [NotMappedColumn]
        public string EntityStr { 
            get
            {
                if (IsEncrypted)
                {
                    return TripleDesEncryption.Decrypt(Entity);
                }
                return Entity;
            }
       }

        /// <summary>
        /// Email to show if is not email configured
        /// </summary>
        public string EmailPlaceHolder { get; set; }
    }
}