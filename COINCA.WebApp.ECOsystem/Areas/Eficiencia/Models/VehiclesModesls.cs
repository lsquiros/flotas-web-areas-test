﻿using System;
using ECOsystem.Utilities;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// Vehicles Model
    /// </summary>
    public class Vehicles 
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string VehicleModel { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public string VehicleType { get; set; }

        /// <summary>
        /// Vehicle year
        /// </summary>
        public int? VehicleYear { get; set; }

        /// <summary>
        /// Plate Id or number for the Vehicle
        /// </summary>
        public string PlateId { get; set; }

        /// <summary>
        /// FK CostCenterId from SubUnit
        /// </summary>
        public int CostCenterId { get; set; }

        /// <summary>
        /// SubUnit Name
        /// </summary>
        public string CostCenterName { get; set; }

        /// <summary>
        /// Unit Name
        /// </summary>
        public string UnitName { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// FK FuelId from Fuels
        /// </summary>
        public int DefaultFuelId { get; set; }


        /// <summary>
        /// FK User Id from Users, each vehicle is associate with one user
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// UserName for UserId
        /// </summary>        
        public string EncryptedUserName { get; set; }

        /// <summary>
        /// UserName for UserId
        /// </summary>
        public string DecryptedUserName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedUserName); }
            set { EncryptedUserName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// FK VehicleCategoryId from VehicleCategories each vehicle is associate with one vehicle category
        /// </summary>
        public int VehicleCategoryId { get; set; }

        /// <summary>
        /// Category type/description
        /// </summary>
        public string CategoryType { get; set; }


        /// <summary>
        /// Fuel Name for the vehicle
        /// </summary>
        public string FuelName { get; set; }

        /// <summary>
        /// Is Active?
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Vehicle Colour
        /// </summary>
        public string Colour { get; set; }

        /// <summary>
        /// Vehicle chassis
        /// </summary>
        public string Chassis { get; set; }

        /// <summary>
        /// Last Dallas for the vehicle
        /// </summary>
        public string LastDallas { get; set; }

        /// <summary>
        /// Freight Temperature for the vehicle
        /// </summary>
        public int FreightTemperature { get; set; }

        /// <summary>
        /// First Freight Temperature Threshold for the vehicle
        /// </summary>
        public int FreightTemperatureThreshold1 { get; set; }

        /// <summary>
        /// Second Freight Temperature Threshold for the vehicle
        /// </summary>
        public int FreightTemperatureThreshold2 { get; set; }

        /// <summary>
        /// Predictive for the vehicle
        /// </summary>
        public bool Predictive { get; set; }

        /// <summary>
        /// AVL for the vehicle
        /// </summary>
        public string AVL { get; set; }

        /// <summary>
        /// Cabin Phone Number
        /// </summary>
        public string CabinPhone { get; set; }

        /// <summary>
        /// Number of SIM that have the AVL 
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public DateTime DriverLicenseExpiration { get; set; }

        /// <summary>
        /// property
        /// </summary>
        public int DailyTransactionLimit { get; set; }

        /// <summary>
        /// Odometer the driver change
        /// </summary>
        public int Odometer { get; set; }

        /// <summary>
        /// Liters
        /// </summary>
        public int Liters { get; set; }

        /// <summary>
        /// Partner Id
        /// </summary>
        public int PartnerId { get; set; }

        /// <summary>
        /// Partner Capacity Unit Id
        /// </summary>
        public int PartnerCapacityUnitId { get; set; }

        /// <summary>
        /// Insurance of Vehicle
        /// </summary>
        public string Insurance { get; set; }

        /// <summary>
        /// Date Expiration of Insurance 
        /// </summary>
        public DateTime? DateExpirationInsurance { get; set; }

        /// <summary>
        /// String Date Expiration of Insurance
        /// </summary>
        public string DateExpirationInsuranceStr
        {
            get { return Miscellaneous.GetDateFormat(DateExpirationInsurance); }
            set { DateExpirationInsurance = Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Coverage Type of Insurance 
        /// </summary>
        public string CoverageType { get; set; }

        /// <summary>
        /// Insurance Company
        /// </summary>
        public string NameEnterpriseInsurance { get; set; }

        /// <summary>
        /// Insurance Periodicity
        /// </summary>
        public int? PeriodicityId { get; set; }

        /// <summary>
        /// Isurance Cost
        /// </summary>
        public decimal? Cost { get; set; }

        /// <summary>
        /// Insurance Periodicity
        /// </summary>
        public int AdministrativeSpeedLimit { get; set; }

        /// <summary>
        /// Intrack Reference
        /// </summary>
        public int? IntrackReference { get; set; }

        /// <summary>
        /// Imei
        /// </summary>
        public decimal? Imei { get; set; }

        /// <summary>
        /// DefaultPerformance
        /// </summary>
        public decimal? DefaultPerformance { get; set; }
    }
}