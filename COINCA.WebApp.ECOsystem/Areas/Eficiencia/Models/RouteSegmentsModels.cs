﻿/************************************************************************************************************
*  File    : RoutesSegmentsBase.cs
*  Summary : RoutesSegmentsBase Models
*  Author  : Danilo Hidalgo
*  Date    : 11/04/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// RoutesSegments Base
    /// </summary>
    public class RoutesSegmentsBase
    {
        /// <summary>
        /// RoutesSegmentsBase Constructor 
        /// </summary>
        public RoutesSegmentsBase()
        {
            Points = new List<RoutesPoints>();
            Segments = new List<RoutesSegments>();
        }
        /// <summary>
        /// Property
        /// </summary>
        public List<RoutesPoints> Points { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public List<RoutesSegments> Segments { get; set; }

        /// <summary>
        /// Route Name
        /// </summary>
        public string RouteName { get; set; }
    }

    /// <summary>
    /// Routes Points
    /// </summary>
    public class RoutesPoints
    {
        /// <summary>
        /// Property
        /// </summary>
        public int PointId { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public int RouteId { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public int PointReference { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public string Time { get; set; }

        public string TimeStr
        {
            get { return Convert.ToDateTime(Time).ToString("hh:mm tt", CultureInfo.InvariantCulture); }
            set { Time = string.Concat(value, ":00"); }
        }

        public string TimeStr24
        {
            get { return Convert.ToDateTime(Time).ToString("HH:mm", CultureInfo.InvariantCulture); }
            set { Time = string.Concat(value, ":00"); }
        }

        /// <summary>
        /// Property
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public string Longitude { get; set; }

        public int IsCommerce { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public int StopTime { get; set; }

        public string StopTimeStr 
        {
            get { return string.Format("{0} mins", StopTime); }
            set { StopTime = Convert.ToInt32(string.Join("", value.ToCharArray().Where(Char.IsDigit))); }
        }

        /// <summary>
        /// Line Number
        /// </summary>
        public int LineNumber { get; set; }

        public int CommerceId { get; set; }

        public bool NewPoint { get; set; }

        public string SerializedObject { get; set; }

        public string CommerceName { get; set; }
    }

    /// <summary>
    /// Routes Segments Class
    /// </summary>
    public class RoutesSegments
    {
        /// <summary>
        /// Property
        /// </summary>
        public int SegmentId { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public int RouteId { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public int StartReference { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public int EndReference { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public string Poliyline { get; set; }
    }

    /// <summary>
    /// Drivers Error Class
    /// </summary>
    public class RoutesErrors
    {
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public string Item { get; set; }


    }
}
