﻿using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ECO_Efficiency.Models
{
    public class LastVehicleTransReportModels
    {
        [NotMapped]
        public int VehicleId { get; set; }

        public string VehicleName { get; set; }

        public string PlateId { get; set; }

        [NotMapped]
        public string EncryptDriverName { get; set; }

        public string DecryptDriverName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptDriverName); } }

        [NotMapped]
        public DateTime? LastTransmission { get; set; }

        public string LastTransmissionDate
        {
            get { return Miscellaneous.GetUtcDateTimeFormat(LastTransmission); }
            set { LastTransmission = Miscellaneous.SetDate(value); }
        }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public string Location { get; set; }

        public int ReportID { get; set; }

        [NotMapped]
        public string EncryptCustomerName { get; set; }

        public string DecryptCustomerName { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptCustomerName); } }

        public string CostCenterName { get; set; }

        public string VehicleGroupName { get; set; }

        public string Dates { get; set; }

        public int Check { get; set; }
    }
}