﻿/************************************************************************************************************
*  File    : PassControlSubUnitModels.cs
*  Summary : PassControlSubUnit Models
*  Author  : Danilo Hidalgo
*  Date    : 11/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// PassControlSubUnit Base
    /// </summary>
    public class PassControlSubUnitBase
    {
        /// <summary>
        /// PassControlSubUnitBase
        /// </summary>
        public PassControlSubUnitBase()
        {
            Data = new PassControlSubUnit();
            List = new List<PassControlSubUnit>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public PassControlSubUnit Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PassControlSubUnit> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// PassControlSubUnit Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PassControlSubUnit : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string CountryCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int CostCenterId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int UserTimeZone { get { return 0.ToTimeOffSet(); } set { 0.ToTimeOffSet(); } }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre Centro de Costo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre Centro de Costo", Width = "250px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// FK UnitId from VehicleUnits
        /// </summary>
        [DisplayName("Unidad")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int UnitId { get; set; }

        /// <summary>
        /// Description of the group
        /// </summary>
        [DisplayName("Unidad")]
        [GridColumn(Title = "Unidad", Width = "100px", SortEnabled = true)]
        public string UnitName { get; set; }

        /// <summary>
        /// Cost Center of Sub Units
        /// </summary>
        [DisplayName("Centro de Costos")]
        [NotMappedColumn]
        public string CostCenter { get; set; }
    }
}


