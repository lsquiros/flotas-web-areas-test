﻿/************************************************************************************************************
*  File    : PassControlUnitModels.cs
*  Summary : PassControlUnit Models
*  Author  : Danilo Hidalgo
*  Date    : 11/13/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using GridMvc.DataAnnotations;

namespace ECO_Efficiency.Models
{
    ///// <summary>
    ///// Default class constructor
    ///// </summary>
    //public class PassControlUnitModels
    //{
        /// <summary>
        /// PassControlUnit Base
        /// </summary>
        public class PassControlUnitBase
        {
            public PassControlUnitBase()
            {
                Data = new PassControlUnit();
                List = new List<PassControlUnit>();
                Menus = new List<AccountMenus>();
            }

            /// <summary>
            /// Data property displays the main information of the entity 
            /// </summary>
            public PassControlUnit Data { get; set; }

            /// <summary>
            /// List of entities to use it for load grid information
            /// </summary>
            public IEnumerable<PassControlUnit> List { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public IEnumerable<AccountMenus> Menus { get; set; }
        }

        /// <summary>
        /// VehicleUnits Model
        /// </summary>
        [GridTable(PagingEnabled = true, PageSize = 30)]
        public class PassControlUnit : ModelAncestor
        {
            /// <summary>
            /// property
            /// </summary>
            [NotMappedColumn]
            public string CountryCode { get; set; }

            /// <summary>
            /// property
            /// </summary>
            [NotMappedColumn]
            public int UserTimeZone { get { return 0.ToTimeOffSet(); } set { 0.ToTimeOffSet(); } }

            /// <summary>
            /// The PRIMARY KEY property uniquely that identifies each record of this model
            /// </summary>
            [NotMappedColumn]
            public int? UnitId { get; set; }

            /// <summary>
            /// property
            /// </summary>
            [NotMappedColumn]
            public int CustomerId { get; set; }

            /// <summary>
            /// Name of current entity
            /// </summary>
            [DisplayName("Nombre de Unidad")]
            [Required(ErrorMessage = "{0} es requerido")]
            [GridColumn(Title = "Nombre de Unidad", Width = "300px", SortEnabled = true)]
            public string Name { get; set; }
    
            /// <summary>
            /// Cost Center Units
            /// </summary>
            [DisplayName("Centro de Costos")]
            [GridColumn(Title = "Centro de Costos", Width = "100px", SortEnabled = true)]
            public string CostCenter { get; set; }

        //}
    }
}
