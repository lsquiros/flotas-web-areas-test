﻿/************************************************************************************************************
*  File    : VehicleCostCentersByGeoFenceModels.cs
*  Summary : VehicleCostCentersByGeoFence Models
*  Author  : Danilo Hidalgo
*  Date    : 11/18/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// VehicleCostCentersByGeoFence Base
    /// </summary>
    public class VehicleCostCentersByGeoFenceBase
    {
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleCostCentersByGeoFenceData Data { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleSubUnit model
        /// </summary>
        [DisplayName("GeoCerca")]
        public int? GeoFenceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// VehicleCostCentersByGeoFence Model
    /// </summary>
    public class VehicleCostCentersByGeoFence : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleSubUnit model
        /// </summary>
        public int? CostCenterId { get; set; }

        /// <summary>
        /// Name for the VehicleSubUnit
        /// </summary>
        [DisplayName("Nombre")]
        public string Name { get; set; }
        
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of GeoFence model
        /// </summary>
        public int? GeoFenceId { get; set; }


    }

    /// <summary>
    /// VehicleCostCenters By GeoFences Data
    /// </summary>
    public class VehicleCostCentersByGeoFenceData
    {
        /// <summary>
        /// VehicleSubUnit By GeoFences Data default Constructor
        /// </summary>
        public VehicleCostCentersByGeoFenceData()
        {
            AvailableVehicleCostCentersList = new List<VehicleCostCentersByGeoFence>();
            ConnectedVehicleCostCentersList = new List<VehicleCostCentersByGeoFence>();
        }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleSubUnit model
        /// </summary>
        public int? GeoFenceId { get; set; }

        /// <summary>
        /// Available VehicleSubUnit List
        /// </summary>
        public IEnumerable<VehicleCostCentersByGeoFence> AvailableVehicleCostCentersList { get; set; }

        /// <summary>
        /// Connected VehicleSubUnit List
        /// </summary>
        public IEnumerable<VehicleCostCentersByGeoFence> ConnectedVehicleCostCentersList { get; set; }
    }


}

