﻿/************************************************************************************************************
*  File    : VehicleUnitsByGeoFenceModels.cs
*  Summary : VehicleUnitsByGeoFence Models
*  Author  : Danilo Hidalgo
*  Date    : 11/18/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// VehicleUnitsByGeoFence Base
    /// </summary>
    public class VehicleUnitsByGeoFenceBase
    {
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleUnitsByGeoFenceData Data { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleUnit model
        /// </summary>
        [DisplayName("GeoCerca")]
        public int? GeoFenceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// VehicleUnitsByGeoFence Model
    /// </summary>
    public class VehicleUnitsByGeoFence : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleUnit model
        /// </summary>
        public int? UnitId { get; set; }

        /// <summary>
        /// Name for the VehicleUnit
        /// </summary>
        [DisplayName("Nombre")]
        public string Name { get; set; }
        
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of GeoFence model
        /// </summary>
        public int? GeoFenceId { get; set; }


    }

    /// <summary>
    /// VehicleUnits By GeoFences Data
    /// </summary>
    public class VehicleUnitsByGeoFenceData
    {
        /// <summary>
        /// VehicleUnit By GeoFences Data default Constructor
        /// </summary>
        public VehicleUnitsByGeoFenceData()
        {
            AvailableVehicleUnitsList = new List<VehicleUnitsByGeoFence>();
            ConnectedVehicleUnitsList = new List<VehicleUnitsByGeoFence>();
        }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleUnit model
        /// </summary>
        public int? GeoFenceId { get; set; }

        /// <summary>
        /// Available VehicleUnit List
        /// </summary>
        public IEnumerable<VehicleUnitsByGeoFence> AvailableVehicleUnitsList { get; set; }

        /// <summary>
        /// Connected VehicleUnit List
        /// </summary>
        public IEnumerable<VehicleUnitsByGeoFence> ConnectedVehicleUnitsList { get; set; }
    }

}




