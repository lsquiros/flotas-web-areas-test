﻿/************************************************************************************************************
*  File    : ParametersModels.cs
*  Summary : Parameters Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;

namespace ECO_Efficiency.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ParametersBase
    {
        public ParametersBase()
        {
            Data = new Parameters();
            PartnerParameters = new List<ParametersByPartner>();
            Menus = new List<AccountMenus>();    
        }

        /// <summary>
        /// 
        /// </summary>
        public Parameters Data { get; set; }
     
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        public IEnumerable<ParametersByPartner> PartnerParameters { get; set; }
    }


    /// <summary>
    /// Partner Models
    /// </summary>
    public class Parameters : ModelAncestor
    {
        /// <summary>
        /// Safe Speed Margin property
        /// </summary>
        [DisplayName("Margen de Velocidad Segura")]
        [Required(ErrorMessage = "Margen es requerido.")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public int? SafeSpeedMargin { get; set; }

        /// <summary>
        /// Error margin of odometer with AVL
        /// </summary>
        [DisplayName("Margen de Error en Odómetro del AVL")]
        [Required(ErrorMessage = "Margen es requerido.")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public int? AVLOdometerErrorMargin { get; set; }

        /// <summary>
        /// Error margin of odometer with POS
        /// </summary>
        [DisplayName("Margen de Error en Odómetro del POS")]
        [Required(ErrorMessage = "Margen es requerido.")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public int? POSOdometerErrorMargin { get; set; }

        /// <summary>
        /// Tolerance time to get arrival to point
        /// </summary>
        [DisplayName("Tiempo de Tolerancia al Punto de Arribo")]
        [Required(ErrorMessage = "Tiempo es requerido.")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public int? PointArrivalTimeTolerance { get; set; }

        /// <summary>
        /// Error Margin of geopoint
        /// </summary>
        [DisplayName("Margen de Error de Distancia del Geopunto")]
        [Required(ErrorMessage = "Margen es requerido.")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public int? GeoPointErrorMargin { get; set; }

        /// <summary>
        /// Password Effective Days
        /// </summary>
        [DisplayName("Días de Vigencia de la Contraseña")]
        [Required(ErrorMessage = "Días de vigencia es requerido.")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public int? PasswordEffectiveDays { get; set; }

        /// <summary>
        /// Passwords to Validate Repetition
        /// </summary>
        [DisplayName("Contraseñas a Validar Repetición")]
        [Required(ErrorMessage = "Es requerido.")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public int? PasswordsValidateRepetition { get; set; }

        /// <summary>
        /// Passwords to Validate Repetition
        /// </summary>
        [DisplayName("Tiempo Considerado sin Transmitir (Mapas)")]
        [Required(ErrorMessage = "Es requerido.")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public int? TransmissionTime { get; set; }

        /// <summary>
        /// Amount Alert Aorcentage
        /// </summary>
        [DisplayName("Porcentaje Mínimo de Alerta en Presupesto")]
        [Required(ErrorMessage = "Es requerido.")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Digitar solo valores numéricos.")]
        public int? AmountAlertPorcentage { get; set; }
    }

    /// <summary>
    /// Parameters By Partner Model
    /// </summary>
    public class ParametersByPartner : ModelAncestor
    {
        public int ParameterId { set; get; }

        public int ParameterByPartnerId { set; get; }

        public int PartnerId { set; get; }

        public string ParameterName { set; get; }

        public string ParameterDescription { set; get; }

        public string ParameterValue { set; get; }

    }
}