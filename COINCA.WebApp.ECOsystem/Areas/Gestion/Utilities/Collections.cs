﻿using ECOsystem.Management.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ECOsystem.Management.Utilities
{
    public class Collections
    {
        public static SelectList GetCatalogDetail(int id, string name)
        {   
            var dictionary = new Dictionary<int?, string>();
            using (var bus = new CatalogBusiness())
            {
                var result = bus.RetrieveCatalog(id, name);
                foreach (var r in result)
                {
                    dictionary.Add(r.Id, r.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        public static SelectList GetSurveys()
        {
            var dictionary = new Dictionary<int?, string>();
            using (var bus = new SurveyBusiness())
            {
                var result = bus.SurveyRetrieve(null, ECOsystem.Utilities.Session.GetCustomerId(), null).Where(x => !string.IsNullOrEmpty(x.QuestionsStr)).ToList();
                foreach (var r in result)
                {
                    dictionary.Add(r.Id, r.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        public static SelectList GetEvents()
        {
            var dictionary = new Dictionary<int?, string>();
            using (var bus = new EventBusiness())
            {
                var result = bus.EventRetrieve(ECOsystem.Utilities.Session.GetCustomerId(), null);
                foreach (var r in result)
                {
                    dictionary.Add(r.Id, r.Init.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }
    }
}
