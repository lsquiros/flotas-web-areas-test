﻿using ECO_Management.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECO_Management.Business;
using ECOsystem.Management.Models;
using ECOsystem.Models.Core;

namespace ECO_Management.Controllers
{
    public class ManagementMapController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var bus = new ManagementMapBusiness())
            {
                var customerid = ECOsystem.Utilities.Session.GetCustomerId();
                Session["CommercesList"] = bus.CommercesRetrieve(customerid);
                Session["AgentList"] = bus.AgentsRetrieve(customerid,null);

                return View(new ManagementMapsBase()
                {
                    Data = new ManagementMaps()
                    {
                        Commerces = (IEnumerable<Commerce>)Session["CommercesList"],
                        Agents = (IEnumerable<Agent>)Session["AgentList"]
                    }
                });
            }
        }

        public ActionResult CommercesDetails(string ObjectIds)
        {
            try
            {
                ViewBag.ObjectIds = ObjectIds;
                var list = (IEnumerable<Commerce>)Session["CommercesList"];
                return PartialView("_partials/_CommercesDetail", list.Where(x => ObjectIds.Split(',').ToList().Contains(x.Id.ToString())));
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Detalle de Comercios", TechnicalError = e.Message });
            }
        }

        public ActionResult AgentDetails(string AgentIds, DateTime? StartDate)
        {
            try
            {
                using (var bus = new ManagementMapBusiness())
                {
                    if (AgentIds.Split(',').Count() > 1)
                    {
                        ViewBag.AgentIds = AgentIds;
                        return PartialView("_partials/_AgentsDetail", bus.AgentsDetailRetrieve(AgentIds).Where(x => AgentIds.Split(',').ToList().Contains(x.UserId.ToString())));
                    }
                    else
                    {
                        return PartialView("_partials/_AgentReconstructing", bus.AgentsReconstructingRetrieve(Convert.ToInt32(AgentIds.Split(',').FirstOrDefault()), StartDate));
                    }
                }
            }
            catch (Exception e)
            {
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Detalle de Agentes", TechnicalError = e.Message });
            }
        }
    }
}