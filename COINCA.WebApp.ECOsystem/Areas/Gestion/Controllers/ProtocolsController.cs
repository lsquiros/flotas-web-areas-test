﻿using ECO_Management.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECO_Management.Models;
using ECO_Management.Business;
using ECOsystem.Business.Utilities;
using System.Web.Script.Serialization;

namespace ECO_Management.Controllers
{
    public class ProtocolsController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new ProtocolsBusiness())
            {
                var model = new ProtocolsBase();
                model.List = business.RetrieveProtocols(ECOsystem.Utilities.Session.GetCustomerId());
                Session["ProtocolsList"] = model.List;
                return View(model);
            }
        }

        [HttpPost]
        public PartialViewResult LoadProtocol(int id) 
        {
            using (var business = new ProtocolsBusiness()) 
            {
                var model = business.RetrieveProtocols(Id: id).FirstOrDefault();

                if (model == null) {
                    model = new Protocols();
                }

                return PartialView("_partials/_Detail", model);
            }
        }

        //[EcoAuthorize]
        public PartialViewResult AddOrEditProtocols(Protocols model)
        {
            using (var business = new ProtocolsBusiness())
            {
                model.UserId = (int)ECOsystem.Utilities.Session.GetUserInfo().UserId;
                model.CustomerId = (int)ECOsystem.Utilities.Session.GetCustomerId();
                business.AddOrEditProtocols(model);
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format((model.Id == null ? "Agrega" : "Edita") + " Protocolo: = {0}, Cliente = {1}, Id de Cliente = {2}",
                                                       new JavaScriptSerializer().Serialize(model), ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                return PartialView("_partials/_Grid", business.RetrieveProtocols(ECOsystem.Utilities.Session.GetCustomerId()));
            }
        }

        public PartialViewResult DeleteProtocols(int Id)
        {
            try
            {
                using (var business = new ProtocolsBusiness())
                {
                    business.Delete_Protocols(Id); 
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se elimina el Formulario: Id: {0}, Cliente = {1}", Id, ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName));
                    return PartialView("_partials/_Grid", business.RetrieveProtocols(ECOsystem.Utilities.Session.GetCustomerId()));
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Eliminar el Evento, Error: {0}", e.Message));
                using (var business = new ProtocolsBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveProtocols(ECOsystem.Utilities.Session.GetCustomerId()));
                }



            }
        }   

        public ActionResult SearchProtocols(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return PartialView("_partials/_Grid", (List<Protocols>)Session["ProtocolsList"]);
            }
            else
            {
                return PartialView("_partials/_Grid", ((List<Protocols>)Session["ProtocolsList"]).Where(x => x.Name.ToUpper().Contains(key.ToUpper())));
            }
        }
	}
}