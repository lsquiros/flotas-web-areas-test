﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECO_Management.Models;
using ECO_Management.Business;
using ECOsystem.Utilities;
using ECOsystem.Business.Utilities;
using Newtonsoft.Json;
using System.Data;
using ECOsystem.Models.Core;

namespace ECO_Management.Controllers
{
    public class GeoFenceByDriverController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetReportData(GeoFenceByDriverParameters model)
        {
            try
            {
                using (var bus = new GeoFenceByDriverBusiness())
                {
                    Session["Reporte"] = bus.GetGeoFenceByDriverReport_Retrieve(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Genera información de las entradas y las salidas de los agentes"));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Generar información para el Reporte de Accesos a Geocercas por Conductor, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Reporte de Accesos a Geocercas por Conductor", TechnicalError = e.Message });
            }
        }


        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                DataSet report = new DataSet();
                if (Session["Report"] != null)
                {
                    report = (DataSet)Session["Report"];
                    Session["Report"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Gestion de Eventos");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "EventsReport", this.ToString().Split('.')[2], "Reporte de Gestion de Eventos");
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al descargar el reporte de gestion de eventos. Error: {0}", e.Message));
                ViewBag.ErrorReport = 1;
                return View("Index");//, new EventsBase() { List = new List<Events>() });
            }
        }
	}
}