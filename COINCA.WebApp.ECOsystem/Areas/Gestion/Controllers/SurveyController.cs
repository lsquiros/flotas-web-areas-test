﻿using ECO_Management.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Management.Business;
using ECOsystem.Management.Models;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ECO_Management.Controllers
{
    public class SurveyController : Controller
    {
        public ActionResult Index()
        {
            using (var bus = new SurveyBusiness())
            {
                Session["SurveyList"] = bus.SurveyRetrieve(null, ECOsystem.Utilities.Session.GetCustomerId(), ECOsystem.Utilities.Session.GetUserInfo().UserId);
                return View((List<Survey>)Session["SurveyList"]);
            }
        }

        public PartialViewResult LoadSurvey(int? id)
        {
            var list = (List<Survey>)Session["SurveyList"];
            var survey = list.Where(x => x.Id == id).FirstOrDefault();
            Session["SurveyDetail"] = survey ?? new Survey();
            return PartialView("_partials/_Detail", (Survey)Session["SurveyDetail"]);
        }

        public ActionResult SearchSurvey(string key)
        {
            var list = (List<Survey>)Session["SurveyList"];
            return PartialView("_partials/_Grid", list.Where(x => x.Name.ToUpper().Contains(key.ToUpper())).ToList());
        }

        public ActionResult AddOrEditSurvey(Survey model, List<OrderItems> questionOrder)
        {
            try
            {
                using (var bus = new SurveyBusiness())
                {
                    model.QuestionsStr = ValidatesOrderAndNextQuestions(questionOrder);
                    bus.SurveyAddOrEdit(model, ECOsystem.Utilities.Session.GetCustomerId(), ECOsystem.Utilities.Session.GetUserInfo().UserId);

                    Session["SurveyList"] = bus.SurveyRetrieve(null, ECOsystem.Utilities.Session.GetCustomerId(), ECOsystem.Utilities.Session.GetUserInfo().UserId);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Agrega o Edita Formulario: = {0}", new JavaScriptSerializer().Serialize(model)));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Mantenimiento de Formularios", TechnicalError = e.Message });
            }
        }

        public ActionResult DeleteSurvey(int id)
        {
            try
            {
                using (var bus = new SurveyBusiness())
                {
                    bus.SurveyDelete(id, ECOsystem.Utilities.Session.GetUserInfo().UserId);

                    Session["SurveyList"] = bus.SurveyRetrieve(null, ECOsystem.Utilities.Session.GetCustomerId(), ECOsystem.Utilities.Session.GetUserInfo().UserId);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Elimina Formulario: = {0}", new JavaScriptSerializer().Serialize(id)));
                    return PartialView("_partials/_Grid", (List<Survey>)Session["SurveyList"]);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Mantenimiento de Formularios", TechnicalError = e.Message });
            }
        }

        [HttpGet]
        public ActionResult ValidateSurveyName(int? id, string name)
        {
            var list = (List<Survey>)Session["SurveyList"];
            return Json(list.Where(x => x.Name.Trim() == name.Trim() && x.Id != id).Count(), JsonRequestBehavior.AllowGet);
        }

        #region QuestionMethods
        public ActionResult ValidateQuestionType(int QuestionType)
        {
            var question = ((Question)Session["QuestionDetail"]);
            ViewBag.showOptions = ShowOptions(QuestionType);
            ViewBag.ShowNextQuestion = CatalogDetail.GetItemId(CatalogDetailEnum.QUESTIONS_TYPE_RADIO) == QuestionType;
            ViewBag.QuestionOrder = question.Order;

            question.QuestionTypeId = QuestionType;
            Session["QuestionDetail"] = question;

            return PartialView("_partials/_OptionDetail", question.Options);
        }

        public ActionResult LoadQuestion(int? questionId, int? questionType)
        {
            var survey = (Survey)Session["SurveyDetail"];
            var question = survey != null ? survey.Questions.Where(x => x.Id == questionId).FirstOrDefault() : new Question();

            ViewBag.showOptions = ShowOptions(questionType);
            ViewBag.ShowNextQuestion = CatalogDetail.GetItemId(CatalogDetailEnum.QUESTIONS_TYPE_RADIO) == questionType;
            ViewBag.QuestionOrder = question == null ? survey.Questions.Max(x => x.Order) + 1 : question.Order;
            Session["QuestionDetail"] = question ?? new Question();
            return PartialView("_partials/_QuestionDetail", (Question)Session["QuestionDetail"]);
        }

        public ActionResult AddOrEditQuestion(int? questionId, string text, int questionType, string description, int? surveyId, string surveyName, List<OrderItems> optionOrder)
        {
            var survey = (Survey)Session["SurveyDetail"];
            var questions = survey.Questions;
            var question = (Question)Session["QuestionDetail"];

            question.Text = text;
            question.QuestionTypeId = questionType;
            question.Description = description;

            //Validate if the options must be removed
            if (!ShowOptions(questionType))
            {
                question.OptionsStr = Miscellaneous.GetXML(new List<Option>(), typeof(Option).Name);
            }
            else
            {
                //Validates Order 
                var options = question.Options;
                foreach (var item in options.Where(x => !x.Delete))
                {                                                               
                    item.Order = optionOrder.Where(x => x.Item == item.Id).FirstOrDefault().Order;
                }
                question.OptionsStr = Miscellaneous.GetXML(options, typeof(Option).Name);
            }

            if (questionId == null)
            {
                question.Id = questions.Any() ? questions.Max(x => x.Id) + 1 : 1;
                question.Order = questions.Any() ? questions.Max(x => x.Order) + 1 : 1;
                questions.Add(question);
            }
            else
            {
                questions[questions.FindIndex(x => x.Id == questionId)] = question;
            }
            survey.QuestionsStr = Miscellaneous.GetXML(questions, typeof(Question).Name);
            //If Survey is null keep name
            if (surveyId == null)
            {
                survey.Name = surveyName;
            }
            Session["SurveyDetail"] = survey;
            return PartialView("_partials/_Detail", (Survey)Session["SurveyDetail"]);
        }

        public ActionResult DeleteQuestion(int questionid, int? surveyId, string surveyName)
        {
            var survey = (Survey)Session["SurveyDetail"];
            var questions = survey.Questions;
            questions.Where(x => x.Id == questionid).ToList().ForEach(a => a.Delete = true);
            survey.QuestionsStr = Miscellaneous.GetXML(questions, typeof(Question).Name);
            if (surveyId == null)
            {
                survey.Name = surveyName;
            }
            Session["SurveyDetail"] = survey;
            return PartialView("_partials/_Detail", (Survey)Session["SurveyDetail"]);
        }

        string ValidatesOrderAndNextQuestions(List<OrderItems> questionOrder)
        {
            var survey = (Survey)Session["SurveyDetail"];
            bool changeOrder = false;

            //Validates questions order
            var questions = survey.Questions;
            foreach (var item in questions.Where(x => !x.Delete))
            {
                changeOrder = changeOrder ? true : item.Order != questionOrder.Where(x => x.Item == item.Id).FirstOrDefault().Order ? true : false;
                item.Order = questionOrder.Where(x => x.Item == item.Id).FirstOrDefault().Order;
            }
            if (changeOrder)
            {
                foreach (var item in questions)
                {
                    var options = item.Options;
                    item.OptionsStr = Miscellaneous.GetXML(options, typeof(Option).Name);
                }
            }                                                                            
            return Miscellaneous.GetXML(questions, typeof(Question).Name);
        }
        #endregion

        #region OptionMethods
        public ActionResult AddOrEditOption(int? optionId, string text, int? nextQuestionId)
        {
            var questionDetail = (Question)Session["QuestionDetail"];
            var options = questionDetail.Options;
            var option = options.Where(x => x.Id == optionId).FirstOrDefault();
            if (option == null)
            {
                option = new Option()
                {
                    Id = options.Any() ? options.Max(x => x.Id) + 1 : 1,
                    Order = options.Any() ? options.Max(x => x.Order) + 1 : 1,
                    Text = text
                };
                options.Add(option);
            }
            else
            {
                option.Text = text;
                option.NextQuestionId = nextQuestionId;
                options[options.FindIndex(x => x.Id == optionId)] = option;
            }
            questionDetail.OptionsStr = Miscellaneous.GetXML(options, typeof(Option).Name);

            //Details for the order and next question
            ViewBag.showOptions = ShowOptions(questionDetail.QuestionTypeId);
            ViewBag.ShowNextQuestion = CatalogDetail.GetItemId(CatalogDetailEnum.QUESTIONS_TYPE_RADIO) == questionDetail.QuestionTypeId;
            ViewBag.QuestionOrder = questionDetail.Order;

            Session["QuestionDetail"] = questionDetail;
            return PartialView("_partials/_OptionDetail", questionDetail.Options);
        }

        private bool ShowOptions(int? questionType)
        {
            if (CatalogDetail.GetItemId(CatalogDetailEnum.QUESTIONS_TYPE_CHECK) == questionType
                || CatalogDetail.GetItemId(CatalogDetailEnum.QUESTIONS_TYPE_DROPDOWN) == questionType
                || CatalogDetail.GetItemId(CatalogDetailEnum.QUESTIONS_TYPE_RADIO) == questionType)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ActionResult DeleteOption(int optionId)
        {
            var question = (Question)Session["QuestionDetail"];
            var options = question.Options;
            options.Where(x => x.Id == optionId).ToList().ForEach(a => a.Delete = true);
            question.OptionsStr = Miscellaneous.GetXML(options, typeof(Option).Name);
            Session["QuestionDetail"] = question;
            return PartialView("_partials/_OptionDetail", question.Options);
        }

        public ActionResult GetNextQuestion(int? order)
        {
            var questions = ((Survey)Session["SurveyDetail"]).Questions.Where(x => x.Order > order && x.Delete == false).ToList();
            return Json(questions, JsonRequestBehavior.AllowGet);
        }
        #endregion        
    }
}