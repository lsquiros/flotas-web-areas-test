﻿using ECO_Management.Business;
using ECO_Management.Models;
using ECO_Management.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Management.Business;
using ECOsystem.Management.Models;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ECO_Management.Controllers.Management
{
    public class EventController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var bus = new EventBusiness())
            {
                var model = bus.EventRetrieve(ECOsystem.Utilities.Session.GetCustomerId());
                Session["EventList"] = model;
                return View(model);
            }
        }

        public PartialViewResult LoadEvent(int? id)
        {
            return PartialView("_partials/_Detail", id == null ? new Event() : ((List<Event>)Session["EventList"]).Where(x => x.Id == id).FirstOrDefault());
        }

        public ActionResult SearchEvent(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return PartialView("_partials/_Grid", (List<Event>)Session["EventList"]);
            }
            else
            {
                return PartialView("_partials/_Grid", ((List<Event>)Session["EventList"]).Where(x => x.Init.Name.ToUpper().Contains(key.ToUpper()) 
                                                                                                     || x.Finish.Name.ToUpper().Contains(key.ToUpper())).ToList());
            }
        }

        public PartialViewResult AddOrEditEvent(Event model)
        {
            try
            {
                using (var bus = new EventBusiness())
                {
                    bus.EventAddOrEdit(model, ECOsystem.Utilities.Session.GetCustomerId(), ECOsystem.Utilities.Session.GetUserInfo().UserId);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format((model.Id == null ? "Agrega" : "Edita") + " Evento: = {0}, Cliente = {1}, Id de Cliente = {2}",
                                                           new JavaScriptSerializer().Serialize(model), ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));

                    var modelV = bus.EventRetrieve(ECOsystem.Utilities.Session.GetCustomerId());
                    Session["EventList"] = modelV;
                    return PartialView("_partials/_Grid", modelV);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Editar o agregar el Evento, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Mantenimiento de Eventos", TechnicalError = e.Message });
            }
        }

        public PartialViewResult DeleteEvent(int Id)
        {
            try
            {
                using (var bus = new EventBusiness())
                {
                    bus.EventDelete(Id, ECOsystem.Utilities.Session.GetCustomerId(), ECOsystem.Utilities.Session.GetUserInfo().UserId);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se elimina el Evento: Id: {0}, Cliente = {1}", Id, ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName));

                    var modelV = bus.EventRetrieve(ECOsystem.Utilities.Session.GetCustomerId());
                    Session["EventList"] = modelV;
                    return PartialView("_partials/_Grid", modelV);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Eliminar el Evento, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Mantenimiento de Eventos", TechnicalError = e.Message });
            }
        }

        #region Report
        public ActionResult ExcelReportDownload()
        {
            try
            {
                using (var bus = new EventBusiness())
                {
                    Session["Report"] = bus.GetReportDownloadData((List<Event>)Session["EventList"], ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al descargar el reporte de Eventos, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Mantenimiento de Eventos", TechnicalError = e.Message });
            }
        }

        public ActionResult DownloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Report"] != null)
                {
                    report = (DataTable)Session["Report"];
                    Session["Report"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Gestion de Eventos");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "EventsReport", this.ToString().Split('.')[2], "Reporte de Gestion de Eventos");
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al descargar el reporte de gestion de eventos. Error: {0}", e.Message));
                ViewBag.ErrorReport = 1;
                return View("Index", (List<Event>)Session["EventList"]);
            }
        }
        #endregion
    }
}