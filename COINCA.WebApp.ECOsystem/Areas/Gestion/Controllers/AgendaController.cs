﻿using ECO_Management.Business;
using ECO_Management.Models;
using ECO_Management.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Management.Models;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ECO_Management.Controllers
{
    public class AgendaController : Controller
    {
        private static string msj = "";

        [EcoAuthorize]
        public ActionResult Index()
        {
            var model = new AgendaBase();
            try
            {
                using (var bus = new AgendaBusiness())
                {
                    model.Columns = bus.RetrieveColumns(ECOsystem.Utilities.Session.GetCustomerId());
                    Session["AgendaColumns"] = model.Columns;
                    return View(model);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("_partials/_Grid", new List<Agenda>());
            }
        }

        public ActionResult AddItemAgenda(int CommerceId, int UserId, DateTime Date)
        {
            try
            {
                using (var bus = new AgendaBusiness())
                {
                    var returnList = bus.RetrieveAgenda(UserId, Date);
                    if (returnList != null && !returnList.Where(x => x.CommerceId == CommerceId).Any())
                    {
                        bus.AddOrEditAgenda(new Agenda()
                        {
                            CommerceId = CommerceId,
                            UserId = UserId,
                            DateOfAgenda = Date
                        });
                        return PartialView("_partials/_Grid", bus.RetrieveAgenda(UserId, Date));
                    }
                    else
                    {
                        return Json("AlreadyId", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("_partials/_Grid", new List<Agenda>());
            }
        }

        public PartialViewResult DeleteItemAgenda(int IdAgenda)
        {
            var returnList = new List<Agenda>();
            using (var business = new AgendaBusiness())
            {
                returnList = business.DeleteAgenda(IdAgenda);
            }
            return PartialView("_partials/_Grid", returnList);
        }

        public PartialViewResult RetrieveAgenda(int UserId, DateTime DateOfAgenda)
        {
            try
            {
                using (var bus = new AgendaBusiness())
                {
                    Session["DateOfAgenda"] = DateOfAgenda;
                    Session["UserId"] = UserId;
                    List<Agenda> list = bus.RetrieveAgenda(UserId, DateOfAgenda);
                    return PartialView("_partials/_Grid", bus.RetrieveAgenda(UserId, DateOfAgenda));
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("_partials/_Grid", new List<Agenda>());
            }
        }

        public ActionResult GetReportData(int UserId, DateTime DateOfCheck)
        {
            try
            {
                using (var bus = new AgendaBusiness())
                {
                    var ReportData = bus.GetReportDataTable(UserId, DateOfCheck);
                    if (ReportData.Rows.Count > 0)
                    {
                        Session["Reporte"] = ReportData;
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("NoData", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }

        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Itinerario por Agente");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "AgendaReport", "Management", "Itinerario por Agente");
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }

        #region DynamicColumns 
        public PartialViewResult DynamicColumnsValuesAddOrEdit(List<AditionalColumns> list)
        {
            try
            {
                using (var bus = new AgendaBusiness())
                {
                    bus.DynamicColumnsValuesAddOrEdit(list);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificación en las columnas dinámicas de agenda. Detalle: {0}", JsonConvert.SerializeObject(list)));
                    return PartialView("_partials/_Grid", bus.RetrieveAgenda((int)Session["UserId"], Convert.ToDateTime(Session["DateOfAgenda"].ToString())));
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("_partials/_Grid", new List<Agenda>());
            }
        }
        #endregion

        #region ImportData
        public ActionResult AddImportAgenda(bool CreateCommerce)
        {
            var returnList = new List<AgendaImport>();
            var list = new List<AgendaImport>();
            var duplicatenumber = 0;
            try
            {
                HttpPostedFileBase file = Request.Files[0];
                var fileName = (dynamic)null;

                #region Go through the file
                if (file != null && file.ContentLength > 0)
                {
                    fileName = Path.GetFileName(file.FileName);
                    var path = Path.GetTempPath() + fileName;
                    file.SaveAs(path);

                    using (TextFieldParser csvReader = new TextFieldParser(path, System.Text.Encoding.UTF7))
                    {
                        list = GetFileData(csvReader);
                        duplicatenumber = list.GroupBy(a => new { a.CommerceName, a.Date }).Where(b => b.Count() > 1).Sum(c => c.Count());
                    }
                }
                #endregion


                if ((list.Count != 0) && (msj == ""))
                {
                    using (var bus = new AgendaBusiness())
                    {
                        returnList = bus.ImportData(list, CreateCommerce).ToList();                        
                    }

                    #region Event log
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Importación de Agenda, resultado completo");
                    #endregion                                  
                }

                ViewBag.DownloadResultFile = returnList.Count > 0 ? true : false;
                ViewBag.ResultData = string.Format("{0}|{1}|{2}|{3}|{4}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"), list.Count - returnList.Count, returnList.Count, msj, duplicatenumber);
                Session["NonSavedList"] = returnList;
                msj = "";

                return View("Index", new AgendaBase());
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e, true);

                ViewBag.DownloadResultFile = returnList.Count > 0 ? true : false;
                ViewBag.ResultData = string.Format("{0}|{1}|{2}|{3}|{4}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"), list.Count - returnList.Count, returnList.Count, e.Message, duplicatenumber);
                Session["NonSavedList"] = returnList;
                msj = "";

                return View("Index", new AgendaBase());
            }
        }

        private List<AgendaImport> GetFileData(TextFieldParser csvReader)
        {
            int lineNumber = 1;
            var list = new List<AgendaImport>();
            var delimiterReader = csvReader;

            csvReader.SetDelimiters(",");
            csvReader.HasFieldsEnclosedInQuotes = true;
            csvReader.TrimWhiteSpace = true;
            string[] colHeader = null;

            while (!csvReader.EndOfData)
            {
                string[] colFields = csvReader.ReadFields();

                if (colFields.Length >= 6)
                {
                    foreach (var column in colFields.Where(x => x != ""))
                    {
                        try
                        {
                            DateTime date;
                            var item = new AgendaImport();

                            if (!DateTime.TryParse(TryToGetDate(colFields[0]).ToString(), out date))
                            {
                                if (lineNumber == 1 && colFields[0].ToString() == "Fecha")
                                {
                                    colHeader = colFields;
                                    break;
                                }
                                else
                                {
                                    msj = "En la línea número " + lineNumber + " se produjo un error de formato en el campo Fecha. El formato debe ser el siguiente: AAAA-MM-DD HH:MM:SS";
                                    break;
                                }
                            }
                            else
                            {
                                item.Date = TryToGetDate(colFields[0]);
                                if (item.Date < DateTime.Today)
                                {
                                    msj = "En la línea número " + lineNumber + " la fecha debe ser igual o mayor al día actual.";
                                    break;
                                }
                            }

                            try
                            {
                                if (string.IsNullOrEmpty(colFields[1]))
                                {
                                    msj = "En la línea número " + lineNumber + " se produjo un error de formato en el campo Código. Asegurese que el documento contenga un valor.";
                                    break;
                                }
                                else
                                {
                                    item.Code = colFields[1];
                                }
                            }
                            catch (Exception)
                            {
                                msj = "En la línea número " + lineNumber + " se produjo un error de formato en el campo Código. Asegúrese que el documento contenga un valor.";
                                break;
                            }

                            try
                            {
                                if (string.IsNullOrEmpty(colFields[2]))
                                {
                                    msj = "En la línea número " + lineNumber + " se produjo un error de formato en el campo Comercio. Asegúrese que el documento contenga un valor.";
                                    break;
                                }
                                else
                                {
                                    item.CommerceName = colFields[2];
                                }
                            }
                            catch (Exception)
                            {
                                msj = "En la línea número " + lineNumber + " se produjo un error de formato en el campo Comercio. Asegurese que el documento contenga un valor.";
                                break;
                            }

                            try
                            {
                                item.Latitude = double.Parse(colFields[3].Replace(",", "."), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, NumberFormatInfo.InvariantInfo); //Convert.ToDouble(colFields[3]);
                            }
                            catch (Exception)
                            {
                                msj = "En la línea número " + lineNumber + " se produjo un error de formato en el campo Latitud. El formato debe ser el siguiente: 9.94516455";
                                break;
                            }

                            try
                            {
                                item.Longitude = double.Parse(colFields[4].Replace(",", "."), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, NumberFormatInfo.InvariantInfo); //Convert.ToDouble(colFields[4]);
                            }
                            catch (Exception)
                            {
                                msj = "En la línea número " + lineNumber + " se produjo un error de formato en el campo Longitud. El formato debe ser el siguiente: -84.19500755";
                                break;
                            }

                            try
                            {
                                if (EmailValidation(colFields[5].Trim()))
                                {
                                    item.UserEmail = ECOsystem.Utilities.TripleDesEncryption.Encrypt(colFields[5].Trim());
                                }
                                else
                                {
                                    msj = "En la línea número " + lineNumber + " se produjo un error de formato en el campo Email Usuario. El formato debe ser el siguiente: agente@dominio.com";
                                    break;
                                }
                            }
                            catch (Exception)
                            {
                                msj = "En la línea número " + lineNumber + " se produjo un error de formato en el campo Email Usuario. El formato debe ser el siguiente: agente@dominio.com";
                                break;
                            }


                            var dynamicCol = new DynamicColumnsBase();
                            dynamicCol = Session["AgendaColumns"] as DynamicColumnsBase;

                            //Dynamic Columns Insert
                            List<AditionalColumns> listDynamicCol = new List<AditionalColumns>();

                            for (int i = 6; i < colFields.Length; i++)
                            {
                                AditionalColumns col = new AditionalColumns();
                                try
                                {
                                    if (string.IsNullOrEmpty(colFields[i]))
                                    {
                                        msj = "En la línea número " + lineNumber + " se produjo un error en el campo número " + i + ". Asegúrese que el documento contenga un valor.";
                                        break;
                                    }
                                    else
                                    {
                                        if (colHeader != null)
                                        {

                                            for (int j = 0; j < dynamicCol.ListColums.Count; j++)
                                            {
                                                if (dynamicCol.ListColums[j].Value.ToString() == string.Format(colHeader[i].ToString()))
                                                {
                                                    col.Id = dynamicCol.ListColums[j].Id;
                                                    break;
                                                }

                                            }
                                            col.Title = colHeader[i];
                                            col.Value = colFields[i];
                                            listDynamicCol.Add(col);
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                    msj = "En la línea número " + lineNumber + " se produjo un error en el campo número " + i + ". Asegurese que el documento contenga el valor.";
                                    break;
                                }
                            }

                            item.ColumnsXml = ECOsystem.Utilities.Miscellaneous.GetXML(listDynamicCol);

                            list.Add(item);
                            lineNumber++;
                            break;
                        }
                        catch (Exception)
                        {
                            msj = "En la línea número " + lineNumber + " se produjo un error al leer los datos. Favor verificar que todos los campos esten completos y cumplan con el formato.";
                            break;
                        }
                    }
                }
                else
                {
                    msj = string.Format("Cada línea debe poseer mínimo 6 columnas, separadas cada una por coma (,).");
                }

                if (msj != "")
                {
                    list = new List<AgendaImport>();
                    break;
                }
            }
            return list;
        }

        private string GetDelimiters(TextFieldParser csvReader, string delimiter)
        {
            csvReader.SetDelimiters(delimiter);
            csvReader.HasFieldsEnclosedInQuotes = true;
            csvReader.TrimWhiteSpace = true;

            string[] colFields = csvReader.ReadFields();

            if (colFields.Length == 6)
            {
                return delimiter;
            }
            else
            {
                return string.Empty;
            }
        }

        public ActionResult DonwloadImportResults()
        {
            var list = (List<AgendaImport>)Session["NonSavedList"];
            Session["NonSavedList"] = null;

            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms, System.Text.Encoding.GetEncoding(1252));

                #region HeaderReport
                sw.WriteLine("Fecha: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                sw.Flush();
                sw.WriteLine("Importación de Agenda");
                sw.Flush();
                sw.WriteLine("Fecha, Codigo, Comercio, Latitud, Longitud, Código Agente, Descripción");
                sw.Flush();
                #endregion

                foreach (var item in list)
                {
                    sw.WriteLine(DateTime.Parse(item.Date.ToString()).ToString("dd/MM/yyyy hh:mm:ss tt") + ", " + item.Code + ", " + item.CommerceName +
                                 ", " + item.Latitude + ", " + item.Longitude + ", " + ECOsystem.Utilities.TripleDesEncryption.Decrypt(item.UserEmail) +
                                 ", " + item.ErrorMessage);
                    sw.Flush();
                }
                return new FileStreamResult(new MemoryStream(ms.ToArray()), "text/plain")
                {
                    FileDownloadName = "Resultado de Importación de Agenda - Datos NO Almacenados.csv"
                };
            }
        }

        public ActionResult DonwloadExampleFile()
        {
            byte[] SummaryFile = System.IO.File.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data/Ejemplo_Importacion_de_Agenda.csv")));

            return new FileStreamResult(new MemoryStream(SummaryFile), "text/csv")
            {
                FileDownloadName = "Ejemplo_Importacion_de_Agenda.csv"
            };
        }

        //Get the dates and verifies the format
        private DateTime? TryToGetDate(string date)
        {
            try
            {
                DateTime dateOut;
                if (DateTime.TryParseExact(date.Replace(" AM", "").Replace(" PM", ""), "dd/MM/yyyy H:mm:ss", CultureInfo.CreateSpecificCulture("es-CR"), DateTimeStyles.None, out dateOut))
                {
                    return DateTime.ParseExact(date.Replace(" AM", "").Replace(" PM", ""), "dd/MM/yyyy H:mm:ss", CultureInfo.CreateSpecificCulture("es-CR"));
                }
                return DateTime.Parse(date);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private bool EmailValidation(string email)
        {
            try
            {
                String format;
                format = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
                if (Regex.IsMatch(email, format))
                {
                    if (Regex.Replace(email, format, String.Empty).Length == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Columns  

        public ActionResult OnDeleteColumn(int id)
        {
            try
            {
                var columns = (DynamicColumnsBase)Session["AgendaColumns"];
                columns.ListColums.RemoveAll(x => x.Id == id);
                Session["AgendaColumns"] = columns;
                return PartialView("_partials/_GridColumns", columns);
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error en Agenda - Borrar Columnas, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Borrar Columnas", TechnicalError = e.Message });
            }
        }

        public ActionResult OnAddNewColumn(string ColumName)
        {
            try
            {
                var columns = (DynamicColumnsBase)Session["AgendaColumns"];
                var item = new AdittionalAgendaColumn()
                {
                    Id = (columns.MaxId == null ? 1 : (int)columns.MaxId) + 1,
                    Value = ColumName
                };
                columns.ListColums.Add(item);
                columns.MaxId = (columns.MaxId == null ? 1 : (int)columns.MaxId) + 1;
                Session["AgendaColumns"] = columns;
                return PartialView("_partials/_GridColumns", columns);
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error en Agenda - Agregar Columnas, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = " Agregar Columnas", TechnicalError = e.Message });
            }
        }

        public ActionResult SaveColumns()
        {
            try
            {
                var columns = (DynamicColumnsBase)Session["AgendaColumns"];
                using (var bus = new AgendaBusiness())
                {
                    bus.ManagementParametersAddorEditColumns(columns, ECOsystem.Utilities.Session.GetCustomerId(), ECOsystem.Utilities.Session.GetUserInfo().UserId);
                }
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se almacenan los cambios correspondientes a las columnas - Agregar Editar Columnas, Datos almacenados: {0}", JsonConvert.SerializeObject(columns)));
                return Json("success");
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error en Agenda - Agregar Editar Columnas, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = " Agregar Columnas", TechnicalError = e.Message });
            }
        }
        #endregion

    }
}