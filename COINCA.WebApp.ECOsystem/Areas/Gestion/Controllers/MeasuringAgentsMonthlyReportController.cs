﻿using ECO_Management.Business;
using ECO_Management.Models;
using ECO_Management.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Management.Controllers.Management
{
    public class MeasuringAgentsMonthlyReportController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetReportData(MeasuringAgentsMonthlyReportParameters model)
        {
            try
            {
                using (var bus = new MeasuringAgentsMonthlyReportBusiness())
                {
                    var data = bus.GetReportDataTable(model);

                    if(data.Rows.Count > 0)
                    {
                        Session["Reporte"] = data;
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Genera información para el Reporte de Cumplimiento de Agentes, parámetros: Fecha Inicio: {0}, Fecha Fin: {1}", model.StartDateStr, model.EndDateStr));
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    return Json("NoData", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Generar información para el Reporte de Cumplimiento de Agentes, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Reporte de Cumplimiento de Agentes", TechnicalError = e.Message });
            }
        }

        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de Cumplimiento de Agentes");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "MeasuringAgentsMonthlyReport", this.ToString().Split('.')[2], "Reporte de Cumplimiento de Agentes");
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}