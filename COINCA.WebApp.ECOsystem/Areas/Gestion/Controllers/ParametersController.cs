﻿using ECO_Management.Business;
using ECO_Management.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Management.Business;
using ECOsystem.Management.Models;
using ECOsystem.Models.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Management.Controllers
{
    public class ParametersController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var bus = new ParametersBusiness())
            {
                var model = bus.ManagementParametersRetrieve(ECOsystem.Utilities.Session.GetCustomerId());
                return View(model);
            }
        }

        #region Parameters        
        public ActionResult ManagementParametersAddOrEdit(ECOsystem.Management.Models.Parameters model)
        {
            try
            {
                var columns = (DynamicColumnsBase)Session["AgendaColumns"];
                using (var bus = new ParametersBusiness())
                {
                    bus.ManagementParametersAddOrEdit(model, ECOsystem.Utilities.Session.GetCustomerId(), ECOsystem.Utilities.Session.GetUserInfo().UserId);         
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edición de Parámetros de Gestión - Cumplimiento, Parámetros: Tiempo Sincronización de Datos {0}, " +
                                                                                "Nombre para identificar movimientos de agentes: {1}, Tipo Visualización de Reconstrucción: {2}",
                                                                                model.DataSync, model.MovementName, model.ReconstructionType));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la Edición de Parámetros de Gestión - Cumplimiento, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Edición de Parámetros de Gestión", TechnicalError = e.Message });
            }
        }

        public ActionResult ManagementParametersGeofencesAddOrEdit(ECOsystem.Management.Models.Parameters model)
        {
            try
            {
                using (var bus = new ParametersBusiness())
                {
                    bus.ManagementParametersGeofencesAddOrEdit(model, ECOsystem.Utilities.Session.GetCustomerId(), ECOsystem.Utilities.Session.GetUserInfo().UserId);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edición de Parámetros de Gestión - Margenes, Parámetros: Distancia {0}, ",
                                                                                model.GeofenceDistance));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la Edición de Parámetros de Gestión - Margenes, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Edición de Parámetros de Gestión", TechnicalError = e.Message });
            }
        }
        #endregion


    }
}