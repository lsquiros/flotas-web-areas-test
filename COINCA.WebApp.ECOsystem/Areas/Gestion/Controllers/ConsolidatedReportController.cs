﻿using ECO_Management.Business;
using ECO_Management.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ECO_Management.Controllers.Management
{
    public class ConsolidatedReportController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetReportData(ConsolidatedParameters parameters)
        {
            try
            {
                using (var business = new ConsolidatedReportBusiness())
                {
                    DataSet datosReporte = business.GetReportDownloadData(parameters);
                    Session["Reporte"] = datosReporte;
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Genera información para el Reporte Consolidado, parámetros: Agentent: {0}, Fecha Inicio: {1}, Fecha Fin: {2}", parameters.UserId, parameters.StartDateStr, parameters.EndDateStr));

                    if (datosReporte != null)
                    {
                        if (datosReporte.Tables.Count > 2)
                        {
                            if (datosReporte.Tables[2].Rows.Count > 0)
                            {
                                if (datosReporte.Tables[0].Rows.Count > 0)
                                    return Json("Success", JsonRequestBehavior.AllowGet);
                                else
                                    return Json("NoAgendaAndSurvey", JsonRequestBehavior.AllowGet);
                            }
                            else
                                return Json("NoData", JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json("NoData", JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("NoData", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataSet report = new DataSet();
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Consolidado");
                        return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "ConsolidatedReport", this.ToString().Split('.')[2], "Reporte Consolidado");
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}