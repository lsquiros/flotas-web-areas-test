﻿using ECO_Management.Business;
using ECO_Management.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Models.Identity;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Management.Controllers.Management
{
    public class ManagementAgentSummaryReportController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetReportData(ManagementAgentSummaryParameters model)
        {
            try
            {
                using (var bus = new ManagementAgentSummaryReportBusiness())
                {
                    var ReportData = bus.GetReportDataTable(model);
                    if (ReportData.Rows.Count > 0)
                    {
                        Session["Reporte"] = ReportData;
                    }
                    else
                        return Json("NoData", JsonRequestBehavior.AllowGet);

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Genera información para el Reporte Gestión por Agente, parámetros: Agentent: {0}, Fecha Inicio: {1}, Fecha Fin: {2}", model.UserId, model.StartDateStr, model.EndDateStr));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Generar información para el Reporte Gestión por Agente, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Reporte Gestion por Agente", TechnicalError = e.Message });
            }
        }

        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Gestion por Agente");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "ManagementAgentSummaryReport", this.ToString().Split('.')[2], "Reporte Gestion por agente");
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}