﻿using ECO_Management.Business;
using ECO_Management.Models;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECO_Management.Models.Identity;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Management.Controllers.Management
{
    public class SurveyReportController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSurveyList(int userId, string date)
        {
            try
            {
                using (var bus = new SurveyReportBusiness())
                {
                    return PartialView("_partials/_Grid", bus.RetrieveSurveysToDownload(userId, Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(date))));                    
                }
            }
            catch (Exception e)
            {
                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetReportData(int UserId, int CommerceId, string DateOfCheck, int SurveyId, int UserEventId)
        {
            try
            {
                using (var bus = new SurveyReportBusiness())
                {
                    var ReportData = bus.GetReportDataTable(UserId, CommerceId, Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(DateOfCheck)), SurveyId, UserEventId);
                    if (ReportData.Rows.Count > 0)
                    {
                        Session["Reporte"] = ReportData;
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("NoData", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }

        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Formulario");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "SurveyReport", this.ToString().Split('.')[2], "Reporte Formulario");
                    }
                }
                return View("Index");
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }

        [HttpPost]
        public ActionResult LoadCommercesbyAgent(int UserId, string Date)
        {
            using (var business = new ManagementMapBusiness())
            {
                var list = business.CommerceByAgentsAndDate(UserId, Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(Date)));

                if (list == null)
                    return Json(null);

                List<ManagementListCombo> modelList = (List<ManagementListCombo>)list.ToList();
                return Json(modelList.AsEnumerable(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}