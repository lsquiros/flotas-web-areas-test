﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECO_Management.Business;
using System.Data;
using ECOsystem.Utilities;
using ECOsystem.Business.Utilities;
using Newtonsoft.Json;
using ECOsystem.Models.Core;
using ECO_Management.Models;
using ECO_Management.Models.Identity;

namespace ECO_Management.Controllers.Management
{
    public class AgentVisitGeographicInformationController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetReportData(AgentVisitParameters model)
        {
            try
            {
                using (var bus = new AgentVisitGeographicInformationBusiness())
                {
                    var data = bus.GetReportDataTable(model);

                    if(data.Rows.Count > 0)
                    {
                        Session["Reporte"] = data;
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Genera información para el Reporte de Visitas por Zona Geopolítica, parámetros: Agentent: {0}, Fecha Inicio: {1}, Fecha Fin: {2}", model.UserId, model.StartDateStr, model.EndDateStr));
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    return Json("NoData", JsonRequestBehavior.AllowGet);
                }                
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Generar información para el Reporte de Visitas por Zona Geopolítica, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Reporte de Visitas por Zona Geopolítica", TechnicalError = e.Message });
            }
        }

        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataTable report = new DataTable();
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de Visitas por Zona Geopolítica");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "AgentVisitGeographicInformation", this.ToString().Split('.')[2], "Reporte de Visitas por Zona Geopolítica");
                    }
                }
                
                return null;
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}