using ECO_Management.Models;
using ECO_Management.Models.Identity;
using ECO_Management.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECO_Management.Business;
using System.Web.Script.Serialization;
using ECOsystem.Business.Utilities;
using ECO_Management.Models;

namespace ECO_Management.Controllers
{
    public class QuestionsByProtocolsController : Controller
    {
        private const string QUESTIONS_TYPE_SUMMARY = "QUESTIONS_TYPE_SUMMARY";

        [EcoAuthorize]
        public ActionResult Index()
        {
            #region Clean Session Vars
            Session["QuestionOrder"] = null;
            Session["CurrentOrder"] = null;
            Session["AnswerOrder"] = null;
            Session["SummaryQuestionId"] = null;
            Session["SummaryProgramProtocolId"] = null;
            #endregion

            return View();
        }

        public ActionResult GetQuestionToProtocol(int? QuestionId, string QuestionText, int ProtocolIdToForm, bool? RelationId, int? ProgramProtocolId, int? AnswerId)
        {
            try
            {
                using (var bus = new QuestionsByProtocolBusiness())
                {
                    if (QuestionId == null)
                    {
                        //Add the question if the question does not exist
                        QuestionId = bus.AddOrEditQuestion(QuestionId, QuestionText, ProtocolIdToForm).Id;
                        
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Inserta Preguntas: {0}, Cliente = {1}, Id de Cliente = {2}",
                                                       new JavaScriptSerializer().Serialize(QuestionId + QuestionText), ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    }

                    int typeAnswer = 1;

                    if (RelationId != null)
                    {
                        //Insert the relation 
                        typeAnswer = bus.AddOrEditProgramProtocol(Id: ProgramProtocolId, ProtocolId: ProtocolIdToForm, AnswerId: AnswerId, Relation: Convert.ToInt32(Session["QuestionOrder"].ToString()), NextQuestion: QuestionId);

                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Inserta Relación: Respuesta = {0}, Pregunta = {1}", AnswerId, QuestionId, ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    }

                    if (typeAnswer != 0)
                    {
                        if(Session["SummaryQuestionId"] != null)
                        {
                            ProgramProtocolId = bus.AddOrEditProgramProtocol(ProtocolId: ProtocolIdToForm, QuestionId: QuestionId, Order: Convert.ToInt32(Session["QuestionOrder"].ToString()));

                            new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Inserta ProgramProtocols: QuestionId = {0}, ProtocolId = {1}, Cliente = {2}, Id de Cliente = {3}",
                                                        QuestionId, ProgramProtocolId, ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));

                            Session["QuestionOrder"] = Session["QuestionOrder"] != null ? (Convert.ToInt32(Session["QuestionOrder"].ToString()) + 1) : 1;
                            ProgramProtocolId = bus.AddOrEditProgramProtocol(Id: Convert.ToInt32(Session["SummaryProgramProtocolId"]), ProtocolId: ProtocolIdToForm, QuestionId: Convert.ToInt32(Session["SummaryQuestionId"].ToString()), Order: Convert.ToInt32(Session["QuestionOrder"].ToString()));

                            new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Actualiza ProgramProtocols: QuestionId = {0}, ProtocolId = {1}, Cliente = {2}, Id de Cliente = {3}",
                                                        Convert.ToInt32(Session["SummaryQuestionId"].ToString()), ProgramProtocolId, ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));

                        }
                        else
                        {
                            //Gets the order based on the previous insertion 
                            Session["QuestionOrder"] = Session["QuestionOrder"] != null ? (Convert.ToInt32(Session["QuestionOrder"].ToString()) + 1) : 1;
                            ProgramProtocolId = bus.AddOrEditProgramProtocol(ProtocolId: ProtocolIdToForm, QuestionId: QuestionId, Order: Convert.ToInt32(Session["QuestionOrder"].ToString()));
                        }

                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Inserta ProgramProtocols: QuestionId = {0}, AnswerId = {1}, ProtocolId = {2}, Relación: = {3}, ProgramProtocols = {4}, Cliente = {5}, Id de Cliente = {6}",
                                                   QuestionId, AnswerId, ProtocolIdToForm, RelationId, ProgramProtocolId, ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));

                        return PartialView("_partials/_QuestionDetail", new QuestionsDetail()
                        {
                            QuestionId = QuestionId,
                            Question = QuestionText,
                            ProgramProtocolId = (int)ProgramProtocolId,
                            Order = Session["SummaryQuestionId"] != null ? (Convert.ToInt32(Session["QuestionOrder"].ToString()) - 1) : Convert.ToInt32(Session["QuestionOrder"].ToString())
                        });
                    }
                    else
                    {
                        return Json("",JsonRequestBehavior.AllowGet);
                    }
                    //Add the question and the protocol to the program protocol
                }   
                //Returns partial with the model
            }   
            catch (Exception e)
            {
                return Json("Error" + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        //Update the question's text
        public ActionResult UpdateQuestionByCustomer(int QuestionId, string QuestionText,int ProtocolId)
        {
            try
            {
                using (var bus = new QuestionsByProtocolBusiness())
                {
                    var text = bus.AddOrEditQuestion(QuestionId, QuestionText, ProtocolId).Text;

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edita Pegunta: = {0}, Cliente = {1}, Id de Cliente = {2}", new JavaScriptSerializer().Serialize(QuestionId + QuestionText), ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    return Json(text, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddSummaryQuestionToProtocol( int ProtocolId, string QuestionText)
        {
            try
            {
                Session["QuestionOrder"] = Session["QuestionOrder"] != null ? (Convert.ToInt32(Session["QuestionOrder"].ToString()) + 1) : 1;

                using (var bus = new QuestionsByProtocolBusiness())
                {
                    //Add the question if the question does not exist
                    int? QuestionId = bus.AddOrEditQuestion(null, QuestionText, ProtocolId).Id;

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Inserta Preguntas: {0}, Cliente = {1}, Id de Cliente = {2}",
                                                    new JavaScriptSerializer().Serialize(QuestionId + QuestionText), ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    Session["SummaryQuestionId"] = QuestionId;

                    //Insert the relation 
                    int ProgramProtocolId = bus.AddOrEditProgramProtocol(ProtocolId: ProtocolId, QuestionId: QuestionId, Order: Convert.ToInt32(Session["QuestionOrder"].ToString()), TypeCode: QUESTIONS_TYPE_SUMMARY);
                        
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Inserta ProgramProtocols: QuestionId = {0}, ProtocolId = {1}, Cliente = {2}, Id de Cliente = {3}",
                                                QuestionId, ProgramProtocolId, ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    Session["SummaryProgramProtocolId"] = ProgramProtocolId;

                    return Json("", JsonRequestBehavior.AllowGet);
                    
                }

            }
            catch (Exception e)
            {
                return Json("Error" + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        //Update the Answer's text
        public ActionResult UpdateAnswerByCustomer(int QuestionId, string QuestionText)
        {
            try
            {
                using (var bus = new QuestionsByProtocolBusiness())
                {
                    var text = bus.AddOrEditAnswer(QuestionId, QuestionText);

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edita Respuesta: = {0}, Cliente = {1}, Id de Cliente = {2}", new JavaScriptSerializer().Serialize(QuestionId + QuestionText), ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    return Json(text, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        //Add a new answer to the question
        public ActionResult AddNewAnswer(int? AnswerId, int OrderId, int ProgramProtocolId, string TypeCode, string AnswerText)
        {
            try
            {
                using (var bus = new QuestionsByProtocolBusiness())
                {
                    //If the current order in the question is different set new one
                    if (Session["CurrentOrder"] == null || Convert.ToInt16(Session["CurrentOrder"].ToString()) != OrderId)
                    {
                        Session["CurrentOrder"] = OrderId;
                        Session["AnswerOrder"] = bus.GetAnswerCountRetrive(ProgramProtocolId, OrderId) + 1; 
                    }
                    else
                    {
                        Session["AnswerOrder"] = Convert.ToInt16(Session["AnswerOrder"].ToString()) + 1; 
                    }

                    if (AnswerId.ToString().Equals(AnswerText)) {
                        AnswerId = null;
                    }
                
                    if (AnswerId == null)
                    {
                        //If the answer does not exit, add new one
                        AnswerId = bus.AddOrEditAnswer(AnswerId, AnswerText);

                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format((AnswerId == null ? "Agrega" : "Edita") + " Respuesta: = {0}, Cliente = {1}, Id de Cliente = {2}",
                                                      new JavaScriptSerializer().Serialize(AnswerId + AnswerText), ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    }
                    //Update the program protocol, add answerid and type
                    bus.AddOrEditProgramProtocol(Id: ProgramProtocolId, AnswerId: AnswerId, TypeCode: TypeCode);

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edita ProgramProtocols: Respuesta = {0}, Cliente = {1}, Id de Cliente = {2}",
                                                      new JavaScriptSerializer().Serialize(AnswerId + AnswerText), ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                
                    return Json(new { AnswerId = AnswerId, AnswerOrderId = Session["AnswerOrder"].ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        //Update the type when it is text
        public ActionResult UpdateProgramProtocol(int programProtocolId, string typeCode, int orderId, int? calification, int? answerId)
        {
            try
            {
                using (var bus = new QuestionsByProtocolBusiness())
                {
                    bus.AddOrEditProgramProtocol(Id: programProtocolId, AnswerId: answerId, Clasification: calification, Order: orderId, TypeCode: typeCode);

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edita ProgramProtocols = {0}, TypeCode = {1}, Calification = {2}, AnswerId = {3}, Cliente = {4}, Id de Cliente = {5}",
                                                     programProtocolId, typeCode, calification, answerId, ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    return Json("success");
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        //Delete Question by Customer
        public ActionResult DeleteQuestion(int QuestionId, int ProtocolId,bool Summary = false)
        {
            try
            {
                if(Summary)
                    QuestionId = Session["SummaryQuestionId"] != null ? Convert.ToInt32(Session["SummaryQuestionId"].ToString()) : QuestionId;

                using (var bus = new QuestionsByProtocolBusiness())
                {
                    var info =  ECOsystem.Utilities.Session.GetCustomerInfo();
                    int retorno = bus.DeleteQuestion(QuestionId, ProtocolId);
                    var list = bus.RetrieveQuestionsAndAnswers( ProtocolId: ProtocolId);
                    var Order = 0;
                    for (int i = 0; i <= list.Count();i++) {
                        var item = list.ElementAt(i);
                        if ((item.QuestionId > QuestionId) && (!Summary))
                        {
                             Order = Order == 0 ? list.ElementAt((i - 1)).Order + 1 : Order + 1 ;
                            bus.AddOrEditProgramProtocol(item.ProgramProtocolId,item.ProtocolId,item.QuestionId,item.AnswerId,item.Clasification,item.Relation,Order,item.Code,item.NextQuestion);
                        }
                    }

                    Session["QuestionOrder"] = Session["QuestionOrder"] != null ? (Convert.ToInt32(Session["QuestionOrder"].ToString()) - 1) : 1;

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Edita QuestionID = {0}, Cliente = {1}, Id de Cliente = {2}",
                                                    QuestionId, info.DecryptedName, info.CustomerId));
                     
                    return Json(retorno);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        

        //Load Current Protocol Information
        public ActionResult ShowCurrentProtocol(int ProtocolId)
        {
            try
            {
                Session["QuestionOrder"] = null;
                using (var bus = new QuestionsByProtocolBusiness())
                {
                    var list = bus.RetrieveQuestionsAndAnswers(ProtocolId);
                    foreach (QuestionsDetailShow item in list)
                    {
                        if (item.Answer != null)
                        {
                            if (item.Answer.StartsWith("'"))
                                item.Answer = item.Answer.Replace("'", "");
                        }

                        if(item.Code == QUESTIONS_TYPE_SUMMARY)
                        {
                            Session["SummaryQuestionId"] = item.QuestionId;
                            Session["SummaryProgramProtocolId"] = item.ProgramProtocolId; }
                    }
                        Session["QuestionOrder"] = list.Count() == 0 ? 0 : list.Max(x => x.Order);
                        return PartialView("_partials/_QuestionDetailShow", list);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

 

        //Load Information for Simulator
        public ActionResult ShowProtocolSimulator(int ProtocolId)
        {
            using (var bus = new QuestionsByProtocolBusiness())
            {
                var list = bus.RetrieveQuestionsAndAnswers(ProtocolId).Where(x => x.Code != null).ToList();

                foreach (var item in list.Where(x => x.Relation != null).Select(s => s.Relation).Distinct())
                {
                    list.Where(a => a.Order == item).ToList().ForEach(x => x.IsRelation = true);    
                }

                Session["SimulationList"] = list;
                return PartialView("_partials/_ProtocolSimulation", list);
            }
        }

        //Get the next question to in the protocol
        public ActionResult GetNextQuestionSimulation(int? NextQuestion, int? Relation)
        {
             
            var list = new List<QuestionsDetailShow>();
            var currentList = (List<QuestionsDetailShow>)Session["SimulationList"];
            QuestionsDetailShow next;

            if (Relation == null)   
            {
                if (currentList.Where(x => x.Order == NextQuestion).FirstOrDefault().Code  == "QUESTIONS_TYPE_RADIO" && NextQuestion==1)
                {
                    NextQuestion += 1;
                }
                 list = currentList.Where(x => x.Order == NextQuestion).ToList();
                 next = currentList.Where(a => a.Order > NextQuestion ).ToList().FirstOrDefault();
            }
            else
            {
                list = currentList.Where(x => x.Order == Relation).ToList();
                 next = currentList.Where(a => a.Order > Relation ).ToList().FirstOrDefault();
            }
            
            if(next != null)
            {
                list.ToList().ForEach(x => x.NextQuestion = next.Order);
            }            

            return Json(list, JsonRequestBehavior.AllowGet);
        }   

        #region Methods for Select2 
        public ActionResult GetQuestionsByCustomer(int? ProtocolId = null)
        {
            #region Clean Session Vars
            Session["SummaryQuestionId"] = null;
            Session["SummaryProgramProtocolId"] = null;
            #endregion

            using (var bus = new QuestionsByProtocolBusiness())
            {
                int protId = ProtocolId != null ? (int)ProtocolId : 0;
                var list = bus.RetrieveQuestionsAndAnswers(ProtocolId: protId);
                foreach (var item in list)
                {
                    if (item.Code == QUESTIONS_TYPE_SUMMARY)
                    {
                        Session["SummaryQuestionId"] = item.QuestionId;
                        Session["SummaryProgramProtocolId"] = item.ProgramProtocolId;
                    }
                }
            }

            return Json(ECO_Management.Utilities.GeneralCollections.GetQuestionsByCostumers(ProtocolId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAnswersByCustomer()
        {
            return Json(ECO_Management.Utilities.GeneralCollections.GetAnswersByCostumers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAnswerLevels()
        {
            return Json(ECO_Management.Utilities.GeneralCollections.GetAnswersLevels, JsonRequestBehavior.AllowGet);
        }
        #endregion        
    }
}
