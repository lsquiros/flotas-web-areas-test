﻿using ECO_Management.Business;
using ECO_Management.Models;
using ECO_Management.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Management.Controllers.Management
{
    public class ManagementAgentReportController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetReportData(int? UserId, string StartDateStr, string EndDateStr)
        {
            try
            {
                using (var bus = new ManagementAgentReportBusiness())
                {
                    var reportData = bus.GetReportDataSet(UserId, StartDateStr, EndDateStr);
                    if (reportData.Tables[1].Rows.Count > 0)
                    {
                        Session["Reporte"] = reportData;
                    }
                    else
                        return Json("NoData", JsonRequestBehavior.AllowGet);
                    
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Genera información para el Reporte Resumen Gestión por Agentes, parámetros: Agentent: {0}, Fecha Inicio: {1}, Fecha Fin: {2}", UserId, StartDateStr, EndDateStr));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Generar información para el Reporte Resumen Gestión por Agentes, Error: {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = "Reporte Resumen Gestion por Agentes", TechnicalError = e.Message });
            }
        }

        public ActionResult DonwloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataSet report = new DataSet();
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Resumen Gestion por Agentes");
                        return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "ManagementAgentReport", this.ToString().Split('.')[2], "Reporte Resumen Gestion por Agentes");
                    }
                }
                return View("Index");
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}