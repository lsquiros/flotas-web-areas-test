﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using System.Collections.Generic;
using ECOsystem.Models.Account;
using GridMvc.DataAnnotations;

namespace ECO_Management.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ProtocolsBase
    {
        public ProtocolsBase()
        {
            Data = new Protocols();
            List = new List<Protocols>();
            Menus = new List<AccountMenus>();    
        }

        /// <summary>
        /// 
        /// </summary>
        public Protocols Data { get; set; }
     
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        public IEnumerable<Protocols> List { get; set; }
    }


    /// <summary>
    /// Partner Models
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Protocols : ModelAncestor
    {
        /// <summary>
        /// Name of the protocol
        /// </summary>
        [NotMappedColumn]
        public int? Id { get; set; }

        /// <summary>
        /// Name of the protocol
        /// </summary>
        [NotMappedColumn]
        [Required(ErrorMessage = "Ingrese Nombre del Formulario")]
        public string Name { get; set; }

        /// <summary>
        /// CustomerId
        /// </summary>
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// UserId
        /// </summary>
        [NotMappedColumn]
        public int UserId { get; set; }
    }
}