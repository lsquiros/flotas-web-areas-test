﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Management.Models
{
    public class AnswersByProtocol
    {
        public int? Id { get; set; }
                
        public string Text { get; set; }

        public int CustomerId { get; set; }
    }
}