﻿using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECO_Management.Models
{
    public class AgentVisitGeographicInformation
    {
        public int VisitCount { get; set; }

        public string GeopoliticalLevel1 { get; set; }

        public string GeopoliticalLevel2 { get; set; }

        public string GeopoliticalLevel3 { get; set; }

        public string EncryptName { get; set; }

        public string DecryptName { get { return TripleDesEncryption.Decrypt(EncryptName); } }        
    }

    public class AgentVisitParameters
    {
        public int? UserId { get; set; }

        public DateTime? StartDate { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        public DateTime? EndDate { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }
    }
}