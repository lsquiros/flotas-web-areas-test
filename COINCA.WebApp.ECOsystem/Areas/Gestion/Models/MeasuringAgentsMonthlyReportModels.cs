﻿using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECO_Management.Models
{
    public class MeasuringAgentsMonthlyReport
    {
        public string AgentWorkTime { get; set; }

		public string AgentClientTime { get; set; }

		public string AgentMovingTime { get; set; }

        public string AgentLostTime { get; set; }

		public DateTime AgentLogIn { get; set; }

		public DateTime AgentLogOut { get; set; }

		public int ClientDifference { get; set; }

		public int MovingDifference { get; set; }

        public int LostDifference { get; set; }

        public int LogInDifference { get; set; }

        public int LogOutDifference { get; set; }

		public string ClientTime { get; set; }

		public string LostTime { get; set; }

		public string MovingTime { get; set; }

		public int StartTime { get; set; }

        public string StartTimeClient { get { return new DateTime(AgentLogIn.Year, AgentLogIn.Month, AgentLogIn.Day, StartTime, 0, 0).ToString("hh:mm tt"); } }

        public int EndTime { get; set; }

        public string EndTimeClient { get { return new DateTime(AgentLogOut.Year, AgentLogOut.Month, AgentLogOut.Day, EndTime, 0, 0).ToString("hh:mm tt"); ; } }        

		public string EncryptDriverName { get; set; }

        public string DecryptName { get { return TripleDesEncryption.Decrypt(EncryptDriverName); } }

        public int? TopMargin { get; set; }

        public int? LowMargin { get; set; }

        public int? TopPercent { get; set; }

        public int? LowPercent { get; set; }

        public int? MiddlePercent { get; set; }

        public string MiddleMargin { get { return string.Format("{0} a {1}", TopMargin + 1, LowMargin); } }

        #region PercentValues
        public int LogInScore { get; set; }
        
        public int LogOutScore { get; set; }

        public int ClientScore { get; set; }

        public int MovingScore { get; set; }

        public int LostScore { get; set; }

        public int TotalPercent { get { return (LogInScore + LogOutScore + ClientScore + MovingScore + LostScore) / 5; } }
        #endregion
        
    }

    public class MeasuringAgentsMonthlyReportParameters
    {
        public DateTime? StartDate { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        public DateTime? EndDate { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }
    }
}