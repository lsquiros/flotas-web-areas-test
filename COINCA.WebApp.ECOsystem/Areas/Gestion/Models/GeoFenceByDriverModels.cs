﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Models.Account;
using System.ComponentModel.DataAnnotations;

namespace ECO_Management.Models
{
    public class GeoFenceByDriverBase
    {
        public GeoFenceByDriverBase()
        {
            Parameters = new GeoFenceByDriverParameters();
            List = new List<GeoFenceByDriver>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public GeoFenceByDriverParameters Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<GeoFenceByDriver> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    public class GeoFenceByDriver
    {
        public DateTime Date { get; set; }
        /// <summary>
        /// StarDate
        /// </summary>
        public string Driver { get; set; }

        /// <summary>
        /// End Date
        /// </summary>
        public string DriverName
        {
            get
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(Driver);
            }
        }

        /// <summary>
        /// Latitud
        /// </summary>
        public string GeoFenceName { get; set; }

        /// <summary>
        /// Longitud
        /// </summary>
        public string AlarmTypeName { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public string GeoFenceTypeName { get; set; }
    }

    public class GeoFenceByDriverParameters 
    {
        public int DriverId { get; set; }

        public DateTime? StartDate { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        public DateTime? EndDate { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }
    }
}