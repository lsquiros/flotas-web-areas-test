﻿using ECO_Management.Models;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Management.Models
{
    public class ProgramProtocolBase
    { 
        public ProgramProtocolBase()
        {
            Data = new ProgramProtocol();
            List = new List<ProgramProtocol>();
            Menus = new List<AccountMenus>();    
        }

        public ProgramProtocol Data { get; set; }
     
        public IEnumerable<AccountMenus> Menus { get; set; }

        public IEnumerable<ProgramProtocol> List { get; set; }
    }

    public class ProgramProtocol
    {
        public int Id { get; set; }

        public int ProtocolId { get; set; }
                
        public float Clasification { get; set; }

        public int Relation { get; set; }

        public int Order { get; set; }

        public int TypeId { get; set; }

        public int QuestionId { get; set; }

        public int AnswerId { get; set; }
    }
}