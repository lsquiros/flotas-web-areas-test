﻿using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECO_Management.Models
{
    public class SurveyReportModels
    {
        public int UserEventId { get; set; }

        public int CommerceId { get; set; }

        public int SurveyId { get; set; }

        public string UserName { get; set; }

        public String UserNameDecrypted { get { return TripleDesEncryption.Decrypt(UserName); } }

        public string Survey { get; set; }

        public int QuestionId { get; set; }

        public string Question { get; set; }

        public string Option { get; set; }

        public string CommerceName { get; set; }

        public int QuestionTypeId { get; set; }

        public string QuestionType { get; set; }

        public Double Latitude { get; set; }

        public Double Longitude { get; set; }

        public string Data { get; set; }

        public int UserId { get; set; }

        public string Map { get { return "http://staticmap.openstreetmap.de/staticmap.php?center=" + Latitude.ToString().Replace(",", ".") + "," + Longitude.ToString().Replace(",", ".") + "&zoom=17&size=350x200&markers=" + Latitude.ToString().Replace(",", ".") + "," + Longitude.ToString().Replace(",", ".") + ",ol-marker"; } }

        public string CustomerName { get; set; }

        public DateTime EventDate { get; set; }

        public string Address { get; set; }

        public string EventName { get; set; }

        public int Id { get; set; }
    }

    public class ManagementListCombo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}