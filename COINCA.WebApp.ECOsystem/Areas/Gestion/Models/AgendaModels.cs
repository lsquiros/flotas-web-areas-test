﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using System.Collections.Generic;
using ECOsystem.Models.Account;
using GridMvc.DataAnnotations;
using System;
using Newtonsoft.Json;
using ECOsystem.Management.Models;
using ECOsystem.Miscellaneous;

namespace ECO_Management.Models
{   
    public class AgendaBase
    {
        public AgendaBase()
        {
            Data = new Agenda();
            List = new List<Agenda>();
            Menus = new List<AccountMenus>();
            Columns = new DynamicColumnsBase();
        }
        public Agenda Data { get; set; }
        public IEnumerable<AccountMenus> Menus { get; set; }
        public IEnumerable<Agenda> List { get; set; }
        public DynamicColumnsBase Columns { get; set; }
    }

    //public class Agenda : ModelAncestor
    //{
    //    public int? Order { get; set; }

    //    public int? IdAgenda { get; set; } 

    //    public int? CommerceId { get; set; }

    //    public int? UserId { get; set; }

    //    public DateTime? Date { get; set; }

    //    public int? Status { get; set; }

    //    public string CommerceName { get; set; }

    //    public string Code { get; set; }

    //    public string Address { get; set; }

    //    public int? LoggedUserId { get; set; }

    //    public string UserName { get; set; }

    //    public string DecryptedName
    //    {            
    //        get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(UserName); } 
    //    }

    //    public string XMLDynamicColumns { get; set; }

    //    public List<AditionalColumns> DynamicColumns { get { return XMLDynamicColumns != null && XMLDynamicColumns != "" ? ECOsystem.Utilities.Miscellaneous.GetXMLDeserialize<List<AditionalColumns>>(XMLDynamicColumns, "AditionalColumns") : new List<AditionalColumns>(); } }
    //}

    public class AgendaImport
    {
        public DateTime? Date { get; set; }

        public string Code { get; set; }

        public string CommerceName { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string UserEmail { get; set; }

        public string ColumnsXml { get; set; }

        public string ErrorMessage { get; set; }
    }

    //public class AditionalColumns
    //{
    //    public int Id { get; set; }
    //    public string Title { get; set; }
    //    public string Value { get; set; }
    //    public int AgendaId { get; set; }
    //}
}
