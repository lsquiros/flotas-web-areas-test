﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Management.Models
{
    public class QuestionsDetail
    {
        public int? QuestionId { get; set; }

        public string Question { get; set; }

        public int Order { get; set; }

        public int ProgramProtocolId { get; set; }
    }

    public class QuestionsByProtocol
    {
        public int? Id { get; set; }
                
        public string Text { get; set; }

        public int CustomerId { get; set; }

        public int Order { get; set; }
    }

    public class QuestionsDetailShow
    {
        public int ProgramProtocolId { get; set; }

        public int ProtocolId { get; set; }

        public string ProtocolName { get; set; }

        public int? QuestionId { get; set; }

        public string Question { get; set; }

        public int? AnswerId { get; set; }

        public string Answer { get; set; }

        public int? Clasification { get; set; }

        public int? Relation { get; set; }

        public int Order { get; set; }

        public string Code { get; set; }

        public int? NextQuestion { get; set; }

        public bool? IsRelation { get; set; }
    }

}