﻿using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Management.Models
{
    public class ManagementMapsBase
    {
        public ManagementMapsBase()
        {
            Data = new ManagementMaps();
        }
        public ManagementMaps Data { get; set; }
    }

    public class ManagementMaps
    {
        public IEnumerable<Commerces> Commerces { get; set; }

        public IEnumerable<Users> Agents { get; set; }
    }

    public class Commerces
    {
        public int? Id { get; set; }
                
        public string Name { get; set; }
                
        public string Description { get; set; }
                
        public double? Latitude { get; set; }
                
        public double? Longitude { get; set; }
                
        public string Address { get; set; }

        public int? Type { get; set; }

        public string Code { get; set; }
    }

    public class Agents
    {
        public int? UserId { get; set; }

        public string EncryptName { get; set; }

        public string DecryptName { get { return TripleDesEncryption.Decrypt(EncryptName); } }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string Address { get; set; }

        public string CommerceName { get; set; }

        public string EventName { get; set; }

        public string Lapse { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public decimal Distance { get; set; }

        public int? ReconstructionId { get; set; }

        public int battery_percentage { get; set; }

        public int Speed { get; set; }

        public string Usage { get; set; }

        public string TotalLapse { get; set; }

        public int? IsBegin { get; set; }

        public int? IsFinish { get; set; }

        public int? ManagementTypeId { get; set; }
    }

    public class AgentReconstructing
    {
        public int? UserId { get; set; }

        public string EncryptName { get; set; }

        public string DecryptName { get { return TripleDesEncryption.Decrypt(EncryptName); } }

        public IEnumerable<Agents> FragmentList { get; set; }
    }
}