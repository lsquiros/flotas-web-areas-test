﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ECO_Management.Models
{
    public enum CatalogDetailEnum
    {
        BeginJourneyEvent = 1, 
        EventTypes = 2,        
        GenericEvent = 3,      
        Icons = 4,             
        LocationEvent = 5,     
        QUESTIONS_TYPE_CHECK = 6,
        QUESTIONS_TYPE_DROPDOWN = 7,
        QUESTIONS_TYPE_RADIO = 8,   
        QuestionTypes = 9
    }

    public class CatalogDetail
    {
        public static int GetItemId(CatalogDetailEnum CatalogDetail)
        {
            switch (CatalogDetail)
            {
                case CatalogDetailEnum.BeginJourneyEvent:
                    return Convert.ToInt32(ConfigurationManager.AppSettings["BeginJourneyEvent"]);
                case CatalogDetailEnum.EventTypes:
                    return Convert.ToInt32(ConfigurationManager.AppSettings["EventTypes"]);
                case CatalogDetailEnum.GenericEvent:
                    return Convert.ToInt32(ConfigurationManager.AppSettings["GenericEvent"]);
                case CatalogDetailEnum.Icons:
                    return Convert.ToInt32(ConfigurationManager.AppSettings["Icons"]);
                case CatalogDetailEnum.LocationEvent:
                    return Convert.ToInt32(ConfigurationManager.AppSettings["LocationEvent"]);
                case CatalogDetailEnum.QUESTIONS_TYPE_CHECK:
                    return Convert.ToInt32(ConfigurationManager.AppSettings["QUESTIONS_TYPE_CHECK"]);
                case CatalogDetailEnum.QUESTIONS_TYPE_DROPDOWN:
                    return Convert.ToInt32(ConfigurationManager.AppSettings["QUESTIONS_TYPE_DROPDOWN"]);
                case CatalogDetailEnum.QUESTIONS_TYPE_RADIO:
                    return Convert.ToInt32(ConfigurationManager.AppSettings["QUESTIONS_TYPE_RADIO"]);
                case CatalogDetailEnum.QuestionTypes:
                    return Convert.ToInt32(ConfigurationManager.AppSettings["QuestionTypes"]);               
                default:
                    return 0;
            }

        }
    }
}