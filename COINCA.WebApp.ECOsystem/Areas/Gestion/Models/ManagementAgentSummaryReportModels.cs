﻿using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECO_Management.Models
{
    public class ManagementAgentSummaryReportModels
    {
        public string AgentName { get; set; }

        public String AgentNameDecrypted
        {
            get
            {
                return TripleDesEncryption.Decrypt(AgentName);
            }
        }

        public string Date { get; set; }

        public int UserId { get; set; }

        public string AVGBeginJourney { get; set; }

        public string AVGFinishJourney { get; set; }

        public string WorkingTime { get; set; }

        public string RealLunchTime { get; set; }

        public string ProductiveTime { get; set; }

        public string PorcProductiveTime { get; set; }

        public decimal Kilometers { get; set; }

        public string MovementTime { get; set; }

        public string StopTime { get; set; }

        public string RawDate { get; set; }

        public DateTime? EventDate { get; set; }
    }

    public class ManagementAgentSummaryParameters
    {
        public int? UserId { get; set; }

        public string UserName { get; set; }      

        public DateTime? StartDate { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        public DateTime? EndDate { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat((EndDate != null) ? Convert.ToDateTime(EndDate).AddHours(23).AddMinutes(59).AddSeconds(59) : EndDate); }
            set { EndDate = (value != null) ? Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)).AddHours(23).AddMinutes(59).AddSeconds(59) : ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }
    }
}