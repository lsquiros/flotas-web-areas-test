﻿using ECO_Management.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ECO_Management.Business
{
    public class MeasuringAgentsMonthlyReportBusiness : IDisposable
    {
        public DataTable GetReportDataTable(MeasuringAgentsMonthlyReportParameters parameters)
        {
            var customer = ECOsystem.Utilities.Session.GetCustomerInfo();
            var model = GetData(parameters.StartDate, parameters.EndDate, customer.CustomerId);

            using (var dt = new DataTable())
            {
                var dates = string.Format("{0} - {1}", Convert.ToDateTime(parameters.StartDate).ToString("dd/MM/yyyy hh:dd:mm"), Convert.ToDateTime(parameters.EndDate).ToString("dd/MM/yyyy hh:dd:mm"));                

                dt.Columns.Add("StartTime");
                dt.Columns.Add("EndTime");
                dt.Columns.Add("AgentStartTime");
                dt.Columns.Add("AgentEndTime");
                dt.Columns.Add("ClientTime");
                dt.Columns.Add("AgentClientTime");                
                dt.Columns.Add("LostTime");
                dt.Columns.Add("AgentLostTime");
                dt.Columns.Add("MovingTime");
                dt.Columns.Add("AgentMovingTime");
                dt.Columns.Add("ClientDifference");
                dt.Columns.Add("MovingDifference");
                dt.Columns.Add("LostDifference");
                dt.Columns.Add("LogInDifference");
                dt.Columns.Add("LogOutDifference");
                dt.Columns.Add("AgentName");
                dt.Columns.Add("Dates");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("TopMargin");
                dt.Columns.Add("LowMargin");
                dt.Columns.Add("TopPercent");
                dt.Columns.Add("LowPercent");
                dt.Columns.Add("MiddlePercent");
                dt.Columns.Add("MiddleMargin");
                dt.Columns.Add("LogInPercent");
                dt.Columns.Add("LogOutPercent");
                dt.Columns.Add("ClientPercent");
                dt.Columns.Add("LostPercent");
                dt.Columns.Add("MovingPercent");
                dt.Columns.Add("TotalPercent");

                foreach (var item in model)
                {
                    var dr = dt.NewRow();

                    dr["StartTime"] = item.StartTimeClient;
                    dr["EndTime"] = item.EndTimeClient;
                    dr["AgentStartTime"] = item.AgentLogIn.ToString("hh:mm tt");
                    dr["AgentEndTime"] = item.AgentLogOut.ToString("hh:mm tt");
                    dr["ClientTime"] = item.ClientTime;
                    dr["AgentClientTime"] = item.AgentClientTime;
                    dr["LostTime"] = item.LostTime;
                    dr["AgentLostTime"] = item.AgentLostTime;
                    dr["MovingTime"] = item.MovingTime;
                    dr["AgentMovingTime"] = item.AgentMovingTime;
                    dr["ClientDifference"] = item.ClientDifference;
                    dr["MovingDifference"] = item.MovingDifference;
                    dr["LostDifference"] = item.LostDifference;
                    dr["LogInDifference"] = item.LogInDifference;
                    dr["LogOutDifference"] = item.LogOutDifference;
                    dr["AgentName"] = item.DecryptName;
                    dr["Dates"] = dates;
                    dr["CustomerName"] = customer.DecryptedName;
                    dr["TopMargin"] = item.TopMargin;
                    dr["LowMargin"] = item.LowMargin;
                    dr["TopPercent"] = item.TopPercent;
                    dr["LowPercent"] = item.LowPercent;
                    dr["MiddlePercent"]  = item.MiddlePercent;
                    dr["MiddleMargin"] = item.MiddleMargin;
                    dr["LogInPercent"] = item.LogInScore;
                    dr["LogOutPercent"] = item.LogOutScore;
                    dr["ClientPercent"] = item.ClientScore;
                    dr["LostPercent"] = item.LostScore;
                    dr["MovingPercent"] = item.MovingScore;
                    dr["TotalPercent"] = item.TotalPercent;

                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }

        public IEnumerable<MeasuringAgentsMonthlyReport> GetData(DateTime? StartDate, DateTime? EndDate, int? CustomerId)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<MeasuringAgentsMonthlyReport>("[Management].[Sp_Retrieve_MeasuringAgentsMonthlyReport]", new
                {
                    CustomerId,
                    StartDate,
                    EndDate
                });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}