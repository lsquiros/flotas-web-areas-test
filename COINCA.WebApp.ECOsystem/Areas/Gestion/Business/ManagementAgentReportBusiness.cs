﻿using ECO_Management.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ECO_Management.Business
{
    public class ManagementAgentReportBusiness : IDisposable
    {
        public DataSet GetReportDataSet(int? UserId, string StartDate, string EndDate)
        {
            var customer = ECOsystem.Utilities.Session.GetCustomerInfo();

            //Get Agents
            ManagementMapBusiness bus = new ManagementMapBusiness();
            var ModelAgents = bus.AgentsRetrieve(customer.CustomerId, UserId);
            var ModelDetails = GetData(UserId, StartDate, EndDate, customer.CustomerId);

            DataSet ds = new DataSet();
            var dates = string.Format("{0} - {1}", StartDate, EndDate);

            ds.Tables.Add(ECOsystem.Utilities.DataTableUtilities.ClassToDataTable(ModelAgents.Where(x => ModelDetails.Select(m => m.UserId).ToList().Exists(m => m == x.UserId))
                                                                                             .Select(x => new
                                                                                             {
                                                                                                 x.UserId,
                                                                                                 UserName = x.DecryptName,
                                                                                                 CustomerName = customer.DecryptedName,
                                                                                                 Dates = dates
                                                                                             }).ToList(), "ManagementAgentReport"));

            ModelDetails.ToList().ForEach(x => {x.Date = x.Date.Contains("Promedio") ? x.Date : x.Date; x.RawDate = x.Date.Contains("Promedio") ? x.Date : Convert.ToDateTime(x.EventDate).ToString("yyyyMMdd"); });

            ds.Tables.Add(ECOsystem.Utilities.DataTableUtilities.ClassToDataTable(ModelDetails.ToList(), "ManagementAgentDetail"));

            return ds;
        }

        public IEnumerable<ManagementAgentSummaryReportModels> GetData(int? UserId, string StartDate, string EndDate, int? CustomerId)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<ManagementAgentSummaryReportModels>("[Management].[Sp_Retrieve_ManagementAgentsReport]", new         
                {
                    UserId,
                    CustomerId,
                    StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(StartDate),
                    EndDate  = Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(EndDate)).AddHours(23).AddMinutes(59).AddSeconds(59)
                });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}