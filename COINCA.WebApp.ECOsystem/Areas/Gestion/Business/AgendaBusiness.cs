﻿using ECO_Management.Models;
using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using ECOsystem.Miscellaneous;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;




namespace ECO_Management.Business
{
    public class AgendaBusiness : IDisposable
    {
        public List<AgendaImport> ImportData(List<AgendaImport> list, bool CreateCommerce)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<AgendaImport>("[Management].[Sp_AgendaImport_Add]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        XmlData = ECOsystem.Utilities.Miscellaneous.GetXML(list),
                        CreateCommerce,
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    }).ToList();
            }
        }

        public List<Agenda> AddOrEditAgenda(Agenda agenda)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Agenda>("[Management].[Sp_Agenda_AddOrEdit]",
                    new
                    {
                        CommerceID = agenda.CommerceId,
                        UserId = agenda.UserId,
                        DateOfAgenda = agenda.DateOfAgenda,
                        LoggedUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    }).ToList();
            }
        }

        public List<Agenda> RetrieveAgenda(int userId, DateTime dateOfAgenda, DateTime? endDateOfAgenda = null)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<Agenda>("[Management].[Sp_Agenda_Retrieve]",
                    new
                    {
                        UserId = userId,
                        DateOfAgenda = dateOfAgenda,
                        EndDate = endDateOfAgenda,
                        ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId
                    }).ToList();
            }
        }
        
        public List<Agenda> RetrieveAgendaConsolidated(int userId, DateTime dateOfAgenda, DateTime? endDateOfAgenda = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Agenda>("[Management].[Sp_AgendaConsolidated_Retrieve]",
                    new
                    {
                        UserId = userId,
                        DateOfAgenda = dateOfAgenda,
                        EndDate = endDateOfAgenda,
                        ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId
                    }).ToList();
            }
        }

        public List<Agenda> DeleteAgenda(int IdAgenda)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Agenda>("[Management].[Sp_Agenda_Delete]",
                    new
                    {
                        AgendaId = IdAgenda
                       ,ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId
                    }).ToList();
            }
        }

        public DataTable GetReportDataTable(int userId, DateTime dateOfAgenda)
        {
            var model = RetrieveAgenda(userId, dateOfAgenda);

            using (var dt = new DataTable())
            {

                dt.Columns.Add("Order");
                dt.Columns.Add("Name");
                dt.Columns.Add("Address");
                dt.Columns.Add("Date");
                dt.Columns.Add("Customer");
                dt.Columns.Add("Agent");
                dt.Columns.Add("Code");

                foreach (var item in model)
                {
                    var dr = dt.NewRow();

                    dr["Order"] = item.Order;
                    dr["Name"] = item.CommerceName;
                    dr["Address"] = item.Address;
                    dr["Date"] = Convert.ToDateTime(item.DateOfAgenda).ToShortDateString();
                    dr["Customer"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    dr["Agent"] = item.DecryptedName;
                    dr["Code"] = item.Code;
                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }

        #region DynamicColumns
        public DynamicColumnsBase RetrieveColumns(int? CustomerId)
        {
            DynamicColumnsBase columns = new DynamicColumnsBase();
            using (var dba = new DataBaseAccess())
            {
                var _AddionalColums = dba.ExecuteReader<DynamicColumnsBase>("[Management].[Sp_AgendaColumn_Retrieve]", new
                {
                    CustomerId = CustomerId.ToString()
                }).ToList().FirstOrDefault();

                columns.MaxId = _AddionalColums == null || _AddionalColums.DynamicColumns == String.Empty ? 1 : _AddionalColums.MaxId;
                columns.ListColums = _AddionalColums == null || _AddionalColums.DynamicColumns == String.Empty ? new List<AdittionalAgendaColumn>() : Miscellaneous.GetXMLDeserialize<List<AdittionalAgendaColumn>>(_AddionalColums.DynamicColumns, string.Empty);
            }
            return columns;
        }

        public void ManagementParametersAddorEditColumns(DynamicColumnsBase columns, int? CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Management].[Sp_ManagementParametersColums_AddOrEdit]", new
                {
                    CustomerId,
                    List = columns.ListColums != null && columns.ListColums.Count > 0 ? Miscellaneous.GetXML(columns.ListColums) : string.Empty,
                    MaxId = columns.ListColums != null && columns.ListColums.Count > 0 ? columns.ListColums.Max(x => x.Id) : (int?)null,
                    UserId
                });
            }
        }

        public void DynamicColumnsValuesAddOrEdit(List<AditionalColumns> list)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Management].[Sp_DynamicColumnValues_AddOrEdit]", new
                {
                    Id = list.FirstOrDefault().AgendaId,
                    Xml = ECOsystem.Utilities.Miscellaneous.GetXML(list)
                });
            }
        }
        #endregion

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}