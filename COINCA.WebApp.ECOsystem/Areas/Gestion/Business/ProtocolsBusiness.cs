﻿using System;
using System.Linq;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECO_Management.Models;

namespace ECO_Management.Business
{
    public class ProtocolsBusiness : IDisposable
    {
        public void AddOrEditProtocols(Protocols model) 
        {
            using (var dba = new DataBaseAccess()) 
            {
                dba.ExecuteNonQuery("[Management].[Sp_AddOrEdit_Protocols]",
                    new 
                    {
                        Id = model.Id,
                        CustomerId = model.CustomerId,
                        Name = model.Name,
                        UserId = model.UserId
                    });
            }
        }

        public int Delete_Protocols(int id)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteNonQuery("[Management].[Sp_Delete_Protocols]", new { Id = id });
            }
        }

        public IEnumerable<Protocols> RetrieveProtocols(int? CustomerId = null, int? Id = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Protocols>("[Management].[Sp_Retrieve_Protocols]",
                    new
                    {
                        CustomerId,
                        Id
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
	}
}