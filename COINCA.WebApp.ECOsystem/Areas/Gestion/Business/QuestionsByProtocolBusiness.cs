﻿using System;
using System.Linq;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECO_Management.Models;
using ECOsystem.Utilities;

namespace ECO_Management.Business
{
    public class QuestionsByProtocolBusiness : IDisposable
    {
        public IEnumerable<QuestionsDetailShow> RetrieveQuestionsAndAnswers(int ProtocolId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<QuestionsDetailShow>("[Management].[Sp_Retrieve_QuestionsAndAnswersByProtocol]",
                    new 
                    { 
                        ProtocolId,
                        Session.GetCustomerInfo().CustomerId
                    });
            }
        }

        public QuestionsByProtocol AddOrEditQuestion(int? Id, string Text, int protocolId)
        {
            using (var dba = new DataBaseAccess()) 
            {
                return dba.ExecuteReader<QuestionsByProtocol>("[Management].[Sp_AddOrEdit_Questions]",
                    new 
                    {
                        Id,
                        Text,
                        CustomerId = Session.GetCustomerId(),                        
                        Session.GetUserInfo().UserId,
                        protocolId
                    }).FirstOrDefault();
            }
        }

        public IEnumerable<QuestionsByProtocol> RetrieveQuestion(int CustomerId, int? Id = null, int? ProtocolId = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<QuestionsByProtocol>("[Management].[Sp_Retrieve_Questions]",
                    new
                    {
                        CustomerId,
                        Id,
                        ProtocolId
                    });
            }
        }

        public int AddOrEditAnswer(int? Id, string Text)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Management].[Sp_AddOrEdit_Answers]",
                    new
                    {
                        Id,
                        CustomerId = Session.GetCustomerId(),
                        Text,
                        Session.GetUserInfo().UserId
                    });
            }
        }

        public IEnumerable<AnswersByProtocol> RetrieveAnswer(int? CustomerId, int? Id = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<AnswersByProtocol>("[Management].[Sp_Retrieve_Answers]",
                    new
                    {
                        CustomerId,
                        Id
                    });
            }
        }

        public IEnumerable<ManagementTypes> RetrieveQuestionsTypes()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ManagementTypes>("[Management].[Sp_Retrieve_QuestionsTypes]", new { });
            }
        }

        public int DeleteAnswer(int AnswerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Management].[SP_DeleteAnswer]",
                    new
                    {
                        AnswerId
                    });
            }
        }

        public int DeleteQuestion(int QuestionId, int ProtocolId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Management].[SP_DeleteQuestion]",
                    new
                    {
                        QuestionId,
                        ProtocolId
                    });
            }

        }

        public int AddOrEditProgramProtocol(int? Id = null, int? ProtocolId = null, int? QuestionId = null, int? AnswerId = null, double? Clasification = null, int? Relation = null, int? Order = null, string TypeCode = null,int? NextQuestion = null) 
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Management].[SP_AddOrEditProgramProtocols]",
                    new
                    {
                        Id,
                        ProtocolId,
                        QuestionId,
                        AnswerId,
                        Clasification,
                        Relation,
                        Order,
                        TypeCode,
                        NextQuestion,
                        Session.GetUserInfo().UserId
                    });
            }
        }

        public IEnumerable<ProgramProtocol> RetrieveProgramProtocol(int ProtocolId) {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ProgramProtocol>("[Management].[SP_Retrieve_ProgramProtocol]",
                    new
                    {
                        ProtocolId
                    });
            }
        }
          
        public int GetAnswerCountRetrive(int ProgramProtocolId, int OrderId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Management].[Sp_Retrieve_AnswersCountByQuestions]",
                    new
                    {
                        ProgramProtocolId,
                        OrderId
                    });
            }
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
	}
}