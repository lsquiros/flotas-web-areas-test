﻿using ECO_Management.Models;
using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ECO_Management.Business
{
    public class ManagementAgentSummaryReportBusiness : IDisposable
    {

        public DataTable GetReportDataTable(ManagementAgentSummaryParameters parameters)
        {
            var customer = ECOsystem.Utilities.Session.GetCustomerInfo();
            var modelAgentReconstructing = AgentReconstructingRetrieve(Convert.ToInt32(parameters.UserId), parameters.StartDate, parameters.EndDate);
            var model = modelAgentReconstructing.FragmentList;
            var geoNames = ECOsystem.Utilities.Session.GetGeopoliticalNames();            

            model.ToList().ForEach(x => { x.AgentName = modelAgentReconstructing.DecryptName;
                x.CustomerName = customer.DecryptedName;
                x.Date = x.StartDate.ToString();
                x.StartDate = (DateTime)parameters.StartDate;
                x.EndDate = (DateTime)parameters.EndDate;                  
                x.Latitude = x.Latitude == 0 ? null : x.Latitude;
                x.Longitude = x.Longitude == 0 ? null : x.Longitude;
                x.GeoNames = geoNames;
            });

            return ECOsystem.Utilities.DataTableUtilities.ClassToDataTable(model.ToList(), "ManagementAgentSummaryReport");
        }

        public AgentReconstructing AgentReconstructingRetrieve(int Id, DateTime? StartDate, DateTime? EndDate = null)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var result = new AgentReconstructing();
                //Get the information about the user, if there's no info, return null
                var model = AgentsDetailRetrieve(Id.ToString()).FirstOrDefault();
                if (model != null)
                {
                    result.UserId = Id;
                    result.EncryptName = model.EncryptName;
                    result.FragmentList = dba.ExecuteReader<Agent>("[Management].[Sp_Retrieve_AgentReconstructing_Report]", new
                    {
                        UserId = Id,
                        StartDate,
                        EndDate
                    });
                }

                return result;
            }
        }

        public IEnumerable<Agent> AgentsDetailRetrieve(string Ids)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Agent>("[Management].[Sp_Retrieve_AgentDetailInformation]", new
                {
                    Ids
                });
            }
        }           

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}