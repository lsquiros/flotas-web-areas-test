﻿using ECO_Management.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO_Management.Utilities
{
    public class GeneralCollections
    {        
        public static SelectList GetProtocolsByCostumers
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var customerid = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId;
            
                using (var bus = new ProtocolsBusiness())
                {
                    var result = bus.RetrieveProtocols(customerid);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.Id, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetAnswerTypes
        {
            get
            {
                var dictionary = new Dictionary<string, string>(); 

                using (var bus = new QuestionsByProtocolBusiness())
                {
                    var result = bus.RetrieveQuestionsTypes();
                    foreach (var r in result)
                    {
                        dictionary.Add(r.Code, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetQuestionsByCostumers (int? ProtocolId = null)
        {
    
            {
                var dictionary = new Dictionary<int?, string>();
                var customerid = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId;

                using (var bus = new QuestionsByProtocolBusiness())
                {
                    var result = bus.RetrieveQuestion((int)customerid, null, ProtocolId);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.Id, r.Text);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
            
        }

        public static SelectList GetAnswersByCostumers
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var customerid = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId;

                using (var bus = new QuestionsByProtocolBusiness())
                {
                    var result = bus.RetrieveAnswer((int)customerid, null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.Id, r.Text);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetAnswersLevels
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(1, "Bueno");
                dictionary.Add(2, "Malo");
                dictionary.Add(3, "Regular");
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetAgents
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var customerid = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId;

                using (var bus = new ManagementMapBusiness())
                {
                    var result = bus.AgentsRetrieve(customerid,null);
                    dictionary.Add(-1, "Todos");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.UserId, r.DecryptName);
					}
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetAgentsSummary
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var customerid = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId;

                using (var bus = new ManagementMapBusiness())
                {
                    var result = bus.AgentsRetrieve(customerid,null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.UserId, r.DecryptName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
		
        public static SelectList GetDriver
        {
            get 
            {
                var dictionary = new Dictionary<int, string>();
                using (var bus = new ManagementMapBusiness())
                {
                    var list = bus.AgentsRetrieve(ECOsystem.Utilities.Session.GetCustomerId(),null);
                    dictionary.Add(-1, "Todos");
                    foreach (var user in list)
                    {
                        dictionary.Add((int)user.UserId, user.DecryptName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetProtocols
        {
            get
            {
                var dictionary = new Dictionary<int, string>();
                using (var bus = new ProtocolsBusiness())
                {
                    var list = bus.RetrieveProtocols(ECOsystem.Utilities.Session.GetCustomerId());
                    foreach (var obj in list)
                    {
                        dictionary.Add((int)obj.Id, obj.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetCommerces
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
              
                using (var bus = new ManagementMapBusiness())
                {
                    var result = bus.CommercesRetrieve();
                    dictionary.Add(-1, "Todos");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.Id, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetCommercesSummary
        {
            get
            {
                var customerid = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId;
                var dictionary = new Dictionary<int?, string>();

                using (var bus = new ManagementMapBusiness())
                {
                    var result = bus.CommercesRetrieve(customerid); 
                    foreach (var r in result)
                    {
                        dictionary.Add(r.Id, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
  
      

    }
}