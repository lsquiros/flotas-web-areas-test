﻿using ECO_Management.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ECO_Management.Business
{
    public class SurveyReportBusiness : IDisposable
    {

        public DataTable GetReportDataTable(int UserId, int CommerceId, DateTime DateOfCheck, int SurveyId, int UserEventId)
        { 
            var model = RetrieveFormsbyCommerceAgent(UserId, CommerceId, DateOfCheck, SurveyId, UserEventId).ToList();
            var Customer = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;

            model.ForEach(x => { x.CustomerName = Customer; x.Data = string.IsNullOrEmpty(x.Data) ? x.Data : x.Data.Replace(@"\", @"/"); });              

            return ECOsystem.Utilities.DataTableUtilities.ClassToDataTable(model, string.Empty);  
        }

        public IEnumerable<SurveyReportModels> RetrieveFormsbyCommerceAgent(int UserId, int CommerceId, DateTime Date, int SurveyId, int UserEventId)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<SurveyReportModels>("[Management].[Sp_Retrieve_ReportForms]", new
                {
                    UserId,
                    CommerceId,
                    Date,
                    SurveyId,
                    UserEventId
                });
            }
        }

        public List<SurveyReportModels> GetDataSurvey(int UserId, DateTime Date, DateTime EndDate)
        {
            var model = RetrieveFormsAnswerbyAgent(UserId, Date, EndDate).ToList();
            var Customer = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;

            model.ForEach(x => { x.CustomerName = Customer; x.Data = string.IsNullOrEmpty(x.Data) ? x.Data : x.Data.Replace(@"\", @"/"); });

            return model;
        }

        public IEnumerable<SurveyReportModels> RetrieveFormsAnswerbyAgent(int UserId, DateTime Date, DateTime EndDate)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<SurveyReportModels>("[Management].[Sp_Retrieve_FormsAnswers]", new
                {
                    UserId,
                    Date,
                    EndDate
                });
            }
        }

        public IEnumerable<SurveyReportModels> RetrieveSurveysToDownload(int UserId, DateTime Date)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<SurveyReportModels>("[Management].[Sp_SurveysToDownload_Retrieve]", new
                {
                    UserId,
                    Date
                });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}