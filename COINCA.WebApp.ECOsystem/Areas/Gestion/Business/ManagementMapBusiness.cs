﻿using ECO_Management.Models;
using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Management.Business
{
    public class ManagementMapBusiness : IDisposable
    {
        public IEnumerable<Commerce> CommercesRetrieve(int? CustomerId=null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Commerce>("[General].[Sp_Commerces_Retrieve]", new
                {
                    CustomerId
                });
            }
        }

        public IEnumerable<Agent> AgentsRetrieve(int? CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Agent>("[Management].[Sp_Agent_Retrieve]", new
                {
                    CustomerId,
                    UserId 
                });
            }
        }

        public IEnumerable<Agent> AgentsDetailRetrieve(string Ids)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Agent>("[Management].[Sp_Retrieve_AgentDetailInformation]", new
                {
                    Ids
                });
            }
        }

        public IEnumerable<ManagementListCombo> CommerceByAgentsAndDate(int UserId, DateTime Date)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ManagementListCombo>("[Management].[Sp_Retrieve_CommerceByAgentsAndDate]", new
                {
                    UserId,
                    Date
                });
            }
        }

        public AgentReconstructing AgentsReconstructingRetrieve(int UserId, DateTime? StartDate, DateTime? EndDate = null)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var result = new AgentReconstructing();
                //Get the information about the user, if there's no info, return null
                var model = AgentsDetailRetrieve(UserId.ToString()).FirstOrDefault();
                if(model != null)
                {
                    result.UserId = UserId;
                    result.EncryptName = model.EncryptName;
                    result.FragmentList = dba.ExecuteReader<Agent>("[Management].[Sp_Retrieve_AgentReconstructing]", new
                    {
                        UserId,
                        StartDate,
                        EndDate
                    });
                }

                return result; 
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}