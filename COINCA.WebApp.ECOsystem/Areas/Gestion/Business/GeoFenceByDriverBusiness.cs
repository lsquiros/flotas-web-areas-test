﻿using ECO_Management.Models;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO_Management.Business
{
    public class GeoFenceByDriverBusiness : IDisposable
    {
        public IEnumerable<GeoFenceByDriver> GetGeoFenceByDriverReport_Retrieve(GeoFenceByDriverParameters model)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<GeoFenceByDriver>("[Efficiency].[SP_GeoFenceByDriverReport_Retrieve]", new
                {
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    StartDate = model.StartDate,
                    EndDate = model.EndDate,
                    DriverId = model.DriverId
                });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}