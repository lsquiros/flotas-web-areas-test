﻿using ECO_Management.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ECO_Management.Business
{
    public class AgentVisitGeographicInformationBusiness : IDisposable
    {
        public DataTable GetReportDataTable(AgentVisitParameters parameters)
        {
            var customer = ECOsystem.Utilities.Session.GetCustomerInfo();
            var model = GetData(parameters.UserId, parameters.StartDate, parameters.EndDate, customer.CustomerId, customer.CountryCode);

            using (var dt = new DataTable())
            {
                var dates = string.Format("{0} - {1}", Convert.ToDateTime(parameters.StartDate).ToString("dd/MM/yyyy hh:mm:ss"), Convert.ToDateTime(parameters.EndDate).ToString("dd/MM/yyyy hh:mm:ss"));       
                var geopoliticalNames = ECOsystem.Utilities.Session.GetGeopoliticalNames();

                dt.Columns.Add("GeopoliticalLevel1");
                dt.Columns.Add("GeopoliticalLevel2");
                dt.Columns.Add("GeopoliticalLevel3");
                dt.Columns.Add("VisitsNumbers");
                dt.Columns.Add("AgentName");
                dt.Columns.Add("GeopoliticalLevel1Customer");
                dt.Columns.Add("GeopoliticalLevel2Customer");
                dt.Columns.Add("GeopoliticalLevel3Customer");
                dt.Columns.Add("Dates");
                dt.Columns.Add("CustomerName");

                foreach (var item in model)
                {
                    var dr = dt.NewRow();

                    dr["GeopoliticalLevel1"] = item.GeopoliticalLevel1;
                    dr["GeopoliticalLevel2"] = item.GeopoliticalLevel2;
                    dr["GeopoliticalLevel3"] = item.GeopoliticalLevel3;
                    dr["VisitsNumbers"] = item.VisitCount;
                    dr["AgentName"] = item.DecryptName;
                    dr["GeopoliticalLevel1Customer"] = geopoliticalNames[0];
                    dr["GeopoliticalLevel2Customer"] = geopoliticalNames[1];
                    dr["GeopoliticalLevel3Customer"] = geopoliticalNames[2];
                    dr["Dates"] = dates;
                    dr["CustomerName"] = customer.DecryptedName;
                    
                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }

        public IEnumerable<AgentVisitGeographicInformation> GetData(int? Id, DateTime? StartDate, DateTime? EndDate, int? CustomerId, string CountryCode)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<AgentVisitGeographicInformation>("[Management].[Sp_Retrieve_AgentVisitGeographicInformation]", new
                        {
                            Id,
		                    CustomerId,
                            StartDate,
	                        EndDate,
	                        CountryCode
                        });
            }
        }              

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}