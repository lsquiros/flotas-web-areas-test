﻿using ECO_Management.Models;
using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ECO_Management.Business
{
    public class ConsolidatedReportBusiness : IDisposable
    {
        public DataSet GetReportDownloadData(ConsolidatedParameters parameters)
        {
            DataSet ds = new DataSet();
            DynamicColumnsBase dynamicCol = new DynamicColumnsBase();
            List<Agenda> listAgenda = new List<Agenda>();
            AgentReconstructing agentRoute = new AgentReconstructing();
            List<SurveyReportModels> listSurvey = new List<SurveyReportModels>();
            List<SurveyReportModels> listSurveyQuestion = new List<SurveyReportModels>();


            //--- Obtener la agenda ---//
            using (var bus = new AgendaBusiness())
            {
                dynamicCol = bus.RetrieveColumns(ECOsystem.Utilities.Session.GetCustomerId());
                listAgenda = bus.RetrieveAgendaConsolidated(parameters.UserId, (DateTime)parameters.StartDate, parameters.EndDate);
            }

            //--- Obtener los eventos ---//
            using (var bus = new ManagementAgentSummaryReportBusiness())
            {
                agentRoute = bus.AgentReconstructingRetrieve(parameters.UserId, parameters.StartDate, parameters.EndDate);
            }

            //--- Obtener los formularios ---//
            using (var bus = new SurveyReportBusiness())
            {
                listSurvey = bus.GetDataSurvey(parameters.UserId, (DateTime)parameters.StartDate, (DateTime)parameters.EndDate);
            }

            if (agentRoute.FragmentList != null && listAgenda != null && listSurvey != null)
            {
                ds.Tables.Add(GetSumaryEventsReport(agentRoute, (DateTime)parameters.StartDate, (DateTime)parameters.EndDate, listAgenda, listSurvey));

                using (var dt = new DataTable("ConsolidatedDetail"))
                {
                    dt.Columns.Add("Id");
                    dt.Columns.Add("Titles");
                    dt.Columns.Add("Type");

                    //--- Agregar los header correspondientes a la agenda ---//
                    AgendaHeaderAddDT(dt, /*listAgenda,*/ dynamicCol.ListColums);

                    //--- Agregar los header correspondientes a los formularios ---//
                    SurveyHeaderAddDT(dt, listSurvey, listSurveyQuestion);

                    ds.Tables.Add(dt);
                }

                using (var dt = new DataTable("ConsolidatedColumnValues"))
                {
                    dt.Columns.Add("Id");
                    dt.Columns.Add("Value");
                    dt.Columns.Add("Type");
                    dt.Columns.Add("UserEventId");
                    GetValueColumnData(dt, listAgenda, listSurvey, listSurveyQuestion, agentRoute.FragmentList);
                    ds.Tables.Add(dt);
                }
            }
            else
            {
                ds.Tables.Add(new DataTable("ConsolidatedSumaryEventsReport"));
                ds.Tables.Add(new DataTable("ConsolidatedDynamicDetail"));
                ds.Tables.Add(new DataTable("ConsolidatedDynamicColumnValues"));
            }

            return ds;
        }

        public DataTable GetSumaryEventsReport(AgentReconstructing agentRoute, DateTime startDate, DateTime endDate, List<Agenda> listAgenda, List<SurveyReportModels> listSurvey)
        {
            var customername = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
            var geoNames = ECOsystem.Utilities.Session.GetGeopoliticalNames();

            agentRoute.FragmentList.Where(s => s.AgentName == null).Select(s => { s.AgentName = agentRoute.DecryptName; return s; }).ToList();
            agentRoute.FragmentList.Where(s => s.GeoNames == null).Select(s => { s.GeoNames = geoNames; return s; }).ToList();
            agentRoute.FragmentList.Where(s => s.Date == null).Select(s => { s.Date = s.StartDate.ToString(); return s; }).ToList();

            DataTable dt = ECOsystem.Utilities.DataTableUtilities.ClassToDataTable(agentRoute.FragmentList.Select(x => new { x.AgentName, x.Date, x.EventName, x.UserEventId, x.CommerceName, x.Lapse, x.Distance, x.Latitude, x.Longitude, x.Address, x.BatteryLevel,
                                                                                                                             x.GeopoliticalInformation, x.State, x.County, x.City, x.GeoCityNames, x.GeoCountyNames, x.GeoStateName, x.GeoNames }).ToList(), "ConsolidatedReport");
            dt.Columns.Add("DateAgenda");
            dt.Columns.Add("CommerceAgenda");
            dt.Columns.Add("SurveyName");
            dt.Columns.Add("CustomerName");
            dt.Columns.Add("InitRangeDate");
            dt.Columns.Add("FiniRangeDate");

            var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
            var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));

            //Get array letters
            char[] letters = startDateStr.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            startDateStr = new string(letters);
            letters = endDateStr.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            endDateStr = new string(letters);
            
            foreach (DataRow row in dt.Rows)
            {
                bool found = false;
                int userEventId = int.Parse(row["UserEventId"].ToString());

                foreach (var visita in listAgenda)
                {
                    if (visita.UserEventId == userEventId)
                    {
                        row["DateAgenda"] = visita.DateOfAgenda.ToString();
                        row["CommerceAgenda"] = visita.CommerceName;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    row["DateAgenda"] = "";
                    row["CommerceAgenda"] = "";
                }
                else
                    found = false;

                foreach (var userEvt in listSurvey.GroupBy(s => s.UserEventId).Select(x => x.First()).ToList())
                {
                    if (userEvt.UserEventId == userEventId)
                    {
                        row["SurveyName"] = userEvt.Survey.ToString();
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    row["SurveyName"] = "";
                }

                row["InitRangeDate"] = startDateStr;
                row["FiniRangeDate"] = endDateStr;
                row["CustomerName"] = customername;
            }

            return dt;
        }

        public DataTable AgendaHeaderAddDT(DataTable dt, List<AdittionalAgendaColumn> columns)
        {
            DataRow dr = dt.NewRow();
            int id = 1;
            if(columns == null || columns.Count == 0)
            {
                dr = dt.NewRow();
                dr["Titles"] = "Columnas dinámicas";
                dr["Id"] = id++;
                dr["Type"] = "CD";
                dt.Rows.Add(dr);

            }
            else
            {
                foreach (var column in columns.OrderBy(x => x.Id).ToList())
                {
                    dr = dt.NewRow();
                    dr["Titles"] = column.Value;
                    dr["Id"] = id++;
                    dr["Type"] = "CD";
                    dt.Rows.Add(dr);
                }
            }

            return dt;
        }

        public DataTable SurveyHeaderAddDT(DataTable dt, List<SurveyReportModels> listSurvey, List<SurveyReportModels> listSurveyQuestion)
        {
            DataRow dr = dt.NewRow();
            int id = dt.Rows.Count;
            if (listSurvey == null || listSurvey.Count == 0)
            {
                dr = dt.NewRow();
                dr["Id"] = ++id;
                dr["Titles"] = "Preguntas formularios";
                dr["Type"] = "CS";
                dt.Rows.Add(dr);
                SurveyReportModels survery = new SurveyReportModels();
                survery.Id = id;
                listSurveyQuestion.Add(survery);
            }
            else
            {
                foreach (var survery in listSurvey.OrderBy(x => x.SurveyId).ToList().OrderBy(s => s.QuestionId).ToList())
                {
                    var found = false;
                    foreach (var form in listSurveyQuestion)
                    {
                        if (survery.QuestionId == form.QuestionId)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found || listSurveyQuestion.Count == 0)
                    {
                        dr = dt.NewRow();
                        dr["Id"] = ++id;
                        dr["Titles"] = survery.Question;
                        dr["Type"] = "CS";
                        dt.Rows.Add(dr);
                        survery.Id = id;
                        listSurveyQuestion.Add(survery);
                    }
                }
            }

            return dt;
        }

        public DataTable GetValueColumnData(DataTable dt, List<Agenda> listAgenda, List<SurveyReportModels> listSurvey, List<SurveyReportModels> listSurveyQuestion, IEnumerable<Agent> list)
        {
            DataRow dr = dt.NewRow();
            int id = 0;
            int fin = 0;

            //--- Agregar los valores correspondientes a la agenda ---//
            if(listAgenda.Count > 0)
            {
                foreach (var visita in listAgenda)
                {
                    if (visita.UserEventId != null)
                    {
                        id = 1;
                        foreach (var column in visita.DynamicColumns)
                        {
                            dr = dt.NewRow();
                            dr["Id"] = id++;
                            dr["Value"] = column.Value;
                            dr["Type"] = "CD";
                            dr["UserEventId"] = visita.UserEventId;
                            dt.Rows.Add(dr);
                        }
                        fin = visita.DynamicColumns.Count;
                    }
                }

                foreach (var evt in list.Where(p => !listAgenda.Any(p2 => p2.UserEventId == p.UserEventId)).ToList())
                {
                    id = 1;
                    for (int i = id; i <= fin; i++)
                    {
                        dr = dt.NewRow();
                        dr["Id"] = i;
                        dr["Value"] = "";
                        dr["Type"] = "CD";
                        dr["UserEventId"] = evt.UserEventId;
                        dt.Rows.Add(dr);
                    }
                }
            }

            //--- Agregar los valores correspondientes a los formularios ---//
            if (listSurvey.Count > 0)
            {
                //List los diferentes identificadores de evento
                foreach (var userEventId in listSurvey.Select(s => s.UserEventId).Distinct().ToList())
                {
                    int idSurvey = listSurvey.Where(s => s.UserEventId == userEventId).ToList().Select(s => s.SurveyId).First();
                    //Lista la preguntas de un evento
                    foreach (var surveryQuestion in listSurveyQuestion.Where(s => s.SurveyId == idSurvey).ToList())
                    {
                        bool foundQuestion = false;
                        bool hasQuestionCheck = false;
                        foreach (var form in listSurvey.Where(s => s.UserEventId == userEventId).ToList())
                        {
                            if (surveryQuestion.QuestionId == form.QuestionId)
                            {
                                if (form.QuestionTypeId == CatalogDetail.GetItemId(CatalogDetailEnum.QUESTIONS_TYPE_CHECK))
                                {
                                    if (!hasQuestionCheck)
                                    {
                                        string options = "";
                                        foreach (var surveryQuestionCheckList in listSurvey.Where(s => s.UserEventId == userEventId).ToList().Where(t => t.QuestionTypeId == CatalogDetail.GetItemId(CatalogDetailEnum.QUESTIONS_TYPE_CHECK) && t.QuestionId == form.QuestionId).ToList())
                                        {
                                            if (options == "")
                                                options = surveryQuestionCheckList.Option;
                                            else
                                                options = options + " - " + surveryQuestionCheckList.Option;
                                        }
                                        dr = dt.NewRow();
                                        dr["Id"] = surveryQuestion.Id;
                                        dr["Value"] = options;
                                        dr["Type"] = "CS";
                                        dr["UserEventId"] = form.UserEventId;
                                        dt.Rows.Add(dr);
                                        hasQuestionCheck = true;
                                        foundQuestion = true;
                                    }
                                }
                                else
                                {
                                    dr = dt.NewRow();
                                    dr["Id"] = surveryQuestion.Id;
                                    dr["Value"] = form.Option == null ? form.Data : form.Option;
                                    dr["Type"] = "CS";
                                    dr["UserEventId"] = form.UserEventId;
                                    dt.Rows.Add(dr);
                                    foundQuestion = true;
                                }
                                break;
                            }
                        }
                        if (!foundQuestion)
                        {//Si la pregunta no posee respuesta
                            dr = dt.NewRow();
                            dr["Id"] = surveryQuestion.Id;
                            dr["Value"] = "";
                            dr["Type"] = "CS";
                            dr["UserEventId"] = userEventId;
                            dt.Rows.Add(dr);
                        }
                    }

                    foreach (var surveryQuestion in listSurveyQuestion.Where(s => s.SurveyId != idSurvey).ToList())
                    {
                        dr = dt.NewRow();
                        dr["Id"] = surveryQuestion.Id;
                        dr["Value"] = "";
                        dr["Type"] = "CS";
                        dr["UserEventId"] = userEventId;
                        dt.Rows.Add(dr);
                    }
                }

                foreach (var evt in (from eee in list
                                     where !(from ppp in listSurvey select ppp.UserEventId).ToList().Contains((int)eee.UserEventId)
                                     select eee).ToList())
                {
                    foreach (var surveryQuestion in listSurveyQuestion)
                    {
                        dr = dt.NewRow();
                        dr["Id"] = surveryQuestion.Id;
                        dr["Value"] = "";
                        dr["Type"] = "CS";
                        dr["UserEventId"] = evt.UserEventId;
                        dt.Rows.Add(dr);
                    }
                }
            }

            return dt;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}