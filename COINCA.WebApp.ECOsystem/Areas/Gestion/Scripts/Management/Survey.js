﻿var ECO_Management = ECO_Management  || {};

ECO_Management.Survey = (function () {
    var options = {};

    var init = function (opts) {
        $.extend(options, opts);

        $('#gridContainer').off('click.edit', 'a[edit_row_survey]').on('click.edit', 'a[edit_row_survey]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();
        });

        $('#btnCloseMSurvey').off('click.btnCloseMSurvey').on('click.btnCloseMSurvey', function (e) {
            showLoader();
            window.location.reload();
            $('#detailContainer').hide('slow');
        });

        $('#btnAdd').off('click.btnAddNew').on('click.btnAddNew', function (e) {
            $('#loadForm').find('#id').val(null);
            $('#loadForm').submit();
        });

        $('#btnAddNewQuestion').off('click.btnAddNewQuestion').on('click.btnAddNewQuestion', function (e) {
            $('#loadQuestionForm').find('#questionId').val(null);
            $('#loadQuestionForm').submit();
            $('#btnAddNewQuestion').attr('disabled', true);
            $('#btnSaveSurvey').attr('disabled', true); 
        });

        $('.editQuestion').off('click').on('click', function (e) {
            removeShadow();
            $('#loadQuestionForm').find('#questionId').val($(this).attr('questionId'));
            $('#loadQuestionForm').find('#questionType').val($(this).attr('questionType'));
            $(this).closest('.sortQuestion').addClass('boxShadowSelect');
            $('#btnAddNewQuestion').attr('disabled', true);
            $('#btnSaveSurvey').attr('disabled', true); 
            $('#loadQuestionForm').submit();
        });

        $('.removeQuestion').off('click').on('click', function (e) {
            $('#DeleteQuestionForm').find('#questionId').val($(this).attr('questionId'));
            $('#DeleteQuestionForm').find('#surveyId').val($('#Id').val());
            $('#DeleteQuestionForm').find('#surveyName').val($('#Name').val());
            $('#deleteQuestionModal').modal('show');
        });

        $('#btnDeleteQuestion').click(function (e) {
            $('#DeleteQuestionForm').submit();
            e.preventDefault();
        });

        $('#btnDeleteOption').click(function (e) {
            $('#DeleteOptionForm').submit();
            e.preventDefault();
        });

        $('#btnSaveSurvey').click(function () {
            var id = $('#Id').val();

            if (id == '' || id == null || id == undefined) {
                addOrEditSurvey();
            } else {
                var name = $('#Name').val();
                if (name == '' || name == null || name == undefined) {
                    $('#SurveyRequireMessage').show('slow');
                    $('#Name').focus();
                } else {
                    $('#AddOrEditConfirmationModal').modal('show');
                }
            }
        });

        $('#btnAddOrEditSurveySubmit').click(function (e) {
            e.preventDefault();
            addOrEditSurvey();
        });
    }

    var addOrEditSurvey = function (id, name) {
        $('#AddOrEditConfirmationModal').modal('hide');
        var name = $('#Name').val();
        var id = $('#Id').val();

        if (name == '' || name == null || name == undefined) {
            $('#SurveyRequireMessage').show('slow');
            $('#Name').focus();
        } else {
            $.ajax({
                url: '/Survey/ValidateSurveyName',
                type: 'GET',
                contentType: "application/json",
                data: ({ id: id, name: name }),
                success: function (data) {
                    if (data > 0) {
                        $('#SurveyRequireMessage').show('slow');
                        $('#Name').focus();
                    } else {
                        $('#SurveyRequireMessage').hide('slow');
                        var model = {
                            'Id': id,
                            'Name': name,
                            'Description': $('#Description').val()
                        };
                        showLoader();
                        $.ajax({
                            url: '/Survey/AddOrEditSurvey',
                            type: 'POST',
                            contentType: "application/json",
                            data: JSON.stringify({
                                model: model,
                                questionOrder: getItemsOrder('sortableQuestion', 'sortQuestion')
                            }),
                            success: function (result) {
                                hideAddOrEdit(result);
                            }
                        });
                    }
                }
            });
        }
    }

    var onSuccessLoad = function () {
        init();
        hideLoader();
        $('#btnAdd').attr('disabled', true);
        $('#deleteQuestionModal').modal('hide');
        $('#detailContainer').show('slow');
        $("#sortableQuestion").sortable();
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
    };

    var successSearch = function () {
        $('#txtSearch').val('');
    };

    var hideAddOrEdit = function (data) {
        if (data == 'Success') {
            $('#detailContainer').hide('slow');
            window.location.reload();
        }
        else {
            $('#InformationMessage').html(data);
        }
    }

    var onSuccessAddOption = function () {
        hideLoader();
        $('#addOptionModal').modal('hide');
    }

    var onSuccessLoadOption = function () {
        $("#sortableOption").sortable();
        $('#deleteOptionModal').modal('hide');        

        $('#OptionContainer').find('#btnAddNewOption').click(function () {
            $('#addOptionModal').find('#optionId').val(null);
            $('#addOptionModal').find('#text').val('');
            $('#addOptionModal').modal('show'); 
            
            var showNext = $(this).attr('showNextQuestion');            
            var order = $(this).attr('questionOrder');
            if (showNext == 'True') {
                showNextQuestions(order, null);
            }
        });

        $('#OptionContainer').find('.editOption').click(function () {
            var showNext = $(this).attr('showNextQuestion');
            var order = $(this).attr('questionOrder');
            var nextQuestionId = $(this).attr('nextQuestionId');
            if (showNext == 'True') {
                showNextQuestions(order, nextQuestionId);
            }

            $('#addOptionModal').find('#optionId').val($(this).attr('optionId'));
            $('#addOptionModal').find('#text').val($(this).attr('optionText'));
            $('#addOptionModal').modal('show');
        });

        $('#OptionContainer').find('.removeOption').click(function (e) {
            $('#DeleteOptionForm').find('#optionId').val($(this).attr('optionId'));
            $('#deleteOptionModal').modal('show');
            e.preventDefault();
        });
    }

    var onSuccessLoadQuestion = function () {
        $('#detailContainer').find('#QuestionContainer').show('slow');

        $("#QuestionTypeId").select2('data', {});
        $("#QuestionTypeId").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        }).on('change', function () {
            $('#ValidateQuestionTypeForm').find('#QuestionType').val($(this).val());
            $('#ValidateQuestionTypeForm').submit();
        });

        $('#btnCloseOption').click(function () {
            $('#btnAddNewQuestion').removeAttr('disabled');
            $('#btnSaveSurvey').removeAttr('disabled'); 
            $('#detailContainer').find('#QuestionContainer').hide('slow');
            removeShadow();
            disableSurveyButtons(false);
        });

        $('#btnAddOrEditQuestion').click(function () {
            if (validatesOptions()) {
                var id = $('#QuestionContainer').find('#questionEditId').val();
                var text = $('#QuestionContainer').find('#Text').val();
                var questionType = $('#QuestionContainer').find('#QuestionTypeId').val();
                var description = $('#QuestionContainer').find('#DescriptionQuestion').val();
                var mainDescription = $('#Description').val();
                var surveyId = $('#Id').val();
                var surveyName = $('#Name').val();

                if ((text == '' || text == null || text == undefined) || (questionType == '' || questionType == null || questionType == undefined)) {
                    $('#QuestionRequireMessage').show('slow');
                } else {
                    showLoader();
                    $('#QuestionRequireMessage').hide('slow');

                    $.ajax({
                        url: '/Survey/AddOrEditQuestion',
                        type: 'POST',
                        contentType: "application/json",
                        data: JSON.stringify({
                            questionId: id,
                            text: text,
                            questionType: questionType,
                            description: description,
                            surveyId: surveyId,
                            surveyName: surveyName,
                            optionOrder: getItemsOrder('sortableOption', 'sortOption')
                        }),
                        success: function (partialViewResult) {
                            $("#detailContainer").html(partialViewResult);
                            $('#Description').val(mainDescription);
                            hideLoader();
                            onSuccessLoad();
                            disableSurveyButtons(false);
                            removeShadow();
                        }
                    });
                }
                $('#btnAddNewQuestion').removeAttr('disabled');
                $('#btnSaveSurvey').removeAttr('disabled');
            } else {
                $('#ValidateOptionModal').modal('show');
            }            
        }); 
        onSuccessLoadOption();
    }

    var validatesOptions = function () {
        var showOptions = $('#showOptions').val();
        var valid = 0;
        if (showOptions == 'True') {
            $('#sortableOption .sortOption').each(function () {
                valid++;
            });
        }
        return (showOptions == undefined || showOptions == '') || valid > 0;
    }

    var removeShadow = function () {
        $('.sortQuestion').each(function () {
            $(this).removeClass('boxShadowSelect');
        });
    }

    var disableSurveyButtons = function (val){
        $('#btnCloseMSurvey').attr('disabled', val);
        $('#btnAddOrEditMSurvey').attr('disabled', val);
    }

    var getItemsOrder = function (container, className) {
        var list = [];
        var order = 1;

        $('#' + container).find('.' + className).each(function () {
            var item = {
                'Order': order,
                'Item': $(this).attr('id')
            };
            list.push(item);
            order++;
        });
        return list;
    }

    var showNextQuestions = function (order, nextQuestionId) {
        $('#nextQuestionId').html('');
        $('#nextQuestionId').append('<option value="">Seleccione Siguiente Pregunta</option>');
        $('#nextQuestionId').val('').trigger('change');

        $.ajax({
            url: '/Survey/GetNextQuestion',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ order: order }),
            success: function (result) {
                if (result.length > 0) {
                    //Clean dbx
                    $('#NextQuestionDrop').removeClass('hide');
                    for (var i = 0; i < result.length; i++) {
                        $('#nextQuestionId').append($("<option></option>").val(result[i].Id).html(result[i].Text));
                    }
                    $('#nextQuestionId').select2('val', nextQuestionId);
                }
            }
        });  
        $('#nextQuestionId').select2('data', {});
        $('#nextQuestionId').select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
    }

    return {
        Init: init,
        OnSuccessLoad: onSuccessLoad,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        SuccessSearch: successSearch,
        HideAddOrEdit: hideAddOrEdit,
        OnSuccessLoadQuestion: onSuccessLoadQuestion, 
        OnSuccessAddOption: onSuccessAddOption,
        OnSuccessLoadOption: onSuccessLoadOption,
        DisableSurveyButtons: disableSurveyButtons
    };
})();