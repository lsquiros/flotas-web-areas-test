﻿var ECO_Management = ECO_Management || {};

ECO_Management.MeasuringAgentsMonthlyReport = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    }

    var onEndGetData = function (data) {
        hideloader();
        if (data == "Success") {
            $('#DonwloadFileForm').submit();
            $('#InfoContainer').html('<br />' +
                                     '<div class="alert alert-success alert-dismissible fade in" role="alert" style="margin-bottom: -5px;">' +
                                        '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Seleccione la información necesaria para generar el reporte.'+
                                     '</div>');
        } else if (data == "NoData") {
            $('#InfoContainer').html('<br />' +
                                     '<div class="alert alert-info alert-dismissible fade in" role="alert" style="margin-bottom: -5px;">' +
                                        '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;No existe información para descargar el reporte con las fechas seleccionadas.'+
                                     '</div>');
        }else {
            $('#InfoContainer').html(data);
        }
    }

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    return {
        Init: initialize,
        ShowLoader: showloader,
        OnEndGetData: onEndGetData
    };
})();