﻿var ECO_Management = ECO_Management || {};

ECO_Management.GeoFenceByDriver = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $("#DriverId").select2('data', {});
        $("#DriverId").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $('#datetimepickerEndDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    }

    var onEndGetData = function (data) {
        hideloader();
        if (data == "Success") {
            $('#DonwloadFileForm').submit();
        } else {
            $('#InfoContainer').html(data);
        }
    }

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    return {
        Init: initialize,
        ShowLoader: showloader,
        OnEndGetData: onEndGetData
    };
})();