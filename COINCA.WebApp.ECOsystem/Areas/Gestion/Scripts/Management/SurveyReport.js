﻿var ECO_Management = ECO_Management || {};

ECO_Management.SurveyReport = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $('#datetimepickerDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $('#UserId').select2();

        $('#InfoContainer').off('click.download_survey_report', 'a[download_survey_report]').on('click.download_survey_report', 'a[download_survey_report]', function (e) {
            var userid = $('#UserId').val();
            var date= $('#StartDate').val();
            var commerceid = $(this).attr('commerceid');
            var survey = $(this).attr('survey');
            var usereventid = $(this).attr('usereventid');
            onDownLoadReport(userid, date, commerceid, survey, usereventid)
        });
    }

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    var loadSurveys = function () {
        var userid = $('#UserId').val();
        var date = $('#StartDate').val();

        if (userid != '' || date != '') {
            showloader();
            $.ajax({
                url: '/SurveyReport/GetSurveyList',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify({ userId: userid, date: date }),
                success: function (result) {
                    hideloader();
                    if (result.split('|')[0] == 'Error') {
                        $('#InfoContainer').html(result.split('|')[0]);
                    } else {
                        $('#InfoContainer').html(result);
                    }
                }
            });
        } else {
            $('#InfoContainer').html('<br />' +
                '<div class="alert alert-info alert-dismissible fade in" role="alert" style="margin-bottom: -5px;">' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Por favor seleccione un Agente y una Fecha para ver los Formularios.' +
                '</div>');
        }
    }

    var onDownLoadReport = function (userid, date, commerceid, surveyid, usereventid) {
        showloader();
        $.ajax({
            url: '/SurveyReport/GetReportData',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ UserId: userid, CommerceId: commerceid, DateOfCheck: date, SurveyId: surveyid, UserEventId: usereventid }),
            success: function () {
                hideloader();
                $('#DonwloadFileForm').submit();
                return false;
            }
        });
    }

    return {
        Init: initialize,
        OnDownLoadReport: onDownLoadReport,
        LoadSurveys: loadSurveys
    };
})();