﻿var ECO_Management = ECO_Management || {};

ECO_Management.ManagementParameters = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $('.number-only').keypress(function (e) {
            if (isNaN(String.fromCharCode(e.charCode))) {
                return false;
            }
        });
    }

    var onCompleteAddOrEdit = function (data) {
        hideloader();
        if (data != 'Success') {
            $('#ControlsContainer').html(data);
        } else {
            ECOsystem.Utilities.setConfirmation();
            window.location.reload();
        }
    }

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    return {
        Init: initialize,
        ShowLoader: showloader,
        OnCompleteAddOrEdit: onCompleteAddOrEdit
    };
})();