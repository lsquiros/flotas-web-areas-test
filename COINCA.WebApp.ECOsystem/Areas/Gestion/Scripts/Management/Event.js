﻿var ECO_Management = ECO_Management || {};

ECO_Management.Event = (function () {

    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);        
 
        //Edit for the vehicle grid
        $('#gridContainer').off('click.edit', 'a[edit_row_events]').on('click.edit', 'a[edit_row_events]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();
            $('#addOrEditModal').modal('show');        
        });

        $('#btnAddEvent').click(function () {
            var showWarning = $('#showWarning').val();
            var eventId = $('#EventTypeId').val();
            var beginjourney = $('#BeginJourney').val();
            var currentEventId = $('#CurrentEventId').val();
            var hasEvents = $('#hasEvents').val();

            if ((showWarning == 'False' && hasEvents == 'True') && 
                (currentEventId != beginjourney && eventId == beginjourney)) {
                $('#ValidateBeginJourneyAdd').modal('show');
            } else {
                
                var genericEvent = $('#GenericEvent').val();
                var initEvent = $('#Init_Name').val();
                var finisEvent = $('#Finish_Name').val();

                if ((eventId == '' || eventId == null || eventId == undefined)
                    || (initEvent == '' || initEvent == null || initEvent == undefined)
                    || (eventId != genericEvent && ((finisEvent == '' || finisEvent == null || finisEvent == undefined)))
                ) {
                    $('#showEventValidation').show('slow');
                } else {
                    $('#addOrEditForm').submit();
                }
            }
        });

        $('#btnClose').off('click.btnClose').on('click.btnClose', function (e) {
            $('#detailContainer').hide('slow');
        });

        $('#btnAdd').off('click.btnAddNew').on('click.btnAddNew', function (e) {
            $('#loadForm').find('#id').val(null);
            $('#loadForm').submit();
        });

        $('#btnGetDownloadData').click(function (e) {
            $('#ExcelReportDownloadForm').submit();
            e.preventDefault();
        });
        
        $("#Parent").select2('data', {});
        $("#Parent").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        $("#EventTypeId").select2('data', {});
        $("#EventTypeId").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
        
        $("#Init_SurveyId").select2('data', {});
        $("#Init_SurveyId").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
        
        $("#Finish_SurveyId").select2('data', {});
        $("#Finish_SurveyId").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        $("#Init_IconId").select2('data', {});
        $("#Init_IconId").select2({
            formatResult: iconSelectFormat,
            formatSelection: iconSelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        $("#Finish_IconId").select2('data', {});
        $("#Finish_IconId").select2({
            formatResult: iconSelectFormat,
            formatSelection: iconSelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
    };

    var iconSelectFormat = function (item) {
        if (!item.id) return item.text;
        return '<div class="row" ><div class="col-md-12" ><i class="mdi ' + item.text.split('|')[1] + '"></i>&nbsp; ' + item.text.split('|')[0] + '</div></div>';
    };
    
    var onSuccessLoad = function () {
        initialize();
        $('#detailContainer').show('slow');
        jscolor.installByClassName("jscolor");
    };
   
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
    };

    var successSearch = function () {
        $('#txtSearch').val('');
    };

    var hideAddOrEdit = function () {        
        $('#detailContainer').hide('slow');
        window.location.reload();
    }   

    var onCompleteDownload = function (data) {
        hideLoader();
        if (data == 'Success') {
            $('#DownloadExcelForm').submit();
        } else {
            $('#InformationMessage').html(data);
        }
    }

    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        SuccessSearch: successSearch,
        HideAddOrEdit: hideAddOrEdit,
        OnCompleteDownload: onCompleteDownload
    };
})();

