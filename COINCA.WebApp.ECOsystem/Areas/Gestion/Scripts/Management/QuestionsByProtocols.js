﻿var ECO_Management = ECO_Management || {};

ECO_Management.QuestionsByProtocols = (function () {
    var options = {};
    var data = [];
    var beginlist = {};
    var endlist = {};

    var initialize = function (opts) {
        $.extend(options, opts);
        $('#tooltipreport').tooltip('disable');

        $("#ddxProtocolId").select2('data', {});
        $("#ddxProtocolId").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        var getquestionsdata = function () {
            var protocolid = $('#ddxProtocolId').val();

            $.ajax({
                url: '/QuestionsByProtocols/GetQuestionsByCustomer',
                type: "POST",
                data: JSON.stringify({ ProtocolId: protocolid }),
                contentType: "application/json",
                success: function (result) {
                    for (var i = 0; i < result.length; i++) {
                        data.push({ "id": result[i].Value, "text": result[i].Text });
                    }

                    $('#divSummary').show();
                }
            });

            $(".txtNextQuestion").select2({
                escapeMarkup: function (m) { return m; },
                tags: data,
                maximumSelectionSize: 1,
                multiple: true
            });

            $("#MainQuestion").select2({
                escapeMarkup: function (m) { return m; },
                tags: data,
                maximumSelectionSize: 1,
                multiple: true
            });

            $('#ShowCurrentProtocolForm').on;
        }

        $('#btnProtocolSimulation').click(function () {
            showLoader();
            $('#ShowProtocolSimulatorForm').find('#ProtocolId').val($('#ddxProtocolId').val());
            $('#ShowProtocolSimulatorForm').submit();
        });

        $('#btnAddQuestion').click(function () {

            var protocolid = $('#ddxProtocolId').val();
            if (protocolid == 0) {
                $('#tooltipreport').tooltip('enable');
                $('#tooltipreport').tooltip().mouseover();
            } else {
                $('#tooltipreport').tooltip('disable');
                showQuestionAnswer(protocolid)
            }

        });

        $('#chkSummary').click(function () {
            //showLoader();
            var protocolId = $('#ddxProtocolId').val();
            var textPregunta = 'Resumen Final';
            
            if ($('#chkSummary').prop('checked') == 1) {
                $.ajax({
                    url: '/QuestionsByProtocols/AddSummaryQuestionToProtocol',
                    type: "POST",
                    data: JSON.stringify({ ProtocolId: protocolId, QuestionText: textPregunta}),
                    contentType: "application/json",
                    success: function (result) {}
                });
            }
            else {
                $.ajax({
                    url: '/QuestionsByProtocols/DeleteQuestion',
                    type: "POST",
                    data: JSON.stringify({ QuestionId: 0, ProtocolId: protocolId, Summary:true }),
                    contentType: "application/json",
                    success: function (result) {}
                });
            }
            
        });

        var showQuestionAnswer = function (protocolid) {

            if (protocolid != "") {
                $('#ProtocolId').val(protocolid);
                $('#QuestionsContainer').show();
                $('#divSummary').show();

                getquestionsdata();
                showLoader();
                clearobjects();
                $('#ShowCurrentProtocolForm').find('#ProtocolId').val(protocolid);
                $('#ShowCurrentProtocolForm').submit();
            }
        }

        $('.addQuestion').click(function () {
            hidequestions();
            $('#AddQuestionToProtocolForm').find('#QuestionId').val($("#MainQuestion").select2('data')[0].id);
            $('#AddQuestionToProtocolForm').find('#QuestionText').val($("#MainQuestion").select2('data')[0].text);
            $('#AddQuestionToProtocolForm').find('#ProtocolIdToForm').val($('#ProtocolId').val());
            $('#AddQuestionToProtocolForm').find('#RelationId').val(null);

            $('#AddQuestionToProtocolForm').submit();
            getquestionsdata();
            $('#MainQuestion').select2('val', '');
            $('#divSummary').show();
        });

        $('#QuestionsContainer').on('click', '.panel-heading', function () {
            hidequestions();
            $(this).nextAll('.panel-body').show();
            $('#divSummary').show();
        });

        $('#QuestionsContainer').on('click', '.btnDeleteQuestion', function () {
            var questionId = $(this).attr('questionid');
            $('#typeDelete').val(0);
            $('#questionIdDelete').val(questionId);
            $('#messageModal').modal('show');
            $('#divSummary').show();
        });

        //Start Edit Answers

        $('#QuestionsContainer').on('click', '.btnDeleteAnswer', function () {
            var questionId = $(this).attr('questionid');
            $('#questionIdDelete').val(questionId);
            $('#typeDelete').val(1);
            $('#messageModal').modal('show');
            $('#divSummary').show();
        });

        $('#QuestionsContainer').on('click', '.btnEditAnswer', function () {
            var orderId = $(this).attr('id');
            $('#lblAnswerId_' + orderId).hide();
            $('#editAnswerId_' + orderId).show('slow');
            $('#divSummary').show();
        });

        $('#QuestionsContainer').on('click', '.btnCancelSaveAnswer', function () {
            var orderId = $(this).attr('id');
            $('#lblAnswerId_' + orderId).show('slow');
            $('#editAnswerId_' + orderId).hide();
            $('#divSummary').show();
        });

        $('#QuestionsContainer').on('click', '.btnSaveAnswer', function () {
            var questionId = $(this).attr('id');
            updateQuestion(questionId);
            $('#divSummary').show();
        });

        var updateQuestion = function (questionId) {
            var text = $('#txtEditAnswer_' + questionId).val();

            $.ajax({
                url: '/QuestionsByProtocols/UpdateAnswerByCustomer',
                type: "POST",
                data: JSON.stringify({ QuestionId: questionId, QuestionText: text }),
                contentType: "application/json",
                success: function (result) {
                    $('#activeAnswerId_' + questionId).html('<b>' + questionId + ':&nbsp;' + text + '</b>')
                    $('#lblAnswerId_' + questionId).show('slow');
                    $('#editAnswerId_' + questionId).hide();
                    $('#divSummary').show();
                }
            });
        }

        //end edit answer

        $('#QuestionsContainer').on('click', '.btnEditQuestion', function () {
            var orderId = $(this).attr('id');

            $('#lblQuestionId_' + orderId).hide();
            $('#editQuestionId_' + orderId).show('slow');
            $('#divSummary').show();
        });

        $('#QuestionsContainer').on('click', '.btnSaveQuestion', function () {
            var orderId = $(this).attr('id');
            var questionId = $(this).attr('questionid');
            updateQuestionPanel(orderId, questionId);
            $('#divSummary').show();
        });

        $('#QuestionsContainer').on('click', '.btnCancelSaveQuestion', function () {
            var orderId = $(this).attr('id');
            $('#lblQuestionId_' + orderId).show('slow');
            $('#editQuestionId_' + orderId).hide();
            $('#divSummary').show();
        });



        $('#QuestionsContainer').on('change', '.ddlTypes', function () {
            var orderId = $(this).attr('id');
            $('#lblQuestionId_' + orderId).show('slow');
            $('#editQuestionId_' + orderId).hide();
            $('#divSummary').show();
        });

        $('#QuestionsContainer').on('click', '.btnAddAnswer', function () {
            var orderId = $(this).attr('id');
            var answerId = $("#txtNewAnswer_" + orderId).select2('data')[0].id;
            var text = $("#txtNewAnswer_" + orderId).select2('data')[0].text;
            var typeCode = $('#ddxTypeId_' + orderId).val();
            var programProtocolId = $('#ProgramProtocolId_' + orderId).val();

            validateAndAddAnswer(answerId, text, orderId, typeCode, programProtocolId);
            $('#divSummary').show();
        });

        $('#QuestionsContainer').on('click', '.btnNextQuestion', function () {

            var orderId = $(this).attr('id');
            var answerId = $(this).attr('answerid');
            var answerOrderId = $(this).attr('answerorderid');
            var text = $('#txtNextQuestion_' + orderId + '_' + answerId + '_' + answerOrderId).select2('data')[0];
            if (text == undefined) {
                $('#ValidateText').modal('show');
            }
            else {
                $('#AddQuestionToProtocolForm').find('#QuestionId').val($('#txtNextQuestion_' + orderId + '_' + answerId + '_' + answerOrderId).select2('data')[0].id);
                $('#AddQuestionToProtocolForm').find('#QuestionText').val($('#txtNextQuestion_' + orderId + '_' + answerId + '_' + answerOrderId).select2('data')[0].text);
                $('#AddQuestionToProtocolForm').find('#ProtocolIdToForm').val($('#ProtocolId').val());
                $('#AddQuestionToProtocolForm').find('#RelationId').val(true);
                $('#AddQuestionToProtocolForm').find('#ProgramProtocolId').val($('#ProgramProtocolId_' + orderId).val());
                $('#AddQuestionToProtocolForm').find('#AnswerId').val(answerId);
                $('#AddQuestionToProtocolForm').submit();
                $('#ConfirmationRelationAnswer').modal('show');
            }
            $('#divSummary').show();
        });

        $('#ProtocolSimulationContainer').on('click', '#btnNextQuestion', function () {
            var nextquestion = $(this).attr('nextquestion');
            var currentquestion = $(this).attr('currentorder');
            var code = $(this).attr('answercode');

            nextQuestionInSimulation(nextquestion, currentquestion, code);
        });

        //$('#ProtocolSimulationContainer').on('click', '.rdbAnswersWithRelation', function () {
        //    var relation = $(this).attr('relation');
        //    if (relation != 'null' || relation == null) {
        //        $('#btnNextQuestion').removeClass('hide');
        //        $('#btnFinishSimulation').addClass('hide');                
        //        $('#btnNextQuestion').attr('currentorder', $(this).attr('order'));
        //        $('#btnNextQuestion').attr('answercode', $(this).attr('answercode'));                
        //    } else {
        //        $('#btnNextQuestion').addClass('hide');
        //        $('#btnFinishSimulation').removeClass('hide');
        //    }
        //});

        // function of Drag And Drop  questions list
        //$("#ShowCurrentProtocolForm").data("validator").settings.submitHandler = function (e) {
        //    $('#DragAndDropContainer').find('#processing').removeClass('hide');
        //    $('#DragAndDropContainer').find('button').attr("disabled", "disabled");
        //    $('#DragAndDropContainer').find('a[atype="button"]').attr("disabled", "disabled");
        //};
    };

    var updateOrderQuestion = function (questionid, order) {
        $.ajax({
            url: '/QuestionsByProtocols/DeleteQuestion',
            type: "POST",
            data: JSON.stringify({ QuestionId: questionId, Order: order }),
            contentType: "application/json",
            success: function (result) {
                $('#messageModal').modal('hide');
                $('#ProtocolId').val(protocolid);
                $('#QuestionsContainer').show();
                $('#divSummary').show();
                showLoader();
                clearobjects();
                $('#ShowCurrentProtocolForm').find('#ProtocolId').val(protocolid);
                $('#btnAddQuestion').click();
            }
        });
    }
    var deleteQuestion = function () {
        questionId = $('#questionIdDelete').val();
        var protocolid = $('#ddxProtocolId').val();

        $.ajax({
            url: '/QuestionsByProtocols/DeleteQuestion',
            type: "POST",
            data: JSON.stringify({ QuestionId: questionId, ProtocolId: protocolid }),
            contentType: "application/json",
            success: function (result) {
                $('#messageModal').modal('hide');
                $('#ProtocolId').val(protocolid);
                $('#QuestionsContainer').show();
                $('#divSummary').show();
                showLoader();
                clearobjects();
                $('#ShowCurrentProtocolForm').find('#ProtocolId').val(protocolid);
                $('#btnAddQuestion').click();
            }
        });
    }

    var updateQuestionPanel = function (orderId, questionId) {
        var text = $('#txtEditQuestion_' + orderId).val();
        var protocolid = $('#ddxProtocolId').val();
        $.ajax({
            url: '/QuestionsByProtocols/UpdateQuestionByCustomer',
            type: "POST",
            data: JSON.stringify({ QuestionId: questionId, QuestionText: text, ProtocolId: protocolid }),
            contentType: "application/json",
            success: function (result) {
                $('#activeQuestionId_' + orderId).html('<b>' + orderId + ':&nbsp;' + result + '</b>')
                $('#lblQuestionId_' + orderId).show('slow');
                $('#editQuestionId_' + orderId).hide();
                $('#divSummary').show();
            }
        });
    }

    var successAdd = function () {
        var summaryQustID = $("body").find('#spanSummary').attr('SummaryQuestionID');

        if (summaryQustID != undefined && summaryQustID != null)
            $('#chkSummary').attr('checked', 'checked');

        $('#divSummary').show();
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });

        $(".ddlTypes").each(function () {
            if (!$(this).data('select2')) {
                var id = $(this).attr('orderId');

                $('#ddxTypeId_' + id).select2('data', {});
                $('#ddxTypeId_' + id).select2({
                    escapeMarkup: function (m) { return m; },
                    allowClear: true
                }).on('change', function () {
                    var orderId = $(this).attr('orderId');
                    var code = $(this).val();
                    hideAnswers(orderId, code);
                });
            }
        });
    }

    var hideAnswers = function (orderId, code) {
        var programProtocolId = $('#ProgramProtocolId_' + orderId).val();
        if (code == 'QUESTIONS_TYPE_TEXT' || code == 'QUESTIONS_TYPE_IMAGE' || code == 'QUESTIONS_TYPE_SIGN') {
            $.ajax({
                url: '/QuestionsByProtocols/UpdateProgramProtocol',
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ programProtocolId: programProtocolId, typeCode: code, orderId: orderId, calification: null, answerId: null }),
                success: function (result) { }
            });
            showInformationForAnswers(code, orderId);
            $('#showAnswers_' + orderId).hide();

        } else {
            if (code == 'QUESTIONS_TYPE_CHECK') {
                $('#NextQuestionColum_' + orderId).addClass('hidden');
                shownextquestion(orderId, false);
                $.ajax({
                    url: '/QuestionsByProtocols/UpdateProgramProtocol',
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({ programProtocolId: programProtocolId, typeCode: code, orderId: orderId, calification: null, answerId: null }),
                    success: function (result) {
                        cleanNextQuestionTexts(orderId);
                    }
                });
            } else {
                $('#NextQuestionColum_' + orderId).removeClass('hidden');
                shownextquestion(orderId, true);
            }
            getAnswersData(orderId);
            showInformationForAnswers(code, orderId);
            $('#showAnswers_' + orderId).show();
        }
        $('#divSummary').show();
    }

    var showInformationForAnswers = function (code, orderId) {
        switch (code) {
            case 'QUESTIONS_TYPE_TEXT':
                $('#infoForAnswers_' + orderId).html('Muestra un cuadro de texto para digitar la respuesta.');
                break;
            case 'QUESTIONS_TYPE_IMAGE':
                $('#infoForAnswers_' + orderId).html('Muestra un cuadro de selección de imagen.');
                break;
            case 'QUESTIONS_TYPE_SIGN':
                $('#infoForAnswers_' + orderId).html('Muestra un cuadro de selección de Firma.');
                break;
            case 'QUESTIONS_TYPE_RADIO':
                $('#infoForAnswers_' + orderId).html('Muestra una lista con una selección única.');
                break;
            case 'QUESTIONS_TYPE_DROPDOWN':
                $('#infoForAnswers_' + orderId).html('Muestra una lista despegable de selección.');
                break;
            case 'QUESTIONS_TYPE_CHECK':
                $('#infoForAnswers_' + orderId).html('Muestra una lista con selección multiple.');
                break;
            default:
                $('#infoForAnswers_' + orderId).html('Seleccione un tipo de respuesta.');
                break;
        }
    }

    var getAnswersData = function (orderId) {
        var data = [];
        $.ajax({
            url: '/QuestionsByProtocols/GetAnswersByCustomer',
            type: "POST",
            contentType: "application/json",
            success: function (result) {
                for (var i = 0; i < result.length; i++) {
                    data.push({ "id": result[i].Value, "text": result[i].Text });
                }
            }
        });

        $("#txtNewAnswer_" + orderId).select2({
            escapeMarkup: function (m) { return m; },
            tags: data,
            maximumSelectionSize: 1,
            multiple: true
        });
    }

    var validateAndAddAnswer = function (answerId, text, orderId, typeCode, programProtocolId) {
        if (answerId == '' || isNaN(answerId)) answerId = null;

        $.ajax({
            url: '/QuestionsByProtocols/AddNewAnswer',
            type: "POST",
            data: JSON.stringify({ AnswerId: answerId, OrderId: orderId, ProgramProtocolId: programProtocolId, TypeCode: typeCode, AnswerText: text }),
            contentType: "application/json",
            success: function (result) {
                answerId = result.AnswerId;
                var AnswerOrderId = result.AnswerOrderId;
                var columnhide = 'thnextquestion_' + orderId + (typeCode != 'QUESTIONS_TYPE_CHECK' ? ' ' : ' hidden');
                $('#listAnswerTable_' + orderId + ' tbody').append('<tr>' +
                    '<th >' + text + '</th>' +
                    '<th class = "' + columnhide + '" ><div class="input-group nextquestiongroup">' +
                    '<span data-toggle="tooltip" data-placement="top" title="Seleccione una de las opciones ó digite su respuesta y presione enter">' +
                    '<input class="form-control " id="txtNextQuestion_' + orderId + '_' + answerId + '_' + AnswerOrderId + '" placeholder="Siguiente Pregunta" type="text">' +
                    '</span>' +
                    '<span class="input-group-btn">' +
                    '<button type="button" class="btn btn-warning btnNextQuestion" id="' + orderId + '" answerid="' + answerId + '" answerorderid ="' + AnswerOrderId + '"><i class="mdi mdi-plus"></i>&nbsp;</button>' +
                    '</span>' +
                    '</div>' +
                    '</th>' +
                    //'<th><select class="form-control ddlLevel" custom="true" id="ddlLevel_' + orderId + '_' + answerId + '_' + AnswerOrderId + '"  orderid="' + orderId + '" answerid="' + answerId + '"><option value="">Seleccione el nivel</option></select></th>' +
                    '</tr>');
                $("#txtNewAnswer_" + orderId).select2('val', '');
                $('#tl_2').remove();
                getAnswerControlsInit(orderId, answerId, AnswerOrderId);
            }
        });
    }

    var shownextquestion = function (orderId, show) {
        $('.thnextquestion_' + orderId).each(function () {
            if (show) {
                $(this).removeClass('hidden');
            } else {
                $(this).addClass('hidden');
            }
        });
    }

    var getAnswerControlsInit = function (orderId, answerId, answerOrderId) {
        //Get the questions 
        var data = [];
        var protocolid = $('#ddxProtocolId').val();
        $.ajax({
            url: '/QuestionsByProtocols/GetQuestionsByCustomer',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ProtocolId: protocolid }),
            success: function (result) {
                for (var i = 0; i < result.length; i++) {

                    if (result[i].Value != answerId) {
                        data.push({ "id": result[i].Value, "text": result[i].Text });
                    }
                }
                $('#txtNextQuestion_' + orderId + '_' + answerId + '_' + answerOrderId).select2({
                    escapeMarkup: function (m) { return m; },
                    tags: data,
                    maximumSelectionSize: 1,
                    multiple: true
                });
            }
        });

        //Get the levels 
        $.ajax({
            url: '/QuestionsByProtocols/GetAnswerLevels',
            type: "POST",
            contentType: "application/json",
            success: function (result) {
                for (var i = 0; i < result.length; i++) {
                    $('#ddlLevel_' + orderId + '_' + answerId + '_' + AnswerOrderId).append($('<option></option>').val(result[i].Value).html(result[i].Text));
                }

                $('#ddlLevel_' + orderId + '_' + answerId + '_' + AnswerOrderId).select2('data', {});
                $('#ddlLevel_' + orderId + '_' + answerId + '_' + AnswerOrderId).select2({
                    escapeMarkup: function (m) { return m; },
                    allowClear: true
                }).on('change', function () {
                    var orderId = $(this).attr('orderId');
                    var answerId = $(this).attr('answerId');
                    var calification = $(this).val();
                    changeCalification(orderId, calification, answerId);
                });
            }
        });
    };

    var changeCalification = function (orderId, calification, answerId) {
        var programProtocolId = $('#ProgramProtocolId_' + orderId).val();
        $.ajax({
            url: '/QuestionsByProtocols/UpdateProgramProtocol',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ programProtocolId: programProtocolId, typeCode: null, calification: calification, answerId: answerId }),
            success: function (result) { }
        });
    }

    var successShow = function () {
        var summaryQustID = $("body").find('#spanSummary').attr('SummaryQuestionID');
        if (summaryQustID != undefined && summaryQustID != null)
            $('#chkSummary').attr('checked', 'checked');

        $('#divSummary').show();
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });

        $(".ddlTypes").each(function () {
            var id = $(this).attr('orderId');
            var selectedvalue = $(this).attr('value');

            $('#ddxTypeId_' + id).select2('data', {});
            $('#ddxTypeId_' + id).select2({
                escapeMarkup: function (m) { return m; },
                allowClear: true
            }).on('change', function () {
                var orderId = $(this).attr('orderId');
                var code = $(this).val();
                hideAnswers(orderId, code);
            });

            $(this).val(selectedvalue).change();
        });

        $(".ddlLevel").each(function () {
            var orderid = $(this).attr('orderid');
            var answerid = $(this).attr('answerid');
            var answercount = $(this).attr('answercount');
            var selectedvalue = $(this).attr('value');

            $('#ddlLevel_' + orderid + '_' + answerid + '_' + answercount).select2('data', {});
            $('#ddlLevel_' + orderid + '_' + answerid + '_' + answercount).select2({
                escapeMarkup: function (m) { return m; },
                allowClear: true
            }).on('change', function () {
                var orderId = $(this).attr('orderid');
                var answerId = $(this).attr('answerid');
                var calification = $(this).val();
                if (calification != '') changeCalification(orderId, calification, answerId);
            });

            $(this).val(selectedvalue).change();
        });

        $('.txtNextQuestion').each(function () {
            var data = [];
            var orderid = $(this).attr('orderid');
            var answerid = $(this).attr('answerid');
            var answercount = $(this).attr('answercount');

            var protocolid = $('#ddxProtocolId').val();


            $.ajax({
                url: '/QuestionsByProtocols/GetQuestionsByCustomer',
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ ProtocolId: protocolid }),
                success: function (result) {
                    for (var i = 0; i < result.length; i++) {
                        data.push({ "id": result[i].Value, "text": result[i].Text });
                    }
                    $("#txtNextQuestion_" + orderid + '_' + answerid + '_' + answercount).select2({
                        escapeMarkup: function (m) { return m; },
                        tags: data,
                        maximumSelectionSize: 1,
                        multiple: true
                    });
                }
            });
        });

        hidealthecardsbutfirst();
    }

    var hidequestions = function () {
        $('.panel-body').each(function () {
            $(this).hide();
        });
        $('#divSummary').hide();
    }

    var hidealthecardsbutfirst = function () {
        var first = 1;
        $('.panel-body').each(function () {
            if (first > 1) $(this).hide();
            first++;
        });
    }

    var cleanNextQuestionTexts = function (orderId) {
        $('.txtNextQuestion').each(function () {
            var currentOrder = $(this).attr('orderid');

            if (orderId == currentOrder) {
                $(this).select2('val', '');
            }
        });
    }

    var clearobjects = function () {
        $('.panel').each(function () { $(this).remove(); })
    }

    var succesShowSimulator = function () {
        hideLoader();
        $('#protocolsimulatorModal').modal('show');
    }

    var nextQuestionInSimulation = function (nextquestion, currentquestion, code) {

        //Validates if the question has relation
        var relation = null;
        if (code == 'QUESTIONS_TYPE_RADIO') {
            $('.rdbAnswer_' + currentquestion).each(function () {
                if ($(this).is(':checked')) {
                    relation = $(this).attr('relation');
                    if (relation != 'null') {
                        nextquestion = currentquestion;
                    }
                }
            });
        }

        $.ajax({
            url: '/QuestionsByProtocols/GetNextQuestionSimulation',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ NextQuestion: nextquestion, Relation: relation }),
            success: function (result) {
                $('#questionTitle').html(result[0].Order + ': ' + result[0].Question);
                var code = result[0].Code;
                var html = '';
                switch (code) {
                    case "QUESTIONS_TYPE_TEXT":
                        html = html + '<textarea class="form-control" cols="20" id="txtQuestion" name="txtQuestion" placeholder="Digite su respuesta" rows="2"></textarea>';
                        break;
                    case "QUESTIONS_TYPE_IMAGE":
                        html = html + '<div class="input-group">' +
                            '<input id="txtFile" name="key" type="text" class="form-control">' +
                            '<span class="input-group-btn">' +
                            '<button id="btnSearchFile" type="button" class="btn btn-primary" title="Buscar" onclick="ECO_Management.QuestionsByProtocols.OnClickImport()"><i class="mdi mdi-magnify"></i>&nbsp;Buscar</button>' +
                            '<input type="file" class="hide" id="ImportImg" size="43" name="fileHidden" onchange="ECO_Management.QuestionsByProtocols.GetFile(this)"/>' +
                            '</span>' +
                            '</div>';
                        break;

                    case "QUESTIONS_TYPE_SIGN":
                        html = html + '<div class="input-group">' +
                            '<input id="txtFile" name="key" type="text" class="form-control">' +
                            '<span class="input-group-btn">' +
                            '<button id="btnSearchFile" type="button" class="btn btn-primary" title="Buscar" onclick="ECO_Management.QuestionsByProtocols.OnClickImport()"><i class="mdi mdi-magnify"></i>&nbsp;Buscar</button>' +
                            '<input type="file" class="hide" id="ImportImg" size="43" name="fileHidden" onchange="ECO_Management.QuestionsByProtocols.GetFile(this)"/>' +
                            '</span>' +
                            '</div>';
                        break;
                    case "QUESTIONS_TYPE_CHECK":
                        for (var i = 0; i < result.length; i++) {
                            html = html + '<div class="checkbox">' +
                                '<label><input type="checkbox">&nbsp;' + result[i].Answer + '</label>' +
                                '</div>';
                        }
                        break;
                    case "QUESTIONS_TYPE_RADIO":
                        for (var i = 0; i < result.length; i++) {
                            html = html + '<div class="radio">' +
                                '<label><input type="radio" class="rdbAnswersWithRelation rdbAnswer_' + result[i].Order + '" relation="' + result[i].Relation + '" order="' + result[i].Order + '" answercode="' + code + '" name="answer">&nbsp;' + result[i].Answer + '</label>' +
                                '</div>';
                        }
                        break;
                }
                $('#currentQuestionContainer').html(html);

                if (result[0].NextQuestion == null) {
                    $('#btnNextQuestion').addClass('hide');
                    $('#btnFinishSimulation').removeClass('hide');
                } else {
                    $('#btnNextQuestion').attr('nextquestion', result[0].NextQuestion);
                    $('#btnNextQuestion').attr('currentorder', result[0].Order);
                    $('#btnNextQuestion').attr('answercode', code);
                }
            }
        });
    }

    var onClickImport = function () {
        $('#ImportImg').trigger('click');
    };

    var getFile = function (input) {
        var value = $("#ImportImg").val();
        var NameOfFile = input.files[0].name;
        $("#txtFile").val(NameOfFile);
    }

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
    };

    return {
        Init: initialize,
        SuccessAdd: successAdd,
        SuccessShow: successShow,
        HideLoader: hideLoader,
        SuccesShowSimulator: succesShowSimulator,
        OnClickImport: onClickImport,
        GetFile: getFile,
        DeleteQuestion: deleteQuestion
    };
})();

