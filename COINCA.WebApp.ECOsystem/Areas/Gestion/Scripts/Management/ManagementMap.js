﻿var ECO_Management = ECO_Management || {};

ECO_Management.ManagementMap = (function () {
    var options = {};
    var fragmentPlaying = 0;
    var playingAll = '';
    var resetFragment = '';
    var cardPlaying = 0;
    var dateRegex = /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;

    var initialize = function (opts) {
        $.extend(options, opts);

        $("#menu-left-toggle").click(function () {
            leftMenuToggle();
        });

        $('#btnLoadCommerces').click(function () {
            onclick = "displayContactList(); location.href = location.href; return false"
            loadCommerces();
        });

        $("#tabAgents").click(function () {
            var checked = false;
            searchAgents('');
            $('[type="checkbox"].checkboxAgent').attr('checked', false);
            $('#txtSearchAgents').val('');
        });
        $("#tabCommerces").click(function () {
            searchCommerces('');
            $('[type="checkbox"].checkboxCommerces').attr('checked', false);
            $('#txtSearchCommerces').val('');
        });

        $('#btnLoadAgents').click(function () {
            if (validateChecks()) {
                $('#playAllIcon').removeClass('mdi-pause');
                $('#playAllIcon').addClass('mdi-play');
                $('#btnPlayAllStart').attr('play-pause', null);
                loadAgents('');
            }
            else {
                $('#ValidateChecked').modal('show');
            }
        });

        $('#btnLoadCommerces').click(function () {
            if (validateChecks()) {
                loadCommerces();
            }
            else {
                $('#ValidateCommerces').modal('show');
            }
        });

        $("#divList").on('click', '.btnCommerceView', function (e) {
            e.stopPropagation();
            var commerceid = $(this).attr('commerceid');

            locateCommerce(commerceid);
        });

        $("#divList").on('click', '.btnAgentView', function (e) {
            e.stopPropagation();
            var agentid = $(this).attr('agentid');

            locateAgent(agentid);
        });

        $('#txtSearchCommerces').keyup(function (event) {
            if (event.keyCode == '13') {
                searchCommerces($('#txtSearchCommerces').val());
            }
        });

        $('#txtSearchAgents').keyup(function (event) {
            if (event.keyCode == '13') {
                searchAgents($('#txtSearchAgents').val());
            }
        });

        $('#btnSearchAgents').click(function () {
            searchAgents($('#txtSearchAgents').val());
        });

        $('#btnSearchCommerces').click(function () {
            searchCommerces($('#txtSearchCommerces').val());
        });

        $('#gridContainer').on('click', '#checkboxAgentAll', function () {
            checkAllAgents($(this).is(':checked'));
        });

        $('#btnGenRecons').click(function () {
            var txtDate = $("#StartDateStr").val();
            if (txtDate == '') {
                $('#ValidateDate').modal('show');
            } else {
                dateRegex.test(txtDate)
                $('#ConfirmationPlayModal').modal('show');
            }
        });

        $('#btnPlayAllStart').click(function () {
            ResetControls();
            var playpause = $(this).attr('play-pause');
            var showAgenda = $('#btnAgengaDetail').attr('isclicked');
            var txtDate = $("#StartDateStr").val();

            var startDate = GetDates(true, new Date(txtDate.split('/')[1] + '/' + txtDate.split('/')[0] + '/' + txtDate.split('/')[2]), false);
            var endDate = GetDates(true, new Date(txtDate.split('/')[1] + '/' + txtDate.split('/')[0] + '/' + txtDate.split('/')[2]), true);

            if (txtDate == '') {
                $('#ValidateDate').modal('show');
            } else if (playingAll == 'Playing' && cardPlaying == 'PA' && playpause == 'null') {
                showloader();
                var userid = $('#CurrentUserId').val();
                $('#playAllIcon').removeClass('mdi-play');
                $('#playAllIcon').addClass('mdi-pause');
                $('#btnPlayAllStart').attr('play-pause', '1');
                var url = $("#mapFrame").attr("OriginalSrc") + userid + '|' + startDate + '|' + endDate + '|R|' + cardPlaying + '|true|' + showAgenda;
                $("#mapFrame").attr("src", url);
            } else {
                playpause = playpause == null ? 1 : playpause;
                $(this).attr('play-pause', playpause);
                ChangePlayAllBt(playpause);
                if (resetFragment == 'RS') {
                    returnResult(playpause, startDate, endDate, resetFragment, showAgenda, '');
                    resetFragment = '';
                } else {
                    returnResult(playpause, startDate, endDate, resetFragment, showAgenda, '');
                }
            }
        });

        $('#btnglobalplay').click(function () {
            showloader();
            var date = $('#StartDateStr').val();
            var playpause = $('#btnPlayAllStart').attr('play-pause');
            var userid = $('#CurrentUserId').val();
            var showAgenda = $('#btnAgengaDetail').attr('isclicked');
            playingAll = 'Playing';

            //$('#playAllIcon').removeClass('mdi-play');
            //$('#playAllIcon').addClass('mdi-pause');
            //$('#btnPlayAllStart').attr('play-pause', '1');
            $('#ConfirmationPlayModal').modal('hide');

            var sdate = GetDates(true, new Date(date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2]), false);
            var edate = GetDates(true, new Date(date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2]), true);
            cardPlaying = 'PA';
            loadAgents(sdate);

            //var url = $("#mapFrame").attr("OriginalSrc") + userid + '|' + sdate + '|' + edate + '|R|' + cardPlaying + '|false|' + showAgenda;
            //$("#mapFrame").attr("src", url);
        });

        $('#btnAgengaDetail').click(function () {
            var clicked = $(this).attr('isclicked');

            if (clicked === 'true') {
                $('#mdiagenda').removeClass('mdi-calendar-check');
                $('#mdiagenda').addClass('mdi-calendar-blank');
                $(this).attr('isclicked', 'false');
                clicked = "false";
            } else {
                $('#mdiagenda').addClass('mdi-calendar-check');
                $('#mdiagenda').removeClass('mdi-calendar-blank');
                $(this).attr('isclicked', 'true');
                clicked = "true";
            }

            if (cardPlaying == '' || cardPlaying == null || cardPlaying == undefined) {
                var date = $('#StartDateStr').val();
                var sdate = GetDates(true, new Date(date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2]), false);
                var edate = GetDates(true, new Date(date.split('/')[1] + '/' + date.split('/')[0] + '/' + date.split('/')[2]), true);

                returnResult(null, sdate, edate, null, clicked, '');
            }
        });
    }

    var validateChecks = function () {
        var checked = false;
        if ($("#tabAgents").hasClass("active")) {
            $(".checkboxAgent").each(function () {
                if ($(this).is(':checked')) checked = true;
            });
        }
        else {
            $(".checkboxCommerces").each(function () {
                if ($(this).is(':checked')) checked = true;
            });
        }
        return checked;
    };

    var ValidateDate = function () {
        var txtDate = $("#StartDateStr").val();
        if (txtDate == '' || dateRegex.test(txtDate)) {
            $('#ValidateDate').modal('show');
        } else {
            $('#ConfirmationPlayModal').modal('show');
        }
    }

    var initRecControls = function () {
        $('.btnPlayPause').click(function () {
            playingAll = '';
            var id = $(this).attr('id');
            var playpause = $(this).attr('play-pause');
            var startDate = null;
            var endDate = null;
            var userid = $(this).attr('userid');
            var flag = false;
            var showAgenda = $('#btnAgengaDetail').attr('isclicked');
            var track = $(this).attr('track');
            var iscontrol = $(this).attr('iscontrol');
            var addClass = $(this).attr('addClass');
            var restartF = cardPlaying;
            iscontrol = iscontrol ? iscontrol : false;

            cardPlaying = id;

            $('#playAllIcon').removeClass('mdi-pause');
            $('#playAllIcon').addClass('mdi-play');
            $('#btnPlayAllStart').attr('play-pause', null);

            if (fragmentPlaying == 0 || fragmentPlaying != id) {
                showloader();
                startDate = $('#' + id + '_StartDate').val();
                endDate = $('#' + id + '_EndDate').val();
                fragmentPlaying = id;
                ResetControls();
                $('#card_' + id).addClass('playingShadowCard');
                $('#card_' + id).removeClass('currentshadow');
                flag = true;
            }

            if (playpause == 0) {
                if (iscontrol == 'true' && addClass == 'true') {
                    $(this).removeClass('mdi-play-circle');
                    $(this).addClass('mdi-pause-circle');
                }
                $(this).attr('play-pause', '1');
            } else if (playpause == 1) {
                if (iscontrol == 'true' && addClass == 'true') {
                    $(this).addClass('mdi-play-circle');
                    $(this).removeClass('mdi-pause-circle');
                }
                $(this).attr('play-pause', '0');
            }

            if (track == null || track == '' || track == undefined) {
                track = restartF == 'RP' ? 'RP' : 'R';
            }

            if (flag) {
                setTimeout(function () { returnResult('', '', '', '', '', 'True'), 500 });
                var Url = $('#mapFrame').attr('OriginalSrc') + userid + '|' + startDate + '|' + endDate + '|' + track + '|' + cardPlaying + '|' + iscontrol + '|' + showAgenda;
                $("#mapFrame").attr("src", Url);
            } else {
                if (resetFragment == 'RS') {
                    $('#card_' + id).addClass('playingShadowCard');
                    $('#card_' + id).removeClass('currentshadow');
                    returnResult(playpause, startDate, endDate, resetFragment, showAgenda, '');
                    resetFragment = '';
                } else {
                    returnResult(playpause, startDate, endDate, resetFragment, showAgenda, '');
                }
            }
        });
    }

    var searchAgents = function (text) {
        $('#checkboxAgentAll').prop('checked', false);
        $(".checkAgent").each(function () {
            var name = $(this).attr('agentname');

            if (text == '') {
                if (typeof name !== 'undefined') {
                    $(this).removeClass('hide');
                    $(this).find('input[type="checkbox"]').removeClass('hide');
                }
            }
            else {
                if (typeof name !== 'undefined') {
                    if (name.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                        $(this).removeClass('hide');
                        $(this).find('input[type="checkbox"]').removeClass('hide');
                    } else {
                        $(this).addClass('hide');
                        $(this).find('input[type="checkbox"]').addClass('hide');
                    }
                }
            }
        });
    }

    var searchCommerces = function (text) {
        $(".checkComm").each(function () {
            var name = $(this).attr('commerceName');

            if (text == '' || text == undefined) {
                if (typeof name !== 'undefined') {
                    $(this).removeClass('hide');
                    $(this).find('input[type="checkbox"]').removeClass('hide');
                }
            }
            else {
                if (typeof name !== 'undefined') {

                    if (name.toUpperCase().indexOf(text.toUpperCase()) >= 0) {
                        $(this).removeClass('hide');
                    } else {
                        $(this).addClass('hide');
                    }
                }
            }
        });
    }

    var loadChkCommerceName = function () {
        var commerceName = 0;
        var listCommerce = $('#hchListCommerce').val();

        if ($('#chkNameCommerce').prop('checked')) {
            commerceName = 1;
        }
        var Url = $('#mapFrame').attr('OriginalSrc') + listCommerce + '|0|0|C|0|' + commerceName;
        $('#mapFrame').attr('src', Url);

    }

    var loadChkAgentName = function () {
        var agentName = 0;
        var agentids = $('#hchListAgents').val();

        if ($('#chkNameAgent').prop('checked')) {
            agentName = 1;
        }
        var Url = $('#mapFrame').attr('OriginalSrc') + agentids + '|0|0|A|0|' + agentName;
        $('#mapFrame').attr('src', Url);

    }

    var leftMenuToggle = function () {
        $("#menu-left").toggleClass("menu-hide");
        $("#map").toggleClass("map-menu-left-hide");
        $("#menu-left-toggle").toggleClass("map-menu-left-hide");
    }

    var loadCommerces = function () {
        var objects = [];
        showloader();
        $('.checkboxCommerces').each(function () {
            if ($(this).is(':checked')) {
                objects.push($(this).attr('id'));
            }
        });

        var Url = $('#mapFrame').attr('OriginalSrc') + objects.join() + '|0|0|C|0';
        $('#mapFrame').attr('src', Url);

        $.ajax({
            url: '/ManagementMap/CommercesDetails',
            type: "GET",
            data: { ObjectIds: objects.join() }
        }).done(function (result) {
            $("#divList").html(result);
            hideloader();

            $('#btnGenRecons').attr('disabled', true);
            $('#btnPlayAllStart').attr('disabled', true);
            $('#btnAgengaDetail').attr('disabled', true);
            $('#StartDateStr').attr('disabled', true);
        });
    }

    var loadAgents = function (newday) {
        var agents = [];
        showloader();
        $('.checkboxAgent').each(function () {
            if ($(this).is(':checked')) {
                agents.push($(this).attr('id'));
            }
        });

        var date = newday == undefined || newday == '' ? new Date() : new Date(newday);
        var startdate = GetDates(true, date, false);

        $.ajax({
            url: '/ManagementMap/AgentDetails',
            type: "GET",
            data: { AgentIds: agents.join(), StartDate: startdate }
        }).done(function (result) {
            //Set fragment list
            $("#divList").html(result);

            //Load Map
            setTimeout(function () {
                var Url = $('#mapFrame').attr('OriginalSrc') + agents.join() + '|0|0|A|0';
                $('#mapFrame').attr('src', Url);
            }, 1500);

            hideloader();

            var val = true;
            if (agents.length == 1 && result.indexOf('No existe información para mostrar') > 0) {
                val = false;
                $('#btnGenRecons').attr('disabled', true);
                $('#btnPlayAllStart').attr('disabled', true);
                $('#btnAgengaDetail').attr('disabled', true);
                $('#StartDateStr').attr('disabled', true);
            }
            else if (agents.length == 1 && result.indexOf('No existe información para mostrar') == -1) {
                val = false;
                $('#btnGenRecons').attr('disabled', false);
                $('#btnPlayAllStart').attr('disabled', false);
                $('#btnAgengaDetail').attr('disabled', false);
                $('#StartDateStr').attr('disabled', false);
            } else {
                val = true;
                $('#btnGenRecons').attr('disabled', true);
                $('#btnPlayAllStart').attr('disabled', true);
                $('#btnAgengaDetail').attr('disabled', true);
                $('#StartDateStr').attr('disabled', true);
            }
            if (!val) $('#CurrentUserId').val(agents);

            enableDate(val, startdate);
        });
    }

    var locateCommerce = function (commerceid) {
        hideAccordion();
        var CommerceName = 0;
        $('#accordion-' + commerceid).collapse('show');

        if ($('#chkNameCommerce').prop('checked')) {
            CommerceName = 1;
        }
        var Url = $('#mapFrame').attr('OriginalSrc') + commerceid + '|0|0|C|0|' + CommerceName;
        $('#mapFrame').attr('src', Url);
    }

    var locateAgent = function (agentid) {
        hideAccordion();
        var agentName = 0;
        $('#accordion-' + agentid).collapse('show');
        if ($('#chkNameAgent').prop('checked')) {
            agentName = 1;
        }
        var Url = $('#mapFrame').attr('OriginalSrc') + agentid + '|0|0|A|0|' + agentName;
        $('#mapFrame').attr('src', Url);
    }

    var checkAllAgents = function (ischecked) {
        $('.checkboxAgent').each(function () {
            if (!$(this).hasClass('hide')) {
                $(this).prop('checked', ischecked);
            }
        });
    }

    var receiveMessage = function (event) {
        hideloader();
        try {
            var obj = jQuery.parseJSON(event.data);

            if (obj.length == undefined) {
                var userid = obj.UserId;
                var commerce = obj.CommerceName == null ? '' : obj.CommerceName;
                var batteryLevel = obj.BatteryLevel == null || obj.BatteryLevel == undefined || obj.BatteryLevel == '' ? 'NA' : obj.BatteryLevel;

                if (obj.IsFinish && cardPlaying == obj.ReconstructionId) {
                    ResetControls();
                    resetFragment = 'RS';
                    cardPlaying = 'RP';
                    fragmentPlaying = 0;
                }

                var classColor = assingCss(obj.battery_percentage);
                var classBattery = assingClass(obj.battery_percentage);
                var useridE = $('#CurrentUserId').val();

                if (obj.CommerceName == undefined) {
                    $('#divList').find('#commerce-' + userid).html("NA");
                } else {
                    $('#divList').find('#commerce-' + userid).html(" " + obj.CommerceName);
                }

                $('#divList').find('#address-' + userid).html(" " + obj.Address);
                $('#divList').find('#event-' + userid).html(" " + obj.EventName);
                $('#divList').find('#lapse-' + userid).html(" " + obj.Lapse == 'null' || obj.Lapse == null ? '00:00:00' : obj.Lapse);
                $('#divList').find('#battery-' + userid).html(" " + batteryLevel + "%");
                $('#divList').find('#batteryclass-' + userid).removeClass();
                $('#divList').find('#batteryclass-' + userid).addClass(classBattery);
                $('#divList').find('#color-' + userid).css("color", classColor);
                $('#divList').find("#dateTime-" + userid).html(obj.Date + " " + obj.Time);
            } else {
                for (var i = 0; i < obj.length; i++) {
                    var userid = obj[i].UserId;
                    var commerce = obj[i].CommerceName == null ? '' : obj[i].CommerceName;
                    var classBattery = assingClass(obj[i].BatteryLevel);
                    var batteryLevel = obj[i].BatteryLevel == null || obj[i].BatteryLevel == undefined || obj[i].BatteryLevel == '' ? 'NA' : obj[i].BatteryLevel;
                    var classColor = assingCss(batteryLevel);


                    if (obj[i].CommerceName == undefined) {
                        $('#divList').find('#commerce-' + userid).html("NA");
                    } else {
                        $('#divList').find('#commerce-' + userid).html(" " + obj[i].CommerceName);
                    }
                    $('#divList').find('#address-' + userid).html(" " + obj[i].Address);
                    $('#divList').find('#event-' + userid).html(" " + obj[i].EventName); 
                    $('#divList').find('#lapse-' + userid).html(" " + obj[i].Lapse == 'null' || obj.Lapse == null ? '00:00:00' : obj[i].Lapse);
                    $('#divList').find('#battery-' + userid).html(" " + batteryLevel + "%");
                    $('#divList').find('#batteryclass-' + userid).removeClass();
                    $('#divList').find('#batteryclass-' + userid).addClass(classBattery);
                    $('#divList').find('#color-' + userid).css("color", classColor);
                    $('#divList').find("#dateTime-" + userid).html(obj[i].Date + " " + obj[i].Time);
                }
            }
        }
        catch (e) {
            var useridE = $('#CurrentUserId').val();
            $('#divList').find('#address-' + useridE).html("NA");
            $('#divList').find('#commerce-' + useridE).html("NA");
            $('#divList').find('#event-' + useridE).html("NA");
            $('#divList').find('#lapse-' + useridE).html("NA");
            $('#divList').find('#battery-' + useridE).html("NA");
            $('#divList').find("#dateTime-" + useridE).html("NA");
        }
    }

    var assingCss = function (percentage) {
        var batteryColor = "";
        if (percentage <= 10) {
            batteryColor = "red";
        } else if (percentage > 10 && percentage < 40) {
            batteryColor = "orange";
        } else if (percentage > 80) {
            batteryColor = "green";
        } else {
            batteryColor = "blue";
        }
        return batteryColor;
    }

    var assingClass = function (percentage) {
        var battery = "";
        if (percentage <= 10) {
            battery = "mdi mdi-battery-alert";
        } else if (percentage > 10 && percentage < 40) {
            battery = "mdi mdi-battery-30";
        } else if (percentage > 80) {
            battery = "mdi mdi-battery-90";
        } else {
            battery = "mdi mdi-battery-60";
        }
        return battery;
    }

    var hideAccordion = function () {
        $('.accordin-right').each(function () {
            $(this).collapse('hide');
        });
    }

    var enableDate = function (val, startdate) {
        if (!val) {
            $('#datetimepickerStartDate').datepicker('remove');
            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#StartDateStr').val(startdate.split('/')[1] + '/' + startdate.split('/')[0] + '/' + startdate.split('/')[2]);
            $('#datetimepickerStartDate').datepicker('update');
            initRecControls();

        } else {
            $('#datetimepickerStartDate').datepicker('remove');
            $('#datetimepickerStartDate').datepicker('update');
        }
    }

    var GetDates = function (mid, date, plus) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        var hour = '00';
        var min = '00';
        var sec = '00';

        if (!mid) {
            hour = date.getHours();
            min = date.getMinutes();
            sec = date.getSeconds();
            if (hour < 10) { hour = '0' + hour }
            if (min < 10) { min = '0' + min }
            if (sec < 10) { sec = '0' + sec }
        }

        if (plus) {
            var lastDay = new Date(yyyy, mm, 0).toString().split(' ')[2];
            if (lastDay == dd) {
                mm = mm + 1;
                dd = "1";
            } else {
                dd = dd + 1;
            }
        }

        if (parseInt(dd) < 10) { dd = '0' + dd }
        if (parseInt(mm) < 10) { mm = '0' + mm }

        date = mm + '/' + dd + '/' + yyyy + ' ' + hour + ':' + min + ':' + sec;
        return date;
    }

    var returnResult = function (playpause, startdate, enddate, resetFragment, showAgenda, finishTrack) {
        var result = playpause + '|' + startdate + '|' + enddate + '|' + resetFragment + '|' + showAgenda + '|' + finishTrack;
        var targetframe = $('#mapFrame')[0].contentWindow;
        targetframe.postMessage(result, '*');
    }

    var ResetControls = function () {
        $('.mdi-pause-circle').each(function () {
            $(this).addClass('mdi-play-circle');
            $(this).removeClass('mdi-pause-circle');
            $(this).attr('play-pause', '0');
        });
        $('.playingShadowCard').each(function () {
            $(this).removeClass('playingShadowCard');
            $(this).addClass('currentshadow');
        });
    };

    var ChangePlayAllBt = function (playpause) {
        if (playpause == 0) {
            $('#playAllIcon').removeClass('mdi-play');
            $('#playAllIcon').addClass('mdi-pause');
            $('#btnPlayAllStart').attr('play-pause', '1');
        } else if (playpause == 1) {
            $('#playAllIcon').addClass('mdi-play');
            $('#playAllIcon').removeClass('mdi-pause');
            $('#btnPlayAllStart').attr('play-pause', '0');
        }
    };

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    return {
        Init: initialize,
        ReceiveMessage: receiveMessage,
        LoadChkAgentName: loadChkAgentName,
        LoadChkCommerceName: loadChkCommerceName
    };
})();
