﻿var ECO_Management = ECO_Management || {};

ECO_Management.Agenda = (function () {

    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        $('#ddxCommerceId').select2();
        $('#ddxUserId').select2();

        $('#datetimepickerDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) {
            onRetrieveAgenda();
        });

        $('#btnImport').click(function () {
            showLoader();
            $('#ImportModal').modal('hide');
        });

        $('#ImportModal').on('show.bs.modal', function (e) {
            $('#chkCreateCommerce').prop('checked', 'true');
        });
    };

    var saveDynamicColumns = function (idagenda) {
        var arr = [];
        $('#DynamicColumns_' + idagenda + ' tr .values_' + idagenda).each(function () {
            var AditionalColumns = {
                'Id': $(this).attr('id'),
                'Title': $(this).attr('title'),
                'Value': $(this).val(),
                'AgendaId': idagenda
            };
            arr.push(AditionalColumns);
        });

        showLoader();
        //Send data to backend 
        $.ajax({
            url: '/Agenda/DynamicColumnsValuesAddOrEdit',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ list : arr }),
            success: function (partialViewResult) {
                $("#gridContainer").html(partialViewResult);
                hideLoader();
            }
        });
    }

    var additionalGridEdit = function () {
        $('#detailContainer').show('slow');
        initialize();
    };

    var onClickImport2 = function () {
        $('#ImportCSV').trigger('click');
    };

    var getFilename = function (input) {
        if (ValidateFormat(input)) {
            if (input.files && input.files[0]) {
                var value = $("#ImportCSV").val();
                var NameOfFile = input.files[0].name;
                $("#txtFile").val(NameOfFile);
                setErrorFormatMsj('', false);
            }
        }
        else {
            $("#txtFile").val("");
            setErrorFormatMsj('* El formato de archivo es inválido, favor seleccionar un archivo de formato CSV.', true);
        }
    }

    var setErrorFormatMsj = function (msj, show) {
        if (show) {
            $('#errorFormatMsj').html(msj);
            $('#validationFormatErrors').removeClass('hide');
        } else {
            $('#errorFormatMsj').html('');
            $('#validationFormatErrors').addClass('hide');
        }
    }

    var ValidateFormat = function (input) {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var files = input.files;
            for (var i = 0, f; f = files[i]; i++) {
                if (!f.name.match('\.csv')) {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    };

    var showresultmodal = function (resultdata) {
        if (resultdata.split('|')[3] == "") {
            $('#resultimportdate').html(resultdata.split('|')[0]);
            $('#resultsaved').html(resultdata.split('|')[1]);
            $('#resultnotsaved').html(resultdata.split('|')[2]);
            $('#resultduplicate').html(resultdata.split('|')[4]);
            $('#SummaryResultModal').modal('show');
        }
        else {
            $('#mesajeresult').html(resultdata.split('|')[3]);
            $('#ErrorResultModal').modal('show');
        }
    }

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
        $('#detailContainer').show('slow');
    };

    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
    };

    var onAddItem = function () {
        showLoader();
        var userId = $("#ddxUserId").val();
        var commerceId = $("#ddxCommerceId").val();
        var date = $('#datetimepickerDate').datepicker('getDate');

        if ((date == "Invalid Date") || (userId == "") || (commerceId == "")) {
            $('#AgendaRequireMessage').show('slow');
            hideLoader();
        }
        else {
            $('#AgendaRequireMessage').hide('slow');
            $.ajax({
                url: '/Agenda/AddItemAgenda',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify({ CommerceId: commerceId, UserId: userId, Date: date }),
                success: function (partialViewResult) {
                    hideLoader();
                    if (partialViewResult === 'AlreadyId') {
                        $('#ValidateModal').find('#validatemessage').html('El comercio existe en la Agenda');
                        $('#ValidateModal').modal('show');
                    } else {
                        $("#gridContainer").html(partialViewResult);
                    }
                    return false;
                }
            });
        }
    };

    var onAddEditAdditional = function () {
        showLoader();
        var commerceId = $("#ddxCommerceId").val();
        var Valor = $('#txtValor').val();

        $.ajax({
            url: '/Agenda/EditAdditionalColumns',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ valor: Valor }),
            success: function (partialViewResult) {
                $("#detailContainer").html(partialViewResult);
                onRetrieveAgenda();
                hideLoader();
                return false;
            }
        });
    };

    var onDeleteItem = function (idAgenda) {        
        showLoader();
        $.ajax({
            url: '/Agenda/DeleteItemAgenda',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ IdAgenda: idAgenda }),
            success: function (partialViewResult) {
                $("#gridContainer").html(partialViewResult);
                hideLoader();
                onRetrieveAgenda();
                return false;
            }
        });
    };

    var onRetrieveAgenda = function () {
        showLoader();
        var userId = $("#ddxUserId").val();
        var date = $('#datetimepickerDate').datepicker('getDate');

        if ((date == "Invalid Date") || (userId == "")) {
            hideLoader();
        }
        else {
            $.ajax({
                url: '/Agenda/RetrieveAgenda',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify({ UserId: userId, DateOfAgenda: date }),
                success: function (partialViewResult) {
                    $("#gridContainer").html(partialViewResult);
                    hideLoader();
                    return false;
                }
            });
        }
    };

    function onDownLoadReport() {
        var userId = $("#ddxUserId").val();
        var date = $('#datetimepickerDate').datepicker('getDate');

        try {
            if ((date != "Invalid Date") || (userId != "")) {
                showLoader();
                $.ajax({
                    url: '/Agenda/GetReportData',
                    type: 'POST',
                    contentType: "application/json",
                    data: JSON.stringify({ UserId: userId, DateOfCheck: date }),
                    success: function (result) {
                        hideLoader();
                        if (result === 'NoData') {
                            $('#ValidateModal').find('#validatemessage').html('No existen datos para la descarga del reporte');
                            $('#ValidateModal').modal('show');
                        } else {
                            $('#DonwloadFileForm').submit();
                        }                        
                        return false;
                    }
                });
            }

        } catch (e) {
            alert(e.message);
        }
    }

    var showDynamicColumns = function () {
        $('#dynamicColumnsContainer').show('slow');
    };

    var hideDynamicColumns = function () {
        $('#dynamicColumnsContainer').hide('slow');
    };

    var onDeleteColum = function (id) {
        showloader();
        $.ajax({
            url: '/Agenda/OnDeleteColumn',
            type: 'POST',
            contentType: 'application/json;',
            data: JSON.stringify({ id: id }),
            success: function (partialViewResult) {
                hideloader();
                $('#gridContainerDynamic').html(partialViewResult);
            }
        });
    }

    var onAddNewColum = function () {
        var columname = $('#txtColumnaID').val();

        if (columname != "") {
            showloader();
            $.ajax({
                url: '/Agenda/OnAddNewColumn',
                type: 'POST',
                contentType: 'application/json;',
                data: JSON.stringify({ ColumName: columname }),
                success: function (partialViewResult) {
                    hideloader();
                    $('#gridContainerDynamic').html(partialViewResult);
                    $('#txtColumnaID').val('');
                    return false;
                }
            });
        }
    }

    var onSaveColum = function (ColumnName) {
        //showloader();
        $.ajax({
            url: '/Agenda/SaveColumns',
            type: 'POST',
            contentType: 'application/json;',
            success: function (data) {
                if (data == 'success') {
                    hideloader();
                    ECOsystem.Utilities.setConfirmation();
                }
            }
        });
    }

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnAddItem: onAddItem,
        OnDeleteItem: onDeleteItem,
        OnRetrieveAgenda: onRetrieveAgenda,
        OnDownLoadReport: onDownLoadReport,
        OnClickImport2: onClickImport2,
        GetFilename: getFilename,
        Showresultmodal: showresultmodal,
        AdditionalGridEdit: additionalGridEdit,
        OnAddEditAdditional: onAddEditAdditional,
        SaveDynamicColumns: saveDynamicColumns,
        ShowDynamicColumns: showDynamicColumns,
        HideDynamicColumns: hideDynamicColumns,
        OnAddNewColum: onAddNewColum,
        OnDeleteColum: onDeleteColum,
        OnSaveColum: onSaveColum
    };
})();

