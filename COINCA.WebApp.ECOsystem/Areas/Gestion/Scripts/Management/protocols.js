﻿var ECO_Management = ECO_Management || {};

ECO_Management.Protocols = (function () {

    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
 
        //Edit for the vehicle grid
        $('#gridContainer').off('click.edit', 'a[edit_row_protocols]').on('click.edit', 'a[edit_row_protocols]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();
            $('#addOrEditModal').modal('show');        
        });

        $('#btnClose').off('click.btnClose').on('click.btnClose', function (e) {
            $('#detailContainer').hide('slow');
        });

        $('#btnAdd').off('click.btnAddNew').on('click.btnAddNew', function (e) {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });      
    };
      
    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
        $('#detailContainer').show('slow');
    };
   
    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
    };

    var successSearch = function () {
        $('#txtSearch').val('');
    };

    var hideAddOrEdit = function () {
        $('#detailContainer').hide('slow');
        $('#ConfirmationModal').modal('show');
    }

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        SuccessSearch: successSearch,
        HideAddOrEdit: hideAddOrEdit
    };
})();

