﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem;
using ECOsystem.Models.Account;
using  ECOsystem.Areas.Admin.Models.Identity;
using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Business.Administration;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Administration Controller Class
    /// </summary>
    public class AdministrationController : Controller
    {
        /// <summary> 
        /// Index
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                int vehiclesModel;
                int driversActive;
                int drivers;
                int CCRequest;
                int CCActive;

                using (var bus = new CreditCardBusiness())
                {
                    CCRequest = bus.RetrieveCardRequest(null, null).Count();
                    CCActive = bus.RetrieveCreditCard(null).Where(m => m.StatusId == 7).Count();
                }

                using (var bus = new VehiclesBusiness())
                {
                    vehiclesModel = bus.RetrieveVehicles(null).Where(m => m.IntrackReference != null).Count();
                }

                using (var bus = new DriversBusiness())
                {
                    var DModel = bus.RetrieveDriversActive();
                    driversActive = DModel.Where(m => m.Active == true).Count();
                    drivers = DModel.Count();
                }

                var model = new AdministrationModel()
                {
                    Menus = new List<AccountMenus>(),
                    Vehicles = Business.Utilities.GeneralCollections.GetVehicles.Count(),
                    Drivers = drivers,
                    VehiclesAVL = vehiclesModel,
                    DriverActive = driversActive,
                    CreditCards = CCActive + CCRequest,
                    CCardsActive = CCActive,
                    CCardsRequest = CCRequest
                };

                return View(model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}