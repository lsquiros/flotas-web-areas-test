﻿/************************************************************************************************************
*  File    : AlarmController.cs
*  Summary : Alarm Controller Actions
*  Author  : Cristian Martínez 
*  Date    : 17/Nov/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ECOsystem.Audit;
using  ECOsystem.Areas.Admin.Business.Administration;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using  ECOsystem.Areas.Admin.Models.Identity;
using  ECOsystem.Areas.Admin.Models;
using  ECOsystem.Areas.Admin.Models.Administration;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Customizer Controller
    /// </summary>
    public class CustomizerController : Controller
    {
        /// <summary>
        /// Main Customization View
        /// </summary>
        /// <returns></returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            IEnumerable<DashboardModule> dasboadModules;
            using (DashboardModulesBusiness dashboardModulesBusiness = new DashboardModulesBusiness())
            {
                dasboadModules = dashboardModulesBusiness.RetrieveDashboarModules((int)ECOsystem.Utilities.Session.GetCustomerId());
            }

            var model = new DashboardModuleBase 
            { 
                Data = new DashboardModule(),
                List = dasboadModules,
                Menus = new List<AccountMenus>()
            };

            return View(model);
        }

        [HttpGet]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public ActionResult FuelCurrency()
        {
            FuelCurrencyModule model = new FuelCurrencyModule();
            using (Business.Administration.FuelCurrencyModulesBusiness business = new Business.Administration.FuelCurrencyModulesBusiness())
            {
                model = business.Retrieve((int)ECOsystem.Utilities.Session.GetCustomerId());
                model.Menus = new List<AccountMenus>();
            }
            return View(model);
        }

        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Save(FuelCurrencyModule model)
        {
            try
            {
                using (FuelCurrencyModulesBusiness business = new FuelCurrencyModulesBusiness())
                {
                    model.CustomerId = (int)ECOsystem.Utilities.Session.GetCustomerId();
                    business.AddOrEdit(model);
                }
            }
            catch(Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
            }

            return RedirectToAction("FuelCurrency");
        }



        /// <summary>
        /// Save Dashboard Module
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult SaveDashboardModule(int customerId, int dashboardModuleId, int order, string rowVersion, int loggedUserId)
        {
            try
            {
                using (DashboardModulesBusiness dashboardModulesBusiness = new DashboardModulesBusiness())
                {
                    DashboardModule dashboardModule = new DashboardModule() { CustomerId = customerId, DashboardModuleId = dashboardModuleId, Order = order, LoggedUserId = loggedUserId, RowVersion = Convert.FromBase64String(rowVersion) };
                    dashboardModulesBusiness.AddOrEditDashboardModule(dashboardModule);
                }
                return Json(customerId, JsonRequestBehavior.DenyGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(0, JsonRequestBehavior.DenyGet);
            }
            
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}