﻿/************************************************************************************************************
*  File    : CreditCardsReportController.cs
*  Summary : CreditCards Report Controller Actions
*  Author  : Danilo Hidalgo
*  Date    : 01/20/2015
*  Mod. By : Gerald Solano
*  Mod.Date: 03/30/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Business.Administration;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using ECOsystem.Business.Utilities;
using System;
using  ECOsystem.Areas.Admin.Models.Identity;
using System.Data;
using ECOsystem.Models.Core;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Applicants Report Class
    /// </summary>
    public class CreditCardsReportController : Controller
    {
       /// <summary>
       /// Renderizado inicial del Módulo de Reporte de Tarjetas
       /// </summary>
       /// <returns></returns>
       /// Modificado por: Gerald Solano
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new CreditCardsReportBusiness())
            {
                return View(business.RetrieveCreditCardsReport(new AdministrationReportsBase()));
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(CreditCardsReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new CreditCardsReportBusiness())
                {
                    // If get all customers
                    if (model.Parameters.CustomerId == -1) {
                        model.Parameters.PartnerId = ECOsystem.Utilities.Session.GetPartnerId();
                    }

                    return View(business.RetrieveCreditCardsReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return RedirectToAction("Index");
            }
            
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<AdministrationReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));
                if (model != null)
                {
                    model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                    model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
                }
                //byte[] report;
                DataTable report = new DataTable();

                using (var business = new CreditCardsReportBusiness())
                {
                    report = business.GenerateCreditCardsReportExcel(model);
                    Session["Reporte"] = report;

                    return View("Index", business.RetrieveCreditCardsReport(model));
                }
                
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return null;
            }
            
        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        public ActionResult PrintReport(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<AdministrationReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));

                ViewBag.PrintView = true;
                using (var business = new CreditCardsReportBusiness())
                {
                    return View("Index", business.RetrieveCreditCardsReport(model));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
               
                return View("Index");
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            DataTable report = new DataTable();

            if (Session["Reporte"] != null)
            {
                report = (DataTable)Session["Reporte"];
                Session["Reporte"] = null;
            }

            Session["nombreReporte"] = "CreditCardsReportByClient";
            Session["datosReporte"] = report;

            if (report.Rows.Count > 0)
            {
                return Redirect("~/ReportsData/reports.aspx");
            }

            return RedirectToAction("Index", new { message = "No hay datos que cumplan con el criterio de búsqueda" });
        }
    }
}
