﻿using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Web.Mvc;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Controller to save the Parameters in the database 
    /// </summary>
    public class CustomerTransacitiosSAPReportController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        //[EcoAuthorize]
        public ActionResult Index()
        {
            using(var bus = new TransactionSapReportBusiness())
            {
                var model = new TransactionsConsolideReportBase()
                {
                    Data = bus.SAPParametersRetrieve(),
                    Menus = new List<AccountMenus>(),
                    List = new List<TransactionsConsolideReport>(),
                    ExportableTranParameters = bus.ExportableTransactionFileParamRetrieve()
                };

                return View(model);
            }            
        }

        /// <summary>
        /// AddOrEditSAPParameters
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[EcoAuthorize]
        public ActionResult AddOrEditSAPParameters(TransactionsConsolideReportBase model)
        {
            try
            {
                using( var bus = new TransactionSapReportBusiness())
                {
                    bus.SAPParametersEdit(model.Data);
                    bus.ExportableTranFileParametersEdit(model.ExportableTranParameters);

                    var modelIndex = new TransactionsConsolideReportBase()
                    {
                        Data = bus.SAPParametersRetrieve(),
                        Menus = new List<AccountMenus>(),
                        List = new List<TransactionsConsolideReport>(),
                        ExportableTranParameters = bus.ExportableTransactionFileParamRetrieve()
                    };

                    return View("Index", modelIndex);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                throw;
            }
            
        }
    }
}