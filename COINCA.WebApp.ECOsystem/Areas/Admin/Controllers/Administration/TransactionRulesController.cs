﻿/************************************************************************************************************
*  File    : TransactionRulesController.cs
*  Summary : TransactionRules Controller Actions
*  Author  : Henry Retana
*  Date    : 09/05/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class TransactionRulesController : Controller
    {

        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    var model = new TransactionRulesModelBase()
                    {
                        Data = new TransactionRulesModel(),
                        List = business.RetrieveTransactionRules(ECOsystem.Utilities.Session.GetCustomerId(), null, null).ToList(),
                        Menus = new List<AccountMenus>()
                    };

                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                var model = new TransactionRulesModelBase()
                {
                    Data = new TransactionRulesModel(),
                    List = new List<TransactionRulesModel>(),
                    Menus = new List<AccountMenus>()
                };
                return View(model);
            }
        }

        /// <summary>
        /// Retrieve the transactionRule Binnacle
        /// </summary>
        /// <param name="RuleId"></param>
        /// <returns></returns>
        //[HttpPost]
        public ActionResult TransactionBinnacleRetrieve(int RuleId, int? VehicleId, int? CostCenterId)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    return PartialView("~/Views/TransactionRules/_partials/_Binnacle.cshtml", business.BinnacleRetrieve(RuleId, VehicleId, CostCenterId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "TransactionRules")
                });
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public ActionResult AddOrEditTransactionRulesPartial(bool IsVehicle, int? AddVehicleId, int AddCostCenterId)
        {
            try
            {
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                List<TransactionRulesModel> rules = json_serializer.Deserialize<List<TransactionRulesModel>>(Request["rules"]);
                var userId = ECOsystem.Utilities.Session.GetUserInfo();
                using (var business = new CreditCardBusiness())
                {
                    foreach (var rule in rules)
                    {
                        rule.RuleCustomerId = (rule.RuleCustomerId == 0) ? null : rule.RuleCustomerId;
                        rule.UserId = (userId != null) ? (int)userId.UserId : 0;
                        business.AddOrEditTransactionRule(rule);

                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificaciones en las reglas de negocio: {0}", new JavaScriptSerializer().Serialize(rule)));                        
                    }
                    var model = business.RetrieveTransactionRules(ECOsystem.Utilities.Session.GetCustomerId(), AddVehicleId, AddCostCenterId);
                    if (IsVehicle) { model.FirstOrDefault().IsVehicles = true; }
                    else { model.FirstOrDefault().IsCostCenter = true; }
                    return PartialView("_partials/_DetailRules", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Vehicles")
                });
            }
        }

        [HttpPost]
        public ActionResult DeleteTransactionRules(bool IsVehicle, int? DeleteVehicleId, int? DeleteCostCenterId)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    //business.DeleteTransactionRules(DeleteVehicleId, DeleteCostCenterId);
                    if (IsVehicle)
                    {
                        DeleteVehicleId = null;
                    }
                    else
                    {
                        DeleteCostCenterId = null;
                    }
                    var model = business.RetrieveTransactionRules(ECOsystem.Utilities.Session.GetCustomerId(), DeleteVehicleId, DeleteCostCenterId);
                    if (IsVehicle)
                    {
                        model.FirstOrDefault().IsVehicles = true;
                    }
                    else
                    {
                        model.FirstOrDefault().IsCostCenter = true;
                    }
                    Session["IsRestore"] = true;
                    return PartialView("_partials/_DetailRules", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                });
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public ActionResult AddOrEditTransactionRules(List<TransactionRulesModel> rules)
        {
            try
            {
                var odometerRangeRule = rules.FirstOrDefault(r => r.RuleId == 10 && r.RuleActive == true);
                if (odometerRangeRule != null)
                {
                    var rangeValues = odometerRangeRule.RangeParameters.Value.Split('-');
                    int result;
                    if (!int.TryParse(rangeValues[0], out result) || !int.TryParse(rangeValues[1], out result))
                        throw new Exception("Invalid range parameters");
                }
                var userId = ECOsystem.Utilities.Session.GetUserInfo();
                using (var business = new CreditCardBusiness())
                {
                    foreach (var rule in rules)
                    {
                        rule.RuleCustomerId = (rule.RuleCustomerId == 0) ? null : rule.RuleCustomerId;
                        rule.UserId = (userId != null) ? (int)userId.UserId : 0;
                        business.AddOrEditTransactionRule(rule);

                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificaciones en las reglas de negocio: {0}", new JavaScriptSerializer().Serialize(rule)));
                    }
                }

                //save Range Parameters
                if (odometerRangeRule != null)
                {
                    ParametersTRAddOrEdit(odometerRangeRule.RangeParameters.Value, odometerRangeRule.RangeParameters.TRId);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetParametersData()
        {
            using (var business = new CreditCardBusiness())
            {
                var model = business.TRParametersRetrieve(null);

                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        public void ParametersTRAddOrEdit(string Value, int TRId)
        {
            using (var business = new CreditCardBusiness())
            {
                business.AddOrEditTRParametersByCustomer(Value, TRId);
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificaciones en los Parámetros de reglas de negocio: Valor: {0}, TRId: {1}", Value, TRId));
            }
        }

        [HttpPost]
        public ActionResult ValidateTankCapacityRule()
        {
            using (var business = new CreditCardBusiness())
            {
                int result = business.ValidateTankCapacityRule();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DailyTransactionLimitAddOrEdit(DailyTransactionLimitAddOrEdit Model)
        {
            try
            {
                using (var bus = new CreditCardBusiness())
                {
                    bus.DailyTransactionLimitAddOrEdit(Model);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                throw;
            }
        }        
    }
}