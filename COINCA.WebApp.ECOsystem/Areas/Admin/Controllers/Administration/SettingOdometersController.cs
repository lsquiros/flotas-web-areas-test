﻿/************************************************************************************************************
*  File    : SettingOdometersController.cs
*  Summary : SettingOdometers Controller Actions
*  Author  : Stefano Quirós
*  Date    : 03/29/2016
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using System.Collections.Generic;
using  ECOsystem.Areas.Admin.Models.Identity;
using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Business.Administration;
using ECOsystem.Business.Utilities;
using Newtonsoft.Json;
using ECOsystem.Utilities;
using System.Data;
using System.Globalization;
using System.Linq;

namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class SettingOdometersController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index(bool? alert)
        {            
            return View(GetData(alert));
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(SettingOdometersBase model)
        {
            if (model.Parameters.ReportStatus == 100)
            {
                Session["Alert"] = false; 
            }
            if (model == null || model.Parameters == null) RedirectToAction("Index");
            ViewBag.OdometerType = model.Parameters.OdometerType;
            Session["Parameters"] = model.Parameters;
            using (var business = new SettingOdometersBusiness())
            {
                var modelR = business.RetrieveSettingOdometers(model.Parameters, (bool?)Session["Alert"]);
                Session["Data"] = modelR.List;
                Session["Asc"] = null;
                ViewBag.CheckAll = !modelR.List.Where(m => m.ShowAlert == false).Any();
                return View(modelR);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update Vehicle Fuel and Credit to all vehicle related to a specific group
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditOdometers()
        {
            try
            {
                var list = (List<SettingOdometers>)Session["Data"];
                var parameters = (ControlFuelsReportsBase)Session["Parameters"];

                ViewBag.IsResult = true;
                using (var business = new SettingOdometersBusiness())
                {   
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Modificación de Odómetros");

                    var modelR = business.AddOrEditOdometer(list.Where(x => x.Selected == true).ToList(), parameters);

                    Session["Data"] = modelR;
                    Session["Asc"] = null;
                    ViewBag.CheckAll = !modelR.Where(m => m.ShowAlert == false).Any();
                    return PartialView("_partials/_Grid", modelR);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "SettingOdometers")
                });
            }
        }
        
        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));
            if (model != null)
            {
                model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
            }
            if (model.Month != null && Convert.ToInt32(model.Month) > 0 && model.Year != null && Convert.ToInt32(model.Year) > 0)
            {
                if (model.StartDate == null)
                {
                    model.StartDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1, 0, 0, 0);
                }

                if (model.EndDate == null)
                {
                    model.EndDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month)), 23, 59, 59);
                }
            }

            DataTable report = new DataTable();
            using (var business = new SettingOdometersBusiness ())
            {
                report = business.GetReportDownloadData(model, (bool?)Session["Alert"]);

                if (model.StartDate == null)
                {
                    int lastDay = DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month));
                    string MonthName = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1).ToString("MMM", CultureInfo.InvariantCulture);
                    Session["StartDate"] = MonthName + 1 + " /" + model.Year;
                    Session["EndDate"] = MonthName + lastDay + " /" + model.Year;
                }
                else
                {
                    DateTime fecha_inicial = Convert.ToDateTime(model.StartDate);
                    int day_inicial = fecha_inicial.Day;
                    string MonthNameInicial = new DateTime(fecha_inicial.Year, fecha_inicial.Month, day_inicial).ToString("MMM", CultureInfo.InvariantCulture);
                    Session["StartDate"] = MonthNameInicial + day_inicial + " /" + fecha_inicial.Year;
                    DateTime fecha_final = Convert.ToDateTime(model.EndDate);
                    int day_final = fecha_final.Day;
                    string MonthNameFinal = new DateTime(fecha_final.Year, fecha_final.Month, day_final).ToString("MMM", CultureInfo.InvariantCulture);
                    Session["EndDate"] = MonthNameFinal + day_final + " /" + fecha_final.Year;
                }

                Session["Reporte"] = report;
                Session["OdometerType"] = model.OdometerType;

                return View("Index", business.RetrieveSettingOdometers(model, (bool?)Session["Alert"]));
            }
        }



        /// <summary>
        /// ExcelReportDownloadOdometer
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownloadOdometer(string p)
        {
            var model = JsonConvert.DeserializeObject<ControlFuelsReportsBase>(ECOsystem.Utilities.Miscellaneous.Base64Decode(p));
            if (model != null)
            {
                model.StartDate = (model.StartDate != null) ? Convert.ToDateTime(model.StartDate).AddDays(1) : model.StartDate;
                model.EndDate = (model.EndDate != null) ? Convert.ToDateTime(model.EndDate).AddDays(-1) : model.EndDate;
            }
            if (model.Month != null && Convert.ToInt32(model.Month) > 0 && model.Year != null && Convert.ToInt32(model.Year) > 0)
            {
                if (model.StartDate == null)
                {
                    model.StartDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), 1, 0, 0, 0);
                }

                if (model.EndDate == null)
                {
                    model.EndDate = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), DateTime.DaysInMonth(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month)), 23, 59, 59);
                }
            }

            DataTable report = new DataTable();
            using (var business = new SettingOdometersBusiness())
            {
                report = business.GetReportDownloadDataOdometerChanged(model);
                Session["type"] = 1;
                Session["Reporte"] = report;

                return View("Index", business.RetrieveSettingOdometers(model, (bool?)Session["Alert"]));
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                if(Session["Data"] == null)
                {
                    return null;
                }
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    if (Convert.ToInt16(Session["type"]) == 1) 
                    {
                        Session["type"] = 0;
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Reporte Cambios de Odómetros");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "OdometerChangedReport", this.ToString().Split('.')[2], "Reporte Cambios en Odómetros");
                    }
                    else
                    { 
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Reporte Ajuste de Odómetros (Historial)");
                        return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "SettingOdometerReport", this.ToString().Split('.')[2], "Reporte Ajuste de Odómetros");
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index", GetData((bool?)Session["Alert"]));
            }
        }

        /// <summary>
        /// Sort Grid
        /// </summary>
        [HttpGet]
        public ActionResult SortGrid(string Column)
        {              
            var model = (List<SettingOdometers>)Session["Data"];
            var SortList = (dynamic)null;
            if(Session["Asc"] == null)
            {
                SortList = model.OrderBy(c => c.GetType().GetProperty(Column).GetValue(c, null));                
                Session["Asc"] = 1;
            }
            else
            {
                SortList = model.OrderByDescending(c => c.GetType().GetProperty(Column).GetValue(c, null));
                Session["Asc"] = null;
            }            
            return PartialView("_partials/_Grid", SortList);
        }
       
        /// <summary>
        /// NO ShowAlerts
        /// </summary>
        /// <param name="TransactionIds"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult NoShowAlerts(List<string> TransactionIdsNoAlert, List<string> TransactionIdsShowAlert)
        {
            try
            {
                using (var bus = new SettingOdometersBusiness())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Modificación de alertas de Odómetros a mostrar");
                    bus.NoShowAlertAddOrEdit(TransactionIdsNoAlert, TransactionIdsShowAlert);
                }

                var list = (List<SettingOdometers>)Session["Data"];
                list.Where(x => TransactionIdsShowAlert.Contains(x.TransactionId.ToString())).ToList().ForEach(x => x.ShowAlert = true);

                Session["Data"] = list; 
                return Json("Success");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.InnerException);
                return Json("");
            }            
        }

        /// <summary>
        /// Get Data
        /// </summary>
        /// <returns></returns>
        public SettingOdometersBase GetData(bool? Alert)
        {
            Session["Alert"] = Alert;
            SettingOdometersBase model = new SettingOdometersBase();
            var currentDate = DateTimeOffset.Now;            
            model.Parameters.Year = currentDate.Year;
            model.Parameters.Month = currentDate.Month;            
            model.Parameters.OdometerType = 1;
            model.Parameters.ReportCriteriaId = 1000;
            Session["Parameters"] = model.Parameters;
            ViewBag.OdometerType = 1;
            using (var business = new SettingOdometersBusiness())
            {
                var modelR = business.RetrieveSettingOdometers(model.Parameters, Alert);
                Session["Data"] = modelR.List;
                Session["Asc"] = null;
                ViewBag.CheckAll = !modelR.List.Where(m => m.ShowAlert == false).Any();
                return modelR;
            }
        }

        public ActionResult GetChanges(int? TransactionId, double? Odometer, bool? IsLast, bool? IsBeforeLast)
        {
            try
            {
                var list = (List<SettingOdometers>)Session["Data"];
                var item = new SettingOdometers();

                if ((Odometer != null && Odometer != 0) && (TransactionId != null))
                {
                    item = list.Where(x => x.TransactionId == TransactionId).Select(x => x).FirstOrDefault();
                    
                    if (IsBeforeLast != null && (bool)IsBeforeLast)
                    {
                        //If there are no changes, return the grid 
                        if (item.OdometerBeforeLast == Odometer) return PartialView("_partials/_Grid", list);

                        list.Where(x => x.TransactionId == TransactionId).Select(x => x).ToList().ForEach(x => { x.OdometerBeforeLast = (double)Odometer; x.Selected = true; });
                    }
                    else
                    {
                        if (IsLast != null && (bool)IsLast)
                        {
                            //If there are no changes, return the grid 
                            if (item.OdometerLast == Odometer) return PartialView("_partials/_Grid", list);

                            item.Travel = Convert.ToInt32(item.Odometer - Odometer);
                            item.OdometerLast = (double)Odometer;
                        }
                        else
                        {
                            //If there are no changes, return the grid 
                            if (item.Odometer == Odometer) return PartialView("_partials/_Grid", list);

                            item.Travel = Convert.ToInt32(Odometer - item.OdometerLast);
                            item.Odometer = (double)Odometer;
                        }

                        item.Performance = item.Travel / item.Liters;
                        item.Selected = true;

                        list.Where(x => x.TransactionId == TransactionId).ToList().ForEach(x => x = item);
                    }
                }

                Session["Data"] = list;
                return Json(new {
                                  Success = "Success", 
                                  OdometerBeforeLast = item.OdometerBeforeLastStr,
                                  OdometerLast = item.OdometerLastStr,
                                  Odometer = item.OdometerStr, 
                                  Selected = item.Selected, 
                                  Travel = item.TravelStr, 
                                  Performance = item.Performance, 
                                  TransactionId = item.TransactionId 
                               }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
