﻿using  ECOsystem.Areas.Admin.Models.Administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class ThirdPartyManagementController : Controller
    {
        // GET: ThirdPartyManagement
        public ActionResult Index()
        {
            return View();
        }
        
        public JsonResult DriversCellsPhoneRetrieve()
        {
            try
            {
                using (var business = new  ECOsystem.Areas.Admin.Business.Administration.ThirdPartyManagementBusiness())
                {
                    var response = business.DriversCellsPhoneRetrieve();

                    return Json(new
                    {
                        Data = response,
                        MessageType = "Exitoso",
                        InfoMessage = response.Count() > 0 ? "Proceso efectuado satisfactoriamente." : "No existen registros con teléfonos válidos",
                        ErrorMessage = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Data = string.Empty,
                    MessageType = "Fallido",
                    InfoMessage = ex.Message,
                    ErrorMessage = ex.StackTrace
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult DriversCellsPhoneAddOrEdit(IEnumerable<UsersWhatsApp> data)
        {
            try
            {
                using (var business = new  ECOsystem.Areas.Admin.Business.Administration.ThirdPartyManagementBusiness())
                {
                    var response = business.DriversCellsPhoneAddOrEdit(data);

                    return Json(new
                    {
                        Data = response,
                        MessageType = "Exitoso",
                        InfoMessage = response.Count() > 0 ? "Proceso efectuado satisfactoriamente." : "No existen registros con teléfonos válidos",
                        ErrorMessage = string.Empty
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Data = string.Empty,
                    MessageType = "Fallido",
                    InfoMessage = ex.Message,
                    ErrorMessage = ex.StackTrace
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}