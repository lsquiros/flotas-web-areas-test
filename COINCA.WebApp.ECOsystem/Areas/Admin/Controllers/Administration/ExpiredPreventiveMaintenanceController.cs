﻿using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Business.Utilities;
using  ECOsystem.Areas.Admin.Models.Identity;
using  ECOsystem.Areas.Admin.Utilities;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;




namespace  ECOsystem.Areas.Admin.Controllers.Administration.Control
{
    public class ExpiredPreventiveMaintenanceController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var bus = new ExpireMaintenanceBusiness())
            {
                var model = new ExpiredPreventiveMaintenanceBase()
                {
                    Record = new PreventiveMaintenanceRecordByVehicleBase
                                 {
                                     Detail = new PreventiveMaintenanceRecordByVehicleCostDetail() { Cost = new List<PreventiveMaintenanceRecordByVehicleCost>() }
                                 },
                    List = bus.ExpireMaintenanceRetrieve((int)ECOsystem.Utilities.Session.GetCustomerId(), (int)ECOsystem.Utilities.Session.GetUserInfo().UserId)
                };

                return View(model);
            }
        }

        /// <summary>
        /// Apply the expire Maintenance
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadMaintenance(int id, int vehicleId)
        {
            using (var business = new PreventiveMaintenanceVehicleBusiness())
            {
                var model = new PreventiveMaintenanceRecordByVehicleBase
                {
                    Data = new PreventiveMaintenanceRecordByVehicle(),
                    Detail = business.PreventiveMaintenanceRecordByVehicleDetail(id),
                };
                model.Data.PreventiveMaintenanceId = id;
                model.Data.VehicleId = vehicleId;
                ViewBag.Symbol = ECOsystem.Utilities.Session.GetCustomerInfo().CurrencySymbol;

                return PartialView("_partials/_DetailCost", model);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {   
            using (var bus = new ExpireMaintenanceBusiness())
            {  
                var model = bus.ExpireMaintenanceRetrieve((int)ECOsystem.Utilities.Session.GetCustomerId(), (int)ECOsystem.Utilities.Session.GetUserInfo().UserId);

                Session["Reporte"] = bus.GetReportData(model.ToList());

                return PartialView("_partials/_Grid", model);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de Mantenimientos Vencidos");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "ExpiredMaintenanceReport", this.ToString().Split('.')[3], "Reporte de Mantenimientos Vencidos");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                using (var bus = new ExpireMaintenanceBusiness())
                {
                    var model = new ExpiredPreventiveMaintenanceBase()
                    {
                        Record = new PreventiveMaintenanceRecordByVehicleBase
                        {
                            Detail = new PreventiveMaintenanceRecordByVehicleCostDetail() { Cost = new List<PreventiveMaintenanceRecordByVehicleCost>() }
                        },
                        List = bus.ExpireMaintenanceRetrieve((int)ECOsystem.Utilities.Session.GetCustomerId(), (int)ECOsystem.Utilities.Session.GetUserInfo().UserId)
                    };

                    return View("Index", model);
                }
            }                  
        }
    }
}

