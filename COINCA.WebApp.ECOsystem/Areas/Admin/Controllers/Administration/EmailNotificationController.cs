﻿using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Email;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Admin.Models.Identity;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Manage the maintenance of new emails for notifications
    /// </summary>
    public class EmailNotificationController : Controller
    {
        /// <summary>
        /// GET: EmailNotification
        /// </summary>
        /// <returns></returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new EmailBusiness())
            {
                var model = new EmailNotificationBase
                {
                    CreateEmail = new EmailNotificationCreate(),
                    List = business.Retrieve_EmailsNotificatios_List(null)
                };

                return View(model);
            }
        }

        /// <summary>
        /// Load Email for Modification
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "SUPER_ADMIN")]
        public PartialViewResult LoadEmailsNotifications(int id)
        {
            try
            {
                var model = new EmailNotificationCreate();

                using (var business = new EmailBusiness())
                {
                    model = business.Retrieve_EmailsNotificatios_List(id).FirstOrDefault();
                    TempData["ActivoInd"] = model.Activo;
                    return PartialView("_partials/_Detail", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "EmailNotification")
                });
            }
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="model"></param>
       /// <returns></returns>
        [HttpPost]
        //[Authorize(Roles = "SUPER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult EmailNotificationAddOrEdit(EmailNotificationCreate model)
        {
            try
            {               

                using (var bus = new EmailBusiness())
                {
                    bus.AddOrEdit_EmailsNotificatios(model);

                    return PartialView("_partials/_Grid", bus.Retrieve_EmailsNotificatios_List(null));
                }
            }
            catch (Exception e )
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("no corresponde"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Error al asignar número de vehículo Flotas BAC Credomatic",
                        TechnicalError = e.Message,
                        ReturnUlr = Url.Action("Index", "EmailNotification")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "EmailNotification")
                    });
                }
            }
            
        }
        
        /// <summary>
        /// Delete Correos de Notificacion
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        //[Authorize(Roles = "SUPER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult DeleteEmailNotification(int id)
        {
            try
            {
                using (var bus = new EmailBusiness())
                {
                    bus.Delete_EmailNotifications(id);
                    return PartialView("_partials/_Grid", bus.Retrieve_EmailsNotificatios_List(null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "EmailNotification")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "EmailNotification")
                    });
                }
            }
        }
    }
}