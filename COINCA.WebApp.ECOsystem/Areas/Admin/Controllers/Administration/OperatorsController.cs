﻿/************************************************************************************************************
*  File    : OperatorsController.cs
*  Summary : Operators Controller Actions
*  Author  : Cristian Martínez Hernández
*  Date    : 02/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Business.Core;
using ECOsystem.Models.Core;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Operators Class
    /// </summary>
    public class OperatorsController : Controller
    {
        /// <summary>
        /// Operators for Client Main View
        /// </summary>
        /// <returns>A object that renders a view</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using(var business = new OperatorsBusiness())
            {
                    var model = new OperatorsBase { 
                        Data = new Operators(),
                        List = business.RetrieveOperators(customerId:ECOsystem.Utilities.Session.GetCustomerId(), operatorId:null, key:null)
                    };
                return View(model);
            }
        }

        /// <summary>
        /// Search Operators
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchOperators(string key) 
        {
            try
            {
                using (var business = new OperatorsBusiness()) {
                    return PartialView("_partials/_Grid", business.RetrieveOperators(customerId:ECOsystem.Utilities.Session.GetCustomerId(), operatorId:null, key:key));
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("Index", "Operators")
                });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity OperatorsCategories
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditOperators(Operators model) 
        {
            try 
            {
                using (var business = new OperatorsBusiness()) 
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditOperators(model);  
                    }
                    return PartialView("_partials/_Grid", business.RetrieveOperators(customerId: ECOsystem.Utilities.Session.GetCustomerId(), operatorId: null, key: null));
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("Index", "Operators")
                });
            }
        }

        /// <summary>
        /// Load Operators
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult LoadOperators(int id)
        {
            try
            {
                using (var business = new OperatorsBusiness())
                {
                    return PartialView("_partials/_Detail", id < 0 ? business.RetrieveOperators(customerId: ECOsystem.Utilities.Session.GetCustomerId(), operatorId:id, isForNew:true): business.RetrieveOperators(customerId: ECOsystem.Utilities.Session.GetCustomerId(), operatorId: id, isForNew:false));
                }
            }
            catch (Exception ex) {

                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("Index", "Operators")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Operators
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult DeleteOperators(int id) 
        {
            try
            {
                using (var business = new OperatorsBusiness())
                {
                    business.DeleteOperators(id);
                    return PartialView("_partials/_Grid", business.RetrieveOperators(customerId: ECOsystem.Utilities.Session.GetCustomerId(), operatorId: null, key: null));
                }
            }


            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Operators")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Operators")
                    });
                }
            }

        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace),
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
	}
}