﻿using ECOsystem.Audit;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using Newtonsoft.Json;


using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class AlertsController : Controller
    {
        // GET: Alerts
        public ActionResult Index()
        {
            try
            {
                using (var bus = new AlertsBusiness())
                {
                    var model = new AlarmsBase()
                    {
                        List = bus.AlertsRetrieve(),
                        Menus = new List<AccountMenus>()
                    };

                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Save the alerts
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AlertsAddOrEdit(List<Alarms> alerts)
        {
            try
            {
                using (var bus = new AlertsBusiness())
                {
                    if (bus.AlertsAddOrEdit(alerts))
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificación de Alertas: {0}", new JavaScriptSerializer().Serialize(alerts)));
                        return Json(1);
                    }
                }

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificación de Alertas: {0}", new JavaScriptSerializer().Serialize(alerts)));
                return Json(2);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}