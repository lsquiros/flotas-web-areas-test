﻿/*************************************************************************************************
*  File    : PreventiveMaintenanceCatalogController.cs
*  Summary : Preventive Maintenance Catalog Controller Methods
*  Author  : Cristian Martínez
*  Date    : 01/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using  ECOsystem.Areas.Admin.Models.Identity;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Preventive Maintenance Catalog Controller
    /// </summary>
    public class PreventiveMaintenanceCatalogController : Controller
    {
        /// <summary>
        /// Operators for Client Main View
        /// </summary>
        /// <returns>A object that renders a view</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new PreventiveMaintenanceCatalogBusiness())
            {
                var model = new PreventiveMaintenanceCatalogBase
                {
                    Data = new PreventiveMaintenanceCatalog(),
                    List = business.RetrievePreventiveMaintenance(customerId: ECOsystem.Utilities.Session.GetCustomerId(), FilterType: null, preventiveMaintenanceCatalogId: null, key: null)
                };
                return View(model);
            }
        }


        /// <summary>
        /// Load the data based on the selection
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(int FilterId)
        {            
            using (var business = new PreventiveMaintenanceCatalogBusiness())
            {
                var model = new PreventiveMaintenanceCatalogBase();

                if (FilterId == 0)
                {
                    model.Data = new PreventiveMaintenanceCatalog();
                    model.List = business.RetrievePreventiveMaintenance(customerId: ECOsystem.Utilities.Session.GetCustomerId(), FilterType: null, preventiveMaintenanceCatalogId: null, key: null);
                    return View(model);
                }
                else
                {
                    model.FilterType = FilterId;
                    model.Data = new PreventiveMaintenanceCatalog();
                    model.List = business.RetrievePreventiveMaintenance(customerId: ECOsystem.Utilities.Session.GetCustomerId(), FilterType: model.FilterType, preventiveMaintenanceCatalogId: null, key: null);
                    return View(model);
                }
            
            }

        }

        /// <summary>
        /// Search Operators
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchPreventiveMaintenance(string key)
        {
            try
            {
                using (var business = new PreventiveMaintenanceCatalogBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrievePreventiveMaintenance(customerId: ECOsystem.Utilities.Session.GetCustomerId(), FilterType: null, preventiveMaintenanceCatalogId: null, key: key));
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("Index", "PreventiveMaintenanceCatalog")
                });
            }
        }

        /// <summary>
        /// Add Or Edit PreventiveMaintenance
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult AddOrEditPreventiveMaintenance(PreventiveMaintenanceCatalog model)
        {
            try
            {
                using (var business = new PreventiveMaintenanceCatalogBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        model.FrequencyDate = DateTime.Now.AddMonths((int)((model.FrequencyMonth != null)? model.FrequencyMonth : 0));                        
                        business.AddOrEditPreventiveMaintenance(model);
                    }
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex.Message);

                return Json("Error: " + ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Preventive Maintenance
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult CostPreventiveMaintenance(int id)
        {
            try
            {
                using (var business = new PreventiveMaintenanceCatalogBusiness())
                {
                    ViewBag.Symbol = ECOsystem.Utilities.Session.GetCustomerInfo().CurrencySymbol;
                    return PartialView("_partials/_Cost", business.RetrievePreventiveMaintenanceCost(id));
                    //pendiente danilo
                    //return PartialView("_partials/_Cost", new System.Collections.Generic.List<PreventiveMaintenanceCost>());
                    //return PartialView("_partials/_Grid", business.RetrievePreventiveMaintenance(customerId: ECOsystem.Utilities.Session.GetCustomerId(), preventiveMaintenanceCatalogId: null, key: null));
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("Index", "PreventiveMaintenanceCatalog")
                });
            }
        }
        
        /// <summary>
        /// Load PreventiveMaintenance
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpGet]
        [EcoAuthorize]
        public ActionResult LoadPreventiveMaintenance(int id)
        {
            try
            {
                using (var business = new PreventiveMaintenanceCatalogBusiness())
                {
                    var model = new PreventiveMaintenanceCatalogBase()
                    {
                        Data = (id <= 0) ? new PreventiveMaintenanceCatalog() : business.RetrievePreventiveMaintenance(customerId: ECOsystem.Utilities.Session.GetCustomerId(), FilterType: null, preventiveMaintenanceCatalogId: id, key: null).FirstOrDefault(),
                    };

                    return View("AddOrEditMaintenance", model);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = ex.Message + ex.StackTrace,
                    ReturnUlr = Url.Action("Index", "PreventiveMaintenanceCatalog")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Preventive Maintenance
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult DeletePreventiveMaintenance(int id)
        {
            try
            {
                using (var business = new PreventiveMaintenanceCatalogBusiness())
                {
                    business.DeletePreventiveMaintenance(id);
                    return RedirectToAction("Index");
                }
            }


            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "PreventiveMaintenanceCatalog")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "PreventiveMaintenanceCatalog")
                    });
                }
            }

        }

        /// <summary>
        /// DeleteCost
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteCost(string id)
        {
            try
            {
                using (var business = new PreventiveMaintenanceCatalogBusiness())
                {
                    business.DeletePreventiveCostMaintenance(Convert.ToInt16(id));
                }
                return Json(new { Result = "Ready" });
            }
            catch(Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(new { Result = "Error" });
            }
        }

        /// <summary>
        /// SaveCostList
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SaveCostList(List<string> list)
        {
            try
            {
                using (var business = new PreventiveMaintenanceCatalogBusiness())
                {
                    business.AddOrEditPreventiveMaintenanceCost(list);
                }
                return Json(new { Result = "Ready" });
            }
            catch(Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return Json(new { Result = "Error" });
            }
        }

        /// <summary>
        /// Render the grid partial
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult RefreshGrid()
        { 
            using (var business = new PreventiveMaintenanceCatalogBusiness())
            {
                var model = (List<PreventiveMaintenanceCatalog>)business.RetrievePreventiveMaintenance(customerId: ECOsystem.Utilities.Session.GetCustomerId(), FilterType: null, preventiveMaintenanceCatalogId: null, key: null);                               
                return PartialView("_partials/_Grid", model);
            }
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace),
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion


    }
}