﻿using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class CustomerTokenController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CustomerTokenRetrieve()
        {
            try
            {
                using (var bus = new APITokenBusiness())
                {
                    return Json(new { token = bus.RetrieveAPIToken(ECOsystem.Utilities.Session.GetCustomerId()) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(new { error = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CustomerTokenAddOrEdit()
        {
            try
            {
                using (var bus = new APITokenBusiness())
                {
                    return Json(new { token = bus.GenerateAPIToken(ECOsystem.Utilities.Session.GetCustomerId()) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(new { error = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}