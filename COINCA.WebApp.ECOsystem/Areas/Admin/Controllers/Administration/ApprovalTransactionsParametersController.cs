﻿using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Business.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class ApprovalTransactionsParametersController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                using (var bus = new ApprovalTransactionsParametersBusiness())
                {
                    var model = bus.RetrieveInformation();
                    Session["ColumnNameList"] = model.List;
                    Session["MaxId"] = model.MaxId;
                    return View(model.List);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al cargar los datos de conciliación de transacciones, Error: {0}", e.Message));
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddOrEditColumn(int? Id, string ColumnName)
        {
            try
            {
                using (var bus = new ApprovalTransactionsParametersBusiness())
                {
                    var list = new List<ApprovalTransactionsParameters>();
                    var item = new ApprovalTransactionsParameters();

                    //New Column if Id is null
                    if (Id == null)
                    {
                        Id = (int)Session["MaxId"];
                        list = (List<ApprovalTransactionsParameters>)Session["ColumnNameList"];
                        item.Id = (int)Id + 1;                        
                        item.Value = ColumnName;

                        list.Add(item);
                    }
                    else
                    {
                        list = (List<ApprovalTransactionsParameters>)Session["ColumnNameList"];
                        list.Where(x => x.Id == Id).ToList().ForEach(s => s.Value = ColumnName);
                    }                    

                    bus.ColumnsAddOrEdit(list, list.Max(x => x.Id));
                    Session["ColumnNameList"] = list;
                    Session["MaxId"] = list.Max(x => x.Id);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Cambios en los parámetros para la conciliación de transacciones, Datos: {0}", new JavaScriptSerializer().Serialize(list)));
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en los cambios para los parámetros de conciliación de transacciones, Error: {0}", e.Message));
                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }            
        }

        public ActionResult RemoveColumnFromList(int Id)
        {
            try
            {
                using (var bus = new ApprovalTransactionsParametersBusiness())
                {
                    var list = (List<ApprovalTransactionsParameters>)Session["ColumnNameList"];
                    list.RemoveAll(x => x.Id == Id);                    
                    bus.ColumnsAddOrEdit(list, (int)Session["MaxId"]);
                    Session["ColumnNameList"] = list;

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se elimina columna, Nuevos Datos: {0}; Columna Eliminada: {1}", new JavaScriptSerializer().Serialize(list), new JavaScriptSerializer().Serialize(list.Where(x => x.Id == Id))));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la eliminación de columna, Error: {0}", e.Message));
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }           
        }

        public ActionResult GetColumnToEdit(int? Id)
        {
            return Json(((List<ApprovalTransactionsParameters>)Session["ColumnNameList"]).Where(x => x.Id == Id).FirstOrDefault().Value, JsonRequestBehavior.AllowGet);
        }
    }
}