﻿/************************************************************************************************************
*  File    : VehiclesByDriverController.cs
*  Summary : VehiclesByDriver Controller Actions
*  Author  : Gerald Solano
*  Date    : 22/07/2015
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Models.Account;
using System.Collections.Generic;
using ECOsystem.Business.Utilities;
using System.Web.Script.Serialization;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// VehiclesByDriverController. Implementation of all action results from views
    /// </summary>
    public class VehiclesByDriverController : Controller
    {

        /// <summary>
        /// VehiclesByDriver Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            var model = new VehiclesByDriverBase
                {
                    UserId = null,
                    Data = new VehiclesByDriverData(),
                    Menus = new List<AccountMenus>()
                };
            return View(model);
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehiclesByDriver
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehiclesByDriver(VehiclesByDriverData model)
        {
            try
            {
                using (var business = new VehiclesByDriverBusiness())
                {
                    business.AddOrEditVehiclesByDriver(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[Cambios en los vehiculos por conductor]{0}", new JavaScriptSerializer().Serialize(model)));
                    return PartialView("_partials/_List", business.RetrieveVehiclesByDriver(model.UserId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehiclesByDriver")
                });
            }
        }


        /// <summary>
        /// Load VehiclesByDriver
        /// </summary>
        /// <param name="id">The drivers by vehicle group Id to find all drivers associated</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadVehiclesByDriver(int id)
        {
            try
            {
                using (var business = new VehiclesByDriverBusiness())
                {
                    return PartialView("_partials/_List", business.RetrieveVehiclesByDriver(id));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehiclesByDriver")
                });
            }
        }


    }
}