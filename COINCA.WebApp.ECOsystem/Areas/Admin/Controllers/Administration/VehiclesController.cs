﻿/************************************************************************************************************
*  File    : VehiclesController.cs
*  Summary : Vehicles Controller Actions
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Business.Utilities;
using  ECOsystem.Areas.Admin.Models.Identity;
using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using System.Collections.Generic;
using ECOsystem.Models.Account;
using System.Web.Script.Serialization;
using ECOsystem.Audit;
using ECOsystem.Models.Core;
using System.Data;
using Newtonsoft.Json;
using ECOsystem.Utilities;
using ECOsystem.ServiceOrderUtilities.Utilities;
using ECOsystem.ServiceOrderUtilities.Models;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Vehicles Controller. Implementation of all action results from views
    /// </summary>
    public class VehiclesController : Controller
    {

        /// <summary>
        /// Vehicles Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new VehiclesBusiness())
            {
                var model = new VehiclesBase
                {
                    Data = new Vehicles(),
                    List = business.RetrieveVehicles(null),
                    Parameters = new AdminReportsBase()
                };
                Session["VehiclesInfo"] = model.List;
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult AddOrEditVehicles(Vehicles model, bool RuleChange)
        {
            var jsonSerializer = new JavaScriptSerializer();
            var rules = jsonSerializer.Deserialize<List<TransactionRulesModel>>(Request["rules"]);
            var userId = ECOsystem.Utilities.Session.GetUserInfo();
            try
            {
                using (var business = new VehiclesBusiness())
                {
                    business.AddOrEditVehicles(model);
                    Session["VehiclesInfo"] = business.RetrieveVehicles(null);
                }
                using (var business2 = new CreditCardBusiness())
                {
                    if (RuleChange)
                    {
                        foreach (var rule in rules)
                        {
                            rule.RuleCustomerId = (rule.RuleCustomerId == 0) ? null : rule.RuleCustomerId;
                            rule.UserId = (userId != null) ? (int)userId.UserId : 0;
                            business2.AddOrEditTransactionRule(rule);

                            #region Event log
                            //Add log event
                            AddLogEvent(
                                    message: string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(rule)),
                                    userId: (userId != null) ? userId.UserId : 0,
                                    customerId: (userId != null) ? userId.CustomerId : 0,
                                    partnerId: (userId != null) ? userId.PartnerId : 0,
                                    exception: null,
                                    isInfo: true);
                            #endregion
                        }
                    }
                    if (!RuleChange && Convert.ToBoolean(Session["IsRestore"]))
                    {
                        business2.DeleteTransactionRules(rules.FirstOrDefault().VehicleId, rules.FirstOrDefault().CostCenterId);
                    }
                }
                Session["IsRestore"] = false;
                return null;
            }            
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("IMEI_NO_CORRESPONDE"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessageWithDismiss.cshtml", new CustomError()
                    {
                        TitleError = "Error al asignar número de vehículo Flotas BAC Credomatic",
                        TechnicalError = "El Imei no corresponde al número de vehículo Flotas BAC Credomatic digitado.",
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                }
                else if (e.Message.Contains("PLATE_IS_EXIST"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessageWithDismiss.cshtml", new CustomError()
                    {
                        TitleError = "Validación de Placa",
                        TechnicalError = "La Placa utilizada para éste vehículo ya se encuentra registrada.",
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                    
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                }
            }
        }
        

        /// <summary>
        /// Performs the References Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult EditVehiclesReferences(VehiclesReferences model)
        {
            try
            {
                using (var business = new VehiclesBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.EditVehiclesReferences(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveVehicles(null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Vehicles")
                });
            }
        }

        /// <summary>
        /// Search Vehicles
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchVehicles(string key)
        {
            try
            {
                using (var business = new VehiclesBusiness())
                {                    
                    var list = business.RetrieveVehicles(null, key);
                    Session["VehiclesInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Vehicles")
                });
            }
        }

        /// <summary>
        /// Load Vehicles
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult LoadVehicles(int id)
        {
            try
            {
                if (id < 0)
                {
                    Vehicles v = new Vehicles();
                    var Customer = ECOsystem.Utilities.Session.GetCustomerInfo();

                    v.Menus = new List<AccountMenus>();
                    v.FreightTemperature = 0;
                    v.FreightTemperatureThreshold1 = 0;
                    v.FreightTemperatureThreshold2 = 0;
                    v.AdministrativeSpeedLimit = 0;
                    v.MaintenanceList = new List<PreventiveMaintenanceVehicleDetail>();
                    v.Transactions = new TransactionsReportBase() { List = new List<TransactionsReport>() };
                    v.CreditCards = new List<CreditCard>();
                    v.Drivers = new List<ECOsystem.Models.Core.DriversUsers>();
                    v.VehicleBinnacles = new VehicleBinnacleBase();

                    using (var BusinessRules = new CreditCardBusiness())
                    {
                        v.Rules = BusinessRules.RetrieveTransactionRules(Customer.CustomerId, null, null);
                    }

                    return View("AddOrEditVehicles", v);
                }
                else 
                {
                    Session["DriversData"] = null;
                    var Customer = ECOsystem.Utilities.Session.GetCustomerInfo();
                    using (var business = new VehiclesBusiness())
                    {
                        using (var BusinessRules = new CreditCardBusiness())
                        {
                            using(var busDrivers = new DriversBusiness())
	                        {
		                        var model = business.RetrieveVehicles(id).FirstOrDefault();
                                model.Rules = BusinessRules.RetrieveTransactionRules(Customer.CustomerId, id, null);
                                model.Menus = new List<AccountMenus>();
                                model.MaintenanceList = business.RetrievePreventiveMaintenanceByVehicle(id);
                                model.Transactions = business.RetrieveTransactionsReport(new  ECOsystem.Areas.Admin.Models.Administration.ControlFuelsReportsBase() { key = model.PlateId });
                                model.CreditCards = BusinessRules.RetrieveCreditCardByVehicle(id);
                                model.Drivers = new List<ECOsystem.Models.Core.DriversUsers>();
                                model.VehicleBinnacles = new VehicleBinnacleBase() { List = business.VehicleBinnacleAddRetrieve((int)model.VehicleId, string.Empty) };

                                return View("AddOrEditVehicles", model);
	                        }                            
                        }
                    }
                } 
                
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Vehicles")
                });
            }
        }

        /// <summary>
        /// Load VehiclesReferences
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadVehiclesReferences(int id)
        {
            try
            {
                using (var business = new VehiclesBusiness())
                {
                    return PartialView("_partials/_DetailReferences", business.RetrieveVehiclesReferences(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Vehicles")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Vehicles
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteVehicles(int id)
        {
            try
            {
                using (var business = new VehiclesBusiness())
                {
                    var currentCustomerId = ECOsystem.Utilities.Session.GetCustomerId();

                    if(currentCustomerId != null)
                    {
                        var result = OrdersUtilities.SendOrder((int)OrdersEnum.Uninstall, currentCustomerId, id, true).FirstOrDefault();

                        if(result != null && string.IsNullOrEmpty(result.ErrorMessage))
                        {
                            new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[WORK_ORDER_PROCESS_VEHICLE_DELETE] Se agrega orden abierta para desinstalación al vehículo con el Id: {0}", id));                            
                        }
                        else
                        {
                            if (result != null)
                            {
                                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[WORK_ORDER_PROCESS_VEHICLE_DELETE] ResponseMessage: ", (!string.IsNullOrEmpty(result.ErrorMessage)? result.ErrorMessage : string.Empty)));
                            }
                            else
                            {
                                new EventLogBusiness().AddLogEvent(LogState.INFO, "[WORK_ORDER_PROCESS_VEHICLE_DELETE] No se logró enviar la orden de desinstalación, el vehículo no está homologado o no cumple con la configuración de GPS en su registro.");
                            }
                        }

                        business.DeleteVehicles(id);

                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Vehículo eliminado correctamente, Id: {0}", id));
                    }

                    var model = business.RetrieveVehicles(null);

                    Session["VehiclesInfo"] = model;

                    return PartialView("_partials/_Grid", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al eliminar vehículo. Error: {0}, Id: {1}", e, id));
                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                }
            }
            
        }

        /// <summary>
        /// Retrieve transactions
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TransactionsReportRetrieve(string StartDateStr, string EndDateStr, string Key, int TransactionType, int? Month, int? Year, int? CustomerId = null, int? DriverId = null)
        {
            var model = new  ECOsystem.Areas.Admin.Models.Administration.ControlFuelsReportsBase()
            {
                StartDateStr = string.IsNullOrEmpty(StartDateStr) ? null : StartDateStr,
                EndDateStr = string.IsNullOrEmpty(EndDateStr) ? null : EndDateStr,
                key = Key,
                Month = Month,
                Year = Year,
                TransactionType = TransactionType,
                CustomerId = CustomerId == null ? 0 : (int)CustomerId,
                DriverId = DriverId 
            };

            using (var business = new VehiclesBusiness())
            {
                return PartialView("_partials/_TransactionsGrid", business.RetrieveTransactionsReport(model).List);               
            }
        }
        
       
        /// <summary>
        /// LoadDrivers
        /// </summary>
        /// <param name="VehicleId"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult LoadDrivers(int VehicleId, int? page)
        {
            var Customer = ECOsystem.Utilities.Session.GetCustomerInfo();

            using (var bus = new VehiclesBusiness())
            {
                IEnumerable<DriversUsers> model = new List<ECOsystem.Models.Core.DriversUsers>();

                if (Session["DriversData"] == null)
                {
                    model = bus.RetrieveHistoricalDrivers(VehicleId);
                    Session["DriversData"] = model;
                }
                else
                {
                    model = (List<ECOsystem.Models.Core.DriversUsers>)Session["DriversData"];
                }

                #region PagerInfo
                Session["AjaxGrid"] = 1;
                Session["FormName"] = "LoadDriversForm";
                Session["PageNum"] = page != null ? (int)page : 1;

                var pager = new GridMvc.Pagination.GridPager();
                pager.ItemsCount = model.Count();
                pager.MaxDisplayedPages = 10;
                pager.PageSize = 30;
                Session["PageModel"] = pager;
                #endregion

                if (page == null || page == 1)
                {
                    return PartialView("_partials/_DriversGrid", model);
                }
                else
                {
                    var modelPage = model.Skip(((int)page - 1) * 30).Take(model.Count()).ToList();

                    return PartialView("_partials/_DriversGrid", modelPage);
                }
            }
        }

        /// <summary>
        /// Add and Retrieve the VehicleBinnacle
        /// </summary>
        /// <param name="VehicleId"></param>
        /// <param name="Comment"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult VehicleBinnacleAdd(int VehicleId, string Comment)
        {
            using (var business = new VehiclesBusiness())
            {
                var model = business.VehicleBinnacleAddRetrieve(VehicleId, Comment);                               

                return PartialView("_partials/_BinnacleGrid", model);
            }            
        }


        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {            
            DataTable report = new DataTable();
            using (var business = new VehiclesBusiness())
            {
                report = business.GetReportDownloadData();
                Session["Reporte"] = report;
                var modelVehicles = new VehiclesBase
                {
                    Data = new Vehicles(),
                    List = (List<Vehicles>)Session["VehiclesInfo"]
                };
                return View("Index", modelVehicles);
            }
        }


        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {                    
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Vehículos");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "VehiclesReport", this.ToString().Split('.')[2], "Descarga de Vehículos");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var modelVehicles = new VehiclesBase
                {
                    Data = new Vehicles(),
                    List = (List<Vehicles>)Session["VehiclesInfo"]
                };
                return View("Index", modelVehicles);
            }    
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            ECOsystem.Business.Utilities.EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion

        /// <summary>
        /// validate if the customer has valid credit cards distribution
        /// </summary>
        /// <returns>json with data</returns>
        public JsonResult CustomerCreditCardValidate()
        {
            try
            {
                using (var business = new CustomerCreditsAdminBusiness())
                {
                    var data = business.RetrieveCustomerCredits();
                    if (data.Count().Equals(0))
                    {
                        return Json(new
                        {
                            Data = data,
                            Messege = "Debe de distribuir el crédito en la tarjeta maestra",
                            MessegeType = "warning",
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            Data = data,
                            Messege = "Cliente con distribución de tarjeta de crédito correcto",
                            MessegeType = "success",
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Data = ex,
                    Messege = "Error al validar la distribución de la Tarjeta Maestra",
                    MessegeType = "error",
                });
            }
        }

        public JsonResult validateVehicleSticker(int VehicleId) {
            int sticker = 0;

            try
            {
                using (var business = new VehiclesBusiness())
                {
                    sticker = business.RetrieveVehicleSticker(VehicleId);
                }
            }
            catch (Exception e)
            {
                sticker = -1;
            }
            
            return Json(sticker);
        }


    }
}