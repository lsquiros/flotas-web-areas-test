﻿/************************************************************************************************************
*  File    : CreditCardController.cs
*  Summary : CreditCard Controller Actions
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Audit;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Models.Email;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// CreditCard Controller. Implementation of all action results from views
    /// </summary>
    public class CreditCardController : Controller
    {
        /// <summary>
        /// CreditCard Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    var model = new CreditCardBase
                    {
                        List = business.RetrieveCreditCard(null)
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                // Return response
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CreditCard")
                });
            }

        }

        /// <summary>
        /// CardRequest Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Admin()
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    var model = new CardRequestBase
                    {
                        List = business.RetrieveCardRequest(null, null)
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                // Return response
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CreditCard")
                });
            }

        }


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CreditCard
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddCreditCardFromRequest(int id)//CreditCardDetails model)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    var model = business.RetrieveCardRequestDetail(id);

                    if (model.RequestInfo.PaymentMethod != 0)
                    {
                        business.AddorEditPaymentInstruments(model.RequestInfo);
                    }
                    else {
                        business.AddOrEditCreditCard(model.CardInfo);
                    }

                    return PartialView("_partials/_GridRequest", business.RetrieveCardRequest(null, null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                //Error Messages
                if (e.Message.Contains("No amount available"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Crédito no disponible",
                        TechnicalError = "No tiene crédito disponible para esta solicitud.",
                        ReturnUlr = Url.Action("Admin", "CreditCard")
                    });

                }
                else if (e.Message.Contains("ERR-CARD-NOT-AVAILABLE"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Asignación de Tarjeta de Crédito",
                        TechnicalError = "No hay tarjetas disponibles para esta Empresa.",
                        ReturnUlr = Url.Action("Admin", "CreditCard")
                    });
                }

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Admin", "CreditCard")
                });
            }
        }

        /// <summary>
        /// Insert credit cards massively
        /// </summary>
        /// <param name="model">Id credit card requests' list</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public ActionResult AddAllCreditCardsFromRequest(List<int> model, List<int> types)
        {
            try
            {
                List<int> PaymentMethodList = new List<int>();

                for (int i = 0; i < model.Count; i++)
                {
                    if (types[i] != 0)
                    {
                        PaymentMethodList.Add(model[i]);
                        model.RemoveAt(i);
                        types.RemoveAt(i);
                        i--;
                    }
                }

                using (var business = new CreditCardBusiness())
                {
                    business.AddCreditCardsMassively(model);

                    for (int i = 0; i < PaymentMethodList.Count; i++)
                    {
                        var model2 = business.RetrieveCardRequestDetail(PaymentMethodList[i]);

                        if (model2.RequestInfo.PaymentMethod != 0)
                        {
                            business.AddorEditPaymentInstruments(model2.RequestInfo);
                        }
                    }

                    return PartialView("_partials/_GridRequest", business.RetrieveCardRequest(null, null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                //Error Messages
                if (e.Message.Contains("No amount available"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Crédito no disponible",
                        TechnicalError = "No tiene crédito disponible para esta solicitud.",
                        ReturnUlr = Url.Action("Admin", "CreditCard")
                    });

                }
                else if (e.Message.Contains("ERR-CARD-NOT-AVAILABLE"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Asignación de Tarjeta de Crédito",
                        TechnicalError = "No hay tarjetas disponibles para esta Empresa.",
                        ReturnUlr = Url.Action("Admin", "CreditCard")
                    });
                }

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Admin", "CreditCard")
                });
            }
        }

        #region DownloadIVEFile
        public ActionResult ValidatesIVEDownload()
        {
            try
            {
                using (var bus = new CreditCardBusiness())
                {
                    return Json(bus.ValidatesIVEDownload(ECOsystem.Utilities.Session.GetCustomerId()), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        } 

        [HttpPost]
        public ActionResult DownloadIVEFile()
        {
            try
            {
                byte[] SummaryFile = System.IO.File.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data/FORMULARIO_IVE_TARJETAS_ADICIONALES.pdf")));
                return new FileStreamResult(new MemoryStream(SummaryFile), "text/csv")
                {
                    FileDownloadName = "FORMULARIO IVE TARJETAS ADICIONALES.pdf"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CreditCard
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult EditCreditCard(List<int> IdList, int StatusId, List<int> TypeList)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Cambios de estado a las tarjetas. Detalle: {0}, Estado: {1}", new JavaScriptSerializer().Serialize(IdList), StatusId));
                    business.EditCreditCardStatus(IdList, StatusId, TypeList);

                    #region Survey Status
                    var controller = ControllerContext.RouteData;
                    var survey = ECOsystem.Business.Core.PartnerSurveyBusiness.ShowSurveyToCustomer(controller.Values["controller"].ToString(), controller.Values["action"].ToString(), string.Format("StatusId={0}", StatusId), ECOsystem.Utilities.Session.GetCustomerId());

                    if (!survey.IsNull())
                    {
                        Session["ShowSurvey"] = JsonConvert.SerializeObject(new PartnerSurveyShow { SurveyId = survey, Elements = IdList.Count });
                    }
                    #endregion

                    return Json("success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);


                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CreditCard")
                });
            }
        }


        /// <summary>
        /// Search CreditCard
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public PartialViewResult SearchCreditCard(string key, int status)
        {
            var filter = key;
            key = String.Empty;
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    return PartialView("_partials/_Grid",
                        business.RetrieveCreditCard(null, key, null, status).
                            Where(creditCard =>
                                    (creditCard.CreditCardHolder != null && creditCard.CreditCardHolder.ToLower().Contains(filter.ToLower())) ||
                                    (creditCard.StatusName != null && creditCard.StatusName.ToLower().Contains(filter.ToLower())) ||
                                    (creditCard.RestrictCreditCardNumber != null && creditCard.RestrictCreditCardNumber.ToLower().Contains(filter.ToLower())) ||
                                    (creditCard.DecryptedCustomerName != null && creditCard.DecryptedCustomerName.ToLower().Contains(filter.ToLower()))
                                ));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CreditCard")
                });
            }
        }


        /// <summary>
        /// Search CreditCard Requests
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public PartialViewResult SearchCreditCardRequests(string key)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    return PartialView("_partials/_GridRequest", business.RetrieveCardRequest(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);


                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CreditCard")
                });
            }
        }


        /// <summary>
        /// Load CreditCard
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadCreditCard(int id)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveCardDetail(id));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CreditCard")
                });
            }
        }


        /// <summary>
        /// Create card file to create physical Cards
        /// </summary>
        [HttpPost]
        public ActionResult CreateCardFile(string creditCardList)
        {
            // Valida el mensaje de notificación y Embosado si es por Conductor o Vehiculo
            int? emisionType = 0;

            try
            {
                // Get parameters to generate credit card file.
                using (var business = new CreditCardBusiness())
                {
                    var model = new CardFileParamsBase
                    {
                        List = business.RetrieveCardFileParams()
                    };
                    if (model.List.Count() > 0)
                    {
                        // Create file with date and time
                        var horaActual = DateTimeOffset.Now;
                        string format = "dmyyyy HHmmss";

                        string path = ConfigurationManager.AppSettings["CREDITCARDFILEPATH"];

                        TextWriter tw = System.IO.File.CreateText(@path + horaActual.ToString(format) + ".txt");

                        string[] cardsId = creditCardList.Substring(0, creditCardList.Length - 1).Split(',');

                        int cardIdNumber = 1; //Contador de tarjetas a embosar.

                        List<int> ccNumbers = new List<int>();

                        for (int i = 0; i < cardsId.Count(); i++)
                        {
                            // Get Credit Card info
                            var creditCardModel = business.RetrieveCreditCard(Convert.ToInt32(cardsId[i])).FirstOrDefault();

                            ccNumbers.Add(Convert.ToInt32(creditCardModel.PartnerId));

                            int customerId = Convert.ToInt32(creditCardModel.CustomerId);
                            string creditCardNumber = creditCardModel.DisplayCreditCardNumber;
                            string year = Convert.ToString(creditCardModel.ExpirationYear);
                            string month = Convert.ToString(creditCardModel.ExpirationMonth);

                            // Get Card Request info
                            var cardRequest = business.RetrieveCardRequestNoRequest(customerId, creditCardModel.CardRequestId, null).FirstOrDefault();
                            emisionType = cardRequest.IssueForId;

                            string person = string.Empty;
                            if (emisionType != null && emisionType == 100) // Drivers
                            {
                                person = (cardRequest.DriverName != null) ? cardRequest.DriverName : ((cardRequest.AuthorizedPerson != null) ? cardRequest.AuthorizedPerson : string.Empty);
                            }
                            else if (emisionType != null && emisionType == 101) // Vehicles
                            {
                                //person = (cardRequest.PlateId != null) ? string.Format("{0} V{1}", cardRequest.PlateId.Replace(" ", ""), cardRequest.VehicleName) : string.Empty;
                                //person = (cardRequest.PlateId != null) ? string.Format("{0} Vehiculo:", cardRequest.PlateId.Replace(" ", "")) : string.Empty;
                                person = (cardRequest.PlateId != null) ? cardRequest.PlateId : string.Empty;
                            }
                            else
                            { // Default
                                person = (cardRequest.DriverName != null) ? cardRequest.DriverName : ((cardRequest.AuthorizedPerson != null) ? cardRequest.AuthorizedPerson : string.Empty);
                            }

                            string companyName = cardRequest.DecryptedCustomerName;
                            string firstName = "";
                            string lastName = "";

                            if (!string.IsNullOrEmpty(person))
                            {
                                #region Extract the Person Name for the Card
                                string[] completeName = person.Split(' ');
                                if (completeName.Count() == 2)
                                {
                                    firstName = completeName[0].ToUpper();
                                    lastName = completeName[1].ToUpper();
                                }
                                else if (completeName.Count() == 3)
                                {
                                    firstName = completeName[0].ToUpper();
                                    lastName = completeName[1].ToUpper() + " " + completeName[2].ToUpper();
                                }
                                else if (completeName.Count() == 4)
                                {
                                    firstName = completeName[0].ToUpper() + " " + completeName[1].ToUpper();
                                    lastName = completeName[2].ToUpper() + " " + completeName[3].ToUpper();
                                }
                                else if (completeName.Count() > 0)
                                {
                                    firstName = completeName[0].ToUpper();
                                }
                                #endregion

                                #region Write to file

                                // Write to file

                                int cardFileParamLenght = 0;
                                string separator = "";
                                string fillChar = "";
                                string fileLine = "";
                                foreach (var param in model.List)
                                {
                                    separator = "";
                                    cardFileParamLenght = param.CardFileParamLenght;
                                    if (param.Separator != null)
                                    {
                                        separator = param.Separator;
                                    }
                                    if (param.FillChar != null)
                                    {
                                        fillChar = param.FillChar;
                                    }

                                    //CardId
                                    if (param.CardFileParamName.Equals("CardId"))
                                    {
                                        fileLine = fileLine + separator + fillString(Convert.ToString(cardIdNumber), param.CardFileParamLenght, fillChar, "L");
                                    }

                                    //CardNumWithSpaces
                                    if (param.CardFileParamName.Equals("CardNumWithSpaces"))
                                    {
                                        fileLine = fileLine + separator + creditCardNumber.Replace("-", " ");
                                    }

                                    //CardHolderNameWithSpaces
                                    if (param.CardFileParamName.Equals("CardHolderNameWithSpaces"))
                                    {
                                        //fileLine = fileLine + separator + fillString(person.ToUpper(), param.CardFileParamLenght, fillChar, "R");
                                        string fullName = lastName + " " + firstName;
                                        fileLine = fileLine + separator + fillString(fullName.ToUpper(), param.CardFileParamLenght, fillChar, "R");
                                    }

                                    //CardHolderCompany
                                    if (param.CardFileParamName.Equals("CardHolderCompany"))
                                    {
                                        fileLine = fileLine + separator + fillString(companyName.ToUpper(), param.CardFileParamLenght, fillChar, "R");
                                    }

                                    //MonthWithSeparator
                                    if (param.CardFileParamName.Equals("MonthWithSeparator"))
                                    {
                                        fileLine = fileLine + separator + fillMonth(month);
                                    }

                                    //YearWithSeparator
                                    if (param.CardFileParamName.Equals("YearWithSeparator"))
                                    {
                                        fileLine = fileLine + separator + year.Substring(2);
                                    }

                                    //CardNumNoSpaces
                                    if (param.CardFileParamName.Equals("CardNumNoSpaces"))
                                    {
                                        fileLine = fileLine + separator + creditCardNumber.Replace("-", "");
                                    }

                                    //FirstName
                                    if (param.CardFileParamName.Equals("FirstName"))
                                    {
                                        fileLine = fileLine + separator + fillString(firstName, param.CardFileParamLenght, fillChar, "R");
                                    }

                                    //LastName
                                    if (param.CardFileParamName.Equals("LastName"))
                                    {
                                        fileLine = fileLine + separator + fillString(lastName, param.CardFileParamLenght, fillChar, "R");
                                    }

                                    //Year
                                    if (param.CardFileParamName.Equals("Year"))
                                    {
                                        fileLine = fileLine + separator + year.Substring(2);
                                    }

                                    //Month
                                    if (param.CardFileParamName.Equals("Month"))
                                    {
                                        fileLine = fileLine + separator + fillMonth(month);
                                    }

                                    //FillOne
                                    if (param.CardFileParamName.Equals("FillOne"))
                                    {
                                        fileLine = fileLine + separator + fillString("", param.CardFileParamLenght, fillChar, "R");
                                    }

                                    //CardNumNoSpacesTwo
                                    if (param.CardFileParamName.Equals("CardNumNoSpacesTwo"))
                                    {
                                        fileLine = fileLine + separator + creditCardNumber.Replace("-", "");
                                    }

                                    //YearTwo
                                    if (param.CardFileParamName.Equals("YearTwo"))
                                    {
                                        fileLine = fileLine + separator + year.Substring(2);
                                    }

                                    //MonthTwo
                                    if (param.CardFileParamName.Equals("MonthTwo"))
                                    {
                                        fileLine = fileLine + separator + fillMonth(month);
                                    }

                                    //FillTwo
                                    if (param.CardFileParamName.Equals("FillTwo"))
                                    {
                                        fileLine = fileLine + separator + fillString("", param.CardFileParamLenght, fillChar, "R");
                                    }

                                    //Arrobas
                                    if (param.CardFileParamName.Equals("Arrobas"))
                                    {
                                        fileLine = fileLine + separator + fillString("", param.CardFileParamLenght, fillChar, "R");
                                    }
                                }
                                tw.WriteLine(fileLine);
                                cardIdNumber++;

                                //Update Credit Card State
                                using (var businessState = new StatusBusiness())
                                {
                                    var result = businessState.RetrieveStatus("CUSTOMER_CREDIT_CARD_SUPER_ADMIN").Where(w => w.Code == "CUSTOMER_CREDIT_CARD_IN_PROCESS_KEY").FirstOrDefault();
                                    creditCardModel.StatusId = result.StatusId;

                                    business.EditCreditCard(creditCardModel);
                                }

                                #endregion
                            }
                            else
                            {
                                throw new Exception("PersonNameRequired");
                            }
                        }
                        tw.Close();

                        try
                        {
                            #region Log Event
                            var user = ECOsystem.Utilities.Session.GetUserInfo();

                            //Add log event
                            AddLogEvent(
                                    message: "Card File Generated " + horaActual.ToString(format) + ".txt",
                                    userId: (user != null) ? user.UserId : 0,
                                    customerId: (user != null) ? user.CustomerId : 0,
                                    partnerId: (user != null) ? user.PartnerId : 0,
                                    exception: null,
                                    isInfo: true);
                            #endregion                            

                            #region SaveEmails
                            EmailEncryptedAlarms emailModel = new EmailEncryptedAlarms();
                            emailModel.To = ConfigurationManager.AppSettings["CREDITCARDMAILTO"];
                            emailModel.From = ConfigurationManager.AppSettings["SMTPUSERACCOUNT"];
                            emailModel.Subject = ConfigurationManager.AppSettings["CREDITCARDMAILSUBJECT"]; ;
                            emailModel.Message = "Card File Generated " + horaActual.ToString(format) + ".txt";
                            using (var em = new EmailBusiness())
                            {
                                em.AddOrEditEmailEncrypted(emailModel);
                            }
                            #endregion


                        }
                        catch (Exception e)
                        {
                            ECOsystem.Utilities.Session.SentrySupport(e);

                            //Add log event
                            //Add log event
                            new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
                        }
                    }

                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Generación de Archivo de Embozado",
                        TechnicalError = "La generación del archivo para embozado se ha realizado satisfactoriamente.",
                        ReturnUlr = Url.Action("Index", "CreditCard")
                    });

                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);


                if (e.Message.Contains("PersonNameRequired"))
                {
                    string title = (emisionType == 100) ? "Titular # Placa" : ((emisionType == 101) ? "Titular de Nombre de Conductor o de Persona Autorizada" : "Titular de # Placa, Nombre de Conductor o de Persona Autorizada"); // 100 = Drivers / 101 = Vehicles

                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = title + " es Requerido",
                        TechnicalError = string.Format("Una o varias de las tarjetas a embozar necesita un {0}.", title),
                        ReturnUlr = Url.Action("Index", "CreditCard")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "CreditCard")
                    });
                }
            }
        }


        /// <summary>
        /// Sent notifications to the email list
        /// </summary>
        /// <param name="email"></param>
       // private void SentNotifications(string hora)
        private void SentNotifications(string hora, List<int> newCCPartners)
        {
            IEnumerable<EmailNotification> email = null;

            #region FiltroPartners
            string partners = "AND PartnerId in(";
            int? t = null;

            foreach (int x in newCCPartners)
            {
                if (t == null)
                    partners = partners + x;
                else
                    partners = partners + "," + x;
                t = x;
            }

            partners = partners + ")";
            #endregion

            using (var em = new EmailBusiness())
            {
                email = em.Retrieve_EmailsNotificatios(partners);
            }

            SmtpClient smtpServer = new SmtpClient(ConfigurationManager.AppSettings["SMTPSERVER"]);
            smtpServer.UseDefaultCredentials = false;
            smtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUSERACCOUNT"], ConfigurationManager.AppSettings["SMTPPASSWORDACCOUNT"]);
            smtpServer.Port = 587;
            smtpServer.EnableSsl = true;

            try
            {
                foreach (var e in email)
                {
                    #region SaveEmailNotifications
                    EmailEncryptedAlarms emailModel = new EmailEncryptedAlarms();
                    emailModel.To = e.Email;
                    emailModel.From = ConfigurationManager.AppSettings["SMTPUSERACCOUNT"];
                    emailModel.Subject = ConfigurationManager.AppSettings["CREDITCARDMAILSUBJECT"]; ;
                    emailModel.Message = "Card File Generated " + hora + ".txt";
                    //using (var em = new EmailBusiness())
                    //{ 
                    //   em.AddOrEditEmailEncrypted(emailModel);
                    //}
                    #endregion

                    MailMessage mail = new MailMessage();

                    mail.From = new MailAddress(ConfigurationManager.AppSettings["SMTPUSERACCOUNT"]);
                    mail.To.Add(new MailAddress(e.Email));
                    mail.Subject = "Testing" + ConfigurationManager.AppSettings["CREDITCARDMAILSUBJECT"];
                    mail.IsBodyHtml = true;
                    mail.Body = "Card File Generated " + hora + ".txt";
                    smtpServer.EnableSsl = true;

                    smtpServer.Send(mail);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// UploadFiletoFTPServer
        /// </summary>
        /// <param name="UFile"></param>
        /// <param name="fileName"></param>
        private void UploadFiletoFTPServer(string UFile, string fileName)
        {
            if (System.IO.File.Exists(UFile))
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://172.22.224.82/" + fileName);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                request.Credentials = new NetworkCredential("corp\\hretana", "Hr3tana!");

                StreamReader sourceStream = new StreamReader(UFile);
                byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                sourceStream.Close();
                request.ContentLength = fileContents.Length;
                request.Proxy = null;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                response.Close();
            }
            //Deletes the file from the hardDrive once the file has been upload.
            System.IO.File.Delete(UFile);
        }


        /// <summary>
        /// Fill Month
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        public string fillMonth(string month)
        {
            if (month.Length == 1)
                return "0" + month;
            else
                return month;
        }


        /// <summary>
        /// Fill String
        /// </summary>
        /// <param name="value"></param>
        /// <param name="lenght"></param>
        /// <param name="fillChar"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public string fillString(string value, int lenght, string fillChar, string direction)
        {
            string result = "";
            string fillString = "";
            int stringLenght = value.Length;
            int fillLenght = lenght - stringLenght;

            if (fillLenght > 0)
            {
                for (int i = 0; i < fillLenght; i++)
                {
                    fillString = fillString + fillChar;
                }

                // Append to the left
                if (direction.Equals("L"))
                {
                    result = fillString + value;
                }
                // Append to the Right
                if (direction.Equals("R"))
                {
                    result = value + fillString;
                }

                return result;
            }

            // Si el value es mayor al lenght realizamos un substring del lenght
            value = value.Substring(0, lenght);

            return value;
        }


        /// <summary>
        /// Load CreditCard
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadCardRequest(int id)
        {
            try
            {
                using (var business = new CreditCardBusiness())
                {
                    var model = business.RetrieveCardRequestDetail(id);

                    if (model.CreditInfo != null)
                    {
                        return PartialView("_partials/_DetailRequest", model);
                    }
                    else
                    {
                        return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                        {
                            TitleError = "Tarjeta Maestra no asociada",
                            TechnicalError = "No existe Tarjeta Maestra asociada a este cliente, por favor asocie la Tarjeta Maestra e intente de nuevo.",
                            ReturnUlr = Url.Action("CardRequest", "CreditCard")
                        });
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("CardRequest", "CreditCard")
                });
            }
        }


        /// <summary>
        /// Load Add CreditCard Request ---> Igual al proceso de Solicitud de Tarjeta en rol de Partner
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult LoadAddOrEditCardRequest(int requestId)
        {
            try
            {
                using (var business = new ECOsystem.Business.Core.CustomersBusiness())
                {
                    var customer = ECOsystem.Utilities.Session.GetCustomerInfo();
                    if (customer == null)
                    {
                        return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                        {
                            TechnicalError = "Missing Customer Id",
                            ReturnUlr = Url.Action("CardRequest", "CreditCard")
                        });
                    }

                    if (requestId < 0)
                    {
                        return PartialView("_partials/_DetailCardRequest", new ECOsystem.Models.Core.CardRequest()
                        {
                            IssueForId = customer.IssueForId,
                            CountryId = customer.CountryId
                        });
                    }
                    else
                    {
                        return PartialView("_partials/_DetailCardRequest",
                                 business.RetrieveCustomerCardRequest(customer.CustomerId, requestId).FirstOrDefault()
                            );
                    }
                }

            }
            catch (Exception e)
            {

                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);


                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("CardRequest", "CreditCard")
                });
            }
        }


        /// <summary>
        /// Load Customers
        /// </summary>
        /// <param name="custId"></param>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[System.Web.Mvc.Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public JsonResult LoadPreviousCustomerCardRequest(int id)
        {
            try
            {
                if (id != -100)
                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = false }
                    };
                using (var business = new ECOsystem.Business.Core.CustomersBusiness())
                {
                    var customer = ECOsystem.Utilities.Session.GetCustomerInfo();
                    var tmp = business.RetrieveCustomerCardRequest(customer.CustomerId, null).FirstOrDefault();
                    if (tmp == null)
                    {
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { result = false }
                        };
                    }
                    var cardRequest = new ECOsystem.Models.Core.CardRequest
                    {
                        StateId = tmp.StateId,
                        CountyId = tmp.CountyId,
                        CityId = tmp.CityId,
                        AddressLine1 = tmp.AddressLine1,
                        AddressLine2 = tmp.AddressLine2,
                        AuthorizedPerson = tmp.AuthorizedPerson,
                        ContactPhone = tmp.ContactPhone,
                        EstimatedDeliveryStr = tmp.EstimatedDeliveryStr
                    };
                    using (var gdBusiness = new GeographicDivisionBusiness())
                    {
                        var listCounties = gdBusiness.RetrieveCountiesJSON(null, tmp.StateId, null);
                        var listCities = gdBusiness.RetrieveCitiesJSON(null, tmp.CountyId, null, null);
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { result = true, cardRequest, listCounties, listCities }
                        };
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = false }
                };
            }
        }


        /// <summary>
        /// Load Counties
        /// </summary>
        /// <param name="stateId">The state Id related to the Counties to be loaded</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[System.Web.Mvc.Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public JsonResult LoadCounties(int? stateId)
        {
            try
            {
                if (stateId == null)
                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = false }
                    };

                using (var business = new GeographicDivisionBusiness())
                {
                    var listCounties = business.RetrieveCountiesJSON(null, stateId, null);
                    if (listCounties == null)
                    {
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { result = false }
                        };
                    }

                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = true, listCounties }
                    };
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = false }
                };
            }
        }


        /// <summary>
        /// Load Cities
        /// </summary>
        /// <param name="countyId">The state Id related to the Counties to be loaded</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[System.Web.Mvc.Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public JsonResult LoadCities(int? countyId)
        {
            try
            {
                if (countyId == null)
                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = false }
                    };

                using (var business = new GeographicDivisionBusiness())
                {
                    var listCities = business.RetrieveCitiesJSON(null, countyId, null, null);
                    if (listCities == null)
                    {
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { result = false }
                        };
                    }

                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = true, listCities }
                    };
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = false }
                };
            }
        }


        /// <summary>
        /// Performs the maintenance Insert entity CustomerCard
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public PartialViewResult AddOrEditCardRequest(ECOsystem.Models.Core.CardRequest model)
        {
            try
            {
                using (var business = new ECOsystem.Business.Core.CustomersBusiness())
                {
                    var customer = ECOsystem.Utilities.Session.GetCustomerInfo();
                    model.PaymentReference = (model.PaymentReference == null) ? string.Empty : model.PaymentReference;
                    model.CustomerId = customer.CustomerId;

                    business.AddOrEditCardRequest(model);
                    String message = string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, message);

                    using (var ccBusiness = new CreditCardBusiness())
                    {
                        var modelCC = new CardRequestBase
                        {
                            List = ccBusiness.RetrieveCardRequest(null, null)
                        };

                        return PartialView("_partials/_GridRequest", modelCC.List);
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                //Retorna error cuando una placa tiene una tarjeta en uso.
                if (e.Message.Contains("ERR_CARD_WITH_PLATE_IN_USE"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Vehículo con Tarjeta Activa",
                        TechnicalError = "Solicitud denegada, el vehiculo ya cuenta con una tarjeta activa.",
                        ReturnUlr = Url.Action("Admin", "CreditCard")
                    });
                }
                else if (e.Message.Contains("There is a previous request or an active credit card for this vehicle/driver"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Vehículo con Solicitud",
                        TechnicalError = "Solicitud denegada, el vehiculo ya cuenta con una solicitud.",
                        ReturnUlr = Url.Action("Admin", "CreditCard")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Admin", "CreditCard")
                    });
                }
            }
        }

        /// <summary>
        /// Remove credit card requests
        /// </summary>
        /// <param name="cardRequestId">Card request id, it'll be deleted</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult RemoveCardRequest(int cardRequestId)
        {
            try
            {
                using (var business = new ECOsystem.Business.Core.CustomersBusiness())
                {
                    ECOsystem.Models.Core.CardRequest model = new ECOsystem.Models.Core.CardRequest();
                    model.CardRequestId = cardRequestId;
                    business.RemoveCardRequest(model);
                    String message = string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, message);

                    using (var ccBusiness = new CreditCardBusiness())
                    {
                        var modelCC = new CardRequestBase
                        {
                            List = ccBusiness.RetrieveCardRequest(null, null)
                        };

                        return PartialView("_partials/_GridRequest", modelCC.List);
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);


                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Admin", "CreditCard")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model CreditCard
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public string DeleteCreditCard()
        {
            return "";
        }

        public JsonResult StickersCount() {

            int count = 0;

            try
            {
                using (var business = new CreditCardBusiness())
                {
                    count = business.GetStickersCount();
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
            }

            return Json(count);
        }

        public JsonResult LoadPaymentMethods() {

            try
            {
                using (var business = new CreditCardBusiness())
                {
                    var methods = business.GetPaymentMethods();
                    return Json(methods);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);
                return Json("");
            }
        }


        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}