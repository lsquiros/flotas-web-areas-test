﻿/************************************************************************************************************
*  File    : FleetCardsImportController.cs
*  Summary : FleetCards Import Controller Actions
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// VehicleUnits Controller. Implementation of all action results from views
    /// </summary>
    public class VehiclesImportController : Controller
    {
        /// <summary>
        /// Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "SUPER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            var menus = new List<AccountMenus>();
            return View(menus);
        }       

    }
}