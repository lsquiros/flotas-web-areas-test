﻿/************************************************************************************************************
*  File    : VehicleGroupsController.cs
*  Summary : VehicleGroups Controller Actions
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Admin.Models.Identity;
using System.Data;
using System.Collections.Generic;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// VehicleGroups Controller. Implementation of all action results from views
    /// </summary>
    public class VehicleGroupsController : Controller
    {

        /// <summary>
        /// VehicleGroups Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new VehicleGroupsBusiness())
            {
                var model = new VehicleGroupsBase
                {
                    Data = new VehicleGroups(),
                    List = business.RetrieveVehicleGroups(null)
                };
                Session["GroupsInfo"] = model.List;
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleGroups
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehicleGroups(VehicleGroups model)
        {
            try
            {
                using (var business = new VehicleGroupsBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        model.Description = (model.Description == null) ? string.Empty : model.Description;
                        business.AddOrEditVehicleGroups(model);
                    }
                    var list = business.RetrieveVehicleGroups(null);
                    Session["GroupsInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleGroups")
                });
            }
        }

        /// <summary>
        /// Search VehicleGroups
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchVehicleGroups(string key)
        {
            try
            {
                using (var business = new VehicleGroupsBusiness())
                {
                    var list = business.RetrieveVehicleGroups(null, key);
                    Session["GroupsInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleGroups")
                });
            }
        }

        /// <summary>
        /// Load VehicleGroups
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadVehicleGroups(int id)
        {
            try
            {
                using (var business = new VehicleGroupsBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveVehicleGroups(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleGroups")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleGroups
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteVehicleGroups(int id)
        {
            try
            {
                using (var business = new VehicleGroupsBusiness())
                {
                    business.DeleteVehicleGroups(id);
                    var list = business.RetrieveVehicleGroups(null);
                    Session["GroupsInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }


            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "VehicleGroups")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "VehicleGroups")
                    });
                }
            }

        }


        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            using (var business = new VehicleGroupsBusiness())
            {
                Session["Reporte"] = business.DownloadData((List<VehicleGroups>)Session["GroupsInfo"]);
                var model = new VehicleGroupsBase
                {
                    Data = new VehicleGroups(),
                    List = (List<VehicleGroups>)Session["GroupsInfo"]
                };
                return View("Index", model);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataSet report = new DataSet();
                if (Session["Reporte"] != null)
                {
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Grupos de Vehículos");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "VehicleGroupsReport", this.ToString().Split('.')[2], "Reporte de Grupos de Vehículos");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var model = new VehicleGroupsBase
                {
                    Data = new VehicleGroups(),
                    List = (List<VehicleGroups>)Session["GroupsInfo"]
                };
                return View("Index", model);
            }
        }


    }
}