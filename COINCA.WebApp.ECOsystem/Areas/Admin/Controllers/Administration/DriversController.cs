﻿/************************************************************************************************************
*  File    : DriversController.cs
*  Summary : Drivers Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem;
using ECOsystem.Audit;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Drivers Controller
    /// </summary>
    public class DriversController : Controller
    {

        private ApplicationUserManager _userManager;
        /// <summary>
        /// Asp Net User Manager
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        /// <summary>
        /// Drivers Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            if (Session["DriversInfo"] == null)
            {
                using (var business = new DriversBusiness())
                {

                    var model = new DriversBase
                    {
                        Data = new Users(),
                        List = business.RetrieveCustomerDrivers(null, customerId: ECOsystem.Utilities.Session.GetCustomerId()).OrderBy(x => x.DecryptedName).ToList(),  //.RetrieveUsers(null, customerId2: ECOsystem.Utilities.Session.GetCustomerId(), isList: true),
                        Parameters = new AdminReportsBase()
                    };

                    Session["DriversInfo"] = model.List;
                    return View(model);
                }
            }
            else
            {
                var model = new DriversBase
                {
                    Data = new Users(),
                    List = (List<Users>)Session["DriversInfo"]
                };
                return View(model);
            }
        }

        // This action handles the form POST and the upload
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [EcoAuthorize]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult AddOrEditImportDrivers()
        {
            if (Request.HttpMethod == "GET")
            {
                return Redirect("/Administracion/Drivers/Index");
            }
            DataTable csvData = new DataTable();
            var ErrorList = new List<string>();
            var List = new List<DriversErrors>();
            int ErrorCount = 0;
            int UpdateCount = 0;
            int ActivateCount = 0;
            int DeactivateCount = 0;
            int SuccessCount = 0;
            int NoProcessCount = 0;
            try
            {
                HttpPostedFileBase file = Request.Files[0];
                // Verify that the user selected a file
                if (file != null && file.ContentLength > 0)
                {
                    // extract only the filename
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.GetTempPath() + fileName;
                    file.SaveAs(path);
                    // redirect back to the index action to show the form once again
                    using (TextFieldParser csvReader = new TextFieldParser(path))
                    {
                        csvReader.SetDelimiters(new string[] { "," });
                        csvReader.HasFieldsEnclosedInQuotes = true;
                        int countColumns = 0;
                        int header = 0;
                        while (!csvReader.EndOfData)
                        {
                            DriversErrors ErrorModel = new DriversErrors();
                            try
                            {
                                #region Row
                                DriversBase model = new DriversBase();
                                string[] colFields = csvReader.ReadFields();
                                string[] ColumnsToModel = new string[11];
                                foreach (string column in colFields)
                                {
                                    DataColumn datecolumn = new DataColumn(column);
                                    datecolumn.AllowDBNull = true;
                                    model.Data.ChangePassword = false;
                                    ColumnsToModel[countColumns] = column;
                                    countColumns++;
                                }

                                header++;
                                if (header == 1)
                                {
                                    countColumns = 0;
                                    continue;
                                }
                                countColumns = 0;

                                bool validField = true;
                                int resultInt = 0;

                                //Default
                                model.Data.ChangePassword = false;
                                model.Data.CustomerUser = null;
                                model.Data.IsCustomerUser = false;
                                model.Data.IsLockedOut = false;
                                model.Data.IsPartnerUser = false;
                                model.Data.IsPasswordExpired = false;
                                model.Data.IsDriverUser = true;
                                model.Data.RoleName = "CUSTOMER_DRIVER";

                                #region Name
                                if (ColumnsToModel[1] != null && ColumnsToModel[1] != "")
                                    model.Data.DecryptedName = ColumnsToModel[1] != null ? ColumnsToModel[1] : "";
                                else
                                    throw new Exception(++ErrorCount + " ) " + "El nombre del usuario es requerido.");
                                #endregion Name

                                #region Email
                                if (ColumnsToModel[0] != null && ColumnsToModel[0] != "")
                                {
                                    if (!ColumnsToModel[0].Contains("@") && !ColumnsToModel[0].Contains("."))
                                    {
                                        throw new Exception(++ErrorCount + " ) " + model.Data.DecryptedName + " - El correo no posee un formato correcto.");
                                    }
                                    model.Data.DecryptedEmail = ColumnsToModel[0] != null ? ColumnsToModel[0] : "";
                                }
                                else
                                    throw new Exception(++ErrorCount + " ) " + model.Data.DecryptedName + " - El correo es requerido.");
                                #endregion Email

                                #region Identification
                                if (ColumnsToModel[2] != null && ColumnsToModel[2] != "")
                                    model.Data.DecryptedIdentification = ColumnsToModel[2] != null ? ColumnsToModel[2] : "";
                                else
                                    throw new Exception(++ErrorCount + " ) " + model.Data.DecryptedName + " - La identificación es requerida.");
                                #endregion Identification

                                model.Data.DecryptedCode = ColumnsToModel[3] != null && ColumnsToModel[3] != "" ? ColumnsToModel[3] : "";
                                model.Data.DecryptedLicense = ColumnsToModel[4] != null && ColumnsToModel[4] != "" ? ColumnsToModel[4] : ColumnsToModel[2];
                                model.Data.IsAgent = ColumnsToModel[10] != null ? (ColumnsToModel[10].ToLower() == "no" || ColumnsToModel[10].ToLower() == "n" || ColumnsToModel[10] == "0" ? false : true) : false;

                                #region LicenseExpiration
                                if (model.Data.IsAgent)
                                {
                                    model.Data.LicenseExpiration = DateTime.Now.AddYears(3);
                                }
                                else
                                {
                                    try
                                    {
                                        if (Convert.ToInt32(ColumnsToModel[5].Replace("�", "").Split('/')[0]) > 12)
                                        {
                                            string dateTime = Convert.ToDateTime(ColumnsToModel[5].Replace("�", "") + " 00:00:00", CultureInfo.GetCultureInfo("es-CR")).ToString("MM/dd/yyyy");
                                            model.Data.LicenseExpiration = Convert.ToDateTime(dateTime);
                                        }
                                        else
                                            model.Data.LicenseExpiration = Convert.ToDateTime(ColumnsToModel[5].Replace("�", ""), CultureInfo.GetCultureInfo("en-US"));
                                    }
                                    catch (Exception)
                                    {
                                        throw new Exception(++ErrorCount + " ) " + model.Data.DecryptedName + " - La fecha de expiración de la licencia es inválida.");
                                    }
                                }
                                #endregion LicenseExpiration

                                #region DallasKey
                                if (ColumnsToModel[6] != null && ColumnsToModel[6].ToString().Trim() != "")
                                {
                                    using (var business = new Business.Administration.DallasKeysBusiness())
                                    {
                                        var dk = business.RetrieveDallasKeys(key: ColumnsToModel[6].ToString().Trim()).FirstOrDefault();
                                        if (dk != null)
                                            model.Data.DallasId = dk.DallasId;
                                        else
                                            throw new Exception(++ErrorCount + " ) " + model.Data.DecryptedName + " - La llave dallas es inválida.");
                                    }
                                }
                                else
                                    model.Data.DallasId = null;
                                #endregion DallasKey

                                #region DailyTransactionLimit
                                validField = int.TryParse(ColumnsToModel[7].ToString().Trim(), out resultInt);
                                if (validField || ColumnsToModel[7].ToString().Trim() == "")
                                    model.Data.DailyTransactionLimit = validField ? resultInt : 0;
                                else
                                    throw new Exception(++ErrorCount + " ) " + model.Data.DecryptedName + " - El máximo de transacciones diarias es inválido.");
                                #endregion DailyTransactionLimit

                                model.Data.DecryptedPhoneNumber = ColumnsToModel[8];
                                model.Data.IsActive = ColumnsToModel[9] != null ? (ColumnsToModel[9].ToLower() == "no" || ColumnsToModel[9].ToLower() == "n" || ColumnsToModel[9] == "0" ? false : true) : false;

                                #endregion Row

                                using (var business = new UsersBusiness())
                                {
                                    try
                                    {
                                        business.AddOrEditUsers(model.Data, UserManager, Url, Request);
                                        //ErrorModel.Item = RowCount + " ) " + model.Data.DecryptedName + " - Almacenado Correctamente";
                                        //List.Add(ErrorModel);
                                        SuccessCount++;
                                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model)));
                                    }
                                    catch (Exception e)
                                    {
                                        var Response = business.UpdateImportDriver(model.Data);
                                        if (Response == "activó")
                                            ActivateCount++;
                                        else if (Response == "desactivó")
                                            DeactivateCount++;
                                        else if (Response == "actualizó")
                                            UpdateCount++;
                                        else
                                            NoProcessCount++;

                                        ErrorCount++;
                                        if (Response == "actualizó" || Response == "activó" || Response == "desactivó")
                                            ErrorModel.Item = ErrorCount + " ) " + model.Data.DecryptedName + " - " + e.Message.Split(':')[1] + ". Se " + Response + " el usuario.";
                                        else
                                            ErrorModel.Item = ErrorCount + " ) " + model.Data.DecryptedName + " - " + e.Message.Split(':')[1] + ". El usuario está asignado al cliente " + TripleDesEncryption.Decrypt(Response) + ", por lo tanto no se modifico.";

                                        List.Add(ErrorModel);
                                        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(ErrorModel)));
                                    }
                                }
                                //RowCount++;
                            }
                            catch (Exception ex)
                            {
                                NoProcessCount++;
                                ErrorModel.Item = ex.Message;
                                List.Add(ErrorModel);
                                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(ErrorModel)));
                            }
                        }

                        using (var business = new DriversBusiness())
                        {
                            Session["SummaryFile"] = business.CreateTxtFormat(List);
                            Session["Path"] = path;
                        }
                    }
                }
                using (var business = new DriversBusiness())
                {
                    var modelDrivers = new DriversBase
                    {
                        Data = new Users(),
                        List = business.RetrieveCustomerDrivers(null, customerId: ECOsystem.Utilities.Session.GetCustomerId()), //.RetrieveUsers(null, customerId2: ECOsystem.Utilities.Session.GetCustomerId(), isList: true)
                    };
                    Session["DriversInfo"] = modelDrivers.List;
                    ViewBag.ResultData = string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(SuccessCount),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(UpdateCount),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(ActivateCount),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(DeactivateCount),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(NoProcessCount),
                                                                          ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals(SuccessCount + ErrorCount));
                    return View("Index", modelDrivers);
                }
            }
            catch (Exception ex)
            {
                ECOsystem.Utilities.Session.SentrySupport(ex);

                using (var business = new DriversBusiness())
                {
                    var modelDrivers = new DriversBase
                    {
                        Data = new Users(),
                        List = business.RetrieveCustomerDrivers(null, customerId: ECOsystem.Utilities.Session.GetCustomerId()), //.RetrieveUsers(null, customerId2: ECOsystem.Utilities.Session.GetCustomerId(), isList: true)
                    };
                    Session["DriversInfo"] = modelDrivers.List;
                    return View("Index", modelDrivers);
                }
            }

        }

        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public ActionResult DonwloadReportFile()
        {
            byte[] SummaryFile = null;
            SummaryFile = (byte[])Session["SummaryFile"];
            Session["SummaryFile"] = null;
            string path = Session["Path"].ToString();
            Session["Path"] = null;
            System.IO.File.Delete(path);
            var CustomerInfo = ECOsystem.Utilities.Session.GetCustomerInfo();
            DateTime Today = DateTime.Now;
            return new FileStreamResult(new MemoryStream(SummaryFile), "text/plain")
            {
                FileDownloadName = CustomerInfo.DecryptedName + "- Reporte de Importación Conductores - " + Today + ".txt"
            };
        }

        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public ActionResult DonwloadExampleFile()
        {
            byte[] SummaryFile = System.IO.File.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data/Example.csv")));

            return new FileStreamResult(new MemoryStream(SummaryFile), "text/csv")
            {
                FileDownloadName = "Archivo_Ejemplo.csv"
            };
        }


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Drivers
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult AddOrEditDrivers(DriverViewModel model)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    model.User.LicenseExpiration = (model.User.IsAgent) ? DateTime.Now.AddYears(3) : model.User.LicenseExpiration;
                    business.AddOrEditUsers(model.User, UserManager, Url, Request);
                    Session["DriversInfo"] = null;

                    //Add log event
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model)));
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                if (e.Message.Contains("ERROR EXIST_USR"))
                {
                    return Json("ERROR EXIST_USR", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var message = "ERROR | " + e.Message;
                    return Json(message, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Search Drivers
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchDrivers(string key, int? statusId)
        {
            try
            {
                DriversBase model = new DriversBase();
                using (var business = new DriversBusiness())
                {
                    if (statusId == 2)
                    {
                        statusId = null;
                    }
                    model.Parameters.Status = statusId;
                    var list = business.RetrieveCustomerDrivers(null, key, customerId: ECOsystem.Utilities.Session.GetCustomerId(), UserStatus: model.Parameters.Status); //.RetrieveUsers(null, key, customerId2: ECOsystem.Utilities.Session.GetCustomerId(), isList: true, UserStatus: model.Parameters.Status);
                    Session["DriversInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Drivers")
                });
            }
        }

        /// <summary>
        /// Load Drivers
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public ActionResult LoadDrivers(int id, bool? isagent)
        {
            try
            {
                var customer = ECOsystem.Utilities.Session.GetCustomerInfo();
                ViewBag.CardTypeRequest = customer.IssueForId;

                DriverViewModel viewModel = new DriverViewModel();

                if (id < 0)
                {
                    viewModel.User.RoleName = "CUSTOMER_DRIVER";
                    viewModel.User.IsDriverUser = true;
                    viewModel.CardTypeRequest = customer.IssueForId;
                    viewModel.User.IsAgent = (bool)isagent;
                    return View("AddOrEditDrivers", viewModel);
                }

                using (var business = new DriversBusiness())
                {
                    using (var BusinessRules = new CreditCardBusiness())
                    {
                        using (var vehicleBusiness = new VehiclesBusiness())
                        {
                            var customer_Id = ECOsystem.Utilities.Session.GetCustomerId();
                            viewModel.User = business.RetrieveCustomerDrivers(id, customerId: customer_Id, bindObtPhoto: true).FirstOrDefault();
                            viewModel.User.RoleName = "CUSTOMER_DRIVER";
                            viewModel.CardTypeRequest = customer.IssueForId;
                            if (customer.IssueForId == 100)
                            {
                                viewModel.Budget = business.RetrieveDriverBudget(id).FirstOrDefault();
                                viewModel.Budget = viewModel.Budget == null ? new DriverBudget() : viewModel.Budget;
                                viewModel.Transactions = vehicleBusiness.RetrieveTransactionsReport(new  ECOsystem.Areas.Admin.Models.Administration.ControlFuelsReportsBase() { CustomerId = (int)customer_Id, DriverId = id });
                                viewModel.CreditCards = BusinessRules.RetrieveCreditCard(null, null, id);
                            }
                            return View("AddOrEditDrivers", viewModel);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Drivers")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model 
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteDrivers(int id)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    business.DeleteUsers(id);
                    #region Event log

                    var message = string.Format("[DELETE_USR] Cuenta de Usuario ha sido eliminada. Usuario afectado es UserId = {0}", id);

                    //Add log event
                    new EventLogBusiness().AddLogEvent(LogState.ERROR, message);

                    #endregion
                    var list = business.RetrieveUsers(null, customerId2: ECOsystem.Utilities.Session.GetCustomerId(), isList: true);
                    Session["DriversInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Drivers")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Drivers")
                    });
                }
            }

        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            using (var business = new DriversBusiness())
            {
                DataTable report = business.GetReportDownloadData();
                Session["Reporte"] = report;

                using (var DriverBusiness = new UsersBusiness())
                {
                    var modelDrivers = new DriversBase
                    {
                        Data = new Users(),
                        List = (List<Users>)Session["DriversInfo"]
                    };
                    return View("Index", modelDrivers);
                }
            }

        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Conductores");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "DriversReport", this.ToString().Split('.')[2], "Reporte de Conductores");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var model = new DriversBase
                {
                    Data = new Users(),
                    List = (List<Users>)Session["DriversInfo"]
                };
                Session["DriversInfo"] = model.List;
                return View("Index", model);
            }
        }


        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        /// <summary>
        /// validate if the customer has valid credit cards distribution
        /// </summary>
        /// <returns>json with data</returns>
        public JsonResult CustomerCreditCardValidate()
        {
            try
            {
                using (var business = new CustomerCreditsAdminBusiness())
                {
                    var data = business.RetrieveCustomerCredits();
                    if (data.Count().Equals(0))
                    {
                        return Json(new
                        {
                            Data = data,
                            Messege = "Debe de distribuir el crédito en la tarjeta maestra",
                            MessegeType = "warning",
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            Data = data,
                            Messege = "Cliente con distribución de tarjeta de crédito correcto",
                            MessegeType = "success",
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Data = ex,
                    Messege = "Error al validar la distribución de la Tarjeta Maestra",
                    MessegeType = "error",
                });
            }
        }
    }
}

