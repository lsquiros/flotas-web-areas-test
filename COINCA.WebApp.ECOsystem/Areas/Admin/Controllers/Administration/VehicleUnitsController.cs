﻿/************************************************************************************************************
*  File    : VehicleUnitsController.cs
*  Summary : VehicleUnits Controller Actions
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Admin.Models.Identity;
using System.Collections.Generic;
using System.Data;
using ECOsystem.Business.Utilities;
using Newtonsoft.Json;
using ECOsystem.Utilities;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// VehicleUnits Controller. Implementation of all action results from views
    /// </summary>
    public class VehicleUnitsController : Controller
    {

        /// <summary>
        /// VehicleUnits Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new VehicleUnitsBusiness())
            {
                var model = new VehicleUnitsBase();
                if (Session["VehicleUnitsInfo"] == null)
                {
                    model = new VehicleUnitsBase
                    {
                        Data = new VehicleUnits(),
                        List = business.RetrieveVehicleUnits(null)
                    };
                    Session["VehicleUnitsInfo"] = model.List;
                }
                else
                {
                    model = new VehicleUnitsBase
                    {
                        Data = new VehicleUnits(),
                        List = (List<VehicleUnits>)Session["VehicleUnitsInfo"]
                    };
                }
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleUnits
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehicleUnits(VehicleUnits model)
        {
            try
            {
                using (var business = new VehicleUnitsBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditVehicleUnits(model);
                    }
                    var list = business.RetrieveVehicleUnits(null);
                    Session["VehicleUnitsInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleUnits")
                });
            }
        }

        /// <summary>
        /// Search VehicleUnits
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchVehicleUnits(string key)
        {
            try
            {
                using (var business = new VehicleUnitsBusiness())
                {
                    var model = business.RetrieveVehicleUnits(null, key);
                    Session["VehicleUnitsInfo"] = model;
                    return PartialView("_partials/_Grid", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleUnits")
                });
            }
        }

        /// <summary>
        /// Load VehicleUnits
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult LoadVehicleUnits(int id)
        {
            try
            {
                using (var business = new VehicleUnitsBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveVehicleUnits(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleUnits")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleUnits
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteVehicleUnits(int id)
        {
            try
            {
                using (var business = new VehicleUnitsBusiness())
                {
                    business.DeleteVehicleUnits(id);
                    var list = business.RetrieveVehicleUnits(null);
                    Session["VehicleUnitsInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }


            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "VehicleUnits")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "VehicleUnits")
                    });
                }
            }

        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            using (var business = new VehicleUnitsBusiness())
            {
                Session["Reporte"] = business.ReportData((List<VehicleUnits>)Session["VehicleUnitsInfo"]);

                var model = new VehicleUnitsBase
                {
                    Data = new VehicleUnits(),
                    List = (List<VehicleUnits>)Session["VehicleUnitsInfo"]
                };
                return View("Index", model);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataSet report = new DataSet();
                if (Session["Reporte"] != null)
                {
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte Unidades de Vehículos");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "VehicleUnitsReport", this.ToString().Split('.')[2], "Reporte Unidades de Vehículos");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var model = new VehicleUnitsBase
                {
                    Data = new VehicleUnits(),
                    List = (List<VehicleUnits>)Session["VehicleUnitsInfo"]
                };
                return View("Index", model);
            }
        }
    }
}