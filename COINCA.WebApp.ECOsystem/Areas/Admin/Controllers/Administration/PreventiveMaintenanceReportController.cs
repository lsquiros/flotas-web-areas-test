﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceController.cs
*  Summary : Preventive Maintenance Controller Actions
*  Author  : Napoleón Alvarado
*  Date    : 12/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System.IO;
using System.Web.Mvc;
//using ECO_Control.Business.Control;
//using ECO_Control.Models.Control;
using ECOsystem.Utilities;
using Newtonsoft.Json;
//using ECO_Control.Models.Identity;
using System;
using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Models.Identity;
using System.Data;
using ECOsystem.Business.Utilities;
using System.Collections.Generic;
using System.Xml.Linq;

namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Preventive Maintenance Report Class
    /// </summary>
    public class PreventiveMaintenanceReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new PreventiveMaintenanceReportBusiness())
            {
                Session["PreventiveMaintenanceReport"] = business.RetrievePreventiveMaintenanceReport(new ControlPreventiveMaintenanceReportBase());
                return View(Session["PreventiveMaintenanceReport"]);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(PreventiveMaintenanceReportBase model)
        {
            if (model == null || model.Parameters == null) RedirectToAction("Index");

            using (var business = new PreventiveMaintenanceReportBusiness())
            {
                if (model.Parameters.FilterType == 0)
                    model.Parameters.FilterType = null;
                Session["FilterType"] = model.Parameters.FilterType;
                Session["PreventiveMaintenanceReport"] = business.RetrievePreventiveMaintenanceReport(model.Parameters);
                return View(Session["PreventiveMaintenanceReport"]);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(ControlFuelsReportsBase model)
        {
            try
            {
                using (var business = new PreventiveMaintenanceReportBusiness())
                {
                    var ds = business.GetReportDownloadData((PreventiveMaintenanceReportBase)Session["PreventiveMaintenanceReport"]);
                    if (ds.Tables[1].Rows.Count == 0)
                    {
                        return Json("No Data");
                    }
                    Session["Reporte"] = ds;
                    return View("Index", Session["PreventiveMaintenanceReport"]);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                if (Session["Reporte"] != null)
                {
                    DataSet report = new DataSet();
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;

                    using (var bus = new ReportsUtilities())
                    {
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte de Mantenimientos");
                        return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "PreventiveMaintenanceReport", this.ToString().Split('.')[2], "Reporte de Mantenimientos");
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return Redirect("Index");
            }
        }


        /// <summary>
        /// Load Counties
        /// </summary>
        /// <param name="stateId">The state Id related to the Counties to be loaded</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[System.Web.Mvc.Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public JsonResult LoadMaintenance(int? FilterType)
        {
            try
            {
                if (FilterType == null)
                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = false }
                    };

                using (var business = new PreventiveMaintenanceReportBusiness())
                {
                    var listTypes = business.RetrieveTypesJSON(FilterType);
                    if (listTypes == null)
                    {
                        return new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { result = false }
                        };
                    }

                    return new JsonResult()
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { result = true, listTypes }
                    };
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return null;
               
            }
        }

        public JsonResult getMaintenanceDetail(int vehicleId, int catalogId)
        {
            try
            {
                using (var business = new PreventiveMaintenanceVehicleBusiness())
                {
                    var docXml = new XDocument(new XElement("xmldata"));
                    var root = docXml.Root;

                    root.Add(new XElement("PreventiveMaintenance",
                    new XAttribute("VehicleId", vehicleId),
                    new XAttribute("CatalogId", catalogId))
                    );

                    var data = business.RetrieveMaintenanceDetail(docXml.ToString());
                    return Json(data);
                }
            }
            catch (Exception e)
            {
                return Json("");
            }
        }

    }
}