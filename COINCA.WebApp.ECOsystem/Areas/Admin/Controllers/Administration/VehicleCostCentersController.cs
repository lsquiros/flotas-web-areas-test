﻿/************************************************************************************************************
*  File    : VehicleCostCentersController.cs
*  Summary : VehicleCostCenters Controller Actions
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Models.Account;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using ECOsystem.Audit;
using System.Data;
using ECOsystem.Business.Utilities;
using Newtonsoft.Json;
using ECOsystem.Utilities;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// VehicleCostCenters Controller. Implementation of all action results from views
    /// </summary>
    public class VehicleCostCentersController : Controller
    {

        /// <summary>
        /// VehicleCostCenters Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new VehicleCostCentersBusiness())
            {
                var model = new VehicleCostCentersBase();
                model = new VehicleCostCentersBase
                {
                    Data = new VehicleCostCenters(),
                    List = business.RetrieveVehicleCostCenters(null)
                };
                Session["CostCenterInfo"] = model.List;
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleCostCenters
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult AddOrEditVehicleCostCenters(VehicleCostCenters model, bool RuleChange)
        {
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            List<TransactionRulesModel> rules = new List<TransactionRulesModel>();
            var userId = ECOsystem.Utilities.Session.GetUserInfo();

            try
            {
                var arrRules = Request["rules"];
                if (arrRules != null && arrRules.Count() > 0)
                    rules = json_serializer.Deserialize<List<TransactionRulesModel>>(arrRules);

                using (var business = new VehicleCostCentersBusiness())
                {
                    business.AddOrEditVehicleCostCenters(model);
                    Session["CostCenterInfo"] = business.RetrieveVehicleCostCenters(null);
                    if (!model.PullPreviousBudgetDisable && model.CostCenterId != null)
                        business.ChangePullPreviousBudget((int)model.CostCenterId, model.PullPreviousBudget);
                }
                using (var business2 = new CreditCardBusiness())
                {
                    if (RuleChange)
                    {
                        foreach (var rule in rules)
                        {
                            rule.RuleCustomerId = (rule.RuleCustomerId == 0) ? null : rule.RuleCustomerId;
                            rule.UserId = (userId != null) ? (int)userId.UserId : 0;
                            business2.AddOrEditTransactionRule(rule);

                            #region Event log
                            //Add log event
                            AddLogEvent(
                                    message: string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(rule)),
                                    userId: (userId != null) ? userId.UserId : 0,
                                    customerId: (userId != null) ? userId.CustomerId : 0,
                                    partnerId: (userId != null) ? userId.PartnerId : 0,
                                    exception: null,
                                    isInfo: true);
                            #endregion
                        }
                    }
                    if (!RuleChange && Convert.ToBoolean(Session["IsRestore"]))
                    {
                        business2.DeleteTransactionRules(rules.FirstOrDefault().VehicleId, rules.FirstOrDefault().CostCenterId);
                    }
                }
                Session["IsRestore"] = false;
                return null;
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCostCenters")
                });
            }
        }

        /// <summary>
        /// Change PullPreviousBudget of all Vehicles on Vehicle Cost Center
        /// </summary>
        /// <param name="id">Vehicle Cost Center Id</param>
        /// <param name="state">New PullPreviousBudget state</param>
        /// <returns></returns>
        public ActionResult ChangePullPreviousBudget(int id, bool state)
        {
            bool changed;
            try
            {
                using (var business = new VehicleCostCentersBusiness())
                {
                    business.ChangePullPreviousBudget(id, state);
                    changed = true;
                }
            }
            catch (Exception)
            {
                changed = false;
            }
            return Json(changed);
        }

        /// <summary>
        /// Search VehicleCostCenters
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchVehicleCostCenters(string key)
        {
            try
            {
                using (var business = new VehicleCostCentersBusiness())
                {
                    var list = business.RetrieveVehicleCostCenters(null, key: key);
                    Session["CostCenterInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCostCenters")
                });
            }
        }

        /// <summary>
        /// Load VehicleCostCenters
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public ActionResult LoadVehicleCostCenters(int id)
        {
            try
            {
                var Customer = ECOsystem.Utilities.Session.GetCustomerInfo();
                using (var business = new VehicleCostCentersBusiness())
                {
                    using (var BusinessRules = new CreditCardBusiness())
                    {
                        var model = business.RetrieveVehicleCostCenters(id).FirstOrDefault();
                        model.Rules = BusinessRules.RetrieveTransactionRules(Customer.CustomerId, null, id);
                        model.Menus = new List<AccountMenus>();

                        return View("AddOrEditCostCenter", model);
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCostCenters")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleCostCenters
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteVehicleCostCenters(int id)
        {
            try
            {
                using (var business = new VehicleCostCentersBusiness())
                {
                    business.DeleteVehicleCostCenters(id);
                    var list = business.RetrieveVehicleCostCenters(null);
                    Session["CostCenterInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }


            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "VehicleCostCenters")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "VehicleCostCenters")
                    });
                }
            }

        }


        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            using (var business = new VehicleCostCentersBusiness())
            {
                Session["Reporte"] = business.DownloadData((List<VehicleCostCenters>)Session["CostCenterInfo"]);
                var modelCostCenter = new VehicleCostCentersBase
                {
                    Data = new VehicleCostCenters(),
                    List = (List<VehicleCostCenters>)Session["CostCenterInfo"]
                };
                return View("Index", modelCostCenter);
            }
        }


        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataSet report = new DataSet();
                if (Session["Reporte"] != null)
                {
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Vehículos por Centro de Costo");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "VehicleCostCenterReport", this.ToString().Split('.')[2], "Reporte de Vehículos por Centro de Costo");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var model = new VehicleCostCentersBase
                {
                    Data = new VehicleCostCenters(),
                    List = (List<VehicleCostCenters>)Session["CostCenterInfo"]
                };
                return View("Index", model);
            }
        }


        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            ECOsystem.Business.Utilities.EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion

    }
}