﻿/************************************************************************************************************
*  File    : DallasKeysController.cs
*  Summary : DallasKeys Controller Actions
*  Author  : Gerald Solano
*  Date    : 27/08/2015
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.Audit;
using  ECOsystem.Areas.Admin.Business.Administration;
using ECOsystem.Business.Utilities;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Models.Miscellaneous;
using System;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Models.Administration;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Dallas Keys Controller Class
    /// </summary>
    public class DallasKeysController : Controller
    {
        /// <summary>
        /// Index for Data Grid
        /// </summary>
        /// <returns>ActionResult</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new DallasKeysBusiness())
                {
                    var model = new DallasKeysBase
                    {
                        Data = new DallasKeys(),
                        List = business.RetrieveDallasKeys(null)
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                // Return response
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Home")
                });
            }
        }

        /// <summary>
        /// Search SearchDallasKeys
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public PartialViewResult SearchDallasKeys(string key, DateTime? startDate, DateTime? endDate)
        {
            try
            {
                using (var business = new DallasKeysBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveDallasKeys(null, startDate, endDate, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                // Return response
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "DallasKeys")
                });
            }
        }

        /// <summary>
        /// Load DallasKeys
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public PartialViewResult LoadDallasKeys(int id)
        {
            try
            {
                if (id < 0)
                {
                    using (var business = new DallasKeysBusiness())
                    {
                        return PartialView("_partials/_Detail", new DallasKeys());
                    }
                }
                using (var business = new DallasKeysBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveDallasKeys(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                // Return response
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "DallasKeys")
                });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity DallasKeys
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditDallasKeys(DallasKeys model)
        {
            try
            {
                using (var business = new DallasKeysBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        if (!business.ExistDallasKey(model.DallasCode))
                        {
                            business.AddOrEditDallasKeys(model);
                        }
                        else {
                            return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                            {
                                TitleError = "Código Dallas existente.",
                                TechnicalError = "El Código Dallas ya existe en el mantenimiento. Por favor validar nuevamente.",
                                ReturnUlr = Url.Action("Index", "DallasKeys")
                            });
                        }
                    }
                    return PartialView("_partials/_Grid", business.RetrieveDallasKeys(null));
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                // Return response
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "DallasKeys")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleCategories
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult DeleteDallasKeys(int id)
        {
            try
            {
                using (var business = new DallasKeysBusiness())
                {
                    var dk = business.RetrieveDallasKeys(id).FirstOrDefault();
                    if (dk != null) {
                        dk.IsDeleted = true;
                        business.AddOrEditDallasKeys(dk);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveDallasKeys(null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                // Return response
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCategories")
                });
            }

        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace),
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}