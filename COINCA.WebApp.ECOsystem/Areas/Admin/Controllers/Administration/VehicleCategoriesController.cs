﻿/************************************************************************************************************
*  File    : VehicleCategoriesController.cs
*  Summary : VehicleCategories Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Admin.Models.Identity;
using System.Globalization;
using System.Data;
using System.Collections.Generic;
using ECOsystem.Business.Utilities;
using Newtonsoft.Json;
using ECOsystem.Utilities;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Vehicle Categories Controller. Implementation of all action results from views
    /// </summary>
    public class VehicleCategoriesController : Controller
    {

        /// <summary>
        /// VehicleCategories Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new VehicleCategoriesBusiness())
            {
                var model = new VehicleCategoriesBase();
                if(Session["VehicleCategoriesInfo"] == null)
                {
                    model = new VehicleCategoriesBase()
                    {
                        Data = new VehicleCategories(),
                        List = business.RetrieveVehicleCategories(null)
                    };
                    Session["VehicleCategoriesInfo"] = model.List;
                }
                else
                {
                    model.List = (List<VehicleCategories>)Session["VehicleCategoriesInfo"];
                    model.Data = new VehicleCategories();
                }                
                return View(model);
            }

        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleCategories
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehicleCategories(VehicleCategories model)
        {
            try
            {
                using (var business = new VehicleCategoriesBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditVehicleCategories(model);
                    }
                    var list = business.RetrieveVehicleCategories(null);
                    Session["VehicleCategoriesInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCategories")
                });
            }
        }

        /// <summary>
        /// Search VehicleCategories
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult SearchVehicleCategories(string key)
        {
            try
            {
                using (var business = new VehicleCategoriesBusiness())
                {
                    var model = business.RetrieveVehicleCategories(null,true,key);
                    Session["VehicleCategoriesInfo"] = model;
                    return PartialView("_partials/_Grid", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCategories")
                });
            }
        }

        /// <summary>
        /// Load VehicleCategories
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadVehicleCategories(int id)
        {
            try
            {
                if (id < 0)
                {
                    using (var business = new FuelsBusiness())
                    {
                        var tmpFuels = business.RetrieveFuels(null, ECOsystem.Utilities.Session.GetCountryId());
                        return PartialView("_partials/_Detail", new VehicleCategories() { FuelsList = tmpFuels.Select(r => new FuelsByVehicleCategory() { FuelId = r.FuelId, FuelName = r.Name, IsFuelChecked = false }).ToList() });
                    }
                }
                using (var business = new VehicleCategoriesBusiness())
                {
                    var model = business.RetrieveVehicleCategories(id).FirstOrDefault();
                    model.DefaultPerformanceStr = ((decimal)model.DefaultPerformance).ToString("N", CultureInfo.CreateSpecificCulture("es-CR"));
                    model.WeightStr = ((decimal)model.Weight).ToString("N", CultureInfo.CreateSpecificCulture("es-CR"));

                    return PartialView("_partials/_Detail", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCategories")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleCategories
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteVehicleCategories(int id)
        {
            try
            {
                using (var business = new VehicleCategoriesBusiness())
                {
                    business.DeleteVehicleCategories(id);
                    var list = business.RetrieveVehicleCategories(null);
                    Session["VehicleCategoriesInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "VehicleCategories")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "VehicleCategories")
                    });
                }
            }
            
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            using (var business = new VehicleCategoriesBusiness())
            {
                Session["Reporte"] = business.ReportData((List<VehicleCategories>)Session["VehicleCategoriesInfo"]);

                var model = new VehicleCategoriesBase
                {
                    Data = new VehicleCategories(),
                    List = (List<VehicleCategories>)Session["VehicleCategoriesInfo"]
                };
                return View("Index", model);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Categorias de Vehículos");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "VehicleCategoriesReport", this.ToString().Split('.')[2], "Reporte de Categorias de Vehículos");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var model = new VehicleCategoriesBase
                {
                    Data = new VehicleCategories(),
                    List = (List<VehicleCategories>)Session["VehicleCategoriesInfo"]
                };
                return View("Index", model);
            }                        
        }
    }
}