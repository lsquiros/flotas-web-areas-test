﻿using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Models.Core;
using ECOsystem.Models.Miscellaneous;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class AlarmMaintenanceController : Controller
    {
        private const int cAlarmTriggerIdStopTime = 508;
        private const int cAlarmTriggerIdOutsideSchedule = 502;
        private const int cAlarmTriggerIdExpiredLicense = 504;

        private const int cEntityTypeIdVehicle = 400;
        private const int cEntityTypeIdDriver = 401;
        private const int cEntityTypeIdLawSpeed = 405;
        private const int cEntityTypeIdCompanySpeed = 406;
        private const int cEntityTypeIdPanicBotton = 407;

        // GET: AlarmMaintenance
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Load Alarm
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alarmTriggerId"></param>
        /// <param name="entityTypeId"></param>
        /// <param name="title"></param>
        /// <param name="entityTypeIdLawSpeed"></param>
        /// <param name="entityTypeIdCustomerSpeed"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult LoadAlarmMaintenance(int id, string codView, ECOsystem.Models.Core.AlarmsModels alarm = null)
        {
            try
            {
                using (var business = new ECOsystem.Business.Core.AlarmBusiness())
                {
                    AlarmBase Model = new AlarmBase();
                    Model.ListAlarm = new List<AlarmsModels>();
                    Model.ListAlarm = business.RetrieveAlarm(AlarmTriggerId: null, CustomerId: ECOsystem.Utilities.Session.GetCustomerId(),
                                                             EntityTypeId: null, AlarmId: null, EntityId: id, CodView: codView);

                    AlarmPropertiesModel propertiesAlarm = new AlarmPropertiesModel();
                    propertiesAlarm.CodView = codView;
                    Model.ListPropertiesAlarm = LoadPropertiesAlarm(propertiesAlarm);


                    foreach (AlarmsModels alarmTemp in Model.ListAlarm)
                    {
                        if (alarmTemp.AlarmTriggerId == cAlarmTriggerIdStopTime)//StopTime
                        {
                            using (var vehicleBusiness = new VehiclesBusiness())
                            {
                                alarmTemp.MaxTime = vehicleBusiness.RetrieveParametersByVehicle(id);
                            }
                        }
                    }


                    return PartialView("~/Views/Shared/_AlarmMaintenance.cshtml", Model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "GeoFences")
                });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddOrEditAlarmMaintenance(List<AlarmModel> pListAlarm) // ECOsystem.Models.Core.AlarmsModels model)
        {
            try
            {
                foreach (AlarmModel alarm in pListAlarm)
                {
                    alarm.CustomerId = Convert.ToInt32(ECOsystem.Utilities.Session.GetCustomerId());
                    alarm.EntityTypeId = 0;

                    if (alarm.AlarmTriggerId == cAlarmTriggerIdStopTime)//StopTime
                        alarm.EntityTypeId = cEntityTypeIdVehicle;
                    else if (alarm.AlarmTriggerId == cAlarmTriggerIdOutsideSchedule)//Fuera de Horario
                        alarm.EntityTypeId = cEntityTypeIdDriver;
                    else if (alarm.AlarmTriggerId == cAlarmTriggerIdExpiredLicense)//Licencia Vencida
                        alarm.EntityTypeId = cEntityTypeIdDriver;

                    if (alarm.TypeSpeed == 1)//LawSpeed
                        alarm.EntityTypeId = cEntityTypeIdLawSpeed;
                    if (alarm.TypeSpeed == 2)//CompanySpeed
                        alarm.EntityTypeId = cEntityTypeIdCompanySpeed;

                    if (alarm.Phone == null || alarm.Phone == "")
                        alarm.Phone = "";
                    else
                    {
                        alarm.SMS = true;
                        if (alarm.Email == null)
                            alarm.Email = "";
                    }

                    if ((alarm.Email != null && alarm.Email != "") || (alarm.Phone != null && alarm.Phone != ""))
                    {
                        using (var business = new ECOsystem.Business.Core.AlarmBusiness())
                        {
                            business.AddOrEditAlarm(alarm);
                        }

                        if (alarm.AlarmTriggerId == cAlarmTriggerIdStopTime)//StopTime
                        {
                            using (var VehicleBusiness = new VehiclesBusiness())
                            {
                                VehicleParameters vehicle = new VehicleParameters();
                                vehicle.VehicleId = alarm.EntityId;
                                if (alarm.MaxTime == null)
                                    vehicle.GPSElapseTime = 0;
                                else
                                    vehicle.GPSElapseTime = alarm.MaxTime;
                                VehicleBusiness.AddOrEditParameterByVehicle(vehicle);
                            }
                        }
                    }
                }

                return Json("success");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "GeoFences")
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //[HttpPost]
        public List<AlarmPropertiesModel> LoadPropertiesAlarm(AlarmPropertiesModel propertiesAlarm)
        {
            List<AlarmPropertiesModel> listPropertiesAlarm = new List<AlarmPropertiesModel>();
            try
            {
                using (var business = new ECOsystem.Business.Core.AlarmBusiness())
                {
                    listPropertiesAlarm = business.AlarmPropertiesRetrieve(propertiesAlarm);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);
            }
            return listPropertiesAlarm;
        }
        
        /// <summary>
        /// Load Alarm
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alarmTriggerId"></param>
        /// <param name="entityTypeId"></param>
        /// <param name="title"></param>
        /// <param name="entityTypeIdLawSpeed"></param>
        /// <param name="entityTypeIdCustomerSpeed"></param>
        /// <returns></returns>
        [HttpPost]
        public AlarmDetail LoadAlarmDetail(int? id, string codView, ECOsystem.Models.Core.AlarmsModels alarm = null)
        {
            try
            {
                using (var business = new ECOsystem.Business.Core.AlarmBusiness())
                {
                    AlarmBase ModelB = new AlarmBase();
                    AlarmDetail Model = new AlarmDetail();
                    ModelB.ListAlarm = new List<AlarmsModels>();
                    ModelB.ListAlarm = business.RetrieveAlarm(AlarmTriggerId: alarm.AlarmTriggerId, CustomerId: ECOsystem.Utilities.Session.GetCustomerId(),
                        EntityTypeId: null, AlarmId: null, EntityId: id, CodView: codView);

                    AlarmPropertiesModel propertiesAlarm = new AlarmPropertiesModel();
                    propertiesAlarm.CodView = codView;
                    ModelB.ListPropertiesAlarm = LoadPropertiesAlarm(propertiesAlarm);

                    Model.Alarm = ModelB.ListAlarm[0];
                    Model.PropertAlarm = ModelB.ListPropertiesAlarm[0];

                    return Model;
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return null;
            }
        }

        [HttpPost]
        public ActionResult GeneralAlarmAddOrEdit(AlarmModel alarm)
        {
            try
            {
                alarm.CustomerId = Convert.ToInt32(ECOsystem.Utilities.Session.GetCustomerId());
                alarm.EntityTypeId = cEntityTypeIdPanicBotton;
                alarm.EntityId = Convert.ToInt32(ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId);
                alarm.LoggedUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId;
                alarm.Active = true;

                if (alarm.Phone == null || alarm.Phone == "")
                    alarm.Phone = "";
                else
                {
                    alarm.SMS = true;
                    if (alarm.Email == null)
                        alarm.Email = "";
                }

                if ((alarm.Email != null && alarm.Email != "") || (alarm.Phone != null && alarm.Phone != ""))
                {
                    using (var business = new ECOsystem.Business.Core.AlarmBusiness())
                    {
                        business.AddOrEditAlarm(alarm);
                        return Json("Success");
                    }
                }
                else return Json("Error");
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json("Error");
            }
        }

    }
}
