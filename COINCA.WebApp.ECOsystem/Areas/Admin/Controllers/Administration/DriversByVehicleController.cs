﻿/************************************************************************************************************
*  File    : DriversByVehicleController.cs
*  Summary : DriversByVehicle Controller Actions
*  Author  : Gerald Solano
*  Date    : 21/07/2015
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Admin.Business.Administration;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using  ECOsystem.Areas.Admin.Models.Identity;
using System.Collections.Generic;
using ECOsystem.Models.Account;
using System.Web.Script.Serialization;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// DriversByVehicle Controller. Implementation of all action results from views
    /// </summary>
    public class DriversByVehicleController : Controller
    {

        /// <summary>
        /// VehiclesByGroup Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            var model = new DriversByVehicleBase
                {
                    VehicleId = null,
                    Data = new DriversByVehicleData(),
                    Menus = new List<AccountMenus>()
                };
            return View(model);
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehiclesByGroup
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditDriversByVehicle(DriversByVehicleData model)
        {
            try
            {
                using (var business = new DriversByVehicleBusiness())
                {
                    business.AddOrEditDriversByVehicle(model);
                    var user = ECOsystem.Utilities.Session.GetUserInfo();
                    #region Event log
                    //Add log event
                    AddLogEvent(
                            message: string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model)),
                            userId: (user != null) ? user.UserId : 0,
                            customerId: (user != null) ? user.CustomerId : 0,
                            partnerId: (user != null) ? user.PartnerId : 0,
                            exception: null,
                            isInfo: true);
                    #endregion
                    return PartialView("_partials/_List", business.RetrieveDriversByVehicle(model.VehicleId));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "DriversByVehicle")
                });
            }
        }


        /// <summary>
        /// Load DriversByVehicle
        /// </summary>
        /// <param name="id">The drivers by vehicle group Id to find all drivers associated</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult LoadDriversByVehicle(int id)
        {
            try
            {
                using (var business = new DriversByVehicleBusiness())
                    return PartialView("_partials/_List", business.RetrieveDriversByVehicle(id));
            }
            catch (Exception e)
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "DriversByVehicle")
                });
            }
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            ECOsystem.Business.Utilities.EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}