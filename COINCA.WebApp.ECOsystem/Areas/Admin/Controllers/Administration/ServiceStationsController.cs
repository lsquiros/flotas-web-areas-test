﻿using  ECOsystem.Areas.Admin.Business.Administration;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Account;
using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Models.Miscellaneous;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using ECOsystem.Utilities;
using ECOsystem.Models.Core;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{    /// <summary>
    ///  ServiceStationsController
    /// </summary>
    public class ServiceStationsController : Controller
    {
        PartnerNewsBusiness business = new PartnerNewsBusiness();
        ///<Summary>
        /// Get the Index
        ///</Summary>
        [EcoAuthorize]
        public ActionResult Index()
        {
            var model = new ServiceStationsBase();
            Session["ServiceStationToBlocked"] = null;
            if (Session["ServiceStationInfo"] == null)
            {
                model = RetriveData(null);
                Session["ServiceStationInfo"] = model.List;
                Session["ServiceStationListAll"] = model.List;
            }
            else
            {
                model.List = (List<ServiceStations>)Session["ServiceStationInfo"];
                model.Data = new ServiceStations();
                model.Menus = new List<AccountMenus>();
            }
            return View(model);
        }

        /// <summary>
        /// AddOrEditServiceStations
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [EcoAuthorize]
        public PartialViewResult AddOrEditServiceStations(ServiceStations model)
        {
            try
            {
                using (var business = new ServiceStationsBusiness())
                {
                    model.CostCenterList = ((List<ServiceStations>)Session["ServiceStationInfo"]).Where(x => x.ServiceStationId == model.ServiceStationId).Select(x => x.CostCenterList).FirstOrDefault();
                    business.ServiceStationsAddOrEdit(model);                 
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificaciones en estaciones de servicio, datos: {0}", new JavaScriptSerializer().Serialize(model)));
                    var list = RetriveData(null, null).List;
                    Session["ServiceStationInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ServiceStations")
                });
            }
        }

        /// <summary>
        /// LoadServiceStations
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [EcoAuthorize]
        public ActionResult LoadServiceStations(int id)
        {
            try
            {
                if (id < 0)
                {
                    var modal = new ServiceStations();
                    return PartialView("_partials/_AddOrEdit", modal);
                }
                var nmodal = RetriveData(id, null).List.FirstOrDefault();
                using (var bus = new ServiceStationsBusiness())
                {
                    nmodal.CostCenterList = bus.RetrieveBlockedCostCenter(id).ToList();
                }
                ((List<ServiceStations>)Session["ServiceStationInfo"]).Where(x => x.ServiceStationId == id).ToList().ForEach(x => x.CostCenterList = nmodal.CostCenterList);
                return PartialView("_partials/_AddOrEdit", nmodal);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ServiceStations")
                });
            }
        }

        /// <summary>
        /// SearchServiceStations
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult SearchServiceStations(string key)
        {
            try
            {
                var model = RetriveData(null, key).List;
                Session["ServiceStationInfo"] = model;
                Session["ServiceStationListAll"] = RetriveData(null).List;
                return PartialView("_partials/_Grid", model);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ServiceStations")
                });
            }
        }

        [HttpPost]
        public PartialViewResult AddBlockedCostCenter(ServiceStations model)
        {
            try
            {
                List<BlockedCostsCenter> item = ((List<ServiceStations>)Session["ServiceStationInfo"]).Where(x => x.ServiceStationId == model.ServiceStationId).Select(x => x.CostCenterList).FirstOrDefault();
                if(!item.Where(x => x.CostCenterId == model.CostCenterId).Any())
                {
                    if (item.Where(x => x.CostCenterId == -1).Any())
                    {
                        item.Remove(item.Where(x => x.CostCenterId == -1).Select(x => x).FirstOrDefault());
                    }
                    else if (model.CostCenterId == -1)
                    {
                        item = new List<BlockedCostsCenter>();                        
                    }
                    item.Add(new BlockedCostsCenter() { CostCenterId = model.CostCenterId, CostCenterName = model.CostCenterName });
                    ((List<ServiceStations>)Session["ServiceStationInfo"]).Where(x => x.ServiceStationId == model.ServiceStationId).ToList().ForEach(x => x.CostCenterList = item);
                }
                
                return PartialView("_partials/_CostCenterGrid", item);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ServiceStations")
                });
            }
        }
        
        public PartialViewResult DeleteBlockedCostCenter(int costCenterId, int serviceStationId)
        {
            try
            {
                List<BlockedCostsCenter> item = ((List<ServiceStations>)Session["ServiceStationInfo"]).Where(x => x.ServiceStationId == serviceStationId).Select(x => x.CostCenterList).FirstOrDefault();
                item.Remove(item.Where(x => x.CostCenterId == costCenterId).Select(x => x).FirstOrDefault());
                ((List<ServiceStations>)Session["ServiceStationInfo"]).Where(x => x.ServiceStationId == serviceStationId).ToList().ForEach(x => x.CostCenterList = item);
                return PartialView("_partials/_CostCenterGrid", item);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ServiceStations")
                });
            }
        }

        /// <summary>
        /// LoadStates
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadStates(int countryId)
        {
            try
            {
                var list = ServiceStationsBusiness.GetStates(countryId);

                if (list == null) return Json(null);

                var statesList = list.ToList();

                return Json(statesList.AsEnumerable(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json(null);
            }
        }

        /// <summary>
        /// LoadCantons
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadCantons(int stateId, int countryId)
        {
            try
            {
                var list = ServiceStationsBusiness.GetCounties(stateId, countryId);

                if (list == null) return Json(null);

                List<ECOsystem.Models.Core.Counties> statesList = (List<ECOsystem.Models.Core.Counties>)list.ToList();

                return Json(statesList.AsEnumerable(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json(null);
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Vehicles
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteServiceStatios(int id)
        {
            try
            {
                using (var business = new ServiceStationsBusiness())
                {
                    business.DeleteServiceStations(id);

                    string message = string.Format("[Delete] ServiceStationId:", new JavaScriptSerializer().Serialize(id));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, message);
                    var list = RetriveData(null, null).List;
                    Session["ServiceStationInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Vehicles")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "ServiceStations")
                    });
                }
            }
        }
                
        private ServiceStationsBase RetriveData(int? id, string key = null)
        {
            int? countryId = null;
            int? customerid = null;
            int? pPartnerId = null; //ECOsystem.Utilities.Session.GetPartnerId();

            if (ECOsystem.Utilities.Session.GetCustomerInfo() != null)
            {
                var cus = ECOsystem.Utilities.Session.GetCustomerInfo();
                customerid = ECOsystem.Utilities.Session.GetCustomerId();
                countryId = cus.CountryId;
            }

            var model = new ServiceStationsBase
            {
                Data = new ServiceStations(),
                List = ServiceStationsBusiness.ServiceStationRetrieve(id, countryId, pPartnerId, customerid, key),
                Menus = new List<AccountMenus>()
            };
            return model;
        }
        
        /// <summary>
        /// GetServiceStationName
        /// </summary>
        /// <param name="bacAfiliado"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetServiceStationName(string bacAfiliado)
        {
            var service = RetriveData(null, bacAfiliado).List.FirstOrDefault();

            if (service != null)
                return Json(service.Name, JsonRequestBehavior.AllowGet);
            else
                return Json(null, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            using (var business = new ServiceStationsBusiness())
            {
                Session["Reporte"] = business.DownloadData((List<ServiceStations>)Session["ServiceStationInfo"]);
                var model = new ServiceStationsBase
                {
                    Data = new ServiceStations(),
                    List = (List<ServiceStations>)Session["ServiceStationInfo"]
                };
                return View("Index", model);
            }
        }

        /// <summary>
        /// Download the Excel File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte Estaciones de Servicio");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "ServiceStationReport", this.ToString().Split('.')[2], "Reporte de Estaciones de Servicio");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var model = new ServiceStationsBase
                {
                    Data = new ServiceStations(),
                    List = (List<ServiceStations>)Session["ServiceStationInfo"]
                };
                return View("Index", model);
            }
        }

        #region Validates Service Stations Blocked
        public ActionResult GetServiceStationToBlocked(int ServiceStationId, bool IsBlocked)
        {
            try
            {
                var listAll = Session["ServiceStationListAll"] != null ? (List<ServiceStations>)Session["ServiceStationListAll"] : new List<ServiceStations>();
                listAll.Where(x => ServiceStationId == -1 || x.ServiceStationId == ServiceStationId).ToList().ForEach(x => x.IsBlocked = IsBlocked);
                Session["ServiceStationListAll"] = listAll;

                return Json(listAll.Where(x => x.IsBlocked).Count().ToString() + "|" + listAll.Where(x => x.IsBlocked == false).Count().ToString(), JsonRequestBehavior.AllowGet);                
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult BlockServiceStations()
        {
            try
            {
                using(var bus = new ServiceStationsBusiness())
	            {
                    var list = (List<ServiceStations>)Session["ServiceStationListAll"];
                    bus.BlockServiceStations(list.Where(x => x.IsBlocked).Select(x => (int)x.ServiceStationId).ToList());
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Bloqueo de estaciones de servicio, estaciones {0}", new JavaScriptSerializer().Serialize(list.Where(x => x.IsBlocked).Select(x => (int)x.ServiceStationId).ToList())));
                    Session["ServiceStationInfo"] = (List<ServiceStations>)Session["ServiceStationListAll"];
                    return PartialView("_partials/_Grid", list);                    
	            }                
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "ServiceStations")
                });
            }
        }
        #endregion
        #region MapsServiceStations

        /// <summary>
        /// Show the partial view with the maps
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ShowMapsServiceStations(int? id)
        {
            String img = business.RetrieveImageStation(RetrievePartnerId());
            if (img != null)
                ViewBag.imageS = img;

            return PartialView("_partials/_Maps");
        }

        ///<Summary>
        /// Get the Index
        ///</Summary>
        [HttpPost]
        public ActionResult Maps(HttpPostedFileBase file)
        {
            if (file.ContentLength > 0)
            {
                MemoryStream ms = new MemoryStream();
                Image img = Image.FromStream(file.InputStream);


                float origWidth = img.Width;
                float origHeight = img.Height;
                float sngRatio = origWidth / origHeight;
                float newWidth = 400;
                float newHeight = newWidth / sngRatio;
                Bitmap newBMP = new Bitmap(img, Convert.ToInt32(newWidth), Convert.ToInt32(newHeight));
                Graphics oGraphics = Graphics.FromImage(newBMP);
                oGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                oGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                oGraphics.DrawImage(img, 0, 0, newWidth, newHeight);

                img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] imageArray = ms.ToArray();

                business.AddOrEditImageStation(RetrievePartnerId(), imageArray);
            }

            return RedirectToAction("Index");
        }


        ///<Summary>
        /// Holder   
        ///</Summary>      

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
            ms.Position = 0; // this is important
            return Image.FromStream(ms, true);
        }

        public int? RetrievePartnerId()
        {
            int? partnerId = ECOsystem.Utilities.Session.GetPartnerId();
            if (partnerId != null)
                return partnerId;
            else
            {
                int id = ECOsystem.Utilities.Session.GetUserInfo().LoggedUserId ?? 0;
                int? PartnerOfUserId = business.RetrievePartnerId(id);
                return PartnerOfUserId;
            }

        }
        #endregion

    }
}
