﻿using ECOsystem.Business;
using ECOsystem.Business.Utilities;
using ECOsystem.Models;
using ECOsystem.Models.Account;
using ECOsystem.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class ProgramSendReportController : Controller
    {
        /// <summary>
        /// Main view. Get program reports config
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var bus = new ProgramSendReportsBusiness())
            {
                var list = bus.ProgramSendReportsRetrieve();
                var model = new ProgramSendReportsBase()
                {
                    List = list,
                    Menus = new List<AccountMenus>()
                };
                
                List<ProgramSendReportModule> ReportsByModules = new List<ProgramSendReportModule>();
                IEnumerable<ProgramSendReportsShow> reportsTemp = new List<ProgramSendReportsShow>();
                ProgramSendReportModule module = new ProgramSendReportModule();
                SelectList selectedValues;
                List<SelectListItem> values;

                //var queryReports = bus.ProgramSendReportCustomer_Retrieve();
                var customer = ECOsystem.Utilities.Session.GetCustomerInfo();
                
                if (customer != null)
                {
                    if (customer.ModuleManagement) {
                        reportsTemp = list.Where(x => x.Module == "G");
                        selectedValues = new SelectList(reportsTemp.Where(p => p.ReportId != -1).Select(q => new { q.ReportId, q.DownloadName } ).Distinct(), "ReportId", "DownloadName");
                        values = selectedValues.ToList();
                        values.Insert(0, new SelectListItem { Text = "Todos", Value = "-1" });

                        module = new ProgramSendReportModule {
                            Reports = new SelectList(values, "Value", "Text"),
                            Name = "Gestión",
                            Module = 'G',
                            Data = reportsTemp.Where(q => !string.IsNullOrEmpty(q.Emails) || !string.IsNullOrEmpty(q.UserEmail) || q.UserId == -1)
                        };
                        ReportsByModules.Add(module);
                    }

                    if (customer.ModuleEfficiency)
                    {
                        reportsTemp = list.Where(x => x.Module == "E");
                        selectedValues = new SelectList(reportsTemp.Where(p => p.ReportId != -1).Select(q => new { q.ReportId, q.DownloadName }).Distinct(), "ReportId", "DownloadName");
                        values = selectedValues.ToList();
                        values.Insert(0, new SelectListItem { Text = "Todos", Value = "-1" });

                        module = new ProgramSendReportModule
                        {
                            Reports = new SelectList(values, "Value", "Text"),
                            Name = "Eficiencia",
                            Module = 'E',
                            Data = reportsTemp.Where(q => !string.IsNullOrEmpty(q.Emails) || !string.IsNullOrEmpty(q.UserEmail) || q.UserId == -1)
                        };
                        ReportsByModules.Add(module);
                    }

                    if (customer.ModuleControl)
                    {
                        reportsTemp = list.Where(x => x.Module == "C");
                        selectedValues = new SelectList(reportsTemp.Where(p => p.ReportId != -1).Select(q => new { q.ReportId, q.DownloadName }).Distinct(), "ReportId", "DownloadName");
                        values = selectedValues.ToList();
                        values.Insert(0, new SelectListItem { Text = "Todos", Value = "-1" });

                        module = new ProgramSendReportModule
                        {
                            Reports = new SelectList(values, "Value", "Text"),
                            Name = "Control",
                            Module = 'C',
                            Data = reportsTemp.Where(q => !string.IsNullOrEmpty(q.Emails) || !string.IsNullOrEmpty(q.UserEmail) || q.UserId == -1)
                        };
                        ReportsByModules.Add(module);
                    }

                    if (customer.ModuleOperation)
                    {
                        reportsTemp = list.Where(x => x.Module == "O");
                        selectedValues = new SelectList(reportsTemp.Where(p => p.ReportId != -1).Select(q => new { q.ReportId, q.DownloadName }).Distinct(), "ReportId", "DownloadName");
                        values = selectedValues.ToList();
                        values.Insert(0, new SelectListItem { Text = "Todos", Value = "-1" });

                        module = new ProgramSendReportModule
                        {
                            Reports = new SelectList(values, "Value", "Text"),
                            Name = "Operación",
                            Module = 'O',
                            Data = reportsTemp.Where(q => !string.IsNullOrEmpty(q.Emails) || !string.IsNullOrEmpty(q.UserEmail) || q.UserId == -1)
                        };
                        ReportsByModules.Add(module);
                    }
                    reportsTemp = list.Where(x => x.Module == "A");
                    selectedValues = new SelectList(reportsTemp.Where(p => p.ReportId != -1).Select(q => new { q.ReportId, q.DownloadName }).Distinct(), "ReportId", "DownloadName");
                    values = selectedValues.ToList();
                    values.Insert(0, new SelectListItem { Text = "Todos", Value = "-1" });

                    module = new ProgramSendReportModule
                    {
                        Reports = new SelectList(values, "Value", "Text"),
                        Name = "Administración",
                        Module = 'A',
                        Data = reportsTemp.Where(q => !string.IsNullOrEmpty(q.Emails) || !string.IsNullOrEmpty(q.UserEmail) || q.UserId == -1)
                    };
                    ReportsByModules.Add(module);

                    model.ReportsByModules = ReportsByModules;
                }
                return View(model);
            }
        }

        /// <summary>
        /// Performs the maintenance insert or update of the entity ProgramSendReports
        /// </summary>
        /// <param name="model">Model or class passed as parameter which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public ActionResult SendProgramReportAddOrEdit(ProgramSendReports model)
        {
            try
            {
                using (var bus = new ProgramSendReportsBusiness())
                {
                    bus.ProgramSendReportAddOrEdit(model);
                    var List = bus.ProgramSendReportsRetrieve();
                    IEnumerable<ProgramSendReportsShow> reportsTemp = List.Where(x => x.Module == model.Module);
                    SelectList selectedValues = new SelectList(reportsTemp.Where(p => p.ReportId != -1).Select(q => new { q.ReportId, q.DownloadName }).Distinct(), "ReportId", "DownloadName");
                    List<SelectListItem> values = selectedValues.ToList();
                    values.Insert(0, new SelectListItem { Text = "Todos", Value = "-1" });

                    ProgramSendReportModule module = new ProgramSendReportModule
                    {
                        Reports = new SelectList(values, "Value", "Text"),
                        Module = model.Module.Length > 0 ? model.Module.ToCharArray()[0] : 'A',
                        Data = reportsTemp.Where(q => !string.IsNullOrEmpty(q.Emails) || !string.IsNullOrEmpty(q.UserEmail) || q.UserId == -1)
                    };

                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Modificacion de los reportes programdos. Detalles: {0}", new JavaScriptSerializer().Serialize(List)));
                    return PartialView("_partials/_ModuleConfiguration", module);
                }
            }
            catch (Exception e)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al modificar los reportes programados. Detalle: {0}", e.Message));
                return Json(e.Message);
            }
        }

        /// <summary>
        ///  Get program reports config
        /// </summary>
        /// <param name="Id">Id for program report user configuration</param>
        /// <returns>A json response with program report configuration</returns>
        [HttpPost]
        public ActionResult GetData(int? Id)
        {
            using (var bus = new ProgramSendReportsBusiness())
            {
                var report = bus.ProgramSendReportsRetrieve().Where(x => x.Id == Id).FirstOrDefault();
                return Json(report, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Delete a specific programreport
        /// </summary>
        /// <param name="Id">Id for program report user configuration</param>
        /// <param name="ReportId">Id for program report</param>
        /// <returns>Success or fail status</returns>
        public ActionResult DeleteProgramReport(string Id, int? ReportId)
        {
            try
            {
                using (var bus = new ProgramSendReportsBusiness())
                {
                    bus.DeleteProgramReport(ReportId, Convert.ToInt32(Id));
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se eliminó el reporte. ReportId: {0} | Id: {1}", ReportId, Id));

                    Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                    return Json(ReportId, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Eliminación de reporte Falló. ReportId: {0}", Id));
                Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                return Json(ex.Message);
            }
        }
    }
}