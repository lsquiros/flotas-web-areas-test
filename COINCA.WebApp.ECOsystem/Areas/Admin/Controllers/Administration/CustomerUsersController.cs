﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem.Business.Core;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Models.Core;
using Microsoft.AspNet.Identity.Owin;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using System.Web.Script.Serialization;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Data;
using System.Collections.Generic;

namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class CustomerUsersController : Controller
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? (_userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());
            }
            private set
            {
                _userManager = value;
            }
        }

        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new UsersBusiness())
            {
                var model = new UsersBase
                {
                    Data = new Users(),
                    List = business.RetrieveUsers(null, customerId: ECOsystem.Utilities.Session.GetCustomerId(), IsCustomer: true)
                };
                Session["UsersInfo"] = model.List;
                return View(model);
            }            
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditUsers(Users model)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    business.AddOrEditUsers(model, UserManager, Url, Request);

                    #region Event log
                    var userId = ECOsystem.Utilities.Session.GetUserInfo();
                    //Add log event
                    AddLogEvent(
                            message: string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model)),
                            userId: (userId != null) ? userId.UserId : 0,
                            customerId: (userId != null) ? userId.CustomerId : 0,
                            partnerId: (userId != null) ? userId.PartnerId : 0,
                            exception: null,
                            isInfo: true);
                    #endregion
                    var modelV = business.RetrieveUsers(null, customerId: ECOsystem.Utilities.Session.GetCustomerId(), IsCustomer: true);
                    Session["UsersInfo"] = modelV;
                    return PartialView("_partials/_Grid", modelV);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("ERROR EXIST_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Usuario ya existe",
                        TechnicalError = "El usuario con el correo especificado ya existe en el sistema.",
                        ReturnUlr = Url.Action("Index", "CustomerUsers")
                    });

                }
                else if (e.Message.Contains("ERROR UPDT_USR"))
                {
                    return PartialView("~/Views/Shared/_PartialGlobalErrorMessage.cshtml", new CustomError()
                    {
                        TitleError = "Error de Actualización",
                        TechnicalError = "El usuario que se desea actualizar presenta inconsistencias, por favor intente de nuevo el proceso de actualización.",
                        ReturnUlr = Url.Action("Index", "CustomerUsers")
                    });

                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "CustomerUsers")
                    });
                }
            }
        }

        [HttpPost]
        public PartialViewResult SearchUsers(string key, int? statusId)
        {
            try
            {
                UsersBase model = new UsersBase();
                using (var business = new UsersBusiness())
                {
                    model.Parameters.Status = statusId == 2 ? null : statusId;
                    var result = business.RetrieveUsers(null, key, customerId: ECOsystem.Utilities.Session.GetCustomerId(), UserStatus: model.Parameters.Status, IsCustomer: true).ToList();
                    Session["UsersInfo"] = result;
                    return PartialView("_partials/_Grid", result);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CustomerUsers")
                });
            }
        }

        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadUsers(int id)
        {
            if (id < 0)
            {
                return PartialView("_partials/_Detail", new Users { CustomerUser = new CustomerUsers(), IsCustomerUser = true });
            }
            try
            {
                using (var business = new UsersBusiness())
                {
                    var model = business.RetrieveUsers(id, customerId: ECOsystem.Utilities.Session.GetCustomerId(), bindObtPhoto: true, IsCustomer: true, LoadSingleUser: true).FirstOrDefault();

                    return PartialView("_partials/_Detail", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CustomerUsers")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model 
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteUsers(int id)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    business.DeleteUsers(id);
                    #region Event log

                    var user = ECOsystem.Utilities.Session.GetUserInfo();

                    //Add log event
                    AddLogEvent(
                            message: string.Format("[DELETE_USR] Cuenta de Usuario ha sido eliminada. Usuario afectado es UserId = {0}", id),
                            userId: (user != null) ? user.UserId : 0,
                            customerId: (user != null) ? user.CustomerId : 0,
                            partnerId: (user != null) ? user.PartnerId : 0,
                            exception: null,
                            isInfo: true);

                    #endregion
                    var modelV = business.RetrieveUsers(null, customerId: ECOsystem.Utilities.Session.GetCustomerId(), IsCustomer: true);
                    Session["UsersInfo"] = modelV;
                    return PartialView("_partials/_Grid", modelV);
                }
            }


            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Customers")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "CustomerUsers")
                    });
                }
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload()
        {
            using (var business = new UsersBusiness())
            {
                DataTable report = business.GetReportDownloadData();
                Session["Reporte"] = report;
                var modelUsers = new UsersBase
                {
                    Data = new Users(),
                    List = (List<Users>)Session["UsersInfo"]
                };
                return View("Index", modelUsers);
            }
        }

        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Usuarios");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "UsersReport", this.ToString().Split('.')[2], "Reporte de Usuarios");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                var model = new UsersBase
                {
                    Data = new Users(),
                    List = (List<Users>)Session["UsersInfo"]
                };
                Session["UsersInfo"] = model.List;
                return View("Index", model);
            }
        }

        #region Log        
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion
    }
}
