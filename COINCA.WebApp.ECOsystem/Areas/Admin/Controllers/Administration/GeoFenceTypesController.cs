﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Business.Administration;
using ECOsystem.Business.Utilities;
using System.Web.Script.Serialization;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Models.Core;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class GeoFenceTypesController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new GeoFenceTypesBusiness())
            {
                var model = new GeoFenceTypesBase();
                model.List = business.RetrieveGeoFenceTypes();
                Session["GeoFenceTypesList"] = model.List;
                return View(model);
            }
        }

        public PartialViewResult LoadGeoFenceType(int id)
        {
            using (var business = new GeoFenceTypesBusiness())
            {
                var model = business.RetrieveGeoFenceTypes(id).FirstOrDefault();

                if (model == null)
                {
                    model = new GeoFenceTypes();
                }

                return PartialView("_partials/_Detail", model);
            }
        }

        public PartialViewResult AddOrEditGeoFenceTypes(GeoFenceTypes model)
        {
            try
            {
                using (var business = new GeoFenceTypesBusiness())
                {
                    business.AddOrEditGeoFenceType(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format((model.Id >= 1 ? "Agrega" : "Edita") + " Tipo de Geocerca: = {0}, Cliente = {1}, Id de Cliente = {2}",
                                                           new JavaScriptSerializer().Serialize(model), ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    var list = business.RetrieveGeoFenceTypes();
                    Session["GeoFenceTypesList"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                var message = "Error al agregar o editar tipo de geocerca.";
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format(message + " Detalle del error = {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = message, TechnicalError = e.Message }); 
            }            
        }

        public PartialViewResult DeleteGeoFenceType(int id)
        {
            try
            {
                using (var business = new GeoFenceTypesBusiness())
                {
                    business.DeleteGeoFenceType(id);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Elimina Tipo de Geocerca: = {0}, Cliente = {1}, Id de Cliente = {2}",
                                                           id, ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName, ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId));
                    var list = business.RetrieveGeoFenceTypes();
                    Session["GeoFenceTypesList"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                var message = "Error al eliminar tipo de geocerca.";
                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format(message + " Detalle del error = {0}", e.Message));
                return PartialView("~/Views/Shared/_ErrorPartialView.cshtml", new ErrorModels() { Title = message, TechnicalError = e.Message }); 
            }           
        }

        public ActionResult SearchGeoFenceType(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return PartialView("_partials/_Grid", (List<GeoFenceTypes>)Session["GeoFenceTypesList"]);
            }
            else
            {
                return PartialView("_partials/_Grid", ((List<GeoFenceTypes>)Session["GeoFenceTypesList"]).Where(x => x.Name.ToUpper().Contains(key.ToUpper())));
            }
        }
	}
}