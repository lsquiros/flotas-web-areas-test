﻿/************************************************************************************************************
*  File    : ParametersController.cs
*  Summary : Parameters Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Linq;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Models.Miscellaneous;
using System;
using ECOsystem.Audit;
using ECOsystem.Business.Utilities;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Models.Account;
using System.Collections.Generic;




namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Parameters Controller. Implementation of all action results from views
    /// </summary>
    public class CustomerThresholdsController : Controller
    {          

        /// <summary>
        /// Parameters Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new CustomerThresholdsBusiness())
            {
                var model = business.RetrieveThresholds().OrderByDescending(o => o.CodingThresholdsId).FirstOrDefault();
                model.Menus = new List<AccountMenus>();
                return View(model);
            }

        }

        /// <summary>
        /// Post Invocation from View
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index(CustomerThresholds model)
        {
            try
            {
                using (var business = new CustomerThresholdsBusiness())
                {
                    
                    model.CustomerId = ECOsystem.Utilities.Session.GetCustomerId();
                    business.AddOrEditThresholds(model);
                    
                    var modelIndex = business.RetrieveThresholds().OrderByDescending(o => o.CodingThresholdsId).FirstOrDefault();
                    modelIndex.Menus = new List<AccountMenus>();

                    return PartialView("_partials/_Detail", modelIndex);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "CustomerThresholds")
                });
            }
            
        }

        #region Log

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {

            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }

        #endregion

    }
}