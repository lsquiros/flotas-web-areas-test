﻿using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class CustomReportController : Controller
    {        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadReport(int? CReportId)
        {
            try
            {
                using (var bus = new CustomReportsBusiness())
                {
                    return PartialView("_partials/_Detail", bus.CustomReportRetrive(CReportId));
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult ReportAddOrEdit(CustomReport model)
        {
            try
            {
                using (var bus = new CustomReportsBusiness())
                {
                    if (model.SheetName == null)
                        model.SheetName = "Hoja 1";

                    bus.CustomReportAddOrEdit(model);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Creación o modificación de Reporte Personalizado, Datos: {0}", new JavaScriptSerializer().Serialize(model)));
                    return Json("Sucess", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la Creación o modificación de Reporte Personalizado, detalle del error: {0}", e.Message));
                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }            
        }


        public ActionResult ReportDelete(int? Id)
        {
            try
            {
                using (var bus = new CustomReportsBusiness())
                {
                    bus.CustomReportDelete(Id);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Eliminación de Reporte Personalizado, Datos: {0}", new JavaScriptSerializer().Serialize(Id)));
                    return Json("Sucess", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la Creación o modificación de Reporte Personalizado, detalle del error: {0}", e.Message));
                return Json(string.Format("Error|{0}", e.Message), JsonRequestBehavior.AllowGet);
            }
        }
        
        #region DropDownData
        public ActionResult GetColumnsValues(int? ResourceId, bool? IsFormula)
        {
            return Json(CustomReportsCollections.GetReportColumns(ResourceId, IsFormula), JsonRequestBehavior.AllowGet);
        }
        #endregion


    }
}