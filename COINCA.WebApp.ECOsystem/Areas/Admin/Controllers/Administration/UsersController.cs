﻿/************************************************************************************************************
*  File    : UsersController.cs
*  Summary : Users Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Models.Miscellaneous;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ECOsystem.Business.Utilities;
using ECOsystem.Audit;
using ECOsystem;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using ECOsystem.Business.Core;
using  ECOsystem.Areas.Admin.Business.Administration;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    /// <summary>
    /// Users Controller. Implementation of all action results from views
    /// </summary>
    public class UsersController : Controller
    {

        private ApplicationUserManager _userManager;

        /// <summary>
        /// Asp Net User Manager
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get
            {
                if (_userManager == null)
                {
                    _userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                }
                return _userManager;
            }
            private set
            {
                _userManager = value;
            }
        }


        /// <summary>
        /// Users Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
       // [Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var business = new UsersBusiness())
            {
                var model = new UsersBase
                {
                    Data = new Users(),
                    List = business.RetrieveUsers(null, customerId: ECOsystem.Utilities.Session.GetCustomerId())
                };
                 Session["UsersInfo"] = model.List;
                return View(model);
            }
        }
        

        /// <summary>
        /// Search Users
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public PartialViewResult SearchUsers(string key, int? statusId)
        {
            try
            {
                UsersBase model = new UsersBase();
                using (var business = new UsersBusiness())
                {                    
                    var list = business.RetrieveUsers(null, key, customerId: ECOsystem.Utilities.Session.GetCustomerId());
                    Session["UsersInfo"] = list;
                    return PartialView("_partials/_Grid", list);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Users")
                });
            }
        }

        /// <summary>
        /// Load Users
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        //[Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        public PartialViewResult LoadUsers(int id)
        {
            if (id < 0)
            {
                return PartialView("_partials/_Detail", new Users());
            }
            try
            {
                using (var business = new UsersBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveUsers(id, customerId: ECOsystem.Utilities.Session.GetCustomerId()).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "Users")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model 
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
       // [Authorize(Roles = "CUSTOMER_ADMIN, SUPER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult DeleteUsers(int id)
        {
            try
            {
                using (var business = new UsersBusiness())
                {
                    business.DeleteUsers(id);

                    #region Event log

                    var user = ECOsystem.Utilities.Session.GetUserInfo();

                    //Add log event
                    AddLogEvent(
                            message: string.Format("[DELETE_USR] Cuenta de Usuario ha sido eliminada. Usuario afectado es UserId = {0}", id),
                            userId: (user != null) ? user.UserId : 0,
                            customerId: (user != null) ? user.CustomerId : 0,
                            partnerId: (user != null) ? user.PartnerId : 0,
                            exception: null,
                            isInfo: true);

                    #endregion

                    return PartialView("_partials/_Grid", business.RetrieveUsers(null, customerId: ECOsystem.Utilities.Session.GetCustomerId()));
                }
            }


            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                //Add log event
                new EventLogBusiness().AddLogEvent(LogState.ERROR, e.Message);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "Users")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "Users")
                    });
                }
            }

        }

        /// <summary>
        /// Performs the the operation of check if the user name exists 
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize]
        public JsonResult CheckUserName(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = false, message = "Nombre de usuario inválido" }
                };
            }

            var user = UserManager.FindByName(TripleDesEncryption.Encrypt(username));
            if (user != null && UserManager.IsEmailConfirmed(user.Id))
            {
                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = false, message = "Nombre de usuario: '" + username + "' se encuentra en uso, por favor digitar otro." }
                };
            }
            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new { result = true}
            };
        }

        /// <summary>
        /// Get User Profile image JSON
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        //[Authorize]
        public JsonResult GetUserProfileImageJSON(int? userId)
        {
            try
            {
                var imageInfo = ECOsystem.Utilities.Session.GetUserProfileImage(userId);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = imageInfo }
                };
            }
            catch (Exception ex)
            {

                ECOsystem.Utilities.Session.SentrySupport(ex);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, ex.Message);

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = "" }
                };
            }
        }

        /// <summary>
        /// Add log event for this Controller
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="message"></param>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <param name="partnerId"></param>
        /// <param name="isWarning"></param>
        /// <param name="isInfo"></param>
        /// <param name="isError"></param>
        public void AddLogEvent(Exception exception, string message, int? userId = null, int? customerId = null, int? partnerId = null, bool isWarning = false, bool isInfo = false, bool isError = false)
        {
            var messageEvent = string.Empty;

            if (exception != null)
            {
                messageEvent = string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace);
            }
            else
            {
                messageEvent = message;
            }

            EventLogBusiness.AddEventLog(new EventLog()
            {
                UserId = userId,
                CustomerId = customerId,
                PartnerId = partnerId,
                Controller = this.ControllerContext.RouteData.Values["controller"].ToString(),
                Action = this.ControllerContext.RouteData.Values["action"].ToString(),
                Message = messageEvent,
                IsWarning = isWarning,
                IsInfo = isInfo,
                IsError = isError
            });
        }
    
        /// <summary>
        /// Unlock Support Users
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UnlockSupportUsers(string Email, bool ResetPass)
        {
            using (var bus = new SupportUsersBusiness())
            {
                if (bus.UnlockUser(ECOsystem.Utilities.TripleDesEncryption.Encrypt(Email.Trim()), ResetPass))
                {
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Failed", JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}