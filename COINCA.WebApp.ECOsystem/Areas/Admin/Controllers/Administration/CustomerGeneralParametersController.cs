﻿using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Business.Administration;
using System.Globalization;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using ECOsystem.Models.Core;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class CustomerGeneralParametersController : Controller
    {
        private const int cAlarmTriggerIdPanicBotton = 511;

        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var bus = new CustomerGeneralParametersBusiness())
            {
                var result = bus.CustomerGeneralParametersRetrieve();

                AlarmsModels alarm = bus.EmailPanicBottonRetrieve();
                if (alarm != null)
                {
                    result.Email = alarm.Email;
                    result.AlarmId = alarm.AlarmId;
                }

                using (var alarmCtr = new AlarmMaintenanceController())
                {
                    result.AlarmDetailModel = new AlarmDetail();
                    ECOsystem.Models.Core.AlarmsModels alarmM = new AlarmsModels();
                    alarmM.AlarmId = alarm.AlarmId;
                    alarmM.AlarmTriggerId = cAlarmTriggerIdPanicBotton;
                    result.AlarmDetailModel = alarmCtr.LoadAlarmDetail(null, "GenParm", alarmM);
                }

                return View(new CustomerGeneralParametersBase()
                {
                    Data = result,
                    Menus = new List<AccountMenus>()
                });
            }
        }

        [HttpPost]
        public ActionResult AlarmTemperatureDelayAddOrEdit(int AlarmTemperatureDelay)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.AlarmTemperatureDelatAddOrEdit(AlarmTemperatureDelay);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Cambios en el tiempo de espera de alarmas de temperatura, Tiempo de espera: {0}", AlarmTemperatureDelay));
                    return Json("Success");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en los cambios de tiempo de espera de alertas de temperatura, Error: {0}", e.Message));
                return Json("Error");
            }
        }

        [HttpPost]
        public ActionResult MinimumStopTimeAddOrEdit(int MinimumStopTime)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.MinimumStopTimeAddOrEdit(MinimumStopTime);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Cambios en el tiempo de parada para rutas, Tiempo de parada: {0}", MinimumStopTime));
                    return Json("Success");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en los cambios de tiempo de parada para rutas, Error: {0}", e.Message));
                return Json("Error");
            }
        }

        [HttpPost]
        public ActionResult VehicleScheduleAddOrEdit(string StartTime, string EndTime)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    DateTime dtStart;
                    DateTime dtEnd;

                    DateTime.TryParse(StartTime, out dtStart);
                    DateTime.TryParse(EndTime, out dtEnd);

                    bus.NormalVehicleScheduleAddOrEdit(dtStart.Hour.ToString(), dtEnd.Hour.ToString());
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Cambios en el horario normal de vehículos, Hora inicial {0}, hora final {1}", StartTime, EndTime));
                    return Json("Success");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en los cambios de horarios de uso normal, Error: {0}", e.Message));
                return Json("Error");
            }
        }

        

        public ActionResult SaveInProcessTransactions(bool AllowProcess)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.SaveInProcessTransactions(AllowProcess);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, AllowProcess ? "Activa" : "Desactiva" + "proceso de transacciones con estado en proceso");
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la activación de Proceso de Transaciones en Proceso, Error: {0}", e.Message));
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveApprovalTransactiosImport(bool ActivateProcess)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.SaveApprovalTransactiosImport(ActivateProcess);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, ActivateProcess ? "Activa" : "Desactiva" + "permite aprobar las transacciones automáticamente en la importación");
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la activación de Proceso que permite aprobar las transacciones automáticamente en la importación, Error: {0}", e.Message));
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult SaveTypeOdometerbyCustomer(bool TypeOdometer)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.TypeOdometerbyCustomer(TypeOdometer);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, TypeOdometer ? "Activa" : "Desactiva" + "Define cual tipo de odometro se va a utilizar para Sp_PreventiveMaintenance_Alarm");
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la definición de tipo de odómetro a utilizar, Error: {0}", e.Message));
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult SaveCostCenterByDriver(bool ActivateProcess)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.SaveCostCenterByDriver(ActivateProcess);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, ActivateProcess ? "Activa" : "Desactiva" + " Centros de Costo por Conductor");
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la activación de Centros de Costo por Conductor, Error: {0}", e.Message));
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveTypeofDitribution(CustomerGeneralParameters model)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.TypeOfDistributionByCustomer(model.TypeBudgetDistribution);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Selecciona el Tipo de distribución para mostrarle al cliente (Monto o Litros). Selección del Cliente: " + model.TypeBudgetDistribution == "0" ? "Monto" : "Litros");
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la definición de tipo de odómetro a utilizar, Error: {0}", e.Message));
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveDefaultEmail(CustomerGeneralParameters model)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.DefaultAlarmsEmail(model.DefaultEmail);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Guarda el correo por defecto para las alertas: " + model.DefaultEmail);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error en la definición de tipo de odómetro a utilizar, Error: {0}", e.Message));
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult NotApproveTransactions(CustomerGeneralParameters model)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.NotApproveTransactions(model.NotApproveTransactions);
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Se habilita/deshabilita la conciliacion de transacciones, valor:" + model.NotApproveTransactions);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al habilitar/deshabilitar la conciliacion de transacciones, Error: {0}", e.Message));
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RadioDistanceAddOrEdit(CustomerGeneralParameters model)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.ActiveRadioDistance(model.ActiveRadioDistance);

                    if (model.ActiveRadioDistance == true)
                    {
                        bus.RadioDistanceByCommerce(model.RadioDistance);
                        new EventLogBusiness().AddLogEvent(LogState.INFO, "Se Agregar/Modificar dirección de un Punto utilizando los Comercios, valor:" + model.RadioDistance);
                    }
                    new EventLogBusiness().AddLogEvent(LogState.INFO, model.ActiveRadioDistance ? "Activa" : "Desactiva" + "dirección de un Punto utilizando los Comercios");
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Agregar/Modificar dirección de un Punto utilizando los Comercios, Error: {0}", e.Message));
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        //NO eliminar, eventualmente se utilizara para un mantenimiento de alarmas.
        //public ActionResult AlarmNonTransmissionGPSAddOrEdit(bool activateAlarm)
        //{
        //    try
        //    {
        //        using (var bus = new CustomerGeneralParametersBusiness())
        //        {
        //            bus.AlarmNonTransmissionGPSAddOrEdit(activateAlarm);
        //            new EventLogBusiness().AddLogEvent(LogState.INFO, "Se habilita/deshabilita la alarma para No transmision de un GPS.");
        //            return Json("Success", JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al habilitar/deshabilitar la conciliacion de transacciones, Error: {0}", e.Message));
        //        return Json("Error", JsonRequestBehavior.AllowGet);
        //    }
        //}

        public ActionResult VehicleStopLapse(CustomerGeneralParameters model)
        {
            try
            {
                using (var bus = new CustomerGeneralParametersBusiness())
                {
                    bus.VehicleStopLapseAddOrEdit(model.VehicleStopLapse);

                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Se Agregar/Modificar el tiempo de tolerable de parada, valor:" + model.VehicleStopLapse);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Error al Agregar/Modificar el tiempo de tolerable de parada, Error: {0}", e.Message));
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

    }
}
