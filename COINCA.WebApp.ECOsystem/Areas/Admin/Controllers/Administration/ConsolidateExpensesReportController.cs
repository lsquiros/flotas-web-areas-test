﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Business.Administration;
using System.Data;
using ECOsystem.Utilities;
using ECOsystem.Business.Utilities;
using Newtonsoft.Json;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class ConsolidateExpensesReportController : Controller
    {
        // GET: ConsolidateExpensesReport
        public ActionResult Index()
        {
            return View(new ConsolidateExpensesReportBase());
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(ConsolidateExpensesReportBase model)
        {
            try
            {
                Session["RangoFechas"] = Convert.ToDateTime(model.Parameters.StartDate).ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + Convert.ToDateTime(model.Parameters.EndDate).ToString("dd/MM/yyyy hh:mm:ss tt");

                using (var business = new ConsolidateExpensesReportBusiness())
                {
                    Session["Reporte"] = business.PreventiveMaintenanceReportGenerate(model.Parameters);

                    return Json(business.RetrieveConsolidateReport().List.Count(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DonwloadFile()
        {
            try
            {
                DataSet report = new DataSet();
                if (Session["Reporte"] != null)
                {
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Consolidado de Gastos");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "ConsolidateExpensesReport", ToString().Split('.')[2], "Reporte Consolidado de Gastos");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);            
                return View("Index", new ConsolidateExpensesReportBase());                
            }
        }
    }
}
