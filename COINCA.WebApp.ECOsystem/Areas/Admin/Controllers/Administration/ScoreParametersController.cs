﻿using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;
using  ECOsystem.Areas.Admin.Models.Identity;
using ECOsystem.Business.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class ScoreParametersController : Controller
    {
        [EcoAuthorize]
        public ActionResult Index()
        {
            using (var bs = new ScoreParametersBusiness())
            {
                var model = bs.RetrieveParameters();
                Session["DetailList"] = model.List;
                return View(model);
            }
        }

        public ActionResult GetDetailId()
        {
            if(Session["DetailList"] != null)
            {
                var list = (List<GPSScoreParameters>)Session["DetailList"];
                return Json(list.OrderByDescending(m => m.Id).FirstOrDefault().Id + 1, JsonRequestBehavior.AllowGet);
            }
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddDetail(int Id, int From, int To, int Percentage, bool IsNew)
        {
            var list = new List<GPSScoreParameters>();
            if (Session["DetailList"] != null)
            {
                list = (List<GPSScoreParameters>)Session["DetailList"];
            }
            if(IsNew && list.Where(m => m.Id == Id).ToList().Count == 0)
            {
                var detail = new GPSScoreParameters()
                {
                    Id = Id,
                    From = From,
                    To = To,
                    Percentage = Percentage
                };
                list.Add(detail);
            }
            else
            {
                list.Where(m => m.Id == Id).ToList().ForEach(m => { m.From = From; m.To = To; m.Percentage = Percentage; });
            }
            Session["DetailList"] = list;
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ScoreParametersAddOrEdit(ScoreParameters model)
        {
            try
            {
                using (var bs = new ScoreParametersBusiness())
                {
                    bs.ScoreParameterAddOrEdit(model);
                }
                new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Datos Guardados correctamente. Detalle: {0}", JsonConvert.SerializeObject(model)));
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, string.Format("Error al almacenar parámetros. Detalle: {0}", e.Message));
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}