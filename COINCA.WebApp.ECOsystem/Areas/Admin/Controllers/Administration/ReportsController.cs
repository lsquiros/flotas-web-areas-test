﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ECOsystem.Business.Core;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Newtonsoft.Json;




namespace  ECOsystem.Areas.Admin.Controllers.Administration
{
    public class ReportsController : Controller
    {

        public ActionResult Index(int? Id)
        {
            using (var bus = new CustomReportsBusiness())
            {
                return View(bus.GetReportInformation(Id));
            }
            
        }

        // GET: Reports/Details/5
        public ActionResult ExcelReportDownload(CustomReport model)
        {
            try
            {             
                using (var business = new CustomReportsBusiness())
                {
                    DataSet datosReporte = business.GetReportDownloadData(model);
                    Session["Reporte"] = datosReporte;
                    new EventLogBusiness().AddLogEvent(LogState.INFO, string.Format("Se generan los datos para el reporte, informacion, {0}", new JavaScriptSerializer().Serialize(model.Parameters)));

                    if(datosReporte != null)
                    {
                        if(datosReporte.Tables.Count > 0)
                        {
                            if(datosReporte.Tables[2].Rows.Count > 0)
                                return Json("Success");
                            else
                                return Json("NoDatos");
                        }
                        else
                            return Json("NoDatos");
                    }
                    else
                        return Json("NoDatos");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                DataSet report = new DataSet();
                if (Session["Reporte"] != null)
                {
                    report = (DataSet)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga Reporte Personalizado");
                    return bus.GetReportDataSet(JsonConvert.SerializeObject(report), "CustomReport", ToString().Split('.')[2], "Reporte Personalizado");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);
                ViewBag.ErrorReport = 1;
                return View("Index");
            }
        }
    }
}
