﻿using ECOsystem.Business.Core;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Admin.Utilities
{
    public class GetAccountUtility
    {

        public void GetUserInformation(ref System.Web.HttpContextBase context) //, int? userId)
        {
            HttpCookie aCookie = context.Request.Cookies["ECOCFG"];
            Users user = null;

            if (aCookie != null)
            {
                aCookie.Expires = DateTime.Now.AddMinutes(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ECOCFG_TIME"]));
                //context.Response.Cookies.Add(aCookie);
                string text = aCookie.Value;
                ECOsystem.Models.Account.TokenByUserModel userActive = ECOsystem.Business.Utilities.AccountManagement.TokenUserRetrieve(text);

                if (!context.Request.IsAuthenticated)
                {
                    //if the customer has info it will log with the customer information
                    if (userActive.SerializeUserInfo != null)
                    {
                        context.Session.Add("LOGGED_USER_INFORMATION", userActive.User);
                    }
                    else
                    {
                        using (var business = new UsersBusiness())
                        {
                            user = business.RetrieveUsers(null, userName: userActive.UserId).FirstOrDefault();
                        }
                        context.Session.Add("LOGGED_USER_INFORMATION", user);
                    }
                }
                else
                {
                    //if the customer has info it will log with the customer information
                    if (userActive.SerializeUserInfo != null)
                    {
                        context.Session.Add("LOGGED_USER_INFORMATION", userActive.User);
                    }
                    else
                    {
                        using (var business = new UsersBusiness())
                        {
                            user = business.RetrieveUsers(null, userName: userActive.UserId).FirstOrDefault();
                        }
                        context.Session.Add("LOGGED_USER_INFORMATION", user);
                    }
                }                             
            }
        }
    }
}