﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.GeoFenceTypes = (function () {

    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initToggle();
        showTolerance();
 
        //Edit for the geofence grid
        $('#gridContainer').off('click.edit', 'a[edit_row_type]').on('click.edit', 'a[edit_row_type]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();  
        });

        $('#gridContainer').off('click.edit', 'a[edit_row_type]').on('click.edit', 'a[edit_row_type]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();      
        });

        $('#btnClose').off('click.btnClose').on('click.btnClose', function (e) {
            $('#detailContainer').hide('slow');
        });

        $('#btnAdd').off('click.btnAddNew').on('click.btnAddNew', function (e) {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });

        $('input[type=checkbox]').change(showTolerance);
    };
      
    var onSuccessLoad = function () {
        initialize();
        $('#detailContainer').show('slow');
    };
   
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
    };

    var successSearch = function () {
        $('#txtSearch').val('');
    };

    var hideAddOrEdit = function () {
        hideLoader();
        $('#detailContainer').hide('slow');
    }

    var initToggle = function () {
        $('.toggleCheck').bootstrapToggle({
            on: 'Si',
            off: 'No'
        });
    };

    var showTolerance = function () {
        if ($('.toggleCheck').prop("checked")){
            $("#tolerance").show();
        }
        else{
            $("#tolerance").hide();
        }
    }

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        SuccessSearch: successSearch,
        HideAddOrEdit: hideAddOrEdit
    };
})();

