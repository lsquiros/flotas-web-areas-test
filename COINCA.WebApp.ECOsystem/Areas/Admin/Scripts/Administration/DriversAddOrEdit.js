﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.DriversAddOrEdit = (function () {
    var options = {};


    /* initialize function */

    var initialize = function (opts) {
        $.extend(options, opts);

        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        var date = $('#driverLicenseExpirationId').val();

        $('#driverLicenseExpirationId').datepicker({ language: 'es', minDate: date, format: 'dd/mm/yyyy', autoclose: true, todayHighlight: true, startDate: today });
        $("#DriverUser_LicenseExpirationStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });
        
        //if (!date) {
        //    $('#driverLicenseExpirationId').datepicker('setDate', date);
        //}

        $("#DailyTransactionLimit").numeric("integer");      

        //Init dropDown List Card Request
        $("#DriverUser_CardRequestId").select2('data', {});
        $("#DriverUser_CardRequestId").select2({
            minimumInputLength: 3,
            formatResult: cardRequestSelectFormat,
            formatSelection: cardRequestSelectedFormat,
            escapeMarkup: function (m) { return m; },
            matcher: cardRequestSearch,
            allowClear: true
        });

        $('#CostCenterId').select2('data', {});
        $('#CostCenterId').select2({
            //formatResult: CostCenterSelectFormat,
            //formatSelection: CostCenterSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        //Init DropDown List Available Dallas Keys
        $('#User_DallasId').select2('data', {});
        $('#User_DallasId').select2({
            //minimumInputLength: 3,
            formatResult: dallasKeysSelectFormat,
            formatSelection: dallasKeysSelectFormat,
            escapeMarkup: function (m) { return m; },
            matcher: dallasKeysSearch,
            allowClear: false
        });

        $("#PeriodicityTypeId").select2('data', {});
        $("#PeriodicityTypeId").select2({
            formatResult: PeriodicitySelectFormat,
            formatSelection: PeriodicitySelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });


        //trigger click on hidden file input
        $('#btnUpload').off('click.btnUpload').on('click.btnUpload', function (e) {
            $('#fileHidden').trigger('click');
            e.stopPropagation();
        });    

        //Form addOrEditForm
        $("#addOrEditForm").validate();

        $("#btnConfirmAddOrEditDriver").click(function () {
            
            if ($("#addOrEditForm").valid()) {

                var nowDate = new Date();
                var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
                var date = $('#driverLicenseExpirationId').val();
                var new_date = new Date(date)

                if (new_date < today) {
                    ECOsystem.Utilities.SetMessageShow("La Fecha de Expiración no puede ser menor a la fecha actual", "ERROR");
                }
                else {
                    $('#confirmModal').modal('show'); 
                }
            }
            
        });

        $("#sendDriver").click(function () {
            $('#confirmModal').modal('hide'); 
            $('#confirmRulesModal').modal('hide'); 
            
            $('#addOrEditForm').trigger('submit');            
            return;
        });

        $("#User_IsAgent").click(function () {
            if (!$(this).is(":checked")) {
                $('.IsDriver').each(function () {
                    $(this).removeClass('hide');
                });
            }
            else {
                $('.IsDriver').each(function () {
                    $(this).addClass('hide');
                });
            }
        });

        fileApiUpload();
        formValidation();
        initCheckUsername();

    }

    var onAddOrEditProcessEnd = function (data) {
        if (data == "Success") {
            window.location.href = 'Index';
        } else if (data.indexOf('ERROR EXIST_USR') > -1) {
            ECOsystem.Utilities.HideLoader();
            ECOsystem.Utilities.SetMessageShow("El correo electrónico ya es utilizado.", "ERROR");
        } else if (data.indexOf('ERROR') > -1) {
            ECOsystem.Utilities.HideLoader();
            $('#msgValidationAlert').html(data)
            $('#validationAlert').show();
        }
    };

    var onAcceptAtentionModal = function () {
        $('#errorModal').modal('hide');
    };

    var onClickAddAlarm = function(obj) {
        $('#alarmForm').find('#id').val($(obj).attr('userid'));
        $('#alarmForm').submit();
        $(this).attr('disabled', 'disabled');
    };

    var alarmMaintenanceModalShow = function () {
        $('#alarmMaintenanceModal').on('shown.bs.modal', function (e) {

            $('.rangeValue').click(function () {
                var val = $(this).val();
                var id = $(this).attr('alarmtipoid');
                $('#demo_' + id).html(val);
            });

            $('.validateRepeat').click(function () {
                var id = $(this).attr('alarmtipoid');
                if ($('#Alarm_' + id).find('#txtAlarmPhone').val() == null ||
                    $('#Alarm_' + id).find('#txtAlarmPhone').val() == '' ||
                    $('#Alarm_' + id).find('#txtAlarmPhone').val() == '0000-0000') {
                    $(this).removeAttr('checked');
                    setAmountErrorMsj('El servicio para el envío de mensajes recurrentes, es por medio de SMS, por lo cual se debe indicar un número de celular para su activación.', true, 'e2');

                } else {
                    setAmountErrorMsj('', true, 'e2');
                    if ($(this).is(":checked")) {
                        $('#rngAlarmRepeatTimes_' + id).removeAttr('disabled');
                        $('#txtAlarmIntervalRepeat_' + id).removeAttr('disabled');
                    } else {
                        $('#rngAlarmRepeatTimes_' + id).attr('disabled', 'disabled');
                        $('#txtAlarmIntervalRepeat_' + id).attr('disabled', 'disabled');
                    }
                }
            });

            $('#btnSendAlarm').click(function () {
                var message = '';
                var error = false;
                var ids = $(this).attr('allIds');
                var items = ids.substring(1, ids.length - 1).split(",")
                var list = [];
                if (ids != null) {
                    for (var i = 0; i < items.length; i++) {
                        message += ValidateInputs(items[i]);
                        if (message != '') {
                            error = true;
                        }
                        list.push(GetValues(items[i]));
                    }
                    setAmountErrorMsj(message, true, 'e2');
                    if (!error) {
                        AddOrEditAlarmMaintenance(list);
                    }
                }
            });
        }).modal('show');

        $('#btnCloseAlarm').click(function () {
            alarmMaintenanceModalClose();
        });

        $('#btnCancelAlarm').click(function () {
            alarmMaintenanceModalClose();
        });
    };

    var ValidateInputs = function (alarmTriggerId) {
        var message = '';
        //if ($('#Alarm_' + alarmTriggerId).find('#chkAlarmActive').is(":checked")) {
            if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val() == null ||
                $('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val() == '') {

                if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() == null ||
                    $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() == '') {
                    message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Verifique que agregó al menos un correo o un número de celular. <br/>';
                }
                else if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val(), false)) {
                    message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888<br/>';
                }
            }
            else if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val(), true)) {
                message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El correo debe cumplir el siguiente formato: ejemplo1@dominio.com. <br/>';
            } else {
                if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() != undefined &&
                    $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() != null &&
                    $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() != '') {
                    if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val(), false)) {
                        message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888<br/>';
                    }
                }
            }

            if (alarmTriggerId == 504) {
                if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmDaysBeforeAlert').val() == null ||
                    $('#Alarm_' + alarmTriggerId).find('#txtAlarmDaysBeforeAlert').val() == '') {
                    message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Debe indicar el tiempo máximo de parada. <br/>';
                }
            }
        //}
        return message;

    };

    var verificarFormato = function (data, email) {
        var result = false;
        var list = data.replace(" ", "").split(";");
        var regex = email ? /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/ : /^([0-9\+\s\+\-])+$/;

        for (var item = 0; item < list.length; item++) {
            if (regex.test(list[item])) {
                result = true;
            }
            else {
                result = false;
                break;
            }
        }
        return result;
    }

    /* Show or hide errors messages*/
    var setAmountErrorMsj = function (msj, show, typeError) {

        typeError = typeError != null ? typeError : 'e1';

        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').removeClass('hide');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').addClass('hide');
                }
                break;
            case 'e3':
                if (show) {
                    $('#errorMsj').html(msj);
                    $('#validationError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationError').addClass('hide');
                }
                break;
            default:
                break;
        }


    }

    var GetValues = function (alarmTriggerId) {
        var alarma = {};
        alarma.AlarmId = $('#Alarm_' + alarmTriggerId).attr('alarmid');
        alarma.AlarmTriggerId = alarmTriggerId;
        alarma.EntityId = $('#id').val();
        alarma.Email = $('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val();
        alarma.Phone = $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val();
        alarma.RepeatTimes = $('#Alarm_' + alarmTriggerId).find('#rngAlarmRepeatTimes_' + alarmTriggerId).val();
        alarma.IntervalRepeat = $('#Alarm_' + alarmTriggerId).find('#txtAlarmIntervalRepeat_' + alarmTriggerId).val();
        alarma.DaysBeforeAlert = $('#Alarm_' + alarmTriggerId).find('#txtAlarmDaysBeforeAlert').val();
        alarma.Active = ($('#Alarm_' + alarmTriggerId).find('#chkAlarmActive').is(":checked")) ? true : false;

        if ((alarma.Email == null || alarma.Email == "") && (alarma.Phone == null || alarma.Phone == "")) {
            alarma.Active = false;
        }

        if (!$('#Alarm_' + alarmTriggerId).find('#chkAlarmRepeat').is(":checked") || (alarma.Phone == null || alarma.Phone == "")) {
            alarma.RepeatTimes = 0;
            alarma.IntervalRepeat = 0;
        }

        return alarma;
    }

    var AddOrEditAlarmMaintenance = function (listAlarm) {
        //$('body').loader('show');
        $.ajax({
            url: '/Administration/AlarmMaintenance/AddOrEditAlarmMaintenance',
            type: 'POST',
            data: JSON.stringify({ pListAlarm: listAlarm }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                alarmMaintenanceModalClose();
            }
        });
    };

    var alarmMaintenanceModalClose = function () {
        $('#alarmMaintenanceModal').on('hide.bs.modal', function (e) {
            $('#btnAddAlarm').removeAttr('disabled');
        }).modal('hide');
    };

    var onClickDriverLicense = function(obj) {
        $("#driverLicenseExpirationId").datepicker("show");
        setTimeout(function () {
            $('#driverLicenseExpirationId').datepicker("hide");
            $('#driverLicenseExpirationId').blur();
        }, 2000)
    };

    var CostCenterSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
               '</div>';
    };


    /*initCheckUsername*/
    var initCheckUsername = function () {
        $('#detailContainer').off('change.email', '#DecryptedEmail').on('change.email', '#DecryptedEmail', function (e) {
            checkValidUserName();
        });

        $("#DecryptedEmail").keyup(function () {
            checkValidUserName();
        });
    }

    var checkValidUserName = function () {
        if (validateDetailEmail()) {
            $('#checkForm').find('#username').val($('#DecryptedEmail').val());
            $('#checkForm').submit();
        } else {
            $('.modal-footer button[type="submit"]').attr('disabled', 'disabled');
        } 2
    };

    /*on Success Check User*/
    var onSuccessCheckUser = function (data) {
        if (!data.result) {
            $('.modal-footer button[type="submit"]').attr('disabled', 'disabled');
            setErrorMsj(data.message, true);
        } else {
            $('.modal-footer button[type="submit"]').removeAttr('disabled');
            setErrorMsj('', false);
        }
    }

    /* Standard Error Function for File API */

    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };

    /* Select Format*/
    var PeriodicitySelectFormat = function (item) {
        
        if (!item.id || item.id == "") return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-6">' + json.Name + '</div>' +
                '</div>';
    };

    /* Update progress for File API */

    var fileApiUpdateProgress = function (e) {
        if (e.lengthComputable) {
            var percentLoaded = Math.round((e.loaded / e.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                $('.percent').css('width', percentLoaded + '%').html(percentLoaded + '%');
            }
        }
    };


    /* File Upload implementation */

    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            $('#fileHidden').off('change.fileHidden').on('change.fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onprogress = fileApiUpdateProgress;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {
                    $('#imgPhoto').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        alert("La extensión del archivo es inválida");
                        continue;
                    }
                    // Closure to capture the file information.
                    reader.onload = (function (F) {
                        $('.percent').html('100%').css('width', '100%');
                        setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgPhoto').removeClass('imgloading');", 2000);
                        return function (e) {
                            var lsrc = e.target.result;
                            $('#imgPhoto').attr({
                                src: lsrc,
                                title: escape(F.name)
                            });
                            $('#User_Photo').val(lsrc);
                        };
                    })(f);
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
            });
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.');
        }
    };
   

    /* Show or hide errors messages*/
    var setErrorFormatMsj = function (msj, show) {
        if (show) {
            $('#errorFormatMsj').html(msj);
            $('#validationFormatErrors').removeClass('hide');
        } else {
            $('#errorFormatMsj').html('');
            $('#validationFormatErrors').addClass('hide');
        }

    }

    /* Form validation to prevent an invalid submission*/

    var formValidation = function () {

        //Validate Card Type Request
        var cardTypeRequest = $("#CardTypeRequest").val();
        if (cardTypeRequest != "100") { // Solicitud es diferente por Conductor
            $("#CardRequestControl").remove(); // Remuevo el control para no validarlo
        }

        $('#detailContainer').off('click.btnSaveSubmit', 'button[type=submit]').on('click.btnSaveSubmit', 'button[type=submit]', function (e) {
            setErrorMsj('', false);
            if (validateEmail()) {
                return true;
            } else {
                return false;
            }
        });
    };

    /* Email validation to prevent an invalid submission*/
    var validateDetailEmail = function () {
        setErrorMsj('', false);
        if ($('#DecryptedEmail').val().length == 0) {

            return false;
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#DecryptedEmail').val().match(email)) {

            return true;
        } else {
            setErrorMsj('Correo electrónico invalido', true);

            return false;
        }
    };

    /* Email validation to prevent an invalid submission*/
    var validateEmail = function () {
        if ($('#Email').val().length == 0) {
            return true;
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#Email').val().match(email)) {
            return true;
        } else {
            setErrorMsj('Correo electrónico invalido', true);
            return false;
        }
    };

    /* Show or hide errors messages*/
    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj').html(msj);
            $('#validationErrors').removeClass('hide');
        } else {
            $('#errorMsj').html('');
            $('#validationErrors').addClass('hide');
        }

    }

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
    }; 

    /* CardRequest Select Format*/
    var cardRequestSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-1">' + json.NumSolicitud + '</div>' +
                '<div class="col-md-7">' + json.DriverName + '</div>' +
                '<div class="col-md-2">' + json.StateName + '</div>' +
                '<div class="col-md-2">' + json.CityName + '</div>' +
                '</div>';
    };

    /* CardRequest Selected Format*/
    var cardRequestSelectedFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-1">' + json.NumSolicitud + '</div>' +
                '<div class="col-md-7">' + json.DriverName + '</div>' +
                '<div class="col-md-2">' + json.StateName + '</div>' +
                '<div class="col-md-2">' + json.CityName + '</div>' +
                '</div>';
    };

    /*CardRequest search*/
    var cardRequestSearch = function (term, text) {

        var obj = null;
        try { obj = JSON.parse(text.replace(/\t/g, '').replace(/'/g, "\"")); } catch (e) { }
        if (obj === null) { return false; }
        text = obj.DriverName + ' ' + obj.NumSolicitud;

        var terms = term.split(" ");
        for (var i = 0; i < terms.length; i++) {
            var tester = new RegExp("\\b" + terms[i], 'i');
            if (tester.test(text) == false) {
                return (text === 'Other');
            }
        }
        return true;
    }

    /* DallasKeys Select Format*/
    var dallasKeysSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));

        if (json.Code == "Ninguna") {
            return json.Code;

            //'<div class="row">' +
            //            '<div class="col-md-2">'
            //                    + json.Code + '</div>' +
            //        '</div>';
        }

        return json.Code;

        //'<div class="row">' +
        //            '<div class="col-md-2"> '
        //                    + json.Code + '</div>' +
        //        '</div>';
    };

    /*DallasKeys search*/
    var dallasKeysSearch = function (term, text) {
        var obj = null;
        try { obj = JSON.parse(text.replace(/\t/g, '').replace(/'/g, "\"")); } catch (e) { }
        if (obj === null) { return false; }
        text = obj.Code;

        var terms = term.split(" ");
        for (var i = 0; i < terms.length; i++) {
            var tester = new RegExp("\\b" + terms[i], 'i');
            if (tester.test(text) == false) {
                return (text === 'Other');
            }
        }
        return true;
    }


    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadExcelForm').submit();
    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        OnSuccessCheckUser: onSuccessCheckUser,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnClickAddAlarm: onClickAddAlarm,
        OnClickDriverLicense: onClickDriverLicense,
        AlarmMaintenanceModalShow: alarmMaintenanceModalShow,
        AlarmMaintenanceModalClose: alarmMaintenanceModalClose,
        OnAddOrEditProcessEnd: onAddOrEditProcessEnd,
        OnAcceptAtentionModal: onAcceptAtentionModal
    };
})();

