﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.MonthlyBillReport = (function () {
    var options = {};
   
    var initialize = function (opts) {
        $.extend(options, opts);

        $("#Month").select2('data', {});
        $("#Month").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
        $("#Year").select2('data', {});
        $("#Year").select2({
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
    }
       
    var onEndGetData = function (data) {
        hideloader();
        if (data == "Success") {
            $('#DonwloadFileForm').submit();
            $('#InfoContainer').html('<br />' +
                                     '<div class="alert alert-success alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                        '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Seleccione la información necesaria para generar el reporte.' +
                                     '</div>');
        } else if (data == "NoData") {
            $('#InfoContainer').html('<br />' +
                                     '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                                        '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;No existe información para descargar el reporte con las fechas seleccionadas.' +
                                     '</div>');
        } else {
            $('#InfoContainer').html(data);
        }
    }

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    return {
        Init: initialize,
        ShowLoader: showloader,
        OnEndGetData: onEndGetData
    };
})();