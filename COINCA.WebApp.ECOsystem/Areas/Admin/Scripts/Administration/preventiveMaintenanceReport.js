﻿var ECO_Admin = ECO_Admin || {};

 ECO_Admin.preventiveMaintenanceReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        initCharts();
        try {
            initializeDropDownList();
        }
        catch (exception) {
        }
    }

    /*init Events*/
    var initEvents = function () {
        $('#page-content-wrapper').off('click.print', 'a[data-print-link]').on('click.print', 'a[data-print-link]', function (e) {
            ECOsystem.Utilities.ShowPopup(options.printURL, 'Formato de impresión');
        });
        $('#page-content-wrapper').off('click.goToPrint', 'button[data-print]').on('click.goToPrint', 'button[data-print]', function (e) {
            window.print();
        });

        if (!options.printMode) {
            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_StartDateStr').val().length == 10)
                //if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects())
                        $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });
            $("#Parameters_StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });

            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_EndDateStr').val().length == 10)
                    //if ($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });

            $("#Parameters_EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });

            $('#datetimepickerDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_DateStr').val().length == 10)
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });

            $("#Parameters_DateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });

        }
    }

    /*Init Charts*/
    var initCharts = function () { }
    
    /*format Label*/
    var formatLabel = function (v) {
        return ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToFloat(v)) + "%";
    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#Parameters_CostCenter").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectCostCenterChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_MaintenanceCatalogId").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMaintenanceCatalogChange(data); return fn.apply(this, arguments); }
                //var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMaintenanceCatalogChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Status").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectStatusChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        var select4 = $("#Parameters_FilterType").select2().data('select2');
        select4.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectTypeChange(data); return fn.apply(this, arguments); }
            }
        })(select4.onSelect);

    };

    /*on Select Cost Center Change*/
    var onSelectCostCenterChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
            $('#Parameters_CostCenter').val(obj.id);
            $('#ReloadForm').submit();
    };

    /*on Select Maintenance Catalog Change*/
    var onSelectMaintenanceCatalogChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
            $('#Parameters_MaintenanceCatalogId').val(obj.id);
            $('#ReloadForm').submit();
    };

    /*on Select Maintenance Catalog Change*/
    var onSelectStatusChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
            $('#Parameters_Date').val('');
            $('#Parameters_Odometer').val('');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');
            $('#Parameters_Status').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            $('#ReloadForm').submit();
    };

    /*on Select Maintenance Catalog Change*/
    var onSelectTypeChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        //$('#Parameters_MaintenanceCatalogId').select2('val', null);
        //$('#loadMaintenanceForm').find("#FilterType").val(obj.id);
        $('#Parameters_FilterType').val(obj.id);
        $('#ReloadForm').submit();
    };

    /*onSuccessLoadTypes*/
    var onSuccessLoadTypes = function (data) {
        if (data.result) {
            loadTypes(data.listCities);
        }
    }

    /*loadTypes*/
    var loadTypes = function (options) {
        
        $('#Parameters_MaintenanceCatalogId').html("");       
        $.each(options, function (index, item) {
            $('#Parameters_MaintenanceCatalogId')
                .append($("<option></option>")
                .attr("value", item.id)
                .text(item.text));
        });
    }

    var onClickDownloadVehicles = function () {

        $('#ExcelReportDownloadForm').submit();
    };

    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadExcelForm').submit();
    };



    /* Public methods */
    return {
        Init: initialize,
        FormatLabel: formatLabel,
        OnClickDownloadVehicles: onClickDownloadVehicles,
        ShowLoader: showLoader,
        HideLoader: hideLoader
    };
})();

