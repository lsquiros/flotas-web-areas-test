﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.ServiceStations = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        initializeDropDownList();
        initBlockButtons();

        $.extend(options, opts);
        $('#gridContainer').off('click.del_row', 'a[del_row]').on('click.del_row', 'a[del_row]', function (e) {
            $('#deleteForm').find('#id').val($(this).attr('id'));
        });

        $('#costcentercontainer').off('click.del_blocked_row', 'a[del_blocked_row]').on('click.del_blocked_row', 'a[del_blocked_row]', function (e) {
            var ServiceStationId = $("#ServiceStationId").val();
            $('#deleteBlockedForm').find('#costCenterId').val($(this).attr('id'));
            $('#deleteBlockedForm').find('#serviceStationId').val(ServiceStationId);
        });



        $("#BlockCostCenter").click(function () {
            var ServiceStationId = $("#ServiceStationId").val();
            var CostCenterId = $("#CostCenterId").val();
            var CostCenterName = $("#CostCenterId option:selected").text();
            $('#addBlockeCostCenter').find('#costcenterid').val(CostCenterId);
            $('#addBlockeCostCenter').find('#costcentername').val(CostCenterName);
            $('#addBlockeCostCenter').find('#servicestationid').val(ServiceStationId);
            $('#addBlockeCostCenter').submit();
        });

        $('#btnCancel').click(function () {
            $('#detailContainer').hide('slow');
        });
    }

    $('#ConfirmationBlockServiceStations').find('#btnBlockSelectedServiceStations').click(function () {
        $('#detailContainer').hide('slow');
        $('#BlockServiceStationsForm').submit();
    });
  

    $('#AddOrEditConfirmationModal').find('#btnAddOrEditServiceStation').click(function () {
        $('#addOrEditForm').submit();
    });

    var initBlockButtons = function () {

        $('#gridContainer').find('.btnBlockServiceStations').click(function () {
            $('#ConfirmationBlockServiceStations').modal('show');
        });

        $('#gridContainer').find('.blockServiceStation').click(function () {
            var servicestaionid = $(this).attr('id');
            var isblocked = $(this).prop("checked");
            getServiceStationBlocked(servicestaionid, isblocked)
        });
    }

    var getServiceStationBlocked = function (servicestationid, isblocked) {
        $.ajax({
            url: 'ServiceStations/GetServiceStationToBlocked',
            type: 'POST',
            data: JSON.stringify({ ServiceStationId: servicestationid, IsBlocked: isblocked }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.split('|')[0] == 0) {
                    $('#MessageResult').html('<div class="col-sm-12"><p>La siguiente acción desbloqueará todas las estaciones</p></div>');
                    $('input:checkbox').each(function () {
                        $(this).prop('checked', false);
                    });
                } else if (data.split('|')[1] == 0) {
                    $('#MessageResult').html('<div class="col-sm-12"><p>La siguiente acción bloqueará todas las estaciones</p></div>');
                    $('input:checkbox').each(function () {
                        $(this).prop('checked', true);
                    });                    
                } else {
                    var id = -1;
                    $('#MessageResult').html('');
                    $('#' + id).prop('checked', false);
                }               
            }
        });
    };

    var initializeDropDownList = function () {
        $('#CostCenterId').select2('data', {});
        $('#CostCenterId').select2({
            escapeMarkup: function (m) { return m; }
        });
    };

    var addServiceStationModal = function () {
        $('#loadForm').find('#id').val('-1');
        $('#loadForm').submit();
    };

    var onSuccessLoad = function () {
        $('#addOrEditModalServiceStations').modal('show');
    };

    var onCountryChange = function () {
        var vCountryId = $("#ddlPais").val();
        $('#ddlProvincesId').empty();
        $('#ddlProvincesId').append('<option value>Seleccione una Provincia</option>');

        $.ajax({
            url: 'ServiceStations/LoadStates',
            type: 'POST',
            data: JSON.stringify({ countryId: vCountryId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    $('#ddlProvincesId').append('<option value="' + data[i].StateId + '">'
                            + data[i].Name +
                            '</option>');
                }
            }
        });
    };

    var onProvinceChange = function () {
        var vCountryId = $("#CountryId").val();
        var vProvinceId = $('#ddlProvincesId').val();
        $('#ddlCantonsId').empty();
        $('#ddlCantonsId').append('<option value>Seleccione un Canton</option>');

        $.ajax({
            url: '/ServiceStations/LoadCantons',
            type: 'POST',
            data: JSON.stringify({ stateId: vProvinceId, countryId: vCountryId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {

                for (var i = 0; i < data.length; i++) {
                    $('#ddlCantonsId').append('<option value="' + data[i].CountyId + '">'
                            + data[i].Name +
                            '</option>');
                }
            }
        });
    };

    var addOrEditModalClose = function () {
        $('#detailContainer').show('slow');
        initialize();
    };

    var addOrEditSuccess = function () {
        $('body').loader('hide');
        $('#detailContainer').hide('slow');
        //initialize();
    };

    var successSearch = function () {
        $('#txtSearchS').val('');
    };

    var deleteModalClose = function () {
        $('#deleteModalStacionesServicio').modal('hide');
        $('#deleteBlockedModal').modal('hide');
        $('body').loader('hide');
    }

    var onClickDownload = function () {
        $('#ExcelReportDownloadForm').submit();
    };

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadExcelForm').submit();
    };

    return {
        Init: initialize,
        AddServiceStationModal: addServiceStationModal,
        OnSuccessLoad: onSuccessLoad,
        OnCountryChange: onCountryChange,
        OnProvinceChange: onProvinceChange,
        SuccessSearch: successSearch,
        AddOrEditModalClose: addOrEditModalClose,
        AddOrEditSuccess: addOrEditSuccess,
        DeleteModalClose: deleteModalClose,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnClickDownload: onClickDownload,
        InitBlockButtons: initBlockButtons
    };
})();