﻿
const app = new Vue({
    el: "#app",
    data: {
        checked: true,
        status: '',
        filter: '',
        UsersWhatsApp: {
            UserId: 0,
            CustomerId: 0,
            Whatsapp: 0
        },
        DriverUser: {
            UserId: 0,
            DecryptedName: "",
            DriversUserId: "",
            CustomerId: 0,
            DecryptedIdentification: "",
            DecryptedEmail: "",
            DecryptedPhoneNumber: "",
            DecryptedUserName: "",
            Whatsapp: 0
        },
        DriverUserList: [],
        UsersWhatsAppList: []
    },
    mounted() {
        this.metodo3;
    },
    methods: {
        driversCellsPhoneRetrieve: async function () {
            const response = await fetch("../DriversCellsPhoneRetrieve", {
                method: "POST",
                data: this.DriverUser
            })
            const result = await response.json();
            this.DriverUserList = await result.Data;
        },
        driversCellsPhoneAddOrEdit: async function () {

            for (let i = 0; i < this.DriverUserList.length; i++) {
                this.UsersWhatsApp.Whatsapp = this.DriverUserList[i].Whatsapp
                this.UsersWhatsApp.CustomerId = this.DriverUserList[i].CustomerId
                this.UsersWhatsApp.UserId = this.DriverUserList[i].UserId

                this.UsersWhatsAppList.push(this.UsersWhatsApp)
            }
            
            const response = await fetch("../DriversCellsPhoneAddOrEdit", {
                headers: { "accept": "application/json", "content-type": "application/json" },
                method: "POST",
                //data: this.DriverUserList
                body: JSON.stringify({
                    data: this.UsersWhatsAppList
                })
            })

            const result = await response.json();
            this.DriverUserList = await result.Data;            
        },
        checkedValid: function () {
            this.DriverUser.Whatsapp = document.getElementById("chkDato").checked ? 1 : 0
        },
        driversCellsPhone: function () {
            console.log(this.DriverUserList)
        },
        checkedAll: function () {
            this.status = !(this.checked) ? "Todos" : "Ninguno"

            for (let i = 0; i < this.DriverUserList.length; i++) {
                this.DriverUserList[i].Whatsapp = !(this.checked) ? 1 : 0
            }
        },
        highlightMatches(text) {
            const matchExists = text.toLowerCase().includes(this.filter.toLowerCase());
            if (!matchExists) return text;

            const re = new RegExp(this.filter, 'ig');
            return text.replace(re, matchedText => `<strong>${matchedText}</strong>`);
        }
    },
    computed: {
        filteredRows() {
            return this.DriverUserList.filter(row => {
                const decryptedName = row.DecryptedName.toString().toLowerCase();
                const decryptedPhoneNumber = row.DecryptedPhoneNumber.toLowerCase();
                const searchTerm = this.filter.toLowerCase();
                return decryptedName.includes(searchTerm) || decryptedPhoneNumber.includes(searchTerm);
            });
        }
    }
})
