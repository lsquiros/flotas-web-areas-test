﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.ProgramSendReports = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        initializeEvents();
    };

    var initializeEvents = function () {
        initializeChangeEvents();
        initializeKeyPressEvents();
        initializeClickEvents();
    }

    var initializeChangeEvents = function () {
        $('.ddlReports').off('change').on('change', function () {
            var module = $(this).attr("module");
            var tempReports = $('#ddlReports_' + module).val();
            if (tempReports != undefined && tempReports != "") {
                $('#Periodicity_' + module).removeClass('hide');
            } else {
                $('#Periodicity_' + module).addClass('hide');
                $('#Shipping_Detail_' + module).addClass('hide');
                $("input[name='checkbox_" + module + "']").removeAttr("checked");
            }
        });

        $('.ddlCostCenterOrVehicleGroup').off('change').on('change', function () {
            var module = $(this).attr("module");
            var tempCcOrVg = $('#CostCenterOrVehicleGroup_' + module).val();
            if (tempCcOrVg == "1") {
                $('#CostCenterId_' + module).removeClass('hide');
                $('#VehicleGroupId_' + module).addClass('hide');
            } else if (tempCcOrVg == "0") {
                $('#VehicleGroupId_' + module).removeClass('hide');
                $('#CostCenterId_' + module).addClass('hide');
            } else {
                $('#CostCenterId_' + module).addClass('hide');
                $('#VehicleGroupId_' + module).addClass('hide');
            }
        });

        $('.ddlEmailOrUser').off('change').on('change', function () {
            var module = $(this).attr("module");
            var tempEmOrUs = $('#EmailOrUser_' + module).val();
            if (tempEmOrUs == "1") { //User
                $('#UserId_' + module).removeClass('hide');
                $('#EmailsDiv_' + module).addClass('hide');
            } else if (tempEmOrUs == "0") { //Email
                $('#EmailsDiv_' + module).removeClass('hide');
                $('#UserId_' + module).addClass('hide');
            } else {
                $('#UserId_' + module).addClass('hide');
                $('#EmailsDiv_' + module).addClass('hide');
            }
        });
    }

    var initializeKeyPressEvents = function () {
        $('.DayOfTheMonth').off('keypress').on('keypress', function (e) {
            //Only numbers
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;

            //Not higher than 31
            if (parseInt(this.value + "" + String.fromCharCode(e.charCode)) > 31 || parseInt(this.value + "" + String.fromCharCode(e.charCode)) < 1) return false;
        });
    }

    var initializeClickEvents = function () {
        $(".rdbMonthly").off('click').on('click', function () {
            var module = $(this).attr("module");
            toggleDisabledDays(module, true);
            $('#DaysDiv_' + module).addClass('hide');
            $('#DayOfTheMonthDiv_' + module).removeClass('hide');
            $('#Shipping_Detail_' + module).removeClass('hide');
        });

        $(".rdbWeekly").off('click').on('click', function () {
            var module = $(this).attr("module");
            toggleDisabledDays(module, false);
            $('#DaysDiv_' + module).removeClass('hide');
            $('#DayOfTheMonthDiv_' + module).addClass('hide');
            $('#Shipping_Detail_' + module).removeClass('hide');
            $('#DayOfTheMonth_' + module).val('');
        });

        $(".rdbDaily").off('click').on('click', function () {
            var module = $(this).attr("module");
            toggleDisabledDays(module, true);
            $('#DaysDiv_' + module).addClass('hide');
            $('#DayOfTheMonthDiv_' + module).addClass('hide');
            $('#Shipping_Detail_' + module).removeClass('hide');
            $('#DayOfTheMonth_' + module).val('');
        });

        $(".btnAddEmail").off('click').on('click', function () {
            var module = $(this).attr('moduleattr');
            var emails = $('#txtEmails_' + module).val();
            if (emails == '' || !validateInputValues(emails)) {
                ECOsystem.Utilities.SetMessageShow("El correo electrónico no es válido.", "ERROR");
                return;
            }
            addEmailToTable(0, emails, module);
            $('#txtEmails_' + module).val('');
        });

        $('.btnSaveReport').off('click').on('click', function () {
            var module = $(this).attr('module');

            var newReport = obtainData(module);
            if (newReport != null && newReport != undefined) {
                ECOsystem.Utilities.ShowLoader();

                $.ajax({
                    url: 'ProgramSendReport/SendProgramReportAddOrEdit',
                    type: 'POST',
                    data: { model: newReport },
                    dataType: 'html',
                    success: function (data) {
                        $('#ModuleConfiguration-' + module).html(data);
                        initializeEvents();
                    },
                    complete: function (data) {
                        ECOsystem.Utilities.HideLoader();
                    }
                });
            }
        });

        $('.btnDiscard').off('click').on('click', function () {
            var module = $(this).attr('module');

            ContId = 0;

            $('#AddReport-' + module).collapse('hide');
            $("#ProgramReportId_" + module).val('');
            $('#ddlReports_' + module).val('');

            $('#Periodicity_' + module).addClass('hide');
            $('#DaysDiv_' + module).addClass('hide');
            $('#DayOfTheMonthDiv_' + module).addClass('hide');
            $('#Daily-2-' + module).prop("checked", false);
            $('#Weekly-2-' + module).prop("checked", false);
            $('#Monthly-2-' + module).prop("checked", false);
            $("#Sunday_" + module).prop("checked", false);
            $("#Monday_" + module).prop("checked", false);
            $("#Tuesday_" + module).prop("checked", false);
            $("#Wednesday_" + module).prop("checked", false);
            $("#Thursday_" + module).prop("checked", false);
            $("#Friday_" + module).prop("checked", false);
            $("#Saturday_" + module).prop("checked", false);
            $('#DayOfTheMonth_' + module).val('');

            $('#Shipping_Detail_' + module).addClass('hide');
            $('#CostCenterId_' + module).addClass('hide');
            $('#VehicleGroupId_' + module).addClass('hide');
            $('#UserId_' + module).addClass('hide');
            $('#EmailsDiv_' + module).addClass('hide');
            $('#CostCenterOrVehicleGroup_' + module).val('');
            $('#CostCenterId_' + module).val('');
            $('#VehicleGroupId_' + module).val('');
            $('#EmailOrUser_' + module).val('');
            $('#UserId_' + module).val('');
            $('#txtEmails_' + module).val('');
            $('#listEmails_' + module).find('tbody').find('tr').remove();
        });
    }

    var toggleDisabledDays = function (id, change) {
        $(".Days_" + id).each(function () {
            $(this).attr("disabled", change);
            if (change) $(this).prop("checked", !change);
        });
    };

    var validateInputValues = function (emails) {
        if (emails != "") {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            var emailList = emails.split(';');

            for (i = 0; i < emailList.length; i++) {
                if (!re.test(emailList[i].trim())) {
                    return false;
                }
            }
        }
        return true;
    };

    var ContId = 0;
    var addEmailToTable = function (id, email, module) {
        if (id == 0) {
            ContId = ContId + 1;
        } else {
            ContId = id;
        }

        $('#listEmails_' + module + ' tbody').append('<tr>' +
            '<td class="hide">' + ContId + '</td> ' +
            '<td>' + email + '</td>' +
            '<td style="text-align:right;"><a href="javascript:void(0)" id="RowId_' + module + ContId + '" class="delete-color" title="Eliminar" onclick=" ECOsystem.Areas.Admin.ProgramSendReports.DeleteEmailFrom(this)"><i class="mdi mdi-32px mdi-close-box"></i></a></td>' +
            '</tr>');
    }

    var deleteEmailFrom = function (obj) {
        $(obj).parent().parent().remove();
    }

    var obtainData = function (module) {
        var data = {}

        data.Id = $("#ProgramReportId_" + module).val(); //For update
        data.Active = 'True';
        data.Module = module;

        var ReportId = $('#ddlReports_' + module).val();
        if (ReportId != undefined && ReportId != "") {
            data.ReportId = ReportId;
        } else {
            ECOsystem.Utilities.SetMessageShow("Debe completar el campo [Reporte].", "ERROR");
            return null;
        }

        /*** Periodicity start ***/
        data.Days = '';
        if ($("#Monthly-2-" + module).prop("checked")) {
            var Days = $('#DayOfTheMonth_' + module).val();
            if (Days != undefined && Days != "" && (0 < Days && Days < 32)) {
                data.Elapse = 30;
                data.Days = Days;
            } else {
                ECOsystem.Utilities.SetMessageShow("Debe completar el campo [Día de entrega].", "ERROR");
                return null;
            }
        } else if ($("#Weekly-2-" + module).prop("checked")) {
            data.Elapse = 7;
            if ($("#Sunday_" + module).prop("checked")) data.Days = data.Days + '1';
            if ($("#Monday_" + module).prop("checked")) data.Days = data.Days + ',2';
            if ($("#Tuesday_" + module).prop("checked")) data.Days = data.Days + ',3';
            if ($("#Wednesday_" + module).prop("checked")) data.Days = data.Days + ',4';
            if ($("#Thursday_" + module).prop("checked")) data.Days = data.Days + ',5';
            if ($("#Friday_" + module).prop("checked")) data.Days = data.Days + ',6';
            if ($("#Saturday_" + module).prop("checked")) data.Days = data.Days + ',7';
            if (data.Days == '') {
                ECOsystem.Utilities.SetMessageShow("Debe completar el campo [Día de entrega].", "ERROR");
                return null;
            }
        } else if ($("#Daily-2-" + module).prop("checked")) {
            data.Elapse = 1;
        } else {
            ECOsystem.Utilities.SetMessageShow("Debe completar el campo [Periodicidad].", "ERROR");
            return null;
        }
        /*** Periodicity end ***/

        /*** Shipping detail start */
        var tempCcOrVg = $('#CostCenterOrVehicleGroup_' + module).val();
        if (module != "A") {
            if (tempCcOrVg == "1") {
                var CostCenterId = $('#CostCenterId_' + module).val();
                if (CostCenterId != "" && CostCenterId != undefined) {
                    data.CostCenterId = CostCenterId;
                } else {
                    ECOsystem.Utilities.SetMessageShow("Debe completar el campo [Centro de costo].", "ERROR");
                    return null;
                }
            } else if (tempCcOrVg == "0") {
                var VehicleGroupId = $('#VehicleGroupId_' + module).val();
                if (VehicleGroupId != "" && VehicleGroupId != undefined) {
                    data.VehicleGroupId = VehicleGroupId;
                } else {
                    ECOsystem.Utilities.SetMessageShow("Debe completar el campo [Grupo de vehículos].", "ERROR");
                    return null;
                }
            } else {
                ECOsystem.Utilities.SetMessageShow("Debe completar el campo [Tipo de filtro].", "ERROR");
                return null;
            }
        } else {
            //Its Administration
            data.CostCenterId = -1;
            data.VehicleGroupId = -1;
        }

        var tempEmOrUs = $('#EmailOrUser_' + module).val();
        if (tempEmOrUs == "1") { //User
            var UserId = $('#UserId_' + module).val();
            if (UserId != "" && UserId != undefined) {
                data.UserId = UserId;
            } else {
                ECOsystem.Utilities.SetMessageShow("Debe completar el campo [Usuarios].", "ERROR");
                return null;
            }
        } else if (tempEmOrUs == "0") { //Email
            var Emails = obtainEmailsData(module);
            if (Emails.length > 0) {
                data.Emails = Emails.join(';')
            } else {
                ECOsystem.Utilities.SetMessageShow("Debe completar el campo [Correos electrónicos].", "ERROR");
                return null;
            }
        } else {
            ECOsystem.Utilities.SetMessageShow("Debe completar el campo [Enviar a].", "ERROR");
            return null;
        }
        /*** Shipping detail end */

        return data;
    };

    var obtainEmailsData = function (module) {
        var list = [];
        $('#listEmails_' + module + ' tr').each(function () {
            var val = $(this).closest('tr').text();
            if (val.split(' ')[1] != undefined && val.split(' ')[1] != '') {
                var arr = {};
                arr = val.split(' ')[1];
                list.push(arr);
            }
        });
        return list;
    }

    var getData = function (obj) {
        var id = obj.id;

        ECOsystem.Utilities.ShowLoader();

        $.ajax({
            url: 'ProgramSendReport/GetData',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({ Id: id }),
            contentType: 'application/json',
            success: function (data) {
                showData(data);
            },
            error: function (data, status, error) {
                console.log(data);
                console.log(status);
                console.log(error);
                ECOsystem.Utilities.SetMessageShow("Ocurrió un problema al intentar consultar el reporte.", "ERROR");
            },
            complete: function () {
                ECOsystem.Utilities.HideLoader();
            }
        });
    };

    var showData = function (data) {
        var module = data.Module;

        /*** Clean window ***/
        $("#Sunday_" + module).prop("checked", false);
        $("#Monday_" + module).prop("checked", false);
        $("#Tuesday_" + module).prop("checked", false);
        $("#Wednesday_" + module).prop("checked", false);
        $("#Thursday_" + module).prop("checked", false);
        $("#Friday_" + module).prop("checked", false);
        $("#Saturday_" + module).prop("checked", false);
        $('#DayOfTheMonth_' + module).val('');
        $('#CostCenterId_' + module).val('');
        $('#VehicleGroupId_' + module).val('');
        $('#UserId_' + module).val('');
        $('#txtEmails_' + module).val('');
        $('#listEmails_' + module).find('tbody').find('tr').remove();
        /*** Clean window ***/

        $("#ProgramReportId_" + module).val(data.Id); //For update
        $('#ddlReports_' + module).val(data.ReportId);

         /*** Periodicity start ***/
        if (data.Elapse == 30) {
            $("#Monthly-2-" + module).prop("checked", true);
            $('#DayOfTheMonth_' + module).val(data.Days);
            $('#DaysDiv_' + module).addClass('hide');
            $('#DayOfTheMonthDiv_' + module).removeClass('hide');
        } else if (data.Elapse == 7) {
            $("#Weekly-2-" + module).prop("checked", true);
            toggleDisabledDays(module, false);
            for (var y = 0; y < data.Days.split(",").length; y++) {
                switch (data.Days.split(",")[y]) {
                    case '1':
                        $("#Sunday_" + module).prop("checked", true);
                        break;
                    case '2':
                        $("#Monday_" + module).prop("checked", true);
                        break;
                    case '3':
                        $("#Tuesday_" + module).prop("checked", true);
                        break;
                    case '4':
                        $("#Wednesday_" + module).prop("checked", true);
                        break;
                    case '5':
                        $("#Thursday_" + module).prop("checked", true);
                        break;
                    case '6':
                        $("#Friday_" + module).prop("checked", true);
                        break;
                    case '7':
                        $("#Saturday_" + module).prop("checked", true);
                        break;
                }
            }
            $('#DaysDiv_' + module).removeClass('hide');
            $('#DayOfTheMonthDiv_' + module).addClass('hide');
        } else if (data.Elapse == 1) {
            $("#Daily-2-" + module).prop("checked", true);
            $('#DaysDiv_' + module).addClass('hide');
            $('#DayOfTheMonthDiv_' + module).addClass('hide');
        }
        /*** Periodicity end ***/

        /*** Shipping detail start */
        if (data.CostCenterId != null) {
            $('#CostCenterOrVehicleGroup_' + module).val("1");
            $('#CostCenterId_' + module).val(data.CostCenterId);
            //Hide
            $('#VehicleGroupId_' + module).addClass('hide');
            $('#CostCenterId_' + module).removeClass('hide');
        } else {
            $('#CostCenterOrVehicleGroup_' + module).val("0");
            $('#VehicleGroupId_' + module).val(data.VehicleGroupId);
            //Hide
            $('#VehicleGroupId_' + module).removeClass('hide');
            $('#CostCenterId_' + module).addClass('hide');
        }

        ContId = 0;

        if (data.UserId != 0) {
            $('#EmailOrUser_' + module).val("1");
            $('#UserId_' + module).val(data.UserId);
            //Hide
            $('#EmailsDiv_' + module).addClass('hide');
            $('#UserId_' + module).removeClass('hide');
        } else {
            $('#EmailOrUser_' + module).val("0");
            if (data.Emails != null && data.Emails != "") {
                var emails = data.Emails.split(';');
                for (var x = 0; x < emails.length; x++) {
                    addEmailToTable(0, emails[x], module);
                }
            }

            //Hide
            $('#EmailsDiv_' + module).removeClass('hide');
            $('#UserId_' + module).addClass('hide');
        }

        //Hide
        $('#AddReport-' + module).collapse('show');
        $('#Periodicity_' + module).removeClass('hide');
        $('#Shipping_Detail_' + module).removeClass('hide');
    };

    /************** Delete report **************/
    var showDeleteModal = function (obj) {
        $('#ReportId').val(obj.getAttribute("reportid"));
        $('#RowId').val(obj.getAttribute("id"));
        $('#deleteModal').modal('show');
    }

    var deleteReport = function () {
        ECOsystem.Utilities.ShowLoader();
        var ReportId = $('#ReportId').val();
        var id = $('#RowId').val();
        $.ajax({
            url: 'ProgramSendReport/DeleteProgramReport',
            type: 'POST',
            dataType: 'html',
            data: {
                Id: id,
                ReportId: ReportId
            },
            success: function (data) {
                $('#' + id).closest('tr').remove();
            },
            error: function (data, status, error) {
                console.log(data);
                console.log(status);
                console.log(error);
                ECOsystem.Utilities.SetMessageShow("Ocurrió un problema al intentar eliminar el reporte. Recargue la página e intentelo nuevamente", "ERROR");
            },
            complete: function () {
                ECOsystem.Utilities.HideLoader();
            }
        });
        $('#deleteModal').modal('hide');
    }
    /************** Delete report **************/

    return {
        Init: initialize,
        DeleteEmailFrom: deleteEmailFrom,
        ShowDeleteModal: showDeleteModal,
        DeleteReport: deleteReport,
        GetData: getData
    };
})();

