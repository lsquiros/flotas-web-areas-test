﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.VehicleSchedule = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        var select2 = $("#VehicleId").select2({
            formatResult: vehicleSelectFormat,
            formatSelection: vehicleSelectFormat,
            escapeMarkup: function (m) { return m; }
        }).data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectChange(data);
                    return fn.apply(this, arguments);
                }
            }
        })(select2.onSelect);


        $('#DetailContainer').off(('click.btnSubmit', 'button[type="submit"]')).on('click.btnSubmit', 'button[type="submit"]', function (e) {
            var array = [];

            $('#scheduleTable td.box.highlighted').each(function() {
                array.push({
                    id: $(this).attr('id')
                });
            });

            $('#JsonSchedule').val(JSON.stringify(array));
            $('#DetailContainer').find('#processing').removeClass('hide');
        });

        $('#DetailContainer').off(('click.btnCancel', 'a[atype="button"]')).on('click.btnCancel', 'a[atype="button"]', function (e) {
            initCanvas();
        });

        $('#clearModal').off(('click.btnClear', 'button[type="reset"]')).on('click.btnClear', 'button[type="reset"]', function (e) {
            clearData();
            $('#clearModal').modal('hide');
        });

        $('#DetailContainer').off('click.btnAddAlarm', '#btnAddAlarm').on('click.btnAddAlarm', '#btnAddAlarm', function (e) {
            $('#alarmForm').find('#id').val($('#VehicleId').val());
            $('#alarmForm').submit();
            $(this).attr('disabled', 'disabled');          
        });

        $('#DetailContainer').off(('mousedown.scheduleTable', '#scheduleTable td')).on('mousedown.scheduleTable', '#scheduleTable td', function (e) {
            isMouseDown = true;
            $(this).toggleClass("highlighted");
            return false; // prevent text selection
        });

        $('#DetailContainer').off(('mouseover.scheduleTable', '#scheduleTable td')).on('mouseover.scheduleTable', '#scheduleTable td', function (e) {
            if (isMouseDown) {
                $(this).toggleClass("highlighted");
            }
        });

        $(document)
          .mouseup(function () {
              isMouseDown = false;
          });
    };

    /* Vehicle Select Format*/
    var vehicleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-2">' + json.PlateId + '</div>' +
                '<div class="col-md-4">' + json.Name + '</div>' +
                //'<div class="col-md-4">' + json.CostCenterName + '</div>' +
                //'<div class="col-md-2">' + json.FuelName + '</div>' +
                '</div>';
    };

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        $('#DetailContainer').find('#processing').addClass('hide');
        //Init canvas for schedule time
        initCanvas();
    };


    //var alarmModalShow = function () {
    //    $('#alarmModalSchedule').modal('show');
    //};

    var alarmMaintenanceModalShow = function () {
        $('#alarmMaintenanceModal').on('shown.bs.modal', function (e) {
            $('.rangeValue').click(function () {
                var val = $(this).val();
                var id = $(this).attr('alarmtipoid');
                $('#demo_' + id).html(val);
            });

            $('.validateRepeat').click(function () {
                var id = $(this).attr('alarmtipoid');
                if ($('#Alarm_' + id).find('#txtAlarmPhone').val() == null ||
                    $('#Alarm_' + id).find('#txtAlarmPhone').val() == '' ||
                    $('#Alarm_' + id).find('#txtAlarmPhone').val() == '0000-0000') {
                    $(this).removeAttr('checked');
                    setAmountErrorMsj('El servicio para el envío de mensajes recurrentes, es por medio de SMS, por lo cual se debe indicar un número de celular para su activación.', true, 'e2');

                } else {
                    setAmountErrorMsj('', true, 'e2');
                    if ($(this).is(":checked")) {
                        $('#rngAlarmRepeatTimes_' + id).removeAttr('disabled');
                        $('#txtAlarmIntervalRepeat_' + id).removeAttr('disabled');
                    } else {
                        $('#rngAlarmRepeatTimes_' + id).attr('disabled', 'disabled');
                        $('#txtAlarmIntervalRepeat_' + id).attr('disabled', 'disabled');
                    }
                }
            });

            $('#btnSendAlarm').click(function () {
                var message = '';
                var error = false;
                var ids = $(this).attr('allIds');
                var items = ids.substring(1, ids.length - 1).split(",")
                var list = [];
                if (ids != null) {
                    for (var i = 0; i < items.length; i++) {
                        message += ValidateInputs(items[i]);
                        if (message != '') {
                            error = true;
                        }
                        list.push(GetValues(items[i]));
                    }
                    setAmountErrorMsj(message, true, 'e2');
                    if (!error) {
                        AddOrEditAlarmMaintenance(list);
                        //$('#alarmMaintenanceModal').modal('hide');
                    }
                }
            });
        }).modal('show');

        $('#btnCloseAlarm').click(function () {
            alarmMaintenanceModalClose();
        });

        $('#btnCancelAlarm').click(function () {
            alarmMaintenanceModalClose();
        });
    };

    var ValidateInputs = function (alarmTriggerId) {
        var message = '';
        //if ($('#Alarm_' + alarmTriggerId).find('#chkAlarmActive').is(":checked")) {
            if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val() == null ||
                $('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val() == '') {

                if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() == null ||
                    $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() == '') {
                    message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Verifique que agregó al menos un correo o un número de celular. <br/>';
                }
                else if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val(), false)) {
                    message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888<br/>';
                }
            }
            else if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val(), true)) {
                message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El correo debe cumplir el siguiente formato: ejemplo1@dominio.com. <br/>';
            } else {
                if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() != undefined &&
                    $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() != null &&
                    $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() != '') {
                    if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val(), false)) {
                        message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888<br/>';
                    }
                }
            }
        //}

        return message;

    };

    var verificarFormato = function (data, email) {
        var result = false;
        var list = data.replace(" ", "").split(";");
        var regex = email ? /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/ : /^([0-9\+\s\+\-])+$/;

        for (var item = 0; item < list.length; item++) {
            if (regex.test(list[item])) {
                result = true;
            }
            else {
                result = false;
                break;
            }
        }
        return result;
    }

    /* Show or hide errors messages*/
    var setAmountErrorMsj = function (msj, show, typeError) {

        typeError = typeError != null ? typeError : 'e1';

        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').removeClass('hide');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').addClass('hide');
                }
                break;
            default:
                break;
        }


    }

    var GetValues = function (alarmTriggerId) {
        var alarma = {};
        alarma.AlarmId = $('#Alarm_' + alarmTriggerId).attr('alarmid');
        alarma.AlarmTriggerId = alarmTriggerId;
        alarma.EntityId = $('#id').val();
        alarma.Email = $('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val();
        alarma.Phone = $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val();
        alarma.RepeatTimes = $('#Alarm_' + alarmTriggerId).find('#rngAlarmRepeatTimes_' + alarmTriggerId).val();
        alarma.IntervalRepeat = $('#Alarm_' + alarmTriggerId).find('#txtAlarmIntervalRepeat_' + alarmTriggerId).val();
        alarma.Active = ($('#Alarm_' + alarmTriggerId).find('#chkAlarmActive').is(":checked")) ? true : false;
           
        if ((alarma.Email == null || alarma.Email == "") && (alarma.Phone == null || alarma.Phone == "")) {
            alarma.Active = false;
        }

        if (!$('#Alarm_' + alarmTriggerId).find('#chkAlarmRepeat').is(":checked") || (alarma.Phone == null || alarma.Phone == "")) {
            alarma.RepeatTimes = 0;
            alarma.IntervalRepeat = 0;
        }

        return alarma;
    }

    var AddOrEditAlarmMaintenance = function (listAlarm) {
        //$('body').loader('show');
        $.ajax({
            url: '/Administration/AlarmMaintenance/AddOrEditAlarmMaintenance',
            type: 'POST',
            data: JSON.stringify({ pListAlarm: listAlarm }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                alarmMaintenanceModalClose();
            }
        });
    };

    var alarmMaintenanceModalClose = function () {
        $('#alarmMaintenanceModal').on('hide.bs.modal', function (e) {
            $('#btnAddAlarm').removeAttr('disabled');
        }).modal('hide');
    };

    /* On Success Load after select Change*/
    var onSelectChange = function (obj) {
        $('#id').val(obj.id);
        //$("input:hidden[id='VehicleGroupId']").val(obj.id);

        if (obj.id === '') {
            $('#DetailContainer').html('');
        } else {
            $('#loadForm').submit();
        }
    };



    var isMouseDown;        //flag we use to keep track
    var boxes;              //boxes information
    var labels;             //labes details

    /*Init Canvas custom control*/
    var initCanvas = function () {
        boxes = [];
        labels = [];

        loadBoxes();
        loadLabels();
        drawTableBody();

        loadData();

    };

    /*Draw Table Body*/
    var drawTableBody = function () {

        var html = "";
        var k = 0; var t = 0; var i = 0; var j = 0;
        for (i = 0; i < 24; i++) {
            html += '<tr>';
            html += '<td class="time">' + labels[t++] + '</td>';
            for (j = 0; j < 7; j++) {
                if (boxes[k].checked)
                    html += '<td class="box highlighted" id=' + boxes[k++].id + '>&nbsp;</td>';
                else
                    html += '<td class="box" id=' + boxes[k++].id + '>&nbsp;</td>';
            }
            html += '<td class="time2">' + labels[t++] + '</td>';
            for (j = 0; j < 7; j++) {
                if (boxes[k].checked)
                    html += '<td class="box highlighted" id=' + boxes[k++].id + '>&nbsp;</td>';
                else
                    html += '<td class="box" id=' + boxes[k++].id + '>&nbsp;</td>';
            }
            html += '</tr>';
        }

        $('#scheduleBody').html(html);
    };

    /*Load data from DB*/
    var clearData = function () {
        for (var i = 0; i < boxes.length; i++) {
            boxes[i].checked = false;
        }
        drawTableBody();
    };

    /*Load data from DB*/
    var loadData = function () {
        if ($('#JsonSchedule').val().length == 0)
            return;
        try {
            var array = JSON.parse($('#JsonSchedule').val());
            for (var i = 0; i < array.length; i++) {
                for (var j = 0; j < boxes.length; j++) {
                    if (boxes[j].id == array[i].id) {
                        boxes[j].checked = true;
                        break;
                    }
                }
            }
            drawTableBody();
        } catch (e) {
            console.debug('Error loading json from DB... ' + e);
        }
    };

    /*Load Schedule Hours*/
    var loadLabels = function () {

        labels.push("12:00 AM");
        labels.push("12:00 PM");
        labels.push("12:30 AM");
        labels.push("12:30 PM");

        for (var i = 1; i < 12; ++i) {
            labels.push(pad2(i) + ":00 AM");
            labels.push(pad2(i) + ":00 PM");
            labels.push(pad2(i) + ":30 AM");
            labels.push(pad2(i) + ":30 PM");
        }
    };

    /*Load Schedule Boxes*/
    var loadBoxes = function () {

        var k = 0; var i = 0; var j = 0;
        for (i = 0; i < 24; i++) {
            for (j = 0; j < 7; j++) {
                boxes.push({

                    id: pad2(k) + (i % 2 == 0 ? '0' : '3') + '0' + pad2(j),
                    checked: false
                });
            }
            for (j = 0; j < 7; j++) {
                boxes.push({
                    id: pad2(k + 12) + (i % 2 == 0 ? '0' : '3') + '0' + pad2(j),
                    checked: false
                });
            }
            if (i % 2 > 0) k++;
        }
    };

    //Pad a number to two digits
    var pad2 = function (number) {
        return (number < 10 ? '0' : '') + number;
    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        InitCanvas: initCanvas,
        AlarmMaintenanceModalShow: alarmMaintenanceModalShow
    };
})();

