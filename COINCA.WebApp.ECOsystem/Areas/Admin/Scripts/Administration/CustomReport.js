﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.CustomReport = (function () {
    var options = {};
    var res;

    var initialize = function (opts) {
        $.extend(options, opts);
        InitControls();
        $('#tooltipreport').tooltip('disable');
    }

    var InitControls = function () {

        $('#CReportId').select2({});

        $('#btnAddReport').click(function () {
            $('#CReportId').val('').change();
            $('#showReportInfo').submit();
        });

        $('#btnCreateReport').click(function () {
            reportAddOrEdit();
        });

        $('#btnDelete').click(function () {
            reportAddOrEdit();
        });

        $('#btnRemoveColumn').click(function () {
            var id = $(this).attr('columnid');
            $('#sortableColumns .' + id).remove();
            $('#ConfirmRemoveColumn').modal('hide');
        });

        $('#btnCancelFormula').click(function () {
            clearColumnsPanel();
            $('#CancelCreateFormula').modal('hide');
        });

        $('#btnEditReport').click(function () {
            if ($('#CReportId').val() == 0) {
                $('#tooltipreport').tooltip('enable');
                $('#tooltipreport').tooltip().mouseover();
            } else {
                $('#tooltipreport').tooltip('disable');
                $('#showReportInfo').submit();
            }
        })
    }

    var onLoadReport = function () {
        $('#SourceId').select2({ allowClear: true });
        $('#ColumnId').select2({ allowClear: true });

        $('#SourceId').on('change', function () {
            $('#ColumnContainer').hide();
            $('#FormulaContainer').hide();

            if ($(this).val() == 8) {
                getFormulaValues();
            }
            else {
                getColumnValues($(this).val(), null, '#ColumnId');
                $('#ColumnContainer').show('slow');
            }

            if (this.value != "" && this.value != undefined) {
                $("#s2id_SourceId").css("border", "");
            } else {
                $("#s2id_SourceId").css("border", "2px solid red");
            }
        });

        $('#ColumnId').on('change', function () {
            $('#chkFinalSummation').removeAttr('checked');
            if ($(this).find('option:selected').attr('summ') == "56")
                $(divFinalSummation).show();
            else
                $(divFinalSummation).hide();
        });

        $('#btnAddColumn').click(function () {
            var maxId = $('#MaxId').val();
            addColumnToDetail($('#ColumnId').val(), null, $('#ColumnId').select2('data').text, null, isNaN(maxId) ? 1 : parseInt(maxId) + 1, $('#chkFinalSummation').is(":checked") ? 1 : 0);
        });

        $('#btnAddFormula').click(function () {
            addOrEditFormula();
        });

        $("#sortableColumns").sortable();
        initToolTip();
        removeEditColumn();
    }

    var onbtnReportClick = function () {
        var id = $('#CReportId').val();
        $.ajax({
            url: '/CustomReport/ReportDelete',
            type: "POST",
            data: JSON.stringify({ Id: id }),
            contentType: "application/json",
            success: function (result) {
                $('#DeleteReportModal').modal('hide');
                //setConfirmationDelete();
                ECOsystem.Utilities.SetMessageShow("La información fue eliminada correctamente!", "SUCCESS");
                window.location.reload();

            }
        });
    };
    var setConfirmationDelete = function () {
        $("#confirmation").html("La información fue eliminada correctamente!")
        $("#confirmation").removeClass("out");
        $("#confirmation").css("display", "block");
        window.setTimeout(function () {
            $("#confirmation").addClass("in");
        }, 100);

        window.setTimeout(function () {
            $("#confirmation").removeClass("in");
            $("#confirmation").addClass("out");
            window.setTimeout(function () {
                $("#confirmation").css("display", "none");
            }, 2000);
        }, 2000);
    }

    var addOrEditFormula = function () {
        var formulaName = $('#FormulaName').val();
        var formulaDetail = getTextBoxSelect2Text($('#FormulaDetail').select2('data'), true);
        
        var finalSummation = $('#chkFinalSummationFormula').is(":checked") ? 1 : 0;
        if (formulaName == '' || formulaDetail == '') {
            $('#formulaWarning').html('Por favor ingresar el nombre y texto de la Fórmula.');
        } else if (formulaDetail == "Error =") {
            $('#formulaWarning').html('Por favor validar que  la formula no contenga los valores = .');
        } else {
            var isEdition = $('#IsEdition').val();
            var maxId = $('#MaxId').val();
            var orderId = $('#OrderId').val();
            if (isEdition) {
                $('#sortableColumns').find('.' + orderId).attr('formula', formulaDetail);
                $('#sortableColumns').find('.' + orderId).attr('finalSummation', finalSummation);
                $('#sortableColumns').find('.' + orderId).html('<span data-toggle="tooltip" data-placement="bottom" title="' + getTextBoxSelect2Text($('#FormulaDetail').select2('data'), false) + '" >&nbsp;' +
                    formulaName + '&nbsp; <i class="mdi mdi-pencil-box editColumn" columnid="' + orderId + '" formulaName="' + formulaName + '" formula="' + formulaDetail + '" finalSummation="' + finalSummation + '"></i>' +
                    '<i class="mdi mdi-close-box removeColumn" columnid="' + orderId + '"></i> &nbsp;</span>');
                removeEditColumn();
                clearColumnsPanel();
            } else {
                addColumnToDetail(-1, formulaDetail, formulaName, getTextBoxSelect2Text($('#FormulaDetail').select2('data')), isNaN(maxId) ? 1 : parseInt(maxId) + 1, finalSummation);
            }
            $('#formulaWarning').html('');
        }
    }

    var getFormulaValues = function () {
        $('#chkFinalSummationFormula').removeAttr('checked');
        $('#FormulaContainer').show('show');
        $('#IsEdition').val('');
        getColumnValues(null, true, '');
    }

    var getColumnValues = function (id, isFormula, select2Id) {
        $.ajax({
            url: '/CustomReport/GetColumnsValues',
            type: "POST",
            data: JSON.stringify({ ResourceId: id, IsFormula: isFormula }),
            contentType: "application/json",
            success: function (result) {
                if (select2Id == '') {
                    var data = [];
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].Text != undefined) {
                            var res = result[i].Text.split("-");
                            data.push({ "id": result[i].Value, "text": res[0], "summ": res[1] });
                        }
                    }
                    $('#FormulaDetail').select2({
                        escapeMarkup: function (m) { return m; },
                        tags: data,
                        allowRepetitionForMultipleSelect: true,
                        multiple: true,
                        separator: ','
                    });
                }
                else {
                    $(select2Id).empty().change();
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].Text != undefined) {
                            var res = result[i].Text.split("-");
                            $(select2Id).append($('<option></option>').attr("summ", res[1]).val(result[i].Value).html(res[0]));
                        }
                    }
                }
            }
        });
    }

    var addColumnToDetail = function (id, formula, text, formulaDetail, order, FinalSummation) {
        if (formulaDetail == null) {
            $("#sortableColumns").append('<li class="ui-state-default ' + order + '"  id="' + id + '" formula="' + formula + '" finalSummation="' + FinalSummation + '">&nbsp;' + text + '&nbsp; <i class="mdi mdi-close-box removeColumn" columnid="' + order + '"></i> &nbsp;</li > ');
        } else {
            $("#sortableColumns").append('<li class="ui-state-default ' + order + '"  id="' + id + '" formula="' + formula + '" finalSummation="' + FinalSummation + '">' +
                '<span data-toggle="tooltip" data-placement="bottom" title="' + formulaDetail + '" >&nbsp;' + text +
                '&nbsp; <i class="mdi mdi-pencil-box editColumn" columnid="' + order + '" formulaName="' + text + '" formula="' + formula + '" finalSummation="' + FinalSummation + '"></i>' +
                '<i class="mdi mdi-close-box removeColumn" columnid="' + order + '"></i> &nbsp;</span></li > ');
        }
        $('#MaxId').val(order);
        $("#sortableColumns").sortable();
        removeEditColumn();
        initToolTip();
        clearColumnsPanel();
    }

    var validateAddedTablesFor = function () {
        var arr = [];
        $('#sortableColumns li').each(function () {
            var id = $(this).attr('id');
            arr.push(id);
        });

        if (arr.length > 0) {
            $("#s2id_SourceId").css("border", "");
        } else {
            $("#s2id_SourceId").css("border", "2px solid red");
            ECOsystem.Utilities.SetMessageShow("Debe ingresar al menos una columna para el reporte.", "ERROR");
        }
        return arr.length > 0;
    };

    var reportValidate = function () {
        $("#formAddOrEditCustomReport").validate();

        var valid = validateAddedTablesFor();
        if ($("#formAddOrEditCustomReport").valid() && valid) {
            $("#ConfirmSaveReport").modal("show");
        }
    }

    var reportAddOrEdit = function () {
        var arr = [];
        var model = {};

        model.ReportId = $('#ReportId').val();
        model.ReportName = $('#ReportName').val();
        model.SheetName = $('#SheetName').val();

        clearColumnsPanel();
        var i = 1;

        $('#sortableColumns li').each(function () {
            var data = {};
            data.id = $(this).attr('id');
            data.formula = $(this).attr('formula') == "null" ? null : $(this).attr('formula');
            data.formulaName = $(this).attr('formula') != "null" ? $(this).html().split('&nbsp;')[1].replace('&nbsp;', '') : null;
            data.order = i++;
            data.finalSummation = ($(this).attr('finalSummation') != undefined ? $(this).attr('finalSummation') : 0) == "1" ? 1 : 0;
            arr.push(data);
        });

        model.ColumnList = arr;

        $('#ReportWarning').hide();
        showLoader();
        $.ajax({
            url: '/CustomReport/ReportAddOrEdit',
            type: "POST",
            data: JSON.stringify({ model: model }),
            contentType: "application/json",
            success: function (result) {
                $('#ConfirmSaveReport').modal('hide');
                ECOsystem.Utilities.SetMessageShow("La información fue almacenada correctamente!", "SUCCESS");
                window.location.reload();
            }
        });
    }

    var clearColumnsPanel = function () {
        $('#FormulaName').val('');
        $('#FormulaDetail').val('');
        $('#FormulaContainer').hide('slow');
        $('#ColumnContainer').hide('slow');
        $('#SourceId').select2('val', '');
    }

    var removeEditColumn = function () {
        $('.removeColumn').click(function (e) {
            var id = $(this).attr('columnid');
            $('#btnRemoveColumn').attr('columnid', id);
            $('#ConfirmRemoveColumn').modal('show');
            e.preventDefault();
        });

        $('.editColumn').click(function (e) {
            getColumnValues(null, true, '');
            $('#IsEdition').val(true);
            $('#OrderId').val($(this).attr('columnid'));
            $('#FormulaName').val($(this).attr('formulaName'));
            $('#FormulaDetail-selection-no-filter').val(removePipes($(this).attr('formula')));
            $('#FormulaDetail').val(removePipes($(this).attr('formula')));
            $('#IsEdition').val(true);
            $('#OrderId').val($(this).attr('columnid'));
            $('#FormulaName').val($(this).attr('formulaName'));
            $('#FormulaDetail-selection-no-filter').val($(this).attr('formula'));
            //$('#FormulaDetail').val($(this).attr('formula'));
            $('#FormulaContainer').show('show');
            $(this).attr('finalSummation') == "1" ? $('#chkFinalSummationFormula').attr('checked', 'checked') : $('#chkFinalSummationFormula').removeAttr('checked');
            e.preventDefault();
        });
    }

    var removePipes = function (str) {
        var i = 0, strLength = str.length;
        for (i; i < strLength; i++) {
            str = str.replace('|', '');
        }
        return str;
    }

    var getTextBoxSelect2Text = function (obj, values) {
        var text = '';
        for (var i = 0; i < obj.length; i++) {
            if (values) {
                if (isNaN(obj[i].text)) {
                    if (obj[i].text == '=') return "Error =";
                    text = text == '' ? obj[i].id : text + ',' + obj[i].id;
                } else {                    
                    text = text == '' ? obj[i].text : text + ',|' + obj[i].text;
                }
            } else {
                text = text == '' ? obj[i].text : text + obj[i].text;
            }
        }
        return text;
    }

    /*invalid Select Option*/
    var invalidSelectOption = function (mensaje) {
        $('#ControlsContainer').html('<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
            ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' + mensaje + '</div>');
    };

    //Control Methods
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function (data) {
        if (data > 0) {
            $('body').loader('hide');
            $('#DonwloadFileForm').submit();
        } else {
            $('body').loader('hide');
            invalidSelectOption('No datos encontrados con los filtros seleccionados.');
        }        
        //$('body').loader('hide');
    };

    var initToolTip = function () {
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    }
   
    return {
        Init: initialize,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnLoadReport: onLoadReport,
        OnbtnReportClick: onbtnReportClick,
        ReportValidate: reportValidate
    };
})();
