﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.CreditCard = (function () {
    var options = {};


    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        loadGridCardEvents();
        initializeDropDownList();

        $('#detailContainer').off('blur.currencyOnBlur', 'input[data-currency]').on('blur.currencyOnBlur', 'input[data-currency]', function (e) {
            var obj = $(this);
            obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
            obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));
        });
        onClickGenerateCardFile();

        $('#gridContainer').off('click.chkCheckAll', '#chkCheckAll').on('click.chkCheckAll', '#chkCheckAll', function () {
            var check = $(this);
            $('#gridContainer').find('input[type=checkbox]').each(function () {                
                if (check.is(':checked')) {
                    $(this).prop('checked', true);
                    $(this).closest('tr').addClass('selectRow');
                } else {
                    $(this).prop('checked', false);
                    $(this).closest('tr').removeClass('selectRow');                  
                }
            });
        });

        $('#addOrEditModalCard').on('shown.bs.modal', function () {
            $('#CardInfo_StatusId').select2('data', {});
            $('#CardInfo_StatusId').select2({
                escapeMarkup: function (m) { return m; },
                allowClear: true
            });
        });

        $('.btnUpdateSelectedValues').click(function () {
            debugger
            var arr = [];
            $('#gridContainer input[type=checkbox]').each(function (i) {
                if ($(this).is(':checked')) {
                    if ($(this).attr("id") != 'chkCheckAll') {
                        if ($(this).attr('data-statusId') != 0) {
                            var data = {};
                            data.id = $(this).attr('id');
                            data.cardName = $(this).attr('data-cardname');
                            data.cardNumber = $(this).attr('data-creditcardnumber');
                            data.cardType = $(this).attr('data-creditcardtype');
                            data.PaymentInstrumentType = $(this).attr('PaymentInstrumentType');
                            arr.push(data);
                        }
                    }
                }
            });

            if (arr.length > 0) {
                $('#listCardTable tbody').html('');
                for (var i = 0; i < arr.length; i++) {
                    $('#listCardTable tbody').append('<tr>' +
                        '<th class="hide">' + arr[i].id + '|</th>' +
                        '<th>' + arr[i].cardName + '</th>' +
                        '<th>' + arr[i].cardNumber + '</th>' +
                        '<th>' + arr[i].cardType + '</th>' +
                        '<th class="hide">|' + arr[i].PaymentInstrumentType + '</th>' +
                        '</tr>')
                }
                $('#addOrEditModalCard').modal('show');
            } else {
                $('#ValidateSelectionModal').modal('show');
            }
        });

        $('#btnShowConfirmation').click(function () {
            var cardStatus = $('#CardInfo_StatusId').val();
            if (cardStatus == '') {
                $('#StatusIdValidation').removeClass('hide');
                return;
            }
            $('#StatusIdValidation').addClass('hide');
            $('#ConfirmationEditModalCard').modal('show');
            $('#addOrEditModalCard').modal('hide');
        })

        $('#btnChangeStatus').click(function () {
            var cardStatus = $('#CardInfo_StatusId').val();
            var arr = [];
            var arrPayment = [];

            $('#listCardTable tr').each(function () {
                debugger
                var val = $(this).closest('tr').text();
                if (val.split('|')[1] != undefined) {
                    arr.push(val.split('|')[0]);
                    arrPayment.push(val.split('|')[2])
                }
            });
            
            $('body').loader('show');
            $('#ConfirmationEditModalCard').modal('hide');
            $.ajax({
                url: '/CreditCard/EditCreditCard',
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ IdList: arr, StatusId: cardStatus, TypeList: arrPayment }),
                success: function (result) {
                    if (result == 'success') window.location.reload();
                }
            });
        });

        $("#txtSearch").on('keydown', function () {
            var key = event.keyCode || event.charCode;

            if (key == 8 || key == 46) {
                if ($(this).val().length == 0) {
                    $("#btnSearch").trigger("click");
                }
            }
        });
    };

    /*initializeVpos*/
    var initializeVpos = function (opts) {
        $.extend(options, opts);
        loadExecuteEvent();
        loadVPOSSelectTransactSales();
    };

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#StatusId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectStatusChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

    };


    /*on Select Report Criteria Change*/
    var onSelectStatusChange = function (obj) {
        
        $('#searchForm').find("#status").val(obj.id);
        //showLoader();
        $('#searchForm').submit();
    };

    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    /*onClickGenerateCardFile*/
    var onClickGenerateCardFile = function () {        
        $("body").on("click", "#btnGenerarTarjetas", function (e) {
            var creditCardList = "";
            $('div[data-gridname="CreditCardGrid"] table input[type="Checkbox"]').each(function (key, val) {
                if ($(this).is(':checked'))
                {
                    creditCardList = creditCardList + ($(this).attr('id')) + ',';
                }                
            });
            $.ajax({
                url: ECOsystem.Utilities.GetUrl() + '/CreditCard/CreateCardFile',
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ creditCardList: creditCardList }),
                success: function (result) {
                    $("#detailContainer").html(result);
                    //window.location.reload();
                }
            });
        });
    };

    /*loadExecuteEvent*/
    var loadExecuteEvent = function () {
        $('#gridContainer').off('click.creditcard', 'a[data-execute_transaction]').on('click.creditcard', 'a[data-execute_transaction]', function (e) {
            $("#accountNumber").val($(this).attr("data-AccountNumber"));
            $("#expirationDate").val($(this).attr("data-ExpirationDate"));
            $("#creditCardId").val($(this).attr("data-CreditCardId"));
            $("#customer").html($(this).attr("data-Customer"));
            $("#CustomerNameText").val($(this).attr("data-Customer"));
            $('#partnerId').val($(this).attr('data-partnerid'));
            $('#customerId').val($(this).attr('data-customerid'));
            $('#PlateId').val($(this).attr('data-plateid'));
            $('#IsPreauthorized').val($(this).attr('data-ispreauthorized'));
            $('#UnitName').val($(this).attr('data-UnitName'));
            $('#PreAuthorized').html('');

            Sale();
            $("#errorMessage").html("");
            $('#addOrEditModal').modal('show');
        });

        $('#gridContainer').off('click.creditcard', 'a[data-execute_preauthorized_transaction]').on('click.creditcard', 'a[data-execute_preauthorized_transaction]', function (e) {
            
            $("#accountNumber").val($(this).attr("data-AccountNumber"));
            $("#expirationDate").val($(this).attr("data-ExpirationDate"));
            $("#creditCardId").val($(this).attr("data-CreditCardId"));
            $("#customer").html($(this).attr("data-Customer"));
            $("#CustomerNameText").val($(this).attr("data-Customer"));
            $('#partnerId').val($(this).attr('data-partnerid'));
            $('#customerId').val($(this).attr('data-customerid'));
            $('#PlateId').val($(this).attr('data-plateid'));
            $('#IsPreauthorized').val($(this).attr('data-ispreauthorized'));
            $('#UnitName').val($(this).attr('data-UnitName'));
            $('#PreAuthorized').html('Preautorizada');

            SalePre();
            $("#errorMessage").html("");
            $('#addOrEditModal').modal('show');
        });

        $('#gridContainer').off('click.creditcard', 'a[data-execute_reverseorvoid]').on('click.creditcard', 'a[data-execute_reverseorvoid]', function (e) {
            $("#errorMessage").html("");
            $("#retrieveSalesForm").find('#creditCardId').val($(this).attr("data-CreditCardId"));
            $("#retrieveSalesForm").submit();
            //$("#creditCardId").val($(this).attr("data-CreditCardId"));
            //LoadFormData($("#creditCardId").val());
        });

        $('#detailContainer').off('blur.inputblur', '#BacId').on('blur.inputOnBlur', '#BacId', function () {
            
            var afiliado = $('#BacId').val();

            if(afiliado == '')
            {
                $('#BacAfiliadoRequired').html('El número de afiliado es requerido');
            }
            else
            {
                $('#BacAfiliadoRequired').html('');

                $.ajax({
                    url: '/ServiceStations/GetServiceStationName',
                    type: 'POST',
                    data: JSON.stringify({ bacAfiliado: afiliado }),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (result)
                    {
                        
                        $('#ServiceStationName').val(result);
                        $("#errorMessage").html('');
                    },
                    error: function (xhr) {
                        $('#ServiceStationName').val('');
                        $("#errorMessage").html('Error: No existen estaciones de servicio');
                    }
                });
            }

        });

        $('#btnVoid').off('click.btnVoid').on('click.btnVoid', function (e) {
            ECOsystem.Utilities.Spin("#btnVoid>i", "glyphicon-remove");
            SetFormData('Void');
        });
        
        $('#btnReverse').off('click.btnReverse').on('click.btnReverse', function (e) {
            ECOsystem.Utilities.Spin("#btnReverse>i", "glyphicon-remove");
            SetFormData('Reverse');
        });

        $('#btnSale').off('click.btnSale').on('click.btnSale', function (e) {
            
            ECOsystem.Utilities.Spin("#btnSale>i", "glyphicon-ok");
            var val = $('#IsPreauthorized').val();

            if (val == 0)
                SetFormData('Sale');
            else
                SetFormDataPreauthorized('PreauthorizedSale');
        });
        

        //$('#gridContainer').off('click.execute', 'a[dta-execute_transaction]').on('click.execute', 'a[dta-execute_transaction]', function(e) {
        //    $('#loadForm').find('#id').val($(this).attr('data-id'));
        //    $('#loadForm').find('#partnerId').val($(this).attr('data-partnerid'));
        //    $('#loadForm').find('#customerId').val($(this).attr('data-customerid'));
        //    $('#loadForm').find('#customer').val($(this).attr('data-customer'));
        //    $('#loadForm').find('#number').val($(this).attr('data-number'));
        //    $('#loadForm').find('#expirationYear').val($(this).attr('data-expirationYear'));
        //    $('#loadForm').find('#expirationMonth').val($(this).attr('data-expirationMonth'));
        //    $('#loadForm').submit();
        //});
    };

    /* Set FormData */
    var SetFormData = function(transaction) {
        if (!Validate()) {
            $("#errorMessage").html("Favor verificar el formato de los datos de entrada!");
            ECOsystem.Utilities.Unspin("#btnSale>i", "glyphicon-ok");
            ECOsystem.Utilities.Unspin("#btnReverse>i", "glyphicon-remove");
            ECOsystem.Utilities.Unspin("#btnVoid>i", "glyphicon-remove");
            return;
        }
        $("#errorMessage").html("");
        var creditCardTransaction = {
            CreditCardId: $("#creditCardId").val(),
            PartnerId: $("#partnerId").val(),
            CustomerId: $("#customerId").val(),
            Customer: $("#customer").html(),
            AccountNumber: $("#accountNumber").val(),
            ExpirationDate: $("#expirationDate").val(),
            TotalAmount: $("#totalAmount").val(),
            Odometer: $("#odometer").val(),
            Liters: $("#liters").val(),
            Plate: $("#plate").val(),
            AuthorizationNumber: 0,
            ReferenceNumber: 0,
            SystemTraceNumber: 0,
            TerminalId: $("#terminal").val(),
            UnitName: $('#UnitName').val()
        };

        $.ajax({
            url: 'ExecuteCreditCard' + transaction,
            type: 'POST',
            data: JSON.stringify(creditCardTransaction),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            async: true,
            success: function (result) {
                $("#authorizationNumber").val(result.Data.AuthorizationNumber);
                $("#referenceNumber").val(result.Data.ReferenceNumber);
                $("#systemTraceNumber").val(result.Data.SystemTraceNumber);
                if (result.Data.ErrorMessage != "") {
                    $("#errorMessage").html(result.Data.ErrorMessage);
                }
                else {
                    ReverseOrVoid();
                }
                ECOsystem.Utilities.Unspin("#btnSale>i", "glyphicon-ok");
                ECOsystem.Utilities.Unspin("#btnReverse>i", "glyphicon-remove");
                ECOsystem.Utilities.Unspin("#btnVoid>i", "glyphicon-remove");
            },
            error: function (xhr) {
                ECOsystem.Utilities.Unspin("#btnSale>i", "glyphicon-ok");
                ECOsystem.Utilities.Unspin("#btnReverse>i", "glyphicon-remove");
                ECOsystem.Utilities.Unspin("#btnVoid>i", "glyphicon-remove");
                $("#errorMessage").html('Error: ' + xhr.statusText);
            }
        });
    }

    /* Set FormData Preauthorized */
    var SetFormDataPreauthorized = function (transaction) {
        
        if (!ValidatePre()) {
            $("#errorMessage").html("Favor verificar el formato de los datos de entrada!");
            ECOsystem.Utilities.Unspin("#btnSale>i", "glyphicon-ok");
            ECOsystem.Utilities.Unspin("#btnReverse>i", "glyphicon-remove");
            ECOsystem.Utilities.Unspin("#btnVoid>i", "glyphicon-remove");
            return;
        }
        $("#errorMessage").html("");
        var creditCardTransaction = {
            CreditCardId: $("#creditCardId").val(),
            PartnerId: $("#partnerId").val(),
            CustomerId: $("#customerId").val(),
            Customer: $("#customer").html(),
            AccountNumber: $("#accountNumber").val(),
            ExpirationDate: $("#expirationDate").val(),
            TotalAmount: $("#totalAmount").val(),
            Odometer: $("#odometer").val(),
            Liters: $("#liters").val(),
            Plate: $("#plate").val(),
            AuthorizationNumber: $("#authorizationNumber").val(),            
            TerminalId: $("#TerminalId").val(),
            BACAfiliado: $("#BacId").val(),
            ServiceStationName: $("#ServiceStationName").val(),
            DriverCode: $("#DriverCode").val(),
            UnitName: $('#UnitName').val()
        };

        $.ajax({
            url: 'ExecuteCreditCard' + transaction,
            type: 'POST',
            data: JSON.stringify(creditCardTransaction),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            async: true,
            success: function (result) {
                $("#AuthorizationNumber").val(result.Data.AuthorizationNumber);
                $("#ReferenceNumber").val(result.Data.ReferenceNumber);
                $("#SystemTraceNumber").val(result.Data.SystemTraceNumber);
                if (result.Data.ErrorMessage != "")
                {
                    $("#errorMessage").html(result.Data.ErrorMessage);
                }
                else
                {
                    ReverseOrVoidPre();
                }
                ECOsystem.Utilities.Unspin("#btnSale>i", "glyphicon-ok");
                ECOsystem.Utilities.Unspin("#btnReverse>i", "glyphicon-remove");
                ECOsystem.Utilities.Unspin("#btnVoid>i", "glyphicon-remove");
            },
            error: function (xhr) {
                ECOsystem.Utilities.Unspin("#btnSale>i", "glyphicon-ok");
                ECOsystem.Utilities.Unspin("#btnReverse>i", "glyphicon-remove");
                ECOsystem.Utilities.Unspin("#btnVoid>i", "glyphicon-remove");
                $("#errorMessage").html('Error: ' + xhr.statusText);
            }
        });
    }
    
    /* Load FormData */
    var LoadFormData = function(creditCardId) {
        $("#errorMessage").html("");
        $.ajax({
            url: '@retrieveCreditCardSale',
            type: 'POST',
            data: '{CreditCardId: ' + creditCardId + '}',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (result) {
                $("#authorizationNumber").val(result.AuthorizationNumber);
                $("#referenceNumber").val(result.ReferenceNumber);
                $("#systemTraceNumber").val(result.SystemTraceNumber);

                $("#accountNumber").val(result.AccountNumber);
                $("#expirationDate").val(result.ExpirationDate);
                $("#creditCardId").val(result.CreditCardId);
                $("#customer").html(result.Customer);

                $("#totalAmount").val(result.TotalAmount);
                $("#odometer").val(result.Odometer);
                $("#liters").val(result.Liters);
                $("#plate").val(result.Plate);

                ReverseOrVoid();

                $('#addOrEditModal').modal('show');
            },
            error: function (xhr) {
                alert("No existen transacciones previas.");
                //$('Error: No existen transacciones previas ' + xhr.statusText);
            }
        });
    }

    /* Reverse Or Void Enable or Disable controls */
    var ReverseOrVoid = function() {
        $("#btnSale").attr("disabled", true);
        $("#totalAmount").attr("disabled", true);
        $("#odometer").attr("disabled", true);
        $("#liters").attr("disabled", true);
        $("#plate").attr("disabled", true);

        $("#btnReverse").attr("disabled", false);
        $("#btnVoid").attr("disabled", false);
    }

    var ReverseOrVoidPre = function () {
        $("#btnSalePreauthorize").attr("disabled", true);

        $("#btnReversePre").attr("disabled", false);
        $("#btnVoidPre").attr("disabled", false);
    }

    /* Sale Enable or Disable controls */
    var Sale = function() {
        $("#btnSale").attr("disabled", false);
        $("#totalAmount").attr("disabled", false);
        $("#odometer").attr("disabled", false);
        $("#liters").attr("disabled", false);
        $("#plate").attr("disabled", false);
        $('#AnswerData').html('Datos de Respuesta');
        $("#authorizationNumber").attr("disabled", true);
        $("#authorizationNumber").attr("placeholder", "");
        $("#systemTraceNumber").show();
        $("#referenceNumber").show();
        $("#systemTraceNumberLabel").show();
        $("#referenceNumberLabel").show();

        $("#btnReverse").attr("disabled", true);
        $("#btnVoid").attr("disabled", true);
    }

    var SalePre = function () {
        $("#btnSalePreauthorize").attr("disabled", false);
        $('#AnswerData').html('');
        $("#authorizationNumber").attr("disabled", false);
        $("#authorizationNumber").attr("placeholder", "Número de Autorización");
        $("#systemTraceNumber").hide();
        $("#referenceNumber").hide();
        $("#systemTraceNumberLabel").hide();
        $("#referenceNumberLabel").hide();

        $("#btnReverse").attr("disabled", true);
        $("#btnVoid").attr("disabled", true);
    }

    /* Sale Enable or Disable controls */
    var Validate = function () {        
        return $.isNumeric($("#totalAmount").val()) & $.isNumeric($("#odometer").val()) & $.isNumeric($("#liters").val()) & ($("#plate").val().length > 0 ? true : false);
    }

    /* Sale Enable or Disable controls */
    var ValidatePre = function () {
        return $.isNumeric($("#totalAmount").val()) & $.isNumeric($("#odometer").val()) & $.isNumeric($("#liters").val()) & ($("#plate").val().length > 0) & ($("#authorizationNumber").val().length == 6 ? true : false);
    }

    var showModalVPOS_VOID = function() {
        $('#addOrEditModalVPOS_VOID').modal('show');
        //ECOsystem.CreditCard.LoadSelectTransacVPOS();
        loadVPOSSelectTransactSales();
    }

    var loadVPOSSelectTransactSales = function () {
        $("#SystemTraceNumber").select2('data', {});
        $("#SystemTraceNumber").select2({
            //minimumInputLength: 2,
            formatResult: transactSalesSelectFormat,
            formatSelection: transactSalesSelectedFormat,
            escapeMarkup: function (m) { return m; },
            matcher: transactSalesRequestSearch
            //allowClear: true
        });
    };

    /* TransactSales Select Format*/
    var transactSalesSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-3">' + json.NumTransaction + '</div>' +
                '<div class="col-md-6">' + json.Date + '</div>' +
                '<div class="col-md-2">' + json.FuelAmount + '</div>' +
                '</div>';
    };

    /* TransactSales Selection Format*/
    var transactSalesSelectedFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-3">' + json.NumTransaction + '</div>' +
                '<div class="col-md-6">' + json.Date + '</div>' +
                '<div class="col-md-2">' + json.FuelAmount + '</div>' +
                '</div>';
    };

    /* TransactSales Search */
    var transactSalesRequestSearch = function (term, text) {

        var obj = null;
        try { obj = JSON.parse(text.replace(/\t/g, '').replace(/'/g, "\"")); } catch (e) { }
        if (obj === null) { return false; }
        text = obj.NumTransaction + ' | ' +  obj.Date;

        var terms = term.split(" ");
        for (var i = 0; i < terms.length; i++) {
            var tester = new RegExp("\\b" + terms[i], 'i');
            if (tester.test(text) == false) {
                return (text === 'Other');
            }
        }
        return true;
    }

    /*loadGridCardEvents*/
    var loadGridCardEvents = function () {
        $('#gridContainer').off('click.editCard', 'a[data-edit]').on('click.editCard', 'a[data-edit]', function (e) {
            $('#loadForm').find('#id').val($(this).attr('data-id'));
            $('#loadForm').submit();
        });
    };

    /* Reset And Init Custom Select*/
    var resetAndInitCustomControls = function () {
        try {
            $('#CardInfo_UserId').select2('data', {});
            $('#CardInfo_UserId').select2({
                minimumInputLength: 3,
                formatResult: driverSelectFormat,
                formatSelection: driverSelectedFormat,
                escapeMarkup: function (m) { return m; },
                matcher: driverSearch,
                allowClear: true
            });
        } catch (e) {

        }

        try {
            $('#CardInfo_VehicleId').select2('data', {});
            $('#CardInfo_VehicleId').select2({
                minimumInputLength: 3,
                formatResult: vehicleSelectFormat,
                formatSelection: vehicleSelectFormat,
                escapeMarkup: function (m) { return m; },
                matcher: vehicleSearch,
                allowClear: true
            });
        } catch (e) {

        }

    }

    /* Vehicle Select Format*/
    var vehicleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-4">' + json.PlateId + '</div>' +
                //'<div class="col-md-5">' + json.Name + '</div>' +
                '<div class="col-md-8">' + json.Name + '</div>' +
                '</div>';
    };

    /*Vehicle search*/
    var vehicleSearch = function (term, text) {

        var obj = null;
        try { obj = JSON.parse(text.replace(/\t/g, '').replace(/'/g, "\"")); } catch (e) { }
        if (obj === null) { return false; }
        text = obj.PlateId + ' ' + obj.Name + ' ' + obj.CategoryType;

        var terms = term.split(" ");
        for (var i = 0; i < terms.length; i++) {
            var tester = new RegExp("\\b" + terms[i], 'i');
            if (tester.test(text) == false) {
                return (text === 'Other');
            }
        }
        return true;
    }

    /* driver Select Format*/
    var driverSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-2"><img style="width: 48px; height: 48px;" src="' + json.Photo + '"/></div>' +
                '<div class="col-md-4">' + json.Identification + '</div>' +
                //'<div class="col-md-6">' + json.Name + '</div>' +
                '</div>';
    };

    /*driver Selected Format*/
    var driverSelectedFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-2"><img style="width: 24px; height: 24px;" src="' + json.Photo + '"/></div>' +
                '<div class="col-md-4">' + json.Identification + '</div>' +
                '<div class="col-md-6">' + json.Name + '</div>' +
                '</div>';
    };

    /*driver search*/
    var driverSearch = function (term, text) {

        var obj = null;
        try { obj = JSON.parse(text.replace(/\t/g, '').replace(/'/g, "\"")); } catch (e) { }
        if (obj === null) { return false; }
        text = obj.Name + ' ' + obj.Code + ' ' + obj.Identification;

        var terms = term.split(" ");
        for (var i = 0; i < terms.length; i++) {
            var tester = new RegExp("\\b" + terms[i], 'i');
            if (tester.test(text) == false) {
                return (text === 'Other');
            }
        }
        return true;
    }
    
    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initInputCurrencies();
        $('#addOrEditForm').removeData('validator');
        $.validator.unobtrusive.parse($('#addOrEditForm'));
        ECOsystem.Utilities.AddOrEditModalShow();
        resetAndInitCustomControls();
    };

    /*initCardRequest*/
    var initCardRequest = function (opts) {
        $.extend(options, opts);
        loadGridRequestEvents();
        
        $('#detailContainer').off('blur.currencyOnBlur', 'input[data-currency]').on('blur.currencyOnBlur', 'input[data-currency]', function (e) {
            var obj = $(this);
            obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
            obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));
        });

        $('#page-content-wrapper').off('click.btnAddCard', '#btnAddCardRequest').on('click.btnAddCard', '#btnAddCardRequest', function (e) {
            ECOsystem.Utilities.ShowLoader();
            $('#loadAddForm').find('#requestId').val("-1");
            $('#loadAddForm').submit();
        });

        $('#page-content-wrapper').off('click.btnEditCard', 'a[edit-request]').on('click.btnEditCard', 'a[edit-request]', function (e) {
            ECOsystem.Utilities.ShowLoader();
            $('#loadAddForm').find('#requestId').val($(this).attr('data-id'));
            $('#loadAddForm').submit();
        });        
    };

    /*init Dates*/
    var initDates = function () {

        $('#datePickerEstimatedDelivery').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $("#EstimatedDeliveryStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });
    }

    /*initialize Drop Down List*/
    var initDropDownList = function () {
        try {
            var select1 = $("#StateId").select2().data('select2');
            select1.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onStateChange(data); return fn.apply(this, arguments); }
                }
            })(select1.onSelect);

            var select2 = $("#CountyId").select2().data('select2');
            select2.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onCountyChange(data); return fn.apply(this, arguments); }
                }
            })(select2.onSelect);

            var select3 = $("#CityId").select2().data('select2');
            select3.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
                }
            })(select3.onSelect);
            
            if ($('#IssueForId').val() == 101)
            {
                var selectPlate = $("#VehiclePlateId").select2().data('select2');
                selectPlate.onSelect = (function (fn) {
                    return function (data, opts) {
                        var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
                    }
                })(selectPlate.onSelect);
            }else {
                var selectDriver = $("#DriverIdent").select2().data('select2');
                selectDriver.onSelect = (function (fn) {
                    return function (data, opts) {
                        var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
                    }
                })(selectDriver.onSelect);
            }            

        } catch (e) {

        }

    };

    var onStateChange = function (obj) {
        $('#CityId').select2('val', null);
        $('#CountyId').select2('val', null);
        $('#loadCountiesForm').find("input[name=stateId]").val(obj.id);
        $('#loadCountiesForm').submit();
        //$('#CityId').html(""); 
        //$('#CityId').append($("<option></option>").text("Seleccione " + $('#geopoliticalLevel2').val()));
    };

    var onCountyChange = function (obj) {
        $('#CityId').select2('val', null);
        $('#loadCitiesForm').find("input[name=countyId]").val(obj.id);
        $('#loadCitiesForm').submit();
    };

    /*onSuccessLoadPreviousCardRequest*/
    var onSuccessLoadPreviousCardRequest = function (data) {
        ECOsystem.Utilities.HideLoader();
        if (data.result) {
            if (data.cardRequest) {
                $('#StateId').val(data.cardRequest.StateId);
                $('#StateId').select2('val', data.cardRequest.StateId);
                if (data.listCounties != null) {
                    loadCounties(data.listCounties);
                }
                $('#CountyId').val(data.cardRequest.CountyId);
                $('#CountyId').select2('val', data.cardRequest.CountyId);
                if (data.listCities != null) {
                    loadCities(data.listCities);
                }
                $('#CityId').val(data.cardRequest.CityId);
                $('#CityId').select2('val', data.cardRequest.CityId);
                $('#AddressLine1').val(data.cardRequest.AddressLine1);
                $('#AddressLine2').val(data.cardRequest.AddressLine2);
                $('#AuthorizedPerson').val(data.cardRequest.AuthorizedPerson);
                $('#EstimatedDeliveryStr').val(data.cardRequest.EstimatedDeliveryStr);
                $('#ContactPhone').val(data.cardRequest.ContactPhone);
            }
            //loadCounties(data.listCounties)
        }
    }

    /*onSuccessLoadCounties*/
    var onSuccessLoadCounties = function (data) {
        if (data.result) {
            loadCounties(data.listCounties);
        }
    }

    /*onSuccessLoadCities*/
    var onSuccessLoadCities = function (data) {
        if (data.result) {
            loadCities(data.listCities);
        }
    }

    /*loadCounties*/
    var loadCounties = function (options) {
        $('#CountyId').html("");
        $('#CountyId').append($("<option></option>").text("Seleccione " + $('#geopoliticalLevel2').val()));
        $.each(options, function (index, item) {
            $('#CountyId')
                .append($("<option></option>")
                .attr("value", item.id)
                .text(item.text));
        });
    }

    /*loadCities*/
    var loadCities = function (options) {

        $('#CityId').html("");
        $('#CityId').append($("<option></option>").text("Seleccione " + $('#geopoliticalLevel3').val()));
        $.each(options, function (index, item) {
            $('#CityId')
                .append($("<option></option>")
                .attr("value", item.id)
                .text(item.text));
        });
    }

    /*loadGridRequestEvents*/
    var loadGridRequestEvents = function () {
        $('#gridContainer').off('click.generateCard', 'button[data-load]').on('click.generateCard', 'button[data-load]', function (e) {           
            $('#ConfirmationRequestModal').find('#CardRequestId').val($(this).attr('data-id')); 
            $('#ConfirmationRequestModal').find('#CardInformation').html($(this).attr('data-info'));
            $('#ConfirmationRequestModal').modal('show');
        });

        $('#btnRequestCard').click(function () {
            ECOsystem.Utilities.ShowLoader();
            $('#addOrEditForm').find('#id').val($('#ConfirmationRequestModal').find('#CardRequestId').val());
            $('#addOrEditForm').submit();
        });

        $('#gridContainer').off('click', 'button[massive-card-request]').on('click', 'button[massive-card-request]', function (e) {  
            var selectedCheckboxes = $('#gridContainer').find('input[type=checkbox][id!=-1]:checked').length;

            if (selectedCheckboxes > 0) {
                $('#ConfirmationMassiveRequestModal').modal('show');
            } else {
                ECOsystem.Utilities.SetMessageShow("Por favor seleccione al menos una tarjeta.", "ALERT");
            }
        });

        $('#btnMassiveRequestCard').click(function () {
            ECOsystem.Utilities.ShowLoader();
            $('#ConfirmationMassiveRequestModal').modal('hide');
            var arr = [];
            var arrPayment = [];
            $('#gridContainer input[type=checkbox][id!=-1]').each(function (i) {
                if ($(this).is(':checked')) {
                    debugger
                    var data = {};
                    data = $(this).attr('id');
                    paymentType = $(this).attr('paymenttype');
                    arr.push(data);
                    arrPayment.push(paymentType);
                }
            });
            $.ajax({
                url: ECOsystem.Utilities.GetUrl() + '/CreditCard/AddAllCreditCardsFromRequest',
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ model: arr, types: arrPayment }),
                success: function (result) {
                    ECOsystem.Utilities.HideLoader();
                    $("#gridContainer").html(result);
                    loadGridRequestEvents();
                    //window.location.reload();
                }
            });
        });

        $('#gridContainer').find('.generateAllCards').click(function () {
            var id = $(this).attr('id');
            var ischecked = $(this).prop("checked");

            if (id == -1) {
                $('input:checkbox').each(function () {
                    $(this).prop('checked', ischecked);
                });
            } else {
                var allCheckboxes = $('#gridContainer').find('input[type=checkbox][id!=-1]').length;
                var selectedCheckboxes = $('#gridContainer').find('input[type=checkbox][id!=-1]:checked').length;

                if (allCheckboxes == selectedCheckboxes) {
                    $('#-1').prop('checked', true);
                } else {
                    $('#-1').prop('checked', false);
                }
            }
        });
    };

    /*init Currency Inputs*/
    var initInputCurrencies = function () {
        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        try {
            $('#detailContainer').find('input[data-currency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });

        } catch (e) {

        }
    };

    /*onSuccessLoadCardRequest*/
    var onSuccessLoadCardRequest = function () {
        resetAndInitCustomControls();
        initInputCurrencies();
        $('#addOrEditForm').removeData('validator');
        $.validator.unobtrusive.parse($('#addOrEditForm'));
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    var onSuccessAddOrEditLoadCardRequest = function () {
        ECOsystem.Utilities.HideLoader();
        $('#addOrEditCarRequestdForm').removeData('validator');
        $.validator.unobtrusive.parse($('#addOrEditCarRequestdForm'));

        //Modal Solicitud de Tarjeta,  proceso para Usar Información del registro anterior
        $('#addOrEditCardRequestModal').off('click.btnPaste', '#btnPaste').on('click.btnPaste', '#btnPaste', function (e) {
            ECOsystem.Utilities.ShowLoader();
            $('#loadPreviousCardRequestForm').submit();
        });

        initDropDownList(); //Carga de los combo box
        initDates(); //Carga control datepicker
        $('#addOrEditCardRequestModal').modal('show'); //Levantar modal Solicitud tarjeta
    };

    /*onSuccessUpdateCardRequest*/
    var onSuccessUpdateCardRequest = function () {
        ECOsystem.Utilities.HideLoader();

        $('#addOrEditCardRequestModal').modal('hide');
        ECOsystem.Utilities.AddOrEditModalClose();

        $('#gridContainer').find('.generateAllCards').click(function () {
            var id = $(this).attr('id');
            var ischecked = $(this).prop("checked");

            if (id == -1) {
                $('input:checkbox').each(function () {
                    $(this).prop('checked', ischecked);
                });
            } else {
                var allCheckboxes = $('#gridContainer').find('input[type=checkbox][id!=-1]').length;
                var selectedCheckboxes = $('#gridContainer').find('input[type=checkbox][id!=-1]:checked').length;

                if (allCheckboxes == selectedCheckboxes) {
                    $('#-1').prop('checked', true);
                } else {
                    $('#-1').prop('checked', false);
                }
            }
        });
    };

    /*onValidateStatusCreditCard*/
    var onValidateStatusCreditCard= function () {
        $('input[type="Checkbox"]').each(function () {
            if ($(this).attr('data-status') != 0)
            {
                $(this).attr("disabled", "disabled");
            }
        });
    };
    
    var addOrEditRequestBegin = function () {
        $('#ConfirmationRequestModal').modal('hide');
        $('#ConfirmationMassiveRequestModal').modal('hide');
        $('body').loader('show');
    };

    var addOrEditRequestEnd = function () {
        loadGridRequestEvents();
        ECOsystem.Utilities.HideLoader();

        //Download Document
        validatesIVEDownloadFile();
    };

    var validatesIVEDownloadFile = function () {
        $.ajax({
            url: ECOsystem.Utilities.GetUrl() + '/CreditCard/ValidatesIVEDownload',
            type: "POST",
            contentType: "application/json",            
            success: function (result) {
                if (result) {
                    $('#DonwloadIVEFileForm').submit();
                    $('#downloadIVEFileModal').modal('show');

                    $('#btnDownloadIVEComplete').click(function () {
                        $('body').loader('show');
                        $('#downloadIVEFileModal').modal('hide');
                        window.location.reload();
                    });
                }
            }
        });
    }

    var preventCardRequestDoubleClick = function (obj) {
        if ($('#addOrEditCarRequestdForm').valid()) {
            var Type = $("#IssueForId").val();    

            if (Type == 101) {
                var ValidVehicle = $("#VehiclePlateId").val();

                if (ValidVehicle != "") {
                    setErrorMsj('', false);
                    $(obj).attr('disabled', 'disabled');
                    $('#addOrEditCarRequestdForm').submit();
                }
                else {
                    setErrorMsj('* Seleccione el vehículo al cual desea generarle la Tarjeta', true);
                }
            }
            else {
                var ValidDriver = $("#DriverIdent").val();

                if (ValidDriver != "") {
                    setErrorMsj('', false);
                    $(obj).attr('disabled', 'disabled');
                    $('#addOrEditCarRequestdForm').submit();
                }
                else {
                    setErrorMsj('* Seleccione el conductor al cual desea generarle la Tarjeta', true);
                }

            }
            
        }
    };

    /* Show or hide errors messages*/
    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj').html(msj);
            $('#validationErrors').removeClass('hide');
        } else {
            $('#errorMsj').html('');
            $('#validationErrors').addClass('hide');
        }
    }

    var removeCardRequest = function (id) {
        $('#cardRequestId').val(id);
        $('#deleteCardRequestModal').modal('show');
    }

    var confirmRemoveCardRequest = function () {
        ECOsystem.Utilities.ShowLoader();
        $('#deleteCardRequestModal').modal('hide');
        $('#removeCardRequestForm').submit();
    }

    /* Public methods */
    return {
        Init: initialize,
        InitVPOS: initializeVpos,
        InitCardRequest: initCardRequest,
        LoadSelectTransacVPOS: loadVPOSSelectTransactSales,
        ShowModalVPOS_VOID: showModalVPOS_VOID,
        OnSuccessLoadCardRequest: onSuccessLoadCardRequest,
        OnSuccessAddOrEditLoadCardRequest: onSuccessAddOrEditLoadCardRequest,
        OnSuccessUpdateCardRequest: onSuccessUpdateCardRequest,
        OnSuccessLoadPreviousCardRequest: onSuccessLoadPreviousCardRequest,
        OnSuccessLoadCounties: onSuccessLoadCounties,
        OnSuccessLoadCities: onSuccessLoadCities,
        OnSuccessLoad: onSuccessLoad,
        AddOrEditRequestBegin: addOrEditRequestBegin,
        AddOrEditRequestEnd: addOrEditRequestEnd,
        PreventCardRequestDoubleClick: preventCardRequestDoubleClick,
        RemoveCardRequest: removeCardRequest,
        ConfirmRemoveCardRequest: confirmRemoveCardRequest
    };
})();

