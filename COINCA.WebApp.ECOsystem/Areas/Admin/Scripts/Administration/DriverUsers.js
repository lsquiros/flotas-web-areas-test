﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.DriverUsers = (function () {
    var options = {};


    /* initialize function */

    var initialize = function (opts) {
        $.extend(options, opts);

        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        $('#driverLicenseExpirationId').datepicker({ language: 'es', minDate: new Date(), format: 'dd/mm/yyyy', autoclose: true, todayHighlight: true, startDate: today });
        $("#DriverUser_LicenseExpirationStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $("#DailyTransactionLimit").numeric("integer");

        $('#AddAlarmId').off('click.AddAlarmId', '#AddAlarmId').on('click.AddAlarmId', '#AddAlarmId', function (e) {
            degugger;
            $('#alarmForm').find('#id').val($(this).attr('userid'));
            $('#alarmForm').submit();
        });

        $('#CalendarAlarmId').off('click.CalendarAlarmId', '#CalendarAlarmId').on('click.CalendarAlarmId', '#CalendarAlarmId', function (e) {

            $("#driverLicenseExpirationId").datepicker("show");
            setTimeout(function () {
                $('#driverLicenseExpirationId').datepicker("hide");
                $('#driverLicenseExpirationId').blur();
            }, 2000)
        });

        //Init dropDown List Card Request
        $("#DriverUser_CardRequestId").select2('data', {});
        $("#DriverUser_CardRequestId").select2({
            minimumInputLength: 3,
            formatResult: cardRequestSelectFormat,
            formatSelection: cardRequestSelectedFormat,
            escapeMarkup: function (m) { return m; },
            matcher: cardRequestSearch,
            allowClear: true
        });

        $('#CostCenterId').select2('data', {});
        $('#CostCenterId').select2({
            //formatResult: CostCenterSelectFormat,
            //formatSelection: CostCenterSelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        //Init DropDown List Available Dallas Keys
        $('#DallasId').select2('data', {});
        $('#DallasId').select2({
            //minimumInputLength: 3,
            formatResult: dallasKeysSelectFormat,
            formatSelection: dallasKeysSelectFormat,
            escapeMarkup: function (m) { return m; },
            matcher: dallasKeysSearch,
            allowClear: false
        });

        $("#PeriodicityTypeId").select2('data', {});
        $("#PeriodicityTypeId").select2({
            formatResult: PeriodicitySelectFormat,
            formatSelection: PeriodicitySelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });


        //trigger click on hidden file input
        $('#btnUpload').off('click.btnUpload').on('click.btnUpload', function (e) {
            $('#fileHidden').trigger('click');
            e.stopPropagation();
        });

        fileApiUpload();
        formValidation();
        onClickNewDriver();
        onClickNewAgent();
        initCheckUsername();
        downloadFile();
        initializeDropDownList();
        result();

    }

    //Results from Import 
    var result = function () {

        var resultdata = $('#ResultData').val();
        if (resultdata != '' && resultdata != undefined && resultdata != null) showresultmodal(resultdata);
    }


    var showresultmodal = function (resultdata) {
        $('#resultimportdate').html(resultdata.split('|')[0]);
        $('#resultadded').html(resultdata.split('|')[1]);
        $('#resultupdated').html(resultdata.split('|')[2]);
        $('#resultactivated').html(resultdata.split('|')[3]);
        $('#resultdeactivated').html(resultdata.split('|')[4]);
        $('#resultnoprocess').html(resultdata.split('|')[5]);
        $('#resulttotal').html(resultdata.split('|')[6]);
        $('#SummaryResultModal').modal('show');
    }


    var CostCenterSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-12">' + json.Name + '</div>' +
            '</div>';
    };

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        if ($("#Parameters_Status").attr("id") != undefined) {
            var select1 = $("#Parameters_Status").select2().data('select2');
            select1.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectDriverStatusChange(data); return fn.apply(this, arguments); }
                }
            })(select1.onSelect);
        }
    };

    /*on Select Report Criteria Change*/
    var onSelectDriverStatusChange = function (obj) {
        $('#searchForm').find("#statusId").val(obj.id);
        showLoader();
        $('#searchForm').submit();
    };

    var searchByStatus = function (statusType) {
        showLoader();
        setStatusName(statusType);
        $('#searchForm').submit();
    }

    var setStatusName = function (statusType) {

        switch (statusType) {
            case 1:
                $('#btnStatusName').html('Activos <span class="caret"></span>');
                break;
            case 0:
                $('#btnStatusName').html('Inactivos <span class="caret"></span>');
                break;
            default:
                $('#btnStatusName').html('Todos <span class="caret"></span>');
        }

        $('#statusId').val(statusType);
    }

    /*initCheckUsername*/
    var initCheckUsername = function () {
        $('#detailContainer').off('change.email', '#DecryptedEmail').on('change.email', '#DecryptedEmail', function (e) {
            checkValidUserName();
        });

        $("#DecryptedEmail").keyup(function () {
            checkValidUserName();
        });
    }

    var checkValidUserName = function () {
        if (validateDetailEmail()) {
            $('#checkForm').find('#username').val($('#DecryptedEmail').val());
            $('#checkForm').submit();
        } else {
            $('.modal-footer button[type="submit"]').attr('disabled', 'disabled');
        } 2
    };

    /*on Success Check User*/
    var onSuccessCheckUser = function (data) {
        if (!data.result) {
            $('.modal-footer button[type="submit"]').attr('disabled', 'disabled');
            setErrorMsj(data.message, true);
        } else {
            $('.modal-footer button[type="submit"]').removeAttr('disabled');
            setErrorMsj('', false);
        }
    }

    /* Standard Error Function for File API */

    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };

    /* Select Format*/
    var PeriodicitySelectFormat = function (item) {

        if (!item.id || item.id == "") return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-6">' + json.Name + '</div>' +
            '</div>';
    };

    /* Update progress for File API */

    var fileApiUpdateProgress = function (e) {
        if (e.lengthComputable) {
            var percentLoaded = Math.round((e.loaded / e.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                $('.percent').css('width', percentLoaded + '%').html(percentLoaded + '%');
            }
        }
    };


    /* File Upload implementation */

    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            $('#fileHidden').off('change.fileHidden').on('change.fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onprogress = fileApiUpdateProgress;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {
                    $('#imgPhoto').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        alert("La extensión del archivo es inválida");
                        continue;
                    }
                    // Closure to capture the file information.
                    reader.onload = (function (F) {
                        $('.percent').html('100%').css('width', '100%');
                        setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgPhoto').removeClass('imgloading');", 2000);
                        return function (e) {
                            var lsrc = e.target.result;
                            $('#imgPhoto').attr({
                                src: lsrc,
                                title: escape(F.name)
                            });
                            $('#User_Photo').val(lsrc);
                        };
                    })(f);
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
            });
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.');
        }
    };

    var ValidateFormat = function (input) {

        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var files = input.files;
            for (var i = 0, f; f = files[i]; i++) {
                // Only process csv files.
                if (!f.name.match('\.csv') || (f.size > 46030)) {
                    return false;
                }
                else {
                    return true;
                }
            }
            //});
        } else {

            alert('La opción de subir imágenes no es soportada por este navegador.', true);
        }
    };

    var getFilename = function (input) {

        if (ValidateFormat(input)) {
            if (input.files && input.files[0]) {
                var value = $("#ImportCSV").val();
                var NameOfFile = input.files[0].name;
                $("#txtFile").val(NameOfFile);
                setErrorFormatMsj('', false);
            }
        }
        else {
            $("#txtFile").val("");
            setErrorFormatMsj('* El formato de archivo es inválido o el tamaño del archivo sobrepasa lo permitido, favor seleccionar un archivo de formato CSV con un tamaño menor a 45KB (Equivalente a 500 líneas). Se sugiere dividir el archivo para poder subirlo por partes.', true);
        }

    }

    /* Show or hide errors messages*/
    var setErrorFormatMsj = function (msj, show) {
        if (show) {
            $('#errorFormatMsj').html(msj);
            $('#validationFormatErrors').removeClass('hide');
        } else {
            $('#errorFormatMsj').html('');
            $('#validationFormatErrors').addClass('hide');
        }

    }

    /* Form validation to prevent an invalid submission*/

    var formValidation = function () {

        //Validate Card Type Request
        var cardTypeRequest = $("#CardTypeRequest").val();
        if (cardTypeRequest != "100") { // Solicitud es diferente por Conductor
            $("#CardRequestControl").remove(); // Remuevo el control para no validarlo
        }

        $('#detailContainer').off('click.btnSaveSubmit', 'button[type=submit]').on('click.btnSaveSubmit', 'button[type=submit]', function (e) {
            setErrorMsj('', false);
            if (validateEmail()) {
                return true;
            } else {
                return false;
            }
        });
    };

    /* Email validation to prevent an invalid submission*/
    var validateDetailEmail = function () {
        setErrorMsj('', false);
        if ($('#DecryptedEmail').val().length == 0) {

            return false;
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#DecryptedEmail').val().match(email)) {

            return true;
        } else {
            setErrorMsj('Correo electrónico invalido', true);

            return false;
        }
    };

    /* Email validation to prevent an invalid submission*/
    var validateEmail = function () {
        if ($('#Email').val().length == 0) {
            return true;
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#Email').val().match(email)) {
            return true;
        } else {
            setErrorMsj('Correo electrónico invalido', true);
            return false;
        }
    };

    /* Show or hide errors messages*/
    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj').html(msj);
            $('#validationErrors').removeClass('hide');
        } else {
            $('#errorMsj').html('');
            $('#validationErrors').addClass('hide');
        }

    }

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    /* On click BtnNewDriver*/
    var onClickNewDriver = function () {
        $('#btnAddDriver').off('click.btnAddDriver').on('click.btnAddDriver', function (e) {

            customerCreditCardValidate()

            //if (validate) {
            //    $('#loadForm').find('#id').val('-1');
            //    $('#loadForm').find('#isagent').val('false');
            //    $('#loadForm').submit();
            //}
        });
    };

    /* On click BtnNewAgent*/
    var onClickNewAgent = function () {
        $('#btnAddAgent').off('click.btnAddAgent').on('click.btnAddAgent', function (e) {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').find('#isagent').val('true');
            $('#loadForm').submit();
        });
    };


    /* On click Import*/
    var onClickImport = function () {

        //$('#btnImportDriver').off('click.btnImportDriver').on('click.btnImportDriver', function (e) {
        $('#ImportModal').modal('show');
    };//);
    // };

    //trigger click on hidden file input
    var onClickImport2 = function () {
        $('#ImportCSV').trigger('click');
        $("#txtFile").val("");
        //$('#ImportForm').submit;
        //e.stopPropagation();        

    };


    //trigger click on hidden file input
    var onClickImport3 = function () {

        $('body').loader('show');
        $('#ImportForm').submit();

    };


    var downloadFile = function () {

        $('#DonwloadFileForm').submit();
    };



    /* CardRequest Select Format*/
    var cardRequestSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-1">' + json.NumSolicitud + '</div>' +
            '<div class="col-md-7">' + json.DriverName + '</div>' +
            '<div class="col-md-2">' + json.StateName + '</div>' +
            '<div class="col-md-2">' + json.CityName + '</div>' +
            '</div>';
    };

    /* CardRequest Selected Format*/
    var cardRequestSelectedFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-1">' + json.NumSolicitud + '</div>' +
            '<div class="col-md-7">' + json.DriverName + '</div>' +
            '<div class="col-md-2">' + json.StateName + '</div>' +
            '<div class="col-md-2">' + json.CityName + '</div>' +
            '</div>';
    };

    /*CardRequest search*/
    var cardRequestSearch = function (term, text) {

        var obj = null;
        try { obj = JSON.parse(text.replace(/\t/g, '').replace(/'/g, "\"")); } catch (e) { }
        if (obj === null) { return false; }
        text = obj.DriverName + ' ' + obj.NumSolicitud;

        var terms = term.split(" ");
        for (var i = 0; i < terms.length; i++) {
            var tester = new RegExp("\\b" + terms[i], 'i');
            if (tester.test(text) == false) {
                return (text === 'Other');
            }
        }
        return true;
    }

    /* DallasKeys Select Format*/
    var dallasKeysSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));

        if (json.Code == "Ninguna") {
            return json.Code;

            //'<div class="row">' +
            //            '<div class="col-md-2">'
            //                    + json.Code + '</div>' +
            //        '</div>';
        }

        return json.Code;

        //'<div class="row">' +
        //            '<div class="col-md-2"> '
        //                    + json.Code + '</div>' +
        //        '</div>';
    };

    /*DallasKeys search*/
    var dallasKeysSearch = function (term, text) {
        2
        var obj = null;
        try { obj = JSON.parse(text.replace(/\t/g, '').replace(/'/g, "\"")); } catch (e) { }
        if (obj === null) { return false; }
        text = obj.Code;

        var terms = term.split(" ");
        for (var i = 0; i < terms.length; i++) {
            var tester = new RegExp("\\b" + terms[i], 'i');
            if (tester.test(text) == false) {
                return (text === 'Other');
            }
        }
        return true;
    }

    var onClickDownloadDrivers = function () {

        $('#ExcelReportDownloadForm').submit();
    };


    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadExcelForm').submit();
    };

    function customerCreditCardValidate(valor) {
        $.ajax({
            url: '/Administration/Drivers/CustomerCreditCardValidate',
            type: 'POST',
            data: '',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.MessegeType !== "success") {
                    swal("Info de Conductores", data.Messege, data.MessegeType);
                }
                else if (valor) {
                    $("#ImportModal").modal('show'); 
                }
                else {
                    $('#loadForm').find('#id').val('-1');
                    $('#loadForm').find('#isagent').val('false');
                    $('#loadForm').submit();
                }
            }
        });
    }

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        OnSuccessCheckUser: onSuccessCheckUser,
        OnClickImport: onClickImport,
        OnClickImport2: onClickImport2,
        OnClickImport3: onClickImport3,
        GetFilename: getFilename,
        DownloadFile: downloadFile,
        OnClickDownloadDrivers: onClickDownloadDrivers,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        SearchByStatus: searchByStatus,
        SetStatusName: setStatusName,
        CustomerCreditCardValidate: customerCreditCardValidate
    };

})();
