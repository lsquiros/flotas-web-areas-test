﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.VehiclesByDriver = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        var select2 = $("#UserId").select2({
            formatResult: usersSelectFormat,
            formatSelection: usersSelectFormat,
            escapeMarkup: function (m) { return m; }
        }).data('select2');

        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target;
                if (opts != null) {
                    target = $(opts.target);
                }
                if (target) {
                    onSelectChange(data);
                    return fn.apply(this, arguments);
                }
            }
        })(select2.onSelect);

        $('.connected').sortable({
            connectWith: '.connected'
        });

        $('.connected').on('sortdeactivate', function (event, ui) {
            to = $(this).children().index(ui.item);
        });

        $('#btnSubmit').off('click.btnSubmit').on('click.btnSubmit', function (e) {
            $('#ConnectedVehicles').find('input:hidden').each(function (i) {
                $(this).attr('name', 'ConnectedVehiclesList[' + i + '].VehicleId');
            });
            return true;
        });

        $("#saveForm").data("validator").settings.submitHandler = function (e) {
            $('#DragAndDropContainer').find('#processing').removeClass('hide');
            $('#DragAndDropContainer').find('button').attr("disabled", "disabled");
            $('#DragAndDropContainer').find('a[atype="button"]').attr("disabled", "disabled");
        };
    };

    var usersSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-6">' + json.Name + '</div>' +
            '</div>';
    };

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
        $('body').loader('hide');
    };

    var onSelectChange = function (obj) {
        $('#id').val(obj.id);
        $("input:hidden[id='UserId']").val(obj.id);

        if (obj.id === '') {
            $('#DragAndDropContainer').html('');
        } else {
            $('body').loader('show');
            $('#loadForm').submit();
        }
    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad
    };
})();

