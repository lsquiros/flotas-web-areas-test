﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.SettingOdometers = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        initializeDropDownList();

        validateInputValues();

        onClickbtnSearch();

        setChecksValidates();

        try {
            $("#applyAllForm").data("validator").settings.submitHandler = function (e) {
                $('#ControlsContainer').find('#processing').removeClass('hide');
                $('#ControlsContainer').find('button[data-btnmodal=true]').attr("disabled", "disabled");
            };
        } catch (e) {}        
    };

    var InitGrids = function () {
        $('#gridContainer').off('blur.inputOnBlur', '.calculateactual').on('blur.inputOnBlur', '.calculateactual', function () {
            var transactionid = $(this).attr("TransactionId");
            var odometer = $("#txtodometer_" + transactionid).val();

            changeData(transactionid, odometer, null, null);
        });

        $('#gridContainer').off('blur.inputOnBlur', '.getOdometerLast').on('blur.inputOnBlur', '.getOdometerLast', function () {
            var transactionid = $(this).attr("TransactionId");
            var odometer = $("#txtodometerlast_" + transactionid).val();

            changeData(transactionid, odometer, true, null);
        });

        $('#gridContainer').off('blur.inputOnBlur', '.getOdometerBeforeLast').on('blur.inputOnBlur', '.getOdometerBeforeLast', function () {
            var transactionid = $(this).attr("TransactionId");
            var odometer = $("#txtodometerBeforelast_" + transactionid).val();

            changeData(transactionid, odometer, null, true);
        });
    }
    
    var changeData = function (transactionid, odometer, isLast, isBeforeLast) {
        if (odometer != null || odometer != undefined || odometer != ''){
            odometer = odometer.replace(',', '')
        }

        $.ajax({
            url: '/SettingOdometers/GetChanges',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ TransactionId: transactionid, Odometer: odometer, IsLast: isLast, IsBeforeLast: isBeforeLast }),
            success: function (result) {
                if (result != undefined || result != null || result != '') {
                    if (result.Success == "Success") {
                        refreshData(result);//$('#gridContainer').html(result);
                    }
                }
            }
        });
    }

    var refreshData = function (result) {
        if (result.Selected) {
            $("#txtodometer_" + result.TransactionId).val(result.Odometer);
            $("#txtodometerlast_" + result.TransactionId).val(result.OdometerLast);
            $("#txtodometerBeforelast_" + result.TransactionId).val(result.OdometerBeforeLast);
            $("#lblPerformance_" + result.TransactionId).val(result.Performance);
            $("#lblPerformance_" + result.TransactionId).html(result.Performance);
            $("#lblTravel_" + result.TransactionId).val(result.Travel);
            $("#lblTravel_" + result.TransactionId).html(result.Travel);
            var row = $("#txtodometer_" + result.TransactionId).closest(".grid-row").addClass("selectRow selectRowOdometer");
        }
    }
        
    $('#datetimepickerStartDate').datepicker({
        language: 'es',
        minDate: new Date(),
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (e) {
        if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects()){
            $('#Parameters_ReportStatus').val(100);
            $('#ReloadForm').submit();
        }            
        else
            invalidSelectOption();
    });
    $("#Parameters_StartDateStr").alphanum({
        allowNumeric: true,
        allowUpper: false,
        allowLower: false,
        allowCaseless: true,
        allowSpace: false,
        allow: '/',
        maxLength: 10
    });

    $('#datetimepickerEndDate').datepicker({
        language: 'es',
        minDate: new Date(),
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (e) {
        if ($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects()){
            $('#Parameters_ReportStatus').val(100);
            $('#ReloadForm').submit();
        }            
        else
            invalidSelectOption();
    });

    var setAmountErrorMsj = function (msj, show, typeError) {
        typeError = typeError != null ? typeError : 'e1';
        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').removeClass('hide');
                    $('#errorAmountMsj3').html(msj);
                    $('#validationAmountError3').removeClass('hide');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').addClass('hide');
                    $('#errorAmountMsj3').html('');
                    $('#validationAmountError3').addClass('hide');
                }
                break;
            default:
                break;
        }
    }

    var toNum = function (num) {
        if (num.toString().indexOf(".") > -1 && num.toString().indexOf(",") > -1) {
            num = ("" + num).replace(/\./g, '').replace(/,/g, '.');
        }
        //SQuiros: No valida correctamente la función isNaN(num) || 
        return num === '' || num === null ? 0.00 : parseFloat(num);
    }

    var formatNum = function (num) {
        return num.toFixed(2).replace(/\./g, ',').replace(/./g, function (c, i, a) {
            return i && c !== ',' && !((a.length - i) % 3) ? '.' + c : c;
        });
    }

    var round2Fixed = function (num, decimals) {
        var sign = num >= 0 ? 1 : -1;
        return (Math.round((num * Math.pow(10, decimals)) + (sign * 0.001)) / Math.pow(10, decimals)).toFixed(decimals);
    };

    var initializeDropDownList = function () {

        var select1 = $("#Parameters_ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_Month").select2({ allowClear: true }).data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Year").select2({ allowClear: true }).data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        try {           
            var select4 = $("#Parameters_OdometerType").select2().data('select2');
            select4.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectOdometerTypeChange(data); return fn.apply(this, arguments); }
                }
            })(select4.onSelect);
        } catch (e) {

        }
        try {
            var select5 = $("#Parameters_CustomerId").select2().data('select2');
            select5.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectCustomerIdChange(data); return fn.apply(this, arguments); }
                }
            })(select5.onSelect);
        } catch (e) {

        }

    };

    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Parameters_Month").select2('val', '');
            $("#Parameters_Year").select2('val', '');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');

            $('#Parameters_ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            if (checkAllSelects()) {
                $('#Parameters_ReportStatus').val(100);
                $('#ReloadForm').submit();
            }
        }
    };

    var onSelectMonthChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Month').val(obj.id);
            if (checkAllSelects()) {
                $('#Parameters_ReportStatus').val(100);
                $('#ReloadForm').submit();
            }
        }
    };

    var onSelectYearChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Year').val(obj.id);
            if (checkAllSelects()) {
                $('#Parameters_ReportStatus').val(100);
                $('#ReloadForm').submit();
            }
        }
    };

    var onSelectOdometerTypeChange = function (obj) {        
        $('#Parameters_OdometerType').val(obj.id);
        $('#Parameters_ReportStatus').val(100);
        $('#ReloadForm').submit();                
    };

    var checkAllSelects = function () {
        if (($('#Parameters_Year').val() === '') || ($('#Parameters_Month').val() === '')) {            
            return false;
        }
        return true;
    };

    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<div class="text-danger top30">Por favor seleccionar las opciones correspondientes para cargar la información.</div>');
        $('#gridContainer').html('');
    };

    var validateInputValues = function () {
        $('#gridContainer').off('click.chkCheckAll', '#chkCheckAll').on('click.chkCheckAll', '#chkCheckAll', function () {
            var check = $(this);
            $('#gridContainer').find('input[type=checkbox]').each(function () {                
                if (check.is(':checked')) {
                    $(this).prop('checked', true);
                    $(this).closest('tr').addClass('selectRow');
                } else {
                    $(this).prop('checked', false);
                    $(this).closest('tr').removeClass('selectRow');
                }
            });
        });

        $('#gridContainer').off('click.showalert', '.showalert').on('click.showalert', '.showalert', function () {
            $(this).closest('tr').addClass('selectRow');
        });
    };

    var saveShowAlarmsItems = function () {
        var transactionIdsNoAlert = new Array();
        var transactionIdsShowAlert = new Array();
        $('#gridContainer').find('input[type=checkbox]').each(function () {
            var check = $(this);
            if (check.attr("id") != 'chkCheckAll') {
                if (check.is(':checked')) {
                    transactionIdsNoAlert.push($(this).attr("id"));
                } else {
                    transactionIdsShowAlert.push($(this).attr("id"));
                }
            }
        });
        if (transactionIdsNoAlert.length != $('#checkCounts').val())
        {
            var postData = { TransactionIdsNoAlert: transactionIdsNoAlert, TransactionIdsShowAlert: transactionIdsShowAlert };
            $.ajax({
                url: '/SettingOdometers/NoShowAlerts',
                type: 'POST',
                data: postData,
                dataType: 'json',
                traditional: true,
                success: function (data) {}
            });
        }
    };

    var validateAmounts = function () {       
        var isValidValues = true;
        var countOdomerters = 0;
        var countCheck = 0;
        var message = '<ul style="list-style-type:disc">';
        $('#gridContainer').find('input[calculateactual]').each(function (i) {
            var value = parseInt($(this).attr('value'));
            if (value <= 0) {
                isValidValues = false;
            }
        });
        $('#gridContainer tr.selectRow').each(function (i) {            
            var check = $(this).find('input[type=checkbox]');
            if (check.is(':checked')) {
                if (check.attr("id") != 'chkCheckAll') {
                    if ($(this).hasClass('selectRowOdometer')) {
                        countCheck++;
                        countOdomerters++;
                    } else {
                        countCheck++;
                    }
                }
            }
            else if ($(this).hasClass('selectRowOdometer')) {
                countOdomerters++;
            }
        });
        if (countOdomerters == 0 && countCheck == 0) {
            $('#gridContainer tr.selectRowOdometer').each(function (i) {
                 countOdomerters++;               
            });
        }
        if (countOdomerters == 0 && countCheck == 0) {
            var msj = '* No hay ningún valor editado para actualizar';
            setAmountErrorMsj(msj, true, 'e2');
        }
        else if (!isValidValues && countCheck == 0) {
            var msj = '* Ningún monto puede ser igual que 0.00';
            setAmountErrorMsj(msj, true, 'e2');
        } else {
            setAmountErrorMsj('', false, 'e2');
            if (countOdomerters > 0) message = message + '<li>El valor de ' + countOdomerters + ' odómetros será actualizado.</li>'
            if (countCheck > 0) message = message + '<li>' + countCheck + ' odómetros no serán contabilizados dentro de las alertas. </li>';
            message = message + '</ul>';
            $('#confirmModalactualizarText').html('¿Esta seguro que desea realizar esta operación? <br />' + message);
            $('#confirmModalactualizar').modal('show');
        }
    };

    var onClickbtnAppliesMassive = function () {       
        $('#confirmModalactualizar').modal('hide');
        saveShowAlarmsItems();
        showLoader();
        $.ajax({
            url: '/SettingOdometers/AddOrEditOdometers',
            type: "POST",
            contentType: "application/json",            
            success: function (result) {
                $('body').loader('hide');
                $('#gridContainer').html(result);
            }
        });
    };

    var onClickbtnSearch = function () {
        
        var _value = 0;
        $('#btnSearch').off('click.btnSearch').on('click.btnSearch', function (e) {
            $('#gridContainer tr').each(function (i) {
                if ($(this).css('background-color') === 'rgb(255, 255, 115)') {
                    _value++    
                }
            });
            
            if (_value > 0) $("#confirmModalSearch").modal('show');
            else $("#ReloadForm").submit();            
        });
    };

    var initCurrency = function () {
        try {
            
            $('#wrapper').off('blur.currencyOnBlur', 'input[data-currency]').on('blur.currencyOnBlur', 'input[data-currency]', function (e) {
                var obj = $(this);

                obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
                obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));
            });
            $('#wrapper').find('input[data-currency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });
        } catch (e) {}
    }

    var setAppliesMassiveValidation = function () {
        
        $("#errormsj").html('');
        var value = $('#Data_ValueStr').val();
        if (value.length == 0 || ECOsystem.Utilities.ToNum(value) == 0) {
            $("#errormsj").html('Monto requerido y debe ser mayor que 0.00');
        }
        else
            $("#confirmModal").modal('show');
    };

    var onSuccessLoad = function () {        
        $('body').loader('hide');
        setChecksValidates();       
        $('#alarmContainer').load('/Adminsitracion/ASPMenus/Alarms');
    };

    var onClickDownloadOdometers = function () {     
        $('#ExcelReportDownloadForm').submit();
    };

    var onClickDownloadChangeOdometers = function () {
       $('#ExcelReportDownloadFormOdometer').submit();
    };

     

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadFileForm').submit();
    };

    var sortColumn = function (obj) {        
        $.get('/SettingOdometers/SortGrid?Column=' + obj, function (data) {
            $('#gridContainer').html(data);
            if ($('#checkAllVal').val() == 'True') {
                $('#chkCheckAll').prop('checked', true);
            }
        });
    };

    var setChecksValidates = function () {
        var count = 0;
        var countUnchecked = 0;
        $('#gridContainer').find('input[type=checkbox]').each(function () {
            var check = $(this);            
            if (check.attr("id") != 'chkCheckAll') {
                if (check.is(':checked')) {
                    $(this).closest('tr').addClass('selectRow');
                    count++
                } else {
                    countUnchecked++;
                }
            }
        });
        $('#checkCounts').val(count);
        if (countUnchecked == 0) {
            $('#chkCheckAll').prop('checked', true);
        }
    };

    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,        
        SetAppliesMassiveValidation: setAppliesMassiveValidation,
        OnClickDownloadOdometers: onClickDownloadOdometers,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        SortColumn: sortColumn,
        OnClickbtnAppliesMassive: onClickbtnAppliesMassive,
        ValidateAmounts: validateAmounts,
        InitGrids: InitGrids,
        OnClickDownloadChangeOdometers: onClickDownloadChangeOdometers
    };
})();
