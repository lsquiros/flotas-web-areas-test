﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.TransactionRules = (function () {
    var options = {};
    var jsonDataArray = [];

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initToggle();
        //ruleActiveChange();
        getParametersData();
        submitChanges();
        toggleChange();

        $('#modalverification').on('hidden.bs.modal', function () {
            $('#txRangeStartOdometer').focus().select();
        })

        $("#restoreRules").click(function () {
            $('#restoreRulesModal').modal('show');
        });

        $("#sendRestoreRules").click(function () {

            var vehicleid = $("#VehicleId").val();
            var costcenterid = $("#CostCenterId").val();
            var isvehicle = $("#IsVehicle").val();
            $('#RuleChange').val(false);
            $('#DeleteForm').find('#DeleteVehicleId').val(vehicleid);
            $('#DeleteForm').find('#DeleteCostCenterId').val(costcenterid);
            $('#DeleteForm').find('#IsVehicle').val(isvehicle);
            $('#DeleteForm').submit();
        });

        //Validates if the value in the margin textbox is a number
        $("#margintxt").keypress(function (e) {
            var val = $("#margintxt").val();
            val = val + String.fromCharCode(e.charCode);
            val = val.replace(/\,/g, '.');

            if (!$.isNumeric(val)) {
                return false;
            }
        });

        //Gets the data from the modal and save the parameter in the database
        $('#TRSaveMarginParameter').click(function () {
            saveMaginParameterData();
        });

        //Verifies if the rule is checked and if that is checked show the times
        $('input[type=checkbox]').each(function () {
            var check = $(this);
            var ruleId = $(this).attr("ruleId");

            if (ruleId == 13) {
                if (check.prop('checked')) {
                    $('#TimeSlotValue').removeClass('hideText');
                }
                else {
                    $('#TimeSlotValue').addClass('hideText');
                }
            }

            if (ruleId == 12) {
                if (check.prop('checked')) {
                    $('#MarginDivText').removeClass('hideText');
                }
                else {
                    $('#MarginDivText').addClass('hideText');
                }
            }

            if (ruleId == 10) {
                if (check.prop('checked')) {
                    $('#OdoRangeDivText').removeClass('hideText');
                }
                else {
                    $('#OdoRangeDivText').addClass('hideText');
                }
            }

            if (ruleId == 11) {
                if (check.prop('checked')) {
                    $('#DailyTransactionLimitTexts').removeClass('hideText');
                }
                else {
                    $('#DailyTransactionLimitTexts').addClass('hideText');
                }
            }

            if (ruleId == 17) {
                if (check.prop('checked')) {
                    $('#VehiclePerformanceTexts').removeClass('hideText');
                }
                else {
                    $('#VehiclePerformanceTexts').addClass('hideText');
                }
            }
        });

        //Validate the texboxes to receive only numbers and that the max number allowed
        $('.number-only').keypress(function (e) {
            //Get the number in the text
            var hours = $("#TimeSlotValueHours").val();
            var minutes = $("#TimeSlotValueMinutes").val();
            var seconds = $("#TimeSlotValueSeconds").val();

            if (hours.length == 2) hours = '';
            if (minutes.length == 2) minutes = '';
            if (seconds.length == 2) seconds = '';

            if ($(this).attr('id') == 'TimeSlotValueHours') hours = hours + String.fromCharCode(e.charCode);
            if ($(this).attr('id') == 'TimeSlotValueMinutes') minutes = minutes + String.fromCharCode(e.charCode);
            if ($(this).attr('id') == 'TimeSlotValueSeconds') seconds = seconds + String.fromCharCode(e.charCode);

            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) {
                return false;
            }
            else {
                if (hours > 23) {
                    return false;
                }
                else if (minutes >= 60) {
                    return false;
                }
                else if (seconds >= 60) {
                    return false;
                }
            }
        });

        //Validate the texboxes to receive only numbers and that the max number allowed
        $('.no-decimal').keypress(function (e) {
            if (isNaN(String.fromCharCode(e.charCode))) {
                return false;
            }

            //Get the number in the text
            var hours = $("#txRangeStartOdometer").val();
            var minutes = $("#txRangeFinalOdometer").val();

            if (hours.length == 2) hours = '';
            if (minutes.length == 2) minutes = '';

            if ($(this).attr('id') == 'txRangeStartOdometer') hours = hours + String.fromCharCode(e.charCode);
            if ($(this).attr('id') == 'txRangeFinalOdometer') minutes = minutes + String.fromCharCode(e.charCode);
        });

        //Set the max length to the textboxes 
        $("#TimeSlotValueHours").attr('maxlength', '2');
        $("#TimeSlotValueMinutes").attr('maxlength', '2');
        $("#TimeSlotValueSeconds").attr('maxlength', '2');
        $("#txRangeStartOdometer").attr('maxlength', '2');
        $("#txRangeFinalOdometer").attr('maxlength', '2');

        //*************************************************************************************************
        //Set the values as 2 digits when the inputs lost the focus and it has just one number on it
        $('#TimeSlotValue').off('blur.HoursInputOnBlur', '#TimeSlotValueHours').on('blur.HoursInputOnBlur', '#TimeSlotValueHours', function (e) {
            var obj = $('#TimeSlotValueHours');
            var val = $('#TimeSlotValueHours').val();
            if (val.length == 1) obj.val('0' + val); //if the value in the textbox is just one number, adds 0 at the left
            if (val == '') obj.val('00'); //if the value is nothing, add 00 so we avoid null numbers
        });

        $('#TimeSlotValue').off('blur.MinutesInputOnBlur', '#TimeSlotValueMinutes').on('blur.MinutesInputOnBlur', '#TimeSlotValueMinutes', function (e) {
            var obj = $('#TimeSlotValueMinutes');
            var val = $('#TimeSlotValueMinutes').val();
            if (val.length == 1) obj.val('0' + val); //if the value in the textbox is just one number, adds 0 at the left
            if (val == '') obj.val('00'); //if the value is nothing, add 00 so we avoid null numbers
        });

        $('#TimeSlotValue').off('blur.SecondsInputOnBlur', '#TimeSlotValueSeconds').on('blur.SecondsInputOnBlur', '#TimeSlotValueSeconds', function (e) {
            var obj = $('#TimeSlotValueSeconds');
            var val = $('#TimeSlotValueSeconds').val();
            if (val.length == 1) obj.val('0' + val); //if the value in the textbox is just one number, adds 0 at the left
            if (val == '') obj.val('00'); //if the value is nothing, add 00 so we avoid null numbers
        });
        //*************************************************************************************************
    };

    var initToggle = function () {
        $('.toggleCheck').bootstrapToggle({
            on: 'Activo',
            off: 'Inactivo'
        });
    };

    var toggleChange = function () {
        $('input[type=checkbox]').change(function () {
            $('#RuleChange').val(true);
            var check = $(this);
            var ruleId = $(this).attr("ruleId");
            var idValue6 = "#" + $("input[ruleId='6']").attr("id");
            var idValue1 = "#" + $("input[ruleId='1']").attr("id");           

            if ($("#CountryId").val() == 8 && ruleId == 9) {
                if (!$(this).prop('checked')) {
                    $('#InformationMessageRule9Modal').modal({ backdrop: 'static', keyboard: false });
                }                                
            }

            if (ruleId == 1) {
                if ($(this).prop('checked')) {
                    if ($(idValue6).prop('checked'))
                        $(idValue6).bootstrapToggle('off');
                }
            }
            else if (ruleId == 6) {
                if ($(this).prop('checked')) {
                    if ($(idValue1).prop('checked'))
                        $(idValue1).bootstrapToggle('off');
                }
            }
            else if (ruleId == 12) {
                $.ajax({
                    url: '/Administracion/TransactionRules/ValidateTankCapacityRule',
                    type: 'POST',
                    data: JSON.stringify({}),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        if (data != 0) {
                            if (check.prop('checked')) {
                                check.bootstrapToggle('off');
                                $('#InformationMessageModal').modal('show');
                                $('#MarginDivText').addClass('hideText');
                            }
                            else {
                                $('#MarginDivText').addClass('hideText');
                            }
                        }
                        else {
                            if (check.prop('checked')) {
                                $('#MarginDivText').removeClass('hideText');
                            }
                            else {
                                $('#MarginDivText').addClass('hideText');
                            }
                        }
                    }
                });
            }
            else if (ruleId == 13) {
                if (check.prop('checked')) {
                    $('#TimeSlotValue').removeClass('hideText');
                }
                else {
                    $('#TimeSlotValue').addClass('hideText');
                }
            }
            else if (ruleId == 10) {
                if (check.prop('checked')) {
                    $('#OdoRangeDivText').removeClass('hideText');
                }
                else {
                    $('#OdoRangeDivText').addClass('hideText');
                }
            }
            else if (ruleId == 11) {
                if (check.prop('checked')) {
                    $('#DailyTransactionLimitTexts').removeClass('hideText');
                }
                else {
                    $('#DailyTransactionLimitTexts').addClass('hideText');
                }
            }
            else if (ruleId == 17) {
                if (check.prop('checked')) {
                    $('#VehiclePerformanceTexts').removeClass('hideText');
                }
                else {
                    $('#VehiclePerformanceTexts').addClass('hideText');
                }
            }
        });
    };

    var returnRuleState = function () {
        var idValue9 = "#" + $("input[ruleId='9']").attr("id");
        $(idValue9).bootstrapToggle('on');
    };

    var ruleGetData = function (obj) {
        var ruleId = obj.attr("ruleId");
        var ruleCustomerId = obj.attr("ruleCustomerId");
        var customerId = obj.attr("customerId");
        var ruleActive = obj.prop('checked');
        var vehicleid = $("#VehicleId").val();//obj.attr("vehicleid");
        var costcenterid = $("#CostCenterIdForRule").val(); //obj.attr("costcenterid");

        var idValue = "#" + ruleId + "_" + ruleCustomerId + "_Value";
        var ruleValue = $(idValue).val();

        if (findJsonObj(jsonDataArray, "RuleId", ruleId) > 0) {
            removeJsonObj(jsonDataArray, "RuleId", ruleId);
        }

        var data = {
            "CustomerId": customerId,
            "RuleId": ruleId,
            "RuleCustomerId": ruleCustomerId,
            "RuleActive": ruleActive,
            "RuleValue": ruleValue,
            "VehicleId": vehicleid,
            "CostCenterId": costcenterid,
            "RangeParameters": ruleId = "10" ? GetRangeParameters() : null
        }

        jsonDataArray.push(data);
    };

    var findJsonObj = function (array, property, value) {

        var find = 0;

        $.each(array, function (index, result) {
            if (result[property] == value) {
                find++;
            }
        });
        return find;
    }

    var removeJsonObj = function (array, property, value) {
        $.each(array, function (index, result) {
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
            }
        });
    }

    var submitChanges = function () {

        $('#confirmRulesModal').modal('hide');

        $("#submitVehicles").click(function () {
            if ($('input[ruleid="10"]')[0].checked == false) {
                if ($('#AddOrEditForm').valid()) {                   
                        $('#confirmRulesModal').modal('show'); 
                }                
                return;
            }
            if (validateRangeParameters())
                if ($('#AddOrEditForm').valid()) {
                    $('#confirmRulesModal').modal('show');
                }
            else {
                $('#modalverification').modal('show');
            }
        });

        $("#confirmChanges").click(function () {
            if ($('input[ruleid="10"]')[0].checked == false) {
                $('#confirmRulesModal').modal('show');
                return;
            }

            if (validateRangeParameters())
                $('#confirmRulesModal').modal('show');
            else {
                $('#modalverification').modal('show');
            }
        });

        $("#sendRules").click(function () {

            var IsJson = false;
            var vehicleid = $("#VehicleId").val();
            var costcenterid = $("#CostCenterId").val();
            var isvehicle = $("#IsVehicle").val();
            var data = JSON.stringify(jsonDataArray);

            if (vehicleid != undefined || costcenterid != undefined) {

                if (!$('#AddOrEditForm').valid()) {
                    $('#confirmRulesModal').modal('hide');
                    return;
                }

                $('.toggleCheck').each(function () {
                    ruleGetData($(this));
                });

                data = JSON.stringify(jsonDataArray);

                $('#AddOrEditForm').find('#rules').val(data);
                $('#AddOrEditForm').find('#AddVehicleId').val(vehicleid);
                $('#AddOrEditForm').find('#AddCostCenterId').val(costcenterid);
                $('#confirmRulesModal').modal('hide');
                $('#AddOrEditForm').find('#IsVehicle').val(isvehicle);
                $('#AddOrEditForm').submit();

                SaveRangeParameters();
            }
            else {

                $('.toggleCheck').each(function () {
                    ruleGetData($(this));
                });

                data = JSON.stringify(jsonDataArray);

                $('body').loader('show');
                var uri = '/Administracion/TransactionRules/AddOrEditTransactionRules';
                IsJson = true;
                //Send data to backend
                $.ajax({
                    type: "POST",
                    url: uri,
                    data: JSON.stringify(jsonDataArray),
                    success: function (data) {
                        if (data == 'Success') {
                            window.location.reload();
                            $('#confirmRulesModal').modal('hide');
                            $('#invalidRulesConfiguration').hide();
                        } else {
                            jsonDataArray = [];
                            console.error("Internal Error - TransactionRules/AddOrEditTransactionRules");
                            $('#confirmRulesModal').modal('hide');
                            $('#invalidRulesConfiguration').show();
                            $('body').loader('hide');
                            //window.scrollTo(x - coord, y - coord);
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        }
                    },
                    dataType: "json",
                    contentType: "application/json",
                    async: false
                    //traditional: true
                });
            }
            SaveTimeSlot();
            saveMaginParameterData();
            saveDailyTransactionLimit();
            saveVehiclePerformanceParameters();
            //SaveRangeParameters();
        });
    };

    var saveVehiclePerformanceParameters = function () {
        var start = $('#txRangeStartVehiclePerformance').val();
        var end = $('#txRangeFinalVehiclePerformance').val();
        var val = (start == '' ? '15' : start);
        val = val + '-';
        val = val + (end == '' ? '15' : end);

        $.ajax({
            url: '/Administracion/TransactionRules/ParametersTRAddOrEdit',
            type: 'POST',
            data: JSON.stringify({ Value: val, TRId: 17 }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
            }
        });
    };
    
    var getParametersData = function () {
        $.ajax({
            url: '/Administracion/TransactionRules/GetParametersData',
            type: 'POST',
            data: JSON.stringify({}),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                //If there is no data in the DB set the default values
                if (data.length == 0) {
                    DefaultTimeSlotValues();
                }
                else {
                    var par = 0;
                    //goes through the results from the DB and set the values to each input in the TR
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].ParameterName == 'LimitTankCapacity') {
                            $('#margintxt').val(data[i].Value);
                            par = par > 1 ? par : 1;
                        }
                        else if (data[i].ParameterName == 'TimeSlotTransactions') {
                            //If there is no value for the time slot transaction rule, set the default values
                            if (data[i].Value == null || data[i].Value == '') {
                                DefaultTimeSlotValues();
                            }
                            else {
                                $('#TimeSlotValueHours').val(data[i].Value.split(':')[0]);
                                $('#TimeSlotValueMinutes').val(data[i].Value.split(':')[1]);
                                $('#TimeSlotValueSeconds').val(data[i].Value.split(':')[2]);
                                par = 2;
                            }
                        }
                        else if (data[i].ParameterName == 'OdometerCreditCard') {
                            if (data[i].Value == null || data[i].Value == '') {
                                DefaultRangeOdometer(false);
                            }
                            else {
                                $('#txRangeStartOdometer').val(data[i].Value.split('-')[0]);
                                $('#txRangeFinalOdometer').val(data[i].Value.split('-')[1]);
                                par = 3;
                            }
                        }
                        else if (data[i].ParameterName == 'DailyTransactionLimit') {
                            var xml = $.parseXML(data[i].Value);
                            $xml = $(xml);

                            $('#txDailyTransactionLimitNumber').val($xml.find('Number').text());
                            $('#txDailyTransactionLimitAmount').val($xml.find('Amount').text());
                            par = 4;
                        }
                        else if (data[i].ParameterName == 'VehiclePerformance') {
                            if (data[i].Value == null || data[i].Value == '') {
                                DefaultRangeOdometer(true);
                            }
                            else {
                                $('#txRangeStartVehiclePerformance').val(data[i].Value.split('-')[0]);
                                $('#txRangeFinalVehiclePerformance').val(data[i].Value.split('-')[1]);
                                par = 3;
                            }
                            par = 4;
                        }
                    }

                    ////Check if the values are loaded, the set the defaults
                    //switch (par) {
                    //    case 1:
                    //        DefaultTimeSlotValues();
                    //        DefaultRangeOdometer(false);
                    //        break;
                    //    case 2:
                    //        DefaultRangeOdometer(false);
                    //        break;
                    //    case 3:
                    //        break;
                    //}
                }

            }
        });
    };

    var saveMaginParameterData = function () {
        var val = $('#margintxt').val().replace(/\./g, ',');
        var valZero = val.replace(/\,/g, '.');

        if (val == '' || (valZero * 1) == 0) {
            val = '5,00'
        }

        $.ajax({
            url: '/Administracion/TransactionRules/ParametersTRAddOrEdit',
            type: 'POST',
            data: JSON.stringify({ Value: val, TRId: 12 }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
            }
        });
    };

    var saveDailyTransactionLimit = function () {     
        var model = {
            'Number' : $('#txDailyTransactionLimitNumber').val(),
            'Amount' : $('#txDailyTransactionLimitAmount').val()
        }

        $.ajax({
            url: '/Administracion/TransactionRules/DailyTransactionLimitAddOrEdit',
            type: 'POST',
            data: JSON.stringify({ Model : model }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
            }
        });
    };

    var SaveTimeSlot = function () {
        //Save the information for the timeslot tr   
        var val = $('#TimeSlotValueHours').val();
        val = val + ':';
        val = val + $('#TimeSlotValueMinutes').val();
        val = val + ':';
        val = val + $('#TimeSlotValueSeconds').val();

        if (val == '00:00:00') {
            val = '00:15:00'
        }

        $.ajax({
            url: '/Administracion/TransactionRules/ParametersTRAddOrEdit',
            type: 'POST',
            data: JSON.stringify({ Value: val, TRId: 13 }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
            }
        });
    };

    var GetRangeParameters = function () {
        var val = $('#txRangeStartOdometer').val();
        val = val + '-';
        val = val + $('#txRangeFinalOdometer').val();

        return { Value: val, TRId: 10 };
    };

    var SaveRangeParameters = function () {

        //return;

        var val = $('#txRangeStartOdometer').val();
        val = val + '-';
        val = val + $('#txRangeFinalOdometer').val();

        $.ajax({
            url: '/Administracion/TransactionRules/ParametersTRAddOrEdit',
            type: 'POST',
            data: JSON.stringify({ Value: val, TRId: 10 }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
            }
        });
    };

    var validateRangeParameters = function () {
        var val1 = $('#txRangeStartOdometer').val();
        var val2 = $('#txRangeFinalOdometer').val();

        if (!$.isNumeric(val1) || !$.isNumeric(val2)) {
            return false;
        }
        return true;
    };

    var DefaultTimeSlotValues = function () {
        var hours = $('#TimeSlotValueHours');
        var minutes = $('#TimeSlotValueMinutes');
        var seconds = $('#TimeSlotValueSeconds');

        if (hours.val() == '' && minutes.val() == '' && seconds.val() == '') {
            hours.val('00');
            minutes.val('15');
            seconds.val('00');
        }

    };

    var DefaultRangeOdometer = function (performance) {
        if (performance) {
            $('#txRangeStartVehiclePerformance').val('15');
            $('#txRangeFinalVehiclePerformance').val('15');
        } else {
            $('#txRangeStartOdometer').val('10');
            $('#txRangeFinalOdometer').val('10');
        }        
    };

    var onclickSeeBinnacle = function (transactionRuleid) {

        if (!$('#binnacle_' + transactionRuleid).hasClass('clientsee')) {

            var vehicleId = null;
            var costcenterId = null;

            if ($("#IsVehicle").val() == "True") {
                vehicleId = $("#VehicleId").val();
            } else {
                costcenterId = $("#CostCenterId").val();
            }
            $('body').loader('show');
            $.ajax({
                url: '/Administracion/TransactionRules/TransactionBinnacleRetrieve',
                type: 'POST',
                data: JSON.stringify({ RuleId: transactionRuleid, VehicleId: vehicleId, CostCenterId: costcenterId }),
                contentType: 'application/json',
                success: function (data) {
                    $('#binnacle_' + transactionRuleid).html(data);
                    $('#binnacle_' + transactionRuleid).addClass('clientsee');
                    $('#binnacle_' + transactionRuleid).show('slow');
                    $('body').loader('hide');
                }
            });
        }  
        else {
            $('#binnacle_' + transactionRuleid).hide('slow');
            $('#binnacle_' + transactionRuleid).removeClass('clientsee');
        }
        
    };

    /* Public methods */
    return {
        Init: initialize,
        ValidateRangeParameters: validateRangeParameters,
        ReturnRuleState: returnRuleState,
        OnclickSeeBinnacle: onclickSeeBinnacle
    };
})();

