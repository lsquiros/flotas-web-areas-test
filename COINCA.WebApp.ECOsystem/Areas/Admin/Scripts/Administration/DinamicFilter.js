﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.DinamicFilter = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);   
              
        $("#ddlUsers").off('change.ddlUsers').on('change.ddlUsers', function () {
            var val = $("#ddlUsers").val();

            if (val != "")
            {
                onRolesChangeMenu();
                $('#ddlFilterType').attr('disabled', false);
                //Limpiar el árbol
                $('#treeview-checkable').html('');
                $('#treeview-checkable').prepend('<p class="top20 text-success">No se encontraron resultados</p>');
                $('#btn-check-node').addClass('disabled');
                $('#btn-uncheck-node').addClass('disabled');
                $('#btn-check-all').addClass('disabled');
                $('#btn-uncheck-all').addClass('disabled');
                $('#btn-savedata').addClass('hidden');
                $('#CheckAll').addClass('hidden');
                $('#treeViewContainer').addClass('hide');

                $('#select2-chosen-2').html("Seleccione un Tipo de Filtro...");
            }                
            else
            {
                location.reload(); 
            }                
        });

        //Init dropDown 
        $("#ddlUsers").select2('data', {});
        $("#ddlUsers").select2({
            formatResult: userSelectFormat,
            formatSelection: userSelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        $("#ddlFilterType").select2('data', {});
        $("#ddlFilterType").select2({
            formatResult: filterSelectFormat,
            formatSelection: filterSelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });
    };

    /* Select Format*/
    var userSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
                '</div>';
    };

    var filterSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
                '</div>';
    };
   
    var saveDinamicFilter = function () {
        
        var checked = $('#treeview-checkable').treeview('getChecked');
        var key = $('#ddlFilterType').val();
        var userId = $("#ddlUsers").val();
        var list = [];

        $.each(checked, function () {            
                list.push(this.id);            
        })

        $.ajax({
            url: '/DinamicFilter/SaveTreeViewData',
            type: 'POST',
            data: JSON.stringify({ List: list, Key: key, UserId : userId }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data == 1) {                    
                    $('#ConfirmationSaveModalDinamicFilter').modal('hide');
                    $('#InformationMessageModal').modal('show');                    
                    location.reload();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
        });
    }

    var confirmationModalShow = function () {       
        $('#ConfirmationSaveModalDinamicFilter').modal('show');
    };

    var onRolesChangeMenu = function () {
        
        $('#ddlFilterType').attr('disabled', true);
        $('#ddlFilterType').empty();
        $('#ddlFilterType').append('<option value>Seleccione un Tipo de Filtro...</option>');
               
        $.ajax({
            url: '/DinamicFilter/GetFilterType',
            type: 'POST',
            data: JSON.stringify({ }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                
                for (var i = 0; i < data.length; i++) {
                    $('#ddlFilterType').append('<option value="' + data[i].Value + '">'
                            + data[i].Text +
                            '</option>');
                }                
            }
        });      
    }
    
    return {
        Init: initialize,
        SaveDinamicFilter: saveDinamicFilter,
        ConfirmationModalShow: confirmationModalShow
        
    };
})();