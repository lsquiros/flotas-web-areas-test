﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.CostCenter = (function () {

    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
                
        $('#gridContainer').off('click.ref_row', 'a[ref_row]').on('click.ref_row', 'a[ref_row]', function (e) {
            $('#loadRefForm').find('#id').val($(this).attr('id'));
            $('#loadRefForm').submit();
        });

        //Edit for the CostCenter grid
        $('#gridContainer').off('click.edit', 'a[edit_row_costcenter]').on('click.edit', 'a[edit_row_costcenter]', function (e) {
            ECOsystem.Utilities.ShowLoader();
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();
            type = 1;
            //setTitle($(this));
        });

        $("#enableAllVehicles").on("click", function () { changePullPreviousBudget(true) });
        $("#disableAllVehicles").on("click", function () { changePullPreviousBudget(false) });

        $('#UnitId').select2('data', {});
        $('#UnitId').select2({
            formatResult: UnitIdSelectFormat,
            formatSelection: UnitIdSelectFormat,
            escapeMarkup: function (m) { return m; }
        });
    };  

    /* driver Select Format*/
    var UnitIdSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
                '<div class="col-md-12">' + json.Name + '</div>' +
                '</div>';
    };
   
    var goIndex = function(data) {
        ECOsystem.Utilities.HideLoader();
        if (data === "" || data == undefined) {
            window.location.replace('/Administracion/VehicleCostCenters/Index');
        } else {
            $('#modalMessage').html(data);
        }
    };

    var onClickDownloadCostCenter = function () {
        
        $('#ExcelReportDownloadForm').submit();
    };


    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadExcelForm').submit();
    };

    /* Public methods */
    return {
        Init: initialize,
        GoIndex: goIndex,
        OnClickDownloadCostCenter: onClickDownloadCostCenter,
        ShowLoader: showLoader,
        HideLoader: hideLoader
    };
})();
