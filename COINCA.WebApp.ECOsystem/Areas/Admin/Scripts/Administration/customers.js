﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.Customers = (function () {
    var options = {};
    var line = 0;

    /* initialize function */

    var initialize = function (opts) {
        $.extend(options, opts);       
        initSelects();
        initButtons();
        fileApiUpload();
        initCheckUsername();
        initCurrencyInputs();
        initDates();
        initCsv();
    }

    /*initialize Drop Down List*/
    var initDropDownList = function () {
        try {
            var select1 = $("#StateId").select2().data('select2');
            select1.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onStateChange(data); return fn.apply(this, arguments); }
                }
            })(select1.onSelect);

            var select2 = $("#CountyId").select2().data('select2');
            select2.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onCountyChange(data); return fn.apply(this, arguments); }
                }
            })(select2.onSelect);

            var select3 = $("#CityId").select2().data('select2');
            select3.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { return fn.apply(this, arguments); }
                }
            })(select3.onSelect);

        } catch (e) {

        }

    };

    var initCsv = function () {
        $('#UploadRequestContainer').off('hidden.bs.modal', '#importModal').on('hidden.bs.modal', '#importModal', function (e) {
            $('#UploadRequestContainer').html('');
        });

        $('#UploadRequestContainer').off('change.csvfile', '#csvFileInput').on('change.csvfile', '#csvFileInput', function (e) {
            handleFiles(this.files);
        });
    }

    /*init Dates*/
    var initDates = function () {

        $('#PaymentReference').keypress(function (e) {
            var a = [];
            var k = e.which;
            for (i = 48; i < 58; i++)
                a.push(i);
            if (!(a.indexOf(k) >= 0))
                e.preventDefault();
        })

        $('#datePickerEstimatedDelivery').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $("#EstimatedDeliveryStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });
    }
    
    /*init Currency Inputs*/
    var initCurrencyInputs = function () {
        $.fn.alphanum.setNumericSeparators({
            thousandsSeparator: ".",
            decimalSeparator: ","
        });

        try {

            $('body').off('blur.currencyOnBlur', 'input[data-currency], input[datacurrency]').on('blur.currencyOnBlur', 'input[data-currency], input[datacurrency]', function (e) {
                var obj = $(this);
                obj.siblings('input[type="hidden"]').val(ECOsystem.Utilities.ToNum(obj.val()));
                obj.val(ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToNum(obj.val())));
            });
            $('body').find('input[data-currency], input[datacurrency]').numeric({
                allowMinus: false,
                maxDecimalPlaces: 2
            });

        } catch (e) {

        }
    };

    /*initCheckUsername*/
    var initCheckUsername = function () {
        $('#detailContainer').off('change.email', '#AdminEmail').on('change.email', '#AdminEmail', function (e) {
            checkValidUserName();
        });

        $("#AdminEmail").keyup(function () {
            checkValidUserName();
        });
    }

    var checkValidUserName = function () {
        if (validateEmail()) {
            $('#checkForm').find('#username').val($('#AdminEmail').val());
            $('#checkForm').submit();
        } else {
            $('.modal-footer button[type="submit"]').attr('disabled', 'disabled');
        }
    };
    
    /* Email validation to prevent an invalid submission*/
    var validateEmail = function () {
        setErrorMsj('', false);
        if ($('#AdminEmail').val().length == 0) {
            return false;
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#AdminEmail').val().match(email)) {
            return true;
        } else {
            setErrorMsj('Correo electrónico invalido', true);
            return false;
        }
    };

    /* Show or hide errors messages*/
    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj').html(msj);
            $('#validationErrors').removeClass('hide');
        } else {
            $('#errorMsj').html('');
            $('#validationErrors').addClass('hide');
        }
    }

    /*init Selects*/
    var initSelects = function () {
        try {

            $("#searchForm").find('select').select2();

            var select1 = $("#ddlCountryId").select2().data('select2');
            select1.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSearchSelectChange(data); return fn.apply(this, arguments); }
                }
            })(select1.onSelect);
        } catch (e) {

        }
    }


    /*init Buttons*/
    var initButtons = function () {
        try {
            $('input[name="CountryId"]').val($('#ddlCountryId').select2('data').id);
        } catch (e) { }

        try {
            //trigger click on hidden file input
            $('#detailContainer').off('click.btnUpload', '#btnUpload').on('click.btnUpload', '#btnUpload', function (e) {
                $('#fileHidden').trigger('click');
                e.stopPropagation();
            });

        } catch (e) { }

        try {
            $('#gridContainer').off('click.edit_card-row', 'a[edit_card-row]').on('click.edit_card-row', 'a[edit_card-row]', function (e) {
                $('#loadCustomerCardForm').find('#CustomerCardId').val($(this).attr('data-id'));
                $('#loadCustomerCardForm').submit();
            });
            $('#gridCardRequestContainer').off('click.editCardRequest', 'a[edit-card-request]').on('click.editCardRequest', 'a[edit-card-request]', function (e) {
                $('#loadCustomerCardRequestForm').find('#CardRequestId').val($(this).attr('data-id'));
                $('#loadCustomerCardRequestForm').submit();
            });

            $('#gridCardRequestContainer').off('click.delCardRequest', 'a[del-card-request]').on('click.delCardRequest', 'a[del-card-request]', function (e) {
                var deleteForm = $('#deleteForm');
                deleteForm.find('#deleteId').val($(this).attr('data-id'));
                deleteForm.find('#deleteEntity').val("CARD_REQUEST");
                deleteForm.attr('data-ajax-update', '#gridCardRequestContainer');
            });

            $('#gridContainer').off('click.delCustomerCard', 'a[del_card-row]').on('click.delCustomerCard', 'a[del_card-row]', function (e) {
                var deleteForm = $('#deleteForm');
                deleteForm.find('#deleteId').val($(this).attr('data-id'));
                deleteForm.find('#deleteEntity').val("CUSTOMER_CARD");
                deleteForm.attr('data-ajax-update', '#gridContainer');
            });

            //Edit user
            $('#gridUsersContainer').off('click.editUser', 'a[edit-user-row]').on('click.editUser', 'a[edit-user-row]', function (e) {
                $('#loadUsersForm').find('#CustomerUserId').val($(this).attr('id'));
                $('#loadUsersForm').submit();
            });

            $('#gridUsersContainer').off('click.SetUser', 'a[set-user-row]').on('click.SetUser', 'a[set-user-row]', function (e) {
                $('body').loader('show');
                $('#CustomersByCustomersModal').find('#CUserId').val($(this).attr('id'));
                $('#CustomersByCustomersModal').find('#CustomerName').html($(this).attr('data-CustomerName'));
            });

            $('#gridUsersContainer').off('click.delUser', 'a[del-user-row]').on('click.delUser', 'a[del-user-row]', function (e) {               
                $('#deleteModalCustomerUsers').find('#id').val($(this).attr('id'));                
            });

            $('#addOrEditForm').off('click.edit', '#btnEdit').on('click.edit', '#btnEdit', function (e) {
                $('#loadForm').submit();
            });
            $('#addOrEditForm').off('click.cancel-edit', '#btnCancel').on('click.cancel-edit', '#btnCancel', function (e) {
                $('#cancelForm').submit();
            });

            $('#page-full-wrapper').off('click.loadCustomerCardForm', '#btnAddCard').on('click.loadCustomerCardForm', '#btnAddCard', function (e) {
                $('#loadCustomerCardForm').find('#CustomerCardId').val("-1");
                $('#loadCustomerCardForm').submit();
            });

            $('#page-full-wrapper').off('click.btnAddCardRequest', '#btnAddCardRequest').on('click.btnAddCardRequest', '#btnAddCardRequest', function (e) {
                $('#loadCustomerCardRequestForm').find('#CardRequestId').val("-1");
                $('#loadCustomerCardRequestForm').submit();
                ECOsystem.Utilities.ApplyNumericInputs(); //Apply numeric inputs
            });

            //Add New User
            $('#page-full-wrapper').off('click.loadUsersForm', '#btnAddUsers').on('click.loadUsersForm', '#btnAddUsers', function (e) {            
                $('#loadUsersForm').find('#CustomerUserCustId').val();
                $('#loadUsersForm').find('#CustomerUserId').val('');
                $('#loadUsersForm').submit();
            });

            //Agregar Terminal form STEFANO
            
            $('#page-full-wrapper').off('click.loadTerminalRequestForm', '#btnCreateTerminal').on('click.loadTerminalRequestForm', '#btnCreateTerminal', function (e) {
                $('#loadTerminalRequestForm').submit();

                //ECOsystem.Utilities.ApplyNumericInputs(); //Apply numeric inputs
            });

            $('#page-full-wrapper').off('click.btnAddCardRequestFull', '#btnAddCardRequestFull').on('click.btnAddCardRequestFull', '#btnAddCardRequestFull', function (e) {
                $('#LoadUploadModalForm').submit();
            });


        } catch (e) { }
    }

    /*on Select Report Criteria Change*/
    var onSearchSelectChange = function (obj) {
        if (obj.id === '') {
            $('input[name="CountryId"]').removeAttr('value');
            $('#searchForm').submit();
        } else {
            $('input[name="CountryId"]').val(obj.id);
            $('#searchForm').submit();
        }
    };


    var subdelterminal = function () {
        $('#deleteTForm').submit();
    };

    /* Standard Error Function for File API */

    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };


    /* Update progress for File API */
    var fileApiUpdateProgress = function (e) {
        if (e.lengthComputable) {
            var percentLoaded = Math.round((e.loaded / e.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                $('.percent').css('width', percentLoaded + '%').html(percentLoaded + '%');
            }
        }
    };


    /* File Upload implementation */

    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            //trigger click on hidden file input
      
            $('#detailContainer').off('change.fileHidden', '#fileHidden').on('change.fileHidden', '#fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onprogress = fileApiUpdateProgress;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {
                    $('#imgLogo').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        alert("La extensión del archivo es inválida");
                        continue;
                    }
                    // Closure to capture the file information.
                    reader.onload = (function (F) {
                        $('.percent').html('100%').css('width', '100%');
                        setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgLogo').removeClass('imgloading');", 2000);
                        return function (e) {
                            var lsrc = e.target.result;
                            $('#imgLogo').attr({
                                src: lsrc,
                                title: escape(F.name)
                            });
                            $('#Logo').val(lsrc);
                        };
                    })(f);
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
            });
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.');
        }
    };


    /*onSuccessLoad*/
    var onSuccessLoad = function () {
        initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    /*on Success Check User*/
    var onSuccessCheckUser = function (data) {
        if (!data.result) {
            $('.modal-footer button[type="submit"]').attr('disabled', 'disabled');
            setErrorMsj(data.message, true);
        } else {
            $('.modal-footer button[type="submit"]').removeAttr('disabled');
            setErrorMsj('', false);
        }
    }

    /*onSuccessLoadEdit*/
    var onSuccessLoadEdit = function () {
        $('#addOrEditForm').find('button[data-read-only-info]').addClass('hide');
        $('#addOrEditForm').find('button[data-read-write-info]').removeClass('hide');
        $('#addOrEditForm').find('select').not("[custom='true']").select2();
    };

    /*onSuccessCancelEdit*/
    var onSuccessCancelEdit = function () {
        $('#addOrEditForm').find('button[data-read-only-info]').removeClass('hide');
        $('#addOrEditForm').find('button[data-read-write-info]').addClass('hide');
    };

    /*onSuccessEdit*/
    var onSuccessEdit = function () {
        onSuccessCancelEdit();
    };    

    /**/
    var onSuccessLoadCard = function () {
        $("#addOrEditCardModal").find('select').not("[custom='true']").select2();

        $('#addOrEditCardModal').find('#DisplayCreditCardNumber').alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '-',
            maxLength: 19
        });

        initCurrencyInputs();

        $('#addOrEditCardModal').modal('show');
    }; TxtNewUserEmailAdd


    //Partial Add Users
    var onSuccessLoadUsers = function () {        
       // $("#addOrEditUsersModal").find('select').not("[custom='true']").select2();
        //$("#TxtNewUserNameAdd").val('');
        //$('#TxtNewUserEmailAdd').val('');
        //$('#TxtNewUserCellAdd').val('');
        //$('#TxtNewUserIDAdd').val('');
        $('#addOrEditUsersModal').modal('show');
    };

    var onSuccessAddEditUsers = function () {       
        $('#addOrEditUsersModal').modal('hide');
    };
   
    var onSuccessUpdateCard = function () {
        $('#addOrEditCardModal').modal('hide');
    };

    /*onSuccessLoadCardRequest*/
    var onSuccessLoadCardRequest = function () {
        $("#addOrEditCardRequestModal").find('select').not("[custom='true']").select2();
        initDates();

        $('#addOrEditCardRequestModal').off('click.btnPaste', '#btnPaste').on('click.btnPaste', '#btnPaste', function (e) {
            $('#loadPreviousCardRequestForm').submit();
        });

        initDropDownList();

        $('#addOrEditCardRequestModal').removeData('validator');
        $.validator.unobtrusive.parse($('#addOrEditCardRequestModal'));

        $('#addOrEditCardRequestModal').modal('show');

    };


    /*onSuccessTerminalRequest*/
    var onSuccessTerminalRequest = function () {

        $("#addOrEditTerminalModal").find('select').not("[custom='true']").select2();        
        $("#txtTerminal").val('');
        $("#txtDescripcionTerminal").val('');
        $('#addOrEditTerminalModal').modal('show');

    };

    var onBeginTerminalRequest = function () {

        if ($("#txtTerminal").val().trim() == '') {
            if ($('#validationMessage').length > 0)
                return false;

            var validationMessage = '<span id="validationMessage" class="text-danger field-validation-error" data-valmsg-for="Terminal" data-valmsg-replace="true">';
            validationMessage += '<span for="Terminal" class="">ID de la Terminal es requerido.</span>';
            validationMessage += '</span>';
            $(validationMessage).insertAfter($("#txtTerminal"));
            return false;
        }
        else
            $('#validationMessage').remove();

        return true;
    };

    var onSuccessAddorEditTerminal = function () {
        $('#addOrEditTerminalModal').modal('hide');
    };

    /*on Success Load Upload Request*/
    var onSuccessLoadUploadRequest = function () {
        $('#importModal').find(":file").filestyle({
            buttonText: "&nbsp;Seleccionar Archivo...",
            buttonName: "btn-warning",
            buttonBefore: true,
        });
        $('#importModal').modal('show');
    };

    var importModalClose = function () {
        $('#importModal').modal('hide');
    }

    /*on Success Upload Check*/
    var onSuccessUploadCheck = function (data) {
        alert(data.message);
    };

    /*onSuccessUpdateCardRequest*/
    var onSuccessUpdateCardRequest = function () {
        $('#addOrEditCardRequestModal').modal('hide');
    };

    /*onSuccessLoadPreviousCardRequest*/
    var onSuccessLoadPreviousCardRequest = function (data) {
        if (data.result) {
            if (data.cardRequest) {
                $('#StateId').val(data.cardRequest.StateId);
                $('#StateId').select2('val', data.cardRequest.StateId);
                if (data.listCounties != null) {
                    loadCounties(data.listCounties);
                }
                $('#CountyId').val(data.cardRequest.CountyId);
                $('#CountyId').select2('val', data.cardRequest.CountyId);
                if (data.listCities != null) {
                    loadCities(data.listCities);
                }
                $('#CityId').val(data.cardRequest.CityId);
                $('#CityId').select2('val', data.cardRequest.CityId);
                $('#AddressLine1').val(data.cardRequest.AddressLine1);
                $('#AddressLine2').val(data.cardRequest.AddressLine2);
                $('#AuthorizedPerson').val(data.cardRequest.AuthorizedPerson);
                $('#EstimatedDeliveryStr').val(data.cardRequest.EstimatedDeliveryStr);
                $('#ContactPhone').val(data.cardRequest.ContactPhone);
            }
            //loadCounties(data.listCounties)
        }
    }

    var onStateChange = function (obj) {
        $('#CityId').select2('val', null);
        $('#CountyId').select2('val', null);
        $('#loadCountiesForm').find("input[name=stateId]").val(obj.id);
        $('#loadCountiesForm').submit();
        $('#CityId').html("");
        $('#CityId')
            .append($("<option></option>")
            .text("Seleccione Distrito"));
        //$('#CountyId').attr("placeholder", "Seleccione Cantón");
        //$('#CountyId').select2();
        //$("#CountyId").removeAttr("value");
        //$('#CountyId').val(null);
        //$('#CityId').attr("placeholder", "Seleccione Distrito");
        //$('#CityId').select2();
        //$("#CityId").removeAttr("value");
        //$('#CityId').val(null);
    };

    var onCountyChange = function (obj) {
        $('#CityId').select2('val', null);
        $('#loadCitiesForm').find("input[name=countyId]").val(obj.id);
        $('#loadCitiesForm').submit();
        //$('#CityId').attr("placeholder", "Seleccione Distrito");
        //$('#CityId').select2();
        //$("#CityId").removeAttr("value");
        //$('#CityId').val(null);

    };

    /*onSuccessLoadCounties*/
    var onSuccessLoadCounties = function (data) {
        if (data.result) {
            loadCounties(data.listCounties);
        }
    }

    /*onSuccessLoadCities*/
    var onSuccessLoadCities = function (data) {
        if (data.result) {
            loadCities(data.listCities);
        }
    }

    /*loadCounties*/
    var loadCounties = function (options) {

        $('#CountyId').html("");
        $('#CountyId')
            .append($("<option></option>")
            .text("Seleccione Cantón"));
        $.each(options, function (index, item) {
            $('#CountyId')
                .append($("<option></option>")
                .attr("value", item.id)
                .text(item.text));
        });
    }

    /*loadCities*/
    var loadCities = function (options) {

        $('#CityId').html("");
        $('#CityId')
            .append($("<option></option>")
            .text("Seleccione Distrito"));
        $.each(options, function (index, item) {
            $('#CityId')
                .append($("<option></option>")
                .attr("value", item.id)
                .text(item.text));
        });
    }

    /** File Upload **/
    function handleFiles(files) {
        // Check for the various File API support.
        if (window.FileReader) {
            // FileReader are supported.
            getAsText(files[0]);
        } else {
            alert('FileReader are not supported in this browser.');
        }
    }

    function getAsText(fileToRead) {
        var reader = new FileReader();
        // Handle errors load
        reader.onload = loadHandler;
        reader.onerror = errorHandler;
        // Read file into memory as UTF-8      
        reader.readAsText(fileToRead);
    }

    function loadHandler(event) {
        var csv = event.target.result;
        processData(csv);
    }

    function processData(csv) {
        var allTextLines = csv.split(/\r\n|\n/);
        var lines = [];
        while (allTextLines.length) {
            lines.push(allTextLines.shift().split(','));
        }
        drawOutput(lines);
    }

    function errorHandler(evt) {
        if (evt.target.error.name == "NotReadableError") {
            alert("Canno't read file !");
        }
    }

    function drawOutput(lines) {
        //Clear previous data
        $('#csvOutput').html('');
      
        var result = '<table class="table table-striped">';
        
        result += getOutputHeader();
        result += "<tbody>";
        line = 0;
        for (var i = 1; i < lines.length; i++) {

            result += getOutputRow(lines[i], i);
        }
        if (lines.length < 2) {
            result += "<tr><td colspan='10' class='text-danger'>Error procesando información</td></tr>";
        }

        result += '</tbody></table>';

        $('#csvOutput').html(result);

        $('#importModal').find('button:submit').removeAttr('disabled');
    }

    function getOutputHeader() {
        return '<thead><tr>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Placa</span></div></th>' +
            '<th class="grid-header" ><div class="grid-header-title"><span>Conductor</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Identificación</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Provincia</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Cantón</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Distrito</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Dirección de Entrega</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Dirección Adicional</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Ref. Pago</span></div></th>' +
            '<th class="grid-header" ><div class="grid-header-title"><span>Autorizado</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Teléfono</span></div></th>' +
            '<th class="grid-header" nowrap><div class="grid-header-title"><span>Entrega</span></div></th>' +
            '<th class="grid-header" nowrap></th>' +
            '</tr></thead>';
    }

    function getOutputRow(lines, currentLine) {
        try {
            //text-success  text-danger
            var result = "";
            var tipoDeEmision = $("#CurrentTipoEmision").val();

            if (lines.length != 12) {
                result = "<span class='text-danger'><i class='glyphicon glyphicon-remove'></i>&nbsp;&nbsp;Número de columnas en el archivo diferente al esperado</span>";
            } else if (parseDate(lines[11]) == undefined) {
                result = "<span class='text-danger'><i class='glyphicon glyphicon-remove'></i>&nbsp;&nbsp;Formato de fecha incorrecto</span>";
            }
         
            if (result == "") {

                if (tipoDeEmision == "100" && lines[1] == "") { //Emision es por Conductor

                    return "<tr><td colspan='13' class='text-danger'><i class='glyphicon glyphicon-remove'></i>&nbsp;&nbsp;Nombre Conductor es requerido, el Socio emite tarjetas por Conductor -- Número de linea: " + currentLine + "</td></tr>"

                } else if (tipoDeEmision == "101" && lines[0] == "") { //Emision es por Vehiculo

                    return "<tr><td colspan='13' class='text-danger'><i class='glyphicon glyphicon-remove'></i>&nbsp;&nbsp;Placa de Vehiculo es requerido, el Socio emite tarjetas por Vehículo -- Número de linea: " + currentLine + "</td></tr>"
                }

                return '<tr>' +
                    '<td class="grid-cell" nowrap>' + lines[0] + '</td>' +
                    '<td class="grid-cell">' + lines[1] + '</td>' +
                    '<td class="grid-cell">' + lines[2] + '</td>' +
                    '<td class="grid-cell">' + lines[3] + '</td>' +
                    '<td class="grid-cell">' + lines[4] + '</td>' +
                    '<td class="grid-cell">' + lines[5] + '</td>' +
                    '<td class="grid-cell">' + lines[6] + '</td>' +
                    '<td class="grid-cell">' + lines[7] + '</td>' +
                    '<td class="grid-cell">' + lines[8] + '</td>' +
                    '<td class="grid-cell">' + lines[9] + '</td>' +
                    '<td class="grid-cell">' + lines[10] + '</td>' +
                    '<td class="grid-cell">' + lines[11] + '</td>' +
                    '<td class="grid-cell"><i class="text-success glyphicon glyphicon-ok"></i></td>' +
                    '</tr>' +
                    '<input type="hidden" name="list[' + line + '].PlateId" value="' + lines[0] + '">' +
                    '<input type="hidden" name="list[' + line + '].DriverName" value="' + lines[1] + '">' +
                    '<input type="hidden" name="list[' + line + '].DriverIdentification" value="' + lines[2] + '">' +
                    '<input type="hidden" name="list[' + line + '].DeliveryState" value="' + lines[3] + '">' +
                    '<input type="hidden" name="list[' + line + '].DeliveryCounty" value="' + lines[4] + '">' +
                    '<input type="hidden" name="list[' + line + '].DeliveryCity" value="' + lines[5] + '">' +
                    '<input type="hidden" name="list[' + line + '].AddressLine1" value="' + lines[6] + '">' +
                    '<input type="hidden" name="list[' + line + '].AddressLine2" value="' + lines[7] + '">' +
                    '<input type="hidden" name="list[' + line + '].PaymentReference" value="' + lines[8] + '">' +
                    '<input type="hidden" name="list[' + line + '].AuthorizedPerson" value="' + lines[9] + '">' +
                    '<input type="hidden" name="list[' + line + '].ContactPhone" value="' + lines[10] + '">' +
                    '<input type="hidden" name="list[' + (line++) + '].EstimatedDelivery" value="' + lines[11] + '">';
            } else {
                return "<tr><td colspan='13' class='text-danger'>" + result +" -- Número de linea: " + line +  "</td></tr>";
            }
        } catch (e) {
            return "<tr><td colspan='13' class='text-danger'>Error procesando información</td></tr>";
        } 
        
    }

    function parseDate(str) {
        try {
            // validate year as 4 digits, month as 01-12, and day as 01-31 
            if ((str = str.match(/^(\d{4})(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01])$/))) {
                // make a date
                str[0] = new Date(+str[1], +str[2] - 1, +str[3]);
                // check if month stayed the same (ie that day number is valid)
                if (str[0].getMonth() === +str[2] - 1)
                    return str[0];
            }
            return undefined;
        } catch (e) {
            return undefined;
        }
    } 
      
    function saveCustomersByCustomer() {
        
        var checked = $('#treeview-checkable').treeview('getChecked');
        var userId = $('#CUserId').val();
        var role = $('#dbxRoleList').val();
        var list = [];

        if (role != "")
        {
            $('#errorMessage').html('');

            $.each(checked, function () {
                list.push(this.id);
            })

            $.ajax({
                url: '/Customers/SaveTreeViewData',
                type: 'POST',
                data: JSON.stringify({ List: list, UserId: userId, RoleId: role }),
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    if (data == 1) {
                        $('#ConfirmationSaveModalCustomerByCustomers').modal('hide');
                        $('#InformationMessageModal').modal('show');
                        location.reload();
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        $('#treeview-checkable').html('');
                        $('#CustomersByCustomersModal').modal('hide');
                    }
                }
            });
        }
        else
        {
            $('#errorMessage').html('Por favor seleccione un rol');
            $('#ConfirmationSaveModalCustomerByCustomers').modal('hide');
        }        
    }

    /* Prices Maintenance */
    var editMaintenancePrices = function () {
        $("button[data-read-only-prices]").hide();
        $("button[data-write-prices]").show();
        $("#fieldSetPrices").removeAttr("disabled");
        $("#CostCardAdminStr").focus();
    };

    var readMaintenancePrices = function () {
        $("button[data-read-only-prices]").show();
        $("button[data-write-prices]").hide();
        $("#fieldSetPrices").attr("disabled", "disabled");
    };

    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        OnSuccessCheckUser: onSuccessCheckUser,
        OnSuccessLoadEdit: onSuccessLoadEdit,
        OnSuccessEdit: onSuccessEdit,
        OnSuccessCancelEdit: onSuccessCancelEdit,
        OnSuccessLoadCard: onSuccessLoadCard,
        OnSuccessUpdateCard: onSuccessUpdateCard,
        OnSuccessLoadCardRequest: onSuccessLoadCardRequest,
        OnBeginTerminalRequest: onBeginTerminalRequest,
        OnSuccessTerminalRequest: onSuccessTerminalRequest,
        OnSuccessAddorEditTerminal : onSuccessAddorEditTerminal,
        OnSuccessUpdateCardRequest: onSuccessUpdateCardRequest,
        OnSuccessLoadPreviousCardRequest: onSuccessLoadPreviousCardRequest,
        OnSuccessLoadUploadRequest: onSuccessLoadUploadRequest,
        OnSuccessLoadCounties: onSuccessLoadCounties,
        OnSuccessLoadCities: onSuccessLoadCities,
        OnSuccessImport: importModalClose,
        Subdelterminal: subdelterminal,
        OnSuccessLoadUsers: onSuccessLoadUsers,
        OnSuccessAddEditUsers: onSuccessAddEditUsers,
        SaveCustomersByCustomer: saveCustomersByCustomer,
        EditPrices: editMaintenancePrices,
        ReadPrices: readMaintenancePrices
    };

})();
