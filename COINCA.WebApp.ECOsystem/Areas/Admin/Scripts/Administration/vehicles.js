﻿var ECO_Admin = ECO_Admin || {};

 ECO_Admin.Vehicles = (function () {

    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        $('#Year').numeric("integer");
        $("#FreightTemperature").numeric("integer");
        $("#Cost").numeric("integer");
        $("#FreightTemperatureThreshold1").numeric("integer");
        $("#FreightTemperatureThreshold2").numeric("integer");
        $('#datetimepicker1').datepicker({ language: 'es', minDate: new Date(), format: 'dd/mm/yyyy', autoclose: true, todayHighlight: true });
        $("#OdometerVehicle").numeric("integer");
        resetAndInitCustomControls();

        //Create Vehicle Modal
        onClickNew();

        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            resetAndInitCustomControls();
        });

        $('#gridContainer').off('click.ref_row', 'a[ref_row]').on('click.ref_row', 'a[ref_row]', function (e) {
            $('#loadRefForm').find('#id').val($(this).attr('id'));
            $('#loadRefForm').submit();
        });

        //Edit for the vehicle grid
        $('#gridContainer').off('click.edit', 'a[edit_row_vehicles]').on('click.edit', 'a[edit_row_vehicles]', function (e) {
            ECOsystem.Utilities.ShowLoader();
            $('#loadForm').find('#id').val($(this).attr('id'));
            $('#loadForm').submit();
            type = 1;
            //setTitle($(this));
        });

        $('#btnAddAlarm').off('click.btnAddAlarm').on('click.btnAddAlarm', function (e) {
            $('#alarmForm').find('#id').val($('#id').val());
            $('#alarmForm').submit();
            $(this).attr('disabled', 'disabled');
        });

        $('#btnVehicleBinnacleSubmit').off('click.btnVehicleBinnacleSubmit').on('click.btnVehicleBinnacleSubmit', function () {
            var text = $('#txtCommentBinnacle').val();
            if (text == '') {
                $('#txtCommentBinnacle').focus();
            } else {
                $('#VehicleBinnacleAddForm').find('#Comment').val(text);
                $('#VehicleBinnacleAddForm').submit();
            }
        });

    };

    /* Reset And Init Custom Select*/
    var resetAndInitCustomControls = function () {
        $('#UserId').select2('data', {});
        $('#UserId').select2({
            //minimumInputLength: 3,
            formatResult: driverSelectFormat,
            formatSelection: driverSelectedFormat,
            escapeMarkup: function (m) { return m; },
            matcher: driverSearch,
            allowClear: true
        });

        $('#VehicleCategoryId').select2('data', {});
        $('#VehicleCategoryId').select2({
            formatResult: vehicleCategorySelectFormat,
            formatSelection: vehicleCategorySelectFormat,
            escapeMarkup: function (m) { return m; }
        });

        $('#CostCenterId').select2('data', {});
        $('#CostCenterId').select2({
            escapeMarkup: function (m) { return m; }
        });

        $('#PeriodicityId').select2('data', {});
        $('#PeriodicityId').select2({
            formatResult: PeriodicitySelectFormat,
            formatSelection: PeriodicitySelectFormat,
            escapeMarkup: function (m) { return m; }
        });
    };

    /* Cost Center Format*/
    var CostCenterSelectFormat = function (item) {

        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-12">' + json.Name + '</div>' +
            '</div>';
    };

    /* PeriodicityId Format*/
    var PeriodicitySelectFormat = function (item) {

        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-6">' + json.Name + '</div>' +
            '</div>';
    };


    /* On click BtnNew*/
    var onClickNew = function () {
        $('#btnAdd').off('click.btnAddNew').on('click.btnAddNew', function (e) {
            customerCreditCardValidate()
        });
    };

    /* On Success Load after call edit*/
    var onSuccessLoad = function () {
        initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    /* On Success Reference Load after call edit*/
    var onSuccessReferenceLoad = function () {
        $("#referenceEditForm").find('select').not("[custom='true']").select2();
        $('#referenceEditModal').modal('show');
    };

    var referenceEditModalClose = function () {
        $('#referenceEditModal').modal('hide');
    };

    /* driver Select Format*/
    var vehicleCategorySelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-5 less-padding-right format">' + json.Manufacturer + '</div>' +
            '<div class="col-md-5 less-padding-left format">' + json.VehicleModel + '</div>' +
            '<div class="col-md-2 less-padding-left">' + json.Year + '</div>' +
            '</div>';
    };

    /* driver Select Format*/
    var driverSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-5">' + json.Identification + '</div>' +
            '<div class="col-md-7">' + json.Name + '</div>' +
            '</div>';
    };

    /* driver Selected Format*/
    var driverSelectedFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/'/g, "\""));
        return '<div class="row">' +
            //'<div class="col-md-1"><img style="width: 24px; height: 24px;" src="' + json.Photo + '"/></div>' +
            //'<div class="col-md-2">&nbsp;&nbsp;' + json.Code + '</div>' +
            '<div class="col-md-5">' + json.Identification + '</div>' +
            '<div class="col-md-7">' + json.Name + '</div>' +
            '</div>';
    };

    /*driver search*/
    var driverSearch = function (term, text) {

        var obj = null;
        try { obj = JSON.parse(text.replace(/\t/g, '').replace(/'/g, "\"")); } catch (e) { }
        if (obj === null) { return false; }
        text = obj.Name + ' ' + obj.Code + ' ' + obj.Identification;

        var terms = term.split(" ");
        for (var i = 0; i < terms.length; i++) {
            var tester = new RegExp("\\b" + terms[i], 'i');
            if (tester.test(text) == false) {
                return (text === 'Other');
            }
        }
        return true;
    }

    var alarmMaintenanceModalShow = function () {
        $('#alarmMaintenanceModal').on('shown.bs.modal', function (e) {

            $('.rangeValue').click(function () {
                var val = $(this).val();
                var id = $(this).attr('alarmtipoid');
                $('#demo_' + id).html(val);
            });

            $('.validateRepeat').click(function () {
                var id = $(this).attr('alarmtipoid');
                if ($('#Alarm_' + id).find('#txtAlarmPhone').val() == null ||
                    $('#Alarm_' + id).find('#txtAlarmPhone').val() == '' ||
                    $('#Alarm_' + id).find('#txtAlarmPhone').val() == '0000-0000') {
                    $(this).removeAttr('checked');
                    setAmountErrorMsj('El servicio para el envío de mensajes recurrentes, es por medio de SMS, por lo cual se debe indicar un número de celular para su activación.', true, 'e2');

                } else {
                    setAmountErrorMsj('', true, 'e2');
                    if ($(this).is(":checked")) {
                        $('#rngAlarmRepeatTimes_' + id).removeAttr('disabled');
                        $('#txtAlarmIntervalRepeat_' + id).removeAttr('disabled');
                    } else {
                        $('#rngAlarmRepeatTimes_' + id).attr('disabled', 'disabled');
                        $('#txtAlarmIntervalRepeat_' + id).attr('disabled', 'disabled');
                    }
                }
            });

            $('#btnSendAlarm').click(function () {
                var message = '';
                var error = false;
                var ids = $(this).attr('allIds');
                var items = ids.substring(1, ids.length - 1).split(",")
                var list = [];
                if (ids != null) {
                    for (var i = 0; i < items.length; i++) {
                        message += ValidateInputs(items[i]);
                        if (message != '') {
                            error = true;
                        }
                        list.push(GetValues(items[i]));
                    }
                    setAmountErrorMsj(message, true, 'e2');
                    if (!error) {
                        AddOrEditAlarmMaintenance(list);
                    }
                }
            });

        }).modal('show');

        $('#btnCloseAlarm').click(function () {
            alarmMaintenanceModalClose();
        });

        $('#btnCancelAlarm').click(function () {
            alarmMaintenanceModalClose();
        });
    };

    var ValidateInputs = function (alarmTriggerId) {
        var message = '';

        if (!$('#Alarm_' + alarmTriggerId).find('#chkAlarmActive').is(":checked")) {
            if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val() == null || $('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val() == '') {
                if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() == null || $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() == '') {

                    return message;

                } else {
                    if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val(), false)) {
                        message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888<br/>';
                    }

                    if (alarmTriggerId == 510) {
                        if (!$('#Alarm_' + alarmTriggerId).find('#rdbAlarmLawSpeed').is(":checked") && !$('#Alarm_' + alarmTriggerId).find('#rdlAlarmCompanySpeed').is(":checked")) {
                            message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Debe seleccionar el tipo de alarma para el exceso de velocidad. <br/>';
                        }
                    }

                    if (alarmTriggerId == 508) {
                        if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmMaxStopTime').val() == null || $('#Alarm_' + alarmTriggerId).find('#txtAlarmMaxStopTime').val() == '') {
                            message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Debe indicar el tiempo máximo de parada. <br/>';
                        }
                    }
                }

            } else {
                if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val(), true)) {
                    message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El correo debe cumplir el siguiente formato: ejemplo1@dominio.com. <br/>';
                }

                if (alarmTriggerId == 510) {
                    if (!$('#Alarm_' + alarmTriggerId).find('#rdbAlarmLawSpeed').is(":checked") && !$('#Alarm_' + alarmTriggerId).find('#rdlAlarmCompanySpeed').is(":checked")) {
                        message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Debe seleccionar el tipo de alarma para el exceso de velocidad. <br/>';
                    }
                }

                if (alarmTriggerId == 508) {
                    if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmMaxStopTime').val() == null || $('#Alarm_' + alarmTriggerId).find('#txtAlarmMaxStopTime').val() == '') {
                        message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Debe indicar el tiempo máximo de parada. <br/>';
                    }
                }
            }

        } else {
            if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val() != '') {
                if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val(), true)) {
                    message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El correo debe cumplir el siguiente formato: ejemplo1@dominio.com. <br/>';
                }

                if (alarmTriggerId == 510) {
                    if (!$('#Alarm_' + alarmTriggerId).find('#rdbAlarmLawSpeed').is(":checked") && !$('#Alarm_' + alarmTriggerId).find('#rdlAlarmCompanySpeed').is(":checked")) {
                        message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Debe seleccionar el tipo de alarma para el exceso de velocidad. <br/>';
                    }
                }

                if (alarmTriggerId == 508) {
                    if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmMaxStopTime').val() == null || $('#Alarm_' + alarmTriggerId).find('#txtAlarmMaxStopTime').val() == '') {
                        message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Debe indicar el tiempo máximo de parada. <br/>';
                    }
                }

            } else {
                if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val() != '') {
                    if (!verificarFormato($('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val(), false)) {
                        message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888<br/>';
                    }

                    if (alarmTriggerId == 510) {
                        if (!$('#Alarm_' + alarmTriggerId).find('#rdbAlarmLawSpeed').is(":checked") && !$('#Alarm_' + alarmTriggerId).find('#rdlAlarmCompanySpeed').is(":checked")) {
                            message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Debe seleccionar el tipo de alarma para el exceso de velocidad. <br/>';
                        }
                    }

                    if (alarmTriggerId == 508) {
                        if ($('#Alarm_' + alarmTriggerId).find('#txtAlarmMaxStopTime').val() == null || $('#Alarm_' + alarmTriggerId).find('#txtAlarmMaxStopTime').val() == '') {
                            message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Debe indicar el tiempo máximo de parada. <br/>';
                        }
                    }
                } else {
                    message += '* ' + $('#Tab_' + alarmTriggerId).prop('innerText') + '- Verifique que agregó al menos un correo o un número de celular. <br/>';
                }
            }


        }

        return message;
    };

    var verificarFormato = function (data, email) {
        var result = false;
        var list = data.replace(" ", "").split(";");
        var regex = email ? /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/ : /^([0-9\+\s\+\-])+$/;

        for (var item = 0; item < list.length; item++) {
            if (regex.test(list[item])) {
                result = true;
            }
            else {
                result = false;
                break;
            }
        }
        return result;
    }

    /* Show or hide errors messages*/
    var setAmountErrorMsj = function (msj, show, typeError) {

        typeError = typeError != null ? typeError : 'e1';

        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').removeClass('hide');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').addClass('hide');
                }
                break;
            default:
                break;
        }


    }

    var GetValues = function (alarmTriggerId) {
        var alarma = {};
        alarma.AlarmId = $('#Alarm_' + alarmTriggerId).attr('alarmid');
        alarma.AlarmTriggerId = alarmTriggerId;
        alarma.EntityId = $('#id').val();
        alarma.Email = $('#Alarm_' + alarmTriggerId).find('#txtAlarmEmail').val();
        alarma.Phone = $('#Alarm_' + alarmTriggerId).find('#txtAlarmPhone').val();
        alarma.RepeatTimes = $('#Alarm_' + alarmTriggerId).find('#rngAlarmRepeatTimes_' + alarmTriggerId).val();
        alarma.IntervalRepeat = $('#Alarm_' + alarmTriggerId).find('#txtAlarmIntervalRepeat_' + alarmTriggerId).val();
        alarma.MaxTime = $('#Alarm_' + alarmTriggerId).find('#txtAlarmMaxStopTime').val();
        alarma.Active = ($('#Alarm_' + alarmTriggerId).find('#chkAlarmActive').is(":checked")) ? true : false;

        if ($('#Alarm_' + alarmTriggerId).find('#rdbAlarmLawSpeed').is(":checked"))
            alarma.TypeSpeed = "1";
        if ($('#Alarm_' + alarmTriggerId).find('#rdlAlarmCompanySpeed').is(":checked"))
            alarma.TypeSpeed = "2";

        if ((alarma.Email == null || alarma.Email == "") && (alarma.Phone == null || alarma.Phone == "")) {
            alarma.Active = false;
        }

        if (!$('#Alarm_' + alarmTriggerId).find('#chkAlarmRepeat').is(":checked") || (alarma.Phone == null || alarma.Phone == "")) {
            alarma.RepeatTimes = 0;
            alarma.IntervalRepeat = 0;
        }

        return alarma;
    }

    var AddOrEditAlarmMaintenance = function (listAlarm) {
        //$('body').loader('show');
        $.ajax({
            url: '/Administration/AlarmMaintenance/AddOrEditAlarmMaintenance',
            type: 'POST',
            data: JSON.stringify({ pListAlarm: listAlarm }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                alarmMaintenanceModalClose();
            }
        });
    };

    var alarmMaintenanceModalClose = function () {
        $('#alarmMaintenanceModal').on('hide.bs.modal', function (e) {
            $('#btnAddAlarm').removeAttr('disabled');
        }).modal('hide');
    };

    var txtCommentClear = function () {
        $('#txtCommentBinnacle').val('');
    }


    var onClickDownloadVehicles = function () {

        $('#ExcelReportDownloadForm').submit();
    };


    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadExcelForm').submit();
    };

    function customerCreditCardValidate() {
        $.ajax({
            url: '/Administration/Vehicles/CustomerCreditCardValidate',
            type: 'POST',
            data: '',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.MessegeType !== "success") {
                    swal("Info de Flotas", data.Messege, data.MessegeType);
                }
                else {
                    ECOsystem.Utilities.ShowLoader();
                    $('#loadForm').find('#id').val('-1');
                    $('#loadForm').submit();
                }
            }
        });
    }


    /* Public methods */
    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        OnSuccessReferenceLoad: onSuccessReferenceLoad,
        ReferenceEditModalClose: referenceEditModalClose,
        AlarmMaintenanceModalShow: alarmMaintenanceModalShow,
        AlarmMaintenanceModalClose: alarmMaintenanceModalClose,
        TxtCommentClear: txtCommentClear,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnClickDownloadVehicles: onClickDownloadVehicles
    };
})();

