﻿var ECO_Admin = ECO_Admin || {};
 ECOsystem.Areas.Admin.DallasKeys = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);

        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            $('#loadForm').find('#id').val('-1');
            $('#loadForm').submit();
        });

    };

    var addOrEditModalShow = function () {
        $('#addOrEditModal').modal('show');
    };

    /* Public methods */
    return {
        AddOrEditModalShow: addOrEditModalShow,
        Init: initialize
    };
})();

