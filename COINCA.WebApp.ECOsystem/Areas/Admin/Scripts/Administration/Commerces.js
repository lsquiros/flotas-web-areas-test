﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.Commerces = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);
     
         $('#page-content-wrapper').off('click', '#btnAdd').on('click', '#btnAdd', function () {
            $('#loadForm').find('#id').val('');
            $('#loadForm').submit();
        });

        $('#page-content-wrapper').off('click', '#btnGetDownloadData').on('click', '#btnGetDownloadData', function (e) {
            $('#GetDataToDownloadForm').submit();
            e.preventDefault();
        });

        $('#page-content-wrapper').off('click', '#btnCancel').on('click', '#btnCancel', function (e) {
            oncanceledit();
        });

        $('#page-content-wrapper').off('click', '#txtSearch').on('click', '#txtSearch', function (e) {
            if (e.which == 13) {                
                $('#searchForm').submit();
            }
        });

        $('#gridContainer').off('click.edit', 'a[edit_row_commerce]').on('click.edit', 'a[edit_row_commerce]', function (e) {
            var val = $(this).attr('id');
            $('#loadForm').find('#id').val(val);
            $('#currentId').val(val);
            $('#loadForm').submit();            
        });

        $('#gridContainer').off('click.delete', 'a[delete_row_commerce]').on('click.delete', 'a[delete_row_commerce]', function (e) {
            var val = $(this).attr('id');
            $('#deleteForm').find('#id').val(val);
            var managementModule = $('#managementModule').val();

            if (managementModule == 'True') {
                $('#deleteModal').find('#warningAgenda').show();
            }
            
            $('#deleteModal').modal('show');
        });

        $(window).on('beforeunload', function () {
            $(window).scrollTop(0);
        });
    }

    var onSuccessLoad = function () {
        if ($('#reportFilterContainer').css('display') != 'none') {
            $('#reportFilterContainer').hide();
        }
        $('#detailContainer').show('slow');
        $('#btnAdd').attr('disabled','disabled');
        initialize();
    }

    var oncanceledit = function () {
        $('#detailContainer').hide('slow');
        $('#btnAdd').removeAttr('disabled');
    };

    var onSuccessAddOrEdit = function (data) {
        hideLoader();
        $('#detailContainer').hide('slow');

        if (data.indexOf("<strong> Mensaje técnico:") == -1)
        {
            $('#btnAdd').removeAttr('disabled');
            
            ECOsystem.Utilities.setConfirmation();

            var val = $('#currentId').val();
            $('#currentId').val('');

            if (val != "" || val != undefined || val != null){
                $('#' + val).closest('tr').addClass('selected');
                var target = $('#' + val).offset().top;

                if ((target - $('#detailContainer').height()) > $(window).height()) {
                    window.scrollTo(0, target - 100);
                }
            }
        }
    }

    var onSuccessReportData = function (data, status) {
        hideLoader();
        if (data == "Success") {
            $('#DonwloadExcelForm').submit();
        } 
        if (status == "nocontent") {
            ECOsystem.Utilities.SetMessageShow("No se han creado comercios para exportar.", "ERROR");
        }
    }

    var onFailureReportData = function (data) {
        ECOsystem.Utilities.HideLoader();
    }

    var successSearch = function () {
        $('#txtSearch').val('');
    };    

    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');      
    };

    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        OnSuccessAddOrEdit: onSuccessAddOrEdit,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        SuccessSearch: successSearch,
        OnSuccessReportData: onSuccessReportData,
        OnFailureReportData: onFailureReportData
    };
})();