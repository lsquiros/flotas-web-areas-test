﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.CustomerGeneralParameters = (function () {
    var options = {};
    var initialize = function (opts) {
        $.extend(options, opts);

        $('.timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 60,
            minTime: '0',
            maxTime: '11:00pm',
            startTime: '1:00',
            //dynamic: false,
            dropdown: true,
            scrollbar: true
        });

        $('#ActiveRadioDistance').click(function () {
            $("#RadioDistance").prop('disabled', !$(this).prop('checked'));
        });

        $('#btnSaveVehicleSchedule').click(function () {
            saveVehicleSchedule();
        });

        $('#btnSaveEmailPanicBotton').click(function () {
            saveEmailPanicBotton();
        });

        $('#btnSaveInProcessTransactions').click(function () {
            saveInProcessTransactions();
        });

        $('#btnSaveApprovalTransactiosImport').click(function () {
            SaveApprovalTransactiosImport();
        });

        $('#btnCostCenterByDriver').click(function () {
            SaveCostCenterByDriver();
        });

        $('#btnSaveTypeOdometerbyCustomer').click(function () {
            SaveSaveTypeOdometerbyCustomer();
        });

        $('#btnAlarmTemperatureDelayAddOrEdit').click(function () {
            alarmTemperatureDelayAddOrEdit();
        });

        $('#btnMinimumStopTimeAddOrEdit').click(function () {
            minimumStopTimeAddOrEdit();
        });

        $('#btnRadioDistanceAddOrEdit').click(function () {
            radioDistanceAddOrEdit();
        });

        //NO eliminar, eventualmente se utilizara para un mantenimiento de alarmas.
        //$('#btnAlarmNonTransmissionGPS').click(function () {
        //    AddOrEditAlarmNonTransmissionGPS();
        //});

        var alarmTriggerId = $(document).find('#divGeneralAlarm').attr('alarmTriggerId');
        $('#rngAlarmRepeatTimes_' + alarmTriggerId).click(function () {
            var val = $(this).val();
            var id = $(this).attr('alarmtipoid');
            $('#demo_' + id).html(val);
        });

        $('#chkAlarmRepeat').click(function () {
            var id = $(this).attr('alarmtipoid');
            if ($('#txtAlarmPhone').val() == null ||
                $('#txtAlarmPhone').val() == '' ||
                $('#txtAlarmPhone').val() == '0000-0000') {
                $(this).removeAttr('checked');
                setAmountErrorMsj('* El servicio para el envío de mensajes recurrentes, es por medio de SMS, por lo cual se debe indicar un número de celular para su activación.', true, 'e2');
                //$('#ControlsContainerPanicBotton').html('<br />' +
                //    '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                //    '* El servicio para el envío de mensajes recurrentes, es por medio de SMS, por lo cual se debe indicar un número de celular para su activación. </div>');
            } else {
                setAmountErrorMsj('', true, 'e2');
                if ($(this).is(":checked")) {
                    $('#rngAlarmRepeatTimes_' + id).removeAttr('disabled');
                    $('#txtAlarmIntervalRepeat_' + id).removeAttr('disabled');
                } else {
                    $('#rngAlarmRepeatTimes_' + id).attr('disabled', 'disabled');
                    $('#txtAlarmIntervalRepeat_' + id).attr('disabled', 'disabled');
                }
            }
        });
        

                
    };

    var minimumStopTimeAddOrEdit = function () {
        var minimumStopTime = $('#MinimumStopTime').val();
        $('body').loader('show');
        $('#ControlsContainer').html('');
        $.ajax({
            type: 'POST',
            url: '/CustomerGeneralParameters/MinimumStopTimeAddOrEdit',
            data: JSON.stringify({ MinimumStopTime: minimumStopTime }),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $('body').loader('hide');
                if (data == "Success") {
                    $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Datos almacenados correctamente. </div>');
                } else {
                    $('#ControlsContainerPanicBotton').html('<br />' +
                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
                        '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                }
            }
        });
    }

    var alarmTemperatureDelayAddOrEdit = function () {
        var alarmTemperatureDelayMin = $('#AlarmTemperatureDelay').val();
        alarmTemperatureDelayMin = (alarmTemperatureDelayMin == "") ? 0 : alarmTemperatureDelayMin;

        $('body').loader('show');

        $('#ControlsContainer').html('');
        $.ajax({
            type: 'POST',
            url: '/CustomerGeneralParameters/AlarmTemperatureDelayAddOrEdit',
            data: JSON.stringify({ AlarmTemperatureDelay: alarmTemperatureDelayMin }),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $('body').loader('hide');
                if (data == "Success") {
                    $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Datos almacenados correctamente. </div>');
                } else {
                    $('#ControlsContainerPanicBotton').html('<br />' +
                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
                        '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                }
            }
        });
    }

    var radioDistanceAddOrEdit = function () {
        
        var radioDistance = $('#RadioDistance').val();
        var activeRadioDistance = $('#ActiveRadioDistance').prop("checked");;
        $('body').loader('show');
        $('#ControlsContainer').html('');
        $.ajax({
            type: 'POST',
            url: '/CustomerGeneralParameters/RadioDistanceAddOrEdit',
            data: JSON.stringify({ RadioDistance: radioDistance, ActiveRadioDistance: activeRadioDistance }),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $('body').loader('hide');
                if (data == "Success") {
                    $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Datos almacenados correctamente. </div>');
                } else {
                    $('#ControlsContainerPanicBotton').html('<br />' +
                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
                        '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                }
                if (ActiveRadioDistance == false) {
                    $("#RadioDistance").prop('disabled', true);
                    $("#btnRadioDistanceAddOrEdit").prop('disabled', true);
                }
            }
        });
    }

    var saveParameters = function (data) {
        $('body').loader('hide');
        if (data == "Success") {
            $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                'Datos almacenados correctamente. </div>');
        } else {
            $('#ControlsContainerPanicBotton').html('<br />' +
                '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
                '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
        }
    }

    var SaveSaveTypeOdometerbyCustomer = function () {
        var typeOdometer = $('#TypeOdometer').prop("checked");
        $('body').loader('show');
        $('#ControlsContainer').html('');
        $.ajax({
            type: 'POST',
            url: '/CustomerGeneralParameters/SaveTypeOdometerbyCustomer',
            data: JSON.stringify({ TypeOdometer: typeOdometer }),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $('body').loader('hide');
                if (data == "Success") {
                    $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Datos almacenados correctamente. </div>');
                } else {
                    $('#ControlsContainerPanicBotton').html('<br />' +
                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
                        '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                }
            }
        });
    }

    var SaveCostCenterByDriver = function () {
        var ActivateProcess = $('#CostCenterByDriver').prop("checked");
        $('body').loader('show');
        $('#ControlsContainer').html('');
        $.ajax({
            type: 'POST',
            url: '/CustomerGeneralParameters/SaveCostCenterByDriver',
            data: JSON.stringify({ ActivateProcess: ActivateProcess }),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $('body').loader('hide');
                if (data == "Success") {
                    $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Datos almacenados correctamente. </div>');
                } else {
                    $('#ControlsContainerPanicBotton').html('<br />' +
                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
                        '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                }
            }
        });
    }

    var SaveApprovalTransactiosImport = function () {
        var ActivateProcess = $('#AllowApprovalTransactiosImport').prop("checked");
        $('body').loader('show');
        $('#ControlsContainer').html('');
        $.ajax({
            type: 'POST',
            url: '/CustomerGeneralParameters/SaveApprovalTransactiosImport',
            data: JSON.stringify({ ActivateProcess: ActivateProcess }),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $('body').loader('hide');
                if (data == "Success") {
                    $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Datos almacenados correctamente. </div>');
                } else {
                    $('#ControlsContainerPanicBotton').html('<br />' +
                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
                        '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                }
            }
        });
    }

    var saveInProcessTransactions = function () {
        var AllowProcess = $('#AllowInProcessCard').prop("checked");
        $('body').loader('show');
        $('#ControlsContainer').html('');
        $.ajax({
            type: 'POST',
            url: '/CustomerGeneralParameters/SaveInProcessTransactions',
            data: JSON.stringify({ AllowProcess: AllowProcess }),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $('body').loader('hide');
                if (data == "Success") {
                    $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Datos almacenados correctamente. </div>');
                } else {
                    $('#ControlsContainerPanicBotton').html('<br />' +
                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
                        '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                }
            }
        });
    }

    var saveVehicleSchedule = function () {
        var startTime = $('#StartTime').val();
        var endTime = $('#EndTime').val();

        $('#ControlsContainerPanicBotton').html('');

        $('body').loader('show');

        $.ajax({
            type: 'POST',
            url: '/CustomerGeneralParameters/VehicleScheduleAddOrEdit',
            data: JSON.stringify({ StartTime: startTime, EndTime: endTime }),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $('body').loader('hide');
                if (data == "Success") {
                    $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Datos almacenados correctamente. </div>');
                } else {

                    $('#ControlsContainer').html('<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
                        '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                }
            }
        });
    };

    var saveEmailPanicBotton = function () {
        var alarmId = $('#divGeneralAlarm').attr('alarmId');
        var alarmTriggerId = $('#divGeneralAlarm').attr('alarmTriggerId');
        var message = ValidateInputs();
        if (message == '') {
            setAmountErrorMsj('', true, 'e2');
            AddOrEditAlarmGeneral(GetValues(alarmId, alarmTriggerId));
        } else {
            setAmountErrorMsj(message, true, 'e2');
        }
    };

    var validateInputValues = function () {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var emails = $('#Email').val();
        var emailList = emails.replace(" ", "").split(';');

        for (i = 0; i < emailList.length; i++) {
            if (!re.test(emailList[i].trim())) {
                return false;
            }
        }
        return true;
    };

    var ValidateInputs = function () {
        var message = '';
        if ($('#txtAlarmEmail').val() == null ||
            $('#txtAlarmEmail').val() == '') {

            if ($('#txtAlarmPhone').val() == null ||
                $('#txtAlarmPhone').val() == '') {
                //$('#ControlsContainerPanicBotton').html('<br />' +
                //    '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                //    '* Debe indicar los correos o los números de celular a los cuales desea notificar. </div>');
                message += '* Debe indicar los correos o los números de celular a los cuales desea notificar. <br/>';
            }
            else if (!verificarFormato($('#txtAlarmPhone').val(), false)) {
                //$('#ControlsContainerPanicBotton').html('<br />' +
                //    '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                //    '* El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888 </div>');
                message += '* El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888<br/>';
            }
        }
        else if (!verificarFormato($('#txtAlarmEmail').val(), true)) {
                //$('#ControlsContainerPanicBotton').html('<br />' +
                //    '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                //    '* El correo debe cumplir el siguiente formato: ejemplo1@dominio.com </div>');
            message += '* El correo debe cumplir el siguiente formato: ejemplo1@dominio.com. <br/>';
        } else {
            if ($('#txtAlarmPhone').val() != undefined &&
                $('#txtAlarmPhone').val() != null &&
                $('#txtAlarmPhone').val() != '') {
                if (!verificarFormato($('#txtAlarmPhone').val(), false)) {
                    //$('#ControlsContainerPanicBotton').html('<br />' +
                    //    '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                    //    '* El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888 </div>');
                    message += '* El número de teléfono puede contener únicamente números y guiones. Ej: 8888-8888<br/>';
                }
            }
        }
        return message;
    };

    var verificarFormato = function (data, email) {
        var result = false;
        var list = data.split(";");
        var regex = email ? /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/ : /^([0-9\+\s\+\-])+$/;

        for (var item = 0; item < list.length; item++) {
            if (regex.test(list[item].trim())) {
                result = true;
            }
            else {
                result = false;
                break;
            }
        }
        return result;
    }

    /* Show or hide errors messages*/
    var setAmountErrorMsj = function (msj, show, typeError) {

        typeError = typeError != null ? typeError : 'e1';

        switch (typeError) {
            case 'e1':
                if (show) {
                    $('#errorAmountMsj').html(msj);
                    $('#validationAmountError').removeClass('hide');
                } else {
                    $('#errorAmountMsj').html('');
                    $('#validationAmountError').addClass('hide');
                }
                break;
            case 'e2':
                if (show) {
                    $('#errorAmountMsj2').html(msj);
                    $('#validationAmountError2').removeClass('hide');
                } else {
                    $('#errorAmountMsj2').html('');
                    $('#validationAmountError2').addClass('hide');
                }
                break;
            default:
                break;
        }


    }

    var GetValues = function (alarmId, alarmTriggerId) {
        var alarma = {};
        alarma.AlarmId = alarmId;
        alarma.AlarmTriggerId = alarmTriggerId;
        alarma.EntityId = 0;
        alarma.Email = $('#txtAlarmEmail').val();
        alarma.Phone = $('#txtAlarmPhone').val();
        alarma.RepeatTimes = $('#rngAlarmRepeatTimes_' + alarmTriggerId).val();
        alarma.IntervalRepeat = $('#txtAlarmIntervalRepeat_' + alarmTriggerId).val();
        alarma.Active = true; // ($('#chkAlarmActive').is(":checked")) ? true : false;

        if ((alarma.Email == null || alarma.Email == "") && (alarma.Phone == null || alarma.Phone == "")) {
            alarma.Active = false;
        }

        if (!$('#chkAlarmRepeat').is(":checked")) {
            alarma.RepeatTimes = 0;
            alarma.IntervalRepeat = 0;
        }

        return alarma;
    }

    var AddOrEditAlarmGeneral = function (Alarm) {
        $('body').loader('show');
        $.ajax({
            url: '/AlarmMaintenance/GeneralAlarmAddOrEdit',
            type: 'POST',
            data: JSON.stringify({ alarm: Alarm }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                $('body').loader('hide');
                if (data == "Error") {
                    setAmountErrorMsj('Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' + '<strong> Mensaje técnico: </strong> <br />' + data, true, 'e2');

                    $('#ControlsContainerPanicBotton').html('<br />' +
                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
                        '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
                    
                } else {
                    //Alarm.AlarmId = data;
                    $('#divGeneralAlarm').attr('alarmId', 0);
                    $('#ControlsContainerPanicBotton').html('<br />' +
                        '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                        'Datos almacenados correctamente. </div>');
                }
            }
        });
    };

    //NO eliminar, eventualmente se utilizara para un mantenimiento de alarmas.
    //var AddOrEditAlarmNonTransmissionGPS = function () {
    //    var ActivateAlarm = $('#AlarmNonTransmissionGPS').prop("checked");
    //    $('body').loader('show');
    //    $('#ControlsContainer').html('');
    //    $.ajax({
    //        url: '/CustomerGeneralParameters/AlarmNonTransmissionGPSAddOrEdit',
    //        type: 'POST',
    //        data: JSON.stringify({ activateAlarm: ActivateAlarm }),
    //        dataType: 'json',
    //        contentType: 'application/json',
    //        success: function (data) {
    //            $('body').loader('hide');
    //            if (data == "Success") {
    //                $('#ControlsContainerPanicBotton').html('<br />' +
    //                    '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
    //                    'Datos almacenados correctamente. </div>');
    //            } else {
    //                $('#ControlsContainerPanicBotton').html('<br />' +
    //                    '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
    //                    'Ocurrió un error al almacenar los datos, por favor intentelo de nuevo. <hr />' +
    //                    '<strong> Mensaje técnico: </strong> <br />' + data + '</div>');
    //            }
    //        }
    //    });
    //};

    return {
        Init: initialize,
        SaveParameters: saveParameters
    };
})();

