﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.ConsolidateExpensesReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        try {
            initializeDropDownList();
        }
        catch (exception) {
        }
    }

    /*init Events*/
    var initEvents = function () {

            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
        });

            $("#Parameters_StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });


            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $("#Parameters_EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });
    }

    /*format Label*/
    var formatLabel = function (v) {
        return ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToFloat(v)) + "%";
    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#Parameters_ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        try {
            var select5 = $("#Parameters_CostCenterId").select2().data('select2');
            select5.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectCostCenterIdChange(data); return fn.apply(this, arguments); }
                }
            })(select5.onSelect);
        } catch (e) {

        }

    };

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Parameters_Month").select2('val', '');
            $("#Parameters_Year").select2('val', '');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');

            $('#Parameters_ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
        }
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Month').val(obj.id);
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Year').val(obj.id);
        }
    };

    /*on Select Report Fuel Type Change*/
    var onSelectCostCenterIdChange = function (obj) {
        $('#Parameters_CostCenterId').val(obj.id);
    };

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
            '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
            ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' +
            'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        if (($('#Parameters_ReportCriteriaId').val() === '')
            || ($('#Parameters_Month').val() === '')
            || ($('#Parameters_Year').val() === '')
            || ($('#Parameters_CostCenterId').val() === '')
            || ($('#Parameters_StartDateStr').val() === '')
            || ($('#Parameters_EndDateStr').val() === '')
        ) {
            if (($('#Parameters_Month').val() !== '') && ($('#Parameters_Year').val() !== ''))
                return true;
            if (($('#Parameters_StartDateStr').val() !== '') && ($('#Parameters_EndDateStr').val() !== ''))
                return true;
            invalidSelectOption();
            return false;
        }
        return true;
    };

    /*Excecute the excel download*/
    var onClickDownloadReport = function () {
        if (checkAllSelects()) $('#ExcelReportDownloadForm').submit();
    };

    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DownloadFileForm').submit();
    };

    var onClickDownloadSAPReport = function () {
        $('#SAPReportDownloadForm').submit();
    };

    var hideLoaderSAP = function () {
        $('body').loader('hide');
        $('#DonwloadFileFormSAP').submit();
    };

    /* Public methods */
    return {
        Init: initialize,
        FormatLabel: formatLabel,
        OnClickDownloadReport: onClickDownloadReport,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        HideLoaderSAP: hideLoaderSAP
    };
})();

