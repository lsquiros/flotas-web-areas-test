﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.CustomerToken = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        getCurrentToken();

        $('#btnGenerateAPIToken').click(function () {
            $('body').loader('show');
            $('#confirmGenerateTokenPartner').modal('hide');
            $('#errorMessage').hide();
            generateNewToken();
        });

        $('#APIToken').dblclick(function () {
            $(this).select();
            document.execCommand("copy");            
        });
    }

    var getCurrentToken = function () {
        $('body').loader('show');
        $.ajax({
            url: '/CustomerToken/CustomerTokenRetrieve',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                $('body').loader('hide');
                if (data.token) {
                    $('#APITokenToolTip').tooltip('enable');
                    $('#APIToken').val(data.token);
                } else if (data.error) {
                    showError(data.error);
                } else {
                    $('#APITokenToolTip').tooltip('disable');
                    $('#APIToken').val('No existe información que mostrar');
                }
            }
        });
    }

    var generateNewToken = function () {
        $.ajax({
            url: '/CustomerToken/CustomerTokenAddOrEdit',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                $('body').loader('hide');
                if (data.token) {
                    $('#APITokenToolTip').tooltip('enable');
                    $('#APIToken').val(data.token);
                    ECOsystem.Utilities.setConfirmation();
                } else if (data.error) {
                    showError(data.error);
                }
            }
        });
    }

    var showError = function (error) {
        $('#errorMessage').show();
        $('#errorText').html('Se presentó un error en el proceso. <br /> Detalle técnico: ' + error);
    }
    return {
        Init: initialize
    };
})();