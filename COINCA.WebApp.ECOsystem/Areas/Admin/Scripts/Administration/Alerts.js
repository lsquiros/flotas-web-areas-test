﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.Alerts = (function () {
    var options = {};
    var jsonDataArray = [];

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initToggle();       
    };

    var initToggle = function () {       
        $('.toggleCheck').bootstrapToggle({
            on: 'Activo',
            off: 'Inactivo'
        });
    };

    var ruleGetData = function (obj) {        
        var alarmId = obj.attr("alarmid");
        var isActive = obj.prop('checked');
        var hasparameter = obj.attr("hasparameter");
        var parameterValue = $('#' + alarmId + '_ActiveText').val();
        
        var data = {
            "AlarmId": alarmId,
            "IsActive": isActive,
            "HasParameter": hasparameter,
            "ParameterValue": parameterValue            
        }

        jsonDataArray.push(data);        
    };

    $('#btnErrorDismiss').click(function () {
        window.location.reload();
    });

    $("#btnAlertsAddOrEdit").click(function () {
        var validates = false;
        var count = 0;

        //Clean the array
        jsonDataArray = [];
        $('.toggleCheck').each(function () {
            ruleGetData($(this));
        });

        //Validates if the parameter value is not empty
        $(jsonDataArray).each(function () {
            
            if (jsonDataArray[count].IsActive) {
                if(jsonDataArray[count].HasParameter == 1){
                    if(jsonDataArray[count].ParameterValue == null || jsonDataArray[count].ParameterValue == ''){
                        validates = true;
                        $('#' + jsonDataArray[count].AlarmId + '_LabelText').removeClass("hidden");
                        return false;
                    }else{
                        validates = false;
                        $('#' + jsonDataArray[count].AlarmId + '_LabelText').addClass("hidden");
                    }
                }
            }            
            count++;
        });

        //data = JSON.stringify(jsonDataArray);

        if (!validates)
        {
            $('body').loader('show');

            var uri = '/Administracion/Alerts/AlertsAddOrEdit';
            IsJson = true;

            $.ajax({
                type: "POST",
                url: uri,
                data: JSON.stringify(jsonDataArray),
                success: function (data) {
                    if (data == '1') {
                        $('#confirmAlertsModal').modal('hide');
                        window.location.reload();
                    } else {
                        $('body').loader('hide');
                        $('#confirmAlertsModal').modal('hide');
                        $('#ErrorAlertsModal').modal('show');
                        console.error("Internal Error - Alerts/AlersAddOrEdit");
                    }
                },
                dataType: "json",
                contentType: "application/json",
                async: false
            });
        }
        else
        {
            $('#confirmAlertsModal').modal('hide');
        }
    });
    
    $('input[type=checkbox]').change(function () {
         var alarmId = $(this).attr("alarmid");
            
        if ($(this).prop('checked')) {
            $('#' + alarmId + '_DivText').removeClass('hidden');
        }
        else
        {
            $('#' + alarmId + '_DivText').addClass('hidden');
        }
     });

    /* Public methods */
    return {
        Init: initialize
    };
})();


