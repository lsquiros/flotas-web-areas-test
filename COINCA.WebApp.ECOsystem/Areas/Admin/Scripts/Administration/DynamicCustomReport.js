﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.DynamicCustomReport = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);
        InitDatePickers();
    }    

    var InitDatePickers = function () {
        $('#datetimepickerStartDate').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (e) {
            $('#datetimepickerEndDate').datepicker('remove');

            var dateV = $('#Parameters_StartDateStr').val();
            var params = dateV.split("/");
            var days = parseInt(params[0]) + 6;
            var maxDateV = days + "/" + params[1] + "/" + params[2];

            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                //minDate: dateV,
                startDate: dateV,
                //endDate: maxDateV,
                format: 'dd/mm/yyyy',
                autoclose: true
            });
            $("#datetimepickerEndDate").datepicker('update');
            $('#Parameters_EndDateStr').val();
        });

        $("#Parameters_StartDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        $("#Parameters_EndDateStr").alphanum({
            allowNumeric: true,
            allowUpper: false,
            allowLower: false,
            allowCaseless: true,
            allowSpace: false,
            allow: '/',
            maxLength: 10
        });

        initDatePickerTime();
    }

    var initDatePickerTime = function () {
        $('#datetimepickerStartDate').datepicker("setDate", new Date());
        $('#datetimepickerEndDate').datepicker("setDate", new Date());
    }

    //Control Methods
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function (data) {
        $('body').loader('hide');
        if (data == 'Success') {
            $('#ControlsContainer').html('<div class="alert alert-success alert-dismissible fade in" role="alert" >' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Seleccione los datos necesarios para la descarga del Reporte. </div>');
            $('#DownloadFileForm').submit();
        }
        else if (data == 'NoDatos') {
            $('#ControlsContainer').html('<div class="alert alert-info alert-dismissible fade in" role="alert" >' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;No se encontraron datos para las fechas seleccionadas. </div>');
        }
        else {
            $('#ControlsContainer').html('<div class="alert alert-danger alert-dismissible fade in" role="alert" >' +
                '<i class="mdi mdi-information" style="font-size:18px;"></i>&nbsp;Error al generar el reporte. <br/> <b>Detalle técnico: </b>' + data +
                '</div>');
        }
    };

    return {
        Init: initialize,
        ShowLoader: showLoader,
        HideLoader: hideLoader
    };
})();




