﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.ScoreParameters = (function () {
    var options = {};
    var lastId;

    var initialize = function (opts) {
        $.extend(options, opts);

        $('#btnAddDetail').click(function () {
            addDetail();
        });

        $('#btnAddScoreParameters').click(function () {
            saveScoreParameters();
        });
        
        $('#GPSPercentage').on('blur', function (event) {
            var id = $(this).attr('id');
            var val = $(this).val();

            if (val > 100) {
                $('#msjval').html('El valor no puede ser mayor a 100');
                $('#' + id).focus();
            } else {
                $('#speedpercentages').html(100 - parseInt(val));
            }

        });
    };

    var initKeyPress = function () {
        $('.detailtext').keypress(function (event) {
            return isNumber(event);
        });

        $('.lasttext').on('blur', function (event) {
            var id = $(this).attr('id');

            checkInsertValues(id, $(this).val(), event);

            if ($('#txtTo_' + id.split('_')[1]).val() === '' || $('#txtFrom_' + id.split('_')[1]).val() === '') return false;

            if ($('#msjval').html() !== '') return false;

            validatesInputs(id.split('_')[1], event);
        });

        $('.fromText').on('blur', function (event) {
            var id = $(this).attr('id');

            checkInsertValues(id, $(this).val(), event);

            if ($('#txtTo_' + id.split('_')[1]).val() === '' || $('#txtPercentage_' + id.split('_')[1]).val() === '') return false;

            if ($('#msjval').html() !== '') return false;

            validatesInputs(id.split('_')[1], event);
        });

        $('.toText').on('blur', function (event) {
            var id = $(this).attr('id');

            checkInsertValues(id, $(this).val(), event);

            if ($('#txtFrom_' + id.split('_')[1]).val() === '' || $('#txtPercentage_' + id.split('_')[1]).val() === '') return false;

            if ($('#msjval').html() !== '') return false;
            
            validatesInputs(id.split('_')[1], event);
        });
    }

    var addDetail = function () {
        if (!validateDetailText()) {
            var id = getDetailId();
            lastId = id;
            $('#tblDetail tbody').append('<tr id="' + id + '">' +
                '<td>Desde:</td>' +
                '<td><input type="text" class="form-control detailtext fromText" id="txtFrom_' + id + '" /></td>' +
                '<td>Hasta:</td>' +
                '<td><input type="text" class="form-control detailtext toText" id="txtTo_' + id + '"/></td>' +
                '<td>=</td>' +
                '<td><input type="text" class="form-control detailtext lasttext" id="txtPercentage_' + id + '"/></td>' +
                '<td>%</td>' +
                '<td  style="text-align:right;"><a href="#" class="delete-color" title="Eliminar"  onclick=" ECOsystem.Areas.Admin.ScoreParameters.DeleteRow(this)"><i class="mdi mdi-32px mdi-close-box"></i></a></td>' +
                '</tr>');
        }
        initKeyPress();
    }

    var getDetailId = function () {
        var res;
        $.ajax({
            url: '/ScoreParameters/GetDetailId',
            type: "POST",
            contentType: "application/json",
            async: false,
            success: function (data) {
                res = data;
            }
        });
        return res;
    }

    var validateDetailText = function () {
        var res = false;
        $('.detailtext').each(function () {
            if ($(this).val() === '') {
                res = true;
            }
        });
        return res;
    }

    var isNumber = function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ((charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }

    var validatesInputs = function (id, e) {
        e.preventDefault();

        var from = $('#txtFrom_' + id).val();
        var to = $('#txtTo_' + id).val();
        var Percentage = $('#txtPercentage_' + id).val();
        var isnew = lastId > id ? false : true;

        $.ajax({
            url: '/ScoreParameters/AddDetail',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ Id: id, From: from, To: to, Percentage: Percentage, IsNew: isnew }),
            async: false,
            success: function () { }
        });
    }

    var checkInsertValues = function (id, cval, e) {
        e.preventDefault();
        var msj = '';

        if (id.split('_')[0] === 'txtPercentage') {
            var Percentage = $('#' + id).val();
            if (Percentage > 100) {
                msj = 'El valor no puede ser mayor a 100';
                $('#' + id).focus();
            }
        } else {
            $('.fromText').each(function () {
                var val = $(this).attr("id");

                if (val.split('_')[1] !== id.split('_')[1]) {                    
                    var from = $('#txtFrom_' + val.split('_')[1]).val();
                    var to = $('#txtTo_' + val.split('_')[1]).val();

                    if (parseInt(from) === 0 && parseInt(cval) === 0) {
                        msj = 'El valor debe ser mayor a ' + to;
                        $('#' + id).focus();
                    } else if (parseInt(cval) > parseInt(from) && parseInt(cval) < parseInt(to)) {
                        msj = 'El valor debe ser menor a ' + from + ' o mayor a ' + to;
                        $('#' + id).focus();
                    }                    
                } else {
                    if (id.split('_')[0] === 'txtTo') {
                        if (parseInt($('#txtFrom_' + id.split('_')[1]).val()) > parseInt(cval)) {
                            msj = 'El valor no puede ser menor a ' + $('#txtFrom_' + id.split('_')[1]).val();
                            $('#' + id).focus();
                        }
                    }
                }
            });
        }
        $('#msjval').html(msj);
    }

    var checkPercentageSum = function ()   {
        var sum = 0
        $('.lasttext').each(function () {
            sum = sum + parseInt($(this).val());
        });
        if (sum >= 100) return false;

        return true;
    }

    var saveScoreParameters = function () {
        var GpsVal = $("#GPSPercentage").val();

        var ScoreParameters = {
            SpeedPercentage: 100 - parseInt(GpsVal),
            SpeedActive: $("#GPSActive").prop('checked'),
            GPSPercentage: GpsVal,
            GPSActive: $("#GPSActive").prop('checked'),
            List: GetGPSScoreParameters()
        };

        $('body').loader('show');
        $.ajax({
            url: '/ScoreParameters/ScoreParametersAddOrEdit',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ model: ScoreParameters }),
            async: false,
            success: function (data) {
                if (data === 'Success') {
                    ECOsystem.Utilities.setConfirmation();
                    window.location.reload();
                } else {
                    $('body').loader('hide');
                    $('#msjerror').html(data);
                    $('#ModalError').modal('show');
                }
            }
        });
    }

    var GetGPSScoreParameters = function () {
        list = [];

        $('#tblDetail tbody tr').each(function () {
            var id = $(this).attr('id');

            var data = {
                'From': $('#txtFrom_' + id).val(),
                'To': $('#txtTo_' + id).val(),
                'Percentage': $('#txtPercentage_' + id).val()
            }
            list.push(data);
        });
        return list;
    }

    var deleteRow = function (obj) {
        obj.closest('tr').remove();
    }

    return {
        Init: initialize,
        DeleteRow: deleteRow
    };
})();
