﻿var ECO_Admin = ECO_Admin || {};
 ECOsystem.Areas.Admin.PreventiveMaintenanceCatalog = (function () {
    var options = {};
    var hasChanges = 0; // Verifica si hay cambios en el listado de costos de un manteminiento

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        resetAndInitCustomControls();
        initializeDropDownList();

        if ($("#btnAdd").attr('id') != undefined) {
            $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {

                $("#FrequencyTypeId").prop("readonly", false);
                $("#btnCancelPMC").prop('disabled', false);
                $("#btnSavePMC").prop('disabled', false);
                resetAndInitCustomControls();
            });
        }

        if ($("#detGridContainer").attr('id') != undefined) {
            $('#detGridContainer').off('click.del', 'a[del_row_detail]').on('click.del', 'a[del_row_detail]', function (e) {
                $('#deleteCostModal').find('#id').val($(this).attr('id'));
                $('#deleteCostModal').modal('show');
                return false;
            });
        }

        if ($("#btnDeleteCost").attr('id') != undefined) {
            $('#btnDeleteCost').off('click.btnDeleteCost').on('click.btnDeleteCost', function (e) {
                var id = $('#deleteCostModal').find('#id').val();
                var cost = $('#Cost-' + id).val();
                if ($('#Cost-' + id).attr("name").replace('Cost-', '') == '0') {
                    var tr = $($('#Cost-' + id)).closest("tr").hide();
                    tr.fadeOut(400, function () { tr.remove(); });
                    $('#deleteCostModal').modal('hide');
                    sumatory();
                }
                else {
                    var postData = { id: id };
                    $.ajax({
                        type: "POST",
                        url: ECOsystem.Utilities.GetUrl() + '/PreventiveMaintenanceCatalog/DeleteCost',
                        data: postData,
                        success: function (data) {
                            var tr = $($('#Cost-' + id)).closest("tr").hide();
                            tr.fadeOut(400, function () { tr.remove(); });
                            $('#deleteCostModal').modal('hide');
                            sumatory();
                            hasChanges = 1;
                        },
                        dataType: "json",
                        traditional: true
                    });
                }
            });
        }

        initBtnCost();

        if ($("#gridContainer").attr('id') != undefined) {
            $('#gridContainer').off('click.cost', 'a[cost_row]').on('click.cost', 'a[cost_row]', function (e) {
                $('#costForm').find('#id').val($(this).attr('id'));
                $('#costForm').submit();
            });
        }

        $('#StartDateStr').datepicker({
            defaultDate: 'now',
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
            beforeShowDay: function (date) {
                return date > new Date();
            }
        });

        $('#EndDateStr').datepicker({
            language: 'es',
            defaultDate: 'now',
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
            beforeShowDay: function (date) {
                return date > new Date();
            }
        });
    };

    var initBtnCost = function () {
        $("#btnSaveCost").click(function () {
            
            var valid = true;
            var count = 0;
            var list = new Array();
            $('a[del_row_detail]').each(
                function (index) {
                    if (parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val())) < 0) { valid = false; }
                    if ($.trim($('#Name-' + $(this).attr('id')).val()) == '') { valid = false; }
                    var PreventiveMaintenanceCatalogId = $('#costForm').find('#id').val();
                    var PreventiveMaintenanceCostId = $('#Cost-' + $(this).attr('id')).attr("name").replace('Cost-', '');
                    var Description = $.trim($('#Name-' + $(this).attr('id')).val());
                    var Cost = 0;
                    if ($('#Cost-' + $(this).attr('id')).hasClass("formatted") == true) {
                        Cost = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()));
                    }
                    else {
                        Cost = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()) / 100);
                    }
                    list.push("{ PreventiveMaintenanceCatalogId:" + parseInt(PreventiveMaintenanceCatalogId) + ", PreventiveMaintenanceCostId:" + parseInt(PreventiveMaintenanceCostId) + ", Description:'" + Description + "', Cost:" + parseFloat(Cost) + " }");
                    count = count + 1;
                });
            if (valid || count != 0) {
                var postData = { list: list };
                ECOsystem.Utilities.ShowLoader();
                $.ajax({
                    type: "POST",
                    url: ECOsystem.Utilities.GetUrl() + '/PreventiveMaintenanceCatalog/SaveCostList',
                    data: postData,
                    success: function (data) {
                        $('#costModal').modal('hide');
                        ECOsystem.Utilities.HideLoader();
                        submitForm();
                    },
                    dataType: "json",
                    traditional: true
                });
            }
        });

        $("#btnAddCost").click(function () {
            $('#gridCost').removeClass('hidden');
            $('#MessageAlert').addClass('hidden');

            var id = 0;
            $('a[del_row_detail]').each(
                function (index) {
                    var newId = parseFloat($(this).attr('id'));
                    if (newId > id) {
                        id = newId;
                    }
                }
            )
            id = id + 1;
            var row = '';
            row = row + '<tr class="grid-row ">';
            row = row + '<td class="grid-cell grid-col-hide data-Id" data-name="PreventiveMaintenanceCatalogId">';
            row = row + $('#costForm').find('#id').val();
            row = row + '</td>';
            row = row + '<td class="grid-cell grid-col-hide data-Id" data-name="PreventiveMaintenanceCostId">';
            row = row + id;
            row = row + '</td>';
            row = row + '<td class="grid-cell grid-col-name" data-name="Description">';
            row = row + '<div>';
            row = row + '<input type="text" class="form-control nav-justified" value="" id="Name-' + id + '" name="Name-0" data-currency="true">';
            row = row + '</div>';
            row = row + '</td>';
            row = row + '<td class="grid-cell grid-col-center" data-name="Cost">';
            row = row + '<div>';
            row = row + '<div class="input-group">';
            row = row + '<span2 class="input-group-addon">';
            row = row + $('#CurrencySymbol').attr('value');
            row = row + '</span2>';
            row = row + '<input type="text" onblur=" ECOsystem.Areas.Admin.PreventiveMaintenanceCatalog.Sumatory()" class="form-control text-right num-cost only-positive" value="0.00" id="Cost-' + id + '" name="Cost-0" data-currency="true">';
            row = row + '</div>';
            row = row + '</div>';
            row = row + '</td>';
            row = row + '<td class="grid-cell grid-col-btn" data-name="">';
            row = row + '<span class="toolBar1">';
            row = row + '<a href="#" class="delete-color" data-toggle="modal" data-target="#deleteCostModal" id="' + id + '" title="Eliminar" del_row_detail=""><i class="mdi mdi-32px mdi-close-box"></i></a>';
            row = row + '</span>';
            row = row + '</td>';
            row = row + '</tr>';
            $('#detGridContainer table').append(row);
            sumatory();
            validatesPositive();
        });
    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        if ($("#FilterTypeId").attr("id") != undefined) {
            var select2 = $("#FilterTypeId").select2().data('select2');
            select2.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectFilterTypeChange(data); return fn.apply(this, arguments); }
                }
            })(select2.onSelect);
        }
    };

    //Initialize Add or Edit Maintenance
    var initAddorEditMaintenance = function () {

        //Initialize validate plugin for the form
        $('#AddOrEditMaintenanceForm').validate();


        $('#btnSaveAddOrEditMaintenance').click(function () {
            //Validate form
            if ($('#AddOrEditMaintenanceForm').valid()) {

                //Confirm Modal
                $('#ConfirmAddOrEditMaintenance').modal('show');
            }
        });

        //Confirm button trigger
        $('#btnConfirmAddOrEditMaintenance').click(function () {
            addOrEditMaintenance();
        });

        //Get changed event value to validate check box element
        $("#HasMaxRange").change(function () {
            if (!$(this).is(":checked")) {
                $('#MaxRangeKM').attr("disabled", "disabled");
                $('#MaxRangeDateStr').attr("disabled", "disabled");
            } else {
                $('#MaxRangeKM').removeAttr("disabled");
                $('#MaxRangeDateStr').removeAttr("disabled");
            }
        });

        //Init Max range date picker
        $('#MaxRangeDateStr').datepicker({
            language: 'es',
            minDate: new Date(),
            format: 'dd/mm/yyyy',
            autoclose: true
        });
        validatesPositive();
    }

    var validatesPositive = function () {
        $('.only-positive').keypress(function (e) {
            var key = window.Event ? e.which : e.keyCode

            if (key != 46 && key > 31 && (key >= 48 && key <= 57)) {
                return true;
            } else {
                return false;
            }          
        });
    }

    // Add Or Edit Maintenance: Send data for backend //
    var addOrEditMaintenance = function () {
        var modelData = {
            'PreventiveMaintenanceCatalogId': $('#PreventiveMaintenanceCatalogId').val(),
            'FilterType': $('#FilterType:checked').val(),
            'Description': $('#Description').val(),
            'FrequencyKm': $('#FrequencyKm').val(),
            'AlertBeforeKm': $('#AlertBeforeKm').val(),
            'FrequencyMonth': $('#FrequencyMonth').val(),
            'AlertBeforeMonth': $('#AlertBeforeMonth').val(),
            'HasMaxRange': $("#HasMaxRange").is(":checked"),
            'MaxRangeDateStr': $('#MaxRangeDateStr').val(),
            'MaxRangeKM': $('#MaxRangeKM').val(),
            'RowVersion': $('#RowVersion').val(),
            'StartDateStr': $('#StartDateStr').val(),
            'EndDateStr': $('#EndDateStr').val()
            //'AlertBeforeDate': model.AlertBeforeDate,            
            //'FrequencyDate': model.FrequencyDate,            
        }

        ECOsystem.Utilities.ShowLoader();
        $('#ConfirmAddOrEditMaintenance').modal('hide');

        $.ajax({
            url: '/PreventiveMaintenanceCatalog/AddOrEditPreventiveMaintenance',
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ model: modelData }),
            success: function (result) {
                
                if (result.indexOf('Success') > -1) {

                    window.location.href = '/Administracion/PreventiveMaintenanceCatalog'

                } else {
                    ECOsystem.Utilities.HideLoader();

                    $('#dangerMaintenanceMessage').show();
                    $('#msgMaintenanceAlert').html(result);
                }
            }
        });

    }

    /*on Select FilterType*/
    var onSelectFilterTypeChange = function (obj) {

        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#ReloadForm').find('#FilterId').val(obj.id);
            $('#ReloadForm').submit();
        }
    };


    var resetAndInitCustomControls = function () {
        $("#FrequencyKm").numeric("integer");
        $("#AlertBeforeKm").numeric("integer");
        $("#FrequencyMonth").numeric("integer");
        $("#AlertBeforeMonth").numeric("integer");

        //var select1 = $("#FrequencyTypeId").select2().data('select2');
        //select1.onSelect = (function (fn) {
        //    return function (data, opts) {
        //        var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectChange(data); return fn.apply(this, arguments); }
        //    }
        //})(select1.onSelect);

        $('#datetimepicker1').datepicker({ language: 'es', minDate: new Date(), format: 'dd/mm/yyyy', autoclose: true, todayHighlight: true });
    }

    var initDetail = function () {
        sumatory();
    }

    var sumatory = function () {
        var total = 0;
        $('a[del_row_detail]').each(
            function (index) {
                var num = 0;
                try {
                    if ($('#Cost-' + $(this).attr('id')).hasClass("formatted") == true) {
                        num = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()));
                    }
                    else {
                        num = parseFloat(ECOsystem.Utilities.ToNum($('#Cost-' + $(this).attr('id')).val()) / 100);
                        $('#Cost-' + $(this).attr('id')).addClass("formatted");
                    }
                }
                catch (err) {
                    num = 0;
                }
                $('#Cost-' + $(this).attr('id')).val(ECOsystem.Utilities.FormatNum(num));
                total = total + num;
                $('#Cost-total').val(ECOsystem.Utilities.FormatNum(total));
            }
        )
    }

    //Refresh catalog list if changes data
    var refreshChanges = function () {
        submitForm();
    };

    var submitForm = function () {

        //$('#addOrEditForm').submit();
        $('#gridContainer').load('/PreventiveMaintenanceCatalog/RefreshGrid');
    };

    var onSelectChange = function (obj) {
        changeDiv(obj.id, true);
    };

    var addOrEditModalShow = function () {
        resetAndInitCustomControls();
        ECOsystem.Utilities.AddOrEditModalShow();
    };

    var costModalShow = function () {
        $('#costModal').modal('show');
        validatesPositive();
        initBtnCost();
    };

    /* Public methods */
    return {
        Init: initialize,
        AddOrEditModalShow: addOrEditModalShow,
        CostModalShow: costModalShow,
        InitDetail: initDetail,
        Sumatory: sumatory,
        RefreshChanges: refreshChanges,
        InitAddOrEditMaintenance: initAddorEditMaintenance
    };
})();


