﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.ApprovalTransactionsParameters = (function () {
    var options = {};

    var initialize = function (opts) {
        $.extend(options, opts);

        $('#btnAddColumn').click(function () {
            onBeginAddColumn();
        });

        $('#tableContainer').off('click.btnRemoveColumn', 'a[btnRemoveColumn]').on('click.btnRemoveColumn', 'a[btnRemoveColumn]',function () {
            var id = $(this).attr('id');
            $('#RemoveColumnForm').find('#Id').val(id);            
            $('#ConfirmationRemoveColumn').modal('show');
        });

        $('#tableContainer').off('click.btnEditColumn', 'a[btnEditColumn]').on('click.btnEditColumn', 'a[btnEditColumn]', function () {
            var id = $(this).attr('id');
            editColumn(id);            
        });

        $('#btnRemoveColumn').click(function () {
            $('#RemoveColumnForm').submit();
        });

        $('#btnCancel').click(function () {
            window.location.reload();
        });        
    }

    var editColumn = function (id) {
        $('#AddColumnForm').find('#Id').val(id);
        $.ajax({
            url: 'ApprovalTransactionsParameters/GetColumnToEdit',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({ Id: id }),
            contentType: 'application/json',
            success: function (data) {                
                $('#ColumnName').val(data);
                $('#ColumnName').focus();
            }
        });
    }

    var onCompleteAddOrColum = function (data) {
        hideloader();       

        if (data.split('|')[0] == 'Error') {
            $('#ControlsContainer').html('<br />' +
                '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                'Error al agregar columna. <br /><hr /> <b>Error técnico:</b> <br /><hr />' + data + '</div > ');
        } else {
            $('#AddColumnForm').find('#Id').val(null);
            $('#ColumnName').val('');
            ECOsystem.Utilities.setConfirmation();
        }
    }

    var onBeginRemove = function () {
        showloader();
        $('#ConfirmationRemoveColumn').modal('hide');
    };

    var onCompleteRemove = function (data) {
        hideloader();        
        if (data != 'Success') {
            $('#ControlsContainer').html('<br />' +
                '<div class="alert alert-info alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +
                'Error al eliminar columna. <br /><hr /> <b>Error técnico:</b> <br /><hr />' + data + '</div > ');
        } else {
            ECOsystem.Utilities.setConfirmation();
            window.location.reload();
        }
    }
    
    var onBeginAddColumn = function () {
        var val = $('#ColumnName').val();

        if (val != '') {
            showloader();           
            $('#AddColumnForm').submit();
        }
    }    

    var showloader = function () {
        $('body').loader('show');
    }

    var hideloader = function () {
        $('body').loader('hide');
    }

    return {
        Init: initialize,
        ShowLoader: showloader,
        OnCompleteAddOrColum: onCompleteAddOrColum,
        OnBeginAddColumn: onBeginAddColumn,        
        OnCompleteRemove: onCompleteRemove,
        OnBeginRemove: onBeginRemove
    };
})();