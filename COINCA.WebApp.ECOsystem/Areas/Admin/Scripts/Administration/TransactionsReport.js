﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.TransactionsReport = (function () {
    var options = {};

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();
        disabledControls();
        initCharts();
        try {
            initializeDropDownList();
        }
        catch (exception) {
        }
    }

    /*init Events*/
    var initEvents = function () {
        $('#page-content-wrapper').off('click.print', 'a[data-print-link]').on('click.print', 'a[data-print-link]', function (e) {
            ECOsystem.Utilities.ShowPopup(options.printURL, 'Formato de impresión');
        });
        $('#page-content-wrapper').off('click.goToPrint', 'button[data-print]').on('click.goToPrint', 'button[data-print]', function (e) {
            window.print();
        });

        if (!options.printMode) {
            $('#datetimepickerStartDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_StartDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });
            $("#Parameters_StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });


            $('#datetimepickerEndDate').datepicker({
                language: 'es',
                minDate: new Date(),
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                if ($('#Parameters_EndDateStr').val().length == 10 && checkAllSelects())
                    $('#ReloadForm').submit();
                else
                    invalidSelectOption();
            });

            var x = $('#Parameters_TransactionType').val();
            var lp = $('#liProcesadas').val();
            if (x == 1 && lp == 1) $('#liProcesadas').removeClass();

            $("#Parameters_EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: '/',
                maxLength: 10
            });
        }
    }

    /*disabled Controls when printMode = true*/
    var disabledControls = function () {
        if (options.printMode) {
            $('[data-filter]').attr('disabled', 'disabled');
        }
    }

    /*Init Charts*/
    var initCharts = function () { }
    
    /*format Label*/
    var formatLabel = function (v) {
        return ECOsystem.Utilities.FormatNum(ECOsystem.Utilities.ToFloat(v)) + "%";
    }

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#Parameters_ReportCriteriaId").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_Month").select2().data('select2');
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Year").select2().data('select2');
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
            }
        })(select3.onSelect);

        try {
            var select4 = $("#Parameters_TransactionType").select2().data('select2');
            select4.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportTransactionTypeChange(data); return fn.apply(this, arguments); }
                }
            })(select4.onSelect);
        } catch (e) {}        
    };

    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('div[data-repor-by]').addClass("hide");
            $("#Parameters_Month").select2('val', '');
            $("#Parameters_Year").select2('val', '');
            $('#Parameters_StartDateStr').val('');
            $('#Parameters_EndDateStr').val('');

            $('#Parameters_ReportCriteriaId').val(obj.id);
            $('div[data-repor-by=' + obj.id + ']').removeClass("hide");
            if (checkAllSelects()) submitFor();
        }
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Month').val(obj.id);
            if (checkAllSelects()) submitFor();
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        console.debug('cambiando a: ' + obj.id);
        if (obj.id === '') {
            invalidSelectOption();
        } else {
            $('#Parameters_Year').val(obj.id);
            if (checkAllSelects()) submitFor();
        }
    };

    /*on Select Report Fuel Type Change*/
    var onSelectReportTransactionTypeChange = function (obj) {
        
        $('#Parameters_TransactionType').val(obj.id);
        if (checkAllSelects()) submitFor();
    };   

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $('#ControlsContainer').html('<br />' +
                                        '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);">' +                                          
                                        'Por favor seleccionar las opciones correspondientes para cargar la información. </div>');
    };

    /*check All Selects before submit form*/
    var checkAllSelects = function () {
        
        if (($('#Parameters_ReportCriteriaId').val() === '')
                || ($('#Parameters_Month').val() === '')
                || ($('#Parameters_Year').val() === '')
                || ($('#Parameters_TransactionType').val() === '')
                || ($('#Parameters_StartDateStr').val() === '')
                || ($('#Parameters_EndDateStr').val() === '')
            ) {
            if (($('#Parameters_Month').val() !== '') && ($('#Parameters_Year').val() !== '') && ($('#Parameters_TransactionType').val() !== ''))
                return true;
            if (($('#Parameters_StartDateStr').val() !== '') && ($('#Parameters_EndDateStr').val() !== '') && ($('#Parameters_TransactionType').val() !== ''))
                return true;
            invalidSelectOption();
            return false;
        }
        return true;
    };

    var submitFor = function () {
        $('#TransactionsReportRetrieveForm').find('#Month').val($('#Parameters_Month').val());
        $('#TransactionsReportRetrieveForm').find('#Year').val($('#Parameters_Year').val());
        $('#TransactionsReportRetrieveForm').find('#TransactionType').val($('#Parameters_TransactionType').val());
        $('#TransactionsReportRetrieveForm').find('#StartDateStr').val($('#Parameters_StartDateStr').val());
        $('#TransactionsReportRetrieveForm').find('#EndDateStr').val($('#Parameters_EndDateStr').val());

        $('#TransactionsReportRetrieveForm').submit();
    };


    var onClickDownloadPartnerReport = function () {
        $('#ExcelReportDownloadForm').submit();
    };
    
    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
    };

    /* Public methods */
    return {
        Init: initialize,
        FormatLabel: formatLabel,
        OnClickDownloadPartnerReport: onClickDownloadPartnerReport,
        ShowLoader: showLoader,
        HideLoader: hideLoader
    };
})();

