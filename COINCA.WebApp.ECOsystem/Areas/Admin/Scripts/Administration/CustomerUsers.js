﻿var ECO_Admin = ECO_Admin || {};

 ECOsystem.Areas.Admin.CustomerUsers = (function () {
    var options = {};


    /* initialize function */

    var initialize = function (opts) {
        $.extend(options, opts);

        //trigger click on hidden file input
        $('#btnUpload').off('click.btnUpload').on('click.btnUpload', function (e) {

            $('#fileHidden').trigger('click');
            e.stopPropagation();
        });
        fileApiUpload();
        formValidation();
        onClickNew();
        onClickCheckboxChangePassword();
        //initCheckUsername();
        initializeDropDownList();

        //Init dropDown 
        $("#RoleId").select2('data', {});
        $("#RoleId").select2({
            formatResult: roleSelectFormat,
            formatSelection: roleSelectFormat,
            escapeMarkup: function (m) { return m; },
            allowClear: true
        });

        $('#btnUnlockSupport').click(function () {
            var val = $('#txtEmailSupport').val();
            var reset = $('#resetpassword').prop('checked');

            if (val == "") {
                $('#txtEmailSupport').focus();
            }
            else {
                $.ajax({
                    url: 'Users/UnlockSupportUsers',
                    type: 'POST',
                    data: JSON.stringify({ Email: val, ResetPass: reset }),
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (Data) {
                        if (Data == "Success") {
                            $('#txtValidation').html('El proceso se completó correctamente');
                            $('#txtValidation').removeClass('hidden');
                            $('#txtEmailSupport').val('');
                        }
                        else {
                            $('#txtValidation').html('Verifique el correo electrónico e inténtelo de nuevo');
                            $('#txtValidation').removeClass('hidden');
                        }

                    },
                    error: function (xhr) {
                        $('#txtValidation').html('Verifique el correo electrónico e inténtelo de nuevo');
                        $('#txtValidation').removeClass('hidden');
                    }
                });
            }
        });

        $("#txtSearch").on('keydown', function () {
            var key = event.keyCode || event.charCode;

            if (key == 8 || key == 46) {
                if ($(this).val().length == 0) {
                    $("#btnSearch").trigger("click");
                }
            }
        });

        $('#DecryptedEmail').off('blur.DecryptedEmail').on('blur.DecryptedEmail', function () {
            if (!validateEmailFormat()) {
                $('#DecryptedEmail').focus();
                $('#DecryptedEmail').css("border", "2px solid red");
            }
            else {
                $('#DecryptedEmail').css("border", "");
            }
        });
    }

    var validateEmailFormat = function () {
        if (!/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/.test($('#DecryptedEmail').val())) return false;
        else return true;

    };

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#Parameters_Status").select2().data('select2');
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectUserStatusChange(data); return fn.apply(this, arguments); }
            }
        })(select1.onSelect);

    };

    /*on Select Report UserStatusChange*/
    var onSelectUserStatusChange = function (obj) {

        $('#searchForm').find("#statusId").val(obj.id);
        showLoader();
        $('#searchForm').submit();
    };

    var checkValidUserName = function () {
        if ($('#CustomerUserId').val() == '' || $('#CustomerUserId').val() == undefined) {
            if (validateEmailFormat()) {
                $('#checkForm').find('#username').val($('#DecryptedEmail').val());
                $('#checkForm').submit();
            } else {
                    $("#addOrEditForm").submit(); //Do submit will show the errors.                          
            }
        } else {
            $("#addOrEditForm").submit(); //Si es una edición no lo válida.
        }
    };

    /*on Success Check User*/
    var onSuccessCheckUser = function (data) {
        if (!data.result) {
            ECOsystem.Utilities.SetMessageShow("El correo electrónico ya es utilizado.", "ERROR");
        } else {           
                $("#addOrEditForm").submit(); //Do submit will show the errors.
        }
    };

    /* Select Format*/
    var roleSelectFormat = function (item) {
        if (!item.id) return item.text;
        var json = JSON.parse(item.text.replace(/\t/g, '').replace(/\t/g, '').replace(/'/g, "\""));
        return '<div class="row">' +
            '<div class="col-md-12">' + json.Name + '</div>' +
            '</div>';
    };

    /* Standard Error Function for File API */

    var fileApiErrorHandler = function (e) {
        switch (e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                alert("Archivo no encontrado");
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert("El archivo no es legible");
                break;
            case e.target.error.ABORT_ERR:
                break;
            default:
                alert("Ocurrió un error leyendo el archivo");
        };
    };


    /* Update progress for File API */

    var fileApiUpdateProgress = function (e) {
        if (e.lengthComputable) {
            var percentLoaded = Math.round((e.loaded / e.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                $('.percent').css('width', percentLoaded + '%').html(percentLoaded + '%');
            }
        }
    };


    /* File Upload implementation */
    var fileApiUpload = function () {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            $('#fileHidden').off('change.fileHidden').on('change.fileHidden', function (evt) {
                // Reset progress indicator on new file selection.
                $('.percent').html('0%').css('width', '0%');
                // FileList object
                var files = evt.target.files;
                var reader = new FileReader();
                //File reader actions
                reader.onerror = fileApiErrorHandler;
                reader.onprogress = fileApiUpdateProgress;
                reader.onabort = function (e) {
                    alert("Lectura del archivo cancelada");
                    reader.abort();
                };
                reader.onloadstart = function (e) {

                    $('#imgPhoto').addClass('imgloading');
                    $('.progress_bar').addClass('loading').fadeIn(1500);
                };
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        alert("La extensión del archivo es inválida");
                        continue;
                    }
                    // Closure to capture the file information.
                    reader.onload = (function (F) {
                        $('.percent').html('100%').css('width', '100%');
                        setTimeout("$('.progress_bar').fadeOut().removeClass('loading'); $('#imgPhoto').removeClass('imgloading');", 2000);
                        return function (e) {
                            var lsrc = e.target.result;
                            $('#imgPhoto').attr({
                                src: lsrc,
                                title: escape(F.name)
                            });
                            $('#Photo').val(lsrc);
                        };
                    })(f);
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
            });
        } else {
            alert('La opción de subir imágenes no es soportada por este navegador.');
        }
    };


    /* Form validation to prevent an invalid submission*/

    var formValidation = function () {

        $('#detailContainer').off('click.btnSaveSubmit', 'button[type=submit]').on('click.btnSaveSubmit', 'button[type=submit]', function (e) {
            setErrorMsj('', false);
            if (validatePassword() && validateEmail() && validatePasswordTemporal()) {
                return true;
            } else {
                return false;
            }
        });
    };


    /* Email validation to prevent an invalid submission*/

    var validateEmail = function () {
        if ($('#DecryptedEmail').val().length == 0) {
            return true;
        }

        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,10})+$/;
        if ($('#DecryptedEmail').val().match(email)) {
            return true;
        } else {
            setErrorMsj('Correo electrónico invalido', true);
            return false;
        }
    };


    /* Password validation to prevent an invalid submission*/

    var validatePassword = function () {
        if ($('#detailContainer').find('#Password').length > 0) {
            if (($('#Password').val().length == 0) && ($('#PasswordConfirm').val().length == 0)) {
                return true;
            }

            if ($('#Password').val() == $('#PasswordConfirm').val()) {
                var pass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
                if ($('#Password').val().match(pass)) {
                    return true;
                } else {
                    setErrorMsj('La contraseña debe contener al menos entre 8 y 15 caracteres, una letra minúscula, una letra mayúscula, un dígito numérico y un carácter especial', true);
                    return false;
                }

            } else {
                setErrorMsj('Las contraseñas deben ser iguales', true);
                return false;
            }
        }
        return true;
    };


    /* Password Temporar validation to prevent an invalid submission*/

    var validatePasswordTemporal = function () {
        if ($('#detailContainer').find('#PasswordTemporal').length > 0) {
            if ($('#PasswordTemporal').val().length == 0) {
                return true;
            }
            var pass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
            if ($('#PasswordTemporal').val().match(pass)) {
                return true;
            } else {
                setErrorMsj('La contraseña debe contener al menos entre 8 y 15 caracteres, una letra minúscula, una letra mayúscula, un dígito numérico y un carácter especial', true);
                return false;
            }
        }
        return true;
    };

    /* Email validation to prevent an invalid submission*/
    var validateDetailEmail = function () {
        setErrorMsj('', false);
        if ($('#DecryptedEmail').val().length == 0) {
            return false;
        }
        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,20})+$/;
        if ($('#DecryptedEmail').val().match(email)) {
            return true;
        } else {
            return false;
        }
    };

    /* Show or hide errors messages*/

    var setErrorMsj = function (msj, show) {
        if (show) {
            $('#errorMsj').html(msj);
            $('#validationErrors').removeClass('hide');
        } else {
            $('#errorMsj').html('');
            $('#validationErrors').addClass('hide');
        }

    }


    /* On Success Load after call edit*/

    var onSuccessLoad = function () {
        initialize();
        ECOsystem.Utilities.AddOrEditModalShow();
        $('#btnAdd').removeAttr('disabled');
        ECOsystem.Utilities.HideLoader();
    };


    var onSuccessLoadAlarm = function () {
        ECOsystem.Utilities.AlarmModalShow();
    };


    /* On click BtnNew*/
    var onClickNew = function () {
        $('#btnAdd').off('click.btnAdd').on('click.btnAdd', function (e) {
            if (!$('#btnAdd').is(':disabled')) {
                $('#btnAdd').attr('disabled', 'disabled');
                ECOsystem.Utilities.ShowLoader();
                $('#loadForm').find('#id').val('-1');
                $('#loadForm').submit();
            }
        });
    };


    /* On click change pass*/

    var onClickCheckboxChangePassword = function () {
        $('#detailContainer').off('click.checkboxChangePassword', '#ChangePassword').on('click.checkboxChangePassword', '#ChangePassword', function (e) {
            if ($(this).is(':checked')) {
                $('#_PasswordTemporal').removeClass('hide').val('');
            } else {
                $('#_PasswordTemporal').addClass('hide').val('');
            }

        });
    };

    /*initCheckUsername*/
    var onSuccessReload = function () {
        location.reload();
    }

    var onClickDownloadCustomers = function () {

        $('#ExcelReportDownloadForm').submit();
    };


    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadExcelForm').submit();
    };

    /* Public methods */

    return {
        Init: initialize,
        OnSuccessLoad: onSuccessLoad,
        OnSuccessLoadAlarm: onSuccessLoadAlarm,
        OnSuccessCheckUser: onSuccessCheckUser,
        OnSuccessReload: onSuccessReload,
        ShowLoader: showLoader,
        HideLoader: hideLoader,
        OnClickDownloadCustomers: onClickDownloadCustomers,
        CheckValidUserName: checkValidUserName
    };
})();

