﻿using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Utilities;
using ECOsystem.Models.Core;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    public class CostCenterBusiness
    {

        /// <summary>
        /// Retrieve the CostCenters
        /// </summary>
        public IEnumerable<ServiceStations> CostCenterRetrieve()
        {
            int? CustomerId = Session.GetCustomerId() != null ? Session.GetCustomerId() : null;

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ServiceStations>("[General].[RetrieveCostCenterByCustomerId]", new
                {                
                    CustomerId
                });
            }
        }
    }
}