﻿/************************************************************************************************************
*  File    : FuelsBusiness.cs
*  Summary : Fuels Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
//using COINCA.Library.DataAccess;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;
using static ECOsystem.Models.Core.FuelsModels;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Fuels Business Class
    /// </summary>
    public class FuelsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Fuels
        /// </summary>
        /// <param name="fuelId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="countryId">Optional country id to filter fuels by country</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Fuels> RetrieveFuels(int? fuelId, int? countryId = null, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Fuels>("[Control].[Sp_Fuels_Retrieve]",
                    new
                    {
                        FuelId = fuelId,
                        Key = key,
                        CountryId = countryId
                    });
            }
        }


        /// <summary>
        /// Retrieve Fuels Cost
        /// </summary>
        /// <returns></returns>
        public IList<FuelsCost> RetrieveFuelsCost()
        {
            IList<FuelsCost> fuels;
            using (var dba = new DataBaseAccess())
            {
                fuels = dba.ExecuteReader<FuelsCost>("[Control].[Sp_FuelsCost_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    });
            }
            return fuels;
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Fuels
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditFuels(Fuels model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_Fuels_AddOrEdit]",
                    new
                    {
                        model.FuelId,
                        model.Name,
                        model.CountryId,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model Fuels
        /// </summary>
        /// <param name="fuelId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteFuels(int fuelId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_Fuels_Delete]",
                    new { FuelId = fuelId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}