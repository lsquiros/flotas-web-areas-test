﻿/************************************************************************************************************
*  File    : DallasKeysBusiness.cs
*  Summary : DallasKeys Business Methods
*  Author  : Gerald Solano
*  Date    : 27/08/2015
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Dallas Keys Business Class
    /// </summary>
    public class DallasKeysBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve DallasKeys
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        ///   <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<DallasKeys> RetrieveDallasKeys(int? dallasId = null, DateTime? startDate = null, DateTime? endDate = null, string key = null, bool? isUsed = null)
        {
            using (var dba = new DataBaseAccess())
            {

                return dba.ExecuteReader<DallasKeys>("[General].[Sp_DallasKeys_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        DallasId = dallasId,
                        Key = key,
                        StartDateTime = startDate,
                        EndDateTime = endDate,
                        IsUsed = isUsed
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity DallasKeys
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditDallasKeys(DallasKeys model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_DallasKeys_AddOrEdit]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        DallasKey = model.DallasId,
                        DallasCode = model.DallasCode,
                        IsDeleted = model.IsDeleted,
                        LogUserId = model.LoggedUserId,
                        RowVersion = model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity DallasKeysByDriver
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditDallasKeysByDriver(int userId, int dallasId)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                dba.ExecuteNonQuery("[General].[Sp_DallasKeysByDriver_AddOrEdit]",
                    new
                    {
                        DallasId = dallasId,
                        UserId = userId,
                        LogUserId = (user != null)? user.UserId : 0
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of remove dallas key assign for the user.
        /// </summary>
        /// <param name="DallasId"></param>
        public bool RemoveDallasKeys(int userId)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    var result = dba.ExecuteScalar<int>("[General].[Sp_DallasKeys_Remove]",
                        new { UserId = userId });

                    if (result > 0) {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Validate if exist dallas key
        /// </summary>
        /// <returns></returns>
        public bool ExistDallasKey(string dallasCode) {
            try
            {
                var existDallas = RetrieveDallasKeys(null, key: dallasCode).FirstOrDefault();
                if (existDallas != null) {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}