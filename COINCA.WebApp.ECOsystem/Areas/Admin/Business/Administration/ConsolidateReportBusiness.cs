﻿using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    public class ConsolidateReportBusiness : IDisposable
    {
        public DataSet GetReportDownloadData(ConsolidatedReportParameters parameters)
        {
            DataSet ds = new DataSet();          

            using (var bus = new PreventiveMaintenanceVehicleBusiness())
            {
                ds.Tables.Add(DataTableUtilities.ClassToDataTable(GetConsolidatedReportData(parameters).ToList(), "ConsolidateReport"));
            }

            return ds;
        }

        public IEnumerable<ConsolidatedReportModels> GetConsolidatedReportData(ConsolidatedReportParameters parameters)
        {
            using (var dba = new DataBaseAccess())
            {

                return dba.ExecuteReader<ConsolidatedReportModels>("[General].[Sp_ConsolidateMaintenanceReport_Retrieve]",
                new
                {
                    parameters.VehicleId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    parameters.StartDate,
                    parameters.EndDate,
                    parameters.Year,
                    parameters.Month
                });
            }
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}