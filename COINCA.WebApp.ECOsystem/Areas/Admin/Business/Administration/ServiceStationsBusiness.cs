﻿using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Utilities;
using System.Web.Mvc;
using  ECOsystem.Areas.Admin.Business.Administration;
using System.Data;
using System.Web;
using System.Net.Http;
using System.IO;
using System.Net.Http.Headers;
using ModuleConfig;
using ECOsystem.Models.Core;
using System.Collections;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Service Station Business
    /// </summary>
    public class ServiceStationsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve the name of the Canton
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetCantonNameById(int id)
        {
            using (var business = new GeographicDivisionBusiness())
            {
                var model = new ECOsystem.Models.Core.Counties();

                model = business.RetrieveCounties(id, null, null, null).FirstOrDefault();

                return model.Name;
            }            
        }

        /// <summary>
        ///  Retrieve the name of the Province
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetProvinceNameById(int id)
        {
            using (var business = new GeographicDivisionBusiness())
            {
                var model = new ECOsystem.Models.Core.States();

                model = business.RetrieveStates(id, null, null).FirstOrDefault();

                return model.Name;
            }
        }
        
        /// <summary>
        /// Retrieve the States based on the StatesId
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static IEnumerable<ECOsystem.Models.Core.States> GetStates(int? countryId)
        {
            using (var business = new GeographicDivisionBusiness())
            {
                return business.RetrieveStates(null, countryId);               
            }            
        }

        /// <summary>
        /// Retrieve the States based on the CountiesId
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static IEnumerable<ECOsystem.Models.Core.Counties> GetCounties(int? stateId, int? countryId)
        {
            using (var business = new GeographicDivisionBusiness())
            {
                return business.RetrieveCounties(null, stateId, countryId);
            }
        }

        /// <summary>
        /// Load Counties for the Edit Service Statios
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static SelectList GetCountiesEditServiceStatios(int? stateId, int? countryId)
        {
            var dictionary = new Dictionary<int?, string>();
            if (stateId != null)
            {
                using (var business = new GeographicDivisionBusiness())
                {
                    var result = business.RetrieveCounties(null, stateId, countryId);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CountyId, r.Name);
                    }
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        /// <summary>
        /// Service Statios AddOrEdit
        /// </summary>
        /// <param name="model"></param>
        public void ServiceStationsAddOrEdit(ServiceStations model)
        {
            var Users = Session.GetUserInfo();
            int CountryId = (int)Session.GetCustomerInfo().CountryId;
            int? CustomerId = Session.GetCustomerId() != null ?  Session.GetCustomerId() : null;

            using (var dba = new DataBaseAccess())
            {
                model.ServiceStationId = dba.ExecuteScalar<int>("[General].[Sp_ServiceStations_AddOrEdit]", new
                {
                    model.ServiceStationId,
                    model.BacId,
                    model.Name,
                    model.Address,
                    model.CantonId,
                    model.ProvinceId,
                    model.Latitude,
                    model.Longitude,
                    UserId = Users.UserId,
                    CountryId,
                    model.SAPProv,
                    model.Legal_Id,
                    model.Terminal,
                    model.Number_Provider,
                    CustomerId,                   
                    model.PartnerId
                });
            }
            BlockedCosteCenterAdd(model.ServiceStationId, model.CostCenterList);
            //if(model.CostCenterList.Count > 0)
            //{                
            //}
        }

        /// <summary>
        /// Retrieve Service Stations
        /// </summary>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static IEnumerable<ServiceStations> ServiceStationRetrieve(int? id, int? countryId, int? PartnerId, int? customerid , string key = null)
        {

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ServiceStations>("[General].[Sp_ServiceStations_Retrieve]", new
                {
                    ServiceStationId = id,
                    CountryId = countryId,
                    PartnerId,
                    Key = key,
                    CustomerId = customerid
                });
            }
        }

        /// <summary>
        /// Delete Service Stations
        /// </summary>
        /// <param name="SSId"></param>
        public void DeleteServiceStations(int SSId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ServiceStations_Delete]",
                    new { ServiceStationId = SSId });
            }
        }

        /// <summary>
        /// Vehicles Generate
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable DownloadData(List<ServiceStations> list)
        {
            var geopoliticalNames = ECOsystem.Utilities.Session.GetGeopoliticalNames();
            var geopoliticalLevel1 = geopoliticalNames.Count > 0 ? geopoliticalNames[0] : "División geopolítica nivel 1";
            var geopoliticalLevel2 = geopoliticalNames.Count > 1 ? geopoliticalNames[1] : "División geopolítica nivel 2";
            using (var dt = new DataTable())
            {
                dt.Columns.Add("BacId");
                dt.Columns.Add("Name");
                dt.Columns.Add("Address");
                dt.Columns.Add("Province");
                dt.Columns.Add("Canton");
                dt.Columns.Add("LegalId");
                dt.Columns.Add("SAPProv");
                dt.Columns.Add("IsBlocked");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("geoPoliticalLvl1");
                dt.Columns.Add("geoPoliticalLvl2");
                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();
                    row["BacId"] = item.BacId.ToString();
                    row["Name"] = item.Name;
                    row["Address"] = item.Address;
                    row["Province"] = item.ProvinceString;
                    row["Canton"] = item.CantonString;
                    row["LegalId"] = item.Legal_Id;
                    row["SAPProv"] = item.SAPProv.ToString() == "0" ? string.Empty : item.SAPProv.ToString();
                    row["IsBlocked"] = item.IsBlocked;                    
                    row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    row["geoPoliticalLvl1"] = geopoliticalLevel1;
                    row["geoPoliticalLvl2"] = geopoliticalLevel2;
                    dt.Rows.Add(row);
                }
                return dt;
            }
        }

        private void BlockedCosteCenterAdd(int? ServiceStationId, List<BlockedCostsCenter> list)
        {
            var User = Session.GetUserInfo();
            var CustomerId = Session.GetCustomerId();

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_BlockedCostCenter_Add]", new
                {
                    ServiceStationId,      
                    Xml = ECOsystem.Utilities.Miscellaneous.GetXML<BlockedCostsCenter>(list),
                    UserId = User.UserId,
                    CustomerId
                });
            }
        }

        public IEnumerable<BlockedCostsCenter> RetrieveBlockedCostCenter(int? id)
        { 
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<BlockedCostsCenter>("[General].[Sp_BlockedCostCenter_Retrieve]", new
                {
                    CustomerId = Session.GetCustomerId(),
                    ServiceStationId = id
                });
            }             
        }

        public void BlockServiceStations(List<int> list)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_BlockServiceStations_AddOrEdit]", new
                {
                    CustomerId = Session.GetCustomerId(),
                    Xml = ECOsystem.Utilities.Miscellaneous.GetXML<int>(list),
                    UserId = Session.GetUserInfo().UserId
                });
            }

        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
