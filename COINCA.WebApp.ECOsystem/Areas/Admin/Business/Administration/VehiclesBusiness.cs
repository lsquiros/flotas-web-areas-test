﻿/************************************************************************************************************
*  File    : VehiclesBusiness.cs
*  Summary : Vehicles Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using System.Web.Script.Serialization;
using ECOsystem.Business.Utilities;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Linq;
using System.Data;
using System.Web;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Vehicles Class
    /// </summary>
    public class VehiclesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Vehicles
        /// 
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Vehicles> RetrieveVehicles(int? vehicleId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<Vehicles>("[General].[Sp_Vehicles_Retrieve]",
                new
                {
                    VehicleId = vehicleId,
                    CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// Get the vehicles with no credit card
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Vehicles> RetrieveVehiclesWithNoCreditCard()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Vehicles>("[General].[Sp_VehiclesWithNoCreditCard_Retrieve]",
                new
                {             
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                });
            }
        }

        /// <summary>
        /// Retrieve Vehicle by Plate
        /// </summary>
        /// <param name="plateId">The PlateId uniquely that identifies each vehicle</param>
        /// <returns>An IEnumerarble Vehicles Vehicles is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Vehicles> RetrieveVehicleByPlate(string plateId, int customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                int? UserId = null;

                try
                {
                    var user = ECOsystem.Utilities.Session.GetUserInfo();
                    UserId = user.UserId;
                }
                catch (Exception)
                {
                    UserId = null;
                } 

                return dba.ExecuteReader<Vehicles>("[General].[Sp_VehicleByPlate_Retrieve]",
                new {
                    PlateId = plateId,
                    CustomerId = customerId,
                    UserId
                }); 
            }
        }


        /// <summary>
        /// Retrieve Active Customers Count
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public int RetrieveRegisteredVehiclesCount(int? partnerId, int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteScalar<int>("[Insurance].[Sp_RegisteredVehiclesCount_Retrieve]",
                new
                {
                    PartnerId = partnerId,
                    CustomerId = customerId,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// Retrieve VehiclesReferences
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehiclesReferences> RetrieveVehiclesReferences(int? vehicleId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<VehiclesReferences>("[General].[Sp_VehiclesReferences_Retrieve]",
                new
                {
                    VehicleId = vehicleId,
                    Key = key,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicles(Vehicles model)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[General].[Sp_Vehicles_AddOrEdit]",
                    new
                    {
                        model.VehicleId,
                        model.PlateId,
                        model.Name,
                        model.CostCenterId,
                        CustomerId = model.CustomerId ?? ECOsystem.Utilities.Session.GetCustomerId(),
                        model.UserId,
                        model.VehicleCategoryId,
                        model.Active,
                        model.Colour,
                        model.Chassis,
                        model.LastDallas,
                        model.FreightTemperature,
                        model.FreightTemperatureThreshold1,
                        model.FreightTemperatureThreshold2,
                        model.Predictive,
                        model.AVL,
                        model.PhoneNumber,
                        model.Insurance,
                        model.DateExpirationInsurance,
                        model.CoverageType,
                        model.CabinPhone,
                        model.NameEnterpriseInsurance,
                        model.PeriodicityId,
                        model.Cost,
                        model.Odometer,
                        model.LoggedUserId,
                        model.AdministrativeSpeedLimit,
                        model.IntrackReference,
                        model.Imei,
                        model.RowVersion,
                        model.PullPreviousBudget,
                        model.ExternalId,
                        model.OdometerVehicle,
                        model.HasCooler,
                        model.TempSensorCount,
                        model.MinTemperature,
                        model.MaxTemperature,
                        model.FleetioId,
                        model.Depreciation,
                        model.VehiclePrice
                    });
                }
                String message = string.Format("[DataChanges]{0}", new JavaScriptSerializer().Serialize(model));
                new EventLogBusiness().AddLogEvent(LogState.INFO, message);
            }
            catch (Exception exception)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, exception);
                throw;
            }
        }

        /// <summary>
        /// Performs the References Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void EditVehiclesReferences(VehiclesReferences model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehiclesReferences_Edit]",
                new
                {
                    model.VehicleId,
                    model.CustomerId,
                    model.AdministrativeSpeedLimit,
                    model.IntrackReference,
                    model.DeviceReference,
                    model.LoggedUserId,
                    model.RowVersion
                });
            }

        }
        /// <summary>
        /// Performs the the operation of delete on the model Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehicles(int vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Vehicles_Delete]",
                    new { VehicleId = vehicleId,
                          ModifyUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CostCenterId"></param>
        /// <returns></returns>
        public IEnumerable<Vehicles> RetrieveVehicleByCostCenter(int CostCenterId)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<Vehicles>("[General].[Sp_Vehicles_Retrieve]",
                new
                {
                    CostCenterId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),                        
                    user.UserId
                });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="costCenterId"></param>
        /// <param name="opc"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<VehicleCostCenters> RetrieveVehicleCostCenters(int? costCenterId, Boolean opc = false, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<VehicleCostCenters>("[General].[Sp_VehicleCostCenters_Retrieve]",
                new
                {
                    CostCenterId = costCenterId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key,
                    user.UserId
                });

            }
        }

        /// <summary>
        /// Retrieve VehicleGroups
        /// </summary>
        /// <param name="vehicleGroupId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleGroups> RetrieveVehicleGroups(int? vehicleGroupId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<VehicleGroups>("[General].[Sp_VehicleGroups_Retrieve]",
                new
                {
                    VehicleGroupId = vehicleGroupId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// Retrieve VehicleUnits
        /// </summary>
        /// <param name="unitId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleUnits> RetrieveVehicleUnits(int? unitId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<VehicleUnits>("[General].[Sp_VehicleUnits_Retrieve]",
                new
                {
                    UnitId = unitId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key
                });
            }
        }

        /// <summary>
        /// Retrieve VehicleCategories
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        ///   <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleCategories> RetrieveVehicleCategories(int? vehicleCategoryId, bool opc = false, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {

                return dba.ExecuteReader<VehicleCategories>("[General].[Sp_VehicleCategories_Retrieve]",
                new
                {
                    VehicleCategoryId = vehicleCategoryId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key
                });
            }
        }

        /// <summary>
        /// Retrieve Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PreventiveMaintenanceVehicleDetail> RetrievePreventiveMaintenanceByVehicle(int? vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<PreventiveMaintenanceVehicleDetail>("[General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve the VehicleBinnacles
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IEnumerable<VehicleBinnacle> VehicleBinnacleAddRetrieve(int vehicleId, string comment)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<VehicleBinnacle>("[General].[Sp_VehicleBinnacle_Add]",
                new
                {
                    VehicleId = vehicleId,
                    Comment = string.IsNullOrEmpty(comment) ? null : comment,
                    UserName = ECOsystem.Utilities.Session.GetUserInfo().EncryptedEmail
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData()
        {
            try
            {
                var list = new List<Vehicles>();
                list = (List<Vehicles>)HttpContext.Current.Session["VehiclesInfo"];
                return VehiclesGenerate(list);

            }
            catch (Exception e)
            {
                throw e;
            }
        }




        // <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable VehiclesGenerate(IEnumerable<Vehicles> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("Name");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("Manufacturer");
                dt.Columns.Add("Model");
                dt.Columns.Add("Type");
                dt.Columns.Add("CostCenter");
                dt.Columns.Add("Unit");
                dt.Columns.Add("Driver");
                dt.Columns.Add("GPS");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("OdometerVehicle");
                dt.Columns.Add("HasTemperatureSensor");
                dt.Columns.Add("TempSensorCount");
                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();

                    string text = string.Empty;

                    row["Name"] = item.Name;
                    row["PlateId"] = item.PlateId;
                    row["Manufacturer"] = item.Manufacturer;
                    row["Model"] = item.VehicleModel;
                    row["Type"] = item.VehicleType;
                    row["CostCenter"] = item.CostCenterName;
                    row["Unit"] = item.UnitName;
                    row["Driver"] = item.DecryptedUserName;
                    row["GPS"] = item.HasGPS;
                    row["OdometerVehicle"] = item.OdometerVehicle;
                    row["HasTemperatureSensor"] = item.HasCooler;
                    row["TempSensorCount"] = item.TempSensorCount;

                    row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName; 
                    dt.Rows.Add(row);
                }
                return dt;
            }
        }

        public void AddOrEditParameterByVehicle(VehicleParameters model) {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ParametersVehicle_AddOrEdit]",
                new
                {
                    VehicleId = model.VehicleId,
                    GpsElapseTime = model.GPSElapseTime,
                    LoggedUserId = model.LoggedUserId
                });
            }
        }

        public double? RetrieveParametersByVehicle(int VehicleId) {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<double?>("[General].[Sp_ParametersByVehicle_Retrieve]",
                new
                {
                    VehicleId
                });
            }
        }

        /// Retrieve Drivers By Vehicle information
        /// </summary>
        /// <returns>A IEnumerable collection of the Drivers entity model</returns>
        public IEnumerable<ECOsystem.Models.Core.DriversUsers> RetrieveHistoricalDrivers(int? vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                //return business.RetrieveUsers(null, customerId2: ECOsystem.Utilities.Session.GetCustomerId(), opc: true);
                var result = dba.ExecuteReader<ECOsystem.Models.Core.DriversUsers>("[General].[Sp_RetrieveHistorical_VehicleDrivers]",
                new
                {
                    VehicleId = vehicleId
                });

                return result;
            }
        }


        #region Transactions

        /// <summary>
        /// Retrieve Transactions Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of TransactionsReportBase in order to load the chart</returns>
        public TransactionsReportBase RetrieveTransactionsReport(ControlFuelsReportsBase parameters)
        {
            var result = new TransactionsReportBase();
            GetReportDataTransactions(parameters, result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.key = parameters.key;
            result.Parameters.TransactionType = parameters.TransactionType;
            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataTransactions(ControlFuelsReportsBase parameters, TransactionsReportBase result, int? driverId = null)
        {
            var DeniedList = new List<TransactionsReport>();
            using (var dba = new DataBaseAccess())
            {
                int cusId = 0;
                if (parameters.CustomerId == 0)
                    cusId = Convert.ToInt32(Session.GetCustomerId());
                else
                    cusId = parameters.CustomerId;

                var user = ECOsystem.Utilities.Session.GetUserInfo();

                if (parameters.TransactionType == 6)
                {
                    result.List = dba.ExecuteReader<TransactionsReport>("[Control].[Sp_TransactionsReportDenied_Retrieve]",
                        new
                        {
                            CustomerId = cusId,
                            Year = parameters.Year,
                            Month = parameters.Month,
                            StartDate = parameters.StartDate,
                            EndDate = parameters.EndDate,
                            user.UserId,
                            DriverId = parameters.DriverId
                        });

                    foreach (var item in result.List)
                    {
                        var cardNumberStr = item.TransportDataObject.cardNumber != null ? ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat((long)item.TransportDataObject.cardNumber) : item.TransportDataObject.UID;
                        var cardNumberHash = item.TransportDataObject != null ? TripleDesEncryption.Encrypt(cardNumberStr) : "";
                        var ccObj = GetCreditCard(cardNumberHash);

                        item.CreditCardHolder = (ccObj != null) ? ccObj.CreditCardHolder : "Ninguno";
                        item.CreditCardNumber = ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(cardNumberStr);
                        item.VehicleName = (ccObj != null) ? ccObj.VehicleName : "";
                        if (parameters.key != null)
                        {
                            if (item.CreditCardHolder.Contains(parameters.key))
                            {

                                DeniedList.Add(item);
                            }
                        }
                        if (DeniedList.Count != 0)
                        {
                            result.List = DeniedList;
                        }
                        foreach (string tp in item.TransportData.Split(','))
                        {
                            string[] t = tp.Split(':');
                            if (t[0] == "\"terminalId\"")
                            {
                                item.TerminalId = t[1].Replace("\"", string.Empty).Trim();
                            }
                        }
                    }
                }
                else
                {
                    result.List = dba.ExecuteReader<TransactionsReport>("[Control].[Sp_TransactionsReport_Retrieve]",
                    new
                    {
                        CustomerId = cusId,
                        Status = parameters.TransactionType,
                        Key = parameters.key,
                        Year = parameters.Year,
                        Month = parameters.Month,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate,
                        user.UserId,
                        DriverId = parameters.DriverId
                    });
                }
            }
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }


        /// <summary>
        ///  Get the credit cards
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        private CreditCardTransactions GetCreditCard(string cardNumber)
        {
            CreditCardTransactions creditCard = null;

            using (var dba = new DataBaseAccess())
            {
                creditCard = dba.ExecuteReader<CreditCardTransactions>("[Control].[Sp_CreditCardByNumber_Retrieve]",
                new
                {
                    CreditCardNumber = cardNumber
                }).FirstOrDefault();
            }

            if (creditCard == null)
            {
                return null;
            }

            return creditCard;
        }

        #endregion
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        public int RetrieveVehicleSticker(int VehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_Retrieve_VehicleSticker]",
                new
                {
                    VehicleId
                });
            }
        }
    }
}