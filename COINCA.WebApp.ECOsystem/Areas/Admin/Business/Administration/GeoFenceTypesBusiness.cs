﻿/************************************************************************************************************
*  File    : TypesBusiness.cs
*  Summary : Types Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Parameters Business Class
    /// </summary>
    public class GeoFenceTypesBusiness : IDisposable
    {
        public IEnumerable<GeoFenceTypes> RetrieveGeoFenceTypes(int? Id = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<GeoFenceTypes>("[General].[Sp_GeoFenceTypes_Retrieve]", new
                {
                    Id,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                });
            }
        }

        public void AddOrEditGeoFenceType(GeoFenceTypes model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteReader<GeoFenceTypes>("[General].[Sp_AddOrEditGeoFenceTypes]", new
                {
                    Id = model.Id,
                    Name = model.Name,
                    Description = model.Description,
                    IsHome = model.IsHome,
                    Tolerance = model.IsHome == true ? model.Tolerance : 0,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public void DeleteGeoFenceType(int Id) 
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteReader<GeoFenceTypes>("[General].[Sp_DeleteGeoFenceTypes]", new
                {
                    Id,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                });
            }
        }
        
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}