﻿/************************************************************************************************************
*  File    : CreditCardBusiness.cs
*  Summary : CreditCard Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Utilities;
using ECOsystem.Business.Core;
using ECOsystem.Models.Core;
using System.Xml.Linq;
//using ECOsystem.Models.Control;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// CreditCard Class
    /// </summary>
    public class CreditCardBusiness : IDisposable
    {
        /// <summary>
        /// Const for 
        /// </summary>
        public const int PlateCreditCard = 1;
        public const int DriverCodeCreditCard = 6;

        /// <summary>
        /// Retrieve CreditCard
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CreditCard> RetrieveCreditCard(int? creditCardId, string key = null, int? driverId = null, int StatusId = -1)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCard_Retrieve]",
                    new
                    {
                        CreditCardId = creditCardId,
                        CustomerId = Session.GetCustomerId() ?? 0,
                        Key = string.IsNullOrEmpty(key) ? null : key,
                        user.UserId,
                        DriverId = driverId,
                        StatusId
                    });
            }
        }

        /// <summary>
        /// Retrieve CreditCard
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CreditCard> RetrieveCreditCardByVehicle(int VehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardByVehicle_Retrieve]",
                    new
                    {
                        VehicleId
                    });
            }
        }

        /// <summary>
        /// Retrieve CreditCard that are close to the limit
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CreditCard> RetrieveCreditCardCloseToLimite(int? creditCardId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardCloseCreditLimit_Retrieve]",
                    new
                    {
                        CreditCardId = creditCardId,
                        CustomerId = Session.GetCustomerId() ?? 0,
                        Key = string.IsNullOrEmpty(key) ? null : key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve CreditCardByNumber
        /// </summary>
        /// <param name="creditCardNumber">The Credit Card Number that identifies each Credit Card</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CreditCard> RetrieveCreditCardByNumber(string creditCardNumber)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardByNumber_Retrieve]",
                    new
                    {
                        CreditCardNumber = creditCardNumber
                    });
            }
        }

        /// <summary>
        /// Retrieve Credit Card Holder By Number
        /// </summary>
        /// <param name="creditCardNumber">The Credit Card Number that identifies each Credit Card</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public string RetrieveCreditCardHolderByNumber(string creditCardNumber)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardHolder_Retrieve]",
                    new
                    {
                        CreditCardNumber = creditCardNumber
                    }).ToList().FirstOrDefault();

                return (result != null) ? result.CreditCardHolder : string.Empty;
            }
        }

        /// <summary>
        /// Retrieve CardFileParams
        /// </summary>        
        /// <returns></returns>
        public IEnumerable<CardFileParams> RetrieveCardFileParams()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CardFileParams>("[Control].[Sp_CardFileParams_Retrieve]", null);
            }
        }

        /// <summary>
        /// RetrieveCreditCardVPOS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<CreditCard> RetrieveCreditCardVPOS(string key)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCard>("[Control].[Sp_CreditCardVPOS_Retrieve]",
                    new
                    {
                        Key = key
                    });
            }
        }

        /// <summary>
        /// RetrieveCreditCardVPOS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<CreditCardTransaction> RetrieveTransactionVOSForReverse()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CreditCardTransaction>("[Control].[Sp_TransactionsVPOSForReverse_Retrieve]", null);
            }
        }

        /// <summary>
        /// RetrieveCreditCardVPOS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string RetrieveCreditCardNumber(int creditCardId)
        {
            using (var dba = new DataBaseAccess())
            {
                return ECOsystem.Utilities.TripleDesEncryption.Decrypt(dba.ExecuteScalar<string>("[Control].[Sp_CreditCardNumber_Retrieve]",
                    new
                    {
                        CreditCardId = creditCardId
                    }));
            }
        }

        /// <summary>
        /// RetrieveCreditCardVPOS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public int RetrieveIDCostumerbyCreditCardNumber(long creditCardNumber)
        {
            using (var dba = new DataBaseAccess())
            {
                try
                {
                    //TripleDesEncryption.Encrypt(Miscellaneous.ConvertCreditCardToInternalFormat((long)data.cardNumber))

                    string cc_number = ECOsystem.Utilities.TripleDesEncryption.Encrypt(ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat(creditCardNumber));

                    return dba.ExecuteScalar<int>("[Control].[Sp_CustomerByCreditCardNumber_Retrieve]",
                        new
                        {
                            CreditCardNumber = cc_number
                        });
                }
                catch (Exception)
                {
                    return 0; // Default return
                }

            }
        }
        /// <summary>
        /// RetrieveCreditCardVPOS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<CreditCardTransaction> RetrieveCreditCardTransactionVPOS(int creditCardId, string systemTraceNumber = "")
        {
            using (var dba = new DataBaseAccess())
            {
                var dataList = dba.ExecuteReader<CreditCardTransaction>("[Control].[Sp_CreditCardTransaction_Retrieve]",
                    new
                    {
                        CreditCardId = creditCardId,
                        SystemTraceNumber = (string.IsNullOrEmpty(systemTraceNumber)) ? null : systemTraceNumber
                    });

                foreach (var item in dataList)
                {
                    item.TransactionSaleList = dba.ExecuteReader<TransactionSale>("[Control].[Sp_CreditCardTransactionSALE_Retrieve]",
                        new
                        {
                            CreditCardId = creditCardId
                        }).ToList();
                }

                return dataList;
            }
        }

        /// <summary>
        /// AddCreditCardTransactionVPOS
        /// </summary>
        /// <param name="creditCardTransaction"></param>
        /// <returns></returns>
        public CreditCardTransaction AddOrEditCreditCardTransactionVPOS(CreditCardTransaction model)
        {
            using (var dba = new DataBaseAccess())
            {
                string invoiceTemp = (model.TransactionType.Contains("VOID")) ? model.Invoice : string.Empty;

                model.Invoice = dba.ExecuteScalar<int>("[Control].[Sp_CreditCardTransaction_AddOrEdit]",
                    new
                    {
                        model.CCTransactionVPOSId,
                        model.CreditCardId,
                        model.Customer,
                        model.TransactionType,
                        model.TerminalId,
                        model.Invoice,
                        model.EntryMode,
                        model.AccountNumber,
                        model.ExpirationDate,
                        model.TotalAmount,
                        model.Odometer,
                        model.Liters,
                        model.Plate,
                        model.AuthorizationNumber,
                        model.ReferenceNumber,
                        model.SystemTraceNumber,
                        model.LoggedUserId,
                        BacId = model.BACAfiliado,
                        model.DriverCode
                    }).ToString();

                model.CCTransactionVPOSId = Convert.ToInt32(model.Invoice);
                model.Invoice = (model.TransactionType.Contains("VOID")) ? invoiceTemp : model.Invoice;
            }
            return model;
        }

        /// <summary>
        /// Add Log for Transaction VPOS 
        /// </summary>
        /// <param name="CCTransactionVPOSId"></param>
        /// <param name="ResponseCode"></param>
        /// <param name="ResponseCodeDescription"></param>
        /// <param name="Message"></param>
        /// <param name="IsSuccess"></param>
        /// <param name="IsTimeOut"></param>
        /// <param name="IsCommunicationError"></param>
        /// <returns></returns>
        public int AddLogTransactionsVPOS(int CCTransactionVPOSId, string ResponseCode, string ResponseCodeDescription, string Message, bool IsSuccess, bool IsTimeOut, bool IsCommunicationError)
        {
            try
            {
                int LogID;

                using (var dba = new DataBaseAccess())
                {
                    LogID = dba.ExecuteScalar<int>("[Control].[Sp_LogTransactionsVPOS_Add]",
                     new
                     {
                         CCTransactionVPOSId,
                         ResponseCode,
                         ResponseCodeDescription,
                         Message,
                         IsSuccess,
                         IsTimeOut,
                         IsCommunicationError
                     });

                }
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }

        }


        /// <summary>
        /// Add Log for Transaction POS 
        /// </summary>
        /// <param name="CCTransactionPOSId"></param>
        /// <param name="ResponseCode"></param>
        /// <param name="ResponseCodeDescription"></param>
        /// <param name="Message"></param>
        /// <param name="IsSuccess"></param>
        /// <param name="IsTimeOut"></param>
        /// <param name="IsCommunicationError"></param>
        /// <returns></returns>
        public int AddLogTransactionsPOS(int? CCTransactionPOSId, string ResponseCode, string ResponseCodeDescription, string Message, string TransportData, bool IsSuccess, bool IsFail, int CustomerID = 0)

        {
            try
            {
                int LogID;

                using (var dba = new DataBaseAccess())
                {
                    LogID = dba.ExecuteScalar<int>("[Control].[Sp_LogTransactionsPOS_Add]",
                     new
                     {
                         CCTransactionPOSId,
                         ResponseCode,
                         ResponseCodeDescription,
                         Message,
                         TransportData,
                         IsSuccess,
                         IsFail,
                         CustomerID
                     });

                }
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Retrieve CreditCard Balance for API
        /// </summary>
        /// <param name="creditCardNumber">Credit Card Number</param>
        /// <param name="expirationDate">Expiration Date</param>
        /// <returns></returns>
        public decimal RetrieveCreditCardBalance(long creditCardNumber, int? expirationDate)
        {
            int? year = null;
            int? mont = null;

            if (expirationDate != null)
            {
                var expiration = expirationDate.ToString();
                year = expiration.Length == 4 ? int.Parse(expiration.Substring(0, 2)) + 2000 : 0;
                mont = expiration.Length == 4 ? int.Parse(expiration.Substring(2, 2)) : 0;
            }

            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteScalar<decimal?>("[Control].[Sp_CreditCard_Balance_Retrieve]",
                    new
                    {
                        CreditCardNumber = TripleDesEncryption.Encrypt(ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat(creditCardNumber)),
                        ExpirationYear = year,
                        ExpirationMonth = mont
                    });
                return result ?? 0.00m;
            }
        }

        /// <summary>
        /// Retrieve Card Request Detail
        /// </summary>
        /// <param name="cardRequestId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CardRequest> RetrieveCardRequest(int? cardRequestId, string key)
        {
            using (var business = new CustomersBusiness())
            {
                return business.RetrieveCustomerCardRequest(Session.GetCustomerId(), cardRequestId, true, key);
            }
        }

        /// <summary>
        /// Retrieve Card Request Detail
        /// </summary>
        /// <param name="cardRequestId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CardRequest> RetrieveCardRequestNoRequest(int customerId, int? cardRequestId, string key)
        {
            using (var business = new CustomersBusiness())
            {
                return business.RetrieveCustomerCardRequestNoCustomer(customerId, cardRequestId, true, key);
            }
        }

        /// <summary>
        /// Retrieve Card Request Detail
        /// </summary>
        /// <param name="cardRequestId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public CreditCardDetails RetrieveCardRequestDetail(int? cardRequestId)
        {
            var result = new CreditCardDetails();
            using (var business = new CustomersBusiness())
            {
                var customer = business.RetrieveCustomers(Session.GetCustomerId(), Session.GetCountryId()).FirstOrDefault();
                customer = customer ?? new Customers();
                var currentDate = DateTimeOffset.Now;

                result.RequestInfo = business.RetrieveCustomerCardRequest(Session.GetCustomerId(), cardRequestId, true).FirstOrDefault();

                result.CardInfo = new CreditCard()
                {
                    CurrencySymbol = customer.CurrencySymbol,
                    IssueForId = customer.IssueForId,
                    ExpirationMonth = currentDate.Month,
                    ExpirationYear = currentDate.Year + 4,
                    CardRequestId = result.RequestInfo != null ? result.RequestInfo.CardRequestId : null,
                    VehiclePlate = result.RequestInfo.PlateId,
                    UserId = result.RequestInfo.DriverUserId
                };
            }
            using (var business = new CustomerCreditsAdminBusiness())
            {
                result.CreditInfo = business.RetrieveCustomerCredits().FirstOrDefault();
            }
            return result;
        }

        /// <summary>
        /// Retrieve Card Detail
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public CreditCardDetails RetrieveCardDetail(int? creditCardId)
        {
            var result = new CreditCardDetails();
            using (var business = new CustomersBusiness())
            {
                result.CardInfo = RetrieveCreditCard(creditCardId).FirstOrDefault();
                result.RequestInfo = business.RetrieveCustomerCardRequest(result.CardInfo.CustomerId, result.CardInfo.CardRequestId).FirstOrDefault();
            }
            using (var business = new CustomerCreditsAdminBusiness())
            {
                result.CreditInfo = business.RetrieveCustomerCredits(customerId: result.CardInfo.CustomerId).FirstOrDefault();
            }
            return result;
        }


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CreditCard
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditCreditCard(CreditCard model)
        {
            var currentDate = DateTimeOffset.Now; //to update CustomerCredits on SP

            var customer = Session.GetCustomerInfo();
            model.StatusId = 0;
            if (model.CreditCardId == null)
            {
                //var partnerId = (customer.)
                model.CreditCardNumberId = GetNextCreditCardNumber(model.CreditCardType ?? (customer != null ? customer.CreditCardType : null)
                            , customer.CustomerId);
                //, model.PartnerId ?? Session.GetPartnerId() ?? -1);
            }

            //Get the vehicleId based on the plate in the request
            if (!string.IsNullOrEmpty(model.VehiclePlate))
            {
                using (var bus = new VehiclesBusiness())
                {
                    model.VehicleId = bus.RetrieveVehicleByPlate(model.VehiclePlate, (int)customer.CustomerId).FirstOrDefault().VehicleId;
                }
            }

            model.UserId = model.UserId == 0 ? null : model.UserId;

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_CreditCard_AddOrEdit]",
                    new
                    {
                        model.CreditCardId,
                        model.CreditCardNumberId,
                        model.CreditLimit,
                        model.ExpirationMonth,
                        model.ExpirationYear,
                        model.StatusId,
                        model.CardRequestId,
                        CustomerId = model.CustomerId ?? Session.GetCustomerId(),
                        currentDate.Year,
                        currentDate.Month,
                        UserId = model.UserId == -1 ? null : model.UserId,
                        VehicleId = model.VehicleId == -1 ? null : model.VehicleId,
                        model.LoggedUserId,
                        Pin = model.CreditCardId == null ? ECOsystem.Utilities.Miscellaneous.GeneratePin() : model.Pin,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Insert credit cards massively
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains id property to perform insert operation</param>
        public void AddCreditCardsMassively(List<int> model)
        {
            using (var dba = new DataBaseAccess())
            {
                var r = dba.ExecuteNonQuery("[Control].[Sp_CreditCard_AddMassivelyFromRequest]",
                    new
                    {
                        Requests = ECOsystem.Utilities.Miscellaneous.GetXML(model),
                        CustomerId = Session.GetCustomerId(),
                        Session.GetUserInfo().UserId
                    });
            }
        }


        /// <summary>
        /// Performs the maintenancer Update of the entity CreditCard
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void EditCreditCard(CreditCard model)
        {
            var currentDate = DateTimeOffset.Now;

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_CreditCard_AddOrEdit]",
                    new
                    {
                        model.CreditCardId,
                        model.CreditLimit,
                        model.ExpirationMonth,
                        model.ExpirationYear,
                        CustomerId = Session.GetCustomerId(),
                        currentDate.Year,
                        currentDate.Month,
                        model.StatusId,
                        UserId = model.UserId == -1 ? null : model.UserId,
                        VehicleId = model.VehicleId == -1 ? null : model.VehicleId,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Change the status of the credit card
        /// </summary>
        /// <param name="IdList"></param>
        /// <param name="StatusId"></param>
        public void EditCreditCardStatus(List<int> IdList, int StatusId, List<int> TypeList)
        {
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;

            for (int i = 0; i < IdList.Count; i++)
            {
                root.Add(new XElement("CreditCardId", new XAttribute("Id", "" + IdList[i]), new XAttribute("TypeId", "" + TypeList[i])));
            }

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_CreditCardStatus_AddOrEdit]",
                    new
                    {
                        XMLDoc = doc.ToString(),
                        StatusId,
                        CustomerName = Session.GetCustomerInfo().DecryptedName,
                        Session.GetUserInfo().UserId
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model CreditCard
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteCreditCard(int creditCardId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_CreditCard_Delete]",
                    new { CreditCardId = creditCardId });
            }
        }

        /// <summary>
        /// Get Next Credit Card Number
        /// </summary>
        /// <param name="creditCardType"></param>
        /// <param name="partnerId"></param>
        /// <returns>Credit Card Number Id</returns>
        //private int GetNextCreditCardNumber(string creditCardType, int partnerId)
        private int GetNextCreditCardNumber(string creditCardType, int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = 0;
                //while (result == 0)
                //{
                //int cardIdRandom = GetCreditCardRandomIndexByPartner(partnerId);
                result = dba.ExecuteScalar<int>("[Control].[Sp_CreditCardNumbers_Retrieve]",
                new
                {
                    //CreditCardNumberId =  cardIdRandom,
                    CreditCardType = creditCardType,
                    CustomerId = customerId
                });

                if (result == 0)
                {
                    throw new Exception("ERR-CARD-NOT-AVAILABLE");
                }

                //}
                return result;
            }
        }

        /// <summary>
        /// Get Credit Card Random Index By Partner
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        private int GetCreditCardRandomIndexByPartner(int partnerId)
        {
            return new Random().Next(1, 1000);

            /* Tmp return random -- pending until get partnerId from cust user
            switch (partnerId)
            {
                case 4:
                    return new Random().Next(1, 1000);
                default:
                    throw new NotImplementedException("PartnerId: " + partnerId);
            }*/

        }

        /// <summary>
        /// Return State Business Rules for transactions
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public IEnumerable<TransactionRulesModel> RetrieveTransactionRules(int? customerId, int? VehicleId, int? CostCenterId)
        {
            using (var dba = new DataBaseAccess())
            {
                List<TransactionRulesModel> result = dba.ExecuteReader<TransactionRulesModel>("[Control].[Sp_TransactionRules_Retrieve]",
                new
                {
                    customerId,
                    VehicleId,
                    CostCenterId
                }).ToList();

                foreach (var item in result)
                {
                  item.Binnacle = new List<TransactionRuleBinnacle>();
                }                   

                return result.AsEnumerable();
            }
        }

        /// <summary>
        /// Add or edit Transaction Rule Status
        /// </summary>
        /// <param name="rule"></param>
        /// <returns></returns>
        public int AddOrEditTransactionRule(TransactionRulesModel rule)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[Control].[Sp_TransactionRules_AddOrEdit]",
                     new
                     {
                         rule.CustomerId,
                         rule.RuleCustomerId,
                         rule.RuleId,
                         rule.RuleActive,
                         rule.RuleValue,
                         rule.UserId,
                         rule.VehicleId,
                         rule.CostCenterId,
                         rule.RuleVehicleId,
                         rule.RuleCostCenterId
                     });
                }
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Save the Parameters for the transactions rules by customer
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="TransactionId"></param>
        public void AddOrEditTRParametersByCustomer(string Value, int TransactionId)
        {
            if (TransactionId == 10)
            {
                var rangeValues = Value.Split('-');
                int result;
                if (!int.TryParse(rangeValues[0], out result) || !int.TryParse(rangeValues[1], out result))
                    throw new Exception("Invalid range parameters");
            }
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ParametersForTransactionRulesAddOrEdit]",
                 new
                 {
                     Value,
                     ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                     TransactionId,
                     ECOsystem.Utilities.Session.GetUserInfo().UserId
                 });
            }
        }

        /// <summary>
        /// Retrieve the parameters information 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TransactionRulesParameters> TRParametersRetrieve(int? CustomerId)
        {
            if (CustomerId == null)
            {
                CustomerId = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId;
            }

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<TransactionRulesParameters>("[General].[Sp_GetParametersForTransactionRules]",
                 new
                 {
                     CustomerId
                 });
            }
        }

        /// <summary>
        /// Get the result from the database validating if the rule can be applied
        /// </summary>
        /// <returns></returns>
        public int ValidateTankCapacityRule()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_ValidateTankCapacityRule]",
                new
                {
                    ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId
                });
            }
        }

        public void DailyTransactionLimitAddOrEdit(DailyTransactionLimitAddOrEdit Model)
        {
            using (var dba = new DataBaseAccess())
            {
                var list = new List<DailyTransactionLimitAddOrEdit>();
                list.Add(Model);

                dba.ExecuteNonQuery("[General].[Sp_DailyTransactionLimit_AddOrEdit]",
                new
                {
                    XMLData = ECOsystem.Utilities.Miscellaneous.GetXML(list, typeof(DailyTransactionLimitAddOrEdit).Name),
                    Session.GetCustomerInfo().CustomerId,
                    Session.GetUserInfo().UserId
                });
            }
        }

        /// <summary>
        /// Get the odometer last by vehicle
        /// </summary>
        /// <param name="VehicleId"></param>
        /// <param name="Date"></param>
        /// <returns></returns>
        public int GetOdometerLastByVehicle(int VehicleId, DateTime Date)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_GetOdometerLastByVehicle]",
                new
                {
                    VehicleId,
                    Date
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model CreditCard
        /// </summary>
        /// <param name="creditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteTransactionRules(int? VehicleId, int? CostCenterId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_Transaction_Rules_Delete]", new
                {
                    VehicleId,
                    CostCenterId
                });
            }
        }

        public IEnumerable<Status> RetrieveStatusofCreditsCards()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Status>("[General].[Sp_Status_Retrieve]", new { });
            }
        }

        public bool ValidateTransactionRules(List<TransactionRulesModel> rules)
        {
            try
            {
                return rules.Find(x => x.RuleId == PlateCreditCard).RuleActive != rules.Find(x => x.RuleId == DriverCodeCreditCard).RuleActive;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<TransactionRuleBinnacle> BinnacleRetrieve(int? RuleId, int? VehicleId, int? CostCenterId)
        {
            using (var dba = new DataBaseAccess())
            {
               return dba.ExecuteReader<TransactionRuleBinnacle>("[Control].[Sp_TransactionRuleBinnacle_Retrieve]",
               new
               {
                   CustomerId = Session.GetCustomerId(),
                   RuleId,
                   VehicleId,
                   CostCenterId,
               }).ToList();
            }
        }

        public bool ValidatesIVEDownload(int? CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<bool>("[General].[Sp_ValidatesIVEDownload_Retrieve]",
                new
                {
                    CustomerId             
                });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        public void AddorEditPaymentInstruments(CardRequest model) {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_PaymentMethod_AddOrEdit]",
                    new
                    {
                        model.CardRequestId,
                        model.PaymentMethod,
                        CustomerId = Session.GetCustomerId()
                    });
            }
        }

        public int GetStickersCount() {

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Control].[Sp_StickerCount_Retrieve]",new {});
            }

        }

        public class Methods {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public IEnumerable<Methods> GetPaymentMethods() {

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Methods>("[Control].[Sp_PaymentMethods_Retrieve]", new { });
            }

        }
    }
}