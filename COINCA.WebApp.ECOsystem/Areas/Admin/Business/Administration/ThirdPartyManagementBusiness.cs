﻿using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;


namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Get data info ThirdPartyManagement
    /// </summary>
    public class ThirdPartyManagementBusiness : IDisposable
    {
        /// <summary>
        /// Get drivers with CellPhones
        /// for eneable whastapp Messege
        /// </summary>
        /// <returns>Users List with CellPhone</returns>
        public IEnumerable<Users> DriversCellsPhoneRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Users>("[General].[DriversCellsPhone_Retrieve]", new { ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId });
            }
        }

        /// <summary>
        /// Update Drivers with recive 
        /// message with whatsapp
        /// </summary>
        /// <param name="listDrivers"></param>
        /// <returns>Users List with CellPhone</returns>
        public IEnumerable<Users> DriversCellsPhoneAddOrEdit(IEnumerable<UsersWhatsApp> listDrivers)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Users>("[General].[DriversCellsPhone_AddOrEdit]", 
                    new { 
                        ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId 
                    });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}