﻿/************************************************************************************************************
*  File    : StatusBusiness.cs
*  Summary : Status Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
//using COINCA.Library.DataAccess;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Parameters Business Class
    /// </summary>
    public class StatusBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Parameters
        /// </summary>
        /// <param name="usage"></param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Status> RetrieveStatus(string usage)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Status>("[General].[Sp_Status_Retrieve]", new
                {
                    Usage = usage
                });
            }
        }

        
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}