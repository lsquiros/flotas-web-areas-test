﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceVehicleBusiness.cs
*  Summary : PreventiveMaintenanceVehicle Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 02/20/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Utilities;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Admin.Models;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    public class PreventiveMaintenanceVehicleBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PreventiveMaintenanceVehicle> RetrieveVehicles(int? vehicleId, string key = null)
        {
            
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<PreventiveMaintenanceVehicle>("[General].[Sp_Vehicles_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        Key = key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PreventiveMaintenanceVehicleDetail> RetrievePreventiveMaintenanceByVehicle(int? vehicleId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();
                return dba.ExecuteReader<PreventiveMaintenanceVehicleDetail>("[General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Return a list of Preventive Maintenance Catalog not Asigned to Vehicle for populate DropDownListFor
        /// </summary>
        public IEnumerable<PreventiveMaintenanceVehicleDetail> PreventiveMaintenanceCatalogList(int? VehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<PreventiveMaintenanceVehicleDetail>("[General].[Sp_PreventiveMaintenanceCatalogList_Retrieve]",
                    new
                    {
                        VehicleId = VehicleId,
                        CustomerId = Session.GetCustomerId(),
                    });
                return result;
            }
        }

                /// <summary>
        /// Performs the maintenance Insert or Update of the entity Preventive Maintenance By Vehicle
        /// </summary>
        /// <param name="model">Model or class passed as parameter which contains all properties to perform insert or update operations</param>
        public void AddOrEditPreventiveMaintenanceRecordVehicle(PreventiveMaintenanceVehicleAssign model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]",
                    new
                    {
                        VehicleId = model.VehicleId,
                        PreventiveMaintenanceCatalogId = model.PreventiveMaintenanceCatalogId,
                        LastReviewOdometer = model.LastReviewOdometer,
                        LastReviewDate = model.LastReviewDate,
                        LoggedUserId = model.LoggedUserId,
                        RowVersion = model.RowVersion
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model users
        /// </summary>
        /// <param name="userId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeletePreventiveMaintenanceVehicle(int id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceByVehicle_Delete]",
                    new { PreventiveMaintenanceId = id });
            }
        }

        /// <summary>
        /// Retrieve Preventive Maintenance Record By Vehicle Detail
        /// </summary>
        /// <param name="customerId">The FK that identifies each row by Catalog of Users</param>
        /// <param name="preventiveMaintenanceCatalogId">The FK that identifies each row by Catalog of Preventive Type</param>
        /// <returns>IEnumerable collection of the PreventiveMaintenanceRecordByVehicle entity Model</returns>
        public PreventiveMaintenanceRecordByVehicleCostDetail PreventiveMaintenanceRecordByVehicleDetail(int? Id)
        {
            using (var dba = new DataBaseAccess())
            {
                PreventiveMaintenanceRecordByVehicleCostDetail model = new PreventiveMaintenanceRecordByVehicleCostDetail();
                var resultCost = dba.ExecuteReader<PreventiveMaintenanceRecordByVehicleCost>("[Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]",
                new
                {
                    Id = Id
                });
                var resultHeader = dba.ExecuteReader<PreventiveMaintenanceRecordByVehicleCostHeader>("[Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]",
                new
                {
                    Id = Id
                });
                model.Cost = (List<PreventiveMaintenanceRecordByVehicleCost>)resultCost;
                model.Header = (PreventiveMaintenanceRecordByVehicleCostHeader)resultHeader.FirstOrDefault();
                return model;
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Operators
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditPreventiveMaintenanceRecordByVehicleDetail(List<string> model, int PreventiveMaintenanceId, DateTime? Date, double Odometer, int Apply)
        {
            JavaScriptSerializer jsTool = new JavaScriptSerializer();
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            foreach (string item in model)
            {
                var record = jsTool.Deserialize<PreventiveMaintenanceRecordByVehicleCost>(item);
                root.Add(new XElement("PreventiveMaintenanceRecordByVehicleCostDetail", 
                    new XAttribute("PreventiveMaintenanceRecordByVehicleDetailId", 
                    record.PreventiveMaintenanceRecordByVehicleDetailId), 
                    new XAttribute("PreventiveMaintenanceCostId", 
                    record.PreventiveMaintenanceCostId), 
                    new XAttribute("Description", record.Description), 
                    new XAttribute("Cost", record.Cost), 
                    new XAttribute("Record", record.Record),
                    new XAttribute("ServiceProvider", record.ServiceProvider),
                    new XAttribute("InvoiceNumber", record.InvoiceNumber),
                    new XAttribute("MaintenanceTime", record.MaintenanceTime),
                    new XAttribute("Comments", record.Comments),
                    new XAttribute("CostDollar", record.CostDollar)
                    ));
            }
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]",
                    new
                    {
                        XmlData = doc.ToString(),
                        PreventiveMaintenanceId = PreventiveMaintenanceId,
                        Date = Date,
                        Odometer = Odometer,
                        Apply = Apply,
                        LoggedUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }

        }

        public void DeleteRecord(int id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]",
                    new
                    {
                        Id = id,
                        LoggedUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }


        /// <summary>
        /// Retrieve Preventive Maintenance information
        /// </summary>
        /// <param name="customerId">The Primary Key uniquely that identifies each customer</param>
        /// <param name="preventiveMaintenanceCatalogId">The Primary Key uniquely that identifies each preventive maintenance</param>
        /// <param name="key">Key is the search</param>
        /// <returns>IEnumerable collection of the PreventiveMaintenance entity model</returns>
        public IEnumerable<PreventiveMaintenanceCatalog> RetrievePreventiveMaintenance(int? customerId, int? FilterType, int? preventiveMaintenanceCatalogId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PreventiveMaintenanceCatalog>("[General].[Sp_PreventiveMaintenanceCatalog_Retrieve]",
                    new
                    {
                        CustomerId = customerId,
                        FilterType,
                        PreventiveMaintenanceCatalogId = preventiveMaintenanceCatalogId,
                        Key = key
                    });
            }
        }

        public IEnumerable<PreventiveMaintenanceRecordByVehicleCost> RetrieveMaintenanceDetail(string xml)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PreventiveMaintenanceRecordByVehicleCost>("[General].[SP_Retrieve_MaintenanceDetail]",
                    new
                    {
                        xml
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}








