﻿/************************************************************************************************************
*  File    : VehicleCategoriesBusiness.cs
*  Summary : VehicleCategories Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
//using COINCA.Library.DataAccess;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;
using System.Globalization;
using System.Data;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Vehicle Categories Business Class
    /// </summary>
    public class VehicleCategoriesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleCategories
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        ///   <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleCategories> RetrieveVehicleCategories(int? vehicleCategoryId,bool opc=false, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                
                return dba.ExecuteReader<VehicleCategories>("[General].[Sp_VehicleCategories_Retrieve]",
                    new
                    {
                        VehicleCategoryId = vehicleCategoryId,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleCategories
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleCategories(VehicleCategories model)
        {
            model.DefaultPerformance = Convert.ToDecimal(model.DefaultPerformanceStr.Replace(',','.'));
            model.Weight = Convert.ToDecimal(string.Join("", model.WeightStr.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries)).Replace(',', '.'));

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleCategories_AddOrEdit]",
                    new
                    {
                        model.VehicleCategoryId,
                        model.Manufacturer,
                        model.Type,
                        model.DefaultFuelId,
                        model.Liters,
                        model.VehicleModel,
                        CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                        model.Icon, 
                        model.MaximumSpeed, 
                        model.MaximumRPM,
                        model.Year,
                        model.Weight,
                        model.LoadType,
                        model.CylinderCapacity,
                        model.FuelsListXml,
                        model.LoggedUserId,
                        model.RowVersion,
                        model.DefaultPerformance,
                        model.SpeedDelimited
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleCategories
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehicleCategories(int vehicleCategoryId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleCategories_Delete]",
                    new { VehicleCategoryId = vehicleCategoryId,
                          ModifyUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }
          
        /// <summary>
        /// Vehicles Generate
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable ReportData(List<VehicleCategories> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("Manufacturer");
                dt.Columns.Add("Type");
                dt.Columns.Add("VehicleModel");
                dt.Columns.Add("Year");
                dt.Columns.Add("FuelName");
                dt.Columns.Add("Liters");
                dt.Columns.Add("MaximumSpeed");
                dt.Columns.Add("DefaultPerformance");
                dt.Columns.Add("CustomerName");
                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();
                    row["Manufacturer"] = item.Manufacturer;
                    row["Type"] = item.Type;
                    row["VehicleModel"] = item.VehicleModel;
                    row["Year"] = item.Year;
                    row["FuelName"] = item.FuelName;
                    row["Liters"] = item.Liters;
                    row["MaximumSpeed"] = item.MaximumSpeed;
                    row["DefaultPerformance"] = item.DefaultPerformance;
                    row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    dt.Rows.Add(row);
                }
                return dt;
            }
        } 

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}