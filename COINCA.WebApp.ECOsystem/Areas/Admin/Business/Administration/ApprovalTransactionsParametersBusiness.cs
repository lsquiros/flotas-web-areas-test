﻿using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    public class ApprovalTransactionsParametersBusiness : IDisposable
    {
        #region Retrieve Info
        public ApprovalTransactionsParametersBase RetrieveInformation()
        {
            var ret = GetXMLParameters() ?? new ApprovalTransactionsParametersRetrieve();
            return new ApprovalTransactionsParametersBase()
                        {
                            List = string.IsNullOrEmpty(ret.XMLData) ? new List<ApprovalTransactionsParameters>() : 
                                                                       ECOsystem.Utilities.Miscellaneous.GetXMLDeserialize<List<ApprovalTransactionsParameters>>(ret.XMLData, typeof(ApprovalTransactionsParameters).Name),
                            MaxId = ret.MaxId
                        };
        }

        ApprovalTransactionsParametersRetrieve GetXMLParameters()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ApprovalTransactionsParametersRetrieve>("[General].[Sp_ApprovalTransactionsColumn_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    }).FirstOrDefault();
            }
        }
        #endregion

        public void ColumnsAddOrEdit(List<ApprovalTransactionsParameters> list, int MaxId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ApprovalTransactionsColumn_AddOrEdit]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        Xml = ECOsystem.Utilities.Miscellaneous.GetXML(list, typeof(ApprovalTransactionsParameters).Name),
                        MaxId,
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}