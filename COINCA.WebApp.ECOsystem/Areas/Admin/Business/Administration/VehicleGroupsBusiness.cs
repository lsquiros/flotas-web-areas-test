﻿/************************************************************************************************************
*  File    : VehicleGroupsBusiness.cs
*  Summary : VehicleGroups Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;
using System.Data;
using System.Web;
using System.Linq;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// VehicleGroups Class
    /// </summary>
    public class VehicleGroupsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleGroups
        /// </summary>
        /// <param name="vehicleGroupId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleGroups> RetrieveVehicleGroups(int? vehicleGroupId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<VehicleGroups>("[General].[Sp_VehicleGroups_Retrieve]",
                    new
                    {
                        VehicleGroupId = vehicleGroupId,
                        CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                        Key = key,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleGroups
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleGroups(VehicleGroups model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleGroups_AddOrEdit]",
                    new
                    {
                        model.VehicleGroupId,
                        model.Name,
                        model.Description,
                        CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleGroups
        /// </summary>
        /// <param name="vehicleGroupId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehicleGroups(int vehicleGroupId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleGroups_Delete]",
                    new { VehicleGroupId = vehicleGroupId });
            }
        }

        /// <summary>
        /// DownloadData
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataSet DownloadData(List<VehicleGroups> list)
        {
            DataSet ds = new DataSet();
            using (var dt = new DataTable("VehicleGroupsReport"))
            {
                dt.Columns.Add("VehicleGroupId");
                dt.Columns.Add("VehicleGroupName");
                dt.Columns.Add("VehiclesCount");
                dt.Columns.Add("CustomerName");           
                foreach (var vg in list)
                {
                    DataRow row = dt.NewRow();
                    row["VehicleGroupId"] = vg.VehicleGroupId;
                    row["VehicleGroupName"] = vg.Name;
                    row["VehiclesCount"] = vg.CantVehicles;
                    row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    dt.Rows.Add(row);    
                }
                ds.Tables.Add(dt);
            }
            var vehiclelist = RetrieveVehiclesByGroups(null).Where(m => list.Exists(x => (int)x.VehicleGroupId == m.VehicleGroupId));
            using (var dt = new DataTable("VehicleGroupsDetail"))
            {
                dt.Columns.Add("VehicleGroupId");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("Manufacturer");
                dt.Columns.Add("Model");
                dt.Columns.Add("Type");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("Unit");
                dt.Columns.Add("Driver");
                dt.Columns.Add("GPS");
                foreach (var v in vehiclelist)
                {
                    DataRow row = dt.NewRow();
                    row["VehicleGroupId"] = v.VehicleGroupId;
                    row["VehicleName"] = v.Name;
                    row["PlateId"] = v.PlateId;
                    row["Manufacturer"] = v.Manufacturer;
                    row["Model"] = v.VehicleModel;
                    row["Type"] = v.VehicleType;
                    row["CostCenterName"] = v.CostCenterName;
                    row["Unit"] = v.UnitName;
                    row["Driver"] = v.DecryptedUserName;
                    row["GPS"] = v.HasGPS;
                    dt.Rows.Add(row);
                }
                ds.Tables.Add(dt);
            }
            return ds;
        }

        /// <summary>
        /// Retrieve Vehicles
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Vehicles> RetrieveVehiclesByGroups(int? VehicleGroupId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Vehicles>("[Control].[Sp_VehiclesByGroups_Retrieve]",
                new
                {
                    VehicleGroupId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}