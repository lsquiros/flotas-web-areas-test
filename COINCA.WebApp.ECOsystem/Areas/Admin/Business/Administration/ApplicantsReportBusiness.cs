﻿/************************************************************************************************************
*  File    : ApplicantsReportBusiness.cs
*  Summary : ApplicantsReport Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 12/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using COINCA.Library.DataAccess;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using ECOsystem.Models.Core;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// ApplicantsReport Class
    /// </summary>
    public class ApplicantsReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Applicants Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of ApplicantsReportBase in order to load the chart</returns>
        public ApplicantsReportBase RetrieveApplicantsReport(AdministrationReportsBase parameters)
        {
            var result = new ApplicantsReportBase();
            GetReportData(parameters, result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            result.Parameters.CountryId = parameters.CountryId;
            result.Parameters.UserId = parameters.UserId;
            result.UsersList = new SelectList(RetrieveAdminPartnerUsersByCountry(result.Parameters.CountryId), "UserId", "DecryptedUserName", result.Parameters.UserId);
            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(AdministrationReportsBase parameters, ApplicantsReportBase result)
        {
            GetReportDataApplicants(parameters, result);
        }



        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataApplicants(AdministrationReportsBase parameters, ApplicantsReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<ApplicantsReport>("[Control].[Sp_ApplicantsReport_Retrieve]",
                    new
                    {
                        UserId = parameters.UserId,
                        CountryId = parameters.CountryId,
                        Year = parameters.Year,
                        Month = parameters.Month,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate
                    });
            }
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }

        /// <summary>
        /// Generate Real Vs Budget FuelsReport Excel
        /// </summary>
        /// <returns>A model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public byte[] GenerateApplicantsReportExcel(AdministrationReportsBase parameters)
        {
            var result = new ApplicantsReportBase();

            GetReportDataApplicants(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Reporte de Solicitantes", result.List.ToList());
            }

        }

        /// <summary>
        /// Return a list of Countries for current user in order to using it for populate DropDownListFor
        /// </summary>
        public IEnumerable<Users> RetrieveAdminPartnerUsersByCountry(int? CountryId)
        {
            var dictionary = new Dictionary<int?, string>();
            
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<Users>("[General].[Sp_AdminPartnerUsersByCountry_Retrieve]",
                    new
                    {
                        CountryId = CountryId,
                        PartnerId = Session.GetPartnerId()
                    });
                return result;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
