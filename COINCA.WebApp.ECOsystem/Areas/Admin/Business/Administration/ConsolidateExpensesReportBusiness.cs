﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    public class ConsolidateExpensesReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Transactions Sap
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public ConsolidateExpensesReportBase RetrieveConsolidateReport()
        {
            var result = new ConsolidateExpensesReportBase();
            result.List = (List<ConsolidateExpensesList>)HttpContext.Current.Session["ConsolidateReportData"];
            result.Menus = new List<AccountMenus>();
            return result;
        }

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="result"></param>
        /// <param name="parameters"></param>
        public IEnumerable<ConsolidateExpensesList> GetReportFuelData(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                var list = dba.ExecuteReader<ConsolidateExpensesList>("[General].[Sp_ConsolidateExpensesFuelReport_Retrieve]",
                new
                {
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    parameters.CostCenterId,
                    parameters.Year,
                    parameters.Month,
                    parameters.StartDate,
                    parameters.EndDate,
                    parameters.ShowGPS,
                    parameters.ShowZero,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
                HttpContext.Current.Session["ConsolidateReportData"] = list;
                return list;
            }
        }

        private IEnumerable<ConsolidateExpensesList> GetReportMaintenanceData(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ConsolidateExpensesList>("[General].[Sp_ConsolidateExpensesMaintenanceReport_Retrieve]",
                new
                {
                    CustomerId = Session.GetCustomerId(),
                    parameters.CostCenterId,
                    parameters.Year,
                    parameters.Month,
                    parameters.StartDate,
                    parameters.EndDate,
                    parameters.ShowGPS,
                    parameters.ShowZero,
                    Session.GetUserInfo().UserId
                });
            }
        }     

        public DataSet PreventiveMaintenanceReportGenerate(ControlFuelsReportsBase parameters)
        {
            var ds = new DataSet();

            var FuelList = GetReportFuelData(parameters);
            var MaintenanceList = GetReportMaintenanceData(parameters);

            using (var dt = new DataTable("ConsolidateExpensesReport"))
            {
                var customerName = Session.GetCustomerInfo().DecryptedName;
                var dates = HttpContext.Current.Session["RangoFechas"].ToString();
                var HasColumns = MaintenanceList.ToList().Count() > 0 ? "True" : "False";

                //Transaction Part
                dt.Columns.Add("VehicleId");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("HasGPS");
                dt.Columns.Add("Travel");
                dt.Columns.Add("Liters");
                dt.Columns.Add("Performance");
                dt.Columns.Add("FuelCost");                
                //Sum                
                //dt.Columns.Add("TotalCost");
                dt.Columns.Add("CostFuelPerKilometer");
                //dt.Columns.Add("CostMaintenancePerKilometer");
                //Client data
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Dates");
                dt.Columns.Add("HasColumns");

                foreach (var item in FuelList)
                {
                    var drCosolidateReport = dt.NewRow();

                    drCosolidateReport["VehicleId"] = item.VehicleId;
                    drCosolidateReport["VehicleName"] = item.VehicleName;
                    drCosolidateReport["PlateId"] = item.PlateId;
                    drCosolidateReport["HasGPS"] = item.HasGPS;
                    drCosolidateReport["Travel"] = item.Travel.ToString("0.00");
                    drCosolidateReport["Liters"] = item.Liters;
                    drCosolidateReport["Performance"] = item.Performance;
                    drCosolidateReport["FuelCost"] = item.FuelCost;                    
                    //drCosolidateReport["TotalCost"] = 0;
                    drCosolidateReport["CostFuelPerKilometer"] = item.CostPerKilometer;
                    //drCosolidateReport["CostMaintenancePerKilometer"] = 0;
                    drCosolidateReport["CustomerName"] = customerName;
                    drCosolidateReport["Dates"] = dates;
                    drCosolidateReport["HasColumns"] = HasColumns;

                    dt.Rows.Add(drCosolidateReport);
                }

                ds.Tables.Add(dt);
            }

            if (MaintenanceList.ToList().Count() > 0)
            {

                using (var dt = new DataTable("ConsolidateExpensesDetail"))
                {
                    dt.Columns.Add("Titles");
                    dt.Columns.Add("MaintenanceId");
                    dt.Columns.Add("VehicleId");

                    foreach (var item in MaintenanceList)
                    {
                        DataRow dr = dt.NewRow();
                        dr["Titles"] = item.MaintenanceName;
                        dr["MaintenanceId"] = item.MaintenanceId;
                        dr["VehicleId"] = item.VehicleId;
                        dt.Rows.Add(dr);
                    }
                    ds.Tables.Add(dt);
                }
                using (var dt = new DataTable("ConsolidateExpensesColumnValues"))
                {
                    //Maintenance Part
                    dt.Columns.Add("MaintenanceId");
                    dt.Columns.Add("MaintenanceCost");
                    dt.Columns.Add("MaintenanceType");
                    dt.Columns.Add("VehicleId");
                    foreach (var item in MaintenanceList)
                    {

                        DataRow dr = dt.NewRow();
                        dr["MaintenanceId"] = item.MaintenanceId;
                        dr["MaintenanceCost"] = item.MaintenanceCost;
                        dr["MaintenanceType"] = item.MaintenanceType;
                        dr["VehicleId"] = item.VehicleId;
                        dt.Rows.Add(dr);                       
                    }
                    ds.Tables.Add(dt);
                }
            }
            else
            {
                ds.Tables.Add(new DataTable("ConsolidateExpensesDetail"));
                ds.Tables.Add(new DataTable("ConsolidateExpensesColumnValues"));
            }
            return ds;
        }

        /// <summary>
        /// ReportGenerate
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private DataTable ReportGenerate(IEnumerable<ConsolidateExpensesList> list, string tableName)
        {
            using (var dt = new DataTable(tableName))
            {
                var customerName = Session.GetCustomerInfo().DecryptedName;
                var dates = HttpContext.Current.Session["RangoFechas"].ToString();
                //Transaction Part
                dt.Columns.Add("VehicleId");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("HasGPS");
                dt.Columns.Add("Travel");
                dt.Columns.Add("Liters");
                dt.Columns.Add("Performance");
                dt.Columns.Add("FuelCost");
                //Maintenance Part
                dt.Columns.Add("MaintenanceId");
                dt.Columns.Add("MaintenanceName");
                dt.Columns.Add("MaintenanceCost");
                dt.Columns.Add("MaintenanceSubtotal");
                dt.Columns.Add("MaintenanceType");
                //Sum                
                dt.Columns.Add("TotalCost");
                dt.Columns.Add("CostFuelPerKilometer");
                dt.Columns.Add("CostMaintenancePerKilometer");
                //Client data
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("Dates");
                foreach (var item in list)
                {
                    var drCosolidateReport = dt.NewRow();
                    drCosolidateReport["VehicleId"] = item.VehicleId;
                    drCosolidateReport["VehicleName"] = item.VehicleName;
                    drCosolidateReport["PlateId"] = item.PlateId;
                    drCosolidateReport["HasGPS"] = item.HasGPS;
                    drCosolidateReport["Travel"] = item.Travel.ToString("0.00");
                    drCosolidateReport["Liters"] = item.Liters;
                    drCosolidateReport["Performance"] = item.Performance;
                    drCosolidateReport["FuelCost"] = item.FuelCost;
                    drCosolidateReport["MaintenanceId"] = item.MaintenanceId;
                    drCosolidateReport["MaintenanceName"] = item.MaintenanceName;
                    drCosolidateReport["MaintenanceCost"] = item.MaintenanceCost;
                    drCosolidateReport["MaintenanceSubtotal"] = item.MaintenanceSubtotal;
                    drCosolidateReport["MaintenanceType"] = item.MaintenanceType;
                    drCosolidateReport["TotalCost"] = 0;
                    drCosolidateReport["CostFuelPerKilometer"] = item.CostPerKilometer;
                    drCosolidateReport["CostMaintenancePerKilometer"] = 0;
                    drCosolidateReport["CustomerName"] = customerName;
                    drCosolidateReport["Dates"] = dates;
                    
                    dt.Rows.Add(drCosolidateReport);
                }

                return dt;
            }
        }

        /// <summary>
        /// GC Method
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}