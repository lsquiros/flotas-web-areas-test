﻿/************************************************************************************************************
*  File    : GPSReportBusiness.cs
*  Summary : GPS Business Methods
*  Author  : Stefano Quirós
*  Date    : 20/04/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Core;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    public class GPSReportBusiness : IDisposable
    {

        /// <summary>
        /// Retrieve Applicants Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of CreditCardsReportBase in order to load the chart</returns>
        public GPSReportBase RetrieveGPSReport(AdministrationReportsBase parameters)
        {
            var result = new GPSReportBase();

            GetReportData(parameters, result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            result.Parameters.CountryId = parameters.CountryId;
            result.Parameters.UserId = parameters.UserId;

            var custList = RetrieveCustomersByCountry(result.Parameters.CountryId);
            var newCustList = new List<Customers>();
            var custTypeAll = custList.Where(w => w.DecryptedName == "Todos").FirstOrDefault();

            newCustList.Add(custTypeAll);
            newCustList.AddRange(custList.Where(w => w.DecryptedName != "Todos").ToList());
            result.UsersList = new SelectList(newCustList, "CustomerId", "DecryptedName", result.Parameters.UserId);

            return result;
        }


        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(AdministrationReportsBase parameters, GPSReportBase result)
        {
            GetReportDataApplicants(parameters, result);
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataApplicants(AdministrationReportsBase parameters, GPSReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<GPSReportByClient>("[Control].[Sp_GPSReport_Retrieve]",
                    new
                    {
                        PartnerId = parameters.PartnerId,
                        CustomerId = parameters.CustomerId,
                        CountryId = parameters.CountryId,
                        Year = parameters.Year,
                        Month = parameters.Month,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate,
                    });
            }
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }

        /// <summary>
        /// Return a list of Countries for current user in order to using it for populate DropDownListFor
        /// </summary>
        public IEnumerable<Customers> RetrieveCustomersByCountry(int? CountryId)
        {
            var dictionary = new Dictionary<int?, string>();

            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<Customers>("[General].[Sp_CustomersByCountry_Retrieve]",
                    new
                    {
                        CountryId = CountryId,
                        PartnerId = Session.GetPartnerId(),
                    });

                //Order by Customer Name A-Z
                result = result.OrderBy(o => o.DecryptedName).ToList();

                return result;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}