﻿using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using System;
using System.Linq;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    public class CustomerGeneralParametersBusiness : IDisposable
    {
        public CustomerGeneralParameters CustomerGeneralParametersRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerGeneralParameters>("[General].[Sp_CustomerGeneralParameters_Retrieve]", new { ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId }).FirstOrDefault();
            }
        }

        public void AlarmTemperatureDelatAddOrEdit(int AlarmTemperatureDelay)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_AlarmTemperatureDelay_AddOrEdit]",
                    new
                    {
                        ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                        AlarmTemperatureDelay = AlarmTemperatureDelay,
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        public void MinimumStopTimeAddOrEdit(int minimumStopTime)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_MinimumStopTime_AddOrEdit]",
                    new
                    {
                        ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                        MinimumStopTime = minimumStopTime,
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        public void NormalVehicleScheduleAddOrEdit(string StartTime, string EndTime)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_NormalVehicleSchedule_AddOrEdit]",
                    new
                    {
                        ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                        StartTime,
                        EndTime,
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        /*
        public void EmailPanicBottonAddOrEdit(string Email, string AlarmId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Alarm_AddOrEdit]",
                    new
                    {
                        AlarmId = AlarmId,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                        Phone = "0000-0000",
                        Email,
                        AlarmTriggerId = 511,
                        EntityTypeId = 407,
                        EntityId = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                        Active = 1,
                        LoggedUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }
        */
        public AlarmsModels EmailPanicBottonRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                var alarm = dba.ExecuteReader<AlarmsModels>("[General].[Sp_Alarm_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                        AlarmTriggerId = 511
                    }).FirstOrDefault();
                return alarm;
            }
        }

        public void SaveInProcessTransactions(bool AllowProcess)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_AllowInProcessTransactions_AddOrEdit]",
                new
                {
                    ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    AllowProcess,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public void SaveApprovalTransactiosImport(bool AllowProcess)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_AllowApprovalTransactiosImport_AddOrEdit]",
                new
                {
                    ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    AllowProcess,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public void SaveCostCenterByDriver(bool ActivateProccess)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_CostCenterByDriver_AddOrEdit]",
                new
                {
                    ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    ActivateProccess,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }


        public void TypeOdometerbyCustomer(bool TypeOdometer)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_TypeOdometerByCustomer_AddOrEdit]",
                new
                {
                    ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    TypeOdometer,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public void TypeOfDistributionByCustomer(int TypeOfDistribution)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_TypeOfDistributionByCustomer_AddOrEdit]",
                new
                {
                    ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    TypeOfDistribution,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public void DefaultAlarmsEmail(string email)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_DefaultEmail_AddOrEdit]",
                new
                {
                    ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    Email = email,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public void NotApproveTransactions(bool NotApprove)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_NotApproveTransactions_AddOrEdit]",
                new
                {
                    ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    NotApprove,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }
        public void RadioDistanceByCommerce(int RadioDistance)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_RadioDistanceByCommerce_AddOrEdit]", new
                {
                    ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId,
                    RadioDistance
                });
            }
        }

        public void ActiveRadioDistance(bool ActiveRadioDistance)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ActiveRadioDistanceByCommerce_AddOrEdit]",
                new
                {
                    ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    ActiveRadioDistance,
                    ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public void AlarmNonTransmissionGPSAddOrEdit(bool AlarmNonTransmissionGPS)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_AlarmNonTransmissionGPS_AddOrEdit]",
                    new
                    {
                        ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                        AlarmNonTransmissionGPS,
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        public void VehicleStopLapseAddOrEdit(int Lapse)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleStopLapse_AddOrEdit]",
                    new
                    {
                        Lapse,
                        ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,                        
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
