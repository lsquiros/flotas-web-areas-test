﻿/************************************************************************************************************
*  File    : CustomerCreditsAdminBusiness.cs
*  Summary : CustomerCredits Business Methods
*  Author  : Gerald Solano
*  Date    : 16/05/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// CustomerCredits Class
    /// </summary>
    public class CustomerCreditsAdminBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Customer Credits information
        /// </summary>
        /// <returns>A IEnumerable collection of the Drivers entity model</returns>
        public IEnumerable<CustomerCreditsAdmin> RetrieveCustomerCredits(DateTime? date = null, int? customerId = null)
        {
            var currentDate = date ?? DateTimeOffset.Now;
            using (var dba = new DataBaseAccess())
            {                
                return dba.ExecuteReader<CustomerCreditsAdmin>("[Control].[Sp_CustomerCredits_Retrieve]",
                    new
                    {
                        CustomerId = customerId ?? ECOsystem.Utilities.Session.GetCustomerId(),
                        currentDate.Year,
                        currentDate.Month                        
                    });
            }
        }

        /// <summary>
        /// Performs the Insert of the entity transaction via API
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>TransactionId created after insert</returns>
        public int AddCustomerCreditFromApi(CustomerCreditApi model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Control].[Sp_CustomerCredits_AddFromAPI]",
                    new
                    {
                        model.AccountNumber,
                        model.Year,
                        model.Month,
                        model.CreditAmount,
                        model.CreditTypeId,
                        model.LoggedUserId
                    });
            }
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}