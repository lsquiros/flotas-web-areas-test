﻿/************************************************************************************************************
*  File    : DashboardModulesBusiness.cs
*  Summary : Dashboard Modules Business
*  Author  : Manuel Azofeifa Hidalgo
*  Date    : 02/Dic/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Dashboard Modules Business
    /// </summary>
    class DashboardModulesBusiness : IDisposable
    {
        /// <summary>
        ///  Retrieve Alarm informacion
        /// </summary>
        /// <param name="CustomerId">Customer Unique Identifier</param>
        public IEnumerable<DashboardModule> RetrieveDashboarModules(int CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<DashboardModule>("[General].[Sp_DashboardModuleByCustomer_Retrieve]",
                    new
                    {
                        CustomerId = CustomerId
                    });
            }
        }

        /// <summary>
        ///  Performs the maintenance Insert or Update of the entity Dashboard Module
        /// </summary>
        /// <param name="model"></param>
        public void AddOrEditDashboardModule(DashboardModule model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_DashboardModuleByCustomer_AddOrEdit]",
                    new
                    {
                        model.CustomerId,
                        model.DashboardModuleId,
                        model.Order,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
