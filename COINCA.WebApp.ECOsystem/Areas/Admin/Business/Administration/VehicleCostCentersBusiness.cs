﻿/************************************************************************************************************
*  File    : VehicleCostCentersBusiness.cs
*  Summary : VehicleCostCenters Business Methods
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;
using System.Data;
using System.Web;
using System.Linq;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// VehicleCostCenters Class
    /// </summary>
    public class VehicleCostCentersBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleCostCenters
        /// </summary>
        /// <param name="costCenterId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        /// 
        private DataBaseAccess dba = new DataBaseAccess();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="costCenterId"></param>
        /// <param name="opc"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<VehicleCostCenters> RetrieveVehicleCostCenters(int? costCenterId,Boolean opc=false, string key = null)
        {
            using (dba)
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<VehicleCostCenters>("[General].[Sp_VehicleCostCenters_Retrieve]",
                new
                {
                    CostCenterId = costCenterId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Key = key,
                    user.UserId
                });
                
             }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleCostCenters
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleCostCenters(VehicleCostCenters model)
        {
            using (dba)
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleCostCenters_AddOrEdit]",
                    new
                    {
                        model.CostCenterId,
                        model.Name,
                        model.Code,
                        model.UnitId,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleCostCenters
        /// </summary>
        /// <param name="costCenterId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehicleCostCenters(int costCenterId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleCostCenters_Delete]",
                    new { CostCenterId = costCenterId,
                          ModifyUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }


        /// <summary>
        /// Retrieve Vehicles
        /// 
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Vehicles> RetrieveVehiclesByCostCenter(int? CostCenterId)
        {
            using (var dba = new DataBaseAccess())
            {
                var user = ECOsystem.Utilities.Session.GetUserInfo();

                return dba.ExecuteReader<Vehicles>("[Control].[Sp_VehiclesByCostCenter_Retrieve]",
                new
                {         
                    CostCenterId,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),                    
                    user.UserId
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataSet DownloadData(List<VehicleCostCenters> list)
        {
            DataSet ds = new DataSet();
            using (var dt = new DataTable("VehicleCostCenterReport"))
            {
                dt.Columns.Add("CostCenterId");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("CostCenterDescription");
                dt.Columns.Add("VehiclesCount");
                dt.Columns.Add("CustomerName");
                foreach (var cc in list)
                { 
                    DataRow row = dt.NewRow();
                    row["CostCenterId"] = cc.CostCenterId;
                    row["CostCenterName"] = cc.Name;
                    row["CostCenterDescription"] = cc.Name;
                    row["VehiclesCount"] = cc.CantVehicles;
                    row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;                        
                      
                    dt.Rows.Add(row);                      
                }
                ds.Tables.Add(dt);
            }

            var vehiclelist = RetrieveVehiclesByCostCenter(null).Where(m => list.Exists(x => (int)x.CostCenterId == m.CostCenterId));
            using (var dt = new DataTable("VehicleCostCenterDetail"))
            {
                dt.Columns.Add("CostCenterId");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("Manufacturer");
                dt.Columns.Add("Model");
                dt.Columns.Add("Type");
                dt.Columns.Add("Unit");
                dt.Columns.Add("Driver");
                dt.Columns.Add("GPS");
                foreach (var v in vehiclelist)
                {
                    DataRow row = dt.NewRow();
                    row["CostCenterId"] = v.CostCenterId;
                    row["VehicleName"] = v.Name;
                    row["PlateId"] = v.PlateId;
                    row["Manufacturer"] = v.Manufacturer;
                    row["Model"] = v.VehicleModel;
                    row["Type"] = v.VehicleType;
                    row["Unit"] = v.UnitName;
                    row["Driver"] = v.DecryptedUserName;
                    row["GPS"] = v.HasGPS;

                    dt.Rows.Add(row); 
                }
                ds.Tables.Add(dt);
            }
            return ds;
        }


        /// <summary>
        /// ChangePullPreviousBudget
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pullPreviousBudget"></param>
        public void ChangePullPreviousBudget(int id, bool pullPreviousBudget)
        {
            using (var dataBaseAccess = new DataBaseAccess())
            {
                dataBaseAccess.ExecuteNonQuery("General.Sp_VehiclesByCostCenter_PullPreviousBudget_Edit", new
                {
                    CostCenterId = id,
                    PullPreviousBudget = pullPreviousBudget
                });
            }
        }        

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}