﻿/************************************************************************************************************
*  File    : DriversBusiness.cs
*  Summary : Drivers Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using System.IO;
using ECOsystem.Models.Core;
using ECOsystem.Business.Core;
using  ECOsystem.Areas.Admin.Models.Administration;
using System.Data;
using System.Web;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// Drivers Business class performs all related to Drivers business logic
    /// </summary>
    public class DriversBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Drivers information
        /// </summary>
        /// <returns>A IEnumerable collection of the Drivers entity model</returns>
        public IEnumerable<Users> RetrieveDrivers()
        {
            using (var business = new UsersBusiness())
            {
                return business.RetrieveUsers(null, customerId2: ECOsystem.Utilities.Session.GetCustomerId(),opc:true, isList: true);
            }
        }

        public Users RetrieveDriverByIdentification(string DriverIdentification)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Users>("[General].[Sp_DriversByIdentification_Retrieve]",
                new{
                    DriverIdentification,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                }).FirstOrDefault();
            }
        }

        /// <summary>
        /// RetrieveDriversActive
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Drivers> RetrieveDriversActive()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Drivers>("[General].[Sp_DriverActive_Retrieve]",
                new
                {                         
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                });
            }
        }

        /// <summary>
        /// Retrieve Drivers By Vehicle information
        /// </summary>
        /// <returns>A IEnumerable collection of the Drivers entity model</returns>
        public IEnumerable<Users> RetrieveDriversByVehicle(int? vehicleId, int customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                //return business.RetrieveUsers(null, customerId2: ECOsystem.Utilities.Session.GetCustomerId(), opc: true);
                var result = dba.ExecuteReader<Users>("[General].[Sp_Drivers_RetrieveByVehicle]",
                new
                {
                    VehicleId = vehicleId,
                    CustomerId = customerId
                });
           
                return result;
            }
        }

        /// <summary>
        /// Retrieve DriversAPI information
        /// </summary>
        /// <returns>A IEnumerable collection of the Drivers entity model</returns>
        public IEnumerable<Users> RetrieveDriversApi(string username)
        {
            using (var business = new UsersBusiness())
            {
                return business.RetrieveUsers(null, userName: TripleDesEncryption.Encrypt(username), opc: true);
            }
        }

        /// <summary>
        /// Retrieve Active Customers Count
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public int RetrieveRegisteredDriversCount(int? partnerId, int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Insurance].[Sp_RegisteredDriversCount_Retrieve]",
                    new
                    {
                        PartnerId = partnerId,
                        CustomerId = customerId

                    });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelList"></param>
        /// <returns></returns>
        public byte[] CreateTxtFormat(IEnumerable<DriversErrors> modelList)
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    var sw = new StreamWriter(ms);
                    foreach (var item in modelList)
                    {
                        sw.WriteLine(item.Item);
                        sw.Flush();
                    }
                    return ms.ToArray();
                }
            }
            catch (Exception)
            {
                throw new SystemException("System Exception when server tries to generate excel file");
            }
        }
        
        /// <summary>
        /// GetReport Download Data
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData()
        {
            try
            {
                var list = new List<Users>();
                list = (List<Users>)HttpContext.Current.Session["DriversInfo"];
                using (var dba = new DataBaseAccess())
                {
                    foreach (var t in list.Where(t => t.IsDriverUser))
                    {
                        t.DriverUser = dba.ExecuteReader<DriversUsers>("[General].[Sp_DriversUsers_Retrieve]", new { t.UserId }).FirstOrDefault();
                    }
                }
                return DriversGenerate(list);
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Drivers Generate
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable DriversGenerate(IEnumerable<Users> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("Name");
                dt.Columns.Add("Identification");
                dt.Columns.Add("Email");
                dt.Columns.Add("Code");
                dt.Columns.Add("License");
                dt.Columns.Add("ExpirationLicenseDate");
                dt.Columns.Add("Status");
                dt.Columns.Add("IsLockedOut");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("IsAgent");
                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();

                    string text = string.Empty;

                    row["Name"] = item.DecryptedName;
                    row["Identification"] = item.DriverUser.DecryptedIdentification;
                    row["Email"] = item.DecryptedEmail;
                    row["Code"] = item.DriverUser.DecryptedCode;
                    row["License"] = item.DriverUser.DecryptedLicense;
                    row["ExpirationLicenseDate"] = item.DriverUser.LicenseExpiration;
                    row["Status"] = item.IsActive;
                    row["IsLockedOut"] = item.IsLockedOut;
                    row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    row["IsAgent"] = item.IsAgent;
                    dt.Rows.Add(row);
                }
                return dt;
            }            
        }

        public IEnumerable<Users> DriversNoCreditCardRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Users>("[General].[Sp_DriversWithNoCreditCard_Retrieve]",
                    new 
                    { 
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId() 
                    });
            }
        }

        public IEnumerable<Users> RetrieveCustomerDrivers(int? userId = null, string key = null, string userName = null, int? customerId = null, int? UserStatus = null, bool bindObtPhoto = false)
        {
            using (var dba = new DataBaseAccess())
            {
                var result2 = dba.ExecuteReader<Users>("[General].[Sp_Drivers_Retrieve]",
                    new
                    {
                        UserId = userId,
                        Key = TripleDesEncryption.Encrypt(key),
                        UserName = userName,
                        CustomerId = customerId,
                        //bindObtPhoto,
                        UserStatus
                    });

                var result = new List<Users>();
                if (key != null && key != "")
                {
                    foreach (var item in result2)
                    {
                        if (item.DecryptedName.ToLower().Contains(key.ToLower()) || item.DecryptedEmail.ToLower().Contains(key.ToLower()) || item.DecryptedPhoneNumber.Contains(key))
                        {
                            result.Add(item);
                        }
                    } 
                }

                return string.IsNullOrEmpty(key) ? result2 : result;

            }
        }



        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        internal IEnumerable<DriverBudget> RetrieveDriverBudget(int driverId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<DriverBudget>("[Control].[Sp_DriverBudget_Retrieve]",
                    new
                    {
                        DriverId = driverId
                    });
            }
        }
    }
}