﻿using  ECOsystem.Areas.Admin.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Admin.Business
{
    public class MonthlyBillReportBusiness : IDisposable
    {
        public IEnumerable<MonthlyBillReport> GetMonthlyBillReport(int? Month, int? Year, bool? IsServiceReport = false)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<MonthlyBillReport>("[Control].[Sp_MonthlyBillReport_Retrieve]", new
                {
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    Year,
                    Month,
                    IsServiceReport
                });
            }
        }

        public DataTable GetReportData(int? Month, int? Year)
        {
            var data = GetMonthlyBillReport(Month, Year);

            using (var dt = new DataTable())
            {
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("PhoneNumber");
                dt.Columns.Add("RTN");                
                dt.Columns.Add("Address");
                dt.Columns.Add("CurrencySymbol");
                dt.Columns.Add("CurrencyName");
                dt.Columns.Add("Amount");
                dt.Columns.Add("FinalAmount");
                dt.Columns.Add("AccountNumber");
                dt.Columns.Add("ExchangeRate");
                dt.Columns.Add("Detail");
                dt.Columns.Add("DetailNumber");
                dt.Columns.Add("DetailAmount");
                dt.Columns.Add("DetailTotal");
                dt.Columns.Add("Invoice");
                dt.Columns.Add("BillDate");
                dt.Columns.Add("BACService");
                dt.Columns.Add("DetailTotalLetters");
                dt.Columns.Add("BACName");

                var amount = data.Sum(x => x.DetailTotal);
                foreach (var item in data)
	            {
                    var dr = dt.NewRow();
                    item.Amount = amount;
                    item.FinalAmount = amount;

                    dr["CustomerName"] = item.CustomerNameDecrypt;
                    dr["PhoneNumber"] = item.PhoneNumber;
                    dr["RTN"] = item.RTN;
                    dr["Address"] = item.Address;
                    dr["CurrencySymbol"] = item.CurrencySymbol;
                    dr["CurrencyName"] = item.CurrencyName;
                    dr["Amount"] = item.AmountStr;
                    dr["FinalAmount"] = item.FinalAmountStr;
                    dr["AccountNumber"] = item.AccountNumberStr;
                    dr["ExchangeRate"] = item.ExchangeRate;
                    dr["Detail"] = item.Detail;
                    dr["DetailNumber"] = item.DetailNumber;
                    dr["DetailAmount"] = item.DetailAmountStr;
                    dr["DetailTotal"] = item.DetailTotalStr;
                    dr["Invoice"] = item.Invoice;
                    dr["BillDate"] = item.BillDate.ToString("dd/MM/yyyy");
                    dr["BACService"] = item.BACService;
                    dr["DetailTotalLetters"] = item.DetailTotalToLetters;
                    dr["BACName"] = item.BACName;
                    dt.Rows.Add(dr);
	            }    
                return dt; 
            }
        }

        public DataTable GetServicesReportData(int? Month, int? Year, bool IsServiceReport)
        {
            var data = GetMonthlyBillReport(Month, Year, IsServiceReport);

            using (var dt = new DataTable())
            {
                dt.Columns.Add("CustomerName");                
                dt.Columns.Add("CurrencySymbol");
                dt.Columns.Add("CurrencyName");
                dt.Columns.Add("Amount");
                dt.Columns.Add("FinalAmount");
                dt.Columns.Add("AccountNumber");
                dt.Columns.Add("ExchangeRate");
                dt.Columns.Add("Detail");
                dt.Columns.Add("DetailNumber");
                dt.Columns.Add("DetailAmount");
                dt.Columns.Add("DetailTotal");
                dt.Columns.Add("Invoice");
                dt.Columns.Add("BillDate");
                dt.Columns.Add("BACService");
                dt.Columns.Add("DetailTotalLetters");
                dt.Columns.Add("BACName");

                var amount = data.Sum(x => x.DetailTotal);
                foreach (var item in data)
                {
                    var dr = dt.NewRow();
                    item.Amount = amount;
                    item.FinalAmount = amount;

                    dr["CustomerName"] = item.CustomerNameDecrypt;                    
                    dr["CurrencySymbol"] = item.CurrencySymbol;
                    dr["CurrencyName"] = item.CurrencyName;
                    dr["Amount"] = item.AmountStr;
                    dr["FinalAmount"] = item.FinalAmountStr;
                    dr["AccountNumber"] = item.AccountNumberStr;
                    dr["ExchangeRate"] = item.ExchangeRate;
                    dr["Detail"] = item.Detail;
                    dr["DetailNumber"] = item.DetailNumber;
                    dr["DetailAmount"] = item.DetailAmountStr;
                    dr["DetailTotal"] = item.DetailTotalStr;
                    dr["Invoice"] = item.Invoice;
                    dr["BillDate"] = item.BillDate.ToString("dd/MM/yyyy");
                    dr["BACService"] = item.BACService;
                    dr["DetailTotalLetters"] = item.DetailTotalToLetters;
                    dr["BACName"] = item.BACName;
                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}