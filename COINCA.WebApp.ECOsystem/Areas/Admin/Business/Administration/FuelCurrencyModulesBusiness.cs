﻿using  ECOsystem.Areas.Admin.Models;
/**************************************************************************************************
 * 
 *  File    : FuelCurrencyModulesBusiness.cs
 *  Summary : FuelCurrency Modules Business
 *  Author  : Melvin Salas
 *  Date    : 05/NOV/2015
 * 
 *  Copyright 2014 COINCA, All rights reserved
 *  This software and documentation is the confidential and proprietary information of COINCA S.A.
 *  You shall not disclose such Confidential Information and shall use it only in accordance with 
 *  the terms of the license agreement you entered into with this company.
 * 
 *************************************************************************************************/
//using COINCA.Library.DataAccess;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    class FuelCurrencyModulesBusiness : IDisposable
    {
        /// <summary>
        /// Get customer's fuel distribution preferences 
        /// </summary>
        /// <param name="customerId">Customer Identification Number</param>
        /// <returns>Fuel Distribution Preferences</returns>
        internal FuelCurrencyModule Retrieve(int customerId)
        {
            FuelCurrencyModule response;
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    response = dba.ExecuteReader<FuelCurrencyModule>("[General].[Sp_FuelCurrencySettings_Retrieve]",
                        new
                        {
                            CustomerId = customerId
                        }).FirstOrDefault();
                }
            }
            catch
            {
                response = new FuelCurrencyModule();
                // TODO: Save Exception Log
            }
            return response;
        }

        /// <summary>
        /// Edit customer's fuel distribution preferences
        /// </summary>
        /// <param name="model">Fuel Distribution Preferences</param>
        internal Exception AddOrEdit(FuelCurrencyModule model)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[General].[Sp_FuelCurrencySettings_AddOrEdit]", new
                    {
                        model.CustomerId,
                        model.ByCurrency,
                        model.ByFuel
                    });
                }
            }
            catch (Exception e)
            {
                return e;
                // TODO: Save Exception Log
            }
            return null;
        }

        /// <summary>
        /// Disposer
        /// </summary>
        public void Dispose()
        {
            // Forces garbage collector to remove instance
            GC.SuppressFinalize(this);
        }
    }
}
