﻿
/************************************************************************************************************
*  File    : SettingOdometersBusiness.cs
*  Summary : Setting Odometers Business Methods
*  Author  : Stefano Quirós Ruiz
*  Date    : 03/29/2016
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;

using System.Linq;
//using COINCA.Library.DataAccess;
using ECOsystem.DataAccess;
using ECOsystem.Models.Miscellaneous;
using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using ECOsystem.Business.Utilities;
using System.Web.Script.Serialization;
using System.Data;
using System.Web.Mvc;
using System.Net.Http;
using ModuleConfig;
using System.Net.Http.Headers;
using System.IO;
using System.Web;
using System.Xml.Linq;
using System.Text;
using System.Xml.Serialization;
//using ECOsystem.Models.Control;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    public class SettingOdometersBusiness : IDisposable
    {

        /// <summary>
        /// Retrieve Setting Odometers Data 
        /// </summary>
        /// <param name="model"></param>
        /// <returns>SettingOdometersBase Model to render view</returns>
        public SettingOdometersBase RetrieveSettingOdometers(ControlFuelsReportsBase parameters, bool? Alert, bool ExcelReport = false)
        {
            var result = new SettingOdometersBase();
            var list = new List<SettingOdometers>();
            GetSettingOdometersData(parameters, result, Alert, ExcelReport);           
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.OdometerType = parameters.OdometerType;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            return result;
        }


        /// <summary>
        /// Get Setting Odometers Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetSettingOdometersData(ControlFuelsReportsBase parameters, SettingOdometersBase result, bool? alert, bool ExportReport = false)
        {
            using (var dba = new DataBaseAccess())
            {
                int cusId = 0;
                int? filterType = parameters.OdometerType != 1 ? (int?)null : 1;

                if (parameters.CustomerId == 0)
                    cusId = Convert.ToInt32(Session.GetCustomerId());
                else
                    cusId = parameters.CustomerId;

                var user = ECOsystem.Utilities.Session.GetUserInfo();

                result.List = dba.ExecuteReader<SettingOdometers>("[Control].[Sp_Setting_Odometers_Retrieve]",
                    new
                    {
                        CustomerId = cusId,
                        Year = parameters.Year,
                        Month = parameters.Month,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate,
                        key = parameters.key,
                        user.UserId,
                        Alert = alert != true ? false : true,
                        FilterType = filterType,
                        ExportReport
                    });

            }
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }


        /// <summary>
        /// </summary>
        /// <param name="parameters"></param>
        private IEnumerable<OdometerChanged> GetOdometersChangedData(ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                int cusId = 0;

                if (parameters.CustomerId == 0)
                    cusId = Convert.ToInt32(Session.GetCustomerId());
                else
                    cusId = parameters.CustomerId;

                return dba.ExecuteReader<OdometerChanged>("[Control].[Sp_OdometerChanged_Retrieve]",
                    new
                    {
                        CustomerId = cusId,
                        Year = parameters.Year,
                        Month = parameters.Month,
                        StartDate = parameters.StartDate,
                        EndDate = parameters.EndDate
                    });

            }
             
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Vehicles
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public IEnumerable<SettingOdometers> AddOrEditOdometer(List<SettingOdometers> list, ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_SettingOdometers_AddOrEdit]",
                new
                {
                    XmlData = GetXML(list),
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }

            var result = RetrieveSettingOdometers(parameters, (bool?)HttpContext.Current.Session["Alert"]);
            foreach (var item in result.List)
            {
                foreach (var x in list)
                {
                    if (x.TransactionId != item.TransactionId)
                    {
                        item.Result = 101;
                    }
                }
            }
            result.IsResult = true;
            return result.List;
        }

       
        public string GetXML(List<SettingOdometers> list)
        {
            var XMLData = string.Empty;

            using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<SettingOdometers>));
                xmlSerializer.Serialize(stringWriter, list);
                return XMLData = stringWriter.ToString().Substring(stringWriter.ToString().IndexOf('\n') + 1);
            }
        }

        /// <summary>
        /// Get ParamsBy Customer
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        public string GetParamByCustomer()
        {
            using (var dba = new DataBaseAccess())
            {
                int cusId = 0;                
                    cusId = Convert.ToInt32(Session.GetCustomerId());
                    return dba.ExecuteScalar<string>("[General].[Sp_GetParametersByCustomer]",
                    new
                    {
                        CustomerId = cusId,
                        ParamByCust = "SettingOdometers"
                        
                    });

            }
        }

        /// <summary>
        /// Performs Alarm Credit Count
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public int AlarmOdometerCountRetrieve()
        {
            var parameter = GetParamByCustomer();
            int Year = DateTime.Today.Year;
            int Month = DateTime.Today.Month;
            var list = new List<SettingOdometers>();           

            using (var dba = new DataBaseAccess())
            {
                var List = dba.ExecuteReader<SettingOdometers>("[Control].[Sp_Setting_Odometers_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId() == null ? 0 : ECOsystem.Utilities.Session.GetCustomerId(),
                        Year = Year,
                        Month = Month,
                        //Parameter = Convert.ToDecimal(porcent),
                        UserId = Convert.ToInt32(ECOsystem.Utilities.Session.GetUserInfo().UserId)
                    });


                foreach (var item in List)
                {                    
                    var DefaultPerformanceMore = Convert.ToDouble(item.DefaultPerformance) + (Convert.ToDouble(item.DefaultPerformance) * Convert.ToDouble(parameter));
                    var DefaultPerformanceLess = Convert.ToDouble(item.DefaultPerformance) - (Convert.ToDouble(item.DefaultPerformance) * Convert.ToDouble(parameter));

                    if (Convert.ToDouble(item.Performance) >= Math.Round(DefaultPerformanceMore, 2) || Convert.ToDouble(item.Performance) <= Math.Round(DefaultPerformanceLess, 2))
                    {
                        list.Add(item);
                    }

                }
                List = list;
                return List.Count();
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters, bool? Alert)
        {
            try
            {
                var result = new SettingOdometersBase();
                var list = new List<SettingOdometers>();
                GetSettingOdometersData(parameters, result, Alert, true);

                var startDate = new DateTime();
                var endDate = new DateTime();

                if (parameters.StartDate != null && parameters.EndDate != null)
                {
                    startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                    endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
                }
                else if (parameters.Month != null && parameters.Year != null)
                {
                    startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                    endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
                }
                if (parameters.OdometerType == 1)
                {
                    foreach (var item in result.List)
                    {
                        string parameter = GetParamByCustomer();
                        var DefaultPerformanceMore = item.DefaultPerformance + (item.DefaultPerformance * Convert.ToDecimal(parameter));
                        var DefaultPerformanceLess = item.DefaultPerformance - (item.DefaultPerformance * Convert.ToDecimal(parameter));

                        if (item.Performance >= DefaultPerformanceMore || item.Performance <= DefaultPerformanceLess)
                        {
                            list.Add(item);
                        }

                    }
                    result.List = list;
                    return SettingOdometersGenerate(result.List, startDate, endDate);
                }
                result.Parameters.Year = parameters.Year;
                result.Parameters.Month = parameters.Month;
                result.Parameters.StartDate = parameters.StartDate;
                result.Parameters.EndDate = parameters.EndDate;
                result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
                result.Parameters.OdometerType = parameters.OdometerType;
                result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));   
                return SettingOdometersGenerate(result.List, startDate, endDate);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadDataOdometerChanged(ControlFuelsReportsBase parameters)
        {
            try
            {
                var result = new SettingOdometersBase();
                var list = new List<OdometerChanged>();
                list = GetOdometersChangedData(parameters).ToList();


                return OdometerChangedReportGenerate(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private DataTable OdometerChangedReportGenerate(IEnumerable<OdometerChanged> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("Vehiculo");
                dt.Columns.Add("Placa");
                dt.Columns.Add("FechaTransaccion");
                dt.Columns.Add("HoraTransaccion");
                dt.Columns.Add("FechaCambio");
                dt.Columns.Add("HoraCambio");
                dt.Columns.Add("OdometroOriginal");
                dt.Columns.Add("OdometroFinal");
                dt.Columns.Add("Usuario");
                dt.Columns.Add("Encabezado");

                foreach (var item in list)
                {
                    var dr = dt.NewRow();
                    dr["Vehiculo"] = item.Vehiculo;
                    dr["Placa"] = item.Placa;
                    dr["FechaTransaccion"] = item.FechaTransaccion;
                    dr["HoraTransaccion"] = item.HoraTransaccion;
                    dr["FechaCambio"] = item.FechaCambio;
                    dr["HoraCambio"] = item.HoraCambio;
                    dr["OdometroOriginal"] = item.OdometroOriginal;
                    dr["OdometroFinal"] = item.OdometroFinal;
                    dr["Usuario"] = item.Usuario;
                    dr["Encabezado"] = item.Encabezado;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
        } 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable SettingOdometersGenerate(IEnumerable<SettingOdometers> list, DateTime startDate, DateTime endDate)
        {
            var CustomerName = Session.GetCustomerInfo().DecryptedName;
            using (var dt = new DataTable())
            {
                dt.Columns.Add("Date");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("CapacityUnitValue");
                dt.Columns.Add("OdometerLast");
                dt.Columns.Add("Odometer");
                dt.Columns.Add("Travel");
                dt.Columns.Add("DefaultPerformance");
                dt.Columns.Add("Performance");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("OdometerBeforeLast");
                dt.Columns.Add("FuelName");
                dt.Columns.Add("DriverName");
                dt.Columns.Add("DriverIdentification");
                dt.Columns.Add("CustomerName");

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);

                if (HttpContext.Current.Session["Alert"] != null && Convert.ToBoolean(HttpContext.Current.Session["Alert"]))
                {
                    startDateStr = "Al";
                }

                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();
                    row["Date"] = item.Date ;
                    row["VehicleName"] = item.VehicleName;
                    row["PlateId"] = item.PlateId;
                    row["CapacityUnitValue"] = item.CapacityUnitValueStr;
                    row["OdometerLast"] = item.OdometerLast;
                    row["Odometer"] = item.Odometer;
                    row["Travel"] = item.Travel;
                    row["DefaultPerformance"] = item.DefaultPerformance;
                    row["Performance"] = item.Performance;
                    row["InitRangeDate"] = startDateStr;
                    row["FiniRangeDate"] = endDateStr;
                    row["OdometerBeforeLast"] = item.OdometerBeforeLast;
                    row["FuelName"] = ECOsystem.Utilities.Session.GetCustomerCapacityUnitName();
                    row["DriverName"] = item.DecryptedDriverName;
                    row["DriverIdentification"] = item.DecryptedDriverIdentification;
                    row["CustomerName"] = CustomerName;
                    dt.Rows.Add(row);
                }

                return dt;
            }
        } 
       
        /// <summary>
        /// No Show Alert Add Or Edit
        /// </summary>
        /// <param name="TransactionIds"></param>
        public void NoShowAlertAddOrEdit(List<string> TransactionIdsNoAlert, List<string> TransactionIdsShowAlert)
        {
            using (var dba = new DataBaseAccess())
            {
                var docNoAlert = new XDocument(new XElement("xmldata"));
                var rootNoAlert = docNoAlert.Root;
                var docAlert = new XDocument(new XElement("xmldata"));
                var rootAlert = docAlert.Root;
                if (TransactionIdsNoAlert != null)
                {
                    foreach (var item in TransactionIdsNoAlert)
                    {
                        rootNoAlert.Add(new XElement("NoAlert", new XAttribute("TransactionId", item)));
                    }
                }
                if (TransactionIdsShowAlert != null)
                {
                    foreach (var item in TransactionIdsShowAlert)
                    {
                        rootAlert.Add(new XElement("Alert", new XAttribute("TransactionId", item)));
                    }
                }
                dba.ExecuteNonQuery("[General].[Sp_NoShowAlert_AddOrEdit]",
                new 
                {
                    XmlDataNoShowAlert = rootNoAlert.ToString(),
                    XmlDataShowAlert = rootAlert.ToString(),
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerInfo().CustomerId,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });                               
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}