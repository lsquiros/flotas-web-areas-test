﻿/************************************************************************************************************
*  File    : TransactionSapReportBusiness.cs
*  Summary : TransactionsSapReport Business Methods
*  Author  : Henry Retana
*  Date    : 1/02/2016
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Data;
using ECOsystem.Models.Account;
using  ECOsystem.Areas.Admin.Models.Administration;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// TransactionSapReport Class
    /// </summary>
    public class TransactionSapReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Transactions Sap
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public TransactionsConsolideReportBase RetrieveTransactionsSAPReport(ControlFuelsReportsBase parameters)
        {
            var result = new TransactionsConsolideReportBase();
            GetReportData(result, parameters);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.TransactionType = parameters.TransactionType;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            result.Menus = new List<AccountMenus>();
            result.Data = new TransactionsConsolideReport();
            return result; 
        }

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="result"></param>
        /// <param name="parameters"></param>
        public void GetReportData(TransactionsConsolideReportBase result, ControlFuelsReportsBase parameters)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<TransactionsConsolideReport>("[Control].[Sp_TransactionsSAPReport_Retrieve]", 
                new 
                { 
                    CustomerId = Convert.ToInt32(ECOsystem.Utilities.Session.GetCustomerId()),
                    parameters.Year,
                    parameters.Month,
                    parameters.StartDate,
                    parameters.EndDate
                });
            }  
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData(ControlFuelsReportsBase parameters)
        {
            try
            {
                var result = new TransactionsConsolideReportBase();
                GetReportData(result, parameters);

                return TransactionsSAPReportGenerate(result.List);
            }
            catch (Exception e)
            {                
                throw e;
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable TransactionsSAPReportGenerate(IEnumerable<TransactionsConsolideReport> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("FECHA_DOCUMENTO");
                dt.Columns.Add("CLASE_DOCUMENTO");
                dt.Columns.Add("SOCIEDAD");
                dt.Columns.Add("FECHA_CONTABILIZACION");
                dt.Columns.Add("MONEDA");
                dt.Columns.Add("REFERENCIA");
                dt.Columns.Add("TEXTO_CABECERA");
                dt.Columns.Add("CLAVE_CONTABILIZACION");
                dt.Columns.Add("CUENTA");
                dt.Columns.Add("IND_CME");
                dt.Columns.Add("IMPORTE");
                dt.Columns.Add("INDICADOR_IMPUESTO");
                dt.Columns.Add("CALC_IMPUESTO");
                dt.Columns.Add("FECHA_VALOR");
                dt.Columns.Add("ASIGNACION");
                dt.Columns.Add("TEXTO");
                dt.Columns.Add("BANCO_PROPIO");
                dt.Columns.Add("VIA_PAGO");
                dt.Columns.Add("CENTRO_COSTO");
                dt.Columns.Add("CENTRO_BENEFICIO");
                dt.Columns.Add("ORDEN");
                dt.Columns.Add("PEP");
                dt.Columns.Add("POSICION_PRESUPUEST");
                dt.Columns.Add("CENTRO_GESTOR");
                dt.Columns.Add("PROGRAMA_PRESUPUEST");
                dt.Columns.Add("FONDO");
                dt.Columns.Add("CENTRO");
                dt.Columns.Add("BLOQUEA_PAGO");
                dt.Columns.Add("PAGADOR_ALTERNATIVO");
                
                foreach (var item in list)
                {
                    DataRow drDebe = dt.NewRow();
                    DataRow drHaber = dt.NewRow();
                    string text = string.Empty;
                    string CcDescrypt = TripleDesEncryption.Decrypt(item.CreditCardNumber);
                    string CcMask = ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(CcDescrypt);

                    drDebe["FECHA_DOCUMENTO"] = item.DocumentDate.ToString("dd.MM.yyyy");
                    drDebe["CLASE_DOCUMENTO"] = item.DocumentClass;
                    drDebe["SOCIEDAD"] = item.Sociedad;
                    drDebe["FECHA_CONTABILIZACION"] = item.CountDate.ToString("dd.MM.yyyy");
                    drDebe["MONEDA"] = item.Currency;
                    drDebe["REFERENCIA"] = item.Reference;
                    drDebe["TEXTO_CABECERA"] = item.TextoCabecera + " " + item.DocumentDate.ToString("MMM yyyy").ToUpper();
                    drDebe["CLAVE_CONTABILIZACION"] = item.ClaveContabilizacionDebe;
                    drDebe["CUENTA"] = item.Account; 
                    drDebe["IND_CME"] = item.IND_CME;
                    drDebe["IMPORTE"] = Convert.ToInt32(item.Importe).ToString("D"); 
                    drDebe["INDICADOR_IMPUESTO"] = item.IndicadorImpuesto;
                    drDebe["CALC_IMPUESTO"] = item.CalcImpuesto;
                    drDebe["FECHA_VALOR"] = item.FechaValor.ToString("dd.MM.yyyy");
                    drDebe["ASIGNACION"] = item.Asignacion;
                    drDebe["TEXTO"] = text;
                    drDebe["BANCO_PROPIO"] = item.BancoPropio;
                    drDebe["VIA_PAGO"] = item.ViaPagoDebe;
                    drDebe["CENTRO_COSTO"] = item.CentroCosto;
                    drDebe["CENTRO_BENEFICIO"] = item.CentroBeneficio;
                    drDebe["ORDEN"] = item.Orden;
                    drDebe["PEP"] = item.PEP;
                    drDebe["POSICION_PRESUPUEST"] = item.PosicionPresupuesto;
                    drDebe["CENTRO_GESTOR"] = item.CentroGestor;
                    drDebe["PROGRAMA_PRESUPUEST"] = item.ProgramaPresupuesto;
                    drDebe["FONDO"] = item.Fondo;
                    drDebe["CENTRO"] = item.Centro;
                    drDebe["BLOQUEA_PAGO"] = item.BloqueaPagoDebe;
                    drDebe["PAGADOR_ALTERNATIVO"] = item.PagadorAlternativo;

                    drHaber["FECHA_DOCUMENTO"] = text;
                    drHaber["CLASE_DOCUMENTO"] = text;
                    drHaber["SOCIEDAD"] = text;
                    drHaber["FECHA_CONTABILIZACION"] = text;
                    drHaber["MONEDA"] = text;
                    drHaber["REFERENCIA"] = text;
                    drHaber["TEXTO_CABECERA"] = text;
                    drHaber["CLAVE_CONTABILIZACION"] = item.ClaveContabilizacionHaber;
                    drHaber["CUENTA"] = item.SAPProv;
                    drHaber["IND_CME"] = text;
                    drHaber["IMPORTE"] = Convert.ToInt32(item.Importe).ToString("D");
                    drHaber["INDICADOR_IMPUESTO"] = text;
                    drHaber["CALC_IMPUESTO"] = text;
                    drHaber["FECHA_VALOR"] = text;
                    drHaber["ASIGNACION"] = text;
                    drHaber["TEXTO"] = item.Texto + ";T:" + CcMask;
                    drHaber["BANCO_PROPIO"] = "";
                    drHaber["VIA_PAGO"] = item.ViaPagoHaber;
                    drHaber["CENTRO_COSTO"] = text;
                    drHaber["CENTRO_BENEFICIO"] = text;
                    drHaber["ORDEN"] = text;
                    drHaber["PEP"] = text;
                    drHaber["POSICION_PRESUPUEST"] = text;
                    drHaber["CENTRO_GESTOR"] = text;
                    drHaber["PROGRAMA_PRESUPUEST"] = text;
                    drHaber["FONDO"] = text;
                    drHaber["CENTRO"] = text;
                    drHaber["BLOQUEA_PAGO"] = item.BloqueaPagoHaber;
                    drHaber["PAGADOR_ALTERNATIVO"] = text; 

                    dt.Rows.Add(drDebe);
                    dt.Rows.Add(drHaber);
                }

                return dt;
            }
        }

        #region Parameters
        
        /// <summary>
        /// SAPParametersEdit
        /// </summary>
        /// <returns></returns>
        public void SAPParametersEdit(TransactionsConsolideReport model)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[General].[Sp_ParameterSAPReport_AddOrEdit]",
                    new
                    {
                        BancoPropio = model.BancoPropio,
	                    BloqueoPagoHaber =  model.BloqueaPagoHaber,
	                    CabeceraCarga = model.TextoCabecera,
	                    CalcImpuesto = model.CalcImpuesto,
	                    Centro = model.Centro,
	                    CentroBeneficio = model.CentroBeneficio,
	                    CentroCosto = model.CentroCosto,
	                    CentroGestor = model.CentroGestor,
	                    ClaseDocumento = model.DocumentClass,
	                    CuentaDebe = model.Account,
	                    Fondo = model.Fondo,
	                    IndCme = model.IND_CME,
	                    IndicadorImpuesto = model.IndicadorImpuesto,
	                    Moneda = model.Currency,
	                    Orden = model.Orden,
	                    Pep = model.PEP,
	                    PosicionPresupuesto = model.PosicionPresupuesto,
	                    ProgramaPresupuesto = model.ProgramaPresupuesto,
	                    Sociedad = model.Sociedad,
	                    ViaPagoHaber = model.ViaPagoHaber,
                        ServiceStationNumber = model.ServiceStationNumber,
                        ReferenceNumber = model.ReferenceNumber,
                        CECO = model.CECO
                    });
                }
            }
            catch (Exception)
            {                
                throw;
            }
        }

        /// <summary>
        /// SAPParametersEdit
        /// </summary>
        /// <returns></returns>
        public void ExportableTranFileParametersEdit(ExportableTransactionsFileParameters model)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[General].[Sp_ExportableTransFileParam_AddOrEdit]",
                    new
                    {
                        CustomerId = Convert.ToInt32(ECOsystem.Utilities.Session.GetCustomerId()),
                        DocumentType = model.DocumentType,
                        CurrencyCode = model.CurrencyCode,
                        RecordType = model.RecordType,
                        Package = model.Package,
                        OnlyAuthorize = model.OnlyAuthorize,
                        UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// SAPParametersRetrieve
        /// </summary>
        /// <returns></returns>
        public TransactionsConsolideReport SAPParametersRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                IEnumerable<TransactionsConsolideReport> model = new List<TransactionsConsolideReport>();

                model = dba.ExecuteReader<TransactionsConsolideReport>("[General].[Sp_ParameterSAPReport_Retrieve]", new { });

                return model.FirstOrDefault();
            }        
        }

        /// <summary>
        /// Exportable Transaction File Parameters
        /// </summary>
        /// <returns></returns>
        public ExportableTransactionsFileParameters ExportableTransactionFileParamRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                IEnumerable<ExportableTransactionsFileParameters> model = new List<ExportableTransactionsFileParameters>();

                model = dba.ExecuteReader<ExportableTransactionsFileParameters>("[General].[Sp_ExportableTransFileParam_Retrieve]", 
                    new {
                        CustomerId = Convert.ToInt32(ECOsystem.Utilities.Session.GetCustomerId())
                    });

                return model.FirstOrDefault();
            }
        }

        #endregion

        /// <summary>
        /// GC Method
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}