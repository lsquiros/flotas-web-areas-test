﻿/************************************************************************************************************
*  File    : VehicleScheduleBusiness.cs
*  Summary : VehicleSchedule Business Methods
*  Author  : Berman Romero
*  Date    : 09/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// VehicleSchedule Class
    /// </summary>
    public class VehicleScheduleBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleSchedule
        /// </summary>
        /// <param name="vehicleId">The vehicle Id to find all vehicles associated</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public VehicleSchedule RetrieveVehicleSchedule(int? vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<VehicleSchedule>("[General].[Sp_VehicleSchedule_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId
                    }).FirstOrDefault();

                return result ?? new VehicleSchedule { VehicleId = vehicleId };
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleSchedule
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleSchedule(VehicleSchedule model)
        {
            var js = new JavaScriptSerializer();
            var schedules = js.Deserialize<VehicleScheduleJson[]>(model.JsonSchedule);

            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            foreach (var item in schedules)
            {
                root.Add(new XElement("Schedule", 
                    new XAttribute("VehicleId", "" + model.VehicleId),
                    new XAttribute("Time", item.id.Substring(0,2)+":"+item.id.Substring(2,2)),
                    new XAttribute("Day", item.id.Substring(4,2))));
            }
            model.XmlSchedule = doc.ToString();
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleSchedule_AddOrEdit]",
                    new
                    {
                        model.VehicleId,
                        model.JsonSchedule,
                        model.XmlSchedule,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleSchedule
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehicleSchedule(int vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleSchedule_Delete]",
                    new { VehicleId = vehicleId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}