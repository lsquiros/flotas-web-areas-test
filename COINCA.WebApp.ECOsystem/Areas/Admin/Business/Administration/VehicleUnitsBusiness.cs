﻿/************************************************************************************************************
*  File    : VehicleUnitsBusiness.cs
*  Summary : VehicleUnits Business Methods
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
//using COINCA.Library.DataAccess;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using ModuleConfig;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// VehicleUnits Class
    /// </summary>
    public class VehicleUnitsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleUnits
        /// </summary>
        /// <param name="unitId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleUnits> RetrieveVehicleUnits(int? unitId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<VehicleUnits>("[General].[Sp_VehicleUnits_Retrieve]",
                    new
                    {
                        UnitId = unitId,
                        CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleUnits
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleUnits(VehicleUnits model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleUnits_AddOrEdit]",
                    new
                    {
                        model.UnitId,
                        model.Name,
                        CustomerId= ECOsystem.Utilities.Session.GetCustomerId(),
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleUnits
        /// </summary>
        /// <param name="unitId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehicleUnits(int unitId)
        {
            var UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId;
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleUnits_Delete]",
                    new { 
                        UnitId = unitId,
                        ModifyUserId = UserId
                        });
            }
        }

        /// <summary>
        /// GetVehicleUnitsDetail
        /// </summary>
        /// <returns></returns>
        private List<VehicleUnitsDetail> GetVehicleUnitsDetail()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<VehicleUnitsDetail>("[General].[Sp_VehicleInfoByUnit_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    }).ToList();
            }
        }

        /// <summary>
        /// Vehicles Generate
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataSet ReportData(List<VehicleUnits> list)
        {
            DataSet ds = new DataSet();

            using (var dt = new DataTable("VehicleUnitsReport"))
            {
                dt.Columns.Add("UnitId");
                dt.Columns.Add("UnitName");
                dt.Columns.Add("VehicleCount");
                dt.Columns.Add("CustomerName");
                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();
                    row["UnitId"] = item.UnitId;
                    row["UnitName"] = item.Name;
                    row["VehicleCount"] = item.VehicleCount;
                    row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    dt.Rows.Add(row);
                }
                ds.Tables.Add(dt);
            }

            var DetailList = GetVehicleUnitsDetail().Where(m => list.Exists(x => (int)x.UnitId == m.UnitId));

            using (var dt = new DataTable("VehicleUnitsDetail"))
            {
                dt.Columns.Add("UnitId");
                dt.Columns.Add("VehicleName");
                dt.Columns.Add("PlateId");
                dt.Columns.Add("Manufacturer");
                dt.Columns.Add("VahicleModel");
                dt.Columns.Add("Type");
                dt.Columns.Add("CostCenter");
                dt.Columns.Add("DriverName");
                dt.Columns.Add("GPS");
                foreach (var item in DetailList)
                {
                    DataRow row = dt.NewRow();
                    row["UnitId"] = item.UnitId;
                    row["VehicleName"] = item.VehicleName;
                    row["PlateId"] = item.PlateId;
                    row["Manufacturer"] = item.Manufacturer;
                    row["VahicleModel"] = item.VehicleModel;
                    row["Type"] = item.Type;
                    row["CostCenter"] = item.CostCenterName;
                    row["DriverName"] = item.DriverNameStr;
                    row["GPS"] = item.GPS;
                    dt.Rows.Add(row);
                }
                ds.Tables.Add(dt);
            }

            return ds;
        }
              
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}