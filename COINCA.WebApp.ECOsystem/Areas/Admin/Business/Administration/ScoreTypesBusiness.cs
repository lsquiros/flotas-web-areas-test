﻿/************************************************************************************************************
*  File    : ScoreTypesBusiness.cs
*  Summary : ScoreTypes Business Methods
*  Author  : Napoleón Alvarado
*  Date    : 01/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Models.Administration;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    /// <summary>
    /// ScoreTypes Class
    /// </summary>
    public class ScoreTypesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve ScoreTypes
        /// </summary>
        /// <param name="scoreTypeId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<ScoreTypes> RetrieveScoreTypes(int? scoreTypeId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ScoreTypes>("[Operation].[Sp_ScoreTypes_Retrieve]",
                    new
                    {
                        ScoreTypeId = scoreTypeId,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity ScoreTypes
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditScoreTypes(ScoreTypes model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Operation].[Sp_ScoreTypes_AddOrEdit]",
                    new
                    {
                        model.ScoreTypeId,
                        model.Code,
                        model.Name,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model ScoreTypes
        /// </summary>
        /// <param name="scoreTypeId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteScoreTypes(int scoreTypeId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Operation].[Sp_ScoreTypes_Delete]",
                    new { ScoreTypeId = scoreTypeId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}