﻿using  ECOsystem.Areas.Admin.Models.Administration;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    public class ScoreParametersBusiness : IDisposable
    {
        public ScoreParameters RetrieveParameters()
        {
            using(var dba = new DataBaseAccess())
            {
                var res = dba.ExecuteReader<ScoreParameters>("[General].[Sp_ScoreParameters_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    }).FirstOrDefault();

                res = res ?? new ScoreParameters();
                res.List = GetList(res.XMLData).ToList();
                return res;
            }
        }

        public void ScoreParameterAddOrEdit(ScoreParameters model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ScoreParameters_AddOrEdit]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        model.SpeedPercentage,
                        model.SpeedActive,
                        model.GPSPercentage,
                        model.GPSActive,
                        XMLData = ECOsystem.Utilities.Miscellaneous.GetXML(model.List),
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        IEnumerable<GPSScoreParameters> GetList(string XMLData)
        {
            //if(!string.IsNullOrEmpty(XMLData))
            //{
                var xml = XDocument.Parse(string.Format("<root>{0}</root>", XMLData));
                var rows = from node in xml.Descendants("row")
                           select new GPSScoreParameters
                           {
                               Id = Convert.ToInt32(node.Attribute("Id").Value),
                               From = Convert.ToInt32(node.Attribute("From").Value),
                               To = Convert.ToInt32(node.Attribute("To").Value),
                               Percentage = Convert.ToInt32(node.Attribute("Percentage").Value)
                           };
                return rows;
            //}
            //return new List<GPSScoreParameters>();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}