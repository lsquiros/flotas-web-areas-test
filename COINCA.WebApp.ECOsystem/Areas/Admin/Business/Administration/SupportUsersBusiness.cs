﻿using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Admin.Business.Administration
{
    public class SupportUsersBusiness : IDisposable
    {
        public static bool ValidatesSupportUser()
        {
            var user = ECOsystem.Utilities.Session.GetUserInfo();

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<bool>("[General].[Sp_ValidatesSupportUser_Retrieve]",
                new
                {
                    RoleId = user.RoleId,
                    UserName = user.EncryptedUserName
                });
            }
        }

        /// <summary>
        /// unlock Users
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public bool UnlockUser(string UserName, bool ResetPass)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<bool>("[General].[Sp_UnlockSupportUser_Retrieve]",
                new
                {                 
                    UserName,
                    ResetPass
                });
            }
        }

        /// <summary>
        /// GC
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}