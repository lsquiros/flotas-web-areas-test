﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using ECOsystem.DataAccess;
using  ECOsystem.Areas.Admin.Business.Administration;
using  ECOsystem.Areas.Admin.Models.Administration;

namespace  ECOsystem.Areas.Admin.Business.Utilities
{
    /// <summary>
    /// General Collections Class Contains all methods for load Drop Down List
    /// </summary>
    public class GeneralCollections
    {

        /// <summary>
        /// Performs Alarm Credit Count
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public static int AlarmCreditCountRetrieve(int pMinPct)
        {
            int Year = DateTime.Today.Year;
            int Month = DateTime.Today.Month;

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Control].[Sp_AlarmCreditCount_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        Year = Year,
                        Month = Month,
                        MinPct = pMinPct,
                        UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }
            

        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetDatesGroupBy
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {1, "Agrupación por: Año"},
                    {2, "Agrupación por: Mes"},
                    {4, "Agrupación por: Semana"},
                    {3, "Agrupación por: Día"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetComparativeGroupBy
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {100, "Comparación por: Vehículo"},
                    {200, "Comparación por: Marca"},
                    {300, "Comparación por: Modelo"},
                    {400, "Comparación por: Tipo"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }
               
        /// <summary>
        /// Return a list of Countries for current user in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersByPartner
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new ECOsystem.Business.Core.CustomersBusiness())
                {
                    var result = business.RetrieveCustomersCollectionByPartners(ECOsystem.Utilities.Session.GetPartnerId());
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CustomerId, r.DecryptedName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Countries for current user in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersByPartnerWithAll
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(-1, "Todos");
                using (var business = new ECOsystem.Business.Core.CustomersBusiness())
                {
                    var result = business.RetrieveCustomersCollectionByPartners(ECOsystem.Utilities.Session.GetPartnerId());
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CustomerId, r.DecryptedName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of States in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetStates(int? countryId)
        {
            var dictionary = new Dictionary<int?, string>();

            using (var business = new GeographicDivisionBusiness())
            {
                var result = business.RetrieveStates(null, countryId);
                foreach (var r in result)
                {
                    dictionary.Add(r.StateId, r.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        /// <summary>
        /// Return a list of Counties in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCounties(int? stateId, int? countryId)
        {
            var dictionary = new Dictionary<int?, string>();
            if (stateId != null)
            {
                using (var business = new GeographicDivisionBusiness())
                {
                    var result = business.RetrieveCounties(null, stateId, countryId);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CountyId, r.Code + " " + r.Name);
                    }
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        /// <summary>
        /// Return a list of Cities in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCities(int? countyId, int? stateId, int? countryId)
        {
            var dictionary = new Dictionary<int?, string>();
            if (countyId != null)
            {
                using (var business = new GeographicDivisionBusiness())
                {
                    var result = business.RetrieveCities(null, countyId, stateId, countryId);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CityId, r.Code + " " + r.Name);
                    }
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        /// <summary>
        /// Return a list of Acces Countries for selected user in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCountriesAccess
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                var user = Session.GetUserInfo();
                if (!user.IsPartnerUser) return new SelectList(dictionary, "Key", "Value");

                foreach (var r in user.PartnerUser.CountriesAccessList.Where(x => x.IsCountryChecked))
                {
                    dictionary.Add(r.CountryId, r.CountryName);
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
                
        /// <summary>
        /// Return a list of Drivers in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetDrivers
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new DriversBusiness())
                {
                    var result = business.RetrieveDrivers();
                    dictionary.Add(-1, "{'Name': 'Todos'}");
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.UserId))
                        {
                            dictionary.Add(r.UserId, "{'Name':'" + r.DecryptedName + "'}");
                        }
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Drivers in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetDriversWithAll
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(-1, "{'Name':'Todos'}");
                using (var business = new DriversBusiness())
                {
                    var result = business.RetrieveDrivers();
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.UserId))
                        {
                            dictionary.Add(r.UserId, "{'Name':'" + r.DecryptedName + "'}");
                        }
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Fuels in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetFuelsByCountry
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new FuelsBusiness())
                {
                    var result = business.RetrieveFuels(null, countryId: ECOsystem.Utilities.Session.GetCountryId());
                    foreach (var r in result)
                    {
                        dictionary.Add(r.FuelId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Fuels in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetFuels
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new FuelsBusiness())
                {
                    var result = business.RetrieveFuels(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.FuelId, r.Name + " - " + r.CountryName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
               
        /// <summary>
        /// Return a list of Customers in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomers
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new ECOsystem.Business.Core.CustomersBusiness())
                {
                    var result = business.RetrieveCustomers(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CustomerId, r.DecryptedName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of active Customers
        /// </summary>
        public static int GetActiveCustomersCount(int? partnerId,int? customerId,int? countryId)
        {
                using (var business = new ECOsystem.Business.Core.CustomersBusiness())
                {
                    return business.RetrieveActiveCustomersCount(partnerId, customerId, countryId);
                  
                }
        }

        /// <summary>
        /// Return a list of not active Customers
        /// </summary>
        public static int GetNotActiveCustomersCount(int? partnerId, int? customerId, int? countryId)
        {
            using (var business = new ECOsystem.Business.Core.CustomersBusiness())
            {
                return business.RetrieveNotActiveCustomersCount(partnerId, customerId, countryId);

            }
        }
      
        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicles
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicles(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.VehicleId))
                        {
                            dictionary.Add(r.VehicleId, "{'Name':'" + r.Name + "', 'CategoryType':'" + r.CategoryType + "', 'FuelName': '" + r.FuelName + "', 'CostCenterName': '" + r.CostCenterName + "', 'PlateId': '" + r.PlateId + "'}");
                        }
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetVehiclesWithNoCreditCard
        {
            get
            {
                var dictionary = new Dictionary<string, string>();
                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehiclesWithNoCreditCard();
                    result = result.OrderBy(x => x.PlateId);

                    if (result.Count() > 0)
                        dictionary.Add("-1", "Todos");

                    foreach (var item in result)
                    {
                        dictionary.Add(item.PlateId, item.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesWithAll
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(-1, "{'Name':'Todos', 'CategoryType':'Todos', 'FuelName': 'Todos', 'CostCenterName': 'Todos', 'PlateId': 'Todos'}");
                using (var business = new VehiclesBusiness())
                    foreach (var r in business.RetrieveVehicles(null))
                        if (!dictionary.ContainsKey(r.VehicleId))
                            dictionary.Add(r.VehicleId, "{'Name':'" + r.Name + "', 'CategoryType':'" + r.CategoryType + "', 'FuelName': '" + r.FuelName + "', 'CostCenterName': '" + r.CostCenterName + "', 'PlateId': '" + r.PlateId + "'}");
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Units in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleUnits
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehicleUnitsBusiness())
                {
                    var result = business.RetrieveVehicleUnits(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.UnitId, "{'Name':'" + r.Name + "'}");
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Sub Units in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleCostCenters
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var result = new VehicleCostCentersBusiness().RetrieveVehicleCostCenters(null,true);
                foreach (var r in result)
                {
                    dictionary.Add(r.CostCenterId, r.Name);
                }
                
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Sub Units in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleCostCentersProgramSendReports
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var result = new VehicleCostCentersBusiness().RetrieveVehicleCostCenters(null, true);
                dictionary.Add(-1, "Todos");

                foreach (var r in result)
                {
                    dictionary.Add(r.CostCenterId, r.Name);
                }

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleGroupsProgramSendReports
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(-1, "Todos");

                using (var business = new VehicleGroupsBusiness())
                {
                    var result = business.RetrieveVehicleGroups(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.VehicleGroupId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of users in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetUsersProgramSendReports
        {
            get
            {
                var result = new ECOsystem.Business.Core.UsersBusiness().RetrieveUsers(null, customerId: Session.GetCustomerId(), IsCustomer: true, UserStatus: 1);

                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(-1, "Todos");
                foreach (var r in result)
                {
                    dictionary.Add(r.UserId, r.DecryptedName + " " + r.DecryptedEmail);
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Categories in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleCategories
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehicleCategoriesBusiness())
                {
                    var result = business.RetrieveVehicleCategories(null,true);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.VehicleCategoryId, "{'Manufacturer':'" + r.Manufacturer + "', 'VehicleModel':'" + r.VehicleModel + "', 'Year': '" + r.Year + "', 'Type': '" + r.Type + "', 'FuelName': '" + r.FuelName + "', 'Liters': '" + r.Liters + "L'}");
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Drivers in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleDrives
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new DriversBusiness())
                {
                    var result = business.RetrieveDrivers();
                    foreach (var r in result)
                    {
                        dictionary.Add(r.UserId, "{'Name':'" + r.DecryptedName + "', 'Code':'" + r.DriverUser.DecryptedCode + "', 'Identification': '" + r.DriverUser.DecryptedIdentification + "', 'Photo': '" + r.Photo + "'}");
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetVehicleDrivesList
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new DriversBusiness())
                {
                    var result = business.RetrieveCustomerDrivers(customerId: ECOsystem.Utilities.Session.GetCustomerId());
                    dictionary.Add(-1, "Todos");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.UserId, r.DecryptedName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetDriversWithNoCreditCard
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var bus = new DriversBusiness())
                {
                    var result = bus.DriversNoCreditCardRetrieve();

                    result = result.OrderBy(x => x.DecryptedName);
                    if (result.Count() > 0)
                        dictionary.Add(-1, "Todos");

                    foreach (var item in result)
                    {
                        dictionary.Add(item.UserId, item.DecryptedName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Drivers from Vehicle in order to using it for populate DropDownListFor
        /// </summary>
        public static Dictionary<int?, string> GetDrivesByVehicle(int? vehicleId)
        {

            var dictionary = new Dictionary<int?, string>();

            using (var business = new DriversBusiness())
            {
                var customerId = Session.GetCustomerId();
                var result = business.RetrieveDriversByVehicle(vehicleId, ((customerId != null) ? (int)customerId : 0));
                foreach (var r in result)
                {
                    if (!dictionary.ContainsKey(r.UserId))
                    {
                        dictionary.Add(r.UserId, "{'Name':'" + r.DecryptedName + "', 'Code':'" + r.DriverUser.DecryptedCode + "', 'Identification': '" + r.DriverUser.DecryptedIdentification + "', 'Photo': '" + r.Photo + "'}");
                    }
                }
            }
            return dictionary;

        }

        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehicleGroups
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehicleGroupsBusiness())
                {
                    var result = business.RetrieveVehicleGroups(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.VehicleGroupId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Customer Credit Card Status in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomerCreditCardStatus
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new StatusBusiness())
                {
                    var user = Session.GetUserInfo();
                    var result = business.RetrieveStatus("CUSTOMER_CREDIT_CARD_" + ECOsystem.Utilities.Session.GetProfileByRole(user.RoleId));
                    foreach (var r in result)
                    {
                        dictionary.Add(r.StatusId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetPartnerCardStatus
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new StatusBusiness())
                {
                    var result = business.RetrieveStatus("PARTNER_MASTER_CARD");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.StatusId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Customers Units Of Capacity for populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersUnitsOfCapacity
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("CUSTOMERS_CAPACITY_UNITS");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Partner Types to populate DropDownList
        /// </summary>
        public static SelectList GetPartnerTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("PARTNER_TYPE");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Load Types for populating DropDownListFor
        /// </summary>
        public static SelectList GetVehicleLoadTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("VEHICLES_LOAD_WEIGHT");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Customers Credit Card Issue For populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersCreditCardIssueForId
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("CUSTOMERS_CREDIT_CARDS_ISSUE_FOR");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Customers Credit Card Types For populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersCreditCardTypes
        {
            get
            {
                var dictionary = new Dictionary<string, string> { { "C", "Crédito" }, { "D", "Débito" } };
                return new SelectList(dictionary, "Key", "Value", "C");
            }
        }

        /// <summary>
        /// Return a list of Periodicity For populate DropDownListFor
        /// </summary>
        public static SelectList GetPeriodicity
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("CUSTOMER_PERIODICITY_VEHICLE");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, "{'Name':'" + r.Name + "'}");
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Periodicity Alarm For populate DropDownListFor
        /// </summary>
        public static SelectList GetPeriodicityAlarm
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("CUSTOMERS_PERIODICITY_ALARM");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, "{'Name':'" + r.Name + "'}");
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Frequency For populallte DropDownListFor
        /// </summary>
        public static SelectList GetFrequency
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new TypesBusiness())
                {
                    var result = business.RetrieveTypes("CUSTOMER_FREQUENCY");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetMonthNames
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a current Month Name
        /// </summary>
        public static SelectList GetCurrentMonthName
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.Where(w => w.Id == DateTime.Now.Month).ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year - 5; i < year + 9; i++)
                {
                    dictionary.Add(i, i);
                }
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        public static SelectList GetCardYears
        {
            get
            { 
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year ; i < year + 15; i++)
                {   //ValidCreditCard for 15 years
                    dictionary.Add(i, i);
                }
                year = year + 1;
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }
        
        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterDetailTransacReport
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var year = DateTime.UtcNow.Year;
                dictionary.Add(1, "Centro de Costo");
                dictionary.Add(2, "Grupo de Vehículos");
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterSettingOdometers
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();                
                dictionary.Add(1, "Odómetros Erróneos");
                dictionary.Add(2, "Historial de Odómetros");
                return new SelectList(dictionary, "Key", "Value", 1);
            }
        }


        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterPartnerCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var year = DateTime.UtcNow.Year;
                dictionary.Add(1, "Socio");
                dictionary.Add(2, "Clientes");
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetReportCriteria
        {
            get
            {
                return new SelectList(new Dictionary<int, string> { 
                    {(int)ReportCriteria.Period,"Por Período"},
                    {(int)ReportCriteria.DateRange,"Rango de Fechas"}
                }, "Key", "Value", "");
            }
        }

        /// <summary>
        /// Return the modality of the GPS
        /// </summary>
        public static SelectList GetGPSModality
        {
            get
            {
                return new SelectList(new Dictionary<int, string> { 
                    {1,"Alquilado"},
                    {2,"Propio"}
                }, "Key", "Value", "");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Groups in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesByCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicles(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.VehicleId))
                        {
                            dictionary.Add(r.VehicleId, "{'PlateId':'" + r.PlateId + "', 'Name':'" + string.Format("{0}", r.Name.Length > 30 ? r.Name.Substring(0, 30) : r.Name) + "', 'CategoryType':'" + 
                                r.CategoryType + "', 'FuelName': '" + r.FuelName + "', 'CostCenterName': '" + r.CostCenterName + "'}");
                        }
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static IEnumerable<Vehicles> VehiclesListByCustomer
        {
            get
            {
                using (var business = new VehiclesBusiness())
                {
                    var result = business.RetrieveVehicles(null);
                    return result;
                }
            }
        }

        /// <summary>
        /// Return a list of Vehicle Manufacturers in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesManufacturers
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                using (var business = new VehicleCategoriesBusiness())
                {
                    var result = business.RetrieveVehicleCategories(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.Manufacturer)) dictionary.Add(r.Manufacturer, r.Manufacturer);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Models in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesModels
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                using (var business = new VehicleCategoriesBusiness())
                {
                    var result = business.RetrieveVehicleCategories(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.VehicleModel)) dictionary.Add(r.VehicleModel, r.VehicleModel);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Types in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesTypes
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                using (var business = new VehicleCategoriesBusiness())
                {
                    var result = business.RetrieveVehicleCategories(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.Type)) dictionary.Add(r.Type, r.Type);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Types in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesCylinderCapacities
        {
            get
            {
                var dictionary = new Dictionary<int?, int?>();

                using (var business = new VehicleCategoriesBusiness())
                {
                    var result = business.RetrieveVehicleCategories(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.CylinderCapacity)) dictionary.Add(r.CylinderCapacity, r.CylinderCapacity);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Vehicle Years in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetVehiclesYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int?>();

                using (var business = new VehicleCategoriesBusiness())
                {
                    var result = business.RetrieveVehicleCategories(null);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.Year)) dictionary.Add(r.Year, r.Year);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
        
        /// <summary>
        /// Return a list of currencies in order to using it for populate DropDownList
        /// </summary>
        public static SelectList GetPreventiveMaintenanceByCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new PreventiveMaintenanceCatalogBusiness())
                {
                    var result = business.RetrievePreventiveMaintenance(ECOsystem.Utilities.Session.GetCustomerId(), FilterType: null, preventiveMaintenanceCatalogId: null, key: null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.PreventiveMaintenanceCatalogId, "{'Description':'" + r.Description + "', 'FrequencyKm':'" + r.FrequencyKm + "', 'AlertBeforeKm':'" + r.FrequencyMonth + "', 'FrequencyMonth':'" + r.FrequencyMonth + "', 'AlertBeforeMonth':'" + r.AlertBeforeMonth + "', 'FrequencyDate':'" + r.FrequencyDate + "', 'AlertBeforeDate':'" + r.AlertBeforeDate + "'}");
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }

        /// <summary>
        /// Return a list of currencies in order to using it for populate DropDownList
        /// </summary>
        public static SelectList GetPreventiveMaintenanceCatalog
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var business = new PreventiveMaintenanceCatalogBusiness())
                {
                    var result = business.RetrievePreventiveMaintenance(ECOsystem.Utilities.Session.GetCustomerId(), FilterType: null, preventiveMaintenanceCatalogId: null, key: null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.PreventiveMaintenanceCatalogId, r.Description);
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }

        /// <summary>
        /// Return a list of Icon of vehicle in order to using for populate DropDownList 
        /// </summary>
        public static SelectList GetIconVehicleCategory
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                string path = HttpContext.Current.Server.MapPath("~/Content/Images/Vehicles/");
           
                if (Directory.Exists(path))
                {
                    List<String> listImage = Directory.GetFiles(path, "*.png", SearchOption.AllDirectories).ToList();
                    foreach (String itemImage in listImage)
                    {
                        dictionary.Add(Path.GetFileName(itemImage), "{'image':'" + Path.GetFileName(itemImage) + "'}");
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of currencies in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCurrenciesByCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new CurrenciesBusiness())
                {
                    var result = business.RetrieveCurrenciesByCustomer(ECOsystem.Utilities.Session.GetCustomerId());
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CurrencyId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of currency symbol in order to using it for populate DropDownListFor 
        /// </summary>
        public static SelectList GetCurrencySymbolByCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new CurrenciesBusiness())
                {
                    var result = business.RetrieveCurrenciesByCustomer(ECOsystem.Utilities.Session.GetCustomerId());
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CurrencyId, r.Symbol);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
              
        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomerRoles
        {
            get
            {
                return new SelectList(new Dictionary<string, string> { 
                    {"CUSTOMER_ADMIN","Administrador"},
                    {"CUSTOMER_USER","Usuario Regular"},
                }, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetDriverRoles
        {
            get
            {
                return new SelectList(new Dictionary<string, string> { 
                    {"CUSTOMER_DRIVER","Conductor"}
                }, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Card Request available from Customer
        /// </summary>
        public static SelectList GetDriverCardRequestByCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new ECOsystem.Business.Core.CustomersBusiness())
                {
                    var result = business.RetrieveCustomerCardRequest(ECOsystem.Utilities.Session.GetCustomerId(), null);
                    
                    foreach (var r in result.Where(w => w.IssueForId == 100 && !string.IsNullOrEmpty(w.DriverName)))
                    {
                        if (!dictionary.Keys.Contains(r.CardRequestId))
                        {
                            dictionary.Add(r.CardRequestId, "{'NumSolicitud':'" + string.Format("#{0}", r.CardRequestId) + "', 'DriverName':'" + string.Format("({0}){1}", r.DriverIdentification, r.DriverName) + "', 'StateName': '" + ((!string.IsNullOrEmpty(r.StateName)) ? r.StateName : "No Disponible") + "', 'CityName': '" + ((!string.IsNullOrEmpty(r.CityName)) ? r.CityName : "No Disponible") + "'}");
                        }
                    }
                }

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetSuperRoles
        {
            get
            {
                return new SelectList(new Dictionary<string, string> { 
                    {"SUPER_ADMIN","Administrador"},
                    {"SUPER_USER","Usuario Regular"},
                }, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetPartnerRoles
        {
            get
            {
                if (ECOsystem.Utilities.Session.GetPartnerInfo().PartnerTypeId != 801)
                {
                    return new SelectList(new Dictionary<string, string> { 
                    {"PARTNER_ADMIN","Administrador"},
                    {"PARTNER_USER","Usuario Regular"},
                    }, "Key", "Value");
                }
                else
                {
                    return new SelectList(new Dictionary<string, string> { 
                    {"INSURANCE_ADMIN","Administrador"},
                    {"INSURANCE_USER","Usuario Regular"},
                    }, "Key", "Value");

                }
            }
        }

        public static SelectList GetRoles
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new ScoreTypesBusiness())
                {
                    var result = business.RetrieveScoreTypes(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.ScoreTypeId, r.Code + " " + r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Score Types in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetScoreTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new ScoreTypesBusiness())
                {
                    var result = business.RetrieveScoreTypes(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.ScoreTypeId, r.Code + " " + r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of AvailableDallasKeys in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetAvailableDallasKeys
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(0, "{'Code':'Ninguna'}");
                using (var business = new ECOsystem.Business.Core.DallasKeysBusiness())
                {
                    var result = business.RetrieveDallasKeys(null, isUsed: false);
                    foreach (var r in result)
                    {
                        if (!dictionary.ContainsKey(r.DallasId))
                        {
                            dictionary.Add(r.DallasId, "{'Code':'" + r.DallasCode + "'}");
                        }
                    }
                }
                
                return new SelectList(dictionary, "Key", "Value");
            }
        }        

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static HtmlString GetMonthCardNames(int p)
        {     
            if (p == DateTime.Now.Year)
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name))
                                       .SkipWhile(element => element.Key < DateTime.Now.Month).ToList();
                return new HtmlString(JsonConvert.SerializeObject(dictionary.ToList()));
            }
            else
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new HtmlString(JsonConvert.SerializeObject(dictionary.ToList()));
            }
       }



        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetFilterMaintenanceType
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {0, "Todos"},
                    {1, "Preventivos"},
                    {2, "Correctivos"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }


        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetStatusDriver
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {2, "Todos"},
                    {1, "Activos"},
                    {0, "Inactivos"},
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Preventive Maintenance Report Status
        /// </summary>
        public static SelectList GetReportPreventiveMaintenanceStatus
        {
            get
            {
                return new SelectList(new Dictionary<int, string> {
                    {(int)ReportPreventiveMaintenanceStatus.Pendientes,"Mantenimientos Pendientes"},
                    {(int)ReportPreventiveMaintenanceStatus.Realizado,"Mantenimientos Realizados"},
                    {(int)ReportPreventiveMaintenanceStatus.Vencidos,"Mantenimientos Vencidos"},
                }, "Key", "Value", (int)ReportPreventiveMaintenanceStatus.Pendientes);
            }
        }


        /// <summary>
        /// Return a list of currency symbol in order to using it for populate DropDownListFor 
        /// </summary>
        public static SelectList GetCreditCardStatus
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new CreditCardBusiness())
                {
                    dictionary.Add(-1, "Todos");

                    var result = business.RetrieveStatusofCreditsCards();
                    foreach (var r in result)
                    {
                        dictionary.Add(r.StatusId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of currency symbol in order to using it for populate DropDownListFor 
        /// </summary>
        public static SelectList GetCreditCardStatusWithoutRequestedStatus
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new CreditCardBusiness())
                {
                    dictionary.Add(-1, "Todos");

                    var result = business.RetrieveStatusofCreditsCards();
                    foreach (var r in result)
                    {
                        if (!r.Name.Equals("Solicitada"))
                            dictionary.Add(r.StatusId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetCreditCardStatusRequested
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new CreditCardBusiness())
                {
                    dictionary.Add(-1, "Todos");
                    var result = business.RetrieveStatusofCreditsCards();
                    foreach (var r in result)
                    {
                        if (r.Name.Equals("Solicitada") || r.Name.Equals("Bloqueada") || r.Name.Equals("Activa"))
                        {
                            dictionary.Add(r.StatusId, r.Name);
                        }   
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }
    }
}