﻿/************************************************************************************************************
*  File    : CurrenciesModels.cs
*  Summary : Currencies Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// Currencies Base
    /// </summary>
    public class CurrenciesBase
    {
        /// <summary>
        /// CurrenciesBase constructor
        /// </summary>
        public CurrenciesBase()
        {
            Data = new Currencies();
            List = new List<Currencies>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Currencies Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Currencies> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// Currencies Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Currencies : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int? CurrencyId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre Moneda")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre Moneda", Width = "200px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// Code for Currencies
        /// </summary>
        [DisplayName("Código")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Código", Width = "100px", SortEnabled = true)]
        public string Code { get; set; }

        /// <summary>
        /// Symbol of Currencies
        /// </summary>
        [DisplayName("Símbolo")]
        [GridColumn(Title = "Símbolo", Width = "20px", SortEnabled = true)]
        public string Symbol { get; set; }
    }
}