﻿using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    public class ConsolidateExpensesReportBase
    {
        public ConsolidateExpensesReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            Menus = new List<AccountMenus>();
            List = new List<ConsolidateExpensesList>();

        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        public IEnumerable<ConsolidateExpensesList> List { get; set; }
    }

    public class ConsolidateExpensesList
    {
        public int VehicleId { get; set; }

        public string VehicleName { get; set; }

        public string PlateId { get; set; }

        public decimal Liters { get; set; }

        public int Travel { get; set; }

        public decimal Performance { get; set; }

        public decimal FuelCost { get; set; }

        public decimal CostPerKilometer { get; set; }

        public bool HasGPS { get; set; }

        public int MaintenanceId { get; set; }

        public string MaintenanceName { get; set; }

        public decimal MaintenanceCost { get; set; }

        public string MaintenanceType { get; set; }

        public decimal MaintenanceSubtotal { get; set; }

    }
}

