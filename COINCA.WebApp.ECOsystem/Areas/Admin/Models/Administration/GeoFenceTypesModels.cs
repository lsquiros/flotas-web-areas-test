﻿using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using System.Collections.Generic;


namespace  ECOsystem.Areas.Admin.Models.Administration
{

    public class GeoFenceTypesBase
    { 
         public GeoFenceTypesBase()
        {
            Data = new GeoFenceTypes();
            List = new List<GeoFenceTypes>();
            Menus = new List<AccountMenus>();
        }

        public GeoFenceTypes Data { get; set; }

        public IEnumerable<GeoFenceTypes> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }
    
    /// <summary>
    /// GeoFenceTypes Models
    /// </summary>
    public class GeoFenceTypes : ModelAncestor
    {
        /// <summary>
        /// Status Id property
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Status property
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description property
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// IsHome property
        /// </summary>
        public bool IsHome { get; set; }

        /// <summary>
        /// Tolerance property
        /// </summary>
        public double Tolerance { get; set; }
        
    }
}