﻿/************************************************************************************************************
*  File    : VehicleGroupsModels.cs
*  Summary : VehicleGroups Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// VehicleGroups Base
    /// </summary>
    public class VehicleGroupsBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public VehicleGroupsBase()
        {
            Data = new VehicleGroups();
            List = new List<VehicleGroups>();
            Menus = new List<AccountMenus>();
            Parameters = new AdminReportsBase();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public AdminReportsBase Parameters { get; set; }


        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleGroups Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleGroups> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// VehicleGroups Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class VehicleGroups : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre del Grupo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre del Grupo", Width = "150px", SortEnabled = true)]
        public string Name { get; set; }
        
        /// <summary>
        /// Description of the group
        /// </summary>
        [DisplayName("Descripción")]
        [GridColumn(Title = "Descripción", Width = "220px", SortEnabled = true)]
        public string Description { get; set; }

        /// <summary>
        /// Sum of the Vehicles on the Cost Center
        /// </summary>  
        [DisplayName("Cant. de Vehiculos")]
        [GridColumn(Title = "Cant. de Vehiculos", Width = "100px", SortEnabled = true)]
        public int CantVehicles { get; set; }
        


    }
}