﻿/************************************************************************************************************
*  File    : AlarmsModels.cs
*  Summary : Alarms Models
*  Author  : Cristian Martínez Hernández
*  Date    : 17/Nov/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;
using System.Collections.Generic;
using ECOsystem.Models.Account;

namespace ECO_Admin.Models.Administration
{
    /// <summary>
    /// Alarms Models
    /// </summary>
    public class AlarmsModels : ModelAncestor
    {
        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<AlarmsModels> List { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model 
        /// </summary>
        [NotMappedColumn]
        public int? AlarmId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model 
        /// </summary>
        [NotMappedColumn]
        public int? AlarmMaintenanceId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model 
        /// </summary>
        [NotMappedColumn]
        public int? AlarmStopTimeId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model 
        /// </summary>
        [NotMappedColumn]
        public int? AlarmTemperatureId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model 
        /// </summary>
        [NotMappedColumn]
        public int? AlarmLowBatteryId { get; set; }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model 
        /// </summary>
        [NotMappedColumn]
        public int? AlarmGPSDesconnectionId { get; set; }

        /// <summary>
        /// Fk User Id From User
        /// </summary>
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// Phones for send of the alarm
        /// </summary>
        [DisplayName("SMS (Teléfonos):")]
        //[Required(ErrorMessage="{0} es requerido.")]
        [NotMappedColumn]
        public string Phone { get; set; }

        /// <summary>
        /// Email for send of the alarm
        /// </summary>
        [DisplayName("Correos:")]        
        [NotMappedColumn]
        public string Email { get; set; }

        /// <summary>
        /// Email to show if is not email configured
        /// </summary>
        public string EmailPlaceHolder { get; set; }

        /// <summary>
        /// Email for send of the alarm
        /// </summary>
        [DisplayName("Correos:")]        
        [NotMappedColumn]
        public string EmailMaintenance { get; set; }

        /// <summary>
        /// Email to show if is not email configured
        /// </summary>
        public string EmailMaintenancePlaceHolder { get; set; }

        /// <summary>
        /// Email for send of the Stop Time Alarm
        /// </summary>
        [DisplayName("Correos:")]
        [NotMappedColumn]
        public string EmailStopTime { get; set; }

        /// <summary>
        /// Email to show if is not email configured
        /// </summary>
        public string EmailStopTimePlaceHolder { get; set; }

        /// <summary>
        /// Email for send of the temperature alarm 
        /// </summary>
        [DisplayName("Correos:")]
        [NotMappedColumn]
        public string EmailTemperature { get; set; }

        /// <summary>
        /// Email to show if is not email configured
        /// </summary>
        public string EmailTemperaturePlaceHolder { get; set; }

        /// <summary>
        /// Email for send of the battery alarm
        /// </summary>
        [DisplayName("Correos:")]
        [NotMappedColumn]
        public string EmailLowBattery { get; set; }

        /// <summary>
        /// Email to show if is not email configured
        /// </summary>
        public string EmailLowBatteryPlaceHolder { get; set; }

        /// <summary>
        /// Email for send of the GPS alaram
        /// </summary>
        [DisplayName("Correos:")]
        [NotMappedColumn]
        public string EmailGPSDesconnection { get; set; }

        /// <summary>
        /// Email to show if is not email configured
        /// </summary>
        public string EmailGPSDesconnectionPlaceHolder { get; set; }

        /// <summary>
        /// FK AlarmTriggerId from Types
        /// </summary>
        [NotMappedColumn]
        public int AlarmTriggerId { get; set; }


          /// <summary>
        /// FK AlarmTriggerId from Types
        /// </summary>
        [NotMappedColumn]
        public int AlarmTriggerIdMaintenance { get; set; }

        /// <summary>
        /// Id for stop time
        /// </summary>
        [NotMappedColumn]
        public int AlarmTriggerIdStopTimeGPS { get; set; }

        /// <summary>
        /// Id for Temperature battery alarm
        /// </summary>
        [NotMappedColumn]
        public int AlarmTriggerIdTemperature { get; set; }

        /// <summary>
        /// Id for Temperature sensors alarm
        /// </summary>
        [NotMappedColumn]
        public int AlarmTriggerIdLowBattery { get; set; }

        /// <summary>
        /// Id for Temperature GPS alarm
        /// </summary>
        [NotMappedColumn]
        public int AlarmTriggerIdGPSDesconnection { get; set; }

        /// <summary>
        /// FK Periodicity Type from Types
        /// </summary>
        [DisplayName("Periodicidad:")]
        [RequiredIf("AlarmTriggerId == 300", ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? PeriodicityTypeId { get; set; }

        /// <summary>
        /// FK Entity Type Id From Types
        /// </summary>
        [NotMappedColumn]
        public int EntityTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        public int EntityTypeIdLawSpeed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        public int EntityTypeIdCustomerSpeed { get; set; }

        /// <summary>
        /// FK EntityId from Drives, Vehicles, Group, SUbUnit, Unit
        /// </summary>
        [NotMappedColumn]
        public int EntityId { get; set; }

        /// <summary>
        /// Active Alarm
        /// </summary>
        [DisplayName("Habilitada")]
        [NotMappedColumn]
        public bool Active { get; set; }

                /// <summary>
        /// Active Alarm
        /// </summary>
        [DisplayName("Habilitada")]
        [NotMappedColumn]
        public bool ActiveMaintenance { get; set; }

        /// <summary>
        /// Active Alarm
        /// </summary>
        [DisplayName("Habilitada")]
        [NotMappedColumn]
        public bool ActiveStopTime { get; set; }

        /// <summary>
        /// Active Alarm Temperature
        /// </summary>
        [DisplayName("Habilitada")]
        [NotMappedColumn]
        public bool ActiveTemperature { get; set; }

        /// <summary>
        /// Active Alarm Low Battery
        /// </summary>
        [DisplayName("Habilitada")]
        [NotMappedColumn]
        public bool ActiveLowBattery { get; set; }

        /// <summary>
        /// Active Alarm GPS
        /// </summary>
        [DisplayName("Habilitada")]
        [NotMappedColumn]
        public bool ActiveGPSDesconnection { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string TitlePopUp { get; set; }

        /// <summary>
        /// Property for indicate that have periodicity
        /// </summary>
        [NotMappedColumn]
        public bool HasPeriodicity { get; set; }

        /// <summary>
        /// Type of the alarm
        /// </summary>
        [DisplayName("Tipo de Alarma:")]
        [NotMappedColumn]
        public string AlarmType { get; set; }

        /// <summary>
        /// Name of the entity of the alarm
        /// </summary>
        [NotMappedColumn]
        public string Entity { get; set; }

        /// <summary>
        /// Type of tht entity
        /// </summary>
        [DisplayName("Tipo de Entidad:")]
        [NotMappedColumn]
        public string EntityType { get; set; }        

        /// <summary>
        /// Periodiciy of the alarm
        /// </summary>
        [DisplayName("Periodicidad:")]
        [NotMappedColumn]
        public string Periodicity { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public System.DateTime? NextAlarm { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Siguiente Alarma:")]
        [NotMappedColumn]
        public string NextAlarmStr { get{return ECOsystem.Utilities.Miscellaneous.GetDateFormat(NextAlarm);}}
    
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool IsEncrypted { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Entidad:")]
        [NotMappedColumn]
        public string EntityStr { 
            get {
                if (IsEncrypted)
                {
                    return ECOsystem.Utilities.TripleDesEncryption.Decrypt(Entity);
                }
                else {
                    return Entity;
                }
            }
       }

        /// <summary>
        /// SpeedLaw
        /// </summary>        
        [NotMappedColumn]
        public bool LawSpeed { get; set; }

        /// <summary>
        /// SpeedCustomer
        /// </summary>        
        [NotMappedColumn]
        public bool CustomerSpeed { get; set; }


        /// <summary>
        /// SpeedCustomer
        /// </summary>        
        [NotMappedColumn]
        public string TypeSpeed { get; set; }


        /// <summary>
        /// SpeedCustomer
        /// </summary>        
        [NotMappedColumn]
        public double? MaxTime { get; set; }

        public int RepeatTimes { get; set; }

        public int IntervalRepeat { get; set; }

        public bool Repeat { get; set; }

        public bool SMS { get; set; }
    }
}