﻿/************************************************************************************************************
*  File    : TransactionsConsolideReportBase.cs
*  Summary : TransactionsConsolide Models
*  Author  : Henry Retana
*  Date    : 1/02/2016
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// TransactionsConsolideReportBase
    /// </summary>
    public class TransactionsConsolideReportBase
    {
        public TransactionsConsolideReportBase()
        {
            Parameters = new ControlFuelsReportsBase();
            Data = new TransactionsConsolideReport();
            List = new List<TransactionsConsolideReport>();
            Menus = new List<AccountMenus>();
            ExportableTranParameters = new ExportableTransactionsFileParameters();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public ControlFuelsReportsBase Parameters { get; set; }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public TransactionsConsolideReport Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<TransactionsConsolideReport> List { get; set; }

        /// <summary>
        /// List of Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        public ExportableTransactionsFileParameters ExportableTranParameters { get; set; }
             
    }

    /// <summary>
    /// ExportableTransactionsFileParameters
    /// </summary>
    public class ExportableTransactionsFileParameters : ModelAncestor
    {
        /// <summary>
        ///  Debe
        /// </summary>
        [DisplayName("Tipo Documento")]
        public string DocumentType { get; set; }

        /// <summary>
        ///  Debe
        /// </summary>
        [DisplayName("Moneda")]
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Document Date
        /// </summary>
        [DisplayName("Tipo Asiento")]
        public String RecordType { get; set; }

        /// <summary>
        ///  Document Class
        /// </summary>
        [DisplayName("Paquete")]
        public string Package { get; set; }

        /// <summary>
        /// OnlyAuthorize
        /// </summary>
        public bool OnlyAuthorize { get; set; }
    }

    /// <summary>
    /// TransactionsConsolideReport 
    /// </summary>
    public class TransactionsConsolideReport : ModelAncestor
    {
        /// <summary>
        ///  Debe
        /// </summary>
        [DisplayName("Debe")]
        public string Debe { get; set; }

        /// <summary>
        ///  Debe
        /// </summary>
        [DisplayName("Haber")]
        public string Haber { get; set; }
               
        /// <summary>
        /// Document Date
        /// </summary>
        [DisplayName("Fecha Documento")]
        public DateTime DocumentDate { get; set; }           

        /// <summary>
        ///  Document Class
        /// </summary>
        [DisplayName("Clase Documento")]
        [StringLength(2, ErrorMessage = "El campo debe ser de 2 caracteres.")]
        public string DocumentClass { get; set; }

        /// <summary>
        ///  Sociedad
        /// </summary>
        [DisplayName("Sociedad")]
        [StringLength(4, ErrorMessage = "El campo debe ser de 4 caracteres.")]
        [MaxLength(4)]
        public string Sociedad { get; set; }

        /// <summary>
        ///  Fecha Contabilizacion
        /// </summary>
        [DisplayName("Fecha Contablilizacion")]
        public DateTime CountDate { get; set; }
            

        /// <summary>
        ///  Moneda
        /// </summary>
        [DisplayName("Moneda")]
        [StringLength(3, ErrorMessage = "El campo debe ser de 3 caracteres.")]
        public string Currency { get; set; }

        /// <summary>
        ///  Referencia
        /// </summary>
        [DisplayName("Referencia")]
        [StringLength(16, ErrorMessage = "El campo debe ser de 16 caracteres.")]
        public string Reference { get; set; }

        /// <summary>
        ///  Texto Cabecera
        /// </summary>
        [DisplayName("Texto Cabecera")]
        [StringLength(25, ErrorMessage = "El campo debe ser de 25 caracteres.")]
        public string TextoCabecera { get; set; }

        /// <summary>
        ///  ClaveContabilizacionDebe
        /// </summary>              
        public string ClaveContabilizacionDebe { get; set; }

        /// <summary>
        ///  ClaveContabilizacionDebe
        /// </summary>              
        public string ClaveContabilizacionHaber { get; set; } 

        /// <summary>
        ///  Cuenta
        /// </summary>
        [DisplayName("Cuenta")]
        [StringLength(10, ErrorMessage = "El campo debe ser de 10 caracteres.")]
        public string Account { get; set; }
        
        /// <summary>
        ///  IND_CME
        /// </summary>
        [DisplayName("IND_CME")]
        public string IND_CME { get; set; }

        /// <summary>
        ///  Importe
        /// </summary>
        [DisplayName("Importe")]        
        public decimal Importe { get; set; }

        /// <summary>
        ///  IndicadorImpuesto
        /// </summary>
        [DisplayName("Indicador de Impuesto")]
        public string IndicadorImpuesto { get; set; }

        /// <summary>
        ///  CalcImpuesto
        /// </summary>
        [DisplayName("Cálculo de Impuesto")]
        public string CalcImpuesto { get; set; }

        /// <summary>
        ///  FechaValor
        /// </summary>
        [DisplayName("Fecha Valor")]
        public DateTime FechaValor { get; set; }

        /// <summary>
        ///  Asignacion
        /// </summary>
        [DisplayName("Asignación")]
        public string Asignacion { get; set; }

        /// <summary>
        ///  Texto
        /// </summary>
        [DisplayName("Texto")]
        [StringLength(50, ErrorMessage = "El campo debe ser de 50 caracteres.")]
        public string Texto { get; set; }

        /// <summary>
        ///  BancoPropio
        /// </summary>
        [DisplayName("Banco Propio")]
        public string BancoPropio { get; set; }
        
        /// <summary>
        ///  ViaPagoDebe
        /// </summary>
        [DisplayName("Via Pago")]
        public string ViaPagoDebe { get; set; }

        /// <summary>
        ///  ViaPagoDebe
        /// </summary>
        [DisplayName("Via Pago")]
        public string ViaPagoHaber { get; set; }

        /// <summary>
        ///  CentroCosto
        /// </summary>
        [DisplayName("Centro de Costo")]
        [StringLength(10, ErrorMessage = "El campo debe ser de 10 caracteres.")]
        public string CentroCosto { get; set; }

        /// <summary>
        ///  CentroBeneficio
        /// </summary>
        [DisplayName("Centro de Beneficio")]
        public string CentroBeneficio { get; set; }

        /// <summary>
        ///  Orden
        /// </summary>
        [DisplayName("Orden")]
        public string Orden { get; set; }

        /// <summary>
        ///  PEP
        /// </summary>
        [DisplayName("PEP")]
        public string PEP { get; set; }

        /// <summary>
        ///  PosicionPresupuesto
        /// </summary>
        [DisplayName("Posición Presupuesto")]
        public string PosicionPresupuesto { get; set; }

        /// <summary>
        ///  CentroGestor
        /// </summary>
        [DisplayName("Centro Gestor")]
        public string CentroGestor { get; set; }

        /// <summary>
        ///  ProgramaPresupuesto
        /// </summary>
        [DisplayName("Programa Presupuesto")]
        public string ProgramaPresupuesto { get; set; }

        /// <summary>
        ///  Fondo
        /// </summary>
        [DisplayName("Fondo")]
        public string Fondo { get; set; }

        /// <summary>
        ///  Centro
        /// </summary>
        [DisplayName("Centro")]
        public string Centro { get; set; }

        /// <summary>
        ///  BloqueaPagoDebe
        /// </summary>
        [DisplayName("Bloquea Pago")]
        public string BloqueaPagoDebe { get; set; }

        /// <summary>
        ///  BloqueaPagoHaber
        /// </summary>
        [DisplayName("Bloquea Pago")]
        public string BloqueaPagoHaber { get; set; }

        /// <summary>
        ///  SAP Prov
        /// </summary>
        [DisplayName("SAP Prov")]
        public string SAPProv { get; set; }

        /// <summary>
        ///  SAP Prov
        /// </summary>
        [DisplayName("Pagador Alternativo")]
        public string PagadorAlternativo { get; set; }

        /// <summary>
        ///  SAP Prov
        /// </summary>
        [DisplayName("Pagador Alternativo")]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// PaymentInstrumentCode
        /// </summary>
        public string PaymentInstrumentCode { get; set; }

        /// <summary>
        /// PaymentInstrumentType
        /// </summary>
        public string PaymentInstrumentType { get; set; }

        /// <summary>
        /// PaymentInstrument
        /// </summary>
        public string PaymentInstrument
        {
            get
            {
                if (PaymentInstrumentType == "Tarjeta" || PaymentInstrumentType == null)
                    return CreditCardNumber;
                else
                    return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(ECOsystem.Utilities.TripleDesEncryption.Decrypt(PaymentInstrumentCode));
            }
        }

        /// <summary>
        ///  Show Menu
        /// </summary>
        [DisplayName("Mostrar Menu")]
        public string MostrarMenu { get; set; }
        
        /// <summary>
        ///  ReferenceNumber
        /// </summary>
        [DisplayName("Número de Factura")]
        public string ReferenceNumber { get; set; }

        /// <summary>
        ///  ServiceStationNumber
        /// </summary>
        [DisplayName("Número de Estación ")]
        public string ServiceStationNumber { get; set; }

        /// <summary>
        ///  ServiceStationNumber
        /// </summary>
        [DisplayName("CECO")]
        public string CECO { get; set; }
    }
}