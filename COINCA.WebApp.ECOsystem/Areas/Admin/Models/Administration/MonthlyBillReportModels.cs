﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Utilities;

namespace  ECOsystem.Areas.Admin.Models
{
    public class MonthlyBillReport
    {
        public string CustomerName { get; set; }

        public string CustomerNameDecrypt { get { return TripleDesEncryption.Decrypt(CustomerName); } }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string RTN { get; set; }

        public string CurrencySymbol { get; set; }

        public string CurrencyName { get; set; }

        public string CurrencyNamePlural { get; set; }

        public decimal Amount { get; set; }

        public string AmountStr { get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(Amount, CurrencySymbol); } }

        public decimal FinalAmount { get; set; }

        public string FinalAmountStr { get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(FinalAmount, CurrencySymbol); } }

        public string Detail { get; set; }

        public int? DetailNumber { get; set; }

        public decimal DetailTotal { get; set; }

        public string DetailTotalStr { get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(DetailTotal, CurrencySymbol); } }

        public string DetailTotalToLetters
        {
            get
            {
                var convert = new NumToLetter();
                return convert.ConvertToLetters(FinalAmount.ToString(), false) + CurrencyNamePlural;
            }
        }

        public decimal DetailAmount { get; set; }

        public string DetailAmountStr { get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(DetailAmount, CurrencySymbol); } }

        public string AccountNumber { get; set; }

        public string AccountNumberStr { get { return string.IsNullOrEmpty(AccountNumber) ? "" : "**-" + ECOsystem.Utilities.Miscellaneous.ConvertCreditCardToInternalFormat(Convert.ToInt64(TripleDesEncryption.Decrypt(AccountNumber))).Split('-')[3]; } }

        public decimal ExchangeRate { get; set; }

        public DateTime BillDate { get; set; }

        public string StatusName { get; set; }

        public string Invoice { get; set; }

        public string BACService { get; set; }

        public string BACName { get; set; }
    }
}