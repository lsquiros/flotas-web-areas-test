﻿/************************************************************************************************************
*  File    : VehiclesByDriverModels.cs
*  Summary : VehiclesByDriver Models
*  Author  : Gerald Solano
*  Date    : 22/07/2015
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// DriversByVehicle Base
    /// </summary>
    public class VehiclesByDriverBase
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of DriverVehicle model
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehiclesByDriverData Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }


    /// <summary>
    /// Vehicles By Driver Model
    /// </summary>
    public class VehiclesByDriver : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Vehicles model
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Placa
        /// </summary>
        public string Plate { get; set; }

        /// <summary>
        /// Vehicle Name
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// Vehicle Name Desencrypt
        /// </summary>
        [DisplayName("Vehiculo")]
        public string VehicleNameStr
        {
            get
            {
                return TripleDesEncryption.Decrypt(this.VehicleName);
            }
        }

        /// <summary>
        /// Plate Desencrypt
        /// </summary>
        [DisplayName("Placa")]
        public string PlateStr {
            get {
                return TripleDesEncryption.Decrypt(this.Plate);
            } 
        }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of DriverVehicle model
        /// </summary>
        public int? VehicleId { get; set; }
    }

    /// <summary>
    /// Vehicles By Driver Data
    /// </summary>
    public class VehiclesByDriverData
    {
        /// <summary>
        /// Vehicles By Driver Data default Constructor
        /// </summary>
        public VehiclesByDriverData()
        {
            AvailableVehiclesList = new List<VehiclesByDriver>();
            ConnectedVehiclesList = new List<VehiclesByDriver>();
        }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehiclesByDriver model
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Available Vehicles List
        /// </summary>
        public IEnumerable<VehiclesByDriver> AvailableVehiclesList { get; set; }

        /// <summary>
        /// Connected Vehicles List
        /// </summary>
        public IEnumerable<VehiclesByDriver> ConnectedVehiclesList { get; set; }
    }

    public class VehicleByDriverCode {
        /// <summary>
        /// Id del Vehículo
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Placa del Vehículo
        /// </summary>
        public string Plate { get; set; }
        
        /// <summary>
        /// Código del Conductor
        /// </summary>
        public string DriverCode { get; set; }
        
        /// <summary>
        /// Conductor Activo
        /// </summary>
        public bool IsActive { get; set; }
        
        /// <summary>
        /// Conductor Eliminado
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}