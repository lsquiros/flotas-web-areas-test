﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceVehicleDetailModels.cs
*  Summary : Preventive Maintenance Vehicle Detail Models
*  Author  : Danilo Hidalgo
*  Date    : 02/23/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Miscellaneous;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
#pragma warning disable 108, 114
    /// <summary>
    /// PreventiveMaintenanceVehicle Base
    /// </summary>
    public class PreventiveMaintenanceVehicleDetailBase
    {
        public PreventiveMaintenanceVehicleDetailBase()
        {
            Data = new PreventiveMaintenanceVehicleDetail();
            List = new List<PreventiveMaintenanceVehicleDetail>();
            PreventiveMaintenanceCatalogList = new List<SelectListItem>();
            Assign = new PreventiveMaintenanceVehicleAssign();
            Record = new PreventiveMaintenanceRecordByVehicleBase();
            Menus = new List<AccountMenus>();
        }

        /// <summary>8
        /// Data property displays the main information of the entity 
        /// </summary>
        public PreventiveMaintenanceVehicleDetail Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceVehicleDetail> List { get; set; }

        /// <summary>
        /// List of PreventiveMaintenanceCatalog
        /// </summary>
        public IEnumerable<SelectListItem> PreventiveMaintenanceCatalogList { get; set; }

        /// <summary>
        /// Data property displays assign information of the entity 
        /// </summary>
        public PreventiveMaintenanceVehicleAssign Assign { get; set; }

        /// <summary>
        /// Data property displays record information of the entity 
        /// </summary>
        public PreventiveMaintenanceRecordByVehicleBase Record { get; set; }

        /// <summary>
        /// Menus 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
        
    }

    /// <summary>
    /// PreventiveMaintenanceVehicleDetail Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceVehicleDetail : ModelAncestor
    {

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceCatalogId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string PlateId { get; set; }

        /// <summary>
        /// Name of current preventive maintenance
        /// </summary>
        [DisplayName("Mantenimiento Preventivo")]
        [GridColumn(Title = "Vehículo", Width = "120px", SortEnabled = true)]
        public string PreventiveMaintenanceDescription { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int VehicleId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public double LastReviewOdometer { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public double NextReviewOdometer { get; set; }

        /// <summary>
        /// NextReviewDateStr
        /// </summary>
        [DisplayName("Kilometraje Próxima Revisión")]
        [GridColumn(Title = "Kilometraje Próxima Revisión", Width = "120px", SortEnabled = true)]
        public string NextReviewOdometerStr { get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Convert.ToDecimal(NextReviewOdometer)); } }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public DateTime LastReviewDate { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public DateTime NextReviewDate { get; set; }

        /// <summary>
        /// NextReviewDateStr
        /// </summary>
        [DisplayName("Fecha Próxima Revisión")]
        [GridColumn(Title = "Fecha Próxima Revisión", Width = "120px", SortEnabled = true)]
        public string NextReviewDateStr { get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(NextReviewDate); } }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool Registred { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public bool AlarmSent { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int InsertUserId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public DateTime InsertDate { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int ModifyUserId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public DateTime ModifyDate { get; set; }

        private byte[] _rowVersion;

        /// <summary>
        /// Row Version to prevent Bad insert/update
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public byte[] RowVersion

        {
            get { return _rowVersion ?? (_rowVersion = new byte[] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 }); }
            set { _rowVersion = value; }
        }

    }

    /// <summary>
    /// PreventiveMaintenanceVehicleDetail Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceVehicleAssign : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int VehicleId { get; set; }

        /// <summary>
        /// List of PreventiveMaintenanceCatalog
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<SelectListItem> PreventiveMaintenanceCatalogList { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        //[Required(ErrorMessage = "{0} es requerido")]
        public int PreventiveMaintenanceCatalogId { get; set; }

        /// <summary>
        /// Date LastReview
        /// </summary>
        [DisplayName("Fecha Última Revisión")]
        [GridColumn(Title = "Fecha Última Revisión", Width = "250px", SortEnabled = true)]
        public DateTime? LastReviewDate { get; set; }

        /// <summary>
        /// Date String LastReview
        /// </summary>
        [DisplayName("Fecha Última Revisión")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string LastReviewDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(LastReviewDate); }
            set { LastReviewDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Odometer LastReview
        /// </summary>
        [DisplayName("Odómetro Última Revisión")]
        [GridColumn(Title = "Odómetro Última Revisión", Width = "250px", SortEnabled = true)]
        [Required(ErrorMessage = "{0} es requerido")]
        public double LastReviewOdometer { get; set; }

        /// <summary>
        /// Odometer String LastReview
        /// </summary>
        [DisplayName("Odómetro Última Revisión")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string LastReviewOdometerStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetNumberFormat(Convert.ToDecimal(LastReviewOdometer)); }
            set { LastReviewOdometer = Convert.ToDouble(value); }
        }

    }

}
