﻿/************************************************************************************************************
*  File    : DashboardModuleModels.cs
*  Summary : Dashboard Module Models
*  Author  : Manuel Azofeifa Hidalgo
*  Date    : 02/Dic/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECOsystem.Models.Account;
using ECOsystem.Models.Miscellaneous;
using System.Collections.Generic;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
#pragma warning disable 108, 114
    /// <summary>
    /// 
    /// </summary>
    public class DashboardModuleBase
    {
        /// <summary>
        /// 
        /// </summary>
        public DashboardModuleBase()
        {
            Data = new DashboardModule();
            List = new List<DashboardModule>();
            Menus = new List<AccountMenus>();
        }
        /// <summary>
        /// Data
        /// </summary>
        public DashboardModule Data { get; set; }

        /// <summary>
        /// IEnumerable List of Modules
        /// </summary>
        public IEnumerable<DashboardModule> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Dashboard Module Models
    /// </summary>
    public class DashboardModule : ModelAncestor
    {
        /// <summary>
        /// Dashboard Module Id
        /// </summary>
        public int DashboardModuleId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }
        
        /// <summary>
        /// Order
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Logged User Id
        /// </summary>
        public int LoggedUserId { get; set; }

        /// <summary>
        /// Row Version
        /// </summary>
        public byte[] RowVersion { get; set; }
    }
}
