﻿/************************************************************************************************************
*  File    : VehiclesModels.cs
*  Summary : Vehicles Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using System.Globalization;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// Vehicles Base
    /// </summary>
    public class VehiclesBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public VehiclesBase()
        {
            Data = new Vehicles() 
            { 
                Menus = new List<AccountMenus>(), 
            };
            List = new List<Vehicles>();
            Menus = new List<AccountMenus>();
            Rules = new List<TransactionRulesModel>();
            Parameters = new AdminReportsBase();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public AdminReportsBase Parameters { get; set; }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Vehicles Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Vehicles> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }

        public IEnumerable<TransactionRulesModel> Rules { get; set; }
        
    }


    /// <summary>
    /// Vehicles Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Vehicles : ModelAncestor
    {
        public Vehicles()
        {
            CodView = "VehMan";
        }

        public Int64 FleetioId { get; set; }

        public int VehicleNumber { get; set; }

        /// <summary>
        /// For get Alarm of Vehicle
        /// </summary>
        [NotMappedColumn]
        public string CodView { get; set; }


        /// <summary>
        /// Propiedad para el manejo de menus
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<AccountMenus> Menus { get; set; }

          /// <summary>
        /// Propiedad para el manejo de BusinessRules para vehículos
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<TransactionRulesModel> Rules { get; set; }

        /// <summary>
        /// Propiedad para cargar los datos del mantenimiento preventivo
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<PreventiveMaintenanceVehicleDetail> MaintenanceList { get; set; }

        /// <summary>
        /// Propiedad para cargar los datos de transacciones
        /// </summary>
        [NotMappedColumn]
        public TransactionsReportBase Transactions { get; set; }

        /// <summary>
        /// Propiedad para cargar los datos de las tarjetas 
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<CreditCard> CreditCards { get; set; }

        /// <summary>
        /// Propiedad para cargar los conductores 
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<DriversUsers> Drivers { get; set; }

        /// <summary>
        /// Propiedad para cargar la bitacora de vehiculos
        /// </summary>
        [NotMappedColumn]
        public VehicleBinnacleBase VehicleBinnacles { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Vehículo", Width = "120px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Fabricante")]
        [GridColumn(Title = "Fabricante", Width = "100px", SortEnabled = true)]
        public string Manufacturer { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Modelo")]
        [GridColumn(Title = "Modelo", Width = "100px", SortEnabled = true)]
        public string VehicleModel { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Tipo")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Tipo", Width = "50px", SortEnabled = true)]
        public string VehicleType { get; set; }

        /// <summary>
        /// Vehicle year
        /// </summary>
        [DisplayName("Año")]
        [NotMappedColumn]
        public int? VehicleYear { get; set; }

        /// <summary>
        /// Plate Id or number for the Vehicle
        /// </summary>
        [DisplayName("Matrícula")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Matrícula", Width = "50px", SortEnabled = true)]
        public string PlateId { get; set; }

        /// <summary>
        /// FK CostCenterId from SubUnit
        /// </summary>
        [DisplayName("Centro de Costo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int CostCenterId { get; set; }

        /// <summary>
        /// SubUnit Name
        /// </summary>
        [DisplayName("Centro de Costo")]
        [GridColumn(Title = "Centro de Costo", Width = "50px", SortEnabled = true)]
        public string CostCenterName { get; set; }

        /// <summary>
        /// Unit Name
        /// </summary>
        [DisplayName("Unidad")]
        [GridColumn(Title = "Unidad", Width = "50px", SortEnabled = true)]
        public string UnitName { get; set; }

        /// <summary>
        /// Vehicle GroupId
        /// </summary>
        [NotMappedColumn]
        public int VehicleGroupId { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        [NotMappedColumn]
        public int? CustomerId { get; set; }

        /// <summary>
        /// FK FuelId from Fuels
        /// </summary>
        [NotMappedColumn]
        public int DefaultFuelId { get; set; }


        /// <summary>
        /// FK User Id from Users, each vehicle is associate with one user
        /// </summary>
        [DisplayName("Conductor Activo")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? UserId { get; set; }

        /// <summary>
        /// UserName for UserId
        /// </summary>
        [DisplayName("Conductor Asignado")]
        [NotMappedColumn]
        public string EncryptedUserName { get; set; }

        /// <summary>
        /// UserName for UserId
        /// </summary>
        [DisplayName("Conductor Asignado")]
        [GridColumn(Title = "Conductor Asignado", Width = "100px", SortEnabled = true)]
        public string DecryptedUserName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedUserName); }
            set { EncryptedUserName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// FK VehicleCategoryId from VehicleCategories each vehicle is associate with one vehicle category
        /// </summary>
        [DisplayName("Clase de Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int VehicleCategoryId { get; set; }

        /// <summary>
        /// Category type/description
        /// </summary>
        [DisplayName("Clase")]
        [GridColumn(Title = "Clase", Width = "50px", SortEnabled = true)]
        public string CategoryType { get; set; }


        /// <summary>
        /// Fuel Name for the vehicle
        /// </summary>
        [DisplayName("Combustible")]
        [NotMappedColumn]
        public string FuelName { get; set; }

        /// <summary>
        /// Is Active?
        /// </summary>
        [DisplayName("Activo")]
        [NotMappedColumn]
        public bool Active { get; set; }


        /// <summary>
        /// Has GPS?
        /// </summary>
        [DisplayName("Tiene GPS")]
        [NotMappedColumn]
        public bool HasGPS { get; set; }

             /// <summary>
        /// Result after insert
        /// </summary>
        [NotMappedColumn]
        public string GPSIcon
        {
            get
            {
                switch (HasGPS)
                {
                    case true:
                        return "<span2 class='text-success mdi mdi-access-point-network' data-toggle='tooltip' data-placement='right' title='Tiene GPS'></span2>";
                    default:
                        return "";
           }
            }
            set { }
        }
        


        /// <summary>
        /// Vehicle Colour
        /// </summary>
        [DisplayName("Color")]
        [NotMappedColumn]
        public string Colour { get; set; }

        /// <summary>
        /// Vehicle chassis
        /// </summary>
        [DisplayName("Chasis")]
        [NotMappedColumn]
        [MaxLength(50, ErrorMessage = "El Chasis puede tener únicamente 50 dígitos.")]
        public string Chassis { get; set; }

        /// <summary>
        /// Last Dallas for the vehicle
        /// </summary>
        [DisplayName("Último Dallas")]
        [NotMappedColumn]
        [MaxLength(50, ErrorMessage = "El último Dallas puede tener únicamente 50 dígitos.")]
        public string LastDallas { get; set; }

        /// <summary>
        /// Freight Temperature for the vehicle
        /// </summary>
        [DisplayName("Temperatura de Carga")]
        [NotMappedColumn]
        public int FreightTemperature { get; set; }

        /// <summary>
        /// First Freight Temperature Threshold for the vehicle
        /// </summary>
        [DisplayName("Primer Umbral")]
        [NotMappedColumn]
        public int FreightTemperatureThreshold1 { get; set; }

        /// <summary>
        /// Second Freight Temperature Threshold for the vehicle
        /// </summary>
        [DisplayName("Segundo Umbral")]
        [NotMappedColumn]
        public int FreightTemperatureThreshold2 { get; set; }

        /// <summary>
        /// Predictive for the vehicle
        /// </summary>
        [DisplayName("Predictivo")]
        [NotMappedColumn]
        public bool Predictive { get; set; }

        /// <summary>
        /// AVL for the vehicle
        /// </summary>
        [DisplayName("AVL")]
        [NotMappedColumn]
        [MaxLength(50, ErrorMessage = "El AVL puede tener únicamente 50 dígitos.")]
        public string AVL { get; set; }

        /// <summary>
        /// Cabin Phone Number
        /// </summary>
        [DisplayName("Teléfono de Cabina")]
        [NotMappedColumn]
        [MaxLength(50, ErrorMessage = "El Teléfono de Cabina puede tener únicamente 50 dígitos.")]
        public string CabinPhone { get; set; }

        /// <summary>
        /// Number of SIM that have the AVL 
        /// </summary>
        [DisplayName("SIM AVL")]
        [NotMappedColumn]
        [MaxLength(10, ErrorMessage = "El número puede tener únicamente 10 dígitos.")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public DateTime DriverLicenseExpiration { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int DailyTransactionLimit { get; set; }

        /// <summary>
        /// Odometer the driver change
        /// </summary>
        [NotMappedColumn]
        public int Odometer { get; set; }

        /// <summary>
        /// Liters
        /// </summary>
        [NotMappedColumn]
        public int Liters { get; set; }

        /// <summary>
        /// Partner Id
        /// </summary>
        [NotMappedColumn]
        public int PartnerId { get; set; }

        /// <summary>
        /// Partner Capacity Unit Id
        /// </summary>
        [NotMappedColumn]
        public int PartnerCapacityUnitId { get; set; }

        /// <summary>
        /// Insurance of Vehicle
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Póliza")]
        public string Insurance { get; set; }

        /// <summary>
        /// Date Expiration of Insurance 
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Fecha Vencimiento")]
        public DateTime? DateExpirationInsurance { get; set; }

        /// <summary>
        /// String Date Expiration of Insurance
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Fecha Vencimiento")]
        public string DateExpirationInsuranceStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(DateExpirationInsurance); }
            set { DateExpirationInsurance = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Coverage Type of Insurance 
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Tipo Cobertura")]
        public string CoverageType { get; set; }

        /// <summary>
        /// Insurance Company
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Nombre de empresa aseguradora")]
        public string NameEnterpriseInsurance { get; set; }

        /// <summary>
        /// Insurance Periodicity
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Periodicidad")]
        public int? PeriodicityId { get; set; }

        /// <summary>
        /// Isurance Cost
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Costo")]
        public decimal? Cost { get; set; }

        /// <summary>
        /// Insurance Periodicity
        /// </summary>
        [NotMappedColumn]
        //[DisplayName("Velocidad Delimitada por Empresa (km/h)")]
        [DisplayName("Velocidad Max, Empresa (km/h)")]
        public int AdministrativeSpeedLimit { get; set; }

        /// <summary>
        /// Intrack Reference
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Vehículo Flotas BAC Credomatic")]
        public int? IntrackReference { get; set; }

        /// <summary>
        /// Imei
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Número IMEI (AVL)")]
        public decimal? Imei { get; set; }

        /// <summary>
        /// DefaultPerformance
        /// </summary>
        [NotMappedColumn]
        public decimal? DefaultPerformance { get; set; }

        [NotMappedColumn]
        [DisplayName("Presupuesto mensual")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal MonthlyBudget { get { return TotalBudget - AdditionalBudget; } }

        [NotMappedColumn]
        [DisplayName("Presupuesto adicional")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal AdditionalBudget { get; set; }

        [NotMappedColumn]
        [DisplayName("Presupuesto total")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal TotalBudget { get; set; }

        [NotMappedColumn]
        [DisplayName("Compras del mes")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal MonthlyPurchases { get {return TotalBudget - Available;} }

        [NotMappedColumn]
        [DisplayName("Disponible")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal Available { get; set; }

        /// <summary>
        /// PullPreviousBudget
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Trasladar el disponible al siguiente mes")]
        public bool PullPreviousBudget { get; set; }

        /// <summary>
        /// ExternalId
        /// <summary>
        [NotMappedColumn]
        [DisplayName("Identificador numérico")]
        public int? ExternalId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Odómetro")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Odometro", Width = "50px", SortEnabled = true)]
        public Double OdometerVehicle { get; set; }

        /// <summary>
        /// HasCooler
        /// </summary>
        [DisplayName("Tiene Refrigeración")]
        [NotMappedColumn]
        public bool HasCooler { get; set; }
        /// <summary>
        /// Temperature Sensor Count
        /// </summary>
        [DisplayName("Sensores de Temperatura")]
        [NotMappedColumn]
        public int TempSensorCount { get; set; }

        /// <summary>
        /// Minimum Temperature 
        /// </summary>
        [DisplayName("Temperatura Mínima")]
        [NotMappedColumn]
        public decimal MinTemperature { get; set; }
        /// <summary>
        /// Maximum Temperature
        /// </summary>
        [DisplayName("Temperatura Máxima")]
        [NotMappedColumn]
        public decimal MaxTemperature { get; set; }

        [NotMappedColumn]
        public string MaxTemperatureStr { get { return this.MaxTemperature.ToString("#.00", CultureInfo.CreateSpecificCulture("es-CR")); } }

        [DisplayName("Depreciación")]
        [NotMappedColumn]
        public decimal Depreciation { get; set; }

        [DisplayName("Costo del Vehículo")]
        [NotMappedColumn]
        public decimal VehiclePrice { get; set; }


    }

    /// <summary>
    /// VehiclesReferences Model
    /// </summary>
    public class VehiclesReferences : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre Vehículo")]
        public string Name { get; set; }

        /// <summary>
        /// Plate Id or number for the Vehicle
        /// </summary>
        [DisplayName("Matrícula")]
        public string PlateId { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// Insurance Periodicity
        /// </summary>
        [NotMappedColumn]
        public int AdministrativeSpeedLimit { get; set; }

        /// <summary>
        /// Vehicle reference in Intrack Database
        /// </summary>
        [DisplayName("Referencia Flotas BAC Credomatic")]
        public int IntrackReference { get; set; }

        /// <summary>
        /// Device reference in Atrack Database
        /// </summary>
        [DisplayName("Referencia Dispositivo")]
        public int DeviceReference { get; set; }

    }

    /// <summary>
    /// VehicleBinnacleBase
    /// </summary>
    public class VehicleBinnacleBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public VehicleBinnacleBase()
        {
            Data = new VehicleBinnacle();
            List = new List<VehicleBinnacle>();
        }

        /// <summary>
        /// Data
        /// </summary>
        public VehicleBinnacle Data { get; set; }

        /// <summary>
        /// List
        /// </summary>
        public IEnumerable<VehicleBinnacle> List { get; set; }
    }

    /// <summary>
    /// VehicleBinnacle
    /// </summary>
    public class VehicleBinnacle
    {
        /// <summary>
        /// VehicleId
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Comment
        /// </summary>
        [DisplayName("Comentario")]
        public string Comment { get; set; }

        /// <summary>
        /// EncryptedUserName
        /// </summary>
        public string EncryptedUserName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>        
        public string DecryptedUserName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedUserName); }
            set { EncryptedUserName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// InsertDate
        /// </summary>        
        public DateTime InsertDate { get; set; }

    }

    public class VehicleParameters : ModelAncestor
    {
        /// <summary>
        /// VehicleId
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Elapse time of a stop until raise a alarm
        /// </summary>
        public double? GPSElapseTime { get; set; }
    }

}