﻿using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace  ECOsystem.Areas.Admin.Models
{
    public class FuelCurrencyModule
    {
        [Display(Name = "Cálculo de distribución de combustible por monto.")]
        public bool ByCurrency { get; set; }
        [Display(Name = "Cálculo de distribución de combustible por Combustible.")]
        public bool ByFuel { get; set; }
        public int CustomerId { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }
}
