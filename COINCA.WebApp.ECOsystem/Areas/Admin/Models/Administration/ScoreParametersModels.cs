﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    public class ScoreParameters
    {
        public int? Id { get; set; }

        public int? SpeedPercentage { get; set; }

        public bool SpeedActive { get; set; }

        public int? GPSPercentage { get; set; }

        public bool GPSActive { get; set; }
        
        public List<GPSScoreParameters> List { get; set; }

        public string XMLData { get; set; }
    }

    public class GPSScoreParameters
    {
        public int Id { get; set; }

        public int From { get; set; }

        public int To { get; set; }

        public int Percentage { get; set; }
    }
}