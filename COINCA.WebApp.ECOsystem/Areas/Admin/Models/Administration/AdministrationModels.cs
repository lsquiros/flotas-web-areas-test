﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    public class AdministrationModel
    {
        public IEnumerable<AccountMenus> Menus { get; set; }

        public int Vehicles { get; set; }

        public int Drivers { get; set; }

        public int CreditCards { get; set; }

        public int VehiclesAVL { get; set; }

        public int DriverActive { get; set; }

        public int CCardsActive { get; set; }

        public int CCardsRequest { get; set; }
    }
}