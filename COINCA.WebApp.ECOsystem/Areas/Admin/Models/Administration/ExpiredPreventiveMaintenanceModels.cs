﻿//using ECO_Control.Models.Control;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// Model Base
    /// </summary>
    public class ExpiredPreventiveMaintenanceBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ExpiredPreventiveMaintenanceBase()
        {
            Data = new ExpiredPreventiveMaintenance();
            Record = new PreventiveMaintenanceRecordByVehicleBase();
            List = new List<ExpiredPreventiveMaintenance>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data Model
        /// </summary>
        public ExpiredPreventiveMaintenance Data { get; set; }

        /// <summary>
        /// Record
        /// </summary>
        public PreventiveMaintenanceRecordByVehicleBase Record { get; set; } 

        /// <summary>
        /// List Model
        /// </summary>
        public IEnumerable<ExpiredPreventiveMaintenance> List { get; set; }

        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Expire Maintenance Model
    /// </summary>
    public class ExpiredPreventiveMaintenance
    {
        /// <summary>
        /// Maintenace Id
        /// </summary>
        public int MaintenaceId { get; set; }

        /// <summary>
        /// Vehicle Id
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Vehicle Name
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// Plate
        /// </summary>
        public string Plate { get; set; }

        /// <summary>
        /// Maintenance Name
        /// </summary>
        public string MaintenanceName { get; set; }
        
        /// <summary>
        /// Expired Date
        /// </summary>
        public DateTime ExpiredDate { get; set; }

        /// <summary>
        /// Expired Date
        /// </summary>
        public string ExpiredDateStr
        {
            get { return ExpiredDate.ToString("dd/MM/yyyy"); }
        }

        /// <summary>
        /// Last Review Odometer
        /// </summary>
        public double LastReviewOdometer { get; set; }

        /// <summary>
        /// Odometer
        /// </summary>
        public double Odometer { get; set; }

        /// <summary>
        /// Expired Odometer
        /// </summary>
        public double ExpiredOdometer { get; set; }

        /// <summary>
        /// Format Last Review Odometer
        /// </summary>
        public string LastReviewOdometerStr
        {
            get { return LastReviewOdometer.ToString("#,##0"); }
        }

        /// <summary>
        /// Odometer
        /// </summary>
        public string OdometerStr
        {
            get { return Odometer.ToString("#,##0"); }
        }

        /// <summary>
        /// Format Expired Odometer
        /// </summary>
        public string ExpiredOdometerStr
        {
            get { return ExpiredOdometer.ToString("#,##0"); }
        }
        
        /// <summary>
        /// Explired Number of Days
        /// </summary>
        public int ExpiredDays { get; set; }

    }
}