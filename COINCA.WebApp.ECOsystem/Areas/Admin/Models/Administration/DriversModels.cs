﻿/************************************************************************************************************
*  File    : DriversModels.cs
*  Summary : Drivers Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// Drivers Base Class for Views
    /// </summary>
    public class DriversBase
    {
        /// <summary>
        /// DriversBase Constructor
        /// </summary>
        public DriversBase()
        {
            Parameters = new AdminReportsBase();
            Data = new Users();
            List = new List<Users>();
            Menus = new List<AccountMenus>();
            Errors = new List<DriversErrors>();          
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public AdminReportsBase Parameters { get; set; }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Users Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Users> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<DriversErrors> Errors { get; set; }


    }

    /// <summary>
    /// Drivers Error Class
    /// </summary>
    public class DriversErrors
    {
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public string Item { get; set; }


    }


    /// <summary>
    /// User Model has all properties of User's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class Drivers : ModelAncestor
    {
        /// <summary>
        /// UserId is a PK of Drivers Table
        /// </summary>
        [NotMappedColumn]
        public int? UserId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Cliente")]
        [NotMappedColumn]
        public int? CustomerId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre", Width = "200px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Contraseña")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string Password { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Confirmar")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string PasswordConfirm { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Código")]
        [GridColumn(Title = "Código", Width = "40px", SortEnabled = true)]
        public string DriverCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Identificación")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Cédula", Width = "50px", SortEnabled = true)]
        public string Identification { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("No. Licencia")]
        [NotMappedColumn]
        public string DriverLicense { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Dallas")]
        [GridColumn(Title = "Dallas", Width = "180px", SortEnabled = true)]
        public string Dallas { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Correo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string Email { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Celular")]
        [GridColumn(Title = "Celular", Width = "80px", SortEnabled = true)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Usuario de Ingreso")]
        [NotMappedColumn]
        public string UserName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Foto")]
        [NotMappedColumn]
        public string Photo { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Administrador")]
        [NotMappedColumn]
        public bool? IsAdministrator { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Cambiar Contraseña")]
        [NotMappedColumn]
        public bool? ChangePassword { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Activo")]
        [NotMappedColumn]
        public bool? Active { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Bloqueado")]
        [NotMappedColumn]
        public bool? IsLockedOut { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Fecha Vencimiento de Licencia")]
        [NotMappedColumn]
        public DateTime? DriverLicenseExpiration { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Fecha Vencimiento de Licencia")]
        [NotMappedColumn]
        public string DriverLicenseExpirationStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(DriverLicenseExpiration); }
            set { DriverLicenseExpiration = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        ///  Daily transaction limit
        /// </summary>
        [DisplayName("Limite de Transacciones")]
        [NotMappedColumn]
        public int?  DailyTransactionLimit { get; set; }

    }


    /// <summary>
    /// Model to display data on 
    /// </summary>
    public class DriverViewModel : ModelAncestor
    {
        public DriverViewModel()
        {
            User = new Users();
            Budget = new DriverBudget();
            Transactions = new TransactionsReportBase();
            CreditCards = new List<CreditCard>();
        }
        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        public Users User { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        public DriverBudget Budget { get; set; }
        /// <summary>
        /// Propiedad para cargar los datos de transacciones
        /// </summary>
        [NotMappedColumn]
        public TransactionsReportBase Transactions { get; set; }
        /// <summary>
        /// Propiedad para cargar los datos de las tarjetas 
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<CreditCard> CreditCards { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        public int? CardTypeRequest { get; set; }
    }

    /// <summary>
    /// Driver Budget information
    /// </summary>
    public class DriverBudget
    {
        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Presupuesto mensual")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal MonthlyBudget { get { return TotalBudget - AdditionalBudget; } }

        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Presupuesto adicional")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal AdditionalBudget { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Presupuesto total")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal TotalBudget { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Compras del mes")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal MonthlyPurchases { get { return TotalBudget - Available; } }

        /// <summary>
        /// 
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Disponible")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal Available { get; set; }
    }

    /// <summary>
    /// Define Class to Users Whastapp
    /// drivers
    /// </summary>
    public class UsersWhatsApp
    {
        public int UserId { get; set; }
        public string CustomerId { get; set; }
        public int Whatsapp { get; set; }
    }
}