﻿/************************************************************************************************************
*  File    : DriversByVehicleModels.cs
*  Summary : DriversByVehicle Models
*  Author  : Gerald Solano
*  Date    : 21/07/2015
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// DriversByVehicle Base
    /// </summary>
    public class DriversByVehicleBase
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of DriverVehicle model
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public DriversByVehicleData Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }


    /// <summary>
    /// Drivers By Vehicle Model
    /// </summary>
    public class DriversByVehicle : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Vehicles model
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Identification
        /// </summary>
        public string Identification { get; set; }

        /// <summary>
        /// Driver Name
        /// </summary>
        public string DriverName { get; set; }

        /// <summary>
        /// Identification Desencrypt
        /// </summary>
        [DisplayName("Identificación")]
        public string IdentificationStr
        {
            get
            {
                return TripleDesEncryption.Decrypt(this.Identification);
            }
        }

        /// <summary>
        /// Driver Name Desencrypt
        /// </summary>
        [DisplayName("Conductor")]
        public string DriverNameStr {
            get {
                return TripleDesEncryption.Decrypt(this.DriverName);
            } 
        }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of DriverVehicle model
        /// </summary>
        public int? VehicleId { get; set; }
    }

    /// <summary>
    /// Drivers By Vehicle Data
    /// </summary>
    public class DriversByVehicleData
    {
        /// <summary>
        /// Drivers By Vehicle Data default Constructor
        /// </summary>
        public DriversByVehicleData()
        {
            AvailableDriversList = new List<DriversByVehicle>();
            ConnectedDriversList = new List<DriversByVehicle>();
        }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of DriversByVehicle model
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Available Drivers List
        /// </summary>
        public IEnumerable<DriversByVehicle> AvailableDriversList { get; set; }

        /// <summary>
        /// Connected Drivers List
        /// </summary>
        public IEnumerable<DriversByVehicle> ConnectedDriversList { get; set; }
    }
}