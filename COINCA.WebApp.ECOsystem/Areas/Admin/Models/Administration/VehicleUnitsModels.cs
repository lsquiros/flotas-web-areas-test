﻿/************************************************************************************************************
*  File    : VehicleUnitsModels.cs
*  Summary : VehicleUnits Models
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// VehicleUnits Base
    /// </summary>
    public class VehicleUnitsBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public VehicleUnitsBase()
        {
            Data = new VehicleUnits();
            List = new List<VehicleUnits>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleUnits Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleUnits> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// VehicleUnits Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class VehicleUnits : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? UnitId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre de Unidad")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre de Unidad", Width = "300px", SortEnabled = true)]
        public string Name { get; set; }
        
        /// <summary>
        /// Cost Center Name Units
        /// </summary>
        [NotMappedColumn]
        public string CostCenterName { get; set; }

        /// <summary>
        /// Cost Center Name Units
        /// </summary>
        [GridColumn(Title = "Cantidad de Vehículos", Width = "300px", SortEnabled = true)]
        public int VehicleCount { get; set; }

    }

    /// <summary>
    /// VehicleUnitsDetail
    /// </summary>
    public class VehicleUnitsDetail
    {
        /// <summary>
        /// UnitId
        /// </summary>
        public int UnitId { get; set; }

        /// <summary>
        /// VehicleName
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// PlateId
        /// </summary>
        public string PlateId { get; set; }

        /// <summary>
        /// Manufacturer
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// VehicleModel
        /// </summary>
        public string VehicleModel { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// CostCenterName
        /// </summary>
        public string CostCenterName { get; set; }

        /// <summary>
        /// DriverName
        /// </summary>
        public string DriverName { get; set; }

        /// <summary>
        /// DriverNameStr
        /// </summary>
        public string DriverNameStr
        {
            get
            {
                return TripleDesEncryption.Decrypt(this.DriverName);
            }
        }

        /// <summary>
        /// DeviceReference
        /// </summary>
        public int? DeviceReference { get; set; }

        /// <summary>
        /// IntrackReference
        /// </summary>
        public int? IntrackReference { get; set; }

        /// <summary>
        /// GPS
        /// </summary>
        public bool GPS
        {
            get
            {
                return ((DeviceReference != null || DeviceReference != 0)
                        && (IntrackReference != null || IntrackReference != 0));
            }
        }
    }
}