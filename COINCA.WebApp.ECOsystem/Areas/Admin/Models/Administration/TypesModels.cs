﻿/************************************************************************************************************
*  File    : TypesModels.cs
*  Summary : Types Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using ECOsystem.Models.Miscellaneous;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    
    /// <summary>
    /// Types Models
    /// </summary>
    public class Types : ModelAncestor
    {
        /// <summary>
        /// Status Id property
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Status property
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Code property
        /// </summary>
        public string Code { get; set; }

    }
}