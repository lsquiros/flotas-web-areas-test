﻿/************************************************************************************************************
*  File    : CreditCardModels.cs
*  Summary : CreditCard Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using ECOsystem.Models.Core;
using System.Globalization;
using System.Configuration;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// CreditCard Base
    /// </summary>
    public class CreditCardBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public CreditCardBase()
        {
            Details = new CreditCardDetails();
            TransactionDetail = new CreditCardTransaction();
            List = new List<CreditCard>();
            Menus = new List<AccountMenus>();
            StatusId = -1;
        }

        /// <summary>
        /// Details property displays the main information of the entity 
        /// </summary>
        public CreditCardDetails Details { get; set; }

        /// <summary>
        /// Details property displays the main information of the entity 
        /// </summary>
        public CreditCardTransaction TransactionDetail { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<CreditCard> List { get; set; }


        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Status of the CreditCard
        /// </summary>
        public int StatusId { get; set; }
    }

    /// <summary>
    /// CreditCard Base
    /// </summary>
    public class CardRequestBase
    {
        /// <summary>
        /// 
        /// </summary>
        public CardRequestBase() {
            CardRequest = new CardRequest();
            List = new List<CardRequest>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Card Request property displays the main information of the entity 
        /// </summary>
        public CardRequest CardRequest { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<CardRequest> List { get; set; }


        public IEnumerable<AccountMenus> Menus { get; set; }
    }

    /// <summary>
    /// Credit Card Details
    /// </summary>
    public class CreditCardDetails
    {
        /// <summary>
        /// CreditCard property displays the main information of the credit by customer
        /// </summary>
        public CreditCard CardInfo { get; set; }

        /// <summary>
        /// CardRequest property displays the main information of the credit by customer
        /// </summary>
        public CardRequest RequestInfo { get; set; }

        /// <summary>
        /// CreditInfo property displays the main information of the credit by customer
        /// </summary>
        public CustomerCreditsAdmin CreditInfo { get; set; }

    }

    /// <summary>
    /// CreditCard Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class CreditCard : ModelAncestor
    {
        public int? PaymentInstrumentType { get; set; }

        private int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? CreditCardId { get; set; }

        /// <summary>
        /// Credit Card NumberId
        /// </summary>
        [NotMappedColumn]
        public int? CreditCardNumberId { get; set; }
        
        /// <summary>
        /// PaymentInstrumentType
        /// </summary>
        [DisplayName("Medio de Pago")]
        public string PaymentInstrumentTypeName { get; set; }

        /// <summary>
        /// PaymentInstrumentCode
        /// </summary>
        [NotMappedColumn]
        public string PaymentInstrumentCode { get; set; }
        
        /// <summary>
        /// PaymentInstrument
        /// </summary>
        [DisplayName("# Medio de Pago")]
        public string PaymentInstrument
        {
            get
            {
                if (PaymentInstrumentTypeName == "Tarjeta" || PaymentInstrumentTypeName == null)
                    return RestrictCreditCardNumber;
                else
                    return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(ECOsystem.Utilities.TripleDesEncryption.Decrypt(PaymentInstrumentCode));
            }
        }
        /// <summary>
        /// Credit Card NumberId
        /// </summary>
        [NotMappedColumn]
        public int? PartnerId { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        [NotMappedColumn]
        public int? CustomerId { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        [NotMappedColumn]
        public string Pin { get; set; }

        /// <summary>
        /// Credi tCard Type
        /// </summary>
        [NotMappedColumn]
        public string CreditCardType { get; set; }
        

        /// <summary>
        /// Customer Name
        /// </summary>
        [NotMappedColumn]
        public string EncryptedCustomerName { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        [NotMappedColumn]
        public string DecryptedCustomerName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedCustomerName); }
            set { EncryptedCustomerName = TripleDesEncryption.Encrypt(value); }
        }

        private string _creditCardNumber;

        /// <summary>
        /// Credit Card Number
        /// </summary>
        [DisplayName("Número Tarjeta de Crédito")]
        [NotMappedColumn]
        public string CreditCardNumber
        {
            get { return _creditCardNumber ?? ""; }
            set { _creditCardNumber = value; }
        }

        /// <summary>
        /// Credit Card Number with restrict display
        /// </summary>
        [DisplayName("Número Tarjeta de Crédito")]
        [NotMappedColumn]
        public string RestrictCreditCardNumber { get { return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(DisplayCreditCardNumber); } }


        /// <summary>
        /// Credit Card Number for display
        /// </summary>
        [DisplayName("Número Tarjeta de Crédito")]
        [NotMappedColumn]
        public string DisplayCreditCardNumber
        {
            get { return TripleDesEncryption.Decrypt(CreditCardNumber); }
            set { CreditCardNumber = TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// Credit Card Expiration Year
        /// </summary>
        [DisplayName("Año de Expiración")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? ExpirationYear { get; set; }

        /// <summary>
        /// Credit Card ExpirationM onth
        /// </summary>
        [DisplayName("Mes de Expiración")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? ExpirationMonth { get; set; }

        /// <summary>
        /// Month Name
        /// </summary>
        [NotMappedColumn]
        public string ExpirationMonthName
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetMonthName(ExpirationMonth); }
        }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [NotMappedColumn]
        public decimal? CreditLimit { get; set; }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string CreditLimitStr
        {
            get { return CreditLimit == null ? "" : ((decimal)CreditLimit).ToString("N", CultureInfo.CurrentCulture); }
        }

        /// <summary>
        /// Credit Limit
        /// </summary>
        [DisplayName("Monto Límite")]
        [NotMappedColumn]
        public string CreditLimitFormatted
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditLimit, CurrencySymbol); }
        }

        /// <summary>
        /// Credit Available
        /// </summary>
        [NotMappedColumn]
        public decimal? CreditAvailable { get; set; }

        /// <summary>
        /// Credit Available as String
        /// </summary>
        [NotMappedColumn]
        public string CreditAvailableFormatted
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditAvailable, CurrencySymbol); }
        }

        /// <summary>
        /// Liters Available
        /// </summary>
        public decimal AvailableLiters { get; set; }

        public decimal? AvailableLitersFormat
        {
            get { return Math.Round(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(litersCapacityUnitId, Session.GetCustomerCapacityUnitId(), AvailableLiters), 3); }
            set { AvailableLiters = Convert.ToDecimal(ECOsystem.Utilities.Miscellaneous.CapacityUnitConversion(Session.GetCustomerCapacityUnitId(), litersCapacityUnitId, value)); }
        }

        /// <summary>
        /// Credit Extra
        /// </summary>
        [NotMappedColumn]
        public decimal? CreditExtra { get; set; }


        /// <summary>
        /// Credit Available as String
        /// </summary>
        [NotMappedColumn]
        public string CreditExtraFormatted
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(CreditExtra, CurrencySymbol); }
        }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }


        /// <summary>
        /// Expiration Date 
        /// </summary>
        [NotMappedColumn]
        public string ExpirationDate
        {
            get {
                string expiration = ExpirationMonthName + "/" + ExpirationYear;
                if (expiration == "/0")
                {
                    expiration = "N/A";
                }
                //return ExpirationMonthName + "/" + ExpirationYear;
                return expiration;
            }
        }

        /// <summary>
        /// Status Id
        /// </summary>
        [DisplayName("Estado")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? StatusId { get; set; }


        /// <summary>
        /// Status Name
        /// </summary>
        [NotMappedColumn]
        public string StatusName { get; set; }

        /// <summary>
        /// Encrypted Driver Name
        /// </summary>
        [NotMappedColumn]
        public string EncryptedDriverName { get; set; }


        /// <summary>
        /// Driver Name
        /// </summary>
        [NotMappedColumn]
        public string DecryptedDriverName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedDriverName); }
            set { EncryptedDriverName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Vehicle Plate Description
        /// </summary>
        [NotMappedColumn]
        public string VehiclePlate { get; set; }

        /// <summary>
        /// Plate Id for Validate Transactions
        /// </summary>
        [NotMappedColumn]
        public string TransactionPlate { get; set; }

        /// <summary>
        /// Status Name
        /// </summary>
        [DisplayName("Titular")]
        [NotMappedColumn]
        public string CreditCardHolder
        {
            get { return IssueForId == 100 ? DecryptedDriverName : IssueForId == 101 ? VehiclePlate : ""; }
            set
            {
                switch (IssueForId)
                {
                    case 100:
                        DecryptedDriverName = value;
                        break;
                    case 101:
                        VehiclePlate = value;
                        break;
                }
            }
        }

        /// <summary>
        /// User Id
        /// </summary>
        [DisplayName("Conductor")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? UserId { get; set; }

        /// <summary>
        /// User Id
        /// </summary>
        [DisplayName("Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? VehicleId { get; set; }


        /// <summary>
        /// Credit card Issue For Id 
        /// </summary>
        [NotMappedColumn]
        public int? IssueForId { get; set; }

        /// <summary>
        /// Card RequestId 
        /// </summary>
        [NotMappedColumn]
        public int? CardRequestId { get; set; }

        /// <summary>
        /// Estimated Delivery
        /// </summary>
        [DisplayName("Fecha Entrega")]
        [NotMappedColumn]
        public DateTime? EstimatedDelivery { get; set; }

        /// <summary>
        /// Estimated Delivery
        /// </summary>
        [DisplayName("Fecha Entrega")]
        [NotMappedColumn]
        public string EstimatedDeliveryStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(EstimatedDelivery); }
            set { EstimatedDelivery = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// BAC Afiliado
        /// </summary>
        [DisplayName("Nombre de la Unidad")]
        public string UnitName { get; set; }

        public string DriverIdentification { get; set; }

    }

    /// <summary>
    /// CreditCard Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class CreditCardForApproval : ModelAncestor
    {

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? CreditCardId { get; set; }

        /// <summary>
        /// Credit Card Number
        /// </summary>
        [DisplayName("Número Tarjeta de Crédito")]
        [NotMappedColumn]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Número Tarjeta de Crédito")]
        [GridColumn(Title = "Número Tarjeta de Crédito", Width = "120px")]
        public string DisplayCreditCardNumber { get { return ECOsystem.Utilities.Miscellaneous.GetDisplayCreditCardMask(CreditCardNumber); } }

        /// <summary>
        /// Credit Card Expiration Year
        /// </summary>
        [DisplayName("Año de Expiración")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int? ExpirationYear { get; set; }

        /// <summary>
        /// Credit Card Expiration Month
        /// </summary>
        [NotMappedColumn]
        public int? ExpirationMonth { get; set; }

        /// <summary>
        /// Expiration Date 
        /// </summary>
        [GridColumn(Title = "Fecha Expiración", Width = "100px", SortEnabled = true)]
        public string ExpirationDate
        {
            get { return ExpirationMonth + "/" + ExpirationYear; }
        }

        /// <summary>
        /// Status Id
        /// </summary>
        [NotMappedColumn]
        public int? StatusId { get; set; }

        /// <summary>
        /// Status Name
        /// </summary>
        [NotMappedColumn]
        public string StatusName { get; set; }

        /// <summary>
        /// User Id
        /// </summary>
        [NotMappedColumn]
        public int UserId { get; set; }

        /// <summary>
        /// Partner Id
        /// </summary>
        [NotMappedColumn]
        public int PartnerId { get; set; }

    }

    /// <summary>
    /// CreditCardNumbers Model
    /// </summary>
    public class CreditCardNumbers
    {

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        public int? CreditCardNumberId { get; set; }

        private string _creditCardNumber;

        /// <summary>
        /// Credit Card Number
        /// </summary>
        public string CreditCardNumber
        {
            get { return _creditCardNumber ?? ""; }
            set { _creditCardNumber = value; }
        }


        /// <summary>
        /// Credit Card Number for display
        /// </summary>
        public string DisplayCreditCardNumber
        {
            get { return TripleDesEncryption.Decrypt(CreditCardNumber); }
            set { CreditCardNumber = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Is Used
        /// </summary>
        public bool IsUsed { get; set; }

        /// <summary>
        /// Is Reserved
        /// </summary>
        public bool IsReserved { get; set; }

        /// <summary>
        /// Credit Card Type
        /// </summary>
        public string CreditCardType { get; set; }

    }

    /// <summary>
    /// CreditCard Transaction Model
    /// </summary>
    public class CreditCardTransaction : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        public int? CCTransactionVPOSId { get; set; }

        /// <summary>
        /// Credit Card Id reference
        /// </summary>
        public int CreditCardId { get; set; }

        /// <summary>
        /// Partner Id reference
        /// </summary>
        public int? PartnerId { get; set; }

        /// <summary>
        /// Customer Id reference
        /// </summary>
        public int? CustomerId { get; set; }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [DisplayName("Cliente")]
        public string Customer { get; set; }

        /// <summary>
        /// TransactionType
        /// </summary>
        public string TransactionType { get; set; }

        /// <summary>
        /// TerminalId
        /// </summary>
        [Required(ErrorMessage="Terminal es requerido.")]
        public string TerminalId { get; set; }

        /// <summary>
        /// Invoice
        /// </summary>
        [DisplayName("Factura")]
        public string Invoice { get; set; }

        /// <summary>
        /// EntryMode
        /// </summary>
        public string EntryMode { get; set; }

        /// <summary>
        /// Credit Card Number
        /// </summary>
        [DisplayName("Número Tarjeta de Crédito")]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Credit Card Expiration Year
        /// </summary>
        [DisplayName("Fecha de Expiración")]
        public string ExpirationDate { get; set; }

        /// <summary>
        /// TotalAmount
        /// </summary>
        [DisplayName("Monto")]
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// SystemTraceNumber
        /// </summary>
        [Required]
        [DisplayName("Odómetro")]
        public decimal Odometer { get; set; }

        /// <summary>
        /// SystemTraceNumber
        /// </summary>
        [Required]
        [DisplayName("Litros de Combustible")]
        public decimal Liters { get; set; }

        /// <summary>
        /// SystemTraceNumber
        /// </summary>
        [Required]
        [DisplayName("Placa del vehículo")]
        public string Plate { get; set; }

        /// <summary>
        /// AuthorizationNumber
        /// </summary>
        [DisplayName("Autorización")]
        public string AuthorizationNumber { get; set; }

        /// <summary>
        /// AuthorizationNumber
        /// </summary>
        [DisplayName("Error")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// ReferenceNumber
        /// </summary>
        [DisplayName("Referencia")]
        public string ReferenceNumber { get; set; }

        /// <summary>
        /// SystemTraceNumber
        /// </summary>
        [DisplayName("Número del Sistema")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string SystemTraceNumber { get; set; }

        /// <summary>
        /// BAC Afiliado
        /// </summary>
        [DisplayName("BAC Afiliado")]
        public string BACAfiliado { get; set; }

        /// <summary>
        /// BAC Afiliado
        /// </summary>
        [DisplayName("Estación de Servicio")]
        public string ServiceStationName { get; set; }

        /// <summary>
        /// BAC Afiliado
        /// </summary>
        [DisplayName("Código de Conductor")]
        public string DriverCode { get; set; }

        /// <summary>
        /// BAC Afiliado
        /// </summary>
        [DisplayName("Nombre de la Unidad")]
        public string UnitName { get; set; }

        /// <summary>
        /// List of Transactions Sales for this Transaction
        /// </summary>
        public List<TransactionSale> TransactionSaleList { get; set; }

    }

    /// <summary>
    /// Transaction Sale Model
    /// </summary>
    public class TransactionSale 
    {
        /// <summary>
        /// Credit Card VPOS Id Transaction
        /// </summary>
        public int CCTransactionVPOSId { get; set; }

        /// <summary>
        /// System Trace Number 
        /// </summary>
        public string TraceNumber { get; set; }

        /// <summary>
        /// Date trasaction
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Fuel amount Sale
        /// </summary>
        public decimal FuelAmount { get; set; }

        /// <summary>
        /// Format Fuel Amount
        /// </summary>
        public string FuelAmountStr {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(FuelAmount, ""); } 
        }
    }

    /// <summary>
    /// BaseMasterCreditCardInfo
    /// </summary>
    public class BaseMasterCreditCardInfo
    {
        /// <summary>
        /// Credit Card Number
        /// </summary>
        public string EncryptedCreditCardNumber { get; set; }

        /// <summary>
        /// property // Credomatic Format
        /// </summary>
        public long DecryptedCreditCardNumber { get { return long.Parse(TripleDesEncryption.Decrypt(EncryptedCreditCardNumber).Replace("-","")); } }

        /// <summary>
        /// Credit Card Expiration Year
        /// </summary>
        public int? ExpirationYear { get; set; }

        /// <summary>
        /// Credit Card Expiration Month
        /// </summary>
        public int? ExpirationMonth { get; set; }

        /// <summary>
        /// Expiration Date // Credomatic Format
        /// </summary>
        public int ExpirationDate
        {
            get { return int.Parse(((ExpirationYear??2000)-2000).ToString("00")+ "" + (ExpirationMonth??0).ToString("00")); }
        }

    }

    /// <summary>
    /// CreditCard Base
    /// </summary>
    public class CardFileParamsBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public CardFileParamsBase()
        {
            List = new List<CardFileParams>();
        }

        /// <summary>
        /// List of credit card parameters
        /// </summary>
        public IEnumerable<CardFileParams> List { get; set; }
    }

    /// <summary>
    /// Credit Card Parameters to generate file
    /// </summary>
    public class CardFileParams
    {
        /// <summary>
        /// Card File Param Id
        /// </summary>
        public int CardFileParamId { get; set; }

        /// <summary>
        /// Card File Parameter Name
        /// </summary>
        public string CardFileParamName { get; set; }

        /// <summary>
        /// `PArameter Field lenght
        /// </summary>
        public int CardFileParamLenght { get; set; }

        /// <summary>
        /// Char to separate fields
        /// </summary>
        public string Separator { get; set; }

        /// <summary>
        /// Char to fill empty spaces
        /// </summary>
        public string FillChar { get; set; }
    }
}