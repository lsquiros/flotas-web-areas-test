﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using ECOsystem.Utilities;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    public class OdometerChanged
    {  

         /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public int Vehiculo  { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public string Placa { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public string FechaTransaccion { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public string HoraTransaccion { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public string FechaCambio { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public string HoraCambio { get; set; }


        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public double OdometroOriginal { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public double OdometroFinal { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public string Usuario 
        {
            get { return TripleDesEncryption.Decrypt(UsuarioEncripted); }
            set { Usuario = TripleDesEncryption.Encrypt(value); }
        }


        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public string UsuarioEncripted { get; set; }
        
         /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public string Encabezado { get; set; }
 
    } 
}

