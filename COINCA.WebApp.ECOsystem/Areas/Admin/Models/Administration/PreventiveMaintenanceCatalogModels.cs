﻿using System;
/************************************************************************************************************
*  File    : PreventiveMaintenanceCatalogModels.cs
*  Summary : Preventive Maintenance Catalog Models
*  Author  : Cristian Martínez
*  Date    : 01/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// Operators Base Class for Views
    /// </summary>
    public class PreventiveMaintenanceCatalogBase
    {
        /// <summary>
        /// PreventiveMaintenanceBase Constructor
        /// </summary>
        public PreventiveMaintenanceCatalogBase()
        {
            Data = new PreventiveMaintenanceCatalog();
            List = new List<PreventiveMaintenanceCatalog>();
            CostList = new List<PreventiveMaintenanceCost>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public PreventiveMaintenanceCatalog Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceCatalog> List { get; set; }

        /// <summary>
        /// List of entities to use it for cost grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceCost> CostList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Filter of the Maintenance
        /// </summary>
        [DisplayName("Tipo de Mantenimiento")]
        public int FilterType { get; set; }

        

    }

    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceCatalog : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PreventiveMaintenanceCatalogId { get; set; }

        /// <summary>
        /// Kilometers Frequency
        /// </summary>
        [DisplayName("Frecuencia (Km)")]
        [DisplayFormat(DataFormatString = "{0:#,#}")]
        public double? FrequencyKm { get; set; }

        /// <summary>
        /// Kilometers Frequency with format
        /// </summary>
        [DisplayName("Frecuencia (Km)")]
        [NotMappedColumn]
        [GridColumn(Title = "Frecuencia (Km)", Width = "250px", SortEnabled = true)]
        public string FrequencyKmStr
        {            
            get { return (FrequencyKm != null) ? ECOsystem.Utilities.Miscellaneous.GetNumberFormatCRNoDecimals((decimal)FrequencyKm) : ECOsystem.Utilities.Miscellaneous.GetNumberFormat(0); }
        }

        /// <summary>
        /// Alert Before of Km
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Alertar Antes (Km)")]
        [DisplayFormat(DataFormatString = "{0:#,#}")]
        public double? AlertBeforeKm { get; set; }

        /// <summary>
        /// Month Frequency
        /// </summary>
        [DisplayName("Frecuencia (Meses)")]
        [GridColumn(Title = "Frecuencia (Meses)", Width = "250px", SortEnabled = true)]
        public double? FrequencyMonth { get; set; }

        /// <summary>
        /// Alert Before of Month
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Alertar Antes (Meses)")]
        public int? AlertBeforeMonth { get; set; }

        /// <summary>
        /// Start Date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start Date Str
        /// </summary>
        public string StartDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(StartDate); }
            set { StartDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// End Date
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// End Date Str
        /// </summary>
        public string EndDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(EndDate); }
            set { EndDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Date Frequency
        /// </summary>
        [DisplayName("Frecuencia (Fecha)")]
        public DateTime? FrequencyDate { get; set; }

        /// <summary>
        /// Date of Preventive Maintenance entity
        /// </summary>
        [DisplayName("Frecuencia (Fecha)")]
        [GridColumn(Title = "Frecuencia (Fecha)", Width = "250px", SortEnabled = true)]
        //[RequiredIf("FrequencyMonth === null && FrequencyKm === null", ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public string FrequencyDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(FrequencyDate); }
            set { FrequencyDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Currency Symbol 
        /// </summary>
        [NotMappedColumn]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Alert Before of Date
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Alertar Antes (Fecha)")]
        public int? AlertBeforeDate { get; set; }

        /// <summary>
        /// Has Max Range
        /// </summary>
        public bool HasMaxRange { get; set; }

        /// <summary>
        /// Max Range KM
        /// </summary>
        public double? MaxRangeKM { get; set; }

        /// <summary>
        /// Max Range Date
        /// </summary>
        public DateTime? MaxRangeDate { get; set; }

        /// <summary>
        /// Max Range Date Str
        /// </summary>
        public string MaxRangeDateStr
        {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(MaxRangeDate); }
            set { MaxRangeDate = ECOsystem.Utilities.Miscellaneous.SetDate(value); }
        }

        /// <summary>
        /// Description of Preventive Maintenance entity
        /// </summary>
        [DisplayName("Mantenimiento")]
        [GridColumn(Title = "Nombre", Width = "400px", SortEnabled = true)]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Description { get; set; }

        ///// <summary>
        ///// Date Maintenance Preventive 
        ///// </summary>
        //[NotMappedColumn]
        //[DisplayName("Fecha de Mantenimiento")]
        //public DateTime? DateMaintenancePreventive { get; set; }

        /// <summary>
        /// Description of Preventive Maintenance entity
        /// </summary>
        [DisplayName("Costo")]
        public decimal Cost { get; set; }

        /// <summary>
        /// Cost with format
        /// </summary>
        [DisplayName("Costo")]
        [GridColumn(Title = "Costo", Width = "100px", SortEnabled = true)]
        public string CostStr {
            get { return ECOsystem.Utilities.Miscellaneous.GetCurrencyFormat(Cost, CurrencySymbol); }
        }


        /// <summary>
        /// Filter of the Maintenance
        /// </summary>
        [DisplayName("Tipo de Mantenimiento")]
        public int FilterType { get; set; }


        
        /// <summary>
        /// Filter of the Maintenance
        /// </summary>
        [DisplayName("Tipo de Mantenimiento")]
        public string FilterName { get; set; }
        

        [NotMappedColumn]
        public IEnumerable<PreventiveMaintenanceCost> CostList { get; set; }


        public bool DontAllowDelete { get; set; }

    }



}
