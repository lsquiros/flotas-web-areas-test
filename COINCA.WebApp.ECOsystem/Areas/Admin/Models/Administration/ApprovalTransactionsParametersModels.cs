﻿using System.Collections.Generic;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    public class ApprovalTransactionsParametersBase
    {
        public List<ApprovalTransactionsParameters> List { get; set; }
        public int MaxId { get; set; }
    }

    public class ApprovalTransactionsParameters
    {
        public int Id { get; set; }
        public string Value { get; set; }        
    }

    public class ApprovalTransactionsParametersRetrieve
    {
        public string XMLData { get; set; }
        public int MaxId { get; set; }
    }
}