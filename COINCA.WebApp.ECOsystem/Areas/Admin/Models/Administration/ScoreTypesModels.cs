﻿/************************************************************************************************************
*  File    : Score Types Models.cs
*  Summary : Score Types Models
*  Author  : Napoleón Alvarado
*  Date    : 01/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// ScoreTypes Base
    /// </summary>
    public class ScoreTypesBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public ScoreTypesBase()
        {
            Data = new ScoreTypes();
            List = new List<ScoreTypes>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public ScoreTypes Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<ScoreTypes> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// ScoreTypes Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class ScoreTypes : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? ScoreTypeId { get; set; }

        /// <summary>
        /// Code of current entity
        /// </summary>
        [DisplayName("Código")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Código", Width = "50px", SortEnabled = true)]
        public string Code { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre del Tipo de Calificación")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre del Tipo de Calificación", Width = "150px", SortEnabled = true)]
        public string Name { get; set; }
        
    }
}