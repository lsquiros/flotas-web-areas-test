﻿/************************************************************************************************************
*  File    : VehiclesByGroupModels.cs
*  Summary : VehiclesByGroup Models
*  Author  : Berman Romero
*  Date    : 09/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// VehiclesByGroup Base
    /// </summary>
    public class VehiclesByGroupBase
    {

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehiclesByGroupData Data { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        [DisplayName("Grupo de Vehículos")]
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// VehiclesByGroup Model
    /// </summary>
    public class VehiclesByGroup : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Vehicles model
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Name for the Vehicle
        /// </summary>
        [DisplayName("Vehículo")]
        public string VehicleName { get; set; }

        /// <summary>
        /// Plate Id or number for the Vehicle
        /// </summary>
        [DisplayName("Matrícula")]
        public string PlateId { get; set; }

        /// <summary>
        /// Vehicle Type
        /// </summary>
        [DisplayName("Tipo")]
        public string VehicleType { get; set; }


        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        public int? VehicleGroupId { get; set; }


    }

    /// <summary>
    /// Vehicles By Group Data
    /// </summary>
    public class VehiclesByGroupData
    {
        /// <summary>
        /// Vehicles By Group Data default Constructor
        /// </summary>
        public VehiclesByGroupData()
        {
            AvailableVehiclesList = new List<VehiclesByGroup>();
            ConnectedVehiclesList = new List<VehiclesByGroup>();
        }
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of VehicleGroup model
        /// </summary>
        public int? VehicleGroupId { get; set; }

        /// <summary>
        /// Available Vehicles List
        /// </summary>
        public IEnumerable<VehiclesByGroup> AvailableVehiclesList { get; set; }

        /// <summary>
        /// Connected Vehicles List
        /// </summary>
        public IEnumerable<VehiclesByGroup> ConnectedVehiclesList { get; set; }
    }
}
