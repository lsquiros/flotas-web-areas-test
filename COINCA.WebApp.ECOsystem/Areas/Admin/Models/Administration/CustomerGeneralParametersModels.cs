﻿using ECOsystem.Models.Account;
using ECOsystem.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    public class CustomerGeneralParametersBase
    {
        public CustomerGeneralParametersBase()
        {
            Data = new CustomerGeneralParameters();
            List = new List<CustomerGeneralParameters>();
            Menus = new List<AccountMenus>();
        }

        public CustomerGeneralParameters Data { get; set; }

        public List<CustomerGeneralParameters> List { get; set; }

        public List<AccountMenus> Menus { get; set; }
    }

    public class CustomerGeneralParameters
    {
        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public TimeSpan Starthour { get { return !string.IsNullOrEmpty(StartTime) ? new TimeSpan(int.Parse(StartTime), 0, 0) : new TimeSpan(); } }

        public TimeSpan Endhour { get { return !string.IsNullOrEmpty(StartTime) ? new TimeSpan(int.Parse(EndTime), 0, 0) : new TimeSpan(); } }

        public string Start_Time { get { return DateTime.Today.Add(Starthour).ToString("hh:mm tt"); } }

        public string End_Time { get { return DateTime.Today.Add(Endhour).ToString("hh:mm tt"); } }

        public string Email { get; set; }

        public int? AlarmId { get; set; }

        public bool AllowInProcessCard { get; set; }

        public bool AllowApprovalTransactiosImport { get; set; }

        public bool CostCenterByDriver { get; set; }

        public bool TypeOdometer { get; set; }

        public int TypeBudgetDistribution { get; set; }

        public int AlarmTemperatureDelay { get; set; }

        public string DefaultEmail { get; set; }

        public int MinimumStopTime { get; set; }

        public bool HasInternalServiceStation { get; set; }

        public bool NotApproveTransactions { get; set; }

        //NO eliminar, eventualmente se utilizara para un mantenimiento de alarmas.
        //public bool AlarmNonTransmissionGPS { get; set; }

        public ECOsystem.Models.Core.AlarmDetail AlarmDetailModel { get; set; }
    
	    public int RadioDistance { get; set; } 

        public bool ActiveRadioDistance { get; set;}

        public int VehicleStopLapse { get; set; }

        public int TransmissionInterval { get; set; }

        public int LocationInterval { get; set; }

    }
}
