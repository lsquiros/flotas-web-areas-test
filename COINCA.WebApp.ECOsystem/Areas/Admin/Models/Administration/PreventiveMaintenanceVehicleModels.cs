﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceVehicleModels.cs
*  Summary : PreventiveMaintenanceVehicle Models
*  Author  : Danilo Hidalgo
*  Date    : 02/20/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities;
using System.Web.Mvc;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// PreventiveMaintenanceVehicle Base
    /// </summary>
    public class PreventiveMaintenanceVehicleBase
    {
        /// <summary>
        /// 
        /// 
        /// </summary>
        public PreventiveMaintenanceVehicleBase()
        {
            Data = new PreventiveMaintenanceVehicle();
            List = new List<PreventiveMaintenanceVehicle>();
            Menus = new List<AccountMenus>();
        }
        
        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public PreventiveMaintenanceVehicle Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceVehicle> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }
    
    /// <summary>
    /// RealTime Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceVehicle : ModelAncestor
    {
        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int IntrackReference { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public string CountryCode { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int UserTimeZone { get { return 0.ToTimeOffSet(); } set { 0.ToTimeOffSet(); } }

        /// <summary>
        /// property
        /// </summary>
        [NotMappedColumn]
        public int CustomerId { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Vehículo", Width = "120px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// Plate Id or number for the Vehicle
        /// </summary>
        [DisplayName("Matrícula")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Matrícula", Width = "50px", SortEnabled = true)]
        public string PlateId { get; set; }

        /// <summary>
        /// FK CostCenterId from SubUnit
        /// </summary>
        [DisplayName("Centro de Costo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int CostCenterId { get; set; }
            
        /// <summary>
        /// SubUnit Name
        /// </summary>
        [DisplayName("Centro de Costo")]
        [GridColumn(Title = "Centro de Costo", Width = "50px", SortEnabled = true)]
        public string CostCenterName { get; set; }

        /// <summary>
        /// FK User Id from Users, each vehicle is associate with one user
        /// </summary>
        [DisplayName("Conductor")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int UserId { get; set; }

        /// <summary>
        /// UserName for UserId
        /// </summary>
        [DisplayName("Conductor Asignado")]
        [GridColumn(Title = "Conductor Asignado", Width = "100px", SortEnabled = true)]
        public string UserNameDecrypt { get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(UserName); } }

        [NotMappedColumn]
        public string UserName { get; set; }

        /// <summary>
        /// FK VehicleCategoryId from VehicleCategories each vehicle is associate with one vehicle category
        /// </summary>
        [DisplayName("Clase de Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int VehicleCategoryId { get; set; }

        /// <summary>
        /// Category type/description
        /// </summary>
        [DisplayName("Clase")]
        [GridColumn(Title = "Clase", Width = "50px", SortEnabled = true)]
        public string CategoryType { get; set; }

        /// <summary>
        /// Fuel Name for the vehicle
        /// </summary>
        [DisplayName("Combustible")]
        [NotMappedColumn]
        public string FuelName { get; set; }

        /// <summary>
        /// Is Active?
        /// </summary>
        [DisplayName("Activo")]
        [NotMappedColumn]
        public bool Active { get; set; }

        /// <summary>
        /// Vehicle Colour
        /// </summary>
        [DisplayName("Color")]
        [NotMappedColumn]
        public string Colour { get; set; }

        /// <summary>
        /// Vehicle year
        /// </summary>
        [DisplayName("Año")]
        [NotMappedColumn]
        public int? Year { get; set; }

        /// <summary>
        /// Vehicle chassis
        /// </summary>
        [DisplayName("Chasis")]
        [NotMappedColumn]
        public string Chassis { get; set; }
    }
}
