﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceRecordByVehicleModels.cs
*  Summary : Preventive Maintenance Record By Vehicle Models
*  Author  : Danilo Hidalgo
*  Date    : 01/07/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities.Helpers;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities;
using ECOsystem.Models.Core;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// Operators Base Class for Views
    /// </summary>
    public class PreventiveMaintenanceRecordByVehicleBase
    {
        /// <summary>
        /// PreventiveMaintenanceRecordByVehicleBase Constructor 
        /// </summary>
        public PreventiveMaintenanceRecordByVehicleBase() {
            Data = new PreventiveMaintenanceRecordByVehicle();
            List = new List<PreventiveMaintenanceRecordByVehicle>();
            Alarm = new AlarmsModels() {
                EntityTypeId = 400,
                AlarmTriggerId = 503,
                TitlePopUp = "Alarma de Mantenimiento Preventivo"
            };
            Detail = new PreventiveMaintenanceRecordByVehicleCostDetail();
            Catalog = new PreventiveMaintenanceCatalog();
        }



        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public PreventiveMaintenanceRecordByVehicle Data { get; set; }

        /// <summary>
        /// Data property display the information of the catalog
        /// </summary>
        public PreventiveMaintenanceCatalog Catalog { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<PreventiveMaintenanceRecordByVehicle> List { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        public PreventiveMaintenanceRecordByVehicleCostDetail Detail { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Preventive Maintenance Catalog model
        /// </summary>
        [DisplayName("Tipo Mantenimiento Preventivo")]
        public int? PreventiveMaintenanceCatalogId { get; set; }

        /// <summary>
        /// Alarms of Maintenance Preventive
        /// </summary>
        public AlarmsModels Alarm{get;set;}
    }

    public class PreventiveMaintenanceRecordByVehicleCostDetail {
        public List<PreventiveMaintenanceRecordByVehicleCost> Cost { get; set; }
        public PreventiveMaintenanceRecordByVehicleCostHeader Header { get; set; }
    }

    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance By Vehicle's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceRecordByVehicle : ModelAncestor{

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public PreventiveMaintenanceRecordByVehicle Data { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? PreventiveMaintenanceId { get; set; }

        /// <summary>
        /// Plate of Vehicle
        /// </summary>
        [GridColumn(Title = "Matrícula", Width = "150px")]
        public string PlateId { get; set; }

        /// <summary>
        /// Name of Vehicle
        /// </summary>
        [GridColumn(Title = "Vehículo", Width = "150px")]
        public string VehicleName { get; set; }

        /// <summary>
        /// Odometer of Vehicle for Preventive Maintenance
        /// </summary>
        [GridColumn(Title = "Odométro", Width = "250px")]
        public int? Odometer { get; set; }

        /// <summary>
        /// Property
        /// </summary>
        public int? OdometerValue
        {
            get {
                return Odometer;
            }
            set {
                Odometer = value;
                LastReview = value;
            }
        }

        /// <summary>
        /// Next Review for Preventive Maintenance
        /// </summary>
        [GridColumn(Title = "Siguiente Revisión", Width = "250px")]
        public int NextReview { get; set; }

        /// <summary>
        /// First Review for Preventive Maintenance
        /// </summary>
        [DisplayName("Última Revisión")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [RequiredIf("FrequencyTypeId == 300", ErrorMessage = "{0} es requerido")]
        public int? LastReview { get; set; }


        /// <summary>
        /// Month Name
        /// </summary>
        [DisplayName("Tiempo")]
        [GridColumn(Title = "Meses", Width = "250px")]
        public string Time
        {
            get
            {
                if (Month != null && Year != null)
                    if (FrequencyTypeId==302)
                        return Day + "-" +ECOsystem.Utilities.Miscellaneous.GetMonthName(Month) + "-" + Year;
                    else
                        return ECOsystem.Utilities.Miscellaneous.GetMonthName(Month) + "-" + Year;
                return "";
            }
        }

        
        /// <summary>
        /// Next Alert Time of Vehicle for Preventive Maintenance
        /// </summary>
        [GridColumn(Title = "Próxima Alerta", Width = "250px")]
        public string AlertTime
        {
            get
            {
                if (Month == null || Year == null) return "";

                var date = new DateTime((int)Year, (int)Month, 1);
                if (FrequencyTypeId == 302)
                {
                    date = new DateTime((int)Year, (int)Month, (int)Day);
                    date = date.AddDays(-(Convert.ToInt32(AlertBefore)));
                    return date.Day+"-"+ECOsystem.Utilities.Miscellaneous.GetMonthName(date.Month) + "-" + date.Year;
                }
                else
                {
                    date = date.AddMonths(Convert.ToInt32(Frequency));
                    date = date.AddMonths(-(Convert.ToInt32(AlertBefore)));
                    return ECOsystem.Utilities.Miscellaneous.GetMonthName(date.Month) + "-" + date.Year;
                }
            }
        }

        /// <summary>
        /// Next Alert Time of Vehicle for Preventive Maintenance
        /// </summary>
        [GridColumn(Title = "Próxima Revisión", Width = "250px")]
        public string NextReviewTime
        {
            get
            {
                if (Month == null || Year == null) return "";
                var date =  new DateTime((int)Year, (int)Month, 1);
                if (FrequencyTypeId == 302)
                {
                    date = new DateTime((int)Year, (int)Month, (int)Day);
                    date = date.AddMonths((int)Value);
                    return date.Day + "-" + ECOsystem.Utilities.Miscellaneous.GetMonthName(date.Month) + "-" + date.Year;
                }
                else
                {
                    date = date.AddMonths(Convert.ToInt32(Frequency));
                    return ECOsystem.Utilities.Miscellaneous.GetMonthName(date.Month) + "-" + date.Year;
                }
            }
        }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of Preventive Maintenance Catalog model
        /// </summary>
        [DisplayName("Tipo Mantenimiento Preventivo")]
        public int? PreventiveMaintenanceCatalogIdSelect { get; set; }

        /// <summary>
        /// Day of the Vehicle for Preventive Maintenance
        /// </summary>
        [RequiredIf("FrequencyTypeId == 302", ErrorMessage="{0} es requerido")]
        public int? Day { get; set; }

        /// <summary>
        /// Month of the Vehicle for Preventive Maintenance
        /// </summary>
        [DisplayName("Mes de Última Revisión")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [RequiredIf("FrequencyTypeId == 301 || FrequencyTypeId == 302", ErrorMessage = "{0} es requerido")]
        public int? Month { get; set; }

        /// <summary>
        /// Year of the Vehicle for Preventive Maintenance
        /// </summary>
        [DisplayName("Año de Última Revisión")]
        //[Required(ErrorMessage = "{0} es requerido")]        
        [RequiredIf("FrequencyTypeId == 301 || FrequencyTypeId == 302", ErrorMessage = "{0} es requerido")]
        public int? Year { get; set; }

        /// <summary>
        /// Frequency of the Vehicle for Preventive Maintenance
        /// </summary>
        public string Frequency { get; set; }


        /// <summary>
        /// Date of Preventive Maintenance
        /// </summary>
        public string DatePreventiveMaintenance {
            get {
                return Frequency;
            }
            set {
                Frequency = Value.ToString();
            }
        }

        /// <summary>
        /// Alert Before of the Vehicle for Preventive Maintenance
        /// </summary>
        public string AlertBefore { get; set; }

        /// <summary>
        /// Alert for next review of Preventive Maintenance
        /// </summary>
        public int Alert { get; set; }

        /// <summary>
        /// FK Frequency Types
        /// </summary>
        public int FrequencyTypeId { get; set; }

        /// <summary>
        /// Value of Periodicity 
        /// </summary>
        public int? Value { get; set; }

        /// <summary>
        /// Name of Periodicity
        /// </summary>
        public string Periodicity { get; set; }

        /// <summary>
        /// FK of VehicleId
        /// </summary>
        [DisplayName("Vehículo")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int VehicleId { get; set; }
    }

    //    /// <summary>
    ///// Operators Model has all properties of Preventive Maintenance Record By Vehicle's table and additional properties for related objects
    ///// </summary>
    //[GridTable(PagingEnabled = true, PageSize = 30)]
    //public class PreventiveMaintenanceRecordByVehicleCatalogName : ModelAncestor
    //{
    //    /// <summary>
    //    /// Data property display the information of the entity 
    //    /// </summary>
    //    [NotMappedColumn]
    //    public int CatalogName { get; set; }
    //}

    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance Record By Vehicle's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceRecordByVehicleCostHeader : ModelAncestor
    {
        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceRecordByVehicleId { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceId { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [DisplayName("Fecha")]
        [NotMappedColumn]
        public DateTime Date { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [DisplayName("Fecha")]
        [NotMappedColumn]
        public string DateStr {
            get { return ECOsystem.Utilities.Miscellaneous.GetDateFormat(Date); }
            set { Date = Convert.ToDateTime(ECOsystem.Utilities.Miscellaneous.SetDate(value)); }
        }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [DisplayName("Odómetro")]
        [NotMappedColumn]
        public double Odometer { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public DateTime DateClient { get { return Date.ToClientTime(); } }

        /// <summary>
        /// Ddescription of Record
        /// </summary>
        [GridColumn(Title = "Registro", Width = "150px")]
        public decimal Record { get; set; }

    }

    /// <summary>
    /// Operators Model has all properties of Preventive Maintenance Record By Vehicle's table and additional properties for related objects
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class PreventiveMaintenanceRecordByVehicleCost : ModelAncestor
    {
        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceRecordByVehicleDetailId { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceRecordByVehicleId { get; set; }

        /// <summary>
        /// Data property display the information of the entity 
        /// </summary>
        [NotMappedColumn]
        public int PreventiveMaintenanceCostId { get; set; }

        /// <summary>
        /// Ddescription of Record
        /// </summary>
        [GridColumn(Title = "Descripción", Width = "150px")]
        public string Description { get; set; }

        /// <summary>
        /// Cost of Maintenance
        /// </summary>
        [GridColumn(Title = "Costo", Width = "350px")]
        public decimal Cost { get; set; }

        /// <summary>
        /// Record of Maintenance
        /// </summary>
        [GridColumn(Title = "Record", Width = "350px")]
        public decimal Record { get; set; }

        public string ServiceProvider { get; set; }
        public string InvoiceNumber { get; set; }
        public string MaintenanceTime { get; set; }
        public string Comments { get; set; }
        /// <summary>
        /// Cost of Maintenance
        /// </summary>
        [GridColumn(Title = "Costo", Width = "350px")]
        public decimal CostDollar { get; set; }

        public string VehicleId { get; set; }

        public string PreventiveMaintenanceCatalogId { get; set; }

    }

}
