﻿/************************************************************************************************************
*  File    : VehicleCostCentersModels.cs
*  Summary : VehicleCostCenters Models
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// VehicleCostCenters Base
    /// </summary>
    public class VehicleCostCentersBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public VehicleCostCentersBase()
        {
            Data = new VehicleCostCenters();
            List = new List<VehicleCostCenters>();
            Menus = new List<AccountMenus>();
            Rules = new List<TransactionRulesModel>();
            Parameters = new AdminReportsBase();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public AdminReportsBase Parameters { get; set; }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public VehicleCostCenters Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<VehicleCostCenters> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        public IEnumerable<TransactionRulesModel> Rules { get; set; }
    }


    /// <summary>
    /// VehicleCostCenters Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class VehicleCostCenters : ModelAncestor
    {

        public VehicleCostCenters()
        {

            PullPreviousBudgetDisable = true;
           
        }

        /// <summary>
        /// Propiedad para el manejo de menus
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Propiedad para el manejo de BusinessRules para Centro de Costo
        /// </summary>
        [NotMappedColumn]
        public IEnumerable<TransactionRulesModel> Rules { get; set; }

        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? CostCenterId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Nombre Centro de Costo", Width = "250px", SortEnabled = true)]
        public string Name { get; set; }

        /// <summary>
        /// Cost Center Code
        /// </summary>
        [DisplayName("Código")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Código", Width = "50px", SortEnabled = true)]
        public string Code { get; set; }

        /// <summary>
        /// FK UnitId from VehicleUnits
        /// </summary>
        [DisplayName("Unidad")]
        [Required(ErrorMessage = "{0} es requerido")]
        [NotMappedColumn]
        public int UnitId { get; set; }

        /// <summary>
        /// Unit Name
        /// </summary>
        [DisplayName("Unidad")]
        [GridColumn(Title = "Unidad", Width = "100px", SortEnabled = true)]
        public string UnitName { get; set; }

        [NotMappedColumn]
        [DisplayName("Presupuesto mensual")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        //public decimal MonthlyBudget => TotalBudget - AdditionalBudget;
        public decimal MonthlyBudget { get{ return TotalBudget - AdditionalBudget;} }
        
        [NotMappedColumn]
        [DisplayName("Presupuesto adicional")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal AdditionalBudget { get; set; }

        [NotMappedColumn]
        [DisplayName("Presupuesto total")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal TotalBudget { get; set; }

        [NotMappedColumn]
        [DisplayName("Compras del mes")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        //public decimal MonthlyPurchases => TotalBudget - Available;
        public decimal MonthlyPurchases { get{ return TotalBudget - Available;} }


        [NotMappedColumn]
        [DisplayName("Disponible")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal Available { get; set; }

        /// <summary>
        /// PullPreviousBudget
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Trasladar el disponible al siguiente mes")]
        public bool PullPreviousBudget { get; set; }

        /// <summary>
        /// PullPreviousBudgetDisable
        /// </summary>
        [NotMappedColumn]
        public bool PullPreviousBudgetDisable { get; set; }

        /// <summary>
        /// Sum of the Vehicles on the Cost Center
        /// </summary>  
        [DisplayName("Cant. de Vehiculos")]
        [GridColumn(Title = "Cant. de Vehiculos", Width = "30px", SortEnabled = true)]
        public int CantVehicles { get; set; }
        

    }
}