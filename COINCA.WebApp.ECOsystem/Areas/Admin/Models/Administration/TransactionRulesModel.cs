﻿/************************************************************************************************************
*  File    : TransactionRulesModelBase.cs
*  Summary : Transaction Rules Models
*  Author  : Gerald Solano
*  Date    : 18/10/2015
* 
*  Copyright 2016 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using ECOsystem.Models.Account;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;

namespace  ECOsystem.Areas.Admin.Models.Administration
{
    /// <summary>
    /// Base Model
    /// </summary>
    public class TransactionRulesModelBase
    {
        /// <summary>
        /// Constructor TransactionRules
        /// </summary>
        public TransactionRulesModelBase()
        {
            Data = new TransactionRulesModel();
            List = new List<TransactionRulesModel>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Transaction Rules Data
        /// </summary>
        public TransactionRulesModel Data { get; set; }

        /// <summary>
        /// Transaction Rules data list
        /// </summary>
        public IEnumerable<TransactionRulesModel> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

    }
    /// <summary>
    /// Range Parameters Model for maintenance
    /// </summary>
    public class TransactionRangeParametersModel
    {
        public int TRId { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// Model Transaction Rules for maintenance
    /// </summary>
    public class TransactionRulesModel
    {
        public override string ToString()
        {
            return $"CustomerId:{CustomerId} RuleId:{RuleId} RuleName:{RuleName} VehicleId:{VehicleId}";
        }
        public int CustomerId { get; set; }

        public int? CostCenterId { get; set; }

        public bool IsCostCenter { get; set; }

        public int? VehicleId { get; set; }

        public bool IsVehicles { get; set; }

        public int? RuleCustomerId { get; set; }

        public int? RuleVehicleId { get; set; }

        public int? RuleCostCenterId { get; set; }

        public int RuleId { get; set; }

        public int UserId { get; set; }

        public string RuleName { get; set; }

        public string RuleDescription { get; set; }

        public int RuleType { get; set; }

        public string ApplyRule { get; set; }

        public bool RuleActive { get; set; }

        public double RuleValue { get; set; }

        public string TransactionCode { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public TransactionRangeParametersModel RangeParameters { get; set; }

        public List<TransactionRuleBinnacle> Binnacle { get; set; }

    }

    public class TransactionRuleBinnacle {
        public int UserId { get; set; }
        public string Active { get; set; }
        public DateTime LogDateTime { get; set; }
        public string Date
        {
            get { return LogDateTime.ToString("dddd, dd' de 'MMMM' de 'yyyy - hh:mm:ss tt"); }
        }
        public string EncryptedName { get; set; }
        public string DecryptedName
        {
            get { return TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = TripleDesEncryption.Encrypt(value); }
        }
    }

    public class TransactionRulesParameters
    {
        public string ParameterName { get; set; }

        public string Value { get; set; }
    }

    public class DailyTransactionLimitAddOrEdit
    {
        public int Number { get; set; }
        public int Amount { get; set; }
    }
}