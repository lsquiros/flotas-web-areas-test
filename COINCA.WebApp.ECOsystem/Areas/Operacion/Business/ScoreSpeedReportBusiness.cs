﻿/************************************************************************************************************
*  File    : ScoreSpeedRoutesReportBusiness.cs
*  Summary : ScoreSpeedRoutesReport Business Methods
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Collections.Generic;
using ECO_Operation.Models;
using ECOsystem.Business.Core;
using System.Data;
using System.Globalization;

namespace ECO_Operation.Business
{
    
    public class ScoreSpeedReportBusiness : IDisposable
    {
        
        public ScoreSpeedReportBase RetrieveScoreSpeedReport(OperationReportsBase parameters) 
        {
            var result = new ScoreSpeedReportBase();
                                 
            GetReportData(parameters, result);           
            
            FillColors(result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            
            return result;
        }


        private void FillColors(ScoreSpeedReportBase result)
        {
            IList<string> colors = new List<string>();          
            CustomerThresholdsBusiness objsholdsBusi = new CustomerThresholdsBusiness();

            var ThesholdsBusi = objsholdsBusi.RetrieveThresholds().FirstOrDefault();
            result.Parameters.RedHigher = ThesholdsBusi.RedHigher;
            result.Parameters.RedLower = ThesholdsBusi.RedLower;
            result.Parameters.YellowLower = ThesholdsBusi.YellowLower;
            result.Parameters.YellowHigher = ThesholdsBusi.YellowHigher;
            result.Parameters.GreenLower = ThesholdsBusi.GreenLower;
            result.Parameters.GreenHigher = ThesholdsBusi.GreenHigher;

            foreach (ScoreSpeedReport item in result.List)
            {
                if (item.Score <= ThesholdsBusi.GreenHigher && item.Score >= ThesholdsBusi.GreenLower)               
                    colors.Add("#558B2F"); //green              
                              
                if (item.Score <= ThesholdsBusi.RedHigher && item.Score >= ThesholdsBusi.RedLower)
                    colors.Add("#C62828"); //red
              
                if (item.Score <= ThesholdsBusi.YellowHigher && item.Score >= ThesholdsBusi.YellowLower)
                    colors.Add("#F9A825"); //yellow               
            }
            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));          
        }
                
        
        private void GetReportData(OperationReportsBase parameters, ScoreSpeedReportBase result)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var customerId = Session.GetCustomerId();

                result.List = dba.ExecuteReader<ScoreSpeedReport>("[Operation].[Sp_GetScoreSpeedAllDriver]",
                    new
                    {
                        CustomerId = customerId ?? 0,
                        parameters.Year,
                        parameters.Month,
                        parameters.StartDate,
                        parameters.EndDate,
                        parameters.ScoreType,
                        parameters.InvocationType
                    });
            }

            foreach (var s in result.List)           
                s.Score = Math.Round(s.Score, 0);           

            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.DecryptedName)));          
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.Score)));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable ScoreSpeedReportGenerate(IEnumerable<ScoreSpeedReport> list, DateTime startDate, DateTime endDate)
        {
            var CustomerName = Session.GetCustomerInfo().DecryptedName;
            using (var dt = new DataTable())
            {
                dt.Columns.Add("DecryptedIdentification");
                dt.Columns.Add("DecryptedName");
                dt.Columns.Add("Score");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("CostCenterId");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("VehicleGroupId");
                dt.Columns.Add("VehicleGroupName");

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);

                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();

                    row["DecryptedIdentification"] = item.DecryptedIdentification;
                    row["DecryptedName"] = item.DecryptedName;
                    row["Score"] = item.Score;
                    row["InitRangeDate"] = startDateStr;
                    row["FiniRangeDate"] = endDateStr;
                    row["CustomerName"] = CustomerName;
                    row["CostCenterId"] = item.CostCenterId;
                    row["CostCenterName"] = item.CostCenterName;
                    row["VehicleGroupId"] = item.VehicleGroupId;
                    row["VehicleGroupName"] = item.VehicleGroupName;

                    dt.Rows.Add(row);
                }

                return dt;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData(OperationReportsBase parameters)
        {
            try
            {
                var result = new ScoreSpeedReportBase();
                GetReportData(parameters, result);

                var startDate = new DateTime();
                var endDate = new DateTime();

                if (parameters.StartDate != null && parameters.EndDate != null)
                {
                    startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                    endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
                }
                else if (parameters.Month != null && parameters.Year != null)
                {
                    startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                    endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
                }

                return ScoreSpeedReportGenerate(result.List, startDate, endDate);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="parameters"></param>
        ///// <returns></returns>
        public DataTable GenerateScoreSpeedReportData(OperationReportsBase parameters)
        {
            ScoreSpeedReportBase result = new ScoreSpeedReportBase();
            GetReportData(parameters, result);
            var resultApp = new List<ScoreSpeedReport>();

            var startDate = new DateTime();
            var endDate = new DateTime();

            if (parameters.StartDate != null && parameters.EndDate != null)
            {
                startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
            }
            else if (parameters.Month != null && parameters.Year != null)
            {
                startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
            }

            foreach (var item in result.List)
            {
                var e = new ScoreSpeedReport();
                e.DecryptedIdentification = item.DecryptedIdentification;
                e.DecryptedName = item.DecryptedName;
                e.Score = item.Score;

                resultApp.Add(e);
            }

            return ScoreSpeedReportGenerate(resultApp, startDate, endDate);
        }


        public void Dispose() {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    
    }
}