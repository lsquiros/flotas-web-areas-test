﻿/************************************************************************************************************
*  File    : WeightedScoreSettingsBusiness.cs
*  Summary : WeightedScoreSettings Business Methods
*  Author  : Napoleón Alvarado
*  Date    : 01/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECO_Operation.Models;

namespace ECO_Operation.Business
{
    /// <summary>
    /// WeightedScoreSettings Class
    /// </summary>
    public class WeightedScoreSettingsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve WeightedScoreSettings
        /// </summary>
        /// <param name="weightedScoreId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<WeightedScoreSettings> RetrieveWeightedScoreSettings(int? weightedScoreId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<WeightedScoreSettings>("[Operation].[Sp_WeightedScoreSettings_Retrieve]",
                    new
                    {
                        WeightedScoreId = weightedScoreId,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity WeightedScoreSettings
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditWeightedScoreSettings(WeightedScoreSettings model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Operation].[Sp_WeightedScoreSettings_AddOrEdit]",
                    new
                    {
                        model.WeightedScoreId,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        model.ScoreTypeId,
                        model.Weight,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model WeightedScoreSettings
        /// </summary>
        /// <param name="weightedScoreId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteWeightedScoreSettings(int weightedScoreId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Operation].[Sp_WeightedScoreSettings_Delete]",
                    new { WeightedScoreId = weightedScoreId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}