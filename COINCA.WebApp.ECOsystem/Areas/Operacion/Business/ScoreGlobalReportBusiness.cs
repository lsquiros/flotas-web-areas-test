﻿/************************************************************************************************************
*  File    : ScoreGlobalReportBusiness.cs
*  Summary : ScoreGlobalReport Business Methods
*  Author  : Alexander AGuero
*  Date    : 26/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System.Collections.Generic;
using ECO_Operation.Models;
using ECOsystem.Business.Core;
using System.Data;
using System.Globalization;

namespace ECO_Operation.Business
{
    /// <summary>
    /// ScoreGlobalReport Class
    /// </summary>
    public class ScoreGlobalReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Current Fuels Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of ScoreGlobalReportBase in order to load the chart</returns>
        public ScoreGlobalReportBase RetrieveScoreGlobalSumary(OperationReportsBase parameters)
        {
            var result = new ScoreGlobalReportBase();
            if (String.IsNullOrEmpty(parameters.ReportType))              
            GetReportData(parameters, result);
            FillColors(result);
            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public ScoreGlobalReportBase RetrieveScoreGlobalReport(OperationReportsBase parameters)
        {
            var result = new ScoreGlobalReportBase();
            result.Parameters = parameters;
           
            GetReportData(parameters, result);

            FillColors(result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>

        private void FillColors(ScoreGlobalReportBase result)
        {
            List<string> colors = new List<string>();
            int? average = result.Parameters.GreenHigher; //GreenHigher trae asignado el promedio calculado en GetReportData;
            if (average != null)
            {
                decimal actual_value, media = 0;
                int count = result.List.Count() - 1;

                if (result.List.Count() % 2 == 0)
                    if (result.Parameters.ReportType == "R")
                        media = Math.Round((result.List.ToArray()[(count / 2)].WeightedIndexByRoute + result.List.ToArray()[(count / 2) + 1].WeightedIndexByRoute) / 2, 1);
                    else
                        media = Math.Round((result.List.ToArray()[(count / 2)].WeightedIndexByAdmin + result.List.ToArray()[(count / 2) + 1].WeightedIndexByAdmin) / 2, 1);
                else
                    if (result.Parameters.ReportType == "R")
                        media = Math.Round(result.List.ToArray()[(count / 2)].WeightedIndexByRoute, 1);
                    else
                        media = Math.Round(result.List.ToArray()[(count / 2)].WeightedIndexByAdmin, 1);


                foreach (ScoreGlobalReport item in result.List)
                {

                    if (result.Parameters.ReportType == "R")
                        actual_value = item.WeightedIndexByRoute;
                    else
                        actual_value = item.WeightedIndexByAdmin;

                    if (actual_value >= media)
                        if (actual_value > average)
                            colors.Add("#C62828");//red 
                        else
                            colors.Add("#F9A825");//yellow                    
                    else
                        colors.Add("#558B2F");//green

                }
            }
            result.Parameters.Colors = new HtmlString(JsonConvert.SerializeObject(colors));
        }
           

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(OperationReportsBase parameters, ScoreGlobalReportBase result)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var customerId = Session.GetCustomerId();

                result.List = dba.ExecuteReader<ScoreGlobalReport>("[Operation].[Sp_GetGlobalScoresbyCustomer]",
                new
                {
                    CustomerId = (customerId != null)? customerId : 0,
                    parameters.Year,
                    parameters.Month,
                    parameters.StartDate,
                    parameters.EndDate,
                    parameters.ReportType
                });
            }
          
            foreach (var item in result.List) {
                item.Score = Math.Round(item.Score, 0);
                item.ScoreSpeedAdmin = Math.Round(item.ScoreSpeedAdmin, 0);
                item.ScoreSpeedRoute = Math.Round(item.ScoreSpeedRoute, 0);              
            }


            if (parameters.ReportType == "R") {
                result.List = result.List.OrderByDescending(x => x.WeightedIndexByRoute).ToList();
                result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.WeightedIndexByRoute, 2))));
                if (result.List.Any())
                result.Parameters.GreenHigher = (int) Math.Round(result.List.Average(x => x.WeightedIndexByRoute),0);
            }
            else {
                result.List = result.List.OrderByDescending(x => x.WeightedIndexByAdmin).ToList();             
                result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.WeightedIndexByAdmin, 2))));
                if(result.List.Any())
                result.Parameters.GreenHigher = (int) Math.Round(result.List.Average(x => x.WeightedIndexByAdmin),0);
            }
         
           result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.DecryptedName)));
          
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable ScoreGlobalReportGenerate(IEnumerable<ScoreGlobalReport> list, string Report_Type, DateTime startDate, DateTime endDate)
        {
            var CustomerName = Session.GetCustomerInfo().DecryptedName;
            using (var dt = new DataTable())
            {
                dt.Columns.Add("DecryptedIdentification");
                dt.Columns.Add("DecryptedName");
                dt.Columns.Add("TraveledDistance");
                dt.Columns.Add("ScoreSpeed");
                dt.Columns.Add("WeightedIndex");
                dt.Columns.Add("InitRangeDate");
                dt.Columns.Add("FiniRangeDate");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("CostCenterName");
                dt.Columns.Add("CostCenterId");
                dt.Columns.Add("VehicleGroupName");
                dt.Columns.Add("VehicleGroupId");

                var startDateStr = startDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                var endDateStr = endDate.ToString("MMMM d, yyyy", CultureInfo.GetCultureInfo("es-CR"));
                //Get array letters
                char[] letters = startDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                startDateStr = new string(letters);
                letters = endDateStr.ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                // return the array made of the new char array
                endDateStr = new string(letters);

                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();                 
                    row["DecryptedIdentification"] = item.DecryptedIdentification;
                    row["DecryptedName"] = item.DecryptedName;
                    row["TraveledDistance"] = item.TotalDistanceStr;
                    row["ScoreSpeed"] = item.ScoreSpeedRouteStr;
                    row["WeightedIndex"] = item.WeightedIndexByRoute;
                    row["InitRangeDate"] = startDateStr;
                    row["FiniRangeDate"] = endDateStr;
                    row["CustomerName"] = CustomerName;
                    row["CostCenterName"] = item.CostCenterName;
                    row["CostCenterId"] = item.CostCenterId;
                    row["VehicleGroupName"] = item.VehicleGroupName;
                    row["VehicleGroupId"] = item.VehicleGroupId;

                    dt.Rows.Add(row);
                }

                return dt;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData(OperationReportsBase parameters)
        {
            try
            {
                var result = new ScoreGlobalReportBase();
                GetReportData(parameters, result);

                var startDate = new DateTime();
                var endDate = new DateTime();

                if (parameters.StartDate != null && parameters.EndDate != null)
                {
                    startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                    endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
                }
                else if (parameters.Month != null && parameters.Year != null)
                {
                    startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                    endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
                }
                return ScoreGlobalReportGenerate(result.List, parameters.ReportType, startDate, endDate);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="parameters"></param>
        ///// <returns></returns>
        public DataTable GenerateScoreGlobalReportData(OperationReportsBase parameters)
        {
            var result = new ScoreGlobalReportBase();
            GetReportData(parameters, result);
            var resultApp = new List<ScoreGlobalReport>();

            var startDate = new DateTime();
            var endDate = new DateTime();

            if (parameters.StartDate != null && parameters.EndDate != null)
            {
                startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
            }
            else if (parameters.Month != null && parameters.Year != null)
            {
                startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
            }

            foreach (var item in result.List)
            {
                var e = new ScoreGlobalReport();
                e.DecryptedIdentification = item.DecryptedIdentification;
                e.DecryptedName = item.DecryptedName;
                e.TotalDistance = item.TotalDistance;             
                if(result.Parameters.ReportType =="R")
                    e.ScoreSpeedRoute = item.ScoreSpeedRoute;              
                else
                e.ScoreSpeedAdmin = item.ScoreSpeedAdmin;

                resultApp.Add(e);
            }

            return ScoreGlobalReportGenerate(resultApp, result.Parameters.ReportType, startDate, endDate);
        }



        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose(){
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}