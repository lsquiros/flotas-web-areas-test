﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECO_Operation.Models;

namespace ECO_Operation.Business
{
    /// <summary>
    /// General Collections Class Contains all methods for load Drop Down List
    /// </summary>
    public class GeneralCollections
    {   
        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetDatesGroupBy
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {1, "Agrupación por: Año"},
                    {2, "Agrupación por: Mes"},
                    {4, "Agrupación por: Semana"},
                    {3, "Agrupación por: Día"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetMonthNames
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetComparativeGroupBy
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {100, "Comparación por: Vehículo"},
                    {200, "Comparación por: Marca"},
                    {300, "Comparación por: Modelo"},
                    {400, "Comparación por: Tipo"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }
        
        /// <summary>
        /// Return a list of Fuels in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetFuelsByCountry
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehicleCategoriesBusiness())
                {
                    var result = business.RetrieveFuels(null, countryId: ECOsystem.Utilities.Session.GetCountryId());
                    foreach (var r in result)
                    {
                        dictionary.Add(r.FuelId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Load Types for populating DropDownListFor
        /// </summary>
        public static SelectList GetVehicleLoadTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new VehicleCategoriesBusiness())
                {
                    var result = business.RetrieveTypes("VEHICLES_LOAD_WEIGHT");
                    foreach (var r in result)
                    {
                        dictionary.Add(r.TypeId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a current Month Name
        /// </summary>
        public static SelectList GetCurrentMonthName
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.Where(w => w.Id == DateTime.Now.Month).ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year - 5; i < year + 9; i++)
                {
                    dictionary.Add(i, i);
                }
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }      
                

        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetReportCriteria
        {
            get
            {
                return new SelectList(new Dictionary<int, string> { 
                    {(int)ReportCriteria.Period,"Por Período"},
                    {(int)ReportCriteria.DateRange,"Rango de Fechas"}
                }, "Key", "Value", "");
            }
        }

    
        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetDriverReportCriteria
        {
            get
            {
                return new SelectList(new Dictionary<int, string> { 
                     {(int)ReportCriteria.Period,"Por Período"},
                     {(int)ReportCriteria.DateRange,"Rango de Fechas"},
                     {(int)ReportCriteria.DayDate,"Por Día"}
                }, "Key", "Value", "");
            }
        }

        /// <summary>
        /// Return a list of Icon of vehicle in order to using for populate DropDownList 
        /// </summary>
        public static SelectList GetIconVehicleCategory
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                string path = HttpContext.Current.Server.MapPath("~/Content/Images/Vehicles/");
                if (Directory.Exists(path))
                {
                    List<String> listImage = Directory.GetFiles(path, "*.png", SearchOption.AllDirectories).ToList();
                    foreach (String itemImage in listImage)
                    {
                        dictionary.Add(Path.GetFileName(itemImage), "{'image':'" + Path.GetFileName(itemImage) + "'}");
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Score Types in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetScoreTypes
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new ScoreTypesBusiness())
                {
                    var result = business.RetrieveScoreTypes(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.ScoreTypeId, r.Code + " " + r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }     
        
    }
}