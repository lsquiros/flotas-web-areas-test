﻿/************************************************************************************************************
*  File    : ScoreGlobalReportBusiness.cs
*  Summary : ScoreGlobalReport Business Methods
*  Author  : Alexander AGuero
*  Date    : 26/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using ECO_Operation.Models;

namespace ECO_Operation.Business
{
    /// <summary>
    /// ScoreGlobalReport Class
    /// </summary>
    public class DrivingTimeBusiness : IDisposable
    {

        public DrivingTimeBase RetrieveDrivingTimeReport(OperationReportsBase parameters)
        {
            var result = new DrivingTimeBase();
            if (!String.IsNullOrEmpty(parameters.StartDateStr))
            {
                parameters.Month = null;
                GetReportData(parameters, result);
            }
            else
                GetReportData(parameters, result);

            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ReportFuelTypeId = parameters.ReportFuelTypeId;
            result.Parameters.ParametersInBase64 = Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));

            return result;
        }

        /// <summary>
        /// GetReportData
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="result"></param>
        private void GetReportData(OperationReportsBase parameters, DrivingTimeBase result)
        {
            GetDrivingTimeAllDriver(parameters, result);
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetDrivingTimeAllDriver(OperationReportsBase parameters, DrivingTimeBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                string StartDateStr = null; string EndDateStr = null; string DayDateStr = null;

                if (!String.IsNullOrEmpty(parameters.StartDateStr)) {
                    StartDateStr = String.Format("{0:yyyy-MM-dd}", parameters.StartDate);
                    EndDateStr = String.Format("{0:yyyy-MM-dd}", parameters.EndDate);
                }

                if (!String.IsNullOrEmpty(parameters.DayDateStr))              
                    DayDateStr = String.Format("{0:yyyy-MM-dd}", parameters.DayDate);                

                var customerId = Session.GetCustomerId();

                result.List = dba.ExecuteReader<DrivingTimeReport>("[Operation].[Sp_DrivingTimeperDriver_Retrieve]",
               new
               {
                   CustomerId = (customerId != null) ? customerId : 0,
                   StartDateStr,
                   EndDateStr,
                   DayDateStr,
                   parameters.Month,
                   parameters.Year
               });
            }

            result.List = result.List.OrderBy(x => x.WorkingTime).ToList();

            result.Parameters.Data2 = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.WorkingTime - x.DrivingTime, 0)).ToList()));
            result.Parameters.Data = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => Math.Round(x.DrivingTime, 0)).ToList()));
            result.Parameters.Labels = new HtmlString(JsonConvert.SerializeObject(result.List.Select(x => x.DecryptedName).ToList()));
        }


        /// <summary>
        /// Generate ScoreGlobal Repor To Excel
        /// </summary>
        /// <returns>A model of ScoreGlobalReportBase in order to load the chart</returns>
        public byte[] GenerateScoreGlobalReportExcel(OperationReportsBase parameters)
        {
            var result = new DrivingTimeBase();

            GetReportData(parameters, result);

            using (var excel = new ExcelReport())
            {
                return excel.CreateSpreadsheetWorkbook("Puntuación General de Conductores", result.List.ToList());
            }

        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}