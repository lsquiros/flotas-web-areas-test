﻿/************************************************************************************************************
*  File    : WeightedScoreSettingsModels.cs
*  Summary : WeightedScoreSettings Models
*  Author  : Napoleón Alvarado
*  Date    : 01/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ECOsystem.Models.Miscellaneous;
using GridMvc.DataAnnotations;
using ECOsystem.Models.Account;

namespace ECO_Operation.Models
{
    /// <summary>
    /// WeightedScoreSettings Base
    /// </summary>
    public class WeightedScoreSettingsBase
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        public WeightedScoreSettingsBase()
        {
            Data = new WeightedScoreSettings();
            List = new List<WeightedScoreSettings>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public WeightedScoreSettings Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<WeightedScoreSettings> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }
    }


    /// <summary>
    /// WeightedScoreSettings Model
    /// </summary>
    [GridTable(PagingEnabled = true, PageSize = 30)]
    public class WeightedScoreSettings : ModelAncestor
    {
        /// <summary>
        /// The PRIMARY KEY property uniquely that identifies each record of this model
        /// </summary>
        [NotMappedColumn]
        public int? WeightedScoreId { get; set; }

        /// <summary>
        /// The FOREIGN KEY property uniquely that identifies the Customer of this model
        /// </summary>
        [NotMappedColumn]
        public int? CustomerId { get; set; }

        /// <summary>
        /// The FOREIGN KEY property uniquely that identifies the Score Type of this model
        /// </summary>
        [NotMappedColumn]
        [DisplayName("Tipo de Calificación")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int? ScoreTypeId { get; set; }

        /// <summary>
        /// Code of the Score Type Related
        /// </summary>
        [DisplayName("Código")]
        [GridColumn(Title = "Código", Width = "50px", SortEnabled = true)]
        public string ScoreTypeCode { get; set; }

        /// <summary>
        /// Name of the Score Type Related
        /// </summary>
        [DisplayName("Nombre del Tipo de Calificación")]
        [GridColumn(Title = "Nombre del Tipo de Calificación", Width = "150px", SortEnabled = true)]
        public string ScoreTypeName { get; set; }
        
        /// <summary>
        /// Description of the group
        /// </summary>
        [DisplayName("Peso")]
        [Required(ErrorMessage = "{0} es requerido")]
        [GridColumn(Title = "Peso", Width = "50px", SortEnabled = true)]
        public decimal Weight { get; set; }

    }
}