﻿/************************************************************************************************************
*  File    : ScoreSpeedAdminReportModels.cs
*  Summary : ScoreSpeedAdminReport  Models
*  Author  : Alexander Aguero V
*  Date    : 21/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using ECOsystem.Models.Account;

namespace ECO_Operation.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class DrivingTimeBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public DrivingTimeBase()
        {
            Parameters = new OperationReportsBase();
            List = new List<DrivingTimeReport>();
            Menus = new List<AccountMenus>();
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public OperationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<DrivingTimeReport> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus>  Menus { get; set; }

    }


    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class DrivingTimeReport
    {
        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        public string DecryptedName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Identification
        /// </summary>
        [DisplayName("Identificación")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string EncryptedIdentification { get; set; }

        /// <summary>
        /// DecryptedIdentification
        /// </summary>
        [DisplayName("Identificación")]
        public string DecryptedIdentification
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedIdentification); }
            set { EncryptedIdentification = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// WorkingTime
        /// </summary>
        [DisplayName("Tiempo de Jornada")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double WorkingTime { get; set; }

        /// <summary>
        /// WorkingTimestr
        /// </summary>
        public string WorkingTimestr
        {
            get { return WorkingTime.ToString()+" horas"; }   
        }

        /// <summary>
        /// DrivingTime
        /// </summary>
        [DisplayName("Horas Laboradas")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public double DrivingTime { get; set; }

        /// <summary>
        /// DrivingTimestr
        /// </summary>
        public string DrivingTimestr
        {
            get { return DrivingTime.ToString() + " horas"; }
        }

    }



}


