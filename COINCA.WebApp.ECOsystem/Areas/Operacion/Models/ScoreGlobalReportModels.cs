﻿/************************************************************************************************************
*  File    : ScoreSpeedAdminReportModels.cs
*  Summary : ScoreSpeedAdminReport  Models
*  Author  : Alexander Aguero V
*  Date    : 21/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;
using GridMvc.DataAnnotations;
using ECOsystem.Utilities.Helpers;
using System;
using ECOsystem.Models.Account;

namespace ECO_Operation.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ScoreGlobalReportBase
    {
        /// <summary>
        /// Control Fuels Reports Base Constructor
        /// </summary>
        public ScoreGlobalReportBase()
        {
            Parameters = new OperationReportsBase();
            List = new List<ScoreGlobalReport>();
            Menus = new List<AccountMenus>();
            LowScoreCount = 0;
            RegularScoreCount= 0;
            SuperiorScoreCount = 0;
        }

        /// <summary>
        /// Report Parameters
        /// </summary>
        public OperationReportsBase Parameters { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<ScoreGlobalReport> List { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public IEnumerable<AccountMenus> Menus { get; set; }

        /// <summary>
        /// Low Driver Score Count 
        /// </summary>
        public decimal LowScoreCount { get; set; }

        /// <summary>
        /// Regular Driver Score Percentage
        /// </summary>
        public decimal RegularScoreCount { get; set; }

        /// <summary>
        /// Superior Driver Score Percentage
        /// </summary>
        public decimal SuperiorScoreCount { get; set; }               
    }
    
    /// <summary>
    /// Current Fuels Report
    /// </summary>
    public class ScoreGlobalReport
    {
        /// <summary>
        /// DriverScoreId
        /// </summary>
        [ExcelNoMappedColumn]
        public int DriverScoreId { get; set; }
        
        /// <summary>
        /// UserId
        /// </summary>
        [DisplayName("Usuario")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int UserId { get; set; }

        /// <summary>
        /// VehicleId
        /// </summary>        
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public int? VehicleId { get; set; }

        /// <summary>
        /// Identification
        /// </summary>
        [DisplayName("Identificación")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string EncryptedIdentification { get; set; }

        [DisplayName("Identificación")]
        public string DecryptedIdentification
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedIdentification); }
            set { EncryptedIdentification = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public string EncryptedName { get; set; }

        /// <summary>
        /// Decrypted Name
        /// </summary>
        [DisplayName("Nombre")]
        [NotMappedColumn]
        public string DecryptedName
        {
            get { return ECOsystem.Utilities.TripleDesEncryption.Decrypt(EncryptedName); }
            set { EncryptedName = ECOsystem.Utilities.TripleDesEncryption.Encrypt(value); }
        }
        
        /// <summary>
        /// Cantidad de registros
        /// </summary>
        public int TotalRecords { get; set; }

        /// <summary>
        /// Velocidad total en exceso
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double TotalOverSpeed { get; set; }

        /// <summary>
        /// Velocidad total en exceso
        /// </summary>
        [DisplayName("Velocidad total en exceso")]
        [NotMappedColumn]
        [ExcelMappedColumn("ΣVec")]
        public decimal TotalOverSpeedStr
        {
            get { return Math.Round(decimal.Parse(TotalOverSpeed.ToString()), 0); }
        }

        /// <summary>
        /// Cantidad de excesos de velocidad
        /// </summary>
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double TotalOverSpeedAmount { get; set; }

        /// <summary>
        /// Cantidad de excesos de velocidad
        /// </summary>
        [DisplayName("Cantidad de excesos de velocidad")]
        [NotMappedColumn]
        [ExcelMappedColumn("Nec")]
        public decimal TotalOverSpeedAmountStr
        {
            get { return Math.Round(decimal.Parse(TotalOverSpeedAmount.ToString()), 0); }
        }

        /// <summary>
        /// Total Distancia Recorrida
        /// </summary>
        [DisplayName("Distancia Recorrida")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double TotalDistance { get; set; }

        /// <summary>
        /// Total Distancia Recorrida
        /// </summary>
        [DisplayName("Distancia Recorrida")]
        [NotMappedColumn]
        [ExcelMappedColumn("ΣDt")]
        public decimal TotalDistanceStr
        {
            get { return Math.Round(decimal.Parse(TotalDistance.ToString()), 0); }
        }

        /// <summary>
        /// Total Distancia Recorrida
        /// </summary>
        [DisplayName("Distancia Recorrida")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double TotalExcessDistance { get; set; }

        /// <summary>
        /// Total Distancia Recorrida
        /// </summary>
        [DisplayName("Distancia Recorrida")]
        [NotMappedColumn]
        [ExcelMappedColumn("ΣDec")]
        public decimal TotalExcessDistanceStr
        {
            get { return Math.Round(decimal.Parse(TotalExcessDistance.ToString()), 0); }
        }
        
        /// <summary>
        /// property Currency Symbol
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double Score { get; set; }

        /// <summary>
        /// ScoreType
        /// </summary>
        [DisplayName("Tipo Puntuación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string ScoreType { get; set; }

        /// <summary>
        /// Photo
        /// </summary>
        [DisplayName("Foto")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string Photo { get; set; }

        /// <summary>
        /// Scorestr
        /// </summary>
        [DisplayName("Puntuación")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public string Scorestr
        {
            get {
                if (TotalRecords != 0)
                {
                    var scoreResult = (decimal)Score / TotalRecords;

                    return ECOsystem.Utilities.Miscellaneous.GetPercentageFormat(scoreResult, 0); 
                }
                else
                {
                    return ECOsystem.Miscellaneous.Miscellaneous.GetPercentageFormat(0, 0);
                }
                
            }
        }

        /// <summary>
        /// ScoreSpeedAdmin
        /// </summary>
        [DisplayName("Puntuación Administrativa")]
        [NotMappedColumn]
        [ExcelNoMappedColumn]
        public double ScoreSpeedAdmin { get; set; }

        /// <summary>
        /// ScoreSpeedAdminstr
        /// </summary>
        [DisplayName("Puntuación Administrativa")]
        [NotMappedColumn]
        [ExcelMappedColumn("DS (Admin)")]
        public string ScoreSpeedAdminstr
        {
            get {
                if (TotalRecords != 0)
                {
                    var scoreResult = (decimal)ScoreSpeedAdmin / TotalRecords;

                    return ECOsystem.Utilities.Miscellaneous.GetPercentageFormat(scoreResult, 0);
                }
                else
                {
                    return ECOsystem.Miscellaneous.Miscellaneous.GetPercentageFormat(0, 0);
                }
            }
        }

        /// <summary>
        /// ScoreSpeedRoute
        /// </summary>
        [DisplayName("Puntuación Carretera")]
        [ExcelNoMappedColumn]
        [NotMappedColumn]
        public double ScoreSpeedRoute { get; set; }

        /// <summary>
        /// ScoreSpeedRouteStr
        /// </summary>
        [DisplayName("Puntuación Carretera")]
        [NotMappedColumn]
        [ExcelMappedColumn("DS (Route)")]
        public string ScoreSpeedRouteStr
        {
           get {
               if (TotalRecords != 0)
               {
                   var scoreResult = (decimal)ScoreSpeedRoute / TotalRecords;

                   return ECOsystem.Utilities.Miscellaneous.GetPercentageFormat(scoreResult, 0);
               }
               else
               {
                   return ECOsystem.Miscellaneous.Miscellaneous.GetPercentageFormat(0, 0);
               }
           }
        }

        /// <summary>
        /// WeightedIndexByRoute
        /// </summary>
        [DisplayName("Indice Ponderado")]
        [NotMappedColumn]
        [ExcelMappedColumn("IPC[C]")]
        public decimal WeightedIndexByRoute
        {
            get
            {
                if (ScoreSpeedRoute != 0)
                {
                    var Irc = PercentageOverSpeedDistance;
                    var Xvc = (TotalOverSpeedAmount > 0)? (TotalOverSpeed / TotalOverSpeedAmount) : 0;
                    var Ipc = (decimal)Xvc * Irc;

                    return decimal.Parse(Math.Round(Ipc, 2).ToString());
                }
                
                return 0; 
            }
        }

        /// <summary>
        /// WeightedIndexByRoute
        /// </summary>
        [DisplayName("Indice Ponderado")]
        [NotMappedColumn]
        [ExcelMappedColumn("IPC[C]")]
        public decimal WeightedIndexByAdmin
        {
            get
            {
                if (ScoreSpeedRoute != 0)
                {
                    var Irc = PercentageOverSpeedDistance;
                    var Xvc = (TotalOverSpeedAmount > 0) ? (TotalOverSpeed / TotalOverSpeedAmount) : 0;
                    var Ipc = (decimal)Xvc * Irc;

                    return decimal.Parse(Math.Round(Ipc, 2).ToString());
                }
                return 0;
            }
        }

        /// <summary>
        /// Indice de Distancia en Exceso
        /// </summary>
        [DisplayName("Indice de Distancia en Exceso")]
        [NotMappedColumn]
        [ExcelMappedColumn("Irc")]
        public decimal PercentageOverSpeedDistance {
            get
            {
                if (TotalDistance != 0)
                    return decimal.Parse(Math.Round((TotalExcessDistance / TotalDistance), 2).ToString());
                else
                    return 0;
            }
        }

        public int? CostCenterId { get; set; }
        public string CostCenterName { get; set; }
        public int? VehicleGroupId { get; set; }
        public string VehicleGroupName { get; set; }
    }
}