﻿var ECO_Operation = ECO_Operation || {};

ECO_Operation.ScoreSpeedRoutesReport = (function () {
    var options = {};
    window.chartIndicatiorsClass = 0;

    var submitForm = function () {
        ECOsystem.Utilities.ShowLoader();

        if ($('#ReloadForm').length > 0) {
            $('#ReloadForm').submit();
        } else {
            window.location.href = window.location.href;
        }
    };

    /*invalid Select Option*/
    var invalidSelectOption = function () {
        $("#ControlsContainer").html('<div class="text-danger top30">Por favor seleccionar las opciones correspondientes para cargar la información.</div>');
    };

      /*check All Selects before submit form*/
    var checkAllSelects = function () {
        if (($("#Parameters_ReportCriteriaId").val() === "")
                || ($("#Parameters_Month").val() === "")
                || ($("#Parameters_Year").val() === "")
                || ($("#Parameters_ReportFuelTypeId").val() === "")
                || ($("#Parameters_StartDateStr").val() === "")
                || ($("#Parameters_EndDateStr").val() === "")
            ) {
            if (($("#Parameters_Month").val() !== "") && ($("#Parameters_Year").val() !== ""))
                return true;
            if (($("#Parameters_StartDateStr").val() !== "") && ($("#Parameters_EndDateStr").val() !== ""))
                return true;
            invalidSelectOption();
            return false;
        }
        return true;
    };

    /*on Select Month Change*/
    var onSelectMonthChange = function (obj) {
        console.debug("cambiando a: " + obj.id);
        if (obj.id === "") {
            invalidSelectOption();
        } else {
            $("#Parameters_Month").val(obj.id);
            if (checkAllSelects()) submitForm();
        }
    };

    /*on Select Year Change*/
    var onSelectYearChange = function (obj) {
        console.debug("cambiando a: " + obj.id);
        if (obj.id === "") {
            invalidSelectOption();
        } else {
            $("#Parameters_Year").val(obj.id);
            if (checkAllSelects()) submitForm();
        }
    };

    /*on Select Report Fuel Type Change*/
    var onSelectReportFuelTypeChange = function (obj) {
        if (obj.id === "") {
            invalidSelectOption();
        } else {
            $("#Parameters_ReportFuelTypeId").val(obj.id);
            if (checkAllSelects()) submitForm();
        }
    };


    /*init Events*/
    var initEvents = function () {
        $("#page-content-wrapper").off("click.print", "a[data-print-link]").on("click.print", "a[data-print-link]", function () {
            ECOsystem.Utilities.ShowPopup(options.printURL, "Formato de impresión");
        });
        $("#page-content-wrapper").off("click.goToPrint", "button[data-print]").on("click.goToPrint", "button[data-print]", function () {
            window.print();
        });

        if (!options.printMode) {
            $("#datetimepickerStartDate").datepicker({
                language: "es",
                minDate: new Date(),
                format: "dd/mm/yyyy",
                autoclose: true,
                todayHighlight: true
            }).on("changeDate", function() {
                if ($("#Parameters_StartDateStr").val().length === 10 && checkAllSelects())
                    submitForm();
                else
                    invalidSelectOption();
            });
            $("#Parameters_StartDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: "/",
                maxLength: 10
            });


            $("#datetimepickerEndDate").datepicker({
                language: "es",
                minDate: new Date(),
                format: "dd/mm/yyyy",
                autoclose: true,
                todayHighlight: true
            }).on("changeDate", function() {
                if ($("#Parameters_EndDateStr").val().length === 10 && checkAllSelects())
                    submitForm();
                else
                    invalidSelectOption();
            });

            $("#Parameters_EndDateStr").alphanum({
                allowNumeric: true,
                allowUpper: false,
                allowLower: false,
                allowCaseless: true,
                allowSpace: false,
                allow: "/",
                maxLength: 10
            });
        }
    }

    /*disabled Controls when printMode = true*/
    var disabledControls = function () {
        if (options.printMode) {
            $("[data-filter]").attr("disabled", "disabled");
        }
    }

    /*Init Charts*/

    /*format Label*/
    var formatLabel = function (v) {
        return ECOsystem.Utilities.ToInt(v) + "%";
    }


    /*on Select Report Criteria Change*/
    var onSelectReportCriteriaChange = function (obj) {
        if (obj.id === "") {
            invalidSelectOption();
        } else {
            $("div[data-repor-by]").addClass("hide");
            $("#Parameters_Month").select2("val", "");
            $("#Parameters_Year").select2("val", "");
            $("#Parameters_StartDateStr").val("");
            $("#Parameters_EndDateStr").val("");

            $("#Parameters_ReportCriteriaId").val(obj.id);
            $("div[data-repor-by=" + obj.id + "]").removeClass("hide");
            if (checkAllSelects()) submitForm();
        }
    };

    /*initialize Drop Down List*/
    var initializeDropDownList = function () {

        var select1 = $("#Parameters_ReportCriteriaId").select2().data("select2");
        select1.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportCriteriaChange(data); return fn.apply(this, arguments); }
                return fn.apply(this, arguments);
            }
        })(select1.onSelect);

        var select2 = $("#Parameters_Month").select2().data("select2");
        select2.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectMonthChange(data); return fn.apply(this, arguments); }
                return fn.apply(this, arguments);
            }
        })(select2.onSelect);

        var select3 = $("#Parameters_Year").select2().data("select2");
        select3.onSelect = (function (fn) {
            return function (data, opts) {
                var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectYearChange(data); return fn.apply(this, arguments); }
                return fn.apply(this, arguments);
            }
        })(select3.onSelect);

        try {
            var select4 = $("#Parameters_ReportFuelTypeId").select2().data("select2");
            select4.onSelect = (function (fn) {
                return function (data, opts) {
                    var target; if (opts != null) { target = $(opts.target); } if (target) { onSelectReportFuelTypeChange(data); return fn.apply(this, arguments); }
                    return fn.apply(this, arguments);
                }
            })(select4.onSelect);
        } catch (e) {

        }

    };

    var refreshGraphicCanvas = function () {
        //$(window).resize(function () {
        //   submitForm();
        //});
    };

    /* initialize function */
    var initialize = function (opts) {
        $.extend(options, opts);
        initEvents();    
        disabledControls();     
        refreshGraphicCanvas();
        try {
            initializeDropDownList();
        }
        catch (exception) {
        }
    }

    var onClickDownloadPartnerReport = function () {
        //$('body').loader('show');
        $('#ExcelReportDownloadForm').submit();
    };

    // Loader Functions
    var showLoader = function () {
        $('body').loader('show');
    };

    var hideLoader = function () {
        $('body').loader('hide');
        $('#DonwloadFileForm').submit();
    };



/* Public methods */
return {
    Init: initialize,
    FormatLabel: formatLabel,
    OnClickDownloadPartnerReport: onClickDownloadPartnerReport,
    ShowLoader: showLoader,
    HideLoader: hideLoader,
    
};
})();






