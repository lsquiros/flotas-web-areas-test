﻿/************************************************************************************************************
*  File    : ScoreSpeedRoutesReportController.cs
*  Summary : Score of  speed in Road
*  Author  : Alexander
*  Date    : 20/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using ECO_Operation.Business;
using ECO_Operation.Models;
using ECO_Operation.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Mvc;



namespace ECO_Operation.Controllers.Operation
{
    /// <summary>
    /// Current Fuels Report Class
    /// </summary>
    public class ScoreSpeedRoutesReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new ScoreSpeedReportBusiness())
                {
                    OperationReportsBase model = new OperationReportsBase();
                    model.ScoreType = "R"; //Report
                    model.InvocationType = "R"; //Report
                    return View(business.RetrieveScoreSpeedReport(model));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(ScoreSpeedReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new ScoreSpeedReportBusiness())
                {
                    model.Parameters.ScoreType = "R"; //Route
                    model.Parameters.InvocationType = "R"; //Report
                    return View(business.RetrieveScoreSpeedReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                DataTable report = new DataTable();
                ScoreSpeedReportBase model = JsonConvert.DeserializeObject<ScoreSpeedReportBase>(p);
                model.Parameters.ScoreType = "R";
                model.Parameters.InvocationType = "R";
                using (var business = new ScoreSpeedReportBusiness())
                {
                    report = business.GetReportDownloadData(model.Parameters);
                    Session["Reporte"] = report;
                    return View("Index", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Calificación de Velocidad por Ley");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "ScoreSpeedRouteReport", this.ToString().Split('.')[2], "Reporte de Calificación de Velocidad por Ley");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                ViewBag.ErrorReport = 1;
                using (var business = new ScoreSpeedReportBusiness())
                {
                    OperationReportsBase model = new OperationReportsBase();
                    model.ScoreType = "R";
                    model.InvocationType = "R";
                    return View("Index", business.RetrieveScoreSpeedReport(model));
                }
            }
        }
    }
}