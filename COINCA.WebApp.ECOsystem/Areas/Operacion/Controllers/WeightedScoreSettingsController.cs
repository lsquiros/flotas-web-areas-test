﻿/************************************************************************************************************
*  File    : WeightedScoreSettingsController.cs
*  Summary : WeightedScoreSettings Controller Actions
*  Author  : Napoleón Alvarado
*  Date    : 01/12/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECO_Operation.Business;
using ECO_Operation.Models;
using ECOsystem.Models.Miscellaneous;
using ECO_Operation.Models.Identity;



using ECOsystem.Business.Utilities;

namespace ECO_Operation.Controllers.Operation
{
    /// <summary>
    /// WeightedScoreSettings Controller. Implementation of all action results from views
    /// </summary>
    public class WeightedScoreSettingsController : Controller
    {

        /// <summary>
        /// WeightedScoreSettings Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new WeightedScoreSettingsBusiness())
                {
                    var model = new WeightedScoreSettingsBase
                    {
                        Data = new WeightedScoreSettings(),
                        List = business.RetrieveWeightedScoreSettings(null)
                    };
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity WeightedScoreSettings
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult AddOrEditWeightedScoreSettings(WeightedScoreSettings model)
        {
            try
            {
                using (var business = new WeightedScoreSettingsBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditWeightedScoreSettings(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveWeightedScoreSettings(null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "WeightedScoreSettings")
                });
            }
        }

        /// <summary>
        /// Search WeightedScoreSettings
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchWeightedScoreSettings(string key)
        {
            try
            {
                using (var business = new WeightedScoreSettingsBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveWeightedScoreSettings(null, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "WeightedScoreSettings")
                });
            }
        }

        /// <summary>
        /// Load WeightedScoreSettings
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        public PartialViewResult LoadWeightedScoreSettings(int id)
        {
            try
            {
                using (var business = new WeightedScoreSettingsBusiness())
                {
                    return PartialView("_partials/_Detail", business.RetrieveWeightedScoreSettings(id).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "WeightedScoreSettings")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model WeightedScoreSettings
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult DeleteWeightedScoreSettings(int id)
        {
            try
            {
                using (var business = new WeightedScoreSettingsBusiness())
                {
                    business.DeleteWeightedScoreSettings(id);
                    return PartialView("_partials/_Grid", business.RetrieveWeightedScoreSettings(null));
                }
            }

            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "WeightedScoreSettings")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "WeightedScoreSettings")
                    });
                }
            }
        }
    }
}