﻿/************************************************************************************************************
*  File    : ScoreSpeedAdminReportController.cs
*  Summary : Current Fuels Report Controller Actions
*  Author  : Alexander Aguero
*  Date    : 25/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System.Web.Mvc;
using Newtonsoft.Json;
using ECO_Operation.Business;
using ECO_Operation.Models;
using ECO_Operation.Models.Identity;
using System.Data;
using ECOsystem.Utilities;
using System;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;




namespace ECO_Operation.Controllers.Operation
{
    /// <summary>
    /// Current Fuels Report Class
    /// </summary>
    public class ScoreSpeedAdminReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new ScoreSpeedReportBusiness())
                {
                    OperationReportsBase model = new OperationReportsBase();
                    model.ScoreType = "A"; //Admin
                    model.InvocationType = "R"; //Report
                    return View(business.RetrieveScoreSpeedReport(model));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(ScoreSpeedReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new ScoreSpeedReportBusiness())
                {
                    model.Parameters.ScoreType = "A"; //Admin
                    model.Parameters.InvocationType = "R"; //Report
                    return View(business.RetrieveScoreSpeedReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Excel Report Download
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                DataTable report = new DataTable();

                ScoreSpeedReportBase model = JsonConvert.DeserializeObject<ScoreSpeedReportBase>(p);
                model.Parameters.ScoreType = "A";
                model.Parameters.InvocationType = "R";
                using (var business = new ScoreSpeedReportBusiness())
                {
                    report = business.GetReportDownloadData(model.Parameters);
                    Session["Reporte"] = report;
                    return View("Index", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download File
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Calificación de Velocidad por Empresa");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "ScoreSpeedAdminReport", this.ToString().Split('.')[2], "Reporte de Calificación de Velocidad por Empresa");
                }
            }
            catch (Exception e)
            {

                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                ViewBag.ErrorReport = 1;
                using (var business = new ScoreSpeedReportBusiness())
                {
                    OperationReportsBase model = new OperationReportsBase();
                    model.ScoreType = "A";
                    model.InvocationType = "R";
                    return View("Index", business.RetrieveScoreSpeedReport(model));
                }
            }
        }
    }
}