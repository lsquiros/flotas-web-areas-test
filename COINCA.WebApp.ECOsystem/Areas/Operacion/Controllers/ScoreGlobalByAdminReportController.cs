﻿using ECO_Operation.Business;
using ECO_Operation.Models;
using ECO_Operation.Models.Identity;
using ECOsystem.Business.Utilities;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using Newtonsoft.Json;


using System;

using System.Data;
using System.IO;
using System.Web.Mvc;

namespace ECO_Operation.Controllers.Operation
{
    public class ScoreGlobalByAdminReportController : Controller
    {

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new ScoreGlobalReportBusiness())
                {
                    OperationReportsBase p = new OperationReportsBase();
                    p.ReportType = "A";
                    return View(business.RetrieveScoreGlobalReport(p));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(ScoreGlobalReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new ScoreGlobalReportBusiness())
                {
                    model.Parameters.ReportType = "A";
                    return View(business.RetrieveScoreGlobalReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Excel Report Download
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                DataTable report = new DataTable();

                ScoreGlobalReportBase model = JsonConvert.DeserializeObject<ScoreGlobalReportBase>(p);
                model.Parameters.ReportType = "A";
                using (var business = new ScoreGlobalReportBusiness())
                {
                    report = business.GetReportDownloadData(model.Parameters);
                    Session["Reporte"] = report;
                    return View("Index", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// DownloadFile
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadFile()
        {
            try
            {
                DataTable report = new DataTable();
                if (Session["Reporte"] != null)
                {
                    report = (DataTable)Session["Reporte"];
                    Session["Reporte"] = null;
                }
                using (var bus = new ReportsUtilities())
                {
                    new EventLogBusiness().AddLogEvent(LogState.INFO, "Descarga de Reporte de Índice Ponderado de Hábitos Velocidad por Empresa");
                    return bus.GetReportDataTable(JsonConvert.SerializeObject(report), "ScoreAdminGlobalReport", this.ToString().Split('.')[2], "Reporte de Índice Ponderado de Hábitos Velocidad por Empresa");
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                ViewBag.ErrorReport = 1;
                using (var business = new ScoreGlobalReportBusiness())
                {
                    OperationReportsBase p = new OperationReportsBase();
                    p.ReportType = "A";
                    return View("Index", business.RetrieveScoreGlobalReport(p));
                }
            }
        }
    }
}