﻿/************************************************************************************************************
*  File    : VehicleCategoriesController.cs
*  Summary : VehicleCategories Controller Actions
*  Author  : Berman Romero
*  Date    : 09/09/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Models.Identity;
using System.Globalization;
using ECO_Operation.Business;
using ECO_Operation.Models;



using ECOsystem.Business.Utilities;

namespace ECO_Operation.Controllers.Operation
{
    /// <summary>
    /// Vehicle Categories Controller. Implementation of all action results from views
    /// </summary>
    public class VehicleCategoriesController : Controller
    {

        /// <summary>
        /// VehicleCategories Main View
        /// </summary>
        /// <returns>A object that renders a view. This string representation would be written to the Response stream during execution</returns>
       // [Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public ActionResult Index(String menutype)
        {
            try
            {
                using (var business = new VehicleCategoriesBusiness())
                {
                    var model = new VehicleCategoriesBase
                    {
                        Data = new VehicleCategories(),
                        List = business.RetrieveVehicleCategories(null)
                    };
                    Session["MenuType"] = menutype;
                    return View(model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleCategories
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult AddOrEditVehicleCategories(VehicleCategories model)
        {
            try
            {
                using (var business = new VehicleCategoriesBusiness())
                {
                    if (ModelState.IsValid)
                    {
                        business.AddOrEditVehicleCategories(model);
                    }
                    return PartialView("_partials/_Grid", business.RetrieveVehicleCategories(null));
                }

            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCategories")
                });
            }
        }

        /// <summary>
        /// Search VehicleCategories
        /// </summary>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        public PartialViewResult SearchVehicleCategories(string key)
        {
            try
            {
                using (var business = new VehicleCategoriesBusiness())
                {
                    return PartialView("_partials/_Grid", business.RetrieveVehicleCategories(null, true, key));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCategories")
                });
            }
        }

        /// <summary>
        /// Load VehicleCategories
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        [EcoAuthorize]
        public PartialViewResult LoadVehicleCategories(int id)
        {
            try
            {
                using (var business = new VehicleCategoriesBusiness())
                {
                    if (id < 0)
                    {
                        var tmpFuels = business.RetrieveFuels(null, ECOsystem.Utilities.Session.GetCountryId());
                        return PartialView("_partials/_Detail", new VehicleCategories() { FuelsList = tmpFuels.Select(r => new FuelsByVehicleCategory() { FuelId = r.FuelId, FuelName = r.Name, IsFuelChecked = false }).ToList() });
                    }

                    var model = business.RetrieveVehicleCategories(id).FirstOrDefault();
                    model.DefaultPerformanceStr = ((decimal)model.DefaultPerformance).ToString("N", CultureInfo.CreateSpecificCulture("es-CR"));
                    model.WeightStr = ((decimal)model.Weight).ToString("N", CultureInfo.CreateSpecificCulture("es-CR"));

                    return PartialView("_partials/_Detail", model);
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                {
                    TechnicalError = e.Message + e.StackTrace,
                    ReturnUlr = Url.Action("Index", "VehicleCategories")
                });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleCategories
        /// </summary>
        /// <param name="id">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>A object that renders a partial view. This string representation would be written to the Response stream during execution</returns>
        [HttpPost]
        //[Authorize(Roles = "CUSTOMER_ADMIN")]
        [EcoAuthorize]
        public PartialViewResult DeleteVehicleCategories(int id)
        {
            try
            {
                using (var business = new VehicleCategoriesBusiness())
                {
                    business.DeleteVehicleCategories(id);
                    return PartialView("_partials/_Grid", business.RetrieveVehicleCategories(null));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                if (e.Message.Contains("REFERENCE constraint"))
                {
                    return PartialView("~/Views/Shared/_PartialErrorAttention.cshtml", new CustomError()
                    {
                        TitleError = "Eliminación de Registro",
                        TechnicalError = "El registro se encuentra asociado a otros datos, no es posible eliminarlo.",
                        ReturnUlr = Url.Action("Index", "VehicleCategories")
                    });
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialError.cshtml", new CustomError()
                    {
                        TechnicalError = e.Message + e.StackTrace,
                        ReturnUlr = Url.Action("Index", "VehicleCategories")
                    });
                }
            }
        }
    }
}