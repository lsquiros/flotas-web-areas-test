﻿
using System.Web.Mvc;
using System;
using System.Linq;
using ECO_Operation.Business;
using ECOsystem.Business.Core;
using ECO_Operation.Models;
using ECOsystem.Models.Core;
using ECO_Operation.Models.Identity;



using ECOsystem.Business.Utilities;

namespace ECO_Operation.Controllers.Operation
{
    /// <summary>
    /// Operation Controller Class
    /// </summary>
    public class OperationController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new ScoreGlobalReportBusiness())
                {
                    using (var businessThresholds = new CustomerThresholdsBusiness())
                    {

                        CustomerThresholds customerThresholds = businessThresholds.RetrieveThresholds().OrderByDescending(o => o.CodingThresholdsId).ToList().FirstOrDefault();
                        ScoreGlobalReportBase model = business.RetrieveScoreGlobalSumary(new OperationReportsBase());
                        var list = model.List.ToList();

                        int GlobalScoreListCount = list.Count();

                        if (GlobalScoreListCount > 0)
                        {
                            model.LowScoreCount = Convert.ToDecimal((from score in list
                                                                     where Math.Round(score.Score, 0) <= customerThresholds.RedHigher && Math.Round(score.Score) >= customerThresholds.RedLower
                                                                     select score).Count());

                            model.RegularScoreCount = Convert.ToDecimal((from score in list
                                                                         where Math.Round(score.Score) <= customerThresholds.YellowHigher && Math.Round(score.Score) >= customerThresholds.YellowLower
                                                                         select score).Count());

                            model.SuperiorScoreCount = Convert.ToDecimal((from score in list
                                                                          where Math.Round(score.Score) <= customerThresholds.GreenHigher && Math.Round(score.Score) >= customerThresholds.GreenLower
                                                                          select score).Count());
                        }
                        return View(model);
                    }
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
