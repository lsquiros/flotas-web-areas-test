﻿/************************************************************************************************************
*  File    : ScoreGlobalReportController.cs
*  Summary : Current Fuels Report Controller Actions
*  Author  : Alexander Aguero
*  Date    : 25/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.IO;
using System.Web.Mvc;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using ECO_Operation.Business;
using ECO_Operation.Models;
using ECO_Operation.Models.Identity;
using System;



using ECOsystem.Business.Utilities;

namespace ECO_Operation.Controllers
{
    public class DrivingTimePerDriverReportController : Controller
    {
        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [EcoAuthorize]
        public ActionResult Index()
        {
            try
            {
                using (var business = new DrivingTimeBusiness())
                {
                    return View(business.RetrieveDrivingTimeReport(new OperationReportsBase()));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Main Index view
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateInput(false)]
        [EcoAuthorize]
        public ActionResult Index(ScoreGlobalReportBase model)
        {
            try
            {
                if (model == null || model.Parameters == null) RedirectToAction("Index");

                using (var business = new DrivingTimeBusiness())
                {
                    return View(business.RetrieveDrivingTimeReport(model.Parameters));
                }
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ExcelReportDownload
        /// </summary>
        /// <returns>File Stream Result</returns>
        public ActionResult ExcelReportDownload(string p)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<OperationReportsBase>(Miscellaneous.Base64Decode(p));

                byte[] report;
                using (var business = new DrivingTimeBusiness())
                {
                    report = business.GenerateScoreGlobalReportExcel(model);
                }
                return new FileStreamResult(new MemoryStream(report), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "ExcelReport.xlsx"
                };
            }
            catch (Exception e)
            {
                ECOsystem.Utilities.Session.SentrySupport(e);

                new EventLogBusiness().AddLogEvent(LogState.ERROR, e);

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Action Result for Print Report
        /// </summary>
        /// <param name="p"></param>
        /// <returns>ActionResult</returns>
        public ActionResult PrintReport(string p)
        {
            var model = JsonConvert.DeserializeObject<OperationReportsBase>(Miscellaneous.Base64Decode(p));

            ViewBag.PrintView = true;
            using (var business = new DrivingTimeBusiness())
            {
                return View("Index", business.RetrieveDrivingTimeReport(model));
            }
        }
    }
}