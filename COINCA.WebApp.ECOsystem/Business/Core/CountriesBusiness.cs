﻿/************************************************************************************************************
*  File    : CountriesBusiness.cs
*  Summary : Countries Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// Countries Business Class
    /// </summary>
    public class CountriesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Countries
        /// </summary>
        /// <param name="countryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Countries> RetrieveCountries(int? countryId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Countries>("[General].[Sp_Countries_Retrieve]",
                    new
                    {
                        CountryId = countryId,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Retrieve Countries Current User
        /// </summary>
        /// <param name="countryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Countries> RetrieveCountriesCurrentUser(int? countryId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Countries>("[General].[Sp_CountriesByUser_Retrieve]",
                    new
                    {
                        CountryId = countryId,
                        LoggedUserId = Session.GetUserInfo().UserId,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Countries
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditCountries(Countries model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Countries_AddOrEdit]",
                    new
                    {
                        model.CountryId,
                        model.Name,
                        model.CurrencyId,
                        model.Code,
                        model.LoggedUserId,
                        model.RowVersion,
                        model.GeopoliticalLevel1,
                        model.GeopoliticalLevel2,
                        model.GeopoliticalLevel3
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model Countries
        /// </summary>
        /// <param name="countryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteCountries(int countryId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Countries_Delete]",
                    new { CountryId = countryId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}