﻿using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

#pragma warning disable CS1591
namespace ECOsystem.Business.Core
{
    public class PartnerSurveyBusiness : IDisposable
    {
        public List<PartnerSurvey> PartnerSurveyRetrieve(int? PartnerId, int? CustomerId = null, int? Id = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerSurvey>("[General].[SP_PartnerSurvey_Retrieve]", new
                {
                    PartnerId,
                    CustomerId, 
                    Id
                }).ToList();
            }
        }

        public void PartnerSurveyAddOrEdit(PartnerSurvey model, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                //Get the questions list based on the modifications made before
                var questions = model.NewQuestions != null && model.NewQuestions.Any() ? model.NewQuestions : model.Questions;
                var QuestionsXML = ECOsystem.Utilities.Miscellaneous.GetXML(questions, typeof(PartnerSurveyQuestion).Name);

                dba.ExecuteNonQuery("[General].[Sp_PartnerSurvey_AddOrEdit]", new
                {
                    model.Id,
                    model.PartnerId,
                    model.Name,
                    model.Description,                   
                    model.TargetId,
                    model.Active,
                    model.Mandatory,
                    model.Delete,
                    QuestionsXML,
                    UserId
                });
            }
        }

        public List<PartnerSurveyQuestionTypes> PartnerSurveyQuestionTypesRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerSurveyQuestionTypes>("[General].[SP_PartnerSurveyQuestionTypes_Retrieve]", new { }).ToList();
            }
        }

        public List<PartnerSurveyTargets> PartnerSurveyTargetsRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerSurveyTargets>("[General].[SP_PartnerSurveyTargets_Retrieve]", new { }).ToList();
            }
        }
                
        public void CustomerAnswersAddOrEdit(PartnerSurvey model, List<PartnerSurveyAnswer> answers, int? CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PartnerSurveyAnswerByCustomer_Add]", new
                {
                    SurveyId = model.Id,
                    model.Elements,
                    AnswersXML = ECOsystem.Utilities.Miscellaneous.GetXML(answers, typeof(PartnerSurveyAnswer).Name),
                    CustomerId,
                    UserId
                });
            }
        }

        public static int? ShowSurveyToCustomer(string Controller, string Action, string Option, int? CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int?>("[General].[Sp_SurveyToCustomer_Retrieve]", new
                {
                    Controller,
                    Action,
                    Option,
                    CustomerId
                });
            }
        }

        public static string GetQuestionTypeToPrint(int Question)
        {
            switch ((QuestionTypes)Question)
            {
                case QuestionTypes.RadioButton:
                    return Resources.PartnerSurvey.RadioButtonPartial;
                case QuestionTypes.CheckBox:
                    return Resources.PartnerSurvey.CheckBoxPartial;
                case QuestionTypes.TextBox:
                    return Resources.PartnerSurvey.TextBoxPartial;
            }
            return string.Empty;
        }

        #region Report
        public List<Customers> PartnerSurveyReportCustomers(int? PartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Customers>("[General].[SP_CustomersByPartnerSurvey_Retrieve]", new
                {
                    PartnerId
                }).ToList();
            }
        }

        public List<PartnerSurvey> PartnerSurveyNameForReport(int? PartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerSurvey>("[General].[SP_PartnerSurveyNamesForReport_Retrieve]", new
                {
                    PartnerId
                }).ToList();
            }
        }        

        List<PartnerSurveyReport> PartnerSurveyReportRetrieve(int? PartnerId, int? SurveyId, int? CustomerId, DateTime StartDate, DateTime EndDate)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerSurveyReport>("[General].[SP_PartnerSurveyReport_Retrieve]", new
                {
                    PartnerId,
                    SurveyId,
                    CustomerId,
                    StartDate,
                    EndDate
                }).ToList();
            }
        }

        public DataSet PartnerSurveyReportDataSet(int? PartnerId, int? SurveyId, int? CustomerId, DateTime StartDate, DateTime EndDate)
        {
            var data = PartnerSurveyReportRetrieve(PartnerId, SurveyId, CustomerId, StartDate, EndDate);
            if (!data.Any())
            {
                return new DataSet();
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(DataTableUtilities.ClassToDataTable(data.Select(x => new
                                                                        {
                                                                            x.Id,
                                                                            x.Name,
                                                                            x.Target,
                                                                            x.Elements, 
                                                                            Date = string.Format("{0} - {1}", StartDate.ToString("dd/MM/yyyy"), EndDate.ToString("dd/MM/yyyy")),
                                                                            Customer = x.CustomerNameDecrypt,
                                                                            SurveyDate = x.Date.ToString("dd/MM/yyyy HH:mm:ss")
                                                                        }).ToList(), "PartnerSurveyReport"));
            ds.Tables.Add(DataTableUtilities.ClassToDataTable(GetDetail(data), "PartnerSurveyDetail"));
            return ds;
        }

        List<PartnerSurveyReportDetail> GetDetail(List<PartnerSurveyReport> data)
        {
            var list = new List<PartnerSurveyReportDetail>();

            foreach (var item in data)
            {
                list.AddRange(item.ReportDetail);
            }
            return list;
        }
        #endregion

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
#pragma warning restore CS1591