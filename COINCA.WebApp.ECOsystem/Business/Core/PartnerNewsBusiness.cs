﻿/************************************************************************************************************
*  File    : PartnersBusiness.cs
*  Summary : Partners Business Methods
*  Author  : Manuel Azofeifa Hidalgo
*  Date    : 11/25/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;

namespace ECOsystem.Business.Core{
#pragma warning disable 1591

    /// <summary>
    /// Partner News Business Tier
    /// </summary>
    public class PartnerNewsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Partners
        /// </summary>
        /// /// <param name="partnerId">Partner Id</param>
        /// <param name="newsId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PartnerNews> RetrieveNewsByPartners(int? partnerId, int? newsId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerNews>("[General].[Sp_NewsByPartners_Retrieve]",
                    new
                    {
                        PartnerId = partnerId,
                        NewsId = newsId,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Retrieve Partners
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PartnerNews> RetrieveAllNewsByPartners(int? partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerNews>("[General].[Sp_NewsByPartnersAll_Retrieve]",
                    new
                    {
                        PartnerId = partnerId
                    });
            }
        }

        /// <summary>
        /// Retrieve Partners
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<PartnerNews> RetrieveActiveNewsByPartners(int? partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerNews>("[General].[Sp_NewsByPartnersActive_Retrieve]",
                    new
                    {
                        PartnerId = partnerId
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Partners News
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditNewsByPartners(PartnerNews model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_NewsByPartners_AddOrEdit]",
                    new
                    {
                        model.NewsId,
                        model.PartnerId,
                        model.Title,
                        model.Content,
                        model.Image,
                        model.IsActive,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model Partners
        /// </summary>    
        /// <param name="newsId"></param>
        public void DeleteNewsByPartners(int newsId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_NewsByPartners_Delete]",
                    new { NewsId = newsId });
            }
        }

        public void AddOrEditImageStation(int? partnerId,byte[] img) 
        {
            var imageStation = Convert.ToBase64String(img);
            imageStation = "data:image/png;base64," + imageStation;
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ImageServiceStations_AddOrEdit]",
                     new {  PartnerId = partnerId  , ImageStation = imageStation   });  
            }  
        }

        public string RetrieveImageStation(int? partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                var response = dba.ExecuteScalar<object>("[General].[Sp_ImageStation_Retrieve]",
                    new {  PartnerId  =  partnerId  });
                return (response == DBNull.Value) ? string.Empty : response.ToString();
            }
        }

        public int? RetrievePartnerId(int userid)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int?>("[General].[Sp_PartnerByUser_Retrieve]",
                     new { UserId = userid });
            }


        }

        /// <summary>
        /// Get the Partner based on the id
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public string RetrievePartnerNameById(int partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[SP_Partners_Name_Retrieve]",
                    new { PartnerId = partnerId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}

