﻿/************************************************************************************************************
*  File    : VehicleCostCentersBusiness.cs
*  Summary : VehicleCostCenters Business Methods
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
//using COINCA.Library.DataAccess;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// VehicleCostCenters Class
    /// </summary>
    public class VehicleCostCentersBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleCostCenters
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        private readonly DataBaseAccess _dba = new DataBaseAccess();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="costCenterId"></param>
        /// <param name="opc"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<VehicleCostCenters> RetrieveVehicleCostCenters(int? costCenterId,Boolean opc=false, string key = null)
        {
            using (_dba)
            {
                var user = Session.GetUserInfo();

                return _dba.ExecuteReader<VehicleCostCenters>("[General].[Sp_VehicleCostCenters_Retrieve]",
                new
                {
                    CostCenterId = costCenterId,
                    CustomerId = Session.GetCustomerId(),
                    Key = key,
                    user.UserId
                });
                
             }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleCostCenters
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleCostCenters(VehicleCostCenters model)
        {
            using (_dba)
            {
                _dba.ExecuteNonQuery("[General].[Sp_VehicleCostCenters_AddOrEdit]",
                    new
                    {
                        model.CostCenterId,
                        model.Name,
                        model.Code,
                        model.UnitId,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleCostCenters
        /// </summary>
        /// <param name="costCenterId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehicleCostCenters(int costCenterId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleCostCenters_Delete]",
                    new { CostCenterId = costCenterId,
                          ModifyUserId = Session.GetUserInfo().UserId
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}