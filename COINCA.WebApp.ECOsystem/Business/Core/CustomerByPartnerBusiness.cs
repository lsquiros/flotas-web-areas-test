﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.DataAccess;
using ECOsystem.Utilities;
using ECOsystem.Models.Core;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomerByPartnerBusiness : IDisposable
    {
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>


        /// <summary>
        /// Roles for Permissions
        /// </summary>
        /// <returns></returns>
        public static SelectList RetrieveCustomerbyPartners()
        {
            //var user = ECOsystem.Utilities.Session.GetUserInfo();
            var partner = Session.GetPartnerInfo();
            var dictionary = new Dictionary<int, string>();
            using (var business = new CustomerByPartnerBusiness())
            {
                var clientes = business.CustomersRetrieve(partner.PartnerId);

                foreach (var c in clientes)
                {
                    dictionary.Add(c.CustomerId, c.DecryptedCustomerName);
                }

                return new SelectList(dictionary, "key", "value");
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="PartnerId"></param>
        /// <returns></returns>
        public IEnumerable<CustomerByPartner> CustomersRetrieve(int? PartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerByPartner>("[dbo].[Sp_ASPNetCustomers_Retrieve]",
                    new
                    {
                        PartnerId
                    });
            }
        }


        /// <summary>
        /// Retrieve the Customers
        /// </summary>
        /// <param name="PartnerUserId"></param>
        /// <returns></returns>
        public static IEnumerable<CustomerByPartner> CustomersByPartner_Retrieve(int PartnerUserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerByPartner>("[dbo].[Sp_ASPNetCustomersByPartner_Retrieve]", new
                {
                    PartnerUserId
                });
            }
        }

        /// <summary>
        /// Save the customer the partner will have access to
        /// </summary>
        /// <param name="Customers"></param>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        public void CustomersByPartner_AddOrEdit(List<int> Customers, int UserId, string RoleId, int PartnerId)
        {
            var Role = UserRolesPermissionsBusiness.RetrieveRolesByUsers(UserId).FirstOrDefault();
            if (Customers == null)
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[dbo].[Sp_ASPNetCustomersByPartner_Update]",
                    new
                    {
                        UserId,
                        CustomerId = 0,
                        Role.RoleId,
                        Delete = 1,
                        RoleForUser = RoleId,
                        PartnerIdParam = PartnerId
                    });
                }
            }

            else
            {

                int Delete = 1; //the first time it will save the flag goes as 1

                foreach (var UId in Customers)
                {
                    using (var dba = new DataBaseAccess())
                    {
                        dba.ExecuteNonQuery("[dbo].[Sp_ASPNetCustomersByPartner_Update]",
                        new
                        {
                            UserId,
                            CustomerId = UId,
                            Role.RoleId,
                            Delete,
                            RoleForUser = RoleId,
                            PartnerIdParam = PartnerId
                        });
                    }
                    Delete = 0;
                }
            }
        }

        /// <summary>
        /// Retrieve the partners in the database 
        /// </summary>
        /// <param name="PartnerUserId"></param>
        /// <returns></returns>
        public static IEnumerable<Partners> PartnerAuthByPartnerRetrieve(int PartnerUserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Partners>("[dbo].[Sp_ASPNetPartnersByPartner_Retrieve]", new
                {
                    PartnerUserId
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Partners"></param>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>

        public void PartnersByPartner_AddOrEdit(List<int> Partners, int UserId, string RoleId)
        {
            var Role = UserRolesPermissionsBusiness.RetrieveRolesByUsers(UserId).FirstOrDefault();

            if (Partners == null)
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[dbo].[Sp_ASPNetPartnersByPartner_AddOrEdit]",
                    new
                    {
                        UserId,
                        PartnerId = 0,
                        Role.RoleId,
                        Delete = 1,
                        RoleForUser = RoleId
                    });
                }
            }
            else
            {

                int Delete = 1; //the first time it will save the flag goes as 1

                foreach (var UId in Partners)
                {
                    using (var dba = new DataBaseAccess())
                    {
                        dba.ExecuteNonQuery("[dbo].[Sp_ASPNetPartnersByPartner_AddOrEdit]",
                        new
                        {
                            UserId,
                            PartnerId = UId,
                            Role.RoleId,
                            Delete,
                            RoleForUser = RoleId
                        });
                    }
                    Delete = 0;
                }
            }
        }

        public static IEnumerable<ServiceStations> ServiceStationsByPartner_Retrieve(int UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ServiceStations>("[General].[Sp_ServiceStationUserInfo_Retrieve]", new
                {
                    UserId
                });
            }
        }


        public void ServiceStationsByPartner_AddOrEdit(List<int> stationslist, int UserId, int PartnerId)
        {

            if (stationslist == null)
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[dbo].[Sp_ServiceStationsUserInfoByPartner_Update]",
                    new
                    {
                        UserId,
                        Delete = 1,
                    });
                }
            }
            else
            {
                int Delete = 1; //the first time it will save the flag goes as 1              
                foreach (int ServiceStationId in stationslist)
                {
                    using (var dba = new DataBaseAccess())
                    {
                        dba.ExecuteNonQuery("[dbo].[Sp_ServiceStationsUserInfoByPartner_Update]",
                        new
                        {
                            UserId,
                            ServiceStationId,
                            Delete,
                            CustomerId = PartnerId
                        });
                    }
                    Delete = 0;
                }
            }
        }

        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }


    }
}