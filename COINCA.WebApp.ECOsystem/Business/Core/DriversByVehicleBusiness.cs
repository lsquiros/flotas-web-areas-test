﻿/************************************************************************************************************
*  File    : DriversByVehicleBusiness.cs
*  Summary : DriversByVehicle Business Methods
*  Author  : Gerald Solano
*  Date    : 21/07/2015
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Models.Administration;
using ECOsystem.Utilities;
//using COINCA.Library.DataAccess;

namespace ECOsystem.Business.Administration
{
    /// <summary>
    /// DriversByVehicle Class
    /// </summary>
    public class DriversByVehicleBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve  DriversByVehicle
        /// </summary>
        /// <param name="vehicleId">The vehicle group Id to find all vehicles associated</param>
        /// <returns>An IEnumerable of T, T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public DriversByVehicleData RetrieveDriversByVehicle(int? vehicleId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = new DriversByVehicleData();
                var aux = dba.ExecuteReader<DriversByVehicle>("[General].[Sp_DriversByVehicle_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId,
                        CustomerId= Session.GetCustomerId()
                    });
                
                result.AvailableDriversList = aux.Where(x => x.VehicleId == null);
                result.ConnectedDriversList = aux.Where(x => x.VehicleId != null);
                result.VehicleId = vehicleId;

                return result;
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity DriversByVehicle
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditDriversByVehicle(DriversByVehicleData model)
        {
            
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            var i = 0;
            foreach (var item in model.ConnectedDriversList)
            {
                root.Add(new XElement("Driver", new XAttribute("UserId", item.UserId), new XAttribute("Index", ++i)));
            }

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_DriversByVehicle_AddOrEdit]",
                    new
                    {
                        model.VehicleId,
                        XmlData = doc.ToString(),
                        LoggedUserId = Session.GetUserInfo().UserId,
                        CustomerId = Session.GetCustomerId()
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model DriversByVehicle
        /// </summary>
        public void DeleteDriversByVehicle(int driverId)
        {
            using (var dba = new DataBaseAccess())
            {
                //dba.ExecuteNonQuery("[General].[Sp_VehiclesByGroup_Delete]",
                //    new { DriverId = driverId });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}