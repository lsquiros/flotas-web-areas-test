﻿using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class AlertsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve all the alerts
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Alarms> AlertsRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Alarms>("[General].[Sp_Alerts_Retrieve]", 
                new 
                {
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                });
            }            
        }

        /// <summary>
        /// Add or Edit Alerts
        /// </summary>
        /// <param name="List"></param>
        /// <returns></returns>
        public bool AlertsAddOrEdit(List<Alarms> List)
        {
            try
            {
                bool FirstRecord = true;

                using (var dba = new DataBaseAccess())
                {
                    foreach (var item in List)
                    {
                        dba.ExecuteNonQuery("[General].[Sp_Alerts_AddOrEdit]",
                        new
                        {
                            AlertId = item.AlarmId,
                            CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                            ParameterValue = item.ParameterValue,
                            IsActive = item.IsActive, 
                            UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId,
                            FirstRecord
                        });
                        FirstRecord = false; 
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Retrieve all the alerts with the counts
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Alarms> AlertsCountRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Alarms>("[General].[Sp_AlertsCount_Retrieve]",
                new
                {
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        /// <summary>
        /// GC
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}