﻿/************************************************************************************************************
*  File    : CustomersBusiness.cs
*  Summary : Customers Business Methods
*  Author  : Berman Romero
*  Date    : 09/15/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ECOsystem.Business.Utilities;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using ECOsystem.Models.Account;
using ECOsystem.Models;
using Microsoft.AspNet.Identity;
using System.Data;
using ECOsystem.Models.Miscellaneous;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// Customers Business Class
    /// </summary>
    public class CustomersBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Customers
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="countryId"></param>
        /// <param name="key"></param>
        /// <param name="LoadSingleCustomer"></param>
        /// <param name="statusId"></param>
        /// <param name="queryWithOutPartner"></param>
        /// <returns></returns>
        public IEnumerable<Customers> RetrieveCustomers(int? customerId, int? countryId = null, string key = null, bool LoadSingleCustomer = false, int statusId = 2, bool queryWithOutPartner = false)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                IList<Customers> result;
                if (countryId != null)
                {
                    result = dba.ExecuteReader<Customers>("[General].[Sp_Customers_Retrieve]",
                        new
                        {
                            CustomerId = customerId,
                            CountryId = countryId,
                            Key = key,
                            PartnerId = (!queryWithOutPartner) ? Session.GetPartnerId() : null,
                            LoadSingleCustomer
                        });

                    //Validate flags
                    result = (result.Count > 0) ? result.Where(w => w.IsActive && w.IsDeleted == false).ToList() : result;

                    //Validate Available Modules
                    foreach (var item in result)
                    {
                        if (item != null && item.AvailableModules != null)
                        {
                            if (item.AvailableModules.Contains("E")) { item.ModuleEfficiency = true; } else { item.ModuleEfficiency = false; }
                            if (item.AvailableModules.Contains("C")) { item.ModuleControl = true; } else { item.ModuleControl = false; }
                            if (item.AvailableModules.Contains("O")) { item.ModuleOperation = true; } else { item.ModuleOperation = false; }
                            if (item.AvailableModules.Contains("G")) { item.ModuleManagement = true; } else { item.ModuleManagement = false; }
                        }
                    }

                    var finalResult2 = result.ToList();

                    var searchitems2 = string.IsNullOrEmpty(key) ? finalResult2 : finalResult2.Where(x =>
                    Regex.IsMatch(((string.IsNullOrEmpty(x.DecryptedName)) ? "" : x.DecryptedName.ToUpper()), key.ToUpper(), RegexOptions.IgnoreCase) ||
                    Regex.IsMatch(((string.IsNullOrEmpty(x.BusinessName)) ? "" : x.BusinessName.ToUpper()), key.ToUpper(), RegexOptions.IgnoreCase) ||
                    Regex.IsMatch(((string.IsNullOrEmpty(x.ContactName)) ? "" : x.ContactName.ToUpper()), key.ToUpper(), RegexOptions.IgnoreCase)).ToList();

                    if (searchitems2 != null && searchitems2.Count() > 0)
                        finalResult2 = searchitems2;

                    return finalResult2;
                }

                var country = GeneralCollections.GetCountriesAccess;
                IList<int?> countriesList = new List<int?>();
                if (country != null)
                {
                    countriesList = country.Select(x => (int?)int.Parse(x.Value)).ToList();
                }
                result = dba.ExecuteReader<Customers>("[General].[Sp_Customers_Retrieve]",
                    new
                    {
                        CustomerId = customerId,
                        Key = key,
                        PartnerId = (!queryWithOutPartner) ? Session.GetPartnerId() : null,
                        LoadSingleCustomer

                    });//.Where(x => countriesList.Contains(x.CountryId)).ToList();

                //Validate Available Modules
                foreach (var item in result)
                {
                    if (item != null && item.AvailableModules != null)
                    {
                        if (item.AvailableModules.Contains("E")) { item.ModuleEfficiency = true; } else { item.ModuleEfficiency = false; }
                        if (item.AvailableModules.Contains("C")) { item.ModuleControl = true; } else { item.ModuleControl = false; }
                        if (item.AvailableModules.Contains("O")) { item.ModuleOperation = true; } else { item.ModuleOperation = false; }
                        if (item.AvailableModules.Contains("G")) { item.ModuleManagement = true; } else { item.ModuleManagement = false; }
                    }
                }

                //Validate flags
                result = (result.Count > 0) ? result.Where(w => w.IsDeleted == false).ToList() : result;

                //Validate status
                result = (statusId == 2) ? result : result.Where(w => w.IsActive == ((statusId == 1) ? true : false)).ToList();

                var finalResult = result.ToList();

                var searchitems = string.IsNullOrEmpty(key) ? finalResult : finalResult.Where(x =>
                    Regex.IsMatch(((string.IsNullOrEmpty(x.DecryptedName)) ? "" : x.DecryptedName.ToUpper()), key.ToUpper(), RegexOptions.IgnoreCase) ||
                    Regex.IsMatch(((string.IsNullOrEmpty(x.BusinessName)) ? "" : x.BusinessName.ToUpper()), key.ToUpper(), RegexOptions.IgnoreCase) ||
                    Regex.IsMatch(((string.IsNullOrEmpty(x.ContactName)) ? "" : x.ContactName.ToUpper()), key.ToUpper(), RegexOptions.IgnoreCase)).ToList();

                if (key != null && key != "")
                    finalResult = searchitems;

                finalResult = finalResult.OrderBy(q => q.DecryptedName).ToList();
                return finalResult;
            }
        }

        /// <summary>
        /// Search Customers from Partner
        /// </summary>
        /// <param name="pageNum"></param>
        /// <param name="pageSize"></param>
        /// <param name="key"></param>
        /// <param name="statusId"></param>
        /// <param name="filterType"></param>
        /// <returns></returns>
        public IEnumerable<Customers> SearchCustomersFromPartner(int? pageNum, int? pageSize, string key = null, int statusId = 2, int filterType = 0)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                var result = dba.ExecuteReader<Customers>("[General].[Sp_Search_Customers]",
                    new
                    {
                        Key = key,
                        PartnerId = Session.GetPartnerId(),
                        Status = statusId,
                        filterType = filterType,
                        PageNo = pageNum,
                        PageSize = pageSize
                    });

                // Si el filtro es por nombre de cliente o correo se solicita obtener todos los clientes, 
                // debido a que la busqueda se realiza por código (debido a la encriptación)
                if (filterType == 1 || filterType == 2)
                {
                    var searchitems =
                        string.IsNullOrEmpty(key) ? result :
                            (
                               (filterType == 1) ?
                                 result.Where(x =>
                                        Regex.IsMatch(((string.IsNullOrEmpty(x.DecryptedName)) ? "" : x.DecryptedName.ToUpper()), key.ToUpper(), RegexOptions.IgnoreCase))
                                    : ((filterType == 2) ?
                                            result.Where(x =>
                                                Regex.IsMatch(((string.IsNullOrEmpty(x.DecryptedPrimaryEmail)) ? "" : x.DecryptedPrimaryEmail.ToUpper()), key.ToUpper(), RegexOptions.IgnoreCase))
                                        :
                                          result));

                    result = (searchitems != null && searchitems.Count() > 0) ? searchitems.ToList() : new List<Customers>();
                }
                return result;
            }
        }

        /// <summary>
        /// Get Partners For Trasladate Customers
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Partners> GetPartnersForTrasladateCustomers()
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<Partners>("[General].[Sp_GetPartnersForTrasladateCustomers]",
                   new
                   {
                       PartnerId = Session.GetPartnerId()
                   });
            }
        }


        /// <summary>
        /// Retrieve Customers
        /// </summary>
        /// <param name="customerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="countryId">countryId for filter  (FK)</param>
        /// <returns>Customers Edit Base</returns>
        public CustomersEditBase RetrieveCustomersEdit(int? customerId, int? countryId = null)
        {
            if (customerId == -1)
            {
                var result = new CustomersEditBase
                {
                    Data = new Customers(),
                    List = new List<CustomerCreditCards>(),
                    Card = new CustomerCreditCards(),
                    CardRequest = new CardRequest(),
                    RequestList = new List<CardRequest>(),
                    CustomerBudget = new CustomerBudget() { MasterCards = new List<CustomerCreditCards>() },
                    AddInfo = new CustomerAdditionalInformation(),
                    Menus = new List<AccountMenus>(),
                    Vehicles = new List<Vehicles>(),
                    Vehicle = new Vehicles(),
                    ContractInformation = new CustomerContractInformation(),
                    ContractsInformation = new List<CustomerContractInformation>(),
                    CustomerBinnacleInformation = new CustomerBinnacleBase()
                };
                return result;
            }
            else
            {
                var result = new CustomersEditBase
                {
                    Data = RetrieveCustomers(customerId, countryId, LoadSingleCustomer: true).FirstOrDefault(),
                    List = RetrieveCustomerCreditCards(customerId, null),
                    Card = new CustomerCreditCards(),
                    CardRequest = new CardRequest(),
                    RequestList = RetrieveCustomerCardRequest(customerId, null),
                    CustomerBudget = RetrieveBudgetClosing(customerId).FirstOrDefault(),
                    AddInfo = new CustomerAdditionalInformation(),
                    Menus = new List<AccountMenus>(),
                    Vehicles = RetrieveVehiclesByCustomer(customerId).ToList(),
                    ContractInformation = new CustomerContractInformation(),
                    ContractsInformation = RetrieveContractInformation(customerId).ToList(),
                    CustomerBinnacleInformation = new CustomerBinnacleBase() { List = AddOrRetrieveCustomerBinnacle(new CustomerBinnacle() { CustomerId = (int)customerId }) }
                };

                var productType = Session.GetProductInfoByPartner((int)Session.GetPartnerId());
                result.ContractInformation.IsProductLineVitaECO = productType == ProductInfo.LineVitaECO;
                result.ContractsInformation.ForEach(c => { c.IsProductLineVitaECO = productType == ProductInfo.LineVitaECO; });

                result.DataTerminal.TerminalList = RetrieveCustomerTerminals(customerId);
                result.CustomerBudget = result.CustomerBudget ?? new CustomerBudget();
                result.CustomerBudget.MasterCards = result.List.ToList();
                if (Session.GetPartnerInfo().HasPayments && result.CustomerBudget.MasterCards.Count() > 0)
                {
                    result.CustomerBudget.MasterCards.Where(x => !x.InternationalCard).FirstOrDefault().Payments = RetrieveCustomerPayments(new Filters(), customerId).ToList();
                }                
                result.CustomerBudget.UnitOfCapacityId = (result.Data != null) ? result.Data.UnitOfCapacityId : 0;
                result.CustomerBudget.NameDate = result.CustomerBudget.Periodicity == 30 ? result.CustomerBudget.NameDay : 1;

                if (result.Data != null && result.Data.AvailableModules != null)
                {
                    if (result.Data.AvailableModules.Contains("E")) { result.Data.ModuleEfficiency = true; } else { result.Data.ModuleEfficiency = false; }
                    if (result.Data.AvailableModules.Contains("C")) { result.Data.ModuleControl = true; } else { result.Data.ModuleControl = false; }
                    if (result.Data.AvailableModules.Contains("O")) { result.Data.ModuleOperation = true; } else { result.Data.ModuleOperation = false; }
                    if (result.Data.AvailableModules.Contains("G")) { result.Data.ModuleManagement = true; } else { result.Data.ModuleManagement = false; }
                }
                HttpContext.Current.Session["CustomerIDTerminal"] = customerId;
                using (var business = new UsersBusiness())
                {
                    result.UserList = business.RetrieveUsers(null, customerId: customerId);
                }
                return result;
            }
        }

        /// <summary>
        /// Retrieve Contract Information
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="ContractId"></param>
        /// <param name="IsReport"></param>
        /// <returns></returns>
        public List<CustomerContractInformation> RetrieveContractInformation(int? CustomerId, int? ContractId = null, bool? IsReport = null)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<CustomerContractInformation>("[General].[Sp_CustomerContracts_Retrieve]",
                    new
                    {
                        CustomerId,
                        ContractId,
                        IsReport
                    }).ToList();
            }
        }

        /// <summary>
        /// Retrieve Terminals
        /// </summary>
        /// <param name="customerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CustomerTerminals> RetrieveCustomerTerminals(int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerTerminals>("[General].[Sp_CustomerTerminal_Retrieve]",
                    new
                    {
                        CustomerId = customerId
                    });
            }
        }

        /// <summary>
        /// Retrieve Customers
        /// </summary>
        /// <param name="customerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="customerCreditCardId">customer CreditCard Id</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CustomerCreditCards> RetrieveCustomerCreditCards(int? customerId, int? customerCreditCardId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerCreditCards>("[General].[Sp_CustomerCreditCards_Retrieve]",
                    new
                    {
                        CustomerId = customerId,
                        CustomerCreditCardId = customerCreditCardId
                    });
            }
        }
        

        /// <summary>
        /// Retrieve Active Customers Count
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="customerId"></param>
        /// <param name="countryId"></param>
        public int RetrieveActiveCustomersCount(int? partnerId, int? customerId, int? countryId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Insurance].[Sp_ActiveCustomersCount_Retrieve]",
                    new
                    {
                        PartnerId = partnerId,
                        CustomerId = customerId,
                        CountryId = countryId
                    });
            }
        }

        /// <summary>
        /// Retrieve Active Customers Count
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="customerId"></param>
        /// <param name="countryId"></param>
        public int RetrieveNotActiveCustomersCount(int? partnerId, int? customerId, int? countryId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Insurance].[Sp_NotActiveCustomersCount_Retrieve]",
                    new
                    {
                        PartnerId = partnerId,
                        CustomerId = customerId,
                        CountryId = countryId
                    });
            }
        }

        /// <summary>
        /// Retrieve Customers by Partner
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Customers> RetrieveCustomersByPartners(int? partnerId)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<Customers>("[General].[Sp_CustomersByPartners_Retrieve]",
                    new
                    {
                        PartnerId = partnerId
                    });
            }
        }

        /// <summary>
        /// Retrieve Customers Names for Collection by Partner
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Customers> RetrieveCustomersCollectionByPartners(int? partnerId)
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<Customers>("[General].[Sp_CustomersByPartners_CollectionRetrieve]",
                    new
                    {
                        PartnerId = partnerId
                    });
            }
        }

        /// <summary>
        /// Retrieve Customers
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Customers> RetrieveAllCustomers()
        {
            using (var dba = new DataBaseAccess("ConnectionRead"))
            {
                return dba.ExecuteReader<Customers>("[General].[Sp_AllCustomers_Retrieve]",
                    new
                    {

                    });
            }
        }

        /// <summary>
        /// Retrieve Customers
        /// </summary>
        /// <param name="customerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="cardRequestId">customer cardRequestId</param>
        /// <param name="pendingOnly">pendingOnly</param>
        /// <param name="key"></param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CardRequest> RetrieveCustomerCardRequest(int? customerId, int? cardRequestId, bool? pendingOnly = null, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CardRequest>("[Control].[Sp_CardRequest_Retrieve]",
                    new
                    {
                        CustomerId = customerId,
                        CardRequestId = cardRequestId,
                        PendingOnly = pendingOnly,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Validate the plate number for a new credit card
        /// </summary>
        /// <param name="plateId"></param>
        /// <returns>Valid or invalid</returns>
        public string ValidatePlate_CreditCard(string plateId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[Control].[Sp_ValidatePlate_CreditCard]",
                    new
                    {
                        PlateId = plateId
                    });
            }
        }


        public IEnumerable<CustomerBudget> RetrieveBudgetClosing(int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerBudget>("[Control].[Sp_Budget_Closing_Retrieve]",
                    new
                    {
                        CustomerId = customerId
                    });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public void AddOrEditBudgetClosingPeriodicity(CustomerBudget model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_Budget_Closing_AddOrEdit]",
                    new
                    {
                        model.CustomerId,
                        model.Periodicity,
                        model.NameDay,
                        model.Repeat,
                        InsertUserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }


        /// <summary>
        /// Retrieve Customers
        /// </summary>
        /// <param name="customerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="cardRequestId">customer cardRequestId</param>
        /// <param name="pendingOnly">pendingOnly</param>
        /// <param name="key"></param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CardRequest> RetrieveCustomerCardRequestNoCustomer(int customerId, int? cardRequestId, bool? pendingOnly = null, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CardRequest>("[Control].[Sp_CardRequestNoCustomer_Retrieve]",
                    new
                    {
                        CustomerId = customerId,
                        CardRequestId = cardRequestId,
                        PendingOnly = pendingOnly,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CustomerCreditCards
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditCustomerCreditCards(CustomerCreditCards model)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[General].[Sp_CustomerCreditCards_AddOrEdit]",
                        new
                        {
                            model.CustomerCreditCardId,
                            model.CustomerId,
                            model.CreditCardHolder,
                            model.CreditCardNumber,
                            model.ExpirationYear,
                            model.ExpirationMonth,
                            model.CreditLimit,
                            model.StatusId,
                            model.LoggedUserId,
                            model.InternationalCard
                        });
                }
            }
            catch (Exception exception)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, exception);
                throw exception;
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CustomersByPartner
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditCustomersByPartner(CustomerByPartnerData model)
        {
            DeleteCustomersByPartners(model.PartnerId);
            using (var dba = new DataBaseAccess())
            {
                foreach (Customers customer in model.ConnectedCustomersList)
                {
                    dba.ExecuteNonQuery("[General].[Sp_CustomersByPartners_AddOrEdit]",
                        new
                        {
                            customer.CustomerId,
                            model.PartnerId,
                            model.LoggedUserId
                        });
                }
            }

        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CustomerCreditCards
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditCardRequest(CardRequest model)
        {
            using (var dba = new DataBaseAccess())
            {
                var DriverUserId = model.DriverUserId == 0 ? (int?)null : model.DriverUserId;
                dba.ExecuteNonQuery("[Control].[Sp_CardRequest_AddOrEdit]",
                new
                {
                    model.CardRequestId,
                    model.CustomerId,
                    model.PlateId,
                    model.DriverName,
                    model.DriverIdentification,
                    model.StateId,
                    model.CountyId,
                    model.CityId,
                    model.DeliveryState,
                    model.DeliveryCounty,
                    model.DeliveryCity,
                    model.AddressLine1,
                    model.AddressLine2,
                    model.PaymentReference,
                    model.AuthorizedPerson,
                    model.ContactPhone,
                    model.EstimatedDelivery,
                    model.LoggedUserId,
                    DriverUserId,
                    model.RowVersion,
                    PaymentMethod = model.PaymentMethod
                });
            }
        }


        /// <summary>
        /// Remove credit card requests
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void RemoveCardRequest(CardRequest model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_CardRequest_Remove]",
                new
                {
                    model.CardRequestId,
                    model.LoggedUserId
                });
            }
        }


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity CustomerCreditCards
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditCustomerTerminal(CustomerTerminals model)
        {
            try
            {
                int Custome = Convert.ToInt32(HttpContext.Current.Session["CustomerIDTerminal"]);
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[General].[Sp_CustomerTerminal_AddOrEdit]",
                        new
                        {
                            //model.CustomerId,
                            CustomerId = Custome,
                            model.TerminalId,
                            model.MerchantDescription
                        });
                }
            }
            catch (Exception exception)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, exception);
                throw exception;
            }

        }


        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Customers
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <param name="userManager">userManager</param>
        /// <param name="url">url</param>
        /// <param name="request">request</param>
        public void AddOrEditCustomers(Customers model, ApplicationUserManager userManager, UrlHelper url, HttpRequestBase request)
        {
            int customerId;
            using (var dba = new DataBaseAccess())
            {
                //Flag indicate if the current customer is new
                bool isNewCustomer = true;

                //Flag for trigger the logic process to add user
                bool addUser = true;

                // Get product type
                var productType = Session.GetProductInfoByPartner((int)Session.GetPartnerId());
              
                //Validate if email is empty
                if (!string.IsNullOrEmpty(model.Email))
                {
                    //Validate Email
                    model.Email = Regex.Replace(model.Email, @"\s", "").ToLower();

                    //Validate customer user
                    var user = userManager.FindByName(TripleDesEncryption.Encrypt(model.Email));
                    if (user != null && model.CustomerId == null)
                    {
                        var message = string.Format("[VALIDATE_USER_FROM_PARTNER] Usuario {0} ya existe en plataforma.", model.Email);

                        using (var userbusiness = new UsersBusiness())
                        {
                            var customerByUser = GetCustomerIdFromEmail(model.Email);
                            if (!string.IsNullOrEmpty(customerByUser))
                                message += "|" + customerByUser;
                        }

                        throw new Exception(message);
                    }
                    else if (model.CustomerId > 0)
                    {
                        var referenceCustomerId = GetCustomerIdFromEmail(model.Email);

                        addUser = (!string.IsNullOrEmpty(referenceCustomerId) && !referenceCustomerId.Equals(model.CustomerId.ToString())) ? true : false;

                        if (user != null && addUser)
                        {
                            var message = string.Format("[VALIDATE_USER_FROM_PARTNER] Usuario {0} ya existe en plataforma.", model.Email);
                            new EventLogBusiness().AddLogEvent(LogState.WARNING, message);
                            addUser = false;

                            using (var userbusiness = new UsersBusiness())
                            {
                                var customerByUser = GetCustomerIdFromEmail(model.Email);
                                if (!string.IsNullOrEmpty(customerByUser))
                                    message += "|" + customerByUser;
                            }

                            throw new Exception(message);
                        }
                        else if (user == null)
                        {
                            addUser = true;
                        }

                    }
                }
                else
                {
                    addUser = false;
                }
                

                //model.CostCardAdmin = Miscellaneous.ConvertCurrencyWithCulture("es-CR", model.CostCardAdminStr);

                model.AvailableModules =
                    ((model.ModuleEfficiency) ? "E" : "") +
                    ((model.ModuleControl) ? "C" : "") +
                    ((model.ModuleOperation) ? "O" : "") +
                    ((model.ModuleManagement) ? "G" : "");

                customerId = dba.ExecuteScalar<int>("[General].[Sp_Customers_AddOrEdit]",
                    new
                    {
                        model.CustomerId,
                        Name = model.EncryptedName,
                        //model.FinancialPartnerId,
                        model.CurrencyId,
                        model.Logo,
                        model.IsActive,
                        model.AccountNumber,
                        model.UnitOfCapacityId,
                        model.IssueForId,
                        model.CreditCardType,
                        model.AvailableModules,
                        //model.InsurancePartnerId,
                        //model.CostCardAdmin,
                        model.LoggedUserId,
                        model.RowVersion,
                        PartnerId = Session.GetPartnerId(),
                        model.TransactionsOffline,
                        model.isDemo,
                        IsDemoAlert = ECOsystem.Utilities.Miscellaneous.SetDate(model.IsDemoAlertStr),
                        model.CustomerManagerId,
                        model.SMSAlarms,
                        model.CustomerCharges,
                        model.DecryptedName,
                        model.CostCenterFuelDistribution,
                        model.SapCode,
                        model.FuelPermit,
                        model.FuelMargin,
                        model.HasFleetio
                    });

                if (model.CustomerId > 0)
                {
                    isNewCustomer = false;
                }
                else
                {
                    model.CustomerId = customerId;
                }

                AddOrEditAdditionalInfo(model, customerId);

                // Add User By Customer
                if (addUser)
                    AddUserByCustomer(productType, Session.GetPartnerId(), userManager, model);

                //Restore customer id in object
                model.CustomerId = (isNewCustomer) ? null : model.CustomerId;
            }
        }

        /// <summary>
        /// Add User By Customer
        /// </summary>
        /// <param name="productType"></param>
        /// <param name="partnerId"></param>
        /// <param name="userManager"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool AddUserByCustomer(ProductInfo productType, int? partnerId, ApplicationUserManager userManager, Customers model)
        {
            using (var dba = new DataBaseAccess())
            {
                if (productType == ProductInfo.LineVitaECO)
                {
                    int userId = 0;
                    var partnerInfo = dba.ExecuteReader<ControlCarPartnerInfo>("[General].[Sp_PartnerLineVita_Retrieve]", new { }).FirstOrDefault(); // POR PARAMETRO

                    var usermodel = new Users
                    {
                        CustomerUser = new CustomerUsers(),
                        DriverUser = new DriversUsers(),
                        IsCustomerUser = true,
                        IsDriverUser = true,
                        CustomerId = model.CustomerId,
                        DecryptedEmail = model.Email,
                        DecryptedName = model.DecryptedName,
                        RoleName = partnerInfo.RoleName,
                        IsActive = true,
                        ChangePassword = true,
                        IsLockedOut = false
                    };
                    usermodel.DecryptedIdentification = (string.IsNullOrEmpty(model.AdminIdentification)) ? "0-0000-0000" : model.AdminIdentification;
                    usermodel.LicenseExpiration = DateTime.Now.AddYears(2);
                    usermodel.CustomerUser.DecryptedIdentification = (string.IsNullOrEmpty(model.AdminIdentification)) ? "0-0000-0000" : model.AdminIdentification;
                    usermodel.CustomerUser.CustomerId = model.CustomerId;
                    usermodel.DriverUser.DecryptedIdentification = (string.IsNullOrEmpty(model.AdminIdentification)) ? "0-0000-0000" : model.AdminIdentification;
                    usermodel.DriverUser.CustomerId = (int)model.CustomerId;
                    usermodel.DriverUser.DecryptedIdentification = "0-0000-0000";
                    usermodel.DriverUser.DecryptedLicense = "00000";
                    usermodel.DriverUser.DailyTransactionLimit = 5;

                    using (var business = new UsersBusiness())
                    {
                        business.AddOrEditUsersWithOutCallback(usermodel, userManager, ref userId);

                        business.AddVehicleReferencesToCustomerUsers((int)model.CustomerId);
                    }
                }

            }

            return true;
        }

        /// <summary>
        /// Performs the maintenance Update of the entity Customers
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void EditCustomersPrices(Customers model)
        {
            int customerId;
            using (var dba = new DataBaseAccess())
            {
                //Format currency value to numeric value
                model.CostCardAdmin = ECOsystem.Utilities.Miscellaneous.ConvertCurrencyWithCulture("es-CR", model.CostCardAdminStr);
                model.CostCardEmision = ECOsystem.Utilities.Miscellaneous.ConvertCurrencyWithCulture("es-CR", model.CostCardEmisionStr);
                model.CostModuleEO = ECOsystem.Utilities.Miscellaneous.ConvertCurrencyWithCulture("es-CR", model.CostModuleEOStr);
                model.CostRentedGPS = ECOsystem.Utilities.Miscellaneous.ConvertCurrencyWithCulture("es-CR", model.CostRentedGPSStr);
                model.CostRentedBAC = ECOsystem.Utilities.Miscellaneous.ConvertCurrencyWithCulture("es-CR", model.CostRentedBACStr);
                model.MembershipFee = ECOsystem.Utilities.Miscellaneous.ConvertCurrencyWithCulture("es-CR", model.MembershipFeeStr);
                model.CostCardReposition = ECOsystem.Utilities.Miscellaneous.ConvertCurrencyWithCulture("es-CR", model.CostCardRepositionStr);
                // Send update data
                customerId = dba.ExecuteScalar<int>("[General].[Sp_CustomersPrices_AddOrEdit]",
                    new
                    {
                        model.CustomerId,
                        model.CostCardAdmin,
                        model.CostCardEmision,
                        model.CostModuleEO,
                        model.GPSModality,
                        model.CostRentedGPS,
                        model.CostRentedBAC,
                        model.LoggedUserId,
                        model.RowVersion,
                        model.MembershipFee,
                        model.CostCardReposition,
                        model.ManagementPricesXml,
                        model.SMSPricesXml
                    });
            }
        }


        /// <summary>
        /// Performs the Insert of the entity Customers via API
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <returns>CustomerId created after insert</returns>
        public int AddCustomerFromApi(CustomersApi model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_Customers_AddFromAPI]",
                    new
                    {
                        Name = model.CustomerName,
                        model.PartnerId,
                        model.CurrencyCode,
                        AccountNumber = model.CustomerAccountNumber,
                        MasterCreditCard = model.CustomerCreditCard,
                        model.LoggedUserId
                    });
            }
        }


        /// <summary>
        /// Performs the the operation of delete on the model Customers
        /// </summary>
        /// <param name="customerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteCustomers(int customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Customers_Delete]",
                    new
                    {
                        CustomerId = customerId,
                        PartnerId = Session.GetPartnerId()
                    });

            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model CardRequest
        /// </summary>
        /// <param name="cardRequestId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteCustomerCardRequest(int cardRequestId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_Customers_CardRequest]",
                    new { CardRequestId = cardRequestId });
            }
        }


        /// <summary>
        /// Performs the the operation of delete on the model CustomerTerminal
        /// </summary>
        public void DeleteCustomerTerminal(string terminalId)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                    dba.ExecuteNonQuery("[General].[Sp_CustomerTerminal_Delete]", new { TerminalId = terminalId });

            }
            catch (Exception exception)
            {
                new EventLogBusiness().AddLogEvent(LogState.ERROR, exception);
                throw exception;
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model CustomerCreditCards
        /// </summary>
        /// <param name="customerCreditCardId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteCustomerCard(int customerCreditCardId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_CustomerCreditCards_Delete]",
                    new { CustomerCreditCardId = customerCreditCardId });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model CustomersByPartners
        /// </summary>
        /// <param name="partnerId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteCustomersByPartners(int? partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_CustomersByPartners_Delete]",
                    new { PartnerId = partnerId });
                String message = string.Format("[Delete] PartnerId: {0}", (int)partnerId);
                new EventLogBusiness().AddLogEvent(LogState.INFO, message);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int GetPartnerIdByUserId(int userId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_PartnerIdByUserId_Retrieve]",
                    new { UserId = userId });
            }
        }

        /// <summary>
        /// Retrieve the Customer which belong to the partner
        /// </summary>
        /// <param name="PartnerId"></param>
        /// <returns></returns>
        public IEnumerable<Customers> RetrieveCustomersForTreeView(int? PartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Customers>("[General].[Sp_Customers_Retrieve]",
                new
                {
                    PartnerId
                });
            }
        }

        /// <summary>
        /// Save the customers Ids
        /// </summary>
        /// <param name="Customers"></param>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        public void CustomerByCustomerAddOrEdit(List<int> Customers, int UserId, string RoleId)
        {
            var Role = UserRolesPermissionsBusiness.RetrieveRolesByUsers(UserId).FirstOrDefault();

            if (Customers == null)
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[General].[Sp_CustomerByCustomer_AddOrEdit]",
                    new
                    {
                        UserId,
                        CustomerId = 0,
                        Role.RoleId,
                        Flag = 1,
                        RoleForCustomer = RoleId
                    });
                }
            }
            else
            {

                int Flag = 1; //the first time it will save the flag goes as 1

                foreach (var UId in Customers)
                {
                    using (var dba = new DataBaseAccess())
                    {
                        dba.ExecuteNonQuery("[General].[Sp_CustomerByCustomer_AddOrEdit]",
                        new
                        {
                            UserId,
                            CustomerId = UId,
                            Role.RoleId,
                            Flag,
                            RoleForCustomer = RoleId
                        });
                    }
                    Flag = 0;
                }
            }
        }

        /// <summary>
        /// Retrieve Customer by Customer
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public IEnumerable<Customers> CustomerByCustomerRetrieve(int UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Customers>("[General].[Sp_CustomerByCustomer_Retrieve]",
                new
                {
                    UserId
                });
            }
        }

        /// <summary>
        /// Retrieve Branch By Customer
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IEnumerable<Customers> BranchByCustomerRetrieve(int CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Customers>("[General].[Sp_CustomerBranch_Retrieve]",
                new
                {
                    CustomerId
                });
            }
        }

        /// <summary>
        /// BranchByCustomerAddOrEdit
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="CustomerList"></param>
        public void BranchByCustomerAddOrEdit(int? CustomerId, List<BranchCustomers> CustomerList)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteReader<Customers>("[General].[Sp_CustomerBranch_AddOrEdit]",
                new
                {
                    CustomerId
                   ,CustomerBranch = getXml(CustomerList)
                   ,ModifyUserId = Session.GetUserInfo().UserId
                });
            }
        }

        string getXml(List<BranchCustomers> list)
        {
            using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<BranchCustomers>));
                xmlSerializer.Serialize(stringWriter, list);
                return stringWriter.ToString().Substring(stringWriter.ToString().IndexOf('\n') + 1);
            }
        }



        /// <summary>
        /// Get the role for the customer
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="selectionType"></param>
        /// <returns></returns>
        public string GetRoleForCustomerByCustomer(int userId, int? selectionType)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_RoleForUserAuthorization_Retrieve]",
                new
                {
                    UserId = userId,
                    SelectionType = selectionType
                });
            }
        }

        /// <summary>
        /// Add Or Edit Additional Info
        /// </summary>
        /// <param name="model"></param>
        /// <param name="CustomerId"></param>
        public void AddOrEditAdditionalInfo(Customers model, int CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_CustomersAdditionalInformation_AddOrEdit]",
                    new
                    {
                        model.AdditionalId,
                        CustomerId,
                        model.Address,
                        model.BusinessName,
                        model.CityId,
                        model.CountyId,
                        model.StateId,
                        model.ContactName,
                        model.Email,
                        model.SecundaryPhone,
                        model.MainPhone,
                        model.Latitude,
                        model.Longitude,
                        model.ZoneId,
                        model.LegalDocument,
                        model.ApplicantClient,
                        model.SupportTicket,
                        model.SupportTicketURL,
                        model.FinancialContact,
                        model.FinancialEmail,
                        model.RegisteredOwner,
                        Session.GetUserInfo().UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve Customers Additional
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<CustomerAdditionalInformation> RetrieveCustomersAdditional(int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerAdditionalInformation>("[General].[Sp_CustomersAdditionalInformation_Retrieve]",
                new
                {
                    CustomerId = customerId
                }).ToList();
            }
        }

        /// <summary>
        /// Retrieve Vehicles By Customer
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IEnumerable<Vehicles> RetrieveVehiclesByCustomer(int? CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Vehicles>("[General].[Sp_Vehicles_Retrieve]",
                new
                {
                    CustomerId,
                    Session.GetUserInfo().UserId
                });
            }
        }

        /// <summary>
        /// Retrieve Vehicle By VehicleId
        /// </summary>
        /// <param name="VehicleId"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public Vehicles RetrieveVehicleByVehicleId(int? VehicleId, int CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Vehicles>("[General].[Sp_Vehicles_Retrieve]",
                new
                {
                    CustomerId,
                    VehicleId,
                    Session.GetUserInfo().UserId
                }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Add or edit vehicle from partner profile
        /// </summary>
        /// <param name="model"></param>
        public void AddOrEditVehicleByCustomer(Vehicles model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleByCustomer_ForPartner_AddOrEdit]",
                    new
                    {
                        VehicleId = model.VehicleId,
                        CustomerId = model.CustomerId,
                        PlateId = model.PlateId.Trim(),
                        Name = model.Name.Trim(),
                        Colour = (string.IsNullOrEmpty(model.Colour)) ? string.Empty : model.Colour.Trim(),
                        Chassis = model.Chassis.Trim(),
                        AVL = (string.IsNullOrEmpty(model.AVL)) ? string.Empty : model.AVL.Trim(),
                        Odometer = model.Odometer,
                        Imei = (string.IsNullOrEmpty(model.AVL)) ? 0 : Convert.ToDecimal(model.AVL),
                        BrandName = model.Manufacturer.Trim(),
                        ModelName = model.VehicleModel.Trim(),
                        ModelYear = model.VehicleYear,
                        SupportTicket = model.SupportTicket,
                        SupportTicketURL = model.SupportTicketURL,
                        ClassificationId = model.ClassificationId,
                        LoggedUserId = Session.GetUserInfo().UserId,
                        OperationBAC = model.OperationBAC
                        //FleetioId = model.FleetioId
                    });
            }
        }

        /// <summary>
        ///  Add or Retrieve the Customer Binnacles
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IEnumerable<CustomerBinnacle> AddOrRetrieveCustomerBinnacle(CustomerBinnacle model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerBinnacle>("[General].[Sp_CustomerBinnacle_Add]",
                new
                {
                    CustomerId = model.CustomerId,
                    Id = model.Id,
                    Comment = string.IsNullOrEmpty(model.Comment) ? null : model.Comment,
                    UserName = model.EncryptedUserName,
                    Delete = model.Deleted
                });
            }
        }


        /// <summary>
        /// Add Or Edit Customer Contract 
        /// </summary>
        /// <param name="model"></param>
        public void CustomerContractAddOrEdit(CustomerContractInformation model)
        {
            using (var dba = new DataBaseAccess())
            {
                model.YearsOrMonths = model.ContractYears;
                model.ContractYears = (model.UseMonths) ? ((model.ContractYears > 0) ? (model.ContractYears / 12) : 1) : model.ContractYears;

                dba.ExecuteNonQuery("[General].[Sp_CustomerContracts_AddOrEdit]",
                    new
                    {
                        Id = model.ContractId,
                        model.Since,
                        model.To,
                        Years = model.ContractYears,
                        Vehicles = ECOsystem.Utilities.Miscellaneous.GetXML(model.Vehicles),
                        model.CustomerId,
                        Session.GetUserInfo().UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve Vehicles list in Contract By ContractId
        /// </summary>
        /// <param name="ContractId"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public List<Vehicles> RetrieveContractByContractId(int? ContractId, int CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Vehicles>("[General].[Sp_CustomerContractByContractId_Retrieve]",
                new
                {
                    ContractId,
                    CustomerId
                }).ToList();
            }
        }

        /// <summary>
        /// Retrieve the Payments related to the customer
        /// </summary>
        /// <param name="Filters"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public List<Payments> RetrieveCustomerPayments(Filters Filters, int? CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Payments>("[Control].[Sp_CustomerPayments_Retrieve]",
                new
                {
                    Month = Filters.ReportCriteriaId == (int?)ReportCriteria.DateRange ? null : Filters.Month,
                    Year = Filters.ReportCriteriaId == (int?)ReportCriteria.DateRange ? null : Filters.Year,
                    Filters.StartDate,
                    Filters.EndDate,
                    CustomerId = CustomerId == null ? Filters.CustomerId : CustomerId
                }).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Payment"></param>
        /// <param name="CustomerId"></param>
        public void AddOrEditCustomerPayments(decimal Payment, int CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Control].[Sp_CustomerPayment_AddOrEdit]",
                new
                {
                    Payment
                   ,CustomerId
                   ,Session.GetUserInfo().UserId
                });
            }
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="ContractId"></param>
        /// <returns></returns>
        public DataTable GetReportData(int CustomerId, int ContractId)
        {
            return DataTableUtilities.ClassToDataTable(RetrieveContractInformation(CustomerId, ContractId, true), string.Empty);
        }

        /// <summary>
        /// Allow to cancel a contract usind the contract id and customer id.
        /// </summary>
        /// <param name="CustomerId">Customer id</param>
        /// <param name="ContractId">Contract id</param>
        public void CustomerContractCancel(int CustomerId, int ContractId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_CustomerContracts_Cancel]",
                    new
                    {
                        CustomerId,
                        ContractId,
                        Session.GetUserInfo().UserId
                    });
            }
        }

        /// <summary>
        /// Trasladate Vehicle Between Customer
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="currentCustomerId"></param>
        /// <param name="assignedCustomerId"></param>
        public void VehicleTrasladatedBetweenCustomer(int vehicleId, int currentCustomerId, int assignedCustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_AssignNewCustomerToVehicle]",
                    new
                    {
                        CurrentCustomerId = currentCustomerId,
                        AssignedCustomerId = assignedCustomerId,
                        VehicleId = vehicleId
                    });
            }
        }

        /// <summary>
        /// Trasladate Customer Between Partner
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="currentPartnerId"></param>
        /// <param name="assignedPartnerId"></param>
        public void CustomerTrasladatedBetweenPartners(int customerId, int assignedPartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_AssignNewPartnerToCustomer]",
                    new
                    {
                        AssignedPartnerId = assignedPartnerId,
                        CustomerId = customerId
                    });
            }
        }

        /// <summary>
        /// Get Customer Id from Current Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string GetCustomerIdFromEmail(string email)
        {
            using (var userbusiness = new UsersBusiness())
            {
                var existUser = userbusiness.RetrieveUsers(null, null, userName: TripleDesEncryption.Encrypt(email)).FirstOrDefault();
                if (existUser != null && existUser.CustomerId > 0)
                    return existUser.CustomerId.ToString();

                return string.Empty;
            }
        }
        
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
