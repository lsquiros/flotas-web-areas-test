﻿/************************************************************************************************************
*  File    : VehicleUnitsBusiness.cs
*  Summary : VehicleUnits Business Methods
*  Author  : Berman Romero
*  Date    : 09/22/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
//using COINCA.Library.DataAccess;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// VehicleUnits Class
    /// </summary>
    public class VehicleUnitsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleUnits
        /// </summary>
        /// <param name="unitId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<VehicleUnits> RetrieveVehicleUnits(int? unitId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<VehicleUnits>("[General].[Sp_VehicleUnits_Retrieve]",
                    new
                    {
                        UnitId = unitId,
                        CustomerId= Session.GetCustomerId(),
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleUnits
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehicleUnits(VehicleUnits model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleUnits_AddOrEdit]",
                    new
                    {
                        model.UnitId,
                        model.Name,
                        CustomerId= Session.GetCustomerId(),
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model VehicleUnits
        /// </summary>
        /// <param name="unitId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteVehicleUnits(int unitId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehicleUnits_Delete]",
                    new { UnitId = unitId,
                          ModifyUserId = Session.GetUserInfo().UserId
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}