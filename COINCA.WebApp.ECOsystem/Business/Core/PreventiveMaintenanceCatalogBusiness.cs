﻿/************************************************************************************************************
*  File    : PreventiveMaintenanceBusiness.cs
*  Summary : Preventive Maintenance Business Methods
*  Author  : Cristian Martínez
*  Date    : 01/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;

namespace ECOsystem.Business.Core
{    
    /// <summary>
    /// Preventive Maintenance Business class performs all related to operators business logic
    /// </summary>
    public class PreventiveMaintenanceCatalogBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Preventive Maintenance information
        /// </summary>
        /// <param name="customerId">The Primary Key uniquely that identifies each customer</param>
        /// <param name="preventiveMaintenanceCatalogId">The Primary Key uniquely that identifies each preventive maintenance</param>
        /// <param name="key">Key is the search</param>
        /// <returns>IEnumerable collection of the PreventiveMaintenance entity model</returns>
        public IEnumerable<PreventiveMaintenanceCatalog> RetrievePreventiveMaintenance(int? customerId, int? preventiveMaintenanceCatalogId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PreventiveMaintenanceCatalog>("[General].[Sp_PreventiveMaintenanceCatalog_Retrieve]",
                    new
                    {
                        CustomerId = customerId,
                        PreventiveMaintenanceCatalogId = preventiveMaintenanceCatalogId,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model PreventiveMaintenanceUnits
        /// </summary>
        /// <param name="preventiveMaintenanceCatalogId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeletePreventiveMaintenance(int preventiveMaintenanceCatalogId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceCatalog_Delete]",
                    new { PreventiveMaintenanceCatalogId = preventiveMaintenanceCatalogId });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Operators
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditPreventiveMaintenance(PreventiveMaintenanceCatalog model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceCatalog_AddOrEdit]",
                    new
                    {
                        model.PreventiveMaintenanceCatalogId,
                        CustomerId= Session.GetCustomerId(), model.Description, model.FrequencyKm, model.AlertBeforeKm, model.FrequencyMonth, model.AlertBeforeMonth, model.FrequencyDate, model.AlertBeforeDate, model.LoggedUserId, model.RowVersion
                    });
            }
        }

        /// <summary>
        /// Retrieve Preventive Maintenance Cost information
        /// </summary>
        /// <param name="preventiveMaintenanceCatalogId">The Primary Key uniquely that identifies each preventive maintenance</param>
        /// <returns>IEnumerable collection of the PreventiveMaintenanceCost entity model</returns>
        public IEnumerable<PreventiveMaintenanceCost> RetrievePreventiveMaintenanceCost(int preventiveMaintenanceCatalogId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PreventiveMaintenanceCost>("[General].[Sp_PreventiveMaintenanceCost_Retrieve]",
                    new
                    {
                        PreventiveMaintenanceCatalogId = preventiveMaintenanceCatalogId
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model PreventiveMaintenanceCost
        /// </summary>
        public void DeletePreventiveCostMaintenance(int preventiveMaintenanceCostId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceCost_Delete]",
                    new { PreventiveMaintenanceCostId = preventiveMaintenanceCostId });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Operators
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditPreventiveMaintenanceCost(List<string> model)
        {
            var jsTool = new JavaScriptSerializer();
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            foreach (var item in model)
            {
                var cost = jsTool.Deserialize<PreventiveMaintenanceCost>(item);
                root.Add(new XElement("PreventiveMaintenanceCost", new XAttribute("PreventiveMaintenanceCatalogId", cost.PreventiveMaintenanceCatalogId), new XAttribute("PreventiveMaintenanceCostId", cost.PreventiveMaintenanceCostId), new XAttribute("Description", cost.Description), new XAttribute("Cost", cost.Cost)));
            }
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PreventiveMaintenanceCatalogCost_AddOrEdit]",
                    new
                    {
                        XmlData = doc.ToString(),
                        LoggedUserId = Session.GetUserInfo().UserId
                    });
            }

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}