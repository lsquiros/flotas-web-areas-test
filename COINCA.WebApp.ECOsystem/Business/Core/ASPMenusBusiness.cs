﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;

namespace ECOsystem.Business.Administration
{
#pragma warning disable 1591

    /// <summary>
    /// </summary>
    public class ASPMenusBusiness : IDisposable
    {
        /// <summary>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="roleName"></param>
        /// <param name="header"></param>
        /// <returns></returns>
        public static IEnumerable<AccountMenus> RetrieveMenus(int? header, int? parent, string RoleId = null)
        {
            var partnerId = ECOsystem.Utilities.Session.GetPartnerId(); 
            var customerId = ECOsystem.Utilities.Session.GetCustomerId();

            if (customerId != null)
            {
                partnerId = null;
            }

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<AccountMenus>("[dbo].[SP_AspNetMenus_Retrieve]",
                    new
                    {
                        RoleId = RoleId,
                        Header = header,
                        Parent = parent,
                        CustomerId = customerId,
                        PartnerId = partnerId

                    });
            }            
        }
             
        /// <summary>
        /// Retrieve Permissions
        /// </summary>
        /// <param name="header"></param>
        /// <param name="parent"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public static IEnumerable<AccountMenus> RetrievePermissions(int? header, int? parent, string roleId = null)
        {
            var customerId = ECOsystem.Utilities.Session.GetCustomerId();

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<AccountMenus>("[dbo].[Sp_ASPNetUserPermissions_Retrieve]",
                    new
                    {
                        RoleId = roleId,
                        Header = header,
                        Parent = parent,
                        CustomerId = customerId
                    });
            }
        }

        /// <summary>
        /// Retrieve Menus Parents
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        public static IEnumerable<AccountMenus> RetrieveMenusParents(int? header)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<AccountMenus>("[dbo].[SP_AspNetMenusParents_Retrieve]",
                    new
                    {
                        Header = header
                    });
            }
        }

        /// <summary>
        /// Get Parents
        /// </summary>
        public static SelectList GetParents
        {
            get
            {
                object x = null;
                var dictionary = new Dictionary<int?, string>();
                using (var dba = new DataBaseAccess())
                {
                    var Parents = dba.ExecuteReader<AccountMenus>("[dbo].[SP_AspNetMenusParents_Retrieve]",
                    new
                    {
                        Header = x
                    });

                    foreach (var parent in Parents)
                    {
                        dictionary.Add(parent.MenuId, parent.Name);
 
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }

            }
        
        }

        /// <summary>
        /// Dipose GC
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}