﻿using ECOsystem.Models;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Business
{
    public class ProgramSendReportsBusiness : IDisposable
    {

        public IEnumerable<ProgramSendReportsShow> ProgramSendReportCustomer_Retrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                var reports = dba.ExecuteReader<ProgramSendReportsShow>("[General].[ReportsCustomer_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    });

                return reports;
            }
        }

        public IEnumerable<ProgramSendReportsShow> ProgramSendReportsRetrieve()
        {
            int? customerId = ECOsystem.Utilities.Session.GetCustomerId();
            int? partnerId = ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId;
            partnerId = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser ? null : partnerId;

            using (var dba = new DataBaseAccess())
            {
                var reports = dba.ExecuteReader<ProgramSendReportsShow>("[General].[ReportsRetrieve]",
                    new
                    {
                        CustomerId = customerId,
                        PartnerId = partnerId,
                        ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser
                    });

                foreach (var item in reports)
                {
                    item.Parameters = dba.ExecuteReader<ReportParameters>("[General].[ReportsParametersRetrieve]",
                    new
                    {
                        item.ReportId,
                    }).FirstOrDefault();
                }
                HttpContext.Current.Session["ProgramSendReports"] = reports;
                return reports;
            }
        }

        /// <summary>
        /// Performs the maintenance insert or update of the entity ProgramSendReports FOR A CUSTOMER USER
        /// </summary>
        /// <param name="model">Model or class passed as parameter which contains all properties to perform insert or update operations</param>
        public void ProgramSendReportAddOrEdit(ProgramSendReports model)
        {
            using (var dba = new DataBaseAccess())
            {
                int? customerId = ECOsystem.Utilities.Session.GetCustomerId();
                int? partnerId = customerId == null ? ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId : null;
                partnerId = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser ? null : partnerId;

                dba.ExecuteNonQuery("[General].[Sp_SendProgramReport_AddOrEdit]",
                new
                {
                    model.Id,
                    model.ReportId,
                    model.Emails,
                    model.StartDate,
                    model.EndDate,
                    CustomerId = customerId == null ? model.CustomerId : customerId,
                    PartnerId = partnerId,
                    model.VehicleId,
                    model.CostCenterId,
                    model.VehicleGroupId,
                    model.VehicleUnitId, //is send null
                    model.DriverId,
                    model.Elapse,
                    model.Days,
                    Active = true,
                    model.TerminalDetail, //is send null
                    model.IsAlert,
                    UserEmailId = model.UserId,
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId,
                    IsCoinca = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser,
                    model.Module
                });
            }
        }

        public void ProgramSendReportAddOrEdit(List<ProgramSendReports> list)
        {
            using (var dba = new DataBaseAccess())
            {
                int? customerId = ECOsystem.Utilities.Session.GetCustomerId();
                int? partnerId = customerId == null ? ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId : null;
                partnerId = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser ? null : partnerId;

                if (list.FirstOrDefault().CostCenterList != null)
                {
                    //Delete all the reports for this customer
                    foreach (var item in list.FirstOrDefault().CostCenterList)
                    {
                        dba.ExecuteNonQuery("[General].[Sp_SendProgramReport_Delete]",
                        new
                        {
                            CustomerId = customerId,
                            PartnerId = partnerId,
                            IsCoinca = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser,
                            list.FirstOrDefault().ReportId,
                            item.Id
                        });
                    }
                }
                else
                {
                    //Delete all the reports for this customer
                    foreach (var item in list)
                    {
                        dba.ExecuteNonQuery("[General].[Sp_SendProgramReport_Delete]",
                        new
                        {
                            CustomerId = customerId,
                            PartnerId = partnerId,
                            IsCoinca = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser,
                            item.ReportId,
                            item.Id
                        });
                    }
                }
              
                foreach (var item in list.Where(x => x.CostCenterList == null || x.CostCenterList.Count == 0))
                {
                    dba.ExecuteNonQuery("[General].[Sp_SendProgramReport_AddOrEdit]",
                    new
                    {
                        ReportId = item.ReportId,
                        Emails = item.Emails,
                        StartDate = item.StartDate,
                        EndDate = item.EndDate,
                        CustomerId = customerId == null ? item.CustomerId : customerId,
                        PartnerId = partnerId,
                        VehicleId = item.VehicleId,
                        CostCenterId = item.CostCenterId,
                        VehicleGroupId = item.VehicleGroupId,
                        VehicleUnitId = item.VehicleUnitId,
                        DriverId = item.DriverId,
                        Elapse = item.Elapse,
                        Days = item.Days,
                        Active = item.Active,
                        TerminalDetail = item.TerminalDetail,
                        IsAlert = item.IsAlert,
                        UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId,
                        IsCoinca = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser
                    });
                }
                if (list.Where(x => x.CostCenterList != null || x.CostCenterList.Count > 0) != null)
                {
                    list.Where(x => x.CostCenterList == null || x.CostCenterList.Count == 0).ToList().ForEach(x => x.CostCenterList = new List<ProgramSummaryEventByCostCenters>());

                    //Save the for the costcenterlist
                    foreach (var item in list.Where(x => x.CostCenterList != null || x.CostCenterList.Count > 0).ToList())
                    {
                        foreach (var cc in item.CostCenterList)
                        {
                            dba.ExecuteNonQuery("[General].[Sp_SendProgramReport_AddOrEdit]",
                            new
                            {
                                ReportId = item.ReportId,
                                Emails = cc.CostCenterEmails,
                                StartDate = item.StartDate,
                                EndDate = item.EndDate,
                                CustomerId = customerId == null ? item.CustomerId : customerId,
                                PartnerId = partnerId,
                                VehicleId = item.VehicleId,
                                CostCenterId = cc.CostCenterId,
                                VehicleGroupId = item.VehicleGroupId,
                                VehicleUnitId = item.VehicleUnitId,
                                DriverId = item.DriverId,
                                Elapse = item.Elapse,
                                Days = item.Days,
                                Active = item.Active,
                                TerminalDetail = item.TerminalDetail,
                                IsAlert = item.IsAlert,
                                UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId,
                                IsCoinca = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser
                            });
                        }
                    }
                }
            }
        }

        public List<ProgramSendReportsShow> ReturnDataToView(int? Id)
        {
            //var list = ProgramSendReportsRetrieve();
            var list = (List<ProgramSendReportsShow>)HttpContext.Current.Session["ProgramSendReports"];

            var returnlist = new List<ProgramSendReportsShow>();

            foreach (var item in list.Where(x => x.Id == Id))
            {
                if (!returnlist.Where(x => x.Id == item.Id).Any())
                {
                    var model = new ProgramSendReportsShow();
                    model = item;
                    if (item.Parameters == null) item.Parameters = new ReportParameters();
                    if (item.Parameters.CostCenterTable)
                    {
                        model.CostCenterList = new List<ProgramSummaryEventByCostCenters>();                        
                        foreach (var y in list.Where(x => x.CostCenterId != null && x.Elapse == item.Elapse && x.Days == item.Days && x.ReportId == item.ReportId))
                        {
                            var cc = new ProgramSummaryEventByCostCenters();
                            cc.Id = y.Id;
                            cc.CostCenterId = y.CostCenterId;
                            cc.CostCenterName = y.CostCenterId == -1 ? "Todos" : y.CostCenterName;
                            cc.CostCenterEmails = y.Emails;
                            model.CostCenterList.Add(cc);
                        }
                    }
                    returnlist.Add(model);
                }
            }
            return returnlist;
        }

        public void SendTestProgramEmail(int? ReportId)
        {
            using (var dba = new DataBaseAccess())
            {
                int? customerId = ECOsystem.Utilities.Session.GetCustomerId();
                int? partnerId = customerId == null ? ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId : null;

                dba.ExecuteNonQuery("[General].[Sp_SendTestReport_Edit]",
                new
                {
                    ReportId,
                    CustomerId = customerId,
                    PartnerId = partnerId
                });
            }
        }


        public void DeleteProgramReport(int? ReportId, int? Id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_SendProgramReport_DeleteById]", new
                {
                    ReportId
                   ,Id

                });
            }

        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}

