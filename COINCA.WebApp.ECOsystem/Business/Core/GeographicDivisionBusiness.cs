﻿/************************************************************************************************************
*  File    : GeographicDivisionBusiness.cs
*  Summary : Geographic Division Business Methods
*  Author  : Napoleón Alvarado
*  Date    : 12/17/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// Geographic Division Business Class
    /// </summary>
    public class GeographicDivisionBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve States
        /// </summary>
        /// <param name="stateId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="countryId">Country Id related to the States</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<States> RetrieveStates(int? stateId, int? countryId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<States>("[General].[Sp_States_Retrieve]",
                    new
                    {
                        StateId = stateId,
                        CountryId = countryId,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Retrieve Counties
        /// </summary>
        /// <param name="countyId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="stateId">State Id related to the Counties</param>
        /// <param name="countryId">Country Id related to the States</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Counties> RetrieveCounties(int? countyId, int? stateId, int? countryId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Counties>("[General].[Sp_Counties_Retrieve]",
                    new
                    {
                        CountyId = countyId,
                        StateId = stateId,
                        CountryId = countryId,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Retrieve Counties JSON
        /// </summary>
        /// <param name="countyId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="stateId">State Id related to the Counties</param>
        /// <param name="countryId">Country Id related to the States</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<dynamic> RetrieveCountiesJSON(int? countyId, int? stateId, int? countryId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Counties>("[General].[Sp_Counties_Retrieve]",
                    new
                    {
                        CountyId = countyId,
                        StateId = stateId,
                        CountryId = countryId,
                        Key = key
                    }).Select(x => new {id = x.CountyId, text = x.Code + " " + x.Name});                
            }
        }

        /// <summary>
        /// Retrieve Cities
        /// </summary>
        /// <param name="cityId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="countyId">County Id related to the Cities</param>
        /// <param name="stateId">State Id related to the Counties</param>
        /// <param name="countryId">Country Id related to the States</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Cities> RetrieveCities(int? cityId, int? countyId, int? stateId, int? countryId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Cities>("[General].[Sp_Cities_Retrieve]",
                    new
                    {
                        CityId = cityId,
                        CountyId = countyId,
                        StateId = stateId,
                        CountryId = countryId,
                        Key = key
                    });
            }
        }

        /// <summary>
        /// Retrieve Cities JSON
        /// </summary>
        /// <param name="cityId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="countyId">County Id related to the Cities</param>
        /// <param name="stateId">State Id related to the Counties</param>
        /// <param name="countryId">Country Id related to the States</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<dynamic> RetrieveCitiesJSON(int? cityId, int? countyId, int? stateId, int? countryId, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Cities>("[General].[Sp_Cities_Retrieve]",
                    new
                    {
                        CityId = cityId,
                        CountyId = countyId,
                        StateId = stateId,
                        CountryId = countryId,
                        Key = key
                    }).Select(x => new { id = x.CityId, text = x.Code + " " + x.Name });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}