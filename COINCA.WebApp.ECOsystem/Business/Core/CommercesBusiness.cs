﻿using ECOsystem.Models.Core;
using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ECOsystem.Business.Core
{
    public class CommercesBusiness :IDisposable
    {
        public IEnumerable<Commerces> CommercesRetrieve(int? id = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Commerces>("[General].[Sp_Commerces_Retrieve]", new
                {
                    Id = id,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                });
            }
        }


        public IEnumerable<Commerces> CommercesNear(int  pointID)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Commerces>("[General].[Sp_CommercesbyNear_Retrieve]", new
                {
                    PointId = pointID,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                });
            }
        }   

        public void CommercesAddOrEdit(Commerces model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Commerces_AddOrEdit]", new
                {
                    model.Id, 
                    model.Name,
                    model.Description,
                    model.Latitude, 
                    model.Longitude,
                    model.Type,
                    model.Code,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public void CommercesDelete(int id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Commerces_Delete]", new
                {
                    Id = id,
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                    UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId
                });
            }
        }

        public DataTable ReportData(List<Commerces> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("Name");
                dt.Columns.Add("Description");
                dt.Columns.Add("Address");
                dt.Columns.Add("Latitude");
                dt.Columns.Add("Longitude");                
                dt.Columns.Add("CustomerName");
                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();
                    row["Name"] = item.Name;
                    row["Description"] = item.Description;
                    row["Address"] = item.Address;
                    row["Latitude"] = item.Latitude;
                    row["Longitude"] = item.Longitude;                    
                    row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    dt.Rows.Add(row);
                }
                return dt;
            }            
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}