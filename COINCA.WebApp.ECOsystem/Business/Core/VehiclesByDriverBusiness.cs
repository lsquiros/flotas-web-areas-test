﻿/************************************************************************************************************
*  File    : VehiclesByDriverBusiness.cs
*  Summary : VehiclesByDriver Business Methods
*  Author  : Gerald Solano
*  Date    : 22/07/2015
* 
*  Copyright 2015 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Linq;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Models.Administration;
using ECOsystem.Utilities;
//using COINCA.Library.DataAccess;

namespace ECOsystem.Business.Administration
{
    /// <summary>
    /// VehiclesByDriverBusiness Class
    /// </summary>
    public class VehiclesByDriverBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve  VehiclesByDriver
        /// </summary>
        /// <param name="userId">The driver group Id to find all vehicles associated</param>
        /// <returns>An IEnumerable of T, T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public VehiclesByDriverData RetrieveVehiclesByDriver(int? userId)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = new VehiclesByDriverData();
                var user = Session.GetUserInfo();

                var aux = dba.ExecuteReader<VehiclesByDriver>("[General].[Sp_VehiclesByDriver_Retrieve]",
                    new
                    {
                        UserId = userId,
                        CustomerId= Session.GetCustomerId(),
                        UserIdDinamic = user.UserId
                    });
                
                result.AvailableVehiclesList = aux.Where(x => x.UserId == null);
                result.ConnectedVehiclesList = aux.Where(x => x.UserId != null);
                result.UserId = userId;

                return result;
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehiclesByDriver
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditVehiclesByDriver(VehiclesByDriverData model)
        {
            
            var doc = new XDocument(new XElement("xmldata"));
            var root = doc.Root;
            var i = 0;
            foreach (var item in model.ConnectedVehiclesList)
            {
                root.Add(new XElement("Vehicle", new XAttribute("VehicleId", item.VehicleId), new XAttribute("Index", ++i)));
            }

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_VehiclesByDriver_AddOrEdit]",
                    new
                    {
                        model.UserId,
                        XmlData = doc.ToString(),
                        LoggedUserId = Session.GetUserInfo().UserId,
                        CustomerId = Session.GetCustomerId()
                    });
            }

        }

        /// <summary>
        /// Performs the the operation of delete on the model DriversByVehicle
        /// </summary>
        public void DeleteVehiclesByDriver(int userId)
        {
            using (var dba = new DataBaseAccess())
            {
               //TODO: 
            }
        }

        /// <summary>
        /// Retrieve Vehicle By Driver Code
        /// </summary>
        /// <param name="code"></param>
        /// <param name="customerId"></param>
        /// <param name="plateId"></param>
        /// <returns></returns>
        public VehicleByDriverCode RetrieveVehicleByDriverCode(string code, int customerId, string plateId) {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    return dba.ExecuteReader<VehicleByDriverCode>("[General].[Sp_VehiclesByDriverCode_Retrieve]",
                       new {
                           DriverCode = code,
                           CustomerId = customerId,
                           PlateId = plateId
                        }).FirstOrDefault();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Retrieve Driver Identification
        /// </summary>
        /// <param name="code"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public string RetrieveDriverIdByDriverCode(string code, int customerId)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    return dba.ExecuteScalar<string>("[General].[Sp_DriverByDriverCode_Retrieve]",
                       new
                       {
                           DriverCode = code,
                           CustomerId = customerId                           
                       });
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}