﻿using ECOsystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOsystem.Business.Core
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class APITokenBusiness : IDisposable
    {
        public string RetrieveAPIToken(int? CustomerId = null, int? PartnerId = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_APIToken_Retrieve]",
                    new
                    {
                        CustomerId,
                        PartnerId
                    });
            }
        }

        public string GenerateAPIToken(int? CustomerId = null, int? PartnerId = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_ClientTokenApi_AddOrEdit]",
                    new
                    {
                        CustomerId,
                        PartnerId
                    });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member

}