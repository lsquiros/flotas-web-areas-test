﻿
/************************************************************************************************************
*  File    : SettingOdometersBusiness.cs
*  Summary : Setting Odometers Business Methods
*  Author  : Stefano Quirós Ruiz
*  Date    : 03/29/2016
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// </summary>
    public class SettingOdometersBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Setting Odometers Data 
        /// </summary>
        /// <param name="model">
        ///     Model or class passed as parameter which contains all properties to perform insert or update operations
        ///     Vehicle Fuel By Group
        ///     Model or class passed as parameter which contains all properties to perform insert or update operations
        /// </param>
        /// <returns>SettingOdometersBase Model to render view</returns>
        /// <summary>
        /// Get Setting Odometers Data
        /// </summary>
        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Vehicles
        /// </summary>
        /// <summary>
        /// Execute Add Or Edit Vehicle Fuel By Group
        /// </summary>
        /// <returns>List of Vehicle Fuel By Group</returns>
        /// <summary>
        /// Performs the maintenance Insert or Update of the entity VehicleFuel
        /// </summary>
        public SqlError AddOrEditOdometer(SettingOdometers model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<SqlError>("[Control].[Sp_SettingOdometers_AddOrEdit]",
                    new
                    {
                        model.TransactionId,
                        model.Odometer                       
                    }).FirstOrDefault();
            }

        }


        /// <summary>
        /// Get ParamsBy Customer
        /// </summary>
        public string GetParamByCustomer()
        {
            using (var dba = new DataBaseAccess())
            {
                var cusId = Convert.ToInt32(Session.GetCustomerId());
                return dba.ExecuteScalar<string>("[General].[Sp_GetParametersByCustomer]",
                    new
                    {
                        CustomerId = cusId,
                        ParamByCust = "SettingOdometers"
                        
                    });
            }
        }


        /// <summary>
        /// Performs Alarm Credit Count
        /// </summary>
        public int AlarmOdometerCountRetrieve()
        {
            var parameter = GetParamByCustomer();
            var year = DateTime.Today.Year;
            var month = DateTime.Today.Month;
            var list = new List<SettingOdometers>();           

            using (var dba = new DataBaseAccess())
            {
                var List = dba.ExecuteReader<SettingOdometers>("[Control].[Sp_Setting_Odometers_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId() ?? 0,
                        Year = year,
                        Month = month,                      
                        UserId = Convert.ToInt32(Session.GetUserInfo().UserId)
                    });


                list.AddRange(from item in List let defaultPerformanceMore = Convert.ToDouble(item.DefaultPerformance) +
                              (Convert.ToDouble(item.DefaultPerformance)*Convert.ToDouble(parameter)) let defaultPerformanceLess = Convert.ToDouble(item.DefaultPerformance) -
                              (Convert.ToDouble(item.DefaultPerformance)*Convert.ToDouble(parameter)) where Convert.ToDouble(item.Performance) >= Math.Round(defaultPerformanceMore, 2) 
                              || Convert.ToDouble(item.Performance) <= Math.Round(defaultPerformanceLess, 2) select item);
                List = list;
                return List.Count;
            }
        }

       

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}