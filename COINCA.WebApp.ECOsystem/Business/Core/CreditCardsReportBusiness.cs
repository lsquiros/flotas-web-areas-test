﻿/************************************************************************************************************
*  File    : CreditCardsReportBusiness.cs
*  Summary : CreditCardsReport Business Methods
*  Author  : Danilo Hidalgo
*  Date    : 01/20/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Newtonsoft.Json;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// CreditCardsReport Class
    /// </summary>
    public class CreditCardsReportBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Applicants Report
        /// </summary>
        /// <param name="parameters">parameters options</param>
        /// <returns>An model of CreditCardsReportBase in order to load the chart</returns>
        public CreditCardsReportBase RetrieveCreditCardsReport(AdministrationReportsBase parameters)
        {
            var result = new CreditCardsReportBase();

            GetReportData(parameters, result);
            result.Parameters.Year = parameters.Year;
            result.Parameters.Month = parameters.Month;
            result.Parameters.StartDate = parameters.StartDate;
            result.Parameters.EndDate = parameters.EndDate;
            result.Parameters.ReportCriteriaId = parameters.ReportCriteriaId;
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
            result.Parameters.CountryId = parameters.CountryId;
            result.Parameters.UserId = parameters.UserId;

            var custList = RetrieveCustomersByCountry(result.Parameters.CountryId).ToList();
            var newCustList = new List<Customers>();
            var custTypeAll = custList.FirstOrDefault(w => w.DecryptedName == "Todos");

            newCustList.Add(custTypeAll);
            newCustList.AddRange(custList.Where(w => w.DecryptedName != "Todos").ToList());
            result.UsersList = new SelectList(newCustList, "CustomerId", "DecryptedName", result.Parameters.UserId);

            return result;
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportData(AdministrationReportsBase parameters, CreditCardsReportBase result)
        {
            GetReportDataApplicants(parameters, result);
        }

        /// <summary>
        /// Get Report Data
        /// </summary>
        /// <param name="parameters">Report parameters</param>
        /// <param name="result">Result model</param>
        private void GetReportDataApplicants(AdministrationReportsBase parameters, CreditCardsReportBase result)
        {
            using (var dba = new DataBaseAccess())
            {
                result.List = dba.ExecuteReader<CreditCardsReportByClient>("[Control].[Sp_CreditCardsReport_Retrieve]",
                    new
                    {
                        parameters.PartnerId, parameters.CustomerId, parameters.CountryId, parameters.Year, parameters.Month, parameters.StartDate, parameters.EndDate
                    });
            }
            result.Parameters.ParametersInBase64 = ECOsystem.Utilities.Miscellaneous.Base64Encode(JsonConvert.SerializeObject(parameters));
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <returns>A model of RealVsBudgetFuelsReportBase in order to load the chart</returns>
        public DataTable GenerateCreditCardsReportExcel(AdministrationReportsBase parameters)
        {
            var result = new CreditCardsReportBase();

            GetReportDataApplicants(parameters, result);

            var startDate = new DateTime();
            var endDate = new DateTime();

            if (parameters.StartDate != null && parameters.EndDate != null)
            {
                startDate = parameters.StartDate.HasValue ? parameters.StartDate.Value : DateTime.Now;
                endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : DateTime.Now;
            }
            else if (parameters.Month != null && parameters.Year != null) {
                startDate = new DateTime((int)parameters.Year, (int)parameters.Month, 1, 0, 0, 0);
                endDate = new DateTime((int)parameters.Year, (int)parameters.Month, DateTime.DaysInMonth((int)parameters.Year, (int)parameters.Month), 23, 59, 59);
            }

            var rep = new GlobalReportUtility();
            
            return rep.GetCCReportByClientDataTable((List<CreditCardsReportByClient>)result.List, startDate, endDate);
        }

        /// <summary>
        /// Return a list of Countries for current user in order to using it for populate DropDownListFor
        /// </summary>
        public IEnumerable<Customers> RetrieveCustomersByCountry(int? CountryId)
        {
                      
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<Customers>("[General].[Sp_CustomersByCountry_Retrieve]",
                    new
                    {
                        CountryId,
                        PartnerId = Session.GetPartnerId()
                    });

                //Order by Customer Name A-Z
                result = result.OrderBy(o => o.DecryptedName).ToList();

                return result;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
