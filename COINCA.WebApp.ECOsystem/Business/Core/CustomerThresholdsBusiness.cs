﻿/************************************************************************************************************
*  File    : ThresholdsBusiness.cs
*  Summary : ThresholdsBusiness Methods
*  Author  : Alexander Aguero V
*  Date    : 13/01/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
//using COINCA.Library.DataAccess;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// Parameters Business Class
    /// </summary>
    public class CustomerThresholdsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Parameters
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CustomerThresholds> RetrieveThresholds()
        {
            using (var dba = new DataBaseAccess())
            {
               
                return dba.ExecuteReader<CustomerThresholds>("[General].[Sp_CustomerThresholds_Retrieve]", 
                    
                    new
                    {
                        CustomerId= Session.GetCustomerId()
                    });
                   
            }
        }

        /// <summary>
        /// Retrieve Threshold by customer id parameter
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<CustomerThresholds> RetrieveThresholdByCustomer(int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {

                return dba.ExecuteReader<CustomerThresholds>("[General].[Sp_CustomerThresholds_Retrieve]",

                    new
                    {
                        CustomerId = customerId
                    });

            }
        }
        
        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Parameters
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditThresholds(CustomerThresholds model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_CustomerThresholds_AddOrEdit]", new
                {
                    model.CodingThresholdsId, model.CustomerId, model.RedLower, model.RedHigher, model.YellowLower, model.YellowHigher, model.GreenLower, model.GreenHigher, model.LoggedUserId, model.RowVersion, model.CreditPercentageMin
                });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}