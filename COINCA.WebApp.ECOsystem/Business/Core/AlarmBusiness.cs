﻿/************************************************************************************************************
*  File    : AlarmBusiness.cs
*  Summary : Alarm Business Methods
*  Author  : Cristian Martínez 
*  Date    : 17/Nov/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// Alarm Business class performs all related to operators business logic
    /// </summary>
    public class AlarmBusiness : IDisposable
    {
        /// <summary>
        /// property 
        /// </summary>
        private readonly int MaintenancePreventive = 503;
        private readonly int LicenseExpiration = 504;

        /// <summary>
        ///  Retrieve Alarm informacion
        /// </summary>
        /// <param name="AlarmId">The PRIMARY KEY uniquely thar identifies each record of this model</param>
        /// <param name="AlarmTriggerId">FK AlarmTriggerId from Types</param>
        /// <param name="CustomerId"></param>
        /// <param name="EntityTypeId">FK Entity Type Id From Types</param>
        /// <param name="EntityId"> FK EntityId from Drives, Vehicles, Group, SUbUnit, Unit</param>
        public List<AlarmsModels> RetrieveAlarm(int? AlarmTriggerId = null, int? CustomerId = null, int? EntityTypeId = null, 
            int? AlarmId = null, int? EntityId = null, string CodView = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<AlarmsModels>("[General].[Sp_Alarm_Retrieve]",
                    new
                    {
                        AlarmId = AlarmId,
                        CustomerId = CustomerId,
                        AlarmTriggerId = AlarmTriggerId,
                        EntityTypeId = EntityTypeId,
                        EntityId = EntityId,
                        CodView = CodView
                    }).ToList();
            }
        }

        /// <summary>
        /// Deletes the alarnm entity
        /// </summary>
        /// <param name="alarmId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteAlarms(int alarmId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Alarms_Delete]",
                new { AlarmId = alarmId });
            }
        }

        /// <summary>
        ///  Retrieve Alarm informacion
        /// </summary>
        /// <param name="alarmId">The PRIMARY KEY uniquely thar identifies each record of this model</param>
        /// <param name="alarmTriggerId">FK AlarmTriggerId from Types</param>
        /// <param name="customerId"></param>
        /// <param name="entityTypeId">FK Entity Type Id From Types</param>
        /// <param name="entityId"> FK EntityId from Drives, Vehicles, Group, SUbUnit, Unit</param>
        public AlarmsModels RetrieveFirstAlarm(int? alarmTriggerId = null, int? customerId = null, int? entityTypeId = null, int? alarmId = null, int? entityId = null)
        {
            var alarm = RetrieveAlarm(alarmTriggerId, customerId, entityTypeId, alarmId, entityId).FirstOrDefault() ??
                        new AlarmsModels
                        {
                            CustomerId = (int)customerId,
                            EntityId = (int)entityId,
                            EntityTypeId = (int)entityTypeId,
                            AlarmTriggerId = (int)alarmTriggerId,
                            Active = true
                        };
            if (alarm.AlarmTriggerId == MaintenancePreventive || alarm.AlarmTriggerId == LicenseExpiration)
                alarm.HasPeriodicity = true;
            return alarm;
        }
        /// <summary>
        ///  Performs the maintenance Insert or Update of the entity Alarm
        /// </summary>
        /// <param name="model"></param>
        public void AddOrEditAlarm(AlarmsModels model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Alarm_AddOrEdit]",
                    new
                    {
                        model.AlarmId,
                        model.CustomerId,
                        model.Phone,
                        model.Email,
                        model.AlarmTriggerId,
                        model.EntityTypeId,
                        model.EntityId,
                        model.Active,
                        model.PeriodicityTypeId,
                        model.LoggedUserId,
                        model.DaysBeforeAlert,
                        model.RowVersion,
                        model.RepeatTimes,
                        model.IntervalRepeat,
                        model.SMS
                    });
            }
        }

        /// <summary>
        ///  Performs the maintenance Insert or Update of the entity Alarm
        /// </summary>
        /// <param name="model"></param>
        public void AddOrEditAlarm(AlarmModel model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Alarm_AddOrEdit]",
                    new
                    {
                        model.AlarmId,
                        model.CustomerId,
                        model.Phone,
                        model.Email,
                        model.AlarmTriggerId,
                        model.EntityTypeId,
                        model.EntityId,
                        model.Active,
                        model.PeriodicityTypeId,
                        model.LoggedUserId,
                        model.DaysBeforeAlert,
                        model.RowVersion,
                        model.RepeatTimes,
                        model.IntervalRepeat,
                        model.SMS
                    });
            }
        }

        /// <summary>
        ///  Retrieve Alarm By Vehicle informacion
        /// </summary>
        /// <param name="AlarmId">The PRIMARY KEY uniquely thar identifies each record of this model</param>
        /// <param name="AlarmTriggerId">FK AlarmTriggerId from Types</param>
        /// <param name="CustomerId"></param>
        /// <param name="EntityTypeId">FK Entity Type Id From Types</param>
        /// <param name="EntityId"> FK EntityId from Drives, Vehicles, Group, SUbUnit, Unit</param>
        /// <param name="StartDate"> Initial date to filter the report</param>
        /// <param name="EndDate"> Final date to filter the report</param>
        public IEnumerable<AlarmsModels> RetrieveAlarmsByVehicles(int? AlarmTriggerId = null, int? CustomerId = null, int? EntityTypeId = null, int? AlarmId = null, int? EntityId = null, DateTime? StartDate = null, DateTime? EndDate = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<AlarmsModels>("[General].[Sp_AlarmsByVehicles_Retrieve]",
                    new
                    {
                        AlarmId,
                        CustomerId,
                        AlarmTriggerId,
                        EntityTypeId,
                        EntityId,
                        StartDate,
                        EndDate
                    });
            }
        }

        /// <summary>
        ///  Performs the maintenance Insert or Update of the entity Alarm
        /// </summary>
        /// <param name="model"></param>
        public List<AlarmPropertiesModel> AlarmPropertiesRetrieve(AlarmPropertiesModel model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<AlarmPropertiesModel>("[General].[AlarmProperties_Retrieve]",
                    new
                    {
                        model.Id,
                        model.AlarmTriggerId, 
                        model.CodView,
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    }).ToList();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}