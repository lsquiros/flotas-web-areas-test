﻿/************************************************************************************************************
*  File    : OperatorsBusiness.cs
*  Summary : Operators Business Methods
*  Author  : Cristian Martínez 
*  Date    : 02/Oct/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// Operators Business class performs all related to operators business logic
    /// </summary>
    public class OperatorsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Operators information
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="operatorId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search</param>
        /// <returns>IEnumerable collection of the Operators entity model </returns>
        public IEnumerable<Operators> RetrieveOperators(int? customerId, int? operatorId=null, string key = null) 
        {
            using (var dba = new DataBaseAccess()) 
            { 
                return dba.ExecuteReader<Operators>("[General].[Sp_Operators_Retrieve]", 
                    new{
                        CustomerId = customerId, 
                        OperatorId = operatorId,
                        Key = key
                    }); 
            }
        }
        
        /// <summary>
        /// Retrieve RetrieveOperators
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="operatorId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="isForNew">Flag For New View</param>
        /// <returns></returns>
        public Operators RetrieveOperators(int? customerId, int? operatorId=null, bool isForNew = false)
        {
            var result = new Operators ();
            if (isForNew)
            {
                using (var business = new CustomersBusiness())
                {
                    var customer = business.RetrieveCustomers(Session.GetCustomerId()).FirstOrDefault();
                    if (customer == null) return result;
                    result.CurrencySymbol = customer.CurrencySymbol;
                    result.CurrencyId = customer.CurrencyId;
                }
            }
            else {
                result = RetrieveOperators(customerId:customerId, operatorId:operatorId, key:null).FirstOrDefault();
            }
            return result;
        }

        /// <summary>
        /// Performs the the operation of delete on the model OperatorsUnits
        /// </summary>
        /// <param name="operatorId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteOperators(int operatorId) 
        {
            using (var dba = new DataBaseAccess()) 
            {
                dba.ExecuteNonQuery("[General].[Sp_Operators_Delete]",
                    new { OperatorID = operatorId});
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Operators
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditOperators(Operators model) 
        {
            using (var dba = new DataBaseAccess()) 
            {
                dba.ExecuteNonQuery("[General].[Sp_Operators_AddOrEdit]", 
                    new { 
                       model.OperatorId, 
                       CustomerId= Session.GetCustomerId(),
                       model.Name, 
                       model.Rate, 
                       model.CurrencyId, 
                       model.LoggedUserId, 
                       model.RowVersion
                    });
            }
        }



        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}