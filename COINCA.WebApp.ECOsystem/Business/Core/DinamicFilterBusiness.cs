﻿//using COINCA.Library.DataAccess;

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Utilities;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class DinamicFilterBusiness : IDisposable
    {
        /// <summary>
        /// TreeViewRetrieve
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DinamicFilter> TreeViewRetrieve(string Key)
        {
            var user = Session.GetCustomerInfo();

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<DinamicFilter>("[dbo].[SP_AspDinamicFilters_Retrieve]", new
                {
                    user.CustomerId,
                    FilterType = Key
                });
            }
        }

        /// <summary>
        /// TreeViewCheckedRetrieve
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GetDataChecked> TreeViewCheckedRetrieve(string Key, int UserId)
        {
			var user = Session.GetCustomerInfo();
			using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<GetDataChecked>("[dbo].[Sp_UserDynamicFilter_Retrive]", new
                {
                    UserId,
                    Type = Key,
					CustomerId = user==null?null:user.CustomerId
                });                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="List"></param>
        /// <param name="Key"></param>
        /// <param name="UserId"></param>
        public void SaveFilters( List<string> List, string Key, int UserId)
        {
			//int f = 0;
			var user = Session.GetCustomerInfo();

			using (var dba = new DataBaseAccess())
            {
                if (Key == "CC")
                {
                    foreach (var item in List)
                    {                        
                        //string Type = string.Empty;
                        //if (f == 0)
                        //    Type = "CC";
                        //else
                        //    Type = "VH";
                            
                        dba.ExecuteNonQuery("[dbo].[Sp_UserDynamicFilter_AddOrEdit]",
                        new
                        {
                            UserId,
	                        Type = "VH",
	                        Value = item ,
							user.CustomerId
                        });                            
                        
                        //f = 1;
                    }
                }

                if (Key == "GP")
                {
                    foreach (var item in List)
                    {                        
                        //string Type = string.Empty;
                        //if (f == 0)
                        //    Type = "GP";
                        //else
                        //    Type = "VH";

                        dba.ExecuteNonQuery("[dbo].[Sp_UserDynamicFilter_AddOrEdit]",
                        new
                        {
                            UserId,
                            Type = "VH",
                            Value = item
                        });  
                       
                        //f = 1;
                    }
                }

                //if (Key == "VH")
                //{
                //    foreach (var item in List)
                //    {
                //        if (item != "")
                //        {
                //            dba.ExecuteNonQuery("[dbo].[Sp_UserDynamicFilter_AddOrEdit]",
                //            new
                //            {
                //                user.UserId,
                //                Type = "VH",
                //                Value = item
                //            });
                //        } 
                //    }
                //}
            }
        }

        /// <summary>
        /// GC
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }       
}