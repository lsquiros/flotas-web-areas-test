﻿using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ECOsystem.Models.Core;
using System.Linq;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class UserRolesPermissionsBusiness : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static IEnumerable<UsersPermissions> UserPermissionsRetrieve(int? id, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<UsersPermissions>("[dbo].[Sp_ASPNetUserPermissions_Retrieve]", new
                {
                    UserPerissionsId = id,
                    Key = key
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="role_name"></param>
        /// <param name="isUserCoinca"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static IEnumerable<UsersRoles> RolesRetrieve(string id, string role_name, int? isUserCoinca, string parent)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<UsersRoles>("[dbo].[Sp_ASPNetRoles_Retrieve]", new
                {
                    RoleId = id,
                    RoleName = role_name,
                    CustomerId = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser == true ? null : ECOsystem.Utilities.Session.GetCustomerId(),
                    PartnerId = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser == true ? null : ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId,
                    Parent = parent
                });
            }
        }

        /// <summary>
        /// This is to validate 
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Resource"></param>
        /// <param name="Action"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public bool UserPermissionValidation(string UserName, string Resource, string Action, string RoleId)
        {
            var user = ECOsystem.Utilities.Session.GetUserInfo();
            var userProfile = ECOsystem.Utilities.Session.GetProfileByRole(user.RoleId);

            if (Resource == "HOME")
            {
                if (Action == "SAINDEX" && (userProfile == "SUPER_ADMIN" || userProfile == "SUPER_USER"))
                {
                    return true;
                }
                if (Action == "PAINDEX" && (userProfile == "PARTNER_ADMIN" || userProfile == "PARTNER_USER"))
                {
                    return true;
                }
                if (Action == "IAINDEX" && (userProfile == "INSURANCE_ADMIN" || userProfile == "INSURANCE_USER"))
                {
                    return true;
                }
                if (Action == "INDEX" && (userProfile == "CUSTOMER_ADMIN" || userProfile == "CUSTOMER_USER")
                   || Action == "INDEX" && (userProfile == "PARTNER_ADMIN" || Action == "INDEX" && (userProfile == "PARTNER_USER"))
                   || Action == "INDEX" && (userProfile == "LINEVITA_ADMIN" || Action == "INDEX" && (userProfile == "LINEVITA_USER"))
                   || Action == "INDEX" && (userProfile == "VPOS_USER") || Action == "INDEX" && (userProfile == "SUPER_ADMIN")
                   || Action == "INDEX" && (userProfile == "INSURANCE_ADMIN") || Action == "INDEX" && (userProfile == "INSURANCE_USER")
                   || Action == "INDEX" && (userProfile == "SERVICESTATION_ADMIN") || Action == "INDEX" && (userProfile == "SERVICESTATION_USER"))
                {
                    return true;
                }
            }

            //Validates Service Stations
            if (Resource == "SERVICESTATIONSHOME" && Action == "INDEX" && (userProfile == "SERVICESTATION_ADMIN") || Action == "INDEX" && (userProfile == "SERVICESTATION_USER")) return true;

            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteScalar<int>("[dbo].[Sp_ASPNetUserPermissions_Validate]", new
                {
                    UserName,
                    Resource,
                    Action,
                    RoleId
                });

                return result > 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public IEnumerable<Modules> RetrieveModules(int? id, string key = null, string RoleId = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Modules>("[dbo].[SP_AspNetMenusModules_Retrieve]", new
                {
                    RoleId = RoleId,
                    Key = key,
                    ModuleId = id
                });
            }
        }

        /// <summary>
        /// Roles for Permissions
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public static SelectList RetrieveRolesPermissions(string RoleId, bool? createdByFilter, int? customerId = null)
        {
            var user = ECOsystem.Utilities.Session.GetUserInfo();
            var dictionary = new Dictionary<string, string>();

            using (var bus = new UserRolesPermissionsBusiness())
            {
                var roles = bus.RetrieveRolesByMenu(RoleId, Convert.ToInt32(user.UserId), createdByFilter, customerId);

                foreach (var r in roles)
                {
                    dictionary.Add(r.RoleId, "{'Name':'" + r.Name + "'}");
                }

                return new SelectList(dictionary, "key", "value");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UsersRoles> RetrieveRolesByMenu(string RoleId, int userId, bool? createdByFilter, int? customerId = null)
        {

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<UsersRoles>("[dbo].[Sp_ASPNetRolesByMenu_Retrieve]", new
                {
                    CustomerId = customerId ?? (ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser == true ? null : ECOsystem.Utilities.Session.GetCustomerId()),
                    PartnerId = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser == true ? null : ECOsystem.Utilities.Session.GetPartnerId(),
                    FilterCustomer = createdByFilter
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="roleName"></param>
        public void DeleteModules(int id, string RoleId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[dbo].[Sp_AspNetMenusModules_Delete]",
                new
                {
                    ModuleId = id,
                    RoleId = RoleId
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="roleName"></param>
        public void AddOrEditModules(Modules model, string RoleId)
        {
            if (model.Ico == null) model.Ico = "";

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[dbo].[Sp_AspNetMenusModules_AddOrEdit]",
                new
                {
                    ModuleId = model.ModuleId,
                    Name = model.Name,
                    Ico = model.Ico,
                    Resource = model.Controller,
                    Action = model.Action,
                    RoleId = RoleId
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public string AddOrEditRoles(UsersRoles model)
        {
            string id = "";
            var user = ECOsystem.Utilities.Session.GetUserInfo();
            using (var dba = new DataBaseAccess())
            {
                id = dba.ExecuteScalar<string>("[dbo].[Sp_AspNetRoles_AddOrEdit]",
                new
                {
                    model.RoleId,
                    model.Name,
                    model.Parent,
                    CustomerId = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser == true ? null : ECOsystem.Utilities.Session.GetCustomerId(),
                    PartnerId = ECOsystem.Utilities.Session.GetUserInfo().IsCoincaUser == true ? null : ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId,
                    user.UserId
                });
            }
            return string.IsNullOrEmpty(model.RoleId) ? id : model.RoleId;
        }

        /// <summary>
        /// RetrieveLastRoleCreated
        /// </summary>
        /// <param name="RoleName"></param>
        /// <returns></returns>
        public string RetrieveLastRoleCreated(string RoleName)
        {
            var user = ECOsystem.Utilities.Session.GetUserInfo();
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[dbo].[Sp_ASPNetLastRoleCreated_Retrieve]",
                new
                {
                    RoleName,
                    user.UserId
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void DeleteRoles(string id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[dbo].[Sp_AspNetRoles_Delete]",
                new
                {
                    RoleId = id
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RoleName"></param>
        /// <returns></returns>
        public IEnumerable<UsersRoles> RetrieveRolesByProfile(string RoleName)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<UsersRoles>("[dbo].[Sp_ASPNetRolesByProfile_Retrieve]", new
                {
                    RoleName
                });
            }
        }

		/// <summary>
		/// RoleName
		/// </summary>
		/// <param name="RoleId"></param>
		/// <returns></returns>
		public string RetrieveMasterRoleByRole(string RoleId)
        { 
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[dbo].[Sp_ASPNetMasterRole_Retrieve]", new
                {
                    RoleId
                });
            }
        }


		/// <summary>
		/// 
		/// </summary>
		/// <param name="ids"></param>
		/// <param name="ModuleId"></param>
		/// <param name="Role"></param>
        public void AddOrEditPermissionsbyRoles(List<UsersPermissions> ids,int ModuleId, string Role)
        {
			if (ids.Count == 0)
			{
				UsersPermissions per = new UsersPermissions();
				per.Role = Role;
				ids.Add(per);
			}
			var Xml = ECOsystem.Utilities.Miscellaneous.GetXML<UsersPermissions>(ids);
			using (var dba = new DataBaseAccess())
            {
                //var primera = true;

                //if (ids.Count > 0)
                //{
					
					dba.ExecuteNonQuery("[dbo].[Sp_ASPNetOptionByRole_AddOrEdit]", new
					{
						Xml,
						ModuleId
					});


					//foreach (var item in ids)
     //               {
     //                   dba.ExecuteNonQuery("Sp_ASPNetOptionByRole_Update", new
     //                   {
     //                       item.MenuId,
     //                       item.View,
     //                       item.Edit,
     //                       item.Delete,
     //                       item.Role,
     //                       ModuleId,
     //                       Primera = primera
     //                   });
     //                   primera = false;
     //               }
     //           }
     //           else
     //           {
     //               var item = new UsersPermissions();
     //               dba.ExecuteNonQuery("Sp_ASPNetOptionByRole_Update", new
     //               {
     //                   item.MenuId,
     //                   item.View,
     //                   item.Edit,
     //                   item.Delete,
     //                   Role,
     //                   ModuleId,
     //                   Primera = primera
     //               });
                //}    
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="roleId"></param>
        public void DeleteModulebyRole(int moduleId, string roleId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("Sp_ASPNetModulebyRole_Delete", new 
                { 
                    moduleId,
                    role = roleId
                });

            }
        }
        
        /// <summary>
        /// OHG - 4Geeks - Para obtener los customer por permisos de usuario.
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<CustomerByPartnerData> RetrieveCustomerAuthByPartner(int? FilterType, int? userId, string roleName = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomerByPartnerData>("[dbo].[Sp_GetCustomerAuthByPartner]",
                new
                {
                    RoleName = roleName,
                    UserID = userId
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleName"></param>
        /// <param name="selectValue"></param>
        /// <param name="roleNameTemp"></param>
        /// <returns></returns>
        public static IEnumerable<CustomerByPartnerData> RetrieveCustomerAuthByPartnerStatic(int? userId, string roleName = null, string selectValue = null, string roleNameTemp = null)
        {
            if (!string.IsNullOrEmpty(roleNameTemp))
                roleName = roleNameTemp;


            using (var dba = new DataBaseAccess())
            {
                IEnumerable<CustomerByPartnerData> Result = dba.ExecuteReader<CustomerByPartnerData>("[dbo].[Sp_GetCustomerAuthByPartner]",
                                                                             new
                                                                             {
                                                                                 RoleName = roleName,
                                                                                 UserID = userId
                                                                             });
                foreach (var item in Result)
                    item.RoleNameTemp = roleNameTemp.Replace(" ", "_");

                return Result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleName"></param>
        /// <param name="selectValue"></param>
        /// <param name="roleNameTemp"></param>
        /// <returns></returns>
        public static IEnumerable<CustomerByPartnerData> RetrievePartnerAuthByPartnerStatic(int? userId, string roleName = null, string selectValue = null, string roleNameTemp = null)
        {
            if (!string.IsNullOrEmpty(roleNameTemp))
                roleName = roleNameTemp;


            using (var dba = new DataBaseAccess())
            {
                IEnumerable<CustomerByPartnerData> Result = dba.ExecuteReader<CustomerByPartnerData>("[dbo].[Sp_ASPNetPartnersByPartner_Retrieve]",
                                                                             new
                                                                             {

                                                                                 PartnerUserId = userId
                                                                             });
                foreach (var item in Result)
                    item.RoleNameTemp = roleNameTemp.Replace(" ", "_");

                return Result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleName"></param>
        /// <param name="selectValue"></param>
        /// <param name="roleNameTemp"></param>
        /// <returns></returns>
        public static IEnumerable<CustomerByPartnerData> CustomerByCustomerRetrieveStatic(int? userId, string roleName = null, string selectValue = null, string roleNameTemp = null)
        {
            if (!string.IsNullOrEmpty(roleNameTemp))
                roleName = roleNameTemp;


            using (var dba = new DataBaseAccess())
            {
                IEnumerable<CustomerByPartnerData> Result = dba.ExecuteReader<CustomerByPartnerData>("[General].[Sp_CustomerByCustomer_Retrieve]",
                                                                             new
                                                                             {
                                                                                 //RoleName = roleName,
                                                                                 UserId = userId
                                                                             });
                foreach (var item in Result)
                    item.RoleNameTemp = roleNameTemp.Replace(" ", "_");

                return Result;
            }

        }
   
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static IEnumerable<UsersRoles> RetrieveRolesByUsers (int UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<UsersRoles>("[dbo].[Sp_ASPNetRolesByUser_Retrieve]", new
                {
                    UserId
                });
            }
        }

        /// <summary>
		/// Get the node object
		/// </summary>
		/// <param name="childs">Node information</param>
		/// <param name="per">permissions</param>
		/// <param name="val">values</param>
		/// <returns>json object equivalent to childs</returns>
		private object RetrieveJson(AccountMenus childs, object[] per, object[] val)
		{
			if (per != null)
			{
				if (val.Count() == per.Where(x => !string.IsNullOrEmpty(x.ToString())).Count())
				{
					return new { text = " " + childs.Name, id = childs.MenuId.ToString(), state = new { @checked = "true" }, nodes = new List<object>() };
				}
			}
			return new { text = " " + childs.Name, id = childs.MenuId.ToString(), state = new { @checked = "" }, nodes = new List<object>() };
		}
		
        /// <summary>
		/// Generate nodes for See, Edit and Delete
		/// </summary>
		/// <param name="childs">Node information</param>
		/// <param name="per">permissions</param>
		/// <param name="val">values</param>
		/// <param name="list">Lista donde se almacenaran los noso</param>
		private void RetrieveNodes(AccountMenus childs, object[] per, object[] val, List<object> list)
		{
			if (val != null)
			{
				if (val.Any() && (string)val[0] == "V")
				{

					if (per != null && per.Any() && (string)per[0] == "V")
					{
							list.Add(new { text = " Ver", id = childs.MenuId.ToString() + ".1", state = new { @checked = "true" } });
					}
					else
					{
						list.Add(new { text = " Ver", id = childs.MenuId.ToString() + ".1" });
					}
				}

				if (val.Count() >= 2 && (string)val[1] == "A" || val.Count() >= 3 && (string)val[2] == "ED")
				{
					if (per != null && (per.Count() >= 2 && (string)per[1] == "A" || per.Count() >= 3 && (string)per[2] == "ED"))
					{
							list.Add(new { text = " Agregar/Editar", id = childs.MenuId.ToString() + ".2", state = new { @checked = "true" } });
					}
					else
					{
						list.Add(new { text = " Agregar/Editar", id = childs.MenuId.ToString() + ".2" });
					}
				}

				if (val.Count() >= 4 && (string)val[3] == "E")
				{
					if (per != null && per.Count() >= 4 && (string)per[3] == "E")
					{
							list.Add(new { text = " Eliminar", id = childs.MenuId.ToString() + ".3", state = new { @checked = "true" } });
					}
					else
					{
						list.Add(new { text = " Eliminar", id = childs.MenuId.ToString() + ".3" });
					}

				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="tree"></param>
		/// <returns></returns>
		public List<object> GetPermissionList(List<AccountMenus> tree)
		{
			var resultList = new List<object>();
			tree.ForEach(t =>
			{
				if (t.IsParent)
				{
					var countCheckedMiddleParents = 0;
					var countChildsMiddleParents = tree.Count(m => Convert.ToInt32(m.Parent) == t.MenuId);

					var headJson1 = new { text = t.Name, id = "", state = new { @checked = "" }, nodes = new List<Object>() };

					tree.Where(m => Convert.ToInt32(m.Parent) == t.MenuId).ToList().ForEach(childs =>
					{
						var per = childs.IsChecked?.Split(',');
						var val = childs.ValidFields?.Split(',');
						dynamic json = RetrieveJson(childs, per, val);
						RetrieveNodes(childs, per, val, json.nodes);
						headJson1.nodes.Add(json);
					});

					countCheckedMiddleParents = headJson1.nodes.Count(node => ((dynamic)node).state.@checked == "true");
					if (countChildsMiddleParents == countCheckedMiddleParents)
					{
						if (countCheckedMiddleParents == 0)
						{
							var per = t.IsChecked?.Split(',');
							var val = t.ValidFields?.Split(',');
							dynamic json = RetrieveJson(t, per, val);

							RetrieveNodes(t, per, val, json.nodes);
						}
						else
						{
							resultList.Add(new { text = " " + headJson1.text, id = headJson1.id.ToString(), state = new { @checked = "true" }, headJson1.nodes });
						}						
					}
					else
					{
						resultList.Add(headJson1);
					}
				}
				if (!t.IsParent && t.MenuId == t.Parent)
				{
					var per = t.IsChecked?.Split(',');
					var val = t.ValidFields?.Split(',');
					dynamic json = RetrieveJson(t, per, val);

					RetrieveNodes(t, per, val, json.nodes);

					resultList.Add(json);

				}
			});

			return resultList;
		}
        
        public void AddRoleCustomer(int CustomerId, string RoleId, int CustPartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[dbo].[Sp_AddRoleCustomer_AddOrEdit]",
                    new
                    {
                        RoleId,
                        ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId,
                        CustomerId,
                        CustPartnerId,
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        public void DeleteRoleCustomer(int CustomerId, string RoleId, int CustPartnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[dbo].[Sp_RoleCustomer_Delete]",
                    new
                    {
                        RoleId,
                        ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId,
                        CustomerId,
                        CustPartnerId
                    });
            }
        }

        public List<Customers> RoleByCustomerRetrieve(string RoleId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Customers>("[dbo].[Sp_RoleByCustomer_Retrieve]",
                    new
                    {
                        RoleId,
                        ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId
                    }).ToList();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }


    }
}
