﻿using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;

namespace ECOsystem.Business.Core
{
    public class EmailTemplateBusiness
    {
        public static IEnumerable<EmailTemplateModels> Retrieve(int? customerId, int? partnerId, int templateId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<EmailTemplateModels>("[General].[Sp_GetHTMLByEmailTemplate]",
                    new
                    {
                        EmailTemplatesTypeId = templateId,
                        PartnerId = partnerId,
                        CustomerId = customerId
                    });
            }
        }
    }
}