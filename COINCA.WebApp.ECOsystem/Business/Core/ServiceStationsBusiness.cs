﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// Service Station Business
    /// </summary>
    public class ServiceStationsBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve the name of the Canton
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetCantonNameById(int id)
        {
            using (var business = new GeographicDivisionBusiness())
            {
                var model = business.RetrieveCounties(id, null, null).FirstOrDefault();
                return model.Name;
            }
        }

        /// <summary>
        ///  Retrieve the name of the Province
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetProvinceNameById(int id)
        {
            using (var business = new GeographicDivisionBusiness())
            {
                var model = business.RetrieveStates(id, null).FirstOrDefault();
                return model.Name;
            }
        }
        
        /// <summary>
        /// Retrieve the States based on the StatesId
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static IEnumerable<States> GetStates(int? countryId)
        {
            using (var business = new GeographicDivisionBusiness())
            {
                return business.RetrieveStates(null, countryId);               
            }            
        }

        /// <summary>
        /// Retrieve the States based on the CountiesId
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static IEnumerable<Counties> GetCounties(int? stateId, int? countryId)
        {
            using (var business = new GeographicDivisionBusiness())
            {
                return business.RetrieveCounties(null, stateId, countryId);
            }
        }

        /// <summary>
        /// Return a list of States in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetStatesServiceStations(int? countryId)
        {
            var dictionary = new Dictionary<int?, string>();

            using (var business = new GeographicDivisionBusiness())
            {
                var result = business.RetrieveStates(null, countryId);
                foreach (var r in result)
                {
                    dictionary.Add(r.StateId, r.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        /// <summary>
        /// Load Counties for the Edit Service Statios
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static SelectList GetCountiesEditServiceStatios(int? stateId, int? countryId)
        {
            var dictionary = new Dictionary<int?, string>();
            if (stateId != null)
            {
                using (var business = new GeographicDivisionBusiness())
                {
                    var result = business.RetrieveCounties(null, stateId, countryId);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CountyId, r.Name);
                    }
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        /// <summary>
        /// Service Statios AddOrEdit
        /// </summary>
        /// <param name="model"></param>
        public void ServiceStationsAddOrEdit(ServiceStations model)
        {
            var users = Session.GetUserInfo();
            var country = Session.GetPartnerInfo();
            int? CustomerId = Session.GetCustomerId() != null ?  Session.GetCustomerId() : null;

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ServiceStations_AddOrEdit]", new
                {
                    model.ServiceStationId,
                    model.BacId,
                    model.Name,
                    model.Address,
                    model.CantonId,
                    model.ProvinceId,
                    model.Latitude,
                    model.Longitude,
                    users.UserId,
                    country.CountryId,
                    model.SAPProv,
                    model.Legal_Id,
                    model.Terminal,
                    model.Number_Provider, CustomerId
                });
            }
        }

        /// <summary>
        /// Retrieve Service Stations
        /// </summary>
        /// <param name="id"></param>
        /// <param name="countryId"></param>
        /// <param name="userId"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static IEnumerable<ServiceStations> ServiceStationRetrieve(int? id, int? countryId, int? userId ,string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ServiceStations>("[General].[Sp_ServiceStations_Retrieve]", new
                {
                    ServiceStationId = id,
                    CountryId = countryId,
                    Key = key,
                    UserId = userId
                });
            }
        }

        /// <summary>
        /// Delete Service Stations
        /// </summary>
        /// <param name="SSId"></param>
        public void DeleteServiceStations(int SSId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ServiceStations_Delete]",
                    new { ServiceStationId = SSId });
            }
        }
        
        
        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
