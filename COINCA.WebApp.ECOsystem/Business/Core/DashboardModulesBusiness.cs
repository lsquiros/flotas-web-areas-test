﻿/************************************************************************************************************
*  File    : DashboardModulesBusiness.cs
*  Summary : Dashboard Modules Business
*  Author  : Manuel Azofeifa Hidalgo
*  Date    : 02/Dic/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;

namespace ECOsystem.Business.Core
{
    /// <summary>
    /// Dashboard Modules Business
    /// </summary>
    public class DashboardModulesBusiness : IDisposable
    {

        /// <summary>
        /// Retrieve Dashboard Core Indicators
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public DashboardCoreIndicators RetrieveDashboardIndicator(int CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                DashboardCoreIndicators result = dba.ExecuteReader<DashboardCoreIndicators>("[General].[Sp_DashboardCoreIndicators_Retrieve]",
                    new
                    {
                        CustomerId
                    }).FirstOrDefault();
                if (result != null)
                {
                    List<DashboardFuelAvailable> FuelAvailable = dba.ExecuteReader<DashboardFuelAvailable>("[Control].[Sp_FuelsByCredit_Retrieve]",
                        new
                        {
                            CustomerId = Session.GetCustomerId()
                        }).ToList();
                    result.FuelAvailableAmmount = "[";
                    int count = 0;
                    foreach (var x in FuelAvailable)
                    {
                        if (count > 0) 
                        {
                            result.FuelAvailableLabels = result.FuelAvailableLabels + ",";
                            result.FuelAvailableAmmount = result.FuelAvailableAmmount + ",";
                        }
                        result.FuelAvailableLabels = result.FuelAvailableLabels + x.FuelName;
                        result.FuelAvailableAmmount = result.FuelAvailableAmmount + x.Available;
                        count = count + 1;
                    }
                    result.FuelAvailableAmmount = result.FuelAvailableAmmount + "]";
                }
                return result;
            }
        }


        /// <summary>
        ///  Retrieve Dashboard Modules
        /// </summary>
        /// <param name="CustomerId">Customer Unique Identifier</param>
        public IEnumerable<DashboardModule> RetrieveDashboarModules(int CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<DashboardModule>("[General].[Sp_DashboardModuleByCustomer_Retrieve]",
                    new
                    {
                        CustomerId
                    });
            }
        }

        /// <summary>
        ///  Performs the maintenance Insert or Update of the entity Dashboard Module
        /// </summary>
        /// <param name="model"></param>
        public void AddOrEditDashboardModule(DashboardModule model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_DashboardModuleByCustomer_AddOrEdit]",
                    new
                    {
                        model.CustomerId,
                        model.DashboardModuleId,
                        model.Order,
                        model.LoggedUserId,
                        model.RowVersion
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
