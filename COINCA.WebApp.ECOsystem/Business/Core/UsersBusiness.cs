﻿/************************************************************************************************************
*  File    : UsersBusiness.cs
*  Summary : Users Business Methods
*  Author  : Berman Romero
*  Date    : 09/10/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;
using ECOsystem.Models.Identity;
using ECOsystem.Utilities;
using Microsoft.AspNet.Identity;
using System.Data;
using ECOsystem.Business.Utilities;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Configuration;

namespace ECOsystem.Business.Core
{

    /// <summary>
    /// Users Business class performs all related to users business logic
    /// </summary>
    public class UsersBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Users information
        /// </summary>
        /// <param name="userId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <param name="userName">Allows to retrieve information from users by a particular user name</param>
        /// <param name="customerId">Current CustomerId of the logged User</param>
        /// <param name="partnerId">Current partnerId of the logged User</param>
        /// <param name="customerId2">customerId2 for drivers users</param>
        /// <param name="opc"></param>
        /// <param name="isList"></param>
        /// <param name="bindObtPhoto">Indicate to obtain the corresponding image to the user or client</param>
        /// <returns>A IEnumerable collection of the Users entity model</returns>
        public IEnumerable<Users> RetrieveUsers(int? userId, string key = null, string userName = null, int? customerId = null, int? partnerId = null, int? customerId2 = null, int? UserStatus = null, bool opc = false, bool isList = false, bool bindObtPhoto = false, bool IsCustomer = false, bool LoadSingleUser = false)
         {            
            if(UserStatus == 2)
            {
                UserStatus = null;
            }
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<Users>("[General].[Sp_Users_Retrieve]",
                    new
                    {
                        UserId = userId,
                        Key = TripleDesEncryption.Encrypt(key),
                        UserName = userName,
                        CustomerId = customerId,
                        CustomerId2 = customerId2,
                        PartnerId = partnerId,
                        bindObtPhoto,
                        UserStatus,
                        IsCustomer, 
                        LoadSingleUser
                    }).Where(x => key == null || (x.DecryptedName.ToLower().Contains(key.ToLower()) || x.DecryptedEmail.ToLower().Contains(key.ToLower()) || x.DecryptedPhoneNumber.Contains(key) || x.DecryptedUserName.ToLower().Contains(key.ToLower())
                                                  || x.DecryptedIdentification.ToLower().Contains(key.ToLower())));
                
                if (result.Count() > 0)
                {
                    if (!isList && result.FirstOrDefault().PartnerId != null && result.FirstOrDefault().PartnerId != 0)
                    {
                        foreach (var t in result.Where(t => t.IsPartnerUser))
                        {
                            t.PartnerUser = new PartnerUsers();
                            t.PartnerUser.CountriesAccessList = dba.ExecuteReader<PartnerUsersByCountry>("[General].[Sp_PartnerUsersByCountry_Retrieve]", new { t.PartnerUserId });
                        }
                        result.Where(x => x.PartnerUser == null).ToList().ForEach(s => s.PartnerUser = new PartnerUsers());
                        result.Where(x => x.PartnerUser != null).ToList().Where(x => x.PartnerUser.CountriesAccessList == null).ToList()
                            .ForEach(s => s.PartnerUser.CountriesAccessList = new List<PartnerUsersByCountry>());

                    }
                }                

                return result.OrderBy(o => o.DecryptedName).ToList();
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="customerId"></param>
		/// <param name="partnerId"></param>
		/// <param name="UserStatus"></param>
		/// <returns></returns>
		public IEnumerable<Users> RetrieveDynamicFilterUsers(int? userId, int? customerId = null, int? partnerId = null, int? UserStatus = null)
		{
			using (var dba = new DataBaseAccess())
			{
				var result2 = dba.ExecuteReader<Users>("[General].[Sp_DynamicFilterUsers_Retrieve]",
					new
					{
						UserId = userId,
						CustomerId = customerId,
						PartnerId = partnerId,
						UserStatus,
					});

				return result2;
			}
		}

		/// <summary>
		/// Update Import Driver
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public string UpdateImportDriver(Users model)
        {
            var UserId = ECOsystem.Utilities.Session.GetUserInfo().UserId;
            var CustomerId = ECOsystem.Utilities.Session.GetCustomerId();

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[UpdateImportDrivers]", new {
                    UserName = model.EncryptedName,
                    Identification = model.EncryptedIdentification,
                    License = model.IsAgent == true || model.EncryptedLicense == null || model.EncryptedLicense == "" ? model.EncryptedIdentification : model.EncryptedLicense,
                    Code = model.EncryptedCode,
                    model.IsActive,
                    Email = model.EncryptedEmail,
                    PhoneNumber = model.EncryptedPhoneNumber,                        
                    model.LicenseExpiration,  
                    Dallas = model.DallasId,
                    DailyTransactionLimit = model.IsAgent == true || model.DailyTransactionLimit == null ? 0 : model.DailyTransactionLimit,
                    CustomerId,
                    UserId,
                    model.IsAgent
                });
            }        
        }

        /// <summary>
        /// Retrieve Partner Users By Country
        /// </summary>
        /// <param name="partnerUserId">partnerUserId</param>
        /// <returns>List of PartnerUsersByCountry</returns>
        public IList<PartnerUsersByCountry> RetrievePartnerUsersByCountry(int partnerUserId = 0)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PartnerUsersByCountry>("[General].[Sp_PartnerUsersByCountry_Retrieve]", new { PartnerUserId = partnerUserId });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Users
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <param name="userManager">userManager</param>
        /// <param name="url">url</param>
        /// <param name="request">request</param>
        /// <returns>void</returns>
        public void AddOrEditUsers(Users model, ApplicationUserManager userManager, UrlHelper url, HttpRequestBase request)
        {
            // Get product type

            // For Coinca user use: ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId
            int partnerId = Session.GetPartnerId() == null ? (int)ECOsystem.Utilities.Session.GetPartnerInfo().PartnerId : (int)Session.GetPartnerId();
            var productType = Session.GetProductInfoByPartner(partnerId);

            var user = userManager.FindByName(model.UserId == null ? model.EncryptedEmail : model.EncryptedUserName);

            if (model.UserId == null) //Create User
            {
                if (user != null && userManager.IsEmailConfirmed(user.Id))
                    //throw new Exception("ERROR EXIST_USR: There is an user already defined with username = '" + model.DecryptedEmail + "'");
                    throw new Exception("ERROR EXIST_USR: Ya existe un usuario con este nombre = '" + model.DecryptedEmail + "'");

                //Create User in ASP.NET Auth
                var newUser = new ApplicationUser { UserName = model.EncryptedEmail, Email = model.EncryptedEmail, PhoneNumber = model.EncryptedPhoneNumber, LockoutEnabled = true };
                var result = userManager.Create(newUser, ConfigurationManager.AppSettings["DEFAULTPASSWORD"]);
                if (!result.Succeeded)
                    //throw new Exception("ERROR EXIST_USR: There is an user already defined with username = '" + model.DecryptedEmail + "'");
                    throw new Exception("ERROR EXIST_USR: Ya existe un usuario con este nombre = '" + model.DecryptedEmail + "'");
                model.DecryptedUserName = model.DecryptedEmail;
                model.ChangePassword = true;
                model.IsLockedOut = false;

                //Create User in ECOsystem
                var userId = ExecuteAddOrEditUsers(model);
                model.UserId = userId;
                AddOrEditAditionalUserInfo(userId, model);

                //Validate DallasKey 
                if (model != null && model.DallasId != null)
                {
                    using (var businessDK = new DallasKeysBusiness())
                    {
                        if (model.DallasId > 0)
                        {
                            businessDK.AddOrEditDallasKeysByDriver((int)model.UserId, (int)model.DallasId);
                        }
                    }
                }                

                //Validate ProductType
                if (productType == ProductInfo.LineVitaECO)
                {
                    //Consume Control Car API
                    this.GeneratePasswordReset(model.EncryptedEmail);
                }
                else
                {
                    //Send email to confirm the email account
                    var code = userManager.GenerateEmailConfirmationToken(newUser.Id);
                    
                    var callbackUrl = new UriBuilder(url.Action("ConfirmEmail", "Account", new { userId = newUser.Id, code, email = newUser.Email }, request.Url.Scheme));
                    
                    callbackUrl.Path = "/Account/ConfirmEmail";

                    new EmailSender().SendEmail(model.DecryptedEmail, "Confirmar su cuenta de correo electrónico", "<h3>Notificación de la Plataforma</h3><br>Por favor confirme su correo electrónico haciendo click <a href=\"" + callbackUrl + "\">aquí</a><br><br><hr>Namutek<br><small>namutek.com</small>");
                }
            }
            else // Update User
            {
                if (user == null)
                    throw new Exception("ERROR UPDT_USR: Not exists an user defined with username = '" + model.DecryptedEmail + "'");

                user.PhoneNumber = model.EncryptedPhoneNumber;
                user.Email = model.EncryptedEmail;
                user.UserName = (user.UserName != model.EncryptedEmail) ? model.EncryptedEmail : user.UserName;
                var result = userManager.Update(user);
                if (!result.Succeeded)
                {

                    foreach (string err in result.Errors)
                    {
                        if (err.Contains("already taken"))
                        {
                            throw new Exception("ERROR EXIST_USR: Already exists an user defined with username = '" + model.DecryptedEmail + "'");
                        }
                    }

                    throw new Exception("ERROR UPDT_USR: Not exists an user defined with username = '" + model.DecryptedEmail + "'");
                }

                var aspuser = userManager.FindByName(model.EncryptedUserName);
                var token = userManager.GeneratePasswordResetToken(aspuser.Id);

                if ( model.ChangePrevPassword && !String.IsNullOrEmpty(model.PrevPasswordTemp))
                {
                    userManager.ResetPassword(user.Id, token, model.PrevPasswordTemp);

                    this.AddPasswordsHistory(
                        new PasswordsHistoryModel() {
                            Password = model.PrevPasswordTemp,
                            PasswordDate = DateTime.Now,
                            UserId = (int) model.UserId
                        },
                        0
                    );
                }

                if (model.ChangePassword)
                {
                    //Validate ProductType
                    if (productType == ProductInfo.LineVitaECO)
                    {
                        //Consume Control Car API
                        this.GeneratePasswordReset(model.EncryptedEmail);
                    }
                    else
                    {                      
                        if (String.IsNullOrEmpty(model.PrevPasswordTemp))
                            userManager.ResetPassword(user.Id, token, ConfigurationManager.AppSettings["DEFAULTPASSWORD"]);

                        //Send email to confirm the email account
                        var code = userManager.GenerateEmailConfirmationToken(user.Id);
                        var callbackUrl = new UriBuilder(url.Action("ConfirmEmail", "Account", new { userId = user.Id, code, email = user.Email }, request.Url.Scheme));
                        callbackUrl.Path = "/Account/ConfirmEmail";

                        EmailSender.SendEmail(model.DecryptedEmail, "Notificación - Cambio de Contraseña", 1, callbackUrl.ToString());
                    }
                }

                //Update User Info in ECOsystem
                AddOrEditAditionalUserInfo(ExecuteAddOrEditUsers(model), model);

                //Validate DallasKey 
                if (model != null && model.DallasId != null)
                {
                    using (var businessDK = new DallasKeysBusiness())
                    {
                        if (model.DallasId > 0)
                        {
                            businessDK.AddOrEditDallasKeysByDriver((int)model.UserId, (int)model.DallasId);
                        }
                        else
                        {
                            var resultRemove = businessDK.RemoveDallasKeys((int)model.UserId);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Add Or Edit Users With Out Callback
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userManager"></param>
        /// <param name="userId"></param>
        public void AddOrEditUsersWithOutCallback(Users model, UserManager<ApplicationUser> userManager, ref int userId)
        {
            var user = userManager.FindByName(model.UserId == null ? model.EncryptedEmail : model.EncryptedUserName);
            //var userId = 0;

            if (model.UserId == null) //Create User
            {
                if (user != null && userManager.IsEmailConfirmed(user.Id))
                    throw new Exception("[USER_EXISTS] Ya existe un usuario con el correo: '" + model.DecryptedEmail + "', por favor validar la información suministrada.");

                //Create User in ASP.NET Auth
                var newUser = new ApplicationUser { UserName = model.EncryptedEmail, Email = model.EncryptedEmail, PhoneNumber = model.EncryptedPhoneNumber, LockoutEnabled = true };
                var result = userManager.Create(newUser, ConfigurationManager.AppSettings["DEFAULTPASSWORD"]);
                if (!result.Succeeded)
                {
                    var errors = string.Join(" | ", result.Errors);
                    throw new Exception("[CREATE_USER_ERROR] Se encontraron errores en la creación de usuario con el correo: '" + model.DecryptedEmail + "'. Errores: " + errors);
                }

                model.DecryptedUserName = model.DecryptedEmail;
                model.DriverUser.LicenseExpiration = DateTime.Now;
                model.ChangePassword = true;
                model.IsLockedOut = false;

                //Create User in ECOsystem
                userId = ExecuteAddOrEditUsers(model);
                model.UserId = userId;
                AddOrEditAditionalUserInfo(userId, model);

            }
            else // Update User
            {
                if (user == null)
                    throw new Exception("[NOT_EXIST_USER] No existe un usuario con el siguiente correo: '" + model.DecryptedEmail + "', por favor validar la información suministrada.");

                user.PhoneNumber = model.EncryptedPhoneNumber;
                user.Email = model.EncryptedEmail;
                user.UserName = (user.UserName != model.EncryptedEmail) ? model.EncryptedEmail : user.UserName;

                using (var dba = new DataBaseAccess())
                {
                    var result = dba.ExecuteScalar<string>("[General].[Sp_UserLineVita_Update]",
                            new
                            {
                                UserId = model.UserId,
                                UserName = user.UserName,
                                Email = user.Email,
                                PhoneNumber = user.PhoneNumber
                            });

                    switch (result)
                    {
                        case "NOT_EXIST_USER":
                            throw new Exception("[NOT_EXIST_USER] No se logró actualizar el usuario con el correo '" + model.DecryptedEmail + "', no existe en la plataforma.");
                        default:
                            break;
                    }
                }

                //Update User Info in ECOsystem
                model.EncryptedEmail = user.Email;
                model.EncryptedUserName = user.UserName;
                model.ChangePassword = false;
                model.IsActive = true;

                AddOrEditAditionalUserInfo(ExecuteAddOrEditUsers(model), model);
            }
        }

        /// <summary>
        /// Add Vehicle References To Customer Users
        /// </summary>
        /// <param name="customerId"></param>
        public void AddVehicleReferencesToCustomerUsers(int customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_AddVehicleReferencesToCustomerUsers]", new
                {
                    CustomerId = customerId
                });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Users
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <param name="userManager"></param>
        /// <param name="url"></param>
        /// <param name="request"></param>
        /// <returns>void</returns>
        public void AddUser(Users model, ApplicationUserManager userManager, UrlHelper url, HttpRequestBase request)
        {
            var user = userManager.FindByName(model.UserId == null ? model.EncryptedEmail : model.EncryptedUserName);

            if (user != null && userManager.IsEmailConfirmed(user.Id))
                throw new Exception("There is an user already defined with username = '" + model.DecryptedEmail + "'");

            var newUser = new ApplicationUser { UserName = model.EncryptedEmail, Email = model.EncryptedEmail, PhoneNumber = model.EncryptedPhoneNumber, LockoutEnabled = true };
            var result = userManager.Create(newUser, ConfigurationManager.AppSettings["DEFAULTPASSWORD"]);
            if (!result.Succeeded)
                throw new Exception("There is an user already defined with username = '" + model.DecryptedEmail + "'");

            model.DecryptedUserName = model.DecryptedEmail;
            model.ChangePassword = true;
            model.IsLockedOut = false;

            AddOrEditAditionalUserInfo(ExecuteAddOrEditUsers(model), model);

            //Send email to confirm the email account
            var code = userManager.GenerateEmailConfirmationToken(newUser.Id);
            //var primaryUri = new UriBuilder(ConfigurationManager.AppSettings["ECOSYSTEM_URI_BASE"]);
            var callbackUrl = new UriBuilder(url.Action("ConfirmEmail", "Account", new { userId = newUser.Id, code, email = newUser.Email }, request.Url.Scheme));
            //callbackUrl.Host = primaryUri.Host;
            //callbackUrl.Port = primaryUri.Port;
            callbackUrl.Path = "/Account/ConfirmEmail";
            new EmailSender().SendEmail(model.DecryptedEmail, "Confirmar su cuenta de correo electrónico", "<h3>Notificación de la Plataforma</h3><br>Por favor confirme su correo electrónico haciendo click <a href=\"" + callbackUrl + "\">aquí</a><br><br><hr>Namutek<br><small>namutek.com</small>");
        }

        /// <summary>
        /// Add Or Edit Aditional User Info
        /// </summary>
        /// <param name="userId">userId</param>
        /// <param name="model">model</param>
        private void AddOrEditAditionalUserInfo(int userId, Users model)
        {
            using (var dba = new DataBaseAccess())
            {
                if (Session.GetProfileByRole(model.RoleId) == "SERVICESTATION_ADMIN")
                {
                    dba.ExecuteNonQuery("[Stations].[Sp_ServiceStationsUsersByPartner_AddOrEdit]", new
                    {
                        UserId = userId,
                        PartnerId = ECOsystem.Utilities.Session.GetPartnerId(),
                        model.LoggedUserId
                    });
                }
                else
                {
                    if (model.IsCustomerUser)
                    {
                        dba.ExecuteNonQuery("[General].[Sp_CustomerUsers_AddOrEdit]", new
                        {
                            UserId = userId,
                            model.CustomerUserId,
                            CustomerId = model.CustomerId ?? Session.GetCustomerId(),
                            Identification = model.EncryptedIdentification,
                            model.LoggedUserId,
                            model.ChangeOdometer
                        });
                    }
                    if (model.IsDriverUser)
                    {
                        dba.ExecuteNonQuery("[General].[Sp_DriversUsers_AddOrEdit]", new
                        {
                            UserId = userId,
                            model.DriversUserId,
                            CustomerId = model.CustomerId ?? Session.GetCustomerId(),
                            Identification = model.EncryptedIdentification,
                            Code = model.EncryptedCode,
                            License = model.IsAgent == true || model.EncryptedLicense == null || model.EncryptedLicense == "" ? model.EncryptedIdentification : model.EncryptedLicense,
                            model.LicenseExpiration,
                            Dallas = model.EncryptedDallas,
                            DailyTransactionLimit = model.IsAgent == true || model.DailyTransactionLimit == null ? 0 : model.DailyTransactionLimit,
                            model.LoggedUserId,
                            model.CardRequestId,
                            model.CostCenterId,
                            model.PullPreviousBudget //update budget wiith previus. 
                        });
                    }

                    if (model.IsPartnerUser && !model.IsSupportUser)
                    {
                        var doc = new XDocument(new XElement("xmldata"));
                        var root = doc.Root;
                        if (model.PartnerUser.CountriesAccessList != null)
                        {
                            foreach (var item in model.PartnerUser.CountriesAccessList.Where(x => x.IsCountryChecked))
                            {
                                root.Add(new XElement("Country", new XAttribute("CountryId", item.CountryId)));
                            }
                            model.PartnerUser.CountriesAccessXml = doc.ToString();
                        }
                        else
                        {
                            model.PartnerUser.CountriesAccessXml = "";
                        }
                        int? partnerId = model.PartnerId == 0 ? null : (int?)model.PartnerId;
                        partnerId = Session.GetPartnerId() != null ? Session.GetPartnerId() != 0 ? Session.GetPartnerId() : partnerId : partnerId;
                        dba.ExecuteNonQuery("[General].[Sp_PartnerUsers_AddOrEdit]", new
                        {
                            UserId = userId,
                            model.PartnerUserId,
                            PartnerId = partnerId,
                            model.PartnerUser.CountriesAccessXml,
                            model.LoggedUserId
                        });
                    }
                    if (model.IsSupportUser)
                    {
                        dba.ExecuteNonQuery("[General].[Sp_SupportUsers_AddOrEdit]", new
                        {
                            UserId = userId,
                            PartnerId = Session.GetPartnerInfo().PartnerId,
                            InsertUserId = Session.GetUserInfo().UserId
                        });
                    }
                }
            }
        }     

        /// <summary>
        /// Execute Add Or Edit Users
        /// </summary>
        /// <param name="model"></param>
        /// <param name="loggedUserId">loggedUserId</param>
        /// <returns></returns>
        public int ExecuteAddOrEditUsers(Users model, int? loggedUserId = null)
        {
            int? CustomerId = ECOsystem.Utilities.Session.GetCustomerId();

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_Users_AddOrEdit]",
                    new
                    {
                        model.UserId,
                        Name = model.EncryptedName,
                        model.ChangePassword,
                        model.Photo,
                        model.IsActive,
                        Email = model.EncryptedEmail,
                        PhoneNumber = model.EncryptedPhoneNumber,
                        UserName = model.EncryptedUserName,
                        model.IsLockedOut,
                        model.RoleId,
                        model.RoleName,
                        model.PasswordExpirationDate,
                        LoggedUserId = loggedUserId ?? model.LoggedUserId,
                        model.RowVersion,
                        EmailConfimation = ((model.UserId == null)? model.IsEmailConfirmed : false),
                        model.IsAgent,
                        model.AddCustomers,
                        model.IsSupportUser,
                        DecryptedPhoneNumberCode = model.DecryptedPhoneNumber,
                        CustomerId,
                        model.HasWhatsApp
                    });
            }
        }


        /// <summary>
        /// Execute Add Or Edit Users
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oldUser"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public int ExecuteAddOrEditFixUsers(Users model, string oldUser, string email)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_FixUsers_AddOrEdit]",
                    new
                    {
                        OldUserName = oldUser,
                        NewUserName = model.EncryptedEmail,
                        UserEmail = email
                    });
            }
        }

        /// <summary>
        /// Performs the the operation of delete on the model users
        /// </summary>
        /// <param name="userId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        public void DeleteUsers(int userId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Users_Delete]",
                    new { UserId = userId });
            }
        }

        /// <summary>
        /// Performs the maintenance Add of the entity PasswordsHistory
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddPasswordsHistory(PasswordsHistoryModel model, Int32 Template)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_PasswordsHistory_Add]",
                    new
                    {
                        model.UserId,
                        model.Password,
                        Template
                    });
            }

        }

        /// <summary>
        /// Performs the repetition validation of PasswordsHistory
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public int CheckPasswordsHistory(PasswordsHistoryModel model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_PasswordsHistory_Check]",
                    new
                    {
                        model.UserId,
                        model.Password
                    });
            }

        }


        /// <summary>
        /// Performs the repetition validation of PasswordsHistory
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public string CheckPasswordsLowSecurityLineVitaECO(PasswordsHistoryModel model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_PasswordsHistory_Check_LineVitaECO]",
                    new
                    {
                        model.UserId,
                        model.Password
                    });
            }

        }

        /// <summary>
        /// Get profile image
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="profileId"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        public string RetrieveProfileImage(int? userId = null, int? profileId = null, string profile = null) 
        {
            try
            {
                using (var dba = new DataBaseAccess()) 
                {
                    return dba.ExecuteScalar<string>("[General].[Sp_ProfileUser_Retrieve]",
                    new
                    {
                        UserId = userId,
                        ProfileId = profileId,
                        UserType = profile
                    });
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// GetReportDownloadData
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetReportDownloadData()
        {
            try
            {
                var list = new List<Users>();
                list = (List<Users>)HttpContext.Current.Session["UsersInfo"];                
                return DriversGenerate(list);

            }
            catch (Exception e)
            {
                throw e;
            }
        } 

        /// <summary>
        /// DriversGenerate
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable DriversGenerate(IEnumerable<Users> list)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("Name");                
                dt.Columns.Add("Email");                
                dt.Columns.Add("Status");
                dt.Columns.Add("IsLockedOut");
                dt.Columns.Add("CustomerName");
                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();

                    string text = string.Empty;

                    row["Name"] = item.DecryptedName;                   
                    row["Email"] = item.DecryptedEmail;                    
                    row["Status"] = item.IsActive;
                    row["IsLockedOut"] = item.IsLockedOut;
                    row["CustomerName"] = ECOsystem.Utilities.Session.GetCustomerInfo().DecryptedName;
                    dt.Rows.Add(row);
                }
                return dt;
            }
        }

        /// <summary>
        /// Retrieve Drivers
        /// </summary>
        /// <param name="vehicleId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<DriversUsers> RetrieveDrivers(int? CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<DriversUsers>("[General].[Sp_Drivers_Retrieve]",
                new
                {
                    CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                });
            }
        }

        /// <summary>
        /// Generate Password Reset
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string GeneratePasswordReset(string email)
        {
            using (var business = new GeneralCollections())
            {
                //Obtenemos el usuario
                var user = RetrieveUsers(null, userName: email).FirstOrDefault();
                if(user == null)
                {
                    throw new Exception("[RecoveryPassword_ALERT] El correo no corresponde a ningún usuario de nuestra plataforma. Por favor digite un correo de una cuenta existente.");
                }
               
                //Obtenemos la configuración del API
                var apiConfig = business.RetrieveControlCarApiConfig();
                if (apiConfig == null)
                {
                    throw new Exception("[RecoveryPassword_ERROR] La configuración del servicio API no está definida.");
                }

                //Conectamos al servicio API para proceder a enviar las credenciales temporales
                var credentials = apiConfig.Where(w => w.Key == "API_CREDENTIALS").FirstOrDefault();
                var uri = apiConfig.Where(w => w.Key == "API_URL").FirstOrDefault();
                var endpoint = apiConfig.Where(w => w.Key == "RESET_USER_CREDENTIALS").FirstOrDefault();
                var urlParam = "/" + user.UserId.ToString();

                HttpClient client = new HttpClient();
                client.Timeout = TimeSpan.FromMinutes(2);

                string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(credentials.Value));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + svcCredentials);

                var serviceResult =  client.GetAsync(string.Format("{0}{1}{2}", uri.Value, endpoint.Value, urlParam));
                var response = serviceResult.Result;
                var responseResult = response.Content.ReadAsStringAsync();

                if(response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new Exception("[RecoveryPassword_ERROR] El servicio API ha dado error, no se puede procesar la solicitud. Error: " + responseResult.Result);
                }

                return "SUCCESS";
            }
        }


        /// <summary>
        /// Retrieve ControlCar ECO Info 
        /// </summary>
        /// <returns>An IEnumerarble String Info from ControlCar Params</returns>
        public IEnumerable<ControlCarInfo> ControlCarInfoRetrieve()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ControlCarInfo>("[General].[Sp_InfoLineVita_Retrieve]",
                    new
                    {
                        UserId = 0
                    });
            }
        }

        /// <summary>
        /// Valid if the user can use this email
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        /// <param name="userManager">userManager</param>
        /// <returns>True or false like int</returns>
        public int IsValidEmail(Users model, ApplicationUserManager userManager)
        {
            var user = userManager.FindByName(model.EncryptedEmail);

            return user == null ? 1 : 0;
        }

	/// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
