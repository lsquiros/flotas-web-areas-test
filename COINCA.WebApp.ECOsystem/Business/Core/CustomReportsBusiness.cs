﻿using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml;

namespace ECOsystem.Business.Core
{
#pragma warning disable CS1591 
    public class CustomReportsBusiness : IDisposable
    {
        public CustomReport CustomReportRetrive(int? ReportId)
        {
            using (var dba = new DataBaseAccess())
            {
                var model = new CustomReport();
                var report = GetReports().Where(x => x.Id == ReportId).FirstOrDefault();
                if (report != null)
                {
                    model.ReportId = report.Id;
                    model.ReportName = report.ReportName;
                    model.SheetName = report.SheetName;
                }
                model.ColumnList = dba.ExecuteReader<ReportColumns>("[General].[Sp_CustomReport_Retrieve]", new { ReportId }).ToList();
                return model;
            }
        }

        public void CustomReportAddOrEdit(CustomReport model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_CustomReport_AddOrEdit]",
                    new
                    {
                        model.ReportId,
                        model.ReportName,
                        model.SheetName,
                        Xml = ECOsystem.Utilities.Miscellaneous.GetXML(model.ColumnList),
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId(),
                        ECOsystem.Utilities.Session.GetUserInfo().UserId
                    });
            }
        }

        public void CustomReportDelete(int? Id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Delete_CustomReport]",
                    new
                    {
                        Id 
                    });
            }
        }




        public CustomReport GetReportInformation(int? Id)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CustomReport>("[General].[Sp_GetReportInformation]",
                    new
                    {
                        Id
                    }).FirstOrDefault();
            }
        }
        
        public string CustomReportDataRetrieve(CustomReport model)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_CustomReportData_Retrieve]",
                    new
                    {
                        model.ReportId,
                        model.Parameters.StartDate,
                        model.Parameters.EndDate,
                        CustomerId = Session.GetCustomerId(),
                        Session.GetUserInfo().UserId                        
                    });
            }
        }

        public DataSet GetReportDownloadData(CustomReport Model)
        {
            var report = CustomReportRetrive(Model.ReportId);
            var xml = CustomReportDataRetrieve(Model);
            var ds = new DataSet();

            if (report.ColumnList.ToList().Count() > 0)
            {
                using (var dt = new DataTable("CustomReport"))
                {
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("Dates");
                    dt.Columns.Add("ReportName");
                    dt.Columns.Add("PageName");

                    DataRow dr = dt.NewRow();
                    dr["CustomerName"] = Session.GetCustomerInfo().DecryptedName;
                    dr["Dates"] = string.Format("{0} - {1}", Convert.ToDateTime(Model.Parameters.StartDate).ToString("dd/MM/yyyy"), Convert.ToDateTime(Model.Parameters.EndDate).ToString("dd/MM/yyyy"));
                    dr["ReportName"] = report.ReportName;
                    dr["PageName"] = report.SheetName;
                    dt.Rows.Add(dr);

                    ds.Tables.Add(dt);
                }

                using (var dt = new DataTable("CustomDetail"))
                {
                    dt.Columns.Add("Titles");
                    dt.Columns.Add("Order");

                    foreach (var item in report.ColumnList.ToList().OrderBy(x => x.Order))
                    {
                        DataRow dr = dt.NewRow();
                        dr["Titles"] = string.IsNullOrEmpty(item.FormulaName) ? item.Name : item.FormulaName;
                        dr["Order"] = item.Order;
                        dt.Rows.Add(dr);
                    }
                    ds.Tables.Add(dt);
                }

                using (var dt = new DataTable("CustomColumnValues"))
                {
                    dt.Columns.Add("Id");
                    dt.Columns.Add("Value");                    
                    dt.Columns.Add("Order");
                    dt.Columns.Add("FinalSummation");

                    int rowVal = 1;
                    XmlDocument doc = new XmlDocument();
                    //doc.Load(new System.IO.StreamReader(xml));
                    doc.InnerXml = xml;
                    XmlElement root = doc.DocumentElement;
                    XmlNodeList nodes = root.SelectNodes("row"); // You can also use XPath here
                    
                    foreach (XmlNode node in nodes)
                    {
                        int orderVal = 1;
                        foreach (XmlNode cNode in node.ChildNodes)  //for (int index = 0; index < node.ChildNodes.Count; index++) //
                        {
                            DataRow dr = dt.NewRow();
                            dr["Id"] = rowVal;
                            dr["Order"] = orderVal;

                            foreach (var item in report.ColumnList.ToList().Where(x => x.Order.Equals(orderVal)))
                            {
                                if (item.IsEncrypted)
                                    dr["Value"] = TripleDesEncryption.Decrypt(cNode.InnerText);
                                else
                                    dr["Value"] = cNode.InnerText;

                                dr["FinalSummation"] = item.FinalSummation;
                            }

                            dt.Rows.Add(dr);
                            orderVal++;
                        }
                        // use node variable here for your beeds
                        rowVal++;
                    }
                    ds.Tables.Add(dt);
                }
            }
            else
            {
                ds.Tables.Add(new DataTable("CustomReport"));
                ds.Tables.Add(new DataTable("CustomDetail"));
                ds.Tables.Add(new DataTable("CustomColumnValues"));
            }
            return ds;
        }

        #region Collections
        public List<ReportSources> GetReportSources()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ReportSources>("[General].[Sp_ReportSources_Retrieve]", new { }).ToList();
            }
        }

        public List<ReportSources> GetReportColumns(int? id, bool? IsFormula)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ReportSources>("[General].[Sp_ReportColumns_Retrieve]",
                    new
                    {
                        ReportSourceId = id,
                        IsFormula,
						ECOsystem.Utilities.Session.GetUserInfo().CountryId
					}).ToList();
            }
        }

        public List<Reports> GetReports()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Reports>("[General].[Sp_Reports_Retrieve]",
                    new
                    {
                        CustomerId = ECOsystem.Utilities.Session.GetCustomerId()
                    }).ToList();
            }
        }
        #endregion

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
#pragma warning restore CS1591 
}