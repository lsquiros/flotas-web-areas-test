﻿/************************************************************************************************************
*  File    : HomeBusiness.cs
*  Summary : Home Business Methods
*  Author  : Manuel Azofeifa Hidalgo
*  Date    : 11/14/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
//using COINCA.Library.DataAccess;

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using ECOsystem.Models.Home;
using System.Linq;
using System.Web;

namespace ECOsystem.Business.Home
{
    /// <summary>
    /// Home Business Layer
    /// </summary>
    public class HomeBusiness : IDisposable
    {
        /// <summary>
        /// RetrieveSAIndicators
        /// </summary>
        /// <returns>Super Admin Indicators</returns>
        public IEnumerable<SAIndicators> RetrieveSAIndicators()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<SAIndicators>("[General].[Sp_SAIndicators_Retrieve]",
                    new
                    {
                    });
            }
        }

        /// <summary>
        /// Retrieve PAIndicators
        /// </summary>
        /// <returns>Super Admin Indicators</returns>
        public IEnumerable<PAIndicators> RetrievePAIndicators(int? partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<PAIndicators>("[General].[Sp_PAIndicators_Retrieve]",
                    new
                    {
                        partnerId
                    });
            }
        }

        /// <summary>
        /// RetrieveCAIndicators
        /// </summary>
        /// <returns>Customer Admin Indicators</returns>
        public IEnumerable<CAIndicators> RetrieveCAIndicators(int? CustomerId)
        {
            if (CustomerId == null)
                return new List<CAIndicators>();

            using (var dba = new DataBaseAccess())
            {
                var user = Session.GetUserInfo();

                return dba.ExecuteReader<CAIndicators>("[General].[Sp_CAIndicators_Retrieve]",
                    new
                    {
                        CustomerId,
                        user.UserId
                    });
            }
        }

        /// <summary>
        /// Retrieve Application Version
        /// </summary>
        /// <returns>Customer Admin Indicators</returns>
        public IEnumerable<AppInfo> RetrieveAppVersion()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<AppInfo>("[General].[Sp_ApplicationVersion_Retrieve]",new { });
            }
        }

        /// <summary>
        /// Get the information for the dashboard
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DashboarPartnerModel> GetDashboardPartnerIndicatos()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<DashboarPartnerModel>("[General].[Sp_DashBoardPartner_Retrieve]",
                new
                {
                    Session.GetUserInfo().PartnerId
                });
            }
        }

        /// <summary>
        /// Get the information for the dashboard
        /// </summary>
        /// <returns></returns>
        public static string GetRoleForSuperAdmin()
        {  
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_RoleForCoincaModule_Retrieve]",
                new
                {
                    Session.GetUserInfo().UserId
                });                
            }
        }

        /// <summary>
        /// Get the role for the user if the user will be VPOS and customer 
        /// </summary>
        /// <returns></returns>
        public static string GetCustomerRole()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_RoleForCustomerModule_Retrieve]",
                new
                {
                    Session.GetUserInfo().UserId
                });                
            }
        }

        /// <summary>
        /// Get the role for the VPOS
        /// </summary>
        /// <returns></returns>
        public static RoleValidateModel GetVPOSRole()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<RoleValidateModel>("[General].[Sp_RoleForVPOSModule_Retrieve]",
                new
                {
                    Session.GetUserInfo().UserId
                }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Retrieve PAIndicators
        /// </summary>
        /// <returns>Super Admin Indicators</returns>
        public static IList<FuelChange> RetrieveFuelChanges(int? PartnerId, int? CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<FuelChange>("[General].[Sp_PriceByLiterChangesAlert_Retrieve]",
                    new
                    {
                        PartnerId,
                        CustomerId
                    });
            }
        }

        /// <summary>
        /// Retrive the info of the services stations
        /// </summary>
        /// <returns>Service Stations</returns>
        public IEnumerable<ServiceStationsUser> RetrieveServiceStationUserInfo(int UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ServiceStationsUser>("[General].[Sp_ServiceStationUserInfo_Retrieve]",
                    new
                    {
                        UserId
                    });
            }
        }

        /// <summary>
        /// Retrive the info for the transaction types
        /// </summary>
        /// <returns>Service Stations</returns>
        public IEnumerable<TransactionsTypes> RetrieveTransactionsTypes()
        {
            using (var dba = new DataBaseAccess())
            {
                var IsPartner = ECOsystem.Utilities.Session.GetCustomerInfo() == null ? true : false;
                return dba.ExecuteReader<TransactionsTypes>("[Control].[TransactionsType_Retrieve]", new { IsPartner });
            }
        }

        public void EditShowNews(bool ShowNews, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_ShowNews_Edit]", 
                    new 
                    { 
                        ShowNews,
                        UserId
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}