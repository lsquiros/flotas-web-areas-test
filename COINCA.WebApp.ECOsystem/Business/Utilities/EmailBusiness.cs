﻿﻿/************************************************************************************************************
*  File    : EmailBusiness.cs
*  Summary : Email Business Methods
*  Author  : Cristian Martínez 
*  Date    : 30/Nov/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Models.Email;
//using COINCA.Library.DataAccess;

namespace ECOsystem.Business.Utilities
{
    /// <summary>
    /// Email Business class performs all related to operators business logic
    /// </summary>
    public class EmailBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve Email Encrypted
        /// </summary>
        /// <returns>IEnumerable collection of the EmailFleetCard entity model</returns>
        public IEnumerable<EmailEncryptedAlarms> RetrieveEmailEncrypted()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<EmailEncryptedAlarms>("[General].[Sp_EmailEncryptedAlarms_Retrieve]",
                    new
                    {
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Email Encrypted
        /// </summary>
        /// <param name="model"></param>
        public void AddOrEditEmailEncrypted(EmailEncryptedAlarms model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_EmailEncryptedAlarms_AddOrEdit]",
                    new
                    {
                        model.EmailId,
                        model.To,
                        model.From,
                        model.CC,
                        model.Subject,
                        model.Message,
                        model.IsHtml,
                        model.Sent,
                        LoggedUserId = 0,
                        model.RowVersion
                    });
            }
        }

        
        
        /// <summary>
        /// Retrieve Email
        /// </summary>
        /// <returns>IEnumerable collection of the Email entity model</returns>
        public IEnumerable<Email> RetrieveEmail()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Email>("[General].[Sp_Email_Retrieve]",
                    new
                    {
                    });
            }
        }

        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Email
        /// </summary>
        /// <param name="model"></param>
        public void AddOrEditEmail(Email model)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[Sp_Email_AddOrEdit]",
                    new
                    {
                        model.EmailId,
                        model.To,
                        model.From,
                        model.CC,
                        model.Subject,
                        model.Message,
                        model.IsHtml,
                        model.Sent,
                        LoggedUserId = 0,
                        model.RowVersion
                    });
            }
        }

        /// <summary>
        /// Retrieve the emails from filter by Partner
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EmailNotification> Retrieve_EmailsNotificatios(string partners)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<EmailNotification>("[General].[SP_Emails_Notifications_Retrieve]",
                new
                {
                    PartnerIds = partners
                });
            }
        }
        
        /// <summary>
        /// Add or Edit Email Notifications
        /// </summary>
        /// <param name="model"></param>
        public void AddOrEdit_EmailsNotificatios(EmailNotificationCreate model)
        {        

            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[SP_Emails_Notifications_AddOrEdit]",
                    new
                    {
                        IDEmail = model.ID_Email,
                        model.Email,
                        model.PartnerId,
                        Active = model.Activo,
                        model.TypeId
                    });
            }
        }

        /// <summary>
        /// Delete Email for Notification
        /// </summary>
        /// <param name="id"></param>
        public void Delete_EmailNotifications(int id)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[General].[SP_Emails_Notifications_Delete]",
                    new { ID_Email = id });
            }
        }

        /// <summary>
        /// Retrieve the list of email created
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EmailNotificationCreate> Retrieve_EmailsNotificatios_List(int? id_Email)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<EmailNotificationCreate>("[General].[SP_Emails_Notifications_Retrieve_List]",
                    new
                    {
                        ID_Email = id_Email
                    });
            }
        }
       
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
