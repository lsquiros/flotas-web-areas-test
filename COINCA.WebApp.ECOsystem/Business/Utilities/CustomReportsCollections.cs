﻿using ECOsystem.Business.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECOsystem.Business.Utilities
{
#pragma warning disable CS1591
    public class CustomReportsCollections : IDisposable
    {
        public static SelectList GetReports
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                using (var bus = new CustomReportsBusiness())
                {
                    var result = bus.GetReports();
                    foreach (var r in result)
                    {
                        dictionary.Add(r.Id, r.ReportName);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetReportSources(bool IsFormula = false)
        {
            var dictionary = new Dictionary<int?, string>();
            using (var bus = new CustomReportsBusiness())
            {
                var result = bus.GetReportSources();
                result = IsFormula ? result.Where(x => x.Id != 7).ToList() : result;
                foreach (var r in result)
                {
                    dictionary.Add(r.Id, r.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }        

        public static SelectList GetReportColumns(int? id, bool? IsFormula)
        {
            var dictionary = new Dictionary<int?, string>();
            using (var bus = new CustomReportsBusiness())
            {
                var result = bus.GetReportColumns(id, IsFormula);
                foreach (var r in result)
                {
                    dictionary.Add(r.Id, r.Name + "-" + r.SysTypeId.ToString());
                }
            }
            return new SelectList(dictionary, "Key", "Value");
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
#pragma warning restore CS1591
}