﻿using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace ECOsystem.Business.Utilities
{
    public class LineVitaCommandsBusiness : IDisposable
    {
        public string LineVitaCommandURL()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_LineVitaCommandUrl_Retrieve]", new { });
            }
        }

        public VehicleCommands GetCommandList(int vehicleId, string url)
        {
            using (var client = new HttpClient())
            {
                var username = ConfigurationManager.AppSettings["LineVitaCommandUserName"];
                var pass = ConfigurationManager.AppSettings["LineVitaCommandPassword"];
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", username, pass))));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetStringAsync(string.Format("api/LineVita/SMSCommands/{0}", vehicleId)).Result;
                if (!string.IsNullOrEmpty(response))
                {
                    return JsonConvert.DeserializeObject<VehicleCommands>(response);
                }
                return new VehicleCommands();
            }
        }

        public List<VehicleCommandReport> GetCommandLog(int device, DateTime? startdate, DateTime? enddate, string url)
        {
            using (var client = new HttpClient())
            {
                var username = ConfigurationManager.AppSettings["LineVitaCommandUserName"];
                var pass = ConfigurationManager.AppSettings["LineVitaCommandPassword"];
                var parameters = string.Format("api/LineVita/RetrieveLogCommands/{0}/{1}/{2}/", device, Convert.ToDateTime(startdate).ToString("yyyy-MM-dd"), Convert.ToDateTime(enddate).ToString("yyyy-MM-dd"));

                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", username, pass))));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetStringAsync(parameters).Result;
                if (!string.IsNullOrEmpty(response))
                {
                    return JsonConvert.DeserializeObject<List<VehicleCommandReport>>(response);
                }
                return new List<VehicleCommandReport>();
            }
        }

        public bool PostCommand(int vehicleId, string url, string command)
        {

            var parameters = string.Format("api/LineVita/Command{0}/", command == "WakeUp" ? "SMS" : string.Empty);
            using (var client = new HttpClient())
            {
                var username = ConfigurationManager.AppSettings["LineVitaCommandUserName"];
                var pass = ConfigurationManager.AppSettings["LineVitaCommandPassword"];
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", username, pass))));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                var response = client.PostAsJsonAsync(string.Format("{0}/{1}/{2}/", parameters, vehicleId, command), string.Empty).Result;

                return response.IsSuccessStatusCode;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}