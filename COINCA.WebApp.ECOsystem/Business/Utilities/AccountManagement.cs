﻿/************************************************************************************************************
*  File    : AccountManagement.cs
*  Summary : Account Management Business Methods
*  Author  : Berman Romero
*  Date    : 11/07/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Web;
using ECOsystem.DataAccess;
using ECOsystem.Models.Account;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ECOsystem.Business.Utilities
{
    /// <summary>
    /// Account Management
    /// </summary>
    public class AccountManagement
    {

        /// <summary>
        /// </summary>
        public static ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        /// <summary>
        /// Api Login
        /// </summary>
        /// <returns></returns>
        public static bool ApiLogin(string username, string password)
        {
            var partner = new Partners {DisplayApiUserName = username, DisplayApiPassword = password};
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<bool>("[General].[Sp_Partners_Authentication]",
                    new
                    {
                        partner.ApiUserName,
                        partner.ApiPassword
                    });
            }
        }

        /// <summary>
        /// API App login
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool ApiAppLogin(string username, string password) 
        {
            //Check the driver user from the EcoSystemApp in ECOsystem
            var aspuser = UserManager.FindByName(TripleDesEncryption.Encrypt(username));

            // Check the valid password
            return UserManager.CheckPassword(aspuser, password);
        }

        /// <summary>
        /// Token User Insert
        /// </summary>
        /// <param name="pEncryptedUserName"></param>
        /// <param name="password"></param>
        public static string TokenUserAddOrEdit(string pEncryptedUserName, string password)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_TokenByUser_AddOrEdit]",
                new
                {
                    UserName = pEncryptedUserName,
                    Context = password
                });
            }
        }

        /// <summary>
        /// Token User Update
        /// </summary>
        /// <param name="SerializedUser"></param>
        /// <param name="TokenId"></param>
        /// <returns></returns>
        public static bool UpdateTokenInformation(string SerializeUserInfo, string TokenId)
        {
             using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<bool>("[General].[Sp_TokenByUser_Update]",
                new
                {
                    SerializeUserInfo,
                    TokenId
                });
            }
        }

        /// <summary>
        /// Token User Retrieve
        /// </summary>
        /// <returns></returns>
        public static TokenByUserModel TokenUserRetrieve(string pToken)
        {
            using (var dba = new DataBaseAccess())
            {
                var list = dba.ExecuteReader<TokenByUserModel>("[General].[Sp_TokenByUser_Retrieve]",
                new
                {
                    Token = pToken
                });

                foreach (var x in list) { return x; }
                return null;               
            }
        }


    }
}