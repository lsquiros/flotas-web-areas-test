﻿using ECOsystem.Business.Core;
using ECOsystem.Business.Home;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Models.Home;
using ECOsystem.Models.Miscellaneous;
using ECOsystem.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECOsystem.Models.Core;
using static ECOsystem.Models.Core.FuelsModels;

namespace ECOsystem.Business.Utilities
{
#pragma warning disable 1591

    /// <summary>
    /// General Collections Class Contains all methods for load Drop Down List
    /// </summary>
    public class GeneralCollections : IDisposable
    {
        /// <summary>
        /// Performs Alarm Credit Count
        /// </summary>
        public static int AlarmCreditCountRetrieve(int pMinPct)
        {
            var Year = DateTime.Today.Year;
            var Month = DateTime.Today.Month;

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[Control].[Sp_AlarmCreditCount_Retrieve]",
                    new
                    {
                        CustomerId = Session.GetCustomerId(),
                        Year,
                        Month,
                        MinPct = pMinPct,
                        Session.GetUserInfo().UserId
                    });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public int RetrievePartnerByCustomerId(int? CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<Int32>("[General].[Sp_PartnerByCustomer_Retrieve]",
                    new
                    {
                        CustomerId

                    });
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<Partners> RetrievePartners(int? partnerId, string key = null)
        {
            IList<Partners> result;
            using (var dba = new DataBaseAccess())
            {
                result = dba.ExecuteReader<Partners>("[General].[Sp_Partners_Retrieve]",
                    new
                    {
                        PartnerId = partnerId,
                        Key = key
                    });
            }
            if (partnerId == null || result.FirstOrDefault() == null) return result;

            using (var business = new UsersBusiness())
            {
                var aux = result.FirstOrDefault();
                aux.TempUsersList = business.RetrieveUsers(null, partnerId: partnerId).Select(x => new PartnerTempUser { UserFullName = x.DecryptedName, UserEmail = x.DecryptedEmail, RoleName = x.RoleName }).ToList();
                return new List<Partners> { aux };
            }
        }


        /// <summary>
        /// Return a list of Countries in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCountries
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                using (var business = new CountriesBusiness())
                {
                    var result = business.RetrieveCountries(null);
                    foreach (var r in result)
                    {
                        dictionary.Add(r.CountryId, r.Name);
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetDatesGroupBy
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {1, "Agrupación por: Año"},
                    {2, "Agrupación por: Mes"},
                    {4, "Agrupación por: Semana"},
                    {3, "Agrupación por: Día"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of group by date ranges
        /// </summary>
        public static SelectList GetComparativeGroupBy
        {
            get
            {
                var dictionary = new Dictionary<int?, string>
                {
                    {100, "Comparación por: Vehículo"},
                    {200, "Comparación por: Marca"},
                    {300, "Comparación por: Modelo"},
                    {400, "Comparación por: Tipo"}
                };

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Acces Countries for selected user in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetCountriesAccess
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();

                var user = Session.GetUserInfo();
                if (!user.IsPartnerUser) return new SelectList(dictionary, "Key", "Value");

                if (user.PartnerUser != null)
                {
                    foreach (var r in user.PartnerUser.CountriesAccessList.Where(x => x.IsCountryChecked))
                    {
                        dictionary.Add(r.CountryId, r.CountryName);
                    }
                }

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Customers Credit Card Types For populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomersCreditCardTypes
        {
            get
            {
                var dictionary = new Dictionary<string, string> { { "C", "Crédito" }, { "D", "Débito" } };
                return new SelectList(dictionary, "Key", "Value", "C");
            }
        }

        /// <summary>
        /// Return a list of Report Criteria in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetReportCriteria
        {
            get
            {
                return new SelectList(new Dictionary<int, string> {
                    {(int)ReportCriteria.Period,"Por Período"},
                    {(int)ReportCriteria.DateRange,"Rango de Fechas"}
                }, "Key", "Value", "");
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetMonthNames
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetMonthNamesByMonth(int? Month)
        {
            var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
            var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
            var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
            var month = Month != null && Month != 0 ? Month : DateTime.UtcNow.Month;
            return new SelectList(dictionary, "Key", "Value", month);
        }

        /// <summary>
        /// Return a current Month Name
        /// </summary>
        public static SelectList GetCurrentMonthName
        {
            get
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.Where(w => w.Id == DateTime.Now.Month).ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new SelectList(dictionary, "Key", "Value", DateTime.UtcNow.Month);
            }
        }

        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year - 5; i < year + 9; i++)
                {
                    dictionary.Add(i, i);
                }
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        

        public static SelectList GetClassifications {
            get {
                var dictionary = new Dictionary<int, string>();

                using (var dba = new DataBaseAccess())
                {
                    var data = dba.ExecuteReader<ClassificationList>("[General].[Sp_ClassifficationTypes_Retrieve]", null);

                    if (data.Count > 0)
                    {
                        dictionary.Add(-1, "Todas");
                        foreach (var item in data)
                        {
                            dictionary.Add(item.Id, item.Name);
                        }
                    }
                }

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetCardYears
        {
            get
            {
                var dictionary = new Dictionary<int?, int>();
                var year = DateTime.UtcNow.Year;

                for (var i = year; i < year + 15; i++)
                {   //ValidCreditCard for 15 years
                    dictionary.Add(i, i);
                }
                year = year + 1;
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        public static SelectList GetCardYearsByYear(int? yearval)
        {
            var dictionary = new Dictionary<int?, int>();
            var year = DateTime.UtcNow.Year;

            for (var i = year; i < year + 15; i++)
            {   //ValidCreditCard for 15 years
                dictionary.Add(i, i);
            }
            year = yearval != null && yearval != 0 ? (int)yearval : year + 1;
            return new SelectList(dictionary, "Key", "Value", year);
        }


        /// <summary>
        /// Return a list of Transactions Types it for populate DropDownListFor
        /// </summary>
        public static SelectList GetTransactionTypes
        {
            get
            {
                using (var bus = new HomeBusiness())
                {
                    var dictionary = new Dictionary<int?, string>();
                    var model = bus.RetrieveTransactionsTypes();

                    foreach (var item in model)
                    {
                        dictionary.Add(item.Id, item.Name);
                    }

                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }

        /// <summary>
        /// GetActiveInactive
        /// </summary>
        public static SelectList GetActiveInactive
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(1, "Activo");
                dictionary.Add(0, "Inactivo");
                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterDetailTransacReport
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var year = DateTime.UtcNow.Year;
                dictionary.Add(1, "Centro de Costo");
                dictionary.Add(2, "Grupo de Vehículos");
                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterSettingOdometers
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                dictionary.Add(1, "Odómetros Erróneos");
                dictionary.Add(2, "Historial de Odómetros");
                return new SelectList(dictionary, "Key", "Value", 1);
            }
        }


        /// <summary>
        /// Return a list of Filtersfor populate DropDownListFor
        /// </summary>
        public static SelectList GetFilterPartnerCustomer
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();
                var year = DateTime.UtcNow.Year;

                if (ValidateCustomerAccess())
                {
                    dictionary.Add(2, "{'Name':'Clientes'}");
                    dictionary.Add(4, "{'Name':'VPOS'}");
                }
                else
                {
                    dictionary.Add(1, "{'Name':'Socio'}");
                    dictionary.Add(2, "{'Name':'Clientes'}");

                    if (ValidateSuperAdmin())
                    {
                        dictionary.Add(3, "{'Name':'Coinca'}");
                    }

                    if (ValidateVPOSAccess())
                    {
                        dictionary.Add(4, "{'Name':'VPOS'}");
                    }
                }

                return new SelectList(dictionary, "Key", "Value", year);
            }
        }

        /// <summary>
        /// If the user is authenticated validates the result of the role for super admin
        /// </summary>
        /// <returns></returns>
        public static bool ValidateSuperAdmin()
        {
            if (HttpContext.Current.Session["LOGGED_USER_INFORMATION"] != null)
            {
                string SuperAdminRole = HomeBusiness.GetRoleForSuperAdmin();

                if (!string.IsNullOrEmpty(SuperAdminRole))
                {
                    HttpContext.Current.Session["SuperAdminRole"] = SuperAdminRole;
                    return true;
                }
            }
            HttpContext.Current.Session["SuperAdminRole"] = null;
            return false;
        }

        /// <summary>
        /// Validates if the users has access to the customer role 
        /// </summary>
        /// <returns></returns>
        public static bool ValidateCustomerAccess()
        {
            if (HttpContext.Current.Session["LOGGED_USER_INFORMATION"] != null)
            {
                string CustomerRole = HomeBusiness.GetCustomerRole();

                if (!string.IsNullOrEmpty(CustomerRole))
                {
                    HttpContext.Current.Session["CustomerRole"] = CustomerRole;
                    return true;
                }
            }
            HttpContext.Current.Session["CustomerRole"] = null;
            return false;
        }

        /// <summary>
        /// Validates if the users has access to the customer role 
        /// </summary>
        /// <returns></returns>
        public static bool ValidateVPOSAccess()
        {
            if (HttpContext.Current.Session["LOGGED_USER_INFORMATION"] != null)
            {
                var VPOSRole = HomeBusiness.GetVPOSRole();

                if (!string.IsNullOrEmpty(VPOSRole.Name))
                {
                    HttpContext.Current.Session["VPOSRole"] = VPOSRole;
                    return true;
                }
            }
            HttpContext.Current.Session["VPOSRole"] = null;
            return false;
        }

        /// <summary>
        /// Return the modality of the GPS
        /// </summary>
        public static SelectList GetGPSModality
        {
            get
            {
                return new SelectList(new Dictionary<int, string> {
                    {3,"Todos"},
                    {1,"Renta"},
                    {2,"Venta"}
                }, "Key", "Value", "3");
            }
        }

        /// <summary>
        /// Return a list of Icon of vehicle in order to using for populate DropDownList 
        /// </summary>
        public static SelectList GetIconVehicleCategory
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                string path = HttpContext.Current.Server.MapPath("~/Content/Images/Vehicles/");
                if (Directory.Exists(path))
                {
                    List<String> listImage = Directory.GetFiles(path, "*.png", SearchOption.AllDirectories).ToList();
                    foreach (String itemImage in listImage)
                    {
                        dictionary.Add(Path.GetFileName(itemImage), "{'image':'" + Path.GetFileName(itemImage) + "'}");
                    }
                }
                return new SelectList(dictionary, "Key", "Value");
            }
        }



        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetCustomerRoles
        {
            get
            {
                return new SelectList(new Dictionary<string, string> {
                    {"CUSTOMER_ADMIN","Administrador"},
                    {"CUSTOMER_USER","Usuario Regular"}
                }, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetDriverRoles
        {
            get
            {
                return new SelectList(new Dictionary<string, string> {
                    {"CUSTOMER_DRIVER","Conductor"}
                }, "Key", "Value");
            }
        }


        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetSuperRoles
        {
            get
            {
                return new SelectList(new Dictionary<string, string> {
                    {"SUPER_ADMIN","Administrador"},
                    {"SUPER_USER","Usuario Regular"}
                }, "Key", "Value");
            }
        }

        /// <summary>
        /// Return a list of Roles to populate DropDownListFor
        /// </summary>
        public static SelectList GetPartnerRoles
        {
            get
            {
                if (Session.GetPartnerInfo().PartnerTypeId != 801)
                {
                    return new SelectList(new Dictionary<string, string> {
                    {"PARTNER_ADMIN","Administrador"},
                    {"PARTNER_USER","Usuario Regular"}
                    }, "Key", "Value");
                }
                return new SelectList(new Dictionary<string, string> {
                    {"INSURANCE_ADMIN","Administrador"},
                    {"INSURANCE_USER","Usuario Regular"}
                }, "Key", "Value");
            }
        }


        /// <summary>
        /// Return a list of Month Names in order to using it for populate DropDownListFor
        /// </summary>
        public static HtmlString GetMonthCardNames(int p)
        {
            if (p == DateTime.Now.Year)
            {

                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name))
                                       .SkipWhile(element => element.Key < DateTime.Now.Month + 1).ToList();
                return new HtmlString(JsonConvert.SerializeObject(dictionary.ToList()));
            }
            else
            {
                var monthNames = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR")).MonthNames.Take(12).ToList();
                var result = monthNames.Select(m => new { Id = monthNames.IndexOf(m) + 1, Name = m });
                var dictionary = result.ToDictionary(r => r.Id, r => new CultureInfo("es-CR").TextInfo.ToTitleCase(r.Name));
                return new HtmlString(JsonConvert.SerializeObject(dictionary.ToList()));
            }
        }

        /// <summary>
        /// Retrieve Parameters
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Parameters> RetrieveParameters()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Parameters>("[General].[Sp_Parameters_Retrieve]", null);
            }
        }

        /// <summary>
        /// Retrieve Partner Parameters
        /// </summary>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<ParametersByPartner> RetrievePartnerParameters(int? partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ParametersByPartner>("[General].[Sp_PartnerParameterRules_Retrieve]", new
                {
                    PartnerId = partnerId
                });
            }
        }

        /// <summary>
        /// Get the Urls for the Modules
        /// </summary>
        /// <returns></returns>
        public static ModulesUrl RetrieveModulesUrl()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ModulesUrl>("[General].[Sp_AreasUrls_Retrieve]", new { }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Return a list of Countries in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetNearCommerces(int pointID)
        {

            var dictionary = new Dictionary<int?, string>();

            using (var business = new CommercesBusiness())
            {
                var result = business.CommercesNear(pointID);
                foreach (var r in result)
                {
                    dictionary.Add(r.Id, r.Name);
                }
            }
            return new SelectList(dictionary, "Key", "Value");

        }

        /// <summary>
        /// Retrieve ControlCar Api Config
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ApiServiceModel> RetrieveControlCarApiConfig()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<ApiServiceModel>("[General].[Retrieve_ControlCarAPI_Config]", new { });
            }
        }

        /// <summary>
        /// Retrieve Product Type By Partner
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public ProductInfo RetrieveProductTypeByPartner(int partnerId)
        {
            using (var dba = new DataBaseAccess())
            {
                var productType = dba.ExecuteScalar<Int32>("[General].[Sp_ProductTypeRetrieve_ByPartner]",
                new
                {
                    PartnerId = partnerId

                });

                switch (productType)
                {
                    case 0:
                        return ProductInfo.Services;
                    case 1:
                        return ProductInfo.IntrackECO;
                    case 2:
                        return ProductInfo.BACFlota;
                    case 3:
                        return ProductInfo.LineVitaECO;
                    default:
                        break;
                }

                return ProductInfo.IntrackECO;
            }
        }

        #region PartnerSurveySelectList
        public static SelectList GetPartnerSurveyQuestionTypes
        {
            get
            {
                using (var bus = new PartnerSurveyBusiness())
                {
                    var dictionary = new Dictionary<int, string>();
                    var list = bus.PartnerSurveyQuestionTypesRetrieve();
                    foreach (var item in list)
                    {
                        dictionary.Add(item.Id, item.Name);
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }

        public static SelectList GetPartnerQuestionsNext(List<PartnerSurveyQuestion> list)
        {
            var dictionary = new Dictionary<int?, string>();
            foreach (var r in list)
            {
                dictionary.Add(r.Id, r.Text);
            }
            return new SelectList(dictionary, "Key", "Value");

        }

        public static SelectList GetPartnerSurveyTargets
        {
            get
            {
                using (var bus = new PartnerSurveyBusiness())
                {
                    var dictionary = new Dictionary<int, string>();
                    var list = bus.PartnerSurveyTargetsRetrieve();
                    foreach (var item in list)
                    {
                        dictionary.Add(item.Id, item.Description);
                    }
                    return new SelectList(dictionary, "Key", "Value");
                }
            }
        }
        #endregion       

        /// <summary>
        /// Retrieve Fuels
        /// </summary>
        /// <param name="fuelId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="countryId">Optional country id to filter fuels by country</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable of T. T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public static IEnumerable<Fuels> RetrieveFuels(int? fuelId, int? countryId = null, string key = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Fuels>("[Control].[Sp_Fuels_Retrieve]",
                    new
                    {
                        FuelId = fuelId,
                        Key = key,
                        CountryId = countryId
                    });
            }
        }

        /// <summary>
        /// Return a list of Fuels in order to using it for populate DropDownListFor
        /// </summary>
        public static SelectList GetFuelsByCountry
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();



                var result = RetrieveFuels(null, countryId: ECOsystem.Utilities.Session.GetCountryId()).ToList();
                foreach (var r in result)
                {
                    dictionary.Add(r.FuelId, r.Name);
                }

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        public static SelectList GetFuelsByCountryAll
        {
            get
            {
                var dictionary = new Dictionary<int?, string>();



                var result = RetrieveFuels(null, countryId: ECOsystem.Utilities.Session.GetCountryId()).ToList();

                dictionary.Add(-1, "Todos");
                foreach (var r in result)
                {
                    dictionary.Add(r.FuelId, r.Name);
                }

                return new SelectList(dictionary, "Key", "Value");
            }
        }

        /// <summary>
        /// Disposer
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }

    internal class ClassificationList
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}