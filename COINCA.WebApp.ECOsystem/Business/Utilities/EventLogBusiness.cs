﻿/**************************************************************************************************
 *  File    : EventLogBusiness.cs
 *  Summary : Clase que provee la insercción de log de eventos registrados en el sistema
 *  Author  : Albert Estrada
 *  Created : 12/Mayo/2015
 *  
 *  Modify  : Gerald Solano
 *  Updated : 26/Agosto/2015
 *  
 *  Modify  : Melvin Salas
 *  Updated : 06/NOV/2015
 *  Summary : Add events more friendly way
 * 
 *  Copyright 2015 COINCA, All rights reserved
 *  This software and documentation is the confidential and proprietary information of COINCA S.A.
 *  You shall not disclose such Confidential Information and shall use it only in accordance with the
 *  terms of the license agreement you entered into with this company.
 * 
 *************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Web;
using ECOsystem.Audit;
using ECOsystem.DataAccess;
using ECOsystem.Models.Core;
using ECOsystem.Utilities;

namespace ECOsystem.Business.Utilities
{
#pragma warning disable 1591

    /// <summary>
    /// Class for write to log
    /// </summary>
    public class EventLogBusiness : IDisposable
    {
        /// <summary>
        /// Agrega un nuevo evento en el log de la base de datos
        /// </summary>
        public static void AddEventLog(EventLog auditEvent)
        {
            try
            {
                using (var dba = new DataBaseAccess())
                {
                    dba.ExecuteNonQuery("[General].[Sp_EventLog_Add]",
                    new
                    {
                        EventType = auditEvent.EventType.ToString(), auditEvent.Message, auditEvent.Controller, auditEvent.Action, auditEvent.IsError, auditEvent.IsInfo, auditEvent.IsWarning, auditEvent.UserId, auditEvent.CustomerId, auditEvent.PartnerId
                    });
                }
            }
            catch (Exception)
            {
                //Si no se alamancena en la base de datos, se almacacena en un archivo de texto
                //FileStream stream = new FileStream("Error-" +  DateTime.Now.Millisecond + "-" + DateTime.Now.Millisecond  + ".txt", FileMode.OpenOrCreate, FileAccess.Write);
                //StreamWriter writer = new StreamWriter(stream);
                //writer.WriteLine("Tipo Mensaje: " + tipoMensaje );                   
                //writer.WriteLine("Fecha: " + DateTime.Now );
                //writer.WriteLine("Usuario: " + usuario);
                //writer.WriteLine("Mensaje: " + mensaje);     
                //writer.Close();
            }
        }

        /// <summary>
        /// Add log event on database
        /// </summary>
        /// <param name="user">User information</param>
        /// <param name="state">State of log event</param>
        /// <param name="message">Message of log event</param>
        /// <param name="exception">Exception of error</param>
        private void AddLogEvent(Users user, LogState state, string message, Exception exception)
        {
            string controller, action;

            try { controller = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString(); }
            catch { controller = string.Empty; }

            try { action =  HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString(); }
            catch { action = string.Empty; }

            var eventLog = new EventLog
            {
                UserId = user == null ? null : user.UserId,
                CustomerId = user == null ? null : user.CustomerId,
                PartnerId = user == null ? null : (int?)user.PartnerId,
                Controller = controller,
                Action = action,
                Message = exception == null ? message : string.Format("Message: {0} | StackTrace: {1}", exception.Message, exception.StackTrace),
                IsWarning = state == LogState.WARNING,
                IsInfo = state == LogState.INFO,
                IsError = state == LogState.ERROR
            };
            AddEventLog(eventLog);
        }

        /// <summary>
        /// Add log event from exception
        /// </summary>
        /// <param name="state">State of log event</param>
        /// <param name="message">Message of log event</param>
        /// <param name="isAnonymous"></param>
        public void AddLogEvent(LogState state, string message, bool isAnonymous = false)
        {
            AddLogEvent(isAnonymous ? null : Session.GetUserInfo(), state, message, null);
        }

        /// <summary>
        /// Add log event from message
        /// </summary>
        /// <param name="state">State of log event</param>
        /// <param name="exception">Exception of error</param>
        /// <param name="isAnonymous"></param>
        public void AddLogEvent(LogState state, Exception exception, bool isAnonymous = false)
        {
            AddLogEvent(isAnonymous ? null : Session.GetUserInfo(), state, null, exception);
        }

        /// <summary>
        /// Retorna los Audit Events según Controller y Action del Sistema
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<EventLog> RetrieveAuditEvents(EventLog auditEvent)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<EventLog>("[General].[Sp_EventLog_Retrieve]",
                    new
                    {
                        EventType = auditEvent.EventType.ToString(), auditEvent.Controller, auditEvent.Action, auditEvent.IsError, auditEvent.IsInfo, auditEvent.IsWarning, auditEvent.UserId, auditEvent.CustomerId, auditEvent.PartnerId, auditEvent.LogDateTime
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }

    /// <summary>
    /// Allows at event can register with only a status type
    /// </summary>
    public enum LogState { WARNING, INFO, ERROR }
}
