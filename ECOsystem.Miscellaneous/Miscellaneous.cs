﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ECOsystem.Miscellaneous
{
    public static class Miscellaneous
    {
        /// <summary>
        /// Get Display CreditCard Mask
        /// </summary>
        /// <param name="value">Value to apply mask</param>
        /// <returns>The display format with mask</returns>
        public static string GetDisplayCreditCardMask(string value)
        {
            var mask = string.Empty.ToArray();

            if (value.Replace("-", "").Count() == 16)
                mask = ConfigurationManager.AppSettings["DisplayCreditCardMask"].ToCharArray();
            else
                mask = ConfigurationManager.AppSettings["DisplayCreditCardMaskAmex"].ToCharArray();

            var result = value.ToCharArray();

            if (mask.Length == result.Length)
            {
                for (var i = 0; i < result.Length; ++i)
                {
                    if (mask[i] == '*')
                        result[i] = '*';
                }
                return new string(result);
            }
            return GetDefaultMask(value);
        }

        /// <summary>
        /// Convert CreditCard To Internal Format
        /// </summary>
        /// <param name="value">Value to apply mask</param>
        /// <returns>The display format with mask</returns>
        public static string ConvertCreditCardToInternalFormat(long value)
        {
            return String.Format("{0:0000-0000-0000-0000}", value);
        }

        /// <summary>
        /// Get Display Account Number Mask
        /// </summary>
        /// <param name="value">Value to apply mask</param>
        /// <returns>The display format with mask</returns>
        public static string GetDisplayAccountNumberMask(string value)
        {
            return GetDefaultMask(value);
        }

        /// <summary>
        /// Get Default Mask
        /// </summary>
        /// <param name="value">Value to apply mask</param>
        /// <returns>The display format with mask</returns>
        public static string GetDefaultMask(string value)
        {
            var result = value.ToCharArray();
            for (var i = 3; i < result.Length - 2; i++)
            {
                result[i] = '*';
            }
            return new string(result);
        }

        /// <summary>
        /// Get Format Currency
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <param name="symbol">Currency Symbol</param>
        /// <returns>The display format string</returns>
        public static string GetCurrencyFormat(decimal? value, string symbol)
        {
            return value == null ? "" : symbol + ((decimal)value).ToString("N", CultureInfo.CreateSpecificCulture("es-CR"));
        }


        /// <summary>
        /// Get Format Number
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <returns>The display format string</returns>
        public static string GetNumberFormatCR(decimal? value)
        {
            return value == null ? "" : ((decimal)value).ToString("N", CultureInfo.CreateSpecificCulture("en-US"));
        }



        /// <summary>
        /// Get Format Number
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <returns>The display format string</returns>
        public static string GetNumberFormatCRNoDecimals(decimal? value)
        {
            //string val = ((decimal)value).ToString("G", CultureInfo.CreateSpecificCulture("en-US"));

            return value == null ? "" : Convert.ToInt32(value).ToString("N0", CultureInfo.CreateSpecificCulture("en-US"));
        }


        /// <summary>
        /// Get Format CurrencyNumber
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <param name="symbol">Currency Symbol</param>
        /// <returns>The display format string</returns>
        public static string GetNumberFormatCurrencyCR(decimal? value, string symbol)
        {
            return value == null ? "" : symbol + ((decimal)value).ToString("N", CultureInfo.CreateSpecificCulture("es-CR"));
        }

        /// <summary>
        /// Get Format Number
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <returns>The display format string</returns>
        public static string GetNumberFormat(decimal? value)
        {
            return value == null ? "" : (Math.Round((decimal)value, 0)).ToString();
        }


        /// <summary>
        /// Get Format Number
        /// </summary>
        /// <param name="value">Value to apply format</param>
        /// <returns>The display format string</returns>
        public static string GetNumberFormatWithDecimal(decimal? value, int cantDec)
        {
            string strresult = "";
            string strFormat = "N" + cantDec;
            decimal valorConDecimal = value == null ? 0 : (decimal)value;
            long valorSinDecimal = (long)valorConDecimal;
            if ((valorConDecimal - valorSinDecimal) > 0)
                strresult = value == null ? "" : (Math.Round((decimal)value, cantDec)).ToString(strFormat, CultureInfo.CreateSpecificCulture("en-US"));
            else
                strresult = valorSinDecimal.ToString();

            return strresult;
        }

        public static string GetNumberFormatWithDecimal(decimal? value, int cantDec, string specificCulture, bool forceDecimal)
        {
            string strresult = "";
            string strFormat = "N" + cantDec;
            decimal valorConDecimal = value == null ? 0 : (decimal)value;
            long valorSinDecimal = (long)valorConDecimal;
            if (!forceDecimal && (valorConDecimal - valorSinDecimal) > 0)
                strresult = value == null ? "" : (Math.Round((decimal)value, cantDec)).ToString(strFormat, CultureInfo.CreateSpecificCulture(specificCulture));
            else
                strresult = valorSinDecimal.ToString("N", CultureInfo.CreateSpecificCulture(specificCulture));

            return strresult;
        }

        /// <summary>
        /// </summary>
        /// Get Format Number
        /// <param name="value">Value to apply format</param>
        /// <returns>The display format string</returns>
        public static string GetPercentageFormat(decimal? value)
        {
            return value == null ? "" : ((decimal)value).ToString("N", CultureInfo.CreateSpecificCulture("es-CR")) + "%";
        }

        /// <summary>
        /// GetPercentageFormat
        /// </summary>
        /// <param name="value"></param>
        /// <param name="rounddecimals"></param>
        /// <returns></returns>
        public static string GetPercentageFormat(decimal value, int rounddecimals)
        {
            return (Math.Round(value, rounddecimals)) + "%";
        }

        /// <summary>
        /// Get Format Number
        /// </summary>
        /// <param name="month">Value to get Name</param>
        /// <returns>The display a month Name</returns>
        public static string GetMonthName(int? month)
        {
            var dateTimeFormatInfo = DateTimeFormatInfo.GetInstance(CultureInfo.CreateSpecificCulture("es-CR"));
            if (dateTimeFormatInfo == null) return "";
            var monthNames = dateTimeFormatInfo.MonthNames.Take(12).ToList();
            var firstOrDefault = monthNames.Select(m => new { Month = monthNames.IndexOf(m) + 1, MonthNames = new CultureInfo("es-CR").TextInfo.ToTitleCase(m) }).FirstOrDefault(x => x.Month == month);
            return firstOrDefault != null ? firstOrDefault.MonthNames : "";
        }

        /// <summary>
        /// Get Format Date
        /// </summary>
        /// <param name="date">Value to get date</param>
        /// <returns>The display format date</returns>
        public static string GetDateFormat(DateTime? date)
        {
            return date == null ? null : ((DateTime)date).ToString("d", CultureInfo.CreateSpecificCulture("es-CR"));
        }

        /// <summary>
        /// Get Format Date
        /// </summary>
        /// <param name="date">Value to get date</param>
        /// <returns>The display format date</returns>
        public static string GetDateFormat2(DateTime? date)
        {
            return date == null ? null : (((DateTime)date)).ToString("d", CultureInfo.CreateSpecificCulture("es-CR"));
        }

        /// <summary>
        /// Get Format Date
        /// </summary>
        /// <param name="date">Value to get date</param>
        /// <returns>The display format date</returns>
        public static string GetUtcDateFormat(DateTime? date)
        {
            return date == null ? null : ((DateTime)date).ToString("d", CultureInfo.CreateSpecificCulture("es-CR"));
        }

        /// <summary>
        /// Get Format Date
        /// </summary>
        /// <param name="date">Value to get date</param>
        /// <returns>The display format date</returns>
        public static string GetUtcDateTimeFormat(DateTime? date)
        {
            return date == null ? null : (((DateTime)date)).ToString("dd/MM/yyyy HH:mm:ss");
        }

        /// <summary>
        /// Set Date
        /// </summary>
        /// <param name="date">Value to set date</param>
        /// <returns>A date value</returns>
        public static DateTime? SetDate(string date)
        {
            try
            {
                return date == null ? (DateTime?)null : DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("es-CR"));
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Generate Colors
        /// </summary>
        /// <returns>string array</returns>
        public static IEnumerable<string> GenerateColors(int cantItems = 0)
        {
            var colors = new List<string>
            {
                "rgba(31, 119, 180,1)",
                "rgba(241,92,117,1)",
                "rgba(173, 216, 248,1)",
                "rgba(255, 127, 14,1)",
                "rgba(172, 112, 122,1)",
                "rgba(0, 142, 142,1)",
                "rgba(213, 70, 72,1)",
                "rgba(142, 70, 143,1)",
                "rgba(89, 132, 40,1)",
                "rgba(179, 168, 1,1)",
                "rgba(0, 142, 214,1)",
                "rgba(158, 9, 13,1)",
                "rgba(162, 134, 192,1)",
                "rgba(115, 168, 240,1)",
                "rgba(49, 128, 71,1)",
                "rgba(245, 197, 51,1)",
                "rgba(0, 71, 137,1)",
                "rgba(41, 131, 32,1)",
                "rgba(143, 86, 153,1)",
                "rgba(30, 144, 255,1)",
                "rgba(62, 214, 98,1)",
                "rgba(80, 235, 232,1)",
                "rgba(128, 128, 128,1)",
                "rgba(228, 77, 152,1)",
                "rgba(183, 173, 0,1)"
            };

            if (cantItems > colors.Count)
            {
                var colorsToAdd = colors.ToList();
                var diff = cantItems - colors.Count;
                var each = Convert.ToDecimal(diff) / Convert.ToDecimal(colors.Count);
                var maxEach = Math.Round(each);
                maxEach += (each > maxEach) ? (each - maxEach) : 0;

                for (var i = 0; i < maxEach; i++)
                {
                    colors.AddRange(colorsToAdd);
                }
            }

            var result = colors.ToArray();
            if (cantItems > 0)
            {
                result = colors.Take(cantItems).ToArray();
            }

            return result;
        }

        /// <summary>
        /// Generate Colors
        /// </summary>
        /// <returns>string array</returns>
        public static IEnumerable<string> GenerateAlphaColors()
        {
            var colors = new List<string>
            {
                "rgba(31, 119, 180,0.2)",
                "rgba(255, 127, 14,0.2)",
                "rgba(173, 216, 248,0.2)",
                "rgba(248, 188, 16,0.2)",
                "rgba(140, 186, 0,0.2)",
                "rgba(0, 142, 142,0.2)",
                "rgba(213, 70, 72,0.2)",
                "rgba(142, 70, 143,0.2)",
                "rgba(89, 132, 40,0.2)",
                "rgba(179, 168, 1,0.2)",
                "rgba(0, 142, 214,0.2)",
                "rgba(158, 9, 13,0.2)",
                "rgba(162, 134, 192,0.2)",
                "rgba(115, 168, 240,0.2)",
                "rgba(49, 128, 71,0.2)",
                "rgba(245, 197, 51,0.2)",
                "rgba(0, 71, 137,0.2)",
                "rgba(41, 131, 32,0.2)",
                "rgba(143, 86, 153,0.2)",
                "rgba(30, 144, 255,0.2)",
                "rgba(62, 214, 98,0.2)",
                "rgba(80, 235, 232,0.2)",
                "rgba(128, 128, 128,0.2)",
                "rgba(228, 77, 152,0.2)",
                "rgba(183, 173, 0,0.2)"
            };

            return colors.ToArray();
        }

        /// <summary>
        /// Generate Colors
        /// </summary>
        /// <returns>string array</returns>
        public static IEnumerable<string> GenerateHighlighColors()
        {
            var colors = new List<string>
            {
                "#1F77B4",//rgba(31, 119, 180,0.9)"
                "#FF7F0E",//"rgba(255, 127, 14,0.9)",
                "#ADD8F8",//"rgba(173, 216, 248,0.9)",
                "#F8BC10",//"rgba(248, 188, 16,0.9)",
                "#8CBA00",//"rgba(140, 186, 0,0.9)",
                "#008E8E",//"rgba(0, 142, 142,0.9)",
                "#D54648",//"rgba(213, 70, 72,0.9)",
                "#8E468F",//"rgba(142, 70, 143,0.9)",
                "#598428",//"rgba(89, 132, 40,0.9)",
                "#B3A801",//"rgba(179, 168, 1,0.9)",
                "#008ED6",//"rgba(0, 142, 214,0.9)",
                "#9E090D",//"rgba(158, 9, 13,0.9)",
                "#A286C0",//"rgba(162, 134, 192,0.9)",
                "#73A8F0",//"rgba(115, 168, 240,0.9)",
                "#318047",//"rgba(49, 128, 71,0.9)",
                "#F5C533",//"rgba(245, 197, 51,0.9)",
                "#004789",//"rgba(0, 71, 137,0.9)",
                "#298320",//"rgba(41, 131, 32,0.9)",
                "#8F5699",//"rgba(143, 86, 153,0.9)",
                "#1E90FF",//"rgba(30, 144, 255,0.9)",
                "#3ED662",//"rgba(62, 214, 98,0.9)",
                "#50EBE8",//"rgba(80, 235, 232,0.9)",
                "#808080",//"rgba(128, 128, 128,0.9)",
                "#E44D98",//"rgba(228, 77, 152,0.9)",
                "#B7AD00"//"rgba(183, 173, 0,0.9)"
            };

            return colors.ToArray();
        }

        /// <summary>
        /// Base64 Encode
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns>string Base64</returns>
        public static string Base64Encode(string plainText)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText));
        }

        public static string GeneratePin()
        {
            string result = "0000" + new Random().Next(9999);
            return result.Substring(result.Length - 4);
        }

        /// <summary>
        /// Base64 Decode
        /// </summary>
        /// <param name="encodedText">encodeText</param>
        /// <returns>string plain text</returns>
        public static string Base64Decode(string encodedText)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(encodedText));
        }

        /// <summary>
        /// Liters to Gallons Conversion
        /// </summary>
        /// <param name="liters">liters to convert</param>
        /// <returns>decimal Gallons</returns>
        public static decimal LitersToGallons(decimal? liters)
        {
            decimal litersNotNull = 0;
            decimal conversionValue = Convert.ToDecimal(ConfigurationManager.AppSettings["GALLONSTOLITERSEQUIVALENCE"]);

            if (liters != null)
            {
                litersNotNull = Convert.ToDecimal(liters);
            }
            var gallons = litersNotNull / conversionValue;

            return gallons;
        }

        /// <summary>
        /// Gallons to Liters Conversion
        /// </summary>
        /// <param name="gallons">gallons to convert</param>
        /// <returns>decimal Liters</returns>
        public static decimal GallonsToLiters(decimal? gallons)
        {
            decimal gallonsNotNull = 0;
            decimal conversionValue = Convert.ToDecimal(ConfigurationManager.AppSettings["GALLONSTOLITERSEQUIVALENCE"]);

            if (gallons != null)
            {
                gallonsNotNull = Convert.ToDecimal(gallons);
            }
            var liters = gallonsNotNull * conversionValue;

            return liters;
        }

        /// <summary>
        /// Capacity Unit Conversion
        /// </summary>
        /// <param name="capacityUnitIdFrom">Capacity Unit Id Origin</param>
        /// <param name="capacityUnitIdTo">Capacity Unit Id Destination</param>
        /// <param name="capacityUnitValue">Value to be converted</param>
        /// <returns>decimal capacityUnitValueResult</returns>
        public static decimal CapacityUnitConversion(int capacityUnitIdFrom, int capacityUnitIdTo, decimal? capacityUnitValue)
        {
            decimal capacityUnitValueResult = 0;
            int litersCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["LITERSCAPACITYUNITID"]);
            int gallonsCapacityUnitId = Convert.ToInt16(ConfigurationManager.AppSettings["GALLONSCAPACITYUNITID"]);
            //if (capacityUnitIdFrom == capacityUnitIdTo || Session.GetPartnerCapacityUnitId() == 1)
            //{
            //    if (capacityUnitValue != null)
            //    {
            //        capacityUnitValueResult = Convert.ToDecimal(capacityUnitValue);
            //    }
            //}
            //else
            if (capacityUnitIdFrom == litersCapacityUnitId && capacityUnitIdTo == gallonsCapacityUnitId)
            {
                capacityUnitValueResult = LitersToGallons(capacityUnitValue);
            }
            else if (capacityUnitIdFrom == gallonsCapacityUnitId && capacityUnitIdTo == litersCapacityUnitId)
            {
                capacityUnitValueResult = GallonsToLiters(capacityUnitValue);
            }
            else
            {
                capacityUnitValueResult = capacityUnitValue ?? 0;
            }
            return capacityUnitValueResult;
        }

        /// <summary>
        /// Get Status Checked and Unchecked
        /// </summary>
        /// <param name="Activo"></param>
        /// <returns></returns>
        public static string GetStatusEmailsNotifications(int Activo)
        {
            return Activo == 1 ? "checked=\"checked\"" : "";
        }
        internal static int GetPercentageFormat(double p1, int p2)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Validate Guid Regular Expression
        /// </summary>
        private static readonly Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);

        /// <summary>
        /// Validate if string is a Guid type
        /// </summary>
        /// <param name="candidate"></param>
        /// <returns></returns>
        public static bool IsGuid(string candidate)
        {
            return candidate != null && isGuid.IsMatch(candidate);
        }

        /// <summary>
        /// Return Numeric Currency with culture info
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="currencyAmount"></param>
        /// <returns></returns>
        public static decimal ConvertCurrencyWithCulture(string culture, string currencyAmount, bool defaultCulture = true)
        {
            try
            {
                CultureInfo cultureInfo;
                decimal number;

                if (defaultCulture)
                {
                    cultureInfo = CultureInfo.GetCultureInfo(culture);
                    var decSepar = cultureInfo.NumberFormat.NumberDecimalSeparator;
                    var milSepar = cultureInfo.NumberFormat.NumberGroupSeparator;

                    currencyAmount = currencyAmount.Replace(milSepar, "");
                    currencyAmount = currencyAmount.Replace(decSepar, CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator);

                    number = decimal.Parse(currencyAmount, CultureInfo.InvariantCulture);
                }
                else
                {
                    cultureInfo = CultureInfo.InvariantCulture;
                    var decSepar = cultureInfo.NumberFormat.NumberDecimalSeparator;

                    currencyAmount = currencyAmount.Replace(decSepar, CultureInfo.GetCultureInfo(culture).NumberFormat.NumberDecimalSeparator);

                    number = decimal.Parse(currencyAmount, CultureInfo.GetCultureInfo(culture));
                }

                return number;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Return Numeric Currency with culture info in String format
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="currencyAmount"></param>
        /// <returns></returns>
        public static string ConvertCurrencyWithCultureStr(string culture, string currencyAmount, bool defaultCulture = true, bool currentCulture = false)
        {
            try
            {
                decimal decAmount;
                decimal.TryParse(currencyAmount, out decAmount);

                var cultureInfo = CultureInfo.InvariantCulture;
                var decSepar = cultureInfo.NumberFormat.NumberDecimalSeparator;
                var milSepar = cultureInfo.NumberFormat.NumberGroupSeparator;
                var format = string.Format("#{0}##0{1}00", milSepar, decSepar);

                var number = decAmount.ToString(format);
                currencyAmount = number;

                currencyAmount = currencyAmount.Replace(",", "[M]");
                currencyAmount = currencyAmount.Replace(".", "[D]");

                if (defaultCulture)
                {
                    currencyAmount = currencyAmount.Replace("[D]", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator);
                    currencyAmount = currencyAmount.Replace("[M]", CultureInfo.InvariantCulture.NumberFormat.NumberGroupSeparator);

                    number = currencyAmount;
                }
                else if (currentCulture)
                {
                    currencyAmount = currencyAmount.Replace("[D]", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                    currencyAmount = currencyAmount.Replace("[M]", CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator);

                    number = currencyAmount;
                }
                else
                {
                    currencyAmount = currencyAmount.Replace("[D]", CultureInfo.GetCultureInfo(culture).NumberFormat.NumberDecimalSeparator);
                    currencyAmount = currencyAmount.Replace("[M]", CultureInfo.GetCultureInfo(culture).NumberFormat.NumberGroupSeparator);

                    number = currencyAmount;
                }

                return number;
            }
            catch (Exception)
            {
                return "0,00";
            }
        }

        /// <summary>
        /// Convert StringDate to DateTime
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime ConvertDateFromApprovalToDateTime(string date)
        {
            if (date.Contains("ENE"))
            {
                date = date.Replace("ENE", "JAN");
            }
            else if (date.Contains("ABR"))
            {
                date = date.Replace("ABR", "APR");
            }
            else if (date.Contains("AGO"))
            {
                date = date.Replace("AGO", "AUG");
            }
            else if (date.Contains("SET") || date.Contains("SEP"))
            {
                date = date.Replace("SET", "SEP");
            }
            else if (date.Contains("DIC"))
            {
                date = date.Replace("DIC", "DEC");
            }

            return DateTime.ParseExact(date, "d-MMM-yy", CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Get XML Data 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public static string GetXML<T>(List<T> list, string root = null)
        {
            var XMLData = string.Empty;

            using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<T>));
                if (!string.IsNullOrEmpty(root))
                {
                    xmlSerializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute(root));
                }
                xmlSerializer.Serialize(stringWriter, list);
                return XMLData = stringWriter.ToString().Substring(stringWriter.ToString().IndexOf('\n') + 1);
            }
        }

        /// <summary>
        /// Return List from XML
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="XML"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public static T GetXMLDeserialize<T>(string XML, string root)
        {
            using (XmlReader reader = XmlReader.Create(new StringReader(XML)))
            {
                return (T)new XmlSerializer(typeof(T), new XmlRootAttribute(root)).Deserialize(reader);
            }
        }

        public static object JsonSerializeIgnoreNulls(object obj)
        {
            return JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject
                   (
                        obj,
                        Newtonsoft.Json.Formatting.Indented,
                        new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        }
                   ));
        }

    }
}
