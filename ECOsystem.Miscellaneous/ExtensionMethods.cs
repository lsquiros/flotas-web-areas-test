﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace ECOsystem.Miscellaneous
{
    /// <summary>
    /// Extension methods are static methods, but they're called as if they were instance methods on the extended type. 
    /// For client code written in C#, F# and Visual Basic, there's no apparent difference between calling an extension 
    /// method and the methods defined in a type.
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Word Count
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int WordCount(this String str)
        {
            return str.Split(new char[] { ' ', '.', '?' },
                             StringSplitOptions.RemoveEmptyEntries).Length;
        }

        /// <summary>
        /// Remove special characters
        /// #$%&/.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToRemoveSpecialCharacters(this string str)
        {
            str = str.ToLower().ToRemoveAccents();

            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_' || c == ' ' || c == '-')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Change accents á é í ó ú ñ
        /// </summary>
        /// <param name="str"></param>
        /// <returns>a e i o u n: In uppercase</returns>
        public static string ToRemoveAccents(this string str)
        {
            str = str.ToLower();

            Regex replace_a_Accents = new Regex("[á|à|ä|â]", RegexOptions.Compiled);
            Regex replace_e_Accents = new Regex("[é|è|ë|ê]", RegexOptions.Compiled);
            Regex replace_i_Accents = new Regex("[í|ì|ï|î]", RegexOptions.Compiled);
            Regex replace_o_Accents = new Regex("[ó|ò|ö|ô]", RegexOptions.Compiled);
            Regex replace_u_Accents = new Regex("[ú|ù|ü|û]", RegexOptions.Compiled);
            Regex replace_n_Accents = new Regex("[ñ|Ñ]", RegexOptions.Compiled);

            str = replace_a_Accents.Replace(str, "a");
            str = replace_e_Accents.Replace(str, "e");
            str = replace_i_Accents.Replace(str, "i");
            str = replace_o_Accents.Replace(str, "o");
            str = replace_u_Accents.Replace(str, "u");
            str = replace_n_Accents.Replace(str, "n");

            return str.ToUpper();
        }
    }
}
