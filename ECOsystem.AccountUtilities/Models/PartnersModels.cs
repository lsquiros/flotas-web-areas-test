﻿/************************************************************************************************************
*  File    : PartnersModels.cs
*  Summary : Partners Models
*  Author  : Berman Romero
*  Date    : 09/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using System.ComponentModel;

namespace ECOsystem.AccountUtilities.Models
{
#pragma warning disable 1591

    /// <summary>
    /// Currencies Base
    /// </summary>
    public class PartnersBase
    {
        /// <summary>
        /// PartnersBase default constructor 
        /// </summary>
        public PartnersBase()
        {
            Data = new Partners();
            List = new List<Partners>();          
        }

        /// <summary>
        /// Data property displays the main information of the entity 
        /// </summary>
        public Partners Data { get; set; }

        /// <summary>
        /// List of entities to use it for load grid information
        /// </summary>
        public IEnumerable<Partners> List { get; set; }

    }    

    /// <summary>
    /// Partner Models
    /// </summary>
    public class Partners 
    {
        /// <summary>
        /// property
        /// </summary>
       
        public int? PartnerId { get; set; }

        /// <summary>
        /// Name of current entity
        /// </summary>
        [DisplayName("Nombre")]        
       
        public string Name { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("País")]        
       
        public int CountryId { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("País")]
       
        public string CountryName { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Logo")]
       
        public string Logo { get; set; }

        /// <summary>
        /// property
        /// </summary>
       
        public string ApiUserName { get; set; }

        /// <summary>
        /// property
        /// </summary>
       
        public string ApiPassword { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Usuario API")]
       
        public string DisplayApiUserName
        {
            get { return TripleDesEncryption.Decrypt(ApiUserName); }
            set { ApiUserName = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Contraseña API")]
       
        public string DisplayApiPassword
        {
            get { return TripleDesEncryption.Decrypt(ApiPassword); }
            set { ApiPassword = TripleDesEncryption.Encrypt(value); }
        }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Confirmar Contraseña")]
       
        public string PasswordConfirm { get; set; }

        /// <summary>
        /// Capacity Unit property
        /// </summary>
       
        public bool ChangePassword { get; set; }

        [DisplayName("Tipo de producto")]
       
        public int? ProductTypesId { get; set; }

        /// <summary>
        /// Capacity Unit property
        /// </summary>
        [DisplayName("Unidad de Medida de Combustible")]
       
        public int? CapacityUnitId { get; set; }

        /// <summary>
        /// Partner Type property Id
        /// </summary>
        [DisplayName("Tipo de Socio")]
       
        public int PartnerTypeId { get; set; }

        /// <summary>
        /// Partner Type property name
        /// </summary>
       
        public string PartnerTypeName { get; set; }

        /// <summary>
        /// Capacity Unit Name property
        /// </summary>
       
        public string CapacityUnitName { get; set; }

        /// <summary>
        /// Temp Users List
        /// </summary>
       
        public IList<PartnerTempUser> TempUsersList { get; set; }

        /// <summary>
        /// property
        /// </summary>
        [DisplayName("Porcentaje Error en Precio de Combustible")]        
       
        public int FuelErrorPercent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Rol")]
        
       
        public string RoleName { get; set; }

    }

    /// <summary>
    /// Partner Temp User Info
    /// </summary>
    public class PartnerTempUser
    {
        /// <summary>
        /// UserFullName
        /// </summary>
        public string UserFullName { get; set; }

        /// <summary>
        /// UserEmail
        /// </summary>
        public string UserEmail { get; set; }
        
        /// <summary>
        /// UserEmail
        /// </summary>
        public string RoleName { get; set; }


    }


}