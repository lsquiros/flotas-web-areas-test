﻿using System.Linq;
using System.Web;
using ECOsystem;
using ECOsystem.AccountUtilities.Models;
using ECOsystem.DataAccess;

namespace ECOsystem.AccountUtilities
{
    /// <summary>
    /// Account Management
    /// </summary>
    public class AccountLogin
    {
        public static bool ApiLogin(string username, string password)
        {
            var partner = new Partners { DisplayApiUserName = username, DisplayApiPassword = password };
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<bool>("[General].[Sp_Partners_Authentication]",
                    new
                    {
                        partner.ApiUserName,
                        partner.ApiPassword
                    });
            }
        }

        public static bool ApiAppLogin(string username, string password) 
        {
            //Check the driver or lineVita user from the EcoSystemApp / LineVitaECOApp in ECOsystem
            var usernameEncrypt = TripleDesEncryption.Encrypt(((!string.IsNullOrEmpty(username))? username.Trim() : ""));
            var passwordEncrypt = TripleDesEncryption.Encrypt(((!string.IsNullOrEmpty(password)) ? password.Trim() : ""));

            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<bool>("[General].[Sp_App_Authentication]",
                    new
                    {
                        UserName = usernameEncrypt,
                        Password = passwordEncrypt
                    });
            }
        }

        /// <summary>
        /// Token User Insert
        /// </summary>
        /// <param name="pEncryptedUserName"></param>
        /// <param name="password"></param>
        public static string TokenUserAddOrEdit(string pEncryptedUserName, string password)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[General].[Sp_TokenByUser_AddOrEdit]",
                new
                {
                    UserName = pEncryptedUserName,
                    Context = password
                });
            }
        }

        /// <summary>
        /// Token User Update
        /// </summary>
        /// <param name="SerializedUser"></param>
        /// <param name="TokenId"></param>
        /// <returns></returns>
        public static bool UpdateTokenInformation(string SerializeUserInfo, string TokenId)
        {
             using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<bool>("[General].[Sp_TokenByUser_Update]",
                new
                {
                    SerializeUserInfo,
                    TokenId
                });
            }
        }
        
        public static LoginDataModel ECOsystemLogin(string username, string password)
        {
            using (var dba = new DataBaseAccess())
            {
                var result = dba.ExecuteReader<LoginDataModel>("[General].[Sp_ECOsystemAuthentication_Retrieve]",
                    new
                    {
                        UserName = TripleDesEncryption.Encrypt(username),
                        Password = TripleDesEncryption.Encrypt(password)
                    }).FirstOrDefault();
                return result ?? new LoginDataModel();
            }
        }
    }
}