USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ValidateElectronicInvoice]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ValidateElectronicInvoice]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Jason Bonilla
-- Create Date: 20/01/2020
-- Description: Validate Electronic Invoice Fields
-- =============================================
CREATE PROCEDURE [General].[Sp_ValidateElectronicInvoice]
(
    @pFuelAmount DECIMAL(16,2),
	@pLiters DECIMAL(16,3),
	@pInsertDate DATE,
	@pLegalDocument VARCHAR(50),
	@pHaciendaClave VARCHAR(50),
	@pHaciendaMessage VARCHAR(MAX),
	@pHaciendaMessageDetail VARCHAR(MAX),
	@pEmailInfo VARCHAR(500),
	@pLote INT
)
AS
BEGIN
    
    SET NOCOUNT ON

	DECLARE @TransactionId INT

	BEGIN TRY 
		SET @TransactionId = ISNULL((
			SELECT TOP 1
				[TransactionId]
			FROM [Control].[Transactions] t
			INNER JOIN [Control].[CreditCard] cc 
				ON cc.[CreditCardId] = t.[CreditCardId]
			INNER JOIN [General].[CustomerAdditionalInformation] a 
				ON cc.[CustomerId] = a.[CustomerId]
			WHERE
				[Liters] = @pLiters
			AND
				[FuelAmount] = @pFuelAmount
			AND
				a.[LegalDocument] = @pLegalDocument
			AND
				CAST(t.[InsertDate] as Date) = @pInsertDate
			AND
				t.[ElectronicInvoice] is null
		),0)

		IF @TransactionId <> 0
			BEGIN
				UPDATE [Control].[Transactions] SET [ElectronicInvoice] = @pHaciendaClave WHERE [TransactionId] = @TransactionId

				DECLARE @AffectedRows INT
				SET @AffectedRows = (SELECT @@rowcount)

				INSERT INTO [General].[ElectronicInvoiceLog]
						VALUES(
							@TransactionId,
							@pFuelAmount,
							@pLiters,
							@pLegalDocument,
							@pHaciendaClave,
							@pHaciendaMessage,
							@pHaciendaMessageDetail,
							CASE WHEN @AffectedRows > 0 THEN 1 ELSE 0 END,
							GETDATE(),
							CASE WHEN @AffectedRows > 0 THEN 'Registro actualizado satisfactoriamente' ELSE 'No se encontró el registro' END,
							@pEmailInfo,
							@pLote
						)
			END
		ELSE
			BEGIN
				INSERT INTO [General].[ElectronicInvoiceLog]
						VALUES(
							@TransactionId,
							@pFuelAmount,
							@pLiters,
							@pLegalDocument,
							@pHaciendaClave,
							@pHaciendaMessage,
							@pHaciendaMessageDetail,
							0,
							GETDATE(),
							'No se encontró el registro',
							@pEmailInfo,
							@pLote
						)
			END
	END TRY
	BEGIN CATCH  
		DECLARE @ErrorMessage NVARCHAR(4000);  
		DECLARE @ErrorSeverity INT;  
		DECLARE @ErrorState INT;  

		SELECT   
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE(); 

		INSERT INTO [General].[ElectronicInvoiceLog]
						VALUES(
							@TransactionId,
							@pFuelAmount,
							@pLiters,
							@pLegalDocument,
							@pHaciendaClave,
							@pHaciendaMessage,
							@pHaciendaMessageDetail,
							0,
							GETDATE(),
							@ErrorMessage,
							@pEmailInfo,
							@pLote
						)


		-- RAISE ERROR en bloque catch para forzar la devolución de error personalizado
		RAISERROR (@ErrorMessage, -- Message text.  
				   @ErrorSeverity, -- Severity.  
				   @ErrorState -- State.  
				   );  
	END CATCH;
	

	SET NOCOUNT OFF

END
GO


