USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailConfig_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_EmailConfig_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Jason Bonilla
-- Create Date: 16/01/2020
-- Description: Retrieve parameters for Email Processor (GMAIL)
-- =============================================
CREATE PROCEDURE [General].[Sp_EmailConfig_Retrieve]
AS
BEGIN
    SET NOCOUNT ON

	DECLARE
		@hostname VARCHAR(50),
		@port INT,
		@ssl BIT,
		@user VARCHAR(50),
		@password VARCHAR(50),
		@mailbox VARCHAR(50)

	SET @hostname = (SELECT [Value] FROM [dbo].[GeneralParameters] where [ParameterID] = 'EMAIL_HOSTNAME')
	SET @port = (SELECT [Value] FROM [dbo].[GeneralParameters] where [ParameterID] = 'EMAIL_PORT')
	SET @ssl = (SELECT [Value] FROM [dbo].[GeneralParameters] where [ParameterID] = 'EMAIL_SSL')
	SET @user = (SELECT [Value] FROM [dbo].[GeneralParameters] where [ParameterID] = 'EMAIL_USER')
	SET @password = (SELECT [Value] FROM [dbo].[GeneralParameters] where [ParameterID] = 'EMAIL_PASSWORD')
	SET @mailbox = (SELECT [Value] FROM [dbo].[GeneralParameters] where [ParameterID] = 'EMAIL_MAILBOX')

	SELECT
		@hostname as Hostname,
		@port as Port,
		@ssl as Ssl,
		@user as [User],
		@password as Password,
		@mailbox as Mailbox

	SET NOCOUNT OFF
END
GO
