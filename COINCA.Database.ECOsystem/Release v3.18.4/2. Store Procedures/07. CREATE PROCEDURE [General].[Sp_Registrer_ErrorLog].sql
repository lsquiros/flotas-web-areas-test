USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Registrer_ErrorLog]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Registrer_ErrorLog]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Jason Bonilla
-- Create Date: 20/01/2020
-- Description: Registrer Error Log
-- =============================================
CREATE PROCEDURE [General].[Sp_Registrer_ErrorLog]
(
    @pFunctionName varchar(100),
	@pErrorMessage varchar(max),
	@pStackTrace varchar(1000),
	@pDescription varchar(500)
)
AS
BEGIN
    SET NOCOUNT ON

	INSERT INTO [General].[ErrorLog]
	VALUES(
		@pFunctionName,
		@pErrorMessage,
		@pStackTrace,
		@pDescription,
		GETDATE()
	)

	SET NOCOUNT OFF
END
GO
