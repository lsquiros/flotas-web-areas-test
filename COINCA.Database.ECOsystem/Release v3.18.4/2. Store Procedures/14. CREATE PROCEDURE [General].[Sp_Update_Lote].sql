USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Update_Lote]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Update_Lote]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Jason Bonilla
-- Create Date: 29/01/2020
-- Description: Update Parameter EMAIL_LOTE
-- =============================================
CREATE PROCEDURE [General].[Sp_Update_Lote]
AS
BEGIN
    
	DECLARE @LOTE INT

	SET @LOTE = (SELECT CAST([Value] AS INT) FROM [dbo].[GeneralParameters] WHERE [ParameterID]	= 'EMAIL_LOTE')

	UPDATE [dbo].[GeneralParameters]
	SET
	    [dbo].[GeneralParameters].[Value] = @LOTE + 1
	WHERE
		[ParameterID] = 'EMAIL_LOTE'

END
GO
