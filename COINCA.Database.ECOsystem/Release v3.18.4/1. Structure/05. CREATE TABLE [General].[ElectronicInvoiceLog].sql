/****** Object:  Table [General].[ElectronicInvoiceLog]    Script Date: 1/20/2020 11:39:14 AM ******/
DROP TABLE [General].[ElectronicInvoiceLog]
GO

/****** Object:  Table [General].[ElectronicInvoiceLog]    Script Date: 1/20/2020 11:39:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [General].[ElectronicInvoiceLog](
	[ElectronicInvoiceId] [int] IDENTITY(1,1) NOT NULL,
	[TransactionId] [int] NOT NULL,
	[FuelAmount] [decimal](16, 2) NULL,
	[Liters] [decimal](16, 3) NULL,
	[LegalDocument] [varchar](50) NULL,
	[HaciendaClave] [varchar](50) NULL,
	[HaciendaMessage] [varchar](max) NULL,
	[HaciendaMessageDetail] [varchar](max) NULL,
	[WasUpdated] [bit] NOT NULL,
	[RegisterDate] [datetime] NOT NULL,
	[Description] [varchar](max) NULL,
	[Information] [varchar](500) NOT null,
	[Lot] [int] NOT null
 CONSTRAINT [PK_ElectronicInvoiceLog] PRIMARY KEY CLUSTERED 
(
	[ElectronicInvoiceId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


