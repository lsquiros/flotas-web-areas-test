USE [ECOsystemDev]
GO

INSERT INTO [dbo].[GeneralParameters]
(
    [ParameterID]
    ,[Property]
    ,[Description]
    ,[Value]
    ,[NumericValue]
)
VALUES
(
    'EMAIL_LOTE',
    'NA',
    'N�mero de lote de procesamiento actual',
    '1',
    0
),
(
    'EMAIL_HOSTNAME',
    'NA',
    'Hostname utilizado para conectarse a gmail',
    'imap.gmail.com',
    0
),
(
    'EMAIL_PORT',
    'NA',
    'Puerto utilizado para conectarse a gmail',
    '993',
    0
),
(
    'EMAIL_SSL',
    'NA',
    'Protocolo utilizado para conectarse a gmail',
    'true',
    0
),
(
    'EMAIL_USER',
    'NA',
    'Usuario utilizado para conectarse a gmail',
    'facturas@bacflota.com',
    0
),
(
    'EMAIL_PASSWORD',
    'NA',
    'Password utilizado para conectarse a gmail',
    'Flotas3100',
    0
),
(
    'EMAIL_MAILBOX',
    'NA',
    'Carpeta de correos Bandeja de Entrada',
    'INBOX',
    0
),
(
    'HACIENDA_NODO',
    'NA',
    'Nodo en el que viene el mensaje de hacienda',
    'MensajeHacienda',
    0
),
(
    'HACIENDA_CODIGO',
    'NA',
    'Codigo de hacienda para indicar que la factura electronica fue aprobada',
    '1',
    0
),
(
    'FACTURA_NODO',
    'NA',
    'Nodo en el que vienen los datos de la factura electronica',
    'FacturaElectronica',
    0
)

GO


