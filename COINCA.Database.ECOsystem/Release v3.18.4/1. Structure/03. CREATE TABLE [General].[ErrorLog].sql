/****** Object:  Table [General].[ErrorLog]    Script Date: 1/17/2020 11:29:41 AM ******/
DROP TABLE [General].[ErrorLog]
GO

/****** Object:  Table [General].[ErrorLog]    Script Date: 1/17/2020 11:29:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [General].[ErrorLog](
	[ErrorID] [int] IDENTITY(1,1) NOT NULL,
	[FunctionName] [varchar](100) NOT NULL,
	[ErrorMessage] [varchar](max) NOT NULL,
	[StackTrace] [varchar](max) NOT NULL,
	[Description] [varchar](500) NULL,
	[DateLog] [datetime] NOT NULL,
 CONSTRAINT [PK_ErrorLog] PRIMARY KEY CLUSTERED 
(
	[ErrorID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


