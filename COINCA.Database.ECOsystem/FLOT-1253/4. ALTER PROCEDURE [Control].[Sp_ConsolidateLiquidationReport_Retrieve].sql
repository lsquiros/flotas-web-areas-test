USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ConsolidateLiquidationReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_ConsolidateLiquidationReport_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================================
-- Author:		Andrés Oviedo Brenes.
-- Create date: 03/02/2015
-- Description:	Retrieve ConsolidateLiquidationReport information
-- ==========================================================================================================================
-- Melvin Salas - 1/22/2015 - Dinamic Filter
-- Stefano Quiros - 13/04/2016 - Add Void Transactions Validation
-- Stefano Quiros - 14/11/2017 - Add Void Transactions Validation
-- Maria de los Angeles Jimenez Chavarria - APR/03/2019 - Send report program. Include UnitOfCapacityId and UnitOfCapacityName
-- Modify by Marjorie Garbanzo - 28/05/2020 Add PaymentInstrumentCode and PaymentInstrumentType column
-- ===========================================================================================================================
CREATE PROCEDURE [Control].[Sp_ConsolidateLiquidationReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pUserId INT						--@pUserId: User Id
	,@pStartDate DATETIME = NULL
    ,@pEndDate DATETIME = NULL
	,@pCostCenterId INT = NULL
	,@pVehicleGroupId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	
	--DECLARE @pCustomerId INT = 30
	--DECLARE @pYear INT = 2015
	--DECLARE @pMonth INT = 11	

	DECLARE @lUnitCapacityId INT = (SELECT TOP 1 [UnitOfCapacityId] FROM [General].[Customers] WHERE [CustomerId] = @pCustomerId)
	
	DECLARE @lUnitCapacityName VARCHAR(100) = (	
										SELECT TOP 1 d.[Name] 
										FROM [General].[Customers] c
										INNER JOIN [General].[Types] d
											ON d.[TypeId] = c.[UnitOfCapacityId]
										WHERE [CustomerId] = @pCustomerId)

	declare @ty int= (select top (1) t.TypeId from General.Customers c INNER JOIN General.[Types] t ON t.TypeId=c.IssueForId 
	where CustomerId=@pCustomerId)
	
	SELECT v.VehicleId,
		v.PlateId,
		v.Name AS [VehicleName],
		du.Identification,
		vf.Liters,
		ISNULL((SELECT top(1) pv.TrxReportedOdometer 
			FROM General.PerformanceByVehicle pv
			WHERE ((@pYear IS NOT NULL AND @pMonth IS NOT NULL AND MONTH(TrxDate)=@pMonth AND YEAR(TrxDate)=@pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL AND TrxDate BETWEEN @pStartDate AND @pEndDate))
					AND VehicleId=v.VehicleId 
					AND 1 > (
							SELECT ISNULL(Count(1), 0)
							FROM (
								SELECT TOP 1 [CreditCardId],[TransactionPOS],[ProcessorId] 
								FROM [Control].[Transactions]
								WHERE [TransactionId] = pv.TransactionId
							)tx
							INNER JOIN  [Control].[Transactions] tx2
								ON  tx2.[CreditCardId] = tx.[CreditCardId] AND
									tx2.[TransactionPOS] = tx.[TransactionPOS] AND
									tx2.[ProcessorId] = tx.[ProcessorId]
							WHERE (tx2.IsReversed = 1 OR tx2.IsVoid = 1)
						)
			order by PerformanceVehicleId desc), 0)		  
		as TotalOdometer,
		vc.Liters as TankCapacity,
		f.FuelId,
		f.Name as FuelName,
		vu.UnitId,
		vu.Name as UnitName,
		@pCostCenterId [CostCenterId],
		vcc.[Name] as CostCenterName,
		@pVehicleGroupId [VehicleGroupId] ,
		vg.[Name] AS [VehicleGroupName],
		vc.Manufacturer,
		vc.VehicleModel,
		vc.[Year],
		du.Code,
		(case @ty when 100 then (SELECT top(1) cc.CreditCardNumber FROM [Control].CreditCardByDriver ccd INNER JOIN [Control].CreditCard cc  ON ccd.CreditCardId=cc.CreditCardId WHERE CCD.UserId=du.UserId)
		ELSE
		(SELECT top(1) cc.CreditCardNumber FROM [Control].CreditCardByVehicle ccd INNER JOIN [Control].CreditCard cc  ON ccd.CreditCardId=cc.CreditCardId WHERE CCD.VehicleId=V.VehicleId)
		END) AS CreditCardNumber,
		--pay.[Code] AS [PaymentInstrumentCode],
		--(CASE WHEN t.[PaymentInstrumentId] IS NULL 
				--THEN 'Tarjeta'
				--ELSE pit.[Name] END) AS [PaymentInstrumentType],
		@lUnitCapacityId [UnitOfCapacityId],
		@lUnitCapacityName [UnitOfCapacityName]
		FROM General.Vehicles v 
		INNER JOIN [Control].[FuelDistribution] vf ON v.VehicleId=vf.VehicleId
		INNER JOIN General.VehicleCategories vc ON v.VehicleCategoryId =vc.VehicleCategoryId
		INNER JOIN [Control].Fuels f ON vc.DefaultFuelId=f.FuelId
		INNER JOIN General.VehicleCostCenters vcc ON V.CostCenterId=vcc.CostCenterId
		INNER JOIN General.VehicleUnits vu ON vcc.UnitId=vu.UnitId
		INNER JOIN General.VehiclesByUser vbu ON vbu.VehicleId=v.VehicleId
		INNER JOIN General.DriversUsers du ON vbu.UserId=du.UserId
		LEFT JOIN [General].[VehiclesByGroup] vbg ON v.[VehicleId] = vbg.[VehicleId]
		LEFT JOIN [General].[VehicleGroups] vg ON vbg.[VehicleGroupId] = vg.[VehicleGroupId] 

		--INNER JOIN [Control].[PaymentInstruments] pay
			--ON t.[PaymentInstrumentId] = pay.[Id]
		--INNER JOIN [Control].[PaymentInstrumentsTypes] pit
			--ON pay.[typeId] = pit.[Id]


		WHERE V.CustomerId=@pCustomerId 
		AND (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
		AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL AND vf.[Month]=@pMonth AND vf.[Year]=@pYear) OR
			(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL AND vf.[Month] BETWEEN MONTH(@pStartDate) AND MONTH(@pEndDate) AND vf.[Year] BETWEEN YEAR(@pStartDate) AND YEAR(@pEndDate)))
		AND (@pCostCenterId IS NULL OR vcc.[CostCenterId] = @pCostCenterId)
		AND (@pVehicleGroupId IS NULL OR vg.[VehicleGroupId] = @pVehicleGroupId)
		  
	SET NOCOUNT OFF
END

