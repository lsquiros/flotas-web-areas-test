﻿--Device Prueba 8589
​
DECLARE @lStartDate DATETIME = DATEADD(HOUR, -1, GETDATE())
​
CREATE TABLE #Devices
(
	[Device] INT
   ,[UserId] INT
)
​
CREATE TABLE #Result
(
	[Id] INT IDENTITY(1, 1)
   ,[Device] INT
   ,[UserId] INT
   ,[LawScore] DECIMAL(16, 2)
   ,[AdminScore] DECIMAL(16, 2)
)
​
DECLARE @lDevice INT,
		@lUserId INT
​
INSERT INTO #Devices
	SELECT DISTINCT [DeviceReference]
				   ,[UserId]
	FROM [General].[Vehicles] v
	INNER JOIN [General].[Customers] c
		ON c.[CustomerId] = v.[CustomerId]
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[CustomerId] = c.[CustomerId]
	INNER JOIN [General].[Partners] p	
		ON p.[PartnerId] = cp.[PartnerId]
	INNER JOIN [General].[VehiclesByUser] vu
		ON vu.[VehicleId] = v.[VehicleId]
		AND [LastDateDriving] IS NULL
	WHERE c.[IsActive] = 1
	AND (c.[IsDeleted] IS NULL
		 OR c.[IsDeleted] = 0)
	AND v.[Active] = 1
	AND (v.[IsDeleted] = 0 
			OR v.[IsDeleted] IS NULL)	
	AND ([DeviceReference] IS NOT NULL 
			AND [DeviceReference] > 0)
	AND ([ProductTypesId] = 2 
		 OR [ProductTypesId] IS NULL)--[PartnerId] IN (1023,1026,1029,1031,1032,1033,1035)
	GROUP BY [DeviceReference], [UserId]			
​
SET @lDevice = (SELECT MIN([Device]) 
					 FROM #Devices)
​
WHILE @lDevice IS NOT NULL
BEGIN
	
	SELECT @lUserId = [UserId] 
	FROM #Devices 
	WHERE [Device] = @lDevice
​
	CREATE TABLE #Reports
	(
		[Id] INT IDENTITY (1, 1)
	   ,[VSSSPEED] DECIMAL(16, 2)
	   ,[Report] BIGINT
	   ,[MaximumSpeed] INT
	   ,[PermittedSpeed] INT
	   ,[Odometer] INT
	   ,[Longitude] DECIMAL(16,3)
	   ,[Latitude] DECIMAL(16,3)
	)
​
   DECLARE @lLawExceedTravel INT
		  ,@lAdminExceedTravel INT
          ,@lLawExceedCount INT
          ,@lAdminExceedCount INT
          ,@lSumLawExceed INT
          ,@lSumAdminExceed INT
		  ,@lTotalTravel INT
		  ,@LawXVC FLOAT
		  ,@LawIRC FLOAT 
		  ,@LawIPC FLOAT
		  ,@LawVarianza FLOAT
		  ,@LawS2 FLOAT
		  ,@LawSPC FLOAT
		  ,@LawCV FLOAT
		  ,@LawScore FLOAT 
		  ,@AdminXVC FLOAT
		  ,@AdminIRC FLOAT
		  ,@AdminIPC FLOAT
		  ,@AdminVarianza FLOAT
		  ,@AdminS2 FLOAT
		  ,@AdminSPC FLOAT
		  ,@AdminCV FLOAT
		  ,@AdminScore FLOAT 
​
	INSERT INTO #Reports
	SELECT [VSSSPEED]
		  ,[Report]
		  ,NULL
		  ,NULL
		  ,[Odometer]
		  ,[Longitude]
		  ,[Latitude]
	FROM [Reports] r 	
	WHERE [RepDateTime] >= DATEADD(DAY, -1, [RepDateTime])
	      AND [Device] = @lDevice
		  AND [RepDateTime] BETWEEN @lStartDate AND DATEADD(HOUR, 1, @lStartDate)
		  AND [VSSSPEED] > 0	  
	ORDER BY [RepDateTime]	
​
	UPDATE r
	SET [PermittedSpeed] = ISNULL([Efficiency].[Fn_GetPermittedSpeed_Retrieve] (r.[Longitude], r.[Latitude]), 0) --Calculo Velocidad de Ley
	   ,[MaximumSpeed] = ISNULL(vc.[MaximumSpeed], 0) --Calculo Velocidad Administrativa
	FROM #Reports r
	INNER JOIN [General].[Vehicles] v
		ON v.[DeviceReference] = @lDevice 
	INNER JOIN [General].[VehicleCategories] vc
		ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
	
	--SELECT * FROM #Reports WHERE Vssspeed > permittedspeed
​
	;WITH groupsLaw AS (
	    SELECT
		  (Id) - ROW_NUMBER() OVER (ORDER BY id) AS [Flag],
	      [Id] - 1 [Anterior al Exceso],
		  [Id] + 1 [Posterior al Exceso],
		  (SELECT [Odometer] FROM #Reports WHERE [Id] = r1.[Id] - 1) [OdometerBegin],
		  (SELECT [Odometer] FROM #Reports WHERE [Id] = r1.[Id] + 1) [OdometerEnd]
	    FROM #Reports r1
		WHERE r1.[VSSSPEED] > r1.[PermittedSpeed]
		AND r1.[PermittedSpeed] > 0
	  )
	
	SELECT MAX([OdometerEnd]) - MIN([OdometerBegin]) [Recorrido]
	INTO #Travel
	FROM groupsLaw g
	GROUP BY [Flag]
​
	;WITH groupsAdmin AS (
	    SELECT
		  (Id) - ROW_NUMBER() OVER (ORDER BY id) AS [Flag],
	      [Id] - 1 [Anterior al Exceso],
		  [Id] + 1 [Posterior al Exceso],
		  (SELECT [Odometer] FROM #Reports WHERE [Id] = r1.[Id] - 1) [OdometerBegin],
		  (SELECT [Odometer] FROM #Reports WHERE [Id] = r1.[Id] + 1) [OdometerEnd]
	    FROM #Reports r1
		WHERE r1.[VSSSPEED] > r1.[MaximumSpeed]
		AND r1.[MaximumSpeed] > 0
	  )
	
	SELECT MAX([OdometerEnd]) - MIN([OdometerBegin]) [Recorrido]
	INTO #TravelAdmin
	FROM groupsAdmin g
	GROUP BY [Flag]
	
	SET @lLawExceedTravel = (SELECT ISNULL(SUM([Recorrido]), 0) [Recorrido] 
					      FROM #Travel)
​
	SET @lAdminExceedTravel = (SELECT ISNULL(SUM([Recorrido]), 0) [Recorrido] 
					      FROM #TravelAdmin)
	
	SET @lLawExceedCount = (SELECT COUNT(1)
						    FROM #Reports
						    WHERE [VSSSPEED] > [PermittedSpeed]
							AND [PermittedSpeed] > 0
							)
	
	SET @lAdminExceedCount = (SELECT COUNT(1)
							  FROM #Reports
							  WHERE [VSSSPEED] > [MaximumSpeed]
							  AND [MaximumSpeed] > 0)
	
	SET @lSumLawExceed = (SELECT SUM([VSSSPEED] - [PermittedSpeed]) 
						  FROM #Reports
						  WHERE [VSSSPEED] > [PermittedSpeed]
						  AND [PermittedSpeed] > 0)
	
	SET @lSumAdminExceed = (SELECT ISNULL(SUM([VSSSPEED] - [MaximumSpeed]), 0) 
						    FROM #Reports
						    WHERE [VSSSPEED] > [MaximumSpeed]
							AND [MaximumSpeed] > 0)
​
	SET @lTotalTravel = (SELECT MAX([Odometer]) - MIN([Odometer])
						 FROM #Reports)
​

	IF @lLawExceedCount > 0 AND @lSumLawExceed > 0
	BEGIN
		SET @LawXVC = @lSumLawExceed / @lLawExceedCount 	
		
		SET @LawIRC = @lLawExceedTravel / @lTotalTravel	
		
		SET @LawIPC = @LawXVC * @LawIRC
		
		SET @LawVarianza = POWER(@lSumLawExceed - @LawXVC, 2)
		
		SET @LawS2 = @LawVarianza / @lLawExceedCount 
		
		SET @LawSPC = SQRT(@LawS2)
		
		SET @LawCV = @LawSPC / @LawXVC
		
		SET @LawScore = 100 - (@LawIRC * 100) - @LawXVC - @LawSPC
	END​
	ELSE
	BEGIN
		SET @LawScore = 100
	END

	IF @lAdminExceedCount > 0 AND @lSumAdminExceed > 0
	BEGIN
		SET @AdminXVC = @lSumAdminExceed / @lAdminExceedCount 

		SET @AdminIRC = @lAdminExceedTravel / @lTotalTravel

		SET @AdminIPC = @AdminXVC * @AdminIRC

		SET @AdminVarianza = POWER(@lSumAdminExceed - @AdminXVC, 2)

		SET @AdminS2 = @AdminVarianza / @lAdminExceedCount 

		SET @AdminSPC = SQRT(@AdminS2)

		SET @AdminCV = @AdminSPC / @AdminXVC

		SET @AdminScore = 100 - (@AdminIRC * 100) - @AdminXVC - @AdminSPC
	END
	ELSE
	BEGIN
		SET @AdminScore = 100
	END
​
	INSERT INTO #Result
	SELECT @lDevice 
		  ,@lUserId
		  ,@LawScore
		  ,@AdminScore 
​
	DROP TABLE #Reports
	DROP TABLE #Travel
	DROP TABLE #TravelAdmin
​
	DELETE FROM #Devices 
	WHERE [Device] = @lDevice
​
	SELECT @lDevice = MIN([Device]) 
	FROM #Devices
END
​
SELECT * FROM #Result
​
DROP TABLE #Devices
DROP TABLE #Result
​
Select top 10 * from ​[dbo].[Speeding]

select top 10 * from [Operation].[DriversScores]

select top 100 * from [Operation].[DriversScoresDaily] order by date_time desc
