USE [ECOSystem]
GO

-- ======================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Add API Fleetio Parameter
-- ======================================

IF EXISTS(SELECT * FROM [General].[ParametersByCustomer] 
		  WHERE [CustomerId] = 11761 
			    AND [Name] = 'Fleetio_Token')
BEGIN
	DELETE FROM [General].[ParametersByCustomer] 
	WHERE [CustomerId] = 11761 
		  AND [Name] = 'Fleetio_Token'
END

INSERT INTO [General].[ParametersByCustomer]
(
	[CustomerId]
   ,[Name]
   ,[Value]
   ,[ResuourceKey]
   ,[InsertDate]
)
VALUES
(
	11761
   ,'Fleetio_Token'
   ,'Token 4ab987566de2b8edad0fadb8c6d745b66a734396'
   ,'Fleetio_Token'
   ,GETDATE()
)

IF EXISTS(SELECT * FROM [General].[ParametersByCustomer] 
		  WHERE [CustomerId] = 11761 
			    AND [Name] = 'Fleetio_Token_Customer')
BEGIN
	DELETE FROM [General].[ParametersByCustomer] 
	WHERE [CustomerId] = 11761 
		  AND [Name] = 'Fleetio_Token_Customer'
END

INSERT INTO [General].[ParametersByCustomer]
(
	[CustomerId]
   ,[Name]
   ,[Value]
   ,[ResuourceKey]
   ,[InsertDate]
)
VALUES
(
	11761
   ,'Fleetio_Token_Customer'
   ,'8dfb399d18'
   ,'Fleetio_Token_Customer'
   ,GETDATE()
)

IF EXISTS(SELECT * FROM [General].[ParametersByCustomer] 
		  WHERE [CustomerId] = 11761 
			    AND [Name] = 'Fleetio_LastTransactionId')
BEGIN
	DELETE FROM [General].[ParametersByCustomer] 
	WHERE [CustomerId] = 11761 
		  AND [Name] = 'Fleetio_LastTransactionId'
END

INSERT INTO [General].[ParametersByCustomer]
(
	[CustomerId]
   ,[Name]
   ,[Value]
   ,[ResuourceKey]
   ,[InsertDate]
)
VALUES
(
	11761
   ,'Fleetio_LastTransactionId'
   ,(SELECT TOP 1 [TransactionId]
     FROM [Control].[Transactions] t
	 INNER JOIN [General].[Vehicles] v
		ON v.[VehicleId] = t.[VehicleId]
	 WHERE [CustomerId] = 11761
	 ORDER BY t.[InsertDate] DESC)
   ,'Fleetio_LastTransactionId'
   ,GETDATE()
)