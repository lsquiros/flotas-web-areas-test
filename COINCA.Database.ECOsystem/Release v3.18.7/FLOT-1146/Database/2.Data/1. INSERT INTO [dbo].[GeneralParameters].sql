USE [ECOSystem]
GO

-- ======================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Add API Fleetio Parameter
-- ======================================

IF EXISTS(SELECT * FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'FLEETIO_API_URL')
BEGIN
	DELETE FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'FLEETIO_API_URL'
END

INSERT INTO [dbo].[GeneralParameters]
VALUES
(
	'FLEETIO_API_URL'
   ,'URL'
   ,'Ruta del API de Fleetio'
   ,'https://secure.fleetio.com/api/v1/'
   ,0
)