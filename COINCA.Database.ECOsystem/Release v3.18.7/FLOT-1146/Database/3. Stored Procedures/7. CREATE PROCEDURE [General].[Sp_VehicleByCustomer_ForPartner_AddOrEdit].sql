USE [ECOSystem]
GO

IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_VehicleByCustomer_ForPartner_AddOrEdit' AND TYPE = 'P')
	DROP PROC [General].[Sp_VehicleByCustomer_ForPartner_AddOrEdit]
GO


/****** Object:  StoredProcedure [General].[Sp_VehicleByCustomer_ForPartner_AddOrEdit]    Script Date: 26/03/2020 3:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		 Gerald Solano
-- Create date:  27/09/2018
-- Description:	 Insert or Update Vehicle Client from Partner Profile
-- Modify by:	 Gerald Solano - 11/10/2018 - Validate Chasis for new vehicles
-- Modify by:	 Gerald Solano - 28/11/2018 - Add classification type to vehicle
-- Modify by:	 Gerald Solano - 11/01/2019 - Are passed the previous values for update the plate and chassis values.
-- Modify by:	 Gerald Solano - 14/05/2019 - Added the columns with reference to Support in the process of insert and update
-- Modify by:	 Gerald Solano - 06/06/2019 - Added logic to link vehicles to CAFSA and Lexus client.
-- Modify by:    Jason Bonilla - 26/03/2020 - Added Field FleetioId
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleByCustomer_ForPartner_AddOrEdit]
(
	 @pVehicleId INT = NULL							--@pVehicleId: PK of the table
	,@pPlateId VARCHAR(10)							--@pPlateId: Vehicle Plate Number
	,@pName VARCHAR(250) 							--@pName: Vehicle Name
	,@pCustomerId INT = NULL						--@pCustomerId: FK of Customer
	,@pColour VARCHAR(50) = NULL     				--@pColour: Vehicle Colour
	,@pChassis VARCHAR(50) = NULL					--@pChassis: Vehicle Chassis or VIN
	,@pAVL VARCHAR(50) = NULL						--@PAVL: Vehicle AVL 	
	,@pOdometer FLOAT = 0							--@pInitialOdometer: Odometer of Vehicle
	,@pImei DECIMAL = NULL							--@pImei: Imei number of AVL, use to validate IntrackReference number
	,@pBrandName VARCHAR(250) = NULL
	,@pModelName VARCHAR(250) = NULL
	,@pModelYear INT = 0
	,@pSupportTicket VARCHAR(20) = NULL
	,@pSupportTicketURL VARCHAR(150) = NULL
	,@pClassificationId INT = NULL
	,@pLoggedUserId INT = 0							--@pLoggedUserId: Logged UserId that executes the operation
	,@pOperationBAC VARCHAR(100) = NULL
	--,@pFleetioId BIGINT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lUserIdByVehicle INT 
            DECLARE @lVehiclesByUserId INT
            DECLARE @lResult INT
			DECLARE @lIntrackReference INT
			DECLARE @lDeviceReference INT							
			DECLARE @lAdmSpeedLimitPrev INT
            DECLARE @lCustomerPlateCount INT = 0
			DECLARE @lCustomerChasisCount INT = 0
			DECLARE @pAdministrativeSpeedLimit INT = 110
			DECLARE @pIntrackReference INT = 0
 			
			DECLARE @pVehicleCategoryId INT = NULL
			DECLARE @pUnitId INT = 0
			DECLARE @pCostCenterId INT = 0
			DECLARE @pIsAClientLinked INT = 0
			DECLARE @OldPlate VARCHAR(10) = NULL
			DECLARE @OldChasis VARCHAR(50) = NULL

            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END								
			 
		-- GET INFO FOR LINKED CUSTOMERS
			DECLARE @CafsaCustomer INT = 0
			DECLARE @LexusCustomer INT = 0
			DECLARE @CafsaClassificationId INT = 0
			DECLARE @LexusClassificationId INT = 0
			DECLARE @CustomerLinked INT = 0

			SELECT TOP 1 @LexusCustomer = CustomerId FROM [General].[PartnerLinkedToCustomer]
			WHERE PartnerId = (SELECT TOP 1 p.[PartnerId] FROM [General].[Partners] p WHERE p.[Name] = 'Lexus CR')

			SELECT TOP 1 @CafsaCustomer = CustomerId FROM [General].[PartnerLinkedToCustomer]
			WHERE PartnerId = (SELECT TOP 1 p.[PartnerId] FROM [General].[Partners] p WHERE p.[Name] = 'CAFSA')

			SELECT @LexusClassificationId = Id FROM [General].[ClassificationType] WHERE [Name] = 'Lexus'

			SELECT @CafsaClassificationId = Id FROM [General].[ClassificationType] WHERE [Name] = 'CAFSA'

			-- /////////////////////////////						   
			IF (@pVehicleId IS NULL)
			BEGIN
			
				-- VALIDAMOS EN LA INSERCIÓN SI YA EXISTE LA PLACA / CHASIS
				SELECT @lCustomerPlateCount = COUNT(*)       
                FROM [General].[Vehicles]
                WHERE [PlateId] = @pPlateId AND ([IsDeleted] = 0 OR [IsDeleted] IS NULL)

				SELECT @lCustomerChasisCount = COUNT(*)       
                FROM [General].[Vehicles]
                WHERE ([Chassis] = @pChassis OR [Chassis] LIKE '%'+ @pChassis +'%') AND ([IsDeleted] = 0 OR [IsDeleted] IS NULL)

				--Validate vehicle plate / chasis is not duplicate for the customer
				IF @lCustomerPlateCount > 0
				BEGIN
					RAISERROR ('PLATE_IS_EXIST', 16, 1)
				END
				ELSE IF @lCustomerChasisCount > 0
				BEGIN 
					RAISERROR ('CHASIS_IS_EXIST', 16, 1)
				END
				ELSE
				BEGIN
					
					SELECT @pVehicleCategoryId = [VehicleCategoryId]
					FROM [General].[VehicleCategories]
					WHERE [Manufacturer] = @pBrandName
						  AND [VehicleModel] = @pModelName
						  AND [Year] = @pModelYear
						  AND [CustomerId] = @pCustomerId

                    IF @pVehicleCategoryId IS NULL
					BEGIN
						-- ADD VEHICLE CATEGORY DEFAULT
						INSERT INTO [General].[VehicleCategories]
								([Manufacturer]
								,[Type]
								,[DefaultFuelId]
								,[Liters]
								,[VehicleModel]
								,[CustomerId]
								,[Icon]
								,[MaximumSpeed]
								,[MaximumRPM]
								,[Year]
								,[Weight]
								,[LoadType]
								,[CylinderCapacity]
								,[InsertDate]
								,[InsertUserId]
								,[DefaultPerformance]
								,[SpeedDelimited])
						VALUES	(@pBrandName,'Tipo',1,45,@pModelName,@pCustomerId,'alza32E.png'
								,100,0, @pModelYear,40,700,0,GETUTCDATE(),0,0,0)

						SET @pVehicleCategoryId =  SCOPE_IDENTITY()
						--////////////////////////////////////////////////
					END

					-- ADD UNIT DEFAULT
					INSERT INTO [General].[VehicleUnits]
						([Name]
						,[CustomerId]
						,[InsertDate]
						,[InsertUserId])
					VALUES	('Unidad'
							,@pCustomerId
							,GETUTCDATE()
							,0)

					SET @pUnitId = SCOPE_IDENTITY()
					--////////////////////////////////////////////////


					-- ADD COST CENTER DEFAULT
					INSERT INTO [General].[VehicleCostCenters]
						([Name]
						,[Code]
						,[UnitId]
						,[InsertDate]
						,[InsertUserId])
					VALUES	('Vehículo '
							,UPPER(@pPlateId)
							,@pUnitId
							,GETUTCDATE()
							,0)

					SET @pCostCenterId = SCOPE_IDENTITY()
					--///////////////////////////////////////////////

					-- Find Device Reference if exist
					IF(@pIntrackReference IS NULL OR @pIntrackReference <= 0)
					BEGIN
						SELECT TOP 1 @pIntrackReference = [Vehiculo]
						FROM [dbo].[Vehiculos]
						WHERE [vin] LIKE '%'+ LTRIM(RTRIM(@pChassis)) + '%' OR
								[chasis] LIKE '%'+ LTRIM(RTRIM(@pChassis)) + '%'
						ORDER BY [vehiculo] DESC
					END 

					SELECT @lDeviceReference = de.device
					FROM [dbo].[Composiciones] c
					INNER JOIN DispositivosAVL d ON d.dispositivo = c.dispositivo
					INNER JOIN Devices de ON d.terminalId = Cast(de.UnitID AS VARCHAR)
					WHERE c.[vehiculo] = @pIntrackReference


					-- ADD VEHICLE
					INSERT INTO [General].[Vehicles]
							([PlateId]
							,[Name]
							,[CostCenterId]
							,[CustomerId]
							,[VehicleCategoryId]
							,[Active]
							,[Colour]
							,[Chassis]
							,[AVL]							
							,[PhoneNumber]
							,[PeriodicityId]					
							,[IntrackReference]
							,[DeviceReference]
							,[Imei]
							,[Odometer]
							,[InsertDate]
							,[InsertUserId]
							,[SupportTicket]
							,[SupportTicketURL]
							,[OperationBAC]
							,[FleetioId])
					VALUES	(UPPER(@pPlateId)
							,@pName
							,@pCostCenterId
							,@pCustomerId
							,@pVehicleCategoryId
							,1
							,@pColour
							,@pChassis							
							,@pAVL							
							,'00000000'							
							,0
							,@pIntrackReference
							,@lDeviceReference
							,@pImei
							,@pOdometer
							,GETUTCDATE()
							,@pLoggedUserId
							,@pSupportTicket
							,@pSupportTicketURL
							,@pOperationBAC
							--,@pFleetioId
							)
							
					SET @pVehicleId = SCOPE_IDENTITY() 

					--Inserta registro en distribucion de combustible por defect, solamente la primera vez
					DECLARE @pMonth INT = MONTH(GETDATE()); 
					DECLARE @pYear INT = YEAR(GETDATE()); 
										
					INSERT INTO [Control].[FuelDistribution]
								([VehicleId]
								,[DriverId]
								,[Month]
								,[Year]
								,[Amount]
								,[Liters]
								,[AdditionalAmount]
								,[AdditionalLiters]
								,[InsertDate]
								,[InsertUserId]
								,[ModifyDate]
								,[ModifyUserId])
							VALUES
								(@pVehicleId
								,NULL
								,@pMonth
								,@pYear
								,0.00
								,0.00
								,0.00
								,0.00
								,GETDATE()
								,0
								,NULL
								,0)


					--ADD CLASSIFICATION
					IF(@pClassificationId IS NOT NULL AND @pClassificationId > 0)
					BEGIN 						
						INSERT INTO [General].[VehicleClassification]
								([VehicleId]
								,[ClassificationId])
							VALUES
								(@pVehicleId
								,@pClassificationId)
								
						SET @lRowCount = @@ROWCOUNT
						
						-- CUSTOMER LINKED VALIDATION
						IF (@pClassificationId = @LexusClassificationId OR @pClassificationId = @CafsaClassificationId)
						BEGIN
							SET @CustomerLinked = @LexusCustomer

							IF(@pClassificationId = @CafsaClassificationId) BEGIN 
								SET @CustomerLinked = @CafsaCustomer
							END

							IF NOT EXISTS(SELECT 1 FROM [General].[VehiclesByCustomers] WHERE VehicleId = @pVehicleId) 
							BEGIN								
								INSERT INTO [General].[VehiclesByCustomers]([VehicleId],[CustomerId]) VALUES (@pVehicleId,@CustomerLinked)
							END
						END			
						
					END

				END -- END DUPLICATE PLATE VALIDATION
				
			END
			ELSE
			BEGIN
				
				-- VALIDAMOS EN LA ACTUALIZACIÓN SI YA EXISTE OTRO VEHICULO CON LA MISMA PLACA
				SELECT @lCustomerPlateCount = COUNT(*)				  
				FROM [General].[Vehicles]
				WHERE [CustomerId] = @pCustomerId 
					  AND [PlateId] = @pPlateId 
					  AND [VehicleId] <> @pVehicleId 
					  AND ([IsDeleted] IS NULL OR [IsDeleted] = 0)

				--Validate vehicle plate is not duplicate for the customer
				IF @lCustomerPlateCount > 0
				BEGIN
					RAISERROR ('PLATE_IS_EXIST', 16, 1)
				END
				ELSE
				BEGIN
					--Validamos si un vehiculo esta siendo actualizado por un cliente que esta asociado y no por el cliente original del vehiculo
					--Esto evitara que se le actualicen las referencias del vehiculo y por ende evitara errores en los flujos existentes
					IF EXISTS(SELECT 1 FROM [General].[VehiclesByCustomers] WHERE CustomerId = @pCustomerId AND VehicleId = @pVehicleId )	
					BEGIN 
						SET @pIsAClientLinked = 1
					END
					
					IF(@pIsAClientLinked = 0)
					BEGIN 
						--Update Vehicle Category and Device Reference
						SELECT @pIntrackReference = [IntrackReference]
						FROM [General].[Vehicles]
						WHERE [VehicleId] = @pVehicleId

						IF(@pIntrackReference IS NULL OR @pIntrackReference <= 0)
						BEGIN 
							SELECT @pIntrackReference = [Vehiculo]
							FROM [dbo].[VehicleByVehicleGPS]
							WHERE [VehicleId] = @pVehicleId

							IF(@pIntrackReference IS NULL OR @pIntrackReference <= 0)
							BEGIN
								SELECT TOP 1 @pIntrackReference = [Vehiculo]
								FROM [dbo].[Vehiculos]
								WHERE [vin] LIKE '%'+ LTRIM(RTRIM(@pChassis)) + '%' OR
									  [chasis] LIKE '%'+ LTRIM(RTRIM(@pChassis)) + '%'
								ORDER BY [vehiculo] DESC
							END 
						END

					SELECT @pVehicleCategoryId = [VehicleCategoryId]
					FROM [General].[VehicleCategories]
					WHERE [Manufacturer] = @pBrandName
						  AND [VehicleModel] = @pModelName
						  AND [Year] = @pModelYear
						  AND [CustomerId] = @pCustomerId

                    IF @pVehicleCategoryId IS NULL
					BEGIN
						-- ADD VEHICLE CATEGORY DEFAULT
						INSERT INTO [General].[VehicleCategories]
								([Manufacturer]
								,[Type]
								,[DefaultFuelId]
								,[Liters]
								,[VehicleModel]
								,[CustomerId]
								,[Icon]
								,[MaximumSpeed]
								,[MaximumRPM]
								,[Year]
								,[Weight]
								,[LoadType]
								,[CylinderCapacity]
								,[InsertDate]
								,[InsertUserId]
								,[DefaultPerformance]
								,[SpeedDelimited])
						VALUES	(@pBrandName,'Tipo',1,45,@pModelName,@pCustomerId,'alza32E.png'
								,100,0, @pModelYear,40,700,0,GETUTCDATE(),0,0,0)

						SET @pVehicleCategoryId =  SCOPE_IDENTITY()
						--////////////////////////////////////////////////
					END

						--Update Device Reference
						SELECT TOP 1 @lDeviceReference = de.device
						FROM [dbo].[Composiciones] c
						INNER JOIN DispositivosAVL d ON d.dispositivo = c.dispositivo
						INNER JOIN Devices de ON d.terminalId = Cast(de.UnitID AS VARCHAR)
						WHERE c.[vehiculo] = @pIntrackReference

						--Obtenemos los valores anteriores
						SELECT 
							@OldPlate = [PlateId],
							@OldChasis = [Chassis]
						FROM [General].[Vehicles]
						WHERE [VehicleId] = @pVehicleId

						--Update Vehicle
						UPDATE [General].[Vehicles]
							SET  [PlateId] = UPPER(@pPlateId)
								,[Name] = @pName
								,[CustomerId] = @pCustomerId
								,[Colour] = @pColour
								,[Chassis] = @pChassis
								,[VehicleCategoryId] = @pVehicleCategoryId
								,[AVL] = @pAVL
								,[IntrackReference] = @pIntrackReference
								,[DeviceReference] = @lDeviceReference
								,[Imei] = @pImei
								,[Odometer] = @pOdometer
								,[ModifyDate] = GETUTCDATE()
								,[ModifyUserId] = @pLoggedUserId
								,[SupportTicket] = @pSupportTicket
								,[SupportTicketURL] = @pSupportTicketURL
								,[OperationBAC] = @pOperationBAC
								--,[FleetioId] = @pFleetioId
						WHERE [VehicleId] = @pVehicleId
						  --AND [RowVersion] = @pRowVersion
					END
					ELSE
					BEGIN
						DECLARE @LogMessage VARCHAR(500) = ''

						SET @LogMessage = CONCAT('[UPDATE_LINKED_VEHICLE] VehicleId = ', CONVERT(VARCHAR(100), @pVehicleId), 
									 '| CustomerId = ', CONVERT(VARCHAR(100), @pCustomerId), 
									 '| Message: Vehiculo no se puede actualizar debido a que la operacion la realiza un Cliente asociado, no el Cliente que pertenece al vehiculo.')

						 --Inserta el control de logs
						INSERT INTO [General].[EventDBLog] ([ProcessName],[Message],[IsInfo],[IsWarning],[IsError],[LogUTCDateTime])
						VALUES ('[General].[Sp_VehicleByCustomer_ForPartner_AddOrEdit]',@LogMessage,0,0,1,GETDATE())
						
					END

					--ADD OR UPDATE CLASSIFICATION
					IF(@pClassificationId IS NOT NULL AND @pClassificationId > 0)
					BEGIN 	
						IF NOT EXISTS(SELECT 1 FROM [General].[VehicleClassification] WHERE [VehicleId] = @pVehicleId)
						BEGIN
							INSERT INTO [General].[VehicleClassification]
									([VehicleId]
									,[ClassificationId])
								VALUES
									(@pVehicleId
									,@pClassificationId)
						END
						ELSE BEGIN
							UPDATE [General].[VehicleClassification]
							SET [ClassificationId] = @pClassificationId
							WHERE [VehicleId] = @pVehicleId
						END
						
						SET @lRowCount = @@ROWCOUNT
						
						-- CUSTOMER LINKED VALIDATION
						IF (@pClassificationId = @LexusClassificationId OR @pClassificationId = @CafsaClassificationId)
						BEGIN
							SET @CustomerLinked = @LexusCustomer

							IF(@pClassificationId = @CafsaClassificationId) BEGIN 
								SET @CustomerLinked = @CafsaCustomer
							END

							IF EXISTS(SELECT 1 FROM [General].[VehiclesByCustomers] WHERE VehicleId = @pVehicleId) 
							BEGIN								
								UPDATE [General].[VehiclesByCustomers]
								SET CustomerId = @CustomerLinked
								WHERE VehicleId = @pVehicleId
							END	
							ELSE BEGIN 
								INSERT INTO [General].[VehiclesByCustomers]([VehicleId],[CustomerId]) VALUES (@pVehicleId,@CustomerLinked)
							END						
						END
						ELSE BEGIN 							
							IF EXISTS(SELECT 1 FROM [General].[VehiclesByCustomers] WHERE VehicleId = @pVehicleId) 
							BEGIN
								DELETE FROM [General].[VehiclesByCustomers]	WHERE VehicleId = @pVehicleId
							END
						END
					END

				END --END DUPLICATE PLATE VALIDATION 
			END          
			IF(@lRowCount = 0)
			BEGIN
				SET @lRowCount = @@ROWCOUNT
			END
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
				--Replicate data in Atrack Database
				IF @pVehicleId IS NOT NULL AND @pAdministrativeSpeedLimit IS NOT NULL --and IsNull(@lAdmSpeedLimitPrev, 0) <> IsNull(@pAdministrativeSpeedLimit, 0)
				BEGIN
					IF @lIntrackReference = 0 
						Set @lIntrackReference = null
					IF @lDeviceReference = 0 
						Set @lDeviceReference = null
					--Aunque hubiera Intrack References, si no hay DeviceRefence no se ejecuta la replicación
					IF @lDeviceReference is not null
					BEGIN
						Exec General.Sp_Vehicles_Replication @pVehicleId, @pCustomerId, @lIntrackReference, @lDeviceReference, @pAdministrativeSpeedLimit, @pResult = @lResult output, @pErrorDesc = @lErrorMessage output						
					END
				END --End Replication
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. Version verification failed.', 16, 1)
			END

			--Add homologation for Vehicle ECO and Vehicle Intrack
			SET @pPlateId = UPPER(@pPlateId)
			
			EXEC [dbo].[IntrackService_AddOrEditVehicleToIntrack]
			 @pVehicleId = @pVehicleId, 
			 @pCustomerId = @pCustomerId,
			 @pPlate =  @pPlateId,
			 @pChasis = @pChassis, 
			 @pBrandName = @pBrandName, 
			 @pColor = @pColour, 
			 @pYearModel = @pModelYear,
			 @pOldPlate = @OldPlate, 
			 @pOldChasis = @OldChasis
			 
			 --Validate linked Customer
			 IF(@pIsAClientLinked = 0)
  			 BEGIN 
					DECLARE @PartnerId INT = 0
					DECLARE @LinkedCustomerId INT = 0
					
					SELECT @PartnerId = PartnerId FROM [General].[CustomersByPartner]
					WHERE CustomerId = @pCustomerId
					
					IF EXISTS(SELECT 1 FROM [General].[PartnerLinkedToCustomer] WHERE PartnerId = @PartnerId)
					BEGIN 
						SELECT @LinkedCustomerId = MIN(CustomerId) FROM [General].[PartnerLinkedToCustomer] WHERE PartnerId = @PartnerId
						
						WHILE(@LinkedCustomerId IS NOT NULL)
						BEGIN
						
							IF NOT EXISTS(SELECT 1 FROM [General].[VehiclesByCustomers] WHERE CustomerId = @LinkedCustomerId AND VehicleId = @pVehicleId)
							BEGIN 
								INSERT INTO [General].[VehiclesByCustomers]
											   ([VehicleId]
											   ,[CustomerId])
										 VALUES
											   (@pVehicleId
											   ,@LinkedCustomerId)
							END
						
							--NEXT LINKED CLIENT	
							SELECT @LinkedCustomerId = MIN(CustomerId) FROM [General].[PartnerLinkedToCustomer] 
							WHERE PartnerId = @PartnerId AND CustomerId > @LinkedCustomerId 
						END
					END
			END
			--END Validate linked Customer
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
