USE [ECOSystem]
GO

IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_CustomersFleetio_Retrieve' AND TYPE = 'P')
	DROP PROC [General].[Sp_CustomersFleetio_Retrieve]
GO

-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Retrieve Customers that have Fleetio
-- ================================================================================================

 CREATE PROCEDURE [General].[Sp_CustomersFleetio_Retrieve]
 AS
 BEGIN	
	SET NOCOUNT ON	
		SELECT [CustomerId]
			  ,(SELECT [Value]
				FROM [General].[ParametersByCustomer]
				WHERE [CustomerId] = c.[CustomerId]
				AND [Name] = 'Fleetio_Token') [Token]
			  ,(SELECT [Value]
				FROM [General].[ParametersByCustomer]
				WHERE [CustomerId] = c.[CustomerId]
				AND [Name] = 'Fleetio_Token_Customer') [CustomerToken]
			  ,(SELECT [Value]
			    FROM [GeneralParameters]
				WHERE [ParameterID] = 'FLEETIO_API_URL') [ApiUrl]
			  ,(SELECT [Value]
			    FROM [General].[ParametersByCustomer]
				WHERE [CustomerId] = c.[CustomerId]
				AND [Name] = 'Fleetio_LastTransactionId') [TransactionId]
		FROM [General].[Customers] c
		WHERE [HasFleetio] = 1
    SET NOCOUNT OFF
 END

GO

