USE [ECOSystem]
GO

IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_Customers_AddOrEdit' AND TYPE = 'P')
	DROP PROC [General].[Sp_Customers_AddOrEdit]
GO

/****** Object:  StoredProcedure [General].[Sp_Customers_AddOrEdit]    Script Date: 24/03/2020 5:28:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================  
-- Summary:    Insert or Update Customer information  
-- Modified By:   Gerald Solano  
-- Modified date: 04/04/2016  
-- Description:   Add column CostCardAdmin for insert or update process  
-- Modify: Henry Retana 11/28/2016  
-- Validates if the values for the customer are nulls  
-- Modify: Henry Retana 03/05/2017  
-- Add the transaccition offline parameter to the customer  
-- Modify: Kevin Peña 15/11/2017  
-- Add the isDemo parameter to the customer  
-- Modify: Esteban Solís 30/11/2017  
-- When a new customer is created, the Rent Id is assigned to the GPSModalityId field by default
-- Modify: Henry Retana - 14/05/2018  
-- Add the new customer to the users selected
-- Modify: Henry Retana - 20/06/2018  
-- Add SMS Alarms
-- Modify: Henry Retana - 26/07/2018  
-- Add Customer Manager Id
-- Modify: Henry Retana - 14/08/2019
-- Add flag for CostCenterFuelDistribution
-- Modify: Juan Carlos Santamaria - 11/11/2019
-- Add FIELD SapCode
-- Modify: Jason Bonilla - 31/01/2020 Add FuelPermit
-- Modify: Jason Bonilla - 24/03/2020 Add HasFleetio
-- ================================================================================================ 
CREATE PROCEDURE [General].[Sp_Customers_AddOrEdit]  
(  
	  @pCustomerId INT = NULL  
	 ,@pName VARCHAR(250)  
	 ,@pCurrencyId INT = NULL  
	 ,@pLogo VARCHAR(MAX) = NULL  
	 ,@pAccountNumber VARCHAR(50) = NULL  
	 ,@pIsActive BIT  
	 ,@pUnitOfCapacityId INT = 0  
	 ,@pIssueForId INT = 101  
	 ,@pCreditCardType CHAR(1) = 'C'  
	 ,@pAvailableModules VARCHAR(100)  
	 ,@pCostCardAdmin NUMERIC(16,2) = 0  
	 ,@pLoggedUserId INT  
	 ,@pRowVersion TIMESTAMP  
	 ,@pPartnerId INT  
	 ,@pTransactionsOffline BIT = 0  
	 ,@pisDemo BIT = 0
	 ,@pSMSAlarms BIT = 0
	 ,@pIsDemoAlert DATETIME = NULL
	 ,@pCustomerManagerId VARCHAR(MAX) = NULL
	 ,@pCustomerCharges INT = NULL
	 ,@pDecryptedName VARCHAR(250) = NULL
	 ,@pCostCenterFuelDistribution BIT = NULL
	 ,@pSapCode VARCHAR(50) = NULL
	 ,@pFuelPermit BIT = NULL
	 ,@pFuelMargin VARCHAR(50) = NULL
	 ,@pHasFleetio BIT = NULL
)  	 
AS  
BEGIN  
   
	SET NOCOUNT ON  
    SET XACT_ABORT ON  
      
    BEGIN TRY  
		BEGIN TRANSACTION  

		DECLARE	@lErrorMessage NVARCHAR(4000)  
				,@lErrorSeverity INT  
				,@lErrorState INT  
				,@GPSModalityId INT = NULL     
				,@lTimeZoneParameter INT 
				,@lClosingDate DATETIME = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)   
				
		SELECT @lTimeZoneParameter = [TimeZone] 
		FROM [General].[Countries] co 
		INNER JOIN [General].[Customers] cu 
			ON co.[CountryId] = cu.[CountryId]
		WHERE cu.[CustomerId] = @pCustomerId 
				
		
		CREATE TABLE #Available
		(
			[CreditCardId] INT
		   ,[FuelId] INT
		   ,[AmountAvailable] DECIMAL (16, 2)
		   ,[LitersAvailable] DECIMAL (16, 3)
			
		)

		CREATE TABLE #Fuels
		(
			[FuelId] INT
		   ,[FuelDistribution] DECIMAL (16, 2)
		)
					     

		DECLARE @pCountryId INT =  (
									SELECT	[CountryId]  
									FROM	[General].[Partners] p   
									WHERE	p.[PartnerId] = @pPartnerId
								   )  
		
		SET @GPSModalityId =	(
									SELECT	GPSMOdalityId 
									FROM	[Control].[GPSModality] 
									WHERE	Name ='Alquilado'
								)
		
		--SET NAME FOR API
		IF @pDecryptedName IS NULL SET @pDecryptedName = @pName

		--IF THE CX IS DEMO, SET THE ALERT FOR NEXT MONTH
		IF @pIsDemoAlert IS NULL AND @pisDemo = 1 SET @pIsDemoAlert = DATEADD(MONTH, 1, GETDATE())

		--VALIDATES CX IN CHARGE OF BILL
		--DELETE CURRENT CONFIGURATION AND ADD NEW ONE
		DELETE FROM [General].[CustomerChargesByCustomer]
		WHERE [CustomerId] = @pCustomerId

		IF @pCustomerCharges IS NOT NULL AND @pCustomerCharges > 0
		BEGIN 
			INSERT INTO [General].[CustomerChargesByCustomer]
			VALUES 
			(
				@pCustomerId, 
				@pCustomerCharges,
				@pPartnerId,
				@pLoggedUserId,
				GETDATE()
			)
		END

		IF @pUnitOfCapacityId IS NULL  
		BEGIN  
			SELECT @pUnitOfCapacityId = [CapacityUnitId]  
			FROM [General].[Partners]  
			WHERE [PartnerId] = @pPartnerId  
		END  
  
		IF @pCurrencyId IS NULL OR @pCurrencyId = 0  
		BEGIN  
			SELECT @pCurrencyId = co.[CurrencyId]  
			FROM [General].[Countries] co       
			INNER JOIN [General].[Partners] p  
				ON co.[CountryId] = p.[CountryId]  
			WHERE p.[PartnerId] = @pPartnerId  
		END  
     
		IF @pCustomerId IS NULL  
		BEGIN     
			INSERT INTO [General].[Customers]  
			(  
				 [Name]  
				,[CurrencyId]  
				,[Logo]  
				,[IsActive]  
				,[UnitOfCapacityId]  
				,[IssueForId]  
				,[CreditCardType]  
				,[InsertDate]  
				,[InsertUserId]  
				,[CountryId]  
				,[AccountNumber]  
				,[AvailableModules]  
				,[CostCardAdmin]  
				,[TransactionsOffline]  
				,[isDemo]
				,[GPSModalityId]
				,[CustomerManagerId]
				,[SMSAlarms]
				,[CostCenterFuelDistribution]
				,[SapCode]
				,[FuelPermit]
				,[HasFleetio]
			)  
			VALUES   
			(  
				@pName  
				,@pCurrencyId  
				,@pLogo  
				,@pIsActive  
				,@pUnitOfCapacityId  
				,@pIssueForId  
				,@pCreditCardType  
				,GETUTCDATE()  
				,@pLoggedUserId  
				,@pCountryId  
				,@pAccountNumber  
				,@pAvailableModules  
				,@pCostCardAdmin  
				,@pTransactionsOffline  
				,@pisDemo 
				,@GPSModalityId 
				,@pCustomerManagerId
				,@pSMSAlarms
				,@pCostCenterFuelDistribution
				,@pSapCode
				,@pFuelPermit
				,@pHasFleetio
			)
        
			SET @pCustomerId = SCOPE_IDENTITY()
			
			IF NOT EXISTS(SELECT * FROM [General].[ParametersByCustomer] 
					  WHERE [CustomerId] = @pCustomerId
							AND [Name] = 'Fleetio_LastTransactionId')
			BEGIN
				INSERT INTO [General].[ParametersByCustomer]
				([CustomerId],[Name],[Value],[ResuourceKey],[InsertDate])
				VALUES(
					@pCustomerId,
					'Fleetio_LastTransactionId',
					0,
					'Fleetio_LastTransactionId',
					GETDATE()
				)
			END

			INSERT INTO [General].[CustomersByPartner]
			(
				[CustomerId],
				[PartnerId],
				[IsDefault]
			)
			VALUES
			(
				@pCustomerId,
				@pPartnerId,
				1
			)  

			--ADD THE NEW CUSTOMER TO THE USERS SELECTED
			EXEC [General].[Sp_AddNewCustomerToUser_AddOrEdit] @pCustomerId
  
			-- SE INSERTAN LOS VALORES EN CERO POR DEFECTO PARA QUE NO EXISTA ERROR EN LA DISTRIBUCION DE CREDITO  
			INSERT INTO [Control].[FuelsByCredit]
			(
				[FuelId],
				[CustomerId],
				[Month],
				[Year],
				[Total],
				[Assigned],
				[InsertDate],
				[InsertUserId]
			)   
			SELECT DISTINCT FuelId  
							,@pCustomerid
							,MONTH(GETDATE())
							,YEAR(GETDATE())
							,0
							,0
							,GETDATE()
							,@pLoggedUserId
			FROM [Control].[PartnerFuel] 
			WHERE [Partnerid] = @pPartnerId  
			AND [History] IS NULL     

			INSERT INTO [Control].[BudgetClosingPeriodByCustomer]
			(
			 	[CustomerId]
				,[Elapse]
				,[Day]
				,[Repeat]
				,[InsertDate]
				,[LastExecutionDate]
			)
			VALUES 
			(
			 	@pCustomerId
				,30
				,1
				,1
				,GETDATE()
				,DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0)
			)
			EXEC [General].[SP_DefaultTransactionsRulesForCustomer_Add] @pCustomerId, @pLoggedUserId

			IF @pFuelPermit = 1
			BEGIN
				UPDATE [Control].[TransactionsRulesByCustomer]
				SET [RuleActive] = 1
				WHERE [CustomerId] = @pCustomerId	
					  AND [RuleId] = 9

				UPDATE trv
				SET trv.[RuleActive] = 1
				FROM [Control].[TransactionsRulesByVehicle] trv
				INNER JOIN [General].[Vehicles] v
					ON v.[VehicleId] = trv.[VehicleId]
				WHERE [CustomerId] = @pCustomerId	
					  AND [RuleId] = 9

				UPDATE trc
				SET trc.[RuleActive] = 1
				FROM [Control].[TransactionsRulesByCostCenter] trc
				INNER JOIN [General].[VehicleCostCenters] vcc
					ON trc.[CostCenterId] = vcc.[CostCenterId]
				INNER JOIN [General].[VehicleUnits] vu	
					ON vu.UnitId = vcc.UnitId
				WHERE
				    vu.[CustomerId]	= @pCustomerId	
				    AND trc.[RuleId] = 9

				INSERT INTO [General].[ParametersbyCustomer]
				(
					[CustomerId]
				   ,[Name]
				   ,[Value]
				   ,[ResuourceKey]
				   ,[InsertUserId]
				   ,[InsertDate]
				)
				VALUES
				(
					@pCustomerId
				   ,'FuelsMargin'
				   ,@pFuelMargin
				   ,'TR_PARAMETERS'
				   ,@pLoggedUserId
				   ,GETDATE()
				)							
			END
		END  
		ELSE  
		BEGIN  
			UPDATE [General].[Customers]  
			SET  [Name] = @pName  
				,[CurrencyId] = @pCurrencyId  
				,[Logo] = @pLogo  
				,[IsActive] = @pIsActive  
				,[UnitOfCapacityId] = @pUnitOfCapacityId  
				,[IssueForId] = @pIssueForId  
				,[AccountNumber] = @pAccountNumber  
				,[CreditCardType] = @pCreditCardType  
				,[AvailableModules] = @pAvailableModules  
				,[ModifyDate] = GETUTCDATE()  
				,[ModifyUserId] = @pLoggedUserId 
				,[TransactionsOffline] = @pTransactionsOffline  
				,[IsDemo] = @pisDemo  
				,[CustomerManagerId] = @pCustomerManagerId
				,[SMSAlarms] = @pSMSAlarms
				,[CostCenterFuelDistribution] = @pCostCenterFuelDistribution
				,[SapCode] = @pSapCode
				,[FuelPermit] = @pFuelPermit
				,[HasFleetio] = @pHasFleetio
			WHERE [CustomerId] = @pCustomerId --AND [RowVersion] = @pRowVersion

			DECLARE @LastTransactionId INT = (SELECT MAX(t.[TransactionId]) FROM Control.Transactions t
			INNER JOIN General.Vehicles v ON t.VehicleId = v.vehicleId
			WHERE v.CustomerId = @pCustomerId)
			
			IF NOT EXISTS(SELECT * FROM [General].[ParametersByCustomer] 
					  WHERE [CustomerId] = @pCustomerId
							AND [Name] = 'Fleetio_LastTransactionId')
			BEGIN
				INSERT INTO [General].[ParametersByCustomer]
				([CustomerId],[Name],[Value],[ResuourceKey],[InsertDate])
				VALUES(
					@pCustomerId,
					'Fleetio_LastTransactionId',
					@LastTransactionId,
					'Fleetio_LastTransactionId',
					GETDATE()
				)
			END

			--Recalculamos disponible Tarjeta y Disponible Combustibles
			   IF @pFuelPermit = 1
				BEGIN
					INSERT INTO #Available
					SELECT cc.[CreditCardId]
					      ,vc.[FuelId]
					      ,(SUM([Amount]) + SUM([AdditionalAmount])) - [Control].[Fn_FuelDistribution_TransactionsAmount](cc.[CreditCardId], @lTimeZoneParameter, @lClosingDate, NULL, vc.[FuelId]) [AmountAvailable]
						  ,(SUM([Liters]) + SUM([AdditionalLiters])) - [Control].[Fn_FuelDistribution_TransactionsLiters](cc.[CreditCardId], @lTimeZoneParameter, @lClosingDate, NULL, 1, vc.[FuelId]) [LitersAvailable]
					FROM [Control].[CreditCard] cc
					INNER JOIN [Control].[CreditCardByVehicle] ccv
						ON ccv.[CreditCardId] = cc.[CreditCardId]
					INNER JOIN [General].[Vehicles] v
						ON v.[VehicleId] = ccv.[VehicleId]
					INNER JOIN (SELECT vc.[DefaultFuelId] [FuelId]
										      ,v.[VehicleId]
											  ,vc.[VehicleModel]
											  ,vc.[VehicleCategoryId]
										FROM [General].[Vehicles] v	
										INNER JOIN [General].[VehicleCategories] vc 
											ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
										INNER JOIN [Control].[Fuels] f	
											ON f.[FuelId] = vc.[DefaultFuelId]
										WHERE v.[CustomerId] = @pCustomerId
										UNION
										SELECT fvc.[FuelId] 
										      ,v.[VehicleId]
											  ,vc.[VehicleModel]
											  ,fvc.[VehicleCategoryId]
										FROM [General].[Vehicles] v
										INNER JOIN [General].[VehicleCategories] vc
											ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
										INNER JOIN [General].[FuelsByVehicleCategory] fvc
											ON fvc.[VehicleCategoryId] = vc.[VehicleCategoryId]
										INNER JOIN [Control].[Fuels] f	
											ON f.[FuelId] = fvc.[FuelId]
										WHERE v.[CustomerId] = @pCustomerId) vc
						ON vc.[VehicleId] = v.[VehicleId]
					LEFT JOIN [Control].[FuelDistribution] fd
						ON fd.[VehicleId] = v.[VehicleId]
						AND fd.[FuelId] = vc.[FuelId]
						AND fd.[InsertDate] >= @lClosingDate 
					WHERE cc.[StatusId] = 7
					AND v.[Active] = 1
					AND (v.[IsDeleted] IS NULL
						 OR v.[IsDeleted] = 0)
					GROUP BY cc.[CreditCardId], vc.[FuelId]

					INSERT INTO #Fuels
					SELECT vc.[FuelId], ISNULL((SUM([Amount]) + SUM([AdditionalAmount])), 0) [FuelDistribution]
					FROM [Control].[CreditCard] cc
					INNER JOIN [Control].[CreditCardByVehicle] ccv
						ON ccv.[CreditCardId] = cc.[CreditCardId]
					INNER JOIN [General].[Vehicles] v
						ON v.[VehicleId] = ccv.[VehicleId]
					INNER JOIN (SELECT vc.[DefaultFuelId] [FuelId]
										      ,v.[VehicleId]
											  ,vc.[VehicleModel]
											  ,vc.[VehicleCategoryId]
										FROM [General].[Vehicles] v	
										INNER JOIN [General].[VehicleCategories] vc 
											ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
										INNER JOIN [Control].[Fuels] f	
											ON f.[FuelId] = vc.[DefaultFuelId]
										WHERE v.[CustomerId] = @pCustomerId
										UNION
										SELECT fvc.[FuelId] 
										      ,v.[VehicleId]
											  ,vc.[VehicleModel]
											  ,fvc.[VehicleCategoryId]
										FROM [General].[Vehicles] v
										INNER JOIN [General].[VehicleCategories] vc
											ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
										INNER JOIN [General].[FuelsByVehicleCategory] fvc
											ON fvc.[VehicleCategoryId] = vc.[VehicleCategoryId]
										INNER JOIN [Control].[Fuels] f	
											ON f.[FuelId] = fvc.[FuelId]
										WHERE v.[CustomerId] = @pCustomerId) vc
						ON vc.[VehicleId] = v.[VehicleId]
					LEFT JOIN [Control].[FuelDistribution] fd
						ON fd.[VehicleId] = v.[VehicleId]
						AND fd.[FuelId] = vc.[FuelId]
						AND fd.[InsertDate] >= @lClosingDate 
					WHERE cc.[StatusId] = 7
					AND cc.[CustomerId] = @pCustomerId
					GROUP BY vc.[FuelId]
			    END
				ELSE
				BEGIN
					INSERT INTO #Available
					SELECT cc.[CreditCardId]
					      ,vc.[FuelId]
					      ,(SUM([Amount]) + SUM([AdditionalAmount])) - [Control].[Fn_FuelDistribution_TransactionsAmount](cc.[CreditCardId], @lTimeZoneParameter, @lClosingDate, NULL, vc.[FuelId]) [AmountAvailable]
						  ,(SUM([Liters]) + SUM([AdditionalLiters])) - [Control].[Fn_FuelDistribution_TransactionsLiters](cc.[CreditCardId], @lTimeZoneParameter, @lClosingDate, NULL, 1, vc.[FuelId]) [LitersAvailable]
					FROM [Control].[CreditCard] cc
					INNER JOIN [Control].[CreditCardByVehicle] ccv
						ON ccv.[CreditCardId] = cc.[CreditCardId]
					INNER JOIN [General].[Vehicles] v
						ON v.[VehicleId] = ccv.[VehicleId]
					INNER JOIN (SELECT vc.[DefaultFuelId] [FuelId]
										      ,v.[VehicleId]
											  ,vc.[VehicleModel]
											  ,vc.[VehicleCategoryId]
										FROM [General].[Vehicles] v	
										INNER JOIN [General].[VehicleCategories] vc 
											ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
										INNER JOIN [Control].[Fuels] f	
											ON f.[FuelId] = vc.[DefaultFuelId]
										WHERE v.[CustomerId] = @pCustomerId
								) vc
						ON vc.[VehicleId] = v.[VehicleId]
					LEFT JOIN [Control].[FuelDistribution] fd
						ON fd.[VehicleId] = v.[VehicleId]
						AND fd.[FuelId] = vc.[FuelId]
						AND fd.[InsertDate] >= @lClosingDate 
					WHERE cc.[StatusId] = 7
					AND v.[Active] = 1
					AND (v.[IsDeleted] IS NULL
						 OR v.[IsDeleted] = 0)
					GROUP BY cc.[CreditCardId], vc.[FuelId]

					INSERT INTO #Fuels
					SELECT vc.[FuelId], ISNULL((SUM([Amount]) + SUM([AdditionalAmount])), 0) [FuelDistribution]
					FROM [Control].[CreditCard] cc
					INNER JOIN [Control].[CreditCardByVehicle] ccv
						ON ccv.[CreditCardId] = cc.[CreditCardId]
					INNER JOIN [General].[Vehicles] v
						ON v.[VehicleId] = ccv.[VehicleId]
					INNER JOIN (SELECT vc.[DefaultFuelId] [FuelId]
										      ,v.[VehicleId]
											  ,vc.[VehicleModel]
											  ,vc.[VehicleCategoryId]
										FROM [General].[Vehicles] v	
										INNER JOIN [General].[VehicleCategories] vc 
											ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
										INNER JOIN [Control].[Fuels] f	
											ON f.[FuelId] = vc.[DefaultFuelId]
										WHERE v.[CustomerId] = @pCustomerId
					            ) vc
						ON vc.[VehicleId] = v.[VehicleId]
					LEFT JOIN [Control].[FuelDistribution] fd
						ON fd.[VehicleId] = v.[VehicleId]
						AND fd.[FuelId] = vc.[FuelId]
						AND fd.[InsertDate] >= @lClosingDate 
					WHERE cc.[StatusId] = 7
					AND cc.[CustomerId] = @pCustomerId
					GROUP BY vc.[FuelId]
				END

				UPDATE ccc
				SET [CreditAvailable] = ISNULL([AmountAvailable], 0)
				   ,[AvailableLiters] = ISNULL([LitersAvailable], 0)
				FROM [Control].[CreditCard] ccc
				INNER JOIN (SELECT SUM([AmountAvailable]) [AmountAvailable]
								  ,SUM([LitersAvailable]) [LitersAvailable]
								  ,a.[CreditCardId]
							FROM [Control].[CreditCard] cc
							INNER JOIN #Available a
								ON cc.[CreditCardId] = a.[CreditCardId]
							GROUP BY a.[CreditCardId]) b
					ON ccc.[CreditCardId] = b.[CreditCardId]			

				UPDATE fc
				SET [Assigned] = [FuelDistribution]
				FROM [Control].[FuelsByCredit] fc
				INNER JOIN #Fuels f
					ON f.[FuelId] = fc.[FuelId]
				WHERE fc.[CustomerId] = @pCustomerId
				      AND fc.[InsertDate] >= @lClosingDate
        END    
  
		IF @pFuelPermit	 = 1
		BEGIN

			UPDATE [Control].[TransactionsRulesByCustomer]
			SET [RuleActive] = 1
			WHERE [CustomerId] = @pCustomerId	
				  AND [RuleId] = 9

			UPDATE trv
			SET trv.[RuleActive] = 1
			FROM [Control].[TransactionsRulesByVehicle] trv
			INNER JOIN [General].[Vehicles] v
				ON v.[VehicleId] = trv.[VehicleId]
			WHERE [CustomerId] = @pCustomerId	
				  AND [RuleId] = 9

			UPDATE trc
			SET trc.[RuleActive] = 1
			FROM [Control].[TransactionsRulesByCostCenter] trc
			INNER JOIN [General].[VehicleCostCenters] vcc
				ON trc.[CostCenterId] = vcc.[CostCenterId]
			INNER JOIN [General].[VehicleUnits] vu	
				ON vu.UnitId = vcc.UnitId
			WHERE
			    vu.[CustomerId]	= @pCustomerId	
			    AND trc.[RuleId] = 9
				
			IF NOT EXISTS(SELECT * 
						  FROM [General].[ParametersbyCustomer]
						  WHERE [CustomerId] = @pCustomerId
								AND [Name] = 'FuelsMargin')
			BEGIN
				INSERT INTO [General].[ParametersbyCustomer]
				(
					[CustomerId]
				   ,[Name]
				   ,[Value]
				   ,[ResuourceKey]
				   ,[InsertUserId]
				   ,[InsertDate]
				)
				VALUES
				(
					@pCustomerId
				   ,'FuelsMargin'
				   ,@pFuelMargin
				   ,'TR_PARAMETERS'
				   ,@pLoggedUserId
				   ,GETDATE()
				)				
			END	
			ELSE
			BEGIN
				UPDATE [General].[ParametersbyCustomer]
				SET [Value] = @pFuelMargin
				WHERE [CustomerId] = @pCustomerId
				      AND [Name] = 'FuelsMargin'
			END			
		END		

		--UPDATE IS DEMO ALARM FOR THE CUSTOMER 
		EXEC [General].[Sp_CustomerDemoAlert_AddOrEdit]  @pCustomerId, @pIsDemo, @pIsDemoAlert, @pLoggedUserId 
		
		--UPDATE MENU OPTIONS
		EXEC [General].[Sp_FuelDistributionCostCenterMenuOption_AddOrEdit]  @pCustomerId, @pCostCenterFuelDistribution 

		-- ADD NEW HISTORIC ROW  
		INSERT INTO [General].[CustomersHx]  
		(  
			 [Name]  
			,[CurrencyId]  
			,[CountryId]  
			,[AccountNumber]  
			,[Logo]  
			,[IsActive]  
			,[UnitOfCapacityId]  
			,[CreditCardType]  
			,[IssueForId]  
			,[DistributionTypeId]  
			,[IsDeleted]  
			,[InsertDate]  
			,[InsertUserId]  
			,[ModifyDate]  
			,[ModifyUserId]  
			,[AvailableModules]  
			,[CostCardAdmin]  
			,[TransactionsOffline]  
			,[SendSMSAlarm]
			,[CostRentedBAC]
			,[isDemo]
			,[CustomerManagerId]
			,[SMSAlarms]
			,SapCode
		)  
		SELECT [Name]  
			  ,[CurrencyId]  
			  ,[CountryId]  
			  ,[AccountNumber]  
			  ,[Logo]  
			  ,[IsActive]  
			  ,[UnitOfCapacityId]  
			  ,[CreditCardType]  
			  ,[IssueForId]  
			  ,[DistributionTypeId]  
			  ,[IsDeleted]  
			  ,[InsertDate]  
			  ,[InsertUserId]  
			  ,[ModifyDate]  
			  ,[ModifyUserId]  
			  ,[AvailableModules]  
			  ,[CostCardAdmin]  
			  ,[TransactionsOffline]  
			  ,[SendSMSAlarm]
			  ,[CostRentedBAC]
			  ,[isDemo]
			  ,[CustomerManagerId]
			  ,[SMSAlarms]
			  ,[SapCode]
		FROM [General].[Customers]  
		WHERE [CustomerId] = @pCustomerId 		

		--Replicate Information in Intrack
		EXEC [dbo].[IntrackService_AddOrEditClientToIntrack]
			 @pCustomerId = @pCustomerId,
			 @pClientName = @pDecryptedName,
			 @pID = NULL
						            
      	COMMIT TRANSACTION  		
    END TRY  
    BEGIN CATCH
		ROLLBACK TRANSACTION  
    
		SELECT	@lErrorMessage = ERROR_MESSAGE(), 
				@lErrorSeverity = ERROR_SEVERITY(), 
				@lErrorState = ERROR_STATE()     
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)      
	END CATCH  
   
	SELECT @pCustomerId  
   
	SET NOCOUNT OFF  
    SET XACT_ABORT OFF  
END  