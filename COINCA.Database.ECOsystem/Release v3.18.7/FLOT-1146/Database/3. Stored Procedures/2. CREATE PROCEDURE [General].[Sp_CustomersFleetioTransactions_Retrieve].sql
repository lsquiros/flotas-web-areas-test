USE [ECOSystemDEV]
GO
IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_CustomersFleetioTransactions_Retrieve' AND TYPE = 'P')
	DROP PROC [General].[Sp_CustomersFleetioTransactions_Retrieve]
GO
-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Retrieve transactions for send to Fleetio
-- ================================================================================================
 CREATE PROCEDURE [General].[Sp_CustomersFleetioTransactions_Retrieve]
 (
	@pCustomerId INT
   ,@pTransactionId INT
 )
 AS
 BEGIN	
	SET NOCOUNT ON	

		DECLARE @lUSGallons DECIMAL(16, 5) = 3.78541
               ,@lUKGallons DECIMAL(16, 5) = 4.54609
			   ,@lUSGallonsToUkGallons DECIMAL(16, 6) = 0.832674

		SELECT DISTINCT t.[TransactionId]
					   ,CONVERT(INT, v.[FleetioId]) [Vehicle_id]
					   ,v.[VehicleId] [VehicleId]
					   ,v.[PlateId]
					   ,[Date]
					   ,ffd.[FleetioId] [Fuel_type_id]
					   ,CONVERT(FLOAT, CASE WHEN p.[CapacityUnitId] = 1
					    THEN ([Liters] / @lUSGallons)
					    ELSE [Liters]
					    END) [Liters]
					   ,[BacId] [External_id]
					   ,CONVERT(FLOAT, CASE WHEN [Liters] = 0
					    THEN 0
						ELSE ([FuelAmount] / [Liters])
						END) [Price_per_volume_unit]
					   ,CONVERT(FLOAT, CASE WHEN p.[CapacityUnitId] = 1
					    THEN ([Liters] * @lUSGallonsToUkGallons)
					    ELSE ([Liters] * @lUKGallons)
					    END) [Uk_gallons]
					   ,CONVERT(FLOAT, CASE WHEN p.[CapacityUnitId] = 1
					    THEN [Liters]
					    ELSE ([Liters] * @lUSGallons)
					    END) [Us_gallons]
					   ,ss.[FleetioId] [Vendor_id]
					   ,ss.[ServiceStationId]
					   ,ss.[Name] [ServiceStationName]
					   ,CONVERT(FLOAT, [FuelAmount])
					   ,t.[Odometer]
					   ,s.[Name] [City]
					   ,coun.[Name] [Country]
					   ,ss.[Address]
		FROM [Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v
			ON v.[VehicleId] = t.[VehicleId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = v.[CustomerId]
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = cp.[PartnerId]
		INNER JOIN [Control].[Fuels] f
			ON f.[FuelId] = t.[FuelId]
		LEFT JOIN [Control].[FuelFleetioDetailByCustomer] ffd
			ON ffd.[FuelId] = f.[FuelId]
			   AND c.[CustomerId] = ffd.[CustomerId] 
		LEFT JOIN [General].[TerminalsByServiceStations] ts
			ON ts.[TerminalId] = t.[ProcessorId]
		LEFT JOIN [General].[ServiceStations] ss
			ON ss.[ServiceStationId] = ts.[ServiceStationId]
		LEFT JOIN [General].[States] s
			ON s.[StateId] = ss.[ProvinceId]
		LEFT JOIN [General].[Countries]coun
			ON coun.[CountryId] = ss.[CountryId]
		LEFT JOIN [General].[FleetioErrorLog] fl
			ON t.[TransactionId] = fl.[TransactionId]
			AND [RegisterDate] > DATEADD(DAY, -7, GETDATE())
		WHERE t.[TransactionId] > @pTransactionId
		AND c.[CustomerId] = @pCustomerId
    SET NOCOUNT OFF
 END
GO