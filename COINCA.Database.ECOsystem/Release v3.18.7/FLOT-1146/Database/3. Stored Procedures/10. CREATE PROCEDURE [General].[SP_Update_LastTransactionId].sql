USE [ECOSystem]
GO

IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'SP_Update_LastTransactionId' AND TYPE = 'P')
	DROP PROC [General].[SP_Update_LastTransactionId]
GO


-- =============================================
-- Author:      Jason Bonilla
-- Create Date: 03/04/2020
-- Description: Update Last Transaction ID proccesed by Fleetio
-- =============================================
CREATE PROCEDURE [General].[SP_Update_LastTransactionId]
(
    @pTransactionId INT,
	@pCustomerId INT
)
AS
BEGIN
    SET NOCOUNT ON

	UPDATE [General].[ParametersByCustomer]
	SET [Value] = @pTransactionId
	WHERE [CustomerId] = @pCustomerId
	AND [Name] = 'Fleetio_LastTransactionId'
END
GO
