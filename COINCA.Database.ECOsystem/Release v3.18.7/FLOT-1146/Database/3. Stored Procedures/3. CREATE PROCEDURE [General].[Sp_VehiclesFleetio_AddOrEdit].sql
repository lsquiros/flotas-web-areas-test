USE [ECOSystem]
GO
IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_VehiclesFleetio_AddOrEdit' AND TYPE = 'P')
	DROP PROC [General].[Sp_VehiclesFleetio_AddOrEdit]
GO
-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Update Vehicles set FleetioId
-- ================================================================================================
 CREATE PROCEDURE [General].[Sp_VehiclesFleetio_AddOrEdit]
 (
	@pVehicleId INT
   ,@pVehicleFleetioId BIGINT
 )
 AS
 BEGIN	
	SET NOCOUNT ON	

	UPDATE [General].[Vehicles]
	SET [FleetioId] = @pVehicleFleetioId
	WHERE [VehicleId] = @pVehicleId

    SET NOCOUNT OFF
 END
GO