USE [ECOSystem]
GO

IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'SP_Save_Fleetio_Error' AND TYPE = 'P')
	DROP PROC [General].[SP_Save_Fleetio_Error]
GO
-- =============================================
-- Author:      Jason Bonilla
-- Create Date: 27/03/2020
-- Description: Save Fleetio Errors
-- =============================================
CREATE PROCEDURE [General].[SP_Save_Fleetio_Error]
(
    @pTransactionId INT,
	@pServiceType INT = NULL,
	@pMethod VARCHAR(80) = NULL,
	@pErrorMessage VARCHAR(5000) = NULL,
	@pResponseMessage VARCHAR(5000) = NULL,
	@pSendedData VARCHAR(5000) = NULL
)
AS
BEGIN
    INSERT INTO [General].[FleetioErrorLog]
	VALUES(
		@pTransactionId,
		@pServiceType,
		@pMethod,
		@pErrorMessage,
		@pResponseMessage,
		@pSendedData,
		GETDATE()
	)
END
GO
