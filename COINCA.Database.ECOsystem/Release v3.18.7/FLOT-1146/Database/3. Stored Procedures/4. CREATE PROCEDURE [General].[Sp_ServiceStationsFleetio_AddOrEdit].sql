USE [ECOSystem]
GO
IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_ServiceStationsFleetio_AddOrEdit' AND TYPE = 'P')
	DROP PROC [General].[Sp_ServiceStationsFleetio_AddOrEdit]
GO
-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Update Vehicles set FleetioId
-- ================================================================================================
 CREATE PROCEDURE [General].[Sp_ServiceStationsFleetio_AddOrEdit]
 (
	@pServiceStationId INT
   ,@pServiceStationFleetioId BIGINT
 )
 AS
 BEGIN	
	SET NOCOUNT ON	

	UPDATE [General].[ServiceStations]
	SET [FleetioId] = @pServiceStationFleetioId
	WHERE [ServiceStationId] = @pServiceStationId

    SET NOCOUNT OFF
 END
GO