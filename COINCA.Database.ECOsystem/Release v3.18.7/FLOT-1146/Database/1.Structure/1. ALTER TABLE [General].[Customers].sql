USE [ECOSystem]
GO

-- ======================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Add Fleetio Field
-- ======================================

ALTER TABLE [General].[Customers]
ADD [HasFleetio] BIT

ALTER TABLE [General].[Customers]
ADD [FleetioId] INT

