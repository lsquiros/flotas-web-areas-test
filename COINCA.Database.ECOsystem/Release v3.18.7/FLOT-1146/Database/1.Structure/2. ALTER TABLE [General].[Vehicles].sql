USE [ECOSystem]
GO

-- ======================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Add Fleetio Field
-- ======================================

ALTER TABLE [General].[Vehicles]
ADD [FleetioId] BIGINT