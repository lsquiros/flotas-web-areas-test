/****** Object:  Table [General].[FleetioDetail]    Script Date: 26/03/2020 1:56:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Control].[FuelFleetioDetailByCustomer](
	[FleetioDetailId] [int] IDENTITY(1,1) NOT NULL,
	[FleetioId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[FuelId] [int] NOT NULL,
 CONSTRAINT [PK_FleetioDetail] PRIMARY KEY CLUSTERED 
(
	[FleetioDetailId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Control].[FuelFleetioDetailByCustomer]  WITH CHECK ADD  CONSTRAINT [FK_FleetioDetailCustomer] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])
GO

ALTER TABLE [Control].[FuelFleetioDetailByCustomer] CHECK CONSTRAINT [FK_FleetioDetailCustomer]
GO


