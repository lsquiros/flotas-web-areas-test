/****** Object:  Table [General].[FleetioErrorLog]    Script Date: 27/03/2020 12:24:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [General].[FleetioErrorLog](
	[FleetioErrorId] [int] IDENTITY(1,1) NOT NULL,
	[TransactionId] [int] NULL,
	[ServiceType] [int] NULL,
	[Method] [varchar](80) NULL,
	[ErrorMessage] [varchar](5000) NULL,
	[ResponseMessage] [varchar](5000) NULL,
	[SendedData] [varchar](5000) NULL,
	[RegisterDate] [datetime] NULL,
 CONSTRAINT [PK_FleetioErrorLog] PRIMARY KEY CLUSTERED 
(
	[FleetioErrorId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


