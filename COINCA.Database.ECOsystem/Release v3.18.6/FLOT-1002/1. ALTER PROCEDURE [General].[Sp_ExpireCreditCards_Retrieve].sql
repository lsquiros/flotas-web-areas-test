/****** Object:  StoredProcedure [General].[Sp_ExpireCreditCards_Retrieve]    Script Date: 3/4/2020 1:49:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Stefano Quirós
-- Create date: 02/12/2019
-- Description:	Retrieve the CreditCard that are soon to Expire (Master and Childrens) of one Partner
-- ================================================================================================

ALTER PROCEDURE [General].[Sp_ExpireCreditCards_Retrieve] --1023
(	
	 @pPartnerId INT = NULL
	,@pCustomerId INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
	,@pCostCenterId INT = NULL
	,@pVehicleGroupId INT = NULL
	,@pUserId INT = NULL
)
AS
BEGIN	
	SET NOCOUNT ON	

		DECLARE @lTimeZone INT = (SELECT [TimeZone]
								  FROM [General].[Partners] p 
								  INNER JOIN [General].[Countries] c
									ON c.[CountryId] = p.[CountryId]
								  WHERE [PartnerId] = @pPartnerId)

		DECLARE @lDate DATE = DATEADD(HOUR, @lTimeZone, GETDATE())

		CREATE TABLE #Result
		(
			[EncryptCustomerName] VARCHAR(250)
		   ,[CreditCardNumber] VARCHAR(520)
		   ,[ExpirationMonth] INT
		   ,[ExpirationYear] INT
		   ,[CreditCardType] VARCHAR(50)
		   ,[ExpirationDays] INT
		   ,[ExpireCreditCardBefore] INT
		   ,[PartnerName] VARCHAR(250)
		)
		
		INSERT INTO #Result
		SELECT c.[Name]
			  ,[CreditCardNumber]
			  ,[ExpirationMonth]
			  ,[ExpirationYear]
			  ,'Tarjeta Hija'
			  ,DATEDIFF(DAY, @lDate, EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1)))
			  ,gp.[ExpireCreditCardBefore]
			  ,p.[Name]
			  ,s.[Name] [State]
		FROM [Control].[CreditCard] cc
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = cc.[CustomerId]
		INNER JOIN [General].[GeneralParametersByPartner] gp
			ON gp.[PartnerId] = cp.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = cp.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = gp.[PartnerId]
		INNER JOIN [General].[Status] s
			ON s.[StatusId] = cc.[StatusId]
		WHERE cp.[PartnerId] =  @pPartnerId 
		AND [ExpireCreditCardActive] = 1
		AND cc.[StatusId] IN (7, 8)
	    AND DATEDIFF(DAY, @lDate, EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1))) = gp.[ExpireCreditCardBefore]
		OR (
		    EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1)) > CONVERT(DATE, GETDATE()) 
			AND DATEDIFF(DAY, @lDate, EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1))) < gp.[ExpireCreditCardBefore]
		    AND [ExpireCreditCardPeriodicity] = 7 
		    AND [ExpireCreditCardNameDay] = DATEPART(dw,@lDate) 
		    AND ([LastExpireNotificationSent] IS NULL OR DATEDIFF(WEEK, [LastExpireNotificationSent], @lDate) >= [ExpireCreditCardRepeat])
		   )
		OR (
		    EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1)) > CONVERT(DATE, GETDATE()) 
		    AND DATEDIFF(DAY, @lDate, EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1))) < gp.[ExpireCreditCardBefore]
		    AND [ExpireCreditCardPeriodicity] = 30 
		    AND [ExpireCreditCardDay] = Day(@lDate)
		    AND ([LastExpireNotificationSent] IS NULL OR DATEDIFF(MONTH, [LastExpireNotificationSent], @lDate) >= [ExpireCreditCardRepeat])
		   )
		   
		UNION

		SELECT c.[Name]
			  ,[CreditCardNumber]
			  ,[ExpirationMonth]
			  ,[ExpirationYear]
			  ,'Tarjeta Madre' 
			  ,DATEDIFF(DAY, @lDate, EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1)))
			  ,gp.[ExpireCreditCardBefore]
			  ,p.[Name]
			  ,s.[Name] [State]
		FROM [General].[CustomerCreditCards] cc
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = cc.[CustomerId]
		INNER JOIN [General].[GeneralParametersByPartner] gp
			ON gp.[PartnerId] = cp.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = cp.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = gp.[PartnerId]
		INNER JOIN [General].[Status] s
			ON s.[StatusId] = cc.[StatusId]
		WHERE cp.[PartnerId] =  @pPartnerId 
		AND [ExpireCreditCardActive] = 1
		AND cc.[StatusId] = 100
		AND DATEDIFF(DAY, @lDate, EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1))) = gp.[ExpireCreditCardBefore]
		OR (
			EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1)) > CONVERT(DATE, GETDATE()) 
			AND DATEDIFF(DAY, @lDate, EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1))) < gp.[ExpireCreditCardBefore]
		    AND [ExpireCreditCardPeriodicity] = 7 
		    AND [ExpireCreditCardNameDay] = DATEPART(dw,@lDate) 
		    AND ([LastExpireNotificationSent] IS NULL OR DATEDIFF(WEEK, [LastExpireNotificationSent], @lDate) >= [ExpireCreditCardRepeat])
		      )
		OR (
		    EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1)) > CONVERT(DATE, GETDATE()) 
		    AND DATEDIFF(DAY, @lDate, EOMONTH(DATEFROMPARTS([ExpirationYear], [ExpirationMonth], 1))) < gp.[ExpireCreditCardBefore]
		    AND [ExpireCreditCardPeriodicity] = 30 
		    AND [ExpireCreditCardDay] = Day(@lDate)
		    AND ([LastExpireNotificationSent] IS NULL OR DATEDIFF(MONTH, [LastExpireNotificationSent], @lDate) >= [ExpireCreditCardRepeat])
		   ) 

		DECLARE @lCount INT = (SELECT COUNT(*) 
						   FROM #Result)
		   
		IF @lCount > 0
		BEGIN
			DECLARE @lCount2 INT = (SELECT COUNT(*) 
								FROM #Result 
								WHERE [ExpirationDays] = [ExpireCreditCardBefore])
			IF @lCount = @lCount2
			BEGIN
				UPDATE [General].[GeneralParametersByPartner]
				SET [LastExpireNotificationSent] = GETDATE()
				WHERE [PartnerId] = @pPartnerId
			END
		
		END 
	
		SELECT * FROM #Result 

    SET NOCOUNT OFF
END