USE [ECOsystemDev]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleLineVita_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleLineVita_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_VehicleLineVita_Retrieve]    Script Date: 2/16/2016 4:37:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/02/2016
-- Description:	Retrieve VehicleList LineVita ECO
-- Modified by: Kevin Pe�a
-- Description:	Change Inner JOIN table
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleLineVita_Retrieve]
(
	@pUserId INT	
)
AS
BEGIN	
	
	--GET THE DATE ONE DAY BEFORE THE PARAMETER DATE
	SELECT v.[VehicleId]
		  ,v.[PlateId]
		  ,v.[Name]
	FROM [General].[Vehicles] v
	INNER JOIN [General].[VehiclesDrivers] vu
		ON v.[VehicleId] = vu.[VehicleId]
	WHERE vu.[UserId] = @pUserId

END

GO


