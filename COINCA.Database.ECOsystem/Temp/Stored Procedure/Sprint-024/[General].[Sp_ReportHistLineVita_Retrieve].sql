USE [ECOsystemDev]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ReportHistLineVita_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ReportHistLineVita_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/02/2016
-- Description:	Retrieve Report Hist LineVita ECO
-- Modified by: Kevin Pe�a   
-- Description:	Separate device retrieve data from the main query
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ReportHistLineVita_Retrieve]
(
	@pPlateId VARCHAR(MAX),
	@pDate DATETIME
)
AS
BEGIN	
	
	--GET THE DATE ONE DAY BEFORE THE PARAMETER DATE
	DECLARE @pFinishDate DATETIME	
	Declare @Range INT
	DECLARE @Device INT

	SET @Range = (SELECT [NumericValue] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'TimeRange' AND [Description] = 'LineVita Report Hist')
	SET @pFinishDate = DATEADD(HOUR, @Range, @pDate)

	--GET DEVICE - Kevin Pe�a
	SET @Device = (SELECT TOP(1) d.Device 
	               FROM [dbo].[DispositivosAVL] av
	                    INNER JOIN [dbo].[Composiciones] comp 
	                          ON av.dispositivo = comp.dispositivo
	                    INNER JOIN [dbo].[Devices] d 
	                          ON CAST(d.UnitID as varchar) = av.terminalID
	                    INNER JOIN [dbo].[Reports] r 
	                          ON r.Device = d.Device
	                    INNER JOIN [dbo].[Vehiculos] vi 
	                          ON vi.vehiculo = comp.vehiculo
	               WHERE vi.placa = @pPlateId
						 AND d.Activo = 1)
	--GET HIST	
	SELECT r.[GPSDateTime]
		   ,r.[Longitude]
		   ,r.[Latitude]
		   ,r.[Odometer]
		   ,r.[InputStatus]
		   ,r.[OutputStatus]
	       ,r.[VSSSpeed]
		   ,r.[MainVolt]
		   ,r.[BatVolt]
	FROM [dbo].[Reports] r
	WHERE r.GPSDateTime BETWEEN @pDate AND @pFinishDate 
	      AND r.Device = @Device
	ORDER BY  r.GPSDateTime ASC

END

GO


