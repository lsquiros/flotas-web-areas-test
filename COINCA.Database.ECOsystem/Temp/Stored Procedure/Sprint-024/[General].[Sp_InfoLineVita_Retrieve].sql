USE [ECOsystemDev]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_InfoLineVita_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_InfoLineVita_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_InfoLineVita_Retrieve]    Script Date: 2/17/2016 10:58:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Kevin Pe�a   
-- Create date: 12/02/2016
-- Description:	Retrieve Info Params LineVita ECO 
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_InfoLineVita_Retrieve]
AS
BEGIN	
	
	--GET THE DATE ONE DAY BEFORE THE PARAMETER DATE

	Declare @LVE_Servicios varchar(MAX)
	Declare @LVE_Info varchar(MAX)
	Declare @LVE_Robo varchar(MAX)
	Declare @LVE_Numbers varchar(MAX)

	SET @LVE_Servicios = (SELECT [Description] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'LineVita_ServiciosAnuales')
	
	SET @LVE_Info = (SELECT [Description] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'LineVita_Info')
	
	SET @LVE_Robo = (SELECT [Description] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'LineVita_CasoRobo')
	
	SET @LVE_Numbers = (SELECT [Description] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'LineVita_Numbers')

	SELECT @LVE_Servicios as [LVE_Servicios], @LVE_Info as [LVE_Info], @LVE_Robo as [LVE_Robo], @LVE_Numbers as [LVE_Numbers]

END

GO


