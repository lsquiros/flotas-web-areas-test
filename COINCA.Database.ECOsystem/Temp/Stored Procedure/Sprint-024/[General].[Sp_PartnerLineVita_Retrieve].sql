USE [ECOsystemDev]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerLineVita_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PartnerLineVita_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 08/02/2016
-- Description:	Retrieve Current Partner LineVita ID
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PartnerLineVita_Retrieve]
AS
BEGIN
	SELECT CAST([NumericValue] AS INT) AS [NumericValue] 
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'LineVitaPartner' AND [Property] = 'CRC'
END
GO


