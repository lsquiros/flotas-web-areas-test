USE [ECOSystem]

--Modify setting for the user mbonilla (Customer - to Partners)
UPDATE [dbo].[AspNetUserRoles] SET [RoleId] = 'A0465644-860E-4B8E-8779-7719B65D5E6E' 
WHERE [UserId] = '8d9e4870-bb17-4f5e-a4cf-eaaddda1c2f2' 
	  AND [RoleId] = 'F024C5BD-C5F4-408C-8A86-B5DBA55435E0' 
GO

DELETE FROM [General].[CustomerUsers] 
WHERE [CustomerUserId] = 49
      AND [UserId] = 1155 
GO
	  
INSERT INTO [General].[PartnerUsers]
([UserId], [PartnerId], [InsertDate], [InsertUserId])
VALUES (1155, 1023, GETDATE(), 1183)
GO

INSERT INTO [General].[PartnerUsersByCountry]
([PartnerUserId], [CountryId], [InsertDate], [InsertUserId])
VALUES ((SELECT [PartnerUserId] FROM [General].[PartnerUsers] WHERE [UserId] = 1155), 3, GETDATE(), 1183)
GO