
INSERT [dbo].[GeneralParameters] ([ParameterID], [Property], [Description], [Value], [NumericValue]) VALUES (N'LineVita_CasoRobo', N'NA', N'Repórtelo inmediatamente a nuestro Centro de Soporte el cual trabajar las 24 horas del día, los 7 días de la semana, los 365 días del año:

(506) 2205-0000

Brinde al operador un número de teléfono donde pueda ser localizado rápidamente.
Haga la denuncia de manera inmediata a las autoridades correspondientes (911).

Envíe una copia de la denuncia al correo electrónico soporte@linevita.com.
Cuando vehículo sea localizado, se le comunicará a las autoridades correspondientes para que procedan con la recuperación.

Nuestro personal le informará que la unidad de policía tiene en custodia su vehículo para que usted se comunique y le informen el procedimiento para la devolución del mismo.', N' ', CAST(0.00 AS Numeric(18, 2)))
INSERT [dbo].[GeneralParameters] ([ParameterID], [Property], [Description], [Value], [NumericValue]) VALUES (N'LineVita_Info', N'NA', N'Plataforma que facilita la información del vehículo al conductor en tiempo real y de forma histórica.

Permite reconstrucción de velocidades, monitoreo y localización en tiempo real.

Posibilita el proceso de recuperación de vehículos en caso de robo.
Asistencia Vial las 24 horas del día,los 7 días de la semana, los 365 días del año.

Retroalimentación de la nota de conducción, la distancia recorrida, velocidades promedio en exceso y calificaciones históricas.', N' ', CAST(0.00 AS Numeric(18, 2)))
INSERT [dbo].[GeneralParameters] ([ParameterID], [Property], [Description], [Value], [NumericValue]) VALUES (N'LineVita_Numbers', N'NA', N'2222-2222*8888-8888*9999-9999', N' ', CAST(0.00 AS Numeric(18, 2)))
INSERT [dbo].[GeneralParameters] ([ParameterID], [Property], [Description], [Value], [NumericValue]) VALUES (N'LineVita_ServiciosAnuales', N'NA', N'Servicios de asistencia gratuita:

- Hasta 2 asistencias con grúa anuales.

- Hasta 2 auxilios viales para cambio de llanta o paso de corriente

La asistencia incluye un monto máximo de:
$150 por evento de grúas y $75 por cambio de llanta o paso de corriente.', N' ', CAST(0.00 AS Numeric(18, 2)))
INSERT [dbo].[GeneralParameters] ([ParameterID], [Property], [Description], [Value], [NumericValue]) VALUES (N'TimeRange', N'NA', N'LineVita Report Hist', N'', CAST(24.00 AS Numeric(18, 2)))