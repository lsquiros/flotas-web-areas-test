USE [ECOSystem]

--Modify setting for the user jherrera (Customer - to Partners)
UPDATE [dbo].[AspNetUserRoles] SET [RoleId] = 'A0465644-860E-4B8E-8779-7719B65D5E6E' 
WHERE [UserId] = '8ea866cc-a292-4590-abb2-cff59d22950f' 
	  AND [RoleId] = '97100F38-18F5-45BD-A62B-D1C27126157B' 
GO

DELETE FROM [General].DriversUsers 
WHERE [DriversUserId] = 56
      AND [UserId] = 1173 
GO
	  
INSERT INTO [General].[PartnerUsers]
([UserId], [PartnerId], [InsertDate], [InsertUserId])
VALUES (1173, 1023, GETDATE(), 1183)
GO

INSERT INTO [General].[PartnerUsersByCountry]
([PartnerUserId], [CountryId], [InsertDate], [InsertUserId])
VALUES ((SELECT [PartnerUserId] FROM [General].[PartnerUsers] WHERE [UserId] = 1173), 3, GETDATE(), 1183)
GO