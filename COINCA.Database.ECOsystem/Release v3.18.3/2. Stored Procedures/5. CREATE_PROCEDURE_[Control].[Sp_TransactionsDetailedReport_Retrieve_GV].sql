
IF EXISTS (Select * from sys.objects where name = 'Sp_TransactionsDetailedReport_Retrieve_GV' and type = 'P')
	DROP PROC [Control].[Sp_TransactionsDetailedReport_Retrieve_GV]
GO

-- ================================================================================================
-- Author:		Stefano Quiros
-- Create date: 22/2/2016
-- Description:	Retrieve TransactionsDetailedReport information
-- Modify 9/8/16 - Henry Retana 
-- Add the country name to the retrieve information
-- Update 19/08/2016 - Cindy Vargas - Add the transaction ExchangeValue to the retrieve information
-- Update 29/08/2016 - Cindy Vargas - Add the transaction Amount to the retrieve information
-- Modify 28/9/16 - Henry Retana - change the amount retrieve
-- Modify By: Stefano Quirós - Add the Time Zone Parameter - 12/06/2016
-- Modify by Henry Retana - 19/09/2017
-- Use function to get the driver id and driver name 
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Modify By: Stefano Quirós - 22/12/2017 - Add if the MasterCard is International to the retrieve
-- Modify by: Esteban Solis 16/05/2018 Add CustomerId to dbo.Sp_UserDynamicFilter_Retrive call
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- Juan Carlos Santamaria V. - 12/27/2019 - Add Column 'NOMBRE DE LA EMPRESA' Customer Branch - JSA-001
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_TransactionsDetailedReport_Retrieve_GV]
(
	@pCustomerId INT,					--@pCustomerId: CustomerId		
	@pYear INT = NULL,					--@pYear: Year
	@pMonth INT = NULL,					--@pMonth: Month
	@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
	@pEndDate DATETIME = NULL,			--@pEndDate: End Date	
	@pKey VARCHAR(800) = NULL,			--@pKey :Key
	@pUserId INT						--@pUserId: User Id
)
AS
BEGIN

	SET NOCOUNT ON	

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	DECLARE @lTimeZoneParameter INT 
	DECLARE @pIssueForId INT
	DECLARE @lConvertionValue DECIMAL(16, 5) = 3.78
	DECLARE @lPartnerUnit INT
	DECLARE @lIsCustomerBranch INT

	SELECT @lPartnerUnit = [CapacityUnitId] 
	FROM [General].[Partners] p
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[PartnerId] = p.[PartnerId]
	WHERE [CustomerId] = @pCustomerId
		
	SET @pIssueForId = (SELECT [IssueForId] FROM [General].[Customers] WHERE [CustomerId] = @pCustomerId)

	SELECT @lTimeZoneParameter = [TimeZone]	
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	SELECT *  /*JSA-001*/
	INTO #Customers
	FROM (
		SELECT   a.[CustomerId]
				,a.[Name]
				,a.[Name] AS CustomerBranchName
				,a.[CustomerId] AS CustomerBranch
				,a.[UnitOfCapacityId]
				,a.[CurrencyId]
				,a.[CountryId]
		FROM [General].[Customers] a
		WHERE a.[CustomerId] = @pCustomerId	
		------------------------------------
		UNION /*add CustomerBracnh */
		------------------------------------
		SELECT   cb.[CustomerId]
				,cb.[Name]
				,a.[Name] AS CustomerBranchName
				,cb.CustomerBranch
				,cb.[UnitOfCapacityId]
				,cb.[CurrencyId]
				,a.[CountryId]
		FROM [General].[Customers] a
		INNER JOIN [General].[Customers] cb on a.CustomerId = cb.CustomerBranch
		WHERE a.[CustomerId] = @pCustomerId
		AND cb.CustomerBranch IS NOT NULL
	) AS Customers

	SELECT @lIsCustomerBranch = COUNT(1) 
	FROM #Customers

	IF @pIssueForId  = 101
	BEGIN
		--VEHICLE
		SELECT  DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date],
				v.[Name] [VehicleName],
				v.[PlateId],
				cc.[CreditCardNumber],
				vg.[Name] as [VehicleGroupName],
				vu.[Name] AS [UnitName],
				c.[Name] AS [CustomerName],
				c.[CustomerBranchName],
				IsCustomerBranch = @lIsCustomerBranch,
				ISNULL((SELECT TOP 1 u.[Name]
						FROM [General].[Users] u
						INNER JOIN [General].[DriversUsers] du
						ON u.[UserId] = du.[UserId]
						WHERE du.[CustomerId] = c.[CustomerId] 
						AND du.[Code] = t.[DriverCode]
						ORDER BY du.[InsertDate] DESC), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 0)) [DriverName],
				ISNULL((SELECT TOP 1 du.[Identification]
						FROM [General].[Users] u
						INNER JOIN [General].[DriversUsers] du
						ON u.[UserId] = du.[UserId]
						WHERE du.[CustomerId] = c.[CustomerId] 
						AND du.[Code] = t.[DriverCode]
						ORDER BY du.[InsertDate] DESC), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 1)) [DriverId],	
				(SELECT TOP 1 TT.[Odometer] FROM(
					SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
					WHERE NT.[VehicleId] = t.[VehicleId] 
					      AND NT.[DATE] <= t.[InsertDate] 
						  AND (NT.[IsFloating] IS NULL OR NT.[IsFloating] = 0) 
						  AND (NT.[IsDenied] IS NULL OR NT.[IsDenied] = 0)
						  AND 1 > (SELECT ISNULL(COUNT(1), 0) 
								   FROM [Control].[Transactions] t2
								   WHERE t2.[CreditCardId] = NT.[CreditCardId] 
								   AND t2.[TransactionPOS] = NT.[TransactionPOS] 
								   AND t2.[ProcessorId] = NT.[ProcessorId] 
								   AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   ORDER BY NT.[Date] DESC ) TT 
				ORDER BY TT.[Date] ASC) AS [OdometerLast],
				t.[Odometer],		
				CASE WHEN @lPartnerUnit = 1 THEN (SELECT TOP 1 TT.[Liters] 
												  FROM(SELECT TOP 2 NT.* 
													   FROM [Control].[Transactions] NT
		    										   WHERE NT.[VehicleId] = t.[VehicleId] 
													   AND NT.[DATE] <= t.[InsertDate]
													   AND (NT.[IsFloating] IS NULL OR NT.[IsFloating] = 0) 
													   AND (NT.[IsDenied] IS NULL OR NT.[IsDenied] = 0)
													   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																FROM [Control].[Transactions] t2
																WHERE t2.[CreditCardId] = NT.[CreditCardId] 
																AND t2.[TransactionPOS] = NT.[TransactionPOS] 
																AND t2.[ProcessorId] = NT.[ProcessorId] 
																AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)) 
													   ORDER BY NT.[Date] DESC ) TT 
												  ORDER BY TT.[Date] ASC) * @lConvertionValue
			 ELSE (SELECT TOP 1 TT.[Liters] 
			 FROM(SELECT TOP 2 NT.* 
			      FROM [Control].[Transactions] NT
		    	  WHERE NT.[VehicleId] = t.[VehicleId] 
				  AND NT.[DATE] <= t.[InsertDate]
				  AND (NT.[IsFloating] IS NULL OR NT.[IsFloating] = 0) 
						  AND (NT.[IsDenied] IS NULL OR NT.[IsDenied] = 0)
						  AND 1 > (SELECT ISNULL(COUNT(1), 0) 
								   FROM [Control].[Transactions] t2
								   WHERE t2.[CreditCardId] = NT.[CreditCardId] 
								   AND t2.[TransactionPOS] = NT.[TransactionPOS] 
								   AND t2.[ProcessorId] = NT.[ProcessorId] 
								   AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)) 
				  ORDER BY NT.[Date] DESC ) TT 
			 ORDER BY TT.[Date] ASC)
			 END [LitersLast],	
		    CASE WHEN @lPartnerUnit = 1 THEN t.[Liters] * @lConvertionValue
				 ELSE t.[Liters] 
				 END [Liters],			
					t.[FuelAmount],
					f.[Name] AS [FuelName],
					[General].[GetServiceStationInfo](LTRIM(RTRIM(t.[ProcessorId])), c.[CustomerId], 0) AS [MerchantDescription],
				t.[TransactionPOS] AS [Reference],
				vc.[Manufacturer] AS [Manufacturer],
				DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [FechaValor],
				ISNULL(t.[Invoice], '-') AS [Invoice],
				g.[Symbol] AS [CurrencySymbol]
				,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
									FROM [General].[Countries] coun
									WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName]
				,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
					   WHEN t.[ExchangeValue] IS NOT NULL 
					   THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
								  (SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
									FROM [Control].[ExchangeRates] er
									INNER JOIN [Control].[Currencies] cue 
										ON er.[CurrencyCode] = cue.[CurrencyCode]
									INNER JOIN [General].[Countries] coun 
										ON coun.[CountryId] = c.[CountryId]
									WHERE cue.[Code] = coun.[Code]
										AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
											OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
				   END) AS [ExchangeValue]
				, (CASE 
							WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL THEN 
							[Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
							WHEN t.[IsInternational] = 0 THEN
							t.FuelAmount
							ELSE t.FuelAmount END) AS [RealAmount]
				,ISNULL((SELECT [InternationalCard] 
						FROM [General].[CustomerCreditCards]
						WHERE [CustomerId] = c.[CustomerId]
							  AND [StatusId] = 100
							  AND [InternationalCard] = 1), 0) [IsInternational]
		FROM [Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v 
			ON t.[VehicleId] = v.[VehicleId]
		INNER JOIN [General].[VehicleCategories] vc
			ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
		INNER JOIN #Customers c
			ON v.[CustomerId] = c.[CustomerId]
		INNER JOIN [Control].[Fuels] f 
			ON t.[FuelId] = f.[FuelId]
		INNER JOIN [Control].[CreditCardByVehicle] cv 
			ON t.[CreditCardId] = cv.[CreditCardId]
		INNER JOIN [Control].[Currencies] g 
			ON c.[CurrencyId] = g.[CurrencyId]
		INNER JOIN [Control].[CreditCard] cc
			ON cc.CreditCardId = t.CreditCardId
		INNER JOIN [General].[VehiclesByGroup] vbg
			ON vbg.[VehicleId] = v.[VehicleId]
		INNER JOIN [General].[VehicleGroups] vg
			ON vg.[VehicleGroupId] = vbg.[VehicleGroupId]
		LEFT JOIN [General].[VehicleCostCenters] e
			ON t.CostCenterId = e.CostCenterId	
		LEFT JOIN [General].[VehicleUnits] vu
			ON e.[UnitId] = vu.[UnitId]		
		LEFT OUTER JOIN [General].[Countries] co
			ON t.[CountryCode] = co.[Code] 		
		WHERE (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
		AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
		AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				 FROM [Control].[Transactions] t2
				 WHERE t2.[CreditCardId] = t.[CreditCardId] 
				 AND t2.[TransactionPOS] = t.[TransactionPOS] 
				 AND t2.[ProcessorId] = t.[ProcessorId] 
				 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))	
		AND (@pKey IS NULL 
			OR vg.[Name] like '%'+@pKey+'%'
			OR v.[PlateId] like '%'+@pKey+'%'
			OR v.[Name] like '%'+@pKey+'%')				
		AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
				AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
				AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear) OR
			(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
					AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
		ORDER BY vg.[Name] DESC, t.[Date] DESC	
	END
	ELSE
	BEGIN
	--DRIVERS
		SELECT  DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date],
				v.[Name] [VehicleName],
				v.[PlateId],
				cc.[CreditCardNumber],
				vg.[Name] as [VehicleGroupName],
				vu.[Name] AS [UnitName],
				u.[Name] [HolderName],
				c.[Name] AS [CustomerName],
				c.[CustomerBranchName],
				IsCustomerBranch = @lIsCustomerBranch,
				ISNULL((SELECT TOP 1 u.[Name]
						FROM [General].[Users] u
						INNER JOIN [General].[DriversUsers] du
						ON u.[UserId] = du.[UserId]
						WHERE du.[CustomerId] = c.[CustomerId] 
						AND du.[Code] = t.[DriverCode]
						ORDER BY du.[InsertDate] DESC), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 0)) [DriverName],
				ISNULL((SELECT TOP 1 du.[Identification]
						FROM [General].[Users] u
						INNER JOIN [General].[DriversUsers] du
						ON u.[UserId] = du.[UserId]
						WHERE du.[CustomerId] = c.[CustomerId] 
						AND du.[Code] = t.[DriverCode]
						ORDER BY du.[InsertDate] DESC), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 1)) [DriverId],	
				(SELECT TOP 1 TT.[Odometer] FROM(
					SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
					WHERE NT.[VehicleId] = t.[VehicleId] 
					      AND NT.[DATE] <= t.[InsertDate] 
						  AND (NT.[IsFloating] IS NULL OR NT.[IsFloating] = 0) 
						  AND (NT.[IsDenied] IS NULL OR NT.[IsDenied] = 0)
						  AND 1 > (SELECT ISNULL(COUNT(1), 0) 
								   FROM [Control].[Transactions] t2
								   WHERE t2.[CreditCardId] = NT.[CreditCardId] 
								   AND t2.[TransactionPOS] = NT.[TransactionPOS] 
								   AND t2.[ProcessorId] = NT.[ProcessorId] 
								   AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   ORDER BY NT.[Date] DESC ) TT 
				ORDER BY TT.[Date] ASC) AS [OdometerLast],
				t.[Odometer],		
				CASE WHEN @lPartnerUnit = 1 THEN (SELECT TOP 1 TT.[Liters] 
														  FROM(SELECT TOP 2 NT.* 
															   FROM [Control].[Transactions] NT
				    										   WHERE NT.[VehicleId] = t.[VehicleId] 
															   AND NT.[DATE] <= t.[InsertDate]
															   AND (NT.[IsFloating] IS NULL OR NT.[IsFloating] = 0) 
															   AND (NT.[IsDenied] IS NULL OR NT.[IsDenied] = 0)
															   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																		FROM [Control].[Transactions] t2
																		WHERE t2.[CreditCardId] = NT.[CreditCardId] 
																		AND t2.[TransactionPOS] = NT.[TransactionPOS] 
																		AND t2.[ProcessorId] = NT.[ProcessorId] 
																		AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)) 
															   ORDER BY NT.[Date] DESC ) TT 
														  ORDER BY TT.[Date] ASC) * @lConvertionValue
					 ELSE (SELECT TOP 1 TT.[Liters] 
					 FROM(SELECT TOP 2 NT.* 
					      FROM [Control].[Transactions] NT
				    	  WHERE NT.[VehicleId] = t.[VehicleId] 
						  AND NT.[DATE] <= t.[InsertDate]
						  AND (NT.[IsFloating] IS NULL OR NT.[IsFloating] = 0) 
								  AND (NT.[IsDenied] IS NULL OR NT.[IsDenied] = 0)
								  AND 1 > (SELECT ISNULL(COUNT(1), 0) 
										   FROM [Control].[Transactions] t2
										   WHERE t2.[CreditCardId] = NT.[CreditCardId] 
										   AND t2.[TransactionPOS] = NT.[TransactionPOS] 
										   AND t2.[ProcessorId] = NT.[ProcessorId] 
										   AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)) 
						  ORDER BY NT.[Date] DESC ) TT 
					 ORDER BY TT.[Date] ASC)
					 END [LitersLast],		
		    CASE WHEN @lPartnerUnit = 1 THEN t.[Liters] * @lConvertionValue
				 ELSE t.[Liters] 
				 END [Liters],			
					t.[FuelAmount],
					f.[Name] AS [FuelName],
					[General].[GetServiceStationInfo](LTRIM(RTRIM(t.[ProcessorId])), c.[CustomerId], 0) AS [MerchantDescription],
				t.[TransactionPOS] AS [Reference],
				vc.[Manufacturer] AS [Manufacturer],
				DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [FechaValor],
				ISNULL(t.[Invoice], '-') AS [Invoice],
				g.[Symbol] AS [CurrencySymbol]
				,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
									FROM [General].[Countries] coun
									WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName]
				,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
					   WHEN t.[ExchangeValue] IS NOT NULL 
					   THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
								  (SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
									FROM [Control].[ExchangeRates] er
									INNER JOIN [Control].[Currencies] cue 
										ON er.[CurrencyCode] = cue.[CurrencyCode]
									INNER JOIN [General].[Countries] coun 
										ON coun.[CountryId] = c.[CountryId]
									WHERE cue.[Code] = coun.[Code]
										AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
											OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
				   END) AS [ExchangeValue]
				, (CASE 
							WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL THEN 
							[Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
							WHEN t.[IsInternational] = 0 THEN
							t.FuelAmount
							ELSE t.FuelAmount END) AS [RealAmount]
				,ISNULL((SELECT [InternationalCard] 
						FROM [General].[CustomerCreditCards]
						WHERE [CustomerId] = c.[CustomerId]
							  AND [StatusId] = 100
							  AND [InternationalCard] = 1), 0) [IsInternational]
		FROM [Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v 
			ON t.[VehicleId] = v.[VehicleId]
		INNER JOIN [General].[VehicleCategories] vc
			ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
		INNER JOIN #Customers c
			ON v.[CustomerId] = c.[CustomerId]
		INNER JOIN [Control].[Fuels] f 
			ON t.[FuelId] = f.[FuelId]
		INNER JOIN [Control].[CreditCardByDriver] cd 
			ON t.[CreditCardId] = cd.[CreditCardId]				
		INNER JOIN [General].[Users] u 
				ON cd.[UserId] = u.[UserId]
		INNER JOIN [Control].[Currencies] g 
			ON c.[CurrencyId] = g.[CurrencyId]
		INNER JOIN [Control].[CreditCard] cc
			ON cc.CreditCardId = t.CreditCardId
		INNER JOIN [General].[VehiclesByGroup] vbg
			ON vbg.[VehicleId] = v.[VehicleId]
		INNER JOIN [General].[VehicleGroups] vg
			ON vg.[VehicleGroupId] = vbg.[VehicleGroupId]
		LEFT JOIN [General].[VehicleCostCenters] e
			ON t.CostCenterId = e.CostCenterId	
		LEFT JOIN [General].[VehicleUnits] vu
			ON e.[UnitId] = vu.[UnitId]		
		LEFT OUTER JOIN [General].[Countries] co
			ON t.[CountryCode] = co.[Code] 		
		WHERE (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
		AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
		AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				 FROM [Control].[Transactions] t2
				 WHERE t2.[CreditCardId] = t.[CreditCardId] 
				 AND t2.[TransactionPOS] = t.[TransactionPOS] 
				 AND t2.[ProcessorId] = t.[ProcessorId] 
				 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))			
		AND (@pKey IS NULL 
			OR vg.[Name] like '%'+@pKey+'%'
			OR v.[PlateId] like '%'+@pKey+'%'
			OR v.[Name] like '%'+@pKey+'%')				
		AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
				AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
				AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear) OR
			(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
					AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
		ORDER BY vg.[Name] DESC, t.[Date] DESC		
	END
	SET NOCOUNT OFF
END

