/****** Object:  StoredProcedure [General].[Sp_CustomerBranch_AddOrEdit]    Script Date: 12/30/2019 10:39:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---- =============================================
---- Author:      Juan K Santamaria
---- Create Date: 12-23-2019
---- Description: add and edit a customer's branches
---- =============================================
ALTER PROC [General].[Sp_CustomerBranch_AddOrEdit]
(
	@pCustomerId INT, 
	@pCustomerBranch VARCHAR(MAX),  ---XML
	@pModifyUserId INT
)
AS
BEGIN 

	DECLARE @lxmlData XML = CONVERT(XML, @pCustomerBranch)		

	CREATE TABLE #CustomersBranchs 
	(
		[CustomerId] INT
	)

	INSERT INTO #CustomersBranchs
	SELECT   [CustomerId] = m.c.value('CustomerId[1]', 'INT') 
	FROM @lxmlData.nodes('//BranchCustomers') AS m(c)
	
	--STEP-1 
	UPDATE [General].[Customers]
	SET [CustomerBranch] = NULL
	WHERE [CustomerBranch] = @pCustomerId

	--STEP-2
	UPDATE c	
	SET  c.CustomerBranch = @pCustomerId
		,c.ModifyDate = GETDATE()
		,c.ModifyUserId = @pModifyUserId
	FROM [General].[Customers]  c
	INNER JOIN #CustomersBranchs AS b 
		ON c.[CustomerId] = b.[CustomerId]

END


