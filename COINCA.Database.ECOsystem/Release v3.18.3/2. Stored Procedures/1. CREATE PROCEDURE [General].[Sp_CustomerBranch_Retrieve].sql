USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerBranch_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerBranch_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 23/12/2019
-- Description:	Retrieve CustomerBranch Information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerBranch_Retrieve]
(
	@pCustomerId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
		
		SELECT [CustomerId]
			  ,[Name] [EncryptedName]
		FROM [General].[Customers] c
		WHERE [CustomerBranch] = @pCustomerId

	SET NOCOUNT OFF
END