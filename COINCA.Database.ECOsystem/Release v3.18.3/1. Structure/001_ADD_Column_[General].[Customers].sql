/**
	Creacion de la columna CustomerBranch
	PARA RELACIONAR LA COLUMNA CustomerID
	Fecha: 2019-12-23
**/

IF NOT EXISTS(select * from sys.columns where name = 'CustomerBranch')
	ALTER TABLE [General].[Customers] ADD CustomerBranch INT NULL

