USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_TermsAndConditions_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_TermsAndConditions_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/05/2019
-- Description:	Retrieve Terms and conditions by Id
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_TermsAndConditions_Retrieve]
(
	@pPartnerId INT = NULL
)
AS
BEGIN		
	SET NOCOUNT ON
	
	SELECT	t.[Id],
			t.[Support],
			t.[Thefth], 
			t.[Services], 
			t.[Info], 
			t.[Terms],
			t.[PartnerId],
			p.[Name] [PartnerName],
			ISNULL(t.[ShowTerms], CAST(1 AS BIT)) [ShowTerms],
			ISNULL(t.[Active], CAST(1 AS BIT)) [Active]
	FROM [General].[TermsAndConditions] t
	INNER JOIN [General].[Partners] p
		ON t.[PartnerId] = p.[PartnerId]
	WHERE p.[PartnerId] = @pPartnerId 
	AND t.[Delete] = 0

    SET NOCOUNT OFF
END
