USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_TermsAndConditions_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_TermsAndConditions_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/05/2019
-- Description:	Retrieve Terms and conditions by Id
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_TermsAndConditions_AddOrEdit]
(
	@pId INT = NULL,
    @pSupport VARCHAR(300),
    @pThefth VARCHAR(MAX) = NULL,
    @pServices VARCHAR(MAX) = NULL,
    @pInfo VARCHAR(MAX) = NULL,
    @pTerms VARCHAR(MAX) = NULL,
    @pPartnerId INT = NULL,
    @pShowTerms BIT,
    @pActive BIT,
	@pUserId INT
)
AS
BEGIN		
	SET NOCOUNT ON	
    SET XACT_ABORT ON

	 BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000),
				@lErrorSeverity INT,
				@lErrorState INT,
				@lLocalTran BIT = 0
              
        IF @@TRANCOUNT = 0
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END

        IF @pId IS NULL
		BEGIN
			INSERT INTO [General].[TermsAndConditions]
			(
				[Support],
				[Thefth], 
				[Services], 
				[Info], 
				[Terms],
				[PartnerId],				
				[ShowTerms],
				[Active],
				[InsertUserId],
				[InsertDate]
			)
			VALUES
			(
				@pSupport,
				@pThefth,
				@pServices,
				@pInfo,
				@pTerms,
				@pPartnerId,
				@pShowTerms,
				@pActive,
				@pUserId,
				GETDATE()
			)
	    END
		ELSE		     
		BEGIN
			UPDATE [General].[TermsAndConditions]
			SET [Support] = @pSupport,
				[Thefth] = @pThefth, 
				[Services] = @pServices, 
				[Info] = @pInfo, 
				[Terms] = @pTerms,
				[ShowTerms] = @pShowTerms,
				[Active] = @pActive,
				[ModifyUserId] = @pUserId,
				[ModifyDate] = GETDATE()
			WHERE [Id] = @pId
		END 
		   		
        IF @@TRANCOUNT > 0 AND @lLocalTran = 1
		BEGIN
			COMMIT TRANSACTION
		END			
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0
		BEGIN
			ROLLBACK TRANSACTION
		END	
			
		SELECT @lErrorMessage = ERROR_MESSAGE(), 
			   @lErrorSeverity = ERROR_SEVERITY(), 
			   @lErrorState = ERROR_STATE() 

		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH

	SET XACT_ABORT OFF
    SET NOCOUNT OFF
END
