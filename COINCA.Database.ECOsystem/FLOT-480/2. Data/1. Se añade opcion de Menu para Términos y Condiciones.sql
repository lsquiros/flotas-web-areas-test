USE [ECOsystemDev]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/05/2019
-- Description:	Se a�ade opcion de Menu para T�rminos y Condiciones
-- ================================================================================================

DECLARE @NewPermID NVARCHAR(128) = NEWID()
	   ,@lParentId INT

INSERT INTO [dbo].[AspNetPermissions]
VALUES
(
	@NewPermID
	,'View_TermsAndConditions'
	,'TermsAndConditions'
	,'Index'
	,''
	,GETDATE()
)

SELECT @lParentId = [Id]
FROM [dbo].[AspMenus]
WHERE [Name] = 'Socios'
AND [Header] = 2

INSERT INTO [dbo].[AspMenus]
VALUES
(
	'T�rminos y Condiciones',
	'',
	@NewPermID,
	@lParentId,
	2,
	27,
	GETDATE()
)

DECLARE @NewMenuID INT = SCOPE_IDENTITY()

--INSERT PERMISSIONS TO THE MENU AND ROLE
INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('21C5EB03-6C0D-4B16-8F0A-A10C994A8157', @NewMenuID) -- Super Usuario Coinca
INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('4b356447-c286-45d6-bace-d1cc1ae298db', @NewMenuID) -- SUPER_ADMIN

INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('21C5EB03-6C0D-4B16-8F0A-A10C994A8157', @NewPermID) -- Super Usuario Coinca
INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('4b356447-c286-45d6-bace-d1cc1ae298db', @NewPermID) -- SUPER_ADMIN