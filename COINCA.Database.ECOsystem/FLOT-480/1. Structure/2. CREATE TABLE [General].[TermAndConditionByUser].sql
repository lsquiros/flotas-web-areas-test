USE [ECOsystemDev]
GO

IF OBJECT_ID (N'[General].[TermsAndConditionsByUser]', N'U') IS NOT NULL DROP TABLE [General].[TermsAndConditionsByUser]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/05/2019
-- Description:	Create table Term And Condition by User
-- ================================================================================================

CREATE TABLE [General].[TermsAndConditionsByUser]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[TermsAndConditionsId] INT,
	[UserId] INT, 
	[AcceptDate] DATETIME,
	[Archive] BIT, 
	[PartnerId] INT
) 
GO

ALTER TABLE [General].[TermsAndConditionsByUser] WITH CHECK ADD CONSTRAINT [FK_TermsAndConditionsByUser_TermsAndConditionsId] FOREIGN KEY([TermsAndConditionsId])
REFERENCES [General].[TermsAndConditions] ([Id])

GO

ALTER TABLE [General].[TermsAndConditionsByUser] WITH CHECK ADD CONSTRAINT [FK_TermsAndConditionsByUser_PartnerId] FOREIGN KEY([PartnerId])
REFERENCES [General].[Partners] ([PartnerId])

GO
