USE [ECOsystemDev]
GO

IF OBJECT_ID (N'[General].[TermsAndConditions]', N'U') IS NOT NULL DROP TABLE [General].[TermsAndConditions]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/05/2019
-- Description:	Create table Term And Condition
-- ================================================================================================

CREATE TABLE [General].[TermsAndConditions]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[Support] VARCHAR(300),
	[Thefth] VARCHAR(MAX), 
	[Services] VARCHAR(MAX), 
	[Info] VARCHAR(MAX), 
	[Terms] VARCHAR(MAX),
	[PartnerId] INT,
	[ShowTerms] BIT,
	[Active] BIT DEFAULT(1),
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME	
) 
GO

ALTER TABLE [General].[TermsAndConditions] WITH CHECK ADD CONSTRAINT [FK_TermsAndConditions_PartnerId] FOREIGN KEY([PartnerId])
REFERENCES [General].[Partners] ([PartnerId])

GO
