
--DROP TYPE TypeTerminals
--GO
-- ================================================================================================
-- Author:		Juan Carlos Santamaria V.
-- Create date: 2019-11-05
-- Description:	Define Type Table by terminals
-- ================================================================================================
CREATE TYPE [dbo].[TypeTerminals] AS TABLE(
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [ServiceStationId] INT ,
    [TerminalId] VARCHAR(100),
    [Commerce] VARCHAR(500),
    [UserId] INT
)

GO



