
IF EXISTS(SELECT 1 FROM sys.objects WHERE name = 'Sp_ByServiceStations_Import' AND type = 'P')
	DROP PROC [General].[Sp_ByServiceStations_Import]
GO

-- ================================================================================================
-- Author:		Juan Carlos Santamaria V.
-- Create date: 2019-11-05
-- Description:	Add Terminals  to Service Stations
-- ================================================================================================
CREATE  PROCEDURE [General].[Sp_ByServiceStations_Import] 
( 
   @pCountryId INT
  ,@pUserId INT
  ,@pPartnerId INT
  ,@pXml VARCHAR(MAX)= NULL
)
AS

BEGIN

  SET NOCOUNT ON;
  SET XACT_ABORT ON;

  BEGIN TRY

    DECLARE @lErrorMessage VARCHAR(4000);
    DECLARE @lErrorSeverity INT;
    DECLARE @lErrorState INT;
    DECLARE @lxmlData XML= CONVERT(XML, @pXml);
    DECLARE @TerminalsTable TypeTerminals;

    SELECT m.c.value('BacId[1]', 'VARCHAR(100)') AS [BacId],
           m.c.value('Name[1]', 'VARCHAR(100)') AS [Name],
           m.c.value('Address[1]', 'VARCHAR(100)') AS [Address],
           m.c.value('Legal_Id[1]', 'VARCHAR(100)') AS [Legal_Id],
           m.c.value('TerminalId[1]', 'VARCHAR(100)') AS [TerminalId],
           @pCountryId AS CountryId,
           @pUserId AS UserId,
           @pPartnerId AS PartnerId
    INTO #ResultXml
    FROM @lxmlData.nodes('//ServiceStations') AS m(c);

    SELECT DISTINCT [BacId], [Name], [Address], [Legal_Id], [CountryId], [UserId], [PartnerId]
    INTO #ServiceStations
    FROM #ResultXml sta

    INSERT INTO @TerminalsTable
    SELECT DISTINCT see.ServiceStationId
          , ter.TerminalId
          , ter.Name
          , ter.UserId    
    FROM #ResultXml ter
      INNER JOIN [General].[ServiceStations] see ON ter.BacId = see.BacId
    
    BEGIN TRAN

      UPDATE [General].[ServiceStations]
      SET [General].[ServiceStations].ModifyDate	= GETDATE()
         ,[General].[ServiceStations].ModifyUserId = @pUserId
         ,[General].[ServiceStations].[Name] = t.[Name]
         ,[General].[ServiceStations].[Address] = t.[Address]
         ,[General].[ServiceStations].[Legal_Id] = t.[Legal_Id]
      FROM #ServiceStations t
        INNER JOIN [General].[ServiceStations] A ON T.BacId = A.BacId

    COMMIT

    /*Send Terminals for Update*/
    EXEC [General].[Sp_TerminalsByServiceStations_Import] @TerminalsTable

	/* Return ServiceStations don't exists*/
    SELECT a.[BacId]
          , a.[Name]
          , a.[Address]
          , a.[Legal_Id]
    FROM #ServiceStations a
      LEFT JOIN [General].[ServiceStations] b WITH(NOLOCK) ON A.BacId = b.BacId
    WHERE B.BacId IS NULL

  END TRY
  BEGIN CATCH
  	
    IF (@@TRANCOUNT > 0 AND XACT_STATE() > 0)
  	BEGIN
      ROLLBACK TRANSACTION
    END

  	SELECT @lErrorMessage = ERROR_MESSAGE()
  		  , @lErrorSeverity = ERROR_SEVERITY()
  		  , @lErrorState = ERROR_STATE()
  	RAISERROR 
  	( 
  		@lErrorMessage
  		,@lErrorSeverity
  		,@lErrorState
  	)
  END CATCH

  SET NOCOUNT OFF;
  SET XACT_ABORT OFF;
END;

GO



