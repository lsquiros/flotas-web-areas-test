
IF EXISTS(SELECT 1 FROM sys.objects WHERE name = 'Sp_TerminalsByServiceStations_Import' AND type = 'P')
	DROP PROC [General].[Sp_TerminalsByServiceStations_Import]
GO

-- ================================================================
-- Author:		Juan Carlos Santamaria V.
-- Create date: 2019-11-05
-- Description:	Terminals Update by Block
-- ================================================================
CREATE  PROCEDURE [General].[Sp_TerminalsByServiceStations_Import]
(
    @pTerminalsTable TypeTerminals READONLY
)
AS
BEGIN

    DELETE DE
    FROM [General].[TerminalsByServiceStations] DE
    INNER JOIN @pTerminalsTable A ON DE.ServiceStationId = A.ServiceStationId

    INSERT INTO [General].[TerminalsByServiceStations]
    SELECT ServiceStationId,
           TerminalId,
           Commerce,
           InsertUserId = [UserId],
           InsertDate = GETDATE()
    FROM @pTerminalsTable A 

END

GO


