/****** Object:  StoredProcedure [Control].[Sp_TransactionsReportDenied_Retrieve]    Script Date: 12/27/2019 10:42:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:  Kevin Peña  
-- Create date: 07/24/2015  
-- Description: Retrieve DeniedTransactionsReport log  
-- =============================================================================================================== 
-- Melvin Salas - 01/22/2015 - Dinamic Filter
-- Henry Retana - 03/03/2015 - Edit UTC Time
-- Cindy Vargas - 08/12/2015 - Resolve issue in Scope "While"  
-- Cindy Vargas - 19/08/2016 - Add the transaction country name to the retrieve information  
-- Cindy Vargas - 29/08/2016 - Add the transaction ExchangeValue and Amount to the retrieve information  
-- Cindy Vargas PENDIENTE VALIDAR OBTENER EL FUELAMOUNT DEL LOG 
-- Henry Retana - 29/09/2016 - Change the retrieve in the exchange value, change the country name (remove)  
-- Stefano Quirós - 12/06/2016 - Add the Time Zone Parameter - Add TransactionId value to the retrieve   
-- Henry Retana - 07/09/2017 - Add CostCenter by Driver  
-- Esteban Solís - 18/12/2017 - Add Driver Id logic 
-- Stefano Quirós - 10/01/2018 - Change logic that use new table [LogTransactionPosDetail]
-- Stefano Quirós - 15/02/2019 - Add condition to retrieve only the left join where be the same CustomerId on the parameter
-- María de los Ángeles Jiménez - FEB/27/2019 - Show denied transaction detail (magnifying glass)
-- Stefano Quirós - 09/09/2019 - Delete Inner Join from CustomerCredit, duplicates the info, make a subquery.
-- ===============================================================================================================
ALTER PROCEDURE [Control].[Sp_TransactionsReportDenied_Retrieve] --9713, 2019, 10, null, null, 1, null--14031, null, null, '2019-09-09 00:00:00.000', '2019-09-10 00:00:00.000', 1, null
(
	 @pCustomerId INT
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL 
	,@pUserId INT 
	,@pDriverId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	-- DYNAMIC FILTER  
	DECLARE @Results TABLE (items INT)
	DECLARE @count INT

	INSERT @Results
	EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId

	SET @count = (
			SELECT COUNT(*)
			FROM @Results
			)

	DECLARE @lTimeZoneParameter INT
	,@lCostCenterDriver BIT

	SET @lCostCenterDriver = ISNULL(
		(
			SELECT TOP 1 [Value]
			FROM [General].[ParametersByCustomer]
			WHERE [CustomerId] = @pCustomerId
				AND [ResuourceKey] = 'COSTCENTERBYDRIVER'
		), 0)

	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId

	IF @pDriverId IS NULL
	BEGIN
		SELECT ltpd.[SystemTraceNumber]
		,CAST(NULL AS CHAR(10)) [HolderName]
		,CAST(NULL AS CHAR(10)) [FuelName]
		,CAST(NULL AS CHAR(10)) [Date]
		,CAST(ltpd.[ExpirationDate] AS VARCHAR(4)) [ExpirationDate]
		,ltpd.[Amount] AS [FuelAmount]
		,CAST(ltpd.[Odometer] AS FLOAT) [Odometer]
		,ltpd. [Liters]
		,v.[PlateId] [PlateId]
		,CAST(NULL AS CHAR(10)) [CurrencySymbol]
		,'Rechazada' [State]
		,ltp.[Message]
		,ltpd.[TransportData]
		,DATEADD(HOUR, @lTimeZoneParameter, ltpd.[InsertDate]) [InsertDate] --UTC  
		,ltpd.[ResponseCode]
		,ltpd.[ResponseDescription] [ResponseCodeDescription]
		,ltpd.[Id] [TransactionId]  --[TransactionId] no exist
		,ltpd.[CardNumber] [CreditCardNumber]
		,cu.[Name] [CustomerName]
		,CASE @lCostCenterDriver WHEN 0 THEN	
			(SELECT TOP 1 cc.[Name]
			 FROM [General].[Vehicles] v
			 INNER JOIN [General].[VehicleCostCenters] cc 
			 	ON v.[CostCenterId] = cc.[CostCenterId]
			 WHERE (
			 		@count = 0
			 		OR v.[VehicleId] IN (
			 			SELECT items
			 			FROM @Results
			 			)
			 		) -- DYNAMIC FILTER  
			 	AND [CustomerId] = @pCustomerId
			 	AND [PlateId] = v.[PlateId])
		 ELSE (SELECT TOP 1 cc.[Name]
				FROM [General].[DriversUsers] u
				INNER JOIN [General].[VehicleCostCenters] cc ON u.[CostCenterId] = cc.[CostCenterId]
				INNER JOIN [General].[VehiclesByUser] vbu ON u.[UserId] = vbu.[UserId]
					AND vbu.[VehicleId] = (
						SELECT TOP 1 [VehicleId]
						FROM [General].[Vehicles]
						WHERE [PlateId] = v.[PlateId]
							AND (
								[IsDeleted] = 0
								OR [IsDeleted] IS NULL
								)
							AND [CustomerId] = @pCustomerId
						)
				WHERE vbu.[LastDateDriving] >= ltpd.[InsertDate]
					OR vbu.[LastDateDriving] IS NULL)
		 END AS [CostCenterName]
		,ltpd.[TerminalId] [TerminalId]
		,(
			SELECT  TOP 1 s.[Name]
			FROM [General].[TerminalsByServiceStations] t
			INNER JOIN [General].[ServiceStations] s
			ON t.[TerminalId] = ltpd.[TerminalId]
			AND s.[ServiceStationId] = t.[ServiceStationId]
		 ) AS [ServiceStationName]
		,ISNULL((SELECT [InternationalCard] 
						FROM [General].[CustomerCreditCards]
						WHERE [CustomerId] = cu.[CustomerId]
							  AND [StatusId] = 100
							  AND [InternationalCard] = 1), 0) [IsInternational]	
	FROM [Control].[LogTransactionsPOSDetail] ltpd
	INNER JOIN [Control].[LogTransactionsPOS] ltp
		ON ltpd.[Id] = ltp.[LTPDId]
	LEFT JOIN [General].[Vehicles] v
		ON v.[PlateId] = ltpd.[CarTag]
		   AND v.[CustomerId] = @pCustomerId
	INNER JOIN [General].[Customers] cu 
		ON cu.CustomerId = ltp.CustomerId
	WHERE ltp.[CustomerId] = @pCustomerId
		AND ((@pYear IS NOT NULL
			  AND @pMonth IS NOT NULL
			  AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, ltpd.[InsertDate])) = @pMonth
			  AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, ltpd.[InsertDate])) = @pYear)
			  OR (@pStartDate IS NOT NULL
			  AND @pEndDate IS NOT NULL
			  AND DATEADD(HOUR, @lTimeZoneParameter, ltpd.[InsertDate]) BETWEEN @pStartDate
			  AND DATEADD(day, 1, @pEndDate)))
		AND ((ltp.ResponseCode LIKE 'F%' OR ltp.ResponseCode = '05') AND ltpd.[ResponseCode] <> '00')
	ORDER BY ltpd.[InsertDate] DESC
	END
	ELSE
	BEGIN
		SELECT ltpd.[SystemTraceNumber]
		,CAST(NULL AS CHAR(10)) [HolderName]
		,CAST(NULL AS CHAR(10)) [FuelName]
		,CAST(NULL AS CHAR(10)) [Date]
		,CAST(ltpd.[ExpirationDate] AS VARCHAR(4)) [ExpirationDate]
		,ltpd.[Amount] AS [FuelAmount]
		,CAST(ltpd.[Odometer] AS FLOAT) [Odometer]
		,ltpd. [Liters]
		,v.[PlateId] [PlateId]
		,CAST(NULL AS CHAR(10)) [CurrencySymbol]
		,'Rechazada' [State]
		,ltp.[Message]
		,ltpd.[TransportData]
		,DATEADD(HOUR, @lTimeZoneParameter, ltp.[InsertDate]) [InsertDate] --UTC  
		,ltpd.[ResponseCode]
		,ltpd.[ResponseDescription] [ResponseCodeDescription]
		,ltpd.[Id] [TransactionId] --[TransactionId] no exist
		,ltpd.[CardNumber] [CreditCardNumber]
		,cu.[Name] [CustomerName]
		,CASE @lCostCenterDriver WHEN 0 THEN	
			(SELECT cc.[Name]
			 FROM [General].[Vehicles] v
			 INNER JOIN [General].[VehicleCostCenters] cc 
			 	ON v.[CostCenterId] = cc.[CostCenterId]
			 WHERE (
			 		@count = 0
			 		OR v.[VehicleId] IN (
			 			SELECT items
			 			FROM @Results
			 			)
			 		) -- DYNAMIC FILTER  
			 	AND [CustomerId] = @pCustomerId
			 	AND [PlateId] = v.[PlateId])
		 ELSE (SELECT cc.[Name]
				FROM [General].[DriversUsers] u
				INNER JOIN [General].[VehicleCostCenters] cc ON u.[CostCenterId] = cc.[CostCenterId]
				INNER JOIN [General].[VehiclesByUser] vbu ON u.[UserId] = vbu.[UserId]
					AND vbu.[VehicleId] = (
						SELECT TOP 1 [VehicleId]
						FROM [General].[Vehicles]
						WHERE [PlateId] = v.[PlateId]
							AND (
								[IsDeleted] = 0
								OR [IsDeleted] IS NULL
								)
							AND [CustomerId] = @pCustomerId
						)
				WHERE vbu.[LastDateDriving] >= ltpd.[InsertDate]
					OR vbu.[LastDateDriving] IS NULL)
		 END AS [CostCenterName]
		,ltpd.[TerminalId] [TerminalId]
		,(
			SELECT  TOP 1 s.[Name]
			FROM [General].[TerminalsByServiceStations] t
			INNER JOIN [General].[ServiceStations] s
			ON t.[TerminalId] = ltpd.[TerminalId]
			AND s.[ServiceStationId] = t.[ServiceStationId]
		 ) AS [ServiceStationName]
		,ISNULL((SELECT [InternationalCard] 
						FROM [General].[CustomerCreditCards]
						WHERE [CustomerId] = cu.[CustomerId]
							  AND [StatusId] = 100
							  AND [InternationalCard] = 1), 0) [IsInternational]	
	FROM [Control].[LogTransactionsPOSDetail] ltpd
	INNER JOIN [Control].[LogTransactionsPOS] ltp
		ON ltpd.[Id] = ltp.[LTPDId]
	LEFT JOIN [General].[Vehicles] v
		ON v.[PlateId] = ltpd.[CarTag] 
		   AND v.[CustomerId] = @pCustomerId
	INNER JOIN [General].[Customers] cu 
		ON cu.CustomerId = ltp.CustomerId	
	WHERE ltp.[CustomerId] = @pCustomerId
		AND ((@pYear IS NOT NULL
			  AND @pMonth IS NOT NULL
			  AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, ltpd.[InsertDate])) = @pMonth
			  AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, ltpd.[InsertDate])) = @pYear)
			  OR (@pStartDate IS NOT NULL
			  AND @pEndDate IS NOT NULL
			  AND DATEADD(HOUR, @lTimeZoneParameter, ltpd.[InsertDate]) BETWEEN @pStartDate
			  AND DATEADD(day, 1, @pEndDate)))
		AND ((ltp.ResponseCode LIKE 'F%' OR ltp.ResponseCode = '05') AND ltpd.[ResponseCode] <> '00')
	ORDER BY ltpd.[InsertDate] DESC
		
	END
	SET NOCOUNT OFF
END