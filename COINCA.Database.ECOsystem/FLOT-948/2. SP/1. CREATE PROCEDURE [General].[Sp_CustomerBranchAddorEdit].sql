
IF EXISTS (SELECT * FROM SYS.OBJECTS WHERE NAME = 'Sp_CustomerBranchAddorEdit')
	DROP PROC [General].[Sp_CustomerBranchAddorEdit]
GO

---- =============================================
---- Author:      Juan K Santamaria
---- Create Date: 12-23-2019
---- Description: add and edit a customer's branches
---- =============================================
CREATE PROC [General].[Sp_CustomerBranchAddorEdit]
(
	@CustomerId INT, 
	@CustomerBranch VARCHAR(MAX),  ---XML
	@ModifyUserId INT
)
AS
BEGIN 

	DECLARE @lxmlData XML = CONVERT(XML, @CustomerBranch)		
	CREATE TABLE #CustomersBranchs ([CustomerBranch] VARCHAR(100), CustomerId INT)

	INSERT INTO #CustomersBranchs
	SELECT   CustomerBranch = m.c.value('CustomerBranch[1]', 'VARCHAR(100)') 
			,CustomerId = @CustomerId 
	FROM @lxmlData.nodes('//Customer') AS m(c)
	
	--STEP-1 
	UPDATE [General].[Customers]
	SET [CustomerBranch] = NULL
	WHERE CustomerId = @CustomerId

	--STEP-2
	UPDATE [General].[Customers]
	SET  CustomerBranch = b.CustomerId
		,ModifyDate = GETDATE()
		,ModifyUserId = @ModifyUserId
	FROM [General].[Customers] 
	INNER JOIN #CustomersBranchs AS b 
		ON [General].[Customers].CustomerId = b.[CustomerBranch]

END


GO
