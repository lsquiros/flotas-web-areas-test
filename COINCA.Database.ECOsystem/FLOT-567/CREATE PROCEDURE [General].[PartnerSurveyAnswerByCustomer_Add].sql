IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[PartnerSurveyAnswerByCustomer_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[PartnerSurveyAnswerByCustomer_Add]
GO

-- ================================================================================================
-- Author:		María de los Ángeles Jiménez 
-- Create date: JUL/03/2019
-- Description:	Add or edit a customer's answer for a survey.
-- ================================================================================================
CREATE PROCEDURE [General].[PartnerSurveyAnswerByCustomer_Add]
(
	 @pAnswersXML VARCHAR(MAX) = NULL
	,@pCustomerId INT
	,@pUserId INT	
)
AS BEGIN

	BEGIN TRANSACTION

	BEGIN TRY
	
		DECLARE @lAnswersXML XML = CONVERT(XML, @pAnswersXML)

		INSERT INTO [General].[PartnerSurveyAnswerByCustomer] (
			 [PartnerSurveyQuestionId]
			,[PartnerSurveyAnswerId]
			,[CustomerId]
			,[Text]
			,[InsertUserId]
			,[InsertDate]
		)
		SELECT m.c.value('PartnerSurveyQuestionId[1]', 'int') AS [PartnerSurveyQuestionId]
			,CASE WHEN m.c.value('Id[1]', 'int') = 0 THEN NULL ELSE m.c.value('Id[1]', 'int') END AS [PartnerSurveyAnswerId]
			,@pCustomerId
			,m.c.value('AnswerText[1]', 'VARCHAR(1200)') AS [Text]
			,@pUserId
			,GETDATE()
		FROM @lAnswersXML.nodes('/PartnerSurveyAnswer/PartnerSurveyAnswer') AS m(c)

		COMMIT TRANSACTION
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT

		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
END