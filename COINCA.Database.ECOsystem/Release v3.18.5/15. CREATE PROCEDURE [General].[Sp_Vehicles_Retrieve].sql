USE [ECOSYSTEMQA]

IF EXISTS(SELECT 1 FROM SYS.OBJECTS WHERE NAME LIKE '%[General].[Sp_Vehicles_Retrieve]%')
	DROP PROC [General].[Sp_Vehicles_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[SP_FuelDistribution_Retrieve]    Script Date: 2/14/2020 2:15:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Vehicle information
-- Modify by: Melvin Salas - 1/22/2015 - Dinamic Filter 
-- Modify by: Cindy Vargas - 13/07/2016 - Add Condition 'AND a.[IsDeleted] =  @IsNotDeleted'
-- Modify by: Melvin Salas - Aug/04/2016 - Add PullPreviousBudget
-- Modify by: Melvin Salas - Aug/11/2016 - Return Credit Card Balance
-- Modify by: Stefano Quirós - Sept/05/2016 - Fix retrieve data 
-- Modify by: Marco Cabrera - 24/02/2017 - Add ExternalId to the result output
-- Modify by: Albert Estrada - Sep/22/2017 - Add Odometer retrieve 
-- Modify by: Albert Estrada - Oct/17/2017 - Add (cc.statusid is null or cc.statusid = 7) on Where
-- Modify by: Henry Retana - 23/10/2017 - Validates Credit Card Status
-- Modify by: Esteban Solís - Oct/23/2017 - Added HasCooler, TempSensorCount, MinTemperature and MaxTemperature fields
-- Modify by: Henry Retana - 24/07/2018 - Add Vehicle Number in the retrieve
-- Modify by: Henry Retana - 18/09/2018 - Add Vehicles from Vehicles by Customers
-- Modify by: Gerald Solano - 20/09/2018 - Odometer column is changed to return the value without decimals
-- Modify by: Gerald Solano - 28/09/2018 - Year column is added in the data selection
-- Modify by: Gerald Solano - 04/10/2018 - Update query to get linked customer
-- Modify by: Gerald Solano - 28/11/2018 - Update query to get vehicle classification
-- Modify by: Gerald Solano - 07/05/2019 - Add columns with available commands info
-- Modify by: Gerald Solano - 08/05/2019 - Added the columns SupportTicket and SupportTicketURL
-- Modify by: Gerald Solano - 05/06/2019 - Applied the distinct statement in the final query
-- Modify vy: Stefano Quirós - 24/07/2019 - Add Case when the creditcard is not Active or Blocked then don't return the assigned credit
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Vehicles_Retrieve] --33594, 7181, null, 1
(
	 @pVehicleId INT = NULL
	,@pCustomerId INT
	,@pKey VARCHAR(800) = NULL
	,@pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	DECLARE @pPartnerId INT = NULL,
			@IsNotDeleted INT = 0,
			@lInsertDate DATETIME,
			@lGetVehiclesByCustomer BIT = 0 

	--Validates if the Customer is linked to LV Vehicles 
	SELECT @lGetVehiclesByCustomer = CAST(COUNT(1) AS BIT)
	FROM [General].[PartnerLinkedToCustomer]
	WHERE CustomerId = @pCustomerId

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)	

	SELECT @pPartnerId = cp.[PartnerId]
	FROM [General].[CustomersByPartner] cp
	WHERE cp.[CustomerId] = @pCustomerId	
	
	SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)

	IF(@pVehicleId IS NOT NULL)
	BEGIN
		SELECT 
			CAST(ROW_NUMBER() OVER(ORDER BY query.[VehicleId] DESC) AS INT) [VehicleNumber]
			,query.*
		FROM 
			( 
				SELECT DISTINCT 
						a.[VehicleId]
						,a.[PlateId]
						,a.[Name]
						,a.[CostCenterId]
						,c.[Name] AS CostCenterName
						,ISNULL(e.[UserId], 0) AS [UserId]
						,ISNULL(e.[Name], '') AS [EncryptedUserName]
						,a.[VehicleCategoryId]
						,d.[Manufacturer]
						,d.[VehicleModel]
						,d.[Year][VehicleYear]
						,d.[Type] as VehicleType
						,d.[Manufacturer]+ '-' +d.VehicleModel + ', ' + d.[Type] AS CategoryType
						,d.[Type] AS CategoryType2
						,d.[Liters]
						,b.[Name] AS FuelName
						,b.[FuelId] as DefaultFuelId
						,a.[Active]
						,a.[Colour]
						,a.[Chassis]
						,a.[LastDallas]
						,a.[FreightTemperature]
						,a.[FreightTemperatureThreshold1]
						,a.[FreightTemperatureThreshold2]
						,a.[Predictive]
						,a.[AVL]
						,a.[PhoneNumber]
						,a.[Insurance]
						,a.[DateExpirationInsurance]
						,a.[CoverageType]
						,a.[NameEnterpriseInsurance]
						,a.[PeriodicityId]
						,a.[Cost]
						,g.[PartnerId]
						,g.[CapacityUnitId] [PartnerCapacityUnitId]
						,a.[AdministrativeSpeedLimit]
						,a.[CabinPhone]
						,a.[IntrackReference]
						,a.[Imei]
						,a.[RowVersion]
						,a.[PullPreviousBudget]
						,CASE WHEN cc.[CreditCardId] IS NULL 
						 THEN 0 
						 ELSE (ISNULL(fdg.[Amount], 0) + ISNULL(fdg.[AdditionalAmount], 0)) 
						 END [TotalBudget]
						,cc.[CreditAvailable] [Available]
						,CASE WHEN cc.[CreditCardId] IS NULL 
						 THEN 0 
						 ELSE ISNULL(fdg.[AdditionalAmount], 0)
						 END [AdditionalBudget]
						,vg.[VehicleGroupId]
						,vg.[Name] [VehicleGroupName]
						,a.[ExternalId]
						,CAST(a.[Odometer] AS INT) [OdometerVehicle]
						,a.[HasCooler]
						,a.[TempSensorCount]
						,a.[MinTemperature]
						,a.[MaxTemperature]
						,cst.[Id] [ClassificationId] 
						,ISNULL(cst.[Name], 'Ninguno') [ClassificationName]
						,(CASE WHEN oe.[ComdApertura] IS NULL THEN CAST(0 AS BIT) ELSE oe.[ComdApertura] END) [HasOpenDoors]
						,(CASE WHEN oe.[ComdCierre] IS NULL THEN CAST(0 AS BIT) ELSE oe.[ComdCierre] END) [HasTurnOff]
						,(CASE WHEN oe.[HasDallas] IS NULL THEN CAST(0 AS BIT) ELSE oe.[HasDallas] END) [HasDallas]
						,a.[SupportTicket]
						,a.[SupportTicketURL]
				FROM [General].[Vehicles] a
				INNER JOIN [General].[VehicleCostCenters] c
					ON a.[CostCenterId] = c.[CostCenterId]
				INNER JOIN [General].[VehicleCategories] d		
					ON a.[VehicleCategoryId] = d.[VehicleCategoryId]
				INNER JOIN [Control].[Fuels] b					
					ON d.[DefaultFuelId] = b.[FuelId]
				LEFT JOIN  [General].[Users] e					
					ON e.[UserId] = (SELECT TOP 1 [UserId]
									 FROM [General].[VehiclesByUser]
									 WHERE [VehicleId] = a.[VehicleId]
									 ORDER BY [InsertDate] DESC)
				INNER JOIN [General].[Customers] f				
					ON a.[CustomerId] = f.[CustomerId]
				INNER JOIN [General].[CustomersByPartner] h			
					ON h.[CustomerId] = f.[CustomerId]
				INNER JOIN [General].[Partners] g
					ON h.[PartnerId] = g.[PartnerId]
				LEFT JOIN  [Control].[CreditCardByVehicle] ccbv	
						ON a.[VehicleId] = ccbv.[VehicleId]
						AND ccbv.[CreditCardId] IN (SELECT [CreditCardId] 
													FROM [Control].[CreditCard] c 
													WHERE c.[CustomerId] = a.[CustomerId] 
													AND c.[StatusId] IN (7, 8))
				LEFT JOIN  [Control].[CreditCard] cc
					ON ccbv.[CreditCardId] = cc.[CreditCardId] 
					AND cc.[StatusId] IN (7, 8)
				LEFT JOIN [General].[VehiclesByGroup] vbg
					ON a.[VehicleId] = vbg.[VehicleId]
				LEFT JOIN [General].[VehicleGroups] vg
					ON vg.[VehicleGroupId] = vbg.[VehicleGroupId]
				LEFT JOIN (SELECT [VehicleId]
								 ,SUM([Amount]) [Amount]
								 ,SUM([AdditionalAmount]) [AdditionalAmount]
						   FROM [Control].[FuelDistribution] fd
						   WHERE CONVERT(DATETIME, fd.[InsertDate]) >= @lInsertDate
						   GROUP BY [VehicleId]
						  ) fdg
					ON fdg.[VehicleId] = a.[VehicleId]
				LEFT JOIN [General].[VehiclesByCustomers] vc
					ON vc.[VehicleId] = a.[VehicleId]
				LEFT JOIN [General].[VehicleClassification] vcf
					ON vcf.[VehicleId] = a.[VehicleId]
				LEFT JOIN [General].[ClassificationType] cst
					ON cst.[Id] = vcf.[ClassificationId]
				LEFT JOIN [dbo].[OrdenesEncabezado] oe
					ON oe.[orden] = (
						SELECT TOP 1 oe2.orden FROM  Composiciones AS cmp
											INNER JOIN DispositivosAVL dal ON dal.dispositivo = cmp.dispositivo
											INNER JOIN Devices de ON dal.terminalId = Cast(de.UnitID AS VARCHAR) 
											INNER JOIN [dbo].[OrdenesEncabezado] oe2 ON oe2.vehiculo = cmp.vehiculo
						WHERE de.device = a.DeviceReference 
						ORDER BY oe2.orden DESC
					)
				WHERE (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
				AND (a.[CustomerId] = @pCustomerId 
					 OR (@lGetVehiclesByCustomer = 1 AND vc.[CustomerId] = @pCustomerId))
				AND (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
				AND (@lGetVehiclesByCustomer = 1 
					 OR (@pPartnerId IS NULL OR g.[PartnerId] = @pPartnerId))
				AND (@pKey IS NULL			OR a.[Name] like '%'+@pKey+'%'
											OR a.[Chassis] like '%'+@pKey+'%'
											OR a.[PlateId] like '%'+@pKey+'%'
											OR b.[Name] like '%'+@pKey+'%'
											OR c.[Name] like '%'+@pKey+'%'
											OR d.[Type] like '%'+@pKey+'%'
											OR CONVERT(VARCHAR(20),d.[Year]) like '%'+@pKey+'%')
				AND (a.[IsDeleted] IS NULL OR a.[IsDeleted] = @IsNotDeleted)
		) query
		ORDER BY query.[VehicleId] DESC
	END
	ELSE	
	BEGIN
		
		SELECT 
			CAST(ROW_NUMBER() OVER(ORDER BY query.[VehicleId] DESC) AS INT) [VehicleNumber]
			,query.*
		FROM 
			( 
				SELECT DISTINCT 
						a.[VehicleId]
						,a.[PlateId]
						,a.[Name]
						,a.[CostCenterId]
						,c.[Name] [CostCenterName]
						,ISNULL(e.[UserId], 0) [UserId]
						,ISNULL(e.[Name], '') [EncryptedUserName]
						,a.[VehicleCategoryId]
						,d.[Manufacturer]
						,d.[VehicleModel]
						,d.[Year][VehicleYear]
						,d.[Type] [VehicleType]
						,d.[Manufacturer] + '-' +d.[VehicleModel] + ', ' + d.[Type] [CategoryType]
						,d.[Type] [CategoryType2]
						,d.[Liters]
						,b.[Name] [FuelName]
						,b.[FuelId] [DefaultFuelId]
						,a.[Active]
						,a.[Colour]
						,a.[Chassis]
						,a.[LastDallas]
						,a.[FreightTemperature]
						,a.[FreightTemperatureThreshold1]
						,a.[FreightTemperatureThreshold2]
						,a.[Predictive]
						,a.[AVL]
						,a.[PhoneNumber]
						,a.[Insurance]
						,a.[DateExpirationInsurance]
						,a.[CoverageType]
						,a.[NameEnterpriseInsurance]
						,a.[PeriodicityId]
						,a.[Cost]
						,g.[PartnerId]
						,g.[CapacityUnitId] [PartnerCapacityUnitId]
						,a.[AdministrativeSpeedLimit]
						,a.[CabinPhone]
						,a.[IntrackReference]
						,a.[Imei]
						,a.[RowVersion]
						,a.[PullPreviousBudget]
						,CAST(CASE 
							  WHEN (a.[DeviceReference] > 0 AND a.[DeviceReference] IS NOT NULL) 
									AND (a.[IntrackReference] > 0 AND a.[IntrackReference] IS NOT NULL)					
							  THEN 1
							  ELSE 0
						 END AS bit) [HasGPS]
						 ,a.ExternalId
						 ,CAST(a.[Odometer] AS INT) [OdometerVehicle]
						 ,a.[HasCooler]
						 ,a.[TempSensorCount]
						 ,a.[MinTemperature]
						 ,a.[MaxTemperature]
						 ,cst.[Id] [ClassificationId] 
						 ,ISNULL(cst.[Name], 'Ninguno') [ClassificationName]
						 ,(CASE WHEN oe.[ComdApertura] IS NULL THEN CAST(0 AS BIT) ELSE oe.[ComdApertura] END) [HasOpenDoors]
						,(CASE WHEN oe.[ComdCierre] IS NULL THEN CAST(0 AS BIT) ELSE oe.[ComdCierre] END) [HasTurnOff]
						,(CASE WHEN oe.[HasDallas] IS NULL THEN CAST(0 AS BIT) ELSE oe.[HasDallas] END) [HasDallas]
						,a.[SupportTicket]
						,a.[SupportTicketURL]
				FROM [General].[Vehicles] a
				INNER JOIN [General].[VehicleCostCenters] c
					ON a.[CostCenterId] = c.[CostCenterId]
				INNER JOIN [General].[VehicleCategories] d		
					ON a.[VehicleCategoryId] = d.[VehicleCategoryId]
				INNER JOIN [Control].[Fuels] b					
					ON d.[DefaultFuelId] = b.[FuelId]
				LEFT JOIN  [General].[Users] e					
					ON e.[UserId] = (SELECT TOP 1 [UserId]
									 FROM [General].[VehiclesByUser]
									 WHERE [VehicleId] = a.[VehicleId]
									 ORDER BY [InsertDate] DESC)
				INNER JOIN [General].[Customers] f				
					ON a.[CustomerId] = f.[CustomerId]
				INNER JOIN [General].[CustomersByPartner] h			
					ON h.[CustomerId] = f.[CustomerId]
				INNER JOIN [General].[Partners] g
					ON h.[PartnerId] = g.[PartnerId]
				LEFT JOIN [General].[VehiclesByCustomers] vc
					ON vc.[VehicleId] = a.[VehicleId]	
				LEFT JOIN [General].[VehicleClassification] vcf
					ON vcf.[VehicleId] = a.[VehicleId]
				LEFT JOIN [General].[ClassificationType] cst
					ON cst.[Id] = vcf.[ClassificationId]
				LEFT JOIN [dbo].[OrdenesEncabezado] oe
					ON oe.[orden] = (
						SELECT TOP 1 oe2.orden FROM  Composiciones AS cmp
											INNER JOIN DispositivosAVL dal ON dal.dispositivo = cmp.dispositivo
											INNER JOIN Devices de ON dal.terminalId = Cast(de.UnitID AS VARCHAR) 
											INNER JOIN [dbo].[OrdenesEncabezado] oe2 ON oe2.vehiculo = cmp.vehiculo
						WHERE de.device = a.DeviceReference 
						ORDER BY oe2.orden DESC
					)
				WHERE (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
				AND (a.[CustomerId] = @pCustomerId 
					 OR (@lGetVehiclesByCustomer = 1 AND vc.[CustomerId] = @pCustomerId))
				AND (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
				AND (@lGetVehiclesByCustomer = 1 
					 OR (@pPartnerId IS NULL OR g.[PartnerId] = @pPartnerId))
				AND (@pKey IS NULL			OR a.[Name] like '%'+@pKey+'%'
											OR a.[Chassis] like '%'+@pKey+'%'
											OR a.[PlateId] like '%'+@pKey+'%'
											OR b.[Name] like '%'+@pKey+'%'
											OR c.[Name] like '%'+@pKey+'%'
											OR d.[Type] like '%'+@pKey+'%'
											OR CONVERT(VARCHAR(20),d.[Year]) like '%'+@pKey+'%')
				AND (a.[IsDeleted] IS NULL OR a.[IsDeleted] = @IsNotDeleted)
		) query
		ORDER BY query.[VehicleId] DESC	
	END
	
    SET NOCOUNT OFF
END