/****** Object:  StoredProcedure [Control].[Sp_TransactionRules_AddOrEdit]    Script Date: 2/10/2020 6:53:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano.
-- Create date: 17/08/2015
-- Description:	Add Or Edit Status for Transactions Rules 
-- Modify By: Stefano Quirós Ruiz
-- Description: Insert Rules by Vehicle, CostCenter or Customer
-- Modify By: Stefano Quirós - Add the Time Zone Parameter - 12/06/2016 -- Add validation when
-- @pRuleCustomerId is 0
-- Stefano Quirós - Add Validation when FuelPermit is 1 - 10/02/2020
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_TransactionRules_AddOrEdit]
(
	@pCustomerId INT,	--@pCustomerId: FK of Customer
	@pRuleCustomerId INT = NULL,
	@pRuleId INT,
	@pRuleActive BIT = 0,
	@pRuleValue FLOAT = NULL,
	@pUserId INT = NULL,
	@pVehicleId INT = NULL,
	@pCostCenterId INT = NULL,
	@pRuleVehicleId INT = NULL,
	@pRuleCostCenterId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
		DECLARE @lTimeZoneParameter INT 
		DECLARE @lFuelPermit BIT

		SELECT @lTimeZoneParameter = [TimeZone]	
		      ,@lFuelPermit = [FuelPermit]
		FROM [General].[Countries] co 
		INNER JOIN [General].[Customers] cu
			ON co.[CountryId] = cu.[CountryId] 
		WHERE cu.[CustomerId] = @pCustomerId

	    IF(@pVehicleId !=0)
		BEGIN
			IF(@pRuleCustomerId IS NOT NULL AND @pRuleCustomerId <> 0)
			BEGIN 
				UPDATE [Control].[TransactionsRulesByVehicle]
				SET
					[RuleActive] = @pRuleActive,
					[RuleValue] = @pRuleValue,
					[ModifyDate] = DATEADD(hour, @lTimeZoneParameter, GETDATE()),
					[ModifyUserId] = @pUserId
				WHERE [Id] = @pRuleCustomerId
			END	
			ELSE
			BEGIN
				INSERT INTO [Control].[TransactionsRulesByVehicle]
				(
				    [VehicleId]
				   ,[RuleId]
				   ,[RuleActive]
				   ,[RuleValue]
				   ,[InsertDate]
				   ,[InsertUserId]
				)
				VALUES
				(
				    @pVehicleId
				   ,@pRuleId
				   ,CASE WHEN @lFuelPermit = 1 THEN 1
				    ELSE @pRuleActive
					END
				   ,@pRuleValue
				   ,DATEADD(hour, @lTimeZoneParameter, GETDATE())
				   ,@pUserId
				)
			END
		END
		ELSE IF(@pCostCenterId != 0)
		BEGIN
			IF(@pRuleCustomerId IS NOT NULL AND @pRuleCustomerId <> 0)
			BEGIN 
				UPDATE [Control].[TransactionsRulesByCostCenter]
				SET
					[RuleActive] = @pRuleActive,
					[RuleValue] = @pRuleValue,
					[ModifyDate] = DATEADD(hour, @lTimeZoneParameter, GETDATE()),
					[ModifyUserId] = @pUserId
				WHERE [Id] = @pRuleCustomerId
			END	
			ELSE
			BEGIN
				INSERT INTO [Control].[TransactionsRulesByCostCenter]
				(
				    [CostCenterId]
				   ,[RuleId]
				   ,[RuleActive]
				   ,[RuleValue]
				   ,[InsertDate]
				   ,[InsertUserId]
				)
				VALUES
				(
				    @pCostCenterId
				   ,@pRuleId
				   ,CASE WHEN @lFuelPermit = 1 THEN 1
				    ELSE @pRuleActive
					END
				   ,@pRuleValue
				   ,DATEADD(hour, @lTimeZoneParameter, GETDATE())
				   ,@pUserId
				)
			END
		END
		ELSE
		BEGIN
			IF(@pRuleCustomerId IS NOT NULL AND @pRuleCustomerId <> 0)
			BEGIN 
				UPDATE [Control].[TransactionsRulesByCustomer]
				SET
					[RuleActive] = @pRuleActive,
					[RuleValue] = @pRuleValue,
					[ModifyDate] = DATEADD(hour, @lTimeZoneParameter, GETDATE()),
					[ModifyUserId] = @pUserId
				WHERE [Id] = @pRuleCustomerId
			END	
			ELSE
			BEGIN
				INSERT INTO [Control].[TransactionsRulesByCustomer]
				(
				    [CustomerId]
				   ,[RuleId]
				   ,[RuleActive]
				   ,[RuleValue]
				   ,[InsertDate]
				   ,[InsertUserId]
				)
				VALUES
				(
				    @pCustomerId
				   ,@pRuleId
				   ,CASE WHEN @lFuelPermit = 1 THEN 1
				    ELSE @pRuleActive
					END
				   ,@pRuleValue
				   ,DATEADD(hour, @lTimeZoneParameter, GETDATE())
				   ,@pUserId
				)
			END
		END
		
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
