/****** Object:  StoredProcedure [General].[Sp_Customers_Retrieve]    Script Date: 31/01/2020 11:06:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:	  Andrés Oviedo Brenes
-- Create date: 20/01/2014
-- Description: Retrieve Customer information
-- Modify By: Gerald Solano - 19/04/2016 - Add column CostCardEmision and CostModuleEO for retrieve customers
-- Modify By: Stefano Quirós - 25/04/2016 - Add column [GPSMOdalityId], [CostRentedGPS] and CountryCode for retrieve customers
-- Modify By: Melvin Salas - Jan 06, 2017 - Add Membership Fee
-- Modify By: Marco Cabrera - Mar 01, 2017 - Add timezone
-- Modify By: Marco Cabrera - Mar 21, 2017 - Add CostCardReposition
-- Modify By: Kevin Peña - Mar 30, 2017 - Quit Logo value
-- Modify By: Henry Retana - 03/05/2015 - Add the transaccions offline parameter to the retrieve
-- Modify By: Henry Retana - 05/09/2015 - Add the costcenter by driver to the retrieve
-- Modify By: Kevin Peña - 15/11/2017 - Add the isDemo,CostRentedBAC to the retrieve
-- Modify By: Henry Retana - 15/11/2017 - Validates GPS Count
-- Modify By: Henry Retana - 18/06/2018 - Add the prices edit
-- Modify By: Henry Retana - 18/06/2018 - SP Optimization
-- Modify By: Gerald Solano - 26/09/2018 - Add columns for [General].[CustomerAdditionalInformation] table 
-- Modify By: Gerald Solano - 29/11/2018 - Add vehicles with classification and the same Partner ID for the current search
-- Modify By: Stefano Quirós - 14/01/2019 - Add Distribution Type Parameter to the retrieve
-- Modify By: Gerald Solano - 02/04/2019 - Add PartnerId Column in the final query
-- Modify By: Henry Retana - 16/08/2019 - Add the CostCenterFuelDistribution to the retrieve
-- Modify By: Henry Retana - 11/11/2019 - Add Field SapCode
-- Modify By: Jason Bonilla - 31/01/2020 - Add Fiel FuelPermit
-- ================================================================================================

ALTER PROCEDURE [General].[Sp_Customers_Retrieve] 
( 
	 @pCustomerId INT = NULL,
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL,
	 @pPartnerId INT = NULL,
	 @pLoadSingleCustomer BIT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	--Classification ID
	DECLARE @ClassificationID INT = 0 

	--Default Logo
	DECLARE @DefaultLogo VARCHAR(MAX) = 'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjUzRUM2NkYzMEFCQzExRTY5OEY3RUZBMjFFNjEyMTg5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjUzRUM2NkY0MEFCQzExRTY5OEY3RUZBMjFFNjEyMTg5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTNFQzY2RjEwQUJDMTFFNjk4RjdFRkEyMUU2MTIxODkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NTNFQzY2RjIwQUJDMTFFNjk4RjdFRkEyMUU2MTIxODkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAAGBAQEBQQGBQUGCQYFBgkLCAYGCAsMCgoLCgoMEAwMDAwMDBAMDg8QDw4MExMUFBMTHBsbGxwfHx8fHx8fHx8fAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wAARCAGAAcwDAREAAhEBAxEB/8QAgwABAQEBAAMBAQAAAAAAAAAAAAcFBgIDBAEIAQEBAAAAAAAAAAAAAAAAAAAAARABAAECAwIKBwUGBgMBAAAAAAECAwQFBhEWITFxUpLSs1Q1NkFRYbESg5OBIrITc5Gh0UIjQ8EycqLCFGKCYyQRAQEBAAAAAAAAAAAAAAAAAAARAf/aAAwDAQACEQMRAD8A/o1UAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfDmeeZXlsR/278UVzw02421Vz/6wDJ3+yL1XuhHWKpv9kXNvdCOsUN/si5t7oR1ihv8AZFzb3QjrFDf7Iube6EdYob/ZFzb3QjrFDf7Iube6EdYob/ZFzb3QjrFDf7Iube6EdYob/ZFzb3QjrFDf7Iube6EdYob/AGRc290I6xQ3+yLm3uhHWKG/2Rc290I6xQ3+yLm3uhHWKG/2Rc290I6xQ3+yLm3uhHWKG/2Rc290I6xQ3+yLm3uhHWKG/wBkXNvdCOsUN/si5t7oR1ij6MJrTIcRci3+dVZqngibtPwx0o2xH2iNyJiYiYnbE8MTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPkzbHxgMtxGMmNs2qNtMTxTVPBTH7ZBJcTib+Jv1379c3Ltyfirqn0yivWAAAAAAAAAAAAAAAAAAAAADuNA5xduRdy29VNUW6fzLEz6Kduyqn98bFwdiIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAw9aeXMVy2+0pBMkVQNKZJlOKyHDX8Rhbd27V8fxV1RtmdlyqI/co192sh7ja6Ihu1kPcbXRA3ayHuNrogbtZD3G10QN2sh7ja6IG7WQ9xtdEDdrIe42uiBu1kPcbXRA3ayHuNrogbtZD3G10QN2sh7ja6IG7WQ9xtdEDdrIe42uiBu1kPcbXRA3ayHuNrogbtZD3G10QN2sh7ja6IG7WQ9xtdEDdrIe42uiBu1kPcbXRBNc7s2rOb4y1apii3RerpoojiiIniRWtoLx75NfvgwUZUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYetPLmK5bfaUgmSKp2i/LeE5bnaVKjbAAAAAAAAAAAAAAAAAAABJtQ+OY/8AXr/EitPQXj3ya/fBgoyoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAw9aeXMVy2+0pBMkVTtF+W8Jy3O0qVG2AAAAAAAAAAAAADkdSa0qw12vB5bsm7RPw3cRPDFM+qmOKZj1yK5C9nObXq/ju4y9VP+uqI+yInZCD7Mu1XneCriYxFV+36bV6ZriY5Z+9H2SCg5JnWFzbCfn2fu108F21PHRV/D1SqNAAEm1D45j/ANev8SK09BePfJr98GCjKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADD1p5cxXLb7SkEyRVO0X5bwnLc7SpUbYAAAAAAAAAAAAM3UmPrwOS4nEW52XYpii3PqqrmKdv2bdoJQigANvR2PrwmeWaIn+niZ/JuU+v4v8v8Au2ApyoAk2ofHMf8Ar1/iRWnoLx75NfvgwUZUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYetPLmK5bfaUgmSKp2i/LeE5bnaVKjbAAABi6m1FbynDfDb2V427H9G3Pojn1ez3gnOIzLMMTdm7fxFyu5M7ds1Twckej7EV0WltXX7F+nCZjdm5hrnBRernbNFXo21T/L7lHfCAAAAAMTWViu9p7EfBG2bc03Jj2U1Rt/cCYooADT01Yrv59gaKY4abtNyeS39+fcCrKgCTah8cx/69f4kVp6C8e+TX74MFGVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGHrTy5iuW32lIJkiqdovy3hOW52lSo2wAAZ+eZ1h8pwU37v3rlXBZtemqr+EemQS3HY3EY3FXMTiKvju3J2zPoj1RHshFegAHcaL1N+ZFGV4yv78cGFuT6Y5k+31KOxEAAAAeNy3Rct1W64iqiuJpqpnimJ4JgEy1FpvE5ViKqqaZrwVc/0r3Hs2/y1eqfeisUHlRRXcriiimaq6p2U00xtmZ9kQChaP03Xl1urF4uNmLuxspo5lHHsn2z6VHSiAJNqHxzH/r1/iRWnoLx75NfvgwUZUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYetPLmK5bfaUgmSKp2i/LeE5bnaVKjbAB82Y5hhsvwleKxNXw26I4vTVPopj2yCW5xm+JzTG1Ym/OyOK1bjiop9ER/iivhAAB+01VU1RVTMxVE7YmOCYmAUfSepKczw/8A1sRVEY6zHD/9KY/mj2+tUdCAAAAD8qpprpmmqIqpmNk0zG2JgGVe0pp69X8deCoiZ5k1UR+yiaYB9WByfK8DO3CYai1VxfHEbaulO2QfYAACTah8cx/69f4kVp6C8e+TX74MFGVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGHrTy5iuW32lIJkiqdovy3hOW52lSo2weF+/Zw9mu9erii1biaq654oiATDUmoL2b4vbG2jCWpmLFr/lV7ZRWQAAAAD24bE38LiKMRYrmi7bn4qKo9YKhp/PLGb4KLtOynEUbIv2vVV649k+hUagAAAAAAAAAJNqHxzH/AK9f4kVp6C8e+TX74MFGVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGHrTy5iuW32lIJkiqdovy3hOW52lSo25mKYmZnZEcMzPFEAnOrdSzmN6cJhatmBtTwzH9yqP5uSPQiucAAAAAAB9mU5picsxtGKsTw08FdE8VVM8dMgqeWZlhsxwdGKw9W2ivjpnjpq9NM+2FR9QAAAOZ1Nq6jLqpwmC+G5jP7lU8NNv+NQrl7WstQ0XfzJxPxxt4aKqKPhn2cER+5B2+n9R4XN7OyNlrF0R/VsTP8Aup9cKjXABJtQ+OY/9ev8SK09BePfJr98GCjKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADD1p5cxXLb7SkEyRVO0X5bwnLc7SpUYWs9T/AJk15Zgq/wCnHBibtPpnmR7PWK45AAAAAAAABr6cz+9lGM+KdtWFubIv2o9XOj2wCn2L9m/ZovWa4rtXIiqiuOKYlUeYAOX1XqunBU1YLBVROMmNly5HDFuJ/wCXuFT+qqqqqaqpmapnbMzwzMyg/AezD4i/hr1F+xXNu7bnbRXTxxIKNprVNjNKIsX9lrHUxw0cUVxHpp/xhUb4JNqHxzH/AK9f4kVp6C8e+TX74MFGVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGHrTy5iuW32lIJkiulo1NOC0xhsvwlWzF3Iufm3I/t0zcq4I/8AKYBzQAAAAAAAAAAOl0hqWcvvRg8VV/8Aiuz92qf7dU+n/TPp/aCiRMTG2OGJ4pVHL6r1XTgqasDgaonGTGy5cjhi3E/8vcKn9VVVVU1VTM1TO2ZnhmZlB+AAA8rdyu3XTct1TRXTO2mqmdkxMemJBQNL6uox0U4PHVRRjOKi5xU3P4VKON1D45j/ANev8SDT0F498mv3wYKMqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMPWnlzFctvtKQTJFAAAAAAAAAAAAAb+D1jmWFyirAUcNyPu2cRM/eoo9MfZ6AYNVVVVU1VTM1TO2ZnhmZkH4AAAABEzExMTsmOKQed27cu3Krt2qa7lc7aqp4ZmfXIOg0F498mv3wYKMqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMPWnlzFctvtKQTJFAAAAAAAAAAAAAAAAAAAAAAdHoLx75NfvgwUZUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYetPLmK5bfaUgmSKAAAAAAAAAAAAAAAAAAAAAA6PQXj3ya/fBgoyoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAw9aeXMVy2+0pBMkUAAAAAAAAAAAAAAAAAAAAAB0egvHvk1++DBRlQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABia08uYrlt9pSCYooAAAAAAAAAAAAAAAAAAAAADo9BeO/Jr98GCjKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD5szwNGOwF/CVzsi9RNMVeqeOmfskEmx2BxOCxNeGxNE0XaJ2TE8Ux649cSivQAAAAAAAAAAAAAAAAAAAAADvNCZJew1u5mGIpmiu/T8FiieCfg27Zqn/VsjYo60QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB82OyzAY+iKMXYovRH+Wao4Y5Ko4YBmbl6c7rP1LnWA3K053afqXOsKblac7tP1LnWA3K053afqXOsBuVpzu0/UudYDcrTndp+pc6wG5WnO7T9S51gNytOd2n6lzrAblac7tP1LnWA3K053afqXOsBuVpzu0/UudYDcrTndp+pc6wG5WnO7T9S51gNytOd2n6lzrAblac7tP1LnWA3K053afqXOsBuVpzu0/UudYDcrTndp+pc6wG5WnO7T9S51gNytOd2n6lzrAblac7tP1LnWB78JpfIcLci5awlPxxwxVXNVeyfZFUzAjVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//Z'

	--Flags for the Functions
	DECLARE @lWithGPS BIT = 1,
			@lWithOutGPS BIT = 0,
			@lXmlManagementResult VARCHAR(MAX) = NULL,
			@lXmlSMSResult VARCHAR(MAX) = NULL,
			@lManagementPriceId INT = 0,
			@lSMSPriceId INT = 0,
			@ActiveVehicleSearch INT = 0,
			@ClassificationSearch INT = 0,
			@ProductType INT = 0
			
	DECLARE @tblCustomerByVehicle TABLE( CustomerId INT )		
	
	-- Get product type from Partner
	SELECT @ProductType = [ProductTypesId] 
	FROM [General].[Partners]
	WHERE [PartnerId] = @pPartnerId
	
	--GET THE XML INFORMATION FOR THE MANAGEMENT PRICES				
	IF @pCustomerId IS NOT NULL
	BEGIN
		SELECT @lManagementPriceId = [Value]
		FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'CUSTOMER_PRICE_AGENTS' 

		SELECT @lSMSPriceId = [Value]
		FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'CUSTOMER_PRICE_SMS'

		--GET MANAGEMENT PRICES
		EXEC [General].[Sp_CustomerPrices_Retrieve] @pCustomerId, NULL, @lManagementPriceId, @pXMLResult = @lXmlManagementResult OUTPUT

		--GET SMS PRICES
		EXEC [General].[Sp_CustomerPrices_Retrieve] @pCustomerId, NULL, @lSMSPriceId, @pXMLResult = @lXmlSMSResult OUTPUT
	END 

	--Validate ProductType and PartnerId from CustomerId
	IF(@ProductType = 3) --ControlCar
	BEGIN
		--Evitamos traer las imagenes en base 64 // tema de rendimiento
		SET @pLoadSingleCustomer = 1
		SET @DefaultLogo = ''

		IF(@pCustomerId > 0)
		BEGIN
			DECLARE @PartnerFromCustomer INT = 0
				
			SELECT @PartnerFromCustomer = PartnerId 
			FROM [General].[CustomersByPartner]
			WHERE CustomerId = @pCustomerId
		
			IF(@PartnerFromCustomer <> @pPartnerId)
			BEGIN 
				SET @pPartnerId = @PartnerFromCustomer
			END
		END
	END
	
	--Get classification from PartnerId 
	IF(@pPartnerId IS NOT NULL)
	BEGIN
		
		SELECT @pCountryId = [CountryId] 
		FROM [General].[Partners] 
		WHERE PartnerId = @pPartnerId

		SELECT @ClassificationID = [ClassificationId] 
		FROM [General].[ClassificationByPartner] 
		WHERE [PartnerId] = @pPartnerId

	END
	
	-- Customer Search in Vehicles table
	IF(@pKey IS NOT NULL AND LEN(@pKey) > 0)
	BEGIN 
		INSERT INTO @tblCustomerByVehicle(CustomerId)
		SELECT v.CustomerId FROM [General].[Vehicles] v
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.CustomerId = v.CustomerId 
		INNER JOIN [General].[Customers] c
			ON cp.CustomerId = c.CustomerId 
		WHERE (@pPartnerId IS NULL OR cp.[PartnerId] = @pPartnerId) AND
			  c.[IsDeleted] <> 1 AND
			  (UPPER(v.PlateId) LIKE '%' + @pKey + '%' OR
			  UPPER(v.Chassis) LIKE '%' + @pKey + '%')
		
		--Get vehicles from classification
		IF(@ClassificationID IS NOT NULL)
		BEGIN 
			INSERT INTO @tblCustomerByVehicle(CustomerId)
			SELECT DISTINCT v.CustomerId FROM [General].[Vehicles] v			
			INNER JOIN [General].[Customers] c
				ON v.CustomerId = c.CustomerId 
			INNER JOIN [General].[CustomersByPartner] cp
				ON cp.CustomerId = c.CustomerId
			INNER JOIN [General].[Partners] p
				ON p.PartnerId = cp.[PartnerId]
			WHERE v.VehicleId IN ( SELECT [VehicleId] FROM [General].[VehicleClassification] WHERE [ClassificationId] = @ClassificationID ) AND
				  c.[IsDeleted] <> 1 AND
				  p.[CountryId] = @pCountryId AND
				  (UPPER(v.PlateId) LIKE '%' + @pKey + '%' OR
				  UPPER(v.Chassis) LIKE '%' + @pKey + '%')
		END

		IF((SELECT COUNT(1) FROM @tblCustomerByVehicle) > 0)
		BEGIN
			SET @ActiveVehicleSearch = 1
		END			
	END
	ELSE
	BEGIN
		
		--Get vehicles from classification
		IF(@ClassificationID IS NOT NULL)
		BEGIN 
			INSERT INTO @tblCustomerByVehicle(CustomerId)
			SELECT DISTINCT v.CustomerId FROM [General].[Vehicles] v			
			INNER JOIN [General].[Customers] c
				ON v.CustomerId = c.CustomerId 
			WHERE v.VehicleId IN ( SELECT [VehicleId] 
										FROM [General].[VehicleClassification] 
											WHERE [ClassificationId] = @ClassificationID ) AND
				  c.[IsDeleted] <> 1 
		END

		IF((SELECT COUNT(1) FROM @tblCustomerByVehicle) > 0)
		BEGIN
			SET @ClassificationSearch = 1
		END		
	END

	-- RESULT QUERY SELECT
	SELECT cbp.[PartnerId]
		  ,a.[CustomerId]
		  ,a.[Name] [EncryptedName]
		  ,a.[CurrencyId]
		  ,b.[Name] [CurrencyName]
		  ,CASE @pLoadSingleCustomer 
				WHEN 1 
				THEN (
						CASE WHEN (a.[Logo] = 'N/A') THEN @DefaultLogo
							 ELSE a.[Logo] END
					 )
				ELSE @DefaultLogo
		   END [Logo]
		  ,a.[IsActive]
		  ,a.[AccountNumber]
		  ,a.[UnitOfCapacityId]
		  ,d.[Name] [UnitOfCapacityName]
		  ,a.[IssueForId]
		  ,c.[Name] [IssueForStr]
		  ,a.[CreditCardType]
		  ,CASE a.[AvailableModules] 
				WHEN 'L' 
				THEN 'ECO' 
				ELSE ISNULL(a.[AvailableModules], '') 
		   END [AvailableModules]
		  ,CASE a.[CreditCardType] 
				WHEN 'C' 
				THEN 'Crédito' 
				WHEN 'D' 
				THEN 'Débito' 
				ELSE '' 
		   END [CreditCardTypeStr]
		  ,b.[Symbol] [CurrencySymbol]
		  ,a.[CountryId]
		  ,e.[Name] [CountryName]
		  ,e.[Code] [CountryCode]
		  ,a.[InsertDate]
		  ,a.[RowVersion]
		  ,a.[CostCardAdmin]
		  ,a.[CostCardEmision]
		  ,a.[CostModuleEO]
		  ,a.[GPSMOdalityId] [GPSModality]
		  ,a.[CostRentedGPS]
		  ,a.[CostRentedBAC] 
		  ,a.[MembershipFee]
		  ,a.[CostCardReposition]
		  ,cbp.[IsDefault]
		  ,e.[TimeZone]
		  ,CAST(ISNULL(a.[TransactionsOffline], 0) AS BIT) [TransactionsOffline]
		  ,CAST(ISNULL((SELECT [Value] 
		  			    FROM [General].[ParametersByCustomer]
		  			    WHERE [CustomerId] = @pCustomerId
		  			    AND [ResuourceKey] = 'COSTCENTERBYDRIVER'), 0) AS BIT) [CostCenterByDriver]
		  ,(SELECT COUNT(1) 
			FROM [General].[Vehicles] cv
			WHERE cv.[CustomerId] = a.customerid			
			AND (cv.[IsDeleted] IS NULL  OR cv.[IsDeleted] = 0)
			AND cv.[Active] = 1) [VehicleCant]
		  ,(SELECT COUNT(1) 
			FROM [General].[Vehicles] cv
			WHERE cv.[CustomerId] = a.customerid			
			AND cv.[IsDeleted] <> 1 
			AND cv.[Active] = 1
			AND (cv.[DeviceReference] > 0 AND cv.[DeviceReference] IS NOT NULL) 
			AND (cv.[IntrackReference] > 0 AND cv.[IntrackReference] IS NOT NULL)) [GPSCant]
		  ,(SELECT COUNT(1) 
		    FROM [Control].[CreditCard] 
		    WHERE [CustomerId] = a.[CustomerId] 
		    AND [StatusId] IN (7,8)) [CreditCardCant]
		  ,a.[isDemo]
		  ,a.[CustomerManagerId]
		  ,(SELECT [DateAlert]
		    FROM [General].[CustomerDemoAlert]
			WHERE [CustomerId] = a.[CustomerId]) [IsDemoAlert]
		  ,CAST(CASE WHEN (SELECT [DateAlert]
						   FROM [General].[CustomerDemoAlert]
						   WHERE [CustomerId] = a.[CustomerId]) < GETDATE() 
						   AND a.[isDemo] = 1
					 THEN 1
					 ELSE 0 
				END AS BIT) [ShowDemoAlert]
		  ,@lXmlManagementResult [ManagementPricesXml]
		  ,@lXmlSMSResult [SMSPricesXml]
		  ,[SMSAlarms]
		  ,@lManagementPriceId [ManagementPriceId]
		  ,@lSMSPriceId [SMSPriceId]
		  ,(SELECT [CustomerCharges]
			FROM [General].[CustomerChargesByCustomer]
			WHERE [CustomerId] = @pCustomerId) [CustomerCharges]
		  ,CAST(ISNULL([SMSAlarms], 0) AS BIT)[SMSAlarms]
		  --Get Additional Information
		  ,ai.[Id][AdditionalId]	      
	      ,ai.[Address]
	      ,ai.[LegalDocument]
		  ,ai.[BusinessName]
		  ,ai.[ApplicantClient]
		  ,ai.[SupportTicket]
		  ,ai.[SupportTicketURL]
		  ,ai.[MainPhone]
		  ,ai.[SecundaryPhone]
		  ,ai.[StateId]
		  ,ai.[CityId]
		  ,ai.[CountyId]
		  ,ai.[ContactName]
		  ,ai.[FinancialContact]
		  ,ai.[FinancialEmail]
		  ,ai.[RegisteredOwner]
		  ,ai.[Email]
		  ,ai.[Latitude]
		  ,ai.[Longitude]
		  ,ai.[ZoneId]	
		  ,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'TypeOfDistribution'
						AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
					), 0) AS INT) [TypeBudgetDistribution]
		  ,CAST(ISNULL(a.[CostCenterFuelDistribution], 0) AS BIT) [CostCenterFuelDistribution]
		  ,a.SapCode
		  ,a.FuelPermit
    FROM [General].[Customers] a
	INNER JOIN [Control].[Currencies] b
		ON a.[CurrencyId] = b.[CurrencyId]
	INNER JOIN [General].[CustomersByPartner] cbp
		ON a.[CustomerId] = cbp.[CustomerId]
	INNER JOIN [General].[Countries] e
		ON a.[CountryId] = e.[CountryId]
	LEFT JOIN [General].[Types] c
		ON c.[TypeId] = a.[IssueForId]
	INNER JOIN [General].[Types] d
	    ON d.[TypeId] = a.[UnitOfCapacityId]
	LEFT JOIN [General].[CustomerAdditionalInformation] ai
		ON ai.[CustomerId] = a.[CustomerId]
	LEFT JOIN [General].[CollectionZones] z
		ON ai.[ZoneId] = z.[ZoneId]
	WHERE a.[IsDeleted] <> 1
	AND (@pPartnerId IS NULL OR cbp.[PartnerId] = @pPartnerId)
	AND (@pCountryId IS NULL OR a.[CountryId] = @pCountryId)
	AND (@pCustomerId IS NULL OR a.[CustomerId] = @pCustomerId)
	AND (@ActiveVehicleSearch = 0 OR a.[CustomerId] IN (SELECT DISTINCT CustomerId FROM @tblCustomerByVehicle))	
	
		
    SET NOCOUNT OFF
END

