/****** Object:  StoredProcedure [General].[Sp_VehicleCategories_AddOrEdit]    Script Date: 17/02/2020 5:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	-- ================================================================================================
	-- Author:		Berman Romero L.
	-- Create date: 09/11/2014
	-- Description:	Insert or Update VehicleCategory information
	-- Modify By:	Marco Cabrera
	-- Description: When the customer has the Capacity Unit in galons then calculate the amount in liters
	-- ================================================================================================
	ALTER PROCEDURE [General].[Sp_VehicleCategories_AddOrEdit]
	(
		 @pVehicleCategoryId INT = NULL
		,@pManufacturer VARCHAR(250)
		,@pType VARCHAR(250)
		,@pDefaultFuelId INT
		,@pLiters INT
		,@pVehicleModel VARCHAR(250)
		,@pCustomerId INT
		,@pIcon VARCHAR(MAX) = NULL
		,@pMaximumSpeed INT = NULL
		,@pMaximumRPM INT = NULL
		,@pYear INT = NULL
		,@pWeight NUMERIC(16,2) = NULL
		,@pLoadType INT = NULL
		,@pCylinderCapacity INT = NULL
		,@pFuelsListXml VARCHAR(MAX) = NULL
		,@pLoggedUserId INT
		,@pRowVersion TIMESTAMP
		,@pDefaultPerformance DECIMAL(16, 2) 
		,@pSpeedDelimited int
	)
	AS
	BEGIN
	
		SET NOCOUNT ON
		SET XACT_ABORT ON
    
		BEGIN TRY
				DECLARE @lErrorMessage NVARCHAR(4000)
				DECLARE @lErrorSeverity INT
				DECLARE @lErrorState INT
				DECLARE @lLocalTran BIT = 0
				DECLARE @lRowCount INT = 0
				DECLARE @lxmlData XML = CONVERT(XML,@pFuelsListXml)
				DECLARE @lCapacityUnitId INT

				DECLARE @lClosingDate DATETIME = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)

				DECLARE @lTimeZoneParameter INT = (SELECT [TimeZone] 
												   FROM [General].[Countries] co 
												   INNER JOIN [General].[Customers] cu 
												   ON co.[CountryId] = cu.[CountryId]
												   WHERE cu.[CustomerId] = @pCustomerId)

				SELECT @lCapacityUnitId = UnitOfCapacityId 
				FROM General.Customers
				WHERE CustomerId = @pCustomerId

				IF (@lCapacityUnitId = 1)
					SET @pLiters = @pLiters * 3.78541178
			
				IF (@pVehicleCategoryId IS NULL)
				BEGIN
					INSERT INTO [General].[VehicleCategories]
					(
						[Manufacturer]
					   ,[Type]
					   ,[DefaultFuelId]
					   ,[Liters]
					   ,[VehicleModel]
					   ,[CustomerId]
					   ,[Icon]
					   ,[MaximumSpeed]
					   ,[MaximumRPM]
					   ,[Year]
					   ,[Weight]
					   ,[LoadType]
					   ,[CylinderCapacity]
					   ,[InsertDate]
					   ,[InsertUserId]
					   ,DefaultPerformance
					   ,SpeedDelimited
					)
					VALUES	
					(
						@pManufacturer
					   ,@pType
					   ,@pDefaultFuelId
					   ,@pLiters
					   ,@pVehicleModel
					   ,@pCustomerId
					   ,@pIcon
					   ,@pMaximumSpeed
					   ,@pMaximumRPM
					   ,@pYear
					   ,@pWeight
					   ,@pLoadType
					   ,@pCylinderCapacity
					   ,GETUTCDATE()
					   ,@pLoggedUserId
					   ,@pDefaultPerformance
					   ,@pSpeedDelimited
					)
						
					SET @pVehicleCategoryId = SCOPE_IDENTITY()
				END
				ELSE
				BEGIN
					UPDATE [General].[VehicleCategories]
						SET  [Manufacturer] = @pManufacturer
							,[Type] = @pType
							,[DefaultFuelId] = @pDefaultFuelId
							,[Liters] = @pLiters
							,[VehicleModel] = @pVehicleModel
							,[CustomerId] = @pCustomerId
							,[Icon] = @pIcon
							,[MaximumSpeed] = @pMaximumSpeed
							,[MaximumRPM] = @pMaximumRPM
							,[Year] = @pYear
							,[Weight] = @pWeight
							,[LoadType] = @pLoadType
							,[CylinderCapacity] = @pCylinderCapacity
							,[ModifyDate] = GETUTCDATE()
							,[ModifyUserId] = @pLoggedUserId
							,DefaultPerformance=@pDefaultPerformance
							,SpeedDelimited=@pSpeedDelimited
					WHERE [VehicleCategoryId] = @pVehicleCategoryId
					  --AND [RowVersion] = @pRowVersion
				END
            
				SET @lRowCount = @@ROWCOUNT
            
				DELETE FROM [General].[FuelsByVehicleCategory]
				WHERE [VehicleCategoryId] = @pVehicleCategoryId
	        
				INSERT INTO [General].[FuelsByVehicleCategory]
				(
					[VehicleCategoryId]
				   ,[FuelId]
				   ,[InsertDate]
				   ,[InsertUserId]
				)
				SELECT 
					 @pVehicleCategoryId
					,RowData.col.value('./@FuelId', 'INT') AS [FuelId]
					,GETUTCDATE()
					,@pLoggedUserId
				FROM @lxmlData.nodes('/xmldata/Fuel') RowData(col)					
				
				SELECT cc.[CreditCardId]
				      ,(SUM([Amount]) + SUM([AdditionalAmount])) - [Control].[Fn_FuelDistribution_TransactionsAmount](cc.[CreditCardId], @lTimeZoneParameter, @lClosingDate, NULL, NULL) [AmountAvailable]
					  ,(SUM([Liters]) + SUM([AdditionalLiters])) - [Control].[Fn_FuelDistribution_TransactionsLiters](cc.[CreditCardId], @lTimeZoneParameter, @lClosingDate, NULL, 1, NULL) [LitersAvailable]
				INTO #Available
				FROM [Control].[CreditCard] cc
				INNER JOIN [Control].[CreditCardByVehicle] ccv
					ON ccv.[CreditCardId] = cc.[CreditCardId]
				INNER JOIN [General].[Vehicles] v
					ON v.[VehicleId] = ccv.[VehicleId]
				INNER JOIN (SELECT vc.[DefaultFuelId] [FuelId]
									      ,v.[VehicleId]
										  ,vc.[VehicleModel]
										  ,vc.[VehicleCategoryId]
									FROM [General].[Vehicles] v	
									INNER JOIN [General].[VehicleCategories] vc 
										ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
									INNER JOIN [Control].[Fuels] f	
										ON f.[FuelId] = vc.[DefaultFuelId]
									WHERE v.[CustomerId] = @pCustomerId
									UNION
									SELECT fvc.[FuelId] 
									      ,v.[VehicleId]
										  ,vc.[VehicleModel]
										  ,fvc.[VehicleCategoryId]
									FROM [General].[Vehicles] v
									INNER JOIN [General].[VehicleCategories] vc
										ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
									INNER JOIN [General].[FuelsByVehicleCategory] fvc
										ON fvc.[VehicleCategoryId] = vc.[VehicleCategoryId]
									INNER JOIN [Control].[Fuels] f	
										ON f.[FuelId] = fvc.[FuelId]
									WHERE v.[CustomerId] = @pCustomerId) vc
					ON vc.[VehicleId] = v.[VehicleId]
				LEFT JOIN [Control].[FuelDistribution] fd
					ON fd.[VehicleId] = v.[VehicleId]
					AND fd.[FuelId] = vc.[FuelId]
					AND fd.[InsertDate] >= @lClosingDate 
				WHERE vc.[VehicleCategoryId] = @pVehicleCategoryId
				AND cc.[StatusId] = 7
				AND v.[Active] = 1
				AND (v.[IsDeleted] IS NULL
					 OR v.[IsDeleted] = 0)
				GROUP BY cc.[CreditCardId]

				UPDATE cc
				SET [CreditAvailable] = [AmountAvailable]
				   ,[AvailableLiters] = [LitersAvailable]
				FROM [Control].[CreditCard] cc
				INNER JOIN #Available a
					ON cc.[CreditCardId] = a.[CreditCardId]

				SELECT vc.[FuelId], ISNULL((SUM([Amount]) + SUM([AdditionalAmount])), 0) [FuelDistribution]
				INTO #Fuels
				FROM [Control].[CreditCard] cc
				INNER JOIN [Control].[CreditCardByVehicle] ccv
					ON ccv.[CreditCardId] = cc.[CreditCardId]
				INNER JOIN [General].[Vehicles] v
					ON v.[VehicleId] = ccv.[VehicleId]
				INNER JOIN (SELECT vc.[DefaultFuelId] [FuelId]
									      ,v.[VehicleId]
										  ,vc.[VehicleModel]
										  ,vc.[VehicleCategoryId]
									FROM [General].[Vehicles] v	
									INNER JOIN [General].[VehicleCategories] vc 
										ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
									INNER JOIN [Control].[Fuels] f	
										ON f.[FuelId] = vc.[DefaultFuelId]
									WHERE v.[CustomerId] = @pCustomerId
									UNION
									SELECT fvc.[FuelId] 
									      ,v.[VehicleId]
										  ,vc.[VehicleModel]
										  ,fvc.[VehicleCategoryId]
									FROM [General].[Vehicles] v
									INNER JOIN [General].[VehicleCategories] vc
										ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
									INNER JOIN [General].[FuelsByVehicleCategory] fvc
										ON fvc.[VehicleCategoryId] = vc.[VehicleCategoryId]
									INNER JOIN [Control].[Fuels] f	
										ON f.[FuelId] = fvc.[FuelId]
									WHERE v.[CustomerId] = @pCustomerId) vc
					ON vc.[VehicleId] = v.[VehicleId]
				LEFT JOIN [Control].[FuelDistribution] fd
					ON fd.[VehicleId] = v.[VehicleId]
					AND fd.[FuelId] = vc.[FuelId]
					AND fd.[InsertDate] >= @lClosingDate 
				WHERE cc.[StatusId] = 7
				AND cc.[CustomerId] = @pCustomerId
				GROUP BY vc.[FuelId]

				UPDATE fc
				SET [Assigned] = [FuelDistribution]
				FROM [Control].[FuelsByCredit] fc
				INNER JOIN #Fuels f
					ON f.[FuelId] = fc.[FuelId]
				WHERE fc.[CustomerId] = @pCustomerId
				      AND fc.[InsertDate] >= @lClosingDate

		END TRY
		BEGIN CATCH	

			SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
			RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
		END CATCH
        
		SET NOCOUNT OFF
		SET XACT_ABORT OFF
	END