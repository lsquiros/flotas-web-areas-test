/****** Object:  StoredProcedure [General].[Sp_FuelPrice_Retrieve]    Script Date: 2/7/2020 11:16:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Stefano Quir�s Ruiz
-- Create date: 07/02/2020
-- Description:	Retrieve Multiple Fuels Prices information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_FuelMultiplePrices_Retrieve]
(
	 @pVehicleId INT,
	 @pTerminalId VARCHAR(50) = NULL
)
AS
BEGIN
	SET NOCOUNT ON	
	
	DECLARE @lFuelId INT
	DECLARE @pLiterPrice DECIMAL(16, 2)
	DECLARE @lPartnerCapacityValue INT 
	DECLARE @lConvertionValue DECIMAL (16, 13)= 3.78541	

	SELECT @lPartnerCapacityValue = [CapacityUnitId]
	FROM [General].[Partners] p
	INNER JOIN [General].[CustomersByPartner] cp
		ON p.[PartnerId] = cp.[PartnerId]
	INNER JOIN [General].[Vehicles] v
		ON v.[CustomerId] = cp.[CustomerId]
	WHERE v.[VehicleId] = @pVehicleId	

	CREATE TABLE #Fuels
	(
		[FuelId] INT
	   ,[Name] VARCHAR(100)
	)

	CREATE TABLE #Result
	(
		[FuelId] INT
	   ,[Price] DECIMAL(16,2)		
	)

	INSERT INTO #Fuels
	SELECT vc.[DefaultFuelId]
	      ,f.[Name]
	FROM [General].[Vehicles] v	
	INNER JOIN [General].[VehicleCategories] vc 
		ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
	INNER JOIN [Control].[Fuels] f	
		ON f.[FuelId] = vc.[DefaultFuelId]
	WHERE v.[VehicleId] = @pVehicleId 
	UNION
	SELECT fvc.[FuelId]
	      ,f.[Name]
	FROM [General].[Vehicles] v
	INNER JOIN [General].[VehicleCategories] vc
		ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
	INNER JOIN [General].[FuelsByVehicleCategory] fvc
		ON fvc.[VehicleCategoryId] = vc.[VehicleCategoryId]
	INNER JOIN [Control].[Fuels] f	
		ON f.[FuelId] = fvc.[FuelId]
    WHERE v.[VehicleId] = @pVehicleId

	INSERT INTO #Result
	SELECT DISTINCT fs.[FuelId]
				   ,[Price]
	FROM [General].[ServiceStations] s
	INNER JOIN [Stations].[FuelPriceByServiceStations] fs
		ON s.[ServiceStationId] = fs.[ServiceStationId]
	INNER JOIN [General].[TerminalsByServiceStations] ts
		ON s.[ServiceStationId] = ts.[ServiceStationId]
	WHERE ts.[TerminalId] = @pTerminalId
		AND fs.[FuelId] IN (SELECT [FuelId] FROM #Fuels)
		AND fs.[EndDate] IS NULL
	    AND fs.[Price] > 0
		AND (s.[IsDeleted] IS NULL OR s.[IsDeleted] = 0)

	IF EXISTS(SELECT * FROM #Result) AND @pTerminalId IS NOT NULL
	BEGIN
		IF @lPartnerCapacityValue = 1
		BEGIN
			SET @pLiterPrice = @pLiterPrice * @lConvertionValue
		END

		SELECT f.[Name] [FuelName]
		      ,r.[Price] [FuelPrice] 
		FROM #Result r
		INNER JOIN #Fuels f
			ON f.[FuelId] = r.[FuelId]
	END
	ELSE
	BEGIN
		INSERT INTO #Result
		SELECT DISTINCT c.[FuelId]
				       ,[LiterPrice]
		FROM [Control].[PartnerFuel] c
		INNER JOIN [General].[CustomersByPartner] 	cp
		  ON c.[PartnerId] = cp.[PartnerId]
		INNER JOIN [General].[Vehicles]	v
		  ON cp.[CustomerId] = v.[CustomerId]
		WHERE c.[FuelId] IN (SELECT [FuelId] FROM #Fuels)
		AND v.[VehicleId] = @pVehicleId
		AND c.[ScheduledId] IS NULL
		AND c.[EndDate] > GETDATE()
		AND c.[History] IS NULL

		IF @lPartnerCapacityValue = 1
		BEGIN
			SET @pLiterPrice = @pLiterPrice * @lConvertionValue
		END
		
		SELECT f.[Name] [FuelName]
		      ,r.[Price] [FuelPrice] 
	    FROM #Result r
		INNER JOIN #Fuels f
			ON f.[FuelId] = r.[FuelId]
	END

    SET NOCOUNT OFF
END

