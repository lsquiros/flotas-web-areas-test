/****** Object:  UserDefinedFunction [Control].[Fn_FuelDistribution_TransactionsAmount]    Script Date: 2/17/2020 4:18:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:		Stefano Quirós
-- Create date: 08-03-2018
-- Description:	Get fuel amount of the transactions
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- =================================================================

ALTER FUNCTION [Control].[Fn_FuelDistribution_TransactionsAmount] 
(
	 @pCreditCardId INT = NULL
	,@pTimeZoneParameter INT = NULL
	,@pInsertDate DATE = NULL
	,@pFinalDate DATE = NULL
	,@pFuelId INT = NULL
)
RETURNS DECIMAL(16, 6)
AS
BEGIN

	DECLARE @RealAmount DECIMAL(16, 6) = 0	
	
	SET @RealAmount = ISNULL((
				SELECT SUM(x.[FuelAmount])
				FROM [Control].[Transactions] x
				WHERE x.[CreditCardId] = @pCreditCardId
					AND x.[IsFloating] = 0
					AND x.[IsReversed] = 0
					AND x.[IsDuplicated] = 0
					AND (x.[IsDenied] IS NULL OR x.[IsDenied] = 0)
					AND 1 > (
						SELECT COUNT(1)
						FROM CONTROL.Transactions t2
						WHERE t2.[CreditCardId] = x.[CreditCardId]
							AND t2.[TransactionPOS] = x.[TransactionPOS]
							AND t2.[ProcessorId] = x.[ProcessorId]
							AND (
								t2.IsReversed = 1
								OR t2.IsVoid = 1
								)
						)
					--NUEVO FILTRO DONDE LA FECHA DE INSERCION SEA IGUAL A LA FECHA DEL ULTIMO CIERRE  
					AND ((@pFinalDate IS NOT NULL
							AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) >= @pInsertDate
							AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) <= @pFinalDate)
						 OR (@pFinalDate IS NULL AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) >= @pInsertDate))
					AND (@pFuelId IS NULL
						 OR x.[FuelId] = @pFuelId)
				), 0.0)

	RETURN @RealAmount;

END

