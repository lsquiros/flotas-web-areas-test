/****** Object:  StoredProcedure [Control].[Sp_MonthlyClosing]    Script Date: 2/17/2020 2:33:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author: Berman Romero
-- Create date: 16/06/2015
-- Description: Execute tasks of the Monthly Closing
-- =============================================================================================
-- Modify by Gerald Solano - 23/12/2015 - Change the execute process Monthly Closing for each customer
-- Modify by Danilo Hidalgo - 20/04/2016 - Adding management for additional amount in the closing month
-- Modify by Melvin Salas - AUG/09/2016 - PULL PREVIOUS BUDGET REVISION
-- Modify by Gerald Solano - SEPT/06/2016 - FIX: RESTORED AVAILABLE AMOUNT WHEN THE CREDIT EXTRA IS NEGATIVE
-- Modify by Gerald Solano - DIC/02/2016 - FIX: VALIDATE NULL VALUES IN THE COLUMN CREDITAVAILABLE IN THE TABLE CREDITCARD 
-- Modify by Stefano Quirós - FEB/09/2017 - Add to process of MonthlyClose the CreditCards assigned to the drivers and add
-- the vehicle delete filter to update FuelsByCredit table
-- Modify by Stefano Quirós - NOV/10/2017 - Update the logic to manage the periodic closing budget for the clients and add
-- last execution date to the table
-- Modify by Gerald Solano - ABR/26/2018 - For driver distrubution, get the last record applyed in the month
-- Modify by Stefano Quirós - 13/06/2018 - Delete filter is active on the fuel distribution
-- Modify by María de los Ángeles Jiménez Chvarría - DIC/24/2018 - Add credit card available liters.
-- Stefano Quiros - Delete Begin, Commit and Rollback Transactions - 05/02/2019
-- ==============================================================================================
ALTER PROCEDURE [Control].[Sp_MonthlyClosing]
AS 
BEGIN
	SET NOCOUNT ON
    
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0 
        DECLARE @lCurrentMonth INT
        DECLARE @lCurrentYear INT
        DECLARE @lClosingMonth INT
        DECLARE @lClosingYear INT
        DECLARE @lProcessDate DATETIME		
        DECLARE @lCount INT	

		DECLARE @To NVARCHAR(500) = 'soporte@bacflota.com' --'monthly_closing_test@yopmail.com'--
		DECLARE @From NVARCHAR(100) = 'intrack-eco@coinca.tv'
		DECLARE @Subject NVARCHAR(300) = NULL
		DECLARE @Message NVARCHAR(1000) = NULL
		DECLARE @tmpCustomersToProcess TABLE( CustomerId INT ) -- CLIENTES A PROCESAR

		--Creamos la tabla para manejar la logica de los montos por combustible del cliente, 
		--se borra por cada cliente que analiza
		CREATE TABLE #TotalAmountPerFuel
		(
			[FuelId] INT
		   ,[Amount] DECIMAL
		   ,[CustomerId] INT
		   ,[CostCenterId] INT
		)
	
		DECLARE @CustomerId INT = 0
		DECLARE @ONLY_ONE_Customer BIT = 0 -- 1 = SE REALIZA EL PROCESO SOLO A UN CUSTOMER
		DECLARE @DELETE_DATA_Customer BIT = 0 -- 1 = SE REALIZA EL PROCESO DE BORRADO DEL MES ACTUAL

		-- VALIDA EL MES AL CUAL RESTAURAR LOS DISPONIBLES
		Set @lProcessDate = GETDATE()    --'2015-11-01'--
		Set @lCurrentMonth = DATEPART(MM, @lProcessDate)
		Set @lCurrentYear = DATEPART(YY, @lProcessDate)

		-- OBTENEMOS EL MES ANTERIOR CON BASE AL ACTUAL
		IF @lCurrentMonth = 1
		BEGIN
			Set @lClosingMonth = 12
			Set @lClosingYear = @lCurrentYear - 1
		END
		ELSE
		BEGIN
			Set @lClosingMonth = @lCurrentMonth - 1
			Set @lClosingYear = @lCurrentYear
		END	 

		-- VALIDA SI EL PROCESO ES PARA MÁS DE UN CUSTOMER
		IF(@ONLY_ONE_Customer IS NULL OR @ONLY_ONE_Customer = 0)
		BEGIN
			
			-- SELECCIONAMOS LOS CLIENTES QUE TIENEN EN SU CONFIGURACIÓN PARA QUE REINICIE EL PRESUPUESTO EL DÍA QUE ESTÁ CORRIENDO EL CIERRE
			INSERT INTO @tmpCustomersToProcess
			EXEC [Control].[Sp_ClosurePeriod_CustomersRetrieve]

			SELECT @CustomerId = MIN([CustomerId]) 
			FROM @tmpCustomersToProcess
		END

		-- RECORRE UNO O MÁS CUSTOMERS
		WHILE @CustomerId IS NOT NULL   -- INICIA WHILE CUSTOMERS
		BEGIN 					
			BEGIN TRY -- INICIA TRY DE UN SOLO CLIENTE POR RECORRIDO
				
				DECLARE @IsIncomplete INT = 0 -- BANDERA PARA INDICAR SI EL PROCESO DE CIERRE MENSUAL DE UNA SOLA EMPRESA ESTA INCOMPLETO
				DECLARE @lPreviousClosingDate DATETIME 

				SELECT TOP 1 @lPreviousClosingDate = [InsertDate] 
				FROM [Control].[CustomerCredits] 
				WHERE [InsertDate] < @lProcessDate 
				AND [CustomerId] = @CustomerId
				ORDER BY [InsertDate] DESC	

				-- PROCESSING THE NEW DATA FOR THE CURRENT MONTH ------------------------------------

				--Copy CustomerCredit data for the Current Month		
				Insert into [Control].[CustomerCredits]
				(
					[CustomerId]
				   ,[Year]
				   ,[Month]
				   ,[CreditAmount]
				   ,[CreditAssigned]
				   ,[CreditTypeId]
				   ,[InsertDate]
				)
				SELECT TOP 1 [CustomerId]
							,@lCurrentYear
							,@lCurrentMonth
							,[CreditAmount]
							,0
							,[CreditTypeId]
							,@lProcessDate
				FROM [Control].[CustomerCredits] 
				WHERE [CustomerId] = @CustomerId
				ORDER BY [InsertDate] DESC
				
				--Copy CustomerCreditByCostCenter data for the Current Month		
				Insert into [Control].[CustomerCreditsByCostCenter]
				(
					[CustomerId]
				   ,[CostCenterId]
				   ,[Year]
				   ,[Month]
				   ,[TotalAmount]
				   ,[AssignedAmount]
				   ,[AvailableAmount]
				   ,[InsertDate]
				)
				SELECT [CustomerId]
					  ,[CostCenterId]
					  ,@lCurrentYear
					  ,@lCurrentMonth
					  ,[TotalAmount]
					  ,0
					  ,0
					  ,@lProcessDate
				FROM [Control].[CustomerCreditsByCostCenter] 
				WHERE [CustomerId] = @CustomerId
				AND [InsertDate] >= @lPreviousClosingDate
				ORDER BY [InsertDate] DESC


				IF NOT EXISTS (SELECT TOP 1 C.CustomerId FROM Control.CustomerCredits C	WHERE C.CustomerId = @CustomerId)
				BEGIN
					--There is a Card but no record in CustomerCredit
					INSERT INTO [Control].[CustomerCredits]
					(
						[CustomerId]
					   ,[Year]
					   ,[Month]
					   ,[CreditAmount]
					   ,[CreditAssigned]
					   ,[CreditTypeId]
					   ,[InsertDate]
					)
					SELECT CCC.[CustomerId]
						  ,@lCurrentYear
						  ,@lCurrentMonth
						  ,CCC.[CreditLimit]
						  ,0
						  ,1000
						  ,@lProcessDate
						FROM [General].[CustomerCreditCards] CCC 
						INNER JOIN (SELECT CustomerID, MAX(CustomerCreditCardId) [CustomerCreditCardId] 
									FROM General.CustomerCreditCards
									WHERE StatusId IN (100, 101)
									GROUP BY CustomerID) CLast
							ON CCC.CustomerId = CLast.CustomerId						
						WHERE CLast.CustomerID = @CustomerId
						AND [InternationalCard] = 0
						ORDER BY [InsertDate] DESC
					-----------------------------------------------------
				END					
			
				IF(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se envía notificación
				BEGIN
					SET @IsIncomplete = 1
					SET @Message = 'La copia de Customer Credit (Mes anterior) al Cliente con [#'+ CAST(@CustomerId AS varchar(20)) +'] - NO se Actualizó'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'
				END
				ELSE BEGIN
					SET @Message = 'La copia de Customer Credit (Mes anterior) al Cliente con [#'+ CAST(@CustomerId AS varchar(20)) +'] - Se Actualizó Exitosamente'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
				END 
				-------------------------------------------------					
				
				  
				--Copy FuelsByCredit data for the Current Month
				INSERT INTO [Control].[FuelsByCredit]
				(
					[CustomerId]
				   ,[FuelId]
				   ,[Month]
				   ,[Year]
				   ,[Total]
				   ,[Assigned]
				   ,[InsertDate]
				)
				--Se agregó un top para traer solamente los ultimos registros de la cantidad de combustibles del cliente

				SELECT @CustomerId
					   ,b.[FuelId]
					   ,@lCurrentMonth
					   ,@lCurrentYear
					   ,ISNULL(MAX([Total]), 0) [Total]
					   ,0
					   ,@lProcessDate
				FROM [Control].[Fuels] b
				INNER JOIN [General].[Customers] c
					ON c.[CountryId] = b.[CountryId]
				LEFT JOIN [Control].[FuelsByCredit] a
					ON a.[FuelId] = b.[FuelId]
					AND a.[InsertDate] >= @lPreviousClosingDate
					AND a.[CustomerId] = @CustomerId	
				WHERE c.[CustomerId] = @CustomerId
				GROUP BY b.[FuelId]
				ORDER BY b.[FuelId]		
				
				--Copy [Control].[FuelsByCreditByCostCenter] data for the Current Month
				INSERT INTO [Control].[FuelsByCreditByCostCenter]
				(
					[CustomerId]
				   ,[CustomerCreditByCostCenterId]
				   ,[CostCenterId]
				   ,[FuelId]
				   ,[Month]
				   ,[Year]
				   ,[TotalAmount]
				   ,[TotalLiters]
				   ,[AssignedAmount]
				   ,[AssignedLiters]
				   ,[AvailableAmount]
				   ,[AvailableLiters]
				   ,[InsertDate]
				)
				SELECT @CustomerId,
					   (SELECT TOP 1 [Id] 
					    FROM [Control].[CustomerCreditsByCostCenter] ccc 
					    WHERE ccc.[CostCenterId] = a.[CostCenterId] 
						AND ccc.[InsertDate] >= @lPreviousClosingDate
						ORDER BY [InsertDate] DESC) [CustomerCreditByCostCenter]
					   ,[CostCenterId]
					   ,b.[FuelId]
					   ,@lCurrentMonth
					   ,@lCurrentYear
					   ,ISNULL(MAX([TotalAmount]), 0) [TotalAmount]
					   ,ISNULL(MAX([TotalLiters]), 0) [TotalLiters]
					   ,0
					   ,0
					   ,0
					   ,0
					   ,@lProcessDate
				FROM [Control].[Fuels] b
				INNER JOIN [General].[Customers] c
					ON c.[CountryId] = b.[CountryId]
				LEFT JOIN [Control].[FuelsByCreditByCostCenter] a
					ON a.[FuelId] = b.[FuelId]
					AND a.[InsertDate] >= @lPreviousClosingDate
					AND a.[CustomerId] = @CustomerId	
				WHERE c.[CustomerId] = @CustomerId
				GROUP BY b.[FuelId], a.[CostCenterId]
				ORDER BY b.[FuelId]	
						

				if(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se envía notificación
				BEGIN
					SET @IsIncomplete = 1
					SET @Message = 'NO hubo actualización de Credito de Combustible para Cliente con [#'+ CAST(@CustomerId AS varchar(20)) + ']'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'
				END
				ELSE BEGIN
					SET @Message = 'Se procesó correctamente la actualización de Credito de Combustible para Cliente con [#'+ CAST(@CustomerId AS varchar(20)) + ']'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
				END 
				----------------------------------------------------------

				--Verificamos si el Cliente está configurada la emisión de tarjetas por vehículo o por conductor

				DECLARE @lCreditCardEmissionType INT = 0

				SELECT @lCreditCardEmissionType = IssueForId 
												  FROM [General].[Customers]
												  WHERE [CustomerId] = @CustomerId

				--Variables used on the two process (ByVehicle and ByDriver)
				DECLARE @IndexYear INT = 0				
				DECLARE @wIndex int = 0 --RECORRE LOS INDICES DE LA TABLA TEMPORAL
				DECLARE @currentCCId int = 0
				DECLARE @currentCCLimit numeric (16,2) = 0
				DECLARE @updateCCAmount numeric (16,2) = 0
				DECLARE	@pullPreviousBudgetCount INT = 0
				DECLARE @previousBudget NUMERIC(16,2) = 0
				DECLARE @previousCreditExtra NUMERIC(16,2) = 0 -- GSOLANO


	            --Inicio del proceso si el cliente tiene las tarjetas ligadas a los vehículoS.
				IF(@lCreditCardEmissionType = 101)
				BEGIN
					INSERT INTO [Control].[FuelDistribution]
					(
						[VehicleId]
					   ,[Month]
					   ,[Year]
					   ,[Liters]
					   ,[Amount]
					   ,[AdditionalAmount]
					   ,[AdditionalLiters]
					   ,[InsertDate]
					   ,[FuelId]
					)
					SELECT fd.VehicleId
						  ,@lCurrentMonth
						  ,@lCurrentYear
						  ,MAX(fd.Liters)
						  ,MAX(fd.[Amount])
						  ,0
						  ,0
						  ,@lProcessDate
						  ,fd.[FuelId]
					FROM [Control].[FuelDistribution] fd
					INNER JOIN [General].[Vehicles] v 
						ON fd.VehicleId = v.VehicleId
					WHERE v.[CustomerId] = @CustomerId  					
					AND fd.[InsertDate] >= @lPreviousClosingDate
					AND (v.[IsDeleted] = 0 OR v.[IsDeleted] IS NULL) 
					GROUP BY fd.[VehicleId], fd.[FuelId]

					INSERT INTO #TotalAmountPerFuel
					SELECT vc.[FuelId]
					      ,ISNULL((SUM([Amount]) + SUM([AdditionalAmount])), 0) [Amount]
						  ,v.[CustomerId]
						  ,v.[CostCenterId]
					FROM [Control].[CreditCard] cc
					INNER JOIN [Control].[CreditCardByVehicle] ccv
						ON ccv.[CreditCardId] = cc.[CreditCardId]
					INNER JOIN [General].[Vehicles] v
						ON v.[VehicleId] = ccv.[VehicleId]
					INNER JOIN (SELECT vc.[DefaultFuelId] [FuelId]
										      ,v.[VehicleId]
											  ,vc.[VehicleModel]
											  ,vc.[VehicleCategoryId]
										FROM [General].[Vehicles] v	
										INNER JOIN [General].[VehicleCategories] vc 
											ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
										INNER JOIN [Control].[Fuels] f	
											ON f.[FuelId] = vc.[DefaultFuelId]
										WHERE v.[CustomerId] = @pCustomerId
										UNION
										SELECT fvc.[FuelId] 
										      ,v.[VehicleId]
											  ,vc.[VehicleModel]
											  ,fvc.[VehicleCategoryId]
										FROM [General].[Vehicles] v
										INNER JOIN [General].[VehicleCategories] vc
											ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
										INNER JOIN [General].[FuelsByVehicleCategory] fvc
											ON fvc.[VehicleCategoryId] = vc.[VehicleCategoryId]
										INNER JOIN [Control].[Fuels] f	
											ON f.[FuelId] = fvc.[FuelId]
										WHERE v.[CustomerId] = @pCustomerId) vc
						ON vc.[VehicleId] = v.[VehicleId]
					LEFT JOIN [Control].[FuelDistribution] fd
						ON fd.[VehicleId] = v.[VehicleId]
						AND fd.[FuelId] = vc.[FuelId]
						AND fd.[InsertDate] >= @lClosingDate 
					WHERE cc.[StatusId] = 7
					AND cc.[CustomerId] = @pCustomerId
					GROUP BY vc.[FuelId]


					--Update FuelsByCredit Assigned Amount   
					Update FC
						set FC.Assigned = VFG.Amount
						from Control.FuelsByCredit FC
						inner join (Select [CustomerId]
										  ,[DefaultFuelId]
										  ,Sum([Amount]) [Amount]
									FROM #TotalAmountPerFuel
									GROUP BY CustomerId, FuelId) VFG
							ON FC.CustomerId = VFG.CustomerId 
							   AND FC.FuelId = VFG.FuelId
						WHERE FC.CustomerId = @CustomerId
						AND FC.[InsertDate] >= @lProcessDate

						--Update FuelsByCredit Assigned Amount   
					Update FC
						set FC.AssignedAmount = VFG.Amount
						from Control.FuelsByCreditByCostCenter FC
						inner join (Select [CustomerId], [DefaultFuelId], Sum([Amount]) [Amount], [CostCenterId]
									from #TotalAmountPerFuel
									group by CustomerId, DefaultFuelId, [CostCenterId]) VFG
							on FC.CustomerId = VFG.CustomerId and FC.FuelId = VFG.DefaultFuelId AND fc.[CostCenterId] = vfg.[CostCenterId]
						where FC.CustomerId = @CustomerId
						AND FC.[InsertDate] >= @lProcessDate

					--Update Credit Assigned for CustomerCredit
					UPDATE C
						SET C.[CreditAssigned] = fcb.[Total] 
						FROM [Control].[CustomerCredits] c
						INNER JOIN (SELECT fc.[CustomerId], IsNull(SUM(fc.[Total]), 0) [Total] 
									FROM [Control].[FuelsByCredit] fc
									WHERE fc.[InsertDate] >= @lProcessDate
									AND fc.[CustomerId] = @CustomerId
									group by [CustomerId]) fcb
							ON c.[CustomerId] = fcb.[CustomerId]
						WHERE C.[InsertDate] >= @lProcessDate	
						
							--Update Credit Assigned for CustomerCredit
					UPDATE C
						SET C.[AssignedAmount] = fcb.[TotalAmount] 
						FROM [Control].[CustomerCreditsByCostCenter] c
						INNER JOIN (SELECT fc.[CustomerId], IsNull(SUM(fc.[TotalAmount]), 0) [TotalAmount], [CostCenterId]
									FROM [Control].[FuelsByCreditByCostCenter] fc
									WHERE fc.[InsertDate] >= @lProcessDate
									AND fc.[CustomerId] = @CustomerId
									group by [CustomerId], [CostCenterId]) fcb
							ON c.[CustomerId] = fcb.[CustomerId] AND c.[CostCenterId] = fcb.[CostCenterId]
						WHERE C.[InsertDate] >= @lProcessDate						

					if(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se envía notificación
					BEGIN
						SET @IsIncomplete = 1
						SET @Message = 'NO hubo actualización del Monto Asignado de Credito de Combustible para el Cliente con [#'+ CAST(@CustomerId AS varchar(20)) + ']'

						-- AGREGAMOS AL LOG DE BASE DE DATOS
						EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'
					END
					ELSE BEGIN
						SET @Message = 'Se procesó correctamente la actualización del Monto Asignado de Credito de Combustible al Cliente con [#'+ CAST(@CustomerId AS varchar(20)) + ']'

						-- AGREGAMOS AL LOG DE BASE DE DATOS
						EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
					END 
					------------------------------------------

					-- UPDATE CREDIT AVALIBLE ON CREADIT CARD WITH FUEL VEHICULE AMOUNT 
					-- AND ADD PREVIOUS BUDGET VALUE OR 0 IF DISABLE
					UPDATE fd					
					SET	fd.[AdditionalAmount] = CASE WHEN v.[PullPreviousBudget] = 1 AND (cc.[CreditAvailable] > 0)
													THEN ISNULL(cc.[CreditAvailable], 0)
													ELSE 0
												END,
						fd.[AdditionalLiters] = CASE WHEN v.[PullPreviousBudget] = 1 AND (cc.[AvailableLiters] > 0)
													THEN ISNULL(cc.[AvailableLiters], 0)
												ELSE 0
					END		
				    FROM [Control].[CreditCard] cc
					INNER JOIN [Control].[CreditCardByVehicle] ccv
						ON cc.[CreditCardId] = ccv.[CreditCardId]
					INNER JOIN [General].[Vehicles] v
						ON v.[VehicleId] = ccv.[VehicleId] 
					INNER JOIN [Control].[FuelDistribution] fd
						ON fd.[VehicleId] = v.[VehicleId]										   							
					WHERE v.CustomerId = @CustomerId 
					AND cc.[StatusId] IN (7, 8)
					AND fd.[InsertDate] >= @lProcessDate;

					
					UPDATE cc					
					SET	cc.[CreditAvailable] =	CASE WHEN v.[PullPreviousBudget] = 1 AND (cc.[CreditAvailable] > 0)
													THEN ISNULL(fd.[Amount], 0) + ISNULL(cc.[CreditAvailable], 0)
												ELSE ISNULL(fd.[Amount], 0)
												END,
						cc.[AvailableLiters] =	CASE WHEN v.[PullPreviousBudget] = 1 AND (cc.[AvailableLiters] > 0)
													THEN ISNULL(fd.[Liters], 0) + ISNULL(cc.[AvailableLiters], 0)
												ELSE ISNULL(fd.[Liters], 0)
												END
				    FROM [Control].[CreditCard] cc
					INNER JOIN [Control].[CreditCardByVehicle] ccv
						ON cc.[CreditCardId] = ccv.[CreditCardId]
					INNER JOIN [General].[Vehicles] v
						ON v.[VehicleId] = ccv.[VehicleId] 
					INNER JOIN [Control].[FuelDistribution] fd
						ON fd.[VehicleId] = v.[VehicleId]										   							
					WHERE v.CustomerId = @CustomerId 
					AND cc.[StatusId] IN (7, 8)
					AND fd.[InsertDate] >= @lProcessDate;
					
					if(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se envía notificación
					BEGIN
						SET @IsIncomplete = 1
						SET @Message = 'NO hubo actualización de Monto Asignado a las Tarjetas para el Cliente con [#'+ CAST(@CustomerId AS varchar(20)) + ']'

						-- AGREGAMOS AL LOG DE BASE DE DATOS
						EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'

						
					END
					ELSE BEGIN
						SET @Message = 'Se procesó correctamente la actualización del Monto Asignado a las Tarjetas para el Cliente con [#'+ CAST(@CustomerId AS varchar(20)) + ']'

						-- AGREGAMOS AL LOG DE BASE DE DATOS
						EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
					END 
					--------------------------------------------------

					-- SI EL PROCESO HA SIDO INCOMPLETO PARA UN CLIENTE SE ENVIARA UNA NOTIFICACION
					IF(@IsIncomplete = 1)
					BEGIN 
						SET @Subject = '[Monthly_Closing_Incomplete] Cliente #' + CAST(@CustomerId AS varchar(20))
						SET @Message = '[Monthly_Closing] Cliente #'+ CAST(@CustomerId AS varchar(20))  +' con problemas en el proceso de inicio de Mes.'
						SET @Message = @Message + ' [' + CAST(GETUTCDATE() AS VARCHAR(25)) +']'

						--SELECT @Message

						-- AGREGAMOS AL LOG DE BASE DE DATOS
						EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'E'

						-- ENVIAMOS LA NOTIFICACION
						Exec General.Sp_Email_AddOrEdit null, @To, null, null, @Subject, @Message, 0, 0, null, null 
					END
					--///////////////////////////////////////////////////////////////////////////
				END --Finaliza el proceso por vehículo
				ELSE
				BEGIN
					--SQUIROS: Inicio del proceso si las tarjetas están ligadas a los conductores (Agregado el 02/09/17)					
					
					SELECT TOP 1 @lPreviousClosingDate = [InsertDate] 
					FROM [Control].[CustomerCredits] 
					WHERE [InsertDate] < @lProcessDate 
					AND [CustomerId] = @CustomerId
					ORDER BY [InsertDate] DESC

					INSERT INTO [Control].[FuelDistribution]
					(
						[DriverId]
					   ,[Month]
					   ,[Year]
					   ,[Liters]
					   ,[Amount]
					   ,[AdditionalAmount]
					   ,[AdditionalLiters]
					   ,[InsertDate]
					)
					SELECT fd.[DriverId]
						  ,@lCurrentMonth
						  ,@lCurrentYear
						  ,MAX(fd.Liters)
						  ,MAX(fd.[Amount])
						  ,0
						  ,0
						  ,@lProcessDate
					FROM [Control].[FuelDistribution] fd
					INNER JOIN [General].[DriversUsers] d 
						ON d.[UserId] = fd.[DriverId]							 
					WHERE d.[CustomerId] = @CustomerId  					
					AND fd.[InsertDate] >= @lPreviousClosingDate
					GROUP BY fd.[DriverId]
					

					-- Inserts the Driver´s that not have closing the previous month's for any reason, but to this closing period they have to be iniciated too
					INSERT INTO [Control].[FuelDistribution]
					(
						[DriverId]
					   ,[Month]
					   ,[Year]
					   ,[Liters]
					   ,[Amount]
					   ,[AdditionalAmount]
					   ,[AdditionalLiters]
					   ,[InsertDate]
					)
					SELECT du.[UserId]
						  ,@lCurrentMonth
						  ,@lCurrentYear
						  ,0
						  ,0
						  ,0
						  ,0
						  ,@lProcessDate
					FROM [General].[DriversUsers] du
					INNER JOIN [General].[Users] u
						ON u.[UserId] = du.[UserId]
					WHERE du.[CustomerId] = @CustomerId 
					AND (u.[IsDeleted] = 0 OR u.[IsDeleted] IS NULL)  					
					AND du.[UserId] NOT IN (SELECT fd.[DriverId] 
											  FROM [Control].[FuelDistribution] fd
											  INNER JOIN [General].[DriversUsers] du
												ON du.[UserId] = fd.[DriverId]
											  WHERE du.[CustomerId] = @CustomerId
											  AND fd.[InsertDate] >= @lProcessDate)
																								 	
					--Llenamos la tabla que vamos a usar para actualizar el asignado en [FuelsByCredit]
					INSERT INTO #TotalAmountPerFuel
					Select VC.DefaultFuelId
						  ,fd.[Amount]
						  ,fd.[DriverId]
						  ,V.CustomerId
						  ,v.CostCenterId
					FROM [Control].[FuelDistribution] fd
					INNER JOIN [General].[VehiclesByUser] vbu
						ON fd.[DriverId] = vbu.[UserId]
					INNER JOIN General.Vehicles V 
						ON vbu.VehicleId = V.VehicleId
					INNER JOIN General.VehicleCategories VC 
						ON V.VehicleCategoryId = VC.VehicleCategoryId
					WHERE V.[CustomerId] = @CustomerId
					AND fd.[DriverId] IN (SELECT u.[UserId] 
										  FROM [General].[DriversUsers] du
										  INNER JOIN [General].[Users] u
											ON u.[UserId] = du.[UserId]						
										  WHERE du.[CustomerId] = @CustomerId 
										  AND u.[IsDeleted] <> 1 AND u.[IsActive] = 1)
					AND vbu.[LastDateDriving] IS NULL												
					AND fd.[InsertDate] >= @lProcessDate
					ORDER BY fd.[InsertDate] DESC

					--Update FuelsByCredit Assigned Amount   
					Update FC
					set FC.Assigned = VFG.Amount
					from Control.FuelsByCredit FC
					inner join (Select [CustomerId], [DefaultFuelId], Sum([Amount]) [Amount]
								from #TotalAmountPerFuel
								group by CustomerId, DefaultFuelId) VFG
					on FC.CustomerId = VFG.CustomerId and FC.FuelId = VFG.DefaultFuelId
					where FC.CustomerId = @CustomerId
					AND FC.[InsertDate] >= @lProcessDate

					if(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se envía notificación
					BEGIN
						SET @IsIncomplete = 1
						SET @Message = 'NO hubo actualización del Monto Asignado de Credito de Combustible para el Cliente con [#'+ CAST(@CustomerId AS varchar(20)) + ']'

						-- AGREGAMOS AL LOG DE BASE DE DATOS
						EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'
					END
					ELSE BEGIN
						SET @Message = 'Se procesó correctamente la actualización del Monto Asignado de Credito de Combustible al Cliente con [#'+ CAST(@CustomerId AS varchar(20)) + ']'

						-- AGREGAMOS AL LOG DE BASE DE DATOS
						EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
					END 
					------------------------------------------

					-- UPDATE CREDIT AVALIBLE ON CREADIT CARD WITH FUEL VEHICULE AMOUNT 
					-- AND ADD PREVIOUS BUDGET VALUE OR 0 IF DISABLE
					UPDATE fd				
					SET	fd.[AdditionalAmount] = CASE WHEN du.[PullPreviousBudget] = 1 AND (cc.[CreditAvailable] > 0)
													THEN ISNULL(cc.[CreditAvailable], 0)
													ELSE 0
												END,
						fd.[AdditionalLiters] = CASE WHEN du.[PullPreviousBudget] = 1 AND (cc.[AvailableLiters] > 0)
													THEN ISNULL(cc.[AvailableLiters], 0)
												ELSE 0
					END
					FROM [Control].[CreditCard] cc
					INNER JOIN [Control].[CreditCardByDriver] ccv
						ON cc.[CreditCardId] = ccv.[CreditCardId]
					INNER JOIN [General].[DriversUsers] du
						ON du.[UserId] = ccv.[UserId] 
					INNER JOIN [Control].[FuelDistribution] fd
						ON fd.[DriverId] = du.[UserId]										   							
					WHERE du.CustomerId = @CustomerId 
					AND cc.[StatusId] IN (7, 8)
					AND fd.[InsertDate] >= @lProcessDate;	
										

					UPDATE cc				
					SET	cc.[CreditAvailable] =	CASE WHEN du.[PullPreviousBudget] = 1 AND (cc.[CreditAvailable] > 0)
													THEN ISNULL(fd.[Amount], 0) + ISNULL(cc.[CreditAvailable], 0)
												ELSE ISNULL(fd.[Amount], 0)
												END,
						cc.[AvailableLiters] =	CASE WHEN du.[PullPreviousBudget] = 1 AND (cc.[AvailableLiters] > 0)
													THEN ISNULL(fd.[Liters], 0) + ISNULL(cc.[AvailableLiters], 0)
												ELSE ISNULL(fd.[Liters], 0)
												END
					FROM [Control].[CreditCard] cc
					INNER JOIN [Control].[CreditCardByDriver] ccv
						ON cc.[CreditCardId] = ccv.[CreditCardId]
					INNER JOIN [General].[DriversUsers] du
						ON du.[UserId] = ccv.[UserId] 
					INNER JOIN [Control].[FuelDistribution] fd
						ON fd.[DriverId] = du.[UserId]										   							
					WHERE du.CustomerId = @CustomerId 
					AND cc.[StatusId] IN (7, 8)
					AND fd.[InsertDate] >= @lProcessDate;

					if(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se envía notificación
					BEGIN
						SET @IsIncomplete = 1
						SET @Message = 'NO hubo actualización de Monto Asignado a las Tarjetas para el Cliente con [#'+ CAST(@CustomerId AS varchar(20)) + ']'

						-- AGREGAMOS AL LOG DE BASE DE DATOS
						EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'

						
					END
					ELSE BEGIN
						SET @Message = 'Se procesó correctamente la actualización del Monto Asignado a las Tarjetas para el Cliente con [#'+ CAST(@CustomerId AS varchar(20)) + ']'

						-- AGREGAMOS AL LOG DE BASE DE DATOS
						EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
					END 
					--------------------------------------------------

					-- SI EL PROCESO HA SIDO INCOMPLETO PARA UN CLIENTE SE ENVIARA UNA NOTIFICACION
					IF(@IsIncomplete = 1)
					BEGIN 
						SET @Subject = '[Monthly_Closing_Incomplete] Cliente #' + CAST(@CustomerId AS varchar(20))
						SET @Message = '[Monthly_Closing] Cliente #'+ CAST(@CustomerId AS varchar(20))  +' con problemas en el proceso de inicio de Mes.'
						SET @Message = @Message + ' [' + CAST(GETUTCDATE() AS VARCHAR(25)) +']'

						--SELECT @Message

						-- AGREGAMOS AL LOG DE BASE DE DATOS
						EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'E'

						-- ENVIAMOS LA NOTIFICACION
						Exec General.Sp_Email_AddOrEdit null, @To, null, null, @Subject, @Message, 0, 0, null, null 
					END
					--///////////////////////////////////////////////////////////////////////////
				END -- Finaliza el proceso si es por conductores


			END TRY  -- FINALIZA EL TRY DE UN SOLO CLIENTE POR RECORRIDO
			BEGIN CATCH
				
				SET @Subject = '[Monthly_Closing_Incomplete] Cliente #' + CAST(@CustomerId AS varchar(20))
				SET @Message = '[Monthly_Closing] Cliente #'+ CAST(@CustomerId AS varchar(20))  +' con problemas en el proceso de inicio de Mes.'
				SET @Message = @Message + ' [ErrorMessage: ' + ERROR_MESSAGE() + ' | ' + CAST(GETUTCDATE() AS VARCHAR(25)) +']'

				--SELECT @Message

				-- AGREGAMOS AL LOG DE BASE DE DATOS
				EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'E'

				-- ENVIAMOS LA NOTIFICACION
				Exec General.Sp_Email_AddOrEdit null, @To, null, null, @Subject, @Message, 0, 0, null, null 
			END CATCH

			UPDATE [Control].[BudgetClosingPeriodByCustomer] 
			SET [LastExecutionDate] = CONVERT(DATE, @lProcessDate)
			WHERE [CustomerId] = @CustomerId

			---- VALIDA SI EL PROCESO ES PARA MÁS DE UN CUSTOMER -----------------------
			IF(@ONLY_ONE_Customer IS NULL OR @ONLY_ONE_Customer = 0)
			BEGIN
				-- NEXT INDEX
				SELECT @CustomerId = MIN(CustomerId) FROM @tmpCustomersToProcess --[General].[Customers]
				WHERE  CustomerId > @CustomerId  -- AND [IsDeleted] IS NULL OR [IsDeleted] = 0
			END
			ELSE 
			BEGIN 
				SET @CustomerId = NULL
			END

			--Vacía la tabla para volverla a llenar con los datos del siguiente cliente
			DELETE FROM #TotalAmountPerFuel

		END  -- TERMINA WHILE CUSTOMERS
		        

	    --Warning Emails /////////////////////////////////////   TO DO: REUBICAR EL SCRIPT PARA QUE SE EJECUTE POR CUSTOMER
	    --Exec Control.Sp_MonthlyClosing_WarningEmails @lCurrentYear, @lCurrentMonth
	    
	    --EXEC [Control].[Sp_MonthlyClosing_LowPerformanceEmails] @lCurrentYear, @lCurrentMonth
		-- ///////////////////////////////////////////////////
         
	END TRY
	BEGIN CATCH
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH
        
	SET NOCOUNT OFF
END
