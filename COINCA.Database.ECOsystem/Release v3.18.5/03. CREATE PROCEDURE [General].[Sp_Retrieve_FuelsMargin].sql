-- =============================================
-- Author: Jason Bonilla
-- Create Date: 10/02/2020
-- Description: Retrieve Fuel Margin Data
-- =============================================
CREATE PROCEDURE [General].[Sp_Retrieve_FuelsMargin]
(
    @pCustomerId INT
)
AS
BEGIN
    SELECT * FROM [General].[parametersbyCustomer] 
	WHERE [Name] = 'FuelsMargin' 
	and [CustomerId] = @pCustomerId
END
GO
