USE ECOsystemDev
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 19/Dic/2016
-- Description:	Retrieve Service Alarm Configuration
-- Modify By:	Marco Cabrera
-- Modify Date:	19-06-2017
-- ================================================================================================
ALTER PROCEDURE [General].[Sp_ServiceAlarmConfig_Retrieve]
AS
BEGIN
	SET NOCOUNT ON

	SELECT 
		[ParameterID]
        ,[Description]
        ,[Value] AS [StrValue]
        ,[NumericValue] AS [NumValue]
	FROM [dbo].[GeneralParameters] gp
	WHERE [ParameterID] IN (
		'ServiceMail_Interval',
		'ServiceMail_DummySubject',
		'ServiceMail_DummyMail',
		'SM_BacFlota_SMTPSERVER',
		'SM_BacFlota_SMTPACCOUNT',
		'SM_BacFlota_SMTPPASS'
	)

	SET NOCOUNT OFF 
END