USE ECOsystem
GO

IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'Sp_SendProgramReport_AddOrEdit' AND TYPE = 'P')
	DROP PROC [General].[Sp_SendProgramReport_AddOrEdit]
GO

-- ===========================================================================================================
-- Author:		Stefano Quirós
-- Create date: 20/03/2017
-- Description:	AddOrEdit SendProgramReport
-- ===========================================================================================================
-- Modify: Henry Retana - 30/5/2017 - Add the send date to the parameters
-- Modify: Henry Retana - 04/04/2017 - Add the terminal detail to the program send report
-- Modify: Henry Retana - 28/11/2017 - Add the IsAlert detail to the program send report
-- Modify: Maria de los Angeles Jimenez Chavarria - JAN/23/2018 - Add user email (id, name, email) and module.
-- Modify: Maria de los Angeles Jimenez Chavarria - JAN/25/2018 - Update according id.
-- Modify: Juan Carlos Santamaria V - 2020-02-02 - Parameter @pId SET Default value in 0 - FLOT-1064 - 
-- ===========================================================================================================
CREATE PROCEDURE [General].[Sp_SendProgramReport_AddOrEdit]
(
	 @pId INT = 0
	,@pReportId INT
	,@pEmails VARCHAR(MAX) = NULL
	,@pStartDate DATETIME = NULL 
	,@pEndDate DATETIME = NULL
	,@pCustomerId INT = NULL
	,@pPartnerId INT = NULL
	,@pVehicleId INT = NULL
	,@pCostCenterId INT = NULL	
	,@pVehicleGroupId INT = NULL
	,@pVehicleUnitId INT = NULL
	,@pDriverId INT = NULL
	,@pElapse INT 
	,@pDays VARCHAR(20) = NULL
	,@pTerminalDetail BIT = NULL
	,@pIsAlert BIT = NULL
	,@pUserEmailId INT = NULL
	,@pUserId INT = NULL
	,@pActive BIT = NULL
	,@pIsCoinca BIT = NULL
	,@pModule VARCHAR(1) = NULL
)
AS
BEGIN
	SET NOCOUNT ON	

	UPDATE [General].[SendProgramReports]
	SET [Active] = @pActive
	WHERE [ReportId] = @pReportId
	AND [CustomerId] = @pCustomerId

	IF (@pId = 0)
	BEGIN
		INSERT INTO [General].[SendProgramReports]
		(
			[ReportId]
		   ,[Emails]
		   ,[StartDate]
		   ,[EndDate]
		   ,[CustomerId]
		   ,[PartnerId]
		   ,[VehicleId]
		   ,[CostCenterId]
		   ,[VehicleGroupId]
		   ,[VehicleUnitId]
		   ,[DriverId]
		   ,[Elapse]
		   ,[Days]
		   ,[UserId]
		   ,[Active]
		   ,[InsertDate]
		   ,[InsertUserId]	   
		   ,[IsDownloaded]
		   ,[TerminalDetail]	   
		   ,[IsAlert]
		   ,[IsCoinca]
		   ,[Module]
		)
		VALUES 
		(
			 @pReportId
			,@pEmails 
			,@pStartDate 
			,@pEndDate
			,@pCustomerId 
			,@pPartnerId 
			,@pVehicleId 
			,@pCostCenterId 
			,@pVehicleGroupId 
			,@pVehicleUnitId 
			,@pDriverId 
			,@pElapse 
			,@pDays 
			,@pUserEmailId
			,@pActive 
			,GETDATE() 
			,@pUserId 
			,0
			,@pTerminalDetail
			,@pIsAlert
			,@pIsCoinca
			,@pModule
		)	
	END
	ELSE
	BEGIN
		UPDATE [General].[SendProgramReports]
		SET [ReportId] = @pReportId
			   ,[Emails] = @pEmails
			   ,[StartDate] = @pStartDate
			   ,[EndDate] = @pEndDate
			   ,[CustomerId] = @pCustomerId
			   ,[PartnerId] = @pPartnerId
			   ,[VehicleId] = @pVehicleId
			   ,[CostCenterId] = @pCostCenterId
			   ,[VehicleGroupId] = @pVehicleGroupId
			   ,[VehicleUnitId] = @pVehicleUnitId
			   ,[DriverId] = @pDriverId
			   ,[Elapse] = @pElapse
			   ,[Days] = @pDays
			   ,[UserId] = @pUserEmailId
			   ,[Active] = @pActive
			   ,[ModifyDate] = GETDATE()
			   ,[IsDownloaded] = 0
			   ,[TerminalDetail] = @pTerminalDetail
			   ,[IsAlert] = @pIsAlert
			   ,[IsCoinca] = @pIsCoinca
			   ,[Module] = @pModule
		WHERE [Id]= @pId
	END

	SET NOCOUNT OFF
END


GO

