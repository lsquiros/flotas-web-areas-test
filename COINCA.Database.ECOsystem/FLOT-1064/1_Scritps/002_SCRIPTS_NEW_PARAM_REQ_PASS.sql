-- ================================================================================================
-- Author:		Juan Carlos Santamaria 
-- Create date: 19/Dic/2016
-- Description:	ADD require credencials or not.  
-- ================================================================================================

IF NOT EXISTS (SELECT * FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'SM_BacFlota_SMTPPASS_REQ')
BEGIN
	INSERT INTO [dbo].[GeneralParameters]
	SELECT 
	  ParameterID	 = 'SM_BacFlota_SMTPPASS_REQ'
	 ,Property		 = 'NA'
	 ,Description	 = 'Flag para indicar si requiere (1) o no (0) el Password para la cuenta'
	 ,Value			 = ''
	 ,NumericValue	 = '0'
END

GO

SELECT * FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'SM_BacFlota_SMTPPASS_REQ'
GO
EXEC [General].[Sp_ServiceAlarmConfig_Retrieve]