USE ECOsystem
GO

-- ====================================================
-- Author:      Juan Carlos Santamaria V.
-- Create Date: 20-02-2020
-- Description: Actualizacion del Releay Gmail SMTP.
-- ====================================================

--SELECT * FROM [dbo].[GeneralParameters] WHERE [ParameterID] IN ('SM_BacFlota_SMTPSERVER')

UPDATE [dbo].[GeneralParameters] 
SET [Value] = 'smtp-relay.gmail.com'
WHERE [ParameterID] IN ('SM_BacFlota_SMTPSERVER')

--SELECT * FROM [dbo].[GeneralParameters] WHERE [ParameterID] IN ('SM_BacFlota_SMTPACCOUNT')

UPDATE [dbo].[GeneralParameters] 
SET [Value] = 'informativos@bacflota.com'
WHERE [ParameterID] IN ('SM_BacFlota_SMTPACCOUNT')

GO
