USE ECOsystemDeV
GO

IF EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE NAME = 'Sp_CancelCreditCardAlert_Send' AND TYPE = 'P')
	DROP PROC [Control].[Sp_CancelCreditCardAlert_Send]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 29/07/2019
-- Description:	Retrieve parameters for the partner
-- Modify: 2019-11-28 by Juan Carlos Santamaria // Send new parameter // JSA-001
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CancelCreditCardAlert_Send]
(	
	@pCustomerId INT,
	@pCustomerName VARCHAR(500),
	@pStatusId INT, 
	@pCreditCardCount INT,
	@pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON	

	IF @pStatusId = 9 
	BEGIN 
		DECLARE @lCreditCardNumber INT, 
				@lCreditCardEmails VARCHAR(1500),
				@lCancelCreditCardSend DATETIME,
				@lCount INT,
				@lActive INT,
				@lClosed INT,
				@lAdminCard INT

		SELECT @lCreditCardNumber = gp.[CancelCreditCardNumber], 
			   @lCreditCardEmails = gp.[CancelCreditCardEmails]
		FROM [General].[GeneralParametersByPartner] gp
		INNER JOIN [General].[CustomersByPartner] cp
			ON gp.[PartnerId] = cp.[PartnerId]
		WHERE gp.[CancelCreditCardActive] = 1
		AND cp.[CustomerId] = @pCustomerId
		
		IF @lCreditCardEmails IS NOT NULL
		BEGIN 

			/*---< JSA-001 >---*/
			SELECT @lActive = count(1)
			FROM [Control].[CreditCard] a
			WHERE a.[CustomerId] = 30
			AND a.StatusId	IN (7)

			SELECT   lCount = CASE WHEN a.[StatusId] = 9 THEN COUNT(1) ELSE 0 END
					,Managed = CASE WHEN a.[StatusId] NOT IN (6,9) THEN COUNT(1) ELSE 0 END
			INTO #CardAdmin
			FROM [Control].[CreditCard] a
			INNER JOIN [General].[Status] b 
					ON b.[StatusId] = a.[StatusId]
			WHERE a.[CustomerId] = @pCustomerId
			AND DATEPART(MONTH, a.[ModifyDate]) = DATEPART(MONTH, GETDATE())				
			AND DATEPART(YEAR, a.[ModifyDate]) = DATEPART(YEAR, GETDATE())			
			GROUP BY a.[StatusId],a.CustomerId
						
			SELECT @lActive = @lActive
				  ,@lAdminCard = SUM(Managed)
				  ,@lCount = SUM(lCount)
				  ,@lClosed = @pCreditCardCount
			FROM #CardAdmin
			/*END JSA-001*/

			IF @pCreditCardCount >= @lCreditCardNumber OR @lCount >= @lCreditCardNumber			
			BEGIN
				IF	@pCreditCardCount >= @lCreditCardNumber OR NOT EXISTS (
															SELECT *
															FROM [Control].[CreditCardAlarm]
															WHERE [CustomerId] = @pCustomerId
															AND [MonthlyCount] >= @lCreditCardNumber
															AND DATEPART(MONTH, [InsertDate]) = DATEPART(MONTH, GETDATE())	 
															AND DATEPART(YEAR, [InsertDate]) = DATEPART(YEAR, GETDATE())
														  )
				BEGIN 
					EXEC [General].[Sp_SendAlarm]	@pAlarmTriggerId = 651,
													@pEmail = @lCreditCardEmails, 
													@pCreditCards = @lCount, ---
													@pCustomerName = @pCustomerName,
													@pActive = @lActive,
													@pAdminCard = @lAdminCard,
													@pClosed = @lClosed 

				
					--Log the alert
					INSERT INTO [Control].[CreditCardAlarm]
					(
						[CustomerId],
						[StatusId],
						[MonthlyCount],
						[AlertCount],
						[InsertDate],
						[InsertUserId]
					)
					VALUES 
					(
						 @pCustomerId,
						 @pStatusId,
						 @lCount,
						 @pCreditCardCount,
						 GETDATE(),
						 @pUserId
					)	
				END
			END
		END
	END	
	
	SET NOCOUNT OFF
END

GO
