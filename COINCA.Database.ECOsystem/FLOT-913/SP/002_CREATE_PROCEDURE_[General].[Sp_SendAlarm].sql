USE ECOsystemDeV
GO

IF EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE NAME = 'Sp_SendAlarm' AND TYPE = 'P')
	DROP PROC [General].[Sp_SendAlarm]  
GO

-- ================================================================================================  
-- Author:  Cristian Martínez   
-- Create date: 24/11/2014  
-- Description: Store Procedure   
-- Modify By: Stefano Quirós -- Add new Email (OverSpeed)  
-- Modify By: Henry Retana -- 25/10/2016 - Add the Maintenance Preventive Alarm  
-- Modify By: Henry Retana -- 31/10/2016 - Modify the message for the overspeed alarm  
-- Modify By: Stefano Quirós - 02/28/2017 - Modify for New alarm StopsTime  
-- Modify By: Henry Retana -- 31/03/2017 - Modify the parameter Lapse to receive null  
-- Modify By: Marco Cabrera -- 08/05/2017 - Add new alarm(511 - Panic botton)  
-- Modify By: Gerald Solano -- 17/07/2017 - Add Maintenance History and App Notifications  
-- Modify By: Jose Ramirez  -- 31/10/2017 - Add temperature Alarm  
-- Modify By: Esteban Solís -- 31/10/2017 - Changes to maintenance service email information
-- Modify By: Esteban Solís - 29/11/2016   -- Added last 5 speed records for over speed alarm  
-- Modify By: Jose Julian / Gerald Solano -- 31/01/2018 - Cambios menores en templates
-- Modify By: Gerald Solano -- 09/04/2018 - Se concatena la placa del vehículo al Subject del correo - Alarma de Desconexión de GPS
-- Modify By: Manuel Corea -- 18/04/2018 - Alarma de uso de Grúa
-- Modify By: Manuel Corea -- 23/04/2018 - Actualizacion de Templates
-- Modify By: Henry Retana -- 30/05/2018 - Is Demo Alert for Customers 
-- Modify By: Marjorie Garbanzo -- 13/07/2018 - Add Non Transmission GPS Alarm
-- Modify By: Henry Retana -- 10/08/2018 - Contract Alert for Customers 
-- Modify By: Gerald Solano -- 09/01/2019 - Se sustituye en Subject los "-" para evitar que la consola de correos lo filtre por un tipo de correo
-- Modify By: Gerald Solano -- 27/03/2019 - Se valida la fecha de notificacion para las alertas de desconexion
-- Modify By: Gerald Solano -- 06/08/2019 - The product type is validated for customer notifications in Test Mode
-- Modify By: Henry Retana -- 29/07/2018 - Cancel Credit Card Alert
-- Modify By: JuanK Santamaria -- 29/11/2019 - New params Card Canceled JSA-001
-- =================================================================================================== 

CREATE PROCEDURE [General].[Sp_SendAlarm]  
(
	@pAlarmId INT = NULL,  
	@pAlarmTriggerId INT = NULL,  
	@pPeriodicityAlarm INT = NULL,  
	@pCustomerId INT = NULL,  
	@pPlate VARCHAR(30) = NULL,  
	@pVehicleId INT = NULL,  
	@pDescription VARCHAR(500) = NULL,   
	@pName VARCHAR(250) = NULL,  
	@pTimeLastReport DATETIME = NULL,   
	@pPhone VARCHAR(100) = NULL,   
	@pEmail VARCHAR (500) = NULL,   
	@pInOut VARCHAR (20) = NULL,   
	@pRowVersion TIMESTAMP = NULL,  
	@pVehicleName VARCHAR(100) = NULL,  
	@pMAXSpeed DECIMAL(16,2) = NULL,  
	@pMINSpeed DECIMAL(16,2) = NULL,  
	@pAGVSpeed DECIMAL(16,2) = NULL,  
	@pLocation VARCHAR(MAX) = NULL,  
	@pLongitude DECIMAL(13,10) = NULL,  
	@pLatitude DECIMAL(13,10)  = NULL,  
	@pCountryCode VARCHAR(10) = NULL,  
	@pPreventiveMaintenanceId INT = NULL,  
	@pMaintenanceName VARCHAR(200) = NULL,  
	@pOdometer INT = NULL,  
	@pNextReviewOdometer INT = NULL,
	@pNextReviewDate DATETIME = NULL,  
	@pExpiredDays INT = NULL,  
	@pExpiredOdometer INT = NULL,  
	@pDescriptionDate VARCHAR (150) = NULL,  
	@pDescriptionOdometer VARCHAR (150)  = NULL,  
	@pFuelName VARCHAR (200)  = NULL,  
	@pLiterPrice DECIMAL(16,2) = NULL,  
	@pEndDate VARCHAR(20) = NULL,  
	@pAlarmDays INT = NULL,  
	@pValue INT = NULL,  
	@pLapse VARCHAR(100) = NULL,  
	@pTemperature FLOAT = NULL,  
	@pSensor  VARCHAR(20) = NULL,  
	@pLimiteTemperatura FLOAT = NULL,
	@pXMLSpeed XML = NULL,
	@psite varchar(200) = NULL,
	@pFrom VARCHAR (500) = NULL,
	@pMainVolt FLOAT = NULL,
	@pStartDate DATETIME = NULL,
	@pCustomerName VARCHAR (500) = NULL,
	@pVehicles INT = NULL,
	@pCreditCards INT = NULL,
	@pReportLasteDate VARCHAR(20) = NULL,
	@pDaysLeft INT = NULL,
	@pActive INT = NULL,
	@pAdminCard INT = NULL,
	@pClosed INT = NULL

)
AS  
BEGIN   
	DECLARE @lErrorMessage NVARCHAR(4000)  
	DECLARE @lErrorSeverity INT  
	DECLARE @lErrorState INT  
	DECLARE @lSubject VARCHAR(300)  
	DECLARE @lMessage VARCHAR (MAX)  
	DECLARE @lValue INT = 0  
	DECLARE @lNextAlarm DATE  
	DECLARE @tbl_splitPartners TABLE (PartnerId int)  
	DECLARE @int_TimeZone INT = -6  
	DECLARE @str_Partners varchar(max)=''  
	DECLARE @xml_strPartners XML  
	DECLARE @int_PartnerId int = 0  
	DECLARE @int_NotificationType int = null 
	DECLARE @GPSDateTime DATETIME
	DECLARE @Speed DECIMAL(18,2)
	DECLARE @Comments VARCHAR(400)  
	DECLARE @Ind INT
	DECLARE @lPartnerName VARCHAR(400) 
	DECLARE @lPartnerType INT
	DECLARE @lHeaderAlertUrl VARCHAR(300) 
  
	IF EXISTS (SELECT * 
			   FROM [General].[Alarms] a
			   INNER JOIN [General].[Customers] c
				  ON a.[CustomerId] = c.[CustomerId]
			   WHERE [AlarmTriggerId] = @pAlarmTriggerId
			   AND a.[CustomerId] = @pCustomerId
			   AND a.[SMS] = 1
			   AND c.[SMSAlarms] = 1) 
	BEGIN 
		--SEND SMS ALARM
		EXEC [General].[Sp_SMSSendAlarm]  
			@pAlarmId,  
			@pAlarmTriggerId,  	
			@pCustomerId,  
			@pName,
			@pPlate,
			@pDescription,
			@pInOut,   	
			@pVehicleName,  
			@pMAXSpeed,  
			@pMINSpeed, 
			@pEndDate,
			@pMainVolt,
			@pValue,
			@pTemperature,
			@pSensor,
			@pLapse,
			@pMaintenanceName,
			@pReportLasteDate,
			@pLocation

		IF @pAlarmTriggerId = 13 
		BEGIN 
			UPDATE [General].[Alarms]
			SET [ReportLasteDate] = @pReportLasteDate
			WHERE [AlarmId] = @pAlarmId
		END
	END 
	ELSE 
	BEGIN 
		BEGIN TRY     
			--Obtenemos la diferencia horaria  
			SELECT @int_TimeZone = ISNULL(co.[TimeZone], -6) 
			FROM [General].[Countries] co   
			INNER JOIN [General].[Customers] cu 
				ON co.[CountryId] = cu.[CountryId]  
			WHERE cu.CustomerId = @pCustomerId  
	  
			--Desde un parámetro se obtiene los Socios que requieren notificaciones via App  
			SELECT @str_Partners = [Value] 
			FROM [dbo].[GeneralParameters] 
			WHERE [ParameterID] = 'PARTNER_APP_NOTIFICATION'  
	  
			--Convertimos el string plano de los valores a un valor XML  
			SET @xml_strPartners = CAST(('<row><col>' + REPLACE(@str_Partners,',','</col></row><row><col>') + '</col></row>') AS XML)  
	  
			--Insertamos en una tabla tipo variable para facilitar la consulta de los Socios   
			INSERT INTO @tbl_splitPartners  
			SELECT t.n.value('col[1]','int')  
			FROM  @xml_strPartners.nodes ('/row') AS t(n)  
	  
			--Obtenemos el Socio relacionado al Cliente   
			SELECT @int_PartnerId = p.[PartnerId],
				   @lPartnerName = p.[Name],
				   @lPartnerType = p.[ProductTypesId]
			FROM [General].[CustomersByPartner] cp
			INNER JOIN [General].[Partners] p
				ON cp.[PartnerId] = p.[PartnerId]
			WHERE cp.[CustomerId] = @pCustomerId  
			  
			-- Get Template Message  
			SELECT @lMessage = b.[Message]   
			FROM [General].[Types] a  
			INNER JOIN [General].[Values] b  
				ON a.[TypeId] = b.[TypeId]  
			WHERE a.[TypeId] = @pAlarmTriggerId  
			AND (@pValue IS NULL OR b.[Value] = @pValue)  
		
			IF @pPeriodicityAlarm IS NOT NULL  
			BEGIN   		  
				SELECT @lValue = b.[Value]   
				FROM [General].[Types] a  
				INNER JOIN [General].[Values] b  
					ON a.[TypeId] = b.[TypeId]  
				WHERE a.[TypeId] = @pPeriodicityAlarm  
			END   
			 		   
			IF (@lValue IS NOT NULL AND @lMessage IS NOT NULL)  
			BEGIN     
				
				SELECT @lHeaderAlertUrl = [Value] 
				FROM [dbo].[GeneralParameters] 
				WHERE [ParameterID] = CASE WHEN @lPartnerType = 3 
										   THEN 'ALERT_CONTROL_CAR_HEADER' 
										   ELSE 'ALERT_FLOTAS_HEADER' 
									  END 

				SET @lMessage = REPLACE(@lMessage, '%pictureHeader%', @lHeaderAlertUrl)
				SET @lNextAlarm = DATEADD(DAY, @lValue, DATEADD(Hour, @int_TimeZone,GETUTCDATE()))  
				
				IF @pAlarmTriggerId = 518
				BEGIN 
					SET @lSubject = @pCustomerName + ' | Notificación de Vencimiento de Contrato'
					SET @lMessage = REPLACE(@lMessage, '%partner%', @lPartnerName)  
					SET @lMessage = REPLACE(@lMessage, '%customerName%', @pCustomerName)  
					SET @lMessage = REPLACE(@lMessage, '%startDate%', CAST(@pStartDate AS DATE))  
					SET @lMessage = REPLACE(@lMessage, '%endDate%', CAST(@pEndDate AS DATE))   
					SET @lMessage = REPLACE(@lMessage, '%daysLeft%', @pDaysLeft)  
					SET @lMessage = REPLACE(@lMessage, '%site%', 'flotas.baccredomatic.com')  
					SET @lNextAlarm = NULL 
				END

				IF @pAlarmTriggerId = 517
				BEGIN 
					IF @lPartnerType = 3
					BEGIN 
						SELECT @lMessage = b.[Message]
						FROM [General].[Types] a  
						INNER JOIN [General].[Values] b  
							ON a.[TypeId] = b.[TypeId]  
						WHERE a.[Code] = 'CUSTOMER_DEMOALERT_CCAR'  
						
						SET @lSubject = @pCustomerName + ' | Alerta de Cliente en Modo Prueba'
						
					END 
					ELSE 
					BEGIN
						SET @lSubject = @pCustomerName + ' | Alerta de Cliente en Modo Piloto'
						SET @lMessage = REPLACE(@lMessage, '%creditCards%', @pCreditCards)  
						SET @lMessage = REPLACE(@lMessage, '%vehicles%', @pVehicles)  	
					END	
					
					SET @lSubject = @pCustomerName + ' | Alerta de Cliente en Modo Piloto'
					SET @lMessage = REPLACE(@lMessage, '%partner%', @lPartnerName)  
					SET @lMessage = REPLACE(@lMessage, '%customerName%', @pCustomerName)  
					SET @lMessage = REPLACE(@lMessage, '%startDate%', CAST(@pStartDate AS DATE))  
					SET @lMessage = REPLACE(@lMessage, '%isDemoAlert%', CAST(@pEndDate AS DATE))   					
					SET @lNextAlarm = NULL 
				END

				IF @pAlarmTriggerId = 280
				BEGIN    
					SET @lSubject = @pPlate + ' | Alarma Activación de Plataforma de Grúa'   
					SET @lMessage = REPLACE(@lMessage, '%dateAlarm%', @pEndDate)  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)   
					SET @lMessage = REPLACE(@lMessage, '%driver%', ISNULL(@pName, ''))  
					SET @lMessage = REPLACE(@lMessage, '%name%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))  
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)          
					SET @lMessage = REPLACE(@lMessage, '%site%', @psite)
					SET @lNextAlarm = NULL   
				END 
	   
				IF @pAlarmTriggerId = 281
				BEGIN    
					SET @lSubject = @pPlate + ' | Alarma Inactivación de Plataforma de Grúa'   
					SET @lMessage = REPLACE(@lMessage, '%dateAlarm%', @pEndDate)  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)   
					SET @lMessage = REPLACE(@lMessage, '%driver%', ISNULL(@pName, ''))  
					SET @lMessage = REPLACE(@lMessage, '%name%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))  
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)          
					SET @lMessage = REPLACE(@lMessage, '%site%', @psite)
					SET @lNextAlarm = NULL   
				END      

				IF @pAlarmTriggerId = 10  
				BEGIN   
					SET @lSubject = 'Alarma Voltaje Bajo en Batería'  
					SET @lMessage = REPLACE(@lMessage, '%dateAlarm%', @pEndDate)  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)   
					SET @lMessage = REPLACE(@lMessage, '%name%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%site%', @psite)
					SET @lMessage = REPLACE(@lMessage, '%voltaje%', @pMainVolt)
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)    
				END  
	  
				IF @pAlarmTriggerId = 12  
				BEGIN   
					SET @lSubject = @pPlate + ' | Alarma Desconexión de Energía del GPS'
					SET @lSubject = REPLACE(@lSubject, '-', '_')  --Permite evitar que la consola de correos lo filtre por un tipo de correo

					--Validamos fecha de notificacion
					IF(DATEDIFF(HOUR, @pEndDate, DATEADD(Hour, @int_TimeZone, GETUTCDATE())) > 1) --Que la notificacion no tenga diferencia de horas
					BEGIN 
						SET @pEndDate = DATEADD(Hour, @int_TimeZone, GETUTCDATE())
					END

					SET @lMessage = REPLACE(@lMessage, '%dateAlarm%', @pEndDate)  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)   
					SET @lMessage = REPLACE(@lMessage, '%name%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%site%', @psite)
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)   
				END  
	  
				IF @pAlarmTriggerId = 13  
				BEGIN   
					SET @lSubject = @pPlate + 'Alarma de Vehículos Sin Transmitir'
					SET @lSubject = REPLACE(@lSubject, '-', '_')  --Permite evitar que la consola de correos lo filtre por un tipo de correo
					   
					SET @lMessage = REPLACE(@lMessage, '%dateAlarm%', @pEndDate)  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)   
					SET @lMessage = REPLACE(@lMessage, '%name%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%dateLastReport%', @pReportLasteDate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)  
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)   
				END  
	  
				IF @pAlarmTriggerId = 25  
				BEGIN   
					SET @lSubject = 'Alarma  Vehículo Apagado por Comando'  
					SET @lMessage = REPLACE(@lMessage, '%dateAlarm%', @pEndDate)  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)   
					SET @lMessage = REPLACE(@lMessage, '%name%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%site%', @psite)
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)   
	  	  
					SET @int_NotificationType = 1  
				END  
	   
				IF @pAlarmTriggerId = 31  
				BEGIN  
					SET @lSubject = 'Alarma  Vehículo Temperatura Máxima Excedida'  
					SET @lMessage = REPLACE(@lMessage, '%dateAlarm%', @pEndDate)  
					SET @lMessage = REPLACE(@lMessage, '%tempReportLim%', CONVERT(VARCHAR,@pLimiteTemperatura) + '&deg;C')  
					SET @lMessage = REPLACE(@lMessage, '%tempReport%', CONVERT(VARCHAR,@pTemperature) + '&deg;C')  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)   
					SET @lMessage = REPLACE(@lMessage, '%name%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%site%', @psite)
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))  
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)   
				END  
		 
				IF @pAlarmTriggerId = 32  
				BEGIN  
					SET @lSubject = 'Alarma  Vehículo Temperatura por debajo del Minimo permitido'  
					SET @lMessage = REPLACE(@lMessage, '%dateAlarm%', @pEndDate)  
					SET @lMessage = REPLACE(@lMessage, '%tempReportMin%', CONVERT(VARCHAR,@pLimiteTemperatura) + '&deg;C')  
					SET @lMessage = REPLACE(@lMessage, '%tempReport%', CONVERT(VARCHAR,@pTemperature) + '&deg;C')  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)   
					SET @lMessage = REPLACE(@lMessage, '%name%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%site%', @psite)
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))  
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)   
				END  
		 
				IF @pAlarmTriggerId = 33  
				BEGIN  
					SET @lSubject = 'Error con sensor de temperatura'  
					SET @lMessage = REPLACE(@lMessage, '%dateAlarm%', @pEndDate)  
					SET @lMessage = REPLACE(@lMessage, '%sensor%', @pSensor)  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)   
					SET @lMessage = REPLACE(@lMessage, '%name%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%site%', @psite)
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))  
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)   
				END  
		 
				IF @pAlarmTriggerId = 500   
				BEGIN   		  
					IF(@pInOut = 'Entrado')  
					BEGIN  
						SET @pInOut = 'entrado en'  
					END  
					ELSE  
					BEGIN  
						SET @pInOut = 'salido de'  
					END  
		  
					SET @lSubject = 'Alarma Geocerca'  
					SET @lMessage = REPLACE(@lMessage, '%vehicle%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%InOut%', @pInOut)  
					SET @lMessage = REPLACE(@lMessage, '%Description%', @pDescription) 		
					SET @lMessage = REPLACE(@lMessage, '%latitude%', ISNULL(@pLatitude, 0))  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', ISNULL(@pLongitude, 0)) 
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, 'N/A'))
					SET @lMessage = REPLACE(@lMessage, '%map%', ISNULL(CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode, '')) 		 
					SET @lMessage = REPLACE(@lMessage, '%time%', ISNULL(CONVERT(VARCHAR(10), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 111) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 108), ''))  
					SET @lNextAlarm = NULL   
				END   
	   
				IF @pAlarmTriggerId = 502   
				BEGIN   
					SET @lSubject = 'Alarma Vehículo Usado Fuera de Horario'  
					SET @lMessage = REPLACE(@lMessage, '%vehicle%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  	
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude) 		
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)  
					SET @lMessage = REPLACE(@lMessage, '%time%', CONVERT(VARCHAR(10), DATEADD(Hour, @int_TimeZone,@pTimeLastReport), 111) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @int_TimeZone,@pTimeLastReport), 108))--@pTimeLastReport)  
					SET @lNextAlarm = DATEADD(HOUR, 1, DATEADD(Hour, @int_TimeZone,GETUTCDATE()))  
				END   
	   
				IF @pAlarmTriggerId  = 503  
				BEGIN   
					SET @lSubject = 'Alarma de Mantenimiento Preventivo'    
					SET @lMessage = REPLACE(@lMessage, '%vehicle%', @pVehicleName)    
					SET @lMessage = REPLACE(@lMessage, '%plateid%', @pPlate)        
					SET @lMessage = REPLACE(@lMessage, '%odometer%', FORMAT(@pOdometer, '#,##0'))    
					SET @lMessage = REPLACE(@lMessage, '%maintenancename%', @pMaintenanceName)    
					SET @lMessage = REPLACE(@lMessage, '%nextreviewdate%', @pNextReviewDate)    
					SET @lMessage = REPLACE(@lMessage, '%expireddays%', CASE WHEN @pExpiredDays < 0   
									THEN 'Vencido por ' +  FORMAT(@pExpiredDays * -1, '#,##0')  
									ELSE 'Faltan ' + FORMAT(@pExpiredDays, '#,##0')  
									END)    
					SET @lMessage = REPLACE(@lMessage, '%nextreviewodometer%', @pNextReviewOdometer)    
					SET @lMessage = REPLACE(@lMessage, '%expiredodometer%', CASE WHEN @pExpiredOdometer < 0  
									THEN 'Vencido por ' + FORMAT(@pExpiredOdometer * -1, '#,##0')  
									ELSE 'Faltan ' + FORMAT(@pExpiredOdometer, '#,##0')  
									END)    
					SET @lMessage = REPLACE(@lMessage, '%descriptiondate%', @pDescriptionDate)    
					SET @lMessage = REPLACE(@lMessage, '%descriptionodometer%', @pDescriptionOdometer)    
					SET @lMessage = REPLACE(@lMessage, '%time%', DATEADD(Hour, @int_TimeZone,GETUTCDATE()) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 108))    
	  
					--Se asigna el tipo de notificación para App  
					SET @int_NotificationType = 0 -- Maintenance Type  
				END   
		 
				IF @pAlarmTriggerId = 504     
				BEGIN   
					SET @lSubject = 'Alarma por Vencimiento de Licencia'  
					SET @lMessage = REPLACE(@lMessage, '%Driver%', @pName) 
					SET @lMessage = REPLACE(@lMessage, '%expirationdate%', @pEndDate) 
					SET @lMessage = REPLACE(@lMessage, '%remaining%', @pExpiredDays)       
					SET @lMessage = REPLACE(@lMessage, '%time%', CONVERT(VARCHAR(10), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 111) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 108))  
				END   
		
				IF @pAlarmTriggerId = 505  
				BEGIN   
					SET @lSubject = 'Alarma Ajuste de Odómetro'  
					SET @lMessage = REPLACE(@lMessage, '%Vehicle%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%time%', CONVERT(VARCHAR(10), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 111) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 108))  
					SET @lNextAlarm = NULL   
				END  
		  
				IF @pAlarmTriggerId = 508  
				BEGIN   
					SET @lSubject = 'Alarma Tiempo de Parada'  
					SET @lMessage = REPLACE(@lMessage, '%PlateId%', @pPlate) 		
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude) 		
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode) 
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))  
					SET @lMessage = REPLACE(@lMessage, '%lapse%', @pLapse)  
					SET @lMessage = REPLACE(@lMessage, '%time%', CONVERT(VARCHAR(10), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 111) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 108))  
					SET @lNextAlarm = NULL   
				END  
		  			
				IF @pAlarmTriggerId = 510  
				BEGIN   
					SET @lSubject = 'Alarma Exceso de Velocidad'  
					SET @lMessage = REPLACE(@lMessage, '%time%', CONVERT(VARCHAR(10), @pTimeLastReport, 111) + ' ' + CONVERT(VARCHAR(8), @pTimeLastReport, 108))
					SET @lMessage = REPLACE(@lMessage, '%maxspeed%', @pMAXSpeed)  
					SET @lMessage = REPLACE(@lMessage, '%agvspeed%', @pAGVSpeed)   
					SET @lMessage = REPLACE(@lMessage, '%minspeed%', @pMINSpeed)  
					SET @lMessage = REPLACE(@lMessage, '%vehicle%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%driver%', ISNULL(@pName, ''))   
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)  
					SET @lMessage = REPLACE(@lMessage, '%Countrycode%', @pCountryCode)  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)        

					SET @Ind = 1;
					WHILE (@Ind < 6) 
					BEGIN
						SELECT	@GPSDateTime = c.value('@GPSDateTime', 'datetime'),
								@Speed = c.value('@Speed','decimal(18,2)'),
								@Comments = c.value('@Comments','varchar(400)')
						FROM	@pXMLSpeed.nodes('xmldata/speed') AS T(c)
						WHERE	c.value('@Id', 'int') = @Ind		

						SET @lMessage = REPLACE(@lMessage, '%GPSDateTime'+CONVERT(VARCHAR(5), @Ind) +'%', ISNULL(CONVERT(VARCHAR(10), @GPSDateTime, 111) + ' ' + CONVERT(VARCHAR(8), @GPSDateTime, 108), 'N/A'))           
						SET @lMessage = REPLACE(@lMessage, '%Speed'+CONVERT(VARCHAR(5), @Ind)+'%', ISNULL(CONVERT(VARCHAR(18), @Speed), 'N/A'))           
						SET @lMessage = REPLACE(@lMessage, '%Comments'+CONVERT(VARCHAR(5), @Ind)+'%', ISNULL(CONVERT(VARCHAR(400), @Comments), 'N/A'))           

						SET @Ind = @Ind + 1
					END

					SET @lNextAlarm = NULL   
				END    
		 
				IF @pAlarmTriggerId = 511  
				BEGIN   
					SET @lSubject = 'Alarma de Boton de Pánico'  
					SET @lMessage = REPLACE(@lMessage, '%dateAlarm%', @pEndDate)  
					SET @lMessage = REPLACE(@lMessage, '%plate%', @pPlate)  
					SET @lMessage = REPLACE(@lMessage, '%latitude%', @pLatitude)  
					SET @lMessage = REPLACE(@lMessage, '%longitude%', @pLongitude)   
					SET @lMessage = REPLACE(@lMessage, '%driver%', ISNULL(@pName, ''))  
					SET @lMessage = REPLACE(@lMessage, '%name%', @pVehicleName)  
					SET @lMessage = REPLACE(@lMessage, '%location%', ISNULL(@pLocation, ''))  
					SET @lMessage = REPLACE(@lMessage, '%map%', CONVERT(VARCHAR(MAX),@pLatitude) + '|' + CONVERT(VARCHAR(MAX),@pLongitude) + '|' + @pCountryCode)          
					SET @lMessage = REPLACE(@lMessage, '%site%', @psite)
					SET @lNextAlarm = NULL   
				END      
		 
				IF @pAlarmTriggerId = 515 AND @pValue = 0  
				BEGIN   
					SET @lSubject = 'Cambio precio combustible'  
					SET @lMessage = REPLACE(@lMessage, '%time%', CONVERT(VARCHAR(10), DATEADD(Hour, @int_TimeZone, GETUTCDATE()), 111) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @int_TimeZone, GETUTCDATE()), 108))  
					SET @lMessage = REPLACE(@lMessage, '%fuelname%', @pFuelName)  
					SET @lMessage = REPLACE(@lMessage, '%literprice%', @pLiterPrice)  
					SET @lMessage = REPLACE(@lMessage, '%changetime%', @pEndDate)       
					SET @lNextAlarm = NULL   
				END     
	  
				IF @pAlarmTriggerId = 515 AND @pValue = 1  
				BEGIN   
					DECLARE @lProgramText VARCHAR(100) = ''  
						   ,@lNoPrice VARCHAR(20) = ''  

					IF @pLiterPrice IS NULL  
					BEGIN  
						SET @lProgramText = 'No existe un cambio programado para este combustible'  
						SET @lNoPrice = 'No disponible'  
					END  
					ELSE  
						SET @lNoPrice = CAST(@pLiterPrice AS VARCHAR(20))  
	  
					SET @lSubject = 'Cambio precio combustible'  
					SET @lMessage = REPLACE(@lMessage, '%time%', CONVERT(VARCHAR(10), DATEADD(Hour, @int_TimeZone, GETUTCDATE()), 111) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @int_TimeZone, GETUTCDATE()), 108))  
					SET @lMessage = REPLACE(@lMessage, '%fuelname%', @pFuelName)  
					SET @lMessage = REPLACE(@lMessage, '%literprice%', @lNoPrice)  
					SET @lMessage = REPLACE(@lMessage, '%changetime%', @pEndDate)       
					SET @lMessage = REPLACE(@lMessage, '%alarmdays%', @pAlarmDays)       
					SET @lMessage = REPLACE(@lMessage, '%programalarm%', @lProgramText)       
					SET @lNextAlarm = NULL   
				END  

				IF @pAlarmTriggerId = 651
				BEGIN   
					SET @lSubject = 'Tarjetas canceladas'  
					SET @lMessage = REPLACE(@lMessage, '%customerName%', @pCustomerName)
					SET @lMessage = REPLACE(@lMessage, '%canceled%',@pClosed)
					SET @lMessage = REPLACE(@lMessage, '%managedCards%',@pAdminCard)
					SET @lMessage = REPLACE(@lMessage, '%closedCards%',@pCreditCards)
					SET @lMessage = REPLACE(@lMessage, '%activeCards%',@pActive)
					SET @lNextAlarm = NULL
				END  
	   
				--Validamos que el mensaje venga con información
				IF(@lMessage IS NOT NULL AND @pEmail IS NOT NULL AND LEN(@pEmail) > 0)
				BEGIN
					-- [TEMPORAL] - EVITAMOS ENVIAR LAS SIGUIENTES ALERTAS -- Alerta de Bateria Baja y Temperaturas
					IF(@pAlarmTriggerId NOT IN (10, 31, 32, 33))
					BEGIN
						INSERT INTO [General].[Emails] 
						(
							[To], 
							[From], 
							[Subject], 
							[Message], 
							[InsertDate]
						)  
						VALUES 
						(
							@pEmail,
							@pFrom, 
							@lSubject, 
							@lMessage, 
							DATEADD(Hour, @int_TimeZone,GETUTCDATE())
						)  

						IF @pAlarmTriggerId = 13 
						BEGIN 
							UPDATE [General].[Alarms]
							SET [ReportLasteDate] = @pReportLasteDate
							WHERE [AlarmId] = @pAlarmId
						END						
					END
				END 
		
				UPDATE [General].[Alarms]  
				SET [NextAlarm] = @lNextAlarm  
				WHERE [CustomerId] = @pCustomerId  
				AND [AlarmId] = @pAlarmId  
				AND [RowVersion] = @pRowVersion  
		
				IF(@pAlarmId IS NOT NULL)
				BEGIN
					INSERT INTO [General].[AlarmSend] 
					(
						[AlarmId], 
						[DateSend], 
						[InsertDate], 
						[InsertUserId]
					)  
					VALUES 
					(
						@pAlarmId, 
						DATEADD(Hour, @int_TimeZone, GETUTCDATE()), 
						DATEADD(Hour, @int_TimeZone,GETUTCDATE()),
						0
					)   
				END	
	  
				--Validamos si el Socio relacionado al cliente tiene habilitado la Notificación hacia Apps  
				IF EXISTS(SELECT 1 
						  FROM @tbl_splitPartners 
						  WHERE [PartnerId] IN (@int_PartnerId))  
				BEGIN 
					--Realizamos el proceso de acuerdo al tipo de notificación  
					IF (@int_NotificationType = 0) --MAINTENANCE    
					BEGIN 
						DECLARE @CurrentTime NVARCHAR(30) = CONVERT(VARCHAR(10), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 111) +   
								' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @int_TimeZone,GETUTCDATE()), 108)  
	  
						DECLARE @NextReviewDate NVARCHAR(30) = CONVERT(VARCHAR(10), @pNextReviewDate, 111) +   
								' ' + CONVERT(VARCHAR(8), @pNextReviewDate, 108)  
		   
						DECLARE @InsertDate DATETIME = DATEADD(Hour, @int_TimeZone,GETUTCDATE())  
	  
						--Ejecutamos el SP para agregar el mantenimiento y posteriormente agregar una notificación  
						EXEC [General].[Add_MaintenanceHistory]  
							@pCustomerId,  
							@pVehicleId,  
							@pPreventiveMaintenanceId,  
							@pMaintenanceName,  
							@CurrentTime,  
							@pExpiredDays,  
							@NextReviewDate,  
							@pDescriptionDate,  
							@pOdometer,  
							@pExpiredOdometer,  
							@pNextReviewOdometer,  
							@pDescriptionOdometer,  
							@InsertDate  
					END  
				END      
			END  
		END TRY   
		BEGIN CATCH   
			SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
	   
			RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)  
		END CATCH  
	END
END  
