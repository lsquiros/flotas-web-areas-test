
-- =============================================
-- Author:      Juan Carlos Santamaria
-- Create Date: 11-28-2019
-- Description: Creacion del nuevo html para el envio de correos para las tarjetas canceladas. 
-- =============================================

UPDATE [General].[Values]
SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml"><head> <meta name="viewport" content="width=device-width"> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <title></title> <style>body{color: #666; background-color: #fff; font-family: "Open Sans", Arial, sans-serif; font-size: 14px; font-weight: 500; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; line-height: 1.7em; width: 800px; max-width: 800px;}.center-div{text-align: center;}.container{width: 800px; max-width: 800px;}.content{margin: 40px;}.egt{margin: 0 auto;}.egt th{padding: .2em .8em; border: 2px solid rgba(107, 4, 21, 0.726); background: rgba(232, 0, 39, 0.877); text-align: center; color: white;}.egt td{padding: .7em .15em; border: 2px solid rgba(23, 84, 175, 0.25); background: rgba(55, 59, 65, 0.25); text-align: center;}</style></head><body cz-shortcut-listen="true"> <div class="container"> <div class="center-div"> <img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%"> </div><div> <h2><span style="text-align:center !important; color: rgb(232, 0, 39, 0.877);">Estimado Socio </span></h2> <p>Adjunto encontrar&aacute; la informaci&oacute;n relacionada a la gesti&oacute;n de tarjetas en el mes, para el cliente: <b>%customerName%</b></p></div><div class="content center-div"> <div> <table class="egt"> <tr> <th>Cerradas</th> <th>Tarjetas Administradas (Acumulado en el mes)</th> <th>Tarjetas Cerradas (Acumulado en el mes)</th> <th>Tarjetas Activas (Acumulado en el mes)</th> </tr><tr> <td><b>%canceled%</b> </td><td><b>%managedCards%</b></td><td><b>%closedCards%</b></td><td><b>%activeCards%</b></td></tr></table> </div><h3>Para nosotros es un placer servirle.</h3> <h2 style="color:rgb(240, 125, 52);">Namutek Fintech</h2> <p>En caso de necesitar asistencia sobre la plataforma de FLOTAS s&iacute;rvase comunicarse al tel&eacute;fono: <a href="tel:+506 2205 5151">+ 506 2205 5151</a> o al email <a href="mailto:soporte@bacflota.com">soporte@bacflota.com</a></p></div><div class="center-div"> <img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%"> </div></div></body></html>'
FROM  [General].[Types] AS a 
INNER JOIN [General].[Values] ON a.[TypeId] = [General].[Values].[TypeId]
WHERE (a.TypeId = 651)


