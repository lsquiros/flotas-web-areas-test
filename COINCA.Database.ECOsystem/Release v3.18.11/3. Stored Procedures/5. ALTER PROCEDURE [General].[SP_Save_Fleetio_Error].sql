/****** Object:  StoredProcedure [General].[SP_Save_Fleetio_Error]    Script Date: 8/19/2020 10:58:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:      Jason Bonilla  
-- Create Date: 27/03/2020  
-- Description: Save Fleetio Errors  
-- =============================================  
-- Author:      Juan Carlos Santamaria V. 
-- Create Date: 20-07-2020
-- Description: WHERE is added to the Update, so don't update everything
-- =============================================  
-- Author:      Juan Carlos Santamaria V. 
-- Create Date: 19-08-2020
-- Description: Update IsFixed Flag because is sended again and again
-- =============================================  
ALTER PROCEDURE [General].[SP_Save_Fleetio_Error]  
(  
 @pTransactionId INT,  
 @pServiceType INT = NULL,  
 @pMethod VARCHAR(80) = NULL,  
 @pErrorMessage VARCHAR(5000) = NULL,  
 @pResponseMessage VARCHAR(5000) = NULL,  
 @pSendedData VARCHAR(5000) = NULL,
 @pIsFixed BIT = 0  
)  
AS  
BEGIN  
 IF NOT EXISTS(SELECT *   
      FROM [General].[FleetioErrorLog] WITH(NOLOCK)
      WHERE [TransactionId] = @pTransactionId
	  AND [TransactionId] != 0)
 BEGIN  
  INSERT INTO [General].[FleetioErrorLog]  
  VALUES(  
   @pTransactionId,  
   @pServiceType,  
   @pMethod,  
   @pErrorMessage,  
   @pResponseMessage,  
   @pSendedData,  
   GETDATE(),  
   0  
  )  
 END  
 ELSE  
 BEGIN  
  UPDATE [General].[FleetioErrorLog]  
  SET [ServiceType] = @pServiceType  
     ,[Method] = @pMethod  
     ,[ErrorMessage] = @pErrorMessage  
     ,[ResponseMessage] = @pResponseMessage  
     ,[SendedData] = @pSendedData  
     ,[RegisterDate] = GETDATE()  
     ,[IsFixed] = @pIsFixed   
	WHERE [TransactionId] = @pTransactionId 
 END  
END  