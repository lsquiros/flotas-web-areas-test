/****** Object:  StoredProcedure [dbo].[SP_AspNetMenus_Retrieve]    Script Date: 20/8/2020 09:33:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================================================================  
-- Author:  Henry Retana  
-- Create date: 12/14/2015  
-- Description: Retrieve ASPNET Menus  
-- Modify - Henry Retana - 10/24/2016 - Add the option to set menus by customer  
-- Stefano Quirós Ruiz - 10/31/2016 - Change RoleName for RoleId - Retrieve menu option ByDriver   
-- or ByVehicle  
-- Modify - Henry Retana - 10/24/2016 - Validates menus by partner  
-- Modify By - Stefano Quirós / Henry Retana - 21/08/2017 - Change the name of the drivers name menu  
-- Modify By - Esteban Solís  - 11/12/2017 - Ajuste de presupuesto por conductor 
-- Modify By - Stefano Quirós - Add logic to show CustomReports - 23/04/2018
-- Modify By - Luis Carlos Rojas - The partnerId parameter is added to filter the loading of the partner menus - 20/08/2020
-- ==================================================================================================  

ALTER PROCEDURE [dbo].[SP_AspNetMenus_Retrieve]    --'10703CAB-999E-4293-A423-28C94620EE6D', 3, null, null, 5 --'F024C5BD-C5F4-408C-8A86-B5DBA55435E0', 3, null, 30, null
(  
  @pRoleId  VARCHAR(100) = NULL  
 ,@pHeader INT = NULL  
 ,@pParent INT = NULL  
 ,@pCustomerId INT = NULL  
 ,@pPartnerId INT = NULL
)   
AS  
BEGIN   
 SET NOCOUNT ON;  
  
  DECLARE @lCreditCardEmissionType INT = 0,  
   @lAvailableModules VARCHAR(10) = NULL,  
   @lPartnerId INT = NULL,
   @lParameter INT = NULL

   CREATE TABLE #Result
   (
		[MenuId] INT
	   ,[Name] VARCHAR(100)
	   ,[Ico] VARCHAR(100)
	   ,[Controller] VARCHAR(100)
	   ,[Action] VARCHAR(100)
	   ,[Parent] INT
	   ,[Header] INT
	   ,[Order] INT
	   ,[CustomerId] INT
   )
  
 SELECT  @lPartnerId = [PartnerId]  
 FROM [General].[CustomersByPartner]  
 WHERE [CustomerId] = @pCustomerId  
  
 SELECT @lCreditCardEmissionType = [IssueForId], @lAvailableModules = [AvailableModules]   
 FROM [General].[Customers]  
 WHERE [CustomerId] = @pCustomerId  

 SELECT @lParameter = [NumericValue] 
 FROM [dbo].[GeneralParameters] 
 WHERE parameterid = 'ASPMENU_CUSTOM_REPORT'
  
INSERT INTO #Result
 SELECT  m.[Id] [MenuId]  
     ,CASE WHEN @lCreditCardEmissionType = 100 AND m.[Name] = 'Por Vehículos' THEN 'Por Conductores'  
           WHEN @lAvailableModules LIKE '%G%' AND m.[Name] = 'Conductores' THEN 'Conductores/Agentes'  
   ELSE m.[Name] END AS [Name]  
     ,m.[Ico]  
     ,p.[Resource] [Controller]  
     ,p.[Action]       
     ,m.[Parent]  
     ,m.[Header]  
     ,m.[Order]   
     ,mc.[CustomerId]  
 FROM [dbo].[aspmenus] m   
 LEFT OUTER JOIN [dbo].[aspnetpermissions] p  
  ON m.[PermissionId] = p.[Id]  
 INNER JOIN [dbo].[aspmenusbyroles] mr  
  ON mr.[MenuId] = m.[Id]  
 INNER JOIN [dbo].[AspNetRoles] r  
  ON mr.[RoleId] = r.[Id]   
 LEFT OUTER JOIN [dbo].[AspMenusByCustomers] mc  
  ON m.[Id] = mc.[MenuId]    
 WHERE  (@pCustomerId IS NULL OR mc.[CustomerId] = @pCustomerId OR mc.[PartnerId] = @lPartnerId)  
       AND (@pRoleId IS NULL OR r.[Id] = @pRoleId)  
    AND (@pHeader IS NULL OR m.[Header] = @pHeader)  
    AND (@pParent IS NULL OR m.[Parent] = @pParent)
	AND (@pPartnerId IS NULL OR mc.[PartnerId] = @pPartnerId)
    --AND (@lCreditCardEmissionType = 101 OR m.Name <> 'Presupuesto Adicional')  
 UNION   
 SELECT m.[Id] [MenuId]  
     ,CASE WHEN @lCreditCardEmissionType = 100 AND m.[Name] = 'Por Vehículos' THEN 'Por Conductores'  
           WHEN @lAvailableModules LIKE '%G%' AND m.[Name] = 'Conductores' THEN 'Conductores/Agentes'  
   ELSE m.[Name] END AS [Name]  
     ,m.[Ico]  
     ,p.[Resource] [Controller]  
     ,p.[Action]       
     ,m.[Parent]  
     ,m.[Header]  
     ,m.[Order]   
     ,mc.[CustomerId]  
 FROM [dbo].[aspmenus] m   
 LEFT OUTER JOIN [dbo].[aspnetpermissions] p  
  ON m.[PermissionId] = p.[Id]  
 INNER JOIN [dbo].[aspmenusbyroles] mr  
  ON mr.[MenuId] = m.[Id]  
 INNER JOIN [dbo].[AspNetRoles] r  
  ON mr.[RoleId] = r.[Id]   
 LEFT OUTER JOIN [dbo].[AspMenusByCustomers] mc  
  ON m.[Id] = mc.[MenuId]   
 WHERE  (mc.[CustomerId] IS NULL AND mc.[PartnerId] IS NULL)  
    AND (@pRoleId IS NULL OR r.[Id] = @pRoleId)  
    AND (@pHeader IS NULL OR m.[Header] = @pHeader)  
    AND (@pParent IS NULL OR m.[Parent] = @pParent)  
    --AND (@lCreditCardEmissionType = 101 OR m.Name <> 'Presupuesto Adicional')  
  
 ORDER BY m.[Order],m.[Id], m.[Parent]  

 IF @pHeader = @lParameter
 BEGIN
	DECLARE @lId INT
		   ,@lController VARCHAR(100) = 'Reports'
		   ,@lAction VARCHAR(100) = 'Index'

	SELECT [Id]
		  ,[ReportName]
	INTO #TempMenu 
	FROM  [General].[Reports] 
	WHERE [IsCustom] = 1 AND [CustomerId] = @pCustomerId 

	IF EXISTS (SELECT * FROM #TempMenu)
	BEGIN
		DECLARE @lParent INT

		INSERT INTO #Result 
		(
			[MenuId]
		   ,[Name]
		   ,[Ico]
		   ,[Header]
		   ,[Order]
		   ,[CustomerId]
		)
		VALUES
		(
			(SELECT (MAX([MenuId]) + 1) FROM #Result)
		   ,'Reportes Personalizados'
		   ,'~/Content/Images/menu_reports.png'
		   ,@pHeader
		   ,(SELECT ([Order] - 1) FROM #Result WHERE [Name] = 'Reportes')
		   ,@pCustomerId
		)		

		SELECT @lParent = MAX([MenuId]) FROM #Result

		SELECT @lId = MIN([Id]) 
		FROM #TempMenu

		WHILE @lId IS NOT NULL
		BEGIN
			INSERT INTO #Result 
			(
				[MenuId]
			   ,[Name]
			   ,[Ico]
			   ,[Controller]
			   ,[Action]
			   ,[Parent]
			   ,[Header]
			   ,[Order]
			   ,[CustomerId]
			)
			SELECT (SELECT (MAX([MenuId]) + 1) FROM #Result) 
			      ,[ReportName]
				  ,''
				  ,@lController
				  ,@lAction
				  ,@lParent
				  ,@pHeader
				  ,@lId
				  ,@pCustomerId
			FROM #TempMenu
			WHERE [Id] = @lId

			DELETE FROM #TempMenu 
			WHERE [Id] = @lId	
			
			SELECT @lId = MIN([Id]) 
			FROM #TempMenu	
		END
	END 
 END


 SELECT * FROM #Result
  
 SET NOCOUNT OFF  
END  