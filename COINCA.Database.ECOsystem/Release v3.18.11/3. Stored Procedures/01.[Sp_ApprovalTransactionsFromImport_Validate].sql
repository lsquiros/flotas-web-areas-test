USE [ECOSystem]
GO
IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_ApprovalTransactionsFromImport_Validate' AND TYPE = 'P')
	DROP PROC [General].[Sp_ApprovalTransactionsFromImport_Validate]
GO

-- =================================================================================================
-- Author:		Henry Retana
-- Create date: 31/8/2017
-- Description:	Update Approval Transactions
-- Modify By: Stefano Quirós - Se cambia la tabla donde comparamos el 
-- odómetro de la transacción original
-- Henry Retana - 12/10/2017
-- Se corrige la tabla de transacciones historica y el nombre de la estacion de servicio
-- Modify by Stefano Quirós - 14/11/2017 - Add Void Transactions Validation
-- Modify by Henry Retana - 30/01/2019 - Validates duplicate transactionids in resulset
-- Stefano Quirós - Modify the logic that validate by descarting transactions and other changes - 05/02/2019 
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- Modify by Marjorie Garbanzo - 01/06/2020 Add logic to include transactions with different payment methods
-- Modify by Joshtyn Mejías - 06/08/2020 Add AuthorizationNumber validation in pending transactions
-- =================================================================================================

CREATE PROCEDURE [Control].[Sp_ApprovalTransactionsFromImport_Validate]
(
	@pXmlData VARCHAR(MAX),
	@pCustomerId INT
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lxmlData XML = CONVERT(XML, @pXmlData)
	DECLARE @lMinDate DATETIME,
			@lMaxDate DATETIME,
			@lCounter INT = 0,
			@lDefault UNIQUEIDENTIFIER,
			@lActiveStatus UNIQUEIDENTIFIER,
			@lTypeName VARCHAR(50),
			@lTypeActive VARCHAR(50),
			@lAllowProcess BIT,
			@lId INT = 0

	--Get Parameter
	SELECT @lAllowProcess = ISNULL([Value], 0)
	FROM [General].[ParametersByCustomer]
	WHERE [CustomerId] = @pCustomerId
	AND [ResuourceKey] = 'ALLOWAPPROVALTRANSACTIONSIMPORT'

	--Active
	SELECT TOP 1 @lActiveStatus = [Id]
				,@lTypeActive = [Name]
	FROM [Control].[ApprovalTransactionTypes]
	WHERE [Name] = 'Autorizado'

	--Default
	SELECT TOP 1 @lDefault = [Id]
				,@lTypeName = [Name]
	FROM [Control].[ApprovalTransactionTypes]
	WHERE [Default] = 1

	CREATE TABLE #ApprovalTransactionsImport
	(
		 [AuthorizationNumber] NVARCHAR(40)
		,[LineNumber] INT
		,[Reference] VARCHAR(50)
		,[Date]	DATETIME
		,[Odometer] INT
		,[Liters] DECIMAL(16, 2)
		,[Amount] DECIMAL(16, 2)
		,[Message] VARCHAR(500)	
	)

	CREATE TABLE #Result
	(
		 [LineNumber] INT
		,[TransactionId] INT
		,[AlreadyApproved] BIT
	)

	CREATE TABLE #DontFoundTransactions
	(
		[Id] INT
	   ,[Date] DATETIME
	   ,[Odometer] INT
	   ,[Amount] DECIMAL(16, 2)
	   ,[AuthorizationNumber] NVARCHAR(40)
	)

	INSERT INTO #ApprovalTransactionsImport
	SELECT  m.c.value('AuthorizationNumber[1]', 'NVARCHAR(40)')
		,	CAST(m.c.value('LineNumber[1]', 'VARCHAR(50)') AS INT)
		,	m.c.value('Reference[1]', 'VARCHAR(50)')
		,	m.c.value('DateFormat[1]', 'DATETIME')
		,	m.c.value('Odometer[1]', 'INT')
		,	m.c.value('Litters[1]', 'DECIMAL(16, 2)')
		,	m.c.value('Amount[1]', 'DECIMAL(16, 2)')
		,	m.c.value('Message[1]', 'VARCHAR(500)')	   
	FROM @lxmlData.nodes('//ApprovalTransactionsImport') AS m(c)	

	SELECT @lMinDate = MIN(Date), 
		   @lMaxDate = MAX(Date) 
	FROM #ApprovalTransactionsImport

	--SELECT * FROM #ApprovalTransactionsImport
	

	SELECT	t.[TransactionId]
		, DATEADD(HOUR, -6, t.[InsertDate]) [Date]
		, ISNULL(th.[Odometer], t.[Odometer]) [Odometer]
		, CONVERT(INT, t.[FuelAmount]) [FuelAmount]
		, t.[Liters]
		, t.[AuthorizationNumber]
	INTO #PendingTransactions  
	FROM [Control].[Transactions] t
	LEFT JOIN [Control].[TransactionsHx] th
		ON t.[TransactionId] = th.[TransactionId]
	WHERE t.[CreditCardId] IN (
									SELECT [CreditCardId] 
									FROM [Control].[CreditCard]
									WHERE [CustomerId] = @pCustomerId
							  )
	AND DATEADD(HOUR, -6, t.[InsertDate]) >= @lMinDate AND DATEADD(HOUR, -6, t.[InsertDate]) < DATEADD(d, 1, @lMaxDate)
	AND 1 > (
				SELECT ISNULL(COUNT(1), 0) 
				FROM [Control].[Transactions] t2
				WHERE (t2.[CreditCardId] = t.[CreditCardId] 
						OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
				AND t2.[TransactionPOS] = t.[TransactionPOS] 
				AND t2.[ProcessorId] = t.[ProcessorId] 
				AND (t2.IsReversed = 1 OR t2.IsVoid = 1)
			)
    AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)


	--select * from #PendingTransactions

	SELECT @lCounter = MIN([LineNumber])
	FROM #ApprovalTransactionsImport

	WHILE @lCounter IS NOT NULL 
	BEGIN
		DECLARE @lDate	DATETIME,
				@lOdometer INT,	        
				@lAmount DECIMAL(16, 2),
				@lTransactionId INT = NULL,
				@lAuthorizationNumber NVARCHAR(40)

		SELECT @lDate = [Date],
			   @lOdometer = [Odometer],
			   @lAmount = [Amount]
			,  @lAuthorizationNumber = AuthorizationNumber
		FROM #ApprovalTransactionsImport
		WHERE [LineNumber] = @lCounter

		--print @lAuthorizationNumber

		SELECT TOP 1 @lTransactionId  = [TransactionId]
		FROM #PendingTransactions
		WHERE AuthorizationNumber = @lAuthorizationNumber
		      AND (DATEADD(DAY, DATEDIFF(DAY, 0, [Date]), 0) = @lDate 
			       OR DATEADD(DAY, DATEDIFF(DAY, 0, [Date]), 0) = DATEADD(DAY, -1, @lDate)
			       OR DATEADD(DAY, DATEDIFF(DAY, 0, [Date]), 0) = DATEADD(DAY, 1, @lDate))
			 --AND CONVERT(INT, [FuelAmount]) = @lAmount
			 --AND [Odometer] like '%' + CONVERT(VARCHAR(20), @lOdometer) + '%'
	

		IF @lTransactionId IS NOT NULL
		BEGIN
		
			IF EXISTS (
							SELECT [TransactionId]
							FROM [Control].[ApprovalTransaction]
							WHERE [TypeId] IN (@lActiveStatus)
							AND [TransactionId] IN (@lTransactionId)
					 )
			BEGIN
				INSERT INTO #Result
				SELECT @lCounter, 
					   [TransactionId],
					   1
				FROM #PendingTransactions
				WHERE [TransactionId] = @lTransactionId

				DELETE FROM #PendingTransactions 
				WHERE [TransactionId] = @lTransactionId
			END
			ELSE 
			BEGIN
				INSERT INTO #Result
				SELECT @lCounter, 
					   [TransactionId],
					   0
				FROM #PendingTransactions
				WHERE [TransactionId] = @lTransactionId

				DELETE FROM #PendingTransactions 
				WHERE [TransactionId] = @lTransactionId
			END			
		END
		ELSE
		BEGIN
			INSERT INTO #DontFoundTransactions
			SELECT @lCounter [Id],
				   [Date],
			       [Odometer],
			       [Amount]	,
				   [AuthorizationNumber]		
			FROM #ApprovalTransactionsImport		
			WHERE [LineNumber] = @lCounter
		END

		SELECT @lCounter = MIN([LineNumber])
		FROM #ApprovalTransactionsImport
		WHERE [LineNumber] > @lCounter
	END

	IF EXISTS(SELECT * FROM #PendingTransactions)
	BEGIN
		SELECT @lId = MIN(Id) 
		FROM #DontFoundTransactions

		WHILE @lId IS NOT NULL AND EXISTS(SELECT * FROM #PendingTransactions)
		BEGIN
			DECLARE @lAmount2 DECIMAL(16, 2),
				    @lTransactionId2 INT = NULL,
					@lAuthorizationNumber2 NVARCHAR(40)

		SELECT @lAmount2 = [Amount]
			,  @lAuthorizationNumber2 = [AuthorizationNumber]
		FROM #DontFoundTransactions
		WHERE [Id] = @lId

		--print @lAmount2

		SELECT TOP 1 @lTransactionId2  = [TransactionId]
		FROM #PendingTransactions
		WHERE  [AuthorizationNumber] = @lAuthorizationNumber2 --CONVERT(INT, [FuelAmount]) = CONVERT(INT, @lAmount2)

		IF @lTransactionId2 IS NOT NULL
		BEGIN	
			IF EXISTS (
							SELECT [TransactionId]
							FROM [Control].[ApprovalTransaction]
							WHERE [TypeId] IN (@lActiveStatus)
							AND [TransactionId] IN (@lTransactionId2)
					 )
			BEGIN
				INSERT INTO #Result
				SELECT @lId, 
					   [TransactionId],
					   1
				FROM #PendingTransactions
				WHERE [TransactionId] = @lTransactionId2

				DELETE FROM #PendingTransactions 
				WHERE [TransactionId] = @lTransactionId2
			END
			ELSE 
			BEGIN
				INSERT INTO #Result
				SELECT @lId, 
					   [TransactionId],
					   0
				FROM #PendingTransactions
				WHERE [TransactionId] = @lTransactionId2

				DELETE FROM #PendingTransactions 
				WHERE [TransactionId] = @lTransactionId2
			END			
		END		

		SELECT @lId = MIN([Id])
		FROM #DontFoundTransactions
		WHERE [Id] > @lId
		END
	
	END

	--If the parameter is active, approve transactions
	IF @lAllowProcess = 1
	BEGIN			
		INSERT INTO [Control].[ApprovalTransaction]
		SELECT DISTINCT [TransactionId]
		               ,@lActiveStatus
			           ,GETDATE()
					   ,NULL
		FROM #Result r
		WHERE r.[TransactionId] NOT IN (
											SELECT [TransactionId] 
											FROM [Control].[ApprovalTransaction]
									   )
	END
	
	SELECT r.[LineNumber]
			,t.[TransactionId]
			,t.[Date]
			,v.[Name] AS [VehicleName]
			,v.VehicleId
			,v.[PlateId]
			,cc.[CreditCardNumber] AS [CreditCard]
			,ISNULL(cc.[CreditCardNumber], pay.[Code]) [PaymentInstrumentCode]
			,(CASE WHEN t.[PaymentInstrumentId] IS NULL 
					THEN 'Tarjeta'
					ELSE pit.[Name] END) AS [PaymentInstrumentType]
			,t.[CostCenterId]
			,vcc.[Name] AS [CostCenterName]			
			,ISNULL((SELECT TOP 1 du.[Identification]
	  		FROM [General].[Users] u
	  		INNER JOIN [General].[DriversUsers] du 
	  			ON du.Code = t.DriverCode
	  				AND du.Code IS NOT NULL
	  				AND du.Code <> ''
	  				AND u.[UserId] = du.[UserId]), 'N/A') AS [DriverId]
			,ISNULL((SELECT TOP 1 u.[Name]
	  		FROM [General].[Users] u
	  		INNER JOIN [General].[DriversUsers] du 
	  			ON du.Code = t.DriverCode
	  				AND du.Code IS NOT NULL
	  				AND du.Code <> ''
	  				AND u.[UserId] = du.[UserId]), 'N/A') [DriverName]
			,vc.[Manufacturer]
			,t.[Liters] AS [Liters]
			,t.[FuelAmount] AS [Amount]
			,f.[Name] AS [FuelName]
			,CASE WHEN t.[MerchantDescription] IS NULL OR t.[MerchantDescription] = '' 
				  THEN  [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0)
			      ELSE t.[MerchantDescription] END AS [ServiceStationName]
			,t.[ProcessorId] [TerminalId]
			,t.[ProcessorId] AS [AffiliateNumber]
			,ISNULL(case t.[Invoice]
	  			WHEN '' THEN NULL
	  			ELSE t.[Invoice]
	  			END, 'N/A') AS [Invoice]	 
			,@lTypeActive [StatusName]
			,@lActiveStatus [Status]	
			,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
								FROM [General].[Countries] coun
								WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName] 
			,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
			   WHEN t.[ExchangeValue] IS NOT NULL 
			   THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
			              (SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
							FROM [Control].[ExchangeRates] er
							INNER JOIN [Control].[Currencies] cue 
								ON er.[CurrencyCode] = cue.[CurrencyCode]
                            INNER JOIN [General].[Countries] coun 
								ON coun.[CountryId] = c.[CountryId]
							WHERE cue.[Code] = coun.[Code]
								AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
									OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
           END) AS [ExchangeValue]
		   ,(CASE  WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL THEN 
					[Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
					WHEN t.[IsInternational] = 0 THEN
					t.FuelAmount
					ELSE t.FuelAmount END) AS [RealAmount]
		   ,@lAllowProcess [AllowProcess]
		   ,[AlreadyApproved]
	FROM [Control].[Transactions] t
	INNER JOIN #Result r
		ON t.[TransactionId] = r.[TransactionId]
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]
	INNER JOIN [General].[VehicleCategories] vc 
		ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
	LEFT JOIN [Control].[CreditCard] cc 
		ON cc.CreditCardId = t.CreditCardId
	LEFT JOIN [Control].[PaymentInstruments] pay
		ON t.[PaymentInstrumentId] = pay.[Id]
	LEFT JOIN [Control].[PaymentInstrumentsTypes] pit
		ON pay.[typeId] = pit.[Id]
	INNER JOIN [General].[VehicleCostCenters] vcc 
		ON t.CostCenterId = vcc.CostCenterId
	INNER JOIN [Control].[Fuels] f 
		ON f.[FuelId] = t.[FuelId]
	LEFT OUTER JOIN [General].[Countries] co 
			ON t.[CountryCode] = co.[Code]
	WHERE c.[CustomerId] = @pCustomerId
	ORDER BY t.[Date] ASC

	DROP TABLE #Result
	DROP TABLE #ApprovalTransactionsImport
	DROP TABLE #PendingTransactions
	DROP TABLE #DontFoundTransactions

	SET NOCOUNT OFF
END
