/****** Object:  StoredProcedure [Control].[Sp_ValidateAvailable]    Script Date: 18/8/2020 10:37:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================
-- Author:		Stefano Quirós
-- Create date: March 06, 2018
-- Description:	Update Available of the CreditCard and return true or false
--Modify By:
--Luis Rojas 18/08/2020 - Validation of the additional budget so that it cannot be negative
-- =========================================================================

ALTER PROCEDURE [Control].[Sp_ValidateAvailable] 
(	
    @pDriverId	INT = NULL
   ,@pAssigned DECIMAL(16,2)
   ,@pRealAmount DECIMAL(16,2)
   ,@pTotalAssigned DECIMAL(16,2)
   ,@pTotal DECIMAL(16, 2)
   ,@pAvailable DECIMAL(16,2)
   ,@pCreditCardId INT
   ,@pAdditionalAmount DECIMAL(16, 2)
   ,@pResult INT = NULL OUTPUT
)
AS
BEGIN
SET NOCOUNT ON;
	BEGIN TRY	
	
		IF (@pAssigned < @pRealAmount)
		BEGIN
			SELECT @pResult = 50003
		END
		ELSE IF @pDriverId IS NULL AND (@pTotalAssigned > @pTotal)  
		BEGIN
			SELECT @pResult = 50002
		END
		ELSE IF (@pAvailable < 0)
		BEGIN
			SELECT @pResult = 50004
		END
		
		--Valida que el presupuesto adicional no sea negativo
		IF @pAdditionalAmount < 0 
		BEGIN 
			SET @pResult = 50005
		END

		IF @pResult IS NULL
		BEGIN
			SELECT @pResult = 0
		END

		RETURN @pResult
	END TRY
	BEGIN CATCH
		SELECT @pResult = ERROR_NUMBER()

		RETURN @pResult
	END CATCH
END

