
IF EXISTS (SELECT * FROM SYS.OBJECTS WHERE NAME = 'Sp_CreditCard_Retrieve')
	DROP PROC [Control].[Sp_CreditCard_Retrieve] 
GO

  
-- ================================================================================================    
-- Author:  Berman Romero L.    
-- Create date: 09/11/2014    
-- Description: Retrieve CreditCard information    
-- Dinamic Filter - 1/22/2015 - Melvin Salas    
-- Modify: Henry Retana  7/14/16    
-- Preauthorized  filter by creditcarNumber    
-- Modify: Henry Retana  14/9/17    
-- Modify the retrieve based on the status     
-- Modify: Kevin Pena  24/10/17    
-- Modify WHERE sentence when CustomerId is NULL (Coinca)    
-- Modify: Esteban Solis  15/12/2017    
-- Added parameter @pDriverId  
-- Modify: Alvaro Zamora  12/07/2018    
-- Added [InsertDate] and INNER JOIN 'j' with Countries table and Country's Name.  
-- Modify: Alvaro Zamora  26/07/2018    
-- Se cambia el nombre del resultado de tipo de tarjeta de credito, de 'C' a 'Credito' y de 'D' a 'Debito'.  
-- Stefano Quiros - Add AvailableLiters to the retrieve - 14/01/2019  
-- Juan Carlos Santamaria v - 2020-06-25 -	In the case of the company TWO PINES in the VEHICLE Name  --FLOT-981
--											or PERSON Name field, the VEHICLE Name must always come 
--											accompanied by a capital "V" at the beginning of the name
-- ================================================================================================    
CREATE PROCEDURE [Control].[Sp_CreditCard_Retrieve] --30, null, null, null, 1, 7  
(  
  @pCustomerId INT = NULL   
 ,@pCreditCardId INT = NULL  
 ,@pKey VARCHAR(800) = NULL  
 ,@pDriverId INT = NULL   
 ,@pUserId INT   
 ,@pStatusId INT = NULL  
 ,@pPaymentInstrumentType INT = NULL  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
  
 DECLARE @lStatus INT = 1  
 -- DYNAMIC FILTER    
 DECLARE @Results TABLE (items INT)  
 DECLARE @count INT      
 DECLARE @dospinosLetter varchar(4) = 'V ' 
  
 INSERT @Results  
 EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId  
  
 SET @count = (  
     SELECT COUNT(*)  
     FROM @Results  
      )  
  
 -- END    
 IF @pCustomerId IS NULL  
 BEGIN  
  SELECT @pCustomerId = [CustomerId]  
  FROM [Control].[CreditCard]  
  WHERE [CreditCardNumber] = @pKey  
  
  IF @pCustomerId IS NULL  
  BEGIN  
   SELECT @pCustomerId = [CustomerId]  
   FROM [General].[Vehicles]  
   WHERE [PlateId] = @pKey  
  END  
 END  
  
 DECLARE @lStatusTable TABLE ([Id] INT)  
  
 INSERT INTO @lStatusTable  
 VALUES (9)  
  
 CREATE TABLE #CardDataTable   
 (  
   [Id] INT IDENTITY(1,1)  
  ,[CreditCardId] INT --Identity de Sticker  
  ,[CustomerId] INT  
  ,[EncryptedCustomerName] VARCHAR(250)   
  ,[CreditCardNumber] NVARCHAR(520)  
  ,[ExpirationYear] INT  
  ,[ExpirationMonth] INT  
  ,[CreditAvailable] DECIMAL(16,2)  
  ,[AvailableLiters] DECIMAL(16,3)  
  ,[StatusId] INT  
  ,[Step] INT  
  ,[StatusName] VARCHAR(100)  
  ,[CurrencySymbol] NVARCHAR(50)  
  ,[IssueForId] INT  
  ,[UserId] INT  
  ,[VehicleId] INT  
  ,[CreditCardType] VARCHAR(50)  
  ,[EncryptedDriverName] VARCHAR(250)  
  ,[VehiclePlate] VARCHAR(50)  
  ,[CardRequestId] INT  
  ,[EstimatedDelivery] DATETIME  
  ,[InsertDate] DATETIME  
  ,[Usage] VARCHAR(50)  
  ,[PartnerId] INT  
  ,[Country] VARCHAR(250)  
  ,[PaymentInstrumentType] INT  
  ,[PaymentInstrumentTypeName] VARCHAR(50)  
 )  
  
 IF @pCustomerId = 0  
 BEGIN  
  IF ISNULL((  
     SELECT p.[Value]  
     FROM [General].[ParametersByCustomer] p  
     RIGHT JOIN [Control].[CreditCard] c ON p.[CustomerId] = c.[CustomerId]  
     WHERE c.[CreditCardNumber] = @pKey  
      AND p.[Name] = 'AllowProcess'  
      AND p.[ResuourceKey] = 'ALLOWINPROCESSTRANSACTIONS'  
     ), 0) = 0  
  BEGIN  
   INSERT INTO @lStatusTable  
   VALUES (@lStatus)  
  END  
 END  
  
  
 IF @pCreditCardId IS NULL  
 BEGIN  
  INSERT INTO #CardDataTable  
  SELECT  a.[Id] as [CreditCardId] --Identity de Sticker  
    ,a.[CustomerId]  
    ,c.[Name] AS [EncryptedCustomerName]  
    ,a.[Code]  
    ,0 [ExpirationYear]  
    ,0 [ExpirationMonth]  
    ,ISNULL(a.[CreditAvailable],0)[CreditAvailable]  
    ,ISNULL(a.[AvailableLiters],0)[AvailableLiters]  
    ,CONVERT(INT, a.[StatusId]) [StatusId]  
    ,b.[RowOrder] AS [Step]  
    ,b.[Name] AS [StatusName]  
    ,d.[Symbol] AS [CurrencySymbol]  
    ,c.[IssueForId]  
    ,x.[UserId]  
    ,x.[VehicleId]  
    ,xt.Name AS [CreditCardType]  
    ,f.Name AS [EncryptedDriverName]  
    ,@dospinosLetter + CASE WHEN c.[CustomerId] = 30   
        THEN h.[Name] + ' - ' + h.[PlateId]  
        ELSE h.[PlateId]  
        END AS [VehiclePlate]  
    ,x.CardRequestId  
    ,i.[EstimatedDelivery]  
    ,i.[InsertDate]  
    --,'' [RowVersion]  
    ,b.[Usage]  
    ,ISNULL((  
       SELECT TOP 1 p.[PartnerId]  
       FROM [General].[CustomersByPartner] p  
       WHERE p.[CustomerId] = a.[CustomerId]  
      ), 0) [PartnerId]  
    ,j.[Name] AS [Country]  
    ,xt.Id [PaymentInstrumentType]  
    ,xt.Name [PaymentInstrumentTypeName]  
  FROM [Control].[PaymentInstruments] a  
  INNER JOIN [Control].[PaymentInstrumentsByType] x  
   ON x.[PaymentInstrumentId] = a.[Id]  
  INNER JOIN [Control].[PaymentInstrumentsTypes] xt  
   ON xt.Id = a.TypeId  
  INNER JOIN [General].[Status] b   
   ON b.[StatusId] = a.[StatusId]  
  INNER JOIN [General].[Customers] c   
   ON c.[CustomerId] = a.[CustomerId]  
  INNER JOIN [Control].[Currencies] d   
   ON d.[CurrencyId] = c.[CurrencyId]  
  LEFT JOIN [General].[Users] f   
   ON f.[UserId] = x.[UserId]  
  LEFT JOIN [General].[Vehicles] h   
   ON h.[VehicleId] = x.[VehicleId]  
  INNER JOIN [Control].[CardRequest] i   
   ON x.[CardRequestId] = i.[CardRequestId]  
  INNER JOIN [General].[Countries] j  
   ON c.[CountryId] = j.[CountryId]  
  WHERE   
  (@pCustomerId = 0 OR (@pCustomerId = 0 AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')  
      OR (@pCustomerId IS NULL AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')  
      OR a.[CustomerId] = @pCustomerId)  
  AND (@count = 0 OR x.[VehicleId] IN (  
            SELECT items  
            FROM @Results  
           )) -- DYNAMIC FILTER    
  AND (@pCreditCardId IS NULL OR a.[Code] = (SELECT [Code] FROM [Control].[PaymentInstruments] WHERE [Id] = @pCreditCardId))  
  AND   
  (@pKey IS NULL  
   OR f.[Name] LIKE '%' + @pKey + '%'  
   OR h.[PlateId] LIKE '%' + @pKey + '%'    
   OR a.[Code] = @pKey  
   OR j.[Name] LIKE '%' + @pKey + '%')  
  AND (@pDriverId IS NULL OR @pDriverId = x.[UserId])  
  AND a.[StatusId] NOT IN (  
         SELECT [Id]  
         FROM @lStatusTable  
         )  
  AND (@pStatusId = - 1 OR @pStatusId IS NULL OR a.[StatusId] = @pStatusId)  
   
  INSERT INTO #CardDataTable  
  SELECT  a.[CreditCardId]  
    ,a.[CustomerId]  
    ,c.[Name] AS [EncryptedCustomerName]  
    ,a.[CreditCardNumber]  
    ,a.[ExpirationYear]  
    ,a.[ExpirationMonth]  
    ,a.[CreditAvailable]  
    ,a.[AvailableLiters]  
    ,a.[StatusId]  
    ,b.[RowOrder] AS [Step]  
    ,b.[Name] AS [StatusName]  
    ,d.[Symbol] AS [CurrencySymbol]  
    ,c.[IssueForId]  
    ,e.[UserId]  
    ,g.[VehicleId]  
    ,CASE c.[CreditCardType]  
     WHEN 'C' THEN 'Tarjeta de Cr�dito'  
     WHEN 'D' THEN 'Tarjeta de D�bito'  
     ELSE 'Tarjeta de Cr�dito'  
    END AS [CreditCardType]  
    ,f.Name AS [EncryptedDriverName]  
    ,@dospinosLetter + CASE WHEN c.[CustomerId] = 30   
        THEN h.[Name] + ' - ' + h.[PlateId]  
        ELSE h.[PlateId]  
        END AS [VehiclePlate]  
    ,a.CardRequestId  
    ,i.[EstimatedDelivery]  
    ,i.[InsertDate]  
    --,a.[RowVersion]  
    ,b.[Usage]  
    ,ISNULL((  
       SELECT TOP 1 p.[PartnerId]  
       FROM [General].[CustomersByPartner] p  
       WHERE p.[CustomerId] = a.[CustomerId]  
      ), 0) [PartnerId]  
    ,j.[Name] AS [Country]  
    ,0 [PaymentInstrumentType]  
    ,'Tarjeta' [PaymentInstrumentTypeName]  
  FROM [Control].[CreditCard] a  
  INNER JOIN [General].[Status] b   
   ON b.[StatusId] = a.[StatusId]  
  INNER JOIN [General].[Customers] c   
   ON c.[CustomerId] = a.[CustomerId]  
  INNER JOIN [Control].[Currencies] d   
   ON d.[CurrencyId] = c.[CurrencyId]  
  LEFT JOIN [Control].[CreditCardByDriver] e   
   ON e.[CreditCardId] = a.[CreditCardId]  
  LEFT JOIN [General].[Users] f   
   ON f.[UserId] = e.[UserId]  
  LEFT JOIN [Control].[CreditCardByVehicle] g   
   ON g.[CreditCardId] = a.[CreditCardId]  
  LEFT JOIN [General].[Vehicles] h   
   ON h.[VehicleId] = g.[VehicleId]  
  INNER JOIN [Control].[CardRequest] i   
   ON a.[CardRequestId] = i.[CardRequestId]  
  INNER JOIN [General].[Countries] j  
   ON c.[CountryId] = j.[CountryId]  
  WHERE (@pCustomerId = 0 OR (@pCustomerId = 0 AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')  
      OR (@pCustomerId IS NULL AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')  
      OR a.[CustomerId] = @pCustomerId)  
  AND (@count = 0 OR g.[VehicleId] IN (  
            SELECT items  
            FROM @Results  
           )) -- DYNAMIC FILTER    
  AND (@pCreditCardId IS NULL OR a.[CreditCardId] = @pCreditCardId)  
  AND (@pKey IS NULL  
   OR f.[Name] LIKE '%' + @pKey + '%'  
   OR h.[PlateId] LIKE '%' + @pKey + '%'    
   OR Convert(VARCHAR(50), a.[CreditAvailable]) LIKE '%' + @pKey + '%'  
   OR Convert(VARCHAR(50), a.[ExpirationYear]) LIKE '%' + @pKey + '%'  
   OR a.[CreditCardNumber] = @pKey  
   OR j.[Name] LIKE '%' + @pKey + '%')  
  AND (@pDriverId IS NULL OR @pDriverId = e.[UserId])  
  AND a.[StatusId] NOT IN (  
         SELECT [Id]  
         FROM @lStatusTable  
         )  
  AND (@pStatusId = - 1 OR @pStatusId IS NULL OR a.[StatusId] = @pStatusId)  
  ORDER BY [CreditCardId] DESC  
 END  
 ELSE  
 BEGIN  
  IF @pPaymentInstrumentType = 1  
  BEGIN  
   INSERT INTO #CardDataTable  
   SELECT  a.[Id] as [CreditCardId] --Identity de Sticker  
     ,a.[CustomerId]  
     ,c.[Name] AS [EncryptedCustomerName]  
     ,a.[Code]  
     ,0 [ExpirationYear]  
     ,0 [ExpirationMonth]  
     ,ISNULL(a.[CreditAvailable], 0) [CreditAvailable]  
     ,ISNULL(a.[AvailableLiters], 0) [AvailableLiters]  
     ,CONVERT(INT, a.[StatusId]) [StatusId]  
     ,b.[RowOrder] AS [Step]  
     ,b.[Name] AS [StatusName]  
     ,d.[Symbol] AS [CurrencySymbol]  
     ,c.[IssueForId]  
     ,x.[UserId]  
     ,x.[VehicleId]  
     ,xt.Name AS [CreditCardType]  
     ,f.Name AS [EncryptedDriverName]  
     ,@dospinosLetter + CASE WHEN c.[CustomerId] = 30   
         THEN h.[Name] + ' - ' + h.[PlateId]  
         ELSE h.[PlateId]  
         END AS [VehiclePlate]  
     ,x.CardRequestId  
     ,i.[EstimatedDelivery]  
     ,i.[InsertDate]  
     --,'' [RowVersion]  
     ,b.[Usage]  
     ,ISNULL((  
        SELECT TOP 1 p.[PartnerId]  
        FROM [General].[CustomersByPartner] p  
        WHERE p.[CustomerId] = a.[CustomerId]  
       ), 0) [PartnerId]  
     ,j.[Name] AS [Country]  
     ,xt.Id [PaymentInstrumentType]  
     ,xt.Name [PaymentInstrumentTypeName]  
   FROM [Control].[PaymentInstruments] a  
   INNER JOIN [Control].[PaymentInstrumentsByType] x  
    ON x.[PaymentInstrumentId] = a.[Id]  
   INNER JOIN [Control].[PaymentInstrumentsTypes] xt  
    ON xt.Id = a.TypeId  
   INNER JOIN [General].[Status] b   
    ON b.[StatusId] = a.[StatusId]  
   INNER JOIN [General].[Customers] c   
    ON c.[CustomerId] = a.[CustomerId]  
   INNER JOIN [Control].[Currencies] d   
    ON d.[CurrencyId] = c.[CurrencyId]  
   LEFT JOIN [General].[Users] f   
    ON f.[UserId] = x.[UserId]  
   LEFT JOIN [General].[Vehicles] h   
    ON h.[VehicleId] = x.[VehicleId]  
   INNER JOIN [Control].[CardRequest] i   
    ON x.[CardRequestId] = i.[CardRequestId]  
   INNER JOIN [General].[Countries] j  
    ON c.[CountryId] = j.[CountryId]  
   WHERE   
   (@pCustomerId = 0 OR (@pCustomerId = 0 AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')  
       OR (@pCustomerId IS NULL AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')  
       OR a.[CustomerId] = @pCustomerId)  
   AND (@count = 0 OR x.[VehicleId] IN (  
             SELECT items  
             FROM @Results  
            )) -- DYNAMIC FILTER    
   AND (@pCreditCardId IS NULL OR a.[Code] = (SELECT [Code]   
                FROM [Control].[PaymentInstruments]   
                WHERE [Id] = @pCreditCardId))  
   AND   
   (@pKey IS NULL  
    OR f.[Name] LIKE '%' + @pKey + '%'  
    OR h.[PlateId] LIKE '%' + @pKey + '%'    
    OR a.[Code] = @pKey  
    OR j.[Name] LIKE '%' + @pKey + '%')  
   AND (@pDriverId IS NULL OR @pDriverId = x.[UserId])  
   AND a.[StatusId] NOT IN (  
          SELECT [Id]  
          FROM @lStatusTable  
          )  
   AND (@pStatusId = - 1 OR @pStatusId IS NULL OR a.[StatusId] = @pStatusId)  
  END  
  ELSE  
  BEGIN  
   INSERT INTO #CardDataTable  
   SELECT  a.[CreditCardId]  
     ,a.[CustomerId]  
     ,c.[Name] AS [EncryptedCustomerName]  
     ,a.[CreditCardNumber]  
     ,a.[ExpirationYear]  
     ,a.[ExpirationMonth]  
     ,ISNULL(a.[CreditAvailable], 0) [CreditAvailable]  
     ,ISNULL(a.[AvailableLiters], 0) [AvailableLiters]  
     ,a.[StatusId]  
     ,b.[RowOrder] AS [Step]  
     ,b.[Name] AS [StatusName]  
     ,d.[Symbol] AS [CurrencySymbol]  
     ,c.[IssueForId]  
     ,e.[UserId]  
     ,g.[VehicleId]  
     ,CASE c.[CreditCardType]  
      WHEN 'C' THEN 'Tarjeta de Cr�dito'  
      WHEN 'D' THEN 'Tarjeta de D�bito'  
      ELSE 'Tarjeta de Cr�dito'  
     END AS [CreditCardType]  
     ,f.Name AS [EncryptedDriverName]  
     ,@dospinosLetter + CASE WHEN c.[CustomerId] = 30   
         THEN h.[Name] + ' - ' + h.[PlateId]  
         ELSE h.[PlateId]  
         END AS [VehiclePlate]  
     ,a.CardRequestId  
     ,i.[EstimatedDelivery]  
     ,i.[InsertDate]  
     --,a.[RowVersion]  
     ,b.[Usage]  
     ,ISNULL((  
        SELECT TOP 1 p.[PartnerId]  
        FROM [General].[CustomersByPartner] p  
        WHERE p.[CustomerId] = a.[CustomerId]  
       ), 0) [PartnerId]  
     ,j.[Name] AS [Country]  
     ,0 [PaymentInstrumentType]  
     ,'Tarjeta' [PaymentInstrumentTypeName]  
   FROM [Control].[CreditCard] a  
   INNER JOIN [General].[Status] b   
    ON b.[StatusId] = a.[StatusId]  
   INNER JOIN [General].[Customers] c   
    ON c.[CustomerId] = a.[CustomerId]  
   INNER JOIN [Control].[Currencies] d   
    ON d.[CurrencyId] = c.[CurrencyId]  
   LEFT JOIN [Control].[CreditCardByDriver] e   
    ON e.[CreditCardId] = a.[CreditCardId]  
   LEFT JOIN [General].[Users] f   
    ON f.[UserId] = e.[UserId]  
   LEFT JOIN [Control].[CreditCardByVehicle] g   
    ON g.[CreditCardId] = a.[CreditCardId]  
   LEFT JOIN [General].[Vehicles] h   
    ON h.[VehicleId] = g.[VehicleId]  
   INNER JOIN [Control].[CardRequest] i   
    ON a.[CardRequestId] = i.[CardRequestId]  
   INNER JOIN [General].[Countries] j  
    ON c.[CountryId] = j.[CountryId]  
   WHERE (@pCustomerId = 0 OR (@pCustomerId = 0 AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')  
       OR (@pCustomerId IS NULL AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')  
       OR a.[CustomerId] = @pCustomerId)  
   AND (@count = 0 OR g.[VehicleId] IN (  
             SELECT items  
             FROM @Results  
            )) -- DYNAMIC FILTER    
   AND (@pCreditCardId IS NULL OR a.[CreditCardId] = @pCreditCardId)  
   AND (@pKey IS NULL  
    OR f.[Name] LIKE '%' + @pKey + '%'  
    OR h.[PlateId] LIKE '%' + @pKey + '%'    
    OR Convert(VARCHAR(50), a.[CreditAvailable]) LIKE '%' + @pKey + '%'  
    OR Convert(VARCHAR(50), a.[ExpirationYear]) LIKE '%' + @pKey + '%'  
    OR a.[CreditCardNumber] = @pKey  
    OR j.[Name] LIKE '%' + @pKey + '%')  
   AND (@pDriverId IS NULL OR @pDriverId = e.[UserId])  
   AND a.[StatusId] NOT IN (  
          SELECT [Id]  
          FROM @lStatusTable  
          )  
   AND (@pStatusId = - 1 OR @pStatusId IS NULL OR a.[StatusId] = @pStatusId)  
   ORDER BY [CreditCardId] DESC  
  END  
 END  
  
 SELECT *   
 FROM #CardDataTable  
 ORDER BY [Id]  
  
 DROP TABLE #CardDataTable  
  
 SET NOCOUNT OFF  
END  
  