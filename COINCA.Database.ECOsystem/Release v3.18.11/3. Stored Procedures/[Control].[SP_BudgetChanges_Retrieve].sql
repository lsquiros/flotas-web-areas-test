/****** Object:  StoredProcedure [Control].[SP_BudgetChanges_Retrieve]    Script Date: 18/8/2020 12:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author: Stefano Quir�s Ruiz
-- Create date: 29/09/2018
-- Description:	Retrieve the BudgetChanges of a Client
-- ======================================================================================================
-- Maria de los Angeles Jimenez Chavarria - APR/03/2019 - Send report program. Include [UnitOfCapacityName]
-- Stefano Quir�s Ruiz - 23/08/2019 - Change the Filters
-- Luis Rojas 18/08/2020 - Filter for the details of the movements by conductors
-- ======================================================================================================

ALTER PROCEDURE [Control].[SP_BudgetChanges_Retrieve] --30, null, null, 2020, 7, null, null, null, 941, null
(
	@pCustomerId INT,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL,
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pCostCenterId INT = NULL,
	@pVehicleGroupId INT = NULL,
	@pUserId INT = NULL,  --It is not used but its necessary for job
	@pVehicleId INT = NULL, --Puede ser DriverId o VeehicleId depende de la configuracion del cliente
	@pFuelId INT = NULL,
	@pDriverId INT = NULL
) 
AS
BEGIN
	SET NOCOUNT ON

	IF @pMonth IS NOT NULL AND @pYear IS NOT NULL
	BEGIN
		SELECT @pStartDate = CAST(CAST(@pYear AS varchar) + '-' + CAST(@pMonth AS varchar) + '-' + CAST(1 AS varchar) AS DATETIME) 
		SELECT @pEndDate = CAST(CAST(@pYear AS varchar) + '-' + CAST(@pMonth + 1 AS varchar) + '-' + CAST(1 AS varchar) AS DATETIME)
	END

	DECLARE @lConvertionValue FLOAT = 3.78541

	DECLARE @pDistributionType INT = (SELECT [IssueForId] 
									  FROM [General].[Customers] 
									  WHERE [CustomerId] = @pCustomerId)

	DECLARE @lUnitCapacityName VARCHAR(100) 
	       ,@lUnitCapacityId INT

	
	SELECT TOP 1 @lUnitCapacityId = c.[UnitOfCapacityId] 
				,@lUnitCapacityName = d.[Name]
	FROM [General].[Customers] c
	INNER JOIN [General].[Types] d
		ON d.[TypeId] = c.[UnitOfCapacityId]
	WHERE [CustomerId] = @pCustomerId

	IF @pDistributionType = 101 --Es vehiculo
	BEGIN
		IF @pVehicleId IS NOT NULL
		BEGIN
			SELECT e.[UserId] 
				  ,u.[Name] [UserName]
				  ,c.[Name] [EncryptCustomerName]
				  ,@pCostCenterId [CostCenterId]
				  ,@pVehicleGroupId [VehicleGroupId]
				  ,vcc.[Name] [CostCenterName]
				  --,vg.[Name] AS [VehicleGroupName]
				  ,v.[Name] [VehicleName] 
				  ,v.[PlateId] [PlateId] 
				  ,CONVERT(DECIMAL(16,3), CASE WHEN @lUnitCapacityId = 1 
										  THEN (CASE WHEN [Controller] = 'VehicleFuelByGroup' 
										  		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
										  		 THEN CONVERT(DECIMAL(16, 3), JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Liters')) 
										  		 ELSE 0 
										  		 END) / @lConvertionValue
										  ELSE (CASE WHEN [Controller] = 'VehicleFuelByGroup' 
										  		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
										  		 THEN CONVERT(DECIMAL(16, 3), JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Liters')) 
										  		 ELSE 0 
										  		 END)
										  END) [Liters]
				  ,CASE WHEN [Controller] = 'VehicleFuelByGroup' 
						 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
				   THEN CONVERT(DECIMAL(16, 2),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Amount')) 
				   ELSE 0
				   END [Amount] 
				  ,CONVERT(DECIMAL(16,3), CASE WHEN @lUnitCapacityId = 1
									      THEN (CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
									      		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
									      		 THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalLiters')) 
									      		 ELSE 0
									      		 END) / @lConvertionValue
									      ELSE (CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
									      		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
									      		 THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalLiters')) 
									      		 ELSE 0
									      		 END)
									      END) [AdditionalLiters]
				  ,CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
						 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
				   THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalAmount')) 
				   ELSE 0
				   END [AdditionalAmount]
				  ,CONVERT(VARCHAR(100), @pStartDate ) + ' - ' + CONVERT(VARCHAR(100), CASE WHEN @pMonth IS NOT NULL AND @pYear IS NOT NULL
																					   THEN DATEADD(mi, -1, @pEndDate)
																					   ELSE @pEndDate
																					   END) [Dates]
				  ,e.[LogDateTime] [InsertDate]
				  ,@lUnitCapacityName [UnitOfCapacityName]
			FROM [General].[EventLog] e
			INNER JOIN [General].[Users] u
				ON  e.[CustomerId] = @pCustomerId 
				AND e.[UserId] = u.[UserId]
			INNER JOIN [General].[Customers] c
				ON c.[CustomerId] = e.[CustomerId]
			INNER JOIN [General].[Vehicles] v
				ON v.[VehicleId] = CASE WHEN NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
								   THEN JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.VehicleId')
								   ELSE 0
								   END
			INNER JOIN [General].[VehicleCostCenters] vcc
				ON vcc.[CostCenterId] = v.[CostCenterId]
			--LEFT JOIN [General].[VehiclesByGroup] vbg ON v.[VehicleId] = vbg.[VehicleId]
			--LEFT JOIN [General].[VehicleGroups] vg ON vbg.[VehicleGroupId] = vg.[VehicleGroupId] 
			WHERE e.[CustomerId] = @pCustomerId 
			AND e.[LogDateTime] BETWEEN @pStartDate AND @pEndDate	
			AND [EventType] = 'Informative'
			AND [Action] = 'Save'
			AND [Controller] IN ('VehicleFuelByGroup', 'AdditionalVehicleFuelByGroup')
			--AND [Mensaje] like '%Modificaciones en la distribucion de combustible:%'		
			AND (@pCostCenterId IS NULL OR v.[CostCenterId] = @pCostCenterId)
			--AND (@pVehicleGroupId IS NULL OR vg.[VehicleGroupId] = @pVehicleGroupId)
			AND (@pVehicleId IS NULL OR v.[VehicleId] = @pVehicleId)
			AND (@pFuelId IS NULL 
				 OR @pFuelId = JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.FuelId'))
			ORDER BY [LogDateTime] DESC
		END
		ELSE
		BEGIN 
			SELECT e.[UserId] 
				  ,u.[Name] [UserName]
				  ,c.[Name] [EncryptCustomerName]
				  ,@pCostCenterId [CostCenterId]
				  ,@pVehicleGroupId [VehicleGroupId]
				  ,vcc.[Name] [CostCenterName]
				  --,vg.[Name] AS [VehicleGroupName]
				  ,v.[Name] [VehicleName] 
				  ,v.[PlateId] [PlateId] 
				  ,CONVERT(DECIMAL(16,3), CASE WHEN @lUnitCapacityId = 1 
										  THEN (CASE WHEN [Controller] = 'VehicleFuelByGroup' 
										  		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
										  		 THEN CONVERT(DECIMAL(16, 3), JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Liters')) 
										  		 ELSE 0 
										  		 END) / @lConvertionValue
										  ELSE (CASE WHEN [Controller] = 'VehicleFuelByGroup' 
										  		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
										  		 THEN CONVERT(DECIMAL(16, 3), JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Liters')) 
										  		 ELSE 0 
										  		 END)
										  END) [Liters]
				  ,CASE WHEN [Controller] = 'VehicleFuelByGroup' 
						 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
				   THEN CONVERT(DECIMAL(16, 2),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Amount')) 
				   ELSE 0
				   END [Amount] 
				  ,CONVERT(DECIMAL(16,3), CASE WHEN @lUnitCapacityId = 1
									      THEN (CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
									      		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
									      		 THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalLiters')) 
									      		 ELSE 0
									      		 END) / @lConvertionValue
									      ELSE (CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
									      		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
									      		 THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalLiters')) 
									      		 ELSE 0
									      		 END)
									      END) [AdditionalLiters]
				  ,CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
						 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
				   THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalAmount')) 
				   ELSE 0
				   END [AdditionalAmount]
				  ,CONVERT(VARCHAR(100), @pStartDate ) + ' - ' + CONVERT(VARCHAR(100), CASE WHEN @pMonth IS NOT NULL AND @pYear IS NOT NULL
																					   THEN DATEADD(mi, -1, @pEndDate)
																					   ELSE @pEndDate
																					   END) [Dates]
				  ,e.[LogDateTime] [InsertDate]
				  ,@lUnitCapacityName [UnitOfCapacityName]
			FROM [General].[EventLog] e
			INNER JOIN [General].[Users] u
				ON  e.[CustomerId] = @pCustomerId 
				AND e.[UserId] = u.[UserId]
			INNER JOIN [General].[Customers] c
				ON c.[CustomerId] = e.[CustomerId]
			INNER JOIN [General].[Vehicles] v
				ON v.[VehicleId] = CASE WHEN NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
								   THEN JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.VehicleId')
								   ELSE 0
								   END
			INNER JOIN [General].[VehicleCostCenters] vcc
				ON vcc.[CostCenterId] = v.[CostCenterId]
			--LEFT JOIN [General].[VehiclesByGroup] vbg ON v.[VehicleId] = vbg.[VehicleId]
			--LEFT JOIN [General].[VehicleGroups] vg ON vbg.[VehicleGroupId] = vg.[VehicleGroupId] 
			WHERE e.[CustomerId] = @pCustomerId 
			AND e.[LogDateTime] BETWEEN @pStartDate AND @pEndDate	
			AND [EventType] = 'Informative'
			AND [Action] = 'Save'
			AND [Controller] IN ('VehicleFuelByGroup', 'AdditionalVehicleFuelByGroup')
			--AND [Mensaje] like '%Modificaciones en la distribucion de combustible:%'		
			AND (@pCostCenterId IS NULL OR v.[CostCenterId] = @pCostCenterId)
			--AND (@pVehicleGroupId IS NULL OR vg.[VehicleGroupId] = @pVehicleGroupId)
			ORDER BY [LogDateTime] DESC
		END
	END
	ELSE --Conductor
	BEGIN
		IF @pVehicleId IS NOT NULL
		BEGIN
			SELECT e.[UserId] 
				  ,u.[Name] [UserName]
				  ,c.[Name] [EncryptCustomerName]
				  ,@pCostCenterId [CostCenterId]
				  ,@pVehicleGroupId [VehicleGroupId]
				  ,vcc.[Name] [CostCenterName]
				  --,vg.[Name] AS [VehicleGroupName]
				  ,u2.[Name] [EncryptedDriver] 
				  ,u2.[Name] [VehicleName]
				  ,'N/A' [PlateId] 
				  ,CONVERT(DECIMAL(16,3), CASE WHEN @lUnitCapacityId = 1 
											  THEN (CASE WHEN [Controller] = 'VehicleFuelByGroup' 
											  		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
											  		 THEN CONVERT(DECIMAL(16, 3), JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Liters')) 
											  		 ELSE 0 
											  		 END) / @lConvertionValue
											  ELSE (CASE WHEN [Controller] = 'VehicleFuelByGroup' 
											  		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
											  		 THEN CONVERT(DECIMAL(16, 3), JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Liters')) 
											  		 ELSE 0 
											  		 END)
											  END) [Liters]
					  ,CASE WHEN [Controller] = 'VehicleFuelByGroup' 
							 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
					   THEN CONVERT(DECIMAL(16, 2),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Amount')) 
					   ELSE 0
					   END [Amount] 
					  ,CONVERT(DECIMAL(16,3), CASE WHEN @lUnitCapacityId = 1
										      THEN (CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
										      		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
										      		 THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalLiters')) 
										      		 ELSE 0
										      		 END) / @lConvertionValue
										      ELSE (CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
										      		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
										      		 THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalLiters')) 
										      		 ELSE 0
										      		 END)
										      END) [AdditionalLiters]
				  ,CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
						 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
				   THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalAmount')) 
				   ELSE 0
				   END [AdditionalAmount]
				  ,CONVERT(VARCHAR(100), @pStartDate ) + ' - ' + CONVERT(VARCHAR(100), CASE WHEN @pMonth IS NOT NULL AND @pYear IS NOT NULL
																					   THEN DATEADD(mi, -1, @pEndDate)
																					   ELSE @pEndDate
																					   END) [Dates]
				  ,e.[LogDateTime] [InsertDate]
				  ,@lUnitCapacityName [UnitOfCapacityName]
			FROM [General].[EventLog] e
			INNER JOIN [General].[Users] u
				ON  e.[CustomerId] = @pCustomerId 
				AND e.[UserId] = u.[UserId]
			INNER JOIN [General].[Customers] c
				ON c.[CustomerId] = e.[CustomerId]
			INNER JOIN [General].[DriversUsers] du
				ON du.[UserId] = CASE WHEN NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
								   THEN JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.DriverId')
								   ELSE 0
								   END
			INNER JOIN [General].[Users] u2
				ON u2.[UserId] = du.[UserId]
			LEFT JOIN [General].[VehiclesDrivers] vd ON vd.[UserId] = u2.[UserId]
			LEFT JOIN [General].[Vehicles] v ON v.[VehicleId] = vd.[VehicleId]
			LEFT JOIN [General].[VehicleCostCenters] vcc ON vcc.[CostCenterId] = v.[CostCenterId]
			--LEFT JOIN [General].[VehiclesByGroup] vbg ON v.[VehicleId] = vbg.[VehicleId]
			--LEFT JOIN [General].[VehicleGroups] vg ON vbg.[VehicleGroupId] = vg.[VehicleGroupId] 
			WHERE e.[CustomerId] = @pCustomerId 
			AND e.[LogDateTime] BETWEEN @pStartDate AND @pEndDate	
			AND [EventType] = 'Informative'
			AND [Action] = 'Save'
			AND [Controller] IN ('VehicleFuelByGroup', 'AdditionalVehicleFuelByGroup')
			--AND [Mensaje] like '%Modificaciones en la distribucion de combustible:%'
			AND (@pCostCenterId IS NULL OR v.[CostCenterId] = @pCostCenterId)
			--AND (@pVehicleGroupId IS NULL OR vg.[VehicleGroupId] = @pVehicleGroupId)
			AND (@pVehicleId IS NULL OR du.[UserId] = @pVehicleId)
			
			ORDER BY [LogDateTime] DESC
		END
		ELSE
		BEGIN
			SELECT e.[UserId] 
				  ,u.[Name] [UserName]
				  ,c.[Name] [EncryptCustomerName]
				  ,@pCostCenterId [CostCenterId]
				  ,@pVehicleGroupId [VehicleGroupId]
				  ,vcc.[Name] [CostCenterName]
				  --,vg.[Name] AS [VehicleGroupName]
				  ,u2.[Name] [EncryptedDriver] 
				  ,u2.[Name] [VehicleName]
				  ,'N/A' [PlateId] 
				  ,CONVERT(DECIMAL(16,3), CASE WHEN @lUnitCapacityId = 1 
											  THEN (CASE WHEN [Controller] = 'VehicleFuelByGroup' 
											  		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
											  		 THEN CONVERT(DECIMAL(16, 3), JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Liters')) 
											  		 ELSE 0 
											  		 END) / @lConvertionValue
											  ELSE (CASE WHEN [Controller] = 'VehicleFuelByGroup' 
											  		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
											  		 THEN CONVERT(DECIMAL(16, 3), JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Liters')) 
											  		 ELSE 0 
											  		 END)
											  END) [Liters]
					  ,CASE WHEN [Controller] = 'VehicleFuelByGroup' 
							 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
					   THEN CONVERT(DECIMAL(16, 2),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.Amount')) 
					   ELSE 0
					   END [Amount] 
					  ,CONVERT(DECIMAL(16,3), CASE WHEN @lUnitCapacityId = 1
										      THEN (CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
										      		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
										      		 THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalLiters')) 
										      		 ELSE 0
										      		 END) / @lConvertionValue
										      ELSE (CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
										      		 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
										      		 THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalLiters')) 
										      		 ELSE 0
										      		 END)
										      END) [AdditionalLiters]
				  ,CASE WHEN [Controller] = 'AdditionalVehicleFuelByGroup' 
						 AND NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
				   THEN CONVERT(DECIMAL(16, 3),JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.AdditionalAmount')) 
				   ELSE 0
				   END [AdditionalAmount]
				  ,CONVERT(VARCHAR(100), @pStartDate ) + ' - ' + CONVERT(VARCHAR(100), CASE WHEN @pMonth IS NOT NULL AND @pYear IS NOT NULL
																					   THEN DATEADD(mi, -1, @pEndDate)
																					   ELSE @pEndDate
																					   END) [Dates]
				  ,e.[LogDateTime] [InsertDate]
				  ,@lUnitCapacityName [UnitOfCapacityName]
			FROM [General].[EventLog] e
			INNER JOIN [General].[Users] u
				ON  e.[CustomerId] = @pCustomerId 
				AND e.[UserId] = u.[UserId]
			INNER JOIN [General].[Customers] c
				ON c.[CustomerId] = e.[CustomerId]
			INNER JOIN [General].[DriversUsers] du
				ON du.[UserId] = CASE WHEN NULLIF(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''), '') IS NOT NULL
								   THEN JSON_VALUE(REPLACE(REPLACE(REPLACE([Mensaje], 'Modificaciones en la distribucion de combustible:', ''), '[' , ''), ']', ''),'$.DriverId')
								   ELSE 0
								   END
			INNER JOIN [General].[Users] u2
				ON u2.[UserId] = du.[UserId]
			LEFT JOIN [General].[VehiclesDrivers] vd ON vd.[UserId] = u2.[UserId]
			LEFT JOIN [General].[Vehicles] v ON v.[VehicleId] = vd.[VehicleId]
			LEFT JOIN [General].[VehicleCostCenters] vcc ON vcc.[CostCenterId] = v.[CostCenterId]
			--LEFT JOIN [General].[VehiclesByGroup] vbg ON v.[VehicleId] = vbg.[VehicleId]
			--LEFT JOIN [General].[VehicleGroups] vg ON vbg.[VehicleGroupId] = vg.[VehicleGroupId] 
			WHERE e.[CustomerId] = @pCustomerId 
			AND e.[LogDateTime] BETWEEN @pStartDate AND @pEndDate	
			AND [EventType] = 'Informative'
			AND [Action] = 'Save'
			AND [Controller] IN ('VehicleFuelByGroup', 'AdditionalVehicleFuelByGroup')
			--AND [Mensaje] like '%Modificaciones en la distribucion de combustible:%'
			AND (@pCostCenterId IS NULL OR v.[CostCenterId] = @pCostCenterId)
			--AND (@pVehicleGroupId IS NULL OR vg.[VehicleGroupId] = @pVehicleGroupId)
			ORDER BY [LogDateTime] DESC
		
		END
	END
	SET NOCOUNT OFF
END


