INSERT INTO [AspMenusByCustomers]
SELECT 237
      ,NULL
	  ,GETDATE()
	  ,[PartnerId]
FROM [General].[Partners]
WHERE [Name] not like '%flota%'
	  AND [Name] not like '%puma%'
      AND ([IsDeleted] = 0
	       OR [IsDeleted] IS NULL)