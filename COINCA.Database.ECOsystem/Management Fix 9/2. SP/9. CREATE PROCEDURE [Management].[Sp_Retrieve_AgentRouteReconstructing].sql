USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_AgentRouteReconstructing]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_AgentRouteReconstructing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 10/04/2018
-- Description:	Retrieve Agent Reconstructing Information
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_AgentRouteReconstructing]
(
	@pUserId INT = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	

	DECLARE @lShowLines BIT,
			@lLocation INT,
			@lLocationName VARCHAR(200),
			@lDeletedCommerceDesc VARCHAR(100)  

	--VALIDATES DATES IF THE DATES ARE THE SAME OR END DATE IS NULL
	IF @pStartDate = @pEndDate OR @pEndDate IS NULL SET @pEndDate = DATEADD(SECOND, 2, @pStartDate)

	SELECT @lShowLines = CASE WHEN p.[ReconstructionType] = 2 
							  THEN CAST(1 AS BIT)
							  ELSE CAST(0 AS BIT)
						 END
	FROM [Management].[Parameters] p 
	INNER JOIN [Management].[UserEvent] u
		ON p.[CustomerId] = u.[CustomerId]
	WHERE u.[UserId] = @pUserId

	SELECT @lDeletedCommerceDesc = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'DELETE_COMMERCE_DESC'

	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'

	SELECT @lLocationName = p.[MovementName]
	FROM [Management].[Parameters] p 
	INNER JOIN [Management].[UserEvent] u
		ON p.[CustomerId] = u.[CustomerId]
	WHERE u.[UserId] = @pUserId
	
	SELECT  u.[Id],
			u.[UserId],
			us.[Name] [EncryptName],
			u.[EventDetailId],
			ed.[Name] [EventDetailName],
			u.[EventTypeId],
			CASE WHEN u.[EventTypeId] = @lLocation
				 THEN ISNULL(@lLocationName, c.[Name])
				 ELSE ISNULL(ed.[Name], c.[Name]) 
			END [EventName],
			c.[Parent],
			u.[CommerceId],
			CASE WHEN co.[IsDeleted] = 1 
				THEN CONCAT(co.[Name], @lDeletedCommerceDesc) 
				ELSE co.[Name] 
		    END [CommerceName],
			u.[Latitude],
			u.[Longitude],
			u.[BatteryLevel],
			CAST(u.[Speed] AS INT) [Speed],
			u.[EventDate],
			[Efficiency].[Fn_GetAddressByLatLon_Retrieve](u.[Longitude], u.[Latitude], u.[CustomerId]) [Address],
			@lShowLines [ShowLines]	
	FROM [Management].[UserEvent] u
	INNER JOIN [Management].[CatalogDetail] c
		ON u.[EventTypeId] = c.[Id]
	LEFT JOIN [Management].[EventDetail] ed
		ON u.[EventDetailId] = ed.[Id]
	LEFT JOIN [General].[Users] us
		ON u.[UserId] = us.[UserId]
	LEFT JOIN [General].[Commerces] co
	    ON co.[Id] = u.[CommerceId]
	WHERE u.[UserId] = @pUserId 
	AND u.[EventDate] BETWEEN @pStartDate AND DATEADD(SECOND, -1, @pEndDate)
	AND u.[Latitude] IS NOT NULL
	AND u.[Delete] = 0 
	ORDER BY u.[EventDate]
	
    SET NOCOUNT OFF
END
