USE [ECOSystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Fn_GpsHobbsMeterByVehicle]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [General].[Fn_GpsHobbsMeterByVehicle]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 02/24/2015
-- Description:	Retrieve Hobbs meter Report By Vehicle
-- ================================================================================================
CREATE FUNCTION [General].[Fn_GpsHobbsMeterByVehicle](
	 @pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pIntrackReference INT
)
RETURNS @ResultTable TABLE
(
	 [HoursOn] DECIMAL(12,4)
	,[HoursOnStr] VARCHAR(400)
	,[StartOdometer] INT
	,[EndOdometer] INT
)
AS
BEGIN

	IF(@pYear IS NOT NULL AND @pMonth IS NOT NULL)
	BEGIN
		SELECT   @pStartDate = DATEADD(MONTH,@pMonth-1,DATEADD(YEAR,@pYear-1900,0)) /*First*/
				,@pEndDate = DATEADD(SECOND,-1,DATEADD(MONTH,@pMonth,DATEADD(YEAR,@pYear-1900,0))) /*Last*/
	END

	IF(@pStartDate IS NULL OR @pEndDate IS NULL)
	BEGIN
		RETURN
	END
	
	DECLARE @lDevice INT
	
	SELECT @lDevice = d.[Device]
	FROM [IntrackV2]..[Composiciones] b
		INNER JOIN [IntrackV2]..[DispositivosAVL] c
			ON b.[dispositivo] = c.[dispositivo]
		INNER JOIN  [172.22.220.38\BDS_WEB].[Atrack].[dbo].[Devices] d
			ON c.[numeroimei] = [UnitID]
	WHERE b.vehiculo = @pIntrackReference
	
	DECLARE @lTable TABLE
	(	 [Index] INT IDENTITY(1,1) NOT NULL
		,[Report] [BIGINT] NOT NULL
		,[Odometer] [FLOAT] NOT NULL
		,[IsOn] [BIT] NOT NULL
		,[GPSDateTime] [DATETIME] NOT NULL
		,[GroupFlat] INT NULL
	)
	
	INSERT INTO @lTable(
		[Report], [Odometer], [IsOn], [GPSDateTime]
	)
	SELECT
		 e.[Report]
		,e.[Odometer]
		,CASE WHEN e.[InputStatus] % 2 = 1 THEN 1 ELSE 0 END AS [IsOn]
		,e.[GPSDateTime]
	FROM [172.22.220.38\BDS_WEB].[Atrack].[dbo].[Reports] e
	WHERE e.[Device] = @lDevice
	  AND e.[GPSDateTime] BETWEEN @pStartDate AND @pEndDate
	ORDER BY e.[GPSDateTime] ASC
	
	DECLARE @lIndex INT = 1, @lMaxIndex INT, @lGroupFlat INT = 0, @lFlagPrev BIT = NULL, @lFlag BIT = NULL
	SELECT @lMaxIndex = COUNT(1) FROM @lTable t
	
	/*Update in order to group the information on interval on/off*/
	WHILE(@lIndex <= @lMaxIndex)
	BEGIN
		
		SELECT @lFlag = [IsOn] FROM @lTable WHERE [Index] = @lIndex 
		
		IF(@lFlagPrev IS NULL OR @lFlagPrev <> @lFlag)
		BEGIN
			SELECT @lFlagPrev = @lFlag, @lGroupFlat = @lGroupFlat + 1			
		END		
		
		UPDATE @lTable
			SET [GroupFlat] = @lGroupFlat
		WHERE [Index] = @lIndex
		
		SET @lIndex = @lIndex + 1
	END
	
	/*Calculate elapsed time*/
	DECLARE @lInitTime DATETIME = '20500101'
	
	UPDATE @lTable
		SET @lInitTime =  DATEADD(SECOND, t.GPSDateTimeDiff, @lInitTime)
	FROM 
		(SELECT [GroupFlat]
			  ,DATEDIFF(SECOND, MIN([GPSDateTime]), MAX([GPSDateTime])) AS GPSDateTimeDiff
			  ,MAX([Index]) AS [MaxIndex]
		FROM @lTable
		WHERE [IsOn] = 1
		GROUP BY [GroupFlat]) t
	WHERE [Index] = t.[MaxIndex]
	
	DECLARE @lTimeOn DECIMAL(12,4) = CONVERT(VARCHAR(400),DATEDIFF(SECOND, '20500101', @lInitTime) / 60.0 / 60.0)
	
	INSERT @ResultTable
	SELECT
		  @lTimeOn
		 ,RIGHT('0' + CAST (FLOOR(@lTimeOn) AS VARCHAR), 2) + ':' + 
			RIGHT('0' + CAST(FLOOR((((@lTimeOn * 3600) % 3600) / 60)) AS VARCHAR), 2) + ':' + 
			RIGHT('0' + CAST (FLOOR((@lTimeOn * 3600) % 60) AS VARCHAR), 2)
		,MIN(t.Odometer)
		,MAX(t.Odometer)
	FROM @lTable t
	
   RETURN
END
GO


