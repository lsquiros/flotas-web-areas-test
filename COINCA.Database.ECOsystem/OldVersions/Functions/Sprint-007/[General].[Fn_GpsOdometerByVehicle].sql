USE [ECOSystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Fn_GpsOdometerByVehicle]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [General].[Fn_GpsOdometerByVehicle]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 02/24/2015
-- Description:	Retrieve Odometer Gps By Vehicle
-- ================================================================================================
CREATE FUNCTION [General].[Fn_GpsOdometerByVehicle](
	 @pTrxDate DATETIME					--@pTrxDate: Trx Date
	,@pIntrackReference INT				--@pIntrackReference
)
RETURNS @ResultTable TABLE
(
	 [IntrackReference] INT NULL
	,[GpsOdometer] DECIMAL(16,2) NULL
)
AS
BEGIN
	DECLARE @lGpsOdometer DECIMAL(16,2) = NULL
	IF @pIntrackReference IS NULL
	BEGIN
		SET @lGpsOdometer = 0.00
	END
	ELSE
	BEGIN
	
		DECLARE @lDevice INT

		SELECT @lDevice = d.[Device]
		FROM [IntrackV2]..[Composiciones] b
			INNER JOIN [IntrackV2]..[DispositivosAVL] c
				ON b.[dispositivo] = c.[dispositivo]
			INNER JOIN  [172.22.220.38\BDS_WEB].[Atrack].[dbo].[Devices] d
				ON c.[numeroimei] = [UnitID]
		WHERE b.vehiculo = @pIntrackReference
	
		DECLARE @lIndex INT = 0, @lMinDate DATETIME = @pTrxDate, @lMaxDate DATETIME = @pTrxDate
		
		WHILE(@lIndex < 3 AND @lGpsOdometer IS NULL)
		BEGIN
			SELECT @lMinDate = DATEADD(MINUTE,-15,@lMinDate), @lMaxDate = DATEADD(MINUTE,15,@lMaxDate)
			
			SELECT
				TOP 1 @lGpsOdometer = e.[Odometer]
					FROM [172.22.220.38\BDS_WEB].[Atrack].[dbo].[Reports] e
			WHERE e.[Device] = @lDevice
			  AND e.GPSDateTime BETWEEN @lMinDate AND @lMaxDate
			ORDER BY DATEDIFF(SECOND, e.[GPSDateTime], @pTrxDate) ASC			
				
			SET @lIndex = @lIndex + 1
		END
	END
	
	INSERT	INTO @ResultTable([IntrackReference], [GpsOdometer])
			SELECT @pIntrackReference, @lGpsOdometer
	
	RETURN
END
GO


