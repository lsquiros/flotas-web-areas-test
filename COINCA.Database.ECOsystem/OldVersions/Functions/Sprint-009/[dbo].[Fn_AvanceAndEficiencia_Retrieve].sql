USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fn_AvanceAndEficiencia_Retrieve]') AND type in ( N'FN', N'IF', N'TF', N'FS', N'FT' ))
	DROP FUNCTION [dbo].[Fn_AvanceAndEficiencia_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Improvement: Gerald Solano
-- Create date: 20/7/2014
-- Description:	Retrieve Avance And Eficiencia information
-- ================================================================================================
CREATE FUNCTION  [dbo].[Fn_AvanceAndEficiencia_Retrieve]
(
	@UserId int,
	@CurrentDate datetime 
)
RETURNS @Result TABLE
	(
		TotalAvance float NULL,
		TotalEficiencia float NULL
	)
AS
BEGIN
	
	Declare @RouteId int = 0	
	Declare @CurrentDay int = DATEPART(DD, @CurrentDate)
	Declare @CurrentMonth int = DATEPART(MM, @CurrentDate)
	Declare @CurrentYear int = DATEPART(YY, @CurrentDate)
	DECLARE @CurrentRouteDay Char(1)

	DECLARE @CountPuntosRutaMain int = 0
	DECLARE @CountPuntosRutaUser int = 0
	DECLARE @CountPuntosRutaTotal int = 0
	DECLARE @CountPassControlPointAvance int = 0
	DECLARE @CountPassControlPointEficiencia int = 0
	DECLARE @CurrentDivingVehicle int = 0
	DECLARE @TotalEficiencia float = 0
	DECLARE @TotalAvance float = 0

	-- OBTENEMOS EL D�A ACTUAL
	SELECT @CurrentRouteDay = 
		   CASE DATENAME(weekday, @CurrentDate)  -- ESTABLECEMOS EL D�A A CONSULTAR EN LAS RUTAS
			 WHEN 'Monday' THEN 'M' 
			 WHEN 'Tuesday' THEN 'K' 
			 WHEN 'Wednesday' THEN 'M'
			 WHEN 'Thursday' THEN 'J'
			 WHEN 'Friday' THEN 'V' 
			 WHEN 'Saturday' THEN 'S' 
			 WHEN 'Sunday' THEN 'D'
			 ELSE ''
	END

	-- OBTENEMOS EL VEHICULO ACTUAL ASIGNADO
	SELECT TOP(1) @CurrentDivingVehicle = [VehicleId]
	FROM [General].[VehiclesByUser]
	WHERE [LastDateDriving] IS NULL AND UserId = @UserId
	ORDER BY [VehiclesByUserId] DESC

	--SELECT @CurrentDivingVehicle

	-- OBTENEMOS LA RUTA DEL VEHICULO
		SELECT @RouteId = r.[RouteId]
		FROM [Efficiency].[VehicleByRoute] vr
		INNER JOIN [Efficiency].[Routes] r ON r.RouteId = vr.RouteId
		WHERE [VehicleId] = @CurrentDivingVehicle AND 
			  [Days] LIKE '%'+ @CurrentRouteDay +'%' AND
			  [Active] = 1

	-- PUNTOS DE RUTA NO RELACIONADOS CON EL USUARIO
	SELECT @CountPuntosRutaMain = COUNT(*)
	FROM [Efficiency].[RoutesPoints] rp
	LEFT JOIN [General].[ClientVisit] cv 
		ON cv.[RoutePointId] = rp.[PointId] 
	WHERE rp.[RouteId] = @RouteId AND cv.[RoutePointId] IS NULL


	-- PUNTOS DE RUTA RELACIONADOS CON EL USUARIO
	SELECT @CountPuntosRutaUser = COUNT(*)
	FROM [Efficiency].[RoutesPoints] rp
	INNER JOIN [General].[ClientVisit] cv 
		ON cv.[RoutePointId] = rp.[PointId] AND
			cv.UserId = @UserId	-- PUNTOS DE VISITA LIGADOS A UN USUARIO
	WHERE rp.[RouteId] = @RouteId

	SET @CountPuntosRutaTotal = @CountPuntosRutaMain + @CountPuntosRutaUser

	-- CANTIDAD DE PUNTOS DE LA RUTA PRINCIPAL Y LA DE CLIENTES
	SELECT @CountPassControlPointAvance = COUNT(*) FROM [Efficiency].[PassControl] pc
	INNER JOIN [Efficiency].[RoutesPoints] rp 
		ON rp.[RouteId] = @RouteId AND rp.[PointId] = pc.[PointId]
	WHERE [State] <> 'PENDIENTE' AND 
		  [ControlDay] = @CurrentDay AND
		  [ControlMonth] = @CurrentMonth AND
		  [ControlYear] = @CurrentYear AND
		  [VehicleId] = @CurrentDivingVehicle


	-- CANTIDAD DE PUNTOS DE LA RUTA DE CLIENTES
	SELECT @CountPassControlPointEficiencia = COUNT(*) FROM [Efficiency].[PassControl] pc
	INNER JOIN [General].[ClientVisit] cv 
		ON cv.[RoutePointId] = pc.[PointId]
	INNER JOIN [Efficiency].[RoutesPoints] rp 
		ON rp.[RouteId] = @RouteId AND rp.[PointId] = pc.[PointId]
	WHERE cv.[State] = 1 AND
		  [ControlDay] = @CurrentDay AND
		  [ControlMonth] = @CurrentMonth AND
		  [ControlYear] = @CurrentYear AND
		  [VehicleId] = @CurrentDivingVehicle

	IF  @CountPuntosRutaTotal > 0
	BEGIN
		SET  @TotalAvance = (@CountPassControlPointAvance * 100) / @CountPuntosRutaTotal
	END 

	IF  @CountPuntosRutaUser > 0
	BEGIN
		SET  @TotalEficiencia = (@CountPassControlPointEficiencia * 100) / @CountPuntosRutaUser
	END 

	INSERT INTO @Result(TotalAvance, TotalEficiencia)
	SELECT @TotalAvance, @TotalEficiencia

	RETURN

    --SET NOCOUNT OFF
END

GO


