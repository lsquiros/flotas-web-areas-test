USE [ECOsystem]

--Fix the issue with the credit cards
INSERT INTO [dbo].[AspNetRolePermissions] VALUES ('4b356447-c286-45d6-bace-d1cc1ae298db', '2CC4F513-C174-4911-87D8-84B0564669A8')

--Search PreventiveMaintenanceVehicle
INSERT INTO [dbo].[AspNetRolePermissions] VALUES('9baac678-473a-4f96-9d67-9f2a5af778f3', 'D7E4503A-ACF8-4C69-A769-19CE61D710D8')
INSERT INTO [dbo].[AspNetRolePermissions] VALUES('F024C5BD-C5F4-408C-8A86-B5DBA55435E0', 'D7E4503A-ACF8-4C69-A769-19CE61D710D8')
