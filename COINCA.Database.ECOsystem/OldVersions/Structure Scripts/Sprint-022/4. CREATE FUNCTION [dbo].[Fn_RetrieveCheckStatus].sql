USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fn_RetrieveCheckStatus]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[Fn_RetrieveCheckStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 12/07/2015
-- Description:	Return True Or False Based on the results we get from the  ASPNET User Permissions
-- ================================================================================================
CREATE FUNCTION [dbo].[Fn_RetrieveCheckStatus]
(
	@pPermissionId VARCHAR(128),
	@pRoleName VARCHAR(MAX)
)
RETURNS VARCHAR(10)
AS
BEGIN
	
	DECLARE @Result VARCHAR(10)
	DECLARE @COUNTER INT 
	DECLARE @Controller VARCHAR(100) 
	DECLARE @RoleId VARCHAR(128)
	DECLARE @Action VARCHAR(128)

	SET @Controller = (SELECT [Resource] FROM [dbo].[AspNetPermissions] WHERE [Id] = @pPermissionId)
	SET @Action = (SELECT [Action] FROM [dbo].[AspNetPermissions] WHERE [Id] = @pPermissionId)
	SET @RoleId = (SELECT [Id] FROM [dbo].[AspNetRoles]  WHERE [Name] =  @pRoleName)

	IF @Controller <> 'CreditCard'
	BEGIN
			SET @COUNTER = (SELECT COUNT(ANP.[Action]) FROM [dbo].[AspNetPermissions] AS ANP 
			INNER JOIN [dbo].[AspNetRolePermissions] AS ANRP
			ON ANP.[Id] = ANRP.[PermissionId]
			WHERE ANP.[Resource] = @Controller AND ANRP.[RoleId] = @RoleId AND [Action] LIKE 'Index%')

			IF (@COUNTER = 1)
			BEGIN
				SET @Result = CONCAT(@Result, 'V,')
			END

			SET @COUNTER = (SELECT COUNT(ANP.[Action]) FROM [dbo].[AspNetPermissions] AS ANP 
			INNER JOIN [dbo].[AspNetRolePermissions] AS ANRP
			ON ANP.[Id] = ANRP.[PermissionId]
			WHERE ANP.[Resource] = @Controller AND ANRP.[RoleId] = @RoleId AND [Action] LIKE 'Admin%')

			IF (@COUNTER = 1)
			BEGIN
				SET @Result = CONCAT(@Result, 'V,')
			END
	END
	ELSE
	BEGIN
		IF @Action = 'TransactionRules'
		BEGIN
			SET @COUNTER = (SELECT COUNT(ANP.[Action]) FROM [dbo].[AspNetPermissions] AS ANP 
			INNER JOIN [dbo].[AspNetRolePermissions] AS ANRP
			ON ANP.[Id] = ANRP.[PermissionId]
			WHERE ANP.[Resource] = @Controller AND ANRP.[RoleId] = @RoleId AND [Action] = @Action)

			IF (@COUNTER = 1)
			BEGIN
				SET @Result = CONCAT(@Result, 'V,')
			END
		END
		ELSE
		BEGIN
			DECLARE @FLAG INT = 0

			IF @Action <> 'Admin'
			BEGIN

					SET @COUNTER = (SELECT COUNT(ANP.[Action]) FROM [dbo].[AspNetPermissions] AS ANP 
					INNER JOIN [dbo].[AspNetRolePermissions] AS ANRP
					ON ANP.[Id] = ANRP.[PermissionId]
					WHERE ANP.[Resource] = @Controller AND ANRP.[RoleId] = @RoleId AND [Action] LIKE 'Index%')

					IF (@COUNTER = 1)
					BEGIN
						SET @Result = CONCAT(@Result, 'V,')				
					END
			END
			ELSE
			BEGIN
					SET @COUNTER = (SELECT COUNT(ANP.[Action]) FROM [dbo].[AspNetPermissions] AS ANP 
					INNER JOIN [dbo].[AspNetRolePermissions] AS ANRP
					ON ANP.[Id] = ANRP.[PermissionId]
					WHERE ANP.[Resource] = @Controller AND ANRP.[RoleId] = @RoleId AND [Action] LIKE 'Admin%')

					IF (@COUNTER = 1)
					BEGIN
						SET @Result = CONCAT(@Result, 'V,')
					END					
			END		
		END
	END
	

	SET @COUNTER = (SELECT COUNT(ANP.[Action]) FROM [dbo].[AspNetPermissions] AS ANP 
	INNER JOIN [dbo].[AspNetRolePermissions] AS ANRP
	ON ANP.[Id] = ANRP.[PermissionId]
	WHERE ANP.[Resource] = @Controller AND ANRP.[RoleId] = @RoleId AND [Action] LIKE 'Modules%')

	IF (@COUNTER = 1)
	BEGIN
		SET @Result = CONCAT(@Result, 'V,')
	END

	SET @COUNTER = (SELECT COUNT(ANP.[Action]) FROM [dbo].[AspNetPermissions] AS ANP 
	INNER JOIN [dbo].[AspNetRolePermissions] AS ANRP
	ON ANP.[Id] = ANRP.[PermissionId]
	WHERE ANP.[Resource] = @Controller AND ANRP.[RoleId] = @RoleId AND [Action] LIKE 'Roles%')

	IF (@COUNTER = 1)
	BEGIN
		SET @Result = CONCAT(@Result, 'V,')
	END
	
	IF @Action <> 'TransactionRules'
	BEGIN
		SET @COUNTER = (SELECT COUNT(ANP.[Action]) FROM [dbo].[AspNetPermissions] AS ANP 
		INNER JOIN [dbo].[AspNetRolePermissions] AS ANRP
		ON ANP.[Id] = ANRP.[PermissionId]
		WHERE ANP.[Resource] = @Controller AND ANRP.[RoleId] = @RoleId AND [Name] LIKE 'AddOrEdit_%')

		IF (@COUNTER >= 1)
		BEGIN
			SET @Result = CONCAT(@Result, 'A,')
		END
	
		SET @COUNTER = (SELECT COUNT(ANP.[Action]) FROM [dbo].[AspNetPermissions] AS ANP 
		INNER JOIN [dbo].[AspNetRolePermissions] AS ANRP
		ON ANP.[Id] = ANRP.[PermissionId]
		WHERE ANP.[Resource] = @Controller AND ANRP.[RoleId] = @RoleId AND [Name] LIKE 'Load_%')

		IF (@COUNTER >= 1)
		BEGIN
			SET @Result = CONCAT(@Result, 'ED,')
		END

		IF @Action <> 'Admin' AND @Controller = 'CreditCard'
		BEGIN
			SET @COUNTER = (SELECT COUNT(ANP.[Action]) FROM [dbo].[AspNetPermissions] AS ANP 
			INNER JOIN [dbo].[AspNetRolePermissions] AS ANRP
			ON ANP.[Id] = ANRP.[PermissionId]
			WHERE ANP.[Resource] = @Controller AND ANRP.[RoleId] = @RoleId AND [Name] LIKE 'Delete_%')

			IF (@COUNTER >= 1)
			BEGIN
				SET @Result = CONCAT(@Result, 'E')
			END
		END
	END

	RETURN @Result

END
GO

