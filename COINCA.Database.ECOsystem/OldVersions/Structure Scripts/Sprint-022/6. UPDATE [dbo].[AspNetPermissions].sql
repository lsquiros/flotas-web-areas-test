--Update Reglas de Negocio
UPDATE [dbo].[AspNetPermissions]
SET [Resource] = 'CreditCard', [Action] = 'TransactionRules'
WHERE [Id] = '421B371B-A21F-4FB7-BCEA-1666AF23C744'

--Update Unidades de VehÝculos por Geocerca
UPDATE [dbo].[AspNetPermissions] 
SET [Resource] = 'VehicleUnitsByGeoFence'
WHERE [Id] = '162CF71B-BD1E-4DB5-91EA-D4F2B781F846'

--Update Centros de Costo de VehÝculos por Geocerca
UPDATE [dbo].[AspNetPermissions] 
SET [Resource] = 'VehicleCostCentersByGeoFence'
WHERE [Id] = '62DD97B1-271A-4942-B03D-9BF2E04AECDF'

