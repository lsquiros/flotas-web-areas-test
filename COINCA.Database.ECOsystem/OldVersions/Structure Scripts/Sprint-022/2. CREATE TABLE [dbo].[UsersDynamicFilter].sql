use [ECOsystem]
GO

/****** Object:  Table [dbo].[UsersDynamicFilter]    Script Date: 1/19/2016 10:13:42 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsersDynamicFilter]') AND type in (N'P', N'PC'))
	DROP TABLE [dbo].[UsersDynamicFilter]
GO

/****** Object:  Table [dbo].[UsersDynamicFilter]    Script Date: 1/19/2016 10:13:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UsersDynamicFilter](
	[UserId] [int] NOT NULL,
	[Type] [nvarchar](2) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_UsersDynamicFilter] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[UsersDynamicFilter]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [General].[Users] ([UserId])
GO