use [ECOsystem]

--Almacena el Permiso
INSERT INTO [dbo].[AspNetPermissions] VALUES('B9C8D5D0-5C94-4051-9AAA-C1B3181AEBFD', 'View_DinamicFilter', 'DinamicFilter', 'Index', '', GetDate())

--Otorga el permiso para los Usuarios Master del Perfil Customer Admin
INSERT INTO [dbo].[AspNetRolePermissions] VALUES('9baac678-473a-4f96-9d67-9f2a5af778f3', 'B9C8D5D0-5C94-4051-9AAA-C1B3181AEBFD')
INSERT INTO [dbo].[AspNetRolePermissions] VALUES('F024C5BD-C5F4-408C-8A86-B5DBA55435E0', 'B9C8D5D0-5C94-4051-9AAA-C1B3181AEBFD')

--Inserta el Item en la tabla Menus
INSERT INTO [dbo].[AspMenus] VALUES ('Mantenimiento de Filtros por VehÝculos', '', 'B9C8D5D0-5C94-4051-9AAA-C1B3181AEBFD', 116, 8, 117, GetDate())

--Otorga el permiso al menu
INSERT INTO [dbo].[AspMenusByRoles] VALUES ('9baac678-473a-4f96-9d67-9f2a5af778f3', 123)
INSERT INTO [dbo].[AspMenusByRoles] VALUES ('F024C5BD-C5F4-408C-8A86-B5DBA55435E0', 123)

--Actualizacion del mantenimiento preventivo en el menu de control
UPDATE [dbo].[aspmenus]
SET [Parent] = 39, [Header] = 3
WHERE [Id] = 44