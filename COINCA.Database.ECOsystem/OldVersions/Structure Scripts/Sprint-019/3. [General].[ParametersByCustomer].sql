
USE [ECOsystemQA]
GO
--Buscar Id de empresa Dos Pinos
DECLARE @pCustomerId INT = 30
--SELECT TOP 1 @pCustomerId = [CustomerId] FROM [General].[Customers] WHERE [Name] = 'NDU26urUa34RNJyYVG4guw=='

INSERT INTO [General].[ParametersByCustomer]
           ([CustomerId]
           ,[Name]
           ,[Value]
           ,[ResuourceKey]
           ,[InsertUserId]
           ,[InsertDate]
           ,[ModifyUserId]
           ,[ModifyDate])
     VALUES
           (@pCustomerId
           ,'Formato de nombre del vehiculo'
           ,'{0}  V{1}'
           ,'VEHICLE_NAME_FORMAT'
           ,1
           ,GETDATE()
           ,NULL
           ,NULL)
GO


