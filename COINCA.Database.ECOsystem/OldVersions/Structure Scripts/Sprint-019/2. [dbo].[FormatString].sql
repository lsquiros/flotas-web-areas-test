
USE [ECOsystemQA]
GO

IF OBJECT_ID( N'[dbo].[FormatString]', 'FN' ) IS NOT NULL
	DROP FUNCTION [dbo].[FormatString]
GO

/***************************************************
Object Name : FormatString
Purpose : Returns the formatted string.
Author : Karthik D V
Created Date: 21-Dec-2010
Sample Call:
SELECT dbo.FormatString ( N'Format {0} {1} {2} {0}', N'1,2,3' )
Modification History:
----------------------------------------------------------------------------
Date Modified By 
Modification Details
----------------------------------------------------------------------------
----------------------------------------------------------------------------
*******************************************/
CREATE FUNCTION [dbo].[FormatString]
(
	@Format NVARCHAR(4000) ,
	@Parameters NVARCHAR(4000)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @Message NVARCHAR(400),
	@Delimiter CHAR(1)
	
	DECLARE @ParamTable TABLE 
	( 
		ID INT IDENTITY(0,1), 
		Paramter VARCHAR(1000) 
	)

	SELECT @Message = @Format, @Delimiter = ',';
	WITH CTE (StartPos, EndPos) AS
	(
		SELECT 1, CHARINDEX(@Delimiter, @Parameters)
		UNION ALL
		SELECT EndPos + (LEN(@Delimiter)), CHARINDEX(@Delimiter,@Parameters, EndPos + (LEN(@Delimiter)))
		FROM CTE
		WHERE EndPos > 0
	)

	INSERT INTO @ParamTable ( Paramter )
	SELECT [ID] = SUBSTRING ( @Parameters, StartPos, CASE WHEN EndPos > 0 THEN EndPos - StartPos ELSE 4000 END )
	FROM CTE
	UPDATE @ParamTable SET @Message = REPLACE ( @Message, '{'+CONVERT(VARCHAR,ID) + '}', Paramter )
	RETURN @Message
END
GO

GRANT EXECUTE,REFERENCES ON [dbo].[FormatString] TO [public]
GO