USE [ECOsystemDev]
GO

INSERT INTO [General].[WServicesByPartner]
           ([PartnerId]
           ,[Name]
           ,[Environment]
           ,[UserAuth]
           ,[PasswordAuth]
           ,[WSType]
           ,[UriService]
           ,[IsDeleted]
           ,[InsertUserId]
           ,[InsertDate]
           ,[ModifyUserId]
           ,[ModifyDate]
           ,[PartnerCode]
		   ,[Terminal])
     VALUES
           (5
           ,'Servicio de Autorizador BAC Costa Rica'
           ,'DEV'
           ,'ws_coinca'
           ,'Y1zPpauH'
           ,'WCF'
           ,'https://csp.credomatic.com:50581/Services/CSP/AuthorizationService.svc'
           ,0
           ,0
           ,DATEADD(HOUR, -6, GETDATE())
           ,0
           ,DATEADD(HOUR, -6, GETDATE())
           ,'BAC-CR'
		   ,'COINCA01')
GO


