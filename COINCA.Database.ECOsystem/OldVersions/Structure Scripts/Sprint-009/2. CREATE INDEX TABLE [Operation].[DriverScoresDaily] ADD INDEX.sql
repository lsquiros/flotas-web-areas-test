USE [ECOsystem]
GO
CREATE INDEX IDX_DRIVER_SCORESDAILY_YEAR_MONTH_DAY_USERID_CUSTOMER
ON [Operation].[DriversScoresDaily] 
	([ScoreYear], [ScoreMonth], [ScoreDay], [UserId], [CustomerId]);
