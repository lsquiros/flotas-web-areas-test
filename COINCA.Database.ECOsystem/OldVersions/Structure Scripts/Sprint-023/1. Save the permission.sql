USE [ECOSystem]

--Save the permission
INSERT INTO [dbo].[AspNetPermissions] VALUES ('DCF960CA-61B5-4C71-B779-E880718CD313', 'View_CustomerTransacitiosSAPReport' , 'CustomerTransacitiosSAPReport' , 'Index' , '', GETDATE())
INSERT INTO [dbo].[AspNetPermissions] VALUES ('428E260C-D007-4A7C-B4E2-9A49156B1E13', 'AddOrEdit_CustomerTransacitiosSAPReport' , 'CustomerTransacitiosSAPReport' , 'AddOrEditSAPParameters' , '', GETDATE())
INSERT INTO [dbo].[AspNetPermissions] VALUES ('D5E8DA01-06B6-4D03-99F1-3B562B9CB2B3', 'View_TransactionSapReport' , 'TransactionSapReport' , 'Index' , '', GETDATE())
INSERT INTO [dbo].[AspNetPermissions] VALUES ('230C3B46-4F58-4D93-9B0D-89EA5B7448E9', 'AddOrEdit_ImportDrivers' , 'Drivers' , 'AddOrEditImportDrivers' , '', GETDATE())

