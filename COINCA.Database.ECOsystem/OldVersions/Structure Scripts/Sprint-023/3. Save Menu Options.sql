USE [ECOSystem]

--Save Menu Options
INSERT INTO [dbo].[AspMenus] VALUES('Mantenimiento de Parametros - Reporte Consolidado SAP', '', 'DCF960CA-61B5-4C71-B779-E880718CD313', 87, 8, 100, GETDATE())

--Update the Menu Transactions creating a new Main Menu
UPDATE [dbo].[AspMenus] SET [Name] = 'Reportes Transaccionales', [Ico] =  '~/Content/Images/menu_credit_cards.png', [PermissionId] = NULL, [Parent] = NULL, [Order] = 51 WHERE [Id] = 43

--Adding the new menus for transactions
INSERT INTO [dbo].[AspMenus] VALUES('Transacciones', '', 'A1CF8443-C6AC-4D6F-B864-6126FF76ED5C', 43, 3, 52, GETDATE())
INSERT INTO [dbo].[AspMenus] VALUES('Consolidado de SAP', '', 'D5E8DA01-06B6-4D03-99F1-3B562B9CB2B3', 43, 3, 53, GETDATE())
