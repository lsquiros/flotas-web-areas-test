USE [ECOSystem]

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('DEBE'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,'40'
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('HABER'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,'31'
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('CLASE_DOCUMENTO'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,'ST'
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('SOCIEDAD'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,'CR01'
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('MONEDA'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,'CRC'
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('CABECERA_CARGA'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,'Trans BACFLOTA'
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('CUENTA_DEBE'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,'6101004000'
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('IND_CME'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('INDICADOR_IMPUESTO'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('CALC_IMPUESTO'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('BANCO_PROPIO'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('VIA_PAGO_HABER'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,'T'
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('VIA_PAGO_DEBE'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('CENTRO_COSTO'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('CENTRO_BENEFICIO'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('ORDEN'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('PEP'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('POSICION_PRESUPUEST'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('CENTRO_GESTOR'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('PROGRAMA_PRESUPUEST'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('FONDO'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('CENTRO'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);


INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('BLOQUEA_PAGO_HABER'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,'A'
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('BLOQUEA_PAGO_DEBE'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,''
			,0);

INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('PAGADOR_ALTERNATIVO'
			,'NA'
			,'Reporte Concolidado SAP Dos Pinos'
			,'107958'
			,0);
