USE [ECOSystem]

ALTER TABLE [General].[VehiclesDrivers]  DROP  CONSTRAINT [FK_VehiclesDrivers_Users] 
GO

ALTER TABLE [General].[VehiclesDrivers]  DROP  CONSTRAINT [FK_VehiclesDrivers_Vehicles] 
GO

--********************************************************************************************

CREATE INDEX [indexUserId] ON [General].[VehiclesDrivers] ([UserId]);
CREATE INDEX [indexVehicleId] ON [General].[VehiclesDrivers] ([VehicleId]);