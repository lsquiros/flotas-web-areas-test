IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsSAPReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsSAPReport_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 2/2/2016
-- Description:	Retrieve TransactionsSAPReport information
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_TransactionsDetailedReport_Retrieve]
(
	@pCustomerId INT,					--@pCustomerId: CustomerId		
	@pYear INT = NULL,					--@pYear: Year
	@pMonth INT = NULL,					--@pMonth: Month
	@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
	@pEndDate DATETIME = NULL			--@pEndDate: End Date	
)
AS
BEGIN

	SET NOCOUNT ON	

	SELECT 				
		DATEADD(HOUR, -6, t.[InsertDate]) [InsertDate],
		v.[Name] [VehicleName],
		v.[PlateId],
		cc.[CreditCardNumber],
		e.[Code] AS [CostCenter],
		(SELECT TOP 1 TT.[Odometer] FROM(
			SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
			WHERE NT.[VehicleId] = 3 ORDER BY NT.[Date] DESC ) TT ORDER BY TT.[Date] ASC) AS [OdometerLast],
		t.[Odometer],		
		t.[TransactionPOS] AS [Reference],
		t.[FuelAmount] [Importe],
		DATEADD(HOUR, -6, t.[InsertDate]) [FechaValor],
		ISNULL(t.[Invoice], '-') AS [Asignacion]
		
		
	FROM 
		[Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v 
			ON t.[VehicleId] = v.[VehicleId]

		INNER JOIN [General].[VehiclesDrivers] vd
		    ON v.[VehicleId] = vd.[VehicleId]
		INNER JOIN [General].[DriversUsers] du
			ON vd.[UserId] = du.[UserId]

		INNER JOIN [General].[Customers] c 
			ON v.[CustomerId] = c.[CustomerId]
		INNER JOIN [Control].[Fuels] f 
			ON t.[FuelId] = f.[FuelId]
		INNER JOIN [Control].[CreditCardByVehicle] cv 
			ON t.[CreditCardId] = cv.[CreditCardId]
		INNER JOIN [Control].[Currencies] g 
			ON c.[CurrencyId] = g.[CurrencyId]
		INNER JOIN [Control].[CreditCard] cc
			ON cc.CreditCardId = t.CreditCardId
		INNER JOIN [General].[VehicleCostCenters] e
		ON v.CostCenterId = e.CostCenterId
	WHERE	
	    c.[CustomerId] = @pCustomerId AND 
		1 > (
			SELECT ISNULL(COUNT(1), 0) FROM Control.Transactions t2
					WHERE t2.[CreditCardId] = t.[CreditCardId] AND
						t2.[TransactionPOS] = t.[TransactionPOS] AND
						t2.[ProcessorId] = t.[ProcessorId] AND 
						t2.IsReversed = 1
		)		
		AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
				AND DATEPART(m,DATEADD(HOUR, -6, t.[InsertDate])) = @pMonth
				AND DATEPART(yyyy,DATEADD(HOUR, -6, t.[InsertDate])) = @pYear) OR
			(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
					AND DATEADD(HOUR, -6, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))

	ORDER BY t.[InsertDate] DESC

	SET NOCOUNT OFF
END

GO
