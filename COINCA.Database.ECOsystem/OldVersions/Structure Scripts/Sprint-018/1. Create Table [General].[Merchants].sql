-- =========================================
-- Table Merchants
-- Henry Retana
-- 11/5/2015
-- =========================================

IF OBJECT_ID('[General].[Merchants]', 'U') IS NOT NULL
  DROP TABLE [General].[Merchants]
GO

CREATE TABLE [General].[Merchants]
(
	[MerchantId] int NOT NULL IDENTITY(1,1), 
	[MerchantDescription] VARCHAR(100)
	
    CONSTRAINT PK_Merchants PRIMARY KEY ([MerchantId])
)
GO
