USE [ECOsystemDev]
GO

/****** Object:  Table [General].[FuelCurrencySettings]    Script Date: 11/5/2015 1:13:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [General].[FuelCurrencySettings](
	[CustomerId] [int] NOT NULL,
	[ByCurrency] [bit] NOT NULL,
	[ByFuel] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [General].[FuelCurrencySettings]  WITH CHECK ADD FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])
GO


