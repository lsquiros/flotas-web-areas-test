USE [ECOsystemDev]
GO

/****** Object:  Table [Operation].[PreDrivingTime_DayJob]    Script Date: 9/16/2015 3:12:04 PM ******/
DROP TABLE [Operation].[PreDrivingTime_DayJob]
GO

/****** Object:  Table [Operation].[PreDrivingTime_DayJob]    Script Date: 9/16/2015 3:12:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[PreDrivingTime_DayJob](
	[JobId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Identification] [nvarchar](max) NULL,
	[CustomerId] [int] NULL,
	[WorkingTime] [int] NOT NULL DEFAULT ((0)),
	[DrivingTime] [int] NOT NULL DEFAULT ((0)),
	[Daytime] [date] NULL,
	[DeviceReference] [int] NULL,
 CONSTRAINT [PK_PreDrivingTime_DayJob] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO


