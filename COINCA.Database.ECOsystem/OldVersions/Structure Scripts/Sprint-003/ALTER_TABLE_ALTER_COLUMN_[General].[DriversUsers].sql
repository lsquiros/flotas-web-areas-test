USE[ECOSystem]
GO
ALTER TABLE [ECOSystem].[General].[DriversUsers] ALTER COLUMN [Identification] NVARCHAR (MAX);
ALTER TABLE [ECOSystem].[General].[DriversUsers] ALTER COLUMN [Code] NVARCHAR (MAX);
ALTER TABLE [ECOSystem].[General].[DriversUsers] ALTER COLUMN [License] NVARCHAR (MAX);
ALTER TABLE [ECOSystem].[General].[DriversUsers] ALTER COLUMN [Dallas] NVARCHAR (MAX);
