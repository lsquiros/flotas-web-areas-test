USE [ECOsystem]
GO
/****** Object:  Table [General].[Customers]    Script Date: 7/6/2015 11:06:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [General].[ClientVisit](
	[VisitId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[RoutePointId] [int] NULL,
	[Name] [VARCHAR](300) NULL,
	[Contact] [VARCHAR](300) NULL,
	[Address] [VARCHAR](500) NULL,
	[Phone] [VARCHAR](50) NULL,
	[State] [int] NULL,
	[Reason] [VARCHAR](150) NULL,
	[IsVisit] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[InsertDate] [datetime] NULL,
	[InsertUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUserId] [int] NULL
 CONSTRAINT [PK_ClientVisit] PRIMARY KEY CLUSTERED 
(
	[VisitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO
ALTER TABLE [General].[ClientVisit] ADD  DEFAULT ((0)) FOR [IsVisit]
ALTER TABLE [General].[ClientVisit] ADD  DEFAULT ((0)) FOR [IsDeleted]


