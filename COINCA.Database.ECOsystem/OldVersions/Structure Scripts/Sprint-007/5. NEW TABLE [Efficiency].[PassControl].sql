USE [ECOsystem]
GO
/****** Object:  Table [General].[ClientVisit]    Script Date: 7/16/2015 3:18:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Efficiency].[PassControl](
	[PassControlId] [int] IDENTITY(1,1) NOT NULL,
	[VehicleId] [int] NULL,
	[Device] [int] NULL,
	[UnitID] [decimal](20,0) NULL,
	[PointId] [int] NULL,
	[PointName] [varchar](250) NULL,
	[CheckPointTime] [time] NULL,
	[UserCheckPoint] [time] NULL,
	[StoppingTime] [int] NULL,
	[State] [varchar](20) NULL, -- VISITADO, NO VISITADO, PENDIENTE
	[PointLatitude] [float] NULL,
	[PointLongitude] [float] NULL,
	[CurrentRouteDay] [char](1) NULL,
	[ControlDay] [int] NULL,
	[ControlMonth] [int] NULL,
	[ControlYear] [int] NULL,
	[InsertDate] [datetime] NULL,
 CONSTRAINT [PK_PassControl] PRIMARY KEY CLUSTERED 
(
	[PassControlId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

