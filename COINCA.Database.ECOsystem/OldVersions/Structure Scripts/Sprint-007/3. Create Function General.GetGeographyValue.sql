USE [ECOsystem]
GO

DROP FUNCTION [General].[GetGeographyValue]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sergio Monge
-- Create date: 10/07/2015
-- Description:	Convert the geometric value to geography value
-- =============================================
CREATE FUNCTION General.GetGeographyValue 
(
	@polygon geometry
)
RETURNS geography
AS
BEGIN
	DECLARE @geog geography
	DECLARE @geom geometry

	set @geom = geometry::STPolyFromText(@polygon.STAsText(),4326) 
	
	SET @geom = @geom.STUnion(@geom.STStartPoint()); 

	set @geog = geography::STGeomFromWKB(@geom.STAsBinary(), @geom.STSrid);

	RETURN @geog

END
GO

