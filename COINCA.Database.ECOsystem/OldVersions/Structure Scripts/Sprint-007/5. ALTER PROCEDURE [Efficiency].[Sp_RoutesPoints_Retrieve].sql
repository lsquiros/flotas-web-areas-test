USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesPoints_Retrieve]    Script Date: 07/16/2015 09:25:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	Retrieve RoutesPoints information
-- ================================================================================================
ALTER PROCEDURE [Efficiency].[Sp_RoutesPoints_Retrieve]
(
	 @pRouteId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT [PointId]
		,[RouteId]
		,[PointReference]
		,[Order]
		,[Name]
		,CONVERT(varchar(20), [Time]) as [Time]
		,CONVERT(varchar(50), [Latitude]) as [Latitude]
		,CONVERT(varchar(50), [Longitude]) as [Longitude]
		,StopTime
	FROM 
		[Efficiency].[RoutesPoints]
	where [RouteId] = @pRouteId
	
    SET NOCOUNT OFF
END

GO


