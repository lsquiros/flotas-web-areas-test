
--IsInRole("CUSTOMER_ADMIN")
--Modulo 1 Eficiencia
--1
insert into [dbo].[aspmenus] Values('Geocercas','~/Content/Images/menu_geofences.png',NULL,NULL,1,1,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Geocercas','','D9509620-AEDB-417F-8320-99D354A72DB5',1,1,2,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos por Geocerca','','DE790515-4857-4080-80C9-058C6F9A6683',1,1,3,GETDATE())
insert into [dbo].[aspmenus] Values('Grupos por Geocerca','','308FE0A3-8DFB-470D-AF37-AC80E57915CC',1,1,4,GETDATE())
insert into [dbo].[aspmenus] Values('Unidades por Geocerca','','5752860A-B8A8-462D-BA46-BFA0697CDB73',1,1,5,GETDATE())
insert into [dbo].[aspmenus] Values('Centros de Costo por Geocerca','','04389FD5-C880-477F-B0CE-34019EE8780D',1,1,6,GETDATE())
--7
insert into [dbo].[aspmenus] Values('Rutas','~/Content/Images/menu_routes.png',NULL,NULL,1,7,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Rutas','','CCBF3B13-E953-4422-B996-6957BBE2FCE1',7,1,8,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos por Ruta','','9898813D-1C95-456E-8A3A-F1320173C8E2',7,1,9,GETDATE())
insert into [dbo].[aspmenus] Values('Rutas sobre Trayectos','','7EE5AC3A-4719-40D3-9889-EFDA683C5269',7,1,10,GETDATE())
--11
insert into [dbo].[aspmenus] Values('Localizaci�n','~/Content/Images/menu_localization.png',NULL,NULL,1,11,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos en Tiempo Real','','DB1D9632-7813-4C40-86C2-47EE85ED35DE',11,1,12,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos por Grupo en Tiempo Real','','1C790376-D8BB-4FA7-8A0F-C7EE1C34D319',11,1,13,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos por Unidad en Tiempo Real','','B0CE6C3E-26DA-48EC-A4E4-A52A0A7B6E4D',11,1,14,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos por Centro de Costos en Tiempo Real','','00B5BFA1-5555-46D9-882E-070585C0D217',11,1,15,GETDATE())
insert into [dbo].[aspmenus] Values('Trayectos de Veh�culos','','8FACDA1E-674D-4259-A144-1BDDAF3A68A5',11,1,16,GETDATE())
insert into [dbo].[aspmenus] Values('Reconstrucci�n de Recorrido','','E0FCEDC8-189F-441F-A7BB-7A83380B32A1',11,1,17,GETDATE())
--18
insert into [dbo].[aspmenus] Values('Control de Paso','~/Content/Images/menu_passcontrol.png',NULL,NULL,1,18,GETDATE())
insert into [dbo].[aspmenus] Values('Por Veh�culo','','80C760D5-578D-4169-BFBC-98AC21562A39',18,1,19,GETDATE())
insert into [dbo].[aspmenus] Values('Por Grupo','','F6409CA0-8147-4D38-A7DF-B0809EA7FDE7',18,1,20,GETDATE())
insert into [dbo].[aspmenus] Values('Por Unidad','','6BB719C4-D064-4D4F-BF7A-8BD8CF0FFF31',18,1,21,GETDATE())
insert into [dbo].[aspmenus] Values('Por Centro de Costo','','60CB06CC-C386-49A1-AAB2-A47EFC11C090',18,1,22,GETDATE())
--23
insert into [dbo].[aspmenus] Values('Medici�n de Distancias','~/Content/Images/menu_measuring.png','C6E5CD87-C956-4F37-9C49-F959893C118E',23,1,23,GETDATE())

--IsInRole("SUPER_ADMIN")
--Configuracion Modulo 2
--24
insert into [dbo].[aspmenus] Values('Socios','~/Content/Images/menu_partner.png',NULL,NULL,2,24,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Socios','','E22B663A-8D0A-44E0-9689-74BB7AA2C5A5',24,2,25,GETDATE())
insert into [dbo].[aspmenus] Values('Combustible por Socio','','7309581B-7188-4BF6-AA63-A08BC7DCE2B2',24,2,26,GETDATE())
--27
insert into [dbo].[aspmenus] Values('Pa�ses','~/Content/Images/menu_countries.png','600F2539-CDAD-402B-A16C-F863F4A9121B',27,2,27,GETDATE())
--28
insert into [dbo].[aspmenus] Values('Monedas','~/Content/Images/menu_currencies.png','64EB3825-720C-4133-A4D3-0441AB6422FF',28,2,28,GETDATE())
--29
insert into [dbo].[aspmenus] Values('Combustibles','~/Content/Images/menu_combustible.png','005E4F1D-5D06-4FC0-B658-468097E49CFC',29,2,29,GETDATE())
--30
insert into [dbo].[aspmenus] Values('Par�metros Globales','~/Content/Images/menu_parameters.png','E235BA6B-D0DA-41C2-BE53-7410F51DF68F',30,2,30,GETDATE())
--31
insert into [dbo].[aspmenus] Values('FAQ Operaciones','~/Content/Images/menu_faq.png','5AC53B35-3B62-4714-BABE-D15690D7C8EC',31,2,31,GETDATE())

--IsInRole("CUSTOMER_ADMIN")
--Modulo 3 Control
--32
insert into [dbo].[aspmenus] Values('Distribuci�n de Cr�dito','~/Content/Images/menu_budget.png',NULL,NULL,3,32,GETDATE())
insert into [dbo].[aspmenus] Values('Por Combustible','','AEEC8487-A606-46BD-85BF-F320FE8883E3',32,3,33,GETDATE())
insert into [dbo].[aspmenus] Values('Por Veh�culos','','1976B805-E947-4024-830A-1A70304AF3A0',32,3,34,GETDATE())
--35
insert into [dbo].[aspmenus] Values('Flota','~/Content/Images/menu_flota.png',NULL,NULL,3,35,GETDATE())
insert into [dbo].[aspmenus] Values('Franja Horaria para Transacciones','','3DFAFA6D-B3C3-4CC1-9190-779D730F2676',35,3,36,GETDATE())
--37
insert into [dbo].[aspmenus] Values('Mantenimiento','~/Content/Images/menumantenimiento.png',NULL,NULL,3,37,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento Preventivo','','DAF2835F-4DAF-4FDA-8DDB-4A40211EDA36',37,3,38,GETDATE())
--39
insert into [dbo].[aspmenus] Values('Reportes','~/Content/Images/menu_reports.png',NULL,NULL,3,39,GETDATE())
insert into [dbo].[aspmenus] Values('Consumo Real del Per�odo','','9EE6A9FD-E3FE-4C01-9D8A-5E7E6BDDB8B7',39,3,40,GETDATE())
insert into [dbo].[aspmenus] Values('Porcentaje de Presupuesto Consumido','','4ED5805F-9C11-49EE-8D4B-E20A8B33E47E',39,3,41,GETDATE())
insert into [dbo].[aspmenus] Values('Acumulado Mensual','','35D132BD-A07F-4797-B16E-041D23EE053B',39,3,42,GETDATE())
insert into [dbo].[aspmenus] Values('Transacciones','','A1CF8443-C6AC-4D6F-B864-6126FF76ED5C',39,3,43,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento Preventivo','','9B44AF9D-C081-40F1-A62D-07639273E6E3',39,3,44,GETDATE())--
insert into [dbo].[aspmenus] Values('Liquidaci�n Consolidada','','8CAED515-0559-4CE2-8E53-1CB74673F5E0',39,3,45,GETDATE())
insert into [dbo].[aspmenus] Values('Media de Consumo','','4A593B6F-720C-49D9-B398-F2BC367B603F',39,3,46,GETDATE())
insert into [dbo].[aspmenus] Values('Comparativo Rendimiento','','1C3732FD-B55D-49B8-9FE3-48F37918498F',39,3,47,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos con Bajo Rendimiento','','17B1F745-8719-4AC9-8D91-C724E4F7DBEF',39,3,48,GETDATE())
insert into [dbo].[aspmenus] Values('Recorrido de Veh�culos','','3005C036-895D-441A-BE53-EB9AEEE42309',39,3,49,GETDATE())
insert into [dbo].[aspmenus] Values('Reporte de Hor�metros (GPS)','','92D0F5DD-28E7-4818-A0B3-147E1F23C33A',39,3,50,GETDATE())

--IsInRole("CUSTOMER_ADMIN") || User.IsInRole("CUSTOMER_USER")
---Modulo 4 Operacion
--51
insert into [dbo].[aspmenus] Values('Reportes de Operaci�n','~/Content/Images/menu_reports.png',NULL,NULL,4,51,GETDATE())
insert into [dbo].[aspmenus] Values('Calificaci�n H�bitos de Conducci�n seg�n Velocidad de Ley','','095FC1C1-91DE-4BBD-96BA-63EE13188E66',51,4,52,GETDATE())
insert into [dbo].[aspmenus] Values('Calificaci�n H�bitos de Conducci�n Delimitada por Empresa','','51A32959-55A3-4C10-951A-C6D6E1BC8163',51,4,53,GETDATE())
insert into [dbo].[aspmenus] Values('�ndice Ponderado de H�bitos de Conducci�n Velocidad de Ley','','20847DBF-E6C0-47F7-9DC4-F1C8E00E9FBD',51,4,54,GETDATE())
insert into [dbo].[aspmenus] Values('�ndice Ponderado de H�bitos de Conducci�n Delimitada por Empresa','','06DD1B46-FD99-4D31-8652-FA16B2E37D16',51,4,55,GETDATE())
insert into [dbo].[aspmenus] Values('Tiempo Jornada y Conducci�n por Conductor','','0A00EA9C-7624-4F69-B7B2-D3EBBED80B65',51,4,56,GETDATE())
insert into [dbo].[aspmenus] Values('Delimitaci�n de Velocidad de Empresa por Clase de Veh�culos','','9992F2BD-5DA4-47F1-B8D6-284C688D7F84',51,4,57,GETDATE())
--58
insert into [dbo].[aspmenus] Values('Personalizaci�n','~/Content/Images/menu_parameters.png',NULL,NULL,4,58,GETDATE())
--("CUSTOMER_ADMIN")
insert into [dbo].[aspmenus] Values('Par�metros de Calificaci�n Ponderada','','D018EE91-CEC1-4701-8123-56C9F18C7E67',58,4,59,GETDATE())

--("SUPER_ADMIN")
insert into [dbo].[aspmenus] Values('Tipos de Calificaci�n','~/Content/Images/menu_measuring.png','1A20C687-08C3-43EF-9835-19E98BD20792',60,5,60,GETDATE())

--IsInRole("INSURANCE_ADMIN")
--Modulo 6 Operacion
--61
insert into [dbo].[aspmenus] Values('Reportes de Operaci�n','~/Content/Images/menu_reports.png',NULL,NULL,6,61,GETDATE())
insert into [dbo].[aspmenus] Values('Calificaci�n de H�bitos de Conducci�n por Empresa seg�n Velocidad de Ley','','493EA4DB-04B6-459B-814B-17835389A49F',61,6,62,GETDATE())
insert into [dbo].[aspmenus] Values('Tendencia de H�bitos de Conducci�n por Empresa','','6E1710EC-AC21-450E-B31B-82492ED0EF4E',61,6,63,GETDATE())
insert into [dbo].[aspmenus] Values('Calificaci�n de H�bitos de Conducci�n de Empresas con Alto Riesgo','','6E1710EC-AC21-450E-B31B-82492ED0EF4E',61,6,64,GETDATE())
insert into [dbo].[aspmenus] Values('�ndice Ponderado  de H�bitos de Conducci�n por Empresa','','12536446-F447-422D-8924-0DF1CD27B90F',61,6,65,GETDATE())
insert into [dbo].[aspmenus] Values('H�bitos de Conducci�n por Conductor seg�n Velocidad de Ley','','7EBA7F7E-E09E-43A8-AB5F-2B1E4296BAF7',61,6,66,GETDATE())
insert into [dbo].[aspmenus] Values('H�bitos de Conducci�n por Conductor Velocidad Delimitada por Empresa','','EECEBE9F-BB61-44E8-9EA0-EF691A0058E2',61,6,67,GETDATE())

--IsInRole("CUSTOMER_ADMIN")
--Modulo 7 Alarms
--68
insert into [dbo].[aspmenus] Values('Lista de Alarmas','~/Content/Images/menu_alarm.png','F064DE97-A001-40C8-92D5-F97344C3E0C8',68,7,68,GETDATE())


--IsInRole("CUSTOMER_ADMIN")
--Modulo 8 Administracion
--69
insert into [dbo].[aspmenus] Values('Tarjetas de Cr�dito','~/Content/Images/menu_credit_cards.png',NULL,NULL,8,69,GETDATE())
insert into [dbo].[aspmenus] Values('Solicitud de Tarjetas','','AF0805A3-5176-4A3D-A757-0ABA712E3D10',69,8,70,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Tarjetas','','BFF3CBF4-4AC8-4AD2-BB07-E1B80DCF1A7F',69,8,71,GETDATE())
--72
insert into [dbo].[aspmenus] Values('Usuarios','~/Content/Images/menu_users.png','44EBBDC1-329E-43B9-858E-6CBD2A07B1EE',72,8,72,GETDATE())
--73
insert into [dbo].[aspmenus] Values('Conductores','~/Content/Images/menu_drivers.png','7E8F1CC8-3046-4BF1-9D5C-224F8CA4B1A2',73,8,73,GETDATE())
--74
insert into [dbo].[aspmenus] Values('Flota','~/Content/Images/menu_flota.png',NULL,NULL,8,74,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Llaves Dallas','','C5A5EC88-BB63-4991-AF94-D8750F48F21B',74,8,75,GETDATE())
insert into [dbo].[aspmenus] Values('Clases de Veh�culos','','9992F2BD-5DA4-47F1-B8D6-284C688D7F84',74,8,76,GETDATE())
insert into [dbo].[aspmenus] Values('Grupos de Veh�culos','','DADD58E8-A6E3-48BB-AAAD-B2EA72BA9FDB',74,8,77,GETDATE())
insert into [dbo].[aspmenus] Values('Unidades','','D374DF48-BAD6-468B-8829-72C1FD17B580',74,8,78,GETDATE())
insert into [dbo].[aspmenus] Values('Centro de Costos','','05838293-CC76-4F35-A210-40EDDFD7BF72',74,8,79,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos','','48E1CF7B-7324-4DAD-9897-6D40AAE39DCC',74,8,80,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos por Grupo','','2BBA5414-ABE7-457E-B748-8E075AB88753',74,8,81,GETDATE())
insert into [dbo].[aspmenus] Values('Conductores por Veh�culo','','BBC1676C-8790-4B97-AF7B-9094A06D0963',74,8,82,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos por Conductor','','C2623E31-FD64-42FF-9066-BF6A1255A0EF',74,8,83,GETDATE())
insert into [dbo].[aspmenus] Values('Horarios de Veh�culos','','6F655211-EB6B-4B3D-ACF4-FD091753BCB3',74,8,84,GETDATE())
--85
insert into [dbo].[aspmenus] Values('Mantenimiento','~/Content/Images/menumantenimiento.png',NULL,NULL,8,85,GETDATE())
insert into [dbo].[aspmenus] Values('Preventivo - Cat�logo','','DA69B3F0-BA52-4BD4-A312-43BBCE8DCDD5',85,8,86,GETDATE())
--87
insert into [dbo].[aspmenus] Values('Personalizar','~/Content/Images/customizer.png',NULL,NULL,8,87,GETDATE())
insert into [dbo].[aspmenus] Values('Pantalla de Inicio','','247194A5-558E-43F6-A568-01DFC44BA1C2',87,8,88,GETDATE())
insert into [dbo].[aspmenus] Values('Rangos','','16A697A4-3547-411A-B32C-695AD24DFE6D',87,8,89,GETDATE())
insert into [dbo].[aspmenus] Values('Reglas de Negocio','','421B371B-A21F-4FB7-BCEA-1666AF23C744',87,8,90,GETDATE())
--91
insert into [dbo].[aspmenus] Values('Estaciones de Servicio','~/Content/Images/menumantenimiento.png','1DBF7024-EE2D-432F-B7E1-A8A27830ACA9',91,8,91,GETDATE())

--IsInRole("SUPER_ADMIN")
--Modulo 9 Administracion
--92
insert into [dbo].[aspmenus] Values('Tarjetas de Cr�dito','~/Content/Images/menu_credit_cards.png',NULL,NULL,9,92,GETDATE())
insert into [dbo].[aspmenus] Values('Lista de Tarjetas Solicitadas','','BFF3CBF4-4AC8-4AD2-BB07-E1B80DCF1A7F',92,9,93,GETDATE())
insert into [dbo].[aspmenus] Values('POS Virtual','','9D3011E9-B597-4A98-B685-73B9581AC083',92,9,94,GETDATE())
--94
insert into [dbo].[aspmenus] Values('Relaciones Comerciales','~/Content/Images/menu_credit_cards.png',NULL,NULL,9,95,GETDATE())
insert into [dbo].[aspmenus] Values('Asignaci�n de Clientes','','1FCA04C8-28C2-423D-9C88-5C42135EFE27',95,9,96,GETDATE())
--96
insert into [dbo].[aspmenus] Values('Importaci�n de Datos','~/Content/Images/menu_import-data.png',NULL,NULL,9,97,GETDATE())
insert into [dbo].[aspmenus] Values('Fleet Cards','','3A5DF763-AC5D-4EAB-B37C-991F38745E81',97,9,98,GETDATE())
insert into [dbo].[aspmenus] Values('Veh�culos','','F997AA9C-D751-4F5D-A0CF-C4AD755FF056',97,9,99,GETDATE())

--IsInRole("PARTNER_ADMIN") || User.IsInRole("PARTNER_USER")
--Modulo 14 Reports
--99
insert into [dbo].[aspmenus] Values('Reportes','~/Content/Images/menu_reports.png',NULL,NULL,14,100,GETDATE())
insert into [dbo].[aspmenus] Values('Solicitantes','','F8613BDE-3918-403A-A763-E388B8F6AD36',100,14,101,GETDATE())
insert into [dbo].[aspmenus] Values('Tarjetas','','BDA7BD2A-3A62-4601-9AEB-4E0019BED098',100,14,102,GETDATE())
insert into [dbo].[aspmenus] Values('Tarjetas Maestras','','9C5C217B-5F07-40D3-9A6F-AC227A6B37DD',100,14,103,GETDATE())
insert into [dbo].[aspmenus] Values('Transacciones','','06694BCA-07DF-436F-92A4-59C6AD5D0F0B',100,14,104,GETDATE())

--User Roles Permissions
--Modulo 17 Administracion -- partner admin 
insert into [dbo].[aspmenus] Values('Clientes','~/Content/Images/menu_customers.png',Null,Null,17,100,GETDATE())
insert into [dbo].[AspMenus] Values('Mantenimiento de Clientes','','14A0D89A-7A94-4F7C-8456-058B2CD5115D',105,17,101,GETDATE())
insert into [dbo].[aspmenus] Values('Usuarios','~/Content/Images/menu_users.png',Null,Null,17,102,GETDATE())
insert into [dbo].[AspMenus] Values('Mantenimiento de Usuarios','','78D5AC82-08E7-43D0-A50C-C0879C23EC19',107,17,103,GETDATE())
insert into [dbo].[aspmenus] Values('Seguridad','~/Content/Images/menu_encryption.png',Null,Null,17,104,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Permisos','','A7E73FE5-49A8-441F-9977-CB670855CD68',109,17,105,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Roles','','46FB4465-7667-4ABF-9150-513A184334C1',109,17,106,GETDATE())

--roles permission -- administracion super admin
insert into [dbo].[aspmenus] Values('Seguridad','~/Content/Images/menu_encryption.png',Null,Null,9,104,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Permisos','','A7E73FE5-49A8-441F-9977-CB670855CD68',112,9,105,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Roles','','46FB4465-7667-4ABF-9150-513A184334C1',112,9,106,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Modulos','','3EE5482C-F9AE-4730-9A64-DA8A41FF5FD9',112,9,107,GETDATE())

--roles permission -- administracion CUSTOMER admin
insert into [dbo].[aspmenus] Values('Seguridad','~/Content/Images/menu_encryption.png',Null,Null,8,104,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Permisos','','A7E73FE5-49A8-441F-9977-CB670855CD68',116,8,105,GETDATE())
insert into [dbo].[aspmenus] Values('Mantenimiento de Roles','','46FB4465-7667-4ABF-9150-513A184334C1',116,8,106,GETDATE())

--Estaciones de Servicios - Partnet Admin - Customer Admin 
insert into [dbo].[aspmenus] Values('Estaciones de Servicio','~/Content/Images/menumantenimiento.png','1DBF7024-EE2D-432F-B7E1-A8A27830ACA9',119,17,108,GETDATE())

--Noticias - Administracion Super Admin
insert into [dbo].[aspmenus] Values('Noticias','~/Content/Images/menu_news.png','EED60DD5-FD1A-4C36-9083-933F186F136E',120,9,100,GETDATE())

--Noticias - Administracion Partner Admin
insert into [dbo].[aspmenus] Values('Noticias','~/Content/Images/menu_news.png','EED60DD5-FD1A-4C36-9083-933F186F136E',121,17,109,GETDATE())

--Alarmas - Administracion Customer Admin
insert into [dbo].[aspmenus] Values('Alarmas','~/Content/Images/menu_alarm.png','F064DE97-A001-40C8-92D5-F97344C3E0C8',122,8,92,GETDATE())


