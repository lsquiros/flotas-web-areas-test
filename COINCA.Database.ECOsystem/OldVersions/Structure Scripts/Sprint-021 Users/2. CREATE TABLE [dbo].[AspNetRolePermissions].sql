USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetRolePermissions]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetRolePermissions]
GO

/****** Object:  Table [dbo].[AspNetRolePermissions]    Script Date: 12/7/2015 10:44:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetRolePermissions](
	[RoleId] [nvarchar](128) NOT NULL,
	[PermissionId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_AspNetRolePermissions] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[AspNetRolePermissions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetRolePermissions_dbo.AspNetPermissions_PermissionId] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[AspNetPermissions] ([Id])
GO

ALTER TABLE [dbo].[AspNetRolePermissions] CHECK CONSTRAINT [FK_dbo.AspNetRolePermissions_dbo.AspNetPermissions_PermissionId]
GO

ALTER TABLE [dbo].[AspNetRolePermissions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetRolePermissions_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
GO

ALTER TABLE [dbo].[AspNetRolePermissions] CHECK CONSTRAINT [FK_dbo.AspNetRolePermissions_dbo.AspNetRoles_RoleId]
GO


