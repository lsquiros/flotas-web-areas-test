USE [ECOsystemQA]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspMenusByRoles](
	[RoleId] [nvarchar](128) NOT NULL,
	[MenuId] [int] NOT NULL,
 CONSTRAINT [PK_AspMenusByRoles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[AspMenusByRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspMenusByRoles_dbo.AspNetMenus_MenuId] FOREIGN KEY([MenuId])
REFERENCES [dbo].[AspMenus] ([Id])
GO

ALTER TABLE [dbo].[AspMenusByRoles] CHECK CONSTRAINT [FK_dbo.AspMenusByRoles_dbo.AspNetMenus_MenuId]
GO

ALTER TABLE [dbo].[AspMenusByRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspMenusByRoles_dbo.AspNetMenus_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
GO

ALTER TABLE [dbo].[AspMenusByRoles] CHECK CONSTRAINT [FK_dbo.AspMenusByRoles_dbo.AspNetMenus_RoleId]
GO
