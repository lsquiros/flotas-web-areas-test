--Index 
--INSURANCE_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'View_DrivingScoreFleetAdminScoreReport','DrivingScoreFleetAdminScoreReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_DrivingScoreFleetDataSourceReport','DrivingScoreFleetDataSourceReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_DrivingScoreFleetEvolutionReport','DrivingScoreFleetEvolutionReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_DrivingScoreFleetReport','DrivingScoreFleetReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_DrivingScoreFleetRoutesScoreReport','DrivingScoreFleetRoutesScoreReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_EventRebuilding','EventRebuilding','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_InsuranceOperation','InsuranceOperation','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_LowDrivingScoreFleetEvolutionReport','LowDrivingScoreFleetEvolutionReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_WeightedIndexDrivingScoreFleetReport','WeightedIndexDrivingScoreFleetReport','Index','', getdate())
--119
--CUSTOMER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'View_ScoreDrivingFormula','ScoreDrivingFormula','Index','', getdate())
--120
--AddOrEdit
--CustomerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_ScoreDrivingFormula', 'ScoreDrivingFormula', 'AddOrEditScoreDrivingFormula','', getdate())
--121
--Delete
--CustomerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_ScoreDrivingFormula', 'ScoreDrivingFormula', 'DeleteScoreDrivingFormula','', getdate())
--122