--Index
--CustomerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'View_CustomerThresholds','CustomerThresholds','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_CustomerUsers','CustomerUsers','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Customizer','Customizer','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_DriversByVehicle','DriversByVehicle','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Operators','Operators','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PreventiveMaintenanceCatalog','PreventiveMaintenanceCatalog','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_ScoreDrivingSettings','ScoreDrivingSettings','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleCategories','VehicleCategories','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleCostCenters','VehicleCostCenters','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleGroups','VehicleGroups','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehiclesByDriver','VehiclesByDriver','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleUnits','VehicleUnits','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Alarm','Alarm','Admin','Index del ControllerAlarms', GetDate())

--12
--CustomerAdmin-SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'View_Administration','Administration','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_CreditCard','CreditCard','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_DallasKeys','DallasKeys','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Users','Users','Index','', GetDate())
--16
--PartnerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'View_PartnerReports','PartnerReports','Index','', GetDate())
--17
--PartnerAdmin-InsuranceAdmin 
insert into [dbo].[AspNetPermissions] values(newid(),'View_PartnerUsers','PartnerUsers','Index','', GetDate())
--18
--PartnerAdmin-SuperAdmin 
insert into [dbo].[AspNetPermissions] values(newid(),'View_PartnerNews','PartnerNews','Index','', GetDate())
--19
--SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'View_CustomersByPartner','CustomersByPartner','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_EmailNotification','EmailNotification','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_FleetCardsImport','FleetCardsImport','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehiclesImport','VehiclesImport','Index','', GetDate())
--23
--PartnerAdmin--PartnerUser--SuperAdmin--CustomerAdmin 
insert into [dbo].[AspNetPermissions] values(newid(),'View_ServiceStations','ServiceStations','Index','', GetDate())
--24

--PartnerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'View_ApplicantsReport','ApplicantsReport','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_CreditCardsReport','CreditCardsReport','Index','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_MasterCardsReport','MasterCardsReport','Index','', GetDate())
--202
--PartnerAdmin-SuperAdmin 
insert into [dbo].[AspNetPermissions] values(newid(),'View_CreditCard','CreditCard','SearchCreditCard','', GetDate())
--203
--CustomerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'View_CreditCard','CreditCard','SearchCreditCardRequests','', GetDate())
--204
--SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'View_CreditCard','CreditCard','SearchCreditCardVPOS','', GetDate())
--205
***********************************
--NoRole
insert into [dbo].[AspNetPermissions] values(newid(),'View_EcoSystemCardsReport','EcoSystemCardsReport','Index','', GetDate())--NotInUse
***********************************
--AddOrEdit
--CustomerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Alarm','Alarm','AddOrEditAlarm','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Alarm','Alarm','AddOrEditAlarms','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_CreditCard', 'CreditCard', 'AddCreditCardFromRequest', '', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_CreditCard','CreditCard','TransactionRules','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_CustomerUsers','CustomerUsers','AddOrEditUsers','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Customizer','Customizer','Save','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Customizer','Customizer','SaveDashboardModule','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_DriversByVehicle','DriversByVehicle','AddOrEditDriversByVehicle','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Operators','Operators','AddOrEditOperators','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_PreventiveMaintenanceCatalog','PreventiveMaintenanceCatalog','AddOrEditPreventiveMaintenance','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_PreventiveMaintenanceCatalog','PreventiveMaintenanceCatalog','SaveCostList','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_ScoreDrivingSettings','ScoreDrivingSettings','SaveSettings','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehicleCategories','VehicleCategories','AddOrEditVehicleCategories','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehicleCostCenters','VehicleCostCenters','AddOrEditVehicleCostCenters','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehicleGroups','VehicleGroups','AddOrEditVehicleGroups','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehiclesByDriver','VehiclesByDriver','AddOrEditVehiclesByDriver','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehicleUnits','VehicleUnits','AddOrEditVehicleUnits','', GetDate())
--41
--CustomerAdmin--SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_CreditCard', 'CreditCard', 'EditCreditCard', '', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_CreditCard', 'CreditCard', 'AddOrEditCardRequest', '', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_DallasKeys','DallasKeys','AddOrEditDallasKeys','', GetDate())
--44
--PartnerAdmin-InsuranceAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_PartnerUsers','PartnerUsers','AddOrEditUsers','', GetDate())
--45
--PartnerAdmin-SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_PartnerNews','PartnerNews','AddOrEditPartnerNews','', GetDate())
--46
--SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_CustomersByPartner','CustomersByPartner','AddOrEditCustomersByPartner','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_EmailNotification','EmailNotification','EmailNotificationAddOrEdit','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_FleetCardsImport','FleetCardsImport','ProcessCsvFile','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehiclesImport','VehiclesImport','ProcessCsvFile','', GetDate())
--50
--PartnerAdmin-PartnerUser-SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_ServiceStations','ServiceStations','AddOrEditServiceStations','', GetDate())
--51
--SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_CreditCard','CreditCard','CreateCardFile','', GetDate())

--CustomerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_CreditCard','CreditCard','LoadAddOrEditCardRequest','', GetDate())
--******************************************

--Delete
--CustomerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Alarm','Alarm','DeleteAlarms','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_CustomerUsers','CustomerUsers','DeleteUsers','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Operators','Operators','DeleteOperators','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_PreventiveMaintenanceCatalog','PreventiveMaintenanceCatalog','DeletePreventiveMaintenance','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_VehicleCategories','VehicleCategories','DeleteVehicleCategories','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_VehicleCostCenters','VehicleCostCenters','DeleteVehicleCostCenters','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_VehicleGroups','VehicleGroups','DeleteVehicleGroups','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_VehicleUnits','VehicleUnits','DeleteVehicleUnits','', GetDate())
--59
--CustomerAdmin-SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_CreditCard','CreditCard','DeleteCreditCard','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_DallasKeys','DallasKeys','DeleteDallasKeys','', GetDate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Users','Users','DeleteUsers','', GetDate())
--62
--PartnerAdmin-InsuranceAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_PartnerUsers','PartnerUsers','DeleteUsers','', GetDate())
--63
--PartnerAdmin-SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_PartnerNews','PartnerNews','DeletePartnerNews','', GetDate())
--64
--SuperAdmin
--65
--PartnerAdmin-PartnerUser-SuperAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_ServiceStations','ServiceStations','DeleteServiceStatios','', GetDate())
--66
