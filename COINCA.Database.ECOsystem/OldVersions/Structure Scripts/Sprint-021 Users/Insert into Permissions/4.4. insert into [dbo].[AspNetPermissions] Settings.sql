--Index
--(Roles = "INSURANCE_ADMIN, INSURANCE_USER")
insert into [dbo].[AspNetPermissions] values(newid(),'View_InsuranceCustomers','InsuranceCustomers','Index','', getdate())
--136
--(Roles = "PARTNER_ADMIN, PARTNER_USER")
insert into [dbo].[AspNetPermissions] values(newid(),'View_Customers','Customers','Index','', getdate())
--137
--(Roles = "SUPER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'View_Countries','Countries','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Currencies','Currencies','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Fuels','Fuels','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Parameters','Parameters','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PartnerFuel','PartnerFuel','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Partners','Partners','Index','', getdate())
--143
--(Roles = "SUPER_ADMIN,INSURANCE_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'View_Faq','Faq','Index','', getdate())----
insert into [dbo].[AspNetPermissions] values(newid(),'View_Settings','Settings','Index','', getdate())
--145
--(Roles = "SUPER_ADMIN,INSURANCE_ADMIN,INSURANCE_USER")
insert into [dbo].[AspNetPermissions] values(newid(),'View_Faq','Faq','Index','', getdate())
--145
--AddOrEdit
--(Roles = "INSURANCE_ADMIN, INSURANCE_USER")
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_InsuranceCustomers','InsuranceCustomers','AddOrEditCustomers','', getdate())
--146
--(Roles = "PARTNER_ADMIN, PARTNER_USER, SUPER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Customers','Customers','AddCustomers','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Customers','Customers','EditCustomers','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Customers','Customers','AddOrEditUsers','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Customers','Customers','AddorEditTerminal','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Customers','Customers','AddOrEditCard','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Customers','Customers','AddOrEditCardRequest','', getdate())
--152
--(Roles = "SUPER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Countries','Countries','AddOrEditCountries','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Currencies','Currencies','AddOrEditCurrencies','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Fuels','Fuels','AddOrEditFuels','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_PartnerFuel','PartnerFuel','AddOrEditPartnerFuel','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Partners','Partners','AddOrEditPartners','', getdate())
--157
--Delete
--(Roles = "INSURANCE_ADMIN, INSURANCE_USER")
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_InsuranceCustomers','InsuranceCustomers','DeleteCustomers','', getdate())
--158
--(Roles = "PARTNER_ADMIN, PARTNER_USER, SUPER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Customers','Customers','DeleteUsers','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Customers','Customers','DeleteCustomers','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Customers','Customers','DeleteCustomerInfo','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Customers','Customers','DeleteCustomerTerminal','', getdate())
--162
--(Roles = "SUPER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Countries','Countries','DeleteCountries','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Currencies','Currencies','DeleteCurrencies','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Fuels','Fuels','DeleteFuels','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_PartnerFuel','PartnerFuel','DeletePartnerFuel','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Partners','Partners','DeletePartners','', getdate())
--167
