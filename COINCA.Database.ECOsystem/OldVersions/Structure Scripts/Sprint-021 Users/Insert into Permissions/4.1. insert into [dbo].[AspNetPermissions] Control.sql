--Index 
--CUSTOMER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'View_AccumulatedFuelsReport','AccumulatedFuelsReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_ConsolidateLiquidationReport','ConsolidateLiquidationReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Control','Control','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_CostCentersByCredit','CostCentersByCredit','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_CurrentFuelsReport','CurrentFuelsReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_EfficiencyFuelsReport','EfficiencyFuelsReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_FuelsByCredit','FuelsByCredit','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_HobbsMeterPerformanceReport','HobbsMeterPerformanceReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_OdometerTraveledReport','OdometerTraveledReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PerformanceByVehicleComparativeReport','PerformanceByVehicleComparativeReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PreventiveMaintenanceRecordByVehicle','PreventiveMaintenanceRecordByVehicle','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PreventiveMaintenanceReport','PreventiveMaintenanceReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PreventiveMaintenanceVehicle','PreventiveMaintenanceVehicle','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_RealVsBudgetFuelsReport','RealVsBudgetFuelsReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_TimeSlotByVehicle','TimeSlotByVehicle','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_TransactionsReport','TransactionsReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_TrendAndConsumptionReport','TrendAndConsumptionReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleFuelByGroup','VehicleFuelByGroup','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleFuel','VehicleFuel','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleLowPerformanceReport','VehicleLowPerformanceReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehiclesByGroup','VehiclesByGroup','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleSchedule','VehicleSchedule','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Vehicles','Vehicles','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PreventiveMaintenanceRecordByVehicle','PreventiveMaintenanceRecordByVehicle','IndexDetail','', getdate())
--91
--CustomerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'View_Drivers','Drivers','Index','', getdate())
--208
--CUSTOMER_CHANGEODOMETER
insert into [dbo].[AspNetPermissions] values(newid(),'View_AlarmByOdometer','AlarmByOdometer','Index','', getdate())
--92
--**************************************************
--NoRole
insert into [dbo].[AspNetPermissions] values(newid(),'View_ComparativeEfficiencyFuelsReport','ComparativeEfficiencyFuelsReport','Index','', getdate()) --No se Usan
--**************************************************
--AddOrEdit
--CUSTOMER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_ApplyValueToAll','CostCentersByCredit','ApplyValueToAll','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_ApplyValuesToAll','CostCentersByCredit','ApplyValuesToAll','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_AddOrEditPreventiveMaintenanceRecordByVehicle','PreventiveMaintenanceRecordByVehicle','AddOrEditPreventiveMaintenanceRecordByVehicle','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_SaveRecordList','PreventiveMaintenanceRecordByVehicle','SaveRecordList','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_AddOrEditPreventiveMaintenanceVehicle','PreventiveMaintenanceVehicle','AddOrEditPreventiveMaintenanceVehicle','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_SaveRecordList','PreventiveMaintenanceVehicle','SaveRecordList','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_AddOrEditTimeSlotByVehicle','TimeSlotByVehicle','AddOrEditTimeSlotByVehicle','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_ApplyAllVehicleFuel','VehicleFuelByGroup','ApplyAllVehicleFuel','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_ApplyValuesToAll','VehicleFuelByGroup','ApplyValuesToAll','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_AddOrEditVehicleFuel','VehicleFuel','AddOrEditVehicleFuel','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_AddOrEditVehiclesByGroup','VehiclesByGroup','AddOrEditVehiclesByGroup','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_AddOrEditVehicleSchedule','VehicleSchedule','AddOrEditVehicleSchedule','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_AddOrEditVehicles','Vehicles','AddOrEditVehicles','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_EditVehiclesReferences','Vehicles','EditVehiclesReferences','', getdate())
--106
--CUSTOMER_CHANGEODOMETER
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_AddOrEditAlarmByOdometer','AlarmByOdometer','AddOrEditAlarmByOdometer','', getdate())
--107

--CustomerAdmin
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_AddOrEditDrivers','Drivers','AddOrEditDrivers','', getdate())

--**************************************************
--NoRole
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Post','CustomerCredit','Post','', getdate())--No se Usa
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Post','Transaction','Post','', getdate())--No se Usa
--**************************************************
--Delete 
--CUSTOMER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_PreventiveMaintenanceRecordByVehicle','PreventiveMaintenanceRecordByVehicle','DeleteRecord','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_VehicleFuel','VehicleFuel','DeleteVehicleFuel','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Vehicles','Vehicles','DeleteVehicles','', getdate())
--110
--CustomerAdmin
insert into [dbo].[AspNetPermissions] values(newid(), 'Delete_Drivers','Drivers','DeleteDrivers','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(), 'Delete_PreventiveMaintenanceVehicle','PreventiveMaintenanceVehicle','DeletePreventiveMaintenanceVehicle','', getdate())
