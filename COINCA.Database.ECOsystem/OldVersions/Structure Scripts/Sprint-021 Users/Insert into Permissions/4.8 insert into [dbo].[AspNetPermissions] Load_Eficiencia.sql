Use [ECOsystemDev]


---Load
--CUSTOMER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'Load_GeoFences','GeoFences','LoadGeoFences','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PassControlGroup','PassControlGroup','LoadPassControlGroup','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PassControlSubUnit','PassControlSubUnit','LoadPassControlSubUnit','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PassControlUnits','PassControlUnit','LoadPassControlUnits','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_RealTime','RealTime','LoadRealTime','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_RealTimeCostCenter','RealTimeCostCenter','LoadRealTimeCostCenter','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_RealTimeGroup','RealTimeGroup','LoadRealTimeGroup','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_RealTimeUnit','RealTimeUnit','LoadRealTimeUnit','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Reconstructing','Reconstructing','LoadReconstructing','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Routes','Routes','LoadRoutes','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_SeeRoute','SeeRoute','LoadSeeRoute','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehicleByRoute','VehicleByRoute','LoadVehicleByRoute','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehicleGroupsByGeoFence','VehicleGroupsByGeoFence','LoadVehicleGroupsByGeoFence','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehicleRoute','VehicleRoute','LoadVehicleRoute','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehiclesByGroup','VehiclesByGeoFence','LoadVehiclesByGroup','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehicleCostCentersByGeoFence','VehicleSubUnitsByGeoFence','LoadVehicleCostCentersByGeoFence','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehicleUnitsByGeoFence','VehicleUnitsByGeoFence','LoadVehicleUnitsByGeoFence','', getdate())


	
		



