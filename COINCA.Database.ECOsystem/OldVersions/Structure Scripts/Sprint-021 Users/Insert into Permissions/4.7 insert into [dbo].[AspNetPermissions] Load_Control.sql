Use [ECOsystemDev]

---Load
--CUSTOMER_CHANGEODOMETER
insert into [dbo].[AspNetPermissions] values(newid(),'Load_AlarmByOdometer','AlarmByOdometer','LoadAlarmByOdometer','', getdate())


---Load
--CUSTOMER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PreventiveMaintenanceRecordByVehicle','PreventiveMaintenanceRecordByVehicle','LoadPreventiveMaintenanceRecordByVehicle','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PreventiveMaintenanceRecordByVehicleDetail','PreventiveMaintenanceRecordByVehicle','LoadPreventiveMaintenanceRecordByVehicleDetail','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PreventiveMaintenanceVehicle','PreventiveMaintenanceVehicle','LoadPreventiveMaintenanceVehicle','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PreventiveMaintenanceRecordByVehicleDetail','PreventiveMaintenanceVehicle','LoadPreventiveMaintenanceRecordByVehicleDetail','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_TimeSlotByVehicle','TimeSlotByVehicle','LoadTimeSlotByVehicle','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehicleFuel','VehicleFuel','LoadVehicleFuel','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehiclesByGroup','VehiclesByGroup','LoadVehiclesByGroup','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehicleSchedule','VehicleSchedule','LoadVehicleSchedule','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Vehicles','Vehicles','LoadVehicles','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehiclesReferences','Vehicles','LoadVehiclesReferences','', getdate())

	
		



