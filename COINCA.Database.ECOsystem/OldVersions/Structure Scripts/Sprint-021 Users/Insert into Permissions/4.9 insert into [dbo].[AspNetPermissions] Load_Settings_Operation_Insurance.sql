--Load
--SUPER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Countries','Countries','LoadCountries','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Currencies','Currencies','LoadCurrencies','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Fuels','Fuels','LoadFuels','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PartnerFuel','PartnerFuel','LoadPartnerFuel','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Partners','Partners','LoadPartners','', getdate())
--Load
--PARTNER_ADMIN, PARTNER_USER, SUPER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Customers','Customers','LoadCustomers','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Users','Customers','LoadUsers','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_CustomerCard','Customers','LoadCustomerCard','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PreviousCustomerCardRequest','Customers','LoadPreviousCustomerCardRequest','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Counties','Customers','LoadCounties','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Cities','Customers','LoadCities','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_CustomerCardRequest','Customers','LoadCustomerCardRequest','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_TerminalRequest','Customers','LoadTerminalRequest','', getdate())


--Load
--PARTNER_ADMIN--PARTNER_USER
insert into [dbo].[AspNetPermissions] values(newid(),'Load_UploadRequestModal','Customers','LoadUploadRequestModal','', getdate())

--INSURANCE_ADMIN--INSURANCE_USER
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Customers','InsuranceCustomers','LoadCustomers','', getdate())

--Super_Admin
insert into [dbo].[AspNetPermissions] values(newid(), 'View_UserRolesPermission', 'UserRolesPermission', 'Index', '', getdate())
insert into [dbo].[AspNetPermissions] values(newid(), 'View_UserRolesPermission', 'UserRolesPermission', 'Roles', '', getdate())
insert into [dbo].[AspNetPermissions] values(newid(), 'View_UserRolesPermission', 'UserRolesPermission', 'Modules', '', getdate())



