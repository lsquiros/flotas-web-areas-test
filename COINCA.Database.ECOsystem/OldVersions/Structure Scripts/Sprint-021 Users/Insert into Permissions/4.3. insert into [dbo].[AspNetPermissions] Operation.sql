--Index
--(Roles = "CUSTOMER_ADMIN, CUSTOMER_USER")
insert into [dbo].[AspNetPermissions] values(newid(),'View_DrivingTimePerDriverReport','DrivingTimePerDriverReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_ScoreGlobalByAdminReport','ScoreGlobalByAdminReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_ScoreGlobalByRoutesReport','ScoreGlobalByRoutesReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_ScoreGlobalReport','ScoreGlobalReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_ScoreSpeedAdminReport','ScoreSpeedAdminReport','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_ScoreSpeedRoutesReport','ScoreSpeedRoutesReport','Index','', getdate())
--128
--(Roles = "CUSTOMER_ADMIN, CUSTOMER_USER, SUPER_ADMIN,INSURANCE_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'View_Operation','Operation','Index','', getdate())
--129
--(Roles = "SUPER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'View_ScoreTypes','ScoreTypes','Index','', getdate())
--130
--(Roles = "CUSTOMER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'View_WeightedScoreSettings','WeightedScoreSettings','Index','', getdate())
--131
--AddOrEdit
--(Roles = "SUPER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_ScoreTypes','ScoreTypes','AddOrEditScoreTypes','', getdate())
--132
--(Roles = "CUSTOMER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_WeightedScoreSettings','WeightedScoreSettings','AddOrEditWeightedScoreSettings','', getdate())
--133
--Delete
--(Roles = "SUPER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_ScoreTypes','ScoreTypes','DeleteScoreTypes','', getdate())
--134
--(Roles = "CUSTOMER_ADMIN")
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_WeightedScoreSettings','WeightedScoreSettings','DeleteWeightedScoreSettings','', getdate())
--135