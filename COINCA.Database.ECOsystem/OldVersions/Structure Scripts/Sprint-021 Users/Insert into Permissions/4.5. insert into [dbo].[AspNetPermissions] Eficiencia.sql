Use [ECOsystemDev]

---Index
--CUSTOMER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'View_GeoFences','GeoFences','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Localization','Localization','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Measuring','Measuring','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PassControlGroup','PassControlGroup','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PassControlSubUnit','PassControlSubUnit','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PassControlUnit','PassControlUnit','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_PassControlVehicle','PassControlVehicle','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_RealTime','RealTime','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_RealTimeCostCenter','RealTimeCostCenter','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_RealTimeGroup','RealTimeGroup','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_RealTimeUnit','RealTimeUnit','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Reconstructing','Reconstructing','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_Routes','Routes','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_SeeRoute','SeeRoute','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleByRoute','VehicleByRoute','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleGroupsByGeoFence','VehicleGroupsByGeoFence','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleRoute','VehicleRoute','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehiclesByGeoFence','VehiclesByGeoFence','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleCostCentersByGeoFence','VehicleCostCentersByGeoFence','Index','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VehicleUnitsByGeoFence','VehicleUnitsByGeoFence','Index','', getdate())
--187
--AddOrEdit
--CUSTOMER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_GeoFences','GeoFences','AddOrEditGeoFences','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Routes','Routes','AddOrEditRoutes','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_Routes','Routes','SaveRouteDetail','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehicleByRoute','VehicleByRoute','AddOrEditVehicleByRoute','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehicleGroupsByGeoFence','VehicleGroupsByGeoFence','AddOrEditVehicleGroupsByGeoFence','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehiclesByGeoFence','VehiclesByGeoFence','AddOrEditVehiclesByGroup','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehicleCostCentersByGeoFence','VehicleCostCentersByGeoFence','AddOrEditVehicleCostCentersByGeoFence','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehicleUnitsByGeoFence','VehicleUnitsByGeoFence','AddOrEditVehicleUnitsByGroup','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'AddOrEdit_VehicleByRoute','VehicleByRoute','SaveList','', getdate())
--196
--Delete
--CUSTOMER_ADMIN
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_GeoFences','GeoFences','DeleteGeoFences','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Delete_Routes','Routes','DeleteRoutes','', getdate())
--198