Use [ECOsystemDev]

---Load
--CUSTOMER_ADMIN - 211
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Alarm','Alarm','LoadAlarm','Edicion de alarmas', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Alarms','Alarm','LoadAlarms','Edicion de alarmas', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_CardRequest','CreditCard','LoadCardRequest','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_RetrieveCreditCardSale','CreditCard','LoadRetrieveCreditCardSale','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Users','CustomerUsers','LoadUsers','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_DriversByVehicle','DriversByVehicle','LoadDriversByVehicle','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Operators','Operators','LoadOperators','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_CreditCard','CreditCard','CardRequest','Carga solicitud de tarjeta(index)', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PreventiveMaintenance','PreventiveMaintenanceCatalog','LoadPreventiveMaintenance	','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehicleCategories','VehicleCategories','LoadVehicleCategories','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehicleCostCenters','VehicleCostCenters','LoadVehicleCostCenters','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehicleGroups','VehicleGroups','LoadVehicleGroups','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_VehiclesByDriver','VehiclesByDriver','LoadVehiclesByDriver','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_LoadVehicleUnits','VehicleUnits','LoadVehicleUnits','', getdate())
--Load
--CUSTOMER_ADMIN--SUPER_ADMIN--225
insert into [dbo].[AspNetPermissions] values(newid(),'Load_CreditCard','CreditCard','LoadCreditCard	','Edicion de tarjetas de credito', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_DallasKeys','DallasKeys','LoadDallasKeys','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_Users','Users','LoadUsers','', getdate())
--Load
--SUPER_ADMIN-232
insert into [dbo].[AspNetPermissions] values(newid(),'Load_CreditCardVPOS','CreditCard','LoadCreditCardVPOS','Edicion de tarjetas de credito VPOS', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_CustomersByPartner','CustomersByPartner','LoadCustomersByPartner','Edicion de tarjetas de credito VPOS', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_EmailsNotifications','EmailNotification','LoadEmailsNotifications','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'View_VPOS','CreditCard','VPOS','Funciona como un index)', getdate())
--Load
--PARTNER_ADMIN--SUPER_ADMIN --233
insert into [dbo].[AspNetPermissions] values(newid(),'Load_PartnerNews','PartnerNews','LoadPartnerNews','', getdate())
--PARTNER_ADMIN,INSURANCE_ADMIN--234
insert into [dbo].[AspNetPermissions] values(newid(),'Load_LoadUsers','PartnerUsers','LoadUsers','', getdate())
--PARTNER_ADMIN, PARTNER_USER, SUPER_ADMIN--236
insert into [dbo].[AspNetPermissions] values(newid(),'Load_ServiceStations','ServiceStations','LoadServiceStations','', getdate())
insert into [dbo].[AspNetPermissions] values(newid(),'Load_States_ServiceStations','ServiceStations','GetStates','', getdate())
--("PARTNER_ADMIN") || User.IsInRole("PARTNER_USER")
insert into [dbo].[AspNetPermissions] values(newid(),'View_TransactionsReport','TransactionsReport','Admin','', getdate())




