USE ECOSystemQA
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerAuthByPartner]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerAuthByPartner]
GO

/****** Object:  Table [dbo].[CustomerAuthByPartner]    Script Date: 12/29/2015 08:43:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CustomerAuthByPartner](
	[PartnerUserId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CustomerAuthByPartner]  WITH CHECK ADD  CONSTRAINT [FK_CustomerAuthByPartner_AspNetRoles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
GO

ALTER TABLE [dbo].[CustomerAuthByPartner] CHECK CONSTRAINT [FK_CustomerAuthByPartner_AspNetRoles]
GO



