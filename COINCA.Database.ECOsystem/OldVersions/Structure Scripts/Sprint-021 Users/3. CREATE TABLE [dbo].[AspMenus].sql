USE [ECOsystemQA]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspMenus](
	[Id] [int] NOT NULL Identity(1,1),
	[Name] [nvarchar](128) NOT NULL,
	[Ico] [nvarchar](100) NULL,
	[PermissionId] [nvarchar](128) NULL,
	[Parent] [int] NULL,
	[Header] [int] NULL,
	[Order] [int] NULL,
	[InsertDate] [datetime]
 CONSTRAINT [PK_AspNetMenus] PRIMARY KEY CLUSTERED 	
 (
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
