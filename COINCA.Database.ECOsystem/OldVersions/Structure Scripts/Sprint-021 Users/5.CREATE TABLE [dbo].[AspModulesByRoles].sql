USE [ECOsystemQA]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspModulesByRoles](
	[RoleId] [nvarchar](128) NOT NULL,
	[ModuleId] [int] NOT NULL,
 CONSTRAINT [PK_AspModulesByRoles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[AspModulesByRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspModulesByRoles_dbo.AspNetModules_ModuleId] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[AspMenusModules] ([Id])
GO

ALTER TABLE [dbo].[AspModulesByRoles] CHECK CONSTRAINT [FK_dbo.AspModulesByRoles_dbo.AspNetModules_ModuleId]
GO

ALTER TABLE [dbo].[AspModulesByRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspModulesByRoles_dbo.AspNetModules_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
GO

ALTER TABLE [dbo].[AspModulesByRoles] CHECK CONSTRAINT [FK_dbo.AspModulesByRoles_dbo.AspNetModules_RoleId]
GO
