use [ECOsystemQA]

ALTER TABLE [dbo].[AspNetRoles]
ADD   [Master] bit
	  ,[Parent] NVARCHAR(128)
	  ,[Active] bit
	  ,[CreatedBy] int 

--*****************************************

UPDATE [dbo].[AspNetRoles]
SET [Master] = 1

--*****************************************

UPDATE [dbo].[AspNetRoles]
SET [Active] = 1

--*****************************************
insert into aspnetroles values ('F024C5BD-C5F4-408C-8A86-B5DBA55435E0', 'Super Usuario Cliente', 0, '9baac678-473a-4f96-9d67-9f2a5af778f3', 1, null)
insert into aspnetroles values ('10703CAB-999E-4293-A423-28C94620EE6D', 'Super Usuario Socio', 0, 'A0465644-860E-4B8E-8779-7719B65D5E6E', 1, null)
insert into aspnetroles values ('21C5EB03-6C0D-4B16-8F0A-A10C994A8157', 'Super Usuario Coinca', 0, '4b356447-c286-45d6-bace-d1cc1ae298db', 1, null)
