USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetPermissions]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetPermissions]
GO

/****** Object:  Table [dbo].[AspNetPermissions]    Script Date: 12/7/2015 10:43:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetPermissions](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Resource] [nvarchar](128) NOT NULL,
	[Action] [nvarchar](128) NOT NULL,
	[Description] [nvarchar](128) NULL,
	[InsertionDate] [datetime],
 CONSTRAINT [PK_AspNetPermissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO


