USE [ECOsystemDev]

ALTER TABLE [General].[Countries]
ADD GeopoliticalLevel1 VARCHAR(50) NULL,
	GeopoliticalLevel2 VARCHAR(50) NULL,
	GeopoliticalLevel3 VARCHAR(50) NULL

GO