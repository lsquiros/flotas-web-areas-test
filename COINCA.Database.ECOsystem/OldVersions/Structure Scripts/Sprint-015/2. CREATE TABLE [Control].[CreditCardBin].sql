USE [ECOsystemDev]
GO

IF OBJECT_ID(N'[Control].[CreditCardBin]', N'U') IS NOT NULL
BEGIN
  DROP TABLE [Control].[CreditCardBin]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Control].[CreditCardBin](
	[BinId] [int] IDENTITY(1,1) NOT NULL,
	[PartnerId] [int] NULL,
	[CardType] [char](1) NULL,
	[BinData] [nvarchar](max) NULL,
 CONSTRAINT [PK_Control.BinId] PRIMARY KEY CLUSTERED 
(
	[BinId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

SET ANSI_PADDING OFF
GO


