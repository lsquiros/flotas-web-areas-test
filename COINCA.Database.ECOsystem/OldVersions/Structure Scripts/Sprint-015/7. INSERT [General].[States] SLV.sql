USE [ECOsystemDev]
GO
	-- DECLARES
	DECLARE @code CHAR(3);
	DECLARE @countryId INT;

	-- SETS
	SET @code = 'SLV';
	SET @countryId = (SELECT CountryId FROM [General].[Countries] WHERE Code = @code);

	-- INSERTS
	INSERT INTO 
		[General].[States] ([Name], [CountryId], [Code]) 
	VALUES 
		('Ahuachapan', @countryId, 1),
		('Santa Ana', @countryId, 2),
		('Sonsonate', @countryId, 3),
		('Chalatenango', @countryId, 4),
		('La Libertad', @countryId, 5),
		('San Salvador', @countryId, 6),
		('Cuscatlan', @countryId, 7),
		('La Paz', @countryId, 8),
		('Cabanas', @countryId, 9),
		('San Vicente', @countryId, 10),
		('Usulutan', @countryId, 11),
		('San Miguel', @countryId, 12),
		('Morazan', @countryId, 13),
		('La Union', @countryId, 14)

--END
GO