USE [ECOsystemDev]
GO
	-- DECLARES
	DECLARE @code CHAR(3);
	DECLARE @countryId INT;

	-- SETS
	SET @code = 'HND';
	SET @countryId = (SELECT CountryId FROM [General].[Countries] WHERE Code = @code);

	-- INSERTS
	INSERT INTO 
		[General].[States] ([Name], [CountryId], [Code]) 
	VALUES 
		('Atlantica', @countryId, 1),
		('Choluteca', @countryId, 2),
		('Colon', @countryId, 3),
		('Comayagua', @countryId, 4),
		('Copan', @countryId, 5),
		('Cortes', @countryId, 6),
		('El Paraiso', @countryId, 7),
		('Francisco Morazan', @countryId, 8),
		('Gracias a Dios', @countryId, 9),
		('Intibuca', @countryId, 10),
		('Islas de la Bahia', @countryId, 11),
		('La Paz', @countryId, 12),
		('Lempira', @countryId, 13),
		('Ocotepeque', @countryId, 14),
		('Olancho', @countryId, 15),
		('Santa Barbara', @countryId, 16),
		('Valle', @countryId, 17),
		('Yoro', @countryId, 18)

--END
GO