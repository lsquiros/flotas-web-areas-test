USE [ECOsystemDev]
GO

IF OBJECT_ID(N'[General].[WServicesByPartner]', N'U') IS NOT NULL
BEGIN
  DROP TABLE [General].[WServicesByPartner]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [General].[WServicesByPartner](
	[WSId] [int] IDENTITY(1,1) NOT NULL,
	[PartnerId] [int] NULL,
	[Name] [nvarchar](100) NULL,
	[Environment] [nvarchar](20) NULL, -- DEV | PROD
	[UserAuth] [nvarchar](256) NULL,
	[PasswordAuth] [nvarchar](MAX) NULL,
	[WSType] [nvarchar](50) NULL, -- WCF | SOAP
	[UriService] [nvarchar](MAX) NULL,
	[IsDeleted] [bit] DEFAULT(0),
	[InsertUserId] [int] NULL,
	[InsertDate] [datetime] NOT NULL DEFAULT (DATEADD(hour, -6, GETDATE())),
	[ModifyUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_General.WSId] PRIMARY KEY CLUSTERED 
(
	[WSId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

SET ANSI_PADDING OFF
GO


