USE [ECOsystemDev]
GO

IF OBJECT_ID(N'[General].[EventLog]', N'U') IS NOT NULL
BEGIN
  DROP TABLE [General].[EventLog]
END
GO

/****** Object:  Table [General].[EventLog]    Script Date: 8/26/2015 4:24:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [General].[EventLog](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[EventType] [varchar](15) NULL,
	[UserId] [int] NULL,
	[CustomerId] [int] NULL,
	[PartnerId] [int] NULL,
	[Controller] [varchar](50) NULL,
	[Action] [varchar](50) NULL,
	[Mensaje] [varchar](1000) NULL,
	[IsInfo] [bit] NOT NULL DEFAULT (0),
	[IsWarning] [bit] NOT NULL DEFAULT (0),
	[IsError] [bit] NOT NULL DEFAULT (0),
	[LogDateTime] [datetime] NOT NULL DEFAULT (DATEADD(hour, -6, GETDATE())),
 CONSTRAINT [PK_General.EventLogId] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO


