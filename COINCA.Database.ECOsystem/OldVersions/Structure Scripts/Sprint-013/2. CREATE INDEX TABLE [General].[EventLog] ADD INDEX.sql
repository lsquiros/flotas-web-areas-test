USE [ECOsystemDev]
GO

CREATE INDEX IDX_EVENT_LOG_SEARCH_TIME
ON [General].[EventLog] 
	([LogDateTime]);
GO
CREATE INDEX IDX_EVENT_LOG_SEARCH_USERID
ON [General].[EventLog] 
	([UserId]);
GO
CREATE INDEX IDX_EVENT_LOG_SEARCH_CUSTOMERID
ON [General].[EventLog] 
	([CustomerId]);
GO
CREATE INDEX IDX_EVENT_LOG_SEARCH_PARTNERID
ON [General].[EventLog] 
	([PartnerId]);
GO
CREATE INDEX IDX_EVENT_LOG_SEARCH_USER_CUSTOMERID
ON [General].[EventLog] 
	([UserId], [CustomerId], [LogDateTime]);
GO
CREATE INDEX IDX_EVENT_LOG_SEARCH_USER_PARTNERID
ON [General].[EventLog] 
	([UserId], [PartnerId], [LogDateTime]);

