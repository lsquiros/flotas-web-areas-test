USE [ECOsystemDev]
GO

IF OBJECT_ID(N'[General].[DallasKeys]', N'U') IS NOT NULL
BEGIN
  DROP TABLE [General].[DallasKeys]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [General].[DallasKeys](
	[DallasId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[DallasCode] [varchar](100) NULL,
	[IsUsed] [bit] DEFAULT(0),
	[IsDeleted] [bit] DEFAULT(0),
	[InsertUserId] [int] NULL,
	[InsertDate] [datetime] NOT NULL DEFAULT (DATEADD(hour, -6, GETDATE())),
	[ModifyUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_General.DallasId] PRIMARY KEY CLUSTERED 
(
	[DallasId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

SET ANSI_PADDING OFF
GO


