USE [ECOsystemDev]
GO

IF OBJECT_ID(N'[General].[DallasKeysByDriver]', N'U') IS NOT NULL
BEGIN
  DROP TABLE [General].[DallasKeysByDriver]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [General].[DallasKeysByDriver](
	[DallasByDriverId] [int] IDENTITY(1,1) NOT NULL,
	[DallasId] [int] NULL,
	[UserId] [int] NULL,
	[InsertUserId] [int] NULL,
	[InsertDate] [datetime] NOT NULL DEFAULT (DATEADD(hour, -6, GETDATE())),
	[ModifyUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_General.DallasByDriverId] PRIMARY KEY CLUSTERED 
(
	[DallasByDriverId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

--FOREIGN KEY DALLAS_KEYS_BY_DRIVER ---> DALLAS_KEYS
ALTER TABLE [General].[DallasKeysByDriver]  WITH CHECK ADD  CONSTRAINT [FK_DallasKeysByDriver_DallasKeys] FOREIGN KEY([DallasId])
REFERENCES [General].[DallasKeys] ([DallasId])
GO

ALTER TABLE [General].[DallasKeysByDriver] CHECK CONSTRAINT [FK_DallasKeysByDriver_DallasKeys]

GO

--FOREIGN KEY DALLAS_KEYS_BY_DRIVER ---> USERS
ALTER TABLE [General].[DallasKeysByDriver]  WITH CHECK ADD  CONSTRAINT [FK_DallasKeysByDriver_Users] FOREIGN KEY([UserId])
REFERENCES [General].[Users] ([UserId])
GO

ALTER TABLE [General].[DallasKeysByDriver] CHECK CONSTRAINT [FK_DallasKeysByDriver_Users]

SET ANSI_PADDING OFF
GO


