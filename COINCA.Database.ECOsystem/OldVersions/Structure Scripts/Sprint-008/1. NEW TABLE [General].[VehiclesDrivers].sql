USE [ECOsystem]
GO

/****** Object:  Table [General].[VehiclesDrivers]    Script Date: 7/21/2015 2:05:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [General].[VehiclesDrivers](
	[UserId] [int] NOT NULL,
	[VehicleId] [int] NOT NULL,
	[InsertDate] [datetime] NULL,
	[InsertUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[RowVersion] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[VehicleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [General].[VehiclesDrivers]  WITH CHECK ADD  CONSTRAINT [FK_VehiclesDrivers_Users] FOREIGN KEY([UserId])
REFERENCES [General].[Users] ([UserId])
GO

ALTER TABLE [General].[VehiclesDrivers] CHECK CONSTRAINT [FK_VehiclesDrivers_Users]
GO

ALTER TABLE [General].[VehiclesDrivers]  WITH CHECK ADD  CONSTRAINT [FK_VehiclesDrivers_Vehicles] FOREIGN KEY([VehicleId])
REFERENCES [General].[Vehicles] ([VehicleId])
GO

ALTER TABLE [General].[VehiclesDrivers] CHECK CONSTRAINT [FK_VehiclesDrivers_Vehicles]
GO


