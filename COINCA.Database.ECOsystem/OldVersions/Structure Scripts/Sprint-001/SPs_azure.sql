--~ Commented out USE [database] statement because USE statement is not supported to another database.
-- USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Control].[Sp_ComparativeFuelsReport_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ComparativeFuelsReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_ComparativeFuelsReport_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_MonthlyClosing]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MonthlyClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_MonthlyClosing]
GO
/****** Object:  StoredProcedure [Control].[Sp_TrendFuelsReportByGroup_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TrendFuelsReportByGroup_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_TrendFuelsReportByGroup_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_TrendFuelsReportBySubUnit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TrendFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_TrendFuelsReportBySubUnit_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_TrendFuelsReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TrendFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_TrendFuelsReportByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [Operation].[Sp_ScoreSpeedPerMonthJob]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreSpeedPerMonthJob]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_ScoreSpeedPerMonthJob]
GO
/****** Object:  StoredProcedure [Control].[Sp_VehicleFuelByGroup_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuelByGroup_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_VehicleFuelByGroup_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_VehicleLowPerformanceReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleLowPerformanceReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_VehicleLowPerformanceReport_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_MonthlyClosing_LowPerformanceEmails]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MonthlyClosing_LowPerformanceEmails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_MonthlyClosing_LowPerformanceEmails]
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetWightedIndexScoreDrivingFleet]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetWightedIndexScoreDrivingFleet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Insurance].[Sp_GetWightedIndexScoreDrivingFleet]
GO
/****** Object:  StoredProcedure [General].[Sp_Job_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Job_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Job_Alarm]
GO
/****** Object:  StoredProcedure [General].[Sp_LastPerformanceByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_LastPerformanceByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_LastPerformanceByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PerformanceByVehicle_Add]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PerformanceByVehicle_Add]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PerformanceByVehicle_Add]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]
GO
/****** Object:  StoredProcedure [Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceReport_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_ConsolidateLiquidationReport_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ConsolidateLiquidationReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_ConsolidateLiquidationReport_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CostCentersByCredit_AddOrEdit]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CostCentersByCredit_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CostCentersByCredit_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_AccumulatedFuelsReportBySubUnit_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AccumulatedFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_AccumulatedFuelsReportBySubUnit_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_AccumulatedFuelsReportByVehicleGroup_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AccumulatedFuelsReportByVehicleGroup_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_AccumulatedFuelsReportByVehicleGroup_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_EfficiencyFuelsReportBySubUnit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_EfficiencyFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_EfficiencyFuelsReportBySubUnit_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_EfficiencyFuelsReportByVehicleGroup_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_EfficiencyFuelsReportByVehicleGroup_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_EfficiencyFuelsReportByVehicleGroup_Retrieve]
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetDrivingScoreFleetDataSourceReport]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetDrivingScoreFleetDataSourceReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Insurance].[Sp_GetDrivingScoreFleetDataSourceReport]
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetLowScoreDrivingFleetEvolution]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetLowScoreDrivingFleetEvolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Insurance].[Sp_GetLowScoreDrivingFleetEvolution]
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetScoreDrivingFleet]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetScoreDrivingFleet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Insurance].[Sp_GetScoreDrivingFleet]
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetScoreDrivingFleetEvolution]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetScoreDrivingFleetEvolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Insurance].[Sp_GetScoreDrivingFleetEvolution]
GO
/****** Object:  StoredProcedure [Operation].[Sp_GetScoreSpeedAllDriver]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetScoreSpeedAllDriver]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_GetScoreSpeedAllDriver]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFence_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFence_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_GeoFence_Alarm]
GO
/****** Object:  StoredProcedure [Operation].[Sp_GetGlobalScoreAllDriver]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetGlobalScoreAllDriver]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_GetGlobalScoreAllDriver]
GO
/****** Object:  StoredProcedure [Operation].[Sp_GetGlobalScoreByVehicle]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetGlobalScoreByVehicle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_GetGlobalScoreByVehicle]
GO
/****** Object:  StoredProcedure [Operation].[Sp_GetGlobalScoreDataSourceAllDriver]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetGlobalScoreDataSourceAllDriver]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_GetGlobalScoreDataSourceAllDriver]
GO
/****** Object:  StoredProcedure [Control].[Sp_EfficiencyFuelsReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_EfficiencyFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_EfficiencyFuelsReportByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardVPOS_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardVPOS_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCardVPOS_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_AfterSchedule_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AfterSchedule_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_AfterSchedule_Alarm]
GO
/****** Object:  StoredProcedure [Control].[Sp_AlarmByOdometer_AddOrEdit]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AlarmByOdometer_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_AlarmByOdometer_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_AlarmByOdometer_Disabled]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AlarmByOdometer_Disabled]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_AlarmByOdometer_Disabled]
GO
/****** Object:  StoredProcedure [Control].[Sp_AlarmByOdometer_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AlarmByOdometer_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_AlarmByOdometer_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CostCentersByCredit_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CostCentersByCredit_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CostCentersByCredit_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_AddOrEdit]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCard_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCard_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardByNumber_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardByNumber_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCardByNumber_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardCloseCreditLimit_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardCloseCreditLimit_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCardCloseCreditLimit_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportByVehicleGroup_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByVehicleGroup_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicleGroup_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceVehicles_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceVehicles_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceVehicles_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_RealVsBudgetFuelsReportBySubUnit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_RealVsBudgetFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportBySubUnit_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_RealVsBudgetFuelsReportByVehicleGroup_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_RealVsBudgetFuelsReportByVehicleGroup_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportByVehicleGroup_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_PreventiveMaintenanceReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PreventiveMaintenanceReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_PreventiveMaintenanceReport_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_RoutePointTimeStop_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_RoutePointTimeStop_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_RoutePointTimeStop_Alarm]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesByVehicleList_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesByVehicleList_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_RoutesByVehicleList_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_PerformanceByVehicleReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PerformanceByVehicleReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_PerformanceByVehicleReport_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalogList_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalogList_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCatalogList_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenance_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenance_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenance_Alarm]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceByVehicle_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceByVehicle_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_HobbsMeterPerformanceReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_HobbsMeterPerformanceReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_HobbsMeterPerformanceReportByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_OdometerVsRoute_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_OdometerVsRoute_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_OdometerVsRoute_Alarm]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlGroupInfo_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlGroupInfo_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_PassControlGroupInfo_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlSubUnitInfo_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlSubUnitInfo_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_PassControlSubUnitInfo_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlUnitInfo_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlUnitInfo_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_PassControlUnitInfo_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlVehicleDriver_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlVehicleDriver_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_PassControlVehicleDriver_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Vehicles_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Vehicles_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_Vehicles_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Vehicles_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_Vehicles_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Vehicles_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehiclesByGeoFence_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehiclesByGeoFence_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehiclesByGeoFence_AddOrEdit]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehiclesByGeoFence_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehiclesByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehiclesByGeoFence_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_VehiclesByGroup_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByGroup_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehiclesByGroup_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_VehiclesByGroup_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByGroup_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehiclesByGroup_Retrieve]
GO
/****** Object:  StoredProcedure [dbo].[Sp_VehiclesByLevel_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_VehiclesByLevel_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_VehiclesByLevel_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleSchedule_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleSchedule_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleSchedule_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleSchedule_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleSchedule_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleSchedule_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_VehicleFuelByGroup_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuelByGroup_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_VehicleFuelByGroup_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_VehicleFuel_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_VehicleFuel_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_VehicleFuel_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_VehicleFuel_Delete]
GO
/****** Object:  StoredProcedure [Control].[Sp_VehicleFuel_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_VehicleFuel_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_TimeSlotByVehicle_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_TimeSlotByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_TimeSlotByVehicle_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_TimeSlotByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_TimeSlotByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_TimeSlotByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceRecordByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_Transactions_AddFromAPI]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Transactions_AddFromAPI]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_Transactions_AddFromAPI]
GO
/****** Object:  StoredProcedure [Control].[Sp_Transactions_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Transactions_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_Transactions_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_TransactionsForReverse_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsForReverse_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_TransactionsForReverse_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_TransactionsReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_TransactionsReport_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_TrendAndConsumptionComparativeReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TrendAndConsumptionComparativeReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_TrendAndConsumptionComparativeReport_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_ValidateTimeSlotByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ValidateTimeSlotByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_ValidateTimeSlotByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleByPlate_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleByPlate_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleByPlate_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleByRoute_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleByRoute_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleByRoute_AddOrEdit]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleByRoute_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleByRoute_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleByRoute_Delete]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleByRoute_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleByRoute_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleByRoute_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCategories_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCategories_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleCategories_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCategories_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCategories_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleCategories_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCategories_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCategories_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleCategories_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesStopsList_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesStopsList_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_RoutesStopsList_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_SAIndicators_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_SAIndicators_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_SAIndicators_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_UnitReference_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_UnitReference_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_UnitReference_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleCostCentersByGeoFence_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleCostCentersByGeoFence_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleCostCentersByGeoFence_AddOrEdit]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleCostCentersByGeoFence_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleCostCentersByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleCostCentersByGeoFence_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_VehiclesReferences_Edit]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesReferences_Edit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehiclesReferences_Edit]
GO
/****** Object:  StoredProcedure [General].[Sp_VehiclesReferences_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesReferences_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehiclesReferences_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlVehicleInfo_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlVehicleInfo_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_PassControlVehicleInfo_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PAIndicators_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PAIndicators_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PAIndicators_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Partners_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Partners_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PartnerUsers_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerUsers_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PartnerUsers_AddOrEdit]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_IntrackReference_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_IntrackReference_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_IntrackReference_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_MonthlyClosing_WarningEmails]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MonthlyClosing_WarningEmails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_MonthlyClosing_WarningEmails]
GO
/****** Object:  StoredProcedure [General].[Sp_OdometerGpsByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_OdometerGpsByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_OdometerGpsByVehicle_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PartnerUsersByCountry_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerUsersByCountry_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PartnerUsersByCountry_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CardRequest_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CardRequest_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CardRequest_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Cities_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Cities_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Cities_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_CountriesByUser_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CountriesByUser_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CountriesByUser_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_AlarmsByVehicles_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AlarmsByVehicles_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_AlarmsByVehicles_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_CAIndicators_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CAIndicators_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CAIndicators_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_AdminPartnerUsersByCountry_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AdminPartnerUsersByCountry_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_AdminPartnerUsersByCountry_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Alarm_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Alarm_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Alarm_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_Balance_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_Balance_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCard_Balance_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_CreditAvailable_Edit]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_CreditAvailable_Edit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCard_CreditAvailable_Edit]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_Delete]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCard_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_Counties_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Counties_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Counties_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CustomerCardRequest_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerCardRequest_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CustomerCardRequest_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardNumber_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardNumber_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCardNumber_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardNumbers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardNumbers_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCardNumbers_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardsReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardsReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCardsReport_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardStatusRequested_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardStatusRequested_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCardStatusRequested_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CurrenciesByCustomer_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrenciesByCustomer_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CurrenciesByCustomer_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Customers_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Customers_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_Customers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Customers_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomersByCountry_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByCountry_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomersByCountry_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomersByPartners_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByPartners_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomersByPartners_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomersByPartners_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByPartners_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomersByPartners_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomersByPartners_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByPartners_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomersByPartners_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Customers_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Customers_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_FleetCard_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_FleetCard_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_FleetCard_Alarm]
GO
/****** Object:  StoredProcedure [Control].[Sp_FuelsByCredit_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_FuelsByCredit_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_FuelsByCredit_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_FuelsByCredit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_FuelsByCredit_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_FuelsByCredit_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_FuelsCost_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_FuelsCost_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_FuelsCost_Retrieve]
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetRosterInsurance]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetRosterInsurance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Insurance].[Sp_GetRosterInsurance]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleUnitsByGeoFence_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleUnitsByGeoFence_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleUnitsByGeoFence_AddOrEdit]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleUnitsByGeoFence_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleUnitsByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleUnitsByGeoFence_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleRouteSegments_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleRouteSegments_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleRouteSegments_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_Points_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Points_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_Points_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCost_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCost_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCost_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCost_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCost_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCost_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceRecordByVehicle_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicle_AddOrEdit]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesDetail_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesDetail_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_RoutesDetail_AddOrEdit]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesPoints_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesPoints_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_RoutesPoints_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesSegments_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesSegments_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_RoutesSegments_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_LicenseExpiration_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_LicenseExpiration_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_LicenseExpiration_Alarm]
GO
/****** Object:  StoredProcedure [General].[Sp_PartnerUsers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerUsers_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PartnerUsers_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_PartnerFuel_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PartnerFuel_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_PartnerFuel_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_PartnerFuel_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PartnerFuel_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_PartnerFuel_Delete]
GO
/****** Object:  StoredProcedure [Control].[Sp_PartnerFuel_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PartnerFuel_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_PartnerFuel_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleRoutePoints_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleRoutePoints_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleRoutePoints_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleGroupsByGeoFence_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleGroupsByGeoFence_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleGroupsByGeoFence_AddOrEdit]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleGroupsByGeoFence_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleGroupsByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleGroupsByGeoFence_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_SubUnitReference_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_SubUnitReference_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_SubUnitReference_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Users_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Users_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_TransactionLitersByCredit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionLitersByCredit_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_TransactionLitersByCredit_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalogCost_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalogCost_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCatalogCost_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCostCenters_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCostCenters_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleCostCenters_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCostCenters_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCostCenters_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleCostCenters_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCostCenters_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCostCenters_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleCostCenters_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_SendAlarm]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_SendAlarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_SendAlarm]
GO
/****** Object:  StoredProcedure [General].[Sp_States_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_States_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_States_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleGroups_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleGroups_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleGroups_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleGroups_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleGroups_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleGroups_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleGroups_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleGroups_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleGroups_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleUnits_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleUnits_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleUnits_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleUnits_Delete]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleUnits_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleUnits_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleUnits_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleUnits_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_VehicleUnits_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Partners_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Partners_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_Partners_Authentication]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_Authentication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Partners_Authentication]
GO
/****** Object:  StoredProcedure [General].[Sp_Partners_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Partners_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_Operators_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Operators_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Operators_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_Operators_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Operators_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Operators_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_Operators_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Operators_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Operators_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PasswordsHistory_Add]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PasswordsHistory_Add]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PasswordsHistory_Add]
GO
/****** Object:  StoredProcedure [General].[Sp_PasswordsHistory_Check]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PasswordsHistory_Check]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PasswordsHistory_Check]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_GroupReference_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GroupReference_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_GroupReference_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_Routes_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Routes_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_Routes_AddOrEdit]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_Routes_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Routes_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_Routes_Delete]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_Routes_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Routes_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_Routes_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalog_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalog_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalog_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalog_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalog_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalog_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_Retrieve]
GO
/****** Object:  StoredProcedure [Operation].[Sp_WeightedScoreSettings_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_WeightedScoreSettings_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_WeightedScoreSettings_AddOrEdit]
GO
/****** Object:  StoredProcedure [Operation].[Sp_WeightedScoreSettings_Delete]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_WeightedScoreSettings_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_WeightedScoreSettings_Delete]
GO
/****** Object:  StoredProcedure [Operation].[Sp_WeightedScoreSettings_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_WeightedScoreSettings_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_WeightedScoreSettings_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFences_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFences_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_GeoFences_AddOrEdit]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFences_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFences_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_GeoFences_Delete]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFences_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFences_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_GeoFences_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_Fuels_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Fuels_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_Fuels_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_Fuels_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Fuels_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_Fuels_Delete]
GO
/****** Object:  StoredProcedure [Control].[Sp_Fuels_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Fuels_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_Fuels_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_Customers_CardRequest]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Customers_CardRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_Customers_CardRequest]
GO
/****** Object:  StoredProcedure [Control].[Sp_ApplicantsReport_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ApplicantsReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_ApplicantsReport_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CustomerCredits_AddFromAPI]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerCredits_AddFromAPI]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CustomerCredits_AddFromAPI]
GO
/****** Object:  StoredProcedure [Control].[Sp_CustomerCredits_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerCredits_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CustomerCredits_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerUsers_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerUsers_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomerUsers_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerUsers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerUsers_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomerUsers_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_DashboardModuleByCustomer_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DashboardModuleByCustomer_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_DashboardModuleByCustomer_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_DashboardModuleByCustomer_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_DashboardModuleByCustomer_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_DashboardModuleByCustomer_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_DashboardModuleByCustomer_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DashboardModuleByCustomer_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_DashboardModuleByCustomer_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_DriversUsers_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversUsers_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_DriversUsers_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_DriversUsers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversUsers_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_DriversUsers_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerCreditCards_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerCreditCards_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomerCreditCards_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_AddExpirationDriverLicense_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AddExpirationDriverLicense_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_AddExpirationDriverLicense_Alarm]
GO
/****** Object:  StoredProcedure [General].[Sp_Alarm_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Alarm_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Alarm_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_CardRequest_AddOrEdit]    Script Date: 02/24/2015 14:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CardRequest_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CardRequest_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_Alarms_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Alarms_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Alarms_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_AllCustomers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AllCustomers_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_AllCustomers_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerCreditCards_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerCreditCards_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomerCreditCards_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerCreditCards_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerCreditCards_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomerCreditCards_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Countries_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Countries_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Countries_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_Countries_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Countries_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Countries_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_Countries_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Countries_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Countries_Retrieve]
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_CountryCode_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_CountryCode_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_CountryCode_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_MasterCardsReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MasterCardsReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_MasterCardsReport_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Users_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Users_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_Users_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Users_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_Vehicles_Replication]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_Replication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Vehicles_Replication]
GO
/****** Object:  StoredProcedure [General].[Sp_Status_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Status_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Status_Retrieve]
GO
/****** Object:  StoredProcedure [Insurance].[Sp_ScoreDrivingFormula_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_ScoreDrivingFormula_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Insurance].[Sp_ScoreDrivingFormula_AddOrEdit]
GO
/****** Object:  StoredProcedure [Operation].[Sp_ScoreTypes_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreTypes_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_ScoreTypes_AddOrEdit]
GO
/****** Object:  StoredProcedure [Operation].[Sp_ScoreTypes_Delete]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreTypes_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_ScoreTypes_Delete]
GO
/****** Object:  StoredProcedure [Operation].[Sp_ScoreTypes_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreTypes_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Operation].[Sp_ScoreTypes_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Types_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Types_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Types_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_NewsByPartners_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartners_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_NewsByPartners_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_NewsByPartners_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartners_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_NewsByPartners_Delete]
GO
/****** Object:  StoredProcedure [General].[Sp_NewsByPartners_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartners_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_NewsByPartners_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_NewsByPartnersActive_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartnersActive_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_NewsByPartnersActive_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_NewsByPartnersAll_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartnersAll_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_NewsByPartnersAll_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Parameters_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Parameters_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Parameters_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_Parameters_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Parameters_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Parameters_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardTransaction_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardTransaction_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCardTransaction_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardTransaction_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardTransaction_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_CreditCardTransaction_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Email_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Email_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Email_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_Email_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Email_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Email_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_EmailEncryptedAlarms_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailEncryptedAlarms_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_EmailEncryptedAlarms_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_EmailEncryptedAlarms_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailEncryptedAlarms_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_EmailEncryptedAlarms_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_EmailFleetCard_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailFleetCard_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_EmailFleetCard_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_EmailFleetCard_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailFleetCard_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_EmailFleetCard_Retrieve]
GO
/****** Object:  StoredProcedure [Control].[Sp_Currencies_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Currencies_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_Currencies_AddOrEdit]
GO
/****** Object:  StoredProcedure [Control].[Sp_Currencies_Delete]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Currencies_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_Currencies_Delete]
GO
/****** Object:  StoredProcedure [Control].[Sp_Currencies_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Currencies_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_Currencies_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerThresholds_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerThresholds_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomerThresholds_AddOrEdit]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerThresholds_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerThresholds_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_CustomerThresholds_Retrieve]
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetScoreDrivingFormulas]    Script Date: 02/24/2015 14:29:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetScoreDrivingFormulas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Insurance].[Sp_GetScoreDrivingFormulas]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_CreateRole]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_CreateRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_CreateRole]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Reports_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_Reports_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_Reports_Retrieve]
GO
/****** Object:  StoredProcedure [dbo].[Sp_VehicleRoutePointsInfo_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_VehicleRoutePointsInfo_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_VehicleRoutePointsInfo_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_FrequencyTypes_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_FrequencyTypes_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_FrequencyTypes_Retrieve]
GO
/****** Object:  StoredProcedure [General].[Sp_Customers_AddFromAPI]    Script Date: 02/24/2015 14:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_AddFromAPI]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Customers_AddFromAPI]
GO
/****** Object:  StoredProcedure [General].[Sp_Customers_AddFromAPI]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_AddFromAPI]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/19/2014
-- Description:	Insert Customer information from API
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Customers_AddFromAPI]
(
	 @pName VARCHAR(250)
	,@pCurrencyCode VARCHAR(10)
	,@pAccountNumber VARCHAR(50)
	,@pMasterCreditCard VARCHAR(50)
	,@pLoggedUserId INT
)
AS
BEGIN
	PRINT ''not implemented exception''
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_FrequencyTypes_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_FrequencyTypes_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 06/Oct/2014
-- Description:	Frequency Type information
-- =============================================
CREATE PROCEDURE [General].[Sp_FrequencyTypes_Retrieve]
AS
BEGIN	
	SET NOCOUNT ON;
	SELECT 
		a.[FrequencyTypeId],
		a.[Name]
		FROM
		General.[FrequencyTypes] a
	SET NOCOUNT OFF;
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_VehicleRoutePointsInfo_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_VehicleRoutePointsInfo_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/30/2014
-- Description:	Retrieve PassControl information
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_VehicleRoutePointsInfo_Retrieve]
(
	@pUnitID BIGINT,
	@pStartDate VARCHAR(21),
	@pEndDate VARCHAR(21),
	@pPoints VARCHAR(MAX),
	@pDistanceMin INT,
	@pTimeMin INT
)
AS
BEGIN

DECLARE @separator VARCHAR(MAX) = '',''

DECLARE @Splited TABLE
(
	item VARCHAR(MAX)
)

DECLARE @PointsList TABLE
(
	Id BIGINT PRIMARY KEY IDENTITY(1,1),
	PointId BIGINT,
	PointLongitude FLOAT,
	PointLatitude FLOAT
)

DECLARE @ReportsList TABLE
(
	Id BIGINT PRIMARY KEY IDENTITY(1,1),
    Report DECIMAL(20, 0),
    GPSDATETIME DATETIME,
    Longitude FLOAT,
    Latitude FLOAT,
    VSSSpeed INT
)

DECLARE @StopReportsList TABLE
(
	Id BIGINT,
    Report DECIMAL(20, 0),
    GPSDATETIME DATETIME,
    Longitude FLOAT,
    Latitude FLOAT,
    VSSSpeed INT
)

DECLARE @TimeReportsList TABLE
(
	Id BIGINT,
    GPSDATETIME DATETIME,
    Longitude FLOAT,
    Latitude FLOAT,
    First BIT
)

DECLARE @ResultTemp TABLE
(
	Id1 BIGINT NULL,
	GPSDATETIME1 DATETIME NULL, 
	Longitude1 FLOAT NULL,
	Latitude1 FLOAT NULL,
	First1 BIT NULL,
	Id2 BIGINT NULL,
	GPSDATETIME2 DATETIME NULL,
	Longitude2 FLOAT NULL,
	Latitude2 FLOAT NULL,
	First2 BIT NULL,
	seconds int NULL,
	PointId int NULL,
	Distance int NULL,
	TotalDistance float NULL
)

DECLARE @Result TABLE
(
	Id1 BIGINT NULL,
	GPSDATETIME1 DATETIME NULL, 
	Longitude1 FLOAT NULL,
	Latitude1 FLOAT NULL,
	First1 BIT NULL,
	Id2 BIGINT NULL,
	GPSDATETIME2 DATETIME NULL,
	Longitude2 FLOAT NULL,
	Latitude2 FLOAT NULL,
	First2 BIT NULL,
	seconds INT NULL,
	PointId INT NULL,
	Distance INT NULL
)
 
 SET @pPoints = REPLACE(@pPoints,@separator,''''''),('''''')
 SET @pPoints = ''SELECT * from (values(''''''+@pPoints+'''''')) as V(a)'' 
 INSERT INTO @Splited
 EXEC (@pPoints)
 INSERT INTO @PointsList 
	([PointId], 
	[PointLongitude], 
	[PointLatitude])
 SELECT	
	SUBSTRING([item], 1,CHARINDEX(''*'',[item])-1) [PointId],
	SUBSTRING([item], CHARINDEX(''*'',[item])+1,LEN([item])-CHARINDEX('' '',[item])+1) [PointLongitude],
	SUBSTRING([item], CHARINDEX('' '',[item])+1,LEN([item])-CHARINDEX('' '',[item])+1) [PointLatitude] 
FROM @Splited

INSERT INTO @ReportsList 
	([Report], 
	[GPSDATETIME], 
	[Longitude], 
	[Latitude], 
	[VSSSpeed])
SELECT 
	r.[Report],
	r.[GPSDATETIME],
	r.[Longitude],
	r.[Latitude],
	r.[VSSSpeed]
FROM 
	[dbo].[Reports] r 
	INNER JOIN [dbo].[Devices] d ON r.[Device] = d.[Device]
WHERE 
	d.[UnitID] = @pUnitID AND 
	r.[GPSDATETIME] >= @pStartDate AND 
	r.[GPSDATETIME] < @pEndDate 
ORDER BY 
	r.[GPSDATETIME] ASC
	
INSERT INTO @StopReportsList 
	([Id], 
	[Report], 
	[GPSDATETIME], 
	[Longitude], 
	[Latitude], 
	[VSSSpeed])
SELECT 
	[Id], 
	[Report], 
	[GPSDATETIME], 
	[Longitude], 
	[Latitude], 
	[VSSSpeed] 
FROM @ReportsList 
WHERE 
	[VSSSpeed] = 0

INSERT INTO @TimeReportsList 
	([Id], 
	[GPSDATETIME], 
	[Longitude], 
	[Latitude], 
	[First])
SELECT 
	MIN([Id]) [Id], 
	MIN([GPSDATETIME]) [GPSDATETIME], 
	[Longitude] [Longitude], 
	[Latitude] [Latitude], 
	1 [First]
FROM @StopReportsList 
GROUP BY 
	[Longitude], 
	[Latitude]
UNION 
SELECT 
	MAX([Id]) [Id], 
	MAX([GPSDATETIME]) [GPSDATETIME], 
	[Longitude] [Longitude], 
	[Latitude] [Latitude], 
	0 [First]
FROM @StopReportsList 
GROUP BY 
	[Longitude], 
	[Latitude]

INSERT INTO @ResultTemp 
	(Id1, 
	GPSDATETIME1, 
	Longitude1, 
	Latitude1, 
	First1, 
	Id2, 
	GPSDATETIME2, 
	Longitude2, 
	Latitude2, 
	First2, 
	seconds)
SELECT 
	a.[Id] [Id1], 
	a.[GPSDATETIME] [GPSDATETIME1], 
	a.[Longitude] [Longitude1], 
	a.[Latitude] [Latitude1], 
	a.[First] [First1], 
	b.[Id] [Id2], 
	b.[GPSDATETIME] [GPSDATETIME2], 
	b.[Longitude] [Longitude2], 
	b.[Latitude] [Latitude2], 
	b.[First] [First2], 
	DATEDIFF([second], a.[GPSDATETIME], b.[GPSDATETIME])
FROM 
	@TimeReportsList a 
	INNER JOIN @TimeReportsList b ON a.[Longitude] = b.[Longitude] AND a.[Latitude] = b.[Latitude] 
WHERE 
	a.First = 1 AND 
	b.First = 0
	and DATEDIFF([second], a.[GPSDATETIME], b.[GPSDATETIME]) > @pTimeMin
ORDER BY 
	[Id1] 

DECLARE @Iter int = 1
DECLARE @MaxRowNum int = (SELECT max([Id]) from @PointsList)

WHILE @Iter <= @MaxRowNum
	BEGIN
		DECLARE @PointId INT = (SELECT [PointId] FROM @PointsList WHERE [Id] = @Iter)
		DECLARE @PointLongitude FLOAT = (SELECT [PointLongitude] FROM @PointsList WHERE [Id] = @Iter)
		DECLARE @PointLatitude FLOAT = (SELECT [PointLatitude] FROM @PointsList WHERE [Id] = @Iter)
	INSERT INTO @Result 
		([Id1], 
		[GPSDATETIME1], 
		[Longitude1], 
		[Latitude1], 
		[First1], 
		[Id2], 
		[GPSDATETIME2], 
		[Longitude2], 
		[Latitude2], 
		[First2], 
		[seconds], 
		[PointId], 
		[Distance])
	SELECT TOP (1) 
		[Id1] [Id1], 
		[GPSDATETIME1] [GPSDATETIME1], 
		[Longitude1] [Longitude1], 
		[Latitude1] [Latitude1], 
		[First1] [First1], 
		[Id2] [Id2], 
		[GPSDATETIME2] [GPSDATETIME2], 
		[Longitude2] [Longitude2], 
		[Latitude2] [Latitude2], 
		[First2] [First2], 
		[seconds] [seconds], 
		@PointId [PointId],
		GEOGRAPHY::STPointFromText(''POINT('' + CONVERT(VARCHAR(MAX),[Latitude1])  + '' '' + CONVERT(VARCHAR(MAX),[Longitude1]) + '')'',4326).STDistance(GEOGRAPHY::STPointFromText(''POINT('' + CONVERT(VARCHAR(MAX),@PointLatitude) + '' '' + CONVERT(VARCHAR(MAX),@PointLongitude) + '')'',4326)) [Distance]
	FROM @ResultTemp 
		WHERE
			GEOGRAPHY::STPointFromText(''POINT('' + CONVERT(VARCHAR(MAX),[Latitude1])  + '' '' + CONVERT(VARCHAR(MAX),[Longitude1]) + '')'',4326).STDistance(GEOGRAPHY::STPointFromText(''POINT('' + CONVERT(VARCHAR(MAX),@PointLatitude) + '' '' + CONVERT(VARCHAR(MAX),@PointLongitude) + '')'',4326))  <= @pDistanceMin
		order BY 
			GEOGRAPHY::STPointFromText(''POINT('' + CONVERT(VARCHAR(MAX),[Latitude1])  + '' '' + CONVERT(VARCHAR(MAX),[Longitude1]) + '')'',4326).STDistance(GEOGRAPHY::STPointFromText(''POINT('' + CONVERT(VARCHAR(max),@PointLatitude) + '' '' + CONVERT(VARCHAR(MAX),@PointLongitude) + '')'',4326)) ASC, 
			[GPSDATETIME1] DESC
		set @Iter = @Iter + 1
	END
	
SELECT 
	[Id1] [Id], 
	[GPSDATETIME1] [Date], 
	[Longitude1] [Longitude], 
	[Latitude1] [Latitude], 
	[Seconds] [Time], 
	[PointId] [PointId], 
	[Distance] [Distance]
FROM @Result 

    SET NOCOUNT OFF
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_Reports_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_Reports_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/30/2014
-- Description:	Retrieve Reports information
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_Reports_Retrieve]
(
	 @pUnitID bigint = NULL,
	 @pStartDate VARCHAR(21),
	 @pEndDate VARCHAR(21)
)

AS
BEGIN
	
	SET NOCOUNT ON
SELECT 
	r.[Report], 
	r.[Device], 
	r.[GPSDateTime], 
	r.[RTCDateTime], 
	r.[RepDateTime], 
	r.[SvrDateTime], 
	r.[Longitude], 
	r.[Latitude], 
	r.[Heading], 
	r.[ReportID], 
	r.[Odometer], 
	r.[HDOP], 
	r.[InputStatus], 
	r.[OutputStatus], 
	r.[VSSSpeed], 
	r.[AnalogInput], 
	r.[DriverID], 
	r.[Temperature1], 
	r.[Temperature2], 
	r.[MCC], 
	r.[MNC], 
	r.[LAC], 
	r.[CellID], 
	r.[RXLevel], 
	r.[GSMQuality], 
	r.[GSMStatus], 
	r.[Satellites], 
	r.[MainVolt], 
	r.[BatVolt]
FROM 
	[dbo].[Reports] r 
	INNER JOIN [dbo].[Devices] d ON r.[Device] = d.[Device]
WHERE 
	d.[UnitID] = @pUnitID and 
	r.[GPSDateTime] >= @pStartDate AND 
	r.[GPSDateTime] >= @pEndDate 
ORDER BY 
	r.[GPSDateTime] ASC

    SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_CreateRole]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_CreateRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Roles_CreateRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

  --  EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId))
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    INSERT INTO dbo.aspnet_Roles
                (ApplicationId, RoleName, LoweredRoleName)
         VALUES (@ApplicationId, @RoleName, LOWER(@RoleName))

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetScoreDrivingFormulas]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetScoreDrivingFormulas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 24/02/2015
-- Description:	Get Score Driving Formulas of the customer
-- =============================================
CREATE PROCEDURE [Insurance].[Sp_GetScoreDrivingFormulas]
	  @pScoreDrivingFormulaId INT=null
	 ,@pCustomerId INT =null
	 ,@pKey varchar(1000)=null
AS
BEGIN	
	SET NOCOUNT ON;
	SELECT 
	   [ScoreDrivingFormulaId]
	  ,[Name]
      ,[ScoreDrivingFormulaStr]
      ,[ScoreDrivingFormulaSP]
      ,[IsActive]
      ,[CustomerId]
      ,[RowVersion]
		FROM Insurance.ScoreDrivingFormula
		WHERE  (@pCustomerId IS NULL OR [CustomerId] = @pCustomerId)
				AND (@pScoreDrivingFormulaId IS NULL OR 
				@pScoreDrivingFormulaId = ScoreDrivingFormulaId)
				AND (@pKey IS NULL 
				OR [Name] LIKE ''%''+@pKey+''%'' )
      ORDER BY [Name] DESC	
	SET NOCOUNT OFF;
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerThresholds_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerThresholds_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Alexander Aguero Venegas 
-- Create date: 14 enero 2015
-- Description:	Retrieve Thresholds by customer information
-- ================================================================================================
Create PROCEDURE [General].[Sp_CustomerThresholds_Retrieve]
(
	@pCustomerId INT
)
AS 
BEGIN 
SET NOCOUNT ON 

IF (SELECT COUNT(*) FROM  General.CustomerThresholds a 	WHERE a.[CustomerId] = @pCustomerId) > 0
	BEGIN
		SELECT 
			 a.[RedLower]
			,a.[RedHigher]
			,a.[YellowLower]
			,a.[YellowHigher]
			,a.[GreenLower]
			,a.[GreenHigher]
			,a.[RowVersion]
		FROM  General.CustomerThresholds a
		WHERE a.[CustomerId] = @pCustomerId
	END
ELSE
	SELECT   0 as [RedLower], 69 as [RedHigher], 70 AS [YellowLower], 84 as [YellowHigher], 85 as [GreenLower]	,100 as [GreenHigher], 0x00000000000B3FB7 as [RowVersion]

SET NOCOUNT OFF
END ' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerThresholds_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerThresholds_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 29/01/2015
-- Description:	Insert or Update Thresholds information
-- ================================================================================================
Create PROCEDURE [General].[Sp_CustomerThresholds_AddOrEdit]
(
	 @pCodingThresholdsId INT=Null
	,@pCustomerId INT 
	,@pRedLower INT 
	,@pRedHigher INT 
	,@pYellowLower INT 
	,@pYellowHigher INT 
	,@pGreenLower INT 
	,@pGreenHigher INT 
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
	,@pCreditPercentageMin INT=NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
		
			IF (@pCodingThresholdsId IS NULL)
			BEGIN
				INSERT INTO [General].[CustomerThresholds]
						([CustomerId]
						,[RedLower]
						,[RedHigher]
						,[YellowLower]
						,[YellowHigher]
						,[GreenLower]
						,[GreenHigher]
						,[InsertDate]
						,[InsertUserId]
						,CreditPercentageMin)
				VALUES	(@pCustomerId
						,@pRedLower
						,@pRedHigher
						,@pYellowLower
						,@pYellowHigher
						,@pGreenLower
						,@pGreenHigher
						,GETUTCDATE()
						,@pLoggedUserId
						,@pCreditPercentageMin)
			END
			ELSE
			BEGIN
				UPDATE [General].[CustomerThresholds]
					SET  [RedLower] = @pRedLower
						,[RedHigher] = @pRedHigher
						,[YellowLower] = @pYellowLower
						,[YellowHigher] = @pYellowHigher
						,[GreenLower] = @pGreenLower
						,[GreenHigher] = @pGreenHigher
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
						,CreditPercentageMin=@pCreditPercentageMin
				WHERE CodingThresholdsId = @pCodingThresholdsId
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_Currencies_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Currencies_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve country information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Currencies_Retrieve]
(
	 @pCurrencyId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT
		 a.[CurrencyId]
		,a.[Name]
		,a.[Code]
		,a.[Symbol]
		,a.[RowVersion]
    FROM [Control].[Currencies] a
	WHERE (@pCurrencyId IS NULL OR [CurrencyId] = @pCurrencyId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
				OR a.[Code] like ''%''+@pKey+''%''
				OR a.[Symbol] like ''%''+@pKey+''%'')
	ORDER BY [CurrencyId] DESC
	
    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_Currencies_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Currencies_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Delete Currency information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Currencies_Delete]
(
	 @pCurrencyId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Control].[Currencies]
			WHERE [CurrencyId] = @pCurrencyId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_Currencies_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Currencies_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update Currency information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Currencies_AddOrEdit]
(
	 @pCurrencyId INT = NULL
	,@pName VARCHAR(50)
	,@pCode VARCHAR(10)
	,@pSymbol NVARCHAR(4)
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCurrencyId IS NULL)
			BEGIN
				INSERT INTO [Control].[Currencies]
						([Name]
						,[Code]
						,[Symbol]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pName
						,@pCode
						,@pSymbol
						,GETUTCDATE()
						,@pLoggedUserId)				
			END
			ELSE
			BEGIN
				UPDATE [Control].[Currencies]
					SET  [Name] = @pName
						,[Code] = @pCode
						,[Symbol] = @pSymbol
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CurrencyId] = @pCurrencyId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_EmailFleetCard_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailFleetCard_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 28/01/2015
-- Description:	Retrieve Email FleetCard information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_EmailFleetCard_Retrieve]
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		a.[EmailFleetCardId],
		a.[To],
		a.[From],
		a.[CC],
		a.[Subject],
		a.[Message],
		a.[CreditCardNumber],
		a.[IsHtml],
		a.[RowVersion]
	FROM [General].[EmailFleetCardAlarms] a
	WHERE a.[Sent] = 0
	SET NOCOUNT OFF 
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_EmailFleetCard_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailFleetCard_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Cristian Martínez H.
-- Create date: 30/Nov/2014
-- Description:	Add Or Edit Email information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_EmailFleetCard_AddOrEdit]
(
	@pEmailFleetCardId INT = NULL,
	@pTo VARCHAR(500),
	@pFrom VARCHAR(100) = NULL,
	@pCC VARCHAR(100) = NULL,
	@pSubject VARCHAR(300) = NULL, 
	@pMessage NVARCHAR(1000) = NULL, 
	@pCreditCardNumber VARCHAR(500),
	@pIsHtml BIT, 
	@pSent BIT, 
	@pLoggedUserId INT,
	@pRowVersion TIMESTAMP = NULL
)
AS
BEGIN 
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		BEGIN 
			UPDATE [General].[EmailFleetCardAlarms]
			SET [To]  = @pTo,
				[From] = @pFrom,
				[CC] = @pCC,
				[Subject] = @pSubject,
				[Message] = @pMessage,
				[CreditCardNumber] = @pCreditCardNumber,
				[IsHtml] = @pIsHtml,
				[Sent] = @pSent,
				[ModifyUserId] = @pLoggedUserId, 
				[ModifyDate] = GETUTCDATE()
			WHERE [EmailFleetCardId] = @pEmailFleetCardId
			  AND [RowVersion] = @pRowVersion

		END
		
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_EmailEncryptedAlarms_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailEncryptedAlarms_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 28/01/2015
-- Description:	Retrieve Email FleetCard information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_EmailEncryptedAlarms_Retrieve]
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		a.[EmailId],
		a.[To],
		a.[From],
		a.[CC],
		a.[Subject],
		a.[Message],
		a.[IsHtml],
		a.[RowVersion]
	FROM [General].[EmailEncryptedAlarms] a
	WHERE a.[Sent] = 0
	SET NOCOUNT OFF 
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_EmailEncryptedAlarms_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailEncryptedAlarms_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Cristian Martínez H.
-- Create date: 30/Nov/2014
-- Description:	Add Or Edit Email information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_EmailEncryptedAlarms_AddOrEdit]
(
	@pEmailId INT = NULL,
	@pTo VARCHAR(500),
	@pFrom VARCHAR(100) = NULL,
	@pCC VARCHAR(100) = NULL,
	@pSubject VARCHAR(300) = NULL, 
	@pMessage NVARCHAR(1000) = NULL, 
	@pIsHtml BIT, 
	@pSent BIT, 
	@pLoggedUserId INT,
	@pRowVersion TIMESTAMP = NULL
)
AS
BEGIN 
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		BEGIN 
			UPDATE [General].[EmailEncryptedAlarms]
			SET [To]  = @pTo,
				[From] = @pFrom,
				[CC] = @pCC,
				[Subject] = @pSubject,
				[Message] = @pMessage,
				[IsHtml] = @pIsHtml,
				[Sent] = @pSent,
				[ModifyUserId] = @pLoggedUserId, 
				[ModifyDate] = GETUTCDATE()
			WHERE [EmailId] = @pEmailId
			  AND [RowVersion] = @pRowVersion

		END
		
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Email_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Email_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martínez H.
-- Create date: 18/Nov/2014
-- Description:	Retrieve Email information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Email_Retrieve]
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		 a.[EmailId]
		,a.[To]
		,a.[From]
		,a.[CC]
		,a.[Subject]
		,a.[Message]
		,a.[IsHtml]
		,a.[RowVersion]
	FROM [General].[Emails] a
	WHERE a.[Sent] = 0
	SET NOCOUNT OFF 
END' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Email_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Email_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martínez H.
-- Create date: 30/Nov/2014
-- Description:	Add Or Edit Email information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Email_AddOrEdit]
(
	@pEmailId INT = NULL,
	@pTo VARCHAR(500),
	@pFrom VARCHAR(100) = NULL,
	@pCC VARCHAR(100) = NULL,
	@pSubject VARCHAR(300) = NULL, 
	@pMessage NVARCHAR(1000) = NULL, 
	@pIsHtml BIT, 
	@pSent BIT, 
	@pLoggedUserId INT,
	@pRowVersion TIMESTAMP = NULL
)
AS
BEGIN 
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		IF (@pEmailId IS NULL)
		BEGIN 
			INSERT INTO [General].[Emails]
					   ([To],
					    [From],
					    [CC],
					    [Subject],
					    [Message],
					    [IsHtml],
					    [Sent],
					    [InsertUserId],
					    [InsertDate])
				VALUES (
						@pTo,
						@pFrom,
						@pCC,
						@pSubject,
						@pMessage,
						@pIsHtml,
						@pSent,
						@pLoggedUserId,
						GETUTCDATE())
		END
		ELSE
		BEGIN 
			UPDATE [General].[Emails]
			SET [From] = @pFrom,
				[CC] = @pCC, 
				[Subject] = @pSubject,
				[Message] = @pMessage,
				[IsHtml] = @pIsHtml,
				[Sent] = @pSent,
				[ModifyUserId] = @pLoggedUserId, 
				[ModifyDate] = GETUTCDATE()
			WHERE [EmailId] = @pEmailId
			  AND [RowVersion] = @pRowVersion				
		END
		
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardTransaction_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardTransaction_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 05/12/2014
-- Description:	Retrieve CreditCard Transactions through the VPOS Process
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardTransaction_Retrieve]
(
	 @pCreditCardId INT						--@pCreditCardId: Customer Id
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
			
	SELECT TOP 1
	         [CreditCardId]
			,[Customer]
			,[TransactionType]
			,[TerminalID]
			,[Invoice]
			,[EntryMode]
			,[AccountNumber]
			,[ExpirationDate]
			,[TotalAmount]
			,[Odometer]
			,[Liters]
			,[Plate]
			,[AuthorizationNumber]
			,[ReferenceNumber]
			,[SystemTraceNumber]
	FROM [Control].[CreditCardTransactionVPOS]
	WHERE [CreditCardId] = @pCreditCardId
	ORDER BY [InsertDate] DESC
				
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardTransaction_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardTransaction_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 05/12/2014
-- Description:	Insert or Update CreditCard Transactions through the VPOS Process
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardTransaction_AddOrEdit]
(
	 @pCreditCardId INT						--@pCreditCardId: Customer Id
	,@pCustomer varchar(50)					--@pCustomer: Credit Card Number Id
	,@pTransactionType varchar(50)			--@pTransactionType: Credit Card Number Id
	,@pTerminalID nvarchar(50)				--@pTerminalID: Expiration Year
	,@pInvoice nvarchar(50)	= NULL			--@pInvoice: Expiration Month
	,@pEntryMode nvarchar(50) = NULL		--@pEntryMode: Credit Limit
	,@pAccountNumber nvarchar(50) = NULL	--@pAccountNumber: Status Id
	,@pExpirationDate nvarchar(5) = NULL	--@pExpirationDate: Month
	,@pTotalAmount [numeric](16, 2) = NULL	--@pTotalAmount: Year
	,@pOdometer [numeric](16, 2) = NULL		--@pTotalAmount: Year
	,@pLiters [numeric](16, 2) = NULL		--@pTotalAmount: Year
	,@pPlate nvarchar(10) = NULL			--@pTotalAmount: Year
	,@pAuthorizationNumber nvarchar(50) = NULL		--@pAuthorizationNumber: User Id
	,@pReferenceNumber INT = NULL			--@pReferenceNumber: Vehicle Id
	,@pSystemTraceNumber INT = NULL			--@pSystemTraceNumber: Vehicle Id
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
			
	INSERT INTO [Control].[CreditCardTransactionVPOS]
			([CreditCardId]
			,[Customer]
			,[TransactionType]
			,[TerminalID]
			,[Invoice]
			,[EntryMode]
			,[AccountNumber]
			,[ExpirationDate]
			,[TotalAmount]
			,[Odometer]
			,[Liters]
			,[Plate]
			,[AuthorizationNumber]
			,[ReferenceNumber]
			,[SystemTraceNumber]
			,[InsertDate]
			,[InsertUserId])
	VALUES ( @pCreditCardId
			,@pCustomer
			,@pTransactionType
			,@pTerminalID
			,@pInvoice
			,@pEntryMode
			,@pAccountNumber
			,@pExpirationDate
			,@pTotalAmount
			,@pOdometer
			,@pLiters
			,@pPlate
			,@pAuthorizationNumber
			,@pReferenceNumber
			,@pSystemTraceNumber
			,GETUTCDATE()
			,@pLoggedUserId)
			
	SELECT SCOPE_IDENTITY()[CreditCardTransactionVPOS];
				
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Parameters_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Parameters_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Parameters information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Parameters_Retrieve]
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT
		 a.[SafeSpeedMargin]
		,a.[AVLOdometerErrorMargin]
		,a.[POSOdometerErrorMargin]
		,a.[PointArrivalTimeTolerance]
		,a.[GeoPointErrorMargin]
		,a.[PasswordEffectiveDays]
		,a.[PasswordsValidateRepetition]
		,a.[RowVersion]
    FROM [General].[Parameters] a	
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Parameters_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Parameters_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update Parameters information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Parameters_AddOrEdit]
(
	 @pSafeSpeedMargin INT = NULL
	,@pAVLOdometerErrorMargin INT = NULL
	,@pPOSOdometerErrorMargin INT = NULL
	,@pPointArrivalTimeTolerance INT = NULL
	,@pGeoPointErrorMargin INT = NULL
	,@pPasswordEffectiveDays INT = NULL
	,@pPasswordsValidateRepetition INT = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			DECLARE @lCount INT = 0
			SELECT @lCount = COUNT(1) FROM [General].[Parameters]
			
			IF (@lCount = 0)
			BEGIN
				INSERT INTO [General].[Parameters]
						([SafeSpeedMargin]
						,[AVLOdometerErrorMargin]
						,[POSOdometerErrorMargin]
						,[PointArrivalTimeTolerance]
						,[GeoPointErrorMargin]
						,[PasswordEffectiveDays]
						,[PasswordsValidateRepetition]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pSafeSpeedMargin
						,@pAVLOdometerErrorMargin
						,@pPOSOdometerErrorMargin
						,@pPointArrivalTimeTolerance
						,@pGeoPointErrorMargin
						,@pPasswordEffectiveDays
						,@pPasswordsValidateRepetition
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[Parameters]
					SET  [SafeSpeedMargin] = @pSafeSpeedMargin
						,[AVLOdometerErrorMargin] = @pAVLOdometerErrorMargin
						,[POSOdometerErrorMargin] = @pPOSOdometerErrorMargin
						,[PointArrivalTimeTolerance] = @pPointArrivalTimeTolerance
						,[GeoPointErrorMargin] = @pGeoPointErrorMargin
						,[PasswordEffectiveDays] = @pPasswordEffectiveDays
						,[PasswordsValidateRepetition] = @pPasswordsValidateRepetition
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_NewsByPartnersAll_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartnersAll_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 12/01/2014
-- Description:	Retrieve All Partner News
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_NewsByPartnersAll_Retrieve]
(
	 @pPartnerId INT = NULL,
	 @pActive INT = 1
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT
		a.[NewsId]
		,a.[PartnerId]
		,a.[Title]
		,a.[Content]
		,a.[Image]
		,a.[IsActive]
    FROM [General].[NewsByPartner] a		
	WHERE a.PartnerId = isnull(@pPartnerId,a.PartnerId) AND a.IsActive = @pActive
	ORDER BY [NewsId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_NewsByPartnersActive_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartnersActive_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 12/01/2014
-- Description:	Retrieve All Active Partner News
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_NewsByPartnersActive_Retrieve]
(
	 @pPartnerId INT = NULL,
	 @pActive INT = 1
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT
		a.[NewsId]
		,a.[PartnerId]
		,a.[Title]
		,a.[Content]
		,a.[Image]
		,a.[IsActive]
    FROM [General].[NewsByPartner] a		
	WHERE a.PartnerId = isnull(@pPartnerId,a.PartnerId) AND a.IsActive = @pActive
	ORDER BY [NewsId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_NewsByPartners_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartners_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 12/01/2015
-- Description:	Retrieve Partner News
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_NewsByPartners_Retrieve]
(
	 @pPartnerId INT=null,
	 @pNewsId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT
		a.[NewsId]
		,a.[PartnerId]
		,a.[Title]
		,a.[Content]
		,a.[Image]
		,a.[IsActive] 
		,a.[RowVersion]
    FROM [General].[NewsByPartner] a		
	WHERE (@pNewsId IS NULL OR [NewsId] = @pNewsId)
	  AND (@pKey IS NULL 
				OR a.[Title] like ''%''+@pKey+''%''
				OR a.[Content] like ''%''+@pKey+''%'')
	  AND a.PartnerId = isnull(@pPartnerId,a.PartnerId)
	ORDER BY [NewsId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_NewsByPartners_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartners_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 25/11/2014
-- Description:	Delete Partner News
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_NewsByPartners_Delete]
(
	 @pNewsId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [General].[NewsByPartner]
			WHERE [NewsId] = @pNewsId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_NewsByPartners_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartners_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo.
-- Create date: 25/11/2014
-- Description:	Insert or Update New by Partner
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_NewsByPartners_AddOrEdit]
(
	@pNewsId INT = NULL
	,@pPartnerId INT
	,@pTitle VARCHAR(250)
	,@pContent VARCHAR(MAX)
	,@pImage VARCHAR(MAX) = NULL
	,@pIsActive INT
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pNewsId IS NULL)
			BEGIN
				INSERT INTO [General].[NewsByPartner]
						([PartnerId]
						,[Title]
						,[Content]
						,[Image]
						,[IsActive]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pPartnerId
						,@pTitle
						,@pContent
						,@pImage
						,@pIsActive
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[NewsByPartner]
					SET  [Title] = @pTitle
						,[Content] = @pContent
						,[Image] = @pImage
						,[IsActive] = @pIsActive
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [PartnerId] = @pPartnerId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Types_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Types_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Types information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Types_Retrieve](
	@pUsage VARCHAR(50)
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT
		 a.[TypeId]
		,a.[Name]		
		,a.[Code]
    FROM [General].[Types] a
    WHERE a.[Usage] = @pUsage
      AND a.[IsActive] = 1
    ORDER BY a.[RowOrder]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_ScoreTypes_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreTypes_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/01/2014
-- Description:	Retrieve Sp_Score Types information
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_ScoreTypes_Retrieve]
(
	 @pScoreTypeId INT = NULL,				--@pScoreTypeId: PK of the table
	 @pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[ScoreTypeId]
		,a.[Code]
		,a.[Name]
		,a.[RowVersion]
    FROM [Operation].[ScoreTypes] a
	WHERE (@pScoreTypeId IS NULL OR a.[ScoreTypeId] = @pScoreTypeId)	  
	  AND (@pKey IS NULL 
				OR a.[Code] like ''%''+@pKey+''%''
				OR a.[Name] like ''%''+@pKey+''%'')
	ORDER BY a.[Code] ASC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_ScoreTypes_Delete]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreTypes_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/01/2014
-- Description:	Delete Score Types information
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_ScoreTypes_Delete]
(
	 @pScoreTypeId INT				--@pScoreTypeId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Operation].[ScoreTypes]
			WHERE [ScoreTypeId] = @pScoreTypeId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_ScoreTypes_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreTypes_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/01/2014
-- Description:	Insert or Update Score Type information
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_ScoreTypes_AddOrEdit]
(
	 @pScoreTypeId INT = NULL				--@pScoreTypeId: PK of the table
	,@pCode VARCHAR(5)						--@pCode: Code of the Score Type
	,@pName VARCHAR(100)					--@pName: Name of the Score Type
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pScoreTypeId IS NULL)
			BEGIN
				INSERT INTO [Operation].[ScoreTypes]
						([Code]
						,[Name]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(UPPER(@pCode)
						,@pName
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Operation].[ScoreTypes]
					SET  [Code] = UPPER(@pCode)
						,[Name] = @pName
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [ScoreTypeId] = @pScoreTypeId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Insurance].[Sp_ScoreDrivingFormula_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_ScoreDrivingFormula_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 24/02/2015
-- Description:	Insert or Update Score Driving Formula information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_ScoreDrivingFormula_AddOrEdit]
(
	   @pScoreDrivingFormulaId INT=null
	  ,@pName varchar(300)
      ,@pScoreDrivingFormulaStr varchar(2000)
      ,@pScoreDrivingFormulaSP varchar(1000)=null
      ,@pIsActive bit=null
      ,@pCustomerId int
	  ,@pRowVersion TIMESTAMP
	  ,@pLoggedUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF @pIsActive IS NOT NULL and @pScoreDrivingFormulaId is null
            BEGIN
				UPDATE [Insurance].ScoreDrivingFormula SET IsActive=0 WHERE CustomerId=@pCustomerId
            END
			
			IF (@pScoreDrivingFormulaId IS NULL)
			BEGIN
			
				INSERT INTO [Insurance].ScoreDrivingFormula
						(Name,
						 ScoreDrivingFormulaStr,
						 ScoreDrivingFormulaSP,
						 IsActive,
					     CustomerId
						,[InsertDate]
						,[InsertUserId]
						)
				VALUES	(@pName
						,@pScoreDrivingFormulaStr
						,@pScoreDrivingFormulaSP
						,@pIsActive
						,@pCustomerId
						,GETUTCDATE()
						,@pLoggedUserId
						)
			END
			ELSE
			BEGIN
				UPDATE [Insurance].ScoreDrivingFormula
					SET  Name=@pName,
						 ScoreDrivingFormulaStr=@pScoreDrivingFormulaStr,
						 ScoreDrivingFormulaSP=@pScoreDrivingFormulaSP,
						 IsActive=@pIsActive,
					     CustomerId=@pCustomerId
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId						
				WHERE [CustomerId] = @pCustomerId
				  
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	
	SELECT @pCustomerId
	
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Status_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Status_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Status information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Status_Retrieve](
	@pUsage VARCHAR(50)
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT
		 a.[StatusId]
		,a.[Name]		
		,a.[Code]
    FROM [General].[Status] a
    WHERE a.[Usage] = @pUsage
      AND a.[IsActive] = 1
    ORDER BY a.[RowOrder]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Vehicles_Replication]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_Replication]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ===============================================================
-- Author:		Napoleón Alvarado
-- Create date: 11/19/2014
-- Description:	Vehicles specific data replication on Atrack DB
-- ===============================================================
Create Procedure [General].[Sp_Vehicles_Replication]
	@pVehicleId as int,							--@pVehicleId: PK of the table
	@pCustomerId as int,						--@pCustomerId: FK of Customer
	@pVehicleIntrackId as int,					--@pVehicleIntrackId: Reference to Intrack Vehicles
	@pDeviceId as int,							--@pDeviceId: Reference to Atrack Devices
	@pAdministrativeSpeedLimit as int,			--@pAdministrativeSpeedLimit: Maximum speed limit for this car
	@pResult as int = 0 output,					--@pResult: 0 Success, -1 Error
	@pErrorDesc as nvarchar(4000) = '''' output	--@pErrorDesc: Error description
	
AS 
BEGIN
	BEGIN TRY
		Declare @lvehiculoIntrack int,
				@lCustomerId int
		
		Set @pResult = 0
		If @pVehicleId is not null
		begin
			--Verificar que la referencia a los vehículos de Intrack sea válida
			If @pVehicleIntrackId is not null
			begin
				Select @lvehiculoIntrack = vehiculo
				  from dbo.Vehiculos
				 where vehiculo = @pVehicleIntrackId
				
				If @@ROWCOUNT = 0
				begin
					--RAISERROR(''Invalid Intrack Reference'', 16, 1)
					Set @pResult = -1
					Set @pErrorDesc = ''Invalid Intrack Vehicle Reference''
					Return
				end
			end
		
			--Verificar si ya existe el registro
			Select @lCustomerId = CustomerId
			  from dbo.VehiclesECOSystem
			 where VehicleId = @pVehicleId
			 
			If @@ROWCOUNT > 0
			begin
				Update dbo.VehiclesECOSystem
				   set CustomerId = @pCustomerId,
					   VehicleIntrackId = @pVehicleIntrackId,
					   DeviceId = @pDeviceId,
					   AdministrativeSpeedLimit = @pAdministrativeSpeedLimit
				 where VehicleId = @pVehicleId
				 
				If @@ROWCOUNT = 0 
				begin
					--RAISERROR(''Atrack table VehiclesECOSystem was not updated'', 16, 1)
					Set @pResult = -1
					Set @pErrorDesc = ''Atrack table VehiclesECOSystem was not updated''
				end
			end
			else
			begin
				Insert into dbo.VehiclesECOSystem
					(VehicleId, CustomerId, VehicleIntrackId, DeviceId, AdministrativeSpeedLimit)
				values (@pVehicleId, @pCustomerId, @pVehicleIntrackId, @pDeviceId, @pAdministrativeSpeedLimit)

				If @@ROWCOUNT = 0 
				begin
					--RAISERROR(''Record not included in Atrack table VehiclesECOSystem'', 16, 1)
					Set @pResult = -1
					Set @pErrorDesc = ''Record not included in Atrack table VehiclesECOSystem''
				end
			end
		end
	END TRY
    BEGIN CATCH		
		Set @pResult = -1
		Select @pErrorDesc = ERROR_MESSAGE()		
	END CATCH	
	
END

' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Users_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Delete User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Users_Delete]
(
	 @pUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			UPDATE [General].[Users]
				SET [IsDeleted] = 1
			WHERE [UserId] = @pUserId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Users_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Insert or Update User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Users_AddOrEdit]
(
	 @pUserId INT = NULL	
	,@pName VARCHAR(250)
	,@pChangePassword BIT = NULL
	,@pPhoto VARCHAR(MAX) = NULL
	,@pIsActive BIT = 1
	,@pEmail VARCHAR(256)
	,@pPhoneNumber VARCHAR(256) = NULL
	,@pUserName VARCHAR(256)
	,@pIsLockedOut BIT = NULL
	,@pRoleName NVARCHAR(256)
	,@pPasswordExpirationDate DATETIME = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lAspNetUserId NVARCHAR(128)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
	
			SELECT @lAspNetUserId = a.[Id]
				FROM [dbo].[AspNetUsers] a
			WHERE [UserName] = @pUserName
			
			IF (@pUserId IS NULL)
			BEGIN
								
				INSERT INTO [General].[Users]
						([AspNetUserId]
						,[Name]
						,[ChangePassword]
						,[Photo]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@lAspNetUserId
						,@pName
						,@pChangePassword
						,@pPhoto
						,GETUTCDATE()
						,@pLoggedUserId)
						
				SET @pUserId = SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
				DECLARE @lIsLockedOut BIT, @lAccessFailedCount INT = NULL
				SELECT 
					@lIsLockedOut = CASE WHEN a.[IsLockedOut] = 1 OR b.[LockoutEndDateUtc] IS NOT NULL THEN 1 ELSE 0 END
				FROM [General].[Users] a INNER JOIN [dbo].[AspNetUsers] b ON a.[AspNetUserId] = b.[Id]
				WHERE a.[UserId] = @pUserId
				
				IF(@lIsLockedOut = 1 AND @pIsLockedOut = 0)
					SET @lAccessFailedCount = 0
				
				UPDATE [dbo].[AspNetUsers]
					SET  [Email] = @pEmail
						,[PhoneNumber] = @pPhoneNumber
						,[AccessFailedCount] = ISNULL(@lAccessFailedCount, [AccessFailedCount])
						,[LockoutEndDateUtc] = CASE WHEN @lAccessFailedCount = 0 THEN NULL ELSE [LockoutEndDateUtc] END
				WHERE [UserName] = @pUserName
				
				UPDATE [General].[Users]
					SET  [Name] = @pName
						,[ChangePassword] = ISNULL(@pChangePassword,[ChangePassword])
						,[Photo] = @pPhoto
						,[PasswordExpirationDate] = ISNULL(@pPasswordExpirationDate,[PasswordExpirationDate])
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [UserId] = @pUserId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
			DELETE [dbo].[AspNetUserRoles] WHERE [UserId] = @lAspNetUserId
			INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId])
			SELECT 
				@lAspNetUserId, a.[Id]
			FROM [dbo].[AspNetRoles] a
			WHERE a.[Name] = @pRoleName
            
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
			SELECT @pUserId
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_MasterCardsReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MasterCardsReport_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/21/2015
-- Description:	Retrieve Master Cards Report information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_MasterCardsReport_Retrieve]
(
	@pCustomerId INT = NULL,		--@pCustomerId: CustomerId
	@pCountryId INT = NULL,			--@pCountryId: CountryId
	@pYear INT = NULL,				--@pYear: Year
	@pMonth INT = NULL,				--@pMonth: Month
	@pStartDate DATETIME = NULL,	--@pStartDate: Start Date
	@pEndDate DATETIME = NULL		--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @pIssueForId INT

		SELECT 
			a.[CreditCardNumber],
			a.[CreditCardHolder],
			CONVERT(VARCHAR(2),a.[ExpirationMonth]) + '' / '' + CONVERT(VARCHAR(4),a.ExpirationYear) [Expiration],
			a.[CreditLimit],
			c.[Name] [CurrencyName],
			d.[Name] [StatusName],
			c.[Symbol] [CurrencySymbol]
		FROM [General].[CustomerCreditCards] a
			INNER JOIN [General].[Customers] b ON a.[CustomerId] = b.[CustomerId]
			INNER JOIN [Control].[Currencies] c ON b.[CurrencyId] = c.[CurrencyId]
			INNER JOIN [General].[Status] d ON a.[StatusId] = d.[StatusId]
		WHERE 
			a.[CustomerId] = @pCustomerId AND 
			b.[CountryId] = @pCountryId AND
				((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
				AND DATEPART(m,a.[InsertDate]) = @pMonth
				AND DATEPART(yyyy,a.[InsertDate]) = @pYear) OR
				(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
				AND a.[InsertDate] BETWEEN @pStartDate AND @pEndDate))
		ORDER BY CreditCardHolder
		
    SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_CountryCode_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_CountryCode_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/28/2014
-- Description:	Retrieve GeoFences information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_CountryCode_Retrieve]
(
	@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		co.[Code] countryCode
	FROM 
		[General].[Customers] c
		INNER JOIN [General].[Countries] co ON c.[CountryId] = co.[CountryId]
	WHERE 
		c.[CustomerId] = @pCustomerId
	
    SET NOCOUNT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Countries_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Countries_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/09/2014
-- Description:	Retrieve country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Countries_Retrieve]
(
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[CountryId]
		,a.[CurrencyId]
		,a.[Name]
		,a.[Code]
		,b.[Name] AS [CurrencyName]
		,a.[RowVersion]
    FROM [General].[Countries] a
		INNER JOIN [Control].[Currencies] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE (@pCountryId IS NULL OR [CountryId] = @pCountryId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
				OR a.[Code] like ''%''+@pKey+''%''
				OR b.[Name] like ''%''+@pKey+''%'')
	ORDER BY [CountryId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Countries_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Countries_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/09/2014
-- Description:	Delete country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Countries_Delete]
(
	 @pCountryId INT						--@pCountryId: Country Id PK table 
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [General].[Countries]
			WHERE [CountryId] = @pCountryId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Countries_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Countries_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/09/2014
-- Description:	Insert or Update country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Countries_AddOrEdit]
(
	 @pCountryId INT = NULL					--
	,@pName VARCHAR(250)
	,@pCurrencyId INT
	,@pCode CHAR(3)
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCountryId IS NULL)
			BEGIN
				INSERT INTO [General].[Countries]
						([Name]
						,[CurrencyId]
						,[Code]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pName
						,@pCurrencyId
						,UPPER(@pCode)
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[Countries]
					SET  [Name] = @pName
						,[CurrencyId] = @pCurrencyId
						,[Code] = UPPER(@pCode)
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CountryId] = @pCountryId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerCreditCards_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerCreditCards_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/15/2014
-- Description:	Retrieve Customer CreditCards information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerCreditCards_Retrieve]
(
	  @pCustomerId INT
	 ,@pCustomerCreditCardId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT	 a.[CustomerCreditCardId]
			,a.[CustomerId]
			,a.[CreditCardNumber]
			,a.[ExpirationYear]
			,a.[ExpirationMonth]
			,a.[CreditLimit]
			,a.[StatusId]
			,b.[Name] AS [StatusName]
			,a.[CreditCardHolder]
			,d.[Symbol] AS [CurrencySymbol]
			,d.[Name] AS [CurrencyName]
			,a.[RowVersion]
	FROM [General].[CustomerCreditCards] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pCustomerCreditCardId IS NULL OR a.[CustomerCreditCardId] = @pCustomerCreditCardId)
	ORDER BY [CustomerId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerCreditCards_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerCreditCards_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/15/2014
-- Description:	Delete Customer information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerCreditCards_Delete]
(
	 @pCustomerCreditCardId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [General].[CustomerCreditCards]
			WHERE [CustomerCreditCardId] = @pCustomerCreditCardId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_AllCustomers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AllCustomers_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 20/01/2014
-- Description:	Retrieve All Customer information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_AllCustomers_Retrieve]
(
	@pCustomerId INT = NULL,
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL
	
)
AS
BEGIN

	SET NOCOUNT ON
	

	SELECT distinct
		 a.[CustomerId]
		,a.[Name] AS [EncryptedName]
		,a.[CurrencyId]
		,b.[Name] AS [CurrencyName]
		,a.[Logo]
		,a.[IsActive]
		,a.[AccountNumber]
		,a.[UnitOfCapacityId]
		,d.[Name] AS [UnitOfCapacityName]
		,a.[IssueForId]
		,c.[Name] AS IssueForStr
		,a.[CreditCardType]
		,CASE a.[CreditCardType] WHEN ''C'' THEN ''Crédito'' WHEN ''D'' THEN ''Débito'' ELSE '''' END AS CreditCardTypeStr
		,b.[Symbol] AS [CurrencySymbol]
		,a.[CountryId]
		,e.[Name] AS [CountryName]
		,a.[InsertDate]
		,a.[RowVersion]
	  --,a.InsurancePartnerId
    FROM [General].[Customers] a
		INNER JOIN [Control].[Currencies] b
			ON a.[CurrencyId] = b.[CurrencyId]
		LEFT JOIN [General].[Types] c
			ON c.[TypeId] = a.[IssueForId]
		INNER JOIN [General].[Types] d
			ON d.[TypeId] = a.[UnitOfCapacityId]
		INNER JOIN [General].[Countries] e
			ON a.[CountryId] = e.[CountryId]
	
	WHERE a.[IsDeleted]=CAST(0 as bit)
	  and a.IsActive=1
	  AND (@pCustomerId IS NULL OR a.[CustomerId] = @pCustomerId)
	  AND (@pCountryId IS NULL OR a.[CountryId] = @pCountryId)
	
	  /*AND (@pKey IS NULL 
				--OR a.[Name] like ''%''+@pKey+''%'' -- Encrypted --> Linq
				OR b.[Name] like ''%''+@pKey+''%''
				)*/
	ORDER BY [CustomerId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Alarms_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Alarms_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 13/01/2014
-- Description:	Delete Alarm information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Alarms_Delete]
(
	 @pAlarmId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE General.Alarms
			WHERE [AlarmId] = @pAlarmId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CardRequest_AddOrEdit]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CardRequest_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update CustomerCreditCards information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CardRequest_AddOrEdit]
(
	 @pCardRequestId INT = NULL
	,@pCustomerId INT
	,@pPlateId VARCHAR(10) = NULL
	,@pDriverName VARCHAR(250) = NULL
	,@pDriverIdentification VARCHAR(50) = NULL
	,@pStateId INT = NULL
	,@pCountyId INT = NULL
	,@pCityId INT = NULL
	,@pDeliveryState VARCHAR(50) = NULL
	,@pDeliveryCounty VARCHAR(50) = NULL
	,@pDeliveryCity VARCHAR(50) = NULL
	,@pAddressLine1 VARCHAR(1024)
	,@pAddressLine2 VARCHAR(1024) = NULL 
	,@pPaymentReference VARCHAR(50)
	,@pAuthorizedPerson VARCHAR(520)
	,@pContactPhone VARCHAR(50)
	,@pEstimatedDelivery DATETIME = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCardRequestId IS NULL)
			BEGIN
				INSERT INTO [Control].[CardRequest]
						([CustomerId]
						,[PlateId]
						,[DriverName]
						,[DriverIdentification]
						,[StateId]
						,[CountyId]
						,[CityId]
						,[DeliveryState]
						,[DeliveryCounty]
						,[DeliveryCity]
						,[AddressLine1]
						,[AddressLine2]
						,[PaymentReference]
						,[AuthorizedPerson]
						,[ContactPhone]
						,[EstimatedDelivery]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pPlateId
						,@pDriverName
						,@pDriverIdentification
						,@pStateId
						,@pCountyId
						,@pCityId
						,@pDeliveryState
						,@pDeliveryCounty
						,@pDeliveryCity
						,@pAddressLine1
						,@pAddressLine2
						,@pPaymentReference
						,@pAuthorizedPerson
						,@pContactPhone
						,@pEstimatedDelivery
						,GETUTCDATE()
						,@pLoggedUserId)				
			END
			ELSE
			BEGIN
				UPDATE [Control].[CardRequest]
					SET	 [PlateId] = @pPlateId
						,[DriverName] = @pDriverName
						,[DriverIdentification] = @pDriverIdentification
						,[StateId] = @pStateId
						,[CountyId] = @pCountyId
						,[CityId] = @pCityId
						,[DeliveryState] = @pDeliveryState
						,[DeliveryCounty] = @pDeliveryCounty
						,[DeliveryCity] = @pDeliveryCity
						,[AddressLine1] = @pAddressLine1
						,[AddressLine2] = @pAddressLine2
						,[PaymentReference] = @pPaymentReference
						,[AuthorizedPerson] = @pAuthorizedPerson
						,[ContactPhone] = @pContactPhone
						,[EstimatedDelivery] = @pEstimatedDelivery
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CardRequestId] = @pCardRequestId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Alarm_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Alarm_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martínez H.
-- Create date: 17/Nov/2014
-- Description:	Insert or Update Alarm information
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_Alarm_AddOrEdit]
(
	 @pAlarmId INT = NULL 
	,@pCustomerId INT
	,@pPhone VARCHAR(100)
	,@pEmail VARCHAR(500)
	,@pAlarmTriggerId INT 
	,@pEntityTypeId INT 
	,@pEntityId INT
	,@pActive BIT 
	,@pPeriodicityTypeId INT = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP = NULL
)
AS
BEGIN 
	SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pAlarmId IS NULL)
			BEGIN 
				INSERT INTO [General].[Alarms]
						([CustomerId]
						,[Phone]
						,[Email]
						,[AlarmTriggerId]
						,[EntityTypeId]
						,[EntityId]
						,[Active]
						,[PeriodicityTypeId]
						,[InsertDate]
						,[InsertUserId])
				VALUES (@pCustomerId
						,@pPhone
						,@pEmail
						,@pAlarmTriggerId
						,@pEntityTypeId
						,@pEntityId
						,@pActive
						,@pPeriodicityTypeId
						,GETUTCDATE()
						,@pLoggedUserId)
			END 
			ELSE 
			BEGIN 
				UPDATE [General].[Alarms]
					SET  [Phone] = @pPhone
						,[Email] = @pEmail
						,[AlarmTriggerId] = @pAlarmTriggerId
						,[EntityTypeId] = @pEntityTypeId
						,[EntityId] = @pEntityId
						,[Active] = @pActive
						,[PeriodicityTypeId] = @pPeriodicityTypeId
				WHERE [AlarmId] = @pAlarmId
				AND [RowVersion] = @pRowVersion
			END
			
			SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END 
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_AddExpirationDriverLicense_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AddExpirationDriverLicense_Alarm]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [General].[Sp_AddExpirationDriverLicense_Alarm]
AS
BEGIN 
	DECLARE @lAlarmId INT, 
			@lUserId INT,
			@lCustomerId INT						
			
	DECLARE cDrivers CURSOR FOR
	
	SELECT [UserId], [CustomerId] FROM [General].[DriversUsers]
	WHERE [LicenseExpiration] < GETUTCDATE()
	
	OPEN cDrivers
	
	FETCH cDrivers INTO @lUserId, @lCustomerId
	WHILE (@@FETCH_STATUS = 0)
	BEGIN 
		SELECT @lAlarmId = [AlarmId]
		FROM [General].[Alarms]
		WHERE [AlarmTriggerId] = 504 
		  AND [EntityId] = @lUserId
		  AND [CustomerId] = @lCustomerId
		  AND [Active] = ''TRUE''
			
		IF @lAlarmId IS NOT NULL 
		BEGIN 
			PRINT @lUserId 
		END		
		
		FETCH cDrivers INTO @lUserId, @lCustomerId
	END 	
	CLOSE cDrivers
	
	DEALLOCATE cDrivers
	
END' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerCreditCards_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerCreditCards_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update CustomerCreditCards information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerCreditCards_AddOrEdit]
(
	 @pCustomerCreditCardId INT = NULL
	,@pCustomerId INT
	,@pCreditCardHolder VARCHAR(200)
	,@pCreditCardNumber NVARCHAR(520)
	,@pExpirationYear INT
	,@pExpirationMonth INT
	,@pCreditLimit NUMERIC(16,2)
	,@pStatusId INT
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCustomerCreditCardId IS NULL)
			BEGIN
				INSERT INTO [General].[CustomerCreditCards]
						([CustomerId]
						,[CreditCardHolder]
						,[CreditCardNumber]
						,[ExpirationYear]
						,[ExpirationMonth]
						,[CreditLimit]
						,[StatusId]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pCreditCardHolder
						,@pCreditCardNumber
						,@pExpirationYear
						,@pExpirationMonth
						,@pCreditLimit
						,@pStatusId
						,GETUTCDATE()
						,@pLoggedUserId)
						
				SET @lRowCount = @@ROWCOUNT
				SET @pCustomerCreditCardId = SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
				UPDATE [General].[CustomerCreditCards]
					SET  [CreditCardHolder] = @pCreditCardHolder
						,[CreditCardNumber] = @pCreditCardNumber
						,[ExpirationYear] = @pExpirationYear
						,[ExpirationMonth] = @pExpirationMonth
						,[CreditLimit] = @pCreditLimit
						,[StatusId] = @pStatusId
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CustomerCreditCardId] = @pCustomerCreditCardId
				  AND [RowVersion] = @pRowVersion
				  
				SET @lRowCount = @@ROWCOUNT
			END
			
			-----START - Insert || Update CustomerCredits---			
			DECLARE @lCustomerCreditId INT
			
			SET @lCustomerCreditId = COALESCE(
				(SELECT [CustomerCreditId]
					FROM [Control].[CustomerCredits]
				WHERE [CustomerId] = @pCustomerId
				  AND [Year] = DATEPART(year, GETUTCDATE())
				  AND [Month] = DATEPART(month, GETUTCDATE())), -1)
			
			IF( @lCustomerCreditId = -1)
			BEGIN
				INSERT INTO [Control].[CustomerCredits](
						 [CustomerId]
						,[Year]
						,[Month]
						,[CreditAmount]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,DATEPART(year, GETUTCDATE())
						,DATEPART(month, GETUTCDATE())
						,@pCreditLimit
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Control].[CustomerCredits]
				SET  [CreditAmount] = @pCreditLimit
					,[ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
				WHERE [CustomerCreditId] = @lCustomerCreditId
			END
			-----END - Insert || Update CustomerCredits---		
					
            
            -- History control --
			INSERT INTO [General].[CustomerCreditCardsHx]
			   ([CustomerCreditCardId]
			   ,[CustomerId]
			   ,[CreditCardHolder]
			   ,[CreditCardNumber]
			   ,[ExpirationYear]
			   ,[ExpirationMonth]
			   ,[CreditLimit]
			   ,[StatusId]
			   ,[InsertDate]
			   ,[InsertUserId]
			   ,[ModifyDate]
			   ,[ModifyUserId])
			SELECT 
				[CustomerCreditCardId]
			   ,[CustomerId]
			   ,[CreditCardHolder]
			   ,[CreditCardNumber]
			   ,[ExpirationYear]
			   ,[ExpirationMonth]
			   ,[CreditLimit]
			   ,[StatusId]
			   ,[InsertDate]
			   ,[InsertUserId]
			   ,[ModifyDate]
			   ,[ModifyUserId] FROM [General].[CustomerCreditCards] WHERE [CustomerCreditCardId] = @pCustomerCreditCardId
			-- History control --
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_DriversUsers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversUsers_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Retrieve Customer User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DriversUsers_Retrieve]
(
	  @pUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT
		  a.[DriversUserId]
		 ,a.[CustomerId]
		 ,a.[Identification] AS [EncryptedIdentification]
		 ,a.[Code] AS [EncryptedCode]
		 ,a.[License] AS [EncryptedLicense]
		 ,a.[LicenseExpiration]
		 ,a.[Dallas] AS EncryptedDallas
		 ,b.[CountryId]
		 ,a.[DailyTransactionLimit]
    FROM [General].[DriversUsers] a
		INNER JOIN [General].[Customers] b
			ON b.[CustomerId] = a.[CustomerId]		
	WHERE a.[UserId] = @pUserId
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_DriversUsers_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversUsers_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Add Or Edit Driver User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DriversUsers_AddOrEdit]
(
	 @pDriversUserId INT = NULL
	,@pUserId INT
	,@pCustomerId INT
	,@pIdentification VARCHAR(50)
	,@pCode VARCHAR(20) = NULL
	,@pLicense VARCHAR(50)
	,@pLicenseExpiration DATE
	,@pDallas VARCHAR(250) = NULL
	,@pDailyTransactionLimit INT
	,@pLoggedUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		IF (@pDriversUserId IS NULL)
		BEGIN
			INSERT INTO [General].[DriversUsers]
					([UserId]
					,[CustomerId]
					,[Identification]
					,[Code]
					,[License]
					,[LicenseExpiration]
					,[Dallas]
					,[DailyTransactionLimit]
					,[InsertDate]
					,[InsertUserId])
			VALUES	(@pUserId
					,@pCustomerId
					,@pIdentification
					,@pCode
					,@pLicense
					,@pLicenseExpiration
					,@pDallas
					,@pDailyTransactionLimit
					,GETUTCDATE()
					,@pLoggedUserId)
		END
		ELSE
		BEGIN
			UPDATE [General].[DriversUsers]
				SET  [Identification] = @pIdentification
					,[Code] = @pCode
					,[License] = @pLicense
					,[LicenseExpiration] = @pLicenseExpiration
					,[Dallas] = @pDallas
					,[DailyTransactionLimit] = @pDailyTransactionLimit
					,[ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
			WHERE [DriversUserId] = @pDriversUserId
			
		END
        
        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_DashboardModuleByCustomer_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DashboardModuleByCustomer_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 09/11/2014
-- Description:	Retrieve country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DashboardModuleByCustomer_Retrieve]
(
	 @pCustomerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT 
		M.DashboardModuleId [DashboardModuleId],
		M.Name [Name],
		@pCustomerId [CustomerId],
		ISNULL(D.[Order], 0) AS [Order],
		D.[RowVersion] [RowVersion]
	FROM 
		[General].[DashboardModules] M
			LEFT JOIN (SELECT 
							 D.DashboardModuleId
							,D.[Order]
							,D.[RowVersion]
							FROM [General].[DashboardModulesByCustomer] D
							WHERE D.[CustomerId] = @pCustomerId) D
		ON M.DashboardModuleId = D.DashboardModuleId
	ORDER BY [ORDER]
	
    SET NOCOUNT OFF
END


	' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_DashboardModuleByCustomer_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_DashboardModuleByCustomer_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 09/11/2014
-- Description:	Retrieve country information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_DashboardModuleByCustomer_Retrieve]
(
	 @pCustomerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT 
		M.DashboardModuleId,
		M.Name,
		@pCustomerId,
		D.[Order],
		D.[RowVersion]
	FROM 
		[General].[DashboardModules] M
		LEFT OUTER JOIN
		[General].[DashboardModulesByCustomer] D ON M.DashboardModuleId = D.DashboardModuleId
	WHERE
		D.CustomerId IS NULL OR D.CustomerId = @pCustomerId
	ORDER BY
		[ORDER]
	
    SET NOCOUNT OFF
END


	' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_DashboardModuleByCustomer_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DashboardModuleByCustomer_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo.
-- Create date: 02/12/2014
-- Description:	Insert or Update Dashboard Modules
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DashboardModuleByCustomer_AddOrEdit]
(
	 @pCustomerId INT
	,@pDashboardModuleId INT
	,@pOrder INT
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (EXISTS(SELECT 1 FROM [General].[DashboardModulesByCustomer] WHERE [CustomerId] = @pCustomerId AND [DashboardModuleId] = @pDashboardModuleId))
			BEGIN
				UPDATE [General].[DashboardModulesByCustomer]
					SET  [Order] = @pOrder
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CustomerId] = @pCustomerId
				  AND [DashboardModuleId] = @pDashboardModuleId
				  AND [RowVersion] = @pRowVersion							
			END
			ELSE
			BEGIN
				INSERT INTO [General].[DashboardModulesByCustomer]
						([CustomerId]
						,[DashboardModuleId]
						,[Order]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pDashboardModuleId
						,@pOrder
						,GETUTCDATE()
						,@pLoggedUserId)	
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerUsers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerUsers_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Retrieve Customer User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerUsers_Retrieve]
(
	  @pUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @RoleId NVARCHAR(128) 
	SELECT @RoleId = Id FROM [dbo].[AspNetRoles] WHERE Name = ''CUSTOMER_CHANGEODOMETER''

	DECLARE @AspNetUserId NVARCHAR(128) 
	SELECT @AspNetUserId = AspNetUserId FROM [General].[Users] WHERE UserId = @pUserId

	DECLARE @ExistsChangeOdometer BIT
	SELECT @ExistsChangeOdometer = 1 FROM [dbo].[AspNetUserRoles] UR WHERE UR.UserId = @AspNetUserId AND UR.RoleId = @RoleId
		
	SELECT
		  a.[CustomerUserId]
		 ,a.[CustomerId]
		 ,a.[Identification] AS [EncryptedIdentification]
		 ,b.[CountryId]
		 ,ISNULL(@ExistsChangeOdometer,0) [ChangeOdometer]
    FROM [General].[CustomerUsers] a
		INNER JOIN [General].[Customers] b
			ON b.[CustomerId] = a.[CustomerId]		
	WHERE a.[UserId] = @pUserId
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerUsers_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerUsers_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Add Or Edit Customer User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerUsers_AddOrEdit]
(
	 @pCustomerUserId INT = NULL
	,@pUserId INT
	,@pCustomerId INT
	,@pChangeOdometer BIT = 0
	,@pIdentification VARCHAR(50)
	,@pLoggedUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		IF (@pCustomerUserId IS NULL)
		BEGIN
			INSERT INTO [General].[CustomerUsers]
					([UserId]
					,[CustomerId]
					,[Identification]
					,[InsertDate]
					,[InsertUserId])
			VALUES	(@pUserId
					,@pCustomerId
					,@pIdentification
					,GETUTCDATE()
					,@pLoggedUserId)				
		END
		ELSE
		BEGIN
			UPDATE [General].[CustomerUsers]
				SET  [Identification] = @pIdentification
					,[ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
			WHERE [CustomerUserId] = @pCustomerUserId
			
		END
        		
		DECLARE @RoleId NVARCHAR(128) 
		SELECT @RoleId = Id FROM [dbo].[AspNetRoles] WHERE Name = ''CUSTOMER_CHANGEODOMETER''

		DECLARE @AspNetUserId NVARCHAR(128) 
		SELECT @AspNetUserId = AspNetUserId FROM [General].[Users] WHERE UserId = @pUserId

		DECLARE @ExistsChangeOdometer BIT
		SELECT @ExistsChangeOdometer = 1 FROM [dbo].[AspNetUserRoles] UR WHERE UR.UserId = @AspNetUserId AND UR.RoleId = @RoleId
		
		IF @pChangeOdometer = 1 
		BEGIN
			IF @ExistsChangeOdometer = 1 
			BEGIN
				UPDATE [dbo].[AspNetUserRoles] SET RoleId = @RoleId WHERE UserId = @AspNetUserId
			END
			ELSE
			BEGIN
				INSERT INTO [dbo].[AspNetUserRoles] (UserId, RoleId) VALUES (@AspNetUserId,@RoleId)
			END
		END
		ELSE
		BEGIN
			DELETE FROM [dbo].[AspNetUserRoles] WHERE UserId = @AspNetUserId AND RoleId = @RoleId
		END

        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END



' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CustomerCredits_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerCredits_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CustomerCredit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CustomerCredits_Retrieve]
(
	  @pCustomerId INT						--@pCustomerId: Customer Id
	 ,@pYear INT							--@pYear: Year
	 ,@pMonth INT							--@pMonth: Month
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		 a.[CustomerCreditId]
		,a.[CustomerId]
		,a.[Year]
		,a.[Month]
		,a.[CreditAmount]
		,a.[CreditAssigned]
		,a.[CreditAvailable]
		,a.[CreditTypeId]
		,@lCurrencySymbol AS [CurrencySymbol]
		,a.[RowVersion]
    FROM [Control].[CustomerCredits] a
	WHERE a.[CustomerId] = @pCustomerId
	  AND a.[Year] = @pYear
	  AND a.[Month] = @pMonth
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CustomerCredits_AddFromAPI]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerCredits_AddFromAPI]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/30/2014
-- Description:	Insert Customer Credit information from API
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CustomerCredits_AddFromAPI]
(
	 @pAccountNumber VARCHAR(50)
	,@pYear INT
	,@pMonth INT
	,@pCreditAmount DECIMAL(16,2)
	,@pCreditTypeId INT
	,@pLoggedUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lCustomerId INT            
            DECLARE @lCustomerCreditId INT = 0
              
            SELECT @lCustomerId = a.[CustomerId]
				FROM [General].[Customers] a
            WHERE a.[AccountNumber] = @pAccountNumber
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			 
			INSERT INTO [Control].[CustomerCredits](
					 [CustomerId]
					,[Year]
					,[Month]
					,[CreditAmount]
					,[CreditTypeId]
					,[InsertDate]
					,[InsertUserId])
			VALUES	(@lCustomerId
					,@pYear
					,@pMonth
					,@pCreditAmount
					,@pCreditTypeId
					,GETUTCDATE()
					,@pLoggedUserId)
			
			SELECT @lCustomerCreditId = SCOPE_IDENTITY()
					
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	
	SELECT @lCustomerCreditId AS [CustomerCreditId] 
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_ApplicantsReport_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ApplicantsReport_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 12/11/2014
-- Description:	Retrieve TransactionsReport information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_ApplicantsReport_Retrieve]
(
	@pUserId INT = NULL,						--@pUserId: InsrtUserId
	@pCountryId INT = NULL,					--@pCountryId: CountryId
	@pYear INT = NULL,					--@pYear: Year
	@pMonth INT = NULL,					--@pMonth: Month
	@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
	@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @pIssueForId INT

		SELECT 
			c.[Name] [CustomerName], 
			CONVERT(date, cc.[InsertDate]) Date, 
			CASE WHEN c.[CreditCardType] = ''C'' THEN ''Crédito'' ELSE ''Débito'' END [CreditCardType], 
			cu.[Name] [CurrencyName], 
			CASE WHEN c.[IssueForId] = 101 THEN ''Conductor'' ELSE ''Vehículo'' END [FleetCard], 
			COUNT(*) [Quantity], 
			co.[Name] [CountryName]
		FROM 
			[Control].[CardRequest] cc
			INNER JOIN [General].[Customers] c ON cc.[CustomerId] = c.[CustomerId]
			INNER JOIN [General].[Countries] co ON c.[CountryId] = co.[CountryId] 
			INNER JOIN [Control].[Currencies] cu ON c.[CurrencyId] = cu.[CurrencyId] 
		WHERE 
			cc.[InsertUserId] = @pUserId AND 
			co.[CountryId] = @pCountryId AND 
			((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
				AND DATEPART(m,c.[InsertDate]) = @pMonth
				AND DATEPART(yyyy,c.[InsertDate]) = @pYear) OR
				(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
				AND c.[InsertDate] BETWEEN @pStartDate AND @pEndDate))
		GROUP BY
			cc.[CustomerId], 
			c.[Name], 
			CONVERT(date, cc.[InsertDate]), 
			c.[CreditCardType], 
			cu.[Name], 
			c.[IssueForId], 
			co.[Name]
		ORDER BY
			c.[Name]
	
    SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_Customers_CardRequest]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Customers_CardRequest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 12/03/2014
-- Description:	Delete Customer CardRequest information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Customers_CardRequest]
(
	 @pCardRequestId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Control].[CardRequest]
			WHERE [CardRequestId] = @pCardRequestId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_Fuels_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Fuels_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Fuels_Retrieve]
(
	 @pFuelId INT = NULL,
	 @pKey VARCHAR(800) = NULL,
	 @pCountryId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT
		 a.[FuelId]
		,a.[Name]
		,a.[CountryId]
		,a.[RowVersion]
		,b.[Name] AS [CountryName]
    FROM [Control].[Fuels] a
		INNER JOIN [General].[Countries] b
			ON a.[CountryId] = b.[CountryId]
	WHERE (@pFuelId IS NULL OR [FuelId] = @pFuelId)
	  AND (@pCountryId IS NULL OR a.[CountryId] = @pCountryId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
				OR b.[Name] like ''%''+@pKey+''%'')
	ORDER BY [FuelId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_Fuels_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Fuels_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Delete Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Fuels_Delete]
(
	 @pFuelId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Control].[Fuels]
			WHERE [FuelId] = @pFuelId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_Fuels_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Fuels_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Fuels_AddOrEdit]
(
	 @pFuelId INT = NULL
	,@pName VARCHAR(50)
	,@pLoggedUserId INT
	,@pCountryId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pFuelId IS NULL)
			BEGIN
				INSERT INTO [Control].[Fuels]
						([Name]
						,[CountryId]
						,[InsertDate]
						,[InsertUserId])
				VALUES (@pName
						,@pCountryId
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Control].[Fuels]
					SET  [Name] = @pName
						,[CountryId] = @pCountryId
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [FuelId] = @pFuelId
				  AND [RowVersion] = @pRowVersion
			END
			SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFences_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFences_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/13/2014
-- Description:	Retrieve GeoFences information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_GeoFences_Retrieve]
(
	 @pGeoFenceId INT = NULL
	,@pKey VARCHAR(800) = NULL
	,@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		co.[Code] CountryCode,
		g.[GeoFenceId]
		,g.[Name]
		,g.[Active]
		,g.[CustomerId]
		,g.[Alarm]
		,g.[AlarmId]
		,g.[Description]
		,g.[Polygon].STAsText() Polygon
		,g.[In]
		,g.[Out]
		,g.[RowVersion]
		,g.[CustomerId]
    FROM [Efficiency].[GeoFences] g
    INNER JOIN [General].[Customers] c ON g.[CustomerId] = c.[CustomerId]
    INNER JOIN [General].[Countries] co ON c.[CountryId] = co.[CountryId]
	WHERE g.[CustomerId] = @pCustomerId
	  AND (@pGeoFenceId IS NULL OR g.[GeofenceId] = @pGeoFenceId)
	  AND (@pKey IS NULL 
				OR UPPER(g.[Name]) like ''%''+@pKey+''%''
				OR UPPER(g.[Description]) like ''%''+@pKey+''%'')
	ORDER BY g.[GeofenceId] DESC
	
    SET NOCOUNT OFF
END



' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFences_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFences_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/13/2014
-- Description:	Delete GeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_GeoFences_Delete]
(
	 @pGeoFenceId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Efficiency].[GeoFences]
			WHERE [GeofenceId] = @pGeoFenceId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFences_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFences_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'



-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/13/2014
-- Description:	Insert or Update GeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_GeoFences_AddOrEdit]
(
	@pGeoFenceId int = NULL			--@pGeoFenceId: PK of the table
	,@pName varchar(50)					--@pName: GeoFence Name
	,@pActive bit						--@pActive:	If GeoFence Active
	,@pCustomerId int					--@pCustomerId: FK of Customer
	,@pAlarm bit = NULL					--@pAlarm: GeoFence has alarm
	,@pAlarmId int = NULL				--@pAlarmId: Id of alarm
	,@pDescription varchar(MAX)			--@pDescription: GeoFence Description
	,@pPolygon varchar(MAX)				--@pPolygon: GeoFence Polygon
	,@pIn bit							--@@pIn: If Geofence generate out alarm
	,@pOut bit							--@@pOut: If Geofence generate in alarm
	,@pLoggedUserId int					--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion timestamp				--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pGeoFenceId IS NULL)
			BEGIN
				INSERT INTO [Efficiency].[GeoFences]
						([Name]
						,[Active]
						,[CustomerId]
						,[Alarm]
						,[AlarmId]
						,[Description]
						,[Polygon]
						,[In]
						,[Out]
						,[InsertDate]
						,[InsertUserId]
						)
				VALUES	(@pName
						,@pActive
						,@pCustomerId
						,@pAlarm
						,@pAlarmId
						,@pDescription
						,(geometry::STGeomFromText(@pPolygon, 0))
						,@pIn
						,@pOut
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Efficiency].[GeoFences]
					SET  
						[CustomerId] = @pCustomerId
						,[Name] = @pName
						,[Active] = @pActive
						,[Alarm] = @pAlarm
						,[AlarmId] = @pAlarmId
						,[Description] = @pDescription
						,[Polygon] = @pPolygon
						,[In] = @pIn
						,[Out] = @pOut
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [GeoFenceId] = @pGeoFenceId
						  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_WeightedScoreSettings_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_WeightedScoreSettings_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/01/2014
-- Description:	Retrieve Weighted Score Settings information
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_WeightedScoreSettings_Retrieve]
(
	 @pWeightedScoreId INT = NULL,			--@pWeightedScoreId: PK of the table
	 @pCustomerId INT,						--@pCustomerId: FK of Customer
	 @pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[WeightedScoreId]
		,a.[CustomerId]
		,a.[ScoreTypeId] 
		,b.[Code] ScoreTypeCode
		,b.[Name] ScoreTypeName
		,a.[Weight]
		,a.[RowVersion]
    FROM [Operation].[WeightedScoreSettings] a
		INNER JOIN [Operation].[ScoreTypes] b
			ON a.[ScoreTypeId] = b.[ScoreTypeId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pWeightedScoreId IS NULL OR a.[WeightedScoreId] = @pWeightedScoreId)	  
	  AND (@pKey IS NULL 
				OR b.[Code] like ''%''+@pKey+''%''
				OR b.[Name] like ''%''+@pKey+''%'')
	ORDER BY b.[Code] ASC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_WeightedScoreSettings_Delete]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_WeightedScoreSettings_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/01/2014
-- Description:	Delete Weighted Score Settings information
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_WeightedScoreSettings_Delete]
(
	 @pWeightedScoreId INT				--@pWeightedScoreId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Operation].[WeightedScoreSettings]
			WHERE [WeightedScoreId] = @pWeightedScoreId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_WeightedScoreSettings_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_WeightedScoreSettings_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/01/2014
-- Description:	Insert or Update Weighted Score Settings information
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_WeightedScoreSettings_AddOrEdit]
(
	 @pWeightedScoreId INT = NULL			--@pWeightedScoreId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customers
	,@pScoreTypeId INT						--@pScoreTypeId: FK of Score Types
	,@pWeight NUMERIC(5,2)					--@pWeight: Weight of the Score Type
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pWeightedScoreId IS NULL)
			BEGIN
				INSERT INTO [Operation].[WeightedScoreSettings]
						([CustomerId]
						,[ScoreTypeId]
						,[Weight]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pScoreTypeId
						,@pWeight
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Operation].[WeightedScoreSettings]
					SET  [CustomerId] = @pCustomerId
						,[ScoreTypeId] = @pScoreTypeId
						,[Weight] = @pWeight
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [WeightedScoreId] = @pWeightedScoreId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalog_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalog_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 06/Oct/2014
-- Description:	Preventive Maintenance information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_Retrieve]
	  @pCustomerId INT								--@pCustomerID: Customer ID PK from table Customer
	 ,@pPreventiveMaintenanceCatalogId INT = NULL	--
	 ,@pKey VARCHAR(250) = NULL
AS
BEGIN	
	SET NOCOUNT ON;
	SELECT 
		a.[PreventiveMaintenanceCatalogId],
		a.[Description],
		a.[FrequencyKm],
		a.[AlertBeforeKm],
		a.[FrequencyMonth],
		a.[AlertBeforeMonth],
		a.[FrequencyDate],
		a.[AlertBeforeDate],
		a.[Cost],
		a.[RowVersion]
		FROM [General].[PreventiveMaintenanceCatalog] a
		WHERE  a.[CustomerId] = @pCustomerId
				AND (@pPreventiveMaintenanceCatalogId IS NULL 
					OR a.[PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId)
				AND (@pKey IS NULL 
					OR a.[Description] LIKE ''%''+@pKey+''%'')				
	SET NOCOUNT OFF;
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalog_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalog_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 03/Oct/2014
-- Description:	Delete Preventive Maintenance information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_Delete]
	@pPreventiveMaintenanceCatalogId INT
AS
BEGIN	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		DELETE [General].[PreventiveMaintenanceCatalog]
		WHERE [PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId
		
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalog_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalog_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 03/Oct/2014
-- Description:	Insert or Update Preventive Maintenance information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_AddOrEdit]
	 @pPreventiveMaintenanceCatalogId INT = NULL	--@pPreventiveMaintenanceID: PK of the table
	,@pCustomerId INT	= NULL				        --@pCustomer: FK of the table Control.Customer
	,@pDescription VARCHAR(500)						--@pDescription: Preventive Maintenance Description
	,@pFrequencyKm FLOAT = NULL						--@pFrequencyKm: Frequency Kilometers
	,@pAlertBeforeKm INT = NULL						--@pAlertBeforeKm: Alert Before Kilometers
	,@pFrequencyMonth FLOAT = NULL					--@pFrequencyMonth: Frequency Monts
	,@pAlertBeforeMonth INT = NULL					--@pAlertBeforeMonth: Alert Before Monts
	,@pFrequencyDate DATE = NULL				--@pFrequencyMonth: Frequency Date
	,@pAlertBeforeDate INT = NULL					--@pAlertBeforeMonth: Alert Before Date
	,@pLoggedUserId INT								--@pLoggedUserId: Logged UserId that executes the operationAS
	,@pRowVersion TIMESTAMP							--@pRowVersion: Timestamp of row to prevent bad updates
AS
BEGIN	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0
        DECLARE @lCode VARCHAR(400)
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END		

		IF (@pPreventiveMaintenanceCatalogID IS NULL)
		BEGIN
			INSERT INTO [General].[PreventiveMaintenanceCatalog]
				  ([CustomerId]
				   ,[Description]
				   ,[FrequencyKm]
				   ,[AlertBeforeKm]
				   ,[FrequencyMonth]
				   ,[AlertBeforeMonth]
				   ,[FrequencyDate]
				   ,[AlertBeforeDate]
				   ,[InsertUserId]
				   ,[InsertDate])
			VALUES(@pCustomerId
				  ,@pDescription
				   ,@pFrequencyKm
				   ,@pAlertBeforeKm
				   ,@pFrequencyMonth
				   ,@pAlertBeforeMonth
				   ,@pFrequencyDate
				   ,@pAlertBeforeDate
				  ,@pLoggedUserId
				  ,GETUTCDATE())		
		END
		ELSE
		BEGIN
			UPDATE [General].[PreventiveMaintenanceCatalog]
				SET [Description] = @pDescription
					,[FrequencyKm] = @pFrequencyKm
					,[AlertBeforeKm] = @pAlertBeforeKm
					,[FrequencyMonth] = @pFrequencyMonth
					,[AlertBeforeMonth] = @pAlertBeforeMonth
					,[FrequencyDate] = @pFrequencyDate
					,[AlertBeforeDate] = @pAlertBeforeDate
					,[ModifyUserId] = @pLoggedUserId
					,[ModifyDate] = GETUTCDATE()
			WHERE [PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId
			  AND [RowVersion] = @pRowVersion
		END
		
		SET @lRowCount = @@ROWCOUNT
            
        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
			
		IF @lRowCount = 0
		BEGIN
			RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_Routes_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Routes_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/27/2014
-- Description:	Retrieve Routes information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_Routes_Retrieve]
(
	 @pRouteId INT = NULL
	,@pKey VARCHAR(800) = NULL
	,@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		co.[Code] CountryCode,
		g.[RouteId]
		,g.[Name]
		,g.[Active]
		,g.[CustomerId]
		,g.[Description]
		,g.[RowVersion]
        ,ISNULL(g.[Time], 0) [Time] 
        ,ISNULL(ROUND(g.[Distance], 2), 0) [Distance]
    FROM [Efficiency].[Routes] g
    INNER JOIN [General].[Customers] c ON g.[CustomerId] = c.[CustomerId]
    INNER JOIN [General].[Countries] co ON c.[CountryId] = co.[CountryId]
	WHERE g.[CustomerId] = @pCustomerId
	  AND (@pRouteId IS NULL OR g.[RouteId] = @pRouteId)
	  AND (@pKey IS NULL 
				OR UPPER(g.[Name]) like ''%''+@pKey+''%''
				OR UPPER(g.[Description]) like ''%''+@pKey+''%'')
	ORDER BY g.[Name] ASC
	
    SET NOCOUNT OFF
END



' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_Routes_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Routes_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/27/2014
-- Description:	Delete Route information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_Routes_Delete]
(
	 @pRouteId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Efficiency].[Routes]
			WHERE [RouteId] = @pRouteId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_Routes_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Routes_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/13/2014
-- Description:	Insert or Update Route information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_Routes_AddOrEdit]
(
	@pRouteId int = NULL			--@pRouteId: PK of the table
	,@pName varchar(50)					--@pName: Route Name
	,@pActive bit						--@pActive:	If Route Active
	,@pCustomerId int					--@pCustomerId: FK of Customer
	,@pDescription varchar(MAX)			--@pDescription: Route Description
	,@pLoggedUserId int					--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion timestamp				--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pRouteId IS NULL)
			BEGIN
				INSERT INTO [Efficiency].[Routes]
						([Name]
						,[Active]
						,[CustomerId]
						,[Description]
						,[InsertDate]
						,[InsertUserId]
						)
				VALUES	(@pName
						,@pActive
						,@pCustomerId
						,@pDescription
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Efficiency].[Routes]
					SET  
						[CustomerId] = @pCustomerId
						,[Name] = @pName
						,[Active] = @pActive
						,[Description] = @pDescription
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [RouteId] = @pRouteId
						  --AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END



' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_GroupReference_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GroupReference_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/03/2014
-- Description:	Retrieve IntrackReference information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_GroupReference_Retrieve]
(
	@pId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		v.[VehicleGroupId] [VehicleGroupId], 
		co.[Code] [CountryCode],
		c.[CustomerId] [CustomerId]
	FROM 
		[General].[VehicleGroups] v
		INNER JOIN [General].[Customers] c on v.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Countries] co on co.[CountryId] = c.[CountryId]
	WHERE 
		v.[VehicleGroupId] = @pId
	
			
    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PasswordsHistory_Check]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~ Sp_Password is not supported in current version of SQL Azure
--~ Sp_Password is not supported in current version of SQL Azure
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PasswordsHistory_Check]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ==================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/05/2014
-- Description:	Check the new Password against the Password History 
--				0: Success, Greather Than 0: Fail (returns the number of passwords validated)
-- ==================================================================================================
CREATE PROCEDURE [General].[Sp_PasswordsHistory_Check]
(
	 @pUserId INT							--@pUserId: User Id related to Password
	,@pPassword VARCHAR(256)				--@pPassword: Password encrypted
)
AS
BEGIN	
	SET NOCOUNT ON	
	DECLARE @lPasswordsToValidate int
	
	Select @lPasswordsToValidate = PasswordsValidateRepetition 
	  from [General].[Parameters]
	  
	IF @lPasswordsToValidate is null or @lPasswordsToValidate = 0
		Set @lPasswordsToValidate = 5

	IF @pPassword in (Select Top(@lPasswordsToValidate) Password
					    from [General].[PasswordsHistory]
					   where [UserId] = @pUserId
					   order by [PasswordDate] desc)
	BEGIN
		Select @lPasswordsToValidate
	END
	ELSE
	BEGIN
		Select 0
	END
	 
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PasswordsHistory_Add]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~ Sp_Password is not supported in current version of SQL Azure
--~ Sp_Password is not supported in current version of SQL Azure
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PasswordsHistory_Add]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/05/2014
-- Description:	Insert a record in the Password History information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PasswordsHistory_Add]
(
	 @pUserId INT							--@pUserId: User Id related to Password
	,@pPassword VARCHAR(256)				--@pPassword: Password encrypted
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			INSERT INTO [General].[PasswordsHistory]
					([UserId]
					,[PasswordDate]
					,[Password])
			VALUES	(@pUserId
					,GETUTCDATE()
					,@pPassword)
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Operators_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Operators_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 02/Oct/2014
-- Description:	Operators information
-- =============================================
CREATE PROCEDURE [General].[Sp_Operators_Retrieve]
	  @pCustomerId INT
	 ,@pOperatorId INT = NULL
	 ,@pKey VARCHAR(250) = NULL
AS
BEGIN	
	SET NOCOUNT ON;
	SELECT 
		a.[OperatorId],
		a.[Name],
		a.[Rate],
		b.[CurrencyId],
		b.[Symbol] AS CurrencySymbol,
		a.[RowVersion]
		FROM
		General.Operators a
			INNER JOIN Control.Currencies b 
			ON a.[CurrencyId] = b.[CurrencyId]
		WHERE  (@pCustomerId IS NULL OR a.[CustomerId] = @pCustomerId)
				AND (@pOperatorId IS NULL OR 
				a.[OperatorId] = @pOperatorId)
				AND (@pKey IS NULL 
				OR a.[Name] LIKE ''%''+@pKey+''%'' 
				OR b.[Name] LIKE ''%''+@pKey+''%'')
		ORDER BY a.[Name] DESC	
	SET NOCOUNT OFF;
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Operators_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Operators_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 03/Oct/2014
-- Description:	Delete Operators information
-- =============================================
CREATE PROCEDURE [General].[Sp_Operators_Delete]
	@pOperatorID INT
AS
BEGIN	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		DELETE [General].[Operators]
		WHERE [OperatorID] = @pOperatorID
		
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Operators_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Operators_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 03/Oct/2014
-- Description:	Insert or Update Operators information
-- =============================================
CREATE PROCEDURE [General].[Sp_Operators_AddOrEdit]
	 @pOperatorId INT = NULL --@pOperatorID: PK of the table
	,@pCustomerId INT	= NULL	 --@pCustomer: FK of the table Control.Customer
	,@pName VARCHAR(250)     --@pName: Operators Name General.Customers
	,@pRate DECIMAL(14,2)	 --@pName: Operators Rate
	,@pCurrencyId INT        --@pCurrency: FK of the table Control.Currencies
	,@pLoggedUserId INT      --@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP  --@pRowVersion: Timestamp of row to prevent bad updates
AS
BEGIN	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END		
		
		IF (@pOperatorId IS NULL)
		BEGIN
			INSERT INTO [General].[Operators]
				  ([CustomerId]
				   ,[Name]
				   ,[Rate]
				   ,[InsertUserId]
				   ,[InsertDate]
				   ,[CurrencyId])
			VALUES(@pCustomerId
				  ,@pName
				  ,@pRate
				  ,@pLoggedUserId
				  ,GETUTCDATE()
				  ,@pCurrencyId)		
		END
		ELSE
		BEGIN
			UPDATE [General].[Operators]
				SET [Name] = @pName
				   ,[Rate] = @pRate 
				   ,[CurrencyId] = @pCurrencyId
				   ,[ModifyUserId] = @pLoggedUserId
				   ,[ModifyDate] = GETUTCDATE()
			WHERE [OperatorId] = @pOperatorId
			  AND [RowVersion] = @pRowVersion
		END
		
		SET @lRowCount = @@ROWCOUNT
            
        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
			
		IF @lRowCount = 0
		BEGIN
			RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Partners_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Delete Partner information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Partners_Delete]
(
	 @pPartnerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			UPDATE [General].[Partners]
				SET IsDeleted = 1
			WHERE [PartnerId] = @pPartnerId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Partners_Authentication]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_Authentication]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Partner information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Partners_Authentication]
(
	 @pApiUserName VARCHAR(520),
	 @pApiPassword VARCHAR(520)
)
AS
BEGIN

	SET NOCOUNT ON	
	
	SELECT CONVERT( BIT,
			CASE WHEN (SELECT
							COUNT(1)
						FROM [General].[Partners] a
						WHERE a.[ApiUserName] = @pApiUserName
						  AND a.[ApiPassword] = @pApiPassword) = 1
				THEN 1
				ELSE 0
			END) AS [Authenticated]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Partners_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update Partner information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Partners_AddOrEdit]
(
	 @pPartnerId INT = NULL
	,@pName VARCHAR(250)
	,@pCountryId INT
	,@pLogo VARCHAR(MAX) = NULL
	,@pLoggedUserId INT
	,@pApiUserName VARCHAR(520) = NULL
	,@pApiPassword VARCHAR(520) = NULL
	,@pCapacityUnitId INT  = NULL
	,@pPartnerTypeId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pPartnerId IS NULL)
			BEGIN
				INSERT INTO [General].[Partners]
						([Name]
						,[CountryId]
						,[Logo]
						,[ApiUserName]
						,[ApiPassword]
						,[PartnerGroupId]
						,[CapacityUnitId]
						,[PartnerTypeId]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pName
						,@pCountryId
						,@pLogo
						,@pApiUserName
						,@pApiPassword
						,1 -- Future Change here --
						,@pCapacityUnitId
						,@pPartnerTypeId
						,GETUTCDATE()
						,@pLoggedUserId)
				SET  @pPartnerId = SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
				UPDATE [General].[Partners]
					SET  [Name] = @pName
						,[CountryId] = @pCountryId
						,[Logo] = @pLogo
						,[ApiUserName] = @pApiUserName
						,[ApiPassword] = @pApiPassword
						,[CapacityUnitId] = @pCapacityUnitId
						,[PartnerTypeId] = @pPartnerTypeId
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [PartnerId] = @pPartnerId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
			SELECT @pPartnerId AS PartnerId;
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleUnits_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleUnits_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleSubUnit information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleUnits_Retrieve]
(
	 @pUnitId INT = NULL						--@pUnitId: PK of the table
	,@pCustomerId INT							--@pCustomerId: FK of the table General.Customers
	,@pKey VARCHAR(800) = NULL					--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[UnitId]
		,a.[Name]
		,a.[RowVersion]
		,c.[Code] [CountryCode]
    FROM [General].[VehicleUnits] a 
		INNER JOIN [General].[Customers] b ON a.[CustomerId] = b.[CustomerId]
		INNER JOIN [General].[Countries] c ON b.[CountryId] = c.[CountryId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pUnitId IS NULL OR [UnitId] = @pUnitId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%'')
	ORDER BY [UnitId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleUnits_Delete]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleUnits_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Delete VehicleSubUnit information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleUnits_Delete]
(
	@pUnitId INT						--@pVehicleUnitId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [General].[VehicleUnits]
			WHERE [UnitId] = @pUnitId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleUnits_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleUnits_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update VehicleSubUnit information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleUnits_AddOrEdit]
(
	 @pUnitId INT = NULL					--@pUnitId: PK of the table
	,@pName VARCHAR(250)					--@pName: VehicleSubUnit Name
	,@pCustomerId INT						--@pCustomerId: FK of the table General.Customers
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pUnitId IS NULL)
			BEGIN
				INSERT INTO [General].[VehicleUnits]
						([Name]
						,[CustomerId]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pName
						,@pCustomerId
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[VehicleUnits]
					SET  [Name] = @pName
						,[CustomerId] = @pCustomerId
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [UnitId] = @pUnitId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleGroups_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleGroups_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleGroup information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleGroups_Retrieve]
(
	 @pVehicleGroupId INT = NULL				--@pVehicleGroupId: PK of the table
	,@pCustomerId INT							--@pCustomerId: FK of Customer
	,@pKey VARCHAR(800) = NULL					--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT
		 a.[VehicleGroupId]
		,a.[Name]
		,a.[Description]
		,a.[RowVersion]
		,c.[Code] CountryCode
    FROM [General].[VehicleGroups] a
		INNER JOIN [General].[Customers] b ON a.[CustomerId] = b.[CustomerId]
		INNER JOIN [General].[Countries] c ON b.[CountryId] = c.[CountryId] 
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pVehicleGroupId IS NULL OR [VehicleGroupId] = @pVehicleGroupId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
				OR a.[Description] like ''%''+@pKey+''%'')
	ORDER BY [VehicleGroupId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleGroups_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleGroups_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Delete VehicleGroup information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleGroups_Delete]
(
	@pVehicleGroupId INT							--@pVehicleGroupId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [General].[VehicleGroups]
			WHERE [VehicleGroupId] = @pVehicleGroupId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleGroups_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleGroups_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update VehicleGroup information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleGroups_AddOrEdit]
(
	 @pVehicleGroupId INT = NULL			--@pVehicleGroupId: PK of the table
	,@pName VARCHAR(250)					--@pName: VehicleGroup Name
	,@pCustomerId INT						--@pCustomerId: FK of Customer
	,@pDescription VARCHAR(500)				--@pColour: VehicleGroup Colour
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pVehicleGroupId IS NULL)
			BEGIN
				INSERT INTO [General].[VehicleGroups]
						([CustomerId]
						,[Name]
						,[Description]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pName
						,@pDescription
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[VehicleGroups]
					SET  [CustomerId] = @pCustomerId
						,[Name] = @pName
						,[Description] = @pDescription
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [VehicleGroupId] = @pVehicleGroupId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_States_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_States_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/17/2014
-- Description:	Retrieve States information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_States_Retrieve]
(
	 @pStateId INT = NULL,
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[StateId]
		,a.[Name]
		,a.[CountryId]
		,a.[Code]
		,a.[Latitude]
		,a.[Longitude]
		,a.[RowVersion]
    FROM [General].[States] a
	WHERE (@pStateId IS NULL OR [StateId] = @pStateId)
	  AND (@pCountryId IS NULL OR [CountryId] = @pCountryId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
				OR a.[Code] like ''%''+@pKey+''%'')
	ORDER BY a.[Code]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_SendAlarm]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_SendAlarm]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Cristian Martínez 
-- Create date: 24/11/2014
-- Description:	Store Procedure 
-- ================================================================================================
create PROCEDURE [General].[Sp_SendAlarm]
	@pAlarmId INT = NULL,
	@pAlarmTriggerId INT = NULL,
	@pPeriodicityAlarm INT = NULL,
	@pCustomerId INT = NULL,
	@pPlate VARCHAR(10) = NULL,
	@pDescription VARCHAR(500) = NULL,	
	@pName VARCHAR(250) = NULL,
	@pTimeLastReport TIME = NULL, 
	@pPhone VARCHAR(100) = NULL, 
	@pEmail VARCHAR (500) = NULL, 
	@pInOut VARCHAR (10) = NULL, 
	@pRowVersion TIMESTAMP = NULL
AS
BEGIN 
	DECLARE @lErrorMessage NVARCHAR(4000)
    DECLARE @lErrorSeverity INT
    DECLARE @lErrorState INT
    DECLARE @lSubject VARCHAR(300)
	DECLARE @lMessage VARCHAR (MAX)
	DECLARE @lValue INT = 0
	DECLARE @lNextAlarm DATE
		
	BEGIN TRY 
		-- Get Message
		SELECT @lMessage = b.[Message] 
		FROM [General].[Types] a
			INNER JOIN [General].[Values] b
				  ON a.[TypeId] = b.[TypeId]
		WHERE a.[TypeId] = @pAlarmTriggerId
		
		IF @pPeriodicityAlarm IS NOT NULL
		BEGIN 
			-- Get Value		
			SELECT @lValue = b.[Value] 
			FROM [General].[Types] a
				INNER JOIN [General].[Values] b
		   			  ON a.[TypeId] = b.[TypeId]
			WHERE a.[TypeId] = @pPeriodicityAlarm
		END 
					
		IF (@lValue IS NOT NULL AND @lMessage IS NOT NULL)
		BEGIN 		
			SET @lNextAlarm = DATEADD(DAY, @lValue, DATEADD(Hour, -6,GETUTCDATE()))
			
			IF @pAlarmTriggerId = 500 
			BEGIN 
				SET @lSubject = ''Alarma Geocerca''
				SET @lMessage = REPLACE(@lMessage, ''%vehicle%'', @pPlate)
				SET @lMessage = REPLACE(@lMessage, ''%InOut%'', @pInOut)
				SET @lMessage = REPLACE(@lMessage, ''%Description%'', @pDescription)
				SET @lMessage = REPLACE(@lMessage, ''%time%'', CONVERT(VARCHAR(10), DATEADD(Hour, -6,GETUTCDATE()), 104) + '' '' + CONVERT(VARCHAR(8), DATEADD(Hour, -6,GETUTCDATE()), 108))
				SET @lNextAlarm = NULL 
			END 
	
			IF @pAlarmTriggerId = 502 
			BEGIN 
				SET @lSubject = ''Alarma Vehículo Usado Fuera de Horario''
				SET @lMessage = REPLACE(@lMessage, ''%vehicle%'', @pPlate)
				SET @lMessage = REPLACE(@lMessage, ''%time%'', @pTimeLastReport)
				SET @lNextAlarm = DATEADD(HOUR, 1, DATEADD(Hour, -6,GETUTCDATE()))
			END 
	
			IF @pAlarmTriggerId  = 503
			BEGIN 
			SET @lSubject = ''Alarma de Mantenimiento Preventivo''
				SET @lMessage = REPLACE(@lMessage, ''%Vehicle%'', @pPlate)
				SET @lMessage = REPLACE(@lMessage, ''%Description%'', @pDescription)
			END 
			
			IF @pAlarmTriggerId = 504			
			BEGIN 
				SET @lSubject = ''Alarma por Vencimiento de Licencia''
				SET @lMessage = REPLACE(@lMessage, ''%Driver%'', @pName) 
			END 
		
			IF @pAlarmTriggerId = 505
			BEGIN 
				SET @lSubject = ''Alarma Ajuste de Odómetro''
				SET @lMessage = REPLACE(@lMessage, ''%Vehicle%'', @pPlate)
				SET @lNextAlarm = NULL 
			END 		
			
			INSERT INTO [General].[Emails] ([To], [Subject], [Message], [InsertDate])
			VALUES (@pEmail, @lSubject, @lMessage, DATEADD(Hour, -6,GETUTCDATE()))
			
		
			UPDATE [General].[Alarms]
			SET [NextAlarm] = @lNextAlarm
			WHERE [CustomerId] = @pCustomerId
			  AND [AlarmId] = @pAlarmId
			  AND [RowVersion] = @pRowVersion
			  
			INSERT INTO [General].[AlarmSend] (AlarmId, DateSend, InsertDate, InsertUserId)
			VALUES (@pAlarmId, DATEADD(Hour, -6,GETUTCDATE()), DATEADD(Hour, -6,GETUTCDATE()), 0)					
		END
	END TRY 
	BEGIN CATCH 
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCostCenters_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCostCenters_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleCostCenters information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCostCenters_Retrieve]
(
	 @pCostCenterId INT = NULL					--@pCostCenterId: PK of the table
	,@pCustomerId INT							--@pCustomerId: FK of the table General.Customers
	,@pKey VARCHAR(800) = NULL					--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[CostCenterId]
		,a.[Name]
		,a.[Code]
		,a.[UnitId]
		,b.[Name] AS [UnitName]
		,d.[Code] AS [CountryCode]
		,a.[RowVersion]
    FROM [General].[VehicleCostCenters] a
		INNER JOIN [General].[VehicleUnits] b
			ON a.[UnitId] = b.[UnitId]
		INNER JOIN [General].[Customers] c
			ON b.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Countries] d
			ON c.[CountryId] = d.[CountryId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND (@pCostCenterId IS NULL OR [CostCenterId] = @pCostCenterId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
				OR a.[Code] like ''%''+@pKey+''%''
				OR b.[Name] like ''%''+@pKey+''%'')
	ORDER BY [CostCenterId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCostCenters_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCostCenters_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Delete VehicleCostCenter information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCostCenters_Delete]
(
	@pCostCenterId INT						--@pVehicleCostCenterId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [General].[VehicleCostCenters]
			WHERE [CostCenterId] = @pCostCenterId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCostCenters_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCostCenters_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update VehicleCostCenters information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCostCenters_AddOrEdit]
(
	 @pCostCenterId INT = NULL				--@pCostCenterId: PK of the table
	,@pName VARCHAR(250)					--@pName: VehicleCostCenter Name
	,@pCode VARCHAR(20)						--@@pCode: VehicleCostCenter Code
	,@pUnitId INT							--@pUnitId: FK of the table General.VehicleUnits
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCostCenterId IS NULL)
			BEGIN
				INSERT INTO [General].[VehicleCostCenters]
						([Name]
						,[Code]
						,[UnitId]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pName
						,@pCode
						,@pUnitId
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[VehicleCostCenters]
					SET  [Name] = @pName
						,[Code] = @pCode
						,[UnitId] = @pUnitId
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CostCenterId] = @pCostCenterId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalogCost_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalogCost_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/06/2015
-- Description:	Insert or Update Vehicle by Route information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCatalogCost_AddOrEdit]
(
	@pXmlData VARCHAR(MAX),					
	@pLoggedUserId INT						
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0

            DECLARE @lTotal DECIMAL(16, 2)
            DECLARE @lCatalogId INT
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			INSERT INTO [General].[PreventiveMaintenanceCost]
					([PreventiveMaintenanceCatalogId]
					,[Description]
					,[Cost]
					,[InsertUserId]
					,[InsertDateId])
			SELECT 
					Row.col.value(''./@PreventiveMaintenanceCatalogId'', ''INT'') AS [PreventiveMaintenanceCatalogId]
					,Row.col.value(''./@Description'', ''VARCHAR(500)'') AS [Description]
					,Row.col.value(''./@Cost'', ''DECIMAL(16,2)'') AS [Cost]
					,@pLoggedUserId
					,GETUTCDATE()
			FROM @lxmlData.nodes(''/xmldata/PreventiveMaintenanceCost'') Row(col)
			WHERE Row.col.value(''./@PreventiveMaintenanceCostId'', ''INT'') = 0
			
			UPDATE cost SET 
					cost.Description = Row.col.value(''./@Description'', ''VARCHAR(500)''),
					cost.Cost = Row.col.value(''./@Cost'', ''DECIMAL(16,2)''),
					cost.ModifyUserId = @pLoggedUserId,
					cost.ModifyDateId = GETDATE()
			FROM @lxmlData.nodes(''/xmldata/PreventiveMaintenanceCost'') Row(col)
			INNER JOIN [General].[PreventiveMaintenanceCost] cost ON Row.col.value(''./@PreventiveMaintenanceCostId'', ''INT'') = cost.PreventiveMaintenanceCostId
			WHERE Row.col.value(''./@PreventiveMaintenanceCostId'', ''INT'') <> 0
			
			SELECT @lTotal = SUM(Row.col.value(''./@Cost'', ''DECIMAL(16,2)''))
			FROM @lxmlData.nodes(''/xmldata/PreventiveMaintenanceCost'') Row(col)

			SELECT TOP 1 @lCatalogId = Row.col.value(''./@PreventiveMaintenanceCatalogId'', ''DECIMAL(16,2)'')
			FROM @lxmlData.nodes(''/xmldata/PreventiveMaintenanceCost'') Row(col)

			UPDATE [General].[PreventiveMaintenanceCatalog] 
			SET [Cost] = @lTotal 
			WHERE [PreventiveMaintenanceCatalogId] = @lCatalogId

            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_TransactionLitersByCredit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionLitersByCredit_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martinez H.
-- Create date: 21/Oct/2014
-- Description:	Retrieve Transaction Credit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionLitersByCredit_Retrieve]
(
	 @pCustomerId INT		--@pCustomerId: Customer Id 
	,@pAmount DECIMAL(16,2) --@pAmount: Amount
)
AS
BEGIN
	SET NOCOUNT ON
		DECLARE @lExchangeRate DECIMAL(16,8) = 1.0
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[PartnerId] = b.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = b.[CustomerId] AND c.[CurrencyId] = a.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND a.EndDate IS NULL
	  
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId

	SELECT 
		CONVERT(DECIMAL(16,2), @pAmount/(t.LiterPrice*@lExchangeRate)) AS Liters
	FROM [General].[Customers] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[CustomerId] = b.[CustomerId]
		INNER JOIN [Control].[PartnerFuel] c
		ON c.[PartnerId] = b.[PartnerId]
		LEFT JOIN (
					SELECT
						x.[FuelId], x.[LiterPrice] 
					FROM [Control].[PartnerFuel] x
						INNER JOIN [General].[CustomersByPartner] y
							ON x.[PartnerId] = y.[PartnerId]
						INNER JOIN [General].[Customers] z
							ON y.[CustomerId] = z.[CustomerId]
					WHERE z.CustomerId = @pCustomerId
					  AND x.[EndDate] IS NULL) t
		ON c.[FuelId] = t.[FuelId]
	WHERE a.CustomerId = @pCustomerId
	
	SET NOCOUNT OFF
END' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Users_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andres Oviedo B.
-- Create date: 09/01/2015
-- Description:	Retrieve User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Users_Retrieve]
(
	  @pUserId INT = NULL
	 ,@pKey VARCHAR(800) = NULL
	 ,@pUserName VARCHAR(800) = NULL
	 ,@pCustomerId INT = NULL
	 ,@pCustomerId2 INT = NULL
	 ,@pPartnerId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT
		 a.[UserId]
		,a.[Name] AS [EncryptedName]
		,a.[ChangePassword]
		,a.[Photo]
		,a.[IsActive]
		,b.[Email] AS [EncryptedEmail]
		,b.[PhoneNumber] AS [EncryptedPhoneNumber]
		,CONVERT(BIT,CASE WHEN (a.[IsLockedOut] = 1 OR b.[LockoutEndDateUtc] IS NOT NULL) THEN 1 ELSE 0 END) AS [IsLockedOut]
		,b.[UserName] AS [EncryptedUserName]
		,d.[Name] AS [RoleName]
		,CONVERT(BIT,CASE WHEN e.[CustomerUserId] IS NOT NULL THEN 1 ELSE 0 END) AS IsCustomerUser
		,CONVERT(BIT,CASE WHEN f.[DriversUserId] IS NOT NULL THEN 1 ELSE 0 END) AS IsDriverUser
		,CONVERT(BIT,CASE WHEN g.[PartnerUserId] IS NOT NULL THEN 1 ELSE 0 END) AS IsPartnerUser
		,a.[PasswordExpirationDate]
		,CONVERT(BIT,CASE WHEN a.[PasswordExpirationDate] < GETDATE() THEN 1 ELSE 0 END) AS IsPasswordExpired
		,CASE
			WHEN e.[CustomerUserId] IS NOT NULL THEN i.Logo
			WHEN f.[DriversUserId] IS NOT NULL THEN j.Logo
			WHEN g.[PartnerUserId] IS NOT NULL THEN h.Logo
			ELSE NULL
		 END AS EntityLogo
		,a.[RowVersion]
    FROM [General].[Users] a
		INNER JOIN [dbo].[AspNetUsers] b
			ON a.[AspNetUserId] = b.[Id]
		INNER JOIN [dbo].[AspNetUserRoles] c
			ON c.UserId = a.AspNetUserId
		INNER JOIN [dbo].[AspNetRoles] d
			ON d.[Id] = c.[RoleId]
		LEFT JOIN [General].[CustomerUsers] e
			ON e.[UserId] = a.[UserId]
		LEFT JOIN [General].[DriversUsers] f
			ON f.[UserId] = a.[UserId]
		LEFT JOIN [General].[PartnerUsers] g
			ON g.[UserId] = a.[UserId]
		LEFT JOIN [General].[Partners] h
			ON g.PartnerId = h.PartnerId
		LEFT JOIN [General].[Customers] i
			ON i.CustomerId = e.CustomerId
		LEFT JOIN [General].[Customers] j
			ON j.CustomerId = f.CustomerId
	WHERE a.[IsDeleted] = 0
	  AND (@pUserId IS NULL OR a.[UserId] = @pUserId)
	  AND (@pUserName IS NULL OR b.[UserName] = @pUserName)
	  AND (@pKey IS NULL
				OR a.[Name] like ''%''+@pKey+''%''
				OR b.[Email] like ''%''+@pKey+''%''
				OR b.[PhoneNumber] like ''%''+@pKey+''%''
				OR b.[UserName] like ''%''+@pKey+''%''
				OR e.[Identification] like ''%''+@pKey+''%''
				OR f.[Identification] like ''%''+@pKey+''%''
				OR f.[Code] like ''%''+@pKey+''%''
				OR f.[License] like ''%''+@pKey+''%''
				OR f.[Dallas] like ''%''+@pKey+''%'')
	  AND (@pCustomerId IS NULL
				OR e.[CustomerId] = @pCustomerId)
	  AND (@pCustomerId2 IS NULL
				OR f.[CustomerId] = @pCustomerId2)
	  AND (@pPartnerId IS NULL
				OR g.[PartnerId] = @pPartnerId)
	  AND d.[Name] <> ''CUSTOMER_CHANGEODOMETER''
	ORDER BY [UserId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_SubUnitReference_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_SubUnitReference_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/03/2014
-- Description:	Retrieve IntrackReference information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_SubUnitReference_Retrieve]
(
	@pId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		s.[CostCenterId] [SubUnitId], 
		co.[Code] [CountryCode],
		c.[CustomerId] [CustomerId]
	FROM 
		[General].[VehicleCostCenters] s
		INNER JOIN [General].[VehicleUnits] u ON s.[UnitId] = u.[UnitId]
		INNER JOIN [General].[Customers] c on u.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Countries] co on c.[CountryId] = co.[CountryId]
	WHERE s.[CostCenterId] = @pId
		
    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleGroupsByGeoFence_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleGroupsByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/18/2014
-- Description:	Retrieve VehicleGroupsByGeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleGroupsByGeoFence_Retrieve]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		null [GeoFenceId],
		a.[Name] [Name],
		a.[Description] [Description],
		a.[VehicleGroupId] [VehicleGroupId],
		ROW_NUMBER() OVER (ORDER BY a.[VehicleGroupId]) AS [RowNumber] 
	FROM 
		[General].[VehicleGroups] a
	WHERE
		a.[CustomerId] = @pCustomerId
		AND NOT EXISTS(
			SELECT 
				1 
			FROM 
				[Efficiency].[VehicleGroupsByGeoFence] x
			WHERE 
				x.[VehicleGroupId] = a.[VehicleGroupId] AND 
				x.[GeoFenceId] = @pGeoFenceId
			)
	UNION
	SELECT 
		x.[GeoFenceId] [GeoFenceId],
		a.[Name] [Name],
		a.[Description] [Description],
		a.[VehicleGroupId] [VehicleGroupId],
		ROW_NUMBER() OVER (ORDER BY a.[VehicleGroupId]) AS [RowNumber] 
	FROM 
		[General].[VehicleGroups] a
		INNER JOIN [Efficiency].[VehicleGroupsByGeoFence] x ON a.[VehicleGroupId] = x.[VehicleGroupId]
	WHERE x.[GeoFenceId] = @pGeoFenceId
	
    SET NOCOUNT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleGroupsByGeoFence_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleGroupsByGeoFence_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/18/2014
-- Description:	Insert or Update Vehicle Groups by GeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleGroupsByGeoFence_AddOrEdit]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pXmlData VARCHAR(MAX)					--@pName: VehicleGroup Name
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Efficiency].[VehicleGroupsByGeoFence]
			WHERE [GeoFenceId] = @pGeoFenceId
			
			INSERT INTO [Efficiency].[VehicleGroupsByGeoFence]
					([GeoFenceId]
					,[VehicleGroupId]
					,[InsertDate]
					,[InsertUserId])
			SELECT 
					 @pGeoFenceId
					,Row.col.value(''./@VehicleGroupId'', ''INT'') AS [VehicleId]
					,GETUTCDATE()
					,@pLoggedUserId
			FROM @lxmlData.nodes(''/xmldata/VehicleGroup'') Row(col)
			ORDER BY Row.col.value(''./@Index'', ''INT'')
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleRoutePoints_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleRoutePoints_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	Retrieve RoutesSegments information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleRoutePoints_Retrieve]
(
	 @pRouteId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON

		SELECT 
			[PointId], 
			[RouteId], 
			[PointReference], 
			[Order], 
			[Name], 
			[Time], 
			[Latitude], 
			[Longitude], 
			[StopTime]
		FROM 
			[Efficiency].[RoutesPoints] 
		WHERE
			[RouteId] = @pRouteId
	
    SET NOCOUNT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_PartnerFuel_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PartnerFuel_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Partner Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PartnerFuel_Retrieve]
(
	 @pPartnerFuelId INT = NULL				--@pPartnerFuelId: PK of the table
	,@pPartnerId INT						--@pPartnerId: FK of Partner Id
	,@pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT
		 a.[PartnerFuelId]
		,a.[FuelId]
		,b.[Name] AS [FuelName]
		,a.[PartnerId]
		,a.[LiterPrice]
		,CONVERT(VARCHAR(20),a.[StartDate],103) AS [StartDate]
		,CONVERT(VARCHAR(20),a.[EndDate],103) AS [EndDate]
		,a.[RowVersion]
    FROM [Control].[PartnerFuel] a
		INNER JOIN [Control].[Fuels] b
			ON a.[FuelId] = b.[FuelId]
	WHERE a.[PartnerId] = @pPartnerId
	  AND (@pPartnerFuelId IS NULL OR a.[PartnerFuelId] = @pPartnerFuelId)
	  AND (@pKey IS NULL 
				OR b.[Name] like ''%''+@pKey+''%''
				OR CONVERT(VARCHAR(20),a.[LiterPrice]) like ''%''+@pKey+''%'')
	ORDER BY ISNULL(a.[EndDate], GETUTCDATE()) DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_PartnerFuel_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PartnerFuel_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Delete Partner Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PartnerFuel_Delete]
(
	@pPartnerFuelId INT						--@pPartnerFuelId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Control].[PartnerFuel]
			WHERE [PartnerFuelId] = @pPartnerFuelId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_PartnerFuel_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PartnerFuel_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update Partner Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PartnerFuel_AddOrEdit]
(
	 @pPartnerFuelId INT = NULL				--@pPartnerFuelId: PK of the table
	,@pFuelId INT							--@pFuelId: FK of Fuels
	,@pPartnerId INT						--@pPartnerId: FK of Partners
	,@pLiterPrice DECIMAL(16,2)				--@@pLiterPrice: Liter Price
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pPartnerFuelId IS NULL)
			BEGIN
				UPDATE [Control].[PartnerFuel]
					SET [EndDate] = GETUTCDATE()
				WHERE [PartnerId] = @pPartnerId
				  AND [FuelId] = @pFuelId
				  AND [EndDate] IS NULL
				
				INSERT INTO [Control].[PartnerFuel]
						([FuelId]
						,[PartnerId]
						,[LiterPrice]
						,[StartDate]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pFuelId
						,@pPartnerId
						,@pLiterPrice
						,GETUTCDATE()
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Control].[PartnerFuel]
					SET  [FuelId] = @pFuelId
						,[PartnerId] = @pPartnerId
						,[LiterPrice] = @pLiterPrice				
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [PartnerFuelId] = @pPartnerFuelId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PartnerUsers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerUsers_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo.
-- Create date: 16/01/2015
-- Description:	Retrieve Customer User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PartnerUsers_Retrieve]
(
	  @pUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT
		  a.[PartnerUserId]
		 ,a.[PartnerId]
		 ,CASE  WHEN d.[Name]=''PARTNER_USER'' OR d.[Name]=''INSURANCE_USER'' THEN ''Usuario Regular''
						WHEN d.[Name]=''PARTNER_ADMIN'' OR d.[Name]=''INSURANCE_ADMIN'' THEN ''Administrador''
						ELSE '''' END AS [RoleName]
		,a.[InsertDate]
		,e.CountryId
		,e.[PartnerGroupId]
    FROM [General].[PartnerUsers] a
		INNER JOIN [General].[Users] b
			ON b.[UserId] = a.[UserId]
		INNER JOIN [dbo].[AspNetUserRoles] c
			ON c.[UserId] = b.[AspNetUserId]
		INNER JOIN [dbo].[AspNetRoles] d
			ON d.[Id] = c.RoleId
		INNER JOIN [General].[Partners] e
			ON e.[PartnerId] = a.[PartnerId]
	WHERE a.[UserId] = @pUserId
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_LicenseExpiration_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_LicenseExpiration_Alarm]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Cristian Martínez 
-- Create date: 24/11/2014
-- Description:	Add Alarm for Expiration of Driver  License 
-- ================================================================================================

create PROCEDURE [General].[Sp_LicenseExpiration_Alarm]
	@pAlarmId INT,
	@pUserId INT, 
	@pCustomerId INT, 
	@pAlarmTriggerId INT,
	@pPeriodicityAlarm INT,
	@pPhone VARCHAR(100), 
	@pEmail VARCHAR (500),
	@pRowVersion TIMESTAMP
	
AS
BEGIN 
	DECLARE @lErrorMessage NVARCHAR(4000)
    DECLARE @lErrorSeverity INT
    DECLARE @lErrorState INT
	DECLARE @lLicenseExpiration DATE
	DECLARE	@lValue INT
	DECLARE @lName VARCHAR(250)
	DECLARE @lMessage VARCHAR (700)
	DECLARE @lNextAlarm DATETIME			
	
	BEGIN TRY 
		SELECT @lLicenseExpiration = a.[LicenseExpiration],
			   @lName = b.[Name] 
		FROM [General].[DriversUsers] a
			INNER JOIN [General].[Users] b
				ON a.[UserId] = b.[UserId]
			INNER JOIN [General].[ParametersByCustomer] c
				ON a.[CustomerId] = c.[CustomerId]
		WHERE a.[UserId] = @pUserId
		  AND a.[CustomerId] = @pCustomerId
	      AND a.[LicenseExpiration] < CONVERT(DATE,DATEADD(Hour, CONVERT(INT, c.Value),GETUTCDATE()))
	      AND c.[ResuourceKey] = ''CUSTOMER_UTC''

	
		IF (@lLicenseExpiration IS NOT NULL)
		BEGIN 
			EXEC [General].[Sp_SendAlarm] @pAlarmId = @pAlarmId,
										  @pAlarmTriggerId = @pAlarmTriggerId,
										  @pPeriodicityAlarm = @pPeriodicityAlarm,
										  @pCustomerId = @pCustomerId,
										  @pName =	@lName,
										  @pPhone = @pPhone, 
										  @pEmail = @pEmail,
										  @pRowVersion = @pRowVersion
										  
		END 
	END TRY 
	BEGIN CATCH 
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH	
END 

			
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesSegments_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesSegments_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	Retrieve RoutesSegments information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_RoutesSegments_Retrieve]
(
	 @pRouteId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT [SegmentId]
		,[RouteId]
		,[StartReference]
		,[EndReference]
		,[Poliyline].STAsText() Poliyline
		,[Time]
		,[Distance]
	FROM 
		[Efficiency].[RoutesSegments]
	where [RouteId] = @pRouteId
	
    SET NOCOUNT OFF
END



' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesPoints_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesPoints_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	Retrieve RoutesPoints information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_RoutesPoints_Retrieve]
(
	 @pRouteId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT [PointId]
		,[RouteId]
		,[PointReference]
		,[Order]
		,[Name]
		,CONVERT(varchar(20), [Time]) as [Time]
		,CONVERT(varchar(50), [Latitude]) as [Latitude]
		,CONVERT(varchar(50), [Longitude]) as [Longitude]
	FROM 
		[Efficiency].[RoutesPoints]
	where [RouteId] = @pRouteId
	
    SET NOCOUNT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesDetail_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesDetail_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	AddOrEdit RouteDetail information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_RoutesDetail_AddOrEdit]
(
	 @pRouteId INT,
	 @pLoggedUserId INT,
	 @pXmlPoints XML,
	 @pXmlSegments XML
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			--delete points
			DELETE [Efficiency].[RoutesPoints]
			WHERE [RouteId] = @pRouteId
			
			--delete segments
			DELETE [Efficiency].[RoutesSegments]
			WHERE [RouteId] = @pRouteId
			
			--insert points
			INSERT INTO [Efficiency].[RoutesPoints]
				([RouteId]
				,[PointReference]
				,[Order]
				,[Name]
				,[Time]
				,[Latitude]
				,[Longitude]
				,[InsertDate]
				,[InsertUserId]
				,[ModifyDate]
				,[ModifyUserId]
				,[StopTime])
			SELECT
				@pRouteId as [RouteId],
				COALESCE([Table].[Column].value(''PointReference [1]'', ''int''), 0) as [PointReference],
				COALESCE([Table].[Column].value(''Order [1]'', ''int''), 0) as [Order],				
				COALESCE([Table].[Column].value(''Name [1]'', ''varchar(50)''), '''') as [Name],
				COALESCE([Table].[Column].value(''Time [1]'', ''datetime''), '''') as [Time],
				COALESCE([Table].[Column].value(''Latitude [1]'', ''float''), 0) as [Latitude],
				COALESCE([Table].[Column].value(''Longitude [1]'', ''float''), 0) as [Longitude],
				GETUTCDATE() as [InsertDate],
				@pLoggedUserId as [InsertUserId],
				GETUTCDATE() as [ModifyDate],
				@pLoggedUserId as [ModifyUserId],
				COALESCE([Table].[Column].value(''StopTime [1]'', ''int''), 0) as [StopTime]
			FROM @pXmlPoints.nodes(''/ ArrayOfRoutesPoints / RoutesPoints '') as [Table]([Column])
	
			--insert segments
			INSERT INTO [Efficiency].[RoutesSegments]
				([RouteId]
				,[StartReference]
				,[EndReference]
				,[Poliyline]
				,[InsertDate]
				,[InsertUserId]
				,[ModifyDate]
				,[ModifyUserId]
				,[Time]
				,[Distance])
			SELECT
				@pRouteId as [RouteId],
				COALESCE([Table].[Column].value(''StartReference [1]'', ''int''), 0) as [StartReference],
				COALESCE([Table].[Column].value(''EndReference [1]'', ''int''), 0) as [EndReference],
				geometry::STGeomFromText(COALESCE([Table].[Column].value(''Poliyline [1]'', ''varchar(max)''), ''''), 0) as [Poliyline],
				
				--,(geometry::STGeomFromText(@pPolygon, 0))
				
				GETUTCDATE() as [InsertDate],
				@pLoggedUserId as [InsertUserId],
				GETUTCDATE() as [ModifyDate],
				@pLoggedUserId as [ModifyUserId],
				COALESCE([Table].[Column].value(''Time [1]'', ''int''), 0) as [Time],
				COALESCE([Table].[Column].value(''Distance [1]'', ''float''), 0) as [Distance]
			FROM @pXmlSegments.nodes(''/ ArrayOfRoutesSegments / RoutesSegments '') as [Table]([Column])

			declare @stopTime int 
			declare @time int 
			declare @distance float 
			SET @time = (SELECT SUM([StopTime]) FROM [Efficiency].[RoutesPoints] WHERE [RouteId] = @pRouteId) 
			SET @stopTime = (SELECT sum([Time]) FROM [Efficiency].[RoutesSegments] WHERE [RouteId] = @pRouteId)
			SET @distance = (SELECT SUM([Distance]) FROM [Efficiency].[RoutesSegments] WHERE [RouteId] = @pRouteId)
			SELECT @time + @stopTime
			SELECT @distance
			UPDATE Efficiency.Routes SET [Time] = @stopTime + @time, [Distance] = @distance WHERE RouteId = @pRouteId

            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END



' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceRecordByVehicle_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/16/2015
-- Description:	Insert or Update Vehicle by Route information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicle_AddOrEdit]
(
	@pXmlData VARCHAR(MAX),					
	@pLoggedUserId INT						
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0

            DECLARE @lTotal DECIMAL(16, 2)
            DECLARE @lCatalogId INT
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			INSERT INTO [General].[PreventiveMaintenanceCost]
					([PreventiveMaintenanceCatalogId]
					,[Description]
					,[Cost]
					,[InsertUserId]
					,[InsertDateId])
			SELECT 
					Row.col.value(''./@PreventiveMaintenanceCatalogId'', ''INT'') AS [PreventiveMaintenanceCatalogId]
					,Row.col.value(''./@Description'', ''VARCHAR(500)'') AS [Description]
					,Row.col.value(''./@Cost'', ''DECIMAL(16,2)'') AS [Cost]
					,@pLoggedUserId
					,GETUTCDATE()
			FROM @lxmlData.nodes(''/xmldata/PreventiveMaintenanceCost'') Row(col)
			WHERE Row.col.value(''./@PreventiveMaintenanceCostId'', ''INT'') = 0
			
			UPDATE cost SET 
					cost.Description = Row.col.value(''./@Description'', ''VARCHAR(500)''),
					cost.Cost = Row.col.value(''./@Cost'', ''DECIMAL(16,2)''),
					cost.ModifyUserId = @pLoggedUserId,
					cost.ModifyDateId = GETDATE()
			FROM @lxmlData.nodes(''/xmldata/PreventiveMaintenanceCost'') Row(col)
			INNER JOIN [General].[PreventiveMaintenanceCost] cost ON Row.col.value(''./@PreventiveMaintenanceCostId'', ''INT'') = cost.PreventiveMaintenanceCostId
			WHERE Row.col.value(''./@PreventiveMaintenanceCostId'', ''INT'') <> 0
			
			SELECT @lTotal = SUM(Row.col.value(''./@Cost'', ''DECIMAL(16,2)''))
			FROM @lxmlData.nodes(''/xmldata/PreventiveMaintenanceCost'') Row(col)

			SELECT TOP 1 @lCatalogId = Row.col.value(''./@PreventiveMaintenanceCatalogId'', ''DECIMAL(16,2)'')
			FROM @lxmlData.nodes(''/xmldata/PreventiveMaintenanceCost'') Row(col)

			UPDATE [General].[PreventiveMaintenanceCatalog] 
			SET [Cost] = @lTotal 
			WHERE [PreventiveMaintenanceCatalogId] = @lCatalogId

            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCost_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCost_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Danilo Hidalgo González.
-- Create date: 12/31/2014
-- Description:	Preventive Maintenance Cost information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCost_Retrieve]
	 @pPreventiveMaintenanceCatalogId INT
AS
BEGIN	
	SET NOCOUNT ON;

		SELECT 
			co.[PreventiveMaintenanceCostId], 
			co.[PreventiveMaintenanceCatalogId], 
			co.[Description], 
			co.[Cost], 
			cur.[Symbol]
		FROM 
			[General].[PreventiveMaintenanceCost] co
			INNER JOIN [General].[PreventiveMaintenanceCatalog] ca ON co.[PreventiveMaintenanceCatalogId] = ca.[PreventiveMaintenanceCatalogId]
			INNER JOIN [General].[Customers] cu ON ca.[CustomerId] = cu.[CustomerId]
			INNER JOIN [Control].[Currencies] cur ON cu.[CurrencyId] = cur.[CurrencyId]
		WHERE 
			co.[PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId
		ORDER BY
			co.[Description]


	SET NOCOUNT OFF;
END

' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCost_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCost_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Danilo Hidalgo.
-- Create date: 01/05/2015
-- Description:	Delete Preventive Maintenance Cost information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCost_Delete]
	@pPreventiveMaintenanceCostId INT
AS
BEGIN	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		DELETE [General].[PreventiveMaintenanceCost]
		WHERE [PreventiveMaintenanceCostId] = @pPreventiveMaintenanceCostId
		
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_Points_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Points_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/27/2014
-- Description:	Retrieve Points information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_Points_Retrieve]
(
	 @pCustomerId INT = NULL
	,@pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT 
		p.[Name] [description], 
		p.[Latitude] [lat], 
		p.[Longitude] [lon] 
	FROM 
		[Efficiency].[Routes] r 
		INNER JOIN [Efficiency].[RoutesPoints] p ON r.[RouteId] = p.[RouteId]
	WHERE 
		r.[CustomerId] = @pCustomerId
		
		AND (@pKey IS NULL 
		OR UPPER(p.[Name]) like ''%''+@pKey+''%'')


	ORDER BY p.Name
	
    SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleRouteSegments_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleRouteSegments_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	Retrieve RoutesSegments information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleRouteSegments_Retrieve]
(
	 @pRouteId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT [SegmentId]
		,[RouteId]
		,[StartReference]
		,[EndReference]
		,[Poliyline].STAsText() Poliyline
		,[Time]
		,[Distance]
	FROM 
		[Efficiency].[RoutesSegments]
	where [RouteId] = @pRouteId
	
    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleUnitsByGeoFence_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleUnitsByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/18/2014
-- Description:	Retrieve VehicleGroupsByGeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleUnitsByGeoFence_Retrieve]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		null [GeoFenceId],
		a.[Name] [Name],
		a.[UnitId] [UnitId],
		ROW_NUMBER() OVER (ORDER BY a.[UnitId]) AS [RowNumber] 
	FROM 
		[General].[VehicleUnits] a
	WHERE
		a.[CustomerId] = @pCustomerId
		AND NOT EXISTS(
			SELECT 
				1 
			FROM 
				[Efficiency].[VehicleUnitsByGeoFence] x
			WHERE 
				x.[UnitId] = a.[UnitId] AND 
				x.[GeoFenceId] = @pGeoFenceId
			)
	UNION
	SELECT 
		x.[GeoFenceId] [GeoFenceId],
		a.[Name] [Name],
		a.[UnitId] [UnitId],
		ROW_NUMBER() OVER (ORDER BY a.[UnitId]) AS [RowNumber] 
	FROM 
		[General].[VehicleUnits] a
		INNER JOIN [Efficiency].[VehicleUnitsByGeoFence] x ON a.[UnitId] = x.[UnitId]
	WHERE x.[GeoFenceId] = @pGeoFenceId
	
    SET NOCOUNT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleUnitsByGeoFence_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleUnitsByGeoFence_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/18/2014
-- Description:	Insert or Update Vehicle Groups by GeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleUnitsByGeoFence_AddOrEdit]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pXmlData VARCHAR(MAX)					--@pName: VehicleGroup Name
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Efficiency].[VehicleUnitsByGeoFence]
			WHERE [GeoFenceId] = @pGeoFenceId
			
			INSERT INTO [Efficiency].[VehicleUnitsByGeoFence]
					([GeoFenceId]
					,[UnitId]
					,[InsertDate]
					,[InsertUserId])
			SELECT 
					 @pGeoFenceId
					,Row.col.value(''./@VehicleUnitId'', ''INT'') AS [UnitId]
					,GETUTCDATE()
					,@pLoggedUserId
			FROM @lxmlData.nodes(''/xmldata/VehicleUnit'') Row(col)
			ORDER BY Row.col.value(''./@Index'', ''INT'')
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetRosterInsurance]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetRosterInsurance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 09/02/2015
-- Description:	Retrieve Get Roster Insurance information
-- ================================================================================================
create PROCEDURE [Insurance].[Sp_GetRosterInsurance] 
(	@pPartnerId Int, 
	@pCustomerId int =Null
 )
AS
BEGIN
--Select * from  General.CustomersPartners   where PartnerId = @pParnetId 
Select c.CustomerId , c.Name from General.[CustomersByPartner] cbp INNER JOIN General.Customers c ON cbp.CustomerId=c.CustomerId where PartnerId=@pPartnerId and cbp.CustomerId=ISNULL(@pCustomerId,cbp.CustomerId)
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_FuelsCost_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_FuelsCost_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/03/2014
-- Description:	Retrieve Customer Credit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_FuelsCost_Retrieve]
(
	  @pCustomerId INT						--@pCustomerId: Customer Id
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lExchangeRate DECIMAL(16,8) = 1.0
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[PartnerId] = b.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = b.[CustomerId] AND c.[CurrencyId] = a.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND a.EndDate IS NULL
	
	SELECT
		 b.[FuelId]
		,CONVERT(DECIMAL(16,2),(t.LiterPrice * @lExchangeRate)) AS LiterPrice
    FROM [Control].[Fuels] b
		LEFT JOIN (
				SELECT
					x.[FuelId], x.[LiterPrice] 
				FROM [Control].[PartnerFuel] x
					INNER JOIN [General].[CustomersByPartner] y
						ON x.[PartnerId] = y.[PartnerId]
					INNER JOIN [General].[Customers] z
						ON y.[CustomerId] = z.[CustomerId]
				WHERE z.CustomerId = @pCustomerId
				  AND x.[EndDate] IS NULL)t
			ON b.[FuelId] = t.FuelId
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_FuelsByCredit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_FuelsByCredit_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/03/2014
-- Description:	Retrieve Customer Credit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_FuelsByCredit_Retrieve]
(
	  @pCustomerId INT						--@pCustomerId: Customer Id
	 ,@pYear INT							--@pYear: Year
	 ,@pMonth INT = NULL					--@pMonth: Month
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lExchangeRate DECIMAL(16,8) = 1.0
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[PartnerId] = b.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = b.[CustomerId] AND c.[CurrencyId] = a.[CurrencyId]
	WHERE c.[CustomerId] = @pCustomerId
	  AND a.EndDate IS NULL
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	  
	
	SELECT
		 a.[FuelByCreditId]
		,a.[FuelId]
		,a.[Year]
		,a.[Month]
		,a.[Total]
		,a.[Assigned]
		,a.[Available]
		,b.[Name] AS [FuelName]
		,CONVERT(DECIMAL(16,2),a.[Total]/(t.LiterPrice * @lExchangeRate)) AS Liters
		,CONVERT(DECIMAL(16,2),(t.LiterPrice * @lExchangeRate)) AS LiterPrice
		,@lCurrencySymbol AS [CurrencySymbol]
		,a.[RowVersion]
    FROM [Control].[FuelsByCredit] a
		INNER JOIN [Control].[Fuels] b
			ON a.[FuelId] = b.[FuelId]
		LEFT JOIN (
			SELECT
					x.[FuelId], x.[LiterPrice] 
				FROM [Control].[PartnerFuel] x
					INNER JOIN [General].[CustomersByPartner] y
						ON x.[PartnerId] = y.[PartnerId]
					INNER JOIN [General].[Customers] z
						ON y.[CustomerId] = z.[CustomerId]
						AND z.CustomerId = y.CustomerId
				WHERE z.CustomerId = @pCustomerId
				  AND y.IsDefault = 1
				  AND x.[EndDate] IS NULL)t
			ON b.[FuelId] = t.FuelId
	WHERE a.[CustomerId] = @pCustomerId
	  AND a.[Year] = @pYear
	  AND (@pMonth IS NULL OR a.[Month] = @pMonth)
	  and a.[Total]<>0
	ORDER BY a.[Year], a.[Month] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_FuelsByCredit_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_FuelsByCredit_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update Fuels By Credit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_FuelsByCredit_AddOrEdit]
(
	 @pFuelByCreditId INT = NULL			--@pFuelsByCreditId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Partners
	,@pFuelId INT							--@pFuelId: FK of Fuels
	,@pMonth INT							--@pMonth: Month
	,@pYear INT								--@pYear: Year
	,@pTotal DECIMAL(16,2)					--@pTotal: Total
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pFuelByCreditId IS NULL)
			BEGIN				
				
				INSERT INTO [Control].[FuelsByCredit]
						([CustomerId]
						,[FuelId]
						,[Month]
						,[Year]
						,[Total]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pFuelId
						,@pMonth
						,@pYear
						,@pTotal
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Control].[FuelsByCredit]
					SET  [CustomerId] = @pCustomerId
						,[FuelId] = @pFuelId
						,[Month] = @pMonth
						,[Year] = @pYear
						,[Total] = @pTotal
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [FuelByCreditId] = @pFuelByCreditId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_FleetCard_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_FleetCard_Alarm]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/28/2015
-- Description:	Stored Procedure that execute the alarm Add Alarm for Expiration of Fleet Card
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_FleetCard_Alarm]
AS
BEGIN 

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
		DECLARE @RowNum INT
		DECLARE @CreditCardId INT
		DECLARE @CreditCardNumber VARCHAR(500)
		DECLARE @ExpirationMonth INT
		DECLARE @ExpirationYear INT
		DECLARE @Name VARCHAR(500)
		DECLARE @Message VARCHAR(500)

		DECLARE @tCards TABLE(
			[Id] INT IDENTITY(1,1) NOT NULL ,
			[CreditCardId] INT,
			[CreditCardNumber] VARCHAR(500),
			[ExpirationMonth] INT,
			[ExpirationYear] INT,
			[Email] VARCHAR(500))
		
		INSERT INTO @tCards(
			[CreditCardId],
			[CreditCardNumber],
			[ExpirationMonth],
			[ExpirationYear],
			[Email])
		SELECT 
			a.[CreditCardId],
			a.[CreditCardNumber],
			a.[ExpirationMonth],
			a.[ExpirationYear],
			g.[Email]
		FROM [Control].[CreditCard] a
			INNER JOIN [General].[Status] b ON a.[StatusId] = b.[StatusId]
			INNER JOIN [General].[CustomerUsers] c ON a.[CustomerId] = c.[CustomerId]
			INNER JOIN [General].[Users] d ON c.[UserId] = d.[UserId]
			INNER JOIN [AspNetUsers] g ON d.[AspNetUserId] = g.[Id]
			INNER JOIN [AspNetUserRoles] e ON g.[Id] = e.[UserId]
			INNER JOIN [AspNetRoles] f ON e.[RoleId] = f.[Id]
			INNER JOIN [General].[ParametersByCustomer] h ON a.[CustomerId] = h.[CustomerId]
		WHERE 
			b.[IsActive] = 1 AND 
			b.[Code] NOT IN (''CUSTOMER_CREDIT_CARD_REJECTED_KEY'',''CUSTOMER_CREDIT_CARD_CANCELLED_KEY'') AND 
			ISNULL(a.[ExpirationAlarmSent], 0) <> 1 AND 
			DATEADD(MONTH, 1, DATEADD(HOUR, CONVERT(INT, h.Value), GETUTCDATE())) >= CONVERT(varchar(2), a.[ExpirationMonth]) + ''-01-'' + CONVERT(varchar(6), a.[ExpirationYear]) AND 
			d.[IsActive] = 1 AND 
			d.[IsDeleted] = 0 AND
			d.[IsLockedOut] = 0 AND
			f.[Name] = ''CUSTOMER_ADMIN'' AND 
			h.ResuourceKey = ''CUSTOMER_UTC''

		SELECT @Message = a.[Message] FROM [General].[Values] a 
			INNER JOIN [General].[Types] b ON a.[TypeId] = b.[TypeId]
		WHERE 
			b.[Code] = ''CUSTOMER_ALARM_TRIGGER_FLEETCARD_EXPIRATION''

		Select @RowNum = Count(*) From @tCards

		WHILE @RowNum > 0
		BEGIN
			DECLARE @lMessage VARCHAR(500) = @Message
			DECLARE @lMonth VARCHAR(10)

			SELECT 
				@CreditCardId = [CreditCardId], 
				@CreditCardNumber = [CreditCardNumber], 
				@ExpirationMonth = [ExpirationMonth], 
				@ExpirationYear = [ExpirationYear], 
				@Name = [Email]
			FROM @tCards
			WHERE Id = @RowNum
		
			IF @ExpirationMonth = 1 SET @lMonth = ''Enero''
			ELSE IF @ExpirationMonth = 2 SET @lMonth = ''Febrero''
			ELSE IF @ExpirationMonth = 3 SET @lMonth = ''Marzo''
			ELSE IF @ExpirationMonth = 4 SET @lMonth = ''Abril''
			ELSE IF @ExpirationMonth = 5 SET @lMonth = ''Mayo''
			ELSE IF @ExpirationMonth = 6 SET @lMonth = ''Junio''
			ELSE IF @ExpirationMonth = 7 SET @lMonth = ''Julio''
			ELSE IF @ExpirationMonth = 8 SET @lMonth = ''Agosto''
			ELSE IF @ExpirationMonth = 9 SET @lMonth = ''Setiembre''
			ELSE IF @ExpirationMonth = 10 SET @lMonth = ''Octubre''
			ELSE IF @ExpirationMonth = 11 SET @lMonth = ''Noviembre''
			ELSE IF @ExpirationMonth = 12 SET @lMonth = ''Diciembre''
		
			SET @lMessage = REPLACE(@lMessage, ''%ExpirationMonth%'', @lMonth)
			SET @lMessage = REPLACE(@lMessage, ''%ExpirationYear%'', @ExpirationYear)
			
			INSERT INTO [General].[EmailFleetCardAlarms]
				([To], 
				[Subject], 
				[Message], 
				[Sent],
				[CreditCardNumber], 
				[InsertDate])
			VALUES
				(@Name,
				''Alarma de vencimiento de tarjeta'',
				@lMessage,
				0,
				@CreditCardNumber,
				GETUTCDATE())

			UPDATE [Control].[CreditCard] 
			SET [ExpirationAlarmSent] = 1 
			WHERE [CreditCardId] = @CreditCardId

			SET @RowNum = @RowNum - 1
		END 

	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF

END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Customers_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 26/01/2014
-- Description:	Insert or Update Customer information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Customers_AddOrEdit]
(
	 @pCustomerId INT = NULL
	,@pName VARCHAR(250)
	,@pCurrencyId INT
	,@pLogo VARCHAR(MAX) = NULL
	,@pAccountNumber VARCHAR(50)=null
	,@pIsActive BIT
	,@pUnitOfCapacityId INT
	,@pIssueForId INT=null
	,@pCreditCardType CHAR(1)=null
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
	,@pPartnerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            DECLARE @pCountryId INT =  (SELECT 
											[CountryId]
										FROM [General].[Partners] p 
										WHERE p.[PartnerId] = @pPartnerId)
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCustomerId IS NULL)
			BEGIN
			
				INSERT INTO [General].[Customers]
						([Name]
						,[CurrencyId]
						,[Logo]
						,[IsActive]
						,[UnitOfCapacityId]
						,[IssueForId]
						,[CreditCardType]
						,[InsertDate]
						,[InsertUserId]
						,[CountryId]
						,[AccountNumber]
						)
				VALUES	(@pName
						,@pCurrencyId
						,@pLogo
						,@pIsActive
						,@pUnitOfCapacityId
						,@pIssueForId
						,@pCreditCardType
						,GETUTCDATE()
						,@pLoggedUserId
						,@pCountryId
						,@pAccountNumber
						)
						
				SET @pCustomerId = SCOPE_IDENTITY()
				INSERT INTO General.CustomersByPartner(CustomerId,PartnerId,IsDefault)VALUES(@pCustomerId,@pPartnerId,1)
				
			END
			ELSE
			BEGIN
				UPDATE [General].[Customers]
					SET  [Name] = @pName
						,[CurrencyId] = @pCurrencyId
						,[Logo] = @pLogo
						,[IsActive] = @pIsActive
						,[UnitOfCapacityId] = @pUnitOfCapacityId
						,[IssueForId] = @pIssueForId
						,[AccountNumber] = @pAccountNumber
						,[CreditCardType] = @pCreditCardType
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId						
				WHERE [CustomerId] = @pCustomerId
				  AND [RowVersion] = @pRowVersion
				  
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	
	SELECT @pCustomerId
	
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomersByPartners_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByPartners_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 10/02/2015
-- Description:	Retrieve Customers By Partner
-- ================================================================================================
create PROCEDURE [General].[Sp_CustomersByPartners_Retrieve] 
( @pPartnerId Int 
 )
AS
BEGIN

	Select DISTINCT  a.[CustomerId]
		,a.[Name] AS [EncryptedName]
		,a.[CurrencyId]
		,b.[Name] AS [CurrencyName]
		,a.[Logo]
		,a.[IsActive]
		,a.[AccountNumber]
		,a.[UnitOfCapacityId]
		,d.[Name] AS [UnitOfCapacityName]
		,a.[IssueForId]
		,c.[Name] AS IssueForStr
		,a.[CreditCardType]
		,CASE a.[CreditCardType] WHEN ''C'' THEN ''Crédito'' WHEN ''D'' THEN ''Débito'' ELSE '''' END AS CreditCardTypeStr
		,b.[Symbol] AS [CurrencySymbol]
		,a.[CountryId]
		,e.[Name] AS [CountryName]
		,a.[InsertDate]
		,a.[RowVersion]
		,cbp.IsDefault
		
       from General.Customers a LEFT JOIN General.[CustomersByPartner] cbp  ON cbp.CustomerId=a.CustomerId
       INNER JOIN [Control].[Currencies] b
			ON a.[CurrencyId] = b.[CurrencyId]
		LEFT JOIN [General].[Types] c
			ON c.[TypeId] = a.[IssueForId]
		INNER JOIN [General].[Types] d
			ON d.[TypeId] = a.[UnitOfCapacityId]
		INNER JOIN [General].[Countries] e
			ON a.[CountryId] = e.[CountryId]

        where PartnerId=@pPartnerId and a.IsActive=1 and a.IsDeleted=0
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomersByPartners_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByPartners_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 11/02/2015
-- Description:	Deletes Customers By Partner
-- ================================================================================================
create PROCEDURE [General].[Sp_CustomersByPartners_Delete] 
( @pPartnerId Int 
 )
AS
BEGIN

	DELETE FROM [General].CustomersByPartner
    where PartnerId=@pPartnerId and IsDefault is null
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomersByPartners_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByPartners_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo
-- Create date: 10/02/2015
-- Description:	Insert or Update Customer By Partner information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomersByPartners_AddOrEdit]
(
	 @pCustomerId INT						
	,@pPartnerId INT					
	,@pLoggedUserId INT						
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
       
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			INSERT INTO [General].[CustomersByPartner]
					(CustomerId
					,PartnerId
					,[InsertDate]
					,[InsertUserId])
					values
					(@pCustomerId,
					 @pPartnerId,
					 GETUTCDATE(),
					 @pLoggedUserId)
			
		
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CustomersByCountry_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByCountry_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo Gonzñalez
-- Create date: 19/01/2014
-- Description:	Retrieve Customers By Country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomersByCountry_Retrieve]
(
	@pCountryId INT = NULL, 
	@pPartnerId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT
		 a.[CustomerId]
		,a.[Name] AS [EncryptedName]
		,a.[CurrencyId]
		,b.[Name] AS [CurrencyName]
		,a.[Logo]
		,a.[IsActive]
		,a.[UnitOfCapacityId]
		,a.[IssueForId]
		,c.[Name] AS IssueForStr
		,a.[CreditCardType]
		,CASE a.[CreditCardType] WHEN ''C'' THEN ''Crédito'' WHEN ''D'' THEN ''Debito'' ELSE '''' END AS CreditCardTypeStr
		,b.[Symbol] AS [CurrencySymbol]
		,a.[CountryId]
		,d.[Name] AS UnitOfCapacityName
		,a.[InsertDate]
		,a.[RowVersion]
		--,a.InsurancePartnerId
    FROM [General].[Customers] a
		INNER JOIN [Control].[Currencies] b
			ON a.[CurrencyId] = b.[CurrencyId]
		INNER JOIN [General].[Types] c
			ON c.[TypeId] = a.[IssueForId]		
		INNER JOIN [General].[Types] d
			ON d.[TypeId] = a.[UnitOfCapacityId]
		INNER JOIN [General].[CustomersByPartner] e
			ON e.[CustomerId] = a.[CustomerId]
	WHERE a.[IsDeleted] = 0
	  AND a.[CountryId] = @pCountryId
	  AND e.[PartnerId] = @pPartnerId
	 
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Customers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 20/01/2014
-- Description:	Retrieve Customer information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Customers_Retrieve]
(
	@pCustomerId INT = NULL,
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL,
	 @pPartnerId INT=null
)
AS
BEGIN

	SET NOCOUNT ON
	

	SELECT distinct
		 a.[CustomerId]
		,a.[Name] AS [EncryptedName]
		,a.[CurrencyId]
		,b.[Name] AS [CurrencyName]
		,a.[Logo]
		,a.[IsActive]
		,a.[AccountNumber]
		,a.[UnitOfCapacityId]
		,d.[Name] AS [UnitOfCapacityName]
		,a.[IssueForId]
		,c.[Name] AS IssueForStr
		,a.[CreditCardType]
		,CASE a.[CreditCardType] WHEN ''C'' THEN ''Crédito'' WHEN ''D'' THEN ''Débito'' ELSE '''' END AS CreditCardTypeStr
		,b.[Symbol] AS [CurrencySymbol]
		,a.[CountryId]
		,e.[Name] AS [CountryName]
		,a.[InsertDate]
		,a.[RowVersion]
		,cbp.IsDefault
		--,a.InsurancePartnerId
    FROM [General].[Customers] a
		INNER JOIN [Control].[Currencies] b
			ON a.[CurrencyId] = b.[CurrencyId]
		LEFT JOIN [General].[Types] c
			ON c.[TypeId] = a.[IssueForId]
		INNER JOIN [General].[Types] d
			ON d.[TypeId] = a.[UnitOfCapacityId]
		INNER JOIN [General].[Countries] e
			ON a.[CountryId] = e.[CountryId]
		INNER JOIN 	General.CustomersByPartner cbp
			ON a.CustomerId=cbp.CustomerId
	WHERE a.[IsDeleted]=CAST(0 as bit)
	  and a.IsActive=1
	  AND (@pCustomerId IS NULL OR a.[CustomerId] = @pCustomerId)
	  AND (@pCountryId IS NULL OR a.[CountryId] = @pCountryId)
	  AND (@pPartnerId IS NULL OR cbp.PartnerId=@pPartnerId)
	  /*AND (@pKey IS NULL 
				--OR a.[Name] like ''%''+@pKey+''%'' -- Encrypted --> Linq
				OR b.[Name] like ''%''+@pKey+''%''
				)*/
	ORDER BY [CustomerId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Customers_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andres Oviedo Brenes.
-- Create date: 26/01/2014
-- Description:	Delete Customer information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Customers_Delete]
(
	 @pCustomerId INT,
	 @pPartnerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE FROM General.CustomersByPartner
							WHERE [CustomerId] = @pCustomerId AND PartnerId=@pPartnerId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CurrenciesByCustomer_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrenciesByCustomer_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martínez Hernández 
-- Create date: 09/Oct/2014
-- Description:	Retrieve country by customer information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CurrenciesByCustomer_Retrieve]
(
	@pCustomerId INT
)
AS 
BEGIN 
	SET NOCOUNT ON 
	SELECT 
		 a.[CurrencyId]
		,a.[Name]
		,a.[Symbol]
	FROM [Control].[Currencies] a
		INNER JOIN [Control].[PartnerCurrency] b
			ON a.CurrencyId = b.CurrencyId
		INNER JOIN [General].[CustomersByPartner] c
			ON b.[PartnerId] = c.[PartnerId]
		INNER JOIN [General].[Customers] d
			ON d.[CustomerId] = d.[CustomerId]
	WHERE d.[CustomerId] = @pCustomerId
	GROUP BY a.[CurrencyId], a.[Name], a.[Symbol]
	
	SET NOCOUNT OFF
END ' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardStatusRequested_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardStatusRequested_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CreditCard information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardStatusRequested_Retrieve]
(
	 @pCreditCardId INT = NULL				--@pCreditCardId:Credit Card Id, PK of the table
	,@pKey VARCHAR(800) = NULL				--@pKey: Key to perform search operations
)
AS
BEGIN
	
	SET NOCOUNT ON
			
	SELECT
		 a.[CreditCardId]
		,a.[CustomerId]
		,a.[CreditCardNumber]
		,a.[ExpirationYear]
		,a.[ExpirationMonth]
		,a.[CreditLimit]
		,a.[CreditAvailable]
		,a.[CreditExtra]
		,a.[StatusId]
		,b.[RowOrder] AS [Step]
		,b.[Name] AS [StatusName]
		,d.[Symbol] AS [CurrencySymbol]
		,a.[RowVersion]
    FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
	WHERE (@pCreditCardId IS NULL OR a.[CreditCardId] = @pCreditCardId)
	  AND (@pKey IS NULL 
				--OR a.[Name] like ''%''+@pKey+''%''
				--OR b.[Name] like ''%''+@pKey+''%''
				)
	  AND b.Code in (''REQUESTED_STATUS_KEY'',''APPROVED_STATUS_KEY'')
	ORDER BY [CreditCardId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardsReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardsReport_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/20/2015
-- Description:	Retrieve Credit Cards Report information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardsReport_Retrieve]
(
	@pCustomerId INT = NULL,		--@pCustomerId: CustomerId
	@pCountryId INT = NULL,			--@pCountryId: CountryId
	@pYear INT = NULL,				--@pYear: Year
	@pMonth INT = NULL,				--@pMonth: Month
	@pStartDate DATETIME = NULL,	--@pStartDate: Start Date
	@pEndDate DATETIME = NULL		--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @pIssueForId INT

		SELECT 
			a.[CreditCardId],
			a.[CreditCardNumber],
			c.[Name] [ApplicantName],
			d.[Name] [StatusName],
			b.[InsertDate] [RequestDate],
			e.[InsertDate] [DeliveryDate]
		FROM 
			[Control].[CreditCard] a
			INNER JOIN [Control].[CardRequest] b ON a.[CardRequestId] = b.[CardRequestId]
			INNER JOIN [General].[Users] c ON b.[InsertUserId] = c.[UserId]
			INNER JOIN [General].[Status] d ON a.[StatusId] = d.[StatusId]
			LEFT JOIN [Control].[CreditCardHx] e ON a.[CreditCardId] = e.[CreditCardId] AND e.[StatusId] = 5
			INNER JOIN [General].[Customers] f ON a.[CustomerId] = f.[CustomerId]			
		WHERE 
			a.[CustomerId] = @pCustomerId AND
			f.[CountryId] = @pCountryId AND
				((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
				AND DATEPART(m,b.[InsertDate]) = @pMonth
				AND DATEPART(yyyy,b.[InsertDate]) = @pYear) OR
				(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
				AND b.[InsertDate] BETWEEN @pStartDate AND @pEndDate))
		ORDER BY
			b.[InsertDate]
	
    SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardNumbers_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardNumbers_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 11/14/2014
-- Description:	Retrieve Credit Card Numbers information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardNumbers_Retrieve]
(
	 @pCreditCardNumberId INT				--@pCreditCardNumberId: Credit Card Number Id
	,@pCreditCardType CHAR(1)				--@pCreditCardType: Credit Card Type ''C'' Or ''D''
	,@pPartnerId INT						--@pPartnerId: Partner Id
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @lCount INT
	
	SELECT
		@lCount = COUNT(1)
	FROM [Control].[CreditCardNumbers]
	WHERE [CreditCardNumberId] = @pCreditCardNumberId
	  AND [CreditCardType] = @pCreditCardType
	  AND [IsUsed] = 0
	  AND [IsReserved] = 0
	  --AND [PartnerId] = @pPartnerId
	
	IF(@lCount = 0)
	BEGIN
		SELECT 0
	END ELSE
	IF(@lCount = 1)
	BEGIN
		UPDATE [Control].[CreditCardNumbers]
			SET [IsReserved] = 1
		WHERE [CreditCardNumberId] = @pCreditCardNumberId
		
		SELECT @pCreditCardNumberId
	END
	
    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardNumber_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardNumber_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 05/12/2015
-- Description:	Retrieve CreditCard Transactions through the VPOS Process
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardNumber_Retrieve]
(
	 @pCreditCardId INT						--@pCreditCardId: Customer Id
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
			
	SELECT
		CreditCardNumber
	FROM [Control].[CreditCard]
	WHERE [CreditCardId] = @pCreditCardId
				
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CustomerCardRequest_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerCardRequest_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/15/2014
-- Description:	Retrieve Customer Card Request information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CustomerCardRequest_Retrieve]
(
	 @pCustomerId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT	 a.[CardRequestId]
			,a.[CustomerId]
			,a.[PlateId]
			,a.[DriverName]
			,a.[DriverIdentification]
			,a.[AddressLine1]
			,a.[AddressLine2]
			,a.[PaymentReference]
			,a.[AuthorizedPerson]
			,a.[ContactPhone]
			,a.[EstimatedDelivery]
			
			,a.[RowVersion]
	FROM [Control].[CardRequest] a
		LEFT JOIN [Control].[CreditCard] b
			 ON a.[CardRequestId] = b.[CardRequestId]
			AND a.[CustomerId] = b.[CustomerId]
		LEFT JOIN [General].[Status] c
			ON c.[StatusId] = b.[StatusId]
	WHERE a.[CustomerId] = @pCustomerId
	ORDER BY [CustomerId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Counties_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Counties_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/17/2014
-- Description:	Retrieve Counties information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Counties_Retrieve]
(
	 @pCountyId INT = NULL,
	 @pStateId INT = NULL,
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[CountyId]
		,a.[Name]
		,a.[StateId]
		,a.[Code]
		,a.[Latitude]
		,a.[Longitude]
		,b.[CountryId]
		,a.[RowVersion]
    FROM [General].[Counties] a
		INNER JOIN [General].[States] b
			ON a.[StateId] = b.[StateId]
	WHERE (@pCountyId IS NULL OR a.[CountyId] = @pCountyId)
	  AND (@pStateId IS NULL OR a.[StateId] = @pStateId)
	  AND (@pCountryId IS NULL OR b.[CountryId] = @pCountryId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
				OR a.[Code] like ''%''+@pKey+''%'')
	ORDER BY a.[Code]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_Delete]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Delete CreditCard information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCard_Delete]
(
	 @pCreditCardId INT						--@pCreditCardId:Credit Card Id, PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Control].[CreditCard]
			WHERE [CreditCardId] = @pCreditCardId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_CreditAvailable_Edit]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_CreditAvailable_Edit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/11/2014
-- Description:	Update CreditCard CreditAvailable amount
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCard_CreditAvailable_Edit]
(
	 @pCreditCardId INT						--@pCreditCardId:Credit Card Id, PK of the table
	,@pTransactionAmount NUMERIC(16,2)		--@pTransactionAmount: Transaction Amount
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
				  
			UPDATE [Control].[CreditCard]
				SET  [CreditAvailable] = [CreditAvailable] - @pTransactionAmount
			WHERE [CreditCardId] = @pCreditCardId
			  
			SET @lRowCount = @@ROWCOUNT
            				            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_Balance_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_Balance_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CreditCard Balance information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCard_Balance_Retrieve]
(
	  @pCreditCardNumber NVARCHAR(520)		--@pCustomerId: Customer Id
	 ,@pExpirationYear INT = NULL			--@pExpirationYear: Expiration Year
	 ,@pExpirationMonth INT	= NULL			--@pExpirationMonth: Expiration Month
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		a.[CreditAvailable] + a.[CreditExtra] AS CreditAvailable
    FROM [Control].[CreditCard] a
	WHERE a.[CreditCardNumber] = @pCreditCardNumber
	  AND (@pExpirationYear IS NULL OR a.[ExpirationYear] = @pExpirationYear)
	  AND (@pExpirationMonth IS NULL OR a.[ExpirationMonth] = @pExpirationMonth)
	  
    SET NOCOUNT OFF
END
 ' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Alarm_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Alarm_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo B.
-- Create date: 13/01/2015
-- Description: Modification of	Retrieve Alarm information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Alarm_Retrieve]
(
	@pAlarmId INT = NULL, 
	@pCustomerId INT=Null,
	@pAlarmTriggerId INT=Null, 
	@pEntityTypeId INT=Null, 
	@pEntityId INT = NULL
)
AS 
BEGIN 
		SET NOCOUNT ON
		
		SELECT 
			 a.[AlarmId]
			,a.[Phone]
			,a.[Email]
			,a.[AlarmTriggerId]
			,(Select Name FROM [General].[Types] WHERE a.[AlarmTriggerId]=TypeId) AS AlarmType
			,a.[EntityTypeId]
			,case [EntityTypeId] WHEN 400 THEN (Select  PlateId FROM [General].Vehicles WHERE VehicleId=a.[EntityId])
			  WHEN 401 THEN (Select Name FROM General.DriversUsers du INNER JOIN General.Users u ON du.UserId=u.UserId WHERE du.DriversUserId=a.[EntityId])
			  WHEN 402 THEN (Select Name FROM General.VehicleGroups WHERE VehicleGroupId=a.[EntityId])  
			  WHEN 403 THEN (Select Name FROM General.VehicleCostCenters WHERE CostCenterId=a.[EntityId])  
			  WHEN 404 THEN (Select Name FROM General.VehicleUnits WHERE UnitId=a.[EntityId]) 
			 end  as Entity
			 ,cast((case [EntityTypeId]
			  WHEN 401 THEN 1
			  else 0
			  end) as bit)as IsEncrypted
			,(Select Name FROM [General].[Types] WHERE a.[EntityTypeId]=TypeId) as EntityType
			,a.[EntityId]
			,a.[Active]
			,(Select Name FROM [General].[Types] WHERE a.[PeriodicityTypeId]=TypeId) AS Periodicity
			,a.[RowVersion]
			,a.NextAlarm
		FROM [General].[Alarms] a
		WHERE (@pAlarmId IS NULL OR a.[AlarmId] = @pAlarmId)
		  AND (@pEntityId IS NULL OR [a].[EntityId]  = @pEntityId)
		  AND [a].[CustomerId] = isnull(@pCustomerId,a.[CustomerId])
		  AND [a].[AlarmTriggerId] = isnull(@pAlarmTriggerId,a.AlarmTriggerId) 
		  AND [a].[EntityTypeId] =isnull(@pEntityTypeId,a.EntityTypeId)  
		ORDER BY [a].[AlarmId] DESC
		SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_AdminPartnerUsersByCountry_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AdminPartnerUsersByCountry_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo González.
-- Create date: 01/19/2015
-- Description:	Retrieve country by user information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_AdminPartnerUsersByCountry_Retrieve]
(
	 @pCountryId INT = NULL,
	 @pPartnerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		d.UserId,
		d.Name EncryptedUserName
	FROM General.Countries a
		INNER JOIN General.PartnerUsersByCountry b ON a.CountryId = b.CountryId
		INNER JOIN General.PartnerUsers c ON b.PartnerUserId = c.PartnerUserId
		INNER JOIN General.Users d ON c.UserId = d.UserId
	WHERE a.CountryId = @pCountryId AND 
		c.PartnerId = @pPartnerId AND
		d.IsDeleted = 0
	ORDER BY
		d.Name
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CAIndicators_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CAIndicators_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa H.
-- Create date: 11/14/2014
-- Description:	Retrieve Indicators for Customer Admin Landing Page
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CAIndicators_Retrieve]
(
	@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		(SELECT count(1) FROM Efficiency.Routes WHERE CustomerId = @pCustomerId) AS Routes, 
		(SELECT COUNT(1) FROM General.Vehicles WHERE CustomerId = @pCustomerId) AS Vehicles, 
		(SELECT COUNT(1) FROM General.DriversUsers D INNER JOIN General.Users U ON D.UserID = U.UserId WHERE D.CustomerId = @pCustomerId AND U.IsDeleted = 0) AS Drivers, 
		(SELECT COUNT(1) FROM Control.CreditCard WHERE CustomerId = @pCustomerId AND StatusId = 7) AS CreditCards
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_AlarmsByVehicles_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AlarmsByVehicles_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ==
-- ================================================================================================
-- Author:		Andrés Oviedo B.
-- Create date: 14/01/2015
-- Description: Retrieve Alarm by Vehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_AlarmsByVehicles_Retrieve]
(
	@pAlarmId INT = NULL, 
	@pCustomerId INT=Null,
	@pAlarmTriggerId INT=Null, 
	@pEntityTypeId INT=Null, 
	@pEntityId INT = NULL,
	@pStartDate datetime,
	@pEndDate datetime
)
AS 
BEGIN 
		SET NOCOUNT ON
		
		SELECT 
			 a.[AlarmId]
			,a.[Phone]
			,a.[Email]
			,a.[AlarmTriggerId]
			,(Select Name FROM [General].[Types] WHERE a.[AlarmTriggerId]=TypeId) AS AlarmType
			,a.[EntityTypeId]
			,case [EntityTypeId] WHEN 400 THEN (Select  PlateId FROM [General].Vehicles WHERE VehicleId=a.[EntityId])
			  WHEN 401 THEN (Select Name FROM General.DriversUsers du INNER JOIN General.Users u ON du.UserId=u.UserId WHERE du.DriversUserId=a.[EntityId])
			  WHEN 402 THEN (Select Name FROM General.VehicleGroups WHERE VehicleGroupId=a.[EntityId])  
			  WHEN 403 THEN (Select Name FROM General.VehicleCostCenters WHERE CostCenterId=a.[EntityId])  
			  WHEN 404 THEN (Select Name FROM General.VehicleUnits WHERE UnitId=a.[EntityId]) 
			 end  as Entity
			 ,cast((case [EntityTypeId]
			  WHEN 401 THEN 1
			  else 0
			  end) as bit)as IsEncrypted
			,(Select Name FROM [General].[Types] WHERE a.[EntityTypeId]=TypeId) as EntityType
			,a.[EntityId]
			,a.[Active]
			,(Select Name FROM [General].[Types] WHERE a.[PeriodicityTypeId]=TypeId) AS Periodicity
			,a.[RowVersion]
			,a.NextAlarm
		FROM [General].[Alarms] a
		WHERE (@pAlarmId IS NULL OR a.[AlarmId] = @pAlarmId)
		  AND (@pEntityId IS NULL OR [a].[EntityId]  = @pEntityId)
		  AND [a].[CustomerId] = isnull(@pCustomerId,a.[CustomerId])
		  AND [a].[AlarmTriggerId] = isnull(@pAlarmTriggerId,a.AlarmTriggerId) 
		  AND [a].[EntityTypeId] =isnull(@pEntityTypeId,a.EntityTypeId)
		  AND a.NextAlarm is not null  
		  and a.SentDate between @pStartDate and @pEndDate
		ORDER BY [a].[AlarmId] DESC
		SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_CountriesByUser_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CountriesByUser_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo González.
-- Create date: 01/19/2015
-- Description:	Retrieve country by user information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CountriesByUser_Retrieve]
(
	 @pLoggedUserId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		a.[CountryId],
		a.[Name]
	FROM [General].[Countries] a
		INNER JOIN [General].[PartnerUsersByCountry] b ON a.[CountryId] = b.[CountryId]
		INNER JOIN [General].[PartnerUsers] c ON b.[PartnerUserId] = c.[PartnerUserId]
		INNER JOIN [General].[Users] d ON c.[UserId] = d.[UserId]
	WHERE c.[UserId] = @pLoggedUserId 
	ORDER BY
		a.[Name]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Cities_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Cities_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/17/2014
-- Description:	Retrieve Cities information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Cities_Retrieve]
(
	 @pCityId INT = NULL,
	 @pCountyId INT = NULL,
	 @pStateId INT = NULL,
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[CityId]
		,a.[Name]
		,a.[CountyId]
		,a.[Code]
		,a.[Latitude]
		,a.[Longitude]
		,b.[StateId]
		,c.[CountryId]
		,a.[RowVersion]
    FROM [General].[Cities] a
		INNER JOIN [General].[Counties] b
			ON a.[CountyId] = b.[CountyId]
		INNER JOIN [General].[States] c
			ON b.[StateId] = c.[StateId]
	WHERE (@pCityId IS NULL OR a.[CityId] = @pCityId)
	  AND (@pCountyId IS NULL OR a.[CountyId] = @pCountyId)
	  AND (@pStateId IS NULL OR b.[StateId] = @pStateId)
	  AND (@pCountryId IS NULL OR c.[CountryId] = @pCountryId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
				OR a.[Code] like ''%''+@pKey+''%'')
	ORDER BY a.[Code]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CardRequest_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CardRequest_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 09/01/2015
-- Description:	Retrieve CardRequest information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CardRequest_Retrieve]
(
	 @pCardRequestId INT = NULL
	,@pCustomerId INT
	,@pPendingOnly BIT = 0
	,@pKey varchar(max)=null
)
AS
BEGIN
	SET NOCOUNT ON
		
	SELECT 
		 a.[CardRequestId]
		,a.[CustomerId]
		,a.[PlateId]
		,a.[DriverName]
		,a.[DriverIdentification]
		,a.[AddressLine1]
		,a.[AddressLine2]
		,a.[PaymentReference]
		,a.[AuthorizedPerson]
		,a.[ContactPhone]
		,a.[EstimatedDelivery]
		,b.[StatusId]
		,CASE WHEN b.[StatusId] IS NULL 
				THEN (SELECT x.[Name]
						FROM [General].[Status] x
					 WHERE x.StatusId = 0)
			ELSE c.[Name] END AS [StatusName]
		,e.[IssueForId]
		,e.[CountryId]
		,a.[StateId]		
		,f.[Name] AS [StateName]
		,a.[CountyId]
		,g.[Name] AS [CountyName]
		,a.[CityId]
		,h.[Name] AS [CityName]
		,a.[DeliveryState]
		,a.[DeliveryCounty]
		,a.[DeliveryCity]
		,a.[RowVersion]
	FROM [Control].[CardRequest] a
		LEFT JOIN [Control].[CreditCard] b
			ON a.[CardRequestId] = b.[CardRequestId]
		   AND a.[CustomerId] = b.[CustomerId]
		LEFT JOIN [General].[Status] c
			ON c.[StatusId] = b.[StatusId]
		INNER JOIN [General].[Customers] e
			ON a.[CustomerId] = e.[CustomerId]
		LEFT JOIN [General].[States] f
			ON a.[StateId] = f.[StateId]
		LEFT JOIN [General].[Counties] g
			ON a.[CountyId] = g.[CountyId]
		LEFT JOIN [General].[Cities] h
			ON a.[CityId] = h.[CityId]		
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pCardRequestId IS NULL OR a.[CardRequestId] = @pCardRequestId)
	  AND (@pPendingOnly = 0 OR b.[CreditCardId] IS NULL)
	  AND (@pKey IS NULL
				OR a.[PlateId] like ''%''+@pKey+''%''
				OR a.[AuthorizedPerson] like ''%''+@pKey+''%''
				OR a.[ContactPhone] like ''%''+@pKey+''%''
				)
	ORDER BY [CardRequestId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PartnerUsersByCountry_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerUsersByCountry_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Retrieve Partner Users By Country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PartnerUsersByCountry_Retrieve]
(
	@pPartnerUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT *
	FROM
		(SELECT
			   a.[PartnerUsersByCountryId]
			  ,a.[PartnerUserId]
			  ,a.[CountryId]
			  ,b.[Name] AS [CountryName]
			  ,b.[Code] AS [CountryCode]
			  ,CONVERT(BIT,1) AS IsCountryChecked
		FROM [General].[PartnerUsersByCountry] a
			INNER JOIN [General].[Countries] b
				ON b.[CountryId] = a.[CountryId]		
		WHERE a.[PartnerUserId] = @pPartnerUserId
		UNION
		SELECT	 NULL AS [PartnerUsersByCountryId]
				,@pPartnerUserId AS [PartnerUserId]
				,a.[CountryId]
				,a.[Name] AS [CountryName]
				,a.[Code] AS [CountryCode]
				,CONVERT(BIT,0) AS IsCountryChecked
			FROM [General].[Countries] a
		WHERE NOT EXISTS (SELECT 1
								FROM [General].[PartnerUsersByCountry] x
							WHERE x.[PartnerUserId] = @pPartnerUserId
							  AND x.[CountryId] = a.[CountryId]))t
	ORDER BY t.[CountryName]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_OdometerGpsByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_OdometerGpsByVehicle_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Berman Romero
-- Create date: 01/18/2015
-- Description:	Retrieve Odometer Gps By Vehicle
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_OdometerGpsByVehicle_Retrieve]
(
	 @pDate DATETIME						--@pDate: Start Date
	,@pVehicleId INT						--@pVehicleId: VehicleId
	,@pGpsOdometer INT OUTPUT				--@pGpsOdometer: @pGpsOdometer
)
AS
BEGIN

	SET NOCOUNT ON

		DECLARE @lIntrackReference INT
		
		
		SELECT @lIntrackReference = a.[IntrackReference]
			FROM [General].[Vehicles] a
		WHERE a.[VehicleId] = @pVehicleId
		
		IF @lIntrackReference IS NULL
		BEGIN
			SET @pGpsOdometer = 0
		END
		ELSE
		BEGIN
		
			DECLARE @lIndex INT = 0, @lMinDate DATETIME = @pDate, @lMaxDate DATETIME = @pDate
			
			WHILE(@lIndex < 3 AND @pGpsOdometer IS NULL)
			BEGIN
				SELECT @lMinDate = DATEADD(MINUTE,-15,@lMinDate), @lMaxDate = DATEADD(MINUTE,15,@lMaxDate)
				
				SELECT
					TOP 1 @pGpsOdometer = e.[Odometer]
				FROM [Composiciones] b
						INNER JOIN [DispositivosAVL] c
							ON b.[dispositivo] = c.[dispositivo]
						INNER JOIN  [dbo].[Devices] d
							ON c.[numeroimei] = [UnitID]
						INNER JOIN  [dbo].[Reports] e
							ON d.[Device] = e.[Device]
				WHERE b.vehiculo = @lIntrackReference
				  AND e.GPSDateTime BETWEEN @lMinDate AND @lMaxDate
				ORDER BY DATEDIFF(SECOND, e.[GPSDateTime], @pDate) ASC
				
					
				SET @lIndex = @lIndex + 1
			END
			
		END
		
	SET NOCOUNT OFF
	
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_MonthlyClosing_WarningEmails]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MonthlyClosing_WarningEmails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ===============================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/04/2014
-- Description:	Send the Monthly Closing Warning Emails
-- ===============================================================
Create Procedure [Control].[Sp_MonthlyClosing_WarningEmails]
(
	 @pYear INT								--@pYear: Year of monthly closing
	,@pMonth INT							--@pMonth: Month of monthly closing
)
AS
BEGIN
	DECLARE @lTo varchar(100) = '''',
			@lSubject varchar(300),
			@lMessage nvarchar(1000),
			@lEmail varchar(100),
			@lFuelType varchar(50),
			@lCreditAmount decimal (18,2),
			@lTotal decimal (18,2),
			@lAssigned decimal (18,2),
			@lCustomerId int

    --1. VehicleFuel Amount Sum is greather than FuelByCreditTotal
    Set @lCustomerId = null
	Set @lSubject = ''El monto total de Crédito por tipo de Combustible es menor que el total asignado''
    
    Select @lCustomerId = min(FC.CustomerId)
      from Control.FuelsByCredit FC
	 where FC.Year = @pYear
	   and FC.Month = @pMonth
	   and FC.Total < Assigned
	   
	WHILE @lCustomerId is not null
	BEGIN
		Set @lMessage = ''''
		--Send Email to Admin
		Set @lTo = General.Fn_GetEmailsRole(''CUSTOMER_ADMIN'', @lCustomerId, 100)
		
		IF LEN(@lTo) > 0
		BEGIN
			Set @lMessage = ''El monto de Crédito por Tipo de Combustible es menor que el monto total asignado para los siguientes combustibles:'' + CHAR(13)
			
			Select top 1 @lFuelType = F.Name, @lTotal = FC.Total, @lAssigned = FC.Assigned
			  from Control.FuelsByCredit FC
			 inner join Control.Fuels F on FC.FuelId = F.FuelId
			 where FC.Year = @pYear
			   and FC.Month = @pMonth
			   and FC.CustomerId = @lCustomerId
			   and FC.Total < Assigned
			 order by F.Name
			 
			If @@ROWCOUNT = 0
				Set @lFuelType = null
			
			WHILE @lFuelType is not null
			BEGIN

				Set @lMessage = @lMessage + ''- Tipo de Combustible: '' + @lFuelType + '', Monto total: '' + convert(varchar, cast(@lTotal as money), 1) + '', Monto Asignado: '' + convert(varchar, cast(@lAssigned as money), 1) + CHAR(13)
				--Next iteration
				Select top 1 @lFuelType = F.Name, @lTotal = FC.Total, @lAssigned = FC.Assigned
				  from Control.FuelsByCredit FC
				 inner join Control.Fuels F on FC.FuelId = F.FuelId
				 where FC.Year = @pYear
				   and FC.Month = @pMonth
				   and FC.Total < Assigned
				   and FC.CustomerId = @lCustomerId
				   and F.Name > @lFuelType
				 order by F.Name

				If @@ROWCOUNT = 0
					Set @lFuelType = null
			END
			 
			Select @lMessage			 
			Exec General.Sp_Email_AddOrEdit null, @lTo, null, null, @lSubject, @lMessage, 0, 0, null, null 
		END
					
		--Next iteration
		Select @lCustomerId = min(FC.CustomerId)
		  from Control.FuelsByCredit FC
		 where FC.Year = @pYear
		   and FC.Month = @pMonth
		   and FC.Total < Assigned
		   and FC.CustomerId > @lCustomerId
	END

	--2. FuelsByCredit Total Sum is greather than the Credit Amount    
	Set @lSubject = ''El monto del Crédito total es menor que la suma de Créditos por tipo de Combustible''

	Select top 1 @lCustomerId = FC.CustomerId, @lCreditAmount = C.CreditAmount, @lTotal = Sum(FC.Total) 
	  from Control.FuelsByCredit FC
	 inner join Control.CustomerCredits C on FC.CustomerId = C.CustomerId and FC.Year = C.Year and FC.Month = C.Month
	 where FC.Year = @pYear
	   and FC.Month = @pMonth
	 group by FC.CustomerId, C.CreditAmount
	having Sum(FC.Total) > C.CreditAmount
	 order by FC.CustomerId
	 
	If @@ROWCOUNT = 0
		Set @lCustomerId = null
		
	WHILE @lCustomerId is not null
	BEGIN
		Set @lMessage = ''''
		--Send Email to Admin
		Set @lTo = General.Fn_GetEmailsRole(''CUSTOMER_ADMIN'', @lCustomerId, 100)
		
		IF LEN(@lTo) > 0
		BEGIN
			Set @lMessage = ''El monto del crédito total: '' + convert(varchar, cast(@lCreditAmount as money), 1) + 
							'', es menor que la sumatoria de créditos por tipo de combustible: '' + convert(varchar, cast(@lTotal as money), 1)
						 
			Select @lMessage			 
			Exec General.Sp_Email_AddOrEdit null, @lTo, null, null, @lSubject, @lMessage, 0, 0, null, null 
		END
	
		--Next iteration
		Select top 1 @lCustomerId = FC.CustomerId, @lCreditAmount = C.CreditAmount, @lTotal = Sum(FC.Total) 
		  from Control.FuelsByCredit FC
		 inner join Control.CustomerCredits C on FC.CustomerId = C.CustomerId and FC.Year = C.Year and FC.Month = C.Month
		 where FC.Year = @pYear
		   and FC.Month = @pMonth
		   and C.CustomerId > @lCustomerId
		 group by FC.CustomerId, C.CreditAmount
		having Sum(FC.Total) > C.CreditAmount
		 order by FC.CustomerId
		 
		If @@ROWCOUNT = 0
			Set @lCustomerId = null
	END	 

	--3. Assigned in Credit Cards is greather than the Credit Amount    
	Set @lSubject = ''El monto del Crédito total es menor que el monto asignado a tarjetas de crédito''
	Select top 1 @lCustomerId = C.CustomerId, @lCreditAmount = C.CreditAmount, @lAssigned = C.CreditAssigned 
	  from Control.CustomerCredits C 
	 where C.Year = @pYear
	   and C.Month = @pMonth
	   and C.CreditAmount < C.CreditAssigned
	 order by C.CustomerId
	 
	If @@ROWCOUNT = 0
		Set @lCustomerId = null
		
	WHILE @lCustomerId is not null
	BEGIN
		Set @lMessage = ''''
		Set @lTo = General.Fn_GetEmailsRole(''CUSTOMER_ADMIN'', @lCustomerId, 100)
		--Send Email to Admin
		
		IF LEN(@lTo) > 0
		BEGIN
			Set @lMessage = ''El monto del crédito total: '' + convert(varchar, cast(@lCreditAmount as money), 1) + 
							'', es menor que el monto asignado a tarjetas de crédito: '' + convert(varchar, cast(@lAssigned as money), 1)
			Select @lMessage			 
			Exec General.Sp_Email_AddOrEdit null, @lTo, null, null, @lSubject, @lMessage, 0, 0, null, null 
		END
	
		--Next iteration
		Select top 1 @lCustomerId = C.CustomerId, @lCreditAmount = C.CreditAmount, @lAssigned = C.CreditAssigned 
		  from Control.CustomerCredits C 
		 where C.Year = @pYear
		   and C.Month = @pMonth
		   and C.CreditAmount < C.CreditAssigned
		   and C.CustomerId > @lCustomerId
		 order by C.CustomerId
		 
		If @@ROWCOUNT = 0
			Set @lCustomerId = null
	END	 

END' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_IntrackReference_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_IntrackReference_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/03/2014
-- Description:	Retrieve IntrackReference information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_IntrackReference_Retrieve]
(
	@pVeicleId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		c.[CustomerId],
		co.[Code] CountryCode, 
		v.[IntrackReference],
		c.[CustomerId] [CustomerId]
	FROM 
		[General].[Customers] c
		INNER JOIN [General].[Countries] co ON c.[CountryId] = co.[CountryId]
		INNER JOIN [General].[Vehicles] v ON c.[CustomerId] = v.[CustomerId]
	WHERE 
		v.[VehicleId] = @pVeicleId
		
    SET NOCOUNT OFF
END






' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PartnerUsers_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerUsers_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Add Or Edit Partner User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PartnerUsers_AddOrEdit]
(
	 @pPartnerUserId INT = NULL
	,@pUserId INT
	,@pPartnerId INT
	,@pCountriesAccessXml VARCHAR(MAX)
	,@pLoggedUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lxmlData XML = CONVERT(XML,@pCountriesAccessXml)
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		IF (@pPartnerUserId IS NULL)
		BEGIN
			INSERT INTO [General].[PartnerUsers]
					([UserId]
					,[PartnerId]
					,[InsertDate]
					,[InsertUserId])
			VALUES	(@pUserId
					,@pPartnerId
					,GETUTCDATE()
					,@pLoggedUserId)
					
			SET @pPartnerUserId = SCOPE_IDENTITY()
		END
		ELSE
		BEGIN
			UPDATE [General].[PartnerUsers]
				SET  [ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
			WHERE [PartnerUserId] = @pPartnerUserId
			
		END
        
        DELETE
			FROM [General].[PartnerUsersByCountry]
        WHERE [PartnerUserId] = @pPartnerUserId
        
        INSERT INTO [General].[PartnerUsersByCountry]([PartnerUserId],[CountryId],[InsertDate],[InsertUserId])
        SELECT 
			 @pPartnerUserId
			,RowData.col.value(''./@CountryId'', ''INT'') AS [CountryId]
			,GETUTCDATE()
			,@pLoggedUserId
		FROM @lxmlData.nodes(''/xmldata/Country'') RowData(col)
        
        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Partners_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Partner information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Partners_Retrieve]
(
	 @pPartnerId INT = NULL,
	 @pKey VARCHAR(800) = NULL,
	 @pPartnerGroupId INT = NULL,
	 @pPartnerUserId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT
		 a.[PartnerId]
		,a.[CountryId]
		,a.[Name]
		,a.[Logo]
		,b.[Name] AS [CountryName]
		,a.[ApiUserName]
		,a.[ApiPassword]
		,a.[CapacityUnitId]
		,a.[PartnerTypeId]
		,c.[Name] AS [CapacityUnitName]
		,p.[Name] AS [PartnerTypeName]
		,a.[RowVersion]
    FROM [General].[Partners] a
		INNER JOIN [General].[Countries] b
			ON a.[CountryId] = b.[CountryId]
		LEFT JOIN [General].[Types] c
			ON a.[CapacityUnitId] = c.[TypeId] and c.[Usage] = ''CUSTOMERS_CAPACITY_UNITS''
		LEFT JOIN [General].[Types] p
			ON a.[PartnerTypeId] = p.[TypeId] and p.[Usage] = ''PARTNER_TYPE''
	WHERE a.[IsDeleted] = 0
	  AND (@pPartnerId IS NULL OR a.[PartnerId] = @pPartnerId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
				OR b.[Name] like ''%''+@pKey+''%'')
	  AND (@pPartnerGroupId IS NULL OR a.[PartnerGroupId] = @pPartnerGroupId)
	  AND (@pPartnerUserId IS NULL OR EXISTS (SELECT 1
													FROM [General].[PartnerUsersByCountry] x
												WHERE x.[PartnerUserId] = @pPartnerUserId
												  AND x.CountryId = a.[CountryId]))
	ORDER BY [PartnerId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PAIndicators_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PAIndicators_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero
-- Create date: 11/14/2014
-- Description:	Retrieve Indicators for Partner Admin Landing Page
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PAIndicators_Retrieve]
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		(SELECT COUNT(1) FROM [General].[Customers] WHERE IsDeleted = 0 AND IsActive = 1) AS Customers,
		(SELECT COUNT(1) FROM [Control].[CreditCard]) AS Cards, 
		(SELECT COUNT(1) FROM [General].[Vehicles] a WHERE a.Active = 1) AS Vehicles,
		(SELECT COUNT(1) FROM [General].[Users] a WHERE a.IsActive = 1 AND a.InsertDate > DATEADD(day,-7,GETUTCDATE())) AS Users
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlVehicleInfo_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlVehicleInfo_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/12/2014
-- Description:	Retrieve PassControlVehicleInfo information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_PassControlVehicleInfo_Retrieve]
(
	 @pIntrackReference INT 
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		v.[VehicleId] [VehicleId], 
		v.[IntrackReference] [IntrackReference], 
		v.[PlateId] [PlateId], 
		v.[CostCenterId] [CostCenterId], 
		s.[Name] [CostCenterName]
	FROM 
		[General].[Vehicles] v
		INNER JOIN [General].[VehicleCostCenters] s ON v.[CostCenterId] = s.[CostCenterId]
	WHERE 
		[IntrackReference] in (@pIntrackReference)
		
    SET NOCOUNT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehiclesReferences_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesReferences_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/02/2014
-- Description:	Retrieve Vehicle References information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesReferences_Retrieve]
(
	 @pVehicleId INT = NULL					--@pVehicleId: PK of the table
	,@pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[VehicleId]
		,a.[PlateId]
		,a.[Name]
		,a.[CustomerId]
		,a.[AdministrativeSpeedLimit]
		,a.[IntrackReference]
		,a.[DeviceReference]
		,a.[RowVersion]
    FROM [General].[Vehicles] a
	WHERE (@pVehicleId IS NULL OR [VehicleId] = @pVehicleId)
	  AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
                OR a.[PlateId] like ''%''+@pKey+''%'')
	ORDER BY [VehicleId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehiclesReferences_Edit]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesReferences_Edit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/02/2014
-- Description:	Update Vehicle References information (Vehicle Intrack and Device Atrack Reference)
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesReferences_Edit]
(
	 @pVehicleId INT								--@pVehicleId: PK of the table
	,@pCustomerId INT								--@pCustomerId: FK of Customer
	,@pAdministrativeSpeedLimit INT					--@pAdministrativeSpeedLimit: Maximum speed limit for this car
	,@pIntrackReference INT							--@pIntrackReference: Vehicle Id in IntrackV2 database
	,@PDeviceReference INT							--@pDeviceReference: Device Id in Atrack database				
	,@pLoggedUserId INT								--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP							--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0 
            DECLARE @lResult INT   
            
			--Invoke SP to replicate vehicle data in ATRACK database
			Exec General.Sp_Vehicles_Replication @pVehicleId, @pCustomerId, @pIntrackReference, @pDeviceReference, @pAdministrativeSpeedLimit, @pResult = @lResult output, @pErrorDesc = @lErrorMessage output
			If @lResult = -1
			begin
				RAISERROR (@lErrorMessage, 16, 1)
			end
			
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			UPDATE [General].[Vehicles]
				SET  [IntrackReference] = @pIntrackReference
					,[DeviceReference] = @PDeviceReference
					,[ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
			WHERE [VehicleId] = @pVehicleId
			  AND [RowVersion] = @pRowVersion
            
            SET @lRowCount = @@ROWCOUNT
                        
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleCostCentersByGeoFence_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleCostCentersByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/18/2014
-- Description:	Retrieve VehicleSubUnitsByGeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleCostCentersByGeoFence_Retrieve]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		null [GeoFenceId],
		a.[Name] [Name],
		a.[CostCenterId] [CostCenterId],
		ROW_NUMBER() OVER (ORDER BY a.[CostCenterId]) AS [RowNumber] 
	FROM 
		[General].[VehicleCostCenters] a
		INNER JOIN [General].[VehicleUnits] b ON a.[UnitId] = b.[UnitId]
	WHERE
		b.[CustomerId] = @pCustomerId
		AND NOT EXISTS(
			SELECT 
				1 
			FROM 
				[Efficiency].[VehicleCostCenterByGeoFence] x
			WHERE 
				x.[CostCenterId] = a.[CostCenterId] AND 
				x.[GeoFenceId] = @pGeoFenceId
			)
	UNION
	SELECT 
		x.[GeoFenceId] [GeoFenceId],
		a.[Name] [Name],
		a.[CostCenterId] [CostCenterId],
		ROW_NUMBER() OVER (ORDER BY a.[CostCenterId]) AS [RowNumber] 
	FROM 
		[General].[VehicleCostCenters] a
		INNER JOIN [Efficiency].[VehicleCostCenterByGeoFence] x ON a.[CostCenterId] = x.[CostCenterId]
	WHERE x.[GeoFenceId] = @pGeoFenceId
	
    SET NOCOUNT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleCostCentersByGeoFence_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleCostCentersByGeoFence_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/18/2014
-- Description:	Insert or Update Vehicle SubUnits by GeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleCostCentersByGeoFence_AddOrEdit]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pXmlData VARCHAR(MAX)					--@pName: VehicleGroup Name
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Efficiency].[VehicleCostCenterByGeoFence]
			WHERE [GeoFenceId] = @pGeoFenceId
			
			INSERT INTO [Efficiency].[VehicleCostCenterByGeoFence]
					([GeoFenceId]
					,[CostCenterId]
					,[InsertDate]
					,[InsertUserId])
			SELECT 
					 @pGeoFenceId
					,Row.col.value(''./@CostCenterId'', ''INT'') AS [UnitId]
					,GETUTCDATE()
					,@pLoggedUserId
			FROM @lxmlData.nodes(''/xmldata/VehicleCostCenter'') Row(col)
			ORDER BY Row.col.value(''./@Index'', ''INT'')
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_UnitReference_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_UnitReference_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/03/2014
-- Description:	Retrieve IntrackReference information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_UnitReference_Retrieve]
(
	@pId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		co.[Code] CountryCode, 
		v.[IntrackReference], 
		c.[CustomerId] [CustomerId]
	FROM 
		[General].[Customers] c
		INNER JOIN [General].[Countries] co ON c.[CountryId] = co.[CountryId]
		INNER JOIN [General].[Vehicles] v ON c.[CustomerId] = v.[CustomerId]
		INNER JOIN [General].[VehicleCostCenters] cc ON v.[CostCenterId] = cc.[CostCenterId]
		INNER JOIN [General].[VehicleUnits] u ON cc.[UnitId] = u.[UnitId]
	WHERE 
		u.[UnitId] = @pId
		
    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_SAIndicators_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_SAIndicators_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa H.
-- Create date: 11/14/2014
-- Description:	Retrieve Indicators for Super Admin Landing Page
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_SAIndicators_Retrieve]
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		(SELECT COUNT(1) FROM General.Partners) AS Partners, 
		(SELECT COUNT(1) FROM General.Customers where IsDeleted = 0) AS Customers, 
		(SELECT COUNT(1) FROM General.Vehicles) AS Vehicles, 
		(SELECT COUNT(1) FROM General.DriversUsers) AS Drivers
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesStopsList_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesStopsList_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 04/02/2014
-- Description:	Retrieve Stops information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_RoutesStopsList_Retrieve]
(
	 @pVehicleId INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		a.StopId,
		a.Longitude,
		a.Latitude
	 FROM Efficiency.Stops a
		INNER JOIN General.Vehicles b ON a.VehicleId = b.VehicleId 
	WHERE b.IntrackReference = @pVehicleId AND
		a.GPSDateTimeEnd >= @pStartDate AND
		a.GPSDateTimeStart <= @pEndDate
	
    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCategories_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCategories_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve VehicleCategory information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCategories_Retrieve]
(
	 @pVehicleCategoryId INT = NULL
	,@pKey VARCHAR(800) = NULL
	,@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[VehicleCategoryId]
		,a.[Manufacturer]
		,a.[Type]
		,a.[DefaultFuelId]
		,a.[Liters]
		,a.[VehicleModel]
		,b.[Name] AS [FuelName]
		,a.[Icon]
		,a.[MaximumSpeed]
		,a.[MaximumRPM]
		,a.[Year]
		,a.[Weight]
		,a.[LoadType]
		,a.[CylinderCapacity]
		,a.[RowVersion]
		,(SELECT ''['' + STUFF((
			SELECT 
				'',{"FuelByVehicleCategoryId":'' + FuelByVehicleCategoryId
				+ '',"VehicleCategoryId":'' + CAST(VehicleCategoryId AS VARCHAR(MAX))
				+ '',"FuelId":'' + CAST(FuelId  AS VARCHAR(MAX))
				+ '',"FuelName": "'' + FuelName + ''"''
				+ '',"IsFuelChecked":'' + CAST(IsFuelChecked AS VARCHAR(MAX))
				+''}''

			FROM (SELECT
					 CAST(x.FuelByVehicleCategoryId  AS VARCHAR(MAX)) AS FuelByVehicleCategoryId
					,x.VehicleCategoryId
					,x.FuelId
					,y.Name AS FuelName
					,CONVERT(BIT, 1) AS IsFuelChecked 
				FROM [General].[FuelsByVehicleCategory] x
					INNER JOIN [Control].[Fuels] y
						ON y.FuelId = x.FuelId
				WHERE x.VehicleCategoryId = a.VehicleCategoryId
				UNION 
				SELECT
					 ''null'' AS FuelByVehicleCategoryId
					,a.VehicleCategoryId AS VehicleCategoryId
					,x.[FuelId]
					,x.Name AS FuelName
					,CONVERT(BIT, 0) AS IsFuelChecked 
				FROM [Control].[Fuels] x
					INNER JOIN [General].[Customers] y
						ON y.[CountryId] = x.[CountryId]
				WHERE NOT EXISTS (	SELECT 1 
									FROM [General].[FuelsByVehicleCategory] q
									WHERE q.[VehicleCategoryId] = a.VehicleCategoryId
									  AND q.[FuelId] = x.[FuelId])
				  AND y.CustomerId = a.CustomerId) t
			        
			FOR XML PATH(''''), TYPE
		).value(''.'', ''varchar(max)''), 1, 1, '''') + '']'') AS JsonFuelsList
	FROM [General].[VehicleCategories] a
		INNER JOIN [Control].[Fuels] b
			ON a.[DefaultFuelId] = b.[FuelId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pVehicleCategoryId IS NULL OR [VehicleCategoryId] = @pVehicleCategoryId)
	  AND (@pKey IS NULL 
				OR a.[Manufacturer] like ''%''+@pKey+''%''
				OR a.[Type] like ''%''+@pKey+''%''
				OR CONVERT(VARCHAR(20),a.[Liters]) like ''%''+@pKey+''%''
				OR b.[Name] like ''%''+@pKey+''%'')
	ORDER BY [VehicleCategoryId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCategories_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCategories_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Delete VehicleCategory information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCategories_Delete]
(
	 @pVehicleCategoryId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
		
			
			DELETE [General].FuelsByVehicleCategory
			WHERE [VehicleCategoryId] = @pVehicleCategoryId
			
			DELETE [General].[VehicleCategories]
			WHERE [VehicleCategoryId] = @pVehicleCategoryId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleCategories_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCategories_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update VehicleCategory information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCategories_AddOrEdit]
(
	 @pVehicleCategoryId INT = NULL
	,@pManufacturer VARCHAR(250)
	,@pType VARCHAR(250)
	,@pDefaultFuelId INT
	,@pLiters INT
	,@pVehicleModel VARCHAR(250)
	,@pCustomerId INT
	,@pIcon VARCHAR(MAX)
	,@pMaximumSpeed INT = NULL
	,@pMaximumRPM INT = NULL
	,@pYear INT = NULL
	,@pWeight NUMERIC(16,2) = NULL
	,@pLoadType INT = NULL
	,@pCylinderCapacity INT = NULL
	,@pFuelsListXml VARCHAR(MAX) = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
	,@pDefaultPerformance decimal
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lxmlData XML = CONVERT(XML,@pFuelsListXml)
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
				IF (@pVehicleCategoryId IS NULL)
			BEGIN
				INSERT INTO [General].[VehicleCategories]
						([Manufacturer]
						,[Type]
						,[DefaultFuelId]
						,[Liters]
						,[VehicleModel]
						,[CustomerId]
						,[Icon]
						,[MaximumSpeed]
						,[MaximumRPM]
						,[Year]
						,[Weight]
						,[LoadType]
						,[CylinderCapacity]
						,[InsertDate]
						,[InsertUserId]
						,DefaultPerformance)
				VALUES	(@pManufacturer
						,@pType
						,@pDefaultFuelId
						,@pLiters
						,@pVehicleModel
						,@pCustomerId
						,@pIcon
						,@pMaximumSpeed
						,@pMaximumRPM
						,@pYear
						,@pWeight
						,@pLoadType
						,@pCylinderCapacity
						,GETUTCDATE()
						,@pLoggedUserId
						,@pDefaultPerformance)
						
				SET @pVehicleCategoryId = SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
				UPDATE [General].[VehicleCategories]
					SET  [Manufacturer] = @pManufacturer
						,[Type] = @pType
						,[DefaultFuelId] = @pDefaultFuelId
						,[Liters] = @pLiters
						,[VehicleModel] = @pVehicleModel
						,[CustomerId] = @pCustomerId
						,[Icon] = @pIcon
						,[MaximumSpeed] = @pMaximumSpeed
						,[MaximumRPM] = @pMaximumRPM
						,[Year] = @pYear
						,[Weight] = @pWeight
						,[LoadType] = @pLoadType
						,[CylinderCapacity] = @pCylinderCapacity
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
						,DefaultPerformance=@pDefaultPerformance
				WHERE [VehicleCategoryId] = @pVehicleCategoryId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            DELETE
				FROM [General].[FuelsByVehicleCategory]
			WHERE [VehicleCategoryId] = @pVehicleCategoryId
	        
			INSERT INTO [General].[FuelsByVehicleCategory]([VehicleCategoryId],[FuelId],[InsertDate],[InsertUserId])
			SELECT 
				 @pVehicleCategoryId
				,RowData.col.value(''./@FuelId'', ''INT'') AS [FuelId]
				,GETUTCDATE()
				,@pLoggedUserId
			FROM @lxmlData.nodes(''/xmldata/Fuel'') RowData(col)
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleByRoute_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleByRoute_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/20/2014
-- Description:	Retrieve VehicleByRoute information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleByRoute_Retrieve]
(
	 @pVehicleId INT = NULL
	,@pRouteId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		v.[VehicleId],
		v.[RouteId],
		r.[Name] [RouteName],
		v.[Days],
		v.[RowVersion]
	FROM 
		[Efficiency].[VehicleByRoute] v
		INNER JOIN [Efficiency].[Routes] r ON v.[RouteId] = r.[RouteId]	
	WHERE 
		v.[VehicleId] = @pVehicleId AND 
		(@pRouteId IS NULL OR v.[RouteId] = @pRouteId) 
	ORDER BY 
		r.[Name] DESC
	
    SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleByRoute_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleByRoute_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/20/2014
-- Description:	Delete VehicleByRoute information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleByRoute_Delete]
(
	 @pVehicleId INT,
	 @pRouteId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Efficiency].[VehicleByRoute]
			WHERE [VehicleId] = @pVehicleId
				AND [RouteId] = @pRouteId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleByRoute_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleByRoute_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/26/2014
-- Description:	Insert or Update Vehicle by Route information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleByRoute_AddOrEdit]
(
	 @pVehicleId INT						
	,@pXmlData VARCHAR(MAX)					
	,@pLoggedUserId INT						
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Efficiency].[VehicleByRoute]
			WHERE [VehicleId] = @pVehicleId
			
			INSERT INTO [Efficiency].[VehicleByRoute]
					([VehicleId]
					,[RouteId]
					,[Days]
					,[InsertDate]
					,[InsertUserId])
			SELECT 
					 @pVehicleId
					,Row.col.value(''./@RouteId'', ''INT'') AS [UnitId]
					,Row.col.value(''./@Days'', ''VARCHAR(10)'') AS [UnitId]
					,GETUTCDATE()
					,@pLoggedUserId
			FROM @lxmlData.nodes(''/xmldata/VehicleByRoute'') Row(col)
			WHERE Row.col.value(''./@Days'', ''VARCHAR(10)'') <> ''''
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleByPlate_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleByPlate_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martínez H.
-- Create date: 20/Oct/2014
-- Description:	Retrieve Vehicle By Plate information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleByPlate_Retrieve]
(
	@pPlateId VARCHAR(10) --@pPlateId: Plate of the vehicle
)
AS
BEGIN 
	SET NOCOUNT ON
		SELECT
			 a.[VehicleId]
			,a.[PlateId]
			,a.[Name]
			,a.[CostCenterId]
			,c.[Name] AS CostCenterName
			,a.[CustomerId]
			,f.[Name] AS UserName
			,e.[LicenseExpiration] AS DriverLicenseExpiration
			,e.[DailyTransactionLimit]
			,a.[VehicleCategoryId]
			,d.[Type] AS CategoryType
			,d.[Liters]
			,d.[DefaultFuelId]
			,b.[Name] AS FuelName
			,a.[Active]
			,a.[Colour]
			,a.[Chassis]
			,a.[LastDallas]
			,a.[FreightTemperature]
			,a.[FreightTemperatureThreshold1]
			,a.[FreightTemperatureThreshold2]
			,a.[Predictive]
			,a.[AVL]
			,a.[PhoneNumber]
			,h.[PartnerId]
			,h.[CapacityUnitId] AS PartnerCapacityUnitId
			,a.[RowVersion]
		FROM [General].[Vehicles] a
			INNER JOIN [General].[VehicleCostCenters] c
				ON a.[CostCenterId] = c.[CostCenterId]
			INNER JOIN [General].[VehicleCategories] d
				ON a.[VehicleCategoryId] = d.[VehicleCategoryId]
			INNER JOIN [Control].[Fuels] b
				ON d.[DefaultFuelId] = b.[FuelId]
			INNER JOIN [General].[DriversUsers] e
				ON e.[UserId] = (SELECT
									TOP 1 [UserId]
								 FROM [General].[VehiclesByUser]
								 WHERE [VehicleId] = a.[VehicleId]
								 ORDER BY [InsertDate] DESC)
			INNER JOIN [General].[Users] f
				ON e.[UserId] = f.[UserId]
			INNER JOIN [General].[Customers] g
				ON a.[CustomerId] = g.[CustomerId]
			INNER JOIN [General].[CustomersByPartner] i
				ON i.[CustomerId] = g.[CustomerId]
			INNER JOIN [General].[Partners] h
				ON i.[PartnerId] = h.[PartnerId]
		WHERE (a.[PlateId] IS NULL OR a.[PlateId] = @pPlateId)
		ORDER BY [VehicleId] DESC
	SET NOCOUNT OFF
END' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_ValidateTimeSlotByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ValidateTimeSlotByVehicle_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 12/09/2014
-- Description:	Retrieve ValidateVehicleTimeSlot information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ValidateTimeSlotByVehicle_Retrieve]
(
	 @pVehicleId INT						--@pTimeSlotByVehicleId: PK of the table
)
AS
BEGIN
	
	
	SET NOCOUNT ON	
	
	DECLARE @Date datetime = DATEADD(HOUR, -6, CONVERT(TIME, GETUTCDATE()))
	DECLARE @Time TIME = DATEADD(HOUR, -6, CONVERT(TIME, GETUTCDATE()))
	DECLARE @Day int = (DATEPART(dw, @Date))
	DECLARE @lxmlData xml

	select @lxmlData = XmlTimeSlot from General.TimeSlotByVehicle where VehicleId = @pVehicleId

	select 
		RowData.col.value(''./@VehicleId'', ''INT'') AS [VehicleId]
		,RowData.col.value(''./@Day'', ''INT'') AS [Day]
		,RowData.col.value(''./@Time'', ''VARCHAR(5)'') AS [Time]
	FROM @lxmlData.nodes(''/xmldata/TimeSlot'') RowData(col)
	WHERE RowData.col.value(''./@Day'', ''INT'')  = @Day
		AND @Time BETWEEN CONVERT(TIME, RowData.col.value(''./@Time'', ''VARCHAR(5)''))
		AND DATEADD(MINUTE, 30,CONVERT(TIME, RowData.col.value(''./@Time'', ''VARCHAR(5)'')))											
	ORDER BY 2,3

    SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_TrendAndConsumptionComparativeReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TrendAndConsumptionComparativeReport_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes.
-- Create date: 22/01/2015
-- Description:	Retrieve TrendAndConsumptionComparativeReport information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TrendAndConsumptionComparativeReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		   v.PlateId,
		   v.VehicleId,
		   vcc.CostCenterId,
		   vcc.Name as CostCenterName,
		   vc.[Type],
		   (SELECT max(Liters) FROM [Control].Transactions where  v.VehicleId=VehicleId) as MaxConsumption,
		   ((SELECT SUM(Liters) FROM [Control].Transactions where  v.VehicleId=VehicleId)/(SELECT COUNT(*) FROM [Control].Transactions where  v.VehicleId=VehicleId)) as MedConsumption,
		   (SELECT MIN(Liters) FROM [Control].Transactions where  v.VehicleId=VehicleId) as MinConsumption,
		   vu.UnitId,
		   vu.Name as UnitName,
		   @lCurrencySymbol as CurrencySymbol,
		   (SELECT top 1 Liters FROM [Control].Transactions where (MONTH([Date])=@pMonth and YEAR([Date])=@pYear) and VehicleId=v.VehicleId order by [TransactionId] desc) as Volume,
		   (SELECT top 1 FuelAmount FROM [Control].Transactions where (MONTH([Date])=@pMonth and YEAR([Date])=@pYear) and VehicleId=v.VehicleId order by [TransactionId] desc) as FuelAmount
		   FROM General.Vehicles v 
		   INNER JOIN General.VehicleCategories vc ON v.VehicleCategoryId =vc.VehicleCategoryId
		   INNER JOIN General.VehicleCostCenters vcc ON V.CostCenterId=vcc.CostCenterId
		   INNER JOIN General.VehicleUnits vu ON vcc.UnitId=vu.UnitId
		   INNER JOIN [Control].Transactions t ON t.VehicleId=v.VehicleId
		   WHERE V.CustomerId=@pCustomerId and 
		   (Month(t.[Date])=@pMonth and Year(t.Date)=@pYear)
		
	SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_TransactionsReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReport_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 05/12/2014
-- Description:	Retrieve TransactionsReport information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionsReport_Retrieve]
(
	@pCustomerId INT,					--@pCustomerId: CustomerId
	@pStatus INT=null,						--@pStatus: Status
	@pKey VARCHAR(800) = NULL,			--@pKey :Key
	@pYear INT = NULL,					--@pYear: Year
	@pMonth INT = NULL,					--@pMonth: Month
	@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
	@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @pIssueForId INT
		
	SET @pIssueForId = (SELECT [IssueForId] FROM [General].[Customers] WHERE [CustomerId] = @pCustomerId)

	IF @pIssueForId  = 100
		BEGIN
			--DRIVER
			SELECT 
				t.[TransactionId] [TransactionId], 
				u.[Name] [HolderName], 
				t.[Date] [Date], 
				f.[Name] [FuelName],
				t.[FuelAmount] [FuelAmount], 
				t.[Odometer] [Odometer], 
				t.[Liters] [Liters], 
				v.[PlateId] [PlateId]
			FROM 
				[Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v 
					ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c 
					ON v.[CustomerId] = c.[CustomerId]
				INNER JOIN [Control].[Fuels] f 
					ON t.[FuelId] = f.[FuelId]
				INNER JOIN [Control].[CreditCardByDriver] cd 
					ON t.[CreditCardId] = cd.[CreditCardId]
				INNER JOIN [General].[Users] u 
					ON cd.[UserId] = u.[UserId]
			WHERE 
				c.[CustomerId] = @pCustomerId AND 
				(@pStatus is null OR (ISNULL(t.[IsFloating], 0) = case WHEN @pStatus = 2 THEN 1 ELSE 0 END AND --Floating
				ISNULL(t.[IsReversed], 0) = CASE WHEN @pStatus = 3 THEN 1 ELSE 0 END AND --Reversed
				ISNULL(t.[IsDuplicated], 0) = CASE WHEN @pStatus = 4 THEN 1 ELSE 0 END AND --Duplicated
				ISNULL(t.[IsAdjustment], 0) = CASE WHEN @pStatus = 5 THEN 1 ELSE 0 END))  --Adjustment
				AND (@pKey IS NULL 
					OR u.[Name] like ''%''+@pKey+''%''
					OR v.[PlateId] like ''%''+@pKey+''%'')
			    AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,t.[Date]) = @pMonth
						AND DATEPART(yyyy,t.[Date]) = @pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND t.[Date] BETWEEN @pStartDate AND @pEndDate))

			ORDER BY t.[Date]
		END
	ELSE
		BEGIN
			--VEHICLE
			SELECT 
				c.[CustomerId] [CustomerId],
				t.[CreditCardId] [CreditCardId],
				v.[PlateId] [HolderName], 
				t.[Date] [Date], 
				f.[Name] [FuelName],
				t.[FuelAmount] [FuelAmount], 
				t.[Odometer] [Odometer], 
				t.[Liters] [Liters], 
				v.[PlateId] [PlateId]
			FROM 
				[Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v 
					ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c 
					ON v.[CustomerId] = c.[CustomerId]
				INNER JOIN [Control].[Fuels] f 
					ON t.[FuelId] = f.[FuelId]
				INNER JOIN [Control].[CreditCardByVehicle] cv 
					ON t.[CreditCardId] = cv.[CreditCardId]
			WHERE
				c.[CustomerId] = @pCustomerId AND 
				(@pStatus is null OR (ISNULL(t.[IsFloating], 0) = case WHEN @pStatus = 2 THEN 1 ELSE 0 END AND --Floating
				ISNULL(t.[IsReversed], 0) = CASE WHEN @pStatus = 3 THEN 1 ELSE 0 END AND --Reversed
				ISNULL(t.[IsDuplicated], 0) = CASE WHEN @pStatus = 4 THEN 1 ELSE 0 END AND --Duplicated
				ISNULL(t.[IsAdjustment], 0) = CASE WHEN @pStatus = 5 THEN 1 ELSE 0 END))  --Adjustment
				AND (@pKey IS NULL 
					OR v.[PlateId] like ''%''+@pKey+''%'')
			    AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,t.[Date]) = @pMonth
						AND DATEPART(yyyy,t.[Date]) = @pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND t.[Date] BETWEEN @pStartDate AND @pEndDate))

			ORDER BY t.[Date]
		END
	
    SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_TransactionsForReverse_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsForReverse_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 07/01/2014
-- Description:	Retrieve Transactions Info For Reverse Operations
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionsForReverse_Retrieve]
(
	 @pTransactionPOS VARCHAR(250)			--@pTransactionPOS: TransactionPOS (External System TraceNumber)
	,@pProcessorId VARCHAR(250)				--@pProcessorId: ProcessorId (External terminalId)
)
AS
BEGIN
	
	SET NOCOUNT ON
			
	SELECT
		  a.[CreditCardId]
		 ,a.[VehicleId]
		 ,a.[FuelId]
		 ,a.[Odometer]
		 ,a.[FuelAmount]
		 ,a.[Liters]
		 ,b.[CreditCardNumber] AS [EncryptedCreditCardNumber]
		 ,c.[PlateId]
    FROM [Control].[Transactions] a
		INNER JOIN [Control].[CreditCard] b
			ON b.[CreditCardId] = a.[CreditCardId]
		INNER JOIN [General].[Vehicles] c
			ON c.[VehicleId] = a.[VehicleId]
	WHERE a.[TransactionPOS] = @pTransactionPOS
	  AND a.[ProcessorId] = @pProcessorId
	  AND a.[IsFloating] = 0
	  AND a.[IsReversed] = 0
	  AND a.[IsDuplicated] = 0
	  AND COALESCE(a.[IsAdjustment],0) = 0
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_Transactions_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Transactions_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 20/Oct/2014
-- Description:	Retrieve Alarm By Odometer
-- Modified by:	Berman Romero
-- Description:	Was Changed all logic - previous version doesn''t work at all
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Transactions_Retrieve]
(
	@pVehicleId INT							--@pVehicleId
)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM  [Control].[Transactions] a WITH(READUNCOMMITTED)
	WHERE a.[VehicleId] = @pVehicleId
	  AND a.[Date] > DATEADD(MONTH,-3,GETUTCDATE())
	  AND a.[IsAdjustment] = 0
	  AND a.[IsFloating] = 0
	  AND a.[IsReversed] = 0
	  AND a.[IsDuplicated] = 0
	  AND a.[FixedOdometer] IS NULL
	  
	  	
	SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_Transactions_AddFromAPI]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Transactions_AddFromAPI]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/30/2014
-- Description:	Insert Transaction information from API
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Transactions_AddFromAPI]
(
	 @pPlateId VARCHAR(10)	
	,@pFuelId INT
	,@pCreditCardNumber NVARCHAR(520)
	,@pDate DATETIME
	,@pOdometer INT
	,@pLiters DECIMAL(16,2)
	,@pFuelAmount DECIMAL(16,2)	
	,@pTransactionPOS VARCHAR(250)
	,@pSchemePOS VARCHAR(MAX) = NULL
	,@pProcessorId VARCHAR(250) = NULL
	,@pIsFloating BIT = NULL
	,@pIsReversed BIT = NULL
	,@pIsDuplicated BIT = NULL
	,@pLoggedUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lVehicleId INT            
            DECLARE @lTransactionId INT = 0
            DECLARE @lCreditCardId INT
            
            SELECT @lVehicleId = a.[VehicleId]
				FROM [General].[Vehicles] a
            WHERE a.PlateId = @pPlateId
            
            SELECT @lCreditCardId =  [CreditCardId]
				FROM [Control].[CreditCard]
            WHERE [CreditCardNumber] = @pCreditCardNumber
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			 
			INSERT INTO [Control].[Transactions](
					 [VehicleId]
					,[FuelId]
					,[CreditCardId]
					,[Date]
					,[Odometer]
					,[Liters]
					,[FuelAmount]					
					,[TransactionPOS]
					,[SchemePOS]
					,[ProcessorId]
					,[IsFloating]
					,[IsReversed]
					,[IsDuplicated]
					,[InsertDate]
					,[InsertUserId])
			VALUES	(@lVehicleId
					,@pFuelId
					,@lCreditCardId
					,@pDate
					,@pOdometer
					,@pLiters
					,@pFuelAmount					
					,@pTransactionPOS
					,@pSchemePOS
					,@pProcessorId
					,@pIsFloating
					,@pIsReversed
					,@pIsDuplicated
					,GETUTCDATE()
					,@pLoggedUserId)
			
			SELECT @lTransactionId = SCOPE_IDENTITY()
			
			IF (@pIsReversed = 0 and @pIsDuplicated = 0)
			BEGIN
				Exec [Control].[Sp_CreditCard_CreditAvailable_Edit] @lCreditCardId, @pFuelAmount
			END
			
			-- History control --
			INSERT INTO [Control].[TransactionsHx]
			   ([TransactionId]
			   ,[CreditCardId]
			   ,[VehicleId]
			   ,[FuelId]
			   ,[Date]
			   ,[Odometer]
			   ,[FuelAmount]
			   ,[TransactionPOS]
			   ,[SchemePOS]
			   ,[ProcessorId]
			   ,[IsAdjustment]
			   ,[IsFloating]
			   ,[IsReversed]
			   ,[IsDuplicated]
			   ,[FixedOdometer]
			   ,[Liters]
			   ,[InsertDate]
			   ,[InsertUserId]
			   ,[ModifyDate]
			   ,[ModifyUserId])
			SELECT 
				[TransactionId]
			   ,[CreditCardId]
			   ,[VehicleId]
			   ,[FuelId]
			   ,[Date]
			   ,[Odometer]
			   ,[FuelAmount]
			   ,[TransactionPOS]
			   ,[SchemePOS]
			   ,[ProcessorId]
			   ,[IsAdjustment]
			   ,[IsFloating]
			   ,[IsReversed]
			   ,[IsDuplicated]
			   ,[FixedOdometer]
			   ,[Liters]
			   ,[InsertDate]
			   ,[InsertUserId]
			   ,[ModifyDate]
			   ,[ModifyUserId]FROM [Control].[Transactions] WHERE [TransactionId] = @lTransactionId
			-- History control --
					
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	
	SELECT @lTransactionId AS [TransactionId] 
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO

/****** Object:  StoredProcedure [General].[Sp_TimeSlotByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_TimeSlotByVehicle_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 12/04/2014
-- Description:	Retrieve VehicleTimeSlot information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_TimeSlotByVehicle_Retrieve]
(
	 @pVehicleId INT						--@pTimeSlotByVehicleId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	SELECT
		 a.[VehicleId]
		,a.[JsonTimeSlot]
		,a.[RowVersion]
	FROM [General].[TimeSlotByVehicle] a
	WHERE a.[VehicleId] = @pVehicleId	 
	
    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_TimeSlotByVehicle_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_TimeSlotByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 12/04/2014
-- Description:	Insert or Update TimeSlotByVehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_TimeSlotByVehicle_AddOrEdit]
(
	 @pVehicleId INT						--@pTimeSlotByVehicleId: PK of the table
	,@pJsonTimeSlot VARCHAR(MAX)			--@pJsonTimeSlot: Json TimeSlot
	,@pXmlTimeSlot VARCHAR(MAX)				--@pJsonTimeSlot: Json TimeSlot
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lTimeSlotByVehicleId INT
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			SELECT
				@lTimeSlotByVehicleId = a.[TimeSlotByVehicleId] 
			FROM [General].[TimeSlotByVehicle] a
			WHERE a.[VehicleId] = @pVehicleId
			
			IF (@lTimeSlotByVehicleId IS NULL)
			BEGIN
				INSERT INTO [General].[TimeSlotByVehicle]
						([VehicleId]
						,[JsonTimeSlot]
						,[XmlTimeSlot]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pVehicleId
						,@pJsonTimeSlot
						,@pXmlTimeSlot
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[TimeSlotByVehicle]
					SET  [JsonTimeSlot] = @pJsonTimeSlot
						,[XmlTimeSlot] = @pXmlTimeSlot
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [VehicleId] = @pVehicleId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
			
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_VehicleFuel_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Vehicle Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuel_Retrieve]
(
	 @pVehicleFuelId INT = NULL				--@pVehicleFuelId: PK of the table
	,@pVehicleId INT = NULL					--@pVehicleId: FK of Vehicle Id
	,@pYear INT = NULL						--@pYear: Year
	,@pMonth INT = NULL						--@pMonth: Month
	,@pCustomerId INT = NULL				--@pCustomerId: Customer Ids
	,@pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[VehicleFuelId]
		,a.[VehicleId]
		,a.[Month]
		,a.[Year]		
		,a.[Liters]
		,a.[Amount]
		,d.[Name] AS FuelName
		,d.[FuelId]
		,b.[PlateId]
		,b.[Name] AS VehicleName
		,c.[VehicleModel]
		,f.[Symbol] AS [CurrencySymbol]
		,a.[RowVersion]
    FROM [Control].[VehicleFuel] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]
		INNER JOIN [General].[VehicleCategories] c
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] d
			ON c.[DefaultFuelId] = d.[FuelId]
		INNER JOIN [General].[Customers] e
			ON b.[CustomerId] = e.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON e.[CurrencyId] = f.[CurrencyId]
	WHERE (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
	  AND (@pVehicleFuelId IS NULL OR a.[VehicleFuelId] = @pVehicleFuelId)
	  AND (@pYear IS NULL OR a.[Year] = @pYear)
	  AND (@pMonth IS NULL OR a.[Month] = @pMonth)
	  AND (@pCustomerId IS NULL OR b.[CustomerId] = @pCustomerId)
	  AND (@pKey IS NULL 
				OR CONVERT(VARCHAR(20),a.[Month]) like ''%''+@pKey+''%''
				OR CONVERT(VARCHAR(20),a.[Year]) like ''%''+@pKey+''%''
				OR CONVERT(VARCHAR(20),a.[Liters]) like ''%''+@pKey+''%'')
	ORDER BY [VehicleFuelId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_VehicleFuel_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Delete Vehicle Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuel_Delete]
(
	@pVehicleFuelId INT						--@pVehicleFuelId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0            
            DECLARE @lFuelByCreditId INT = 0
            DECLARE @lAssigned DECIMAL(16,2) = 0
			DECLARE @lFuelId INT = 0
			DECLARE @pMonth INT
			DECLARE @pYear INT
			DECLARE @pCustomerId INT
			
			SELECT
				 @lFuelByCreditId = d.[FuelByCreditId]
				,@lFuelId = c.[DefaultFuelId]
				,@pMonth = a.[Month]
				,@pYear = a.[Year]
				,@pCustomerId = b.[CustomerId]
			FROM [Control].[VehicleFuel] a
				INNER JOIN [General].[Vehicles] b
					ON a.[VehicleId] = b.[VehicleId]
				INNER JOIN [General].[VehicleCategories] c
					ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
				INNER JOIN [Control].[FuelsByCredit] d
					ON a.[Month] = d.[Month]
				   AND a.[Year] = d.[Year]
				   AND b.[CustomerId] = d.[CustomerId]
				   AND c.[DefaultFuelId] = d.[FuelId]
			WHERE a.[VehicleFuelId] = @pVehicleFuelId
			
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END		
			
			
			DELETE [Control].[VehicleFuel]
			WHERE [VehicleFuelId] = @pVehicleFuelId
			
			
			SET @lAssigned = COALESCE((SELECT SUM(a.[Amount]) 
				FROM [Control].[VehicleFuel] a
					INNER JOIN [General].[Vehicles] b
						ON a.[VehicleId] = b.[VehicleId]
					INNER JOIN [General].[VehicleCategories] c
						ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
				WHERE a.[Year] = @pYear
				  AND a.[Month] = @pMonth
				  AND b.[CustomerId] = @pCustomerId
				  AND c.[DefaultFuelId] = @lFuelId),0)
				  
			UPDATE [Control].[FuelsByCredit]
			SET [Assigned] = @lAssigned
			WHERE [FuelByCreditId] = @lFuelByCreditId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_VehicleFuel_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update Vehicle Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuel_AddOrEdit]
(
	 @pVehicleFuelId INT = NULL				--@pVehicleFuelId: PK of the table
	,@pVehicleId INT						--@pVehicleId: FK of Vehicles
	,@pLiters DECIMAL(16,10)				--@pLiters: Liters of Fuel
	,@pAmount DECIMAL(16,6)					--@pAmount: Amount of Fuel
	,@pMonth INT							--@pMonth: Month
	,@pYear INT								--@pYear: Year
	,@pCustomerId INT						--@pCustomerId: Customer Id
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
	,@pThrowError BIT = 1					--@pThrowError: Throw Error by Default or performs select 
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lErrorNumber INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pVehicleFuelId IS NULL)
			BEGIN
				INSERT INTO [Control].[VehicleFuel]
						([VehicleId]
						,[Month]
						,[Year]
						,[Liters]
						,[Amount]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pVehicleId
						,@pMonth
						,@pYear
						,@pLiters
						,@pAmount
						,GETUTCDATE()
						,@pLoggedUserId)				
			END
			ELSE
			BEGIN
				UPDATE [Control].[VehicleFuel]
					SET  [VehicleId] = @pVehicleId
						,[Month] = @pMonth
						,[Year] = @pYear
						,[Liters] = @pLiters
						,[Amount] = @pAmount
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [VehicleFuelId] = @pVehicleFuelId
				  AND [RowVersion] = @pRowVersion
				  
			END            
            
            SET @lRowCount = @@ROWCOUNT				
            
            IF @lRowCount > 0
            BEGIN
				DECLARE @lAssigned DECIMAL(16,2) = 0
				DECLARE @lTotal DECIMAL(16,2) = 0
				DECLARE @lFuelId INT = 0
				DECLARE @lDistributionTypeId INT = 0
				
				SELECT @lFuelId = b.DefaultFuelId
				FROM [General].[Vehicles] a
					INNER JOIN [General].[VehicleCategories] b
						ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
				WHERE a.[VehicleId] = @pVehicleId
									
				SET @lAssigned = COALESCE((SELECT SUM(a.[Amount]) 
				FROM [Control].[VehicleFuel] a
					INNER JOIN [General].[Vehicles] b
						ON a.[VehicleId] = b.[VehicleId]
					INNER JOIN [General].[VehicleCategories] c
						ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
				WHERE a.[Year] = @pYear
				  AND a.[Month] = @pMonth
				  AND b.[CustomerId] = @pCustomerId
				  AND c.[DefaultFuelId] = @lFuelId),0)
				  
				SELECT @lTotal= Total
				FROM [Control].[FuelsByCredit]
				WHERE [Year] = @pYear
				  AND [Month] = @pMonth
				  AND [CustomerId] = @pCustomerId
				  AND [FuelId] = @lFuelId
				  
				SELECT
					@lDistributionTypeId = [DistributionTypeId]
				FROM [General].[Customers]
				WHERE [CustomerId] = @pCustomerId
				
				IF @lAssigned < = @lTotal
				BEGIN
					UPDATE [Control].[FuelsByCredit]
						SET [Assigned] = @lAssigned
					WHERE [Year] = @pYear
					  AND [Month] = @pMonth
					  AND [CustomerId] = @pCustomerId
					  AND [FuelId] = @lFuelId
				END ELSE IF(@lDistributionTypeId = 901)
				BEGIN
					
					DECLARE @lFuelByCreditId INT 
					
					SET @lFuelByCreditId = COALESCE(
						(SELECT FuelByCreditId
						FROM [Control].[FuelsByCredit]
						WHERE [Year] = @pYear
						  AND [Month] = @pMonth
						  AND [CustomerId] = @pCustomerId
						  AND [FuelId] = @lFuelId),-1)
					
					IF (@lFuelByCreditId = -1)
					BEGIN
						INSERT INTO [Control].[FuelsByCredit]
								([CustomerId]
								,[FuelId]
								,[Month]
								,[Year]
								,[Total]
								,[Assigned]
								,[InsertDate]
								,[InsertUserId])
						VALUES	(@pCustomerId
								,@lFuelId
								,@pMonth
								,@pYear
								,@lAssigned
								,@lAssigned
								,GETUTCDATE()
								,@pLoggedUserId)
					END
					ELSE
					BEGIN 
						UPDATE [Control].[FuelsByCredit]
							SET  [Assigned] = @lAssigned
								,[Total] = @lAssigned 
						WHERE [Year] = @pYear
						  AND [Month] = @pMonth
						  AND [CustomerId] = @pCustomerId
						  AND [FuelId] = @lFuelId
					END
				END
				ELSE
				BEGIN
					IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
					BEGIN
						ROLLBACK TRANSACTION
						
						SET @lErrorNumber = 50002
						SET @lErrorMessage = ''Error updating Fuels By Credit, Assigned Amount greater than Total.''
						SET @lErrorSeverity = 16
						SET @lErrorState = ''TRUE''
						
						IF(@pThrowError = 1)
							RAISERROR (@lErrorNumber, @lErrorSeverity, @lErrorState)
						ELSE
							SELECT
								 @lErrorNumber AS ErrorNumber
								,@lErrorMessage AS ErrorMessage
								,@lErrorSeverity AS ErrorSeverity
								,@lErrorState AS ErrorState
					END
				END
				  
            END
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
			
				SET @lErrorNumber = 50001
				SET @lErrorMessage = ''Error updating database row, Please try again. TimeStamp verification failed.''
				SET @lErrorSeverity = 16
				SET @lErrorState = ''TRUE''
				
				IF(@pThrowError = 1)
					RAISERROR (@lErrorNumber, @lErrorSeverity, @lErrorState)
				ELSE
					SELECT
						 @lErrorNumber AS ErrorNumber
						,@lErrorMessage AS ErrorMessage
						,@lErrorSeverity AS ErrorSeverity
						,@lErrorState AS ErrorState
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE(), @lErrorNumber = ERROR_NUMBER()
		IF(@pThrowError = 1)
			RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		ELSE
			SELECT
				 @lErrorNumber AS ErrorNumber
				,@lErrorMessage AS ErrorMessage
				,@lErrorSeverity AS ErrorSeverity
				,@lErrorState AS ErrorState
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_VehicleFuelByGroup_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuelByGroup_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Vehicle Fuel information by group
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuelByGroup_Retrieve]
(
	 @pVehicleGroupId INT					--@pVehicleGroupId: PK of the table VehicleGroup
	,@pYear INT								--@pYear: Year
	,@pMonth INT							--@pMonth: Month	
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	SELECT
		 b.PlateId
		,b.[Name] AS  [VehicleName]
		,c.[Manufacturer] + '' - '' + c.[VehicleModel] AS VehicleModel
		,d.Name AS [FuelName]
		,f.[Symbol] AS [CurrencySymbol]
		,ISNULL(t.[Amount], CONVERT(DECIMAL(16,2),0)) AS [Amount]
		,ISNULL(t.[Liters],0) AS [Liters]
		,b.[VehicleId]
		,c.[DefaultFuelId]
		,t.[VehicleFuelId]
		,t.[RowVersion]		
	FROM [General].[VehiclesByGroup] a
		INNER JOIN [General].[Vehicles] b
			ON b.[VehicleId] = a.[VehicleId]
		INNER JOIN [General].[VehicleCategories] c
			ON c.[VehicleCategoryId] = b.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] d
			ON d.[FuelId] = c.[DefaultFuelId]
		INNER JOIN [General].[Customers] e
			ON e.[CustomerId] = b.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON f.[CurrencyId] = e.[CurrencyId]
		LEFT JOIN (
				SELECT
					 x.[VehicleId]
					,x.[Amount]
					,x.[Liters]
					,x.[VehicleFuelId]
					,x.[RowVersion]
				FROM [Control].[VehicleFuel] x
				WHERE x.[Year] = @pYear
				  AND x.[Month] = @pMonth) t
			ON t.VehicleId = b.VehicleId
	WHERE a.[VehicleGroupId] = @pVehicleGroupId
	  
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleSchedule_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleSchedule_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleSchedule information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleSchedule_Retrieve]
(
	 @pVehicleId INT						--@pVehicleScheduleId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	SELECT
		 a.[VehicleId]
		,a.[JsonSchedule]
		,a.[RowVersion]
	FROM [General].[VehicleSchedule] a
	WHERE a.[VehicleId] = @pVehicleId	 
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehicleSchedule_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleSchedule_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update VehicleSchedule information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleSchedule_AddOrEdit]
(
	 @pVehicleId INT						--@pVehicleScheduleId: PK of the table
	,@pJsonSchedule VARCHAR(MAX)			--@pJsonSchedule: Json Schedule
	,@pXmlSchedule VARCHAR(MAX)				--@pJsonSchedule: Json Schedule
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lVehicleScheduleId INT
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			SELECT
				@lVehicleScheduleId = a.[VehicleScheduleId] 
			FROM [General].[VehicleSchedule] a
			WHERE a.[VehicleId] = @pVehicleId
			
			IF (@lVehicleScheduleId IS NULL)
			BEGIN
				INSERT INTO [General].[VehicleSchedule]
						([VehicleId]
						,[JsonSchedule]
						,[XmlSchedule]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pVehicleId
						,@pJsonSchedule
						,@pXmlSchedule
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[VehicleSchedule]
					SET  [JsonSchedule] = @pJsonSchedule
						,[XmlSchedule] = @pXmlSchedule
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [VehicleId] = @pVehicleId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
			
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_VehiclesByLevel_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_VehiclesByLevel_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/21/2015
-- Description:	Retrieve List of Vehicles By Level
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_VehiclesByLevel_Retrieve]
(
	 @pId INT		--@pVehiclesId: List of vehicles 
	,@pLevel INT	--@pLevel: Level of group of vehicles
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		IF @pLevel = 2
			BEGIN
				SELECT 
					CONVERT(decimal, 0) [UnitId],
					b.[PlateId],
					b.[Name],
					a.[VehicleId],
					b.[IntrackReference] 
				FROM 
					[General].[VehiclesByGroup] a
					INNER JOIN [General].[Vehicles] b ON a.[VehicleId] = b.[VehicleId]
				WHERE
					a.[VehicleGroupId] = @pId
			END
		ELSE IF @pLevel = 3
			BEGIN
				SELECT 
					CONVERT(decimal, 0) [UnitId],
					a.[PlateId],
					a.[Name],
					a.[VehicleId],
					a.[IntrackReference]
				FROM 
					[General].[Vehicles] a
					INNER JOIN [General].[VehicleCostCenters] b ON a.[CostCenterId] = b.[CostCenterId]
				WHERE 
					b.[UnitId] = @pId
			END
		ELSE IF @pLevel = 4
			BEGIN
				SELECT 
					CONVERT(decimal, 0) [UnitId],
					[PlateId],
					[Name],
					[VehicleId],
					[IntrackReference] 
				FROM 
					[General].[Vehicles]
				WHERE
					[CostCenterId] = @pId
			END
	
    SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehiclesByGroup_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByGroup_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleGroup information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesByGroup_Retrieve]
(
	 @pVehicleGroupId INT					--@pVehicleGroupId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	
	SELECT
		 null AS [VehicleGroupId]
		,a.[VehicleId]
		,a.[Name] AS VehicleName
		,b.[Manufacturer] + '' - '' + b.[VehicleModel] + '', '' + c.[Name] + '' '' + CONVERT(varchar(50),b.Liters) + ''L'' AS VehicleType
		,a.[PlateId]
		,ROW_NUMBER() OVER (ORDER BY a.[VehicleId]) AS RowNumber
    FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCategories] b
			ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] c
			ON b.[DefaultFuelId] = c.[FuelId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND NOT EXISTS(
			SELECT 1
			FROM [General].[VehiclesByGroup] x
			WHERE x.[VehicleId] = a.[VehicleId]
			  AND x.[VehicleGroupId] = @pVehicleGroupId
		)
	UNION
	SELECT
		 a.[VehicleGroupId]
		,a.[VehicleId]
		,b.[Name] AS VehicleName
		,c.[Manufacturer] + '' - '' + c.[VehicleModel] + '', '' + d.[Name] + '' '' + CONVERT(varchar(50),c.Liters) + ''L'' AS VehicleType
		,b.[PlateId]
		,ROW_NUMBER() OVER (ORDER BY a.[RowVersion]) AS RowNumber
    FROM [General].[VehiclesByGroup] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]
		INNER JOIN [General].[VehicleCategories] c
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] d
			ON c.[DefaultFuelId] = d.[FuelId]
	WHERE a.[VehicleGroupId] = @pVehicleGroupId
	  AND b.[CustomerId] = @pCustomerId
	 
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_VehiclesByGroup_AddOrEdit]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByGroup_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update VehicleGroup information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesByGroup_AddOrEdit]
(
	 @pVehicleGroupId INT					--@pVehicleGroupId: PK of the table
	,@pXmlData VARCHAR(MAX)					--@pName: VehicleGroup Name
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [General].[VehiclesByGroup]
			WHERE [VehicleGroupId] = @pVehicleGroupId
			
			INSERT INTO [General].[VehiclesByGroup]
					([VehicleGroupId]
					,[VehicleId]
					,[InsertDate]
					,[InsertUserId])
			SELECT 
					 @pVehicleGroupId
					,Row.col.value(''./@VehicleId'', ''INT'') AS [VehicleId]
					,GETUTCDATE()
					,@pLoggedUserId
			FROM @lxmlData.nodes(''/xmldata/Vehicle'') Row(col)
			ORDER BY Row.col.value(''./@Index'', ''INT'')
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehiclesByGeoFence_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehiclesByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/17/2014
-- Description:	Retrieve VehicleGeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehiclesByGeoFence_Retrieve]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	
	SELECT
		 null AS [GeoFenceId]
		,a.[VehicleId]
		,a.[Name] AS VehicleName
		,b.[Manufacturer] + '' - '' + b.[VehicleModel] + '', '' + c.[Name] + '' '' + CONVERT(varchar(50),b.Liters) + ''L'' AS VehicleType
		,a.[PlateId]
		,ROW_NUMBER() OVER (ORDER BY a.[VehicleId]) AS RowNumber
    FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCategories] b
			ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] c
			ON b.[DefaultFuelId] = c.[FuelId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND NOT EXISTS(
			SELECT 1
			FROM [Efficiency].[VehiclesByGeoFence] x
			WHERE x.[VehicleId] = a.[VehicleId]
			  AND x.[GeoFenceId] = @pGeoFenceId
		)
	UNION
	SELECT
		 a.[GeoFenceId]
		,a.[VehicleId]
		,b.[Name] AS VehicleName
		,c.[Manufacturer] + '' - '' + c.[VehicleModel] + '', '' + d.[Name] + '' '' + CONVERT(varchar(50),c.Liters) + ''L'' AS VehicleType
		,b.[PlateId]
		,ROW_NUMBER() OVER (ORDER BY a.[RowVersion]) AS RowNumber
    FROM [Efficiency].[VehiclesByGeoFence] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]
		INNER JOIN [General].[VehicleCategories] c
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] d
			ON c.[DefaultFuelId] = d.[FuelId]
	WHERE a.[GeoFenceId] = @pGeoFenceId
	  AND b.[CustomerId] = @pCustomerId
	 
	
    SET NOCOUNT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_VehiclesByGeoFence_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehiclesByGeoFence_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/17/2014
-- Description:	Insert or Update Vehicle by GeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehiclesByGeoFence_AddOrEdit]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pXmlData VARCHAR(MAX)					--@pName: VehicleGroup Name
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Efficiency].[VehiclesByGeoFence]
			WHERE [GeoFenceId] = @pGeoFenceId
			
			INSERT INTO [Efficiency].[VehiclesByGeoFence]
					([GeoFenceId]
					,[VehicleId]
					,[InsertDate]
					,[InsertUserId])
			SELECT 
					 @pGeoFenceId
					,Row.col.value(''./@VehicleId'', ''INT'') AS [VehicleId]
					,GETUTCDATE()
					,@pLoggedUserId
			FROM @lxmlData.nodes(''/xmldata/Vehicle'') Row(col)
			ORDER BY Row.col.value(''./@Index'', ''INT'')
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Vehicles_Retrieve]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Vehicle information
-- ================================================================================================
Create PROCEDURE [General].[Sp_Vehicles_Retrieve]
(
	 @pVehicleId INT = NULL					--@pVehicleId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
	,@pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[VehicleId]
		,a.[PlateId]
		,a.[Name]
		,a.[CostCenterId]
		,c.[Name] AS CostCenterName
		,e.[UserId]
		,e.[Name] AS [EncryptedUserName]
		,a.[VehicleCategoryId]
		,d.[Manufacturer]+ ''-'' +d.VehicleModel + '', '' + d.[Type] AS CategoryType
        ,d.[Type] AS CategoryType
		,d.[Liters]
		,b.[Name] AS FuelName
		,b.[FuelId] as DefaultFuelId
		,a.[Active]
		,a.[Colour]
		,a.[Chassis]
		,a.[LastDallas]
		,a.[FreightTemperature]
		,a.[FreightTemperatureThreshold1]
		,a.[FreightTemperatureThreshold2]
		,a.[Predictive]
		,a.[AVL]
		,a.[PhoneNumber]
		,a.[Insurance]
		,a.[DateExpirationInsurance]
		,a.[CoverageType]
		,a.[NameEnterpriseInsurance]
		,a.[PeriodicityId]
		,a.[Cost]
		,g.[PartnerId]
		,g.[CapacityUnitId] AS PartnerCapacityUnitId
		,a.[AdministrativeSpeedLimit]
		,a.[CabinPhone]
		,a.[RowVersion]
    FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCostCenters] c
			ON a.[CostCenterId] = c.[CostCenterId]
		INNER JOIN [General].[VehicleCategories] d
			ON a.[VehicleCategoryId] = d.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] b
			ON d.[DefaultFuelId] = b.[FuelId]
		INNER JOIN [General].[Users] e
			ON e.[UserId] = 
			(SELECT TOP 1 [UserId]
			 FROM [General].[VehiclesByUser]
			 WHERE [VehicleId] = a.[VehicleId]
			 ORDER BY [InsertDate] DESC)
		INNER JOIN [General].[Customers] f
			ON a.[CustomerId] = f.[CustomerId]
		INNER JOIN [General].[CustomersByPartner] h
			ON h.[CustomerId] = f.[CustomerId]
		INNER JOIN [General].[Partners] g
			ON h.[PartnerId] = g.[PartnerId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND a.[Active] = 1
	  AND h.[IsDefault] = 1
	  AND (@pVehicleId IS NULL OR [VehicleId] = @pVehicleId)
	  AND (@pKey IS NULL 
				                OR a.[Name] like ''%''+@pKey+''%''
                OR a.[Chassis] like ''%''+@pKey+''%''
                OR a.[PlateId] like ''%''+@pKey+''%''
                OR b.[Name] like ''%''+@pKey+''%''
                OR c.[Name] like ''%''+@pKey+''%''
                OR d.[Type] like ''%''+@pKey+''%''
                OR CONVERT(VARCHAR(20),d.[Year]) like ''%''+@pKey+''%'')
	ORDER BY [VehicleId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Vehicles_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Delete Vehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Vehicles_Delete]
(
	@pVehicleId INT							--@pVehicleId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE General.VehiclesByUser
			WHERE [VehicleId]= @pVehicleId
			
			DELETE [General].[Vehicles]
			WHERE [VehicleId] = @pVehicleId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_Vehicles_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update Vehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Vehicles_AddOrEdit]
(
	 @pVehicleId INT = NULL							--@pVehicleId: PK of the table
	,@pPlateId VARCHAR(10)							--@pPlateId: Vehicle Plate Number
	,@pName VARCHAR(250)							--@pName: Vehicle Name
	,@pCostCenterId INT								--@pCostCenterId: FK of CostCenter
	,@pCustomerId INT								--@pCustomerId: FK of Customer
	,@pUserId INT									--@pUserId: FK of Users
	,@pVehicleCategoryId INT						--@pVehicleCategoryId: FK of Vehicle Category
	,@pCreditCardId INT = NULL						--@pCreditCardId: FK of CreditCard
	,@pActive BIT									--@pActive: Is Active
	,@pColour VARCHAR(50) = NULL     				--@pColour: Vehicle Colour
	,@pChassis VARCHAR(50) = NULL					--@pChassis: Vehicle Chassis or VIN
	,@pLastDallas VARCHAR(50) = NULL				--@pLastDallas: Vehicle Last Dallas 
	,@pFreightTemperature INT = NULL    			--@pFreightTemperature: Vehicle Freight Temperature
	,@pFreightTemperatureThreshold1 INT	= NULL		--@pFreightTemperatureThreshold1: Vehicle First Freight Temperature Threshold
	,@pFreightTemperatureThreshold2 INT	= NULL		--@pFreightTemperatureThreshold2: Vehicule Second Freight Temperature Threshold
	,@pPredictive BIT								--@pPredictive: Is Predictive
	,@pAVL VARCHAR(50) = NULL						--@PAVL: Vehicle AVL 
	,@pPhoneNumber VARCHAR(10) = NULL				--@pPhoneNumber: Phone Number
	,@pInsurance VARCHAR(20) = NULL					--@pInsurance: Insurance of Vehicle
	,@pDateExpirationInsurance DATE = NULL			--@pDateExpirationInsurance: Date Expiration Insurance
	,@pCoverageType VARCHAR(20)= NULL				--@pCoverageType: Coverage Type of Insurance
	,@pCabinPhone VARCHAR(50)= NULL					--@pCabinPhone: Cabin Phone Number
	,@pNameEnterpriseInsurance VARCHAR(50) = NULL	--@pNameEnterpriseInsurance: Company Name of Insurance
	,@pPeriodicityId INT = 0						--@@pTypeId: Periodicity of Insurance
	,@pCost DECIMAL(16,2) = NULL					--@pCost: Cost 
	,@pOdometer INT = NULL							--@pInitialOdometer: Odometer of Vehicles By Users
	,@pLoggedUserId INT								--@pLoggedUserId: Logged UserId that executes the operation
	,@pAdministrativeSpeedLimit INT					--@pAdministrativeSpeedLimit: Maximum speed limit for this car
	,@pRowVersion TIMESTAMP							--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lUserIdByVehicle INT 
            DECLARE @lVehiclesByUserId INT
            DECLARE @lResult INT
			DECLARE @lIntrackReference INT
			DECLARE @lDeviceReference INT							
			DECLARE @lAdmSpeedLimitPrev INT
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pVehicleId IS NULL)
			BEGIN
				INSERT INTO [General].[Vehicles]
						([PlateId]
						,[Name]
						,[CostCenterId]
						,[CustomerId]
						,[VehicleCategoryId]
						,[CreditCardId]
						,[Active]
						,[Colour]
						,[Chassis]
						,[LastDallas]
						,[FreightTemperature]
						,[FreightTemperatureThreshold1]
						,[FreightTemperatureThreshold2]
						,[Predictive]
						,[AVL]
						,[PhoneNumber]
						,[Insurance]
						,[DateExpirationInsurance]
						,[CoverageType]
						,[NameEnterpriseInsurance]
						,[PeriodicityId]
						,[Cost]
						,[AdministrativeSpeedLimit]
						,[CabinPhone]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(UPPER(@pPlateId)
						,@pName
						,@pCostCenterId
						,@pCustomerId
						,@pVehicleCategoryId
						,@pCreditCardId
						,@pActive
						,@pColour
						,@pChassis
						,@pLastDallas
						,@pFreightTemperature
						,@pFreightTemperatureThreshold1
						,@pFreightTemperatureThreshold2
						,@pPredictive
						,@pAVL
						,@pPhoneNumber
						,@pInsurance
						,@pDateExpirationInsurance
						,@pCoverageType
						,@pNameEnterpriseInsurance
						,@pPeriodicityId
						,@pCost
						,@pAdministrativeSpeedLimit
						,@pCabinPhone
						,GETUTCDATE()
						,@pLoggedUserId)
				IF (@pUserId IS NOT NULL)
				BEGIN 
					INSERT INTO [General].[VehiclesByUser]
							    ([VehicleId]
								,[UserId]
								,[InitialOdometer]
								,[InsertDate]
								,[InsertUserId])
					     VALUES (SCOPE_IDENTITY()
						        ,@pUserId
						        ,@pOdometer
						        ,GETUTCDATE()
						        ,@pLoggedUserId)
				END
			END
			ELSE
			BEGIN
				IF (@pUserId IS NOT NULL)
				BEGIN 
					SELECT 
						TOP 1 @lUserIdByVehicle = a.[UserId]
					   ,@lVehiclesByUserId = a.[VehiclesByUserId]
					FROM [General].[VehiclesByUser] a
					WHERE a.[VehicleId] = @pVehicleId
					ORDER BY a.[InsertDate] DESC
					
					IF (@lUserIdByVehicle <> @pUserId)
					BEGIN 
						UPDATE [General].[VehiclesByUser]
						SET [LastDateDriving] = GETUTCDATE()
							,[ModifyDate] = GETUTCDATE()
							,[ModifyUserId] = @pLoggedUserId
							,[EndOdometer] = @pOdometer
						WHERE  [VehiclesByUserId] = @lVehiclesByUserId
						
						INSERT INTO [General].[VehiclesByUser]
									([VehicleId]
									,[UserId]
									,[InitialOdometer]
									,[InsertDate]
									,[InsertUserId])
							 VALUES (@pVehicleId
							        ,@pUserId
							        ,@pOdometer
							        ,GETUTCDATE()
							        ,@pLoggedUserId)
					END 
				END 
				
				--Get Previous AdministrativeSpeedLimit to verify if changed
				SELECT @lAdmSpeedLimitPrev = [AdministrativeSpeedLimit]
					  ,@lIntrackReference = [IntrackReference]
					  ,@lDeviceReference = [DeviceReference]					  
				  FROM [General].[Vehicles]
				 WHERE [VehicleId] = @pVehicleId
				
				UPDATE [General].[Vehicles]
					SET  [PlateId] = UPPER(@pPlateId)
						,[Name] = @pName
						,[CostCenterId] = @pCostCenterId
						,[CustomerId] = @pCustomerId
						,[VehicleCategoryId] = @pVehicleCategoryId
						,[CreditCardId] = @pCreditCardId
						,[Active] = @pActive
						,[Colour] = @pColour
						,[Chassis] = @pChassis
						,[LastDallas] = @pLastDallas
						,[FreightTemperature] = @pFreightTemperature
						,[FreightTemperatureThreshold1] = @pFreightTemperatureThreshold1
						,[FreightTemperatureThreshold2] = @pFreightTemperatureThreshold2
						,[Predictive] = @pPredictive
						,[AVL] = @pAVL
						,[PhoneNumber] = @pPhoneNumber
						,[Insurance] = @pInsurance
						,[DateExpirationInsurance] = @pDateExpirationInsurance
						,[CoverageType] = @pCoverageType
						,[NameEnterpriseInsurance] = @pNameEnterpriseInsurance
						,[PeriodicityId] = @pPeriodicityId
						,[Cost] = @pCost
						,[AdministrativeSpeedLimit] = @pAdministrativeSpeedLimit
						,[CabinPhone] = @pCabinPhone
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [VehicleId] = @pVehicleId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
				--Replicate data in Atrack Database
				IF @pVehicleId IS NOT NULL and IsNull(@lAdmSpeedLimitPrev, 0) <> IsNull(@pAdministrativeSpeedLimit, 0)
				BEGIN
					IF @lIntrackReference = 0 
						Set @lIntrackReference = null
					IF @lDeviceReference = 0 
						Set @lDeviceReference = null
					--Aunque hubiera Intrack References, si no hay DeviceRefence no se ejecuta la replicación
					IF @lDeviceReference is not null
					BEGIN
						Exec General.Sp_Vehicles_Replication @pVehicleId, @pCustomerId, @lIntrackReference, @lDeviceReference, @pAdministrativeSpeedLimit, @pResult = @lResult output, @pErrorDesc = @lErrorMessage output
						IF @lResult = -1 
						BEGIN
							--Send Email to Admin
							DECLARE @lTo varchar(100) = '''',
									@lSubject varchar(300),
									@lMessage varchar(1000),
									@lEmail varchar(100)
							
							Set @lTo = General.Fn_GetEmailsRole(''SUPER_ADMIN'', 0, 100)
							
							IF LEN(@lTo) = 0
								Set @lTo = ''coinca@coinca.tv''
								
							Set @lSubject = ''Error replicando datos de Vehículo en Base de Datos Atrack''
							Set @lMessage = ''Durante la replicación del Vehículo Id '' + convert(varchar, @pVehicleId) + '' Placa No '' + UPPER(@pPlateId) + 
											'' se presentó el siguiente error: '' + @lErrorMessage + ''. Los datos en la base de datos Atrack quedaron desactualizados.''
							
							Exec General.Sp_Email_AddOrEdit null, @lTo, null, null, @lSubject, @lMessage, 0, 0, @pLoggedUserId, null 
						END
					END
				END --End Replication
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlVehicleDriver_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlVehicleDriver_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/12/2014
-- Description:	Retrieve PassControlVehicleDriver information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_PassControlVehicleDriver_Retrieve]
(
	 @pId VARCHAR(MAX), 
	 @pStartDate VARCHAR(MAX),  
	 @pEndDate VARCHAR(MAX) 
)
AS
BEGIN
	
	SET NOCOUNT ON

    DECLARE @separator varchar(max) = '',''
    DECLARE @Splited table(item varchar(max))
    SET @pId = REPLACE(@pId,@separator,''''''),('''''')
    SET @pId = ''select * from (values(''''''+@pId+'''''')) as V(A)'' 
    INSERT INTO @Splited
    EXEC (@pId)
    

	SELECT 
		d.[VehicleId] [VehicleId], 
		d.[UserId] [DriverId], 
		u.[Name] [EncryptedDriverName],
		du.[Dallas] [EncryptedDallas],
		ISNULL(d.[InsertDate], GETUTCDATE()) [InDateTime], 
		ISNULL(d.[LastDateDriving], GETUTCDATE()) [OutDateTime]
	FROM 
		[General].[VehiclesByUser] d
		INNER JOIN [General].[Users] u ON d.[UserId] = u.[UserId]
		INNER JOIN [General].[DriversUsers] du ON u.[UserId] = du.[UserId]
	WHERE 
		d.[VehicleId] IN (select item from @Splited) AND 
		(
				(ISNULL(d.InsertDate, GETUTCDATE())  >= @pStartDate AND ISNULL(d.InsertDate, GETUTCDATE()) < @pEndDate) 
			OR
				(ISNULL(d.LastDateDriving, GETUTCDATE()) >= @pStartDate AND ISNULL(d.LastDateDriving, GETUTCDATE()) < @pEndDate)
			OR
				(ISNULL(d.InsertDate, GETUTCDATE()) < @pStartDate AND ISNULL(d.LastDateDriving, GETUTCDATE()) >= @pStartDate)
		)

    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlUnitInfo_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlUnitInfo_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/12/2014
-- Description:	Retrieve PassControlUnitInfo information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_PassControlUnitInfo_Retrieve]
(
	 @pId INT 
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		v.[VehicleId] [VehicleId], 
		v.[IntrackReference] [IntrackReference], 
		v.[PlateId] [PlateId], 
		v.[CostCenterId] [CostCenterId], 
		c.[Name] [CostCenterName]
	FROM 
		[General].[Vehicles] v
		INNER JOIN [General].[VehicleCostCenters] c ON v.[CostCenterId] = c.[CostCenterId]
		INNER JOIN [General].[VehiclesByGroup] g ON v.[VehicleId] = g.[VehicleId]
		INNER JOIN [General].[VehicleUnits] u ON c.[UnitId] = u.[UnitId]
	WHERE 
		u.[UnitId] in (@pId)
		
    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlSubUnitInfo_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlSubUnitInfo_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/12/2014
-- Description:	Retrieve PassControlUnitInfo information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_PassControlSubUnitInfo_Retrieve]
(
	 @pId INT 
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		v.[VehicleId] [VehicleId], 
		v.[IntrackReference] [IntrackReference], 
		v.[PlateId] [PlateId], 
		v.[CostCenterId] [CostCenterId], 
		c.[Name] [CostCenterName]
	FROM 
		[General].[Vehicles] v
		INNER JOIN [General].[VehicleCostCenters] c ON v.[CostCenterId] = c.[CostCenterId]
		INNER JOIN [General].[VehiclesByGroup] g ON v.[VehicleId] = g.[VehicleId]
	WHERE 
		c.[CostCenterId] in (@pId)
		
    SET NOCOUNT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlGroupInfo_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlGroupInfo_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
--modificado

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/12/2014
-- Description:	Retrieve PassControlVehicleInfo information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_PassControlGroupInfo_Retrieve]
(
	 @pId INT 
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		v.[VehicleId] [VehicleId], 
		v.[IntrackReference] [IntrackReference], 
		v.[PlateId] [PlateId], 
		v.[CostCenterId] [CostCenterId],
		c.[Name] [CostCenterName]
	FROM 
		[General].[Vehicles] v
		INNER JOIN [General].[VehicleCostCenters] c ON v.[CostCenterId] = c.[CostCenterId]
		INNER JOIN [General].[VehiclesByGroup] g ON v.[VehicleId] = g.[VehicleId]
	WHERE 
		g.VehicleGroupId in (@pId)
		
		

    SET NOCOUNT OFF
END




' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_OdometerVsRoute_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_OdometerVsRoute_Alarm]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/30/2015
-- Description:	Stored Procedure that execute the alarm Add Alarm for Expiration of Fleet Card
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_OdometerVsRoute_Alarm]
AS
BEGIN 

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @Message VARCHAR(MAX)
        DECLARE @RowNum INT
		
		DECLARE @tData TABLE
			(OdometerVsRouteAlarmId INT NULL,
			VehicleId INT,
			CustomerId INT,
			Active BIT,
			IntrackReference INT,
			Device INT,
			GPSDateTime DATETIME,
			Odometer FLOAT,
			LocalDate DATETIME)
		
		DECLARE @tEmails TABLE(
			[Id] INT IDENTITY(1,1) NOT NULL,
			OdometerVsRouteAlarmId INT,
			VehicleId INT,
			PlateId VARCHAR(10),
			CustomerId INT,
			kmRoute FLOAT,
			Email VARCHAR(MAX))

		--OBTIENE EL LISTADO DE VEHICULOS
		INSERT INTO	@tData
		(OdometerVsRouteAlarmId,
		VehicleId,
		CustomerId,
		Active,
		IntrackReference,
		Device,
		GPSDateTime,
		Odometer,
		LocalDate)
	SELECT DISTINCT
		COUNT(f.VehicleId),
		a.VehicleId,
		a.CustomerId,
		a.Active,
		a.IntrackReference,
		d.[Device],
		e.GPSDateTime,
		e.Odometer,
		DATEADD(HOUR, CONVERT(INT, g.Value), GETUTCDATE()) LocalDate
	FROM 
		General.Vehicles a
		INNER JOIN Composiciones b ON a.IntrackReference = b.vehiculo
		INNER JOIN DispositivosAVL c ON b.dispositivo = c.dispositivo
		INNER JOIN [dbo].[Devices] d ON c.numeroimei = d.[UnitID]
		INNER JOIN [dbo].[Report_Last] e ON d.[Device] = e.[Device]
		LEFT JOIN General.OdometerVsRouteAlarm f ON a.VehicleId = a.VehicleId AND 
		CONVERT(VARCHAR(2), MONTH(DATEADD(HOUR, -6, GETUTCDATE()))) + ''-'' +  CONVERT(VARCHAR(2), DAY(DATEADD(HOUR, -6, GETUTCDATE()))) + ''-'' + CONVERT(VARCHAR(2), DAY(DATEADD(HOUR, -6, GETUTCDATE()))) = 
		CONVERT(VARCHAR(2), MONTH(f.LocalStartDate)) + ''-'' +  CONVERT(VARCHAR(2), DAY(f.LocalStartDate)) + ''-'' + CONVERT(VARCHAR(2), DAY(f.LocalStartDate))
		INNER JOIN General.ParametersByCustomer g ON a.CustomerId = g.CustomerId
	WHERE
		g.ResuourceKey = ''CUSTOMER_UTC''
	group BY f.VehicleId,
		a.VehicleId,
		a.CustomerId,
		a.Active,
		a.IntrackReference,
		d.[Device],
		e.GPSDateTime,
		e.Odometer,
		g.Value

	--INSERTA LOS DATOS EN ODOMETRO POR TABLA
	INSERT INTO General.OdometerVsRouteAlarm
		(VehicleId, 
		CustomerId, 
		Active, 
		OdometerStart, 
		OdometerEnd, 
		kmRoute, 
		AlarmSent, 
		LocalStartDate, 
		InsertDate)
	SELECT 
		VehicleId, 
		CustomerId, 
		Active, 
		Odometer, 
		Odometer, 
		0, 
		0, 
		LocalDate, 
		GETUTCDATE() 
	FROM 
		@tData 
	WHERE 
		ISNULL(OdometerVsRouteAlarmId, 0) = 0

	--ACTUALIZA LOS DATOS EN ODOMETRO POR TABLA
	UPDATE b
	SET b.CustomerId = a.CustomerId,
		b.OdometerEnd = a.Odometer,
		b.LocalEndDate = a.LocalDate,
		b.ModifyDate = GETUTCDATE()
	 FROM @tData a
		INNER JOIN General.OdometerVsRouteAlarm b 
			ON a.VehicleId = b.VehicleId AND 
			CONVERT(VARCHAR(2), MONTH(a.LocalDate)) + ''-'' +  CONVERT(VARCHAR(2), DAY(a.LocalDate)) + ''-'' + CONVERT(VARCHAR(2), DAY(a.LocalDate)) = 
			CONVERT(VARCHAR(2), MONTH(b.LocalStartDate)) + ''-'' +  CONVERT(VARCHAR(2), DAY(b.LocalStartDate)) + ''-'' + CONVERT(VARCHAR(2), DAY(b.LocalStartDate)) 
	WHERE 
		ISNULL(a.OdometerVsRouteAlarmId, 0) <> 0

	--SE ACTUALIZA EL KILOMETRAJE DEFINIDO PARA LAS RUTAS DE CADA VEHICULO
	DECLARE @lDay VARCHAR(1)
	SELECT @lDay = CASE
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 1 THEN ''D''
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 2 THEN ''L''
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 3 THEN ''K''
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 4 THEN ''M''
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 5 THEN ''J''
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 6 THEN ''V''
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 7 THEN ''S''
		END

		UPDATE a 
		SET a.kmRoute = b.Distance
		FROM General.OdometerVsRouteAlarm a
			INNER JOIN 
				(SELECT a.VehicleId VehicleId, 
			sum(b.Distance) Distance
			 FROM Efficiency.VehicleByRoute a
			INNER JOIN Efficiency.Routes b ON a.RouteId = b.RouteId
		WHERE a.Days LIKE ''%''+@lDay+''%''
		GROUP BY a.VehicleId) b
			ON a.VehicleId = b.VehicleId AND 
			CONVERT(VARCHAR(2), MONTH(DATEADD(HOUR, -6, GETUTCDATE()))) + ''-'' +  CONVERT(VARCHAR(2), DAY(DATEADD(HOUR, -6, GETUTCDATE()))) + ''-'' + CONVERT(VARCHAR(2), DAY(DATEADD(HOUR, -6, GETUTCDATE()))) = 
			CONVERT(VARCHAR(2), MONTH(a.LocalStartDate)) + ''-'' +  CONVERT(VARCHAR(2), DAY(a.LocalStartDate)) + ''-'' + CONVERT(VARCHAR(2), DAY(a.LocalStartDate))
			
	
	--CARGA LA TABLA CON EL LISTADO DE CORREOS QUE SE DEBEN ENVIAR
		INSERT INTO @tEmails (
			OdometerVsRouteAlarmId,
			VehicleId,
			PlateId,
			CustomerId,
			kmRoute,
			Email)
		SELECT DISTINCT
			a.OdometerVsRouteAlarmId,
			a.VehicleId,
			v.PlateId,
			a.CustomerId,
			a.kmRoute,
			g.Email	
		FROM 
			General.OdometerVsRouteAlarm a
			INNER JOIN General.Vehicles v ON a.VehicleId = v.VehicleId
			INNER JOIN [General].CustomerUsers c ON a.CustomerId = c.CustomerId
			INNER JOIN [General].[Users] d ON c.[UserId] = d.[UserId]
			INNER JOIN [AspNetUsers] g ON d.[AspNetUserId] = g.[Id]
			INNER JOIN [AspNetUserRoles] e ON g.[Id] = e.[UserId]
			INNER JOIN [AspNetRoles] f ON e.[RoleId] = f.[Id]
		WHERE AlarmSent = 0 AND
			a.kmRoute > 0 AND 
			a.OdometerEnd - OdometerStart > kmRoute AND 
			a.InsertDate > DATEADD(DAY, -2, GETUTCDATE())
			
		--RECORRE EL LISTADO DE CORREOS PARA ENVIAR ALARMAS
		DECLARE @lOdometerVsRouteAlarmId INT
		DECLARE @lVehicleId INT
		DECLARE @lPlateId VARCHAR(10)
		DECLARE @lCustomerId INT
		DECLARE @lkmRoute FLOAT
		DECLARE @lEmail VARCHAR(MAX)

		SELECT @RowNum = Count(*) From @tEmails
		
		SELECT @Message = a.[Message] FROM [General].[Values] a 
			INNER JOIN [General].[Types] b ON a.[TypeId] = b.[TypeId]
		WHERE 
			b.[Code] = ''CUSTOMER_ALARM_TRIGGER_ODOMETER_VS_ROUTE''

		WHILE @RowNum > 0
		BEGIN

			SELECT 
				@lOdometerVsRouteAlarmId = OdometerVsRouteAlarmId, 
				@lVehicleId = VehicleId, 
				@lPlateId = PlateId,
				@lCustomerId = CustomerId, 
				@lkmRoute = kmRoute, 
				@lEmail = Email 
			FROM @tEmails 
			WHERE Id = @RowNum
			
			DECLARE @lMessage VARCHAR(MAX) = @Message
			SET @lMessage = REPLACE(@lMessage, ''%PlateId%'', @lPlateId)
			SET @lMessage = REPLACE(@lMessage, ''%KmRoute%'', @lkmRoute)

			--SE INSERTA EL CORREO A ENVIAR
			INSERT INTO General.EmailEncryptedAlarms
				([To],
				Subject,
				Message,
				Sent,
				InsertDate)
			VALUES(@lEmail,
				''Alarma de Exceso de Kilometraje'',
				@lMessage,
				0,
				GETUTCDATE())
			
			UPDATE General.OdometerVsRouteAlarm 
			SET AlarmSent = 1
			WHERE OdometerVsRouteAlarmId = @lOdometerVsRouteAlarmId

		END
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF

END

' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 02/20/2015
-- Description:	Retrieve Odometer Traveled Report By Vehicle
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pKey VARCHAR(800) = NULL			--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		 a.[VehicleId]
		,b.PlateId
		,MIN(a.[Date]) AS [TrxMinDate]
		,MAX(a.[Date]) AS [TrxMaxDate]
		,MIN(COALESCE(a.FixedOdometer, a.Odometer, 0)) AS [TrxMinOdometer]
		,MAX(COALESCE(a.FixedOdometer, a.Odometer, 0)) AS [TrxMaxOdometer]
		,b.CustomerId
	FROM [Control].[Transactions] a WITH(READUNCOMMITTED)
			INNER JOIN [General].[Vehicles] b
		ON b.[VehicleId] = a.[VehicleId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND a.[IsFloating] = 0
	  AND a.[IsReversed] = 0
	  AND a.[IsDuplicated] = 0
	  AND b.[Active] = 1
	  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,a.[Date]) = @pMonth
								AND DATEPART(yyyy,a.[Date]) = @pYear) OR
									(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND a.[Date] BETWEEN @pStartDate AND @pEndDate))
	  AND (@pKey IS NULL 
				OR b.[PlateId] like ''%''+@pKey+''%'')
	GROUP BY a.[VehicleId]
			,b.PlateId
			,b.CustomerId
	
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_HobbsMeterPerformanceReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_HobbsMeterPerformanceReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 02/20/2015
-- Description:	Retrieve Hobbs meter Performance Report By Vehicle
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_HobbsMeterPerformanceReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pKey VARCHAR(800) = NULL			--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		 a.[VehicleId]
		,b.PlateId
		,MIN(a.[Date]) AS [TrxMinDate]
		,MAX(a.[Date]) AS [TrxMaxDate]
		,MIN(COALESCE(a.FixedOdometer, a.Odometer, 0)) AS [TrxMinOdometer]
		,MAX(COALESCE(a.FixedOdometer, a.Odometer, 0)) AS [TrxMaxOdometer]
	FROM [Control].[Transactions] a WITH(READUNCOMMITTED)
			INNER JOIN [General].[Vehicles] b
		ON b.[VehicleId] = a.[VehicleId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND a.[IsFloating] = 0
	  AND a.[IsReversed] = 0
	  AND a.[IsDuplicated] = 0
	  AND b.[Active] = 1
	  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,a.[Date]) = @pMonth
								AND DATEPART(yyyy,a.[Date]) = @pYear) OR
									(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND a.[Date] BETWEEN @pStartDate AND @pEndDate))
	  AND (@pKey IS NULL 
				OR b.[PlateId] like ''%''+@pKey+''%'')
	GROUP BY a.[VehicleId]
			,b.PlateId
	
	
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ==========================================================
-- Author:		Danilo Hidalgo
-- Create date: 02/23/2015
-- Description:	Preventive Maintenance By Vehicle information
-- ===========================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]
	@pVehicleId INT
AS
BEGIN 
	SET NOCOUNT ON;

	SELECT 
		a.[PreventiveMaintenanceId],
		a.[PreventiveMaintenanceCatalogId], 
		b.[Description] [PreventiveMaintenanceDescription],
		a.[VehicleId], 
		a.[PreventiveMaintenanceCatalogId], 
		a.[LastReviewOdometer], 
		a.[NextReviewOdometer], 
		a.[LastReviewDate], 
		a.[NextReviewDate], 
		a.[Registred], 
		a.[AlarmSent], 
		a.[InsertUserId], 
		a.[InsertDate], 
		a.[ModifyUserId], 
		a.[ModifyDate], 
		a.[RowVersion] 
	FROM 
		[General].[PreventiveMaintenanceByVehicle] a
		INNER JOIN [General].[PreventiveMaintenanceCatalog] b ON a.[PreventiveMaintenanceCatalogId] = b.[PreventiveMaintenanceCatalogId]
	WHERE a.[Registred] = 0 AND 
		a.[IsDeleted] = 0 AND 
		a.[VehicleId] = @pVehicleId
	
	SET NOCOUNT OFF;
END







' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceByVehicle_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceByVehicle_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 10/Oct/2014
-- Description:	Delete Preventive Maintenance By Vehicle information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_Delete]
	@pPreventiveMaintenanceId INT
AS
BEGIN 
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		UPDATE [General].[PreventiveMaintenanceByVehicle]
		SET [IsDeleted] = 1
		WHERE [PreventiveMaintenanceId] = @pPreventiveMaintenanceId
		
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END ' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Danilo Hidalgo.
-- Create date: 02/23/2015
-- Description:	Insert Preventive Maintenance By Vehicle information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]
	 @pVehicleId INT = NULL
	 ,@pPreventiveMaintenanceCatalogId INT = NULL
	 ,@pLastReviewOdometer FLOAT = NULL
	 ,@pLastReviewDate DATETIME = NULL
	 ,@pLoggedUserId INT
	 ,@pRowVersion TIMESTAMP 
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY
		DECLARE @lLines INT = NULL
		DECLARE @lFrequencyOdometer FLOAT = NULL
		DECLARE @lFrequencyDate INT = NULL
		DECLARE @lNextReviewOdometer FLOAT = NULL
		DECLARE @lNextReviewMonth DATETIME = NULL
		DECLARE @lNextReviewDate DATETIME = NULL
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0
        DECLARE @lFrequencyTypeId INT
        DECLARE @lFrequency INT
		        
		IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END		

		SELECT @lLines = COUNT(*) FROM General.PreventiveMaintenanceByVehicle WHERE VehicleId = @pVehicleId AND PreventiveMaintenanceCatalogId = @pPreventiveMaintenanceCatalogId 
		IF @lLines = 0
		BEGIN
		
			SELECT 
				@lFrequencyOdometer = FrequencyKm,
				@lFrequencyDate = FrequencyMonth, 
				@lNextReviewDate = FrequencyDate
			FROM 
				General.PreventiveMaintenanceCatalog 
			WHERE 
				PreventiveMaintenanceCatalogId = @pPreventiveMaintenanceCatalogId
				
			IF @lNextReviewDate IS NULL 
				SET @lNextReviewDate = DATEADD(MONTH, @lFrequencyDate, @pLastReviewDate)
			ELSE
				SET @lNextReviewDate = DATEADD(YEAR, 1, @pLastReviewDate)
			
			SET @lNextReviewOdometer = @pLastReviewOdometer + @lFrequencyOdometer
			
			INSERT INTO General.PreventiveMaintenanceByVehicle
				(VehicleId
				,PreventiveMaintenanceCatalogId
				,LastReviewOdometer
				,LastReviewDate
				,NextReviewOdometer
				,NextReviewDate
				,IsDeleted
				,Registred
				,AlarmSent
				,InsertUserId
				,InsertDate)
			VALUES
				(@pVehicleId
				,@pPreventiveMaintenanceCatalogId
				,@pLastReviewOdometer
				,@pLastReviewDate
				,@lNextReviewOdometer
				,@lNextReviewDate
				,0
				,0
				,0
				,@pLoggedUserId
				,GETUTCDATE())
		END
		
			SET @lRowCount = @@ROWCOUNT
		        
			IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			--IF @lRowCount = 0
			--BEGIN
			--	RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			--END
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END 


' 
END
GO

/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalogList_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalogList_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ==========================================================
-- Author:		Danilo Hidalgo
-- Create date: 02/23/2015
-- Description:	Preventive Maintenance Catalog not Asigned to Vehicle List information
-- ===========================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCatalogList_Retrieve]
	@pCustomerId INT,
	@pVehicleId INT
AS
BEGIN 
	SET NOCOUNT ON;
		SELECT 
			a.[PreventiveMaintenanceCatalogId], 
			a.[Description] [PreventiveMaintenanceDescription]
		FROM 
			[General].[PreventiveMaintenanceCatalog] a
		WHERE 
			a.CustomerId = @pCustomerId AND
			a.PreventiveMaintenanceCatalogId NOT IN 
			(SELECT DISTINCT b.PreventiveMaintenanceCatalogId
			FROM General.PreventiveMaintenanceByVehicle b
			WHERE 
				b.IsDeleted = 0 AND
				b.VehicleId = @pVehicleId)
	SET NOCOUNT OFF;
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_PerformanceByVehicleReport_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PerformanceByVehicleReport_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes.
-- Create date: 03/02/2015
-- Description:	Retrieve PerformanceByVehicleReport information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PerformanceByVehicleReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pVehicleId INT = NULL				--@pVehicleId: Id of the vehicle
	,@pManufacturer varchar(500) = NULL	
	,@pVehicleModel varchar(500) = NULL	
	,@pVehicleYear int = NULL	
	,@pType varchar(500) = NULL		
	,@pCylinderCapacity int = NULL		
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	Declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	Declare @pInitialDate2 datetime
	declare @pFinalDate2 datetime
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT(''0'' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT(''0'' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end

DECLARE @tTable2 Table(
	PlateId VARCHAR(10),
	VehicleId int,
	InitialOdometer decimal,
	FinalOdometer decimal,
	Liters decimal,
	Manufacturer varchar(300),
	VehicleModel varchar(300),
    [Year] int,
	[Type] varchar(300),
	CylinderCapacity int,
	VehicleCategoryId INT,
	TransactionMonth INT,
	TransactionYear INT
)	

DECLARE @tTable Table(
	cte_start_date datetime
)	

;WITH CTE AS
(
    SELECT @pInitialDate AS cte_start_date
    UNION ALL
    SELECT DATEADD(MONTH, 1, cte_start_date)
    FROM CTE
    WHERE DATEADD(MONTH, 1, cte_start_date) <= @pFinalDate   
)
insert INTO @tTable  SELECT * FROM CTE
 
 DECLARE @cte_sd datetime

	DECLARE myCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT cte_start_date AS ''ID'' 
		FROM @tTable
	OPEN myCursor
	FETCH NEXT FROM myCursor INTO @cte_sd
	WHILE @@FETCH_STATUS = 0 BEGIN
		PRINT @cte_sd
		
		set @pInitialDate2=  CAST(
							  CAST(YEAR(@cte_sd) AS VARCHAR(4)) +
							  RIGHT(''0'' + CAST(MONTH(@cte_sd) AS VARCHAR(2)), 2) +
							  RIGHT(''0'' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate2=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate2)+1,0))
		
			INSERT INTO @tTable2
			SELECT
			   v.PlateId,
			   v.VehicleId,
			   ISNULL((SELECT TOP 1 Odometer FROM [Control].Transactions  WHERE [DATE] <= @pInitialDate2 and VehicleId=v.VehicleId ORDER BY [DATE] DESC),0) as InitialOdometer,
			   ISNULL((SELECT TOP 1 Odometer FROM [Control].Transactions  WHERE [DATE] BETWEEN @pInitialDate2 AND @pFinalDate2 and VehicleId=v.VehicleId ORDER BY [DATE] ASC),0) as FinalOdometer,
			   ISNULL((SELECT SUM(Liters) FROM [Control].Transactions  WHERE [DATE] BETWEEN @pInitialDate2 AND @pFinalDate2 and VehicleId=v.VehicleId),0) as Liters,
				vc.Manufacturer,
				vc.VehicleModel,
				vc.[Year],
				vc.[Type],
				vc.CylinderCapacity,
				vc.VehicleCategoryId
				,MONTH(@pInitialDate2) as TransactionMonth
				,YEAR(@pFinalDate2) as TransactionYear
			   FROM General.Vehicles v 
			   INNER JOIN General.VehicleCategories vc ON vc.VehicleCategoryId=v.VehicleCategoryId
			   INNER JOIN Control.Transactions t ON v.VehicleId=t.VehicleId
			   WHERE V.CustomerId=@pCustomerId and v.VehicleId=isnull(@pVehicleId,v.VehicleId)
			   and MONTH(t.[Date])=@pInitialDate2 AND YEAR(t.[Date])=@pInitialDate2

		FETCH NEXT FROM myCursor INTO @cte_sd

	END

CLOSE myCursor
DEALLOCATE myCursor

SELECT VehicleId,PlateId,InitialOdometer,FinalOdometer,[TransactionMonth],[TransactionYear],cast((FinalOdometer-InitialOdometer) as decimal) AS KMTraveled,SUM(Liters) as Liters,
case when SUM(Liters)>0 
then 
	cast((SUM(FinalOdometer-InitialOdometer)/SUM(Liters)) as decimal) 
else 
	cast(0 as decimal)
end	
	 AS Performance,
	 @pCustomerId

FROM @tTable2 WHERE (@pManufacturer IS NULL OR Manufacturer = @pManufacturer)
			   AND (@pVehicleModel IS NULL OR VehicleModel= @pVehicleModel)
			   AND (@pVehicleYear IS NULL OR  [Year] =@pVehicleYear)
			   AND (@pType IS NULL OR [Type] =@pType)
			   AND (@pCylinderCapacity IS NULL OR CylinderCapacity=@pCylinderCapacity)
  GROUP BY VehicleId,PlateId,InitialOdometer,FinalOdometer,[TransactionMonth],[TransactionYear]
			  
		
	SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesByVehicleList_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesByVehicleList_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/20/2014
-- Description:	Retrieve RoutesByVehicleList information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_RoutesByVehicleList_Retrieve]
(
	 @pVehicleId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		r.[RouteId] [Id], 
		r.[Name] [Name]
	FROM 
		[Efficiency].[Routes] r
		INNER JOIN [Efficiency].[VehicleByRoute] v ON r.[RouteId] = v.[RouteId]
	WHERE 
		v.[VehicleId] = 7 
	ORDER BY r.Name
	
    SET NOCOUNT OFF
END


' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/20/2014
-- Description:	Retrieve VehicleByRoute information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]
(
	 @pVehicleId INT = NULL
	,@pRouteId INT = NULL
	,@pCustomerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		vr.[RouteId] [Id],
		r.[Name]
	FROM 
		[Efficiency].[VehicleByRoute] vr
		INNER JOIN [General].[Vehicles] v ON vr.[VehicleId] = v.[VehicleId]
		INNER JOIN [Efficiency].[Routes] r ON vr.[RouteId] = r.[RouteId]	
	WHERE 
		v.[CustomerId] = r.[CustomerId] AND 
		v.[CustomerId] = @pCustomerId AND 
		v.[IntrackReference] = @pVehicleId AND 
		(@pRouteId IS NULL OR vr.[RouteId] = @pRouteId) 
	ORDER BY 
		r.[Name] DESC
	
    SET NOCOUNT OFF
END



' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_RoutePointTimeStop_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_RoutePointTimeStop_Alarm]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 02/03/2015
-- Description:	Stored Procedure that execute the alarm Add Alarm for point time stop
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_RoutePointTimeStop_Alarm]
AS
BEGIN 

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @Message VARCHAR(MAX)
        DECLARE @RowNum INT

		DECLARE @tReports TABLE
			([VehicleId] INT,
			[Device] INT,
			[GPSDateTime] DATETIME,
			[Longitude] FLOAT,
			[Latitude] FLOAT,
			[VSSSpeed] FLOAT)

		DECLARE @tStopTemp TABLE(
			[StopsTempId] INT,
			[VehicleId] INT,
			[GPSDateTimeStart] DATETIME,
			[GPSDateTimeEnd] DATETIME,
			[SpeedStart] FLOAT,
			[SpeedEnd] FLOAT,
			[Longitude] FLOAT,
			[Latitude] FLOAT)

		DECLARE @tStops TABLE
			([VehicleId] INT,
			[VehiclePlateId] VARCHAR(10),
			[VehicleName] VARCHAR(250),
			[CustomerId] INT,
			[GPSDateTimeStart] DATETIME,
			[GPSDateTimeEnd] DATETIME,
			[Longitude] FLOAT,
			[Latitude] FLOAT,
			[RouteId] INT NULL,
			[RouteName] VARCHAR(50) NULL,
			[RouteDays] VARCHAR(10) NULL,
			[RoutePointId] INT NULL,
			[RoutePointName] VARCHAR(250) NULL,
			[RoutePointTime] TIME(7) NULL,
			[RoutePointStopTime] INT NULL,
			[RoutePointLongitude] FLOAT NULL,
			[RoutePointLatitude] FLOAT NULL,
			[Alarm] BIT,
			[Send] BIT)

		DECLARE @tSendMails TABLE
			([Id] INT IDENTITY(1,1) NOT NULL,
			[StopId] INT,
			[VehiclePlateId] VARCHAR(10),
			[RouteName] VARCHAR(50),
			[RoutePointName] VARCHAR(250),
			[RoutePointStopTime] INT,
			[RealStopTime] INT,
			[Email] VARCHAR(MAX))

		INSERT INTO @tReports
			([VehicleId],
			[Device],
			[GPSDateTime],
			[Longitude],
			[Latitude],
			[VSSSpeed])
		SELECT 
			e.[VehicleId],
			a.[Device], 
			a.[GPSDateTime], 
			a.[Longitude], 
			a.[Latitude], 
			a.[VSSSpeed]
		FROM 
			[dbo].[Report_Last] a
			INNER JOIN [dbo].[Devices] b ON a.[Device] = b.[Device]
			INNER JOIN [dbo].[DispositivosAVL] c ON b.[UnitID] = c.[numeroimei]
			INNER JOIN [dbo].[Composiciones] d ON d.[dispositivo] = c.[dispositivo]
			INNER JOIN [General].[Vehicles] e ON d.[vehiculo] = e.[IntrackReference]

		UPDATE b 
		SET b.[GPSDateTimeEnd] = a.[GPSDateTime],
			b.[GPSDateTimeStart] = a.[GPSDateTime],
			b.[SpeedStart] = a.[VSSSpeed],
			b.[SpeedEnd] = a.[VSSSpeed],
			b.[ModifyUserId] = 0,
			b.[ModifyDate] = GETUTCDATE() 
		FROM @tReports a
			INNER JOIN [Efficiency].[StopsTemp] b ON a.[VehicleId] = b.[VehicleId]
		WHERE
			a.[VSSSpeed] = 0 AND
			b.[SpeedStart] <> 0

		UPDATE b 
		SET b.[GPSDateTimeEnd] = a.[GPSDateTime],
			b.[SpeedEnd] = a.[VSSSpeed],
			b.[ModifyUserId] = 0,
			b.[ModifyDate] = GETUTCDATE() 
		FROM @tReports a
			INNER JOIN [Efficiency].[StopsTemp] b ON a.[VehicleId] = b.[VehicleId]
		WHERE
			a.[VSSSpeed] > 0 AND
			b.[SpeedStart] = 0

		INSERT INTO [Efficiency].[StopsTemp]
			([VehicleId],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[SpeedStart],
			[SpeedEnd],
			[Longitude],
			[Latitude],
			[InsertUserId],
			[InsertDate])
		SELECT a.[VehicleId],
			a.[GPSDateTime],
			a.[GPSDateTime],
			a.[VSSSpeed],
			a.[VSSSpeed],
			a.[Longitude],
			a.[Latitude],
			0, GETUTCDATE()
		FROM @tReports a 
		WHERE [VehicleId] NOT IN (SELECT b.[VehicleId] FROM [Efficiency].[StopsTemp] b)

		INSERT INTO @tStopTemp
			([StopsTempId],
			[VehicleId],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[SpeedStart],
			[SpeedEnd],
			[Longitude],
			[Latitude])
		SELECT 
			[StopsTempId],
			[VehicleId],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[SpeedStart],
			[SpeedEnd],
			[Longitude],
			[Latitude]
		FROM [Efficiency].[StopsTemp] 
		WHERE 
			[SpeedStart] = 0 AND 
			[SpeedEnd] > 0

		INSERT INTO @tStops
			([VehicleId],
			[VehiclePlateId],
			[VehicleName],
			[CustomerId],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[Longitude],
			[Latitude],
			[Alarm],
			[Send])
			
		SELECT 
			a.[VehicleId],
			b.[PlateId],
			b.[Name] [VehicleName],
			b.[CustomerId],
			a.[GPSDateTimeStart],
			a.[GPSDateTimeEnd],
			a.[Longitude],
			a.[Latitude],
			0 [Alarm],
			0 [Send]
		FROM @tStopTemp a 
			INNER JOIN [General].[Vehicles] b ON a.[VehicleId] = b.[VehicleId]
			INNER JOIN [General].[ParametersByCustomer] d ON b.[CustomerId] = d.[CustomerId]
		WHERE a.[SpeedStart] = 0 AND 
			a.[SpeedEnd] > 0 AND
			d.[ResuourceKey] = ''CUSTOMER_POINT_STOP_TIME'' AND
			DATEDIFF(MINUTE, a.[GPSDateTimeStart], a.[GPSDateTimeEnd]) >= CONVERT(INT, d.[Value])

		UPDATE a
		SET a.[RouteId] = c.[RouteId],
			a.[RouteName] = d.[Name],
			a.[RouteDays] = c.[Days],
			a.[RoutePointId] = e.[PointId],
			a.[RoutePointName] = e.[Name],
			a.[RoutePointTime] = e.[Time],
			a.[RoutePointStopTime] = e.[StopTime],
			a.[RoutePointLatitude] = e.[Latitude],
			a.[RoutePointLongitude] = e.[Longitude],
			a.[Alarm] =
				CASE 
					WHEN DATEDIFF(MINUTE, a.[GPSDateTimeStart], a.[GPSDateTimeEnd]) > (e.[StopTime] + CONVERT(FLOAT, g.[Value])) THEN 1 
					WHEN DATEDIFF(MINUTE, a.[GPSDateTimeStart], a.[GPSDateTimeEnd]) <= (e.[StopTime] + CONVERT(FLOAT, g.[Value])) THEN 0 
				END
		FROM @tStops a
			INNER JOIN [General].[ParametersByCustomer] b ON a.[CustomerId] = b.[CustomerId]
			INNER JOIN [Efficiency].[VehicleByRoute] c ON a.[VehicleId] = c.[VehicleId]
			INNER JOIN [Efficiency].[Routes] d ON c.[RouteId] = d.[RouteId]
			INNER JOIN [Efficiency].[RoutesPoints] e ON d.[RouteId] = e.[RouteId]
			INNER JOIN [General].[ParametersByCustomer] f ON a.[CustomerId] = f.[CustomerId]
			INNER JOIN [General].[ParametersByCustomer] g ON a.[CustomerId] = g.[CustomerId]
		WHERE 
			b.[ResuourceKey] = ''CUSTOMER_UTC'' AND 
			c.[Days] LIKE 
				CASE DATEPART(DW,DATEADD(HOUR, CONVERT(int,b.[Value]), GETUTCDATE()))
					WHEN 1 THEN ''%D%''
					WHEN 2 THEN ''%L%''
					WHEN 3 THEN ''%K%''
					WHEN 4 THEN ''%M%''
					WHEN 5 THEN ''%J%''
					WHEN 6 THEN ''%V%''
					WHEN 7 THEN ''%S%''
				END  AND 
			f.[ResuourceKey] = ''CUSTOMER_POINT_DISTANCE_MARGIN'' AND 
			g.[ResuourceKey] = ''CUSTOMER_POINT_DELAY'' AND 
			geography::Point(a.[Latitude], a.[Longitude], 4326).STDistance(geography::Point(e.[Latitude], e.[Longitude], 4326)) <= CONVERT(FLOAT, f.[Value])

		INSERT INTO [Efficiency].[Stops]
			([VehicleId],
			[VehiclePlateId],
			[VehicleName],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[Longitude],
			[Latitude],
			[RouteId],
			[RouteName],
			[RouteDays],
			[RoutePointId],
			[RoutePointName],
			[RoutePointTime],
			[RoutePointStopTime],
			[RoutePointLongitude],
			[RoutePointLatitude],
			[Alarm],
			[Send])
		SELECT 
			[VehicleId],
			[VehiclePlateId],
			[VehicleName],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[Longitude],
			[Latitude],
			[RouteId],
			[RouteName],
			[RouteDays],
			[RoutePointId],
			[RoutePointName],
			[RoutePointTime],
			[RoutePointStopTime],
			[RoutePointLongitude],
			[RoutePointLatitude],
			[Alarm],
			0
		FROM @tStops

		UPDATE
			[Efficiency].[StopsTemp] 
		SET 
			[SpeedStart] = [SpeedEnd],
			[ModifyDate] = GETUTCDATE()
		WHERE 
			[SpeedStart] = 0 AND 
			[SpeedEnd] > 0

		SELECT @Message = b.Message
			FROM [General].[Types] a 
			INNER JOIN [General].[Values] b ON a.[TypeId] = b.[TypeId]
		WHERE a.[Code] = ''CUSTOMER_ALARM_TRIGGER_POINT_TIME_STOP''

		INSERT INTO @tSendMails
			([StopId],
			[VehiclePlateId],
			[RouteName],
			[RoutePointName],
			[RoutePointStopTime],
			[RealStopTime],
			[Email])
		SELECT [StopId],
			[VehiclePlateId], 
			[RouteName], 
			[RoutePointName], 
			[RoutePointStopTime], 
			DATEDIFF(MINUTE, [GPSDateTimeStart], [GPSDateTimeEnd]) [RealStopTime], 
			e.[Email] 
		FROM [Efficiency].[Stops] a 
			INNER JOIN [General].[Vehicles] b ON a.[VehicleId] = b.[VehicleId]
			INNER JOIN [General].[CustomerUsers] c ON b.[CustomerId] = c.[CustomerId]
			INNER JOIN [General].[Users] d ON c.[UserId] = d.[UserId]
			INNER JOIN [AspNetUsers] e ON d.[AspNetUserId] = e.[Id]
		WHERE 
			a.[Alarm] = 1 AND 
			a.[Send] = 0

		Select @RowNum = Count(*) From @tSendMails
		
		WHILE @RowNum > 0
		BEGIN
			DECLARE @lMessage VARCHAR(500) = @Message
			DECLARE @lStopId INT = NULL
			DECLARE @lPlateId VARCHAR(10) = NULL
			DECLARE @lRuteName VARCHAR(50) = NULL
			DECLARE @lPointName VARCHAR(250) = NULL
			DECLARE @lRouteTime INT = NULL
			DECLARE @lStopTime INT = NULL
			DECLARE @lEmail VARCHAR(MAX) = NULL

			SELECT 
				@lStopId = [StopId],
				@lPlateId = [VehiclePlateId],
				@lRuteName = [RouteName],
				@lPointName = [RoutePointName],
				@lRouteTime = [RoutePointStopTime],
				@lStopTime = [RealStopTime],
				@lEmail = [Email]
			FROM @tSendMails 
			WHERE [Id] = @RowNum
			
			SET @lMessage = @Message
			SET @lMessage = REPLACE(@lMessage, ''%PlateId%'', @lPlateId)
			SET @lMessage = REPLACE(@lMessage, ''%RuteName%'', @lRuteName)
			SET @lMessage = REPLACE(@lMessage, ''%PointName%'', @lPointName)
			SET @lMessage = REPLACE(@lMessage, ''%RouteTime%'', @lRouteTime)
			SET @lMessage = REPLACE(@lMessage, ''%StopTime%'', @lStopTime)
			
			INSERT INTO [General].EmailEncryptedAlarms
				([To], 
				[Subject], 
				[Message], 
				[Sent],
				[InsertDate])
			VALUES
				(@lEmail,
				''Alarma de Tiempo de Parada'',
				@lMessage,
				0,
				GETUTCDATE())
				
			UPDATE [Efficiency].[Stops] SET [Send] = 1 WHERE [StopId] = @lStopId

			SET @RowNum = @RowNum -1

		END
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF

END

' 
END
GO

/****** Object:  StoredProcedure [Control].[Sp_RealVsBudgetFuelsReportByVehicleGroup_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_RealVsBudgetFuelsReportByVehicleGroup_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Vehicle Group information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportByVehicleGroup_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		 a.[Name] AS [VehicleGroupName]
		,ISNULL(t.[RealAmount],0) AS [RealAmount]
		,s.[AssignedAmount]
		,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) AS [RealAmountPct]
		,@lCurrencySymbol AS [CurrencySymbol]
	FROM [General].[VehicleGroups] a
		LEFT JOIN (SELECT
						 z.[VehicleGroupId]
						,SUM(x.[FuelAmount]) AS [RealAmount]
					FROM [Control].[Transactions] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
						INNER JOIN [General].[VehiclesByGroup] z
							ON z.VehicleId = y.VehicleId
					WHERE y.[CustomerId] = @pCustomerId
					  AND x.[IsFloating] = 0
					  AND x.[IsReversed] = 0
					  AND x.[IsDuplicated] = 0
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,x.[Date]) = @pMonth
								AND DATEPART(yyyy,x.[Date]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
					GROUP BY z.[VehicleGroupId])t
			ON a.[VehicleGroupId] = t.[VehicleGroupId]
		INNER JOIN (SELECT
						 z.[VehicleGroupId]
						,SUM(x.[Amount]) AS [AssignedAmount]
					FROM [Control].[VehicleFuel] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
						INNER JOIN [General].[VehiclesByGroup] z
							ON z.VehicleId = y.VehicleId
					WHERE y.[CustomerId] = @pCustomerId
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND x.[Month] = @pMonth
								AND x.[Year] = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
								AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
					GROUP BY z.[VehicleGroupId])s
			ON a.[VehicleGroupId] = s.[VehicleGroupId]
	ORDER BY 4 desc	
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Vehicle information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		 a.[PlateId] AS [PlateNumber]
		,ISNULL(t.[RealAmount],0) AS [RealAmount]
		,s.[AssignedAmount]
		,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) AS [RealAmountPct]
		,@lCurrencySymbol AS [CurrencySymbol]
	FROM [General].[Vehicles] a
		LEFT JOIN (SELECT
						 y.[VehicleId]
						,SUM(x.[FuelAmount]) AS [RealAmount]
					FROM [Control].[Transactions] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
					  AND x.[IsFloating] = 0
					  AND x.[IsReversed] = 0
					  AND x.[IsDuplicated] = 0
					  AND y.[Active] = 1
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,x.[Date]) = @pMonth
								AND DATEPART(yyyy,x.[Date]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
					GROUP BY y.[VehicleId])t
			ON a.[VehicleId] = t.[VehicleId]
		INNER JOIN (SELECT
						 y.[VehicleId]
						,SUM(x.[Amount]) AS [AssignedAmount]
					FROM [Control].[VehicleFuel] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
					  AND y.[Active] = 1
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND x.[Month] = @pMonth
								AND x.[Year] = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
								AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
					GROUP BY y.[VehicleId])s
			ON a.[VehicleId] = s.[VehicleId]
	ORDER BY 4 desc	
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_RealVsBudgetFuelsReportBySubUnit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_RealVsBudgetFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Sub Unit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportBySubUnit_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		 a.[Name] AS [CostCenterName]
		,ISNULL(t.[RealAmount],0) AS [RealAmount]
		,s.[AssignedAmount]
		,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) AS [RealAmountPct]
		,@lCurrencySymbol AS [CurrencySymbol]
	FROM [General].[VehicleCostCenters] a
		LEFT JOIN (SELECT
						 y.[CostCenterId]
						,SUM(x.[FuelAmount]) AS [RealAmount]
					FROM [Control].[Transactions] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
					  AND x.[IsFloating] = 0
					  AND x.[IsReversed] = 0
					  AND x.[IsDuplicated] = 0
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,x.[Date]) = @pMonth
								AND DATEPART(yyyy,x.[Date]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
					GROUP BY y.[CostCenterId])t
			ON a.CostCenterId = t.CostCenterId
		INNER JOIN (SELECT
						 y.[CostCenterId]
						,SUM(x.[Amount]) AS [AssignedAmount]
					FROM [Control].[VehicleFuel] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND x.[Month] = @pMonth
								AND x.[Year] = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
								AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
					GROUP BY y.[CostCenterId])s
			ON a.[CostCenterId] = s.[CostCenterId]
	ORDER BY 4 desc
		
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceVehicles_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceVehicles_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ==========================================================
-- Author:		Danilo Hidalgo.
-- Create date: 07/01/2015
-- Description:	Preventive Maintenance Record By Vehicle information
-- ===========================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceVehicles_Retrieve]
	 @pCustomerId INT
	,@pPreventiveMaintenanceCatalogId INT = NULL
	,@pKey VARCHAR(MAX) = NULL
AS
BEGIN 
	SET NOCOUNT ON;
	
		SELECT DISTINCT
			d.[PreventiveMaintenanceCatalogId],
			a.[VehicleId], 
			a.[PlateId], 
			a.[Name] as [VehicleName], 
			c.[Name] as [CostCenterName], 
			b.[Type] as [TypeName]
		FROM [General].[Vehicles] a 
			INNER JOIN [General].[VehicleCategories] b ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
			INNER JOIN [General].[VehicleCostCenters] c ON a.[CostCenterId] = c.[CostCenterId]
			INNER JOIN [General].[PreventiveMaintenanceByVehicle] d ON a.[VehicleId] = d.[VehicleId]
		WHERE d.[PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId
			AND a.[CustomerId] = @pCustomerId
			AND (@pKey IS NULL 
				OR a.[Name] like ''%''+@pKey+''%''
				OR a.[PlateId] like ''%''+@pKey+''%'')
			ORDER BY a.[VehicleId] DESC
	
	SET NOCOUNT OFF;
END

' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Sub Unit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		 a.[Name] AS [VehicleUnitName]
		,ISNULL(t.[RealAmount],0) AS [RealAmount]
		,s.[AssignedAmount]
		,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) AS [RealAmountPct]
		,@lCurrencySymbol AS [CurrencySymbol]
		,a.UnitId
	FROM [General].[VehicleUnits] a
		LEFT JOIN (SELECT
						 z.[UnitId]
						,SUM(x.[FuelAmount]) AS [RealAmount]
					FROM [Control].[Transactions] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
						INNER JOIN [General].[VehicleCostCenters] z
							ON z.CostCenterId = y.CostCenterId
					WHERE y.[CustomerId] = @pCustomerId
					  AND x.[IsFloating] = 0
					  AND x.[IsReversed] = 0
					  AND x.[IsDuplicated] = 0
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,x.[Date]) = @pMonth
								AND DATEPART(yyyy,x.[Date]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
					GROUP BY z.[UnitId])t
			ON a.UnitId = t.UnitId
		INNER JOIN (SELECT
						 z.[UnitId]
						,SUM(x.[Amount]) AS [AssignedAmount]
					FROM [Control].[VehicleFuel] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
						INNER JOIN [General].[VehicleCostCenters] z
							ON z.CostCenterId = y.CostCenterId
					WHERE y.[CustomerId] = @pCustomerId
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND x.[Month] = @pMonth
								AND x.[Year] = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
								AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
					GROUP BY z.[UnitId])s
			ON a.[UnitId] = s.[UnitId]
		
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportByVehicleGroup_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByVehicleGroup_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Vehicle Group information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicleGroup_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		 a.[Name] AS [VehicleGroupName]
		,ISNULL(t.[RealAmount],0) AS [RealAmount]
		,s.[AssignedAmount]
		,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) AS [RealAmountPct]
		,@lCurrencySymbol AS [CurrencySymbol]
	FROM [General].[VehicleGroups] a
		LEFT JOIN (SELECT
						 z.[VehicleGroupId]
						,SUM(x.[FuelAmount]) AS [RealAmount]
					FROM [Control].[Transactions] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
						INNER JOIN [General].[VehiclesByGroup] z
							ON z.VehicleId = y.VehicleId
					WHERE y.[CustomerId] = @pCustomerId
					  AND x.[IsFloating] = 0
					  AND x.[IsReversed] = 0
					  AND x.[IsDuplicated] = 0
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,x.[Date]) = @pMonth
								AND DATEPART(yyyy,x.[Date]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
					GROUP BY z.[VehicleGroupId])t
			ON a.[VehicleGroupId] = t.[VehicleGroupId]
		INNER JOIN (SELECT
						 z.[VehicleGroupId]
						,SUM(x.[Amount]) AS [AssignedAmount]
					FROM [Control].[VehicleFuel] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
						INNER JOIN [General].[VehiclesByGroup] z
							ON z.VehicleId = y.VehicleId
					WHERE y.[CustomerId] = @pCustomerId
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND x.[Month] = @pMonth
								AND x.[Year] = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
								AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
					GROUP BY z.[VehicleGroupId])s
			ON a.[VehicleGroupId] = s.[VehicleGroupId]
		
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Vehicle information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		 a.[PlateId] AS [PlateNumber]
		,ISNULL(t.[RealAmount],0) AS [RealAmount]
		,s.[AssignedAmount]
		,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) AS [RealAmountPct]
		,@lCurrencySymbol AS [CurrencySymbol]
		,a.CostCenterId
	FROM [General].[Vehicles] a
		LEFT JOIN (SELECT
						 y.[VehicleId]
						,SUM(x.[FuelAmount]) AS [RealAmount]
					FROM [Control].[Transactions] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
					  AND x.[IsFloating] = 0
					  AND x.[IsReversed] = 0
					  AND x.[IsDuplicated] = 0
					  AND y.[Active] = 1
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,x.[Date]) = @pMonth
								AND DATEPART(yyyy,x.[Date]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
					GROUP BY y.[VehicleId])t
			ON a.[VehicleId] = t.[VehicleId]
		INNER JOIN (SELECT
						 y.[VehicleId]
						,SUM(x.[Amount]) AS [AssignedAmount]
					FROM [Control].[VehicleFuel] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
					  AND y.[Active] = 1
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND x.[Month] = @pMonth
								AND x.[Year] = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
								AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
					GROUP BY y.[VehicleId])s
			ON a.[VehicleId] = s.[VehicleId]
		
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Sub Unit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		 a.[Name] AS [CostCenterName]
		,ISNULL(t.[RealAmount],0) AS [RealAmount]
		,s.[AssignedAmount]
		,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) AS [RealAmountPct]
		,@lCurrencySymbol AS [CurrencySymbol]
		,a.CostCenterId
	FROM [General].[VehicleCostCenters] a
		LEFT JOIN (SELECT
						 y.[CostCenterId]
						,SUM(x.[FuelAmount]) AS [RealAmount]
					FROM [Control].[Transactions] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
					  AND x.[IsFloating] = 0
					  AND x.[IsReversed] = 0
					  AND x.[IsDuplicated] = 0
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,x.[Date]) = @pMonth
								AND DATEPART(yyyy,x.[Date]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
					GROUP BY y.[CostCenterId])t
			ON a.CostCenterId = t.CostCenterId
		INNER JOIN (SELECT
						 y.[CostCenterId]
						,SUM(x.[Amount]) AS [AssignedAmount]
					FROM [Control].[VehicleFuel] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND x.[Month] = @pMonth
								AND x.[Year] = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
								AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
					GROUP BY y.[CostCenterId])s
			ON a.[CostCenterId] = s.[CostCenterId]
		
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardCloseCreditLimit_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardCloseCreditLimit_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andres Oviedo Brenes
-- Create date: 09/11/2014
-- Description:	Retrieve CreditCard Close Credit Limit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardCloseCreditLimit_Retrieve]
(
	 @pCustomerId INT						--@pCustomerId: Customer Id
	,@pCreditCardId INT = NULL				--@pCreditCardId:Credit Card Id, PK of the table
	,@pKey VARCHAR(800) = NULL				--@pKey: Key to perform search operations
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	DECLARE @percentage FLOAT= CAST((SELECT TOP 1 CreditPercentageMin FROM General.CustomerThresholds WHERE  CustomerId=@pCustomerId) AS FLOAT)/100
	SELECT
		 a.[CreditCardId]
		,a.[CustomerId]
		,c.[Name] AS [EncryptedCustomerName]
		,a.[CreditCardNumber]
		,a.[ExpirationYear]
		,a.[ExpirationMonth]
		,a.[CreditLimit]
		,a.[CreditAvailable]
		,a.[CreditExtra]
		,a.[StatusId]
		,b.[RowOrder] AS [Step]
		,b.[Name] AS [StatusName]
		,d.[Symbol] AS [CurrencySymbol]
		,c.[IssueForId]		
		,e.[UserId]
		,g.[VehicleId]
		,c.[CreditCardType]
		,f.Name AS [EncryptedDriverName]
		,''Vehículo: '' + h.[PlateId] AS [VehiclePlate]
		,a.CardRequestId
		,i.[EstimatedDelivery]
		,a.[RowVersion]
	FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [Control].[CreditCardByVehicle] g
			ON g.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] = g.[VehicleId]
		INNER JOIN [Control].[CardRequest] i
			ON a.[CardRequestId] = i.[CardRequestId]
	WHERE ((@pCustomerId = 0 AND b.[Usage] = ''CUSTOMER_CREDIT_CARD_SUPER_ADMIN'')
				OR a.[CustomerId] = @pCustomerId)
	  AND (@pCreditCardId IS NULL OR a.[CreditCardId] = @pCreditCardId)
	  AND (@pKey IS NULL
				OR f.[Name] like ''%''+@pKey+''%''
				OR h.[PlateId] like ''%''+@pKey+''%''
				OR Convert(varchar(50),a.[CreditLimit]) like ''%''+@pKey+''%''
				OR Convert(varchar(50),a.[CreditAvailable]) like ''%''+@pKey+''%''
				OR Convert(varchar(50),a.[ExpirationYear]) like ''%''+@pKey+''%''
				)
	  AND a.[StatusId] <> 9 -- filter the closed cards
	  AND a.CreditAvailable<=(A.CreditLimit*(@percentage))
	ORDER BY [CreditCardId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardByNumber_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardByNumber_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/10/2014
-- Description:	Retrieve CreditCard information by Number
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardByNumber_Retrieve]
(
	 @pCreditCardNumber nvarchar(520)		--@pCreditCardNumber: Credit Card Number encrypted
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		 a.[CreditCardId]
		,a.[CustomerId]
		,c.[Name] AS [EncryptedCustomerName]
		,a.[CreditCardNumber]
		,a.[ExpirationYear]
		,a.[ExpirationMonth]
		,a.[CreditLimit]
		,a.[CreditAvailable]
		,a.[CreditExtra]
		,a.[StatusId]
		,b.[RowOrder] AS [Step]
		,b.[Name] AS [StatusName]
		,d.[Symbol] AS [CurrencySymbol]
		,c.[IssueForId]		
		,e.[UserId]
		,g.[VehicleId]
		,c.[CreditCardType]
		,f.Name AS [EncryptedDriverName]
		,''Vehículo: '' + h.[PlateId] AS [VehiclePlate]
		,a.CardRequestId
		,i.[EstimatedDelivery]
		,a.[RowVersion]
    FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [Control].[CreditCardByVehicle] g
			ON g.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] = g.[VehicleId]
		INNER JOIN [Control].[CardRequest] i
			ON a.[CardRequestId] = i.[CardRequestId]
	WHERE a.[CreditCardNumber] = @pCreditCardNumber
	  AND a.[StatusId] <> 9 -- filter the closed cards
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CreditCard information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCard_Retrieve]
(
	 @pCustomerId INT						--@pCustomerId: Customer Id
	,@pCreditCardId INT = NULL				--@pCreditCardId:Credit Card Id, PK of the table
	,@pKey VARCHAR(800) = NULL				--@pKey: Key to perform search operations
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		 a.[CreditCardId]
		,a.[CustomerId]
		,c.[Name] AS [EncryptedCustomerName]
		,a.[CreditCardNumber]
		,a.[ExpirationYear]
		,a.[ExpirationMonth]
		,a.[CreditLimit]
		,a.[CreditAvailable]
		,a.[CreditExtra]
		,a.[StatusId]
		,b.[RowOrder] AS [Step]
		,b.[Name] AS [StatusName]
		,d.[Symbol] AS [CurrencySymbol]
		,c.[IssueForId]		
		,e.[UserId]
		,g.[VehicleId]
		,c.[CreditCardType]
		,f.Name AS [EncryptedDriverName]
		,''Vehículo: '' + h.[PlateId] AS [VehiclePlate]
		,a.CardRequestId
		,i.[EstimatedDelivery]
		,a.[RowVersion]
    FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [Control].[CreditCardByVehicle] g
			ON g.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] = g.[VehicleId]
		INNER JOIN [Control].[CardRequest] i
			ON a.[CardRequestId] = i.[CardRequestId]
	WHERE ((@pCustomerId = 0 AND b.[Usage] = ''CUSTOMER_CREDIT_CARD_SUPER_ADMIN'')
				OR a.[CustomerId] = @pCustomerId)
	  AND (@pCreditCardId IS NULL OR a.[CreditCardId] = @pCreditCardId)
	  AND (@pKey IS NULL
				OR f.[Name] like ''%''+@pKey+''%''
				OR h.[PlateId] like ''%''+@pKey+''%''
				OR Convert(varchar(50),a.[CreditLimit]) like ''%''+@pKey+''%''
				OR Convert(varchar(50),a.[CreditAvailable]) like ''%''+@pKey+''%''
				OR Convert(varchar(50),a.[ExpirationYear]) like ''%''+@pKey+''%''
				)
	  AND a.[StatusId] <> 9 -- filter the closed cards
	ORDER BY [CreditCardId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_AddOrEdit]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update CreditCard information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCard_AddOrEdit]
(
	 @pCreditCardId INT = NULL				--@pCreditCardId:Credit Card Id, PK of the table
	,@pCustomerId INT = NULL				--@pCustomerId: Customer Id
	,@pCreditCardNumberId INT = NULL		--@pCreditCardNumberId: Credit Card Number Id
	,@pExpirationYear INT					--@pExpirationYear: Expiration Year
	,@pExpirationMonth INT					--@pExpirationMonth: Expiration Month
	,@pCreditLimit NUMERIC(16,2)			--@pCreditLimit: Credit Limit
	,@pStatusId INT	= NULL					--@pStatusId: Status Id
	,@pMonth INT							--@pMonth: Month
	,@pYear INT								--@pYear: Year
	,@pUserId INT = NULL					--@pUserId: User Id
	,@pVehicleId INT = NULL					--@pVehicleId: Vehicle Id
	,@pCardRequestId INT = NULL				--@pCardRequestId: Ref (RequestId)
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCreditCardId IS NULL)
			BEGIN
				DECLARE @lCreditCardNumber NVARCHAR(520)
				
				SELECT 
					@lCreditCardNumber = CreditCardNumber
				FROM [Control].[CreditCardNumbers]
				WHERE [CreditCardNumberId] = @pCreditCardNumberId
				
				UPDATE [Control].[CreditCardNumbers]
					SET  [IsUsed] = 1
						,[IsReserved] = 0
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CreditCardNumberId] = @pCreditCardNumberId
				
				INSERT INTO [Control].[CreditCard]
						([CustomerId]
						,[CreditCardNumber]
						,[ExpirationYear]
						,[ExpirationMonth]
						,[CreditLimit]
						,[CreditAvailable]
						,[StatusId]
						,[CardRequestId]
						,[InsertDate]
						,[InsertUserId])
				VALUES ( @pCustomerId
						,@lCreditCardNumber
						,@pExpirationYear
						,@pExpirationMonth
						,@pCreditLimit
						,@pCreditLimit -- Same when Insert
						,@pStatusId
						,@pCardRequestId
						,GETUTCDATE()
						,@pLoggedUserId)
				SET @lRowCount = @@ROWCOUNT
				SET @pCreditCardId = SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
				DECLARE @lCreditLimit NUMERIC(16,2), @lCreditAvailable NUMERIC(16,2)
				
				SELECT @lCreditAvailable = @pCreditLimit - ([CreditLimit] - [CreditAvailable])
				FROM [Control].[CreditCard]
				WHERE [CreditCardId] = @pCreditCardId
				
				
				SELECT
					@pCustomerId = [CustomerId]
				FROM [Control].[CreditCard]
				WHERE @pCustomerId IS NULL -- When update comes from coinca admin
				  AND [CreditCardId] = @pCreditCardId
				  
				UPDATE [Control].[CreditCard]
					SET  [CustomerId] = @pCustomerId
						,[ExpirationYear] = @pExpirationYear
						,[ExpirationMonth] = @pExpirationMonth
						,[CreditLimit] = @pCreditLimit
						,[CreditAvailable] = @lCreditAvailable
						,[StatusId] = COALESCE(@pStatusId,[StatusId])
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CreditCardId] = @pCreditCardId
				  AND [RowVersion] = @pRowVersion
				  
				SET @lRowCount = @@ROWCOUNT
			END		
            
            IF @lRowCount > 0
            BEGIN
				
				IF @pUserId IS NOT NULL
				BEGIN
					SELECT @lCount = COUNT(1) FROM [Control].[CreditCardByDriver] WHERE [CreditCardId] = @pCreditCardId
					IF @lCount = 0
					BEGIN
						INSERT INTO [Control].[CreditCardByDriver]
							([CreditCardId]
							,[UserId]
							,[InsertDate]
							,[InsertUserId])
						VALUES
							(@pCreditCardId
							,@pUserId
							,GETUTCDATE()
							,@pLoggedUserId)
					END ELSE BEGIN
						UPDATE [Control].[CreditCardByDriver]
						SET  [UserId] = @pUserId
							,[ModifyDate] = GETUTCDATE()
							,[ModifyUserId] = @pLoggedUserId
						WHERE [CreditCardId] = @pCreditCardId
					END
				END
				
				IF @pVehicleId IS NOT NULL
				BEGIN
					SELECT @lCount = COUNT(1) FROM [Control].[CreditCardByVehicle] WHERE [CreditCardId] = @pCreditCardId
					IF @lCount = 0
					BEGIN
						INSERT INTO [Control].[CreditCardByVehicle]
							([CreditCardId]
							,[VehicleId]
							,[InsertDate]
							,[InsertUserId])
						VALUES
							(@pCreditCardId
							,@pVehicleId
							,GETUTCDATE()
							,@pLoggedUserId)
					END ELSE BEGIN
						UPDATE [Control].[CreditCardByVehicle]
						SET  [VehicleId] = @pVehicleId
							,[ModifyDate] = GETUTCDATE()
							,[ModifyUserId] = @pLoggedUserId
						WHERE [CreditCardId] = @pCreditCardId
					END
				END
				
            
				DECLARE @lAssigned DECIMAL(16,2) = 0
				DECLARE @lAvailable DECIMAL(16,2) = 0
									
				SET @lAssigned = COALESCE((SELECT SUM(a.[CreditLimit])+ SUM(a.[CreditExtra]) 
				FROM [Control].[CreditCard] a
				WHERE a.[CustomerId] = @pCustomerId
				  AND a.[StatusId] <> 8),0)
				  
				SELECT
					@lAvailable = a.[CreditAvailable]
				FROM [Control].[CustomerCredits] a
				WHERE a.[Year] = @pYear
				  AND a.[Month] = @pMonth
				  AND a.[CustomerId] = @pCustomerId
				
				IF @lAssigned < = @lAvailable
				BEGIN
					UPDATE [Control].[CustomerCredits]
					SET [CreditAssigned] = @lAssigned
					WHERE [Year] = @pYear
					  AND [Month] = @pMonth
					  AND [CustomerId] = @pCustomerId
				END ELSE
				BEGIN
					IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
					BEGIN
						ROLLBACK TRANSACTION
						
						UPDATE [Control].[CreditCardNumbers]
						SET  [IsUsed] = 0, [IsReserved] = 0
						WHERE [CreditCardNumberId] = @pCreditCardNumberId
					
						RAISERROR (50003, 16, 1)
					END
				END
				
				-- History control --
				INSERT INTO [Control].[CreditCardHx] 
				   ([CreditCardId]
				   ,[CustomerId]
				   ,[CreditCardNumber]
				   ,[ExpirationYear]
				   ,[ExpirationMonth]
				   ,[CreditLimit]
				   ,[CreditAvailable]
				   ,[CreditExtra]
				   ,[StatusId]
				   ,[CardRequestId]
				   ,[InsertDate]
				   ,[InsertUserId]
				   ,[ModifyDate]
				   ,[ModifyUserId])
				SELECT [CreditCardId]
				   ,[CustomerId]
				   ,[CreditCardNumber]
				   ,[ExpirationYear]
				   ,[ExpirationMonth]
				   ,[CreditLimit]
				   ,[CreditAvailable]
				   ,[CreditExtra]
				   ,[StatusId]
				   ,[CardRequestId]
				   ,[InsertDate]
				   ,[InsertUserId]
				   ,[ModifyDate]
				   ,[ModifyUserId] FROM [Control].[CreditCard] WHERE [CreditCardId] = @pCreditCardId
				-- History control --
            END
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
			
			UPDATE [Control].[CreditCardNumbers]
				SET  [IsUsed] = 0, [IsReserved] = 0
			WHERE [CreditCardNumberId] = @pCreditCardNumberId
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CostCentersByCredit_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CostCentersByCredit_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Cost Centers By Credit
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CostCentersByCredit_Retrieve]
(
	 @pCostCenterId INT						--@pCostCenterId: PK of the table VehicleGroup
	,@pYear INT								--@pYear: Year
	,@pMonth INT							--@pMonth: Month	
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	SELECT
		 a.PlateId
		,a.[Name] AS  [VehicleName]
		,b.[Manufacturer] + '' - '' + b.[VehicleModel] AS VehicleModel
		,f.[Symbol] AS [CurrencySymbol]
		,ISNULL(t.[Amount], CONVERT(DECIMAL(16,2),0)) AS [Amount]
		,ISNULL(t.[Liters],0) AS [Liters]
		,a.[VehicleId]
		,b.[DefaultFuelId]
		,c.[Code] + '' - '' + c.[Name] AS CostCenter
		,t.[VehicleFuelId]
		,t.[RowVersion]		
	FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCategories] b
			ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
		INNER JOIN [General].[VehicleCostCenters] c
			ON a.[CostCenterId] = c.[CostCenterId]
		INNER JOIN [General].[Customers] e
			ON e.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON f.[CurrencyId] = e.[CurrencyId]
		LEFT JOIN (
				SELECT
					 x.[VehicleId]
					,x.[Amount]
					,x.[Liters]
					,x.[VehicleFuelId]
					,x.[RowVersion]
				FROM [Control].[VehicleFuel] x
				WHERE x.[Year] = @pYear
				  AND x.[Month] = @pMonth) t
			ON t.VehicleId = a.VehicleId
	WHERE c.[CostCenterId] = @pCostCenterId
	  
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Vehicle information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lExchangeRate DECIMAL(16,8) = 1.0
	
	SELECT
		@lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[PartnerId] = b.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = b.[CustomerId] AND c.[CurrencyId] = a.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND a.EndDate IS NULL
	;
	WITH TmpValues ( Number) as
	(
		  SELECT 0
		  UNION ALL
		  SELECT Number + 1
		  FROM TmpValues
		  WHERE Number < 100
	)
	SELECT
		  q.[VehicleId]
		 ,q.[PlateNumber]
		 ,q.[Year]
		 ,q.[Month]
		 ,SUM(q.[Liters]) AS [Liters]
	FROM
		(SELECT
			 a.[VehicleId]
			,a.[PlateId] AS [PlateNumber]
			,s.[Month]
			,s.[Year]
			,CONVERT(DECIMAL(16,2),ISNULL(s.[RealAmount],0)/(t.LiterPrice * @lExchangeRate)) AS Liters
		FROM [General].[Vehicles] a
			INNER JOIN [General].[VehicleCategories] b
				ON b.[VehicleCategoryId] = a.[VehicleCategoryId]
			INNER JOIN (
					SELECT
						x.[FuelId], x.[LiterPrice] 
					FROM [Control].[PartnerFuel] x
						INNER JOIN [General].[CustomersByPartner] y
							ON x.[PartnerId] = y.[PartnerId]
						INNER JOIN [General].[Customers] z
							ON y.[CustomerId] = z.[CustomerId]
					WHERE z.CustomerId = @pCustomerId
					  AND x.[EndDate] IS NULL)t
				ON b.[DefaultFuelId] = t.FuelId
			INNER JOIN (SELECT
							 y.[VehicleId]
							,DATEPART(m,x.[Date]) AS [Month]
							,DATEPART(yyyy,x.[Date]) AS [Year]
							,SUM(x.[FuelAmount]) AS [RealAmount]
						FROM [Control].[Transactions] x
							INNER JOIN [General].[Vehicles] y
								ON x.[VehicleId] = y.[VehicleId]
						WHERE y.[CustomerId] = @pCustomerId
						  AND x.[IsFloating] = 0
						  AND x.[IsReversed] = 0
						  AND x.[IsDuplicated] = 0
						  AND y.[Active] = 1
						  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
									AND DATEPART(m,x.[Date]) = @pMonth
									AND DATEPART(yyyy,x.[Date]) = @pYear) OR
								(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
									AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
									
						GROUP BY y.[VehicleId], DATEPART(m,x.[Date]), DATEPART(yyyy,x.[Date]))s
				ON s.[VehicleId] = a.[VehicleId]			
			UNION		
			SELECT
				 a.[VehicleId]
				,a.[PlateId] AS [PlateNumber]
				,r.[Month]
				,r.[Year]
				,0 AS Liters
			FROM [General].[Vehicles] a
				CROSS JOIN(
						SELECT
							 DATEPART(m, DATEADD(m, x.[Number], @pStartDate)) AS [Month]
							,DATEPART(yyyy, DATEADD(m, x.[Number], @pStartDate)) AS [Year]
						FROM TmpValues x
						WHERE x.[Number] <= DATEDIFF(m, @pStartDate, @pEndDate)
					UNION
						SELECT @pMonth AS [Month], @pYear AS [Year]
						WHERE @pMonth IS NOT NULL AND @pYear IS NOT NULL
				)r
			WHERE a.[CustomerId] = @pCustomerId
			  AND a.[Active] = 1)q
		GROUP BY q.[VehicleId], q.[PlateNumber], q.[Year], q.[Month]
			
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_AlarmByOdometer_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AlarmByOdometer_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martínez H.
-- Create date: 22/Oct/2014
-- Description:	Alarm By Odometer information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_AlarmByOdometer_Retrieve]
(
	 @pCustomerId INT						--@pCustomerId: FK of Customer
	,@pAlarmOdometerId INT = NULL 
	,@pKey VARCHAR(800) = NULL 
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		 a.[AlarmOdometerId]
		,a.[VehicleId]
		,b.[PlateId]
		,a.[Date]
		,a.[ExpectedOdometer]
		,a.[ReportedOdometer]
	FROM [Control].[AlarmByOdometer] a
		INNER JOIN [General].[Vehicles] b
		ON a.[VehicleId] = b.[VehicleId]
	WHERE  b.[CustomerId] = @pCustomerId
		AND (@pAlarmOdometerId IS NULL OR a.[AlarmOdometerId] = @pAlarmOdometerId)
		AND a.[Fixed] = 0
		AND (@pkey IS NULL 
				OR a.[ExpectedOdometer] LIKE ''%''+@pkey+''%''
				OR a.[ReportedOdometer] LIKE ''%''+@pkey+''%''
				OR b.[PlateId] LIKE ''%''+@pkey+''%'')
	
    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_AlarmByOdometer_Disabled]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AlarmByOdometer_Disabled]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martinez H.
-- Create date: 23/Oct/2014
-- Description:	Disable Odometer Alert
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_AlarmByOdometer_Disabled]
(
	 @pAlarmOdometerId INT
	,@pVehicleId INT
	,@pNewOdometer INT 
) 
AS 
BEGIN		
	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lTransactionId INT
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			/*The original transaction must be recorded into table AlarmByOdometer to avoid this horrible process developed by cmartinez*/
			SELECT 
				TOP 1 @lTransactionId =  a.[TransactionId] 
			FROM [Control].[Transactions] a
			 INNER JOIN [Control].[AlarmByOdometer] b
				ON a.VehicleId = b.VehicleId 
			WHERE a.[VehicleId] = @pVehicleId
			  AND a.Odometer = b.ReportedOdometer
			  AND b.AlarmOdometerId = @pAlarmOdometerId
			ORDER BY [TransactionId] DESC
			
			IF (@lTransactionId IS NOT NULL)
			BEGIN 
				UPDATE [Control].[AlarmByOdometer]
				SET    [Fixed] = 1
				WHERE  [AlarmOdometerID] = @pAlarmOdometerId
				
				UPDATE [Control].[Transactions]
				SET    [FixedOdometer] = @pNewOdometer
				WHERE  [TransactionId] = @lTransactionId					
			END 
			
			SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
			
				RAISERROR (''Error updating the original transaction it doesn''''t exists!'', 16, 1)
			END			
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_AlarmByOdometer_AddOrEdit]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AlarmByOdometer_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martínez H.
-- Create date: 21/Oct/2014
-- Description:	Insert or Update Alarm by odometer information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_AlarmByOdometer_AddOrEdit]
(
	 @pAlarmOdometerId INT = NULL
	,@pVehicleId INT
	,@pDate DATETIME 
	,@pExpectedOdometer INT
	,@pReportedOdometer INT
	,@pFixed BIT
	,@pLoggedUserId INT 
	,@pRowVersion TIMESTAMP = NULL
)
AS
BEGIN 
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0  
            DECLARE @lPlate VARCHAR(10)  
            DECLARE @lAlarmId INT
			DECLARE @lAlarmTriggerId INT
			DECLARE @lPhone VARCHAR(100)
			DECLARE @lEmail VARCHAR(500)
			DECLARE @lCustomerId INT  
			DECLARE @lRowVersion TIMESTAMP      
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pAlarmOdometerId IS NULL)
			BEGIN
				INSERT INTO [Control].[AlarmByOdometer]
						([VehicleID]
						,[Date]
						,[ExpectedOdometer]
						,[ReportedOdometer]
						,[Fixed]
						,[InsertUserId]
						,[InsertDate])
				VALUES (@pVehicleId
					   ,@pDate
					   ,@pExpectedOdometer
					   ,@pReportedOdometer
					   ,@pFixed
					   ,@pLoggedUserId
					   ,GETUTCDATE())
			
				SET @lRowCount = @@ROWCOUNT
					   
				SELECT @lPlate = [PlateId]
				FROM [General].[Vehicles]
				WHERE [VehicleId] = @pVehicleId
					   
				SET @pAlarmOdometerId = SCOPE_IDENTITY()
				
				SELECT @lAlarmTriggerId = a.[TypeId]
				FROM [General].[Types] a
				WHERE a.[Code] = ''CUSTOMER_ALARM_TRIGGER_ODOMETER''
				
				IF @lAlarmTriggerId IS NOT NULL
				BEGIN 
					SELECT TOP 1 @lAlarmId = a.[AlarmId],
					             @lPhone = a.[Phone], 
					             @lEmail = a.[Email], 
					             @lCustomerId = a.[CustomerId], 
					             @lRowVersion = a.[RowVersion]
					FROM [General].[Alarms] a
					WHERE a.[AlarmTriggerId] = @lAlarmTriggerId
					
					IF @lAlarmId IS NOT NULL 
					BEGIN 
						EXEC [General].[Sp_SendAlarm] @pAlarmId = @lAlarmId,
													  @pAlarmTriggerId = @lAlarmTriggerId,
									                  @pCustomerId = @lCustomerId,
									                  @pPlate = @lPlate,
									                  @pPhone = @lPhone, 
									                  @pEmail = @lEmail,
									                  @pRowVersion = @lRowVersion	
					END 
				END
			END 
			ELSE
			BEGIN 
				UPDATE [Control].[AlarmByOdometer]
					SET [VehicleId] = @pVehicleId
					   ,[Date] = @pDate
					   ,[ExpectedOdometer] = @pExpectedOdometer
					   ,[ReportedOdometer] = @pReportedOdometer
					   ,[Fixed] = @pFixed
					   ,[ModifyUserId] = @pLoggedUserId
					   ,[ModifyDate] = GETUTCDATE()
				WHERE [AlarmOdometerId] = @pAlarmOdometerId
				--AND [RowVersion] = @pRowVersion
				
				SET @lRowCount = @@ROWCOUNT
			END			
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (''Error updating database row, Please try again. TimeStamp verification failed.'', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_AfterSchedule_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AfterSchedule_Alarm]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Cristian Martínez 
-- Create date: 24/11/2014
-- Description:	Add alarm by use vehicule after of schedule 
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_AfterSchedule_Alarm]
AS
BEGIN 
	DECLARE @lErrorMessage NVARCHAR(4000)
    DECLARE @lErrorSeverity INT
    DECLARE @lErrorState INT
    DECLARE @lAlarmId INT
	DECLARE	@lCustomerId INT
	DECLARE	@lAlarmTriggerId INT
	DECLARE	@lEntityTypeId INT
	DECLARE	@lEntityId INT
	DECLARE	@lPeriodicityId INT
	DECLARE	@lPhone VARCHAR(100) 
	DECLARE	@lEmail VARCHAR(500)
	DECLARE @lRowVersion TIMESTAMP
	DECLARE @lPlate VARCHAR(10)
	DECLARE @lIntrackReference INT
	DECLARE @lLastReport DATETIME
	DECLARE @lInputStatus INT
	DECLARE @tAlarm TABLE
	(
		AlarmId INT,
		CustomerId INT, 
		AlarmTriggerId INT, 
		EntityTypeId INT, 
		EntityId INT,
		PeriodicityId INT,
		Phone VARCHAR(100),
		Email VARCHAR(500),
		RowVersionAlarm VARBINARY(20)
	)
    
	BEGIN TRY 
		
		INSERT INTO @tAlarm
		(	
			AlarmId, 
			CustomerId,
			AlarmTriggerId, 
			EntityTypeId, 
			EntityId, 
			PeriodicityId, 
			Phone, 
			Email, 
			RowVersionAlarm
		)SELECT a.[AlarmId], 
				a.[CustomerId], 
				a.[AlarmTriggerId], 
				a.[EntityTypeId], 
				a.[EntityId], 
				a.[PeriodicityTypeId], 
				a.[Phone], 
				a.[Email], 
				CONVERT(VARBINARY(20),a.[RowVersion])
		FROM [General].[Alarms] a
			INNER JOIN [General].[Types] b
				  ON a.[AlarmTriggerId] = b.[TypeId]
		WHERE a.[Active] = 1
		 AND (b.[Code] = ''CUSTOMER_ALARM_TRIGGER_AFTER_HOURS'')
		 AND (a.[NextAlarm] IS NULL 
		   OR a.[NextAlarm] < GETUTCDATE()) 
		   
		WHILE EXISTS(SELECT [AlarmId] FROM @tAlarm)
		BEGIN 
			
			SELECT TOP 1 @lAlarmId  = a.[AlarmId], 
						 @lCustomerId = a.[CustomerId],
						 @lAlarmTriggerId = a.[AlarmTriggerId],
						 @lEntityTypeId = a.[EntityTypeId],
						 @lEntityId = a.[EntityId],
						 @lPeriodicityId = a.[PeriodicityId],
						 @lPhone = a.[Phone],
						 @lEmail = a.[Email],
						 @lRowVersion = CONVERT(TIMESTAMP, a.[RowVersionAlarm])
			FROM @tAlarm a
			
			SET @lPlate = (SELECT [PlateId] FROM [General].[Vehicles] 
											WHERE [VehicleId] = @lEntityId
											  AND [Active] = 1)
			SET @lIntrackReference = (SELECT [IntrackReference]
									    FROM [General].[Vehicles] 
										WHERE [VehicleId] = @lEntityId
										  AND [Active] = 1)
			
			IF @lIntrackReference IS NOT NULL
			BEGIN 
				
				SELECT 
					@lLastReport = d.[GPSDateTime], 
					@lInputStatus = d.[InputStatus]
				FROM [Composiciones] a
				INNER JOIN  [DispositivosAVL] b
		     		   ON a.[dispositivo] = b.[dispositivo]
				INNER JOIN [dbo].[Devices] c
				      ON b.[numeroimei] = c.[UnitID]
				INNER JOIN [dbo].[Report_Last] d
				      ON d.[Device] = c.[Device]
				WHERE a.[vehiculo] = @lIntrackReference
				
				--InputStatus 
				IF @lLastReport IS NOT NULL AND (@lInputStatus%2) = 0
				BEGIN 
					
					DECLARE @lXMLData XML
					
					SELECT @lXMLData = XmlSchedule
					FROM [General].[VehicleSchedule] 
					WHERE [VehicleId] = @lEntityId
					
					IF @lXMLData IS NOT NULL
					BEGIN 
						DECLARE @lTimeLastReport TIME 
						DECLARE @lCountLastReport INT
						DECLARE @lDayLastReport INT
						
						SET @lDayLastReport = (DATEPART(dw, @lLastReport)-1)
						SET @lTimeLastReport = DATEADD(HOUR, -6, CONVERT(TIME, @lLastReport))
						
						SELECT @lCountLastReport = COUNT(CONVERT(TIME, RowData.col.value(''./@Time'', ''VARCHAR(5)'')))
                        FROM @lXMLData.nodes(''/xmldata/Schedule'') RowData(col)
						WHERE RowData.col.value(''./@Day'', ''INT'')  = @lDayLastReport
						  AND @lTimeLastReport BETWEEN CONVERT(TIME, RowData.col.value(''./@Time'', ''VARCHAR(5)''))
												   AND DATEADD(MINUTE, 30,CONVERT(TIME, RowData.col.value(''./@Time'', ''VARCHAR(5)'')))											
						ORDER BY 1
						IF @lCountLastReport = 0
						BEGIN
							EXEC [General].[Sp_SendAlarm] @pAlarmId = @lAlarmId,
														  @pAlarmTriggerId = @lAlarmTriggerId,
														  @pCustomerId = @lCustomerId,
													      @pPlate = @lPlate,
														  @pTimeLastReport = @lTimeLastReport,	
														  @pPhone = @lPhone, 
														  @pEmail = @lEmail,
												          @pRowVersion = @lRowVersion
						END					
					END 				
				END 			
			END			
			DELETE @tAlarm WHERE AlarmId = @lAlarmId AND CustomerId = @lCustomerId AND EntityId = @lEntityId
			CONTINUE 
		END 		
	END TRY 
	BEGIN CATCH 
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH 		
END 
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CreditCardVPOS_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardVPOS_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 04/12/2014
-- Description:	Retrieve CreditCard information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardVPOS_Retrieve]
(
	@pKey VARCHAR(800) = NULL				--@pKey: Key to perform search operations
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		 a.[CreditCardId]
		,a.[CustomerId]
		,a.[CreditCardNumber]
		,a.[ExpirationYear]
		,a.[ExpirationMonth]
		,a.[CreditLimit]
		,a.[CreditAvailable]
		,a.[CreditExtra]
		,a.[StatusId]
		,b.[RowOrder] AS [Step]
		,b.[Name] AS [StatusName]
		,d.[Symbol] AS [CurrencySymbol]
		,c.[IssueForId]
		,CONVERT(BIT,CASE WHEN a.[StatusId] IN (4,5) THEN 1 ELSE 0 END) AS IsReadyForCustomer
		,CONVERT(BIT,CASE WHEN a.[StatusId] IN (1,2,3) THEN 1 ELSE 0 END) AS IsReadyForCoinca
		,e.[UserId]
		,g.[VehicleId]
		,c.[CreditCardType]		
		,f.Name AS [EncryptedDriverName]
		,''Vehículo: '' + h.[PlateId] AS [VehiclePlate]
		,a.[RowVersion]
		,c.[Name] AS [EncryptedCustomerName]
    FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [Control].[CreditCardByVehicle] g
			ON g.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] = g.[VehicleId]
	WHERE (@pKey IS NULL 
				OR h.[PlateId] like ''%''+@pKey+''%''
				OR f.[Name] like ''%''+@pKey+''%''
				OR c.[Name]  like ''%''+@pKey+''%'')
	ORDER BY [CreditCardId] DESC
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_EfficiencyFuelsReportByVehicle_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_EfficiencyFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Efficiency Fuels Report By Vehicle information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_EfficiencyFuelsReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lExchangeRate DECIMAL(16,8) = 1.0
	
	SELECT
		@lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[PartnerId] = b.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = b.[CustomerId] AND c.[CurrencyId] = a.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND a.EndDate IS NULL
	;
	WITH TmpValues ( Number) as
	(
		  SELECT 0
		  UNION ALL
		  SELECT Number + 1
		  FROM TmpValues
		  WHERE Number < 100
	)
	
	SELECT
		  q.[VehicleId]
		 ,q.[PlateNumber]
		 ,q.[Year]
		 ,q.[Month]
		 ,SUM(q.[Liters]) AS [Liters]
	FROM
		(SELECT
			 a.[VehicleId]
			,a.[PlateId] AS [PlateNumber]
			,s.[Month]
			,s.[Year]
			,CONVERT(DECIMAL(16,2),ISNULL(s.[RealAmount],0)/(t.LiterPrice * @lExchangeRate)) AS Liters
		FROM [General].[Vehicles] a
			INNER JOIN [General].[VehicleCategories] b
				ON b.[VehicleCategoryId] = a.[VehicleCategoryId]
			INNER JOIN (
					SELECT
						x.[FuelId], x.[LiterPrice] 
					FROM [Control].[PartnerFuel] x
						INNER JOIN [General].[CustomersByPartner] y
							ON x.[PartnerId] = y.[PartnerId]
						INNER JOIN [General].[Customers] z
							ON y.[CustomerId] = z.[CustomerId]
					WHERE z.CustomerId = @pCustomerId
					  AND x.[EndDate] IS NULL)t
				ON b.[DefaultFuelId] = t.FuelId
			INNER JOIN (SELECT
							 y.[VehicleId]
							,DATEPART(m,x.[Date]) AS [Month]
							,DATEPART(yyyy,x.[Date]) AS [Year]
							,SUM(x.[FuelAmount]) AS [RealAmount]
						FROM [Control].[Transactions] x
							INNER JOIN [General].[Vehicles] y
								ON x.[VehicleId] = y.[VehicleId]
						WHERE y.[CustomerId] = @pCustomerId
						  AND x.[IsFloating] = 0
						  AND x.[IsReversed] = 0
						  AND x.[IsDuplicated] = 0
						  AND y.[Active] = 1
						  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
									AND DATEPART(m,x.[Date]) = @pMonth
									AND DATEPART(yyyy,x.[Date]) = @pYear) OR
								(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
									AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
									
						GROUP BY y.[VehicleId], DATEPART(m,x.[Date]), DATEPART(yyyy,x.[Date]))s
				ON s.[VehicleId] = a.[VehicleId]
			WHERE a.[Active] = 1
			UNION		
			SELECT
				 a.[VehicleId]
				,a.[PlateId] AS [PlateNumber]
				,r.[Month]
				,r.[Year]
				,0 AS Liters
			FROM [General].[Vehicles] a
				CROSS JOIN(
						SELECT
							 DATEPART(m, DATEADD(m, x.[Number], @pStartDate)) AS [Month]
							,DATEPART(yyyy, DATEADD(m, x.[Number], @pStartDate)) AS [Year]
						FROM TmpValues x
						WHERE x.[Number] <= DATEDIFF(m, @pStartDate, @pEndDate)
					UNION
						SELECT @pMonth AS [Month], @pYear AS [Year]
						WHERE @pMonth IS NOT NULL AND @pYear IS NOT NULL
				)r
			WHERE a.[CustomerId] = @pCustomerId
			  AND a.[Active] = 1)q
		GROUP BY q.[VehicleId], q.[PlateNumber], q.[Year], q.[Month]
			
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_GetGlobalScoreDataSourceAllDriver]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetGlobalScoreDataSourceAllDriver]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- Modified by:		Andrés Oviedo
-- Modified date:	16/02/2015 
CREATE PROCEDURE [Operation].[Sp_GetGlobalScoreDataSourceAllDriver] 
( @pCustomerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)			--Posibles valores S:Summarized, D:Detailed
)
AS
BEGIN

	
	--Declare @pStartDate datetime =  ''20141019 03:45:02.000'',
	--@pEndDate datetime = ''20141128 11:30am''
	--Verificar configuración de la ponderación
	Declare @TotalWeight numeric(6,2), 
	        @CountWeight int,
	        @pCustomerName varchar(200)
	
	Select
		   @pCustomerName=Name
	  from General.Customers 
	 where CustomerId = @pCustomerId
	 
	Select @TotalWeight = IsNull(SUM(Weight),0),
		   @CountWeight = COUNT(1)
	 from Operation.WeightedScoreSettings w
	 where CustomerId = @pCustomerId
	
	--If @CountWeight > 0 and @TotalWeight <> 100
	--begin
		--Los pesos para la ponderación no suman 100
	--	RAISERROR (''101'', 16, 1)
	--	RETURN
	--end

	--Set Dates 
	Declare @DataPerMonth bit = 0 
	If @pEndDate is null
		If @pStartDate is null
		begin
			If @pMonth is null or @pYear is null
			begin
				RAISERROR (''100'', 16, 1)
				RETURN
			end
			else
			begin
				--Set @pStartDate = [General].[Fn_GetFirstDayOfMonth](@pYear, @pMonth)
				--Set @pEndDate = DATEADD(MM, 1, @pStartDate)
				Set @DataPerMonth = 1
			end
		end
		else
		begin
			Set @pEndDate = DATEADD(DD, 1, @pStartDate)
		end

	--DECLARE @MyTable  dbo.TypeDeviceSpeed
	Declare @VehiclesByUser table
	(Device int,
	 Vehicle int,
	 StartOdometer int,
	 EndOdometer int,
	 StartDate datetime,
	 EndDate datetime)

	Create table #LocalDriverScore
	(DriverScoreId INT Identity(1,1),
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 ScoreType varchar(5),
	 Score Float,
	 Photo varchar(max),
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float
	 )
	Declare @UserId Int
	Declare @Name varchar(250),
			@Identification varchar(50)
		
	Declare  @ScoreAdmOutput nvarchar(max),
			 @ScoreRouteOutput nvarchar(max)
			 	
	Declare  @ScoreAdm Float,
			 @ScoreRoute Float

	If @DataPerMonth = 0 
	begin
		Select @UserId = Min(U.UserId) 
		  from General.Users U
		  inner join General.DriversUsers D on U.UserId = D.UserId
		 Where U.IsActive =  1 

		--Begin transaction
		 
		While  @UserId  is not null
		BEGIN
			Insert into @VehiclesByUser
				(Device, Vehicle, StartOdometer, EndOdometer, StartDate, EndDate)  
			--Select   V.IntrackReference As Device,CASE WHEN  VU.InitialOdometer IS NULL THEN 0 eLSE VU.InitialOdometer end  ,CASE WHEN VU.EndOdometer IS NULL THEN 0 eLSE EndOdometer end    , VU.InsertDate StartDate ,VU.LastDateDriving EndDate
			Select V.DeviceReference, V.IntrackReference, IsNull(VU.InitialOdometer, 0), IsNull(VU.EndOdometer,0), VU.InsertDate, IsNull(VU.LastDateDriving, GETDATE())
			from General.VehiclesByUser VU 
			inner Join General.Vehicles V ON  V.VehicleId = VU.VehicleId
			inner JOIN General.Users U  ON U.UserId = VU.UserId
			Where U.UserId =  @UserId 
			  And V.DeviceReference is not null
			  and ((VU.InsertDate between @pStartDate and @pEndDate)
			   or  (IsNull(VU.LastDateDriving, GETDATE()) between @pStartDate and @pEndDate )
			   or  (VU.InsertDate <= @pStartDate and IsNull(VU.LastDateDriving, GETDATE()) >= @pEndDate))
			order by V.DeviceReference, VU.InsertDate, VU.LastDateDriving

			if (select COUNT(*) FROM @VehiclesByUser ) > 0 
			BEGIN 
				DECLARE @VehiclesByUserString varchar(1000);
				SET @VehiclesByUserString = (SElect * FROm  @VehiclesByUser AS Lista FOR XML AUTO)
				EXECUTE  dbo.Sp_GetScoreDriver @pStartDate, @pEndDate, @VehiclesByUserString, ''A'', @pScoreTable = @ScoreAdmOutput OUTPUT
				EXECUTE  dbo.Sp_GetScoreDriver @pStartDate, @pEndDate, @VehiclesByUserString, ''R'', @pScoreTable = @ScoreRouteOutput OUTPUT
				DECLARE @pScoreAdmTableXML XML;
				SET  @pScoreAdmTableXML = CAST(@ScoreAdmOutput  AS XML);
				
				DECLARE @ScoreAdmTable dbo.TypeDriverScore;
				
				Insert into  @ScoreAdmTable
				SELECT --DeviceSpeedId   = T.Item.value(''@DeviceSpeedId '', ''int''),
					   KmTraveled = T.Item.value(''@KmTraveled'', ''float''),
					   OverSpeed = T.Item.value(''@OverSpeed'', ''float''),
					   OverSpeedAmount  = T.Item.value(''@OverSpeedAmount'',  ''float''),
					   OverSpeedDistancePercentage = T.Item.value(''@OverSpeedDistancePercentage'', ''float''),
					   OverSpeedAverage = T.Item.value(''@OverSpeedAverage'', ''float''),
					   OverSpeedWeihgtedAverage = T.Item.value(''@OverSpeedWeihgtedAverage'', ''float''),
					   StandardDeviation = T.Item.value(''@StandardDeviation'', ''float''),
					   Variance = T.Item.value(''@Variance'', ''float''),
					   VariationCoefficient = T.Item.value(''@VariationCoefficient'', ''float''),
					   AverageScore = T.Item.value(''@AverageScore'', ''float''),
					   OverSpeedDistance=T.Item.value(''@OverSpeedDistance'', ''float'')
				FROM   @pScoreAdmTableXML.nodes(''Lista'') AS T(Item)
				
				DECLARE @pScoreRouteTableXML XML;
				SET  @pScoreRouteTableXML = CAST(@ScoreRouteOutput  AS XML);
				
				DECLARE @ScoreRouteTable dbo.TypeDriverScore;
				
				Insert into  @ScoreRouteTable
				SELECT --DeviceSpeedId   = T.Item.value(''@DeviceSpeedId '', ''int''),
					   KmTraveled = T.Item.value(''@KmTraveled'', ''float''),
					   OverSpeed = T.Item.value(''@OverSpeed'', ''float''),
					   OverSpeedAmount  = T.Item.value(''@OverSpeedAmount'',  ''float''),
					   OverSpeedDistancePercentage = T.Item.value(''@OverSpeedDistancePercentage'', ''float''),
					   OverSpeedAverage = T.Item.value(''@OverSpeedAverage'', ''float''),
					   OverSpeedWeihgtedAverage = T.Item.value(''@OverSpeedWeihgtedAverage'', ''float''),
					   StandardDeviation = T.Item.value(''@StandardDeviation'', ''float''),
					   Variance = T.Item.value(''@Variance'', ''float''),
					   VariationCoefficient = T.Item.value(''@VariationCoefficient'', ''float''),
					   AverageScore = T.Item.value(''@AverageScore'', ''float''),
					   OverSpeedDistance=T.Item.value(''@OverSpeedDistance'', ''float'')
				FROM   @pScoreRouteTableXML.nodes(''Lista'') AS T(Item)
				
				set @ScoreAdm=(select AverageScore from @ScoreAdmTable)
				set @ScoreRoute=(select AverageScore from @ScoreRouteTable)
			
				If (@ScoreAdm is not null or @ScoreRoute is not null)
				begin
					--Obtener nombre e identificación del usuario
					Select @Name = U.Name,
						   @Identification = D.Identification 
					  from General.Users U
					  inner join General.DriversUsers D on U.UserId = D.UserId
					 where U.UserId = @UserId
					
					If  @ScoreAdm is not null
						Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score,
						KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
						Values(@UserId, @Name, @Identification, ''SPADM'', @ScoreAdm,(select KmTraveled from @ScoreAdmTable),
						(select OverSpeed from @ScoreAdmTable),(select OverSpeedAmount from @ScoreAdmTable),(select OverSpeedDistancePercentage from @ScoreAdmTable),
						(select OverSpeedAverage from @ScoreAdmTable),(select OverSpeedWeihgtedAverage from @ScoreAdmTable),(select StandardDeviation from @ScoreAdmTable)
						,(select Variance from @ScoreAdmTable),(select VariationCoefficient from @ScoreAdmTable),(select OverSpeedDistance from @ScoreAdmTable))

					If  @ScoreRoute is not null
						Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score,
						KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
						Values(@UserId, @Name, @Identification, ''SPROU'', @ScoreRoute,(select KmTraveled from @ScoreRouteTable),
						(select OverSpeed from @ScoreRouteTable),(select OverSpeedAmount from @ScoreRouteTable),(select OverSpeedDistancePercentage from @ScoreRouteTable),
						(select OverSpeedAverage from @ScoreRouteTable),(select OverSpeedWeihgtedAverage from @ScoreRouteTable),(select StandardDeviation from @ScoreRouteTable)
						,(select Variance from @ScoreRouteTable),(select VariationCoefficient from @ScoreRouteTable),(select OverSpeedDistance from @ScoreRouteTable))
				end
				
				delete from @ScoreAdmTable
				delete from @ScoreRouteTable
				--Limpia la  tabla para  la nueva seleccion.
				DELETE FROM @VehiclesByUser
			END
			--- Siguiente********************
			Select @UserId = Min(U.UserId) 
			  from General.Users U
			  inner join General.DriversUsers D on U.UserId = D.UserId
			 Where U.IsActive =  1 
			   and U.UserId > @UserId
		END --Fin del While
	end
	else
	begin
		--Para la invocación por mes, recupera las calificaciones precalculadas
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance	)
		Select DS.UserId, U.Name, D.Identification, DS.ScoreType, DS.Score ,DS.KmTraveled,
						DS.OverSpeed ,
						DS.OverSpeedAmount ,
						DS.OverSpeedDistancePercentage ,
						DS.OverSpeedAverage ,
						DS.OverSpeedWeihgtedAverage ,
						DS.StandardDeviation ,
						DS.Variance ,
						DS.VariationCoefficient,
						DS.OverSpeedDistance		
		  from Operation.DriversScores DS
		  inner join General.Users U on DS.UserId = U.UserId
		  inner join General.DriversUsers D on DS.UserId = D.UserId
		 where DS.CustomerId = @pCustomerId
		   and DS.ScoreYear = @pYear
		   and DS.ScoreMonth = @pMonth
	end

	If @CountWeight > 0 
	begin
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score, Photo,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
		Select DS.UserId, DS.Name, DS.Identification, ''TOTAL'', SUM(DS.Score * WS.Weight / 100), U.Photo,
						DS.KmTraveled,
						DS.OverSpeed ,
						DS.OverSpeedAmount ,
						DS.OverSpeedDistancePercentage ,
						DS.OverSpeedAverage ,
						DS.OverSpeedWeihgtedAverage ,
						DS.StandardDeviation ,
						DS.Variance ,
						DS.VariationCoefficient,
						DS.OverSpeedDistance	
	      from #LocalDriverScore DS
	     inner join Operation.ScoreTypes ST on DS.ScoreType = ST.Code 
		 inner join Operation.WeightedScoreSettings WS on ST.ScoreTypeId = WS.ScoreTypeId
		 inner join General.Users U on DS.UserId = U.UserId
		 where WS.CustomerId = @pCustomerId
		group by DS.UserId, DS.Name, DS.Identification, U.Photo,
						DS.KmTraveled,
						DS.OverSpeed ,
						DS.OverSpeedAmount ,
						DS.OverSpeedDistancePercentage ,
						DS.OverSpeedAverage ,
						DS.OverSpeedWeihgtedAverage ,
						DS.StandardDeviation ,
						DS.Variance ,
						DS.VariationCoefficient,
						DS.OverSpeedDistance	
		  	 	  
	end
	else
	begin
	
		--No existe configuración de calificación por peso
		--Se asigna la calificación de carretera como la calificación global
		Insert into #LocalDriverScore (UserID, Name, Identification , ScoreType, Score, Photo,
						KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
		Select DS.UserId, DS.Name   , DS.Identification, ''TOTAL'', DS.Score, U.Photo,
						DS.KmTraveled,
						DS.OverSpeed ,
						DS.OverSpeedAmount ,
						DS.OverSpeedDistancePercentage ,
						DS.OverSpeedAverage ,
						DS.OverSpeedWeihgtedAverage ,
						DS.StandardDeviation ,
						DS.Variance ,
						DS.VariationCoefficient,
						DS.OverSpeedDistance	
	      from #LocalDriverScore DS
		 inner join General.Users U on DS.UserId = U.UserId
	     where DS.ScoreType = ''SPROU''
	end

	If @pReportType = ''D''
	begin
		Select @pCustomerId AS CustomerId,@pCustomerName as CustomerName, DS.DriverScoreId ,DS.UserId,Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo,
		DS.KmTraveled ,
		DS.OverSpeed ,
		DS.OverSpeedAmount ,
		DS.OverSpeedDistancePercentage ,
		DS.OverSpeedAverage ,
		DS.OverSpeedWeihgtedAverage ,
		DS.StandardDeviation ,
		DS.Variance ,
		DS.VariationCoefficient,	DS.OverSpeedDistance	   from #LocalDriverScore DS 
		order by DS.ScoreType, DS.Score asc 
	end
	else
	begin
		Select @pCustomerId AS CustomerId,@pCustomerName as CustomerName, DS.DriverScoreId ,DS.UserId,DS.Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo , IsNull(DSA.Score, 0) ScoreSpeedAdmin, IsNull(DSR.Score, 0) ScoreSpeedRoute
		,DS.KmTraveled ,
		DS.OverSpeed ,
		DS.OverSpeedAmount ,
		DS.OverSpeedDistancePercentage ,
		DS.OverSpeedAverage ,
		DS.OverSpeedWeihgtedAverage ,
		DS.StandardDeviation ,
		DS.Variance ,
		DS.VariationCoefficient
		,DS.OverSpeedDistance	
		  from #LocalDriverScore DS
		 left join #LocalDriverScore DSA on DS.UserId = DSA.UserId and DSA.ScoreType = ''SPADM''
		 left join #LocalDriverScore DSR on DS.UserId = DSR.UserId and DSR.ScoreType = ''SPROU''
		 where DS.ScoreType = ''TOTAL''
		order by DS.ScoreType, DS.Score asc
	end	
	
	Drop table #LocalDriverScore

END
' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_GetGlobalScoreByVehicle]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetGlobalScoreByVehicle]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 28/01/2015
-- Description:	Retrieve Global Score By Vehicle Information
-- ================================================================================================

create PROCEDURE [Operation].[Sp_GetGlobalScoreByVehicle] 
( @pCustomerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1),			--Posibles valores S:Summarized, D:Detailed
  @pVehicleId INT			
)
AS
BEGIN

	--Declare @pStartDate datetime =  ''20141019 03:45:02.000'',
	--@pEndDate datetime = ''20141128 11:30am''
	--Verificar configuración de la ponderación
	--Declare @pStartDate datetime =  ''20141019 03:45:02.000'',
	--@pEndDate datetime = ''20141128 11:30am''
	--Verificar configuración de la ponderación
		--Declare @pStartDate datetime =  ''20141019 03:45:02.000'',
	--@pEndDate datetime = ''20141128 11:30am''
	--Verificar configuración de la ponderación
	Declare @TotalWeight numeric(6,2), 
	        @CountWeight int
	
	Select @TotalWeight = IsNull(SUM(Weight),0),
		   @CountWeight = COUNT(1)
	  from Operation.WeightedScoreSettings
	 where CustomerId = @pCustomerId
	
	--If @CountWeight > 0 and @TotalWeight <> 100
	--begin
		--Los pesos para la ponderación no suman 100
	--	RAISERROR (''101'', 16, 1)
	--	RETURN
	--end

	--Set Dates 
	Declare @DataPerMonth bit = 0 
	If @pEndDate is null
		If @pStartDate is null
		begin
			If @pMonth is null or @pYear is null
			begin
				RAISERROR (''100'', 16, 1)
				RETURN
			end
			else
			begin
				--Set @pStartDate = [General].[Fn_GetFirstDayOfMonth](@pYear, @pMonth)
				--Set @pEndDate = DATEADD(MM, 1, @pStartDate)
				Set @DataPerMonth = 1
			end
		end
		else
		begin
			Set @pEndDate = DATEADD(DD, 1, @pStartDate)
		end

	--DECLARE @MyTable  dbo.TypeDeviceSpeed
	Declare @VehiclesByUser table
	(Device int,
	 Vehicle int,
	 StartOdometer int,
	 EndOdometer int,
	 StartDate datetime,
	 EndDate datetime)

	Create table #LocalDriverScore
	(DriverScoreId INT Identity(1,1),
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 ScoreType varchar(5),
	 Score Float,
	 Photo varchar(max),
	 VehicleId INT
	 )
	Declare @UserId Int
	Declare @Name varchar(250),
			@Identification varchar(50)
	Declare  @ScoreAdmOutput nvarchar(max),
			 @ScoreRouteOutput nvarchar(max)
	Declare  @ScoreAdm float,
			 @ScoreRoute float

	If @DataPerMonth = 0 
	begin
		Select @UserId = Min(U.UserId) 
		  from General.Users U
		  inner join General.DriversUsers D on U.UserId = D.UserId
		 Where U.IsActive =  1 

		--Begin transaction
		 
		While  @UserId  is not null
		BEGIN
			Insert into @VehiclesByUser
				(Device, Vehicle, StartOdometer, EndOdometer, StartDate, EndDate)  
			--Select   V.IntrackReference As Device,CASE WHEN  VU.InitialOdometer IS NULL THEN 0 eLSE VU.InitialOdometer end  ,CASE WHEN VU.EndOdometer IS NULL THEN 0 eLSE EndOdometer end    , VU.InsertDate StartDate ,VU.LastDateDriving EndDate
			Select V.DeviceReference, V.IntrackReference, IsNull(VU.InitialOdometer, 0), IsNull(VU.EndOdometer,0), VU.InsertDate, IsNull(VU.LastDateDriving, GETDATE())
			from General.VehiclesByUser VU 
			inner Join General.Vehicles V ON  V.VehicleId = VU.VehicleId
			inner JOIN General.Users U  ON U.UserId = VU.UserId
			Where U.UserId =  @UserId 
			  AND V.VehicleId=@pVehicleId
			  And V.DeviceReference is not null
			  and ((VU.InsertDate between @pStartDate and @pEndDate)
			   or  (IsNull(VU.LastDateDriving, GETDATE()) between @pStartDate and @pEndDate )
			   or  (VU.InsertDate <= @pStartDate and IsNull(VU.LastDateDriving, GETDATE()) >= @pEndDate))
			order by V.DeviceReference, VU.InsertDate, VU.LastDateDriving

			if (select COUNT(*) FROM @VehiclesByUser ) > 0 
			BEGIN 
				DECLARE @VehiclesByUserString varchar(1000);
				SET @VehiclesByUserString = (SElect * FROm  @VehiclesByUser AS Lista FOR XML AUTO)
				EXECUTE  dbo.Sp_GetScoreDriver @pStartDate, @pEndDate, @VehiclesByUserString, ''A'', @pScoreTable = @ScoreAdmOutput OUTPUT
				EXECUTE  dbo.Sp_GetScoreDriver @pStartDate, @pEndDate, @VehiclesByUserString, ''R'', @pScoreTable = @ScoreRouteOutput OUTPUT

				DECLARE @pScoreAdmTableXML XML;
				SET  @pScoreAdmTableXML = CAST(@ScoreAdmOutput  AS XML);
				
				DECLARE @ScoreAdmTable dbo.TypeDriverScore;
				
				Insert into  @ScoreAdmTable
				SELECT --DeviceSpeedId   = T.Item.value(''@DeviceSpeedId '', ''int''),
					   KmTraveled = T.Item.value(''@KmTraveled'', ''float''),
					   OverSpeed = T.Item.value(''@OverSpeed'', ''float''),
					   OverSpeedAmount  = T.Item.value(''@OverSpeedAmount'',  ''float''),
					   OverSpeedDistancePercentage = T.Item.value(''@OverSpeedDistancePercentage'', ''float''),
					   OverSpeedAverage = T.Item.value(''@OverSpeedAverage'', ''float''),
					   OverSpeedWeihgtedAverage = T.Item.value(''@OverSpeedWeihgtedAverage'', ''float''),
					   StandardDeviation = T.Item.value(''@StandardDeviation'', ''float''),
					   Variance = T.Item.value(''@Variance'', ''float''),
					   VariationCoefficient = T.Item.value(''@VariationCoefficient'', ''float''),
					   AverageScore = T.Item.value(''@AverageScore'', ''float''), 
					   OverSpeedDistance = T.Item.value(''@OverSpeedDistance'', ''float'')
				FROM   @pScoreAdmTableXML.nodes(''Lista'') AS T(Item)
				
				DECLARE @pScoreRouteTableXML XML;
				SET  @pScoreRouteTableXML = CAST(@ScoreRouteOutput  AS XML);
				
				DECLARE @ScoreRouteTable dbo.TypeDriverScore;
				
				Insert into  @ScoreRouteTable
				SELECT --DeviceSpeedId   = T.Item.value(''@DeviceSpeedId '', ''int''),
					   KmTraveled = T.Item.value(''@KmTraveled'', ''float''),
					   OverSpeed = T.Item.value(''@OverSpeed'', ''float''),
					   OverSpeedAmount  = T.Item.value(''@OverSpeedAmount'',  ''float''),
					   OverSpeedDistancePercentage = T.Item.value(''@OverSpeedDistancePercentage'', ''float''),
					   OverSpeedAverage = T.Item.value(''@OverSpeedAverage'', ''float''),
					   OverSpeedWeihgtedAverage = T.Item.value(''@OverSpeedWeihgtedAverage'', ''float''),
					   StandardDeviation = T.Item.value(''@StandardDeviation'', ''float''),
					   Variance = T.Item.value(''@Variance'', ''float''),
					   VariationCoefficient = T.Item.value(''@VariationCoefficient'', ''float''),
					   AverageScore = T.Item.value(''@AverageScore'', ''float''),
					   OverSpeedDistance=T.Item.value(''@OverSpeedDistance'', ''float'')
				FROM   @pScoreRouteTableXML.nodes(''Lista'') AS T(Item)
				
				set @ScoreAdm=(select AverageScore from @ScoreAdmTable)
				set @ScoreRoute=(select AverageScore from @ScoreRouteTable)
				
				delete from @ScoreAdmTable
				delete from @ScoreRouteTable
				If (@ScoreAdm is not null or @ScoreRoute is not null)
				begin
					--Obtener nombre e identificación del usuario
					Select @Name = U.Name,
						   @Identification = D.Identification 
					  from General.Users U
					  inner join General.DriversUsers D on U.UserId = D.UserId
					 where U.UserId = @UserId
					
					If  @ScoreAdm is not null
						Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score)
						Values(@UserId, @Name, @Identification, ''SPADM'', @ScoreAdm)

					If  @ScoreRoute is not null
						Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score)
						Values(@UserId, @Name, @Identification, ''SPROU'', @ScoreRoute)
				end
				--Limpia la  tabla para  la nueva seleccion.
				DELETE FROM @VehiclesByUser
			END
			--- Siguiente********************
			Select @UserId = Min(U.UserId) 
			  from General.Users U
			  inner join General.DriversUsers D on U.UserId = D.UserId
			 Where U.IsActive =  1 
			   and U.UserId > @UserId
		END --Fin del While
	end
	else
	begin
		--Para la invocación por mes, recupera las calificaciones precalculadas
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score,VehicleId)
		Select DS.UserId, U.Name, D.Identification, DS.ScoreType, DS.Score ,VBU.VehicleId					
		  from Operation.DriversScores DS
		  inner join General.Users U on DS.UserId = U.UserId
		  inner join General.DriversUsers D on DS.UserId = D.UserId
		  inner join General.VehiclesByUser vbu on vbu.UserId = D.UserId
		 where DS.CustomerId = @pCustomerId
		   and DS.ScoreYear = @pYear
		   and DS.ScoreMonth = @pMonth
		   AND vbu.VehicleId=@pVehicleId
		  
	end

	--calificación global
	If @CountWeight > 0 
	begin
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score, Photo)
		Select DS.UserId, DS.Name, DS.Identification, ''TOTAL'', SUM(DS.Score * WS.Weight / 100), U.Photo
	      from #LocalDriverScore DS
	     inner join Operation.ScoreTypes ST on DS.ScoreType = ST.Code 
		 inner join Operation.WeightedScoreSettings WS on ST.ScoreTypeId = WS.ScoreTypeId
		 inner join General.Users U on DS.UserId = U.UserId
		 where WS.CustomerId = @pCustomerId
		group by DS.UserId, DS.Name, DS.Identification, U.Photo		
		  	 	  
	end
	else
	begin
		--No existe configuración de calificación por peso
		--Se asigna la calificación de carretera como la calificación global
		Insert into #LocalDriverScore (UserID, Name, Identification , ScoreType, Score, Photo)
		Select DS.UserId, DS.Name   , DS.Identification, ''TOTAL'', DS.Score, U.Photo
	      from #LocalDriverScore DS
		 inner join General.Users U on DS.UserId = U.UserId
	     where DS.ScoreType = ''SPROU''
	end

	If @pReportType = ''D''
	begin
		Select DS.DriverScoreId ,DS.UserId,Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo  from #LocalDriverScore DS
		order by DS.ScoreType, DS.Score asc
	end
	else
	begin
		Select DS.DriverScoreId ,DS.UserId,DS.Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo , IsNull(DSA.Score, 0) ScoreSpeedAdmin, IsNull(DSR.Score, 0) ScoreSpeedRoute
		  from #LocalDriverScore DS
		 left join #LocalDriverScore DSA on DS.UserId = DSA.UserId and DSA.ScoreType = ''SPADM''
		 left join #LocalDriverScore DSR on DS.UserId = DSR.UserId and DSR.ScoreType = ''SPROU''
		 where DS.ScoreType = ''TOTAL''
		order by DS.ScoreType, DS.Score asc
	end	
	
	Drop table #LocalDriverScore


END
' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_GetGlobalScoreAllDriver]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetGlobalScoreAllDriver]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [Operation].[Sp_GetGlobalScoreAllDriver] 
( @pCustomerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)			--Posibles valores S:Summarized, D:Detailed
)
as
BEGIN

	--Declare @pStartDate datetime =  ''20141019 03:45:02.000'',
	--@pEndDate datetime = ''20141128 11:30am''
	--Verificar configuración de la ponderación
	Declare @TotalWeight numeric(6,2), 
	        @CountWeight int
	
	Select @TotalWeight = IsNull(SUM(Weight),0),
		   @CountWeight = COUNT(1)
	  from Operation.WeightedScoreSettings
	 where CustomerId = @pCustomerId
	
	--If @CountWeight > 0 and @TotalWeight <> 100
	--begin
		--Los pesos para la ponderación no suman 100
	--	RAISERROR (''101'', 16, 1)
	--	RETURN
	--end

	--Set Dates 
	Declare @DataPerMonth bit = 0 
	If @pEndDate is null
		If @pStartDate is null
		begin
			If @pMonth is null or @pYear is null
			begin
				RAISERROR (''100'', 16, 1)
				RETURN
			end
			else
			begin
				--Set @pStartDate = [General].[Fn_GetFirstDayOfMonth](@pYear, @pMonth)
				--Set @pEndDate = DATEADD(MM, 1, @pStartDate)
				Set @DataPerMonth = 1
			end
		end
		else
		begin
			Set @pEndDate = DATEADD(DD, 1, @pStartDate)
		end

	--DECLARE @MyTable  dbo.TypeDeviceSpeed
	Declare @VehiclesByUser table
	(Device int,
	 Vehicle int,
	 StartOdometer int,
	 EndOdometer int,
	 StartDate datetime,
	 EndDate datetime)

	Create table #LocalDriverScore
	(DriverScoreId INT Identity(1,1),
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 ScoreType varchar(5),
	 Score Float,
	 Photo varchar(max)
	 )
	Declare @UserId Int
	Declare @Name varchar(250),
			@Identification varchar(50)
		
	Declare  @ScoreAdmOutput nvarchar(max),
			 @ScoreRouteOutput nvarchar(max)
			 	
	Declare  @ScoreAdm Float,
			 @ScoreRoute Float

	If @DataPerMonth = 0 
	begin
		Select @UserId = Min(U.UserId) 
		  from General.Users U
		  inner join General.DriversUsers D on U.UserId = D.UserId
		 Where U.IsActive =  1 

		--Begin transaction
		 
		While  @UserId  is not null
		BEGIN
			Insert into @VehiclesByUser
				(Device, Vehicle, StartOdometer, EndOdometer, StartDate, EndDate)  
			--Select   V.IntrackReference As Device,CASE WHEN  VU.InitialOdometer IS NULL THEN 0 eLSE VU.InitialOdometer end  ,CASE WHEN VU.EndOdometer IS NULL THEN 0 eLSE EndOdometer end    , VU.InsertDate StartDate ,VU.LastDateDriving EndDate
			Select V.DeviceReference, V.IntrackReference, IsNull(VU.InitialOdometer, 0), IsNull(VU.EndOdometer,0), VU.InsertDate, IsNull(VU.LastDateDriving, GETDATE())
			from General.VehiclesByUser VU 
			inner Join General.Vehicles V ON  V.VehicleId = VU.VehicleId
			inner JOIN General.Users U  ON U.UserId = VU.UserId
			Where U.UserId =  @UserId 
			  And V.DeviceReference is not null
			  and ((VU.InsertDate between @pStartDate and @pEndDate)
			   or  (IsNull(VU.LastDateDriving, GETDATE()) between @pStartDate and @pEndDate )
			   or  (VU.InsertDate <= @pStartDate and IsNull(VU.LastDateDriving, GETDATE()) >= @pEndDate))
			order by V.DeviceReference, VU.InsertDate, VU.LastDateDriving

			if (select COUNT(*) FROM @VehiclesByUser ) > 0 
			BEGIN 
				DECLARE @VehiclesByUserString varchar(1000);
				SET @VehiclesByUserString = (SElect * FROm  @VehiclesByUser AS Lista FOR XML AUTO)
				EXECUTE  dbo.Sp_GetScoreDriver @pStartDate, @pEndDate, @VehiclesByUserString, ''A'', @pScoreTable = @ScoreAdmOutput OUTPUT
				EXECUTE  dbo.Sp_GetScoreDriver @pStartDate, @pEndDate, @VehiclesByUserString, ''R'', @pScoreTable = @ScoreRouteOutput OUTPUT
				DECLARE @pScoreAdmTableXML XML;
				SET  @pScoreAdmTableXML = CAST(@ScoreAdmOutput  AS XML);
				
				DECLARE @ScoreAdmTable dbo.TypeDriverScore;
				
				Insert into  @ScoreAdmTable
				SELECT --DeviceSpeedId   = T.Item.value(''@DeviceSpeedId '', ''int''),
					   KmTraveled = T.Item.value(''@KmTraveled'', ''float''),
					   OverSpeed = T.Item.value(''@OverSpeed'', ''float''),
					   OverSpeedAmount  = T.Item.value(''@OverSpeedAmount'',  ''float''),
					   OverSpeedDistancePercentage = T.Item.value(''@OverSpeedDistancePercentage'', ''float''),
					   OverSpeedAverage = T.Item.value(''@OverSpeedAverage'', ''float''),
					   OverSpeedWeihgtedAverage = T.Item.value(''@OverSpeedWeihgtedAverage'', ''float''),
					   StandardDeviation = T.Item.value(''@StandardDeviation'', ''float''),
					   Variance = T.Item.value(''@Variance'', ''float''),
					   VariationCoefficient = T.Item.value(''@VariationCoefficient'', ''float''),
					   AverageScore = T.Item.value(''@AverageScore'', ''float''),
					   OverSpeedDistance= T.Item.value(''@OverSpeedDistance'', ''float'')
				FROM   @pScoreAdmTableXML.nodes(''Lista'') AS T(Item)
				
				DECLARE @pScoreRouteTableXML XML;
				SET  @pScoreRouteTableXML = CAST(@ScoreRouteOutput  AS XML);
				
				DECLARE @ScoreRouteTable dbo.TypeDriverScore;
				
				Insert into  @ScoreRouteTable
				SELECT --DeviceSpeedId   = T.Item.value(''@DeviceSpeedId '', ''int''),
					   KmTraveled = T.Item.value(''@KmTraveled'', ''float''),
					   OverSpeed = T.Item.value(''@OverSpeed'', ''float''),
					   OverSpeedAmount  = T.Item.value(''@OverSpeedAmount'',  ''float''),
					   OverSpeedDistancePercentage = T.Item.value(''@OverSpeedDistancePercentage'', ''float''),
					   OverSpeedAverage = T.Item.value(''@OverSpeedAverage'', ''float''),
					   OverSpeedWeihgtedAverage = T.Item.value(''@OverSpeedWeihgtedAverage'', ''float''),
					   StandardDeviation = T.Item.value(''@StandardDeviation'', ''float''),
					   Variance = T.Item.value(''@Variance'', ''float''),
					   VariationCoefficient = T.Item.value(''@VariationCoefficient'', ''float''),
					   AverageScore = T.Item.value(''@AverageScore'', ''float''),
					   OverSpeedDistance= T.Item.value(''@OverSpeedDistance'', ''float'')
				FROM   @pScoreRouteTableXML.nodes(''Lista'') AS T(Item)
				
				set @ScoreAdm=(select AverageScore from @ScoreAdmTable)
				set @ScoreRoute=(select AverageScore from @ScoreRouteTable)
				delete from @ScoreAdmTable
				delete from @ScoreRouteTable
				If (@ScoreAdm is not null or @ScoreRoute is not null)
				begin
					--Obtener nombre e identificación del usuario
					Select @Name = U.Name,
						   @Identification = D.Identification 
					  from General.Users U
					  inner join General.DriversUsers D on U.UserId = D.UserId
					 where U.UserId = @UserId
					
					If  @ScoreAdm is not null
						Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score)
						Values(@UserId, @Name, @Identification, ''SPADM'', @ScoreAdm)

					If  @ScoreRoute is not null
						Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score)
						Values(@UserId, @Name, @Identification, ''SPROU'', @ScoreRoute)
				end
				--Limpia la  tabla para  la nueva seleccion.
				DELETE FROM @VehiclesByUser
			END
			--- Siguiente********************
			Select @UserId = Min(U.UserId) 
			  from General.Users U
			  inner join General.DriversUsers D on U.UserId = D.UserId
			 Where U.IsActive =  1 
			   and U.UserId > @UserId
		END --Fin del While
	end
	else
	begin
		--Para la invocación por mes, recupera las calificaciones precalculadas
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score)
		Select DS.UserId, U.Name, D.Identification, DS.ScoreType, DS.Score 					
		  from Operation.DriversScores DS
		  inner join General.Users U on DS.UserId = U.UserId
		  inner join General.DriversUsers D on DS.UserId = D.UserId
		 where DS.CustomerId = @pCustomerId
		   and DS.ScoreYear = @pYear
		   and DS.ScoreMonth = @pMonth
	end

	--calificación global
	If @CountWeight > 0 
	begin
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score, Photo)
		Select DS.UserId, DS.Name, DS.Identification, ''TOTAL'', SUM(DS.Score * WS.Weight / 100), U.Photo
	      from #LocalDriverScore DS
	     inner join Operation.ScoreTypes ST on DS.ScoreType = ST.Code 
		 inner join Operation.WeightedScoreSettings WS on ST.ScoreTypeId = WS.ScoreTypeId
		 inner join General.Users U on DS.UserId = U.UserId
		 where WS.CustomerId = @pCustomerId
		group by DS.UserId, DS.Name, DS.Identification, U.Photo		
		  	 	  
	end
	else
	begin
		--No existe configuración de calificación por peso
		--Se asigna la calificación de carretera como la calificación global
		Insert into #LocalDriverScore (UserID, Name, Identification , ScoreType, Score, Photo)
		Select DS.UserId, DS.Name   , DS.Identification, ''TOTAL'', DS.Score, U.Photo
	      from #LocalDriverScore DS
		 inner join General.Users U on DS.UserId = U.UserId
	     where DS.ScoreType = ''SPROU''
	end

	If @pReportType = ''D''
	begin
		Select DS.DriverScoreId ,DS.UserId,Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo  from #LocalDriverScore DS
		order by DS.ScoreType, DS.Score asc
	end
	else
	begin
		Select DS.DriverScoreId ,DS.UserId,DS.Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo , IsNull(DSA.Score, 0) ScoreSpeedAdmin, IsNull(DSR.Score, 0) ScoreSpeedRoute
		  from #LocalDriverScore DS
		 left join #LocalDriverScore DSA on DS.UserId = DSA.UserId and DSA.ScoreType = ''SPADM''
		 left join #LocalDriverScore DSR on DS.UserId = DSR.UserId and DSR.ScoreType = ''SPROU''
		 where DS.ScoreType = ''TOTAL''
		order by DS.ScoreType, DS.Score asc
	end	
	
	Drop table #LocalDriverScore
END
' 
END
GO
/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFence_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFence_Alarm]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/26/2015
-- Description:	Add alarm by geofence
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_GeoFence_Alarm]
AS
BEGIN 

	BEGIN TRY 

		DECLARE @RowNum INT = 0
		DECLARE @lErrorMessage NVARCHAR(4000)
	    DECLARE @lErrorSeverity INT
		DECLARE @lErrorState INT

		DECLARE @GeoFencesList TABLE
		(
			[Id] INT IDENTITY(1,1) NOT NULL ,
			[AlarmId] INT,
			[TypeId] INT,
			[CustomerId] INT,
			[AlarmPhone] VARCHAR(100),
			[AlarmEmail] VARCHAR(500),
			[GeoFenceId] INT,
			[GeoFenceName] VARCHAR(50),
			[Polygon] GEOMETRY,
			[In] BIT,
			[Out] BIT,
			[GeoFenceInsertDate] DATETIME
		)

		DECLARE @VehicleByGeoFence TABLE
		(
			[GeoFenceId] INT,
			[TypeId] INT,
			[VehicleId] INT,
			[PlateId] VARCHAR(10),
			[GeofenceIn] BIT,
			[GeofenceOut] BIT
		)	

		DECLARE @VehicleReportLast TABLE
		(
			[VehicleId] INT,
			[Device] INT,
			[GPSDateTime] DATETIME,
			[Longitude] FLOAT,
			[Latitude] FLOAT
		)

		DECLARE @ChangedRecords TABLE
		(
			[GeoFenceReportAlarmId] INT,
			[VehicleId] INT,
			[GPSDateTime] DATETIME,
			[Longitude] FLOAT,
			[Latitude] FLOAT,
			[LastGPSDateTime] DATETIME,
			[LastLongitude] FLOAT,
			[LastLatitude] FLOAT,
			[InsertDate] DATETIME,
			[ModifyDate] DATETIME
		)	

		DECLARE @InOutRecordsByVehicle TABLE
		(
			[Id] INT IDENTITY(1,1) NOT NULL ,
			[AlarmId] INT,
			[TypeId] INT,
			[CustomerId] INT,
			[AlarmPhone] VARCHAR(MAX),
			[AlarmEmail] VARCHAR(MAX),
			[GeoFenceId] INT,
			[GeoFenceName] VARCHAR(50),
			[In] BIT,
			[Out] BIT,
			[GeofenceIn] BIT,
			[GeofenceOut] BIT,
			[VehicleId] INT,
			[PlateId] VARCHAR(10)
		)	
	
		INSERT INTO @GeoFencesList
			([AlarmId],
			[TypeId],
			[CustomerId],
			[AlarmPhone],
			[AlarmEmail],
			[GeoFenceId],
			[GeoFenceName],
			[Polygon],
			[In],
			[Out],
			[GeoFenceInsertDate])
		SELECT 
			b.[AlarmId],
			a.[TypeId],
			b.[CustomerId],
			b.[Phone] [AlarmPhone],
			b.[Email] [AlarmEmail],
			c.[GeoFenceId],
			c.[Name] [GeoFenceName],
			c.[Polygon],
			c.[In],
			c.[Out],
			c.[InsertDate] [GeoFenceInsertDate]
		 FROM 
			[General].[Types] a 
			INNER JOIN [General].[Alarms] b ON a.[TypeId] = b.[AlarmTriggerId]
			INNER JOIN [Efficiency].[GeoFences] c ON b.[EntityId] = c.[GeoFenceId]
		WHERE 
			a.[Code] = ''CUSTOMER_ALARM_TRIGGER_GEOFENCE'' AND 
			a.[IsActive] = 1 AND
			b.[Active] = 1 AND 
			c.[Active] = 1

		INSERT INTO @VehicleByGeoFence
			([GeoFenceId],
			[VehicleId],
			[PlateId],
			[GeofenceIn],
			[GeofenceOut])
			(SELECT 
				a.[GeoFenceId], 
				a.[VehicleId],
				c.[PlateId],
				b.[In],
				b.[Out]
			FROM [Efficiency].[VehiclesByGeoFence] a
				INNER JOIN @GeoFencesList b ON a.[GeoFenceId] = b.[GeoFenceId]
				INNER JOIN [General].[Vehicles] c ON a.[VehicleId] = c.[VehicleId]
			UNION
			SELECT 
				a.[GeoFenceId], 
				c.[VehicleId],
				d.[PlateId] ,
				b.[In],
				b.[Out]
			FROM [Efficiency].[VehicleGroupsByGeoFence] a
				INNER JOIN @GeoFencesList b ON a.[GeoFenceId] = b.[GeoFenceId]
				INNER JOIN [General].[VehiclesByGroup] c ON a.[VehicleGroupId] = c.[VehicleGroupId]
				INNER JOIN [General].[Vehicles] d ON c.[VehicleId] = d.[VehicleId]
			UNION
			SELECT 
				a.[GeoFenceId], 
				e.[VehicleId], 
				e.[PlateId],
				b.[In],
				b.[Out]
			FROM [Efficiency].[VehicleUnitsByGeoFence] a
				INNER JOIN @GeoFencesList b ON a.[GeoFenceId] = b.[GeoFenceId]
				INNER JOIN [General].[VehicleUnits] c ON a.[UnitId] = c.[UnitId]
				INNER JOIN [General].[VehicleCostCenters] d ON c.[UnitId] = d.[UnitId]
				INNER JOIN [General].[Vehicles] e ON e.[CostCenterId] = d.[CostCenterId]
			UNION
			SELECT 
				a.[GeoFenceId], 
				c.[VehicleId],
				c.[PlateId],
				b.[In],
				b.[Out]
			FROM [Efficiency].[VehicleCostCenterByGeoFence] a
				INNER JOIN @GeoFencesList b ON a.[GeoFenceId] = b.[GeoFenceId]
				INNER JOIN [General].[Vehicles] c ON a.[CostCenterId] = c.[CostCenterId])

			INSERT INTO @VehicleReportLast
				([VehicleId],
				[Device],
				[GPSDateTime],
				[Longitude],
				[Latitude])
			SELECT 
				f.[VehicleId],
				a.[Device],
				a.[GPSDateTime],
				a.[Longitude],
				a.[Latitude]
			FROM [dbo].[Report_Last] a
				INNER JOIN [dbo].[Devices] b ON a.[Device] = b.[Device]
				INNER JOIN [DispositivosAVL] c ON b.[UnitID] = c.[numeroimei]
				INNER JOIN [Composiciones] d ON c.[dispositivo] = d.[dispositivo]
				INNER JOIN [Vehiculos] e ON e.[vehiculo] = d.[vehiculo]
				INNER JOIN [General].[Vehicles] f ON e.[vehiculo] = f.[IntrackReference]
				INNER JOIN (SELECT DISTINCT [VehicleId] FROM @VehicleByGeoFence) g ON f.[VehicleId] = g.[VehicleId]
		
		UPDATE [Efficiency].[GeoFenceReportAlarm] 
			SET [GPSDateTime] = [LastGPSDateTime],
				[Longitude] = [LastLongitude],
				[Latitude] = [LastLatitude]

		INSERT INTO [Efficiency].[GeoFenceReportAlarm] 
			([VehicleId], 
			[GPSDateTime], 
			[Longitude], 
			[Latitude], 
			[LastGPSDateTime], 
			[LastLongitude], 
			[LastLatitude],
			[InsertDate])
		SELECT 
			[VehicleId], 
			[GPSDateTime], 
			[Longitude], 
			[Latitude], 
			[GPSDateTime], 
			[Longitude], 
			[Latitude],
			GETUTCDATE()
		FROM 
			@VehicleReportLast
		WHERE 
			[VehicleId] NOT IN (SELECT [VehicleId] FROM [Efficiency].[GeoFenceReportAlarm])

		UPDATE a
		SET a.[LastGPSDateTime] = b.[GPSDateTime], 
			a.[LastLongitude] = b.[Longitude], 
			a.[LastLatitude] = b.[Latitude], 
			a.[ModifyDate] = GETUTCDATE()
		FROM 
			[Efficiency].[GeoFenceReportAlarm] a
			INNER JOIN @VehicleReportLast b ON a.[VehicleId] = b.[VehicleId]
		WHERE 
			a.[LastGPSDateTime] <> b.[GPSDateTime]

		INSERT INTO @ChangedRecords 
			([GeoFenceReportAlarmId],
			[VehicleId],
			[GPSDateTime],
			[Longitude],
			[Latitude],
			[LastGPSDateTime],
			[LastLongitude],
			[LastLatitude],
			[InsertDate],
			[ModifyDate])
		SELECT 
			[GeoFenceReportAlarmId],
			[VehicleId],
			[GPSDateTime],
			[Longitude],
			[Latitude],
			[LastGPSDateTime],
			[LastLongitude],
			[LastLatitude],
			[InsertDate],
			[ModifyDate]
		FROM 
			[Efficiency].[GeoFenceReportAlarm]
		WHERE 
			[GPSDateTime] <> [LastGPSDateTime] AND 
			[Longitude] <> [LastLongitude] AND 
			[Latitude] <> [LastLatitude]

		Select @RowNum = Count(*) From @GeoFencesList
	
		WHILE @RowNum > 0
			BEGIN
				DECLARE @Id INT
				DECLARE @AlarmId INT
				DECLARE @TypeId INT
				DECLARE @CustomerId INT
				DECLARE @AlarmPhone VARCHAR(MAX)
				DECLARE @AlarmEmail varchar(max)
				DECLARE @GeoFenceId INT
				DECLARE @GeoFenceName VARCHAR(50)
				DECLARE @Polygon GEOGRAPHY
				DECLARE @In BIT
				DECLARE @Out BIT
				DECLARE @GeoFenceInsertDate DATETIME
	
				SELECT  @Id = [Id], 
					@AlarmId = [AlarmId], 
					@TypeId = [TypeId], 
					@CustomerId = [CustomerId], 
					@AlarmPhone = [AlarmPhone], 
					@AlarmEmail = [AlarmEmail], 
					@GeoFenceId = [GeoFenceId], 
					@GeoFenceName = [GeoFenceName], 
					@Polygon = geography::STGeomFromText([Polygon].STAsText(), 4326), 
					@In = [In],
					@Out = [Out],
					@GeoFenceInsertDate = [GeoFenceInsertDate]
				FROM 
					@GeoFencesList 
				WHERE 
					[Id] = @RowNum

				IF @In = 1
					BEGIN
						INSERT INTO @InOutRecordsByVehicle
							([AlarmId],
							[TypeId],
							[CustomerId],
							[AlarmPhone],
							[AlarmEmail],
							[GeoFenceId],
							[GeoFenceName],
							[In],
							[Out],
							[GeofenceIn],
							[GeofenceOut],
							[VehicleId],
							[PlateId])
						SELECT 
							@AlarmId,
							@TypeId,
							@CustomerId,
							@AlarmPhone,
							@AlarmEmail,
							@GeoFenceId,
							@GeoFenceName,
							1,
							0,
							b.GeofenceIn,
							b.GeofenceOut,
							a.[VehicleId],
							b.[PlateId]
						FROM @ChangedRecords a
							INNER JOIN @VehicleByGeoFence b ON a.[VehicleId] = b.[VehicleId]
						WHERE 
							b.[GeoFenceId] = @GeoFenceId AND
							@Polygon.STIntersects(geography::Point([Latitude], [Longitude], 4326)) <> @Polygon.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326))AND 
							@Polygon.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326)) = 1
					END	

				IF @Out = 1
					BEGIN
						INSERT INTO @InOutRecordsByVehicle
							([AlarmId],
							[TypeId],
							[CustomerId],
							[AlarmPhone],
							[AlarmEmail],
							[GeoFenceId],
							[GeoFenceName],
							[In],
							[Out],
							[GeofenceIn],
							[GeofenceOut],
							[VehicleId],
							[PlateId])
						SELECT 
							@AlarmId,
							@TypeId,
							@CustomerId,
							@AlarmPhone,
							@AlarmEmail,
							@GeoFenceId,
							@GeoFenceName,
							0,
							1,
							b.GeofenceIn,
							b.GeofenceOut,
							a.[VehicleId],
							b.[PlateId]
						FROM @ChangedRecords a
							INNER JOIN @VehicleByGeoFence b ON a.[VehicleId] = b.[VehicleId]
						WHERE 
							b.[GeoFenceId] = @GeoFenceId AND
							@Polygon.STIntersects(geography::Point([Latitude], [Longitude], 4326)) <> @Polygon.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326))AND 
							@Polygon.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326)) = 0
					END
	
				SET @RowNum = @RowNum - 1
			END


		Select @RowNum = Count(*) From @InOutRecordsByVehicle
		WHILE @RowNum > 0
			BEGIN
				DECLARE @lAlarmId INT
				DECLARE @lAlarmTriggerId INT
				DECLARE @lCustomerId INT
				DECLARE @lPlate VARCHAR(10)
				DECLARE @lDescription VARCHAR(500)
				DECLARE @lTimeLastReport TIME
				DECLARE @lPhone VARCHAR(100)
				DECLARE @lEmail VARCHAR (500)
				DECLARE @lIn INT
				DECLARE @lOut INT
				DECLARE @lGeofenceIn INT
				DECLARE @lGeofenceOut INT
				DECLARE @lInOut VARCHAR(10)
			
				SELECT @lAlarmId = [AlarmId], 
					@lAlarmTriggerId = [TypeId],
					@lCustomerId =[CustomerId],
					@lPlate = [PlateId],
					@lDescription = [GeoFenceName],
					@lPhone = [AlarmPhone],
					@lEmail = [AlarmEmail],
					@lIn = [In],
					@lOut = [Out],
					@lGeofenceIn = [GeofenceIn],
					@lGeofenceOut = [GeofenceOut]
		
				FROM 
					@InOutRecordsByVehicle 
				WHERE 
					[Id] = @RowNum
			
				IF @lIn = 1 AND @lGeofenceIn = 1
				BEGIN
					SET @lInOut = ''Entrado''
					EXEC [General].[Sp_SendAlarm] 
						@pAlarmId = @lAlarmId,
						@pAlarmTriggerId = @lAlarmTriggerId,
						@pCustomerId = @lCustomerId,
						@pPlate = @lPlate,
						@pDescription = @lDescription,
						@pTimeLastReport = @lTimeLastReport,
						@pPhone = @lPhone,
						@pEmail = @lEmail,
						@pInOut = @lInOut
				END
				IF @lOut = 1 AND @lGeofenceOut = 1
				BEGIN
					SET @lInOut = ''Salido''
					EXEC [General].[Sp_SendAlarm] 
						@pAlarmId = @lAlarmId,
						@pAlarmTriggerId = @lAlarmTriggerId,
						@pCustomerId = @lCustomerId,
						@pPlate = @lPlate,
						@pDescription = @lDescription,
						@pTimeLastReport = @lTimeLastReport,
						@pPhone = @lPhone,
						@pEmail = @lEmail,
						@pInOut = @lInOut
				END
			SET @RowNum = @RowNum - 1
		END

	END TRY 
	BEGIN CATCH 
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH 		
END 

' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_GetScoreSpeedAllDriver]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetScoreSpeedAllDriver]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- Modified by:		Andrés Oviedo
-- Modified date:	16/02/2015 

CREATE PROCEDURE [Operation].[Sp_GetScoreSpeedAllDriver] 
( @pCustomerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pScoreType char(1),			--A- Reporte Administrativo  , R- Reporte de  carretera (Route)  
  @pInvocationType char(1)		--J- Invocación desde el Job, R-Invocación desde un reporte 
)
AS
BEGIN

	--Declare @pStartDate datetime =  ''20141019 03:45:02.000'',
	--@pEndDate datetime = ''20141128 11:30am''

	--Set Dates 
	If @pEndDate is null
		If @pStartDate is null
		begin
			If @pMonth is null or @pYear is null
			begin
				RAISERROR (''100'', 16, 1)
				RETURN
			end
			else
			begin
				If @pInvocationType = ''J''
				begin
					--Para la invocación de un mes completo desde el Job, ejecuta el cálculo
					Set @pStartDate = [General].[Fn_GetFirstDayOfMonth](@pYear, @pMonth)
					Set @pEndDate = DATEADD(MM, 1, @pStartDate)
				end
				else
				begin
					--Para la invocación por mes desde un reporte, recupera las calificaciones precalculadas
					Select 0 DriverScoreId, DS.UserId, U.Name as EncryptedName, D.Identification as EncryptedIdentification, DS.Score,
					DS.[KmTraveled] ,
					DS.[OverSpeed],
					DS.[OverSpeedAmount],
					DS.[OverSpeedDistancePercentage],
					DS.[OverSpeedAverage],
					DS.[OverSpeedWeihgtedAverage],
					DS.[StandardDeviation],
					DS.[Variance],
					DS.[VariationCoefficient],
					DS.OverSpeedDistance 				
					  from Operation.DriversScores DS
					  inner join General.Users U on DS.UserId = U.UserId
					  inner join General.DriversUsers D on DS.UserId = D.UserId
					 where DS.CustomerId = @pCustomerId
					   and DS.ScoreYear = @pYear
					   and DS.ScoreMonth = @pMonth
					   and DS.ScoreType = Case When @pScoreType = ''A'' then ''SPADM'' else ''SPROU'' end
					 order by DS.Score
					   
					RETURN
				end
			end
		end
		else
		begin
			Set @pEndDate = DATEADD(DD, 1, @pStartDate)
		end

	--DECLARE @MyTable  dbo.TypeDeviceSpeed
	Declare @VehiclesByUser table
	(Device int,
	 Vehicle int,
	 StartOdometer int,
	 EndOdometer int,
	 StartDate datetime,
	 EndDate datetime)

	Create table #LocalDriverScore
	(DriverScoreId INT Identity(1,1),
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 Score Float,
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float
	 )
	 
	Declare @UserId Int
	Declare @Name varchar(250),
			@Identification varchar(50)
	Declare  @Score Float

	Select @UserId = Min(U.UserId) 
	  from General.Users U
	  inner join General.DriversUsers D on U.UserId = D.UserId
	 Where U.IsActive =  1 
	   and D.CustomerId = @pCustomerId

	--Begin transaction
	 
	While  @UserId  is not null
	BEGIN
		Insert into @VehiclesByUser
			(Device, Vehicle, StartOdometer, EndOdometer, StartDate, EndDate)  
		--Select   V.IntrackReference As Device,CASE WHEN  VU.InitialOdometer IS NULL THEN 0 eLSE VU.InitialOdometer end  ,CASE WHEN VU.EndOdometer IS NULL THEN 0 eLSE EndOdometer end    , VU.InsertDate StartDate ,VU.LastDateDriving EndDate
		Select V.DeviceReference, V.IntrackReference, IsNull(VU.InitialOdometer, 0), IsNull(VU.EndOdometer,0), VU.InsertDate, IsNull(VU.LastDateDriving, GETDATE())
		from General.VehiclesByUser VU 
		inner Join General.Vehicles V ON  V.VehicleId = VU.VehicleId
		inner JOIN General.Users U  ON U.UserId = VU.UserId
		Where U.UserId =  @UserId 
		  and V.CustomerId = @pCustomerId
		  And V.DeviceReference is not null
		  and ((VU.InsertDate between @pStartDate and @pEndDate)
		   or  (IsNull(VU.LastDateDriving, GETDATE()) between @pStartDate and @pEndDate )
		   or  (VU.InsertDate <= @pStartDate and IsNull(VU.LastDateDriving, GETDATE()) >= @pEndDate))
		order by V.DeviceReference, VU.InsertDate, VU.LastDateDriving

		--SELECT * from @VehiclesByUser
		if (select COUNT(*) FROM @VehiclesByUser ) > 0 
		BEGIN 
			DECLARE @VehiclesByUserString varchar(1000);
			SET @VehiclesByUserString = (SElect * FROm  @VehiclesByUser AS Lista FOR XML AUTO)
			--SELECT  @VehiclesByUserString   
			declare @ScoreOutput nvarchar(max)
			
			EXECUTE  dbo.Sp_GetScoreDriver @pStartDate, @pEndDate, @VehiclesByUserString, @pScoreType, @pScore = @ScoreOutput OUTPUT
			DECLARE @pScoreOutputXML XML;
				SET  @pScoreOutputXML = CAST(@ScoreOutput  AS XML);
				
				DECLARE @ScoreTable dbo.TypeDriverScore;
				
				Insert into  @ScoreTable
				SELECT --DeviceSpeedId   = T.Item.value(''@DeviceSpeedId '', ''int''),
					   KmTraveled = T.Item.value(''@KmTraveled'', ''float''),
					   OverSpeed = T.Item.value(''@OverSpeed'', ''float''),
					   OverSpeedAmount  = T.Item.value(''@OverSpeedAmount'',  ''float''),
					   OverSpeedDistancePercentage = T.Item.value(''@OverSpeedDistancePercentage'', ''float''),
					   OverSpeedAverage = T.Item.value(''@OverSpeedAverage'', ''float''),
					   OverSpeedWeihgtedAverage = T.Item.value(''@OverSpeedWeihgtedAverage'', ''float''),
					   StandardDeviation = T.Item.value(''@StandardDeviation'', ''float''),
					   Variance = T.Item.value(''@Variance'', ''float''),
					   VariationCoefficient = T.Item.value(''@VariationCoefficient'', ''float''),
					   AverageScore = T.Item.value(''@AverageScore'', ''float''),
					   OverSpeedDistance= T.Item.value(''@OverSpeedDistance'', ''float'')
				FROM   @pScoreOutputXML.nodes(''Lista'') AS T(Item)
				
				set @Score=(select AverageScore from @ScoreTable)
				
			
			--Select @Score
			If @Score is not null
			begin
				--Obtener nombre e identificación del usuario
				Select @Name = U.Name,
					   @Identification = D.Identification 
				  from General.Users U
				  inner join General.DriversUsers D on U.UserId = D.UserId
				where U.UserId = @UserId
				 
				Insert into #LocalDriverScore (UserID, Name, Identification, Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
						Values(@UserId, @Name, @Identification, @Score,(select KmTraveled from @ScoreTable),
						(select OverSpeed from @ScoreTable),(select OverSpeedAmount from @ScoreTable),(select OverSpeedDistancePercentage from @ScoreTable),
						(select OverSpeedAverage from @ScoreTable),(select OverSpeedWeihgtedAverage from @ScoreTable),(select StandardDeviation from @ScoreTable),
						(select Variance from @ScoreTable),(select VariationCoefficient from @ScoreTable),(select OverSpeedDistance from @ScoreTable))
				
				--Select * FROM #LocalDriverScore
				----Limpia la  tabla para  la nueva seleccion.
			end
			
			delete from @ScoreTable
			DELETE FROM @VehiclesByUser
		END
		--- Siguiente********************
		Select @UserId = Min(U.UserId) 
		  from General.Users U
		  inner join General.DriversUsers D on U.UserId = D.UserId
		 Where U.IsActive =  1 
		   and D.CustomerId = @pCustomerId
		   and U.UserId > @UserId
	END
	
	--Identification
	Select DriverScoreId , UserId , Name   , Identification, Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,OverSpeedDistance  FROM #LocalDriverScore
	order by Score asc
	
	Drop table #LocalDriverScore
END
' 
END
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetScoreDrivingFleetEvolution]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetScoreDrivingFleetEvolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo
-- Create date: 12/02/2015
-- Description:	Retrieve Score Driving Fleet Evolution information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_GetScoreDrivingFleetEvolution] 
( @pPartnerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)=null,		--Posibles valores S:Summarized, D:Detailed
  @pMonthsQuantity int,
  @pCustomerId int=Null
)
AS BEGIN

DECLARE @tableCustomers TABLE (CustomerId int, Name varchar(250)) 
DECLARE  @li_CustomerId INT
DECLARE @NameCustomer varchar(250)
DECLARE @tableDriversScore TABLE(
DriverScoreId int
,UserId	int
,Name  varchar (250)
,Identification  varchar(50)	
,ScoreType	 varchar(5)
,Score Float
,Photo varchar(max)
,ScoreSpeedAdmin Float
,ScoreSpeedRoute Float
)

DECLARE @ScoreDrivingFleet TABLE(
CustomerId int
,EncryptedName	varchar (1000)
,AverageScore  FLOAT
,[Month] INT
,[Year] INT

)
	Declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT(''0'' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT(''0'' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end
		
DECLARE @Dates TABLE(
	Id INT
	,d_month int
	,d_year	int
)

SET @pMonthsQuantity=@pMonthsQuantity-1

while @pMonthsQuantity>0
begin
	declare @pM INT =MONTH(DATEADD(mm,-@pMonthsQuantity,@pInitialDate))
	declare @pY INT =YEAR(DATEADD(mm,-@pMonthsQuantity,@pInitialDate))
	
	insert @Dates values(@pMonthsQuantity+1,@pM,@pY)
	set @pMonthsQuantity=@pMonthsQuantity-1
	
end

insert into @Dates values(1,MONTH(@pInitialDate),YEAR(@pFinalDate))

INSERT INTO @tableCustomers
EXEC  [Insurance].[Sp_GetRosterInsurance]
		@pPartnerId,@pCustomerId
---SELECT * from @tableCustomers 

DECLARE @count INT=(select count(*) from @Dates)

WHILE @count>0
begin

Select @li_CustomerId = MIN(CustomerId)
from @tableCustomers

WHILE @li_CustomerId is not null
begin

declare @MONTH INT=(select d_month from @Dates where id=@count)
declare @YEAR INT=(select d_year from @Dates where id=@count)


INSERT into   @tableDriversScore  (DriverScoreId,UserId,Name,Identification,ScoreType,Score,Photo ,ScoreSpeedAdmin,ScoreSpeedRoute)
EXEC	 [Operation].[Sp_GetGlobalScoreAllDriver]
		@li_CustomerId ,
		@YEAR ,
		@MONTH ,
		NULL ,
		NULL ,
		@pReportType

Select @NameCustomer = Name FROM  @tableCustomers where CustomerId=@li_CustomerId

insert into @ScoreDrivingFleet SELECT @li_CustomerId CustomerId, @NameCustomer  as EncryptedName ,CASE  WHEN AVG(Score) IS NULL THEN 0 ELSE AVG(Score)  END AS  AverageScore,@MONTH,@YEAR
FROM  @tableDriversScore 
Delete  FROM  @tableDriversScore 

-----------------------------
--Siguiente Customer
		Select @li_CustomerId = MIN(CustomerId)
		  from @tableCustomers 
		 where CustomerId > @li_CustomerId
END  -- End While
set @count=@count-1
end

select * from @ScoreDrivingFleet group by CustomerId,EncryptedName,AverageScore,[Month],[Year]
END
' 
END
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetScoreDrivingFleet]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetScoreDrivingFleet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo
-- Create date: 09/02/2015
-- Description:	Retrieve Score Driving Fleet] information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_GetScoreDrivingFleet] 
( @pPartnerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)=null,	--Posibles valores S:Summarized, D:Detailed
  @pCustomerId int=Null	
)
AS BEGIN

DECLARE @tableCustomers TABLE (CustomerId int, Name varchar(250)) 
DECLARE  @li_CustomerId INT
DECLARE @NameCustomer varchar(250)
DECLARE @tableDriversScore TABLE(
DriverScoreId int
,UserId	int
,Name  varchar (250)
,Identification  varchar(50)	
,ScoreType	 varchar(5)
,Score Float
,Photo varchar(max)
,ScoreSpeedAdmin Float
,ScoreSpeedRoute Float
)

DECLARE @ScoreDrivingFleet TABLE(
CustomerId int
,EncryptedName	varchar (1000)
,AverageScore  FLOAT
)

INSERT INTO @tableCustomers
EXEC  [Insurance].[Sp_GetRosterInsurance]
		@pPartnerId,@pCustomerId
---SELECT * from @tableCustomers 

Select @li_CustomerId = MIN(CustomerId)
from @tableCustomers

WHILE @li_CustomerId is not null
BEGIN

INSERT into   @tableDriversScore  (DriverScoreId,UserId,Name,Identification,ScoreType,Score,Photo ,ScoreSpeedAdmin,ScoreSpeedRoute)
EXEC	 [Operation].[Sp_GetGlobalScoreAllDriver]
		@li_CustomerId ,
		@pYear ,
		@pMonth ,
		@pStartDate ,
		@pEndDate ,
		@pReportType

Select @NameCustomer = Name FROM  @tableCustomers where CustomerId=@li_CustomerId

insert into @ScoreDrivingFleet SELECT @li_CustomerId CustomerId, @NameCustomer  as EncryptedName ,CASE  WHEN AVG(Score) IS NULL THEN 0 ELSE AVG(Score)  END AS  AverageScore
FROM  @tableDriversScore 
Delete  FROM  @tableDriversScore 

-----------------------------
--Siguiente Customer
		Select @li_CustomerId = MIN(CustomerId)
		  from @tableCustomers 
		 where CustomerId > @li_CustomerId
END  -- End While
select * from @ScoreDrivingFleet
END
' 
END
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetLowScoreDrivingFleetEvolution]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetLowScoreDrivingFleetEvolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo
-- Create date: 13/02/2015
-- Description:	Retrieve Low Score Driving Fleet Evolution information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_GetLowScoreDrivingFleetEvolution]
( @pPartnerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)=null,		--Posibles valores S:Summarized, D:Detailed
  @pMonthsQuantity int,
  @pLowScore int,
  @pCustomerId int=Null
)
AS BEGIN

DECLARE @tableCustomers TABLE (CustomerId int, Name varchar(250)) 
DECLARE  @li_CustomerId INT
DECLARE @NameCustomer varchar(250)
DECLARE @tableDriversScore TABLE(
DriverScoreId int
,UserId	int
,Name  varchar (250)
,Identification  varchar(50)	
,ScoreType	 varchar(5)
,Score Float
,Photo varchar(max)
,ScoreSpeedAdmin Float
,ScoreSpeedRoute Float
)
declare @pMQuantity int =@pMonthsQuantity

DECLARE @ScoreDrivingFleet TABLE(
CustomerId int
,EncryptedName	varchar (1000)
,AverageScore  FLOAT
,[Month] INT
,[Year] INT

)
	Declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT(''0'' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT(''0'' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end
		
DECLARE @Dates TABLE(
	Id INT
	,d_month int
	,d_year	int
)

SET @pMonthsQuantity=@pMonthsQuantity-1

while @pMonthsQuantity>0
begin
	declare @pM INT =MONTH(DATEADD(mm,-@pMonthsQuantity,@pInitialDate))
	declare @pY INT =YEAR(DATEADD(mm,-@pMonthsQuantity,@pInitialDate))
	
	insert @Dates values(@pMonthsQuantity+1,@pM,@pY)
	set @pMonthsQuantity=@pMonthsQuantity-1
	
end

insert into @Dates values(1,MONTH(@pInitialDate),YEAR(@pFinalDate))

INSERT INTO @tableCustomers
EXEC  [Insurance].[Sp_GetRosterInsurance]
		@pPartnerId,@pCustomerId
---SELECT * from @tableCustomers 

DECLARE @count INT=(select count(*) from @Dates)

WHILE @count>0
begin

Select @li_CustomerId = MIN(CustomerId)
from @tableCustomers

WHILE @li_CustomerId is not null
begin

declare @MONTH INT=(select d_month from @Dates where id=@count)
declare @YEAR INT=(select d_year from @Dates where id=@count)


INSERT into   @tableDriversScore  (DriverScoreId,UserId,Name,Identification,ScoreType,Score,Photo ,ScoreSpeedAdmin,ScoreSpeedRoute)
EXEC	 [Operation].[Sp_GetGlobalScoreAllDriver]
		@li_CustomerId ,
		@YEAR ,
		@MONTH ,
		NULL ,
		NULL ,
		@pReportType

Select @NameCustomer = Name FROM  @tableCustomers where CustomerId=@li_CustomerId

insert into @ScoreDrivingFleet SELECT @li_CustomerId CustomerId, @NameCustomer  as EncryptedName ,CASE  WHEN AVG(Score) IS NULL THEN 0 ELSE AVG(Score)  END AS  AverageScore,@MONTH,@YEAR
FROM  @tableDriversScore 
Delete  FROM  @tableDriversScore 

-----------------------------
--Siguiente Customer
		Select @li_CustomerId = MIN(CustomerId)
		  from @tableCustomers 
		 where CustomerId > @li_CustomerId
END  -- End While
set @count=@count-1
end

select * from @ScoreDrivingFleet as sdf where (select top 1 (case when sum(AverageScore)=0 then @pMQuantity else sum(AverageScore) end) /@pMQuantity from @ScoreDrivingFleet s where sdf.CustomerId=s.CustomerId)<=@pLowScore group by CustomerId,EncryptedName,AverageScore,[Month],[Year] order by [YEAR],[Month]
END
' 
END
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetDrivingScoreFleetDataSourceReport]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetDrivingScoreFleetDataSourceReport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo
-- Create date: 18/02/2015
-- Description:	Retrieve Score Driving Fleet Data Source information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_GetDrivingScoreFleetDataSourceReport] 
( @pPartnerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)=null,		--Posibles valores S:Summarized, D:Detailed
  @pCustomerId int=null
)
AS BEGIN

DECLARE @tableCustomers TABLE (CustomerId int, Name varchar(250)) 
DECLARE  @li_CustomerId INT
DECLARE @NameCustomer varchar(250)
DECLARE @tableDriversScore TABLE(
CustomerId int, CustomerName varchar(250),
DriverScoreId int
,UserId	int
,Name  varchar (250)
,Identification  varchar(50)	
,ScoreType	 varchar(5)
,Score Float
,Photo varchar(max)
,ScoreSpeedAdmin Float
,ScoreSpeedRoute Float
,[KmTraveled] float
,[OverSpeed] float
,[OverSpeedAmount] float
,[OverSpeedDistancePercentage] float
,[OverSpeedAverage] float
,[OverSpeedWeihgtedAverage] float
,[StandardDeviation] float
,[Variance] float
,[VariationCoefficient] float,
 OverSpeedDistance float
)

	Declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT(''0'' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT(''0'' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end
		
DECLARE @Dates TABLE(
	Id INT
	,d_month int
	,d_year	int
)


insert @Dates values(1,MONTH(@pInitialDate),YEAR(@pFinalDate))

INSERT INTO @tableCustomers
EXEC  [Insurance].[Sp_GetRosterInsurance]
		@pPartnerId,@pCustomerId
---SELECT * from @tableCustomers 

DECLARE @count INT=(select count(*) from @Dates)

WHILE @count>0
begin

Select @li_CustomerId = MIN(CustomerId)
from @tableCustomers

WHILE @li_CustomerId is not null
begin

declare @MONTH INT=(select d_month from @Dates where id=@count)
declare @YEAR INT=(select d_year from @Dates where id=@count)


INSERT   @tableDriversScore  (CustomerId,CustomerName,DriverScoreId,UserId,Name,Identification,ScoreType,Score,Photo ,ScoreSpeedAdmin,ScoreSpeedRoute,
 [KmTraveled] 
,[OverSpeed] 
,[OverSpeedAmount] 
,[OverSpeedDistancePercentage] 
,[OverSpeedAverage] 
,[OverSpeedWeihgtedAverage] 
,[StandardDeviation] 
,[Variance] 
,[VariationCoefficient], OverSpeedDistance )
EXEC [Operation].[Sp_GetGlobalScoreDataSourceAllDriver] 
		@li_CustomerId ,
		@pYear,			
		@pMonth,
		@pStartDate,
		@pEndDate,	
		@pReportType

-----------------------------
--Siguiente Customer
		Select @li_CustomerId = MIN(CustomerId)
		  from @tableCustomers 
		 where CustomerId > @li_CustomerId
END  -- End While
set @count=@count-1
end

select CustomerId,CustomerName as EncryptedCustomerName,DriverScoreId 
,UserId
,Name  as EncryptedName
,Identification  as EncryptedIdentification	
,ScoreType	 
,Score as AverageScore 
,[KmTraveled] 
,[OverSpeed] 
,[OverSpeedAmount] 
,[OverSpeedDistancePercentage] 
,[OverSpeedAverage] 
,[OverSpeedWeihgtedAverage] 
,[StandardDeviation] 
,[Variance] 
,[VariationCoefficient] ,
 OverSpeedDistance  from @tableDriversScore 
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_EfficiencyFuelsReportByVehicleGroup_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_EfficiencyFuelsReportByVehicleGroup_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Efficiency Fuels Report By Vehicle Group information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_EfficiencyFuelsReportByVehicleGroup_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lTable TABLE
	(
		 [VehicleId] INT NOT NULL
		,[PlateNumber] VARCHAR(10) NOT NULL
		,[Year] INT NOT NULL
		,[Month] INT NOT NULL
		,[Liters] DECIMAL(16,2) NOT NULL
	)
	INSERT @lTable  EXEC [Control].[Sp_EfficiencyFuelsReportByVehicle_Retrieve] @pCustomerId = @pCustomerId
																				,@pYear = @pYear
																				,@pMonth = @pMonth
																				,@pStartDate = @pStartDate
																				,@pEndDate = @pEndDate
	
	SELECT
		 s.[VehicleGroupName]
		,t.[Month]
		,t.[Year]
		,SUM([Liters]) AS [Liters]
	FROM @lTable t
		INNER JOIN
			(SELECT
				 b.[VehicleId]
				,a.[Name] AS [VehicleGroupName]
			FROM  [General].[VehicleGroups] a
				INNER JOIN [General].[VehiclesByGroup] b
					ON b.[VehicleGroupId] = a.[VehicleGroupId]
			WHERE a.[CustomerId] = @pCustomerId) s
		ON t.[VehicleId] = s.[VehicleId]
	GROUP BY s.[VehicleGroupName], t.[Month], t.[Year]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_EfficiencyFuelsReportBySubUnit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_EfficiencyFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Efficiency Fuels Report By Sub Unit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_EfficiencyFuelsReportBySubUnit_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	
	DECLARE @lTable TABLE
	(
		 [VehicleId] INT NOT NULL
		,[PlateNumber] VARCHAR(10) NOT NULL
		,[Year] INT NOT NULL
		,[Month] INT NOT NULL
		,[Liters] DECIMAL(16,2) NOT NULL
	)
	INSERT @lTable  EXEC [Control].[Sp_EfficiencyFuelsReportByVehicle_Retrieve] @pCustomerId = @pCustomerId
																				,@pYear = @pYear
																				,@pMonth = @pMonth
																				,@pStartDate = @pStartDate
																				,@pEndDate = @pEndDate
	
	SELECT
		 s.[CostCenterName]
		,t.[Month]
		,t.[Year]
		,SUM([Liters]) AS [Liters]
	FROM @lTable t
		INNER JOIN
			(SELECT
				 a.[VehicleId]
				,b.[Name] AS [CostCenterName]
			FROM [General].[Vehicles] a
				INNER JOIN [General].[VehicleCostCenters] b
					ON a.[CostCenterId] = b.[CostCenterId]
				INNER JOIN [General].[VehicleUnits] c
					ON b.[UnitId] = c.[UnitId]
			WHERE c.[CustomerId] = @pCustomerId) s
		ON t.[VehicleId] = s.[VehicleId]
	GROUP BY s.[CostCenterName], t.[Month], t.[Year]
	
	SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_AccumulatedFuelsReportByVehicleGroup_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AccumulatedFuelsReportByVehicleGroup_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Vehicle Group information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_AccumulatedFuelsReportByVehicleGroup_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lTable TABLE
	(
		 [VehicleId] INT NOT NULL
		,[PlateNumber] VARCHAR(10) NOT NULL
		,[Year] INT NOT NULL
		,[Month] INT NOT NULL
		,[Liters] DECIMAL(16,2) NOT NULL
	)
	INSERT @lTable  EXEC [Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve] @pCustomerId = @pCustomerId
																				,@pYear = @pYear
																				,@pMonth = @pMonth
																				,@pStartDate = @pStartDate
																				,@pEndDate = @pEndDate
	
	SELECT
		 s.[VehicleGroupName]
		,t.[Month]
		,t.[Year]
		,SUM([Liters]) AS [Liters]
	FROM @lTable t
		INNER JOIN
			(SELECT
				 b.[VehicleId]
				,a.[Name] AS [VehicleGroupName]
			FROM  [General].[VehicleGroups] a
				INNER JOIN [General].[VehiclesByGroup] b
					ON b.[VehicleGroupId] = a.[VehicleGroupId]
			WHERE a.[CustomerId] = @pCustomerId) s
		ON t.[VehicleId] = s.[VehicleId]
	GROUP BY s.[VehicleGroupName], t.[Month], t.[Year]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_AccumulatedFuelsReportBySubUnit_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AccumulatedFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Sub Unit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_AccumulatedFuelsReportBySubUnit_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	
	DECLARE @lTable TABLE
	(
		 [VehicleId] INT NOT NULL
		,[PlateNumber] VARCHAR(10) NOT NULL
		,[Year] INT NOT NULL
		,[Month] INT NOT NULL
		,[Liters] DECIMAL(16,2) NOT NULL
	)
	INSERT @lTable  EXEC [Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve] @pCustomerId = @pCustomerId
																				,@pYear = @pYear
																				,@pMonth = @pMonth
																				,@pStartDate = @pStartDate
																				,@pEndDate = @pEndDate
	
	SELECT
		 s.[CostCenterName]
		,t.[Month]
		,t.[Year]
		,SUM([Liters]) AS [Liters]
	FROM @lTable t
		INNER JOIN
			(SELECT
				 a.[VehicleId]
				,b.[Name] AS [CostCenterName]
			FROM [General].[Vehicles] a
				INNER JOIN [General].[VehicleCostCenters] b
					ON a.[CostCenterId] = b.[CostCenterId]
				INNER JOIN [General].[VehicleUnits] c
					ON b.[UnitId] = c.[UnitId]
			WHERE c.[CustomerId] = @pCustomerId) s
		ON t.[VehicleId] = s.[VehicleId]
	GROUP BY s.[CostCenterName], t.[Month], t.[Year]
	
	SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_CostCentersByCredit_AddOrEdit]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CostCentersByCredit_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update Vehicle Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CostCentersByCredit_AddOrEdit]
(
	 @pVehicleFuelId INT = NULL				--@pVehicleFuelId: PK of the table
	,@pVehicleId INT						--@pVehicleId: FK of Vehicles
	,@pLiters DECIMAL(16,10)				--@pLiters: Liters of Fuel
	,@pAmount DECIMAL(16,6)					--@pAmount: Amount of Fuel
	,@pMonth INT							--@pMonth: Month
	,@pYear INT								--@pYear: Year
	,@pCustomerId INT						--@pCustomerId: Customer Id
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	EXEC [Control].[Sp_VehicleFuel_AddOrEdit]	 @pVehicleFuelId
												,@pVehicleId
												,@pLiters
												,@pAmount
												,@pMonth
												,@pYear
												,@pCustomerId
												,@pLoggedUserId
												,@pRowVersion
												,0 -- @pThrowError
	
END
' 
END
GO

/****** Object:  StoredProcedure [Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo González
-- Create date: 19/01/2015
-- Description:	Retrieve Preventive Maintenance Record Header By Vehicle Detail
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]
(
	 @pId INT
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @Id INT
	DECLARE @CatalogId INT
	
	SELECT @Id = PreventiveMaintenanceRecordByVehicleId FROM General.PreventiveMaintenanceRecordByVehicle WHERE PreventiveMaintenanceId = @pId

	IF @id IS NULL
		BEGIN

			SELECT 
				0 [PreventiveMaintenanceRecordByVehicleId],
				[PreventiveMaintenanceId] [PreventiveMaintenanceId],
				GETUTCDATE() [Date],
				CAST(0.00 AS DECIMAL(16, 2)) [Record]
			FROM [General].[PreventiveMaintenanceByVehicle] 
			WHERE 
				[PreventiveMaintenanceId] = @pId
        
		END
	ELSE
		BEGIN
		
			SELECT 
				[PreventiveMaintenanceRecordByVehicleId],
				[PreventiveMaintenanceId],
				[Date],
				[Record]
			FROM [General].[PreventiveMaintenanceRecordByVehicle]
			WHERE 
				[PreventiveMaintenanceId] = @pId
	
		END

    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ==========================================================
-- Author:		Danilo Hidalgo.
-- Create date: 08/01/2015
-- Description:	Preventive Maintenance Record By Vehicle Detail information
-- ===========================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]
	 @pPreventiveMaintenanceId INT
AS
BEGIN 
	SET NOCOUNT ON;
	
		If Exists (SELECT 1 FROM [General].[PreventiveMaintenanceRecordByVehicle] WHERE [PreventiveMaintenanceId] = @pPreventiveMaintenanceId)
		Begin
			SELECT 
				a.[PreventiveMaintenanceRecordByVehicleId],
				b.[PreventiveMaintenanceRecordByVehicleDetailId],
				a.[PreventiveMaintenanceId], 
				b.[PreventiveMaintenanceCostId], 
				b.[Description], 
				b.[Cost], 
				b.[Record] 
			FROM [General].[PreventiveMaintenanceRecordByVehicle] a
				INNER JOIN [General].[PreventiveMaintenanceRecordByVehicleDetail] b ON a.[PreventiveMaintenanceRecordByVehicleId] = b.[PreventiveMaintenanceRecordByVehicleId]
			WHERE a.[PreventiveMaintenanceId] = @pPreventiveMaintenanceId
		End
		Else
		Begin
			SELECT 
				0 as [PreventiveMaintenanceRecordByVehicleId],
				0 as [PreventiveMaintenanceRecordByVehicleDetailId],
				a.[PreventiveMaintenanceId], 
				b.[PreventiveMaintenanceCostId], 
				b.[Description], 
				b.[Cost], 
				0 as [Record]
			FROM [General].[PreventiveMaintenanceByVehicle] a 
				INNER JOIN [General].[PreventiveMaintenanceCost] b ON a.[PreventiveMaintenanceCatalogId] = b.[PreventiveMaintenanceCatalogId]
			WHERE a.[PreventiveMaintenanceId] = @pPreventiveMaintenanceId
			ORDER BY b.Description
		End
	
	SET NOCOUNT OFF;
END

' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Danilo Hidalgo González
-- Create date: 14/01/2015
-- Description:	Retrieve Preventive Maintenance Record By Vehicle Detail
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]
(
	 @pId INT
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @Id INT
	DECLARE @CatalogId INT
	
	SELECT @Id = PreventiveMaintenanceRecordByVehicleId FROM General.PreventiveMaintenanceRecordByVehicle WHERE PreventiveMaintenanceId = @pId

	IF @id IS NULL
		BEGIN

		SELECT @CatalogId = PreventiveMaintenanceCatalogId FROM General.PreventiveMaintenanceByVehicle WHERE PreventiveMaintenanceId = @pId

			SELECT 
				0 [PreventiveMaintenanceRecordByVehicleDetailId],
				0 [PreventiveMaintenanceRecordByVehicleId],
				[PreventiveMaintenanceCostId], 
				[Description], 
				[Cost],
				CAST(0.00 AS DECIMAL(16, 2)) [Record]
			FROM 
				[General].[PreventiveMaintenanceCost] 
			WHERE 
				[PreventiveMaintenanceCatalogId] = @CatalogId
			ORDER BY 
				Description
				
		END
	ELSE
		BEGIN
		
			SELECT 
				[PreventiveMaintenanceRecordByVehicleDetailId],
				[PreventiveMaintenanceRecordByVehicleId],
				[PreventiveMaintenanceCostId],
				[Description],
				[Cost],
				[Record]
			FROM 
				[General].[PreventiveMaintenanceRecordByVehicleDetail]
			WHERE 
				[PreventiveMaintenanceRecordByVehicleId] = @Id
			ORDER BY
				Description
	
		END


    SET NOCOUNT OFF
END

' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Danilo Hidalgo
-- Create date: 16/01/2015
-- Description:	Delete Preventive Maintenance Record By Vehicle Detail
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]
	@pId INT, 
	@pLoggedUserId INT
AS
BEGIN	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
		DECLARE @lPreventiveMaintenanceRecordByVehicleId INT
		DECLARE @lTotalCost DECIMAL(16, 2)
		DECLARE @lTotalRecord DECIMAL(16, 2)

        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		SELECT @lPreventiveMaintenanceRecordByVehicleId = [PreventiveMaintenanceRecordByVehicleId] 
		FROM [General].[PreventiveMaintenanceRecordByVehicleDetail]
		WHERE [PreventiveMaintenanceRecordByVehicleDetailId] = @pId
		
		DELETE [General].[PreventiveMaintenanceRecordByVehicleDetail]
		WHERE [PreventiveMaintenanceRecordByVehicleDetailId] = @pId
		
		
			SELECT @lTotalCost = ISNULL(SUM(Cost), 0) FROM [General].[PreventiveMaintenanceRecordByVehicleDetail] 
			WHERE [PreventiveMaintenanceRecordByVehicleId] = @lPreventiveMaintenanceRecordByVehicleId
			
			SELECT @lTotalRecord = ISNULL(SUM(Record), 0) FROM [General].[PreventiveMaintenanceRecordByVehicleDetail] 
			WHERE [PreventiveMaintenanceRecordByVehicleId] = @lPreventiveMaintenanceRecordByVehicleId
			
			UPDATE [General].[PreventiveMaintenanceRecordByVehicle]
			SET 
				[Cost] = @lTotalCost, 
				[Record] = @lTotalRecord, 
				[ModifyUserId] = @pLoggedUserId,
				[ModifyDateId] = GETUTCDATE()
			WHERE 
				[PreventiveMaintenanceRecordByVehicleId] = @lPreventiveMaintenanceRecordByVehicleId


		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/16/2015
-- Description:	Insert or Update Preventive Maintenance Record By Vehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]
(
	@pXmlData VARCHAR(MAX),	
	@pPreventiveMaintenanceRecordByVehicleId INT,
	@pPreventiveMaintenanceId INT,
	@pLoggedUserId INT						
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0

            DECLARE @lTotalCost DECIMAL(16, 2)
            DECLARE @lTotalRecord DECIMAL(16, 2)
            DECLARE @lCatalogId INT
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pPreventiveMaintenanceRecordByVehicleId = 0)
			BEGIN
				INSERT INTO [General].[PreventiveMaintenanceRecordByVehicle]
					([PreventiveMaintenanceId],
					[Date],
					[Record],
					[Cost],
					[InsertUserId],
					[InsertDateId])
				VALUES
					(@pPreventiveMaintenanceId,
					GETUTCDATE(),
					0,
					0,
					@pLoggedUserId,
					GETUTCDATE())
			
				SELECT @pPreventiveMaintenanceRecordByVehicleId = PreventiveMaintenanceRecordByVehicleId 
				FROM [General].[PreventiveMaintenanceRecordByVehicle]
				WHERE [PreventiveMaintenanceId] = @pPreventiveMaintenanceId
					
			END
			
			INSERT INTO [General].[PreventiveMaintenanceRecordByVehicleDetail]
					([PreventiveMaintenanceRecordByVehicleId]
					,[PreventiveMaintenanceCostId]
					,[Description]
					,[Cost]
					,[Record]
					,[InsertUserId]
					,[InsertDateId])
			SELECT 
					@pPreventiveMaintenanceRecordByVehicleId AS [PreventiveMaintenanceCatalogId]
					,Row.col.value(''./@PreventiveMaintenanceCostId'', ''INT'') AS [PreventiveMaintenanceCatalogId]
					,Row.col.value(''./@Description'', ''VARCHAR(500)'') AS [Description]
					,Row.col.value(''./@Cost'', ''DECIMAL(16,2)'') AS [Cost]
					,Row.col.value(''./@Record'', ''DECIMAL(16,2)'') AS [Cost]
					,@pLoggedUserId
					,GETUTCDATE()
			FROM @lxmlData.nodes(''/xmldata/PreventiveMaintenanceRecordByVehicleCostDetail'') Row(col)
			WHERE Row.col.value(''./@PreventiveMaintenanceRecordByVehicleDetailId'', ''INT'') = 0
			
			UPDATE record SET 
					record.Description = Row.col.value(''./@Description'', ''VARCHAR(500)''),
					record.Cost = Row.col.value(''./@Cost'', ''DECIMAL(16,2)''),
					record.Record = Row.col.value(''./@Record'', ''DECIMAL(16,2)''),
					record.ModifyUserId = @pLoggedUserId,
					record.ModifyDateId = GETUTCDATE()
			FROM @lxmlData.nodes(''/xmldata/PreventiveMaintenanceRecordByVehicleCostDetail'') Row(col)
			INNER JOIN [General].[PreventiveMaintenanceRecordByVehicleDetail] record ON Row.col.value(''./@PreventiveMaintenanceRecordByVehicleDetailId'', ''INT'') = record.PreventiveMaintenanceRecordByVehicleDetailId
			WHERE Row.col.value(''./@PreventiveMaintenanceRecordByVehicleDetailId'', ''INT'') <> 0

			SELECT @lTotalCost = ISNULL(SUM(Cost), 0) FROM [General].[PreventiveMaintenanceRecordByVehicleDetail] 
			WHERE [PreventiveMaintenanceRecordByVehicleId] = @pPreventiveMaintenanceRecordByVehicleId
			
			SELECT @lTotalRecord = ISNULL(SUM(Record), 0) FROM [General].[PreventiveMaintenanceRecordByVehicleDetail] 
			WHERE [PreventiveMaintenanceRecordByVehicleId] = @pPreventiveMaintenanceRecordByVehicleId
			
			UPDATE [General].[PreventiveMaintenanceRecordByVehicle]
			SET 
				[Cost] = @lTotalCost, 
				[Record] = @lTotalRecord, 
				[ModifyUserId] = @pLoggedUserId,
				[ModifyDateId] = GETUTCDATE()
			WHERE 
				[PreventiveMaintenanceId] = @pPreventiveMaintenanceId

            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [General].[Sp_PerformanceByVehicle_Add]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PerformanceByVehicle_Add]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ================================================================================================
-- Author:		Berman Romero
-- Create date: 01/18/2015
-- Description:	Add Performance By Vehicle -- Massive process --
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PerformanceByVehicle_Add]

AS 
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
	BEGIN TRY
    
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
    
		IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		DECLARE @lTable TABLE
		(	 [Index] INT IDENTITY(1,1) NOT NULL
			,[TransactionId] INT NULL
			,[VehicleId] INT NULL
			,[TrxDate] DATETIME NULL
			,[TrxLiters] DECIMAL(16,2) NULL
			,[TrxOdometer] INT NULL
			,[GpsOdometer] INT NULL
			,[PreviousTrxOdometer] INT NULL
			,[PreviousGpsOdometer] INT NULL
		)
		
		INSERT INTO @lTable(
			 [TransactionId]
			,[VehicleId]
			,[TrxDate]
			,[TrxLiters]
			,[TrxOdometer]
		)    
		SELECT
			 a.[TransactionId]
			,a.[VehicleId]
			,a.[Date]
			,COALESCE(a.FixedOdometer, a.Odometer, 0)
			,a.[Liters]
		FROM  [Control].[Transactions] a WITH(READUNCOMMITTED)
		WHERE (a.[IsAdjustment] = 0 OR a.[IsAdjustment] IS NULL)
		  AND a.[IsFloating] = 0
		  AND a.[IsReversed] = 0
		  AND a.[IsDuplicated] = 0
		  AND NOT EXISTS(SELECT 1
							FROM [General].[PerformanceByVehicle] x
							WHERE x.TransactionId = a.TransactionId)
		ORDER BY a.[VehicleId], a.[TransactionId]
		
		/*Update first [PreviousTrxOdometer] to -1*/
		UPDATE @lTable
			SET [PreviousTrxOdometer] = -1	
		FROM(SELECT t.[VehicleId], MIN(t.[Index]) AS [MinIndex]
				FROM @lTable t
			GROUP BY t.[VehicleId])t
		WHERE [Index] = t.[MinIndex]

		DECLARE @lVehicleId INT
		DECLARE @lIndex INT = 1, @lMaxIndex INT
		SELECT @lMaxIndex = COUNT(1) FROM @lTable t
		
		/*Update [PreviousTrxOdometer] from Previous Row*/
		WHILE(@lIndex <= @lMaxIndex)
		BEGIN	
			SELECT @lVehicleId = t.[VehicleId] FROM @lTable t WHERE [Index] = @lIndex 
							
			UPDATE @lTable
				SET [PreviousTrxOdometer] = ISNULL((SELECT t.[TrxOdometer] FROM @lTable t
				INNER JOIN
					(SELECT t.[VehicleId], MAX(t.[Index]) AS [Index]
						FROM @lTable t
					WHERE t.[PreviousTrxOdometer] IS NOT NULL
					  AND t.[VehicleId] = @lVehicleId
					GROUP BY t.[VehicleId])u
				ON t.[Index] = u.[Index]),0)
			WHERE [Index] = @lIndex
			  AND [PreviousTrxOdometer] IS NULL
			
			SET @lIndex = @lIndex + 1
		END	
		
		/*Update from last Odometer*/
		UPDATE @lTable
			SET [PreviousTrxOdometer] = ISNULL((SELECT TOP 1
				COALESCE(a.FixedOdometer, a.Odometer, 0) AS [TrxOdometer]
			FROM  [Control].[Transactions] a WITH(READUNCOMMITTED)
			INNER JOIN  (SELECT
							x.[VehicleId], MAX(TransactionId) AS [TransactionId]
						FROM [General].[PerformanceByVehicle] x
						GROUP BY x.[VehicleId])t
			ON a.TransactionId = t.TransactionId),0)
		WHERE [PreviousTrxOdometer] = -1
		
		/*INSERT [General].[PerformanceByVehicle] From @lTable*/
		INSERT [General].[PerformanceByVehicle](
			 [VehicleId]
			,[TransactionId]
			,[TrxDate]
			,[PerformanceByOdometer]
			,[PerformanceByGPS]
			,[InsertUserId]
			,[InsertDate]
		)
		SELECT
			 t.[VehicleId]
			,t.[TransactionId]
			,t.[TrxDate]
			,ABS((t.TrxOdometer - t.PreviousTrxOdometer)) / CASE WHEN t.TrxLiters = 0 THEN 1 ELSE t.TrxLiters END AS [PerformanceByOdometer]
			,0 AS [PerformanceByGPS]
			,0 AS [InsertUserId]
			,GETUTCDATE() AS [InsertDate]
		FROM @lTable t	
    
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
    END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH
	
    SET XACT_ABORT OFF
	SET NOCOUNT OFF
    
END
' 
END
GO

/****** Object:  StoredProcedure [General].[Sp_Job_Alarm]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Job_Alarm]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =
-- ================================================================================================
-- Author:		Cristian Martínez 
-- Create date: 21/11/2014
-- Description:	Stored Procedure that execute the alarm Add Alarm for Expiration of Driver  License 
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_Job_Alarm]
AS
BEGIN 
	DECLARE @lAlarmId INT
	DECLARE	@lCustomerId INT
	DECLARE	@lAlarmTriggerId INT
	DECLARE	@lEntityTypeId INT
	DECLARE	@lEntityId INT
	DECLARE	@lPeriodicityId INT
	DECLARE	@lPhone VARCHAR(100)
	DECLARE @lCode VARCHAR(400) 
	DECLARE	@lEmail VARCHAR(500)
	DECLARE @lRowVersion TIMESTAMP
	DECLARE @tAlarm TABLE(
		AlarmId INT,
		CustomerId INT, 
		AlarmTriggerId INT,
		Code VARCHAR(400), 
		EntityTypeId INT, 
		EntityId INT,
		PeriodicityId INT,
		Phone VARCHAR(100),
		Email VARCHAR(500),
		RowVersion VARCHAR(20)
	)
	
	INSERT INTO @tAlarm
	(
		AlarmId, 
		CustomerId,
		AlarmTriggerId,
		Code, 
		EntityTypeId, 
		EntityId, 
		PeriodicityId, 
		Phone, 
		Email, 
		RowVersion
	)SELECT a.[AlarmId], 
		    a.[CustomerId], 
		    a.[AlarmTriggerId], 
		    b.[Code],
		    a.[EntityTypeId], 
		    a.[EntityId], 
		    a.[PeriodicityTypeId], 
		    a.[Phone], 
		    a.[Email], 
		    CONVERT(VARCHAR(20),a.[RowVersion])
	FROM [General].[Alarms] a
	INNER JOIN [General].[Types] b
	ON a.[AlarmTriggerId] = b.[TypeId]
	WHERE a.[Active] = 1
	 AND (b.[Code] = ''CUSTOMER_ALARM_TRIGGER_PREVENTIVE_MAINTENANCE'' OR b.[Code] = ''CUSTOMER_ALARM_TRIGGER_LICENSE_EXPIRATION'')
	 AND (a.[NextAlarm] IS NULL 
	 OR CONVERT(DATE, a.[NextAlarm])= CONVERT(DATE,DATEADD(Hour, -6,GETUTCDATE())))
	 
	 WHILE EXISTS(SELECT [AlarmId] FROM @tAlarm)
	 BEGIN 
		SELECT TOP 1 @lAlarmId  = a.[AlarmId], 
			         @lCustomerId = a.[CustomerId],
				     @lAlarmTriggerId = a.[AlarmTriggerId],
				     @lCode = a.[Code],
					 @lEntityTypeId = a.[EntityTypeId],
				     @lEntityId = a.[EntityId],
					 @lPeriodicityId = a.[PeriodicityId],
					 @lPhone = a.[Phone],
					 @lEmail = a.[Email],
					 @lRowVersion = CONVERT(TIMESTAMP, a.[RowVersion]) 
		FROM @tAlarm a
		
		-- Maintenance Preventive
		IF @lCode = ''CUSTOMER_ALARM_TRIGGER_PREVENTIVE_MAINTENANCE''
		BEGIN 		
			SELECT @lEntityId = [VehicleId]
			FROM [General].[PreventiveMaintenanceByVehicle]
			WHERE [PreventiveMaintenanceId] = @lEntityId
			
			/*EXEC [General].[Sp_PreventiveMaintenance_Alarm] @lAlarmId,
															@lEntityId,
															@lCustomerId,
															@lAlarmTriggerId,
															@lPeriodicityId,
															@lPhone,
															@lEmail,
															@lRowVersion*/
		END
		
		--License Expirated
		IF @lCode = ''CUSTOMER_ALARM_TRIGGER_LICENSE_EXPIRATION''
		BEGIN 
			EXEC [General].[Sp_LicenseExpiration_Alarm]	@lAlarmId,
														@lEntityId,
														@lCustomerId,
														@lAlarmTriggerId,
														@lPeriodicityId,
														@lPhone,
														@lEmail,
														@lRowVersion
		END	
		DELETE @tAlarm WHERE AlarmId = @lAlarmId AND CustomerId = @lCustomerId
		CONTINUE 
	 END 		
END' 
END
GO
/****** Object:  StoredProcedure [Insurance].[Sp_GetWightedIndexScoreDrivingFleet]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetWightedIndexScoreDrivingFleet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo
-- Create date: 16/02/2015
-- Description:	Retrieve Wighted Index Score Driving Fleet information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_GetWightedIndexScoreDrivingFleet] 
( @pPartnerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)=null,	--Posibles valores S:Summarized, D:Detailed
  @pCustomerId int =null
)
AS BEGIN

DECLARE @tableCustomers TABLE (CustomerId int, Name varchar(250)) 
DECLARE  @li_CustomerId INT
DECLARE @NameCustomer varchar(250)
DECLARE @tableDriversScore TABLE(
DriverScoreId int
,UserId	int
,Name  varchar (250)
,Identification  varchar(50)	
,ScoreType	 varchar(5)
,Score Float
,Photo varchar(max)
,ScoreSpeedAdmin Float
,ScoreSpeedRoute Float
)

DECLARE @performanceTable TABLE(
VehicleId INT,
PlateId VARCHAR(300),
[TrxMinDate] datetime,
[TrxMaxDate] datetime,
[TrxMinOdometer] INT,
[TrxMaxOdometer] INT,
CustomerId int
)

DECLARE @ScoreDrivingFleet TABLE(
CustomerId int
,EncryptedName	varchar (1000)
,AverageScore  FLOAT
)

INSERT INTO @tableCustomers
EXEC  [Insurance].[Sp_GetRosterInsurance]
		@pPartnerId,@pCustomerId
---SELECT * from @tableCustomers 

Select @li_CustomerId = MIN(CustomerId)
from @tableCustomers

WHILE @li_CustomerId is not null
BEGIN

INSERT INTO @performanceTable exec [Control].[Sp_OdometerTraveledReportByVehicle_Retrieve] @li_CustomerId,@pYear,@pMonth,@pStartDate ,@pEndDate 

INSERT into   @tableDriversScore  (DriverScoreId,UserId,Name,Identification,ScoreType,Score,Photo ,ScoreSpeedAdmin,ScoreSpeedRoute)
EXEC	 [Operation].[Sp_GetGlobalScoreAllDriver]
		@li_CustomerId ,
		@pYear ,
		@pMonth ,
		@pStartDate ,
		@pEndDate ,
		@pReportType

Select @NameCustomer = Name FROM  @tableCustomers where CustomerId=@li_CustomerId

insert into @ScoreDrivingFleet SELECT @li_CustomerId CustomerId, @NameCustomer  as EncryptedName ,CASE  WHEN AVG(Score) IS NULL THEN 0 ELSE AVG(Score)  END AS  AverageScore
FROM  @tableDriversScore 
Delete  FROM  @tableDriversScore 

-----------------------------
--Siguiente Customer
		Select @li_CustomerId = MIN(CustomerId)
		  from @tableCustomers 
		 where CustomerId > @li_CustomerId
END  -- End While

select t.*,(t.KmTraveled/(CASE WHEN t.AverageScore IS NULL OR t.AverageScore=0 then 1 else t.AverageScore end))  as WightedIndex from	
(select sdf.*,(select sum(pt.[TrxMaxOdometer]-pt.[TrxMinOdometer]) from @performanceTable pt where sdf.CustomerId=pt.CustomerId) as KmTraveled from @ScoreDrivingFleet sdf) as t
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_MonthlyClosing_LowPerformanceEmails]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MonthlyClosing_LowPerformanceEmails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 02/19/2015
-- Description:	Send the Monthly Closing Low Performance Emails
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_MonthlyClosing_LowPerformanceEmails]
(
	 @pYear INT								--@pYear: Year of monthly closing
	,@pMonth INT							--@pMonth: Month of monthly closing
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	DECLARE @lTo varchar(100) = '''',
			@lSubject varchar(300),
			@lMessage nvarchar(1000)
	
	SET @lSubject = ''Alerta en vehiculo con Caida de Rendimiento''
		
	
	DECLARE @lTable TABLE
	(	 [Index] INT NULL
		,[VehicleId] INT NULL
		,[CurrentPerformance] INT NULL
		,[PreviousPerformance] INT NULL
		,[DeleteFlag] BIT NULL
	)	
	
	INSERT @lTable(
		 [VehicleId]
		,[CurrentPerformance]
		,[PreviousPerformance]
		,[DeleteFlag]
	)	
	SELECT a.[VehicleId], AVG(a.[PerformanceByOdometer]), 0 AS [PreviousPerformance], 1 AS [DeleteFlag]
		FROM [General].[PerformanceByVehicle] a
	WHERE DATEPART(MONTH, a.[TrxDate]) = @pMonth
	  AND DATEPART(YEAR, a.[TrxDate]) = @pYear
	GROUP BY a.[VehicleId]
	
	IF(@pMonth = 1)
	BEGIN
		SELECT @pMonth = 12, @pYear = @pYear-1
	END
	ELSE
	BEGIN
		SELECT @pMonth = @pMonth-1
	END
	
	UPDATE @lTable
	SET [PreviousPerformance] = t.[PerformanceByOdometer]
	FROM (
		SELECT a.[VehicleId] AS [TempVehicleId], AVG(a.[PerformanceByOdometer]) AS [PerformanceByOdometer]
		FROM [General].[PerformanceByVehicle] a
	WHERE DATEPART(MONTH, a.[TrxDate]) = @pMonth
	  AND DATEPART(YEAR, a.[TrxDate]) = @pYear
	GROUP BY a.[VehicleId]) t
	WHERE [VehicleId] = t.[TempVehicleId]
	
	
	UPDATE @lTable
	SET [DeleteFlag] = 0
	WHERE -2 >= [CurrentPerformance] - [PreviousPerformance]

	DELETE @lTable
	WHERE [DeleteFlag] = 1
	
	DECLARE @lIndex INT = 0
	UPDATE @lTable
	SET [Index] = @lIndex, @lIndex = @lIndex+1
	
	
	
	---Send Emails---
	DECLARE @lCustomerId INT
	
	WHILE(@lIndex > 0)
	BEGIN
		
		SELECT	 @lCustomerId = a.[CustomerId]
				,@lMessage = ''Se produjo una caida de 2 o mas puntos de rendimiento en el siguiente vehiculo: '' + a.PlateId
		FROM [General].[Vehicles] a
			INNER JOIN @lTable t
				ON a.VehicleId = t.VehicleId
		WHERE t.[Index] = @lIndex
		
		SET @lTo = [General].[Fn_GetEmailsRole](''CUSTOMER_ADMIN'', @lCustomerId, 100)
		
		EXEC [General].[Sp_Email_AddOrEdit] null, @lTo, null, null, @lSubject, @lMessage, 0, 0, null, null 
		
		
		SET @lIndex = @lIndex-1
	END
	---Send Emails---
	
			
    SET NOCOUNT OFF
END
' 
END
GO

/****** Object:  StoredProcedure [Control].[Sp_VehicleFuelByGroup_AddOrEdit]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuelByGroup_AddOrEdit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update Vehicle Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuelByGroup_AddOrEdit]
(
	 @pVehicleFuelId INT = NULL				--@pVehicleFuelId: PK of the table
	,@pVehicleId INT						--@pVehicleId: FK of Vehicles
	,@pLiters DECIMAL(16,10)				--@pLiters: Liters of Fuel
	,@pAmount DECIMAL(16,6)					--@pAmount: Amount of Fuel
	,@pMonth INT							--@pMonth: Month
	,@pYear INT								--@pYear: Year
	,@pCustomerId INT						--@pCustomerId: Customer Id
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	EXEC [Control].[Sp_VehicleFuel_AddOrEdit]	 @pVehicleFuelId
												,@pVehicleId
												,@pLiters
												,@pAmount
												,@pMonth
												,@pYear
												,@pCustomerId
												,@pLoggedUserId
												,@pRowVersion
												,0 -- @pThrowError
	
END
' 
END
GO
/****** Object:  StoredProcedure [Operation].[Sp_ScoreSpeedPerMonthJob]    Script Date: 02/24/2015 14:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreSpeedPerMonthJob]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE Procedure [Operation].[Sp_ScoreSpeedPerMonthJob]
AS		
Begin
Declare @ldt_Start_Job datetime,
		@ldt_End_Job datetime,
		@lvc_Job varchar(30),
		@lc_status char(1),
		@li_error int = 0,
		@lvc_error_ref varchar(100),
		@li_CustomerId Int,
		@li_Month Int,
		@li_Year Int

Declare @Scores table
	(DriverScoreId int,
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 Score Float,
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float)	 

	Set @lvc_Job = ''ScoreSpeedPerMonth''
	Set @ldt_Start_Job = GETDATE()
	Set @li_Month = DATEPART(MM, @ldt_Start_Job)
	--Set @li_Month = 10 --Para Pruebas
	Set @li_Year = DATEPART(YY, @ldt_Start_Job)
	
	--Borrar registros previos del mes
	Delete Operation.DriversScores
	 where ScoreMonth = @li_Month
	   and ScoreYear = @li_Year
	   
	Select @li_CustomerId = MIN(D.CustomerId)
	  from General.DriversUsers D
	  inner join General.Users U on D.UserId = U.UserId
	 where U.IsActive = 1   

	While @li_CustomerId is not null
	begin
		--Generar registros de cálculo por límite administrativo
		Insert into @Scores
			(DriverScoreId, UserId, Name, Identification, Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient
					   ,OverSpeedDistance)
		Exec Operation.Sp_GetScoreSpeedAllDriver @li_CustomerId, @li_Year, @li_Month, null, null, ''A'', ''J''
		
		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end
		
		Insert into Operation.DriversScores
			(CustomerId, UserId, ScoreYear, ScoreMonth, ScoreType, Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,OverSpeedDistance)
		Select @li_CustomerId, UserId, @li_Year, @li_Month, ''SPADM'', Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,OverSpeedDistance
		  from @Scores

		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end

		Delete @Scores
		
		--Generar registros de cálculo por límite de carretera
		Insert into @Scores
			(DriverScoreId, UserId, Name, Identification, Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,OverSpeedDistance)
		Exec Operation.Sp_GetScoreSpeedAllDriver @li_CustomerId, @li_Year, @li_Month, null, null, ''R'', ''J''
		
		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end
		
		Insert into Operation.DriversScores
			(CustomerId, UserId, ScoreYear, ScoreMonth, ScoreType, Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
		Select @li_CustomerId, UserId, @li_Year, @li_Month, ''SPROU'', Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance
		  from @Scores

		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end

		Delete @Scores

		--Siguiente Customer
		Select @li_CustomerId = MIN(D.CustomerId)
		  from General.DriversUsers D
		  inner join General.Users U on D.UserId = U.UserId
		 where U.IsActive = 1   
		   and D.CustomerId > @li_CustomerId
	end

	If @li_error <> 0 
	begin
		Set @lc_status = ''E'' --Error
		Set @lvc_error_ref = ''Customer Id: '' + @li_CustomerId
	end
	else
	begin
		Set @lc_status = ''T'' --Terminado		
	end

	--Insertar datos en Log
	Set @ldt_End_Job = GETDATE()
	
	Insert into Operation.JobsLog
		(JobID, StartDate, EndDate, Status, ErrorNumber, ErrorReference)
	values
		(@lvc_Job, @ldt_Start_Job, @ldt_End_Job, @lc_status, @li_error, @lvc_error_ref)
		
end


' 
END
GO

/****** Object:  StoredProcedure [Control].[Sp_TrendFuelsReportBySubUnit_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TrendFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Sub Unit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TrendFuelsReportBySubUnit_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	
	DECLARE @lTable TABLE
	(
		 [VehicleId] INT NOT NULL
		,[Samples] INT NOT NULL
		,[AVGPerformace] DECIMAL(16,2) NOT NULL
		,Interception DECIMAL(16,2) NOT NULL
		,slope DECIMAL(16,2) NOT NULL
		
	)
	INSERT @lTable  EXEC [Control].[Sp_TrendFuelsReportByVehicle_Retrieve] @pCustomerId = @pCustomerId
																				,@pYear = @pYear
																				,@pMonth = @pMonth
																				,@pStartDate = @pStartDate
																				,@pEndDate = @pEndDate
	
	SELECT
		 s.[SubUnitName]
		,AVG([slope]) AS [slope]
	FROM @lTable t
		INNER JOIN
			(SELECT
				 a.[VehicleId]
				,b.[Name] AS [SubUnitName]
			FROM [General].[Vehicles] a
				INNER JOIN [General].[VehicleSubUnits] b
					ON a.[SubUnitId] = b.[SubUnitId]
				INNER JOIN [General].[VehicleUnits] c
					ON b.[UnitId] = c.[UnitId]
			WHERE c.[CustomerId] = @pCustomerId) s
		ON t.[VehicleId] = s.[VehicleId]
	GROUP BY s.[SubUnitName]
	
	SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_TrendFuelsReportByGroup_Retrieve]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TrendFuelsReportByGroup_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Vehicle Group information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TrendFuelsReportByGroup_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lTable TABLE
	(
	     [VehicleId] INT NOT NULL
		,[Samples] INT NOT NULL
		,[AVGPerformace] DECIMAL(16,2) NOT NULL
		,Interception DECIMAL(16,2) NOT NULL
		,slope DECIMAL(16,2) NOT NULL
	)
	INSERT @lTable  EXEC [Control].[Sp_TrendFuelsReportByVehicle_Retrieve] @pCustomerId = @pCustomerId
																				,@pYear = @pYear
																				,@pMonth = @pMonth
																				,@pStartDate = @pStartDate
																				,@pEndDate = @pEndDate
	
	SELECT
		 s.[VehicleGroupName]
		,AVG([slope]) AS [slope]
	FROM @lTable t
		INNER JOIN
			(SELECT
				 b.[VehicleId]
				,a.[Name] AS [VehicleGroupName]
			FROM  [General].[VehicleGroups] a
				INNER JOIN [General].[VehiclesByGroup] b
					ON b.[VehicleGroupId] = a.[VehicleGroupId]
			WHERE a.[CustomerId] = @pCustomerId) s
		ON t.[VehicleId] = s.[VehicleId]
	GROUP BY s.[VehicleGroupName]
	
    SET NOCOUNT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_MonthlyClosing]    Script Date: 02/24/2015 14:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MonthlyClosing]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- ===============================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/03/2014
-- Description:	Execute tasks of the Monthly Closing
-- ===============================================================
Create Procedure [Control].[Sp_MonthlyClosing]
AS 
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0 
        DECLARE @lCurrentMonth INT
        DECLARE @lCurrentYear INT
        DECLARE @lClosingMonth INT
        DECLARE @lClosingYear INT
        DECLARE @lProcessDate DATETIME		
        DECLARE @lCount INT	
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END

        Set @lProcessDate = GETDATE()
        Set @lCurrentMonth = DATEPART(MM, @lProcessDate)
        Set @lCurrentYear = DATEPART(YY, @lProcessDate)
        IF @lCurrentMonth = 1
        BEGIN
			Set @lClosingMonth = 12
			Set @lClosingYear = @lCurrentYear - 1
		END
		ELSE
		BEGIN
			Set @lClosingMonth = @lCurrentMonth - 1
			Set @lClosingYear = @lCurrentYear
		END
		
		Select @lCount = COUNT(1)
		  from Control.CustomerCredits
		 where Year = @lCurrentYear
		   and Month = @lCurrentMonth
		   
		IF @lCount = 0
		BEGIN    
			--Copy CustomerCredit data for the Current Month		
			Insert into Control.CustomerCredits
				(CustomerId, Year, Month, CreditAmount, CreditAssigned, CreditTypeId, InsertDate)
			Select CustomerId, @lCurrentYear, @lCurrentMonth, CreditAmount, 0, CreditTypeId, @lProcessDate
			  from Control.CustomerCredits 
			 where Year = @lClosingYear
			   and Month = @lClosingMonth 
			   
			--There is a Card but no record in CustomerCredit
			Insert into Control.CustomerCredits
				(CustomerId, Year, Month, CreditAmount, CreditAssigned, CreditTypeId, InsertDate)
			Select CCC.CustomerId, @lCurrentYear, @lCurrentMonth, CCC.CreditLimit, 0, 1000, @lProcessDate
			  from General.CustomerCreditCards CCC 
			 inner join (Select CustomerID, Max(CustomerCreditCardId) as CustomerCreditCardId 
						  from General.CustomerCreditCards
						 where StatusId in (100, 101)
						 group by CustomerID) CLast
				on CCC.CustomerId = CLast.CustomerId and CCC.CustomerCreditCardId = CLast.CustomerCreditCardId
			 where not exists (Select C.CustomerId
								 from Control.CustomerCredits C
								where C.CustomerId = CCC.CustomerId
								  and C.Year = @lCurrentYear
								  and C.Month = @lCurrentMonth)
	        
			--Update Credit Assigned for CustomerCredit
			Update C
			   set C.CreditAssigned = IsNull(CCards.SumCreditLimit, 0) 
			  from Control.CustomerCredits C
			  left join (Select CC.CustomerId, SUM(CC.CreditLimit) as SumCreditLimit
						   from Control.CreditCard CC
						  where StatusId in (0, 2, 3, 4, 6, 8)
						  group by CC.CustomerId) CCards
				on C.CustomerId = CCards.CustomerId 
			 where C.Year = @lCurrentYear
			   and C.Month = @lCurrentMonth
	    END
	       
		Select @lCount = COUNT(1)
		  from Control.FuelsByCredit
		 where Year = @lCurrentYear
		   and Month = @lCurrentMonth
		   
		IF @lCount = 0
		BEGIN    
			--Copy FuelsByCredit data for the Current Month
			Insert into Control.FuelsByCredit
				(CustomerId, FuelId, Month, Year, Total, Assigned, InsertDate)
			Select CustomerId, FuelId, @lCurrentMonth, @lCurrentYear, Total, 0, @lProcessDate
			  from Control.FuelsByCredit
			 where Year = @lClosingYear
			   and Month = @lClosingMonth
		END
		   
		Select @lCount = COUNT(1)
		  from Control.VehicleFuel
		 where Year = @lCurrentYear
		   and Month = @lCurrentMonth
		   
		IF @lCount = 0
		BEGIN    
			--Copy VehicleFuel data for the Current Month
			Insert into Control.VehicleFuel
				(VehicleId, Month, Year, Liters, Amount, InsertDate)
			Select VF.VehicleId, @lCurrentMonth, @lCurrentYear, Case When PF.LiterPrice is null then VF.Liters else VF.Amount / PF.LiterPrice end, VF.Amount, @lProcessDate
			  from Control.VehicleFuel VF
			 inner join General.Vehicles V on VF.VehicleId = V.VehicleId
			 inner join General.VehicleCategories VC on V.VehicleCategoryId = VC.VehicleCategoryId
			 inner join General.Customers C on V.CustomerId = C.CustomerId
			 inner join General.CustomersByPartner cp on cp.CustomerId = c.CustomerId
			 left join Control.PartnerFuel PF on C.CustomerId = cp.CustomerId and cp.PartnerId = PF.PartnerId and VC.DefaultFuelId = PF.FuelId and PF.EndDate is null
			 where VF.Year = @lClosingYear
			   and VF.Month = @lClosingMonth
			   and V.Active = 1
			
			--Update FuelsByCredit Assigned Amount   
			Update FC
			   set FC.Assigned = VFG.Amount
			  from Control.FuelsByCredit FC
			 inner join (Select V.CustomerId, VC.DefaultFuelId, Sum(VF.Amount) Amount
						  from Control.VehicleFuel VF
						 inner join General.Vehicles V on VF.VehicleId = V.VehicleId
						 inner join General.VehicleCategories VC on V.VehicleCategoryId = VC.VehicleCategoryId
						 where VF.Year = @lCurrentYear
						   and VF.Month = @lCurrentMonth
						 group by V.CustomerId, VC.DefaultFuelId) VFG
				on FC.CustomerId = VFG.CustomerId and FC.FuelId = VFG.DefaultFuelId
			 where FC.Year = @lCurrentYear
			   and FC.Month = @lCurrentMonth
		END

	    --Warning Emails 
	    Exec Control.Sp_MonthlyClosing_WarningEmails @lCurrentYear, @lCurrentMonth
	    
	    EXEC [Control].[Sp_MonthlyClosing_LowPerformanceEmails] @lCurrentYear, @lCurrentMonth
		
        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
			      
         
	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
' 
END
GO
/****** Object:  StoredProcedure [Control].[Sp_ComparativeFuelsReport_Retrieve]    Script Date: 02/24/2015 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ComparativeFuelsReport_Retrieve]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- ================================================================================================
-- Author:		Andrés Oviedo Brenes.
-- Create date: 05/02/2015
-- Description:	Retrieve ComparativeFuelsReport_Retrieve information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_ComparativeFuelsReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	Declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	Declare @pInitialDate2 datetime
	declare @pFinalDate2 datetime
	
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT(''0'' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT(''0'' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end
	
DECLARE @tTable2 Table(
	PlateId VARCHAR(10),
	VehicleId int,
	InitialOdometer decimal,
	FinalOdometer decimal,
	Liters decimal,
	Manufacturer varchar(300),
	VehicleModel varchar(300),
    [Year] int,
	[Type] varchar(300),
	CylinderCapacity int,
	VehicleCategoryId INT,
	TransactionMonth INT,
	TransactionYear INT,
	[Routes] varchar(500),
	DefaultPerformance decimal
)	

DECLARE @tTable Table(
	cte_start_date datetime
)	

;WITH CTE AS
(
    SELECT @pInitialDate AS cte_start_date
    UNION ALL
    SELECT DATEADD(MONTH, 1, cte_start_date)
    FROM CTE
    WHERE DATEADD(MONTH, 1, cte_start_date) <= @pFinalDate   
)
insert INTO @tTable  SELECT * FROM CTE
 
 DECLARE @cte_sd datetime

	DECLARE myCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT cte_start_date AS ''ID'' 
		FROM @tTable
	OPEN myCursor
	FETCH NEXT FROM myCursor INTO @cte_sd
	WHILE @@FETCH_STATUS = 0 BEGIN
		PRINT @cte_sd
		
		set @pInitialDate2=  CAST(
							  CAST(YEAR(@cte_sd) AS VARCHAR(4)) +
							  RIGHT(''0'' + CAST(MONTH(@cte_sd) AS VARCHAR(2)), 2) +
							  RIGHT(''0'' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate2=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate2)+1,0))
		
			INSERT INTO @tTable2
			SELECT
			   v.PlateId,
			   v.VehicleId,
			   ISNULL((SELECT TOP 1 Odometer FROM [Control].Transactions  WHERE [DATE] <= @pInitialDate2 and VehicleId=v.VehicleId ORDER BY [DATE] DESC),0) as InitialOdometer,
			   ISNULL((SELECT TOP 1 Odometer FROM [Control].Transactions  WHERE [DATE] BETWEEN @pInitialDate2 AND @pFinalDate2 and VehicleId=v.VehicleId ORDER BY [DATE] ASC),0) as FinalOdometer,
			   ISNULL((SELECT SUM(Liters) FROM [Control].Transactions  WHERE [DATE] BETWEEN @pInitialDate2 AND @pFinalDate2 and VehicleId=v.VehicleId),0) as Liters,
				vc.Manufacturer,
				vc.VehicleModel,
				vc.[Year],
				vc.[Type],
				vc.CylinderCapacity,
				vc.VehicleCategoryId
				,MONTH(@pInitialDate2) as TransactionMonth
				,YEAR(@pFinalDate2) as TransactionYear
				,Control.GetRouteConcat(v.VehicleId) As [Routes]
				,vc.DefaultPerformance
			   FROM General.Vehicles v 
			   INNER JOIN General.VehicleCategories vc ON vc.VehicleCategoryId=v.VehicleCategoryId
			   INNER JOIN Control.Transactions t ON v.VehicleId=t.VehicleId
			    WHERE V.CustomerId=@pCustomerId and MONTH(t.[Date])=@pMonth AND YEAR(t.[Date])=@pYear

		FETCH NEXT FROM myCursor INTO @cte_sd

	END

CLOSE myCursor
DEALLOCATE myCursor

	DECLARE @lTable TABLE
	(
	     VehicleId		INT NOT NULL
	    ,PlateNumber		varchar(10)
		,[year]				INT NOT NULL
		,[month]			INT NOT NULL
		,Totalodometer		DECIMAL(16,2) NOT NULL
		,Totalliters		DECIMAL(16,2) NOT NULL
		,[Samples]			INT NOT NULL
		,[AVGPerformace]	DECIMAL(16,2) NOT NULL
		,Interception		DECIMAL(16,2) NOT NULL
		,slope				DECIMAL(16,2) NOT NULL
	
	)
	INSERT @lTable  EXEC [Control].[Sp_TrendFuelsReportByVehicle_Retrieve] @pCustomerId = @pCustomerId
																				,@pYear = @pYear
																				,@pMonth = @pMonth
																				,@pStartDate = null
																				,@pEndDate = null
																				,@pVehiculoId =null

SELECT t.VehicleId,t.DefaultPerformance,t.PlateId as PlateNumber,t.[TransactionMonth] as [Month],t.[TransactionYear] as [Year],cast((t.FinalOdometer-t.InitialOdometer) as decimal) AS TotalOdometer,SUM(t.Liters) as TotalLiters,t.[Routes],t.Manufacturer, t.[Type],t.VehicleModel,t.[Year] as Model,t.CylinderCapacity,
case when SUM(t.Liters)>0 
then 
	cast((SUM(t.FinalOdometer-t.InitialOdometer)/SUM(t.Liters)) as decimal) 
else 
	cast(0 as decimal)
end	
	 AS Performance,
	t2.slope,
	t2.Samples

FROM @tTable2 t INNER JOIN @lTable t2 ON t.VehicleId=t2.VehicleId
  GROUP BY t.VehicleId,t.PlateId,t.[TransactionMonth],t.[TransactionYear],t.InitialOdometer,t.FinalOdometer,t.[Routes],t.Manufacturer,t.[Type],t.VehicleModel,t.CylinderCapacity,t.[Year],t2.slope,
	t2.Samples,t.DefaultPerformance
		
	SET NOCOUNT OFF
END
' 
END
GO

