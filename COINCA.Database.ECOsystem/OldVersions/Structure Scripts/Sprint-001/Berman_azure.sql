--~ Commented out USE [database] statement because USE statement is not supported to another database.
-- USE [ECOSystem]
GO
/****** Object:  UserDefinedTableType [dbo].[TypeDeviceSpeed]    Script Date: 02/24/2015 15:12:09 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'TypeDeviceSpeed' AND ss.name = N'dbo')
DROP TYPE [dbo].[TypeDeviceSpeed]
GO
/****** Object:  UserDefinedTableType [dbo].[TypeDriverScore]    Script Date: 02/24/2015 15:12:09 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'TypeDriverScore' AND ss.name = N'dbo')
DROP TYPE [dbo].[TypeDriverScore]
GO
/****** Object:  UserDefinedTableType [dbo].[TypeDeviceSpeed]    Script Date: 02/24/2015 15:12:09 ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'TypeDeviceSpeed' AND ss.name = N'dbo')
CREATE TYPE [dbo].[TypeDeviceSpeed] AS TABLE(
	[DeviceSpeedId] [int] IDENTITY(1,1) NOT NULL,
	[Device] [int] NOT NULL,
	[StartOdometer] [int] NULL,
	[EndOdometer] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TypeDriverScore]    Script Date: 02/24/2015 15:12:09 ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'TypeDriverScore' AND ss.name = N'dbo')
CREATE TYPE [dbo].[TypeDriverScore] AS TABLE(
	[KmTraveled] [float] NULL,
	[OverSpeed] [float] NULL,
	[OverSpeedAmount] [float] NULL,
	[OverSpeedDistancePercentage] [float] NULL,
	[OverSpeedAverage] [float] NULL,
	[OverSpeedWeihgtedAverage] [float] NULL,
	[StandardDeviation] [float] NULL,
	[Variance] [float] NULL,
	[VariationCoefficient] [float] NULL,
	[AverageScore] [float] NULL,
	[OverSpeedDistance] [float] NULL
)
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.




-- USE [ECOSystem]
GO
/****** Object:  ForeignKey [FK_CustomersByPartner_Customers]    Script Date: 02/17/2015 11:44:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_CustomersByPartner_Customers]') AND parent_object_id = OBJECT_ID(N'[General].[CustomersByPartner]'))
ALTER TABLE [General].[CustomersByPartner] DROP CONSTRAINT [FK_CustomersByPartner_Customers]
GO
/****** Object:  ForeignKey [FK_CustomersByPartner_Partners]    Script Date: 02/17/2015 11:44:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_CustomersByPartner_Partners]') AND parent_object_id = OBJECT_ID(N'[General].[CustomersByPartner]'))
ALTER TABLE [General].[CustomersByPartner] DROP CONSTRAINT [FK_CustomersByPartner_Partners]
GO
/****** Object:  Table [General].[CustomersByPartner]    Script Date: 02/17/2015 11:44:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_CustomersByPartner_Customers]') AND parent_object_id = OBJECT_ID(N'[General].[CustomersByPartner]'))
ALTER TABLE [General].[CustomersByPartner] DROP CONSTRAINT [FK_CustomersByPartner_Customers]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_CustomersByPartner_Partners]') AND parent_object_id = OBJECT_ID(N'[General].[CustomersByPartner]'))
ALTER TABLE [General].[CustomersByPartner] DROP CONSTRAINT [FK_CustomersByPartner_Partners]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[CustomersByPartner]') AND type in (N'U'))
DROP TABLE [General].[CustomersByPartner]
GO
/****** Object:  Table [General].[CustomersByPartner]    Script Date: 02/17/2015 11:44:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[CustomersByPartner]') AND type in (N'U'))
BEGIN
CREATE TABLE [General].[CustomersByPartner](
	[CustomerByPartnerId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PartnerId] [int] NOT NULL,
	[IsDefault] [bit] NULL,
	[InsertDate] [datetime] NULL,
	[InsertUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_CustomersByPartner] PRIMARY KEY CLUSTERED 
(
	[CustomerByPartnerId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
/****** Object:  ForeignKey [FK_CustomersByPartner_Customers]    Script Date: 02/17/2015 11:44:36 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_CustomersByPartner_Customers]') AND parent_object_id = OBJECT_ID(N'[General].[CustomersByPartner]'))
ALTER TABLE [General].[CustomersByPartner]  WITH CHECK ADD  CONSTRAINT [FK_CustomersByPartner_Customers] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_CustomersByPartner_Customers]') AND parent_object_id = OBJECT_ID(N'[General].[CustomersByPartner]'))
ALTER TABLE [General].[CustomersByPartner] CHECK CONSTRAINT [FK_CustomersByPartner_Customers]
GO
/****** Object:  ForeignKey [FK_CustomersByPartner_Partners]    Script Date: 02/17/2015 11:44:36 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_CustomersByPartner_Partners]') AND parent_object_id = OBJECT_ID(N'[General].[CustomersByPartner]'))
ALTER TABLE [General].[CustomersByPartner]  WITH CHECK ADD  CONSTRAINT [FK_CustomersByPartner_Partners] FOREIGN KEY([PartnerId])
REFERENCES [General].[Partners] ([PartnerId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_CustomersByPartner_Partners]') AND parent_object_id = OBJECT_ID(N'[General].[CustomersByPartner]'))
ALTER TABLE [General].[CustomersByPartner] CHECK CONSTRAINT [FK_CustomersByPartner_Partners]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.




-- USE [ECOSystem]
GO
/****** Object:  ForeignKey [FK__Performan__Vehic__6AEFE058]    Script Date: 02/24/2015 15:04:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK__Performan__Vehic__6AEFE058]') AND parent_object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]'))
ALTER TABLE [General].[PerformanceByVehicle] DROP CONSTRAINT [FK__Performan__Vehic__6AEFE058]
GO
/****** Object:  ForeignKey [FK_PerformanceByVehicle_Transactions]    Script Date: 02/24/2015 15:04:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PerformanceByVehicle_Transactions]') AND parent_object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]'))
ALTER TABLE [General].[PerformanceByVehicle] DROP CONSTRAINT [FK_PerformanceByVehicle_Transactions]
GO
/****** Object:  Table [General].[PerformanceByVehicle]    Script Date: 02/24/2015 15:04:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK__Performan__Vehic__6AEFE058]') AND parent_object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]'))
ALTER TABLE [General].[PerformanceByVehicle] DROP CONSTRAINT [FK__Performan__Vehic__6AEFE058]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PerformanceByVehicle_Transactions]') AND parent_object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]'))
ALTER TABLE [General].[PerformanceByVehicle] DROP CONSTRAINT [FK_PerformanceByVehicle_Transactions]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]') AND type in (N'U'))
DROP TABLE [General].[PerformanceByVehicle]
GO
/****** Object:  Table [General].[PerformanceByVehicle]    Script Date: 02/24/2015 15:04:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [General].[PerformanceByVehicle](
	[PerformanceVehicleId] [int] IDENTITY(1,1) NOT NULL,
	[VehicleId] [int] NOT NULL,
	[TransactionId] [int] NOT NULL,
	[TrxDate] [datetime] NOT NULL,
	[PerformanceByOdometer] [decimal](16, 2) NOT NULL,
	[PerformanceByGPS] [decimal](16, 2) NOT NULL,
	[InsertUserId] [int] NULL,
	[InsertDate] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK__Performa__0C517EAC160F4887] PRIMARY KEY CLUSTERED 
(
	[PerformanceVehicleId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
/****** Object:  ForeignKey [FK__Performan__Vehic__6AEFE058]    Script Date: 02/24/2015 15:04:19 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK__Performan__Vehic__6AEFE058]') AND parent_object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]'))
ALTER TABLE [General].[PerformanceByVehicle]  WITH CHECK ADD  CONSTRAINT [FK__Performan__Vehic__6AEFE058] FOREIGN KEY([VehicleId])
REFERENCES [General].[Vehicles] ([VehicleId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK__Performan__Vehic__6AEFE058]') AND parent_object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]'))
ALTER TABLE [General].[PerformanceByVehicle] CHECK CONSTRAINT [FK__Performan__Vehic__6AEFE058]
GO
/****** Object:  ForeignKey [FK_PerformanceByVehicle_Transactions]    Script Date: 02/24/2015 15:04:19 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PerformanceByVehicle_Transactions]') AND parent_object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]'))
ALTER TABLE [General].[PerformanceByVehicle]  WITH CHECK ADD  CONSTRAINT [FK_PerformanceByVehicle_Transactions] FOREIGN KEY([TransactionId])
REFERENCES [Control].[Transactions] ([TransactionId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PerformanceByVehicle_Transactions]') AND parent_object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]'))
ALTER TABLE [General].[PerformanceByVehicle] CHECK CONSTRAINT [FK_PerformanceByVehicle_Transactions]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.


-- USE [ECOSystem]
GO

/****** Object:  Table [Operation].[DriversScores]    Script Date: 02/18/2015 14:42:28 ******/
SET ANSI_NULLS ON
GO

/****** Object:  Table [Operation].[DriversScores]    Script Date: 02/18/2015 14:42:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[ScoreDrivingFormula]') AND type in (N'U'))
DROP TABLE [Insurance].[ScoreDrivingFormula]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.

-- USE [ECOSystem]
GO

/****** Object:  Table [Operation].[DriversScores]    Script Date: 02/18/2015 14:42:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Insurance].[ScoreDrivingFormula](
	   ScoreDrivingFormulaId int primary key identity(1,1),
	   Name varchar(300) not null,
	   ScoreDrivingFormulaStr varchar(1000) not null,
       ScoreDrivingFormulaSP varchar(300) not null,
	   IsActive bit null,
	   CustomerId int not null,
	   [InsertDate] [datetime] NULL,
	   [InsertUserId] [int] NULL,
	   [ModifyDate] [datetime] NULL,
	   [ModifyUserId] [int] NULL,
	   [RowVersion] [timestamp] NOT NULL
)
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.



-- USE [ECOSystem]
GO

/****** Object:  Table [Operation].[DriversScores]    Script Date: 02/18/2015 14:42:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[DriversScores]') AND type in (N'U'))
DROP TABLE [Operation].[DriversScores]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.

-- USE [ECOSystem]
GO

/****** Object:  Table [Operation].[DriversScores]    Script Date: 02/18/2015 14:42:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Operation].[DriversScores](
	[CustomerId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[ScoreYear] [int] NOT NULL,
	[ScoreMonth] [int] NOT NULL,
	[ScoreType] [varchar](5) NOT NULL,
	[Score] [float] NOT NULL,
	[KmTraveled] [float] NULL,
	[OverSpeed] [float] NULL,
	[OverSpeedAmount] [float] NULL,
	[OverSpeedDistancePercentage] [float] NULL,
	[OverSpeedAverage] [float] NULL,
	[OverSpeedWeihgtedAverage] [float] NULL,
	[StandardDeviation] [float] NULL,
	[Variance] [float] NULL,
	[VariationCoefficient] [float] NULL,
	OverSpeedDistance float null
	
	
CONSTRAINT [PK_DriversScores] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC,
	[UserId] ASC,
	[ScoreYear] ASC,
	[ScoreMonth] ASC,
	[ScoreType] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)

GO

SET ANSI_PADDING OFF
GO

