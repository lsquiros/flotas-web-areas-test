--~ Commented out USE [database] statement because USE statement is not supported to another database.
-- USE [ECOsystem]
GO

/****** Object:  Table [General].[PreventiveMaintenanceRecordByVehicleDetail]    Script Date: 02/23/2015 16:32:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceRecordByVehicleDetail]') AND type in (N'U'))
DROP TABLE [General].[PreventiveMaintenanceRecordByVehicleDetail]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.


-- USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PreventiveMaintenanceRecordByVehicle_PreventiveMaintenanceRecordByVehicle]') AND parent_object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceRecordByVehicle]'))
ALTER TABLE [General].[PreventiveMaintenanceRecordByVehicle] DROP CONSTRAINT [FK_PreventiveMaintenanceRecordByVehicle_PreventiveMaintenanceRecordByVehicle]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PreventiveMaintenanceRecordByVehicleDetail_PreventiveMaintenanceId]') AND parent_object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceRecordByVehicle]'))
ALTER TABLE [General].[PreventiveMaintenanceRecordByVehicle] DROP CONSTRAINT [FK_PreventiveMaintenanceRecordByVehicleDetail_PreventiveMaintenanceId]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.

-- USE [ECOsystem]
GO

/****** Object:  Table [General].[PreventiveMaintenanceRecordByVehicle]    Script Date: 02/23/2015 16:32:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceRecordByVehicle]') AND type in (N'U'))
DROP TABLE [General].[PreventiveMaintenanceRecordByVehicle]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.


-- USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PreventiveMaintenanceByVehicle_Customers]') AND parent_object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceByVehicle]'))
ALTER TABLE [General].[PreventiveMaintenanceByVehicle] DROP CONSTRAINT [FK_PreventiveMaintenanceByVehicle_Customers]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PreventiveMaintenanceByVehicle_PreventiveMaintenanceCatalog]') AND parent_object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceByVehicle]'))
ALTER TABLE [General].[PreventiveMaintenanceByVehicle] DROP CONSTRAINT [FK_PreventiveMaintenanceByVehicle_PreventiveMaintenanceCatalog]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PreventiveMaintenanceByVehicle_Vehicles]') AND parent_object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceByVehicle]'))
ALTER TABLE [General].[PreventiveMaintenanceByVehicle] DROP CONSTRAINT [FK_PreventiveMaintenanceByVehicle_Vehicles]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.

-- USE [ECOsystem]
GO

/****** Object:  Table [General].[PreventiveMaintenanceByVehicle]    Script Date: 02/23/2015 16:32:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceByVehicle]') AND type in (N'U'))
DROP TABLE [General].[PreventiveMaintenanceByVehicle]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.


-- USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PreventiveMaintenanceCost_PreventiveMaintenanceCatalogId]') AND parent_object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceCost]'))
ALTER TABLE [General].[PreventiveMaintenanceCost] DROP CONSTRAINT [FK_PreventiveMaintenanceCost_PreventiveMaintenanceCatalogId]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.

-- USE [ECOsystem]
GO

/****** Object:  Table [General].[PreventiveMaintenanceCost]    Script Date: 02/23/2015 16:32:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceCost]') AND type in (N'U'))
DROP TABLE [General].[PreventiveMaintenanceCost]
GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.


-- USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PreventiveMaintenanceCatalog_Customers]') AND parent_object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceCatalog]'))
ALTER TABLE [General].[PreventiveMaintenanceCatalog] DROP CONSTRAINT [FK_PreventiveMaintenanceCatalog_Customers]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PreventiveMaintenanceCatalog_Customers1]') AND parent_object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceCatalog]'))
ALTER TABLE [General].[PreventiveMaintenanceCatalog] DROP CONSTRAINT [FK_PreventiveMaintenanceCatalog_Customers1]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PreventiveMaintenanceCatalog_Types]') AND parent_object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceCatalog]'))
ALTER TABLE [General].[PreventiveMaintenanceCatalog] DROP CONSTRAINT [FK_PreventiveMaintenanceCatalog_Types]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Preventiv__Frequ__336AA144]') AND type = 'D')
BEGIN
ALTER TABLE [General].[PreventiveMaintenanceCatalog] DROP CONSTRAINT [DF__Preventiv__Frequ__336AA144]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PreventiveMaintenanceCatalog_Cost]') AND type = 'D')
BEGIN
ALTER TABLE [General].[PreventiveMaintenanceCatalog] DROP CONSTRAINT [DF_PreventiveMaintenanceCatalog_Cost]
END

GO
--~ Commented out USE [database] statement because USE statement is not supported to another database.

-- USE [ECOsystem]
GO

/****** Object:  Table [General].[PreventiveMaintenanceCatalog]    Script Date: 02/23/2015 16:32:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[PreventiveMaintenanceCatalog]') AND type in (N'U'))
DROP TABLE [General].[PreventiveMaintenanceCatalog]
GO



-----------------------------------



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[PreventiveMaintenanceCatalog](
	[PreventiveMaintenanceCatalogId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Description] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FrequencyKm] [float] NULL,
	[AlertBeforeKm] [int] NULL,
	[FrequencyMonth] [float] NULL,
	[AlertBeforeMonth] [int] NULL,
	[FrequencyDate] [datetime] NULL,
	[AlertBeforeDate] [int] NULL,
	[Cost] [decimal](16, 2) NULL,
	[InsertUserId] [int] NOT NULL,
	[InsertDate] [date] NOT NULL,
	[ModifyUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_PreventiveMaintenanceCatalog] PRIMARY KEY CLUSTERED 
(
	[PreventiveMaintenanceCatalogId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
ALTER TABLE [General].[PreventiveMaintenanceCatalog] ADD  CONSTRAINT [DF_PreventiveMaintenanceCatalog_Cost]  DEFAULT ((0)) FOR [Cost]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[PreventiveMaintenanceByVehicle](
	[PreventiveMaintenanceId] [int] IDENTITY(1,1) NOT NULL,
	[VehicleId] [int] NOT NULL,
	[PreventiveMaintenanceCatalogId] [int] NOT NULL,
	[LastReviewOdometer] [float] NULL,
	[LastReviewDate] [datetime] NULL,
	[NextReviewOdometer] [float] NULL,
	[NextReviewDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[Registred] [bit] NULL,
	[AlarmSent] [bit] NULL,
	[InsertUserId] [int] NULL,
	[InsertDate] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_PreventiveMaintenanceByVehicle] PRIMARY KEY CLUSTERED 
(
	[PreventiveMaintenanceId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[PreventiveMaintenanceCost](
	[PreventiveMaintenanceCostId] [int] IDENTITY(1,1) NOT NULL,
	[PreventiveMaintenanceCatalogId] [int] NOT NULL,
	[Description] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Cost] [decimal](16, 2) NULL,
	[InsertUserId] [int] NULL,
	[InsertDateId] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[ModifyDateId] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_PreventiveMaintenanceCost] PRIMARY KEY CLUSTERED 
(
	[PreventiveMaintenanceCostId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[PreventiveMaintenanceRecordByVehicle](
	[PreventiveMaintenanceRecordByVehicleId] [int] IDENTITY(1,1) NOT NULL,
	[PreventiveMaintenanceId] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[Record] [decimal](16, 2) NOT NULL,
	[InsertUserId] [int] NULL,
	[InsertDateId] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[ModifyDateId] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
	[Cost] [decimal](16, 2) NULL,
 CONSTRAINT [PK_PreventiveMaintenanceRecordByVehicle] PRIMARY KEY CLUSTERED 
(
	[PreventiveMaintenanceRecordByVehicleId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[PreventiveMaintenanceRecordByVehicleDetail](
	[PreventiveMaintenanceRecordByVehicleDetailId] [int] IDENTITY(1,1) NOT NULL,
	[PreventiveMaintenanceRecordByVehicleId] [int] NOT NULL,
	[PreventiveMaintenanceCostId] [int] NOT NULL,
	[Description] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Cost] [decimal](16, 2) NOT NULL,
	[Record] [decimal](16, 2) NOT NULL,
	[InsertUserId] [int] NULL,
	[InsertDateId] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[ModifyDateId] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_PreventiveMaintenanceRecordByVehicleDetail] PRIMARY KEY CLUSTERED 
(
	[PreventiveMaintenanceRecordByVehicleDetailId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
ALTER TABLE [General].[PreventiveMaintenanceCatalog]  WITH CHECK ADD  CONSTRAINT [FK_PreventiveMaintenanceCatalog_Customers] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])
GO
ALTER TABLE [General].[PreventiveMaintenanceCatalog] CHECK CONSTRAINT [FK_PreventiveMaintenanceCatalog_Customers]
GO
ALTER TABLE [General].[PreventiveMaintenanceCatalog]  WITH CHECK ADD  CONSTRAINT [FK_PreventiveMaintenanceCatalog_Customers1] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])
GO
ALTER TABLE [General].[PreventiveMaintenanceCatalog] CHECK CONSTRAINT [FK_PreventiveMaintenanceCatalog_Customers1]
GO
ALTER TABLE [General].[PreventiveMaintenanceByVehicle]  WITH CHECK ADD  CONSTRAINT [FK_PreventiveMaintenanceByVehicle_PreventiveMaintenanceCatalog] FOREIGN KEY([PreventiveMaintenanceCatalogId])
REFERENCES [General].[PreventiveMaintenanceCatalog] ([PreventiveMaintenanceCatalogId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [General].[PreventiveMaintenanceByVehicle] CHECK CONSTRAINT [FK_PreventiveMaintenanceByVehicle_PreventiveMaintenanceCatalog]
GO
ALTER TABLE [General].[PreventiveMaintenanceByVehicle]  WITH CHECK ADD  CONSTRAINT [FK_PreventiveMaintenanceByVehicle_Vehicles] FOREIGN KEY([VehicleId])
REFERENCES [General].[Vehicles] ([VehicleId])
GO
ALTER TABLE [General].[PreventiveMaintenanceByVehicle] CHECK CONSTRAINT [FK_PreventiveMaintenanceByVehicle_Vehicles]
GO
ALTER TABLE [General].[PreventiveMaintenanceCost]  WITH CHECK ADD  CONSTRAINT [FK_PreventiveMaintenanceCost_PreventiveMaintenanceCatalogId] FOREIGN KEY([PreventiveMaintenanceCatalogId])
REFERENCES [General].[PreventiveMaintenanceCatalog] ([PreventiveMaintenanceCatalogId])
GO
ALTER TABLE [General].[PreventiveMaintenanceCost] CHECK CONSTRAINT [FK_PreventiveMaintenanceCost_PreventiveMaintenanceCatalogId]
GO
ALTER TABLE [General].[PreventiveMaintenanceRecordByVehicle]  WITH CHECK ADD  CONSTRAINT [FK_PreventiveMaintenanceRecordByVehicle_PreventiveMaintenanceRecordByVehicle] FOREIGN KEY([PreventiveMaintenanceRecordByVehicleId])
REFERENCES [General].[PreventiveMaintenanceRecordByVehicle] ([PreventiveMaintenanceRecordByVehicleId])
GO
ALTER TABLE [General].[PreventiveMaintenanceRecordByVehicle] CHECK CONSTRAINT [FK_PreventiveMaintenanceRecordByVehicle_PreventiveMaintenanceRecordByVehicle]
GO
ALTER TABLE [General].[PreventiveMaintenanceRecordByVehicle]  WITH CHECK ADD  CONSTRAINT [FK_PreventiveMaintenanceRecordByVehicleDetail_PreventiveMaintenanceId] FOREIGN KEY([PreventiveMaintenanceId])
REFERENCES [General].[PreventiveMaintenanceByVehicle] ([PreventiveMaintenanceId])
GO
ALTER TABLE [General].[PreventiveMaintenanceRecordByVehicle] CHECK CONSTRAINT [FK_PreventiveMaintenanceRecordByVehicleDetail_PreventiveMaintenanceId]
GO

