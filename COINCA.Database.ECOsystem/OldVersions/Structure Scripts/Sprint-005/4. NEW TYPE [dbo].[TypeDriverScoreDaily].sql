USE [ECOsystemDev]
GO
CREATE TYPE [dbo].[TypeDriverScoreDaily] AS TABLE(
	[IdDailyScore] [int] IDENTITY(1,1),
	[KmTraveled] [float] NULL,
	[OverSpeed] [float] NULL,
	[OverSpeedAmount] [float] NULL,
	[OverSpeedDistancePercentage] [float] NULL,
	[OverSpeedAverage] [float] NULL,
	[OverSpeedWeihgtedAverage] [float] NULL,
	[StandardDeviation] [float] NULL,
	[Variance] [float] NULL,
	[VariationCoefficient] [float] NULL,
	[AverageScore] [float] NULL,
	[OverSpeedDistance] [float] NULL,
	[Date_Time] [datetime] NULL,
	[MaxOdometer] [int] NULL,
	[MinOdometer] [int] NULL,
	[Vec] [float] NULL,
	[Dt] [float] NULL,
	[De] [float] NULL,
	[Ce] [float] NULL
)
GO


