USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[EventLog]') AND type in (N'U'))
DROP TABLE [General].[EventLog]
GO
/****** Object:  Table [General].[EventLog]    Script Date: 05/27/2015 10:47:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [General].[EventLog](
	[EventLogID] [int] IDENTITY(1,1) NOT NULL,
	[TipoLog] [varchar](15) NULL,
	[Mensaje] [varchar](1000) NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[FechaLog] [datetime] NOT NULL,
 CONSTRAINT [PK_General.EventLog] PRIMARY KEY CLUSTERED 
(
	[EventLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [General].[EventLog] ADD  CONSTRAINT [DF_General.EventLog_Mensaje]  DEFAULT ('') FOR [Mensaje]
GO

ALTER TABLE [General].[EventLog] ADD  CONSTRAINT [DF_General.EventLog_Usuario]  DEFAULT ('') FOR [Usuario]
GO

ALTER TABLE [General].[EventLog] ADD  CONSTRAINT [DF_General.EventLog_FechaLog]  DEFAULT (getdate()) FOR [FechaLog]
GO


