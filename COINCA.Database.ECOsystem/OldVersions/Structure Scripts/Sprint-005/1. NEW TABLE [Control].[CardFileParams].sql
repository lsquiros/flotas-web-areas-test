USE [ECOSystemDev]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[CardFileParams]') AND type in (N'U'))
DROP TABLE [Control].[CardFileParams]
GO
/****** Object:  Table [Control].[CardFileParams]    Script Date: 06/05/2015 10:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Control].[CardFileParams](
	[CardFileParamId] [int] IDENTITY(1,1) NOT NULL,
	[CardFileParamName] [varchar](50) NOT NULL,
	[CardFileParamLenght] [int] NOT NULL,
	[Separator] [varchar](10) NULL,
	[FillChar] [varchar](10) NULL,
 CONSTRAINT [PK_Control.CardFileParams] PRIMARY KEY CLUSTERED 
(
	[CardFileParamId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('CardId', 6, NULL, '0')
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('CardNumWithSpaces', 19, '$', NULL)
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('CardHolderNameWithSpaces', 28, '# ', ' ')
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('CardHolderCompany', 27, '& ', ' ')
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('MonthWithSeparator', 2, '*', NULL)
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('YearWithSeparator', 2, '/', NULL)
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('CardNumNoSpaces', 16, '"%B', NULL)
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('LastName', 17, '^', ' ')
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('FirstName', 8, '/', ' ')
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('Year', 2, '^', NULL)
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('Month', 2, NULL, NULL)
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('FillOne', 22, NULL, '0')
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('CardNumNoSpacesTwo', 16, '?;', NULL)
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('YearTwo', 2, '=', NULL)
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('MonthTwo', 2, NULL, NULL)
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('FillTwo', 16, NULL, '0')
INSERT [Control].[CardFileParams] ([CardFileParamName], [CardFileParamLenght], [Separator], [FillChar]) VALUES ('Arrobas', 5, '?', '@')

