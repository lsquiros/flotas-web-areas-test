USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[DriversScoresDaily]') AND type in (N'U'))
DROP TABLE [Operation].[DriversScoresDaily]
GO
/****** Object:  Table [Operation].[DriversScores]    Script Date: 6/8/2015 4:49:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Operation].[DriversScoresDaily](
	[CustomerId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[ScoreYear] [int] NOT NULL,
	[ScoreMonth] [int] NOT NULL,
	[ScoreDay] [int] NOT NULL,
	[ScoreType] [varchar](5) NOT NULL,
	[Score] [float] NOT NULL,
	[KmTraveled] [float] NULL,
	[OverSpeed] [float] NULL,
	[OverSpeedAmount] [float] NULL,
	[OverSpeedDistancePercentage] [float] NULL,
	[OverSpeedAverage] [float] NULL,
	[OverSpeedWeihgtedAverage] [float] NULL,
	[StandardDeviation] [float] NULL,
	[Variance] [float] NULL,
	[VariationCoefficient] [float] NULL,
	[OverSpeedDistance] [float] NULL,
	[Date_Time] datetime,
	[MaxOdometer] [int] NULL,
	[MinOdometer]  [int] NULL,
	[Vec] [float] NULL,
	[Dt] [float] NULL,
	[De] [float] NULL,
	[Ce] [float] NULL
 CONSTRAINT [PK_DriversScoresDaily] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC,
	[UserId] ASC,
	[ScoreYear] ASC,
	[ScoreMonth] ASC,
	[ScoreDay] ASC,
	[ScoreType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO


