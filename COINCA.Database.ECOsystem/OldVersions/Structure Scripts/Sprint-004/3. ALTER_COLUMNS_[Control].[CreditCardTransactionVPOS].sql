USE [ECOSystem]
GO

ALTER TABLE [Control].[CreditCardTransactionVPOS]
ALTER COLUMN [SystemTraceNumber] nvarchar(50) null

ALTER TABLE [Control].[CreditCardTransactionVPOS]
ALTER COLUMN [ReferenceNumber] nvarchar(50) null
