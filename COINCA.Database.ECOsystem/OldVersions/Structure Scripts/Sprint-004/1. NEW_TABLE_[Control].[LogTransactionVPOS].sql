USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

/* Object:  Table [Control].[LogTransactionsVPOS]    
   Script Date: 04/14/2015 10:10:28 */

CREATE TABLE [Control].[LogTransactionsVPOS](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[CCTransactionVPOSId] [int] NOT NULL,
	[ResponseCode] [varchar](250) NULL,
	[ResponseCodeDescription] [nvarchar](Max) NULL,
	[Message] [nvarchar](Max) NULL,
	[IsSuccess] [bit] Default 0,
	[IsTimeOut] [bit] Default 0,
	[IsCommunicationError] [bit] Default 0,
	[InsertDate] [datetime] NULL,
	[InsertUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_LogId] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [Control].[LogTransactionsVPOS]  WITH CHECK ADD  CONSTRAINT [FK_LogTransactionsVPOS_CreditCardTransactionVPOS] FOREIGN KEY([CCTransactionVPOSId])
REFERENCES [Control].[CreditCardTransactionVPOS] ([CreditCardTransactionVPOS])
GO

ALTER TABLE [Control].[LogTransactionsVPOS] CHECK CONSTRAINT [FK_LogTransactionsVPOS_CreditCardTransactionVPOS]
GO

ALTER TABLE [Control].[LogTransactionsVPOS] ADD  DEFAULT ((0)) FOR [IsSuccess]
GO

ALTER TABLE [Control].[LogTransactionsVPOS] ADD  DEFAULT ((0)) FOR [IsTimeOut]
GO

ALTER TABLE [Control].[LogTransactionsVPOS] ADD  DEFAULT ((0)) FOR [IsCommunicationError]
GO

