use [ECOSystem]
go

ALTER TABLE [Control].[Transactions]
ADD [IsVoid] bit null

ALTER TABLE [Control].[TransactionsHx]
ADD [IsVoid] bit null