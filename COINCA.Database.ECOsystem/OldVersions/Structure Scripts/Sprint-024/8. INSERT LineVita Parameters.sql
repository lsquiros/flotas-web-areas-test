-- Insert LineVita CR Partner Parameter /////////////////////////////////
INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('LineVitaPartner'
			,'CRC'
			,'LineVita ECO Costa Rica'
			,''
			,0)

GO

-- Insert DEFAULT ROLE FOR LineVita CR Partner //////////////////////////
INSERT INTO [dbo].[GeneralParameters]
           ([ParameterID]
           ,[Property]
           ,[Description]
           ,[Value]
           ,[NumericValue])
     VALUES
			('LineVitaRole'
			,'ROLE'
			,'LineVita ECO Customer Role Name'
			,'LINEVITA_ADMIN'
			,0)
GO

-- Insert DEFAULT PARTNER FOR LineVita CR Partner ///////////////////////
INSERT INTO [General].[Partners]
           ([Name]
           ,[CountryId]
           ,[Logo]
           ,[ApiUserName]
           ,[ApiPassword]
           ,[PartnerGroupId]
           ,[IsDeleted]
           ,[InsertDate]
           ,[InsertUserId]
           ,[ModifyDate]
           ,[ModifyUserId]
           ,[CapacityUnitId]
           ,[PartnerTypeId]
           ,[FuelErrorPercent]
           ,[StationImage])
     VALUES
           ('LineVita ECO Costa Rica'
           ,3
           ,''
           ,'J5ZVQu7kp6Nnvfv4LwKsZvrMAr5Qioph'
           ,'J5ZVQu7kp6NV5xxPFpchG/XWGMrLVm7p'
           ,1
           ,0
           ,GETDATE()
           ,35
           ,GETDATE()
           ,35
           ,0
           ,800
           ,10
           ,NULL)

GO

DECLARE @LAST_IDENTITY_PARTNER INT = SCOPE_IDENTITY()

UPDATE [dbo].[GeneralParameters]
SET [NumericValue] = @LAST_IDENTITY_PARTNER,
    [Value] = @LAST_IDENTITY_PARTNER
WHERE [ParameterID] = 'LineVitaPartner' AND [Property] = 'CRC'

GO
--////////////////////////////////////////////////////////////////7