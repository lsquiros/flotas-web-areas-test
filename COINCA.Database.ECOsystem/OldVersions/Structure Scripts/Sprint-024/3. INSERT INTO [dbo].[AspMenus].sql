--Insertion of menus data for the LineVita APP
INSERT INTO [dbo].[AspMenus] VALUES('Geocercas', '~/Content/Images/menu_geofences.png', NULL, NULL, 18, 1, GETDATE())
INSERT INTO [dbo].[AspMenus] VALUES('Creación de Geocercas', '', 'D9509620-AEDB-417F-8320-99D354A72DB5', 127, 18, 2, GETDATE())
INSERT INTO [dbo].[AspMenus] VALUES('Vehículo asociado a la Geocerca', '', 'DE790515-4857-4080-80C9-058C6F9A6683', 127, 18, 3, GETDATE())
INSERT INTO [dbo].[AspMenus] VALUES('Ubicación y Localización', '', NULL, NULL, 18, 7, GETDATE())
INSERT INTO [dbo].[AspMenus] VALUES('Vehículo en Tiempo Real', '', 'DB1D9632-7813-4C40-86C2-47EE85ED35DE', 130, 18, 8, GETDATE())
INSERT INTO [dbo].[AspMenus] VALUES('Reconstrucción de Recorrido', '', 'E0FCEDC8-189F-441F-A7BB-7A83380B32A1', 130, 18, 9, GETDATE())
INSERT INTO [dbo].[AspMenus] VALUES('Nota de Conducción', '~/Content/Images/menu_reports.png', NULL, NULL, 19, 1, GETDATE())
INSERT INTO [dbo].[AspMenus] VALUES('Conducción Velocidad de Ley', '', '095FC1C1-91DE-4BBD-96BA-63EE13188E66', 133, 19, 2, GETDATE())