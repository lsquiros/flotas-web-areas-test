USE [ECOsystem]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Control].[LogTransactionsPOS](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[CCTransactionPOSId] [int] NULL,
	[ResponseCode] [varchar](250) NULL,
	[ResponseCodeDescription] [nvarchar](max) NULL,
	[Message] [nvarchar](max) NULL,
	[TransportData] [nvarchar](max) NULL,
	[IsSuccess] [bit] NULL,
	[IsFail] [bit] NULL,
	[InsertDate] [datetime] NULL,
	[InsertUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_LogTransactionId] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [Control].[LogTransactionsPOS] ADD  DEFAULT ((0)) FOR [IsSuccess]
GO

ALTER TABLE [Control].[LogTransactionsPOS] ADD  DEFAULT ((0)) FOR [IsFail]
GO

--ALTER TABLE [Control].[LogTransactionsPOS]  WITH CHECK ADD  CONSTRAINT [FK_LogTransactionsPOS_Transactions] FOREIGN KEY([CCTransactionPOSId])
--REFERENCES [Control].[Transactions] ([TransactionId])
--GO[FK_LogTransactionsPOS_Transactions]

--ALTER TABLE [Control].[LogTransactionsPOS] CHECK CONSTRAINT [FK_LogTransactionsPOS_Transactions]
GO


