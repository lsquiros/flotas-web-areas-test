USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[GetGeographyValue]') AND type IN (N'FN', N'IF', N'TF'))--type in (N'P', N'PC'))
	DROP FUNCTION [General].[GetGeographyValue]
GO

/****** Object:  UserDefinedFunction [General].[GetGeographyValue]    Script Date: 8/11/2015 4:03:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sergio Monge
-- Create date: 10/07/2015
-- Description:	Convert the geometric value to geography value
-- =============================================
CREATE FUNCTION [General].[GetGeographyValue] 
(
	@polygon geometry
)
RETURNS geography
AS
BEGIN

	BEGIN TRY

		DECLARE @geog geography
		DECLARE @geom geometry

		set @geom = geometry::STPolyFromText(@polygon.STAsText(),4326) 
	
		SET @geom = @geom.STUnion(@geom.STStartPoint()); 

		set @geog = geography::STGeomFromWKB(@geom.STAsBinary(), @geom.STSrid);

		RETURN @geog

	END TRY
	BEGIN CATCH

		SET @geog = CONVERT(GEOGRAPHY,'POINT (0 0)') -- SET RETURN DEFAULT VALUE 

		RETURN @geog

	END CATCH
END

GO


