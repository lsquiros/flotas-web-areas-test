USE [ECOsystemDev]
GO

/****** Object:  Table [Control].[Transactions]    Script Date: 8/17/2015 11:49:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--DROP TABLE [Control].[TransactionsRules]
SET ANSI_PADDING ON
GO

CREATE TABLE [Control].[TransactionsRules](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RuleName] [varchar](150) NULL,
	[RuleDescription] [varchar](150) NULL,
	[RuleType] [int], -- 0 CheckBox, 1 Value
	[IsActive] [bit] DEFAULT(1),
	[InsertDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_TransactionsRules] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

	--[InvalidCreditCard] [bit] NULL,
	--[StatusCreditCard] [bit] NULL,
	--[ExpirationCreditCard] [bit] NULL,
	--[BalanceCreditCard] [bit] NULL,
	--[PlateCreditCard] [bit] NULL,
	--[LitersCapacityCreditCard] [bit] NULL,
	--[TimeSlotCreditCard] [bit] NULL,
	--[PriceByLiterCreditCard] [bit] NULL,
	--[OdometerCreditCard] [bit] NULL,
	--[DailyTransactionLimit] [bit] NULL,
	--[DriverIdCreditCard] [bit] NULL,

GO

SET ANSI_PADDING OFF
GO
