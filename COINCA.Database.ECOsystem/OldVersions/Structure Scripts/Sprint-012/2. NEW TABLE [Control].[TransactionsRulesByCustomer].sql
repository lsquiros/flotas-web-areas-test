USE [ECOsystemDev]
GO

/****** Object:  Table [Control].[Transactions]    Script Date: 8/17/2015 11:49:15 AM ******/
SET ANSI_NULLS ON
GO
--DROP TABLE [Control].[TransactionsRulesByCustomer]
SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Control].[TransactionsRulesByCustomer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[RuleId] [int] NOT NULL,
	[RuleActive] [bit]	DEFAULT(0),	
	[RuleValue] FLOAT, 
	[InsertDate] [datetime] NULL,
	[InsertUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_TransactionsRulesByCustomer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

	--[InvalidCreditCard] [bit] NULL,
	--[StatusCreditCard] [bit] NULL,
	--[ExpirationCreditCard] [bit] NULL,
	--[BalanceCreditCard] [bit] NULL,
	--[PlateCreditCard] [bit] NULL,
	--[LitersCapacityCreditCard] [bit] NULL,
	--[TimeSlotCreditCard] [bit] NULL,
	--[PriceByLiterCreditCard] [bit] NULL,
	--[OdometerCreditCard] [bit] NULL,
	--[DailyTransactionLimit] [bit] NULL,
	--[DriverIdCreditCard] [bit] NULL,

GO

SET ANSI_PADDING OFF
GO
