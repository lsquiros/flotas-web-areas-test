USE [ECOsystemDev]
GO
	
	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('VehiclePlateValidate'
           ,'Validar placas de veh�culos no registradas en el sistema.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))

	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('DriverCodeCreditCard'
           ,'Validar la tarjeta vinculada al c�digo de un conductor.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))


	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('InvalidCreditCard'
           ,'Validar si la tarjeta de cr�dito es v�lida.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))


	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('StatusCreditCard'
           ,'Validar el estado de la tarjeta de cr�dito.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))


	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('ExpirationCreditCard'
           ,'Validar la fecha de expiraci�n de la tarjeta de cr�dito.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))


	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('BalanceCreditCard'
           ,'Validar el cr�dito disponible de una tarjeta.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))


	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('PlateCreditCard'
           ,'Validar la tarjeta vinculada a la placa de un veh�culo.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))


	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('LitersCapacityCreditCard'
           ,'Validar la capacidad de litros de un veh�culo contra la transacci�n.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))


	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('TimeSlotCreditCard'
           ,'Validar si el veh�culo se encuentra en la franja horaria para una transacci�n.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))


	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('PriceByLiterCreditCard'
           ,'Validar si el monto de la transacci�n no coincide con la cantidad de litros de combustible.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))


	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('OdometerCreditCard'
           ,'Validar si el od�metro est� fuera del rango permitido.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))


	INSERT INTO [Control].[TransactionsRules]
           ([RuleName]
           ,[RuleDescription]
           ,[RuleType]
           ,[InsertDate]
           ,[ModifyDate])
     VALUES
           ('DailyTransactionLimit'
           ,'Validar si excedi� el limite diario de transacciones.'
           ,0
           ,DATEADD(hour, -6, GETDATE())
           ,DATEADD(hour, -6, GETDATE()))
GO


