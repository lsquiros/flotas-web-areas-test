USE [ECOsystemDev]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Saltos_AVL](
    [Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Device] [int] NOT NULL,
	[GPSDateTime] [datetime] NOT NULL,
	[RTCDateTime] [datetime] NOT NULL,
	[RepDateTime] [datetime] NOT NULL,
	[SvrDateTime] [datetime] NOT NULL,
	[Longitude] [float] NOT NULL,
	[Latitude] [float] NOT NULL,
	[Heading] [int] NOT NULL,
	[ReportID] [int] NOT NULL,
	[Odometer] [float] NOT NULL,
	[HDOP] [float] NOT NULL,
	[InputStatus] [tinyint] NOT NULL,
	[OutputStatus] [tinyint] NOT NULL,
	[VSSSpeed] [float] NOT NULL,
	[AnalogInput] [float] NOT NULL,
	[DriverID] [varchar](16) NULL,
	[Temperature1] [float] NULL,
	[Temperature2] [float] NULL,
	[MCC] [int] NULL,
	[MNC] [int] NULL,
	[LAC] [int] NULL,
	[CellID] [int] NULL,
	[RXLevel] [int] NULL,
	[GSMQuality] [int] NULL,
	[GSMStatus] [int] NULL,
	[Satellites] [int] NULL,
	[RealTime] [bit] NULL,
	[MainVolt] [float] NULL,
	[BatVolt] [float] NULL,
	[timestamp] [timestamp] NOT NULL,
CONSTRAINT [PK_Saltos_AVL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


GO


