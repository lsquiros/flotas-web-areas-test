CREATE TABLE [General].[Emails_Notification]
(
	[ID_Email]		INT NOT NULL IDENTITY(1,1) 
	,[Email]			VARCHAR(50) NOT NULL 
	,[PartnerId]		INT NULL 
	,[Activo]		BIT 
	,[TypeId]		INT NULL
	 
    CONSTRAINT PK_Email PRIMARY KEY (ID_Email)
)
GO