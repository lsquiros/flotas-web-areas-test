USE [ECOsystemDev]
GO

/****** Object:  Table [General].[Terminal]    Script Date: 11/3/2015 9:22:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [General].[Terminal](
	[CustomerId] [int] NOT NULL,
	[TerminalId] [varchar](250) NOT NULL,
	[MerchantDescription] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC,
	[TerminalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [General].[Terminal]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Terminal] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])
GO

ALTER TABLE [General].[Terminal] CHECK CONSTRAINT [FK_Customer_Terminal]
GO


