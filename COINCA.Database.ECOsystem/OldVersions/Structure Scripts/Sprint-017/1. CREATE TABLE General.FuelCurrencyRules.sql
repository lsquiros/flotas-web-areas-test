USE [ECOsystemDev] 

go 

/****** Object:  Table [Control].[TransactionsRules]    Script Date: 10/23/2015 8:52:02 AM ******/ 
SET ansi_nulls ON 

go 

SET quoted_identifier ON 

go 

SET ansi_padding ON 

go 

CREATE TABLE General.FuelCurrencyRules 
  ( 
     [Id]              [INT] IDENTITY(1, 1) NOT NULL, 
     [RuleName]        [VARCHAR](150) NULL, 
     [RuleDescription] [VARCHAR](150) NULL, 
     [IsActive]        [BIT] NULL, 
     [RowVersion]      [TIMESTAMP] NOT NULL, 
     CONSTRAINT [PK_FuelCurrencyRules] PRIMARY KEY CLUSTERED ( [Id] ASC )WITH ( 
     pad_index = OFF, statistics_norecompute = OFF, ignore_dup_key = OFF, 
     allow_row_locks = on, allow_page_locks = on) 
  ) 

go 

SET ansi_padding OFF 

go 

ALTER TABLE General.FuelCurrencyRules 
  ADD DEFAULT ((1)) FOR [IsActive] 

go 