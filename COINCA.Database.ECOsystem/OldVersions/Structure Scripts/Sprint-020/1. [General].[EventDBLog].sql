
USE [ECOsystemDev]
GO

/****** Object:  Table [General].[EventLog]    Script Date: 12/3/2015 5:48:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [General].[EventDBLog](
	[EventDBId] [int] IDENTITY(1,1) NOT NULL,
	[ProcessName] [nvarchar](500) NULL,
	[Message] [nvarchar](max) NULL,
	[IsInfo] [bit] NOT NULL DEFAULT ((0)),
	[IsWarning] [bit] NOT NULL DEFAULT ((0)),
	[IsError] [bit] NOT NULL DEFAULT ((0)),
	[LogDateTime] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_General.EventDBLogId] PRIMARY KEY CLUSTERED 
(
	[EventDBId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO


