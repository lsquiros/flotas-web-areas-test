USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK__Performan__Vehic__6AEFE058]') AND parent_object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]'))
ALTER TABLE [General].[PerformanceByVehicle] DROP CONSTRAINT [FK__Performan__Vehic__6AEFE058]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_PerformanceByVehicle_Transactions]') AND parent_object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]'))
ALTER TABLE [General].[PerformanceByVehicle] DROP CONSTRAINT [FK_PerformanceByVehicle_Transactions]
GO

USE [ECOSystem]
GO

/****** Object:  Table [General].[PerformanceByVehicle]    Script Date: 03/04/2015 15:29:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[PerformanceByVehicle]') AND type in (N'U'))
DROP TABLE [General].[PerformanceByVehicle]
GO

USE [ECOSystem]
GO

/****** Object:  Table [General].[PerformanceByVehicle]    Script Date: 03/04/2015 15:29:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [General].[PerformanceByVehicle](
	[PerformanceVehicleId] [int] IDENTITY(1,1) NOT NULL,
	[VehicleId] [int] NOT NULL,
	[TransactionId] [int] NOT NULL,
	[TrxDate] [datetime] NOT NULL,
	[TrxPreviousDate] [datetime] NULL,
	[TrxReportedLiters] [decimal](16, 2) NOT NULL,
	[TrxPreviousOdometer] [decimal](16, 2) NULL,
	[TrxReportedOdometer] [decimal](16, 2) NOT NULL,
	[TrxPerformance] [decimal](16, 2) NULL,
	[GpsPreviousOdometer] [decimal](16, 2) NULL,
	[GpsReportedOdometer] [decimal](16, 2) NULL,
	[GpsPerformance] [decimal](16, 2) NULL,
	[GpsHoursOn] [decimal](16, 2) NULL,
	[GpsHoursOnStr] [varchar](100) NULL,
	[InsertUserId] [int] NULL,
	[InsertDate] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK__Performa__0C517EAC160F4887] PRIMARY KEY CLUSTERED 
(
	[PerformanceVehicleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [General].[PerformanceByVehicle]  WITH CHECK ADD  CONSTRAINT [FK__Performan__Vehic__6AEFE058] FOREIGN KEY([VehicleId])
REFERENCES [General].[Vehicles] ([VehicleId])
GO

ALTER TABLE [General].[PerformanceByVehicle] CHECK CONSTRAINT [FK__Performan__Vehic__6AEFE058]
GO

ALTER TABLE [General].[PerformanceByVehicle]  WITH CHECK ADD  CONSTRAINT [FK_PerformanceByVehicle_Transactions] FOREIGN KEY([TransactionId])
REFERENCES [Control].[Transactions] ([TransactionId])
GO

ALTER TABLE [General].[PerformanceByVehicle] CHECK CONSTRAINT [FK_PerformanceByVehicle_Transactions]
GO


