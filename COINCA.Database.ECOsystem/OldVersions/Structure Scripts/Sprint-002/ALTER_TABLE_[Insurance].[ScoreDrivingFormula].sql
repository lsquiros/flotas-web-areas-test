USE [ECOSystem]
GO

/****** Object:  Table [Operation].[DriversScores]    Script Date: 02/18/2015 14:42:28 ******/
SET ANSI_NULLS ON
GO

/****** Object:  Table [Operation].[DriversScores]    Script Date: 02/18/2015 14:42:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[ScoreDrivingFormula]') AND type in (N'U'))
DROP TABLE [Insurance].[ScoreDrivingFormula]
GO

USE [ECOSystem]
GO

/****** Object:  Table [Operation].[DriversScores]    Script Date: 02/18/2015 14:42:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Insurance].[ScoreDrivingFormula](
	   ScoreDrivingFormulaId int primary key identity(1,1),
	   Name varchar(300) not null,
	   ScoreDrivingFormulaStr varchar(1000) not null,
       ScoreDrivingFormulaSP varchar(300) null,
	   IsActive bit null,
	   IsPending bit null,
	   IsApproved bit null,
	   CustomerId int not null,
	   [InsertDate] [datetime] NULL,
	   [InsertUserId] [int] NULL,
	   [ModifyDate] [datetime] NULL,
	   [ModifyUserId] [int] NULL,
	   [RowVersion] [timestamp] NOT NULL
)
GO


