USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[General].[FK_HobbsMeterByVehicle_Vehicles]') AND parent_object_id = OBJECT_ID(N'[General].[HobbsMeterByVehicle]'))
ALTER TABLE [General].[HobbsMeterByVehicle] DROP CONSTRAINT [FK_HobbsMeterByVehicle_Vehicles]
GO

USE [ECOSystem]
GO

/****** Object:  Table [General].[HobbsMeterByVehicle]    Script Date: 03/04/2015 15:28:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[HobbsMeterByVehicle]') AND type in (N'U'))
DROP TABLE [General].[HobbsMeterByVehicle]
GO

USE [ECOSystem]
GO

/****** Object:  Table [General].[HobbsMeterByVehicle]    Script Date: 03/04/2015 15:28:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [General].[HobbsMeterByVehicle](
	[HobbsMeterVehicleId] [int] IDENTITY(1,1) NOT NULL,
	[VehicleId] [int] NOT NULL,
	[ProcessedDate] [date] NOT NULL,
	[GpsHoursOn] [decimal](16, 4) NULL,
	[GpsHoursOnStr] [varchar](100) NULL,
	[GpsStartOdometer] [decimal](16, 2) NULL,
	[GpsEndOdometer] [decimal](16, 2) NULL,
	[InsertUserId] [int] NULL,
	[InsertDate] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_HobbsMeterByVehicle] PRIMARY KEY CLUSTERED 
(
	[HobbsMeterVehicleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [General].[HobbsMeterByVehicle]  WITH CHECK ADD  CONSTRAINT [FK_HobbsMeterByVehicle_Vehicles] FOREIGN KEY([VehicleId])
REFERENCES [General].[Vehicles] ([VehicleId])
GO

ALTER TABLE [General].[HobbsMeterByVehicle] CHECK CONSTRAINT [FK_HobbsMeterByVehicle_Vehicles]
GO


