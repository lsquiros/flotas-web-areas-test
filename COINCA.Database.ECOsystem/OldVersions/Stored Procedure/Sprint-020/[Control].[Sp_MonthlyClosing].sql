USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MonthlyClosing]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_MonthlyClosing]
GO

/****** Object:  StoredProcedure [Control].[Sp_MonthlyClosing]    Script Date: 12/31/2015 1:11:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================
-- Author:		Gerald Solano
-- Create date: 23/12/2015
-- Description:	Execute tasks of the Monthly Closing
-- ===============================================================
CREATE PROCEDURE [Control].[Sp_MonthlyClosing]
AS 
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0 
        DECLARE @lCurrentMonth INT
        DECLARE @lCurrentYear INT
        DECLARE @lClosingMonth INT
        DECLARE @lClosingYear INT
        DECLARE @lProcessDate DATETIME		
        DECLARE @lCount INT	
        
		DECLARE @To NVARCHAR(500) = 'gdelgado@coinca.tv;gsolano@coinca.tv'
		DECLARE @From NVARCHAR(100) = 'intrack-eco@coinca.tv'
		DECLARE @Subject NVARCHAR(300) = NULL
		DECLARE @Message NVARCHAR(1000) = NULL
		

        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
	
		DECLARE @CustomerId INT = 0
		DECLARE @ONLY_ONE_Customer BIT = 0 -- 1 = SE REALIZA EL PROCESO SOLO A UN CUSTOMER
		DECLARE @DELETE_DATA_Customer BIT = 0 -- 1 = SE REALIZA EL PROCESO DE BORRADO DEL MES ACTUAL

		-- VALIDA EL MES AL CUAL RESTAURAR LOS DISPONIBLES
		Set @lProcessDate = GETDATE()    --'2015-11-01'--
		Set @lCurrentMonth = DATEPART(MM, @lProcessDate)
		Set @lCurrentYear = DATEPART(YY, @lProcessDate)

		-- OBTENEMOS EL MES ANTERIOR CON BASE AL ACTUAL
		IF @lCurrentMonth = 1
		BEGIN
			Set @lClosingMonth = 12
			Set @lClosingYear = @lCurrentYear - 1
		END
		ELSE
		BEGIN
			Set @lClosingMonth = @lCurrentMonth - 1
			Set @lClosingYear = @lCurrentYear
		END

		-- VALIDA SI EL PROCESO ES PARA M�S DE UN CUSTOMER
		IF(@ONLY_ONE_Customer IS NULL OR @ONLY_ONE_Customer = 0)
		BEGIN
			SELECT @CustomerId = MIN(CustomerId) FROM General.Customers
			WHERE [IsDeleted] IS NULL OR [IsDeleted] = 0
		END

		-- RECORRE UNO O M�S CUSTOMERS
		WHILE @CustomerId IS NOT NULL   -- INICIA WHILE CUSTOMERS
		BEGIN 		
			--SELECT @lCount

			-- DELETE DATA FOR CURRENT MONTH(verifica si hayq ue borrar datos del mes actual) --------------------------------------
			IF(@DELETE_DATA_Customer IS NOT NULL AND @DELETE_DATA_Customer = 1)
			BEGIN 
				DELETE FROM [Control].[FuelsByCredit]
				WHERE [Year] = @lCurrentYear AND 
						[Month] = @lCurrentMonth AND 
						[CustomerId] = @CustomerId

				DELETE FROM [Control].[VehicleFuel]
				WHERE [VehicleId] IN (
					SELECT V.[VehicleId] 
					FROM [Control].[VehicleFuel] VF
					INNER JOIN [General].[Vehicles] V 
						ON VF.VehicleId = V.VehicleId
					WHERE VF.[Year] = @lCurrentYear AND 
							VF.[Month] = @lCurrentMonth AND
							V.[CustomerId] = @CustomerId
					) AND [Year] = @lCurrentYear AND [Month] = @lCurrentMonth

				DELETE FROM [Control].[CustomerCredits]
				WHERE [Year] = @lCurrentYear AND 
						[Month] = @lCurrentMonth AND
						[CustomerId] = @CustomerId

				-- AGREGAMOS AL LOG DE BASE DE DATOS
				SET @Message = 'C�digo de Alerta(DLT_Customer_001) En el proceso de inicio de Mes el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' se ha borrado satisfactoriamente los datos del mes y a�o actual.'

				EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'
			END 

			-- PROCESS NEW DATA FOR CURRENT MONTH ------------------------------------
		
			-- GET CustomerCredit REGISTERS
			SELECT @lCount = COUNT(1)
			FROM [Control].[CustomerCredits]
			WHERE [Year] = @lCurrentYear AND 
					[Month] = @lCurrentMonth AND
					[CustomerId] = @CustomerId
		
			IF @lCount = 0 -- inicio IF CustomerCredit
			BEGIN
				--SELECT @CustomerId

				--Copy CustomerCredit data for the Current Month		
				Insert into [Control].[CustomerCredits]
					(CustomerId, Year, Month, CreditAmount, CreditAssigned, CreditTypeId, InsertDate)
				Select CustomerId, @lCurrentYear, @lCurrentMonth, CreditAmount, 0, CreditTypeId, @lProcessDate
					from Control.CustomerCredits 
					where Year = @lClosingYear
					and Month = @lClosingMonth 
					and [CustomerId] = @CustomerId
			
				if(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se env�a notificaci�n
				BEGIN
					SET @Subject = 'Credito de Cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' - No se Actualiz�'
					SET @Message = 'C�digo de Alerta(UPDT_Customer_001) En el proceso de inicio de Mes el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' no se pudo actualizar. Por favor realizar la revisi�n respectiva.'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'

					-- ENVIAMOS NOTIFICACIONES
					Exec General.Sp_Email_AddOrEdit null, @To, null, null, @Subject, @Message, 0, 0, null, null 
				END
				ELSE BEGIN
					SET @Message = 'C�digo de Alerta(UPDT_Customer_001) En el proceso de inicio de Mes el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +', el Credito de Cliente se actualiz� correctamente.'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
				END 
				-------------------------------------------------
			
				--There is a Card but no record in CustomerCredit
				Insert into [Control].[CustomerCredits]
					(CustomerId, Year, Month, CreditAmount, CreditAssigned, CreditTypeId, InsertDate)
				Select CCC.CustomerId, @lCurrentYear, @lCurrentMonth, CCC.CreditLimit, 0, 1000, @lProcessDate
					from General.CustomerCreditCards CCC 
					inner join (Select CustomerID, Max(CustomerCreditCardId) as CustomerCreditCardId 
								from General.CustomerCreditCards
								where StatusId in (100, 101)
								group by CustomerID) CLast
					on CCC.CustomerId = CLast.CustomerId and 
						--CCC.CustomerCreditCardId = CLast.CustomerCreditCardId
						CLast.CustomerCreditCardId = @CustomerId
					where not exists (Select C.CustomerId
										from Control.CustomerCredits C
									where C.CustomerId = @CustomerId
										and C.Year = @lCurrentYear
										and C.Month = @lCurrentMonth)
				-----------------------------------------------------

				--Update Credit Assigned for CustomerCredit
				Update C
					set C.CreditAssigned = IsNull(CCards.SumCreditLimit, 0) 
					from Control.CustomerCredits C
					left join (Select CC.CustomerId, SUM(CC.CreditLimit) as SumCreditLimit
								from Control.CreditCard CC
								where [StatusId] in (0, 2, 3, 4, 6, 8) AND
									[CustomerId] = @CustomerId
								group by CC.CustomerId) CCards
					on C.CustomerId = CCards.CustomerId 
					where C.Year = @lCurrentYear
					and C.Month = @lCurrentMonth
					and C.[CustomerId] = @CustomerId

				if(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se env�a notificaci�n
				BEGIN
					SET @Subject = 'Actualizaci�n de Credito de Cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' - No se Actualiz� Cr�dito Asignado'
					SET @Message = 'C�digo de Alerta(UPDT_Customer_002) En el proceso de inicio de Mes para el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' no hubo actualizaci�n de los montos asignados a las tarjetas del Cliente. Por favor realizar la revisi�n respectiva.'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'

					-- ENVIAMOS NOTIFICACIONES
					Exec General.Sp_Email_AddOrEdit null, @To, null, null, @Subject, @Message, 0, 0, null, null 
				END
				ELSE BEGIN
					SET @Message = 'C�digo de Alerta(UPDT_Customer_002) En el proceso de inicio de Mes el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' se actualiz� correctamente los montos asignados a las tarjetas del Cliente.'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
				END 
				----------------------------------------------------------

			END -- fin de IF CustomerCredit

			-- GET FuelsByCredit REGISTERS
			Select @lCount = COUNT(1)
				from Control.FuelsByCredit
				where Year = @lCurrentYear
				and Month = @lCurrentMonth
				and [CustomerId] = @CustomerId

			IF @lCount = 0 --- inicio IF FuelsByCredit
			BEGIN    
				--Copy FuelsByCredit data for the Current Month
				Insert into Control.FuelsByCredit
					(CustomerId, FuelId, Month, Year, Total, Assigned, InsertDate)
				Select CustomerId, FuelId, @lCurrentMonth, @lCurrentYear, Total, 0, @lProcessDate
					from Control.FuelsByCredit
					where Year = @lClosingYear
					and Month = @lClosingMonth
					and [CustomerId] = @CustomerId


				if(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se env�a notificaci�n
				BEGIN
					SET @Subject = 'Actualizaci�n de Credito de Combustible para Cliente con Cod.'+ CAST(@CustomerId AS varchar(20))
					SET @Message = 'C�digo de Alerta(UPDT_Customer_003) En el proceso de inicio de Mes para el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' no hubo actualizaci�n de los montos del combustible asignado al Cliente. Por favor realizar la revisi�n respectiva.'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'

					-- ENVIAMOS NOTIFICACIONES
					Exec General.Sp_Email_AddOrEdit null, @To, null, null, @Subject, @Message, 0, 0, null, null 
				END
				ELSE BEGIN
					SET @Message = 'C�digo de Alerta(UPDT_Customer_003) En el proceso de inicio de Mes el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' se actualiz� correctamente los montos del combustible asignado al Cliente.'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
				END 
				----------------------------------------------------------
			END--- fin IF FuelsByCredit

			-- GET VehicleFuel REGISTERS
			Select @lCount = COUNT(1)
				from Control.VehicleFuel VF
				INNER JOIN [General].Vehicles V 
				ON VF.VehicleId = V.VehicleId
				where VF.Year = @lCurrentYear
				and VF.Month = @lCurrentMonth
				and V.[CustomerId] = @CustomerId
		   
			IF @lCount = 0
			BEGIN    
				--Copy VehicleFuel data for the Current Month
				DECLARE @VehicleFuelTemp TABLE( 
					VehicleId int
				)

				--- TABLA TEMPORAL PARA OBTENER LOS DISTINCT DE LOS VEHICLESID DEL MES ANTERIOR
				INSERT INTO @VehicleFuelTemp (VehicleId)
				SELECT Distinct VF.VehicleId FROM [Control].[VehicleFuel] VF
				INNER JOIN [General].Vehicles V ON VF.VehicleId = V.VehicleId
				WHERE VF.[Year] = @lClosingYear AND	VF.[Month] = @lClosingMonth AND	V.[CustomerId] = @CustomerId

				--- RECORRMOS LA TABLA TEMPORAL PARA OBTENER LOS VEHICLE FUEL CON BASE A LA TABLA TEMP
				DECLARE @IndexVehicleId INT = 0
				DECLARE @IndexVehicleIdFuel INT = 0
				DECLARE @IndexYear INT = 0

				SELECT @IndexVehicleId = MIN(VehicleId)
				FROM @VehicleFuelTemp

				WHILE(@IndexVehicleId IS NOT NULL) --INICIA WHILE
				BEGIN
				
					-- OBTENEMOS EL DE MAYOR LITROS ASIGNADO
					SELECT TOP 1 @IndexVehicleIdFuel = [VehicleFuelId]
					FROM [Control].[VehicleFuel]
					WHERE [VehicleId] = @IndexVehicleId AND
							[Year] = @lClosingYear AND 
							[Month] = @lClosingMonth
					ORDER BY [Liters] DESC

					INSERT INTO [Control].[VehicleFuel] (VehicleId, Month, Year, Liters, Amount, InsertDate)
					SELECT DISTINCT VF.VehicleId, @lCurrentMonth, @lCurrentYear, 
							VF.Liters, 
							CASE WHEN PF.LiterPrice is null 
							THEN VF.Amount ELSE CAST((VF.Liters * PF.LiterPrice) AS NUMERIC(16,2)) END, 
							@lProcessDate
						FROM [Control].[VehicleFuel] VF
						inner join General.Vehicles V on VF.VehicleId = V.VehicleId
						inner join General.VehicleCategories VC on V.VehicleCategoryId = VC.VehicleCategoryId
						inner join General.Customers C on V.CustomerId = C.CustomerId
						inner join General.CustomersByPartner cp on cp.CustomerId = c.CustomerId
						left join Control.PartnerFuel PF on C.CustomerId = cp.CustomerId and cp.PartnerId = PF.PartnerId and VC.DefaultFuelId = PF.FuelId and PF.EndDate is null
						where VF.[VehicleFuelId] = @IndexVehicleIdFuel
						--and V.Active = 1

					-- NEXT INDEX
					SELECT @IndexVehicleId = MIN(VehicleId)
					FROM @VehicleFuelTemp
					WHERE VehicleId > @IndexVehicleId

				END --TERMINA WHILE

			
				-- UPDATE DEFAULT REGISTER FOR VEHICLES WITHOUT [VehicleFuel]
				INSERT INTO [Control].[VehicleFuel] (VehicleId, Month, Year, Liters, Amount, InsertDate)
				SELECT V.[VehicleId], @lCurrentMonth, @lCurrentYear, 0, 0, @lProcessDate
				FROM [General].[Vehicles] V
				WHERE (V.[IsDeleted] IS NULL OR V.[IsDeleted] = 0) AND
						V.[CustomerId] = @CustomerId AND
						V.[VehicleId] NOT IN (
							SELECT VehicleId FROM @VehicleFuelTemp
						)

		    
				DELETE FROM @VehicleFuelTemp -- DELETE ALL DATA FOR CURRENT CUSTOMER PROCESS 
			
						
				--Update FuelsByCredit Assigned Amount   
				Update FC
					set FC.Assigned = VFG.Amount
					from Control.FuelsByCredit FC
					inner join (Select V.CustomerId, VC.DefaultFuelId, Sum(VF.Amount) Amount
								from Control.VehicleFuel VF
								inner join General.Vehicles V on VF.VehicleId = V.VehicleId
								inner join General.VehicleCategories VC on V.VehicleCategoryId = VC.VehicleCategoryId
								where VF.Year = @lCurrentYear
								and VF.Month = @lCurrentMonth
								and V.[CustomerId] = @CustomerId
								group by V.CustomerId, VC.DefaultFuelId) VFG
					on FC.CustomerId = VFG.CustomerId and FC.FuelId = VFG.DefaultFuelId
					where FC.Year = @lCurrentYear
					and FC.Month = @lCurrentMonth
					and FC.CustomerId = @CustomerId

				if(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se env�a notificaci�n
				BEGIN
					SET @Subject = 'Actualizaci�n de Monto Asignado de Credito de Combustible para Cliente con Cod.'+ CAST(@CustomerId AS varchar(20))
					SET @Message = 'C�digo de Alerta(UPDT_Customer_004) En el proceso de inicio de Mes para el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' no hubo actualizaci�n de los montos del combustible asignado al Cliente. Por favor realizar la revisi�n respectiva.'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'

					-- ENVIAMOS NOTIFICACIONES
					Exec General.Sp_Email_AddOrEdit null, @To, null, null, @Subject, @Message, 0, 0, null, null 
				END
				ELSE BEGIN
					SET @Message = 'C�digo de Alerta(UPDT_Customer_004) En el proceso de inicio de Mes el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' se actualiz� correctamente los montos del combustible asignado al Cliente.'

					-- AGREGAMOS AL LOG DE BASE DE DATOS
					EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
				END 
				------------------------------------------
			END

			-- RESTABLECE EL MONTO DE LAS TARJETAS DEL CUSTOMER CON BASE AL MONTO ASIGNADO AL VEHICULO
			DECLARE @VehicleFuelToCCTemp TABLE(
				Id INT IDENTITY(1,1),
				CreditCardId int,
				VehicleId int,
				CreditLimit numeric(16,2),
				AmountVehicle numeric(16,2),
				LitersVehicle numeric(16,10)
			)

			-- INSERTAMOS EN LA TABLA TEMPORAL TODOS LOS MONTOS LIGADOS A VEHICULOS PARA RESTABLECERLOS EN LAS TARJETAS RELACIONADAS
			INSERT INTO @VehicleFuelToCCTemp 
			SELECT cc.[CreditCardId], 
					v.VehicleId,
					cc.CreditLimit,
					(SELECT TOP 1 vf.Amount FROM [Control].[VehicleFuel] vf 
					WHERE vf.VehicleId = v.[VehicleId] AND vf.[Month] = @lCurrentMonth AND vf.[Year] = @lCurrentYear 
					ORDER BY vf.[InsertDate] DESC) AS AmountVehicle,
					(SELECT TOP 1 vf.Liters FROM [Control].[VehicleFuel] vf
					WHERE vf.VehicleId = v.[VehicleId] AND vf.[Month] = @lCurrentMonth AND vf.[Year] = @lCurrentYear 
					ORDER BY vf.[InsertDate] DESC) AS LitersVehicle
			FROM [Control].[CreditCard] cc
				INNER JOIN [Control].[CreditCardByVehicle] ccv
					ON cc.[CreditCardId] = ccv.[CreditCardId]
				INNER JOIN [General].[Vehicles] v
					ON v.[VehicleId] = ccv.[VehicleId]
			WHERE cc.CustomerId = @CustomerId AND 
					cc.StatusId IN (7)
			
					--RECORREMOS LOS INDICES DE LA TABLA TEMPORAL
					DECLARE @wIndex int = 0
  
					SELECT @wIndex = MIN(Id)
					FROM @VehicleFuelToCCTemp
			  
					WHILE (@wIndex IS NOT NULL) -- INICIA EL WHILE
					BEGIN
					DECLARE @currentCCId int = 0
					DECLARE @currentCCLimit numeric (16,2) = 0
					DECLARE @updateCCAmount numeric (16,2) = 0
	
					SELECT @currentCCId = CreditCardId,
							@currentCCLimit = CreditLimit,
							@updateCCAmount = AmountVehicle
					FROM @VehicleFuelToCCTemp
					WHERE Id = @wIndex

					IF(@updateCCAmount < 1) BEGIN 
						SET @updateCCAmount = @currentCCLimit
					END

					-- ACTUALIZAMOS EL MONTO DIPONIBLE DE LA TARJETA SEG�N MONTO DEL COMBUSTIBLE DEL VEHICULO
					UPDATE [Control].[CreditCard]
					SET [CreditAvailable] = @updateCCAmount,
						[CreditLimit] = @updateCCAmount
					WHERE [CustomerId] = @CustomerId AND 
							[CreditCardId] = @currentCCId

					---- NEXT INDEX
					SELECT @wIndex = MIN(Id) FROM @VehicleFuelToCCTemp
					WHERE Id > @wIndex

					END -- TERMINA EL WHILE
				

			-- SI NO HAY RELACI�N CON ALG�N VEHICULO SE LE RESTABLECE EL MONTO LIMITE YA ASIGNADO A LA TARJETA
			UPDATE [Control].[CreditCard]
			SET [CreditAvailable] = [CreditLimit]
			WHERE 
					[CustomerId] = @CustomerId AND
					[CreditCardId] NOT IN (
					SELECT [CreditCardId] FROM @VehicleFuelToCCTemp ) AND
					[StatusId] = 7
		

			DELETE FROM @VehicleFuelToCCTemp -- DELETE ALL DATA FOR CURRENT CUSTOMER PROCESS 
			-- ///////////////////////////////////////////////////////////////////////////////


			if(@@ROWCOUNT = 0) -- Si no hay cambios se agrega log y se env�a notificaci�n
			BEGIN
				SET @Subject = 'Actualizaci�n de Monto Asignado de Tarjetas para Cliente con Cod.'+ CAST(@CustomerId AS varchar(20))
				SET @Message = 'C�digo de Alerta(UPDT_Customer_005) En el proceso de inicio de Mes para el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' no hubo actualizaci�n de los montos de las tarjetas activas asignadas al Cliente. Por favor realizar la revisi�n respectiva.'

				-- AGREGAMOS AL LOG DE BASE DE DATOS
				EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'W'

				-- ENVIAMOS NOTIFICACIONES
				Exec General.Sp_Email_AddOrEdit null, @To, null, null, @Subject, @Message, 0, 0, null, null 
			END
			ELSE BEGIN
				SET @Message = 'C�digo de Alerta(UPDT_Customer_005) En el proceso de inicio de Mes el cliente con Cod.'+ CAST(@CustomerId AS varchar(20)) +' se actualiz� correctamente los montos de las tarjetas activas asignadas al Cliente.'

				-- AGREGAMOS AL LOG DE BASE DE DATOS
				EXEC [General].[Sp_EventDBLog_Add] '[Control].[Sp_MonthlyClosing]', @Message, 'I'
			END 
			--------------------------------------------------

			-- VALIDA SI EL PROCESO ES PARA M�S DE UN CUSTOMER -----------------------
			IF(@ONLY_ONE_Customer IS NULL OR @ONLY_ONE_Customer = 0)
			BEGIN
				-- NEXT INDEX
				SELECT @CustomerId = MIN(CustomerId) FROM [General].[Customers]
				WHERE [IsDeleted] IS NULL OR [IsDeleted] = 0
						AND CustomerId > @CustomerId
			END
			ELSE 
			BEGIN 
				SET @CustomerId = NULL
			END

		END  -- TERMINA WHILE CUSTOMERS
		        

	    --Warning Emails /////////////////////////////////////
	    Exec Control.Sp_MonthlyClosing_WarningEmails @lCurrentYear, @lCurrentMonth
	    
	    EXEC [Control].[Sp_MonthlyClosing_LowPerformanceEmails] @lCurrentYear, @lCurrentMonth
		-- ///////////////////////////////////////////////////

        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
			      
         
	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO


