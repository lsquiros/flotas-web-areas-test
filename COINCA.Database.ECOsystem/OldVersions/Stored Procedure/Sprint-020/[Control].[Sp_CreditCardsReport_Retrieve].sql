USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardsReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardsReport_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_CreditCardsReport_Retrieve]    Script Date: 12/3/2015 4:00:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/20/2015
-- Description:	Retrieve Credit Cards Report information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardsReport_Retrieve]
(
	@pCustomerId INT = NULL,		--@pCustomerId: CustomerId
	@pCountryId INT = NULL,			--@pCountryId: CountryId
	@pYear INT = NULL,				--@pYear: Year
	@pMonth INT = NULL,				--@pMonth: Month
	@pStartDate DATETIME = NULL,	--@pStartDate: Start Date
	@pEndDate DATETIME = NULL		--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @pIssueForId INT

		SELECT DISTINCT
			a.[CreditCardId],
			a.[CreditCardNumber],
			c.[Name] [ApplicantName],
			d.[Name] [StatusName],
			a.[InsertDate] [RequestDate],
			e.[InsertDate] [DeliveryDate],
			b.[CardRequestId] [RequestNumber]
		FROM 
			[Control].[CreditCard] a
			INNER JOIN [Control].[CardRequest] b ON a.[CardRequestId] = b.[CardRequestId]
			INNER JOIN [General].[Users] c ON b.[InsertUserId] = c.[UserId]
			INNER JOIN [General].[Status] d ON a.[StatusId] = d.[StatusId]
			LEFT JOIN [Control].[CreditCardHx] e ON a.[CreditCardId] = e.[CreditCardId] AND e.[StatusId] = 5
			INNER JOIN [General].[Customers] f ON a.[CustomerId] = f.[CustomerId]			
		WHERE 
			a.[CustomerId] = @pCustomerId AND
			f.[CountryId] = @pCountryId AND
				((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
				AND DATEPART(m,a.[InsertDate]) = @pMonth
				AND DATEPART(yyyy,a.[InsertDate]) = @pYear) OR
				(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
				AND a.[InsertDate] BETWEEN @pStartDate AND @pEndDate))
		ORDER BY
			a.[InsertDate]
	
    SET NOCOUNT OFF
END
