
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReportByPartner_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsReportByPartner_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsReportByPartner_Retrieve]    Script Date: 12/17/2015 5:04:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Kevin Pe�a
-- Create date: 11/10/2015
-- Description:	Retrieve TransactionsReport information By Partner, Update to retrieve TerminalId
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionsReportByPartner_Retrieve] (
	@pPartnerId INT
	,--@pPartnerId: PartnerId
	@pStatus INT = NULL
	,--@pStatus: Status
	@pKey VARCHAR(800) = NULL
	,--@pKey :Key
	@pYear INT = NULL
	,--@pYear: Year
	@pMonth INT = NULL
	,--@pMonth: Month
	@pStartDate DATETIME = NULL
	,--@pStartDate: Start Date
	@pEndDate DATETIME = NULL --@pEndDate: End Date
	)
AS
BEGIN
	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#tmp_CustomersByPartner') IS NOT NULL
		DROP TABLE #tmp_CustomersByPartner

	IF OBJECT_ID('tempdb..#tmp_TransactionsReports') IS NOT NULL
		DROP TABLE #tmp_TransactionsReports

		IF OBJECT_ID('tempdb..#tmp_transactions_driver') IS NOT NULL
		DROP TABLE #tmp_TransactionsReports

	CREATE TABLE #tmp_CustomersByPartner (
		RowId INT identity(1, 1) NOT NULL,
		CustomerId INT
		)

	CREATE TABLE #tmp_TransactionsReports (
		 [CustomerName] VARCHAR(250)
		,[CreditCardId] INT
		,[CreditCardNumber] NVARCHAR(520)
		,[Invoice] VARCHAR(12) NULL
		,[MerchantDescription] VARCHAR(75) NULL
		,[SystemTraceCode] VARCHAR(250) NULL
		,[HolderName] VARCHAR(250) NULL
		,[Date] DATETIME
		,[FuelName] VARCHAR(50)
		,[FuelAmount] DECIMAL(16, 2)
		,[Odometer] INT
		,[Liters] DECIMAL(16, 2)
		,[PlateId] VARCHAR(10)
		,[CurrencySymbol] NVARCHAR(50) NULL
		,[State] VARCHAR(250) NULL
		,[CostCenterName] VARCHAR(250) NULL
		,[TerminalId] VARCHAR(20) NULL
		)

CREATE TABLE #tmp_transactions_driver(
 [TransactionId] INT,
 [Invoice] VARCHAR(12),
 [MerchantDescription]  VARCHAR(75),
 [SystemTraceCode] VARCHAR(250),
 [CreditCardNumber] NVARCHAR(520),
 [HolderName] VARCHAR(250),
 [Date] DATETIME,
 [FuelName] VARCHAR(50),
 [FuelAmount] DECIMAL(16,2),
 [Odometer] INT,
 [Liters] DECIMAL(16,2),
 [PlateId] VARCHAR(10),
 [CurrencySymbol] NVARCHAR(50),
 [State] VARCHAR(250),
 [CostCenterName] VARCHAR(250),
 [TerminalId] VARCHAR(20) NULL
)

	INSERT INTO #tmp_CustomersByPartner
	SELECT [CustomerId]
	FROM [General].[CustomersByPartner]
	WHERE PartnerId = @pPartnerId

	DECLARE @TempID INT
	DECLARE @pCustomerId INT

	WHILE EXISTS (SELECT *FROM #tmp_CustomersByPartner)
	BEGIN
		SELECT TOP 1 @pCustomerId = [CustomerId],@TempID = [RowId]
		FROM #tmp_CustomersByPartner

		DELETE FROM #tmp_CustomersByPartner WHERE RowId = @TempID

		DECLARE @pIssueForId INT

		SET @pIssueForId = (
				SELECT [IssueForId]
				FROM [General].[Customers]
				WHERE [CustomerId] = @pCustomerId
				)

		IF @pIssueForId = 100
		BEGIN
			--DRIVER
			INSERT INTO #tmp_transactions_driver
			SELECT t.[TransactionId] [TransactionId]
				,ISNULL(t.[Invoice], '-') AS [Invoice]
				,ISNULL(t.[MerchantDescription], '-') AS [MerchantDescription]
				,t.[TransactionPOS] AS [SystemTraceCode]
				,cr.[CreditCardNumber] [CreditCardNumber]
				,u.[Name] [HolderName]
				,t.[Date] [Date]
				,f.[Name] [FuelName]
				,t.[FuelAmount] [FuelAmount]
				,t.[Odometer] [Odometer]
				,t.[Liters] [Liters]
				,v.[PlateId] [PlateId]
				,g.[Symbol] [CurrencySymbol]
				,(
					CASE 
						WHEN t.[IsReversed] = 1
							THEN 'Reversada'
						WHEN t.[IsDuplicated] = 1
							THEN 'Duplicada'
						WHEN t.[IsFloating] = 1
							THEN 'Flotante'
						ELSE 'Procesada'
						END
					) AS [State]
				,e.[Name] AS [CostCenterName]
				,t.[ProcessorId] AS [TerminalId] 
			FROM [Control].[Transactions] t
			INNER JOIN [General].[Vehicles] v ON t.[VehicleId] = v.[VehicleId]
			INNER JOIN [General].[Customers] c ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN [Control].[Fuels] f ON t.[FuelId] = f.[FuelId]
			INNER JOIN [Control].[CreditCardByDriver] cd ON t.[CreditCardId] = cd.[CreditCardId]
			INNER JOIN [Control].[CreditCard] cr ON cd.CreditCardId = cr.CreditCardId
			INNER JOIN [General].[Users] u ON cd.[UserId] = u.[UserId]
			INNER JOIN [Control].[Currencies] g ON c.[CurrencyId] = g.[CurrencyId]
			INNER JOIN [General].[VehicleCostCenters] e ON v.CostCenterId = e.CostCenterId
			WHERE c.[CustomerId] = @pCustomerId
				AND (
					@pStatus IS NULL
					OR (
						ISNULL(t.[IsFloating], 0) = CASE 
							WHEN @pStatus = 2
								THEN 1
							ELSE 0
							END
						AND --Floating
						ISNULL(t.[IsReversed], 0) = CASE 
							WHEN @pStatus = 3
								THEN 1
							ELSE 0
							END
						AND --Reversed
						ISNULL(t.[IsDuplicated], 0) = CASE 
							WHEN @pStatus = 4
								THEN 1
							ELSE 0
							END
						AND --Duplicated
						ISNULL(t.[IsAdjustment], 0) = CASE 
							WHEN @pStatus = 5
								THEN 1
							ELSE 0
							END
						)
					) --Adjustment
				AND (
					@pKey IS NULL
					OR u.[Name] LIKE '%' + @pKey + '%'
					OR v.[PlateId] LIKE '%' + @pKey + '%'
					)
				AND (
					(
						@pYear IS NOT NULL
						AND @pMonth IS NOT NULL
						AND DATEPART(m, t.[Date]) = @pMonth
						AND DATEPART(yyyy, t.[Date]) = @pYear
						)
					OR (
						@pStartDate IS NOT NULL
						AND @pEndDate IS NOT NULL
						AND t.[Date] BETWEEN @pStartDate
							AND @pEndDate
						)
					)
			ORDER BY t.[Date] DESC
		END
		ELSE
		BEGIN
			--VEHICLE
			IF (@pStatus = 1)
			BEGIN
				INSERT INTO #tmp_TransactionsReports
				SELECT c.[Name] [CustomerName]
					,t.[CreditCardId] [CreditCardId]
					,cc.[CreditCardNumber] AS [CreditCardNumber]
					,ISNULL(t.[Invoice], '-') AS [Invoice]
					,ISNULL(t.[MerchantDescription], '-') AS [MerchantDescription]
					,t.[TransactionPOS] AS [SystemTraceCode]
					,v.[PlateId] [HolderName]
					,t.[Date] [Date]
					,f.[Name] [FuelName]
					,t.[FuelAmount] [FuelAmount]
					,t.[Odometer] [Odometer]
					,t.[Liters] [Liters]
					,v.[PlateId] [PlateId]
					,g.[Symbol] [CurrencySymbol]
					,(
						CASE 
							WHEN t.[IsReversed] = 1
								THEN 'Reversada'
							WHEN t.[IsDuplicated] = 1
								THEN 'Duplicada'
							WHEN t.[IsFloating] = 1
								THEN 'Flotante'
							ELSE 'Procesada'
							END
						) AS [State]
					,e.[Name] AS [CostCenterName]
					,t.[ProcessorId] AS [TerminalId] 
				FROM [Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c ON v.[CustomerId] = c.[CustomerId]
				INNER JOIN [Control].[Fuels] f ON t.[FuelId] = f.[FuelId]
				INNER JOIN [Control].[CreditCardByVehicle] cv ON t.[CreditCardId] = cv.[CreditCardId]
				INNER JOIN [Control].[Currencies] g ON c.[CurrencyId] = g.[CurrencyId]
				INNER JOIN [Control].[CreditCard] cc ON cc.CreditCardId = cv.CreditCardId
				INNER JOIN [General].[VehicleCostCenters] e ON v.CostCenterId = e.CostCenterId
				WHERE c.[CustomerId] = @pCustomerId
					AND 1 > (
						SELECT ISNULL(COUNT(1), 0) FROM Control.Transactions t2
						WHERE t2.[CreditCardId] = t.[CreditCardId] AND
							t2.[TransactionPOS] = t.[TransactionPOS] AND
							t2.[ProcessorId] = t.[ProcessorId] AND 
							t2.IsReversed = 1
						)
					AND (
						@pKey IS NULL
						OR v.[PlateId] LIKE '%' + @pKey + '%'
						)
					AND (
						(
							@pYear IS NOT NULL
							AND @pMonth IS NOT NULL
							AND DATEPART(m, t.[Date]) = @pMonth
							AND DATEPART(yyyy, t.[Date]) = @pYear
							)
						OR (
							@pStartDate IS NOT NULL
							AND @pEndDate IS NOT NULL
							AND t.[Date] BETWEEN @pStartDate
								AND @pEndDate
							)
						)
				ORDER BY t.[Date] DESC
			END
			ELSE
			BEGIN
				--SELECT 'ENTRE 3'
				INSERT INTO #tmp_TransactionsReports
				SELECT c.[Name] [CustomerName]
					,t.[CreditCardId] [CreditCardId]
					,cc.[CreditCardNumber] [CreditCardNumber]
					,ISNULL(t.[Invoice], '-') AS [Invoice]
					,ISNULL(t.[MerchantDescription], '-') AS [MerchantDescription]
					,t.[TransactionPOS] AS [SystemTraceCode]
					,v.[PlateId] [HolderName]
					,t.[Date] [Date]
					,f.[Name] [FuelName]
					,t.[FuelAmount] [FuelAmount]
					,t.[Odometer] [Odometer]
					,t.[Liters] [Liters]					
					,v.[PlateId] [PlateId]
					,g.[Symbol] [CurrencySymbol]
					,(
						CASE 
							WHEN t.[IsReversed] = 1
								THEN 'Reversada'
							WHEN t.[IsDuplicated] = 1
								THEN 'Duplicada'
							WHEN t.[IsFloating] = 1
								THEN 'Flotante'
							ELSE 'Procesada'
							END
						) AS [State]
					,e.[Name] AS [CostCenterName]
					,t.[ProcessorId] AS [TerminalId] 
				FROM [Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c ON v.[CustomerId] = c.[CustomerId]
				INNER JOIN [Control].[Fuels] f ON t.[FuelId] = f.[FuelId]
				INNER JOIN [Control].[CreditCardByVehicle] cv ON t.[CreditCardId] = cv.[CreditCardId]
				INNER JOIN [Control].[Currencies] g ON c.[CurrencyId] = g.[CurrencyId]
				INNER JOIN [Control].[CreditCard] cc ON cc.CreditCardId = t.CreditCardId
				INNER JOIN [General].[VehicleCostCenters] e ON v.CostCenterId = e.CostCenterId
				WHERE c.[CustomerId] = @pCustomerId
					AND (
						@pStatus IS NULL
						OR @pStatus = 0
						OR (
							ISNULL(t.[IsFloating], 0) = CASE 
								WHEN @pStatus = 2
									THEN 1
								ELSE 0
								END
							AND --Floating
							ISNULL(t.[IsReversed], 0) = CASE 
								WHEN @pStatus = 3
									THEN 1
								ELSE 0
								END
							AND --Reversed
							ISNULL(t.[IsDuplicated], 0) = CASE 
								WHEN @pStatus = 4
									THEN 1
								ELSE 0
								END
							AND --Duplicated
							ISNULL(t.[IsAdjustment], 0) = CASE 
								WHEN @pStatus = 5
									THEN 1
								ELSE 0
								END
							) --Adjustment
						)
					AND (
						@pKey IS NULL
						OR v.[PlateId] LIKE '%' + @pKey + '%'
						)
					AND (
						(
							@pYear IS NOT NULL
							AND @pMonth IS NOT NULL
							AND DATEPART(m, t.[Date]) = @pMonth
							AND DATEPART(yyyy, t.[Date]) = @pYear
							)
						OR (
							@pStartDate IS NOT NULL
							AND @pEndDate IS NOT NULL
							AND t.[Date] BETWEEN @pStartDate
								AND @pEndDate
							)
						)
				ORDER BY t.[Date] DESC

			END
			
		END --fin del while customer exist
		
	END

	SET NOCOUNT OFF
	SELECT * FROM #tmp_TransactionsReports
	--SELECT * FROM #tmp_transactions_driver 

	DROP TABLE #tmp_TransactionsReports
	DROP TABLE #tmp_CustomersByPartner
	DROP TABLE #tmp_transactions_driver
END





GO


