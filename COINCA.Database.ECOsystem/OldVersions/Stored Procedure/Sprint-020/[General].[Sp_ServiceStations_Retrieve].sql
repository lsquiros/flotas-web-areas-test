
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ServiceStations_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ServiceStations_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 20/11/2015
-- Description:	Retrieve Service Stations
-- ================================================================================================
Create PROCEDURE [General].[Sp_ServiceStations_Retrieve]
(
	@pServiceStationId INT = NULL,
	@pCountryId INT = NULL,
	@pKey VARCHAR(800) = NULL
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT
		 [ServiceStationId]  
		 ,[BacId] 
		 ,[Name]
		 ,[Address]
		 ,[CantonId] 
		 ,[ProvinceId]	
		 ,[Latitude]
		 ,[Longitude]	
    FROM [General].[ServiceStations]
	WHERE (@pServiceStationId IS NULL OR [ServiceStationId] = @pServiceStationId)
	  AND (@pKey IS NULL 
				OR [BacId] like '%'+@pKey+'%'
				OR [Name] like '%'+@pKey+'%'
				OR [Address] like '%'+@pKey+'%'
				OR [CantonId] like '%'+@pKey+'%'
				OR [ProvinceId] like '%'+@pKey+'%')
	 AND (@pCountryId IS NULL OR [CountryId] = @pCountryId)

	ORDER BY [BacId] DESC
	
    SET NOCOUNT OFF
END

GO
