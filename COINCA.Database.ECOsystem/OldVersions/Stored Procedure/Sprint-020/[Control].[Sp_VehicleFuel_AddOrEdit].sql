
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_VehicleFuel_AddOrEdit]
GO

/****** Object:  StoredProcedure [Control].[Sp_VehicleFuel_AddOrEdit]    Script Date: 12/11/2015 9:51:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update Vehicle Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuel_AddOrEdit]
(
  	 @pVehicleFuelId INT = NULL				--@pVehicleFuelId: PK of the table
	,@pVehicleId INT						--@pVehicleId: FK of Vehicles
	,@pLiters DECIMAL(16,10)				--@pLiters: Liters of Fuel
	,@pAmount DECIMAL(16,6)					--@pAmount: Amount of Fuel
	,@pMonth INT							--@pMonth: Month
	,@pYear INT								--@pYear: Year
	,@pCustomerId INT						--@pCustomerId: Customer Id
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
	,@pThrowError BIT = 1					--@pThrowError: Throw Error by Default or performs select 
)
AS
BEGIN
	
	SET NOCOUNT ON
	SET XACT_ABORT ON
    
	BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
			DECLARE @lErrorSeverity INT
			DECLARE @lErrorState INT
			DECLARE @lErrorNumber INT
			DECLARE @lLocalTran BIT = 0
			DECLARE @lRowCount INT = 0
			DECLARE @lCreditCardId INT = 0
			DECLARE @lCreditAvailable DECIMAL(16,6)
			DECLARE @lCreditLimit DECIMAL(16,6)
			DECLARE @lCreditDiff DECIMAL(16,6)
            
			IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pVehicleFuelId IS NULL)
			BEGIN
				DECLARE @CountExist int = 0
				
				SELECT @CountExist = COUNT(1) 
				FROM [Control].[VehicleFuel]
				WHERE [Month] = @pMonth AND 
						[Year] = @pYear AND
						(@pVehicleId IS NULL OR [VehicleId] = @pVehicleId)
				
				IF @CountExist = 0
				BEGIN
					INSERT INTO [Control].[VehicleFuel]
							([VehicleId]
							,[Month]
							,[Year]
							,[Liters]
							,[Amount]
							,[InsertDate]
							,[InsertUserId])
					VALUES	(@pVehicleId
							,@pMonth
							,@pYear
							,@pLiters
							,@pAmount
							,GETUTCDATE()
							,@pLoggedUserId)				
				END
				ELSE 
				BEGIN
						SET @lErrorNumber = 50004
						SET @lErrorMessage = 'Code_50004:Error adding Fuels By Credit, Month and Year assigned to exist.'
						SET @lErrorSeverity = 16
						SET @lErrorState = 1
						
						IF(@pThrowError = 1)
							RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
						ELSE
							SELECT
									@lErrorNumber AS ErrorNumber
								,@lErrorMessage AS ErrorMessage
								,@lErrorSeverity AS ErrorSeverity
								,@lErrorState AS ErrorState
					
				END
			END
			ELSE
			BEGIN
				UPDATE [Control].[VehicleFuel]
					SET  [VehicleId] = @pVehicleId
						,[Month] = @pMonth
						,[Year] = @pYear
						,[Liters] = @pLiters
						,[Amount] = @pAmount
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [VehicleFuelId] = @pVehicleFuelId
					AND [RowVersion] = @pRowVersion
				  
			END            
            
			SET @lRowCount = @@ROWCOUNT				
            
			IF @lRowCount > 0
			BEGIN
				DECLARE @lAssigned DECIMAL(16,2) = 0
				DECLARE @lTotal DECIMAL(16,2) = 0
				DECLARE @lFuelId INT = 0
				DECLARE @lDistributionTypeId INT = 0
				
				-- Get Type of Fuel for vehicle
				SELECT @lFuelId = b.DefaultFuelId
				FROM [General].[Vehicles] a
					INNER JOIN [General].[VehicleCategories] b
						ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
				WHERE a.[VehicleId] = @pVehicleId
				
				-- Get amount of fuel assigned by year, month, customer and fuel type					
				SET @lAssigned = COALESCE((SELECT SUM(a.[Amount]) 
				FROM [Control].[VehicleFuel] a
					INNER JOIN [General].[Vehicles] b
						ON a.[VehicleId] = b.[VehicleId]
					INNER JOIN [General].[VehicleCategories] c
						ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
				WHERE a.[Year] = @pYear
					AND a.[Month] = @pMonth
					AND b.[CustomerId] = @pCustomerId
					AND c.[DefaultFuelId] = @lFuelId),0)
				
				-- GEt total assigned to vehicle  
				SELECT @lTotal= Total
				FROM [Control].[FuelsByCredit]
				WHERE [Year] = @pYear
					AND [Month] = @pMonth
					AND [CustomerId] = @pCustomerId
					AND [FuelId] = @lFuelId
				  
				SELECT
					@lDistributionTypeId = [DistributionTypeId]
				FROM [General].[Customers]
				WHERE [CustomerId] = @pCustomerId
				
				-- If assigned is less than total then update assigned
				IF @lAssigned < = @lTotal
				BEGIN
					UPDATE [Control].[FuelsByCredit]
						SET [Assigned] = @lAssigned
					WHERE [Year] = @pYear
						AND [Month] = @pMonth
						AND [CustomerId] = @pCustomerId
						AND [FuelId] = @lFuelId
					  
					-- Updates credit card limit for card attached to vehicle
					SELECT	@lCreditCardId = CC.CreditCardId,
							@lCreditAvailable = CC.CreditAvailable,
							@lCreditLimit = CC.CreditLimit
					FROM	General.Vehicles AS V
							--INNER JOIN Control.CardRequest AS CR ON V.PlateId = CR.PlateId		
							--INNER JOIN Control.CreditCard AS CC ON CC.CardRequestId = CR.CardRequestId
							INNER JOIN [Control].[CreditCardByVehicle] AS CV ON CV.VehicleId = @pVehicleId
							INNER JOIN [Control].[CreditCard] AS CC ON CC.[CreditCardId] = CV.[CreditCardId]
					WHERE	V.VehicleId = @pVehicleId 
							and CC.StatusId = 7
					
					-- UPDATE AVAILABLE FOR THIS CARD
					SELECT	@lCreditDiff = @pAmount - @lCreditLimit
					
					UPDATE	[Control].[CreditCard]
					SET		CreditLimit = @pAmount,
							CreditAvailable = @lCreditAvailable + @lCreditDiff
					WHERE	CreditCardId = @lCreditCardId

					-- INSERT NEW HISTORIC FOR CREDIT CARD AVAILABLE
					INSERT INTO [Control].[CreditCardHx]
						([CreditCardId],[CustomerId],[CreditCardNumber],[ExpirationYear],[ExpirationMonth]
						,[CreditLimit],[CreditAvailable],[CreditExtra],[StatusId],[CardRequestId]
						,[InsertDate],[InsertUserId],[ModifyDate],[ModifyUserId])
					SELECT 
						[CreditCardId],[CustomerId],[CreditCardNumber],[ExpirationYear],[ExpirationMonth]
						,[CreditLimit],[CreditAvailable],[CreditExtra],[StatusId],[CardRequestId]
						,GETDATE(),@pLoggedUserId,GETDATE(),@pLoggedUserId
					FROM [Control].[CreditCard]
					WHERE CreditCardId = @lCreditCardId
					 

				END ELSE IF(@lDistributionTypeId = 901)
				BEGIN
					
					DECLARE @lFuelByCreditId INT 
					
					-- Get fuel by credit	
					SET @lFuelByCreditId = COALESCE(
						(SELECT FuelByCreditId
						FROM [Control].[FuelsByCredit]
						WHERE [Year] = @pYear
							AND [Month] = @pMonth
							AND [CustomerId] = @pCustomerId
							AND [FuelId] = @lFuelId),-1)
					
					-- if not exist insert else update
					IF (@lFuelByCreditId = -1)
					BEGIN
						INSERT INTO [Control].[FuelsByCredit]
								([CustomerId]
								,[FuelId]
								,[Month]
								,[Year]
								,[Total]
								,[Assigned]
								,[InsertDate]
								,[InsertUserId])
						VALUES	(@pCustomerId
								,@lFuelId
								,@pMonth
								,@pYear
								,@lAssigned
								,@lAssigned
								,GETUTCDATE()
								,@pLoggedUserId)
					END
					ELSE
					BEGIN 
						UPDATE [Control].[FuelsByCredit]
							SET  [Assigned] = @lAssigned
								,[Total] = @lAssigned 
						WHERE [Year] = @pYear
							AND [Month] = @pMonth
							AND [CustomerId] = @pCustomerId
							AND [FuelId] = @lFuelId
						  
						-- Updates credit card limit for card attached to vehicle
						SELECT	@lCreditCardId = CC.CreditCardId,
								@lCreditAvailable = CC.CreditAvailable,
								@lCreditLimit = CC.CreditLimit
						FROM	General.Vehicles AS V
								--INNER JOIN Control.CardRequest AS CR ON V.PlateId = CR.PlateId		
								--INNER JOIN Control.CreditCard AS CC ON CC.CardRequestId = CR.CardRequestId
								INNER JOIN [Control].[CreditCardByVehicle] AS CV ON CV.VehicleId = @pVehicleId
								INNER JOIN [Control].[CreditCard] AS CC ON CC.[CreditCardId] = CV.[CreditCardId]
						WHERE	V.VehicleId = @pVehicleId 
								and CC.StatusId = 7
						

						-- UPDATE AVAILABLE FOR THIS CARD
						SELECT	@lCreditDiff = @pAmount - @lCreditLimit
						
						UPDATE	[Control].[CreditCard]
						SET		CreditLimit = @pAmount,
								CreditAvailable = @lCreditAvailable + @lCreditDiff
						WHERE	CreditCardId = @lCreditCardId

						-- INSERT NEW HISTORIC FOR CREDIT CARD AVAILABLE
						INSERT INTO [Control].[CreditCardHx]
							([CreditCardId],[CustomerId],[CreditCardNumber],[ExpirationYear],[ExpirationMonth]
							,[CreditLimit],[CreditAvailable],[CreditExtra],[StatusId],[CardRequestId]
							,[InsertDate],[InsertUserId],[ModifyDate],[ModifyUserId])
						SELECT 
							[CreditCardId],[CustomerId],[CreditCardNumber],[ExpirationYear],[ExpirationMonth]
							,[CreditLimit],[CreditAvailable],[CreditExtra],[StatusId],[CardRequestId]
							,GETDATE(),@pLoggedUserId,GETDATE(),@pLoggedUserId
						FROM [Control].[CreditCard]
						WHERE CreditCardId = @lCreditCardId
					END					
				END
				ELSE
				BEGIN
					IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
					BEGIN
						ROLLBACK TRANSACTION
						
						SET @lErrorNumber = 50002
						SET @lErrorMessage = 'Code_50002:Error updating Fuels By Credit, Assigned Amount greater than Total.'
						SET @lErrorSeverity = 16
						SET @lErrorState = 1 --'TRUE'
						
						IF(@pThrowError = 1)
							RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
						ELSE
							SELECT
									@lErrorNumber AS ErrorNumber
								,@lErrorMessage AS ErrorMessage
								,@lErrorSeverity AS ErrorSeverity
								,@lErrorState AS ErrorState
					END
				END				  
			END
            
			IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
			
				SET @lErrorNumber = 50001
				SET @lErrorMessage = 'Error updating database row, Please try again. TimeStamp verification failed.'
				SET @lErrorSeverity = 16
				SET @lErrorState = 1 --TRUE 
				
				IF(@pThrowError = 1)
					RAISERROR (@lErrorNumber, @lErrorSeverity, @lErrorState)
				ELSE
					SELECT
							@lErrorNumber AS ErrorNumber
						,@lErrorMessage AS ErrorMessage
						,@lErrorSeverity AS ErrorSeverity
						,@lErrorState AS ErrorState
			END
			
	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE(), @lErrorNumber = ERROR_NUMBER()
		IF(@pThrowError = 1)
			RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		ELSE
			SELECT
					@lErrorNumber AS ErrorNumber
				,@lErrorMessage AS ErrorMessage
				,@lErrorSeverity AS ErrorSeverity
				,@lErrorState AS ErrorState
		
	END CATCH
        
	SET NOCOUNT OFF
	SET XACT_ABORT OFF
END