
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ValidatePlate_CreditCard]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_ValidatePlate_CreditCard]
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 11/12/2015
-- Description:	Validate if plate have assign credit card
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_ValidatePlate_CreditCard]
(
	  @pPlateId VARCHAR(10) = NULL
)
AS
BEGIN
		
	SET NOCOUNT ON
	SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
                        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END

		DECLARE @CurrentVehicleId INT = NULL
		DECLARE @CantActiveCards INT = NULL

		SELECT @CurrentVehicleId = VehicleId FROM [General].[Vehicles]
		WHERE PlateId = @pPlateId

		SELECT @CantActiveCards = COUNT(1) FROM [Control].[CreditCardByVehicle] ccv
		INNER JOIN [Control].[CreditCard] cc ON cc.[CreditCardId] = ccv.[CreditCardId]
		WHERE ccv.VehicleId = @CurrentVehicleId AND cc.StatusId <> 9

		IF(@CantActiveCards > 0)
		BEGIN
			RAISERROR ('ERR_CARD_WITH_PLATE_IN_USE', 16, 1)
		END

		SELECT 'SUCCESS'

	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF

END -- END SP