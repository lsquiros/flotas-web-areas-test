USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_DrivingTimeJob]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_DrivingTimeJob]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Kevin Pe�a O.
-- Create date: 09/14/2015
-- Description:	Pre calculated Working & Driving Time per Driver
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_DrivingTimeJob]
AS
BEGIN
	
	SET NOCOUNT ON

DECLARE @DrivingTimeValue datetime 
DECLARE @sumDrivingTime int = 0
DECLARE @sumWorkingDay int = 0
DECLARE @tmpvalue datetime 

DECLARE @TempDrivers TABLE 
(
   Id INT Identity(1,1)
  ,UserId INT
  ,Identification NVARCHAR(MAX)
  ,Name VARCHAR(250)
  ,DeviceReference INT
  ,CustomerId INT
)

INSERT INTO @TempDrivers( UserId, Identification, Name, DeviceReference, CustomerId)

SELECT u.UserId, p.Identification, u.Name, e.DeviceReference, p.CustomerId
FROM General.DriversUsers p
inner join General.Users u
ON p.UserId = u.UserId
INNER JOIN General.VehiclesByUser v 
ON v.UserId = p.UserId
INNER JOIN General.Vehicles e
on e.VehicleId= v.VehicleId 
WHERE DeviceReference IS NOT NULL

DECLARE @tempvalue INT
DECLARE DriverCursor CURSOR FOR 
SELECT Id
FROM @TempDrivers 

OPEN DriverCursor
FETCH NEXT FROM DriverCursor INTO @tempvalue
WHILE @@fetch_status = 0
BEGIN

 DECLARE @iUserId INT         
  SELECT @iUserId = [UserId] FROM @TempDrivers WHERE Id = @tempvalue

 DECLARE @iIdentification NVARCHAR(MAX)
  SELECT @iIdentification = [Identification] FROM @TempDrivers WHERE Id = @tempvalue

 DECLARE @iName  VARCHAR(250)         
  SELECT @iName = [Name] FROM @TempDrivers WHERE Id =@tempvalue

 DECLARE @iCustomerId INT  
  SELECT @iCustomerId = [CustomerId] FROM @TempDrivers WHERE Id = @tempvalue

 DECLARE @iDevice INT 
  SELECT @iDevice = [DeviceReference] FROM @TempDrivers WHERE Id =@tempvalue
    
    EXEC [Operation].[Sp_DrivingTimeperDay]
	   @pUserId = @iUserId 
	  ,@pIdentification = @iIdentification
	  ,@pName = @iName
	  ,@pDevice= @iDevice
	  ,@pCustomerId = @iCustomerId

    FETCH NEXT FROM DriverCursor INTO @tempvalue
END

 CLOSE  DriverCursor
 DEALLOCATE DriverCursor

SET NOCOUNT OFF
END


GO


