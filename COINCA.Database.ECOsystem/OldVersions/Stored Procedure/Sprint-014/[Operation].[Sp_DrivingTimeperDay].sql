USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_DrivingTimeperDay] ') AND type in (N'P', N'PC'))
	DROP PROCEDURE  [Operation].[Sp_DrivingTimeperDay] 
GO

/****** Object:  StoredProcedure [dbo].[ProcessReport]    Script Date: 9/14/2015 4:38:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Kevin Pe�a O.
-- Create date: 09/10/2015
-- Description:	Retrieve Working Time and DrivingTime Day
-- ================================================================================================


CREATE PROCEDURE [Operation].[Sp_DrivingTimeperDay] 
( 
  @pUserId INT
 ,@pIdentification NVARCHAR(MAX) 
 ,@pName VARCHAR(250)
 ,@pDevice INT  
 ,@pCustomerId INT
)
as
BEGIN


DECLARE @DrivingTimeValue datetime 
DECLARE @sumDrivingTime int = 0
DECLARE @sumWorkingDay int = 0
DECLARE @tmpvalue datetime 


   DECLARE ProdInfo CURSOR FOR 
   SELECT [GPSDateTime]       
   FROM [dbo].[Reports]
   WHERE GPSDateTime between CONVERT(date,dateadd(day,-1,getdate()),101) AND CONVERT(date,getdate(),101)
   and Device = @pDevice and VSSSpeed >5

OPEN ProdInfo
FETCH NEXT FROM ProdInfo INTO @DrivingTimeValue
WHILE @@fetch_status = 0
BEGIN
    SET @tmpvalue  = @DrivingTimeValue
    FETCH NEXT FROM ProdInfo INTO @DrivingTimeValue
	DECLARE @difftime int = DATEDIFF ( minute ,@DrivingTimeValue , @tmpvalue )
	SET @sumWorkingDay = @sumWorkingDay + @difftime 
	IF(@difftime > 1)
	SET  @sumDrivingTime = @sumDrivingTime + @difftime 
	
END

INSERT INTO [Operation].[PreDrivingTime_DayJob]
           ([UserId]
           ,[Name]
           ,[Identification]
           ,[CustomerId]
           ,[WorkingTime]
           ,[DrivingTime]
           ,[Daytime]
           ,[DeviceReference])
     VALUES
           (@pUserId
           ,@pName
           ,@pIdentification
           ,@pCustomerId
           ,@sumWorkingDay/60 
           ,@sumDrivingTime/60
           ,CONVERT(date,dateadd(day,-1,getdate()),101)
           ,@pDevice ) 

  CLOSE  ProdInfo
  DEALLOCATE ProdInfo
END
GO