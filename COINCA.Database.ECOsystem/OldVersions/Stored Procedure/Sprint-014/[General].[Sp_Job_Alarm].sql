USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Job_Alarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Job_Alarm]
GO

/****** Object:  StoredProcedure [General].[Sp_Job_Alarm]    Script Date: 9/30/2015 11:09:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- 
-- ================================================================================================
-- Author:		Cristian Mart�nez 
-- Create date: 21/11/2014
-- Description:	Stored Procedure that execute the alarm Add Alarm for Expiration of Driver  License 
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_Job_Alarm]
AS
BEGIN 
	DECLARE @lAlarmId INT
	DECLARE	@lCustomerId INT
	DECLARE	@lAlarmTriggerId INT
	DECLARE	@lEntityTypeId INT
	DECLARE	@lEntityId INT
	DECLARE	@lPeriodicityId INT
	DECLARE	@lPhone VARCHAR(100)
	DECLARE @lCode VARCHAR(400) 
	DECLARE	@lEmail VARCHAR(MAX)
	DECLARE @lRowVersion TIMESTAMP
	DECLARE @tAlarm TABLE(
		AlarmId INT,
		CustomerId INT, 
		AlarmTriggerId INT,
		Code VARCHAR(400), 
		EntityTypeId INT, 
		EntityId INT,
		PeriodicityId INT,
		Phone VARCHAR(100),
		Email VARCHAR(500),
		RowVersion VARCHAR(20)
	)
	
	INSERT INTO @tAlarm
	(
		AlarmId, 
		CustomerId,
		AlarmTriggerId,
		Code, 
		EntityTypeId, 
		EntityId, 
		PeriodicityId, 
		Phone, 
		Email, 
		RowVersion
	)SELECT a.[AlarmId], 
		    a.[CustomerId], 
		    a.[AlarmTriggerId], 
		    b.[Code],
		    a.[EntityTypeId], 
		    a.[EntityId], 
		    a.[PeriodicityTypeId], 
		    a.[Phone], 
		    a.[Email], 
		    CONVERT(VARCHAR(20),a.[RowVersion])
	FROM [General].[Alarms] a
	INNER JOIN [General].[Types] b
	ON a.[AlarmTriggerId] = b.[TypeId]
	WHERE a.[Active] = 1
	 AND (b.[Code] = 'CUSTOMER_ALARM_TRIGGER_PREVENTIVE_MAINTENANCE' OR b.[Code] = 'CUSTOMER_ALARM_TRIGGER_LICENSE_EXPIRATION')
	 AND (a.[NextAlarm] IS NULL 
	 OR CONVERT(DATE, a.[NextAlarm])= CONVERT(DATE,DATEADD(Hour, -6,GETUTCDATE())))
	 
	 WHILE EXISTS(SELECT [AlarmId] FROM @tAlarm)
	 BEGIN 
		SELECT TOP 1 @lAlarmId  = a.[AlarmId], 
			         @lCustomerId = a.[CustomerId],
				     @lAlarmTriggerId = a.[AlarmTriggerId],
				     @lCode = a.[Code],
					 @lEntityTypeId = a.[EntityTypeId],
				     @lEntityId = a.[EntityId],
					 @lPeriodicityId = a.[PeriodicityId],
					 @lPhone = a.[Phone],
					 @lEmail = a.[Email],
					 @lRowVersion = CONVERT(TIMESTAMP, a.[RowVersion]) 
		FROM @tAlarm a
		
		-- Maintenance Preventive
		IF @lCode = 'CUSTOMER_ALARM_TRIGGER_PREVENTIVE_MAINTENANCE'
		BEGIN 		
			SELECT @lEntityId = [VehicleId]
			FROM [General].[PreventiveMaintenanceByVehicle]
			WHERE [PreventiveMaintenanceId] = @lEntityId
			
			EXEC [General].[Sp_PreventiveMaintenance_Alarm] @lAlarmId,
															@lEntityId,
															@lCustomerId,
															@lAlarmTriggerId,
															@lPeriodicityId,
															@lPhone,
															@lEmail,
															@lRowVersion
		END
		
		--License Expirated
		IF @lCode = 'CUSTOMER_ALARM_TRIGGER_LICENSE_EXPIRATION'
		BEGIN 
			EXEC [General].[Sp_LicenseExpiration_Alarm]	@lAlarmId,
														@lEntityId,
														@lCustomerId,
														@lAlarmTriggerId,
														@lPeriodicityId,
														@lPhone,
														@lEmail,
														@lRowVersion
		END	
		DELETE @tAlarm WHERE AlarmId = @lAlarmId AND CustomerId = @lCustomerId
		CONTINUE 
	 END 		
END

GO


