USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_TransactionOdometer_Alarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_TransactionOdometer_Alarm]
GO
/****** Object:  StoredProcedure [General].[Sp_TransactionOdometer_Alarm]    Script Date: 9/24/2015 10:59:50 AM ******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 03/09/2015
-- Description:	Stored Procedure that validate odometer transaction and sent informative email
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_TransactionOdometer_Alarm]
(
	@pVehicleId INT,
	@pOdometer FLOAT
)
AS
BEGIN 

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
		DECLARE @lLastOdometer FLOAT
		DECLARE @lMaxPosibleOdometer FLOAT
		DECLARE @RowNum INT
		DECLARE @Message VARCHAR(MAX)
		DECLARE @PlateId VARCHAR(MAX)
		DECLARE @Email VARCHAR(MAX)
		DECLARE @SetAlarm INT
		DECLARE @lOdometerErrorMargin FLOAT

		SELECT @SetAlarm = 0
		
		SELECT @lOdometerErrorMargin = OdometerErrorMargin / 100.0
		FROM   General.Parameters
		
		SELECT 
			@lLastOdometer = [a].[TrxReportedOdometer] 
		FROM 
			(SELECT TOP 1 ISNULL([Odometer], 0) [TrxReportedOdometer]
			FROM 
				[Control].[TransactionsHx]
			WHERE 
				[VehicleId] = @pVehicleId
			ORDER BY 
				[InsertDate] DESC) [a]
			
		-- Calculate max possible odometer for vehicle
		SELECT	@lMaxPosibleOdometer = (c.Liters * c.DefaultPerformance) + 
									   ((c.Liters * c.DefaultPerformance) * @lOdometerErrorMargin) + 
									   @lLastOdometer
		FROM	[General].[Vehicles] b 
				INNER JOIN [General].[VehicleCategories] c on b.VehicleCategoryId = c.VehicleCategoryId
		WHERE	b.VehicleId = @pVehicleId
		
		IF @lLastOdometer > @pOdometer 
		BEGIN
			SELECT @SetAlarm = 1
		END
		ELSE
		BEGIN
			IF @pOdometer > @lMaxPosibleOdometer 
			BEGIN
				SELECT @SetAlarm = 2
			END
		END
			
		IF (@SetAlarm >= 1)
		BEGIN

			DECLARE @tEmails TABLE(
				[Id] INT IDENTITY(1,1) NOT NULL,
				[PlateId] VARCHAR(10),
				[Email] VARCHAR(MAX))


			--CARGA LA TABLA CON EL LISTADO DE CORREOS QUE SE DEBEN ENVIAR
				INSERT INTO @tEmails (
					[PlateId],
					[Email])
				SELECT
					[a].[PlateId], 
					[d].[Email]
				FROM	
					[General].[Vehicles] [a]
					INNER JOIN [General].[CustomerUsers] [b] ON [a].[CustomerId] = [b].[CustomerId]
					INNER JOIN [General].[Users] [c] ON [b].[UserId] = [c].[UserId]
					INNER JOIN [AspNetUsers] [d] ON [c].[AspNetUserId] = [d].[Id]
					INNER JOIN [AspNetUserRoles] [e] ON [d].[Id] = [e].[UserId]
					INNER JOIN [AspNetRoles] [f] ON [e].[RoleId] = [f].[Id]
				WHERE 
					[f].[Name] = 'CUSTOMER_ADMIN' AND 
					[a].[VehicleId] = @pVehicleId		
		
				SELECT @RowNum = Count(*) From @tEmails
				
				SELECT @Message = [a].[Message] FROM [General].[Values] [a] 
					INNER JOIN [General].[Types] [b] ON [a].[TypeId] = [b].[TypeId]
				WHERE 
					[b].[Code] = 'CUSTOMER_ALARM_TRIGGER_ODOMETER_TRANSACTION'
		
				WHILE @RowNum > 0
				BEGIN

					SELECT 
						@PlateId = [PlateId],
						@Email = [Email]
					FROM @tEmails 
					WHERE Id = @RowNum
		
					DECLARE @lMessage VARCHAR(MAX) = @Message
					SET @lMessage = REPLACE(@lMessage, '%PlateId%', @PlateId)
					SET @lMessage = REPLACE(@lMessage, '%LastOdometer%', @lLastOdometer)
					SET @lMessage = REPLACE(@lMessage, '%Odometer%', @pOdometer)
					SET @lMessage = REPLACE(@lMessage, '%time%', CONVERT(VARCHAR(10), DATEADD(Hour, -6,GETUTCDATE()), 111) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, -6,GETUTCDATE()), 108))
		
					INSERT INTO [General].[EmailEncryptedAlarms]
						([To],
						Subject,
						Message,
						Sent,
						[InsertDate])
					VALUES(@Email,
						'Alarma de Inconsistencias en Od�metro',
						@lMessage,
						0,
						GETUTCDATE())
		
					SET @RowNum = @RowNum - 1
				END
		END
		SELECT @SetAlarm AS SetAlarm,
			   @pOdometer AS Odometer,
			   @lMaxPosibleOdometer AS MaxPosibleOdometer,
			   @lLastOdometer as LastOdometer	
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF

END

GO


