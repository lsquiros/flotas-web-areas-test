USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AfterSchedule_Alarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_AfterSchedule_Alarm]
GO

/****** Object:  StoredProcedure [General].[Sp_AfterSchedule_Alarm]    Script Date: 9/17/2015 4:20:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Cristian Mart�nez 
-- Create date: 24/11/2014
-- Description:	Add alarm by use vehicule after of schedule 
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_AfterSchedule_Alarm]
	AS
	BEGIN 
		DECLARE @lErrorMessage NVARCHAR(4000)
		DECLARE @lErrorSeverity INT
		DECLARE @lErrorState INT
		DECLARE @lAlarmId INT
		DECLARE	@lCustomerId INT
		DECLARE	@lAlarmTriggerId INT
		DECLARE	@lEntityTypeId INT
		DECLARE	@lEntityId INT
		DECLARE	@lPeriodicityId INT
		DECLARE	@lPhone VARCHAR(100) 
		DECLARE	@lEmail VARCHAR(MAX)
		DECLARE @lRowVersion TIMESTAMP
		DECLARE @lPlate VARCHAR(10)
		DECLARE @lIntrackReference INT
		DECLARE @lLastReport DATETIME
		DECLARE @lInputStatus INT
		DECLARE @lVSSpeed Float
		DECLARE @tAlarm TABLE
		(
			AlarmId INT,
			CustomerId INT, 
			AlarmTriggerId INT, 
			EntityTypeId INT, 
			EntityId INT,
			PeriodicityId INT,
			Phone VARCHAR(100),
			Email VARCHAR(500),
			RowVersionAlarm VARBINARY(20)
		)
    
		BEGIN TRY 
		
			INSERT INTO @tAlarm
			(	
				AlarmId, 
				CustomerId,
				AlarmTriggerId, 
				EntityTypeId, 
				EntityId, 
				PeriodicityId, 
				Phone, 
				Email, 
				RowVersionAlarm
			)SELECT a.[AlarmId], 
					a.[CustomerId], 
					a.[AlarmTriggerId], 
					a.[EntityTypeId], 
					a.[EntityId], 
					a.[PeriodicityTypeId], 
					a.[Phone], 
					a.[Email], 
					CONVERT(VARBINARY(20),a.[RowVersion])
			FROM [General].[Alarms] a
				INNER JOIN [General].[Types] b
					  ON a.[AlarmTriggerId] = b.[TypeId]
			WHERE a.[Active] = 1
			 AND (b.[Code] = 'CUSTOMER_ALARM_TRIGGER_AFTER_HOURS')
			 AND (a.[NextAlarm] IS NULL 
			   OR a.[NextAlarm] < DATEADD(HOUR, -6, GETUTCDATE())) 
		   
			WHILE EXISTS(SELECT [AlarmId] FROM @tAlarm)
			BEGIN 
			
				SELECT TOP 1 @lAlarmId  = a.[AlarmId], 
							 @lCustomerId = a.[CustomerId],
							 @lAlarmTriggerId = a.[AlarmTriggerId],
							 @lEntityTypeId = a.[EntityTypeId],
							 @lEntityId = a.[EntityId],
							 @lPeriodicityId = a.[PeriodicityId],
							 @lPhone = a.[Phone],
							 @lEmail = a.[Email],
							 @lRowVersion = CONVERT(TIMESTAMP, a.[RowVersionAlarm])
				FROM @tAlarm a
			
				SET @lPlate = (SELECT [PlateId] FROM [General].[Vehicles] 
												WHERE [VehicleId] = @lEntityId
												  AND [Active] = 1)
				SET @lIntrackReference = (SELECT [IntrackReference]
											FROM [General].[Vehicles] 
											WHERE [VehicleId] = @lEntityId
											  AND [Active] = 1)
			
				IF @lIntrackReference IS NOT NULL
				BEGIN 
				
					SELECT 
						@lLastReport = d.[GPSDateTime], 
						@lInputStatus = d.[InputStatus],
						@lVSSpeed = d.[VSSSpeed]
					FROM [Composiciones] a
					INNER JOIN  [DispositivosAVL] b
		     			   ON a.[dispositivo] = b.[dispositivo]
					INNER JOIN [dbo].[Devices] c
						  ON b.[numeroimei] = c.[UnitID]
					INNER JOIN [dbo].[Report_Last] d
						  ON d.[Device] = c.[Device]
					WHERE a.[vehiculo] = @lIntrackReference
				
					--InputStatus 
					--IF @lLastReport IS NOT NULL AND (@lInputStatus%2) = 0
					IF @lLastReport IS NOT NULL AND @lInputStatus = 1 AND @lVSSpeed > 0
					BEGIN 
					
						DECLARE @lXMLData XML
					
						SELECT @lXMLData = XmlSchedule
						FROM [General].[VehicleSchedule] 
						WHERE [VehicleId] = @lEntityId
					
						IF @lXMLData IS NOT NULL
						BEGIN 
							DECLARE @lTimeLastReport TIME 
							DECLARE @lCountLastReport INT
							DECLARE @lDayLastReport INT
						
							SET @lDayLastReport = (DATEPART(dw, @lLastReport)-1)
							SET @lTimeLastReport = DATEADD(HOUR, -6, CONVERT(TIME, @lLastReport))
						
							SELECT @lCountLastReport = COUNT(CONVERT(TIME, RowData.col.value('./@Time', 'VARCHAR(5)')))
							FROM @lXMLData.nodes('/xmldata/Schedule') RowData(col)
							WHERE RowData.col.value('./@Day', 'INT')  = @lDayLastReport
							  AND (@lTimeLastReport BETWEEN CONVERT(TIME, RowData.col.value('./@Time', 'VARCHAR(5)'))
													   AND DATEADD(MINUTE, 30,CONVERT(TIME, RowData.col.value('./@Time', 'VARCHAR(5)'))))
							  --AND 1 = ISNULL(( SELECT TOP 1 1
									--			FROM @lXMLData.nodes('/xmldata/Schedule') RowData2(col)
									--			WHERE CONVERT(TIME,RowData2.col.value('./@Time', 'VARCHAR(5)')) = 
									--			DATEADD(MINUTE, 30,CONVERT(TIME, RowData.col.value('./@Time', 'VARCHAR(5)')))
									--		), 0)
							ORDER BY 1

							-- SI NO ESTA DENTRO DEL RANGO DEL HORARIO ENV�E LA ALERTA
							IF @lCountLastReport = 0
							BEGIN
								--SET @lEmail = 'gsolano@coinca.tv' -- PRUEBAS

								EXEC [General].[Sp_SendAlarm] @pAlarmId = @lAlarmId,
															  @pAlarmTriggerId = @lAlarmTriggerId,
															  @pCustomerId = @lCustomerId,
														      @pPlate = @lPlate,
															  @pTimeLastReport = @lLastReport,--@lTimeLastReport,	
															  @pPhone = @lPhone, 
															  @pEmail = @lEmail,
													          @pRowVersion = @lRowVersion
								
								UPDATE [General].[Alarms]
								SET [NextAlarm] = DATEADD(HOUR, -6, @lLastReport)
								WHERE AlarmId = @lAlarmId AND 
									  CustomerId = @lCustomerId AND 
									  EntityId = @lEntityId
							END					
						END 				
					END 			
				END			
				DELETE @tAlarm WHERE AlarmId = @lAlarmId AND CustomerId = @lCustomerId AND EntityId = @lEntityId
				CONTINUE 
			END 		
		END TRY 
		BEGIN CATCH 
			SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
			RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		END CATCH 		
	END


GO


