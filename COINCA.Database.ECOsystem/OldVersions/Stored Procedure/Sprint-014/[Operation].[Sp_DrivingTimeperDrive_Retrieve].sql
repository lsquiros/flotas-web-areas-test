USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_DrivingTimeperDriver_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_DrivingTimeperDriver_Retrieve]
GO

/****** Object:  StoredProcedure [Operation].[Sp_DrivingTimeperDriver_Retrieve]    Script Date: 9/16/2015 3:13:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Kevin Pe�a O.
-- Create date: 09/11/2015
-- Description:	Retrieve Working Time and DrivingTime Day 
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_DrivingTimeperDriver_Retrieve]
(
	 @pCustomerId INT = NULL
	,@pStartDateStr VARCHAR(25)= NULL
	,@pEndDateStr VARCHAR(25) = NULL  
	,@pMonth int = NULL 
)
AS
BEGIN
	
SET NOCOUNT ON

DECLARE @DrivingTimeValue datetime 
DECLARE @sumDrivingTime int = 0
DECLARE @sumWorkingDay int = 0
DECLARE @tmpvalue datetime 

IF(@pStartDateStr IS NOT NULL)
BEGIN
    SELECT  [Name] AS [EncryptedName]
            ,[Identification] AS [EncryptedIdentification]
            ,SUM([WorkingTime]) AS WorkingTime
            ,SUM([DrivingTime]) AS DrivingTime
     FROM [Operation].[PreDrivingTime_DayJob]
     WHERE 
	 Daytime BETWEEN @pStartDateStr and @pEndDateStr
	 AND CustomerId =@pCustomerId
	 AND WorkingTime >0 
	 AND DrivingTime >0
	 GROUP BY [Name],[Identification]   
END
ELSE
  BEGIN
   
     SELECT  [Name] AS [EncryptedName]
            ,[Identification] AS [EncryptedIdentification]
            ,SUM([WorkingTime]) AS WorkingTime
            ,SUM([DrivingTime]) AS DrivingTime
     FROM [Operation].[PreDrivingTime_DayJob]
     WHERE DATEPART(MONTH,[Daytime]) = @pMonth 
	 AND CustomerId = @pCustomerId
	 AND WorkingTime >0 
	 AND DrivingTime >0
     GROUP BY [Name],[Identification]     
END

END


GO


