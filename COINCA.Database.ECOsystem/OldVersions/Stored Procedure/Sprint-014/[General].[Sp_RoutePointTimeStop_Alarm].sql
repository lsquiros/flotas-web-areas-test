USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_RoutePointTimeStop_Alarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_RoutePointTimeStop_Alarm]
GO

/****** Object:  StoredProcedure [General].[Sp_RoutePointTimeStop_Alarm]    Script Date: 9/30/2015 11:31:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 02/03/2015
-- Description:	Stored Procedure that execute the alarm Add Alarm for point time stop
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_RoutePointTimeStop_Alarm]
AS
BEGIN 

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @Message VARCHAR(MAX)
        DECLARE @RowNum INT

		DECLARE @tReports TABLE
			([VehicleId] INT,
			[Device] INT,
			[GPSDateTime] DATETIME,
			[Longitude] FLOAT,
			[Latitude] FLOAT,
			[VSSSpeed] FLOAT)

		DECLARE @tStopTemp TABLE(
			[StopsTempId] INT,
			[VehicleId] INT,
			[GPSDateTimeStart] DATETIME,
			[GPSDateTimeEnd] DATETIME,
			[SpeedStart] FLOAT,
			[SpeedEnd] FLOAT,
			[Longitude] FLOAT,
			[Latitude] FLOAT)

		DECLARE @tStops TABLE
			([VehicleId] INT,
			[VehiclePlateId] VARCHAR(10),
			[VehicleName] VARCHAR(250),
			[CustomerId] INT,
			[GPSDateTimeStart] DATETIME,
			[GPSDateTimeEnd] DATETIME,
			[Longitude] FLOAT,
			[Latitude] FLOAT,
			[RouteId] INT NULL,
			[RouteName] VARCHAR(50) NULL,
			[RouteDays] VARCHAR(10) NULL,
			[RoutePointId] INT NULL,
			[RoutePointName] VARCHAR(250) NULL,
			[RoutePointTime] TIME(7) NULL,
			[RoutePointStopTime] INT NULL,
			[RoutePointLongitude] FLOAT NULL,
			[RoutePointLatitude] FLOAT NULL,
			[Alarm] BIT,
			[Send] BIT)

		DECLARE @tSendMails TABLE
			([Id] INT IDENTITY(1,1) NOT NULL,
			[StopId] INT,
			[VehiclePlateId] VARCHAR(10),
			[RouteName] VARCHAR(50),
			[RoutePointName] VARCHAR(250),
			[RoutePointStopTime] INT,
			[RealStopTime] INT,
			[Email] VARCHAR(MAX))

		INSERT INTO @tReports
			([VehicleId],
			[Device],
			[GPSDateTime],
			[Longitude],
			[Latitude],
			[VSSSpeed])
		SELECT 
			e.[VehicleId],
			a.[Device], 
			a.[GPSDateTime], 
			a.[Longitude], 
			a.[Latitude], 
			a.[VSSSpeed]
		FROM 
			[dbo].[Report_Last] a
			INNER JOIN [dbo].[Devices] b ON a.[Device] = b.[Device]
			INNER JOIN [dbo].[DispositivosAVL] c ON b.[UnitID] = c.[numeroimei]
			INNER JOIN [dbo].[Composiciones] d ON d.[dispositivo] = c.[dispositivo]
			INNER JOIN [General].[Vehicles] e ON d.[vehiculo] = e.[IntrackReference]

		UPDATE b 
		SET b.[GPSDateTimeEnd] = a.[GPSDateTime],
			b.[GPSDateTimeStart] = a.[GPSDateTime],
			b.[SpeedStart] = a.[VSSSpeed],
			b.[SpeedEnd] = a.[VSSSpeed],
			b.[ModifyUserId] = 0,
			b.[ModifyDate] = GETUTCDATE() 
		FROM @tReports a
			INNER JOIN [Efficiency].[StopsTemp] b ON a.[VehicleId] = b.[VehicleId]
		WHERE
			a.[VSSSpeed] = 0 AND
			b.[SpeedStart] <> 0

		UPDATE b 
		SET b.[GPSDateTimeEnd] = a.[GPSDateTime],
			b.[SpeedEnd] = a.[VSSSpeed],
			b.[ModifyUserId] = 0,
			b.[ModifyDate] = GETUTCDATE() 
		FROM @tReports a
			INNER JOIN [Efficiency].[StopsTemp] b ON a.[VehicleId] = b.[VehicleId]
		WHERE
			a.[VSSSpeed] > 0 AND
			b.[SpeedStart] = 0

		INSERT INTO [Efficiency].[StopsTemp]
			([VehicleId],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[SpeedStart],
			[SpeedEnd],
			[Longitude],
			[Latitude],
			[InsertUserId],
			[InsertDate])
		SELECT a.[VehicleId],
			a.[GPSDateTime],
			a.[GPSDateTime],
			a.[VSSSpeed],
			a.[VSSSpeed],
			a.[Longitude],
			a.[Latitude],
			0, GETUTCDATE()
		FROM @tReports a 
		WHERE [VehicleId] NOT IN (SELECT b.[VehicleId] FROM [Efficiency].[StopsTemp] b)

		INSERT INTO @tStopTemp
			([StopsTempId],
			[VehicleId],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[SpeedStart],
			[SpeedEnd],
			[Longitude],
			[Latitude])
		SELECT 
			[StopsTempId],
			[VehicleId],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[SpeedStart],
			[SpeedEnd],
			[Longitude],
			[Latitude]
		FROM [Efficiency].[StopsTemp] 
		WHERE 
			[SpeedStart] = 0 AND 
			[SpeedEnd] > 0

		INSERT INTO @tStops
			([VehicleId],
			[VehiclePlateId],
			[VehicleName],
			[CustomerId],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[Longitude],
			[Latitude],
			[Alarm],
			[Send])
			
		SELECT 
			a.[VehicleId],
			b.[PlateId],
			b.[Name] [VehicleName],
			b.[CustomerId],
			a.[GPSDateTimeStart],
			a.[GPSDateTimeEnd],
			a.[Longitude],
			a.[Latitude],
			0 [Alarm],
			0 [Send]
		FROM @tStopTemp a 
			INNER JOIN [General].[Vehicles] b ON a.[VehicleId] = b.[VehicleId]
			INNER JOIN [General].[ParametersByCustomer] d ON b.[CustomerId] = d.[CustomerId]
		WHERE a.[SpeedStart] = 0 AND 
			a.[SpeedEnd] > 0 AND
			d.[ResuourceKey] = 'CUSTOMER_POINT_STOP_TIME' AND
			DATEDIFF(MINUTE, a.[GPSDateTimeStart], a.[GPSDateTimeEnd]) >= CONVERT(INT, d.[Value])

		UPDATE a
		SET a.[RouteId] = c.[RouteId],
			a.[RouteName] = d.[Name],
			a.[RouteDays] = c.[Days],
			a.[RoutePointId] = e.[PointId],
			a.[RoutePointName] = e.[Name],
			a.[RoutePointTime] = e.[Time],
			a.[RoutePointStopTime] = e.[StopTime],
			a.[RoutePointLatitude] = e.[Latitude],
			a.[RoutePointLongitude] = e.[Longitude],
			a.[Alarm] =
				CASE 
					WHEN DATEDIFF(MINUTE, a.[GPSDateTimeStart], a.[GPSDateTimeEnd]) > (e.[StopTime] + CONVERT(FLOAT, g.[Value])) THEN 1 
					WHEN DATEDIFF(MINUTE, a.[GPSDateTimeStart], a.[GPSDateTimeEnd]) <= (e.[StopTime] + CONVERT(FLOAT, g.[Value])) THEN 0 
				END
		FROM @tStops a
			INNER JOIN [General].[ParametersByCustomer] b ON a.[CustomerId] = b.[CustomerId]
			INNER JOIN [Efficiency].[VehicleByRoute] c ON a.[VehicleId] = c.[VehicleId]
			INNER JOIN [Efficiency].[Routes] d ON c.[RouteId] = d.[RouteId]
			INNER JOIN [Efficiency].[RoutesPoints] e ON d.[RouteId] = e.[RouteId]
			INNER JOIN [General].[ParametersByCustomer] f ON a.[CustomerId] = f.[CustomerId]
			INNER JOIN [General].[ParametersByCustomer] g ON a.[CustomerId] = g.[CustomerId]
		WHERE 
			b.[ResuourceKey] = 'CUSTOMER_UTC' AND 
			c.[Days] LIKE 
				CASE DATEPART(DW,DATEADD(HOUR, CONVERT(int,b.[Value]), GETUTCDATE()))
					WHEN 1 THEN '%D%'
					WHEN 2 THEN '%L%'
					WHEN 3 THEN '%K%'
					WHEN 4 THEN '%M%'
					WHEN 5 THEN '%J%'
					WHEN 6 THEN '%V%'
					WHEN 7 THEN '%S%'
				END  AND 
			f.[ResuourceKey] = 'CUSTOMER_POINT_DISTANCE_MARGIN' AND 
			g.[ResuourceKey] = 'CUSTOMER_POINT_DELAY' AND 
			geography::Point(a.[Latitude], a.[Longitude], 4326).STDistance(geography::Point(e.[Latitude], e.[Longitude], 4326)) <= CONVERT(FLOAT, f.[Value])

		INSERT INTO [Efficiency].[Stops]
			([VehicleId],
			[VehiclePlateId],
			[VehicleName],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[Longitude],
			[Latitude],
			[RouteId],
			[RouteName],
			[RouteDays],
			[RoutePointId],
			[RoutePointName],
			[RoutePointTime],
			[RoutePointStopTime],
			[RoutePointLongitude],
			[RoutePointLatitude],
			[Alarm],
			[Send])
		SELECT 
			[VehicleId],
			[VehiclePlateId],
			[VehicleName],
			[GPSDateTimeStart],
			[GPSDateTimeEnd],
			[Longitude],
			[Latitude],
			[RouteId],
			[RouteName],
			[RouteDays],
			[RoutePointId],
			[RoutePointName],
			[RoutePointTime],
			[RoutePointStopTime],
			[RoutePointLongitude],
			[RoutePointLatitude],
			[Alarm],
			0
		FROM @tStops

		UPDATE
			[Efficiency].[StopsTemp] 
		SET 
			[SpeedStart] = [SpeedEnd],
			[ModifyDate] = GETUTCDATE()
		WHERE 
			[SpeedStart] = 0 AND 
			[SpeedEnd] > 0

		SELECT @Message = b.Message
			FROM [General].[Types] a 
			INNER JOIN [General].[Values] b ON a.[TypeId] = b.[TypeId]
		WHERE a.[Code] = 'CUSTOMER_ALARM_TRIGGER_POINT_TIME_STOP'

		INSERT INTO @tSendMails
			([StopId],
			[VehiclePlateId],
			[RouteName],
			[RoutePointName],
			[RoutePointStopTime],
			[RealStopTime],
			[Email])
		SELECT [StopId],
			[VehiclePlateId], 
			[RouteName], 
			[RoutePointName], 
			[RoutePointStopTime], 
			DATEDIFF(MINUTE, [GPSDateTimeStart], [GPSDateTimeEnd]) [RealStopTime], 
			e.[Email] 
		FROM [Efficiency].[Stops] a 
			INNER JOIN [General].[Vehicles] b ON a.[VehicleId] = b.[VehicleId]
			INNER JOIN [General].[CustomerUsers] c ON b.[CustomerId] = c.[CustomerId]
			INNER JOIN [General].[Users] d ON c.[UserId] = d.[UserId]
			INNER JOIN [AspNetUsers] e ON d.[AspNetUserId] = e.[Id]
		WHERE 
			a.[Alarm] = 1 AND 
			a.[Send] = 0

		Select @RowNum = Count(*) From @tSendMails
		
		WHILE @RowNum > 0
		BEGIN
			DECLARE @lMessage VARCHAR(MAX) = @Message
			DECLARE @lStopId INT = NULL
			DECLARE @lPlateId VARCHAR(10) = NULL
			DECLARE @lRuteName VARCHAR(50) = NULL
			DECLARE @lPointName VARCHAR(250) = NULL
			DECLARE @lRouteTime INT = NULL
			DECLARE @lStopTime INT = NULL
			DECLARE @lEmail VARCHAR(MAX) = NULL

			SELECT 
				@lStopId = [StopId],
				@lPlateId = [VehiclePlateId],
				@lRuteName = [RouteName],
				@lPointName = [RoutePointName],
				@lRouteTime = [RoutePointStopTime],
				@lStopTime = [RealStopTime],
				@lEmail = [Email]
			FROM @tSendMails 
			WHERE [Id] = @RowNum
			
			SET @lMessage = @Message
			SET @lMessage = REPLACE(@lMessage, '%PlateId%', @lPlateId)
			SET @lMessage = REPLACE(@lMessage, '%RuteName%', @lRuteName)
			SET @lMessage = REPLACE(@lMessage, '%PointName%', @lPointName)
			SET @lMessage = REPLACE(@lMessage, '%RouteTime%', @lRouteTime)
			SET @lMessage = REPLACE(@lMessage, '%StopTime%', @lStopTime)
			
			INSERT INTO [General].EmailEncryptedAlarms
				([To], 
				[Subject], 
				[Message], 
				[Sent],
				[InsertDate])
			VALUES
				(@lEmail,
				'Alarma de Tiempo de Parada',
				@lMessage,
				0,
				GETUTCDATE())
				
			UPDATE [Efficiency].[Stops] SET [Send] = 1 WHERE [StopId] = @lStopId

			SET @RowNum = @RowNum -1

		END
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF

END

GO


