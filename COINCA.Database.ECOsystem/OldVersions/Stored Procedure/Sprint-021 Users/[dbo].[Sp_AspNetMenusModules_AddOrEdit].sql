USE [ECOsystemQA]

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_AspNetMenusModules_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_AspNetMenusModules_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 17/12/2015
-- Description:	Insert or Update Modules
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_AspNetMenusModules_AddOrEdit]
(
	 @pModuleId INT			
	,@pName VARCHAR(100)
	,@pIco VARCHAR(100)
	,@pResource VARCHAR(100) = null
	,@pAction VARCHAR(100) = null
	,@pRoleName VARCHAR(100) 
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END

			DECLARE @PermissionId VARCHAR(100)
			SELECT @PermissionId = Id FROM [dbo].[AspNetPermissions] WHERE [Resource] = @pResource AND [Action] = @pAction

			DECLARE @RoleId VARCHAR(100)
			SELECT @RoleId = Id FROM [dbo].[Aspnetroles] WHERE Name = @pRoleName

			IF @pModuleId = 0
			BEGIN
				
				DECLARE @order INT 
				SELECT TOP 1 @order = [Order] FROM dbo.AspMenusModules ORDER BY [Order] DESC
				SET @order =  @order + 1

				INSERT INTO [dbo].[AspMenusModules] 
				VALUES ( @pName
						 ,@pIco
						 ,@PermissionId
						 ,@order)
			   
			   SELECT @pModuleId = Id FROM [dbo].[AspMenusModules] WHERE Name = @pName

			   INSERT INTO [dbo].[AspModulesByRoles]
				VALUES (@RoleId, @pModuleId)
			    	
	
			END
			ELSE
			BEGIN
				UPDATE [dbo].[AspMenusModules]
				SET Name = @pName
				    ,Ico = @pIco
					,PermissionId = @PermissionId
				WHERE Id = @pModuleId				

			END
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO
