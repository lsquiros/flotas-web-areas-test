
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Users_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [General].[Sp_Users_Retrieve]    Script Date: 1/11/2016 12:19:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Andres Oviedo B.
-- Create date: 09/01/2015
-- Description:	Retrieve User information
-- Modify by: Stefano Quiros Ruiz
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Users_Retrieve]
(
	  @pUserId INT = NULL
	 ,@pKey VARCHAR(800) = NULL
	 ,@pUserName VARCHAR(800) = NULL
	 ,@pCustomerId INT = NULL
	 ,@pCustomerId2 INT = NULL
	 ,@pPartnerId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON	

	IF(@pCustomerId2 IS NULL)
	BEGIN
		SELECT
			 a.[UserId]
			,a.[Name] AS [EncryptedName]
			,a.[ChangePassword]
			--,a.[Photo] --GSOLANO: Afecta Rendimiento en la consulta
			,'' AS Photo
			,a.[IsActive]
			,b.[Email] AS [EncryptedEmail]
			,b.[PhoneNumber] AS [EncryptedPhoneNumber]
			,CONVERT(BIT,CASE WHEN (a.[IsLockedOut] = 1 OR b.[LockoutEndDateUtc] IS NOT NULL) THEN 1 ELSE 0 END) AS [IsLockedOut]
			,b.[UserName] AS [EncryptedUserName]
			,d.[Id] AS [RoleId]
			,d.[Name] AS [RoleName]
			,CONVERT(BIT,CASE WHEN e.[CustomerUserId] IS NOT NULL THEN 1 ELSE 0 END) AS IsCustomerUser
			,CONVERT(BIT,CASE WHEN f.[DriversUserId] IS NOT NULL THEN 1 ELSE 0 END) AS IsDriverUser
			,CONVERT(BIT,CASE WHEN g.[PartnerUserId] IS NOT NULL THEN 1 ELSE 0 END) AS IsPartnerUser
			,a.[PasswordExpirationDate]
			,CONVERT(BIT,CASE WHEN a.[PasswordExpirationDate] < GETDATE() THEN 1 ELSE 0 END) AS IsPasswordExpired
			,CASE
				WHEN e.[CustomerUserId] IS NOT NULL THEN i.Logo
				WHEN f.[DriversUserId] IS NOT NULL THEN j.Logo
				WHEN g.[PartnerUserId] IS NOT NULL THEN h.Logo
				ELSE NULL
			 END AS EntityLogo
			,a.[RowVersion]
		FROM [General].[Users] a
			INNER JOIN [dbo].[AspNetUsers] b
				ON a.[AspNetUserId] = b.[Id]
			INNER JOIN [dbo].[AspNetUserRoles] c
				ON c.UserId = a.AspNetUserId
			INNER JOIN [dbo].[AspNetRoles] d
				ON d.[Id] = c.[RoleId]
			LEFT JOIN [General].[CustomerUsers] e
				ON e.[UserId] = a.[UserId]
			LEFT JOIN [General].[DriversUsers] f
				ON f.[UserId] = a.[UserId]
			LEFT JOIN [General].[PartnerUsers] g
				ON g.[UserId] = a.[UserId]
			LEFT JOIN [General].[Partners] h
				ON g.PartnerId = h.PartnerId
			LEFT JOIN [General].[Customers] i
				ON i.CustomerId = e.CustomerId
			LEFT JOIN [General].[Customers] j
				ON j.CustomerId = f.CustomerId
		WHERE a.[IsDeleted] = 0
		  AND (@pUserId IS NULL OR a.[UserId] = @pUserId)
		  AND (@pUserName IS NULL OR b.[UserName] = @pUserName)
		  AND (@pKey IS NULL
					OR a.[Name] like '%'+@pKey+'%'
					OR b.[Email] like '%'+@pKey+'%'
					OR b.[PhoneNumber] like '%'+@pKey+'%'
					OR b.[UserName] like '%'+@pKey+'%'
					OR e.[Identification] like '%'+@pKey+'%'
					OR f.[Identification] like '%'+@pKey+'%'
					OR f.[Code] like '%'+@pKey+'%'
					OR f.[License] like '%'+@pKey+'%'
					OR f.[Dallas] like '%'+@pKey+'%')
		  AND (@pCustomerId IS NULL
					OR e.[CustomerId] = @pCustomerId)
		  AND (@pCustomerId2 IS NULL
					OR f.[CustomerId] = @pCustomerId2)
		  AND (@pPartnerId IS NULL
					OR g.[PartnerId] = @pPartnerId)
		  AND d.[Name] <> 'CUSTOMER_CHANGEODOMETER'
		ORDER BY [UserId] DESC
	END 
	ELSE
	BEGIN
		SELECT
			 a.[UserId]
			,a.[Name] AS [EncryptedName]
			,a.[ChangePassword]
			--,a.[Photo] --GSOLANO: Afecta Rendimiento en la consulta
			,'' AS Photo
			,a.[IsActive]
			,b.[Email] AS [EncryptedEmail]
			,b.[PhoneNumber] AS [EncryptedPhoneNumber]
			,f.[License] AS [EncryptedLicense]
			,f.[LicenseExpiration]
			,CONVERT(BIT,CASE WHEN (a.[IsLockedOut] = 1 OR b.[LockoutEndDateUtc] IS NOT NULL) THEN 1 ELSE 0 END) AS [IsLockedOut]
			,b.[UserName] AS [EncryptedUserName]
			,d.[Id] AS [RoleId]
			,d.[Name] AS [RoleName]
			,CONVERT(BIT,CASE WHEN f.[DriversUserId] IS NOT NULL THEN 1 ELSE 0 END) AS IsDriverUser
			,a.[PasswordExpirationDate]
			,CONVERT(BIT,CASE WHEN a.[PasswordExpirationDate] < GETDATE() THEN 1 ELSE 0 END) AS IsPasswordExpired
			,a.[RowVersion]
		FROM [General].[Users] a
			INNER JOIN [dbo].[AspNetUsers] b
				ON a.[AspNetUserId] = b.[Id]
			INNER JOIN [dbo].[AspNetUserRoles] c
				ON c.UserId = a.AspNetUserId
			INNER JOIN [dbo].[AspNetRoles] d
				ON d.[Id] = c.[RoleId]
			LEFT JOIN [General].[DriversUsers] f
				ON f.[UserId] = a.[UserId]
		WHERE a.[IsDeleted] = 0
		   AND (@pUserId IS NULL
					OR a.[UserId] = @pUserId)
		   AND (@pUserName IS NULL OR b.[UserName] = @pUserName)
		   AND (@pCustomerId2 IS NULL
					OR f.[CustomerId] = @pCustomerId2)
		  AND d.[Name] <> 'CUSTOMER_CHANGEODOMETER'
		ORDER BY [UserId] DESC
	END
    SET NOCOUNT OFF
END


GO


