
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionPriceByLiterValidation_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionPriceByLiterValidation_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 03/06/2015
-- Description:	Retrieve Validation Transaction Price by Liter
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionPriceByLiterValidation_Retrieve]
(
	@pVehicleId INT,
	@pLiters FLOAT,
	@pFuelAmount FLOAT
)
AS
BEGIN
	SET NOCOUNT ON

	--DECLARE @pVehicleId INT = 2350 
	--DECLARE @pLiters FLOAT = 22.88
	--DECLARE @pFuelAmount FLOAT = 5400

			SELECT 
				ISNULL(SUM([reg]), 0) [reg] 
			FROM 
				(
					SELECT TOP 1 1 [reg]
					FROM 
						General.Vehicles a
						INNER JOIN General.CustomersByPartner b ON a.CustomerId = b.CustomerId
						INNER JOIN Control.PartnerFuel c ON b.PartnerId = c.PartnerId
						INNER JOIN General.VehicleCategories d ON a.VehicleCategoryId = d.VehicleCategoryId AND c.FuelId = d.DefaultFuelId
						INNER JOIN General.Partners e ON b.PartnerId = e.PartnerId
					WHERE 
						a.VehicleId = @pVehicleId AND 
						b.IsDefault = 1 AND 
						c.EndDate IS NULL AND
						ABS((((@pFuelAmount / @pLiters) / c.LiterPrice) - 1) * 100) <= e.FuelErrorPercent
					UNION
					SELECT TOP 1 1 [reg]
					FROM	 
						General.Vehicles a
						INNER JOIN General.CustomersByPartner b ON a.CustomerId = b.CustomerId
						INNER JOIN Control.PartnerFuel c ON b.PartnerId = c.PartnerId
						INNER JOIN General.FuelsByVehicleCategory d ON a.VehicleCategoryId = d.VehicleCategoryId AND c.FuelId = d.FuelId
						INNER JOIN General.Partners e ON b.PartnerId = e.PartnerId
					WHERE 
						a.VehicleId = @pVehicleId AND 
						b.IsDefault = 1 AND 
						c.EndDate IS NULL AND
						ABS((((@pFuelAmount / @pLiters) / c.LiterPrice) - 1) * 100) <= e.FuelErrorPercent
				) a
	
	SET NOCOUNT OFF
END

