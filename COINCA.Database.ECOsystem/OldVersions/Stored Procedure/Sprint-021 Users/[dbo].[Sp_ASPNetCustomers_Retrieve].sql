USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetCustomers_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetCustomers_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Juan Jos� Arias Solano
-- Create date: 06/01/2016
-- Description:	Customers Retrieve
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ASPNetCustomers_Retrieve](
	@pPartnerId  int = null
)	
AS
BEGIN	
	SET NOCOUNT ON;

		SELECT A.[CustomerId],
			   [Name] as EncryptedCustomerName
		FROM   [General].[Customers] A 
			   INNER JOIN [General].[CustomersByPartner] B
			   ON A.[CustomerId] = B.[CustomerId]
		WHERE  B.[PartnerId] = @pPartnerId

	SET NOCOUNT OFF
END





