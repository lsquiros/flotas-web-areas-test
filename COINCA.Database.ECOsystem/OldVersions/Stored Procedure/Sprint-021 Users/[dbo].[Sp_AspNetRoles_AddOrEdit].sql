USE [ECOsystemQA]

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_AspNetRoles_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_AspNetRoles_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 17/12/2015
-- Description:	Insert or Update Roles
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_AspNetRoles_AddOrEdit]
(
	 @pRoleId VARCHAR(128) = null		
	,@pName VARCHAR(100)
	,@pCreatedBy INT
	,@pParent VARCHAR(100)
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END

			IF @pRoleId IS NULL
			BEGIN	

				INSERT INTO [dbo].[AspNetRoles] 
				VALUES ( NewID() 
						,@pName
						,0
						,@pParent
						,1
						,@pCreatedBy
						 )	
			END
			ELSE
			BEGIN
				UPDATE [dbo].[AspNetRoles]
				SET [Name] = @pName
				    ,[Parent] = @pParent				    
				WHERE Id = @pRoleId				

			END
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO
