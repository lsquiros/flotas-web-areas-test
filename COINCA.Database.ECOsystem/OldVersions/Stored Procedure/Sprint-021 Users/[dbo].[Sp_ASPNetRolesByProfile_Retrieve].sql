USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetRolesByProfile_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetRolesByProfile_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 17/12/2015
-- Description:	Roles Retrieve by Profile (Master Role)
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ASPNetRolesByProfile_Retrieve]
(    
	@pRoleName varchar(50) = null	
)	
AS
BEGIN	
	SET NOCOUNT ON;
	
	DECLARE @RoleParent VARCHAR(128)

	--VERIFIES IF THE ROLES NAME GET IS A MASTER ROLE 
	IF NOT EXISTS (SELECT TOP 1 * FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName AND [Master] = 1) 
	BEGIN		
		SELECT @RoleParent = [Parent] FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName
		SELECT @pRoleName = [Name] FROM [dbo].[AspNetRoles] WHERE [Id] = @RoleParent
	END	

	IF @pRoleName = 'SUPER_ADMIN'
	BEGIN
		SELECT [Id] as [RoleId], [Name] FROM [dbo].[AspNetRoles] WHERE [Active] = 1  AND [Master] = 1 ORDER BY [Name] DESC
	END 

	IF @pRoleName = 'PARTNER_ADMIN'
	BEGIN
		SELECT [Id] as [RoleId], [Name] FROM [dbo].[AspNetRoles] WHERE ([Name] like 'PART%' OR [Name] LIKE 'CUST%') AND [Active] = 1 AND [Master] = 1 ORDER BY [Name] DESC
	END	

	IF @pRoleName = 'CUSTOMER_ADMIN'
	BEGIN
		SELECT [Id] as [RoleId], [Name] FROM [dbo].[AspNetRoles] WHERE [Name] LIKE 'CUST%' AND [Active] = 1 AND [Master] = 1 ORDER BY [Name] DESC
	END	
	
	SET NOCOUNT OFF
END

GO
