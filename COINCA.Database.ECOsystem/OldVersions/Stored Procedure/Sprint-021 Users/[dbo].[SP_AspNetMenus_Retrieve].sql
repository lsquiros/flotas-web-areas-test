
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AspNetMenus_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_AspNetMenus_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO 
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 12/14/2015
-- Description:	Retrieve ASPNET Menus
-- ================================================================================================
CREATE PROCEDURE [dbo].[SP_AspNetMenus_Retrieve]
(
	@pRoleName varchar(100) = null,
	@pHeader	int = null,
	@pParent	int = null
)	
AS
BEGIN	
	SET NOCOUNT ON;

	select m.[Id] as [MenuId]
		   ,m.[Name]
		   ,m.[Ico]
		   ,p.[Resource] as [Controller]
		   ,p.[Action]		   
		   ,m.[Parent]
		   ,m.[Header]
		   ,m.[Order] 
	from [dbo].[aspmenus] as m
	left outer join [dbo].[aspnetpermissions] as p
	on m.[PermissionId] = p.[Id]
	inner join [dbo].[aspmenusbyroles] as mr
	on mr.[MenuId] = m.[Id]
	inner join [dbo].[AspNetRoles] as r
	on mr.[RoleId] = r.[Id]
	
	where (@pRoleName is NULL OR r.[Name] = @pRoleName)
		  and (@pHeader is NULL OR m.[Header] = @pHeader)
		  and (@pParent is NULL OR m.[Parent] = @pParent)

	order by m.[Id], m.[Parent]


	SET NOCOUNT OFF
END
GO