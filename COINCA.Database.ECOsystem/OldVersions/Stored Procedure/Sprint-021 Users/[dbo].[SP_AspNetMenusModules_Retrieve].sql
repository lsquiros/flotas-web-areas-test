USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AspNetMenusModules_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_AspNetMenusModules_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO 
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 12/16/2015
-- Description:	Retrieve ASPNET Modules
-- ================================================================================================
CREATE PROCEDURE [dbo].[SP_AspNetMenusModules_Retrieve]
(
	@pRoleName varchar(20) = null,
	@pKey varchar(100) = null,
	@pModuleId int = null
)	
AS
BEGIN	
	SET NOCOUNT ON;

	select distinct m.[Id] as [ModuleId]
		   ,m.[Name]
		   ,m.[Ico]
		   ,p.[Resource] as [Controller]
		   ,p.[Action] 
		   ,m.[Order] 
	from [dbo].[AspMenusModules] as m
	left outer join [dbo].[AspNetPermissions] as p
	on m.[PermissionId] = p.[Id]
	inner join [dbo].[AspModulesByRoles] as mr
	on mr.[ModuleId] = m.[Id]
	inner join [dbo].[AspNetRoles] as r
	on mr.[RoleId] = r.[Id]
	
	where (@pRoleName is NULL OR r.[Name] = @pRoleName)
		  and (@pModuleId is NULL OR m.[Id] = @pModuleId)
		  and (@pKey is NULL 
			  or m.[Name] LIKE '%'+@pKey+'%'
			  or p.[Resource] LIKE '%'+@pKey+'%'
			  or p.[Action]  LIKE '%'+@pKey+'%')
		  and (m.[Active] = 1)
			   
	order by m.[Order], m.[Name]


	SET NOCOUNT OFF
END
GO
