USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetCustomersByPartner_Update]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetCustomersByPartner_Update]
GO

/****** Object:  StoredProcedure [dbo].[Sp_ASPNetCustomersByPartner_Update]    Script Date: 1/8/2016 12:03:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Juan Jos� Arias Solano
-- Create date: 07/01/2016
-- Description:	Customers by partner Update
-- Modify by: Stefano Quiros Ruiz
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ASPNetCustomersByPartner_Update](
	@pUserId     int = null,
	@pCustomerId int = null,
	@pRoleId	 nvarchar(128) = null,
	@pDelete     bit = null
)	
AS
BEGIN	
	SET NOCOUNT ON;

		IF @pDelete = 1
		BEGIN
			DELETE [dbo].[CustomerAuthByPartner]
			WHERE  [PartnerUserId] = @pUserId AND [CustomerId] = @pCustomerId;
		END;  

		ELSE
		BEGIN
			INSERT INTO [dbo].[CustomerAuthByPartner](
						[PartnerUserId],
						[CustomerId],
						[RoleId])
			VALUES(	
						@pUserId,
						@pCustomerId,
						@pRoleId);
		END;

	SET NOCOUNT OFF
END
GO


