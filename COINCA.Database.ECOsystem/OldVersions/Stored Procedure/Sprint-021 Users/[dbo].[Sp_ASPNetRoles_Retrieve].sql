USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetRoles_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetRoles_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Stefano Quiros
-- Create date: 17/12/2015
-- Description:	Roles Retrieve
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ASPNetRoles_Retrieve]
(
    @pRoleId VARCHAR(128) = NULL,
	@pRoleName VARCHAR(100) = NULL,
	@pCreatedBy INT = NULL,
	@pParent VARCHAR(100) = NULL
)	
AS
BEGIN	
	SET NOCOUNT ON;

	DECLARE @ParentId VARCHAR(100)

	IF @pParent IS NOT NULL
	BEGIN
		SET @ParentId = (SELECT [Id] FROM [dbo].[AspNetRoles] WHERE [Name]  = @pParent)
	END

	SELECT anr.[Id] AS [RoleId]
		   ,anr.[Name]
		   ,anr.[Parent] 
	FROM [dbo].[AspNetRoles] AS anr
		
	WHERE (@pCreatedBy IS NULL OR anr.[CreatedBy] = @pCreatedBy)
	  AND (@pRoleName IS NULL OR anr.[Name] LIKE'%'+ @pRoleName +'%')
	  AND (@pRoleId IS NULL OR anr.[Id] = @pRoleId)
	  AND (@pParent IS NULL OR anr.[Parent] LIKE'%'+ @ParentId +'%')
	  AND [Active] = 1

	ORDER BY anr.[Id]

	SET NOCOUNT OFF
END


GO


