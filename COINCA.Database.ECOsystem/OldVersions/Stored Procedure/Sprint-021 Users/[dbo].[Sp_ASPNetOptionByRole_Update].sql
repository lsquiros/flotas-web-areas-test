USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetOptionByRole_Update]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetOptionByRole_Update]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO 
-- ================================================================================================
-- Author:		Juan Jos� Arias Solano	
-- Create date: 28/12/2015
-- Description:	Option By Role Update
-- ================================================================================================

CREATE PROCEDURE [dbo].[Sp_ASPNetOptionByRole_Update]
(
	@pMenuId int = NULL, 
	@pView   bit = NULL,      
	@pEdit   bit = NULL,      
	@pDelete bit = NULL,    
	@pRole  varchar(120) = NULL,
	@pModuleId int = NULL,
	@pPrimera bit = NULL
)
AS 
BEGIN
SET NOCOUNT ON;

	DECLARE @MenuPermission varchar(100);
	DECLARE @Resource       varchar(100);
	DECLARE @Action         varchar(100);
	DECLARE @IdRol			varchar(120);
	DECLARE @Parent			int;


	SET	@IdRol = (SELECT [Id]
				  FROM [dbo].[AspNetRoles]
				  WHERE [Name] = @pRole);

	SET @Parent = (SELECT ISNULL([Parent],0) 
				   FROM [dbo].[AspMenus]
				   WHERE [id] = @pMenuId);

	SET @MenuPermission = (SELECT [PermissionId] 
						   FROM [dbo].[AspMenus]
						   WHERE [id] = @pMenuId);


						   select *  FROM [dbo].[AspMenus]

	SET @Resource = (SELECT LTRIM(RTRIM([Resource])) 
					 FROM [dbo]. [AspNetPermissions]
					 WHERE Id = @MenuPermission);

	SET @Action = (SELECT LTRIM(RTRIM([Action])) 
					 FROM [dbo]. [AspNetPermissions]
					 WHERE [Id] = @MenuPermission);


	--DELETE [dbo].[AspNetRolePermissions]
	--WHERE RoleId = @IdRol AND PermissionId IN (SELECT id 
	--										   FROM aspnetpermissions 
	--										   WHERE resource = @Resource);

	IF @pPrimera = 1
	BEGIN
		
		DELETE  [dbo].[AspNetRolePermissions]
	    WHERE RoleId = @IdRol AND PermissionId IN (SELECT id 
											       FROM aspnetpermissions 
											       WHERE resource IN (SELECT DISTINCT b.resource
																	  FROM [dbo].[AspMenus] a inner join aspnetpermissions b 
																      ON a.permissionId = b.id
															          WHERE a.Header = @pModuleId));
		IF @pMenuId IS NOT NULL 
		BEGIN
			INSERT INTO [dbo].[AspModulesByRoles](
						[RoleId],
						[ModuleId])
			VALUES(
						@IdRol,
						@pModuleId);
		END;
	END;

	IF @Parent <> 0 
	BEGIN
		IF NOT EXISTS (SELECT * FROM AspMenusByRoles WHERE RoleId = @IdRol AND MenuId = @Parent)
		BEGIN
			IF @Parent <> @pMenuId
			BEGIN
				INSERT INTO [dbo].[AspMenusByRoles](
								  [RoleId],
								  [MenuId])
				VALUES(
								  @IdRol,
							      @Parent);
			END;
		END;
	END;

	IF @pMenuId IS NOT NULL 
    BEGIN
		INSERT INTO [dbo].[AspMenusByRoles](
					[RoleId],
					[MenuId])
		VALUES(
					@IdRol,
					@pMenuId);
	END;


	IF @pView = 1
		BEGIN
				IF @Action = 'TransactionRules'
				BEGIN
					INSERT INTO [dbo].[AspNetRolePermissions](
								[RoleId],
								[PermissionId])
					VALUES(
								@IdRol,
								(SELECT Id FROM AspNetPermissions 
								 WHERE [Resource] = @Resource AND [Action] = @Action));
				END
				ELSE
				BEGIN
					DECLARE @ViewCount INT;
					SET @ViewCount = (SELECT COUNT(*) FROM AspNetPermissions WHERE  Name = 'View_' + @Resource AND [Resource] = @Resource AND [Action] LIKE 'Load%');
				
					IF(@ViewCount = 0)
					BEGIN
						INSERT INTO [dbo].[AspNetRolePermissions](
									[RoleId],
									[PermissionId])
						VALUES(
									@IdRol,
									(SELECT Id FROM AspNetPermissions 
									 WHERE  Name = 'View_' + @Resource AND [Resource] = @Resource AND [Action] = @Action));
					END
					ELSE
					BEGIN
						INSERT INTO [dbo].[AspNetRolePermissions]
									SELECT @IdRol, Id FROM AspNetPermissions 
									WHERE  Name LIKE 'View_%' AND [Resource] = @Resource;
					END
				END
		END;

	IF @pEdit = 1
		BEGIN
			IF @Resource <> 'CreditCard'
			BEGIN
				INSERT INTO [dbo].[AspNetRolePermissions]
				            SELECT @IdRol, Id FROM AspNetPermissions 
							WHERE  Name Like 'Load_%' AND [Resource] = @Resource;

				INSERT INTO [dbo].[AspNetRolePermissions]
							SELECT @IdRol, Id FROM AspNetPermissions 
							WHERE  Name like 'Add%'  AND [Resource] = @Resource;
			END;
			ELSE
			BEGIN
				IF @Action <> 'TransactionRules'
				BEGIN
					IF @Action = 'Admin'
					BEGIN
						INSERT INTO [dbo].[AspNetRolePermissions]
									SELECT @IdRol, Id FROM AspNetPermissions 
									WHERE  Name Like 'Load_%' AND [Resource] = @Resource
									AND    [Action] Like '%Request%';

						INSERT INTO [dbo].[AspNetRolePermissions]
									SELECT @IdRol, Id FROM AspNetPermissions 
									WHERE  Name like 'Add%'  AND [Resource] = @Resource
									AND    [Action] Like '%Request%';
					END;
					ELSE
					BEGIN
						INSERT INTO [dbo].[AspNetRolePermissions]
									SELECT @IdRol, Id FROM AspNetPermissions 
									WHERE  Name Like 'Load_%' AND [Resource] = @Resource
									AND    [Action] Not Like '%Request%';

						INSERT INTO [dbo].[AspNetRolePermissions]
									SELECT @IdRol, Id FROM AspNetPermissions 
									WHERE  Name like 'Add%'  AND [Resource] = @Resource
									AND    [Action] Not Like '%Request%';
					END;
				END
			END;
		END;

	IF @pDelete = 1
		BEGIN
			IF @Resource <> 'CreditCard'
			BEGIN
				INSERT INTO [dbo].[AspNetRolePermissions]
							SELECT @IdRol, Id FROM AspNetPermissions 
							WHERE  Name like 'Delete_%' AND [Resource] = @Resource;
			END;
			ELSE
			BEGIN
				IF @Action <> 'TransactionRules'
				BEGIN
					IF @Action = 'Admin'
					BEGIN
						INSERT INTO [dbo].[AspNetRolePermissions]
									SELECT @IdRol, Id FROM AspNetPermissions 
									WHERE  Name Like 'Delete_%' AND [Resource] = @Resource
									AND    [Action] Like '%Request%';
					END;
					ELSE
					BEGIN
						INSERT INTO [dbo].[AspNetRolePermissions]
									SELECT @IdRol, Id FROM AspNetPermissions 
									WHERE  Name Like 'Delete_%' AND [Resource] = @Resource
									AND    [Action] Not Like '%Request%';
					END;
				END
			END;
		END;

END;



GO
