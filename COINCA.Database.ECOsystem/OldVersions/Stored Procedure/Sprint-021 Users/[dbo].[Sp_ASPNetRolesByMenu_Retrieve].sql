USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetRolesByMenu_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetRolesByMenu_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 23/12/2015
-- Description:	Roles Retrieve by Menu
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ASPNetRolesByMenu_Retrieve]
(    
	@pRoleName varchar(50) = null,
	@pCreatedBy int	
)	
AS
BEGIN	
	SET NOCOUNT ON;
	
	DECLARE @RoleParent VARCHAR(128)
	DECLARE @Role VARCHAR(50)
	
	--Verifies if the role asociated with the user is a master role, if it is gets the information about the role to retrieve all the roles
	IF NOT EXISTS (SELECT TOP 1 * FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName AND [Master] = 1) 
	BEGIN		
		SET @RoleParent = (SELECT [Parent] FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName)
		SET @Role = (SELECT [Name] FROM [dbo].[AspNetRoles] WHERE [Id] = @RoleParent)
	END	
	ELSE
	BEGIN
		SET @RoleParent = (SELECT [Id] FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName)
		SET @Role = (SELECT [Name] FROM [dbo].[AspNetRoles] WHERE [Id] = @RoleParent)
	END

	IF @Role = 'SUPER_ADMIN'
	BEGIN			
		SELECT [Id] as [RoleId], [Name] FROM [dbo].[AspNetRoles] WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END 

	IF @Role = 'SUPER_USER'
	BEGIN			
		SELECT [Id] as [RoleId], [Name] FROM [dbo].[AspNetRoles] WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END 

	IF @Role = 'PARTNER_ADMIN'
	BEGIN		
		SELECT [Id] as [RoleId], [Name] FROM [dbo].[AspNetRoles] WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END	

	IF @Role = 'PARTNER_USER'
	BEGIN		
		SELECT [Id] as [RoleId], [Name] FROM [dbo].[AspNetRoles] WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END	

	IF @Role = 'CUSTOMER_ADMIN'
	BEGIN		
		SELECT [Id] as [RoleId], [Name] FROM [dbo].[AspNetRoles] WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END	

	IF @Role = 'CUSTOMER_USER'
	BEGIN		
		SELECT [Id] as [RoleId], [Name] FROM [dbo].[AspNetRoles] WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END	
		
	SET NOCOUNT OFF
END

GO
