USE [ECOsystemQA]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetUserPermissions_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetUserPermissions_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 12/07/2015
-- Description:	Retrieve ASPNET User Permissions
-- ================================================================================================
Create PROCEDURE [dbo].[Sp_ASPNetUserPermissions_Retrieve]
(
	@pRoleName varchar(50) = null,
	@pHeader	int = null,
	@pParent	int = null
)	
AS
BEGIN	
	SET NOCOUNT ON;

	DECLARE @Role VARCHAR(50)

	IF EXISTS (SELECT TOP 1 [Name] FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName AND [Master] = 1)	
		SET @Role = @pRoleName	
	ELSE
	BEGIN
		DECLARE @RoleId VARCHAR(128)
		SET @RoleId = (SELECT [Parent] FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName)
		SET @Role = (SELECT [Name] FROM [dbo].[AspNetRoles] WHERE [Id] = @RoleId)
	END	

	select m.[Id] as [MenuId]
		   ,m.[Name]
		   ,m.[Ico]
		   ,p.[Resource] as [Controller]
		   ,p.[Action]		   
		   ,m.[Parent]
		   ,m.[Header]
		   ,m.[Order] 
		   ,[dbo].[Fn_RetrieveCheckStatus](m.[PermissionId],@pRoleName) as [IsChecked]
		   ,[dbo].[Fn_RetrieveCheckStatus](m.[PermissionId], @Role) as [ValidFields]
	from [dbo].[aspmenus] as m
	left outer join [dbo].[AspNetPermissions] as p
	on m.[PermissionId] = p.[Id]
	inner join [dbo].[AspMenusByRoles] as mr
	on mr.[MenuId] = m.[Id]
	inner join [dbo].[AspNetRoles] as r
	on mr.[RoleId] = r.[Id]
	
	where (@Role is NULL OR r.[Name] = @Role)
		  and (@pHeader is NULL OR m.[Header] = @pHeader)
		  and (@pParent is NULL OR m.[Parent] = @pParent)

	order by m.[Id], m.[Parent]

SET NOCOUNT OFF
END

GO