USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetCustomersByPartner_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetCustomersByPartner_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Juan Jos� Arias Solano
-- Create date: 06/01/2016
-- Description:	Customers by partner Retrieve
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ASPNetCustomersByPartner_Retrieve](
	@pPartnerUserId int = null
	,@pRoleId nvarchar(128)
)	
AS
BEGIN	
	SET NOCOUNT ON;

	SELECT  A.[PartnerUserId],			
			A.[CustomerId],
			B.[Name] AS EncryptedCustomerName		
			
	FROM	[dbo].[CustomerAuthByPartner] A
			INNER JOIN [General].[Customers] B
			ON A.[CustomerId] = B.[CustomerId]			
			
	WHERE   A.[PartnerUserId] = @pPartnerUserId 
			AND A.[RoleId] = @pRoleId

	SET NOCOUNT OFF
END

