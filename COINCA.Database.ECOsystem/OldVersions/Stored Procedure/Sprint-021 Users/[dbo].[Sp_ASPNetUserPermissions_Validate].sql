USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetUserPermissions_Validate]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetUserPermissions_Validate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 12/07/2015
-- Description:	Validate if the user has permissions
-- ================================================================================================
Create PROCEDURE [dbo].[Sp_ASPNetUserPermissions_Validate]
(
	@pUserName VARCHAR(50),
	@pResource VARCHAR(50),
	@pAction VARCHAR(50),
	@pRole	VARCHAR(50)
)
AS
BEGIN

SET NOCOUNT ON	

	DECLARE @RowNumber INT

	SET @RowNumber =	(SELECT COUNT(*)
						FROM [dbo].[AspNetUsers] AS U
						INNER JOIN [dbo].[AspNetUserRoles] AS UR
						ON U.[Id] = UR.[UserId]
						INNER JOIN [dbo].[AspNetRoles] AS R
						ON R.[Id] = UR.[RoleId]
						INNER JOIN [dbo].[AspNetRolePermissions] AS RP
						ON R.[Id] = RP.[RoleId]
						INNER JOIN  [dbo].[AspNetPermissions] AS P
						ON RP.PermissionId = P.Id
						WHERE U.[UserName] = @pUserName 
							  AND UPPER(P.[Action]) = @pAction
							  AND UPPER(P.[Resource]) = @pResource
							  AND R.[Name] = @pRole)

	IF @RowNumber > 0
		SELECT 1
	ELSE 
	BEGIN
		
		SET @RowNumber = (SELECT COUNT(*) 
						  FROM [dbo].[AspNetUsers] AS U
						  INNER JOIN [General].[Users] AS GU
						  ON  U.[Id] = GU.[AspNetUserId]
						  INNER JOIN [dbo].[CustomerAuthByPartner] AS CAP
						  ON GU.[UserId] = CAP.[PartnerUserId]
						  WHERE U.[UserName] = @pUserName 
						        AND @pRole = 'CUSTOMER_ADMIN' )

		IF @RowNumber > 0
			SELECT 1
		ELSE 
			SELECT 0
	END
	
SET NOCOUNT OFF
END

GO
