USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Helper_GetGeographyValue]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Helper_GetGeographyValue]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gerald Solano
-- Create date: 11/08/2015
-- Description:	Convert the geometric value to geography value
-- =============================================
CREATE PROCEDURE [General].[Helper_GetGeographyValue] 
(
	@polygon geometry,
	@geog geography output
)
AS
BEGIN

	BEGIN TRY

		--DECLARE @geog geography
		DECLARE @geom geometry

		set @geom = geometry::STPolyFromText(@polygon.STAsText(),4326) 
	
		SET @geom = @geom.STUnion(@geom.STStartPoint()); 

		set @geog = geography::STGeomFromWKB(@geom.STAsBinary(), @geom.STSrid);

		--SELECT @geog

	END TRY
	BEGIN CATCH

		SET @geog = CONVERT(GEOGRAPHY,'POINT (0 0)') -- SET RETURN DEFAULT VALUE 
		--SELECT @geog

	END CATCH
END

GO


