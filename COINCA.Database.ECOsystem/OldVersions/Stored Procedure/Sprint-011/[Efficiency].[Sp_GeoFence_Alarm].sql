USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFence_Alarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_GeoFence_Alarm]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFence_Alarm]    Script Date: 8/11/2015 2:23:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/26/2015
-- Description:	Add alarm by geofence
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_GeoFence_Alarm]
AS
BEGIN 

	BEGIN TRY 

		DECLARE @RowNum INT = 0
		DECLARE @lErrorMessage NVARCHAR(4000)
	    DECLARE @lErrorSeverity INT
		DECLARE @lErrorState INT

		DECLARE @GeoFencesList TABLE
		(
			[Id] INT IDENTITY(1,1) NOT NULL ,
			[AlarmId] INT,
			[TypeId] INT,
			[CustomerId] INT,
			[AlarmPhone] VARCHAR(100),
			[AlarmEmail] VARCHAR(500),
			[GeoFenceId] INT,
			[GeoFenceName] VARCHAR(50),
			[Polygon] GEOMETRY,
			[In] BIT,
			[Out] BIT,
			[GeoFenceInsertDate] DATETIME
		)

		DECLARE @VehicleByGeoFence TABLE
		(
			[GeoFenceId] INT,
			[TypeId] INT,
			[VehicleId] INT,
			[PlateId] VARCHAR(10),
			[GeofenceIn] BIT,
			[GeofenceOut] BIT
		)	

		DECLARE @VehicleReportLast TABLE
		(
			[VehicleId] INT,
			[Device] INT,
			[GPSDateTime] DATETIME,
			[Longitude] FLOAT,
			[Latitude] FLOAT
		)

		DECLARE @ChangedRecords TABLE
		(
			[GeoFenceReportAlarmId] INT,
			[VehicleId] INT,
			[GPSDateTime] DATETIME,
			[Longitude] FLOAT,
			[Latitude] FLOAT,
			[LastGPSDateTime] DATETIME,
			[LastLongitude] FLOAT,
			[LastLatitude] FLOAT,
			[InsertDate] DATETIME,
			[ModifyDate] DATETIME
		)	

		DECLARE @InOutRecordsByVehicle TABLE
		(
			[Id] INT IDENTITY(1,1) NOT NULL ,
			[AlarmId] INT,
			[TypeId] INT,
			[CustomerId] INT,
			[AlarmPhone] VARCHAR(MAX),
			[AlarmEmail] VARCHAR(MAX),
			[GeoFenceId] INT,
			[GeoFenceName] VARCHAR(50),
			[In] BIT,
			[Out] BIT,
			[GeofenceIn] BIT,
			[GeofenceOut] BIT,
			[VehicleId] INT,
			[PlateId] VARCHAR(10)
		)	
	
		--Selecciona la informacion de la alerta configurada para las GEOCERCAS. Obtiene la alarma, el tipo (GEOCERCA) y la configuracion de la GEOCERCA
		INSERT INTO @GeoFencesList
			([AlarmId],
			[TypeId],
			[CustomerId],
			[AlarmPhone],
			[AlarmEmail],
			[GeoFenceId],
			[GeoFenceName],
			[Polygon],
			[In],
			[Out],
			[GeoFenceInsertDate])
		SELECT 
			b.[AlarmId],
			a.[TypeId],
			b.[CustomerId],
			b.[Phone] [AlarmPhone],
			b.[Email] [AlarmEmail],
			c.[GeoFenceId],
			c.[Name] [GeoFenceName],
			c.[Polygon],
			c.[In],
			c.[Out],
			c.[InsertDate] [GeoFenceInsertDate]
		 FROM 
			[General].[Types] a 
			INNER JOIN [General].[Alarms] b ON a.[TypeId] = b.[AlarmTriggerId]
			INNER JOIN [Efficiency].[GeoFences] c ON b.[EntityId] = c.[GeoFenceId]
		WHERE 
			a.[Code] = 'CUSTOMER_ALARM_TRIGGER_GEOFENCE' AND 
			a.[IsActive] = 1 AND
			b.[Active] = 1 AND 
			c.[Active] = 1


		--SELECT * FROM @GeoFencesList


		--Obtiene la informacion de quienes utilizan las GEOCERCAS configuradas (vehiculos, grupos, unidades, costos)
		INSERT INTO @VehicleByGeoFence
			([GeoFenceId],
			[VehicleId],
			[PlateId],
			[GeofenceIn],
			[GeofenceOut])
			(SELECT 
				a.[GeoFenceId], 
				a.[VehicleId],
				c.[PlateId],
				b.[In],
				b.[Out]
			FROM [Efficiency].[VehiclesByGeoFence] a
				INNER JOIN @GeoFencesList b ON a.[GeoFenceId] = b.[GeoFenceId]
				INNER JOIN [General].[Vehicles] c ON a.[VehicleId] = c.[VehicleId]
			UNION
			SELECT 
				a.[GeoFenceId], 
				c.[VehicleId],
				d.[PlateId] ,
				b.[In],
				b.[Out]
			FROM [Efficiency].[VehicleGroupsByGeoFence] a
				INNER JOIN @GeoFencesList b ON a.[GeoFenceId] = b.[GeoFenceId]
				INNER JOIN [General].[VehiclesByGroup] c ON a.[VehicleGroupId] = c.[VehicleGroupId]
				INNER JOIN [General].[Vehicles] d ON c.[VehicleId] = d.[VehicleId]
			UNION
			SELECT 
				a.[GeoFenceId], 
				e.[VehicleId], 
				e.[PlateId],
				b.[In],
				b.[Out]
			FROM [Efficiency].[VehicleUnitsByGeoFence] a
				INNER JOIN @GeoFencesList b ON a.[GeoFenceId] = b.[GeoFenceId]
				INNER JOIN [General].[VehicleUnits] c ON a.[UnitId] = c.[UnitId]
				INNER JOIN [General].[VehicleCostCenters] d ON c.[UnitId] = d.[UnitId]
				INNER JOIN [General].[Vehicles] e ON e.[CostCenterId] = d.[CostCenterId]
			UNION
			SELECT 
				a.[GeoFenceId], 
				c.[VehicleId],
				c.[PlateId],
				b.[In],
				b.[Out]
			FROM [Efficiency].[VehicleCostCenterByGeoFence] a
				INNER JOIN @GeoFencesList b ON a.[GeoFenceId] = b.[GeoFenceId]
				INNER JOIN [General].[Vehicles] c ON a.[CostCenterId] = c.[CostCenterId])
			-- /////////////////////////////////////////////////////////////////////////
			
			--SELECT * FROM @VehicleByGeoFence

			-- INSERTA LOS ULTIMOS REPORTES DE POSICIÓN DE LOS VEHICULOS 
			INSERT INTO @VehicleReportLast
				([VehicleId],
				[Device],
				[GPSDateTime],
				[Longitude],
				[Latitude])
			SELECT 
				f.[VehicleId],
				a.[Device],
				a.[GPSDateTime],
				a.[Longitude],
				a.[Latitude]
			FROM [dbo].[Report_Last] a
				INNER JOIN [dbo].[Devices] b ON a.[Device] = b.[Device]
				INNER JOIN [DispositivosAVL] c ON b.[UnitID] = c.[numeroimei]
				INNER JOIN [Composiciones] d ON c.[dispositivo] = d.[dispositivo]
				INNER JOIN [Vehiculos] e ON e.[vehiculo] = d.[vehiculo]
				INNER JOIN [General].[Vehicles] f ON e.[vehiculo] = f.[IntrackReference]
				INNER JOIN (SELECT DISTINCT [VehicleId] FROM @VehicleByGeoFence) g ON f.[VehicleId] = g.[VehicleId]
		--///////////////////////////////////////////////////////////////////

		--SELECT * FROM @VehicleReportLast
		--SELECT * FROM [Efficiency].[GeoFenceReportAlarm]

		UPDATE [Efficiency].[GeoFenceReportAlarm] 
			SET [GPSDateTime] = [LastGPSDateTime],
				[Longitude] = [LastLongitude],
				[Latitude] = [LastLatitude]

		INSERT INTO [Efficiency].[GeoFenceReportAlarm] 
			([VehicleId], 
			[GPSDateTime], 
			[Longitude], 
			[Latitude], 
			[LastGPSDateTime], 
			[LastLongitude], 
			[LastLatitude],
			[InsertDate])
		SELECT 
			[VehicleId], 
			[GPSDateTime], 
			[Longitude], 
			[Latitude], 
			[GPSDateTime], 
			[Longitude], 
			[Latitude],
			GETUTCDATE()
		FROM 
			@VehicleReportLast
		WHERE 
			[VehicleId] NOT IN (SELECT [VehicleId] FROM [Efficiency].[GeoFenceReportAlarm])

		UPDATE a
		SET a.[LastGPSDateTime] = b.[GPSDateTime], 
			a.[LastLongitude] = b.[Longitude], 
			a.[LastLatitude] = b.[Latitude], 
			a.[ModifyDate] = GETUTCDATE()
		FROM 
			[Efficiency].[GeoFenceReportAlarm] a
			INNER JOIN @VehicleReportLast b ON a.[VehicleId] = b.[VehicleId]
		WHERE 
			a.[LastGPSDateTime] <> b.[GPSDateTime]

		INSERT INTO @ChangedRecords 
			([GeoFenceReportAlarmId],
			[VehicleId],
			[GPSDateTime],
			[Longitude],
			[Latitude],
			[LastGPSDateTime],
			[LastLongitude],
			[LastLatitude],
			[InsertDate],
			[ModifyDate])
		SELECT 
			[GeoFenceReportAlarmId],
			[VehicleId],
			[GPSDateTime],
			[Longitude],
			[Latitude],
			[LastGPSDateTime],
			[LastLongitude],
			[LastLatitude],
			[InsertDate],
			[ModifyDate]
		FROM 
			[Efficiency].[GeoFenceReportAlarm]
		WHERE 
			[GPSDateTime] <> [LastGPSDateTime] AND 
			[Longitude] <> [LastLongitude] AND 
			[Latitude] <> [LastLatitude]

		--SELECT * FROM @ChangedRecords
		--SELECT Polygon.ToString() FROM @GeoFencesList

		Select @RowNum = Count(*) From @GeoFencesList
	
		WHILE @RowNum > 0
			BEGIN
				DECLARE @Id INT
				DECLARE @AlarmId INT
				DECLARE @TypeId INT
				DECLARE @CustomerId INT
				DECLARE @AlarmPhone VARCHAR(MAX)
				DECLARE @AlarmEmail varchar(max)
				DECLARE @GeoFenceId INT
				DECLARE @GeoFenceName VARCHAR(50)
				DECLARE @Polygon GEOGRAPHY
				DECLARE @In BIT
				DECLARE @Out BIT
				DECLARE @GeoFenceInsertDate DATETIME
				DECLARE @valuePolygon geometry
								
				SELECT  @Id = [Id], 
					@AlarmId = [AlarmId], 
					@TypeId = [TypeId], 
					@CustomerId = [CustomerId], 
					@AlarmPhone = [AlarmPhone], 
					@AlarmEmail = [AlarmEmail], 
					@GeoFenceId = [GeoFenceId], 
					@GeoFenceName = [GeoFenceName], 
					--@Polygon = geography::STGeomFromText([Polygon].STAsText(), 4326), 
					--@Polygon = General.GetGeographyValue([Polygon]),
					@valuePolygon = [Polygon],
					@In = [In],
					@Out = [Out],
					@GeoFenceInsertDate = [GeoFenceInsertDate]
				FROM 
					@GeoFencesList 
				WHERE 
					[Id] = @RowNum

				--// VALIDATE POLYGON ///////////////
				SELECT @valuePolygon = [Polygon] FROM @GeoFencesList WHERE [Id] = @RowNum

				EXEC [General].[Helper_GetGeographyValue] @valuePolygon, @Polygon out

				--SELECT @Polygon

				--///////////////////////////////////


				IF @In = 1
					BEGIN
						BEGIN TRY 
							INSERT INTO @InOutRecordsByVehicle
								([AlarmId],
								[TypeId],
								[CustomerId],
								[AlarmPhone],
								[AlarmEmail],
								[GeoFenceId],
								[GeoFenceName],
								[In],
								[Out],
								[GeofenceIn],
								[GeofenceOut],
								[VehicleId],
								[PlateId])
							SELECT 
								@AlarmId,
								@TypeId,
								@CustomerId,
								@AlarmPhone,
								@AlarmEmail,
								@GeoFenceId,
								@GeoFenceName,
								1,
								0,
								b.GeofenceIn,
								b.GeofenceOut,
								a.[VehicleId],
								b.[PlateId]
							FROM @ChangedRecords a
								INNER JOIN @VehicleByGeoFence b ON a.[VehicleId] = b.[VehicleId]
							WHERE 
								b.[GeoFenceId] = @GeoFenceId AND
								@Polygon.STIntersects(geography::Point([Latitude], [Longitude], 4326)) <> @Polygon.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326))AND 
								@Polygon.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326)) = 1

						END TRY 
						BEGIN CATCH 
							--RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
							--ERROR EN LA INSERCIÓN
						END CATCH 	
					END	

				IF @Out = 1 
					BEGIN
						
						BEGIN TRY 

							INSERT INTO @InOutRecordsByVehicle
								([AlarmId],
								[TypeId],
								[CustomerId],
								[AlarmPhone],
								[AlarmEmail],
								[GeoFenceId],
								[GeoFenceName],
								[In],
								[Out],
								[GeofenceIn],
								[GeofenceOut],
								[VehicleId],
								[PlateId])
							SELECT 
								@AlarmId,
								@TypeId,
								@CustomerId,
								@AlarmPhone,
								@AlarmEmail,
								@GeoFenceId,
								@GeoFenceName,
								0,
								1,
								b.GeofenceIn,
								b.GeofenceOut,
								a.[VehicleId],
								b.[PlateId]
							FROM @ChangedRecords a
								INNER JOIN @VehicleByGeoFence b ON a.[VehicleId] = b.[VehicleId]
							WHERE 
								b.[GeoFenceId] = @GeoFenceId AND
								@Polygon.STIntersects(geography::Point([Latitude], [Longitude], 4326)) <> @Polygon.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326))AND 
								@Polygon.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326)) = 0
						
						END TRY 
						BEGIN CATCH 
							--RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
							--ERROR EN LA INSERCIÓN
						END CATCH 		
					END
	
				SET @RowNum = @RowNum - 1
			END

		--SELECT * FROM @InOutRecordsByVehicle

		Select @RowNum = Count(*) From @InOutRecordsByVehicle
		WHILE @RowNum > 0
			BEGIN
				DECLARE @lAlarmId INT
				DECLARE @lAlarmTriggerId INT
				DECLARE @lCustomerId INT
				DECLARE @lPlate VARCHAR(10)
				DECLARE @lDescription VARCHAR(500)
				DECLARE @lTimeLastReport TIME
				DECLARE @lPhone VARCHAR(100)
				DECLARE @lEmail VARCHAR (500)
				DECLARE @lIn INT
				DECLARE @lOut INT
				DECLARE @lGeofenceIn INT
				DECLARE @lGeofenceOut INT
				DECLARE @lInOut VARCHAR(10)
			
				SELECT @lAlarmId = [AlarmId], 
					@lAlarmTriggerId = [TypeId],
					@lCustomerId =[CustomerId],
					@lPlate = [PlateId],
					@lDescription = [GeoFenceName],
					@lPhone = [AlarmPhone],
					@lEmail = [AlarmEmail],
					@lIn = [In],
					@lOut = [Out],
					@lGeofenceIn = [GeofenceIn],
					@lGeofenceOut = [GeofenceOut]
		
				FROM 
					@InOutRecordsByVehicle 
				WHERE 
					[Id] = @RowNum
				

				SELECT 
						@lAlarmId,
						@lAlarmTriggerId,
						@lCustomerId,
						@lPlate,
						@lDescription,
						@lTimeLastReport,
						@lPhone,
						@lEmail,
						@lInOut,
						@lIn,
						@lOut
				IF @lIn = 1 AND @lGeofenceIn = 1
				BEGIN
					SET @lInOut = 'Entrado'
					EXEC [General].[Sp_SendAlarm] 
						@pAlarmId = @lAlarmId,
						@pAlarmTriggerId = @lAlarmTriggerId,
						@pCustomerId = @lCustomerId,
						@pPlate = @lPlate,
						@pDescription = @lDescription,
						@pTimeLastReport = @lTimeLastReport,
						@pPhone = @lPhone,
						@pEmail = @lEmail,
						@pInOut = @lInOut
				END
				IF @lOut = 1 AND @lGeofenceOut = 1
				BEGIN
					SET @lInOut = 'Salido'
					EXEC [General].[Sp_SendAlarm] 
						@pAlarmId = @lAlarmId,
						@pAlarmTriggerId = @lAlarmTriggerId,
						@pCustomerId = @lCustomerId,
						@pPlate = @lPlate,
						@pDescription = @lDescription,
						@pTimeLastReport = @lTimeLastReport,
						@pPhone = @lPhone,
						@pEmail = @lEmail,
						@pInOut = @lInOut
				END
			SET @RowNum = @RowNum - 1
		END

	END TRY 
	BEGIN CATCH 
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH 		
END
GO