USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Vehicles_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_Vehicles_Retrieve]    Script Date: 8/13/2015 11:23:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Vehicle information
-- ================================================================================================
Create PROCEDURE [General].[Sp_Vehicles_Retrieve]
(
	 @pVehicleId INT = NULL					--@pVehicleId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
	,@pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON

	DECLARE @pPartnerId INT = NULL

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT @pPartnerId = cp.[PartnerId]
	FROM [General].[CustomersByPartner] cp
	WHERE cp.[CustomerId] = @pCustomerId

	SELECT
		 a.[VehicleId]
		,a.[PlateId]
		,a.[Name]
		,a.[CostCenterId]
		,c.[Name] AS CostCenterName
		,e.[UserId]
		,e.[Name] AS [EncryptedUserName]
		,a.[VehicleCategoryId]
		,d.[Manufacturer]
		,d.[VehicleModel]
		,d.[Type] as VehicleType
		,d.[Manufacturer]+ '-' +d.VehicleModel + ', ' + d.[Type] AS CategoryType
        ,d.[Type] AS CategoryType
		,d.[Liters]
		,b.[Name] AS FuelName
		,b.[FuelId] as DefaultFuelId
		,a.[Active]
		,a.[Colour]
		,a.[Chassis]
		,a.[LastDallas]
		,a.[FreightTemperature]
		,a.[FreightTemperatureThreshold1]
		,a.[FreightTemperatureThreshold2]
		,a.[Predictive]
		,a.[AVL]
		,a.[PhoneNumber]
		,a.[Insurance]
		,a.[DateExpirationInsurance]
		,a.[CoverageType]
		,a.[NameEnterpriseInsurance]
		,a.[PeriodicityId]
		,a.[Cost]
		,g.[PartnerId]
		,g.[CapacityUnitId] AS PartnerCapacityUnitId
		,a.[AdministrativeSpeedLimit]
		,a.[CabinPhone]
		,a.[IntrackReference]
		,a.[Imei]
		,a.[RowVersion]
    FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCostCenters] c
			ON a.[CostCenterId] = c.[CostCenterId]
		INNER JOIN [General].[VehicleCategories] d
			ON a.[VehicleCategoryId] = d.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] b
			ON d.[DefaultFuelId] = b.[FuelId]
		INNER JOIN [General].[Users] e
			ON e.[UserId] = 
			(SELECT TOP 1 [UserId]
			 FROM [General].[VehiclesByUser]
			 WHERE [VehicleId] = a.[VehicleId]
			 ORDER BY [InsertDate] DESC)
		INNER JOIN [General].[Customers] f
			ON a.[CustomerId] = f.[CustomerId]
		INNER JOIN [General].[CustomersByPartner] h
			ON h.[CustomerId] = f.[CustomerId]
		INNER JOIN [General].[Partners] g
			ON h.[PartnerId] = g.[PartnerId]
	WHERE a.[CustomerId] = @pCustomerId
	  --AND a.[Active] = 1
	  --AND h.[IsDefault] = 1 // REVISAR DESPU�S EN ASEGURADORAS // NO DESPLIEGA LOS VEHICULOS SI NO ES EL DEFAULT
	  AND (@pVehicleId IS NULL OR [VehicleId] = @pVehicleId)
	  AND (@pPartnerId IS NULL OR g.[PartnerId] = @pPartnerId)
	  AND (@pKey IS NULL 
				                OR a.[Name] like '%'+@pKey+'%'
                OR a.[Chassis] like '%'+@pKey+'%'
                OR a.[PlateId] like '%'+@pKey+'%'
                OR b.[Name] like '%'+@pKey+'%'
                OR c.[Name] like '%'+@pKey+'%'
                OR d.[Type] like '%'+@pKey+'%'
                OR CONVERT(VARCHAR(20),d.[Year]) like '%'+@pKey+'%')
	ORDER BY [VehicleId] DESC
	
    SET NOCOUNT OFF
END




GO


