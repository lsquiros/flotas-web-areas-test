USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]
GO

/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]    Script Date: 8/12/2015 4:33:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Danilo Hidalgo.
-- Create date: 02/23/2015
-- Description:	Insert Preventive Maintenance By Vehicle information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit]
	 @pVehicleId INT = NULL
	 ,@pPreventiveMaintenanceCatalogId INT = NULL
	 ,@pLastReviewOdometer FLOAT = NULL
	 ,@pLastReviewDate DATETIME = NULL
	 ,@pLoggedUserId INT
	 ,@pRowVersion TIMESTAMP = NULL
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY
		DECLARE @lLines INT = NULL
		DECLARE @lFrequencyOdometer FLOAT = NULL
		DECLARE @lFrequencyDate INT = NULL
		DECLARE @lNextReviewOdometer FLOAT = NULL
		DECLARE @lNextReviewMonth DATETIME = NULL
		DECLARE @lNextReviewDate DATETIME = NULL
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0
        DECLARE @lFrequencyTypeId INT
        DECLARE @lFrequency INT
		        
		IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END		

		SELECT @lLines = COUNT(*) FROM [General].[PreventiveMaintenanceByVehicle] WHERE [VehicleId] = @pVehicleId AND [PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId AND [IsDeleted] = 0 AND [Registred] = 0
		IF @lLines = 0
		BEGIN
		
			SELECT 
				@lFrequencyOdometer = [FrequencyKm],
				@lFrequencyDate = [FrequencyMonth] 
				--,@lNextReviewDate = [FrequencyDate]
			FROM 
				[General].[PreventiveMaintenanceCatalog] 
			WHERE 
				[PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId
				
			--IF @lNextReviewDate IS NULL 
			--	SET @lNextReviewDate = DATEADD(MONTH, @lFrequencyDate, @pLastReviewDate)
			--ELSE
			--	SET @lNextReviewDate = DATEADD(YEAR, 1, @pLastReviewDate)
			
			--FIX:GSOLANO - SOLAMENTE QUE AHORA NO VALIDE EL FrequencyDate SI NO M�S BIEN SOLO LA FRECUENCIA DE MESES
			IF @lFrequencyDate IS NULL 
				SET @lNextReviewDate = DATEADD(YEAR, 1, @pLastReviewDate)
			ELSE
				SET @lNextReviewDate = DATEADD(MONTH, @lFrequencyDate, @pLastReviewDate)
			
			SET @lNextReviewOdometer = @pLastReviewOdometer + @lFrequencyOdometer
			
			INSERT INTO [General].[PreventiveMaintenanceByVehicle]
				([VehicleId]
				,[PreventiveMaintenanceCatalogId]
				,[LastReviewOdometer]
				,[LastReviewDate]
				,[NextReviewOdometer]
				,[NextReviewDate]
				,[IsDeleted]
				,[Registred]
				,[AlarmSent]
				,[InsertUserId]
				,[InsertDate])
			VALUES
				(@pVehicleId
				,@pPreventiveMaintenanceCatalogId
				,@pLastReviewOdometer
				,@pLastReviewDate
				,@lNextReviewOdometer
				,@lNextReviewDate
				,0
				,0
				,0
				,@pLoggedUserId
				,GETUTCDATE())
		END
		
			SET @lRowCount = @@ROWCOUNT
		        
			IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


