USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardByNumber_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardByNumber_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_CreditCardByNumber_Retrieve]    Script Date: 11/1/2015 10:30:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Napole�n Alvarado
-- Create date: 12/10/2014
-- Description:	Retrieve CreditCard information by Number
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardByNumber_Retrieve]
(
	 @pCreditCardNumber nvarchar(520)		--@pCreditCardNumber: Credit Card Number encrypted
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		 a.[CreditCardId]
		,a.[CustomerId]
		,c.[Name] AS [EncryptedCustomerName]
		,a.[CreditCardNumber]
		,a.[ExpirationYear]
		,a.[ExpirationMonth]
		,a.[CreditLimit]
		,a.[CreditAvailable]
		,a.[CreditExtra]
		,a.[StatusId]
		,b.[RowOrder] AS [Step]
		,b.[Name] AS [StatusName]
		,d.[Symbol] AS [CurrencySymbol]
		,c.[IssueForId]		
		,e.[UserId]
		,g.[VehicleId]
		,c.[CreditCardType]
		,f.Name AS [EncryptedDriverName]
		,'Veh�culo: ' + h.[PlateId] AS [VehiclePlate]
		,h.[PlateId] AS [TransactionPlate]
		,a.CardRequestId
		,i.[EstimatedDelivery]
		,a.[RowVersion]
    FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [Control].[CreditCardByVehicle] g
			ON g.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] = g.[VehicleId]
		INNER JOIN [Control].[CardRequest] i
			ON a.[CardRequestId] = i.[CardRequestId]
	WHERE a.[CreditCardNumber] = @pCreditCardNumber
	  AND a.[StatusId] <> 9 -- filter the closed cards
	
    SET NOCOUNT OFF
END

GO


