-- ================================================================================================
-- Author:		Stefano Quiros Ruiz.
-- Create date: 30/10/2015
-- Description:	Insert or Update CustomerTerminal information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerTerminal_AddOrEdit]
(
	 @pCustomerId INT,
	 @pTerminalId Varchar(250),
	 @pMerchantDescription varchar(250)
	 
)
AS
BEGIN
declare @pTemp varchar(250)

            SELECT @pTemp = [TerminalId]
	         FROM [General].[Terminal] 
			 WHERE [TerminalId] = @pTerminalId;
	               
					
			IF (@pTemp IS NULL)
			BEGIN
				INSERT INTO [General].[Terminal]
						([CustomerId]
						,[TerminalId]
						,[MerchantDescription])
				VALUES	(@pCustomerId
				         ,@pTerminalId
						 ,@pMerchantDescription)
						

				
			END
			ELSE
			BEGIN
				UPDATE [General].[Terminal]
					SET  [MerchantDescription] = @pMerchantDescription
						
				WHERE [TerminalId] = @pTerminalId
				 				  

			END
END
	

GO


