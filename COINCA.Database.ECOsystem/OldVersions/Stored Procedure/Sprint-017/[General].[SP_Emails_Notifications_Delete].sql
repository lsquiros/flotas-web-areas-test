USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_Emails_Notifications_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_Emails_Notifications_Delete]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Henry Retana
-- Create date: 10/28/2015
-- Description:	Delete the email in the list. 
-- =============================================
CREATE PROCEDURE [General].[SP_Emails_Notifications_Delete]
(	
	@pID_Email INT 
)
AS
BEGIN
	
	SET NOCOUNT ON;
    SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
	DECLARE @lErrorMessage NVARCHAR(4000)
    DECLARE @lErrorSeverity INT
    DECLARE @lErrorState INT
    DECLARE @lLocalTran BIT = 0
            
            
    IF (@@TRANCOUNT = 0)
	BEGIN
		BEGIN TRANSACTION
		SET @lLocalTran = 1
	END

	Delete from [General].[Emails_Notification] where ID_Email = @pID_Email

	IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO


