USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByDriverCode_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesByDriverCode_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_VehiclesByDriverCode_Retrieve]    Script Date: 11/3/2015 12:03:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 19/08/2015
-- Description:	Retrieve Vehicles By Driver Code
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesByDriverCode_Retrieve]
(
	@pDriverCode NVARCHAR(MAX),	--@pDriverCode
	@pCustomerId INT,	--@pCustomerId: FK of Customer
	@pPlateId VARCHAR(10)
)
AS
BEGIN
	
	SET NOCOUNT ON
	--DECLARE @pDriverCode NVARCHAR(MAX) = 'JCGRD1bjAxs='--''101'
	--DECLARE @pCustomerId INT = 6
	--DECLARE @pPlateId VARCHAR(10) = 'JLT-390cc'

	SELECT top(1)
		v.[VehicleId] AS [VehicleId],
	    v.[PlateId] AS [Plate],
		@pDriverCode AS [DriverCode]
	FROM [General].[DriversUsers] du
	INNER JOIN [General].[VehiclesDrivers] vd
		ON du.[UserId] = vd.[UserId]
	--INNER JOIN [General].[VehiclesByUser] vu
	--	ON du.[UserId] = vu.[UserId]
	INNER JOIN [General].[Vehicles] v
		ON v.[VehicleId] = vd.[VehicleId] 
	WHERE du.[Code] = @pDriverCode AND
		  du.[CustomerId] = @pCustomerId AND
		  v.[PlateId] = @pPlateId
		  --vu.[LastDateDriving] IS NULL
	--ORDER BY vu.[InsertDate] DESC

    SET NOCOUNT OFF
END
GO