-- ================================================================================================
-- Author:		Stefano Quiros Ruiz.
-- Create date: 10/29/2015
-- Description:	Retrieve Customer Terminals information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerTerminal_Retrieve]
(
	  @pCustomerId INT = NULL
)
AS
BEGIN
	
	SELECT	 t.[CustomerId], t.[TerminalId], t.[MerchantDescription]

	FROM [General].[Terminal] t
		INNER JOIN [General].[Customers] c
			ON t.[CustomerId] = c.[CustomerId]
	WHERE c.[CustomerId] = @pCustomerId
	ORDER BY [CustomerId] DESC
	
    SET NOCOUNT OFF
END





GO


