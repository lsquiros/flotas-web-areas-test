USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_Emails_Notifications_Retrieve_List]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_Emails_Notifications_Retrieve_List]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Henry Retana
-- Create date: 10/28/2015
-- Description:	Retrieve the list of emails created
-- =============================================
CREATE PROCEDURE [General].[SP_Emails_Notifications_Retrieve_List] 
	@pID_Email INT = NULL
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT  [ID_Email]
		   ,[Email]
		   ,[PartnerId]
		   ,[Activo]
		   ,[TypeId] 
	FROM [General].[Emails_Notification] 
	WHERE (@pID_Email IS NULL OR ID_Email = @pID_Email)

END
GO


