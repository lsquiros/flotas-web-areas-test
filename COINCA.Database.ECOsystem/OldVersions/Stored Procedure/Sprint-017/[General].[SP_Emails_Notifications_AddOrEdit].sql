USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_Emails_Notifications_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_Emails_Notifications_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Henry Retana
-- Create date: 10/27/2015
-- Description:	Add or Update the emails which are going to be notified 
-- =============================================
CREATE PROCEDURE [General].[SP_Emails_Notifications_AddOrEdit]
(	
	@pIDEmail INT = NULL,
	@pEmail VARCHAR(50) = '',
	@pPartnerId INT = NULL,
	@pActive BIT,
	@pTypeId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pIDEmail IS NULL)
			BEGIN
				INSERT INTO [General].[Emails_Notification]
						([Email]
						,[PartnerId]
						,[Activo]
						,[TypeId])
				VALUES	(@pEmail
						,@pPartnerId
						,@pActive
						,@pTypeId)				
			END
			ELSE
			BEGIN
				UPDATE [General].[Emails_Notification]
					SET  [Email] = @pEmail
						,[PartnerId] = @pPartnerId
						,[Activo] = @pActive
						,[TypeId] = @pTypeId						
				WHERE [ID_Email] = @pIDEmail

			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF

END


GO


