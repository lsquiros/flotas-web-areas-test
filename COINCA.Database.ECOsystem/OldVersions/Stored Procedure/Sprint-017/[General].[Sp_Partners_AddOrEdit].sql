USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Partners_AddOrEdit]
GO

/****** Object:  StoredProcedure [General].[Sp_Partners_AddOrEdit]    Script Date: 10/31/2015 5:39:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--~ Commented out USE [database] statement because USE statement is not supported to another database.
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update Partner information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Partners_AddOrEdit]
(
	 @pPartnerId INT = NULL
	,@pName VARCHAR(250)
	,@pCountryId INT
	,@pLogo VARCHAR(MAX) = NULL
	,@pLoggedUserId INT
	,@pApiUserName VARCHAR(520) = NULL
	,@pApiPassword VARCHAR(520) = NULL
	,@pCapacityUnitId INT  = NULL
	,@pPartnerTypeId INT
	,@pFuelErrorPercent INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pPartnerId IS NULL)
			BEGIN
				INSERT INTO [General].[Partners]
						([Name]
						,[CountryId]
						,[Logo]
						,[ApiUserName]
						,[ApiPassword]
						,[PartnerGroupId]
						,[CapacityUnitId]
						,[PartnerTypeId]
						,[FuelErrorPercent]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pName
						,@pCountryId
						,@pLogo
						,@pApiUserName
						,@pApiPassword
						,1 -- Future Change here --
						,@pCapacityUnitId
						,@pPartnerTypeId
						,@pFuelErrorPercent
						,GETUTCDATE()
						,@pLoggedUserId)
				SET  @pPartnerId = SCOPE_IDENTITY()
				
				-- Add PartnerCurrency DEFAULT INSERT 
				Declare @CurrencyId INT 
				
				SELECT @CurrencyId = ct.CurrencyId FROM [General].[Countries] ct
				WHERE ct.CountryId = @pCountryId
				
				IF(@CurrencyId IS NOT NULL) BEGIN
					INSERT INTO [Control].[PartnerCurrency]
						   ([CurrencyId]
						   ,[PartnerId]
						   ,[ExchangeRate]
						   ,[StartDate]
						   ,[EndDate]
						   ,[InsertDate]
						   ,[InsertUserId]
						   ,[ModifyDate]
						   ,[ModifyUserId])
					 VALUES
						   (@CurrencyId
						   ,@pPartnerId
						   ,1.000000
						   ,GETUTCDATE()
						   ,NULL
						   ,GETUTCDATE()
						   ,NULL
						   ,NULL
						   ,NULL)
				END -- TERMINA IF @CurrencyId
			END
			ELSE
			BEGIN
				
				DECLARE @pApiUserTemp VARCHAR(520) = null
				DECLARE @pApiPassTemp VARCHAR(520) = null

				SELECT 
					@pApiUserTemp = [ApiUserName],
					@pApiPassTemp = [ApiPassword]
				FROM [General].[Partners]
				WHERE [PartnerId] = @pPartnerId

				IF(@pApiUserName IS NULL OR @pApiUserName = '')
				BEGIN
					SET @pApiUserName = @pApiUserTemp
				END 
				
				IF(@pApiPassword IS NULL OR @pApiPassword = '')
				BEGIN
					SET @pApiPassword = @pApiPassTemp
				END
				

				UPDATE [General].[Partners]
					SET  [Name] = @pName
						,[CountryId] = @pCountryId
						,[Logo] = @pLogo
						,[ApiUserName] = @pApiUserName
						,[ApiPassword] = @pApiPassword
						,[CapacityUnitId] = @pCapacityUnitId
						,[PartnerTypeId] = @pPartnerTypeId
						,[FuelErrorPercent] = @pFuelErrorPercent
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [PartnerId] = @pPartnerId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
			SELECT @pPartnerId AS PartnerId;
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO