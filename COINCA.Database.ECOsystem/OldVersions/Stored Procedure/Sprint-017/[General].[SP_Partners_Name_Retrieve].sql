USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_Partners_Name_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_Partners_Name_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Henry Retana
-- Create date: 10/28/2015
-- Description:	Returns the Partner Name based on the Id
-- =============================================
CREATE PROCEDURE [General].[SP_Partners_Name_Retrieve]
(		
	@pPartnerId INT = NULL
)
AS
BEGIN
	
SET NOCOUNT ON    
    
	SELECT [Name]
	FROM [General].[Partners]
	WHERE [PartnerId] = @pPartnerId

END
GO