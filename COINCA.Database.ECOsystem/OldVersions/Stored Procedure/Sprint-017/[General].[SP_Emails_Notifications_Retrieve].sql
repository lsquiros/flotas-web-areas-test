USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_Emails_Notifications_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_Emails_Notifications_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Henry Retana
-- Create date: 10/26/2015
-- Description:	Retrieve the list of emails which are going to be notified, based on the partner. 
-- =============================================
CREATE PROCEDURE [General].[SP_Emails_Notifications_Retrieve]
(	
	@pPartnerIds varchar(50) = ''
)
AS
BEGIN
	
	SET NOCOUNT ON;
    
	declare @query varchar(100) = N'select Email from [General].[Emails_Notification] where Activo = 1' 

	set @query = @query + @pPartnerIds

	EXEC (@query)

END


GO


