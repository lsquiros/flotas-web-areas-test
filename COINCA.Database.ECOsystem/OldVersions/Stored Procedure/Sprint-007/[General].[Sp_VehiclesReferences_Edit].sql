USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesReferences_Edit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesReferences_Edit]
GO

USE [ECOSystem]
GO
/****** Object:  StoredProcedure [General].[Sp_VehiclesReferences_Edit]    Script Date: 12/02/2014 11:03:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/02/2014
-- Description:	Update Vehicle References information (Vehicle Intrack and Device Atrack Reference)
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesReferences_Edit]
(
	 @pVehicleId INT								--@pVehicleId: PK of the table
	,@pCustomerId INT								--@pCustomerId: FK of Customer
	,@pAdministrativeSpeedLimit INT					--@pAdministrativeSpeedLimit: Maximum speed limit for this car
	,@pIntrackReference INT							--@pIntrackReference: Vehicle Id in IntrackV2 database
	,@PDeviceReference INT							--@pDeviceReference: Device Id in Atrack database				
	,@pLoggedUserId INT								--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP							--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0 
            DECLARE @lResult INT   
            
			--Invoke SP to replicate vehicle data in ATRACK database
			Exec General.Sp_Vehicles_Replication @pVehicleId, @pCustomerId, @pIntrackReference, @pDeviceReference, @pAdministrativeSpeedLimit, @pResult = @lResult output, @pErrorDesc = @lErrorMessage output
			If @lResult = -1
			begin
				RAISERROR (@lErrorMessage, 16, 1)
			end
			
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			UPDATE [General].[Vehicles]
				SET  [IntrackReference] = @pIntrackReference
					,[DeviceReference] = @PDeviceReference
					,[ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
			WHERE [VehicleId] = @pVehicleId
			  AND [RowVersion] = @pRowVersion
            
            SET @lRowCount = @@ROWCOUNT
                        
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
