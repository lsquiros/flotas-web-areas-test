USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesSegments_Retrieve]    Script Date: 11/06/2014 10:24:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesSegments_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_RoutesSegments_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesSegments_Retrieve]    Script Date: 11/06/2014 10:24:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	Retrieve RoutesSegments information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_RoutesSegments_Retrieve]
(
	 @pRouteId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT [SegmentId]
		,[RouteId]
		,[StartReference]
		,[EndReference]
		,[Poliyline].STAsText() Poliyline
		,[Time]
		,[Distance]
	FROM 
		[Efficiency].[RoutesSegments]
	where [RouteId] = @pRouteId
	
    SET NOCOUNT OFF
END



GO


