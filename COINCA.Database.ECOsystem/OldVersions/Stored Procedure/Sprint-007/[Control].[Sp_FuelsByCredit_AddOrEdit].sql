USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_FuelsByCredit_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_FuelsByCredit_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update Fuels By Credit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_FuelsByCredit_AddOrEdit]
(
	 @pFuelByCreditId INT = NULL			--@pFuelsByCreditId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Partners
	,@pFuelId INT							--@pFuelId: FK of Fuels
	,@pMonth INT							--@pMonth: Month
	,@pYear INT								--@pYear: Year
	,@pTotal DECIMAL(16,2)					--@pTotal: Total
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pFuelByCreditId IS NULL)
			BEGIN				
				
				INSERT INTO [Control].[FuelsByCredit]
						([CustomerId]
						,[FuelId]
						,[Month]
						,[Year]
						,[Total]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pFuelId
						,@pMonth
						,@pYear
						,@pTotal
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Control].[FuelsByCredit]
					SET  [CustomerId] = @pCustomerId
						,[FuelId] = @pFuelId
						,[Month] = @pMonth
						,[Year] = @pYear
						,[Total] = @pTotal
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [FuelByCreditId] = @pFuelByCreditId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END