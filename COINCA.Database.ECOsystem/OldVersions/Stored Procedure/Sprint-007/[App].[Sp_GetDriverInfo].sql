USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetDriverInfo]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_GetDriverInfo]
GO

USE [ECOsystem]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 ----================================================================================================
 --Author:		Gerald Solano C.
 --Create date: 06/19/2015
 --Description:	Retrieve Driver Information 
 ----================================================================================================
CREATE PROCEDURE [dbo].[Sp_GetDriverInfo]
(
	@pUserId int,
	@pDate datetime = null
)
AS
BEGIN
	Declare @VehiclesByUser table
	(Device int,
	 Vehicle int,
	 StartOdometer int,
	 EndOdometer int,
	 StartDate datetime,
	 EndDate datetime)

	 Declare @AvanceEficienciaData table
	 (
		TotalAvance float null,
		TotalEficiencia float null
	 )
	
	--DECLARE @pUserId int = 1123
	--DECLARE @pDate datetime = '2015-06-01'
	DECLARE @Day int = 0
	DECLARE @Month int = 0
	DECLARE @Year int = 0

	DECLARE @DrivingTime VARCHAR(50)
	DECLARE @EndDate datetime = DATEADD(day, 1, @pDate) 
	DECLARE @GPSTimeMax datetime = @pDate
	DECLARE @GPSTimeMin datetime = @pDate
	DECLARE @CustomerId INT
	DECLARE @OdometerMax INT = 0
	DECLARE @OdometerMin INT = 0
	DECLARE @ActualDevice INT = 0

	Set @Day = DATEPART(DD, @pDate)  -- EXTRAEMOS D�A
	Set @Month = DATEPART(MM, @pDate) -- EXTRAEMOS MES
	Set @Year = DATEPART(YY, @pDate) -- EXTRAEMOS A�O

	-- SELECCIONAMOS EL CUSTOMER
	SELECT @CustomerId = CustomerId FROM [General].[DriversUsers] WHERE UserId = @pUserId

	-- CONSULTAMOS LOS DEVICES QUE USO EL CONDUCTOR PARA EXTRAER DESPUES EL PRIMER ODOMETRO DEL D�A Y EL ULTIMO
	INSERT INTO @VehiclesByUser
	Select V.DeviceReference, V.IntrackReference, IsNull(VU.InitialOdometer, 0), IsNull(VU.EndOdometer,0), VU.InsertDate, IsNull(VU.LastDateDriving, GETDATE())
	from General.VehiclesByUser VU 
	inner Join General.Vehicles V ON  V.VehicleId = VU.VehicleId
	inner JOIN General.Users U  ON U.UserId = VU.UserId
	Where U.UserId =  @pUserId
		and V.CustomerId = @CustomerId
		And V.DeviceReference is not null
		and ((VU.LastDateDriving is null) or (@EndDate <=ISNULL(VU.LastDateDriving,@EndDate) and @pDate>=ISNULL(VU.InsertDate, @pDate)))
	ORDER BY V.DeviceReference, VU.InsertDate, VU.LastDateDriving
	-- ///////////////////////////////////////////////////////////////////////


	-- OBTENEMOS EL MINIMO MANEJO DE TIEMPO DURANTE EL D�A 
	SELECT @ActualDevice = MIN(Device) FROM @VehiclesByUser

	--SELECT *  FROM @VehiclesByUser

	WHILE(@ActualDevice IS NOT NULL)
	BEGIN
		Select
			@GPSTimeMin = MIN(GPSDateTime),
			@OdometerMin = IsNull(MIN(Odometer), 0)  
		FROM Reports  
		WHERE GPSDateTime >= @pDate 
		and GPSDateTime <=  @EndDate  
		and Device = @ActualDevice
		--and Odometer < @OdometerMin	

		--SELECT @GPSTimeMin, @ActualDevice

		---- Siguiente iteraci�n  @ActualDevice > al Minimo
		SELECT @ActualDevice = min(Device)FROM @VehiclesByUser WHERE  Device >  @ActualDevice
	END
	-- /////////////////////////////////////////////////////////////////

	-- OBTENEMOS EL M�XIMO MANEJO DE TIEMPO DURANTE EL D�A 
	SELECT @ActualDevice = MIN(Device) FROM @VehiclesByUser

	WHILE(@ActualDevice IS NOT NULL)
	BEGIN
		
		SELECT 	
			@GPSTimeMax = MAX(GPSDateTime),
			@OdometerMax = IsNull(MAX(Odometer), 0)  		
		FROM [dbo].Reports
		WHERE GPSDateTime >= @pDate 
		and GPSDateTime <=  @EndDate  
		and Device = @ActualDevice
		--and Odometer > @GPSTimeMax	

		--SELECT @GPSTimeMax, @ActualDevice, @EndDate

		---- Siguiente iteraci�n  @ActualDevice > al Minimo
		SELECT @ActualDevice = min(Device)FROM @VehiclesByUser WHERE  Device >  @ActualDevice
	END
	-- /////////////////////////////////////////////////////////////////
	
	-- DIFERENCIA DE HORA MAXIMA Y HORA MINIMA 

	SET @DrivingTime = 
				RIGHT('0' + CAST(DATEDIFF(S, @GPSTimeMin, @GPSTimeMax) / 3600 AS VARCHAR(2)), 2) + ':'
				+ RIGHT('0' + CAST(DATEDIFF(S, @GPSTimeMin, @GPSTimeMax) % 3600 /   60 AS VARCHAR(2)), 2) + ':'
				+ RIGHT('0' + CAST(DATEDIFF(S, @GPSTimeMin, @GPSTimeMax) %   60        AS VARCHAR(2)), 2)

	-- /////////////////////////////////////////////////////////////////

	-- OBTENEMOS LOS RESULTADOS DE AVANCE DE RUTA Y EFICIENCIA
	DECLARE @AvanceResult float = 0
	DECLARE @EficienciaResult float = 0

	SELECT @AvanceResult = TotalAvance, 
		   @EficienciaResult = TotalEficiencia 
	FROM [dbo].[Fn_AvanceAndEficiencia_Retrieve](@pUserId, @pDate)

	-- /////////////////////////////////////////////////////////////////

	-- RETURN
	SELECT u.[Name] AS NameEncrypted, 
		   c.[Name] AS BusinessEncrypted, 
		   SUM(dsd.[Score])/COUNT(*) AS Score, 
		   COALESCE(@DrivingTime, '00:00:00') AS DrivingTime, 
		   SUM(dsd.[KmTraveled])/COUNT(*) AS TraveledDistance,
		   SUM(dsd.[OverSpeedAmount])/COUNT(*) AS SpeedOverLimit,
		   SUM(dsd.[OverSpeedAverage])/COUNT(*) AS AverageSpeedExcess,
		   SUM((dsd.[OverSpeed] / dsd.[KmTraveled]))/COUNT(*) AS DrivingExcessPercentage,
		   @AvanceResult AS RouteAdvance,
		   @EficienciaResult AS RouteEfficiency
	FROM [Operation].[DriversScoresDaily] dsd
		INNER JOIN [General].[Users] u ON u.UserId = @pUserId
		INNER JOIN [General].[DriversUsers] du ON du.UserId = u.UserId
		INNER JOIN [General].[Customers] c ON c.CustomerId = du.CustomerId
	WHERE dsd.[UserId] = @pUserId AND
		  dsd.[ScoreYear] = @Year AND
		  dsd.[ScoreMonth] = @Month AND
		  dsd.[ScoreDay] = @Day
	GROUP BY dsd.[UserId], u.[Name], c.[Name], dsd.[ScoreYear], dsd.[ScoreMonth], dsd.[ScoreDay]

END --End  Stored
GO

