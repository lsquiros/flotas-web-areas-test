USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleSchedule_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleSchedule_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update VehicleSchedule information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleSchedule_AddOrEdit]
(
	 @pVehicleId INT						--@pVehicleScheduleId: PK of the table
	,@pJsonSchedule VARCHAR(MAX)			--@pJsonSchedule: Json Schedule
	,@pXmlSchedule VARCHAR(MAX)				--@pJsonSchedule: Json Schedule
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lVehicleScheduleId INT
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			SELECT
				@lVehicleScheduleId = a.[VehicleScheduleId] 
			FROM [General].[VehicleSchedule] a
			WHERE a.[VehicleId] = @pVehicleId
			
			IF (@lVehicleScheduleId IS NULL)
			BEGIN
				INSERT INTO [General].[VehicleSchedule]
						([VehicleId]
						,[JsonSchedule]
						,[XmlSchedule]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pVehicleId
						,@pJsonSchedule
						,@pXmlSchedule
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[VehicleSchedule]
					SET  [JsonSchedule] = @pJsonSchedule
						,[XmlSchedule] = @pXmlSchedule
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [VehicleId] = @pVehicleId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
			
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO