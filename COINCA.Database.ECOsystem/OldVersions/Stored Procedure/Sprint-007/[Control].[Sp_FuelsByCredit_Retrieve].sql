USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_FuelsByCredit_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_FuelsByCredit_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/03/2014
-- Description:	Retrieve Customer Credit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_FuelsByCredit_Retrieve]
(
	  @pCustomerId INT						--@pCustomerId: Customer Id
	 ,@pYear INT							--@pYear: Year
	 ,@pMonth INT = NULL					--@pMonth: Month
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lExchangeRate DECIMAL(16,8) = 1.0
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	
	SELECT
		@lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[PartnerId] = b.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = b.[CustomerId] AND c.[CurrencyId] = a.[CurrencyId]
	WHERE c.[CustomerId] = @pCustomerId
	  AND a.EndDate IS NULL
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	  
	
	SELECT
		 a.[FuelByCreditId]
		,a.[FuelId]
		,a.[Year]
		,a.[Month]
		,a.[Total]
		,a.[Assigned]
		,a.[Available]
		,b.[Name] AS [FuelName]
		,CONVERT(DECIMAL(16,2),a.[Total]/(t.LiterPrice * @lExchangeRate)) AS Liters
		,CONVERT(DECIMAL(16,2),(t.LiterPrice * @lExchangeRate)) AS LiterPrice
		,@lCurrencySymbol AS [CurrencySymbol]
		,a.[RowVersion]
    FROM [Control].[FuelsByCredit] a
		INNER JOIN [Control].[Fuels] b
			ON a.[FuelId] = b.[FuelId]
		LEFT JOIN (
			SELECT
					x.[FuelId], x.[LiterPrice] 
				FROM [Control].[PartnerFuel] x
					INNER JOIN [General].[CustomersByPartner] y
						ON x.[PartnerId] = y.[PartnerId]
					INNER JOIN [General].[Customers] z
						ON y.[CustomerId] = z.[CustomerId]
						AND z.CustomerId = y.CustomerId
				WHERE z.CustomerId = @pCustomerId
				  AND y.IsDefault = 1
				  AND x.[EndDate] IS NULL)t
			ON b.[FuelId] = t.FuelId
	WHERE a.[CustomerId] = @pCustomerId
	  AND a.[Year] = @pYear
	  AND (@pMonth IS NULL OR a.[Month] = @pMonth)
	  --and a.[Total]<>0 -- GSOLANO: SE COMENTO ESTE FILTRO PORQUE NO DEVUELVE LOS VALORES YA CREADOS CON TOTAL 0, Y POR ESTO SE DUPLICABAN CUANDO EL PROCESO DE [Sp_FuelsByCredit_AddOrEdit] CREABA NUEVOS
	ORDER BY a.[Year], a.[Month] DESC
	
    SET NOCOUNT OFF
END
