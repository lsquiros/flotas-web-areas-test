USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleGroupsByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_VehicleGroupsByGeoFence_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/18/2014
-- Description:	Retrieve VehicleGroupsByGeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleGroupsByGeoFence_Retrieve]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		null [GeoFenceId],
		a.[Name] [Name],
		a.[Description] [Description],
		a.[VehicleGroupId] [VehicleGroupId],
		ROW_NUMBER() OVER (ORDER BY a.[VehicleGroupId]) AS [RowNumber] 
	FROM 
		[General].[VehicleGroups] a
	WHERE
		a.[CustomerId] = @pCustomerId
		AND NOT EXISTS(
			SELECT 
				1 
			FROM 
				[Efficiency].[VehicleGroupsByGeoFence] x
			WHERE 
				x.[VehicleGroupId] = a.[VehicleGroupId] AND 
				x.[GeoFenceId] = @pGeoFenceId
			)
	UNION
	SELECT 
		x.[GeoFenceId] [GeoFenceId],
		a.[Name] [Name],
		a.[Description] [Description],
		a.[VehicleGroupId] [VehicleGroupId],
		ROW_NUMBER() OVER (ORDER BY a.[VehicleGroupId]) AS [RowNumber] 
	FROM 
		[General].[VehicleGroups] a
		INNER JOIN [Efficiency].[VehicleGroupsByGeoFence] x ON a.[VehicleGroupId] = x.[VehicleGroupId]
	WHERE x.[GeoFenceId] = @pGeoFenceId
	
    SET NOCOUNT OFF
END




