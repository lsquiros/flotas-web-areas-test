USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_Balance_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCard_Balance_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CreditCard Balance information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCard_Balance_Retrieve]
(
	  @pCreditCardNumber NVARCHAR(520)		--@pCustomerId: Customer Id
	 ,@pExpirationYear INT = NULL			--@pExpirationYear: Expiration Year
	 ,@pExpirationMonth INT	= NULL			--@pExpirationMonth: Expiration Month
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		a.[CreditAvailable] + a.[CreditExtra] AS CreditAvailable
    FROM [Control].[CreditCard] a
	WHERE a.[CreditCardNumber] = @pCreditCardNumber
	  AND (@pExpirationYear IS NULL OR a.[ExpirationYear] = @pExpirationYear)
	  AND (@pExpirationMonth IS NULL OR a.[ExpirationMonth] = @pExpirationMonth)
	  
    SET NOCOUNT OFF
END
 