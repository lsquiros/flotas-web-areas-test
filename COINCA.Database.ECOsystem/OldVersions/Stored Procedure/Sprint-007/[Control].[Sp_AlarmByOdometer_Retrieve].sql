USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AlarmByOdometer_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_AlarmByOdometer_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Mart�nez H.
-- Create date: 22/Oct/2014
-- Description:	Alarm By Odometer information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_AlarmByOdometer_Retrieve]
(
	 @pCustomerId INT						--@pCustomerId: FK of Customer
	,@pAlarmOdometerId INT = NULL 
	,@pKey VARCHAR(800) = NULL 
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		 a.[AlarmOdometerId]
		,a.[VehicleId]
		,b.[PlateId]
		,a.[Date]
		,a.[ExpectedOdometer]
		,a.[ReportedOdometer]
	FROM [Control].[AlarmByOdometer] a
		INNER JOIN [General].[Vehicles] b
		ON a.[VehicleId] = b.[VehicleId]
	WHERE  b.[CustomerId] = @pCustomerId
		AND (@pAlarmOdometerId IS NULL OR a.[AlarmOdometerId] = @pAlarmOdometerId)
		AND a.[Fixed] = 0
		AND (@pkey IS NULL 
				OR a.[ExpectedOdometer] LIKE '%'+@pkey+'%'
				OR a.[ReportedOdometer] LIKE '%'+@pkey+'%'
				OR b.[PlateId] LIKE '%'+@pkey+'%')
	
    SET NOCOUNT OFF
END

