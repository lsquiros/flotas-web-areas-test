USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_RegisteredDriversCount_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Insurance].[Sp_RegisteredDriversCount_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes
-- Create date: 18/03/2015
-- Description:	Registered Drivers Count information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_RegisteredDriversCount_Retrieve]
(
	@pPartnerId INT=null,
	@pCustomerId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	
	;WITH WITH_TABLE
	AS
	-- Define the CTE query.
	(
		Select c.CustomerId , c.Name from General.[CustomersByPartner] cbp INNER JOIN General.Customers c ON cbp.CustomerId=c.CustomerId where PartnerId=@pPartnerId and cbp.CustomerId=ISNULL(@pCustomerId,cbp.CustomerId)
	)
	SELECT distinct
		   COUNT(d.UserId)
    FROM General.DriversUsers d
        INNER JOIN [General].[Customers] a ON d.CustomerId=A.CustomerId
        INNER JOIN WITH_TABLE w ON w.CustomerId=a.CustomerId
		
	WHERE a.[IsDeleted]=CAST(0 as bit)
	  and a.IsActive=1
	  AND (@pCustomerId IS NULL OR a.[CustomerId] = @pCustomerId)
	
	SET NOCOUNT OFF
END
