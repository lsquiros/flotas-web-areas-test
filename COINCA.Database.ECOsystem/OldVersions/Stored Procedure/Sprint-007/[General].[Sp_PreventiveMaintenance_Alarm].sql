USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenance_Alarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PreventiveMaintenance_Alarm]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Martínez 
-- Create date: 24/11/2014
-- Description:	Add Alarm for Preventive Maintenance
-- ================================================================================================
create PROCEDURE [General].[Sp_PreventiveMaintenance_Alarm]
	@pAlarmId INT,
	@pVehicleId INT,
	@pCustomerId INT, 
	@pAlarmTriggerId INT,
	@pPeriodicityAlarm INT,
	@pPhone VARCHAR(100), 
	@pEmail VARCHAR (500),
	@pRowVersion TIMESTAMP
AS
BEGIN
		DECLARE @lErrorMessage NVARCHAR(4000)
		DECLARE @lErrorSeverity INT
		DECLARE @lErrorState INT
		DECLARE @lPeriocityCode VARCHAR(400)
		DECLARE	@lOdometer INT
		DECLARE	@lNextDateReview DATETIME
		DECLARE	@lAlertBefore INT
		DECLARE	@lNextReview INT
		DECLARE	@lMessage VARCHAR (700)
		DECLARE	@lValue INT
		DECLARE	@lPlate VARCHAR(10)
		DECLARE	@lDescription VARCHAR(500)
		DECLARE	@lYear INT
		DECLARE	@lMonth INT
		DECLARE	@lDay INT				
		DECLARE	@lUTC INT				
		
		BEGIN TRY 
			
			SELECT @lPeriocityCode = c.[Code]
				  ,@lAlertBefore = b.[AlertBefore]
				  ,@lNextReview = a.[NextReview]
				  ,@lPlate = d.[PlateId]
				  ,@lYear = a.[Year]
			      ,@lMonth = a.[Month]
				  ,@lDay = a.[Day]
				  ,@lDescription = b.[Description]
				  ,@lUTC = CONVERT(int, e.Value) 
			FROM [General].[PreventiveMaintenanceByVehicle] a
				INNER JOIN [General].[PreventiveMaintenanceCatalog] b 
					  ON a.[PreventiveMaintenanceCatalogId] = b.[PreventiveMaintenanceCatalogId]
				INNER JOIN [General].[Types] c
					ON b.[FrequencyTypeId] = c.[TypeId]
				INNER JOIN [General].[Vehicles] d 
					ON a.[VehicleId] = d.[VehicleId]
				INNER JOIN General.ParametersByCustomer e 
					ON a.[CustomerId] = e.[CustomerId]
			WHERE a.[VehicleId] = @pVehicleId
			  AND a.[CustomerId] = @pCustomerId 
			  AND e.[ResuourceKey] = 'CUSTOMER_UTC'
			  
			-- Periocity Type Kilometers			  
			IF @lPeriocityCode = 'CUSTOMER_FREQUENCY_KILOMETERS_KEY'
			BEGIN 
				SELECT @lOdometer = [Odometer]
				FROM [Control].[Transactions]
				WHERE VehicleId = @pVehicleId 
				ORDER BY VehicleId DESC
			
				IF @lOdometer IS NOT NULL AND @lOdometer >= (@lNextReview-@lAlertBefore) AND @lOdometer<@lNextReview
				BEGIN 	
					EXEC [General].[Sp_SendAlarm] @pAlarmId = @pAlarmId,
												  @pAlarmTriggerId = @pAlarmTriggerId,
												  @pPeriodicityAlarm = @pPeriodicityAlarm,
												  @pCustomerId = @pCustomerId,
											      @pPlate = @lPlate,
												  @pDescription = @lDescription,	
												  @pPhone = @pPhone, 
												  @pEmail = @pEmail,
												  @pRowVersion = @pRowVersion
				END 			
			END 
		
			IF @lPeriocityCode = 'CUSTOMER_FREQUENCY_MONTHS_KEY'
			BEGIN
				SET @lNextDateReview = DATEADD(mm, (@lYear - 1900) * 12 + @lMonth - 1 , 30 - 1)
				SET @lNextDateReview = DATEADD(MONTH, @lNextReview, @lNextDateReview) 
				IF @lNextDateReview IS NOT NULL AND CONVERT(DATE,DATEADD(Hour, @lUTC,GETUTCDATE())) >= CONVERT(DATE,(DATEADD(MONTH, -(@lAlertBefore),@lNextDateReview))) AND CONVERT(DATE,DATEADD(Hour, @lUTC,GETUTCDATE()))<=CONVERT(DATE,@lNextDateReview)
				BEGIN 
					EXEC [General].[Sp_SendAlarm] @pAlarmId = @pAlarmId,
												  @pAlarmTriggerId = @pAlarmTriggerId,
												  @pPeriodicityAlarm = @pPeriodicityAlarm,
												  @pCustomerId = @pCustomerId,
											      @pPlate = @lPlate,
												  @pDescription = @lDescription,	
												  @pPhone = @pPhone, 
												  @pEmail = @pEmail,
												  @pRowVersion = @pRowVersion
				END 
			END 			
		
			IF @lPeriocityCode = 'CUSTOMER_FREQUENCY_DATE_KEY'
			BEGIN 
				SET @lNextDateReview = DATEADD(mm, (@lYear - 1900) * 12 + @lMonth - 1 , @lDay - 1)
				SET @lNextDateReview = DATEADD(MONTH, @lNextReview, @lNextDateReview) 
				IF @lNextDateReview IS NOT NULL AND CONVERT(DATE,DATEADD(Hour, @lUTC,GETUTCDATE())) >= CONVERT(DATE,(DATEADD(DAY, -(@lAlertBefore),@lNextDateReview))) AND CONVERT(DATE,DATEADD(Hour, @lUTC,GETUTCDATE()))<=CONVERT(DATE,@lNextDateReview)
				BEGIN 
					EXEC [General].[Sp_SendAlarm] @pAlarmId = @pAlarmId,
												  @pAlarmTriggerId = @pAlarmTriggerId,
												  @pPeriodicityAlarm = @pPeriodicityAlarm,
												  @pCustomerId = @pCustomerId,
											      @pPlate = @lPlate,
												  @pDescription = @lDescription,	
												  @pPhone = @pPhone, 
												  @pEmail = @pEmail,
												  @pRowVersion = @pRowVersion
				END 
			END
		END TRY 
		BEGIN CATCH 
			SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
			RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		END CATCH 
END 


