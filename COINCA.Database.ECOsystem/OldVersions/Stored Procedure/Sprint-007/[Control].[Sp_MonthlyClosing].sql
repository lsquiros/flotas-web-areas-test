USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_MonthlyClosing]    Script Date: 07/24/2015 11:28:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MonthlyClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_MonthlyClosing]
GO

USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_MonthlyClosing]    Script Date: 07/24/2015 11:28:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================
-- Author:		Napole�n Alvarado
-- Create date: 12/03/2014
-- Description:	Execute tasks of the Monthly Closing
-- ===============================================================
CREATE Procedure [Control].[Sp_MonthlyClosing]
AS 
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0 
        DECLARE @lCurrentMonth INT
        DECLARE @lCurrentYear INT
        DECLARE @lClosingMonth INT
        DECLARE @lClosingYear INT
        DECLARE @lProcessDate DATETIME		
        DECLARE @lCount INT	
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END

        Set @lProcessDate = GETDATE()
        Set @lCurrentMonth = DATEPART(MM, @lProcessDate)
        Set @lCurrentYear = DATEPART(YY, @lProcessDate)
        IF @lCurrentMonth = 1
        BEGIN
			Set @lClosingMonth = 12
			Set @lClosingYear = @lCurrentYear - 1
		END
		ELSE
		BEGIN
			Set @lClosingMonth = @lCurrentMonth - 1
			Set @lClosingYear = @lCurrentYear
		END
		
		Select @lCount = COUNT(1)
		  from Control.CustomerCredits
		 where Year = @lCurrentYear
		   and Month = @lCurrentMonth
		   
		IF @lCount = 0
		BEGIN    
			--Copy CustomerCredit data for the Current Month		
			Insert into Control.CustomerCredits
				(CustomerId, Year, Month, CreditAmount, CreditAssigned, CreditTypeId, InsertDate)
			Select CustomerId, @lCurrentYear, @lCurrentMonth, CreditAmount, 0, CreditTypeId, @lProcessDate
			  from Control.CustomerCredits 
			 where Year = @lClosingYear
			   and Month = @lClosingMonth 
			   
			--There is a Card but no record in CustomerCredit
			Insert into Control.CustomerCredits
				(CustomerId, Year, Month, CreditAmount, CreditAssigned, CreditTypeId, InsertDate)
			Select CCC.CustomerId, @lCurrentYear, @lCurrentMonth, CCC.CreditLimit, 0, 1000, @lProcessDate
			  from General.CustomerCreditCards CCC 
			 inner join (Select CustomerID, Max(CustomerCreditCardId) as CustomerCreditCardId 
						  from General.CustomerCreditCards
						 where StatusId in (100, 101)
						 group by CustomerID) CLast
				on CCC.CustomerId = CLast.CustomerId and CCC.CustomerCreditCardId = CLast.CustomerCreditCardId
			 where not exists (Select C.CustomerId
								 from Control.CustomerCredits C
								where C.CustomerId = CCC.CustomerId
								  and C.Year = @lCurrentYear
								  and C.Month = @lCurrentMonth)
	        
			--Update Credit Assigned for CustomerCredit
			Update C
			   set C.CreditAssigned = IsNull(CCards.SumCreditLimit, 0) 
			  from Control.CustomerCredits C
			  left join (Select CC.CustomerId, SUM(CC.CreditLimit) as SumCreditLimit
						   from Control.CreditCard CC
						  where StatusId in (0, 2, 3, 4, 6, 8)
						  group by CC.CustomerId) CCards
				on C.CustomerId = CCards.CustomerId 
			 where C.Year = @lCurrentYear
			   and C.Month = @lCurrentMonth
	    END
	       
		Select @lCount = COUNT(1)
		  from Control.FuelsByCredit
		 where Year = @lCurrentYear
		   and Month = @lCurrentMonth
		   
		IF @lCount = 0
		BEGIN    
			--Copy FuelsByCredit data for the Current Month
			Insert into Control.FuelsByCredit
				(CustomerId, FuelId, Month, Year, Total, Assigned, InsertDate)
			Select CustomerId, FuelId, @lCurrentMonth, @lCurrentYear, Total, 0, @lProcessDate
			  from Control.FuelsByCredit
			 where Year = @lClosingYear
			   and Month = @lClosingMonth
		END
		   
		Select @lCount = COUNT(1)
		  from Control.VehicleFuel
		 where Year = @lCurrentYear
		   and Month = @lCurrentMonth
		   
		IF @lCount = 0
		BEGIN    
			--Copy VehicleFuel data for the Current Month
			Insert into Control.VehicleFuel
				(VehicleId, Month, Year, Liters, Amount, InsertDate)
			Select VF.VehicleId, @lCurrentMonth, @lCurrentYear, Case When PF.LiterPrice is null then VF.Liters else VF.Amount / PF.LiterPrice end, VF.Amount, @lProcessDate
			  from Control.VehicleFuel VF
			 inner join General.Vehicles V on VF.VehicleId = V.VehicleId
			 inner join General.VehicleCategories VC on V.VehicleCategoryId = VC.VehicleCategoryId
			 inner join General.Customers C on V.CustomerId = C.CustomerId
			 inner join General.CustomersByPartner cp on cp.CustomerId = c.CustomerId
			 left join Control.PartnerFuel PF on C.CustomerId = cp.CustomerId and cp.PartnerId = PF.PartnerId and VC.DefaultFuelId = PF.FuelId and PF.EndDate is null
			 where VF.Year = @lClosingYear
			   and VF.Month = @lClosingMonth
			   and V.Active = 1
			
			--Update FuelsByCredit Assigned Amount   
			Update FC
			   set FC.Assigned = VFG.Amount
			  from Control.FuelsByCredit FC
			 inner join (Select V.CustomerId, VC.DefaultFuelId, Sum(VF.Amount) Amount
						  from Control.VehicleFuel VF
						 inner join General.Vehicles V on VF.VehicleId = V.VehicleId
						 inner join General.VehicleCategories VC on V.VehicleCategoryId = VC.VehicleCategoryId
						 where VF.Year = @lCurrentYear
						   and VF.Month = @lCurrentMonth
						 group by V.CustomerId, VC.DefaultFuelId) VFG
				on FC.CustomerId = VFG.CustomerId and FC.FuelId = VFG.DefaultFuelId
			 where FC.Year = @lCurrentYear
			   and FC.Month = @lCurrentMonth
		END

		--Se restablece el monto disponible de la tarjeta
		update Control.CreditCard
		set CreditAvailable = CreditLimit
		WHERE StatusId IN (7)

	    --Warning Emails 
	    Exec Control.Sp_MonthlyClosing_WarningEmails @lCurrentYear, @lCurrentMonth
	    
	    EXEC [Control].[Sp_MonthlyClosing_LowPerformanceEmails] @lCurrentYear, @lCurrentMonth
		
        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
			      
         
	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO


