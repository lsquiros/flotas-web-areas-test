USE [ECOsystem]
GO

/****** Object:  StoredProcedure [dbo].[Sp_SpeedingCalculate]    Script Date: 6/1/2015 12:51:13 PM ******/
DROP PROCEDURE [dbo].[Sp_SpeedingCalculate]
GO

/****** Object:  StoredProcedure [dbo].[Sp_SpeedingCalculate]    Script Date: 6/1/2015 12:51:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 --Batch submitted through debugger: [Sp_SpeedingCalculate].sql|7|0|C:\Users\crladesa\Documents\SQL Server Management Studio\Projects\Ecosystem\REportes exceso Velocidad\[Sp_SpeedingCalculate].sql
CREATE Procedure [dbo].[Sp_SpeedingCalculate]
	@CountRows as  int
 AS

--Declare @CountRows as  integer = 200

BEGIN
----- Declaraciones---------------
Create table #Reports
	(Report  bigint not null, 
	  Device int,
	  ReportID int,
	  GPSDateTime datetime not null,
	  Longitude float not null ,
	  Latitude  float not null,
	  VSSSpeed  float not null, 
	  Heading int ,  
	  SvrDateTime  DATETIME not null,
	  Odometer float not null,
	  satellites int,
	  VehicleId int,
	  CustomerId int,
	  AdministrativeSpeedLimit int);
	  
Create table #nearbypoints
	(IDpoint int,
	 distance  bigint not null, 
	 azimuth int,
	 azimuthdiff int not null,
	 SpeedMax int not null,
	 vminp int not null);
	 
DECLARE  @PointReport  bigint,
	@Report bigint,
	@Device int,
	@ReportID int,
	@GPSDateTime datetime,
	@Longitude float,
	@Latitude float,
	@VSSSpeed  float, 
	@Heading int,  
	@SvrDateTime  datetime,
	@distancepoint int,
	@PermittedSpeed int,
	@PromedioVelExceso  decimal,
	@CantExcesoVel as int,
	@Odometer int,
	@Satellites int,
	@SumVelExceso decimal,
	@DistanceTotalExceso   float,
	@PointID int =0,
	@minspeed integer,-- =25,
	@distancemaxpoints int,-- = 30,
	@difereceUtc int,-- = 6 ,
	@LastReport bigint,
	@Reportindex bigint,
	@AzimuthDiff int,
	@Azimuth  int,
	@AdministrativeSpeed int

	SELECT  @PointReport = ReportSpeding FROM dbo.ProcessPointer ---WHERE Device = @Device

	--Asignar valores Parametrizados
	Set @minspeed = dbo.GetNumericParameter('MinSpeedForEvaluation', 'NA')
	Set @distancemaxpoints = dbo.GetNumericParameter('MaxDistancePoints', 'NA')
	Set @difereceUtc = dbo.GetNumericParameter('DifferenceHoursUTC', 'NA')

	Insert into  #Reports
		(Report, Device, ReportID, GPSDateTime, Longitude, Latitude, VSSSpeed, Heading, SvrDateTime, 
		 Odometer, satellites, VehicleId, CustomerId, AdministrativeSpeedLimit)
	SELECT R.Report, R.Device, R.ReportID, R.GPSDateTime, R.Longitude, R.Latitude, R.VSSSpeed, R.Heading, R.SvrDateTime, 
		 R.Odometer, R.Satellites, V.VehicleId, V.CustomerId, V.AdministrativeSpeedLimit
	  FROM dbo.Reports R 
	  inner join dbo.VehiclesECOSystem V on R.Device = V.DeviceId
	 WHERE R.Report > @PointReport 
	   AND R.Report < @PointReport +  @CountRows  
	   and R.GPSDateTime <  DATEADD(hour,@difereceUtc + 1,GETDATE()) 
	   and (R.VSSSpeed > @minspeed or R.VSSSpeed > V.AdministrativeSpeedLimit)
	 order by R.Report Asc   -- Device = @Device

	SELECT @Report = min(Report)
	  FROM  #Reports WITH (NOLOCK)

	BEGIN TRANSACTION
	While @Report is not null
	begin
		SELECT @Report = Report,
			   @Device = device, 
			   @ReportID = ReportID,
			   @GPSDateTime= GPSDateTime, 
			   @Longitude = Longitude, 
			   @Latitude =Latitude, 
			   @VSSSpeed = VSSSpeed, 
			   @Heading = Heading, 
			   @SvrDateTime = SvrDateTime,
			   @Odometer =Odometer,
			   @satellites= satellites,  
			   @AdministrativeSpeed = AdministrativeSpeedLimit
		FROM   #Reports 
		WHERE  Report = @Report 
				
		--Select  @AdministrativeSpeed = AdminitrativeSpeed from #AdministrativeSpeed  where Device = @Device 
		
		IF  @VSSSpeed	 >  @minspeed or @VSSSpeed  > @AdministrativeSpeed-- si  la  velocidad es  menor  a la  minima evaluada  no se considera.
		BEGIN	
			Insert into #nearbypoints
			(IDpoint,distance, azimuth,azimuthdiff,SpeedMax,vminp)
			EXEC dbo.[Sp_GetPointStreet] @Longitude, @Latitude, @distancemaxpoints, @Heading
	
			--Select *  FROM #nearbypoints
			DECLARE @countnearbypoints int =0
			select  @countnearbypoints = COUNT(*)  from #nearbypoints 
			if @countnearbypoints > 0
			BEGIN
				--Si   todos estan punto cercano estan  SIN Azimuth toma la mayor velocidad.
				SET  @AzimuthDiff=-1 
				
				IF ( SELECT COUNT(*) FROM #nearbypoints where azimuth = -1 ) = (@countnearbypoints)
				BEGIN
					-- no me    funciono de  esta  forma SELECT @PermittedSpeed = SpeedMax,  @PointID =IDpoint FROM #nearbypoints where IDpoint in  ( Select MAX (SpeedMax)  FROM #nearbypoints)    --se   considera  el puntocon maxima  velocidad permitida .
					SELECT  tOP(1) @PermittedSpeed = SpeedMax,  @PointID =IDpoint,  @Azimuth =Azimuth  , @AzimuthDiff = azimuthdiff  FROM #nearbypoints ORDER BY  SpeedMax dESC
					
					SET @AzimuthDiff=-1
					SET @Azimuth=-1
						 
					--IF @AdministrativeSpeed > 0 and @VSSSpeed > @AdministrativeSpeed
					IF (@AdministrativeSpeed > 0 and @VSSSpeed > @AdministrativeSpeed) or (@VSSSpeed  > @PermittedSpeed)
					BEGIN
						INSERT dbo.Speeding
							(device,PermittedSpeed ,ActualSpeed,Date_Time ,Longitude,Latitude,Report,PointID,AzimuthDiff, Azimuth,Odometer,Heading,  AdministrativeSpeed)  
						VALUES (@device,@PermittedSpeed, @VSSSpeed,@GPSDateTime,@Longitude, @Latitude, @Report, @PointID,  @AzimuthDiff,@Azimuth ,@Odometer, @Heading,@AdministrativeSpeed)--CAST(@GPSDateTime AS DATETIME
					END
				END
				
				
				-----------------------TODOS o Algunos AZIMUTH---------------------
				Else
				BEGIN
					SET  @PermittedSpeed =null
					--escoge  el   Azimuth menor de   45 grados para  insertarlo
					SELECT  top(1) @PermittedSpeed = SpeedMax  , @PointID = IDPoint , @AzimuthDiff=AzimuthDiff ,  @Azimuth =Azimuth  FROM  #nearbypoints  Where  azimuthdiff < 45 order by azimuthdiff ASC  --se   considera  el punto con  menor aZimut
					IF (@PermittedSpeed is  null)	
					--'como NO hay azimuth menores de 45 grados tomo la maxima  velocidad de todos los  puntos.
					BEGIN
							--se   considera  el puntocon maxima  velocidad permitida .
							 SELECT  top(1) @PermittedSpeed = SpeedMax, @PointID = IDPoint  FROM  #nearbypoints  order by SpeedMax desc 
							 SET @AzimuthDiff=-1
							 SET @Azimuth=-1
					END
					
					--IF 	@AdministrativeSpeed > 0 and @VSSSpeed > @AdministrativeSpeed
					IF (@AdministrativeSpeed > 0 and @VSSSpeed > @AdministrativeSpeed) or (@VSSSpeed  > @PermittedSpeed)
					BEGIN
						INSERT  dbo.Speeding 
							(device,PermittedSpeed ,ActualSpeed,Date_Time ,Longitude,Latitude,Report,PointID,AzimuthDiff, Azimuth,Odometer, Heading,AdministrativeSpeed)  
						VALUES (@device,@PermittedSpeed, @VSSSpeed,@GPSDateTime,@Longitude, @Latitude, @Report, @PointID,   @AzimuthDiff,@Azimuth, @Odometer, @Heading,@AdministrativeSpeed)
					END
				END
			   END
			   
	     END --IF
		---------Siguiente   Reporte*******************
		Delete From #nearbypoints
		SET @AzimuthDiff=-1
		 
		SELECT @Report = min(Report)FROM  #Reports WITH (NOLOCK) where Report > @Report 
		if @Report is not null and 	@Report > 0
		BEGIN
			set @LastReport = @Report
		END 
	END-- While

	DROP Table #nearbypoints
	DROP Table #Reports

	if @LastReport > 0	
		BEGIN
			UPDATE dbo.ProcessPointer SET ReportSpeding = @LastReport
		END
	ELSE
		BEGIN
			UPDATE dbo.ProcessPointer SET ReportSpeding = @PointReport + @CountRows
		END
		
	COMMIT TRANSACTION
	
	--C�lculo de las distancias para los registros nuevos o sin c�lculo
	BEGIN TRANSACTION
	Declare @SpeedingDistance table
		(SpeedingDistanceId int identity(1,1),
		SpeedingId bigint,
		Device int,
		ActualSpeed float,
		SpeedingDate datetime,
		Longitude float,
		Latitude float)
		
	Declare @MaxSecsBetweenPoints int,
			@PenaltySecs int,
			@TotalRecords int,
			@Record int,
			@SpeedingIdPoint1 bigint,
			@SpeedingIdPoint2 bigint,
			@SpeedPoint1 float,
			@SpeedPoint2 float,
			@DevicePoint1 int,
			@DevicePoint2 int,
			@DatePoint1 datetime,
			@DatePoint2 datetime,
			@LongitudePoint1 float,
			@LatitudePoint1 float,
			@LongitudePoint2 float,
			@LatitudePoint2 float,
			@SecsDiff int,
			@Distance float,
			@DistanceIndicator bit,
			@point1 geography,
			@point2 geography
	
	Set @MaxSecsBetweenPoints = dbo.GetNumericParameter('MaxSecsBetweenPoints', 'NA')
	Set @PenaltySecs = dbo.GetNumericParameter('PenaltySecsExcessSpeed', 'NA')
			
	Insert into @SpeedingDistance
		(SpeedingId, Device, ActualSpeed, SpeedingDate, Longitude, Latitude)
	Select SpeedingId, device, ActualSpeed, Date_Time, Longitude, Latitude
	  from dbo.Speeding
	 where DistanceIndicator = 0
	 order by device, Date_Time
	 
	Set @TotalRecords = @@ROWCOUNT
	Set @Record = 1
			
	If @TotalRecords > 0
		--Obtener informaci�n del primer punto
		Select @SpeedingIdPoint1 = SpeedingId, @DevicePoint1 = Device, @SpeedPoint1 = ActualSpeed, @DatePoint1 = SpeedingDate, @LongitudePoint1 = Longitude, @LatitudePoint1 = Latitude
		  from @SpeedingDistance
		 where SpeedingDistanceId = @Record 		 

	While @Record <= @TotalRecords
	begin	
		--Obtener informaci�n del punto siguiente
		Select @SpeedingIdPoint2 = SpeedingId, @DevicePoint2 = Device, @SpeedPoint2 = ActualSpeed, @DatePoint2 = SpeedingDate, @LongitudePoint2 = Longitude, @LatitudePoint2 = Latitude
		  from @SpeedingDistance
		 where SpeedingDistanceId = @Record + 1
		
		If @DevicePoint1 = @DevicePoint2
		begin
			--Calcular tiempo entre puntos
			Set @DistanceIndicator = 1
			Set @SecsDiff = DATEDIFF(SS, @DatePoint1, @DatePoint2)
			If (@SecsDiff <= @MaxSecsBetweenPoints and @SecsDiff > 0)
			begin
				--Calcular distancia por coordenadas geogr�ficas
				SET @point1 = geography::STGeomFromText('POINT(' + CAST( @LongitudePoint1 AS VARCHAR(MAX)) +' ' + CAST( @LatitudePoint1  AS VARCHAR(MAX)) + ')', 4326)
				SET @point2 = geography::STGeomFromText('POINT(' + CAST( @LongitudePoint2 AS VARCHAR(MAX)) +' ' + CAST( @LatitudePoint2  AS VARCHAR(MAX)) + ')', 4326)
				--Dividir entre mil para expresar el el resultado en Kms
				Set @Distance = @point1.STDistance(@point2) / 1000
			end
			else
			begin
				If @SecsDiff > 0
				begin
					--Calcular distancia por tiempo de Castigo
					SET @Distance = ( SELECT cast((  cast(@SpeedPoint1 as float) / cast(3600 as float)) as float)* @PenaltySecs)
				end
				else
				begin
					Set @Distance = 0 
				end
			end
		end 
		else
		begin 
			--Calcular distancia por tiempo de Castigo pero queda como un c�lculo no definitivo
			Set @DistanceIndicator = 0
			SET @Distance = ( SELECT cast((  cast(@SpeedPoint1 as float) / cast(3600 as float)) as float)* @PenaltySecs)
		end
		
		--Actualizar tabla Speeding con la distancia calculada
		Update dbo.Speeding
		   set DistanceIndicator = @DistanceIndicator,
		       Distance = @Distance
		 where SpeedingId = @SpeedingIdPoint1
		 
		--Preparar datos para el siguiente ciclo
		Set @SpeedingIdPoint1 = @SpeedingIdPoint2
		Set @DevicePoint1 = @DevicePoint2
		Set @SpeedPoint1 = @SpeedPoint2
		Set @DatePoint1 = @DatePoint2
		Set @LongitudePoint1 = @LongitudePoint2
		Set @LatitudePoint1 = @LatitudePoint2
		Set @DevicePoint2 = null	
		Set @Record += 1 
	end		
	
	COMMIT TRANSACTION
end
---store



GO


