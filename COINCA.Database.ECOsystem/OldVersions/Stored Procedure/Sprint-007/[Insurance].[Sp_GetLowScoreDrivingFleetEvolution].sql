USE [ECOSystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetLowScoreDrivingFleetEvolution]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Insurance].[Sp_GetLowScoreDrivingFleetEvolution]
GO
USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo
-- Create date: 13/02/2015
-- Description:	Retrieve Low Score Driving Fleet Evolution information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_GetLowScoreDrivingFleetEvolution]
( @pPartnerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)=null,		--Posibles valores S:Summarized, D:Detailed
  @pMonthsQuantity int,
  @pLowScore int,
  @pCustomerId int=Null
)
AS BEGIN

DECLARE @tableCustomers TABLE (CustomerId int, Name varchar(250)) 
DECLARE  @li_CustomerId INT
DECLARE @NameCustomer varchar(250)
DECLARE @tableDriversScore TABLE(
DriverScoreId int
,UserId	int
,Name  varchar (250)
,Identification  varchar(50)	
,ScoreType	 varchar(5)
,Score Float
,Photo varchar(max)
,ScoreSpeedAdmin Float
,ScoreSpeedRoute Float
)
declare @pMQuantity int =@pMonthsQuantity

DECLARE @ScoreDrivingFleet TABLE(
CustomerId int
,EncryptedName	varchar (1000)
,AverageScore  FLOAT
,[Month] INT
,[Year] INT

)
	Declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT('0' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end
		
DECLARE @Dates TABLE(
	Id INT
	,d_month int
	,d_year	int
)

SET @pMonthsQuantity=@pMonthsQuantity-1

while @pMonthsQuantity>0
begin
	declare @pM INT =MONTH(DATEADD(mm,-@pMonthsQuantity,@pInitialDate))
	declare @pY INT =YEAR(DATEADD(mm,-@pMonthsQuantity,@pInitialDate))
	
	insert @Dates values(@pMonthsQuantity+1,@pM,@pY)
	set @pMonthsQuantity=@pMonthsQuantity-1
	
end

insert into @Dates values(1,MONTH(@pInitialDate),YEAR(@pFinalDate))

INSERT INTO @tableCustomers
EXEC  [Insurance].[Sp_GetRosterInsurance]
		@pPartnerId,@pCustomerId
---SELECT * from @tableCustomers 

DECLARE @count INT=(select count(*) from @Dates)

WHILE @count>0
begin

Select @li_CustomerId = MIN(CustomerId)
from @tableCustomers

WHILE @li_CustomerId is not null
begin

declare @MONTH INT=(select d_month from @Dates where id=@count)
declare @YEAR INT=(select d_year from @Dates where id=@count)


INSERT into   @tableDriversScore  (DriverScoreId,UserId,Name,Identification,ScoreType,Score,Photo ,ScoreSpeedAdmin,ScoreSpeedRoute)
EXEC	 [Operation].[Sp_GetGlobalScoreAllDriver]
		@li_CustomerId ,
		@YEAR ,
		@MONTH ,
		NULL ,
		NULL ,
		@pReportType

Select @NameCustomer = Name FROM  @tableCustomers where CustomerId=@li_CustomerId

insert into @ScoreDrivingFleet SELECT @li_CustomerId CustomerId, @NameCustomer  as EncryptedName ,CASE  WHEN AVG(Score) IS NULL THEN 0 ELSE AVG(Score)  END AS  AverageScore,@MONTH,@YEAR
FROM  @tableDriversScore 
Delete  FROM  @tableDriversScore 

-----------------------------
--Siguiente Customer
		Select @li_CustomerId = MIN(CustomerId)
		  from @tableCustomers 
		 where CustomerId > @li_CustomerId
END  -- End While
set @count=@count-1
end

select * from @ScoreDrivingFleet as sdf where (select top 1 (case when sum(AverageScore)=0 then @pMQuantity else sum(AverageScore) end) /@pMQuantity from @ScoreDrivingFleet s where sdf.CustomerId=s.CustomerId)<=@pLowScore group by CustomerId,EncryptedName,AverageScore,[Month],[Year] order by AverageScore,[YEAR],[Month]
END
GO