USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Sub Unit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		 a.[Name] AS [VehicleUnitName]
		,ISNULL(t.[RealAmount],0) AS [RealAmount]
		,s.[AssignedAmount]
		,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) AS [RealAmountPct]
		,@lCurrencySymbol AS [CurrencySymbol]
		,a.UnitId
	FROM [General].[VehicleUnits] a
		LEFT JOIN (SELECT
						 z.[UnitId]
						,SUM(x.[FuelAmount]) AS [RealAmount]
					FROM [Control].[Transactions] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
						INNER JOIN [General].[VehicleCostCenters] z
							ON z.CostCenterId = y.CostCenterId
					WHERE y.[CustomerId] = @pCustomerId
					  AND x.[IsFloating] = 0
					  AND x.[IsReversed] = 0
					  AND x.[IsDuplicated] = 0
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,x.[Date]) = @pMonth
								AND DATEPART(yyyy,x.[Date]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
					GROUP BY z.[UnitId])t
			ON a.UnitId = t.UnitId
		INNER JOIN (SELECT
						 z.[UnitId]
						,SUM(x.[Amount]) AS [AssignedAmount]
					FROM [Control].[VehicleFuel] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
						INNER JOIN [General].[VehicleCostCenters] z
							ON z.CostCenterId = y.CostCenterId
					WHERE y.[CustomerId] = @pCustomerId
					  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND x.[Month] = @pMonth
								AND x.[Year] = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
								AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
					GROUP BY z.[UnitId])s
			ON a.[UnitId] = s.[UnitId]
		
	
    SET NOCOUNT OFF
END
