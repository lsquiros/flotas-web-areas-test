USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesPoints_Retrieve]    Script Date: 11/06/2014 10:24:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesPoints_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_RoutesPoints_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesPoints_Retrieve]    Script Date: 11/06/2014 10:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	Retrieve RoutesPoints information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_RoutesPoints_Retrieve]
(
	 @pRouteId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT [PointId]
		,[RouteId]
		,[PointReference]
		,[Order]
		,[Name]
		,CONVERT(varchar(20), [Time]) as [Time]
		,CONVERT(varchar(50), [Latitude]) as [Latitude]
		,CONVERT(varchar(50), [Longitude]) as [Longitude]
	FROM 
		[Efficiency].[RoutesPoints]
	where [RouteId] = @pRouteId
	
    SET NOCOUNT OFF
END




GO


