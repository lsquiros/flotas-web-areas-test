USE [ECOsystem]
GO

/****** Object:  StoredProcedure [General].[Sp_Users_Retrieve2]    Script Date: 6/5/2015 2:44:44 PM ******/
DROP PROCEDURE [General].[Sp_Users_Retrieve2]
GO

/****** Object:  StoredProcedure [General].[Sp_Users_Retrieve2]    Script Date: 6/5/2015 2:44:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Andres Oviedo B.
-- Create date: 09/01/2015
-- Description:	Retrieve User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Users_Retrieve2]
(
	  @pUserId INT = NULL
	 ,@pKey VARCHAR(800) = NULL
	 ,@pUserName VARCHAR(800) = NULL
	 ,@pCustomerId INT = NULL
	 ,@pCustomerId2 INT = NULL
	 ,@pPartnerId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT
		 a.[UserId]
		,a.[Name] AS [EncryptedName]
		,a.[ChangePassword]
		,a.[Photo]		
		,CONVERT(BIT,CASE WHEN f.[DriversUserId] IS NOT NULL THEN 1 ELSE 0 END) AS IsDriverUser
			
    FROM [General].[Users] a		
		LEFT JOIN [General].[DriversUsers] f
			ON f.[UserId] = a.[UserId]	

	WHERE a.[IsDeleted] = 0
	  AND (@pUserId IS NULL OR a.[UserId] = @pUserId) 
	  AND (@pKey IS NULL
				OR a.[Name] like '%'+@pKey+'%'				
				OR f.[Identification] like '%'+@pKey+'%'
				OR f.[Code] like '%'+@pKey+'%'
				OR f.[License] like '%'+@pKey+'%'
				OR f.[Dallas] like '%'+@pKey+'%')
	  AND (@pCustomerId2 IS NULL
				OR f.[CustomerId] = @pCustomerId2)
					 
	ORDER BY [UserId] DESC
	
    SET NOCOUNT OFF
END


GO


