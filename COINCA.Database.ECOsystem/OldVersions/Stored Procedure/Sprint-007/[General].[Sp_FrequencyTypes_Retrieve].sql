USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_FrequencyTypes_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_FrequencyTypes_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 06/Oct/2014
-- Description:	Frequency Type information
-- =============================================
CREATE PROCEDURE [General].[Sp_FrequencyTypes_Retrieve]
AS
BEGIN	
	SET NOCOUNT ON;
	SELECT 
		a.[FrequencyTypeId],
		a.[Name]
		FROM
		General.[FrequencyTypes] a
	SET NOCOUNT OFF;
END
