USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesReferences_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesReferences_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Napole�n Alvarado
-- Create date: 12/02/2014
-- Description:	Retrieve Vehicle References information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesReferences_Retrieve]
(
	 @pVehicleId INT = NULL					--@pVehicleId: PK of the table
	,@pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[VehicleId]
		,a.[PlateId]
		,a.[Name]
		,a.[CustomerId]
		,a.[AdministrativeSpeedLimit]
		,a.[IntrackReference]
		,a.[DeviceReference]
		,a.[RowVersion]
    FROM [General].[Vehicles] a
	WHERE (@pVehicleId IS NULL OR [VehicleId] = @pVehicleId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
                OR a.[PlateId] like '%'+@pKey+'%')
	ORDER BY [VehicleId] DESC
	
    SET NOCOUNT OFF
END
