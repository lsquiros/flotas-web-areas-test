USE [ECOsystem]
GO

/****** Object:  StoredProcedure [General].[Sp_VehicleCostCenters_Retrieve2]    Script Date: 6/5/2015 2:43:11 PM ******/
DROP PROCEDURE [General].[Sp_VehicleCostCenters_Retrieve2]
GO

/****** Object:  StoredProcedure [General].[Sp_VehicleCostCenters_Retrieve2]    Script Date: 6/5/2015 2:43:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleCostCenters information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCostCenters_Retrieve2]
(
	 @pCostCenterId INT = NULL					--@pCostCenterId: PK of the table
	,@pCustomerId INT							--@pCustomerId: FK of the table General.Customers
	,@pKey VARCHAR(800) = NULL					--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[CostCenterId]
		,a.[Name]

    FROM [General].[VehicleCostCenters] a		
	 
    SET NOCOUNT OFF
END



GO


