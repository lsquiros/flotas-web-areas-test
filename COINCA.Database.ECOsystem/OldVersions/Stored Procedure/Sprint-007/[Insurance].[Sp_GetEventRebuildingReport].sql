USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetEventRebuildingReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Insurance].[Sp_GetEventRebuildingReport]
GO
USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Operation].[Sp_GetScoreSpeedAllDriver]    Script Date: 11/25/2014 09:11:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 27/02/2015
-- Description:	Get Event Rebuilding Report
-- =============================================

CREATE PROCEDURE [Insurance].[Sp_GetEventRebuildingReport]
( @pCustomerId Int, 
  @pYear int = null,			
  @pMonth int  = null ,			
  @pStartDate datetime  = null,	
  @pEndDate datetime = null,	
  @pReportType char(1)			--Posibles valores S:Summarized, D:Detailed
)
as
BEGIN

	if (select Value from ECOSystem.General.ParametersByCustomer where CustomerId=@pCustomerId and ResuourceKey='SCORE_DRIVING_SETTINGS_VISIBILITY')='1'
	begin
		--Verificar configuración de la ponderación
		Declare @TotalWeight numeric(6,2), 
				@CountWeight int
		
		Select @TotalWeight = IsNull(SUM(Weight),0),
			   @CountWeight = COUNT(1)
		  from Operation.WeightedScoreSettings
		 where CustomerId = @pCustomerId
		 
		Declare @pInitialDate datetime
		declare @pFinalDate datetime
		
		if @pMonth is not null
		begin
			set @pInitialDate=  CAST(
								  CAST(@pYear AS VARCHAR(4)) +
								  RIGHT('0' + CAST(@pMonth AS VARCHAR(2)), 2) +
								  RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
							   AS DATETIME)
			set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
		end
		else
		begin
			set @pInitialDate=@pStartDate
			set @pFinalDate=@pEndDate
		end

		--Set Dates by day
		declare @pDaysTable table(
		date_day datetime
		)
		;WITH date_range (calc_date) AS (
			SELECT DATEADD(DAY, DATEDIFF(DAY, 0, @pFinalDate) - DATEDIFF(DAY, @pInitialDate, @pFinalDate), 0)
				UNION ALL SELECT DATEADD(DAY, 1, calc_date)
					FROM date_range
					WHERE DATEADD(DAY, 1, calc_date) <= @pFinalDate)
		insert @pDaysTable SELECT calc_date
		FROM date_range

		--DECLARE temporal tables
		Declare @VehiclesByUser table
		(Device int,
		 Vehicle int,
		 StartOdometer int,
		 EndOdometer int,
		 StartDate datetime,
		 EndDate datetime,
		 VehicleId int,
		 PlateId varchar(10))

		Create table #LocalDriverScore
		(DriverScoreId INT Identity(1,1),
		 UserId int,
		 Name  varchar (250),
		 Identification varchar(50),
		 ScoreType varchar(5),
		 Score Float,
		 Photo varchar(max),
		 [Date] datetime,
		 VehicleId INT,
		 PlateId varchar(10)
		 )
		--Declare important variables
		Declare @UserId Int
		Declare @Name varchar(250),
				@Identification varchar(50)
			
		Declare  @ScoreAdmOutput nvarchar(4000),
				 @ScoreRouteOutput nvarchar(4000)
				 	
		Declare  @ScoreAdm Float,
				 @ScoreRoute Float

			--Selects the first user of the customer 
			Select @UserId = Min(U.UserId) 
			  from General.Users U 
			  inner join General.DriversUsers D on U.UserId = D.UserId
			 Where U.IsActive =  1 AND D.CustomerId=@pCustomerId

			Declare @date_day datetime
			declare @next_date_day datetime
			--Selects the first day
			Select @date_day = Min(date_day),
			@next_date_day=MIN(date_day)
			from @pDaysTable
					
			set @next_date_day=DATEADD(DAY,1,@date_day)
			
		--Start loop through the day dates
		WHILE @date_day is not null
		BEGIN

			--Begin transaction
			While  @UserId  is not null
			BEGIN
				
				--Inserts into vehicleusers table the vehicles information related to the driver
				Insert into @VehiclesByUser
					(Device, Vehicle, StartOdometer, EndOdometer, StartDate, EndDate,VehicleId,PlateId)  
				Select V.DeviceReference, V.IntrackReference, IsNull(VU.InitialOdometer, 0), IsNull(VU.EndOdometer,0), VU.InsertDate, IsNull(VU.LastDateDriving, GETDATE()),V.VehicleId,V.PlateId
				from General.VehiclesByUser VU 
				inner Join General.Vehicles V ON  V.VehicleId = VU.VehicleId
				inner JOIN General.Users U  ON U.UserId = VU.UserId
				Where U.UserId =  @UserId 
				  And V.DeviceReference is not null
				  and ((VU.LastDateDriving is null) or (@next_date_day<=ISNULL(VU.LastDateDriving,@next_date_day) and @date_day>=ISNULL(VU.InsertDate,@date_day)))
				
				order by V.DeviceReference, VU.InsertDate, VU.LastDateDriving
				
				if (select COUNT(*) FROM @VehiclesByUser ) > 0 
				BEGIN 
					DECLARE @VehiclesByUserString varchar(1000);
					SET @VehiclesByUserString = (SElect Device, Vehicle, StartOdometer, EndOdometer, StartDate, EndDate FROm  @VehiclesByUser AS Lista FOR XML AUTO)
							
					DECLARE @VehicleId INT=(SElect top 1 VehicleId FROm  @VehiclesByUser)
					DECLARE @PlateId VARCHAR(10)=(SElect top 1 PlateId FROm  @VehiclesByUser)
					DECLARE @SQL NVARCHAR(1000)
					
					set @SQL=(SELECT ScoreDrivingFormulaSP FROM Insurance.ScoreDrivingFormula WHERE CustomerId=@pCustomerId AND IsActive=1)
					if @SQL is null
					begin
						Set @SQL='[172.22.220.38\BDS_WEB].[Atrack].[dbo].[Sp_GetScoreDriver]';
					end
					
					EXECUTE  @SQL @date_day, @next_date_day, @VehiclesByUserString, 'A', @pScoreTable = @ScoreAdmOutput OUTPUT
					
					EXECUTE  @SQL @date_day, @next_date_day, @VehiclesByUserString, 'R', @pScoreTable = @ScoreRouteOutput OUTPUT
				
					DECLARE @pScoreAdmTableXML XML;
					SET  @pScoreAdmTableXML = CAST(@ScoreAdmOutput  AS XML);
					
					DECLARE @ScoreAdmTable dbo.TypeDriverScore;
					
					Insert into  @ScoreAdmTable
					SELECT 
						   KmTraveled = T.Item.value('@KmTraveled', 'float'),
						   OverSpeed = T.Item.value('@OverSpeed', 'float'),
						   OverSpeedAmount  = T.Item.value('@OverSpeedAmount',  'float'),
						   OverSpeedDistancePercentage = T.Item.value('@OverSpeedDistancePercentage', 'float'),
						   OverSpeedAverage = T.Item.value('@OverSpeedAverage', 'float'),
						   OverSpeedWeihgtedAverage = T.Item.value('@OverSpeedWeihgtedAverage', 'float'),
						   StandardDeviation = T.Item.value('@StandardDeviation', 'float'),
						   Variance = T.Item.value('@Variance', 'float'),
						   VariationCoefficient = T.Item.value('@VariationCoefficient', 'float'),
						   AverageScore = T.Item.value('@AverageScore', 'float'),
						   OverSpeedDistance= T.Item.value('@OverSpeedDistance', 'float')
					FROM   @pScoreAdmTableXML.nodes('Lista') AS T(Item)
					
					DECLARE @pScoreRouteTableXML XML;
					SET  @pScoreRouteTableXML = CAST(@ScoreRouteOutput  AS XML);
					
					DECLARE @ScoreRouteTable dbo.TypeDriverScore;
					
					Insert into  @ScoreRouteTable
					SELECT --DeviceSpeedId   = T.Item.value('@DeviceSpeedId ', 'int'),
						   KmTraveled = T.Item.value('@KmTraveled', 'float'),
						   OverSpeed = T.Item.value('@OverSpeed', 'float'),
						   OverSpeedAmount  = T.Item.value('@OverSpeedAmount',  'float'),
						   OverSpeedDistancePercentage = T.Item.value('@OverSpeedDistancePercentage', 'float'),
						   OverSpeedAverage = T.Item.value('@OverSpeedAverage', 'float'),
						   OverSpeedWeihgtedAverage = T.Item.value('@OverSpeedWeihgtedAverage', 'float'),
						   StandardDeviation = T.Item.value('@StandardDeviation', 'float'),
						   Variance = T.Item.value('@Variance', 'float'),
						   VariationCoefficient = T.Item.value('@VariationCoefficient', 'float'),
						   AverageScore = T.Item.value('@AverageScore', 'float'),
						   OverSpeedDistance= T.Item.value('@OverSpeedDistance', 'float')
					FROM   @pScoreRouteTableXML.nodes('Lista') AS T(Item)
					
					set @ScoreAdm=(select AverageScore from @ScoreAdmTable)
					set @ScoreRoute=(select AverageScore from @ScoreRouteTable)
					delete from @ScoreAdmTable
					delete from @ScoreRouteTable
					
					If (@ScoreAdm is not null or @ScoreRoute is not null)
					begin
					
						--Obtener nombre e identificación del usuario
						Select @Name = U.Name,
							   @Identification = D.Identification 
						  from General.Users U
						  inner join General.DriversUsers D on U.UserId = D.UserId
						 where U.UserId = @UserId
						
						If  @ScoreAdm is not null
							Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score,[Date],VehicleId,PlateId)
							Values(@UserId, @Name, @Identification, 'SPADM', @ScoreAdm,@date_day,@VehicleId,@PlateId)

						If  @ScoreRoute is not null
							Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score,[Date],VehicleId,PlateId)
							Values(@UserId, @Name, @Identification, 'SPROU', @ScoreRoute,@date_day,@VehicleId,@PlateId)
					end
					--Limpia la  tabla para  la nueva seleccion.
					DELETE FROM @VehiclesByUser
				END
				--- Siguiente********************
				Select @UserId = Min(U.UserId) 
				  from General.Users U
				  inner join General.DriversUsers D on U.UserId = D.UserId
				 Where U.IsActive =  1 
				   and U.UserId > @UserId
				   AND D.CustomerId=@pCustomerId
			END --Fin del While
			   
			Select @date_day = Min(date_day) 
			from @pDaysTable where date_day>@date_day
			
			set @next_date_day=DATEADD(DAY,1,@date_day)
		END-- While end
		
		--calificación global
		If @CountWeight > 0 
		begin
			Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score, Photo,[Date],VehicleId,PlateId)
			Select DS.UserId, DS.Name, DS.Identification, 'TOTAL', SUM(DS.Score * WS.Weight / 100), U.Photo,DS.[Date],DS.VehicleId,DS.PlateId
			  from #LocalDriverScore DS
			 inner join Operation.ScoreTypes ST on DS.ScoreType = ST.Code 
			 inner join Operation.WeightedScoreSettings WS on ST.ScoreTypeId = WS.ScoreTypeId
			 inner join General.Users U on DS.UserId = U.UserId
			 where WS.CustomerId = @pCustomerId
			group by DS.UserId, DS.Name, DS.Identification, U.Photo	,DS.[Date],DS.VehicleId,DS.PlateId	
			  	 	  
		end
		else
		begin
			--No existe configuración de calificación por peso
			--Se asigna la calificación de carretera como la calificación global
			Insert into #LocalDriverScore (UserID, Name, Identification , ScoreType, Score, Photo,[Date],VehicleId,PlateId)
			Select DS.UserId, DS.Name   , DS.Identification, 'TOTAL', DS.Score, U.Photo,DS.[Date],DS.VehicleId,DS.PlateId
			  from #LocalDriverScore DS
			 inner join General.Users U on DS.UserId = U.UserId
			 where DS.ScoreType = 'SPROU'
		end

		If @pReportType = 'D'
		begin
			Select DS.DriverScoreId ,DS.UserId,Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo,DS.[Date],DS.VehicleId,@pCustomerId as CostumerId,DS.PlateId  from #LocalDriverScore DS
			order by DS.[Date],DS.ScoreType, DS.Score  asc
		end
		else
		begin
			Select DS.DriverScoreId ,DS.UserId,DS.Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo , IsNull(DSA.Score, 0) ScoreSpeedAdmin, IsNull(DSR.Score, 0) ScoreSpeedRoute,DS.[Date],DS.VehicleId,@pCustomerId as CustomerId,DS.PlateId
			  from #LocalDriverScore DS
			 left join #LocalDriverScore DSA on DS.UserId = DSA.UserId and DSA.ScoreType = 'SPADM'
			 left join #LocalDriverScore DSR on DS.UserId = DSR.UserId and DSR.ScoreType = 'SPROU'
			 where DS.ScoreType = 'TOTAL'
			order by DS.[Date],DS.ScoreType, DS.Score asc
		end	
		
		Drop table #LocalDriverScore
	end
END
