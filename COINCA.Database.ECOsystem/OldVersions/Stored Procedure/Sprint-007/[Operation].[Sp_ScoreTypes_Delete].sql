USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreTypes_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_ScoreTypes_Delete]
GO

USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Operation].[Sp_ScoreTypes_Delete]    Script Date: 12/01/2014 13:57:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/01/2014
-- Description:	Delete Score Types information
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_ScoreTypes_Delete]
(
	 @pScoreTypeId INT				--@pScoreTypeId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Operation].[ScoreTypes]
			WHERE [ScoreTypeId] = @pScoreTypeId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
