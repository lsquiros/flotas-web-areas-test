USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleGroups_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleGroups_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleGroup information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleGroups_Retrieve]
(
	 @pVehicleGroupId INT = NULL				--@pVehicleGroupId: PK of the table
	,@pCustomerId INT							--@pCustomerId: FK of Customer
	,@pKey VARCHAR(800) = NULL					--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT
		 a.[VehicleGroupId]
		,a.[Name]
		,a.[Description]
		,a.[RowVersion]
		,c.[Code] CountryCode
    FROM [General].[VehicleGroups] a
		INNER JOIN [General].[Customers] b ON a.[CustomerId] = b.[CustomerId]
		INNER JOIN [General].[Countries] c ON b.[CountryId] = c.[CountryId] 
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pVehicleGroupId IS NULL OR [VehicleGroupId] = @pVehicleGroupId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR a.[Description] like '%'+@pKey+'%')
	ORDER BY [VehicleGroupId] DESC
	
    SET NOCOUNT OFF
END
