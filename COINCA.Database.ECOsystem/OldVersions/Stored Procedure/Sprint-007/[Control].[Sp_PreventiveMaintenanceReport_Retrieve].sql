USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsReport_Retrieve]    Script Date: 12/10/2014 10:44:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PreventiveMaintenanceReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_PreventiveMaintenanceReport_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_PreventiveMaintenanceReport_Retrieve]    Script Date: 12/10/2014 10:44:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Napole�n Alvarado
-- Create date: 12-11-2014
-- Description:	Retrieve Preventive Maintenance Report information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PreventiveMaintenanceReport_Retrieve]
(
	@pCustomerId INT					--@pCustomerId: CustomerId
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		 a.[FrequencyTypeId]
		,c.[Name] AS VehicleName
	    ,c.[PlateId]
	    ,a.[Description] AS MaintenanceDescription
	    ,d.[Name] AS FrequencyName
	    ,a.[Frequency]
	    ,e.[Name] AS PeriodicityName
	    ,b.[NextReview] 
	    ,b.[Day]
	    ,b.[Month]
	    ,b.[Year]
	    ,f.[Value]
	FROM 
		[General].[PreventiveMaintenanceCatalog] a
		INNER JOIN [General].[PreventiveMaintenanceByVehicle] b 
			ON a.[PreventiveMaintenanceCatalogId] =b.[PreventiveMaintenanceCatalogId]
		INNER JOIN [General].[Vehicles] c 
			ON b.[VehicleId] = c.[VehicleId]
		INNER JOIN [General].[Types] d 
			ON a.[FrequencyTypeId] = d.[TypeId]
		LEFT JOIN [General].[Types] e
			ON a.[PeriodicityTypeId] = e.[TypeId]
		LEFT JOIN [General].[Values] f
			ON e.[TypeId] = f.[TypeId]
	WHERE b.[CustomerId] = @pCustomerId 
	  AND c.[Active] = 1
	ORDER BY a.[Description]
	
    SET NOCOUNT OFF
END


GO

