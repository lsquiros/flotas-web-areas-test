USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_PreventiveMaintenanceByVehicleReport_Retrieve]    Script Date: 03/04/2015 13:36:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PreventiveMaintenanceByVehicleReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_PreventiveMaintenanceByVehicleReport_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_PreventiveMaintenanceByVehicleReport_Retrieve]    Script Date: 03/04/2015 13:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo Gonz�lez
-- Create date: 27/02/2015
-- Description:	Retrieve Preventive Maintenance By Vehicle Report
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PreventiveMaintenanceByVehicleReport_Retrieve]
(
	 @pCustomerId INT,
	 @pMaintenanceCatalogId INT = NULL,
	 @pStatus INT = NULL,
	 @pDate DATE = NULL,
	 @pOdometer FLOAT = NULL,
	 @pStartDate DATE = NULL,
	 @pEndDate DATE = NULL,
	 @pCostCenter INT = NULL,
	 @pKey VARCHAR(MAX) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	
		SELECT 
			b.[PreventiveMaintenanceId],
			a.[PreventiveMaintenanceCatalogId], 
			a.[Description] [MaintenanceName],
			b.[VehicleId],
			c.[PlateId],
			c.[Name] [VehicleName],
			e.[Odometer] [ActualOdometer],
			b.[LastReviewOdometer] [LastReviewOdometer],
			b.[NextReviewOdometer] [NextReviewOdometer],
			b.[LastReviewDate] [LastReviewDate],
			b.[NextReviewDate] [NextReviewDate],
			CASE WHEN a.[FrequencyKm] IS NULL THEN '' ELSE '-' + CONVERT(varchar(50), a.[FrequencyKm]) + ' Km ' END + 
			CASE WHEN a.[FrequencyMonth] IS NULL THEN '' ELSE '-' + CONVERT(varchar(50), a.[FrequencyMonth]) + ' Meses ' END +
			CASE WHEN a.[FrequencyDate] IS NULL THEN '' ELSE ' -Anual ' END [Frequency], 
			d.[Date],
			d.[Odometer],
			a.[Cost], 
			d.[Record],
			b.[Registred],
			g.[Symbol] [CurrencySymbol]
		FROM 
			[General].[PreventiveMaintenanceCatalog] a
			INNER JOIN [General].[PreventiveMaintenanceByVehicle] b 
				ON a.[PreventiveMaintenanceCatalogId] = b.[PreventiveMaintenanceCatalogId]
			INNER JOIN [General].[Vehicles] c 
				ON b.[VehicleId] = c.[VehicleId]
			LEFT JOIN [General].[PreventiveMaintenanceRecordByVehicle] d 
				ON d.[PreventiveMaintenanceId] = b.[PreventiveMaintenanceId]
			LEFT JOIN (SELECT 
						a.[VehicleId], 
						e.[Odometer]
					FROM 
						[General].[Vehicles] a 
						INNER JOIN [IntrackV2]..[Composiciones] b ON a.[IntrackReference] = b.[vehiculo]
						INNER JOIN [IntrackV2]..[DispositivosAVL] c ON b.[dispositivo] = c.[dispositivo]
						INNER JOIN [172.22.220.38\BDS_WEB].[Atrack].[dbo].[Devices] d ON c.[numeroimei] = d.[UnitID]
						INNER JOIN [172.22.220.38\BDS_WEB].[Atrack].[dbo].[Report_Last] e ON d.[Device] = e.[Device]
					WHERE 
						a.[CustomerId] = 23 AND 
						a.[IntrackReference] IS NOT NULL) e
				ON e.[VehicleId] = b.[VehicleId]
			INNER JOIN [General].[Customers] ct 
					ON c.[CustomerId] = ct.[CustomerId]
			INNER JOIN [Control].[Currencies] g 
					ON g.[CurrencyId] = ct.CurrencyId
		WHERE 
			(@pCostCenter IS NULL OR 
			c.CostCenterId = @pCostCenter) AND 
			(@pMaintenanceCatalogId IS NULL 
				OR a.PreventiveMaintenanceCatalogId = @pMaintenanceCatalogId) AND 
			(@pKey IS NULL 
				OR c.[Name] like '%'+@pKey+'%'
                OR c.[PlateId] like '%'+@pKey+'%') AND 

			b.Registred = CASE WHEN @pStatus = 3 THEN 1 ELSE 0 END AND  
			(
				@pStatus <> 2 OR 
				(
					@pStatus = 2 AND 
					(
						(@pOdometer IS NULL OR 
						b.[NextReviewOdometer] < e.[Odometer] + @pOdometer)
						AND 
						(@pDate IS NULL OR 
						b.[NextReviewDate] < @pDate)
					)
				)
			)
			 AND 
			(
				@pStartDate IS NULL OR 
				d.[Date] >= @pStartDate
			)AND
			(
				@pEndDate IS NULL OR 
				d.[Date] <= @pEndDate 
			)AND
			b.[IsDeleted] = 0 AND 
			a.[CustomerId] = @pCustomerId

    SET NOCOUNT OFF
END

GO


