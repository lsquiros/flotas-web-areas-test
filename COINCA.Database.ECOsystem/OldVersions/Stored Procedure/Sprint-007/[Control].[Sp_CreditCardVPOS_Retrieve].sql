USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardVPOS_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardVPOS_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 04/12/2014
-- Description:	Retrieve CreditCard information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardVPOS_Retrieve]
(
	@pKey VARCHAR(800) = NULL				--@pKey: Key to perform search operations
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		 a.[CreditCardId]
		,a.[CustomerId]
		,a.[CreditCardNumber]
		,a.[ExpirationYear]
		,a.[ExpirationMonth]
		,a.[CreditLimit]
		,a.[CreditAvailable]
		,a.[CreditExtra]
		,a.[StatusId]
		,b.[RowOrder] AS [Step]
		,b.[Name] AS [StatusName]
		,d.[Symbol] AS [CurrencySymbol]
		,c.[IssueForId]
		,CONVERT(BIT,CASE WHEN a.[StatusId] IN (4,5) THEN 1 ELSE 0 END) AS IsReadyForCustomer
		,CONVERT(BIT,CASE WHEN a.[StatusId] IN (1,2,3) THEN 1 ELSE 0 END) AS IsReadyForCoinca
		,e.[UserId]
		,g.[VehicleId]
		,c.[CreditCardType]		
		,f.Name AS [EncryptedDriverName]
		,'Veh�culo: ' + h.[PlateId] AS [VehiclePlate]
		,a.[RowVersion]
		,c.[Name] AS [EncryptedCustomerName]
    FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [Control].[CreditCardByVehicle] g
			ON g.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] = g.[VehicleId]
	WHERE (@pKey IS NULL 
				OR h.[PlateId] like '%'+@pKey+'%'
				OR f.[Name] like '%'+@pKey+'%'
				OR c.[Name]  like '%'+@pKey+'%')
	ORDER BY [CreditCardId] DESC
	
    SET NOCOUNT OFF
END
