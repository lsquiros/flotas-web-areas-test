USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerUsersByCountry_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PartnerUsersByCountry_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Retrieve Partner Users By Country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PartnerUsersByCountry_Retrieve]
(
	@pPartnerUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT *
	FROM
		(SELECT
			   a.[PartnerUsersByCountryId]
			  ,a.[PartnerUserId]
			  ,a.[CountryId]
			  ,b.[Name] AS [CountryName]
			  ,b.[Code] AS [CountryCode]
			  ,CONVERT(BIT,1) AS IsCountryChecked
		FROM [General].[PartnerUsersByCountry] a
			INNER JOIN [General].[Countries] b
				ON b.[CountryId] = a.[CountryId]		
		WHERE a.[PartnerUserId] = @pPartnerUserId
		UNION
		SELECT	 NULL AS [PartnerUsersByCountryId]
				,@pPartnerUserId AS [PartnerUserId]
				,a.[CountryId]
				,a.[Name] AS [CountryName]
				,a.[Code] AS [CountryCode]
				,CONVERT(BIT,0) AS IsCountryChecked
			FROM [General].[Countries] a
		WHERE NOT EXISTS (SELECT 1
								FROM [General].[PartnerUsersByCountry] x
							WHERE x.[PartnerUserId] = @pPartnerUserId
							  AND x.[CountryId] = a.[CountryId]))t
	ORDER BY t.[CountryName]
	
    SET NOCOUNT OFF
END
