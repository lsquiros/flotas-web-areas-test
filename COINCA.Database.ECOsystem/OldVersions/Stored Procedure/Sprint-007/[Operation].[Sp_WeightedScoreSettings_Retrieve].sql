USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_WeightedScoreSettings_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_WeightedScoreSettings_Retrieve]
GO

USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Operation].[Sp_WeightedScoreSettings_Retrieve]    Script Date: 12/01/2014 10:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/01/2014
-- Description:	Retrieve Weighted Score Settings information
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_WeightedScoreSettings_Retrieve]
(
	 @pWeightedScoreId INT = NULL,			--@pWeightedScoreId: PK of the table
	 @pCustomerId INT,						--@pCustomerId: FK of Customer
	 @pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[WeightedScoreId]
		,a.[CustomerId]
		,a.[ScoreTypeId] 
		,b.[Code] ScoreTypeCode
		,b.[Name] ScoreTypeName
		,a.[Weight]
		,a.[RowVersion]
    FROM [Operation].[WeightedScoreSettings] a
		INNER JOIN [Operation].[ScoreTypes] b
			ON a.[ScoreTypeId] = b.[ScoreTypeId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pWeightedScoreId IS NULL OR a.[WeightedScoreId] = @pWeightedScoreId)	  
	  AND (@pKey IS NULL 
				OR b.[Code] like '%'+@pKey+'%'
				OR b.[Name] like '%'+@pKey+'%')
	ORDER BY b.[Code] ASC
	
    SET NOCOUNT OFF
END
