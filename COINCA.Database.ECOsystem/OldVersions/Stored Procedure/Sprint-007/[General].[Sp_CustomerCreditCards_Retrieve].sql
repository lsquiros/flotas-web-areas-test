USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerCreditCards_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerCreditCards_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/15/2014
-- Description:	Retrieve Customer CreditCards information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerCreditCards_Retrieve]
(
	  @pCustomerId INT
	 ,@pCustomerCreditCardId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT	 a.[CustomerCreditCardId]
			,a.[CustomerId]
			,a.[CreditCardNumber]
			,a.[ExpirationYear]
			,a.[ExpirationMonth]
			,a.[CreditLimit]
			,a.[StatusId]
			,b.[Name] AS [StatusName]
			,a.[CreditCardHolder]
			,d.[Symbol] AS [CurrencySymbol]
			,d.[Name] AS [CurrencyName]
			,a.[RowVersion]
	FROM [General].[CustomerCreditCards] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pCustomerCreditCardId IS NULL OR a.[CustomerCreditCardId] = @pCustomerCreditCardId)
	ORDER BY [CustomerId] DESC
	
    SET NOCOUNT OFF
END
