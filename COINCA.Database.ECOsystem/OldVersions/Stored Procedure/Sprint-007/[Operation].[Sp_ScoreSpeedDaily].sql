USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreSpeedDaily]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_ScoreSpeedDaily]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Created by:		Gerald Solano C.
-- Created date:	08/06/2015 
-- Description:	Retrieve Score Driving Daily

CREATE PROCEDURE [Operation].[Sp_ScoreSpeedDaily] 
( @pCustomerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pScoreType char(1)			--A- Reporte Administrativo  , R- Reporte de  carretera (Route)
)
AS
BEGIN

	--TABLA TEMP PARA VEHICULOS ASIGNADOS A UN USUARIO
	Declare @VehiclesByUser table
	(Device int,
	 Vehicle int,
	 StartOdometer int,
	 EndOdometer int,
	 StartDate datetime,
	 EndDate datetime)

	--TABLA TEMP PARA LAS CALIFICACIONES PRECALCULADAS
	Create table #LocalDriverScore
	(DriverScoreId INT Identity(1,1),
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 Score Float,
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float,
	 Date_Time datetime,
	 MaxOdometer int,
	 MinOdometer int,
	 Vec float,
	 Dt float,
	 De float,
	 Ce float)
	 
	Declare @UserId Int
	Declare @Name varchar(250),
			@Identification varchar(50)
	Declare  @Score Float

	-- OBTENEMOS EL INDICE MINIMO PARA RECORRER LOS USUARIOS DE UN CUSTOMER	
	Select @UserId = Min(U.UserId) 
	  from General.Users U
	  inner join General.DriversUsers D on U.UserId = D.UserId
	 Where 
	   U.IsActive =  1 
	   and D.CustomerId = @pCustomerId
	-- ////////////////////////////////////////////////////////
	
	-- RECORRIDO DE USUARIOS 
	While  @UserId  is not null
	BEGIN

		-- OBTEMOS LOS VEHICULOS ASIGNADOS AL USUARIO
		Insert into @VehiclesByUser
			(Device, Vehicle, StartOdometer, EndOdometer, StartDate, EndDate)  
		--Select   V.IntrackReference As Device,CASE WHEN  VU.InitialOdometer IS NULL THEN 0 eLSE VU.InitialOdometer end  ,CASE WHEN VU.EndOdometer IS NULL THEN 0 eLSE EndOdometer end    , VU.InsertDate StartDate ,VU.LastDateDriving EndDate
		Select V.DeviceReference, V.IntrackReference, IsNull(VU.InitialOdometer, 0), IsNull(VU.EndOdometer,0), VU.InsertDate, IsNull(VU.LastDateDriving, GETDATE())
		from General.VehiclesByUser VU 
		inner Join General.Vehicles V ON  V.VehicleId = VU.VehicleId
		inner JOIN General.Users U  ON U.UserId = VU.UserId
		Where U.UserId =  @UserId 
		  and V.CustomerId = @pCustomerId
		  And V.DeviceReference is not null
		  and ((VU.LastDateDriving is null) or (@pEndDate<=ISNULL(VU.LastDateDriving,@pEndDate) and @pStartDate>=ISNULL(VU.InsertDate,@pStartDate)))
		order by V.DeviceReference, VU.InsertDate, VU.LastDateDriving

		--SELECT * from @VehiclesByUser

		if (SELECT COUNT(*) FROM @VehiclesByUser ) > 0 
		BEGIN 
			
			-- CONVERTIMOS LA LISTA DE VEHICULOS ASIGNADOS A UN SOLO REGISTRO DE XML
			DECLARE @VehiclesByUserString varchar(max);
			SET @VehiclesByUserString = (SElect * FROm  @VehiclesByUser AS Lista FOR XML AUTO)
			--SELECT  @VehiclesByUserString   

			DECLARE @ScoreOutput nvarchar(max)
			DECLARE @SQL NVARCHAR(max)
				
			--SET @SQL=(SELECT ScoreDrivingFormulaSP FROM Insurance.ScoreDrivingFormula WHERE CustomerId=@pCustomerId AND IsActive=1)
			--IF @SQL is null
			--BEGIN
				--set @SQL='[dbo].[Sp_GetScoreDriver]';
			--END

			-- HACEMOS USO DEL SP CON LA LOGICA DE LA FORMULA PARA LA CALIFICACIÓN
			SET @SQL='[dbo].[Sp_GetScoreDriverDaily]';  
			
			-- EJECUTAMOS EL SP PARA LA CALIFICACIÓN DENTRO DEL RANGO DE FECHAS Y EN BASE AL USO DE LOS VEHICULOS ASIGNADOS
			EXECUTE  @SQL @pStartDate, @pEndDate, @VehiclesByUserString, @pScoreType, @pScoreTable = @ScoreOutput OUTPUT
			
			-- OBTENEMOS LA CALIFICACIÓN POR DÍA Y LO CONVERTIMOS A UN SOLO REGISTRO DE XML
			DECLARE @pScoreOutputXML XML;
			SET  @pScoreOutputXML = CAST(@ScoreOutput  AS XML);
			
			-- DECLARAMOS UN TIPO DE DATO DE BD PARA ASIGNAR LOS RESULTADOS
			DECLARE @ScoreTable dbo.TypeDriverScoreDaily;
			
			--- INSERTAMOS LOS RESULTADOS EN LA VARIABLE @ScoreTable DE TIPO DE DATO dbo.TypeDriverScoreDaily
			Insert into  @ScoreTable
			SELECT --DeviceSpeedId   = T.Item.value('@DeviceSpeedId ', 'int'),
					--IdDailyScore = IDENTITY(INT,1,1),
					KmTraveled = T.Item.value('@KmTraveled', 'float'),
					OverSpeed = T.Item.value('@OverSpeed', 'float'),
					OverSpeedAmount  = T.Item.value('@OverSpeedAmount',  'float'),
					OverSpeedDistancePercentage = T.Item.value('@OverSpeedDistancePercentage', 'float'),
					OverSpeedAverage = T.Item.value('@OverSpeedAverage', 'float'),
					OverSpeedWeihgtedAverage = T.Item.value('@OverSpeedWeihgtedAverage', 'float'),
					StandardDeviation = T.Item.value('@StandardDeviation', 'float'),
					Variance = T.Item.value('@Variance', 'float'),
					VariationCoefficient = T.Item.value('@VariationCoefficient', 'float'),
					AverageScore = T.Item.value('@AverageScore', 'float'),
					OverSpeedDistance= T.Item.value('@OverSpeedDistance', 'float'),
					Date_Time= T.Item.value('@Date_Time', 'datetime'),
					MaxOdometer= T.Item.value('@MaxOdometer', 'int'),
					MinOdometer= T.Item.value('@MinOdometer', 'int'),
					Vec= T.Item.value('@Vec', 'float'),
					Dt= T.Item.value('@Dt', 'float'),
					De= T.Item.value('@De', 'float'),
					Ce= T.Item.value('@Ce', 'float')
			FROM   @pScoreOutputXML.nodes('Lista') AS T(Item)
				
			--SELECT @VehiclesByUserString
			--SELECT * FROM @ScoreTable

			--RECORREMOS LOS RESULTADOS DEL XML DEVUELTO POR EL Sp_GetScoreDriverDaily
			DECLARE @minIndice int = NULL 
			DECLARE @maxIndice int = NULL

			-- OBTENEMOS LOS INDICES MINIMO Y MAXIMO PARA HACER EL RECORRIDO
			SELECT @minIndice = MIN(IdDailyScore), @maxIndice = MAX(IdDailyScore) FROM @ScoreTable
			
			-- INICIAMOS EL RECORRIDO				
			WHILE @minIndice <= @maxIndice
			BEGIN
				--SELECT @minIndice

				-- OBTENEMOS EL SCORE DEL INDICE ACTUAL RECORRIDO
				set @Score=(select AverageScore from @ScoreTable WHERE IdDailyScore = @minIndice)
				--Select @Score
								
				If @Score is not null
				BEGIN
					--Obtener nombre e identificación del usuario
					Select @Name = U.Name,
						   @Identification = D.Identification 
					  from General.Users U
					  inner join General.DriversUsers D on U.UserId = D.UserId
					where U.UserId = @UserId
				 
					-- INSERTAMOS LOS DATOS EN LA TABLA TEMPORAL PARA DEVOLVER COMO RESULTADO FINAL
					Insert into #LocalDriverScore (UserID, Name, Identification, Score,
								KmTraveled,
								OverSpeed,
								OverSpeedAmount,
								OverSpeedDistancePercentage ,
								OverSpeedAverage ,
								OverSpeedWeihgtedAverage ,
								StandardDeviation ,
								Variance ,
								VariationCoefficient,
								OverSpeedDistance,
								Date_Time,
								MaxOdometer,
								MinOdometer,
								Vec,
								Dt,
								De,
								Ce)
					Values(@UserId, @Name, @Identification, @Score,
							(select KmTraveled from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select OverSpeed from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select OverSpeedAmount from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select OverSpeedDistancePercentage from @ScoreTable  WHERE IdDailyScore = @minIndice),
							(select OverSpeedAverage from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select OverSpeedWeihgtedAverage from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select StandardDeviation from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select Variance from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select VariationCoefficient from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select OverSpeedDistance from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select Date_Time from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select MaxOdometer from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select MinOdometer from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select Vec from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select Dt from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select De from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select Ce from @ScoreTable WHERE IdDailyScore = @minIndice))
				
					--Select * FROM #LocalDriverScore
				END


				-- Siguiente Indice menor y mayor al procesado actualmente para seguir con el recorrido
				SELECT @minIndice = MIN(IdDailyScore) 
				FROM @ScoreTable
				WHERE IdDailyScore > @minIndice

				--/////////////////////////////////////////////////////////////////////////////

			END -- FIN DEL WHILE
			
			----Limpia la  tabla para  la nueva seleccion.
			delete from @ScoreTable
			DELETE FROM @VehiclesByUser
			-- /////////////////////////////////////////
		END

		--- Siguiente Indice del Usuario menor y mayor al procesado actualmente para seguir con el recorrido
		Select @UserId = Min(U.UserId) 
		  from General.Users U
		  inner join General.DriversUsers D on U.UserId = D.UserId
		 Where U.IsActive =  1 
		   and D.CustomerId = @pCustomerId
		   and U.UserId > @UserId

	END --FIN RECORRIDO DE USUARIOS
	
	--RETURN SCORES AND DATA FOR REGISTER EN DATA BASE
	Select DriverScoreId, 
			UserId, 
			Name, 
			Identification, 
			Score,KmTraveled,
			OverSpeed ,
			OverSpeedAmount,
			OverSpeedDistancePercentage,
			OverSpeedAverage,
			OverSpeedWeihgtedAverage,
			StandardDeviation,
			Variance,
			VariationCoefficient,
			OverSpeedDistance,
		    Date_Time,
			MaxOdometer,
			MinOdometer,
			Vec,
			Dt,
			De,
			Ce
	FROM #LocalDriverScore
	ORDER BY Score ASC
	
	Drop table #LocalDriverScore -- BORRAMOS TABLA TEMPORAL
	
END


GO


