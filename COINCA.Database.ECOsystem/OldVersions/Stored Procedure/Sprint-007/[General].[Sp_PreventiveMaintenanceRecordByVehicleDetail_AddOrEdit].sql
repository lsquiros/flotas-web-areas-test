USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/16/2015
-- Description:	Insert or Update Preventive Maintenance Record By Vehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_AddOrEdit]
(
	@pXmlData VARCHAR(MAX),	
	--@pPreventiveMaintenanceRecordByVehicleId INT,
	@pPreventiveMaintenanceId INT,
	@pDate DATE,
	@pOdometer FLOAT,
	@pApply INT,
	@pLoggedUserId INT						
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lVehicleId INT
			DECLARE @lPreventiveMaintenanceCatalogId INT
			DECLARE @lPreventiveMaintenanceRecordByVehicleId INT
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0

            DECLARE @lTotalCost DECIMAL(16, 2)
            DECLARE @lTotalRecord DECIMAL(16, 2)
            DECLARE @lCatalogId INT
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			SELECT @lPreventiveMaintenanceRecordByVehicleId = COUNT([PreventiveMaintenanceRecordByVehicleId]) FROM [General].[PreventiveMaintenanceRecordByVehicle] WHERE [PreventiveMaintenanceId] = @pPreventiveMaintenanceId
			IF (@lPreventiveMaintenanceRecordByVehicleId = 0)
			BEGIN
				INSERT INTO [General].[PreventiveMaintenanceRecordByVehicle]
					([PreventiveMaintenanceId],
					[Date],
					[Odometer],
					[Record],
					[Cost],
					[InsertUserId],
					[InsertDateId])
				VALUES
					(@pPreventiveMaintenanceId,
					@pDate,
					@pOdometer,
					0,
					0,
					@pLoggedUserId,
					GETUTCDATE())
			END

			SELECT @lPreventiveMaintenanceRecordByVehicleId = [PreventiveMaintenanceRecordByVehicleId] 
			FROM [General].[PreventiveMaintenanceRecordByVehicle]
			WHERE [PreventiveMaintenanceId] = @pPreventiveMaintenanceId
			
			INSERT INTO [General].[PreventiveMaintenanceRecordByVehicleDetail]
					([PreventiveMaintenanceRecordByVehicleId]
					,[PreventiveMaintenanceCostId]
					,[Description]
					,[Cost]
					,[Record]
					,[InsertUserId]
					,[InsertDateId])
			SELECT 
					DISTINCT
					@lPreventiveMaintenanceRecordByVehicleId AS [PreventiveMaintenanceCatalogId]
					,Row.col.value('./@PreventiveMaintenanceCostId', 'INT') AS [PreventiveMaintenanceCatalogId]
					,Row.col.value('./@Description', 'VARCHAR(500)') AS [Description]
					,Row.col.value('./@Cost', 'DECIMAL(16,2)') AS [Cost]
					,Row.col.value('./@Record', 'DECIMAL(16,2)') AS [Cost]
					,@pLoggedUserId
					,GETUTCDATE()
			FROM @lxmlData.nodes('/xmldata/PreventiveMaintenanceRecordByVehicleCostDetail') Row(col)
			WHERE Row.col.value('./@PreventiveMaintenanceRecordByVehicleDetailId', 'INT') = 0
			
			UPDATE record SET 
					record.Description = Row.col.value('./@Description', 'VARCHAR(500)'),
					record.Cost = Row.col.value('./@Cost', 'DECIMAL(16,2)'),
					record.Record = Row.col.value('./@Record', 'DECIMAL(16,2)'),
					record.ModifyUserId = @pLoggedUserId,
					record.ModifyDateId = GETUTCDATE()
			FROM @lxmlData.nodes('/xmldata/PreventiveMaintenanceRecordByVehicleCostDetail') Row(col)
			INNER JOIN [General].[PreventiveMaintenanceRecordByVehicleDetail] record ON Row.col.value('./@PreventiveMaintenanceRecordByVehicleDetailId', 'INT') = record.PreventiveMaintenanceRecordByVehicleDetailId
			WHERE Row.col.value('./@PreventiveMaintenanceRecordByVehicleDetailId', 'INT') <> 0

			SELECT @lTotalCost = ISNULL(SUM(Cost), 0) FROM [General].[PreventiveMaintenanceRecordByVehicleDetail] 
			WHERE [PreventiveMaintenanceRecordByVehicleId] = @lPreventiveMaintenanceRecordByVehicleId
			
			SELECT @lTotalRecord = ISNULL(SUM(Record), 0) FROM [General].[PreventiveMaintenanceRecordByVehicleDetail] 
			WHERE [PreventiveMaintenanceRecordByVehicleId] = @lPreventiveMaintenanceRecordByVehicleId
			
			UPDATE [General].[PreventiveMaintenanceRecordByVehicle]
			SET 
				[Cost] = @lTotalCost, 
				[Record] = @lTotalRecord, 
				[ModifyUserId] = @pLoggedUserId,
				[ModifyDateId] = GETUTCDATE()
			WHERE 
				[PreventiveMaintenanceId] = @pPreventiveMaintenanceId

			IF (@pApply = 1)
			BEGIN
				--SE OBTIENE EL VEHICULO Y EL CATALOGO
				SELECT @lVehicleId = [VehicleId], @lPreventiveMaintenanceCatalogId = [PreventiveMaintenanceCatalogId]
				FROM [General].[PreventiveMaintenanceByVehicle] 
				WHERE [PreventiveMaintenanceId] = @pPreventiveMaintenanceId 
				
				--SE DESACTIVA EL MANTENIMIENTO PREVENTIVO POR VEHICULO
				UPDATE [General].[PreventiveMaintenanceByVehicle] 
				SET [Registred] = 1 
				WHERE [PreventiveMaintenanceId] = @pPreventiveMaintenanceId
				
				--SE LLAMA EL SP QUE VUELVE A INSERTAR EL NUEVO MANTENIMIENTO PREVENTIVO POR VEHICULO
				EXEC [General].[Sp_PreventiveMaintenanceByVehicle_AddOrEdit] 
					@lVehicleId,
					@lPreventiveMaintenanceCatalogId,
					@pOdometer,
					@pDate,
					@pLoggedUserId,
					NULL

			END

            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO