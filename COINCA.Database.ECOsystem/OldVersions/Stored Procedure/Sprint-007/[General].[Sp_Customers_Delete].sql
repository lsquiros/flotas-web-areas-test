USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Customers_Delete]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andres Oviedo Brenes.
-- Create date: 26/01/2014
-- Description:	Delete Customer information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Customers_Delete]
(
	 @pCustomerId INT,
	 @pPartnerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			
			DELETE FROM General.CustomersByPartner
							WHERE [CustomerId] = @pCustomerId AND PartnerId=@pPartnerId
							
			
			DECLARE @CustomerWithPartnerCount INT = 0
			
			SELECT @CustomerWithPartnerCount = COUNT(*) 
					FROM General.CustomersByPartner cp
					WHERE cp.CustomerId = @pCustomerId AND cp.IsDefault = 1
			
			IF(@CustomerWithPartnerCount = 0) BEGIN --SI LO ELIMIN� EL PARTNER QUE LO CRE� ACTUALIZAR LA BANDERA IS DELETED
				
				UPDATE General.Customers SET IsDeleted = 1 
				WHERE CustomerId = @pCustomerId 
				
			END
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO