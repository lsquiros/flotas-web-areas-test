USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerUsers_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PartnerUsers_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Add Or Edit Partner User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PartnerUsers_AddOrEdit]
(
	 @pPartnerUserId INT = NULL
	,@pUserId INT
	,@pPartnerId INT
	,@pCountriesAccessXml VARCHAR(MAX)
	,@pLoggedUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lxmlData XML = CONVERT(XML,@pCountriesAccessXml)
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		IF (@pPartnerUserId IS NULL)
		BEGIN
			INSERT INTO [General].[PartnerUsers]
					([UserId]
					,[PartnerId]
					,[InsertDate]
					,[InsertUserId])
			VALUES	(@pUserId
					,@pPartnerId
					,GETUTCDATE()
					,@pLoggedUserId)
					
			SET @pPartnerUserId = SCOPE_IDENTITY()
		END
		ELSE
		BEGIN
			UPDATE [General].[PartnerUsers]
				SET  [ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
			WHERE [PartnerUserId] = @pPartnerUserId
			
		END
        
        DELETE
			FROM [General].[PartnerUsersByCountry]
        WHERE [PartnerUserId] = @pPartnerUserId
        
        INSERT INTO [General].[PartnerUsersByCountry]([PartnerUserId],[CountryId],[InsertDate],[InsertUserId])
        SELECT 
			 @pPartnerUserId
			,RowData.col.value('./@CountryId', 'INT') AS [CountryId]
			,GETUTCDATE()
			,@pLoggedUserId
		FROM @lxmlData.nodes('/xmldata/Country') RowData(col)
        
        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
