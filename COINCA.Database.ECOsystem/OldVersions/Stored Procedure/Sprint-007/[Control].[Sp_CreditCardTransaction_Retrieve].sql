USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardTransaction_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardTransaction_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 05/12/2014
-- Description:	Retrieve CreditCard Transactions through the VPOS Process
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardTransaction_Retrieve]
(
	 @pCreditCardId INT,						--@pCreditCardId: Customer Id
	 @pSystemTraceNumber NVARCHAR(50) = NULL				--@pSystemTraceNumber: System Trace Number
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
			
	SELECT TOP 1
			 [CreditCardTransactionVPOS] AS [CCTransactionVPOSId]	
	        ,[CreditCardId]
			,[Customer]
			,[TransactionType]
			,[TerminalID]
			,[Invoice]
			,[EntryMode]
			,[AccountNumber]
			,[ExpirationDate]
			,[TotalAmount]
			,[Odometer]
			,[Liters]
			,[Plate]
			,[AuthorizationNumber]
			,[ReferenceNumber]
			,[SystemTraceNumber]
	FROM [Control].[CreditCardTransactionVPOS]
	WHERE 
		(@pSystemTraceNumber IS NOT NULL OR [CreditCardId] = @pCreditCardId) AND
		(@pSystemTraceNumber IS NULL OR [SystemTraceNumber] = @pSystemTraceNumber)
	ORDER BY [InsertDate] DESC
				
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO



