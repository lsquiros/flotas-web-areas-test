USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Email_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Email_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Martínez H.
-- Create date: 18/Nov/2014
-- Description:	Retrieve Email information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Email_Retrieve]
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		 a.[EmailId]
		,a.[To]
		,a.[From]
		,a.[CC]
		,a.[Subject]
		,a.[Message]
		,a.[IsHtml]
		,a.[RowVersion]
	FROM [General].[Emails] a
	WHERE a.[Sent] = 0
	SET NOCOUNT OFF 
END