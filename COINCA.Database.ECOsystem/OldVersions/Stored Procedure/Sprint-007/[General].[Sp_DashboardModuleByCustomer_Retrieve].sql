USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DashboardModuleByCustomer_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DashboardModuleByCustomer_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 09/11/2014
-- Description:	Retrieve country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DashboardModuleByCustomer_Retrieve]
(
	 @pCustomerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT 
		M.DashboardModuleId [DashboardModuleId],
		M.Name [Name],
		@pCustomerId [CustomerId],
		ISNULL(D.[Order], 0) AS [Order],
		D.[RowVersion] [RowVersion]
	FROM 
		[General].[DashboardModules] M
			LEFT JOIN (SELECT 
							 D.DashboardModuleId
							,D.[Order]
							,D.[RowVersion]
							FROM [General].[DashboardModulesByCustomer] D
							WHERE D.[CustomerId] = @pCustomerId) D
		ON M.DashboardModuleId = D.DashboardModuleId
	ORDER BY [ORDER]
	
    SET NOCOUNT OFF
END


	