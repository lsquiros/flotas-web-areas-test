USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Currencies_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_Currencies_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve country information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Currencies_Retrieve]
(
	 @pCurrencyId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT
		 a.[CurrencyId]
		,a.[Name]
		,a.[Code]
		,a.[Symbol]
		,a.[RowVersion]
    FROM [Control].[Currencies] a
	WHERE (@pCurrencyId IS NULL OR [CurrencyId] = @pCurrencyId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR a.[Code] like '%'+@pKey+'%'
				OR a.[Symbol] like '%'+@pKey+'%')
	ORDER BY [CurrencyId] DESC
	
    SET NOCOUNT OFF
END

GO


