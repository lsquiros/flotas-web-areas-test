USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleByPlate_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleByPlate_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Mart�nez H.
-- Create date: 20/Oct/2014
-- Description:	Retrieve Vehicle By Plate information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleByPlate_Retrieve]
(
	@pPlateId VARCHAR(10) --@pPlateId: Plate of the vehicle
)
AS
BEGIN 
	SET NOCOUNT ON
		SELECT
			 a.[VehicleId]
			,a.[PlateId]
			,a.[Name]
			,a.[CostCenterId]
			,c.[Name] AS CostCenterName
			,a.[CustomerId]
			,f.[Name] AS UserName
			,e.[LicenseExpiration] AS DriverLicenseExpiration
			,e.[DailyTransactionLimit]
			,a.[VehicleCategoryId]
			,d.[Type] AS CategoryType
			,d.[Liters]
			,d.[DefaultFuelId]
			,b.[Name] AS FuelName
			,a.[Active]
			,a.[Colour]
			,a.[Chassis]
			,a.[LastDallas]
			,a.[FreightTemperature]
			,a.[FreightTemperatureThreshold1]
			,a.[FreightTemperatureThreshold2]
			,a.[Predictive]
			,a.[AVL]
			,a.[PhoneNumber]
			,h.[PartnerId]
			,h.[CapacityUnitId] AS PartnerCapacityUnitId
			,a.[RowVersion]
		FROM [General].[Vehicles] a
			INNER JOIN [General].[VehicleCostCenters] c
				ON a.[CostCenterId] = c.[CostCenterId]
			INNER JOIN [General].[VehicleCategories] d
				ON a.[VehicleCategoryId] = d.[VehicleCategoryId]
			INNER JOIN [Control].[Fuels] b
				ON d.[DefaultFuelId] = b.[FuelId]
			INNER JOIN [General].[DriversUsers] e
				ON e.[UserId] = (SELECT
									TOP 1 [UserId]
								 FROM [General].[VehiclesByUser]
								 WHERE [VehicleId] = a.[VehicleId]
								 ORDER BY [InsertDate] DESC)
			INNER JOIN [General].[Users] f
				ON e.[UserId] = f.[UserId]
			INNER JOIN [General].[Customers] g
				ON a.[CustomerId] = g.[CustomerId]
			INNER JOIN [General].[CustomersByPartner] i
				ON i.[CustomerId] = g.[CustomerId]
			INNER JOIN [General].[Partners] h
				ON i.[PartnerId] = h.[PartnerId]
		WHERE (a.[PlateId] IS NULL OR a.[PlateId] = @pPlateId)
		ORDER BY [VehicleId] DESC
	SET NOCOUNT OFF
END