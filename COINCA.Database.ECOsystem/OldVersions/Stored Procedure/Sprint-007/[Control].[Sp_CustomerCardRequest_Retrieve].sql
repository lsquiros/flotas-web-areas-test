USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerCardRequest_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CustomerCardRequest_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/15/2014
-- Description:	Retrieve Customer Card Request information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CustomerCardRequest_Retrieve]
(
	 @pCustomerId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT	 a.[CardRequestId]
			,a.[CustomerId]
			,a.[PlateId]
			,a.[DriverName]
			,a.[DriverIdentification]
			,a.[AddressLine1]
			,a.[AddressLine2]
			,a.[PaymentReference]
			,a.[AuthorizedPerson]
			,a.[ContactPhone]
			,a.[EstimatedDelivery]
			
			,a.[RowVersion]
	FROM [Control].[CardRequest] a
		LEFT JOIN [Control].[CreditCard] b
			 ON a.[CardRequestId] = b.[CardRequestId]
			AND a.[CustomerId] = b.[CustomerId]
		LEFT JOIN [General].[Status] c
			ON c.[StatusId] = b.[StatusId]
	WHERE a.[CustomerId] = @pCustomerId
	ORDER BY [CustomerId] DESC
	
    SET NOCOUNT OFF
END
