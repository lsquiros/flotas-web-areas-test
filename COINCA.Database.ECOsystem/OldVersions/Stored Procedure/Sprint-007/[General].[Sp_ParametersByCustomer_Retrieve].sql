USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ParametersByCustomer_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ParametersByCustomer_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes.
-- Create date: 12/03/2015
-- Description:	Retrieve Parameters By Customer information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ParametersByCustomer_Retrieve]
(
	@pCustomerId INT=NULL,
	@pResuourceKey VARCHAR(400)
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT
		 [ParameterByCustomerId]
		,[CustomerId]
		,[Name]
		,[Value]
		,[ResuourceKey]
        ,[RowVersion]
    FROM [General].ParametersByCustomer WHERE (@pCustomerId IS NULL OR [CustomerId] =@pCustomerId) AND (@pResuourceKey IS NULL OR  [ResuourceKey]=@pResuourceKey)
	
    SET NOCOUNT OFF
END
