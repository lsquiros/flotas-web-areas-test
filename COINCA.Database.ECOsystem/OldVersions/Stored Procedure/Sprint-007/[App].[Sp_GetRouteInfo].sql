USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetRouteInfo]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_GetRouteInfo]
GO

USE [ECOsystem]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 ----================================================================================================
 --Author:		Gerald Solano C.
 --Create date: 06/24/2015
 --Description:	Retrieve Routes from the current day
 ----================================================================================================
CREATE PROCEDURE [dbo].[Sp_GetRouteInfo]
(
	@pUserId int,
	@pCurrentDay Char(1) = null,
	@pCurrentDate datetime = null
)
AS
BEGIN
	
	IF(@pCurrentDay IS NOT NULL)
	BEGIN 

		Declare @CurrentDay int = DATEPART(DD, @pCurrentDate)
		Declare @CurrentMonth int = DATEPART(MM, @pCurrentDate)
		Declare @CurrentYear int = DATEPART(YY, @pCurrentDate)

		DECLARE @CurrentDivingVehicle int = 0
		DECLARE @CurrentRouteId int = null
		DECLARE @CurrentRouteName varchar(150) = ''
		DECLARE @RoutePoints nvarchar(max) = ''
		DECLARE @RoutePointsWithRef nvarchar(max) = ''
		DECLARE @IndexRouteSeg int
		DECLARE @Polyline nvarchar(max) = ''

		-- ////////////////// ACTUAL VEHICULO ASIGNADO AL CONDUCTOR ///////////////////////////
		SELECT TOP(1) @CurrentDivingVehicle = [VehicleId]
		FROM [General].[VehiclesByUser]
		WHERE [LastDateDriving] IS NULL AND UserId = @pUserId
		ORDER BY [VehiclesByUserId] DESC

		--/////////////////////////////////////////////////////////////////////////////////////

		-- /////////// RUTA DEL VEHICULO CON BASE AL D�A ACTUAL Y AL VEHICULO ASIGNADO /////////
		SELECT @CurrentRouteId = r.[RouteId],
			   @CurrentRouteName = r.[Name]
		FROM [Efficiency].[VehicleByRoute] vr
		INNER JOIN [Efficiency].[Routes] r ON r.RouteId = vr.RouteId
		WHERE [VehicleId] = @CurrentDivingVehicle AND 
			  [Days] LIKE '%'+ @pCurrentDay +'%' AND
			  [Active] = 1

		--////////////////////////////////////////////////////////////////////////////////////

		-- /////////////// OBTENEMOS LOS PUNTOS DE LA RUTA (LATITUD Y LONGITUD) PRINCIPAL NO LIGADOS DIRECTAMENTE CON EL USUARIO///////////
		SELECT 	@RoutePoints = CONCAT(@RoutePoints, rp.[PointId], ',', rp.[Name], ',', rp.[Latitude], ',', rp.[Longitude], 
										',', cv.[VisitId],',', cv.[Contact],',', cv.[Address],',', cv.[Phone], ',', rp.[Time], ',', cv.[State],',', cv.[Reason],',', cv.[IsVisit],'|'),
				@RoutePointsWithRef = CONCAT(@RoutePointsWithRef, [PointReference], '*', [Latitude], ' ', [Longitude], ',')
		FROM [Efficiency].[RoutesPoints] rp
		LEFT JOIN [General].[ClientVisit] cv 
			ON cv.[RoutePointId] = rp.[PointId] 
		WHERE rp.[RouteId] = @CurrentRouteId AND cv.[RoutePointId] IS NULL

		-- ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		-- /////////////// OBTENEMOS LOS PUNTOS DE LA RUTA (LATITUD Y LONGITUD) RELACIONADOS CON EL USUARIO ////////////
		SELECT 	@RoutePoints = CONCAT(@RoutePoints, rp.[PointId], ',', rp.[Name], ',', rp.[Latitude], ',', rp.[Longitude],  
										',', cv.[VisitId],',', cv.[Contact],',', cv.[Address],',', cv.[Phone], ',', rp.[Time], ',', 
											CASE cv.[IsVisit]
												WHEN 1 THEN cv.[State]
												ELSE 
													(SELECT pc.[State] FROM [Efficiency].[PassControl] pc 
													 WHERE pc.[PointId] = rp.[PointId] AND  [ControlDay] = @CurrentDay AND [ControlMonth] = @CurrentMonth AND  [ControlYear] = @CurrentYear)
											END
										,',', cv.[Reason],',', cv.[IsVisit],'|'),
				@RoutePointsWithRef = CONCAT(@RoutePointsWithRef, [PointReference], '*', [Latitude], ' ', [Longitude], ',')
		FROM [Efficiency].[RoutesPoints] rp
		INNER JOIN [General].[ClientVisit] cv 
			ON cv.[RoutePointId] = rp.[PointId] AND
			   cv.UserId = @pUserId	-- PUNTOS DE VISITA LIGADOS A UN USUARIO
		WHERE rp.[RouteId] = @CurrentRouteId

		SET @RoutePointsWithRef = CONCAT(@RoutePointsWithRef, '|')
		SET @RoutePointsWithRef = REPLACE(@RoutePointsWithRef, ',|', '')
		--////////////////////////////////////////////////////////////////////////////////////

		--///////// EXTRAEMOS LAS POLYLINEAS DE LA RUTA EN EL ORDEN RESPECTIVO ///////////////
		SELECT @IndexRouteSeg = MIN(SegmentId)
		FROM [Efficiency].[RoutesSegments]
		WHERE [RouteId] = @CurrentRouteId

		WHILE(@IndexRouteSeg IS NOT NULL)
		BEGIN
	
			SELECT @Polyline = CONCAT(@Polyline, [Poliyline].STAsText(), ',')
			FROM [Efficiency].[RoutesSegments]
			WHERE [SegmentId] = @IndexRouteSeg

			SELECT @IndexRouteSeg = MIN(SegmentId)
			FROM [Efficiency].[RoutesSegments]
			WHERE [RouteId] = @CurrentRouteId AND  SegmentId > @IndexRouteSeg 
		END

		--SELECT @Polyline AS [Poliyline]

		SET @Polyline = REPLACE(@Polyline, 'LINESTRING (', '')
		SET @Polyline = REPLACE(@Polyline, ')', '')
		SET @Polyline = CONCAT('LINESTRING (', @Polyline, ')')
		SET @Polyline = REPLACE(@Polyline, ',)', ')')

		-- ////////////////////////////////////////////////////////////////////////////////////

		-- RETORNAMOS LOS DATOS DE LA RUTA
		SELECT @CurrentRouteId AS RouteId, 
			   @CurrentRouteName AS RouteName, 
			   @CurrentDivingVehicle AS CurrentVehicleId, 
			   @RoutePoints AS RoutePoints,
			   @RoutePointsWithRef AS RoutePointsWithRef,
			   @Polyline  AS Polyline
	END
	ELSE
	BEGIN
		--/// NO HAY D�A CONSULTADO
		SELECT 0 AS RouteId, 
			  '' AS RouteName,
			   0 AS CurrentVehicleId, 
			  '' AS RoutePoints,
			  '' AS RoutePointsRef,
			  '' AS Polyline
	END

END --End  Stored
GO

