USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Alarm_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Alarm_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Mart�nez H.
-- Create date: 17/Nov/2014
-- Description:	Insert or Update Alarm information
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_Alarm_AddOrEdit]
(
	 @pAlarmId INT = NULL 
	,@pCustomerId INT
	,@pPhone VARCHAR(100)
	,@pEmail VARCHAR(500)
	,@pAlarmTriggerId INT 
	,@pEntityTypeId INT 
	,@pEntityId INT
	,@pActive BIT 
	,@pPeriodicityTypeId INT = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP = NULL
)
AS
BEGIN 
	SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pAlarmId IS NULL)
			BEGIN 
				INSERT INTO [General].[Alarms]
						([CustomerId]
						,[Phone]
						,[Email]
						,[AlarmTriggerId]
						,[EntityTypeId]
						,[EntityId]
						,[Active]
						,[PeriodicityTypeId]
						,[InsertDate]
						,[InsertUserId])
				VALUES (@pCustomerId
						,@pPhone
						,@pEmail
						,@pAlarmTriggerId
						,@pEntityTypeId
						,@pEntityId
						,@pActive
						,@pPeriodicityTypeId
						,GETUTCDATE()
						,@pLoggedUserId)
			END 
			ELSE 
			BEGIN 
				UPDATE [General].[Alarms]
					SET  [Phone] = @pPhone
						,[Email] = @pEmail
						,[AlarmTriggerId] = @pAlarmTriggerId
						,[EntityTypeId] = @pEntityTypeId
						,[EntityId] = @pEntityId
						,[Active] = @pActive
						,[PeriodicityTypeId] = @pPeriodicityTypeId
				WHERE [AlarmId] = @pAlarmId
				AND [RowVersion] = @pRowVersion
			END
			
			SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END 
GO 