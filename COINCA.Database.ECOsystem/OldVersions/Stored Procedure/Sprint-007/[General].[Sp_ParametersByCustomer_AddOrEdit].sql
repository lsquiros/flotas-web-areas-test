USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ParametersByCustomer_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ParametersByCustomer_AddOrEdit]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_Parameters_AddOrEdit]    Script Date: 03/12/2015 08:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andres Oviedo Brenes
-- Create date: 12/03/2015
-- Description:	Insert or Update Parameters By Customer information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ParametersByCustomer_AddOrEdit]
(
	 @pParameterByCustomerId INT = NULL
	,@pCustomerId INT 
	,@pName varchar(100)
	,@pValue varchar(max)
	,@pResuourceKey varchar(400)
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			declare @cont INT=(select COUNT(*) from General.ParametersByCustomer where ResuourceKey=@pResuourceKey AND CustomerId=@pCustomerId)
			
			IF (@cont=0)
			BEGIN
				INSERT INTO General.ParametersByCustomer
						( [CustomerId]
						 ,[Name]
						 ,[Value]
						 ,[ResuourceKey]
     					 ,[InsertDate]
						 ,[InsertUserId])
				VALUES	(@pCustomerId
						,@pName
						,@pValue
						,@pResuourceKey
						,GETUTCDATE()
						,@pLoggedUserId
						)
			END
			ELSE
			BEGIN
				UPDATE [General].ParametersByCustomer
					SET  [CustomerId]=@pCustomerId
						 ,[Name]=@pName
						 ,[Value]=@pValue
						 ,[ModifyDate] = GETUTCDATE()
						 ,[ModifyUserId] = @pLoggedUserId
				WHERE [ResuourceKey] = @pResuourceKey or ParameterByCustomerId=@pParameterByCustomerId
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
