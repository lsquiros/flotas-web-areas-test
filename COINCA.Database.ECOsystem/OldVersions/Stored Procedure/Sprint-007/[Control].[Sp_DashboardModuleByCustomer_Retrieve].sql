USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_DashboardModuleByCustomer_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_DashboardModuleByCustomer_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 09/11/2014
-- Description:	Retrieve country information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_DashboardModuleByCustomer_Retrieve]
(
	 @pCustomerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT 
		M.DashboardModuleId,
		M.Name,
		@pCustomerId,
		D.[Order],
		D.[RowVersion]
	FROM 
		[General].[DashboardModules] M
		LEFT OUTER JOIN
		[General].[DashboardModulesByCustomer] D ON M.DashboardModuleId = D.DashboardModuleId
	WHERE
		D.CustomerId IS NULL OR D.CustomerId = @pCustomerId
	ORDER BY
		[ORDER]
	
    SET NOCOUNT OFF
END


	