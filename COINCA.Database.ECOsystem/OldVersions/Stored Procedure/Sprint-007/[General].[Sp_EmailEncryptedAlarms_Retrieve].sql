USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_EmailEncryptedAlarms_Retrieve]    Script Date: 01/28/2015 14:02:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailEncryptedAlarms_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_EmailEncryptedAlarms_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_EmailEncryptedAlarms_Retrieve]    Script Date: 01/28/2015 14:02:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 28/01/2015
-- Description:	Retrieve Email FleetCard information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_EmailEncryptedAlarms_Retrieve]
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		a.[EmailId],
		a.[To],
		a.[From],
		a.[CC],
		a.[Subject],
		a.[Message],
		a.[IsHtml],
		a.[RowVersion]
	FROM [General].[EmailEncryptedAlarms] a
	WHERE a.[Sent] = 0
	SET NOCOUNT OFF 
END
GO

