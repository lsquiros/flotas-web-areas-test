USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Partners_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Partner information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Partners_Retrieve]
(
	 @pPartnerId INT = NULL,
	 @pKey VARCHAR(800) = NULL,
	 @pPartnerGroupId INT = NULL,
	 @pPartnerUserId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT
		 a.[PartnerId]
		,a.[CountryId]
		,a.[Name]
		,a.[Logo]
		,b.[Name] AS [CountryName]
		,a.[ApiUserName]
		,a.[ApiPassword]
		,a.[CapacityUnitId]
		,a.[PartnerTypeId]
		,c.[Name] AS [CapacityUnitName]
		,p.[Name] AS [PartnerTypeName]
		,a.[RowVersion]
		,a.[FuelErrorPercent]
    FROM [General].[Partners] a
		INNER JOIN [General].[Countries] b
			ON a.[CountryId] = b.[CountryId]
		LEFT JOIN [General].[Types] c
			ON a.[CapacityUnitId] = c.[TypeId] and c.[Usage] = 'CUSTOMERS_CAPACITY_UNITS'
		LEFT JOIN [General].[Types] p
			ON a.[PartnerTypeId] = p.[TypeId] and p.[Usage] = 'PARTNER_TYPE'
	WHERE a.[IsDeleted] = 0
	  AND (@pPartnerId IS NULL OR a.[PartnerId] = @pPartnerId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR b.[Name] like '%'+@pKey+'%')
	  AND (@pPartnerGroupId IS NULL OR a.[PartnerGroupId] = @pPartnerGroupId)
	  AND (@pPartnerUserId IS NULL OR EXISTS (SELECT 1
													FROM [General].[PartnerUsersByCountry] x
												WHERE x.[PartnerUserId] = @pPartnerUserId
												  AND x.CountryId = a.[CountryId]))
	ORDER BY [PartnerId] DESC
	
    SET NOCOUNT OFF
END
