USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_EmailEncryptedAlarms_AddOrEdit]    Script Date: 01/28/2015 14:08:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EmailEncryptedAlarms_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_EmailEncryptedAlarms_AddOrEdit]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_EmailEncryptedAlarms_AddOrEdit]    Script Date: 01/28/2015 14:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Cristian Mart�nez H.
-- Create date: 30/Nov/2014
-- Description:	Add Or Edit Email information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_EmailEncryptedAlarms_AddOrEdit]
(
	@pEmailId INT = NULL,
	@pTo VARCHAR(500),
	@pFrom VARCHAR(100) = NULL,
	@pCC VARCHAR(100) = NULL,
	@pSubject VARCHAR(300) = NULL, 
	@pMessage NVARCHAR(1000) = NULL, 
	@pIsHtml BIT, 
	@pSent BIT, 
	@pLoggedUserId INT,
	@pRowVersion TIMESTAMP = NULL
)
AS
BEGIN 
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		BEGIN 
			UPDATE [General].[EmailEncryptedAlarms]
			SET [To]  = @pTo,
				[From] = @pFrom,
				[CC] = @pCC,
				[Subject] = @pSubject,
				[Message] = @pMessage,
				[IsHtml] = @pIsHtml,
				[Sent] = @pSent,
				[ModifyUserId] = @pLoggedUserId, 
				[ModifyDate] = GETUTCDATE()
			WHERE [EmailId] = @pEmailId
			  AND [RowVersion] = @pRowVersion

		END
		
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO


