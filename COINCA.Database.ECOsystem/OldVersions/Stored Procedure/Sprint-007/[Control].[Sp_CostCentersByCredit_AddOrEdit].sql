USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CostCentersByCredit_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CostCentersByCredit_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update Vehicle Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CostCentersByCredit_AddOrEdit]
(
	 @pVehicleFuelId INT = NULL				--@pVehicleFuelId: PK of the table
	,@pVehicleId INT						--@pVehicleId: FK of Vehicles
	,@pLiters DECIMAL(16,10)				--@pLiters: Liters of Fuel
	,@pAmount DECIMAL(16,6)					--@pAmount: Amount of Fuel
	,@pMonth INT							--@pMonth: Month
	,@pYear INT								--@pYear: Year
	,@pCustomerId INT						--@pCustomerId: Customer Id
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	EXEC [Control].[Sp_VehicleFuel_AddOrEdit]	 @pVehicleFuelId
												,@pVehicleId
												,@pLiters
												,@pAmount
												,@pMonth
												,@pYear
												,@pCustomerId
												,@pLoggedUserId
												,@pRowVersion
												,0 -- @pThrowError
	
END
GO