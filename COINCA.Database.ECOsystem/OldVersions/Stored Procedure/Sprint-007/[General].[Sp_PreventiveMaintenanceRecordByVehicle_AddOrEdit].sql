USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicle_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/16/2015
-- Description:	Insert or Update Vehicle by Route information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicle_AddOrEdit]
(
	@pXmlData VARCHAR(MAX),					
	@pLoggedUserId INT						
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0

            DECLARE @lTotal DECIMAL(16, 2)
            DECLARE @lCatalogId INT
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			INSERT INTO [General].[PreventiveMaintenanceCost]
					([PreventiveMaintenanceCatalogId]
					,[Description]
					,[Cost]
					,[InsertUserId]
					,[InsertDateId])
			SELECT 
					Row.col.value('./@PreventiveMaintenanceCatalogId', 'INT') AS [PreventiveMaintenanceCatalogId]
					,Row.col.value('./@Description', 'VARCHAR(500)') AS [Description]
					,Row.col.value('./@Cost', 'DECIMAL(16,2)') AS [Cost]
					,@pLoggedUserId
					,GETUTCDATE()
			FROM @lxmlData.nodes('/xmldata/PreventiveMaintenanceCost') Row(col)
			WHERE Row.col.value('./@PreventiveMaintenanceCostId', 'INT') = 0
			
			UPDATE cost SET 
					cost.Description = Row.col.value('./@Description', 'VARCHAR(500)'),
					cost.Cost = Row.col.value('./@Cost', 'DECIMAL(16,2)'),
					cost.ModifyUserId = @pLoggedUserId,
					cost.ModifyDateId = GETDATE()
			FROM @lxmlData.nodes('/xmldata/PreventiveMaintenanceCost') Row(col)
			INNER JOIN [General].[PreventiveMaintenanceCost] cost ON Row.col.value('./@PreventiveMaintenanceCostId', 'INT') = cost.PreventiveMaintenanceCostId
			WHERE Row.col.value('./@PreventiveMaintenanceCostId', 'INT') <> 0
			
			SELECT @lTotal = SUM(Row.col.value('./@Cost', 'DECIMAL(16,2)'))
			FROM @lxmlData.nodes('/xmldata/PreventiveMaintenanceCost') Row(col)

			SELECT TOP 1 @lCatalogId = Row.col.value('./@PreventiveMaintenanceCatalogId', 'DECIMAL(16,2)')
			FROM @lxmlData.nodes('/xmldata/PreventiveMaintenanceCost') Row(col)

			UPDATE [General].[PreventiveMaintenanceCatalog] 
			SET [Cost] = @lTotal 
			WHERE [PreventiveMaintenanceCatalogId] = @lCatalogId

            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO