USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Control].[Sp_CardFileParams_Retrieve]    Script Date: 05/25/2015 14:13:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Kenneth Mora Flores.
-- Create date: 05/25/2015
-- Description:	Retrieve Parameters to create Credit Card File
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CardFileParams_Retrieve]
AS
BEGIN
	
	SET NOCOUNT ON	
	
	SELECT [CardFileParamId]
		  ,[CardFileParamName]
		  ,[CardFileParamLenght]
		  ,[Separator]
		  ,[FillChar]
	  FROM [Control].[CardFileParams]
	  
    SET NOCOUNT OFF
END





