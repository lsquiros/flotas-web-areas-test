USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================
-- Author:		Danilo Hidalgo.
-- Create date: 08/01/2015
-- Description:	Preventive Maintenance Record By Vehicle Detail information
-- ===========================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]
	 @pPreventiveMaintenanceId INT
AS
BEGIN 
	SET NOCOUNT ON;
	
		If Exists (SELECT 1 FROM [General].[PreventiveMaintenanceRecordByVehicle] WHERE [PreventiveMaintenanceId] = @pPreventiveMaintenanceId)
		Begin
			SELECT 
				a.[PreventiveMaintenanceRecordByVehicleId],
				b.[PreventiveMaintenanceRecordByVehicleDetailId],
				a.[PreventiveMaintenanceId], 
				b.[PreventiveMaintenanceCostId], 
				b.[Description], 
				b.[Cost], 
				b.[Record] 
			FROM [General].[PreventiveMaintenanceRecordByVehicle] a
				INNER JOIN [General].[PreventiveMaintenanceRecordByVehicleDetail] b ON a.[PreventiveMaintenanceRecordByVehicleId] = b.[PreventiveMaintenanceRecordByVehicleId]
			WHERE a.[PreventiveMaintenanceId] = @pPreventiveMaintenanceId
		End
		Else
		Begin
			SELECT 
				0 as [PreventiveMaintenanceRecordByVehicleId],
				0 as [PreventiveMaintenanceRecordByVehicleDetailId],
				a.[PreventiveMaintenanceId], 
				b.[PreventiveMaintenanceCostId], 
				b.[Description], 
				b.[Cost], 
				0 as [Record]
			FROM [General].[PreventiveMaintenanceByVehicle] a 
				INNER JOIN [General].[PreventiveMaintenanceCost] b ON a.[PreventiveMaintenanceCatalogId] = b.[PreventiveMaintenanceCatalogId]
			WHERE a.[PreventiveMaintenanceId] = @pPreventiveMaintenanceId
			ORDER BY b.Description
		End
	
	SET NOCOUNT OFF;
END

