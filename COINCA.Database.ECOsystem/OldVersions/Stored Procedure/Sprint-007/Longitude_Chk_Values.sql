ALTER TABLE dbo.Reports
ADD CONSTRAINT Longitude_Chk_Values CHECK (Longitude >= -90 AND Longitude <= 90)
