USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardTransaction_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardTransaction_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 05/12/2014
-- Description:	Insert or Update CreditCard Transactions through the VPOS Process
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardTransaction_AddOrEdit]
(
	 @pCCTransactionVPOSId INT = NULL	    --@pCCTransactionVPOSId: Filter to insert or update process
	,@pCreditCardId INT						--@pCreditCardId: Customer Id
	,@pCustomer varchar(50)					--@pCustomer: Credit Card Number Id
	,@pTransactionType varchar(50)			--@pTransactionType: Credit Card Number Id
	,@pTerminalID nvarchar(50)				--@pTerminalID: Expiration Year
	,@pInvoice nvarchar(50)	= NULL			--@pInvoice: Expiration Month
	,@pEntryMode nvarchar(50) = NULL		--@pEntryMode: Credit Limit
	,@pAccountNumber nvarchar(50) = NULL	--@pAccountNumber: Status Id
	,@pExpirationDate nvarchar(5) = NULL	--@pExpirationDate: Month
	,@pTotalAmount [numeric](16, 2) = NULL	--@pTotalAmount: Year
	,@pOdometer [numeric](16, 2) = NULL		--@pTotalAmount: Year
	,@pLiters [numeric](16, 2) = NULL		--@pTotalAmount: Year
	,@pPlate nvarchar(10) = NULL			--@pTotalAmount: Year
	,@pAuthorizationNumber nvarchar(50) = NULL		--@pAuthorizationNumber: User Id
	,@pReferenceNumber nvarchar(50) = NULL			--@pReferenceNumber: Vehicle Id
	,@pSystemTraceNumber nvarchar(50) = NULL		--@pSystemTraceNumber: Vehicle Id
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
	
	BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCCTransactionVPOSId IS NULL)
			BEGIN
				INSERT INTO [Control].[CreditCardTransactionVPOS]
						([CreditCardId]
						,[Customer]
						,[TransactionType]
						,[TerminalID]
						,[Invoice]
						,[EntryMode]
						,[AccountNumber]
						,[ExpirationDate]
						,[TotalAmount]
						,[Odometer]
						,[Liters]
						,[Plate]
						,[AuthorizationNumber]
						,[ReferenceNumber]
						,[SystemTraceNumber]
						,[InsertDate]
						,[InsertUserId])
				VALUES ( @pCreditCardId
						,@pCustomer
						,@pTransactionType
						,@pTerminalID
						,@pInvoice
						,@pEntryMode
						,@pAccountNumber
						,@pExpirationDate
						,@pTotalAmount
						,@pOdometer
						,@pLiters
						,@pPlate
						,@pAuthorizationNumber
						,@pReferenceNumber
						,@pSystemTraceNumber
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE						
			BEGIN
				UPDATE [Control].[CreditCardTransactionVPOS]
				SET
					 [Invoice] = @pInvoice
					,[AuthorizationNumber] = @pAuthorizationNumber
					,[ReferenceNumber] = @pReferenceNumber
					,[SystemTraceNumber] = @pSystemTraceNumber
					,[ModifyUserId] = @pLoggedUserId
					,[ModifyDate] = GETUTCDATE()
				WHERE 
					[CreditCardTransactionVPOS] = @pCCTransactionVPOSId
					
			END
			
			SET @lRowCount = @@ROWCOUNT
			
			IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
				
				IF(@pCCTransactionVPOSId IS NULL)
				BEGIN
					SELECT SCOPE_IDENTITY()[CreditCardTransactionVPOS];
				END
				ELSE
				BEGIN
					SELECT @pCCTransactionVPOSId	
				END
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO



