USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MonthlyClosing_LowPerformanceEmails]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_MonthlyClosing_LowPerformanceEmails]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 02/19/2015
-- Description:	Send the Monthly Closing Low Performance Emails
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_MonthlyClosing_LowPerformanceEmails]
(
	 @pYear INT								--@pYear: Year of monthly closing
	,@pMonth INT							--@pMonth: Month of monthly closing
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	DECLARE @lTo varchar(100) = '',
			@lSubject varchar(300),
			@lMessage nvarchar(1000)
	
	SET @lSubject = 'Alerta en vehiculo con Caida de Rendimiento'
		
	
	DECLARE @lTable TABLE
	(	 [Index] INT NULL
		,[VehicleId] INT NULL
		,[CurrentPerformance] INT NULL
		,[PreviousPerformance] INT NULL
		,[DeleteFlag] BIT NULL
	)	
	
	INSERT @lTable(
		 [VehicleId]
		,[CurrentPerformance]
		,[PreviousPerformance]
		,[DeleteFlag]
	)	
	SELECT a.[VehicleId], AVG(a.[TrxPerformance]), 0 AS [PreviousPerformance], 1 AS [DeleteFlag]
		FROM [General].[PerformanceByVehicle] a
	WHERE DATEPART(MONTH, a.[TrxDate]) = @pMonth
	  AND DATEPART(YEAR, a.[TrxDate]) = @pYear
	GROUP BY a.[VehicleId]
	
	IF(@pMonth = 1)
	BEGIN
		SELECT @pMonth = 12, @pYear = @pYear-1
	END
	ELSE
	BEGIN
		SELECT @pMonth = @pMonth-1
	END
	
	UPDATE @lTable
	SET [PreviousPerformance] = t.[PerformanceByOdometer]
	FROM (
		SELECT a.[VehicleId] AS [TempVehicleId], AVG(a.[TrxPerformance]) AS [PerformanceByOdometer]
		FROM [General].[PerformanceByVehicle] a
	WHERE DATEPART(MONTH, a.[TrxDate]) = @pMonth
	  AND DATEPART(YEAR, a.[TrxDate]) = @pYear
	GROUP BY a.[VehicleId]) t
	WHERE [VehicleId] = t.[TempVehicleId]
	
	
	UPDATE @lTable
	SET [DeleteFlag] = 0
	WHERE -2 >= [CurrentPerformance] - [PreviousPerformance]

	DELETE @lTable
	WHERE [DeleteFlag] = 1
	
	DECLARE @lIndex INT = 0
	UPDATE @lTable
	SET [Index] = @lIndex, @lIndex = @lIndex+1
	
	
	
	---Send Emails---
	DECLARE @lCustomerId INT
	
	WHILE(@lIndex > 0)
	BEGIN
		
		SELECT	 @lCustomerId = a.[CustomerId]
				,@lMessage = 'Se produjo una caida de 2 o mas puntos de rendimiento en el siguiente vehiculo: ' + a.PlateId
		FROM [General].[Vehicles] a
			INNER JOIN @lTable t
				ON a.VehicleId = t.VehicleId
		WHERE t.[Index] = @lIndex
		
		SET @lTo = [General].[Fn_GetEmailsRole]('CUSTOMER_ADMIN', @lCustomerId, 100)
		
		EXEC [General].[Sp_Email_AddOrEdit] null, @lTo, null, null, @lSubject, @lMessage, 0, 0, null, null 
		
		
		SET @lIndex = @lIndex-1
	END
	---Send Emails---
	
			
    SET NOCOUNT OFF
END
