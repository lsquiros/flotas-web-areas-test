USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_GroupReference_Retrieve]    Script Date: 11/06/2014 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GroupReference_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_GroupReference_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_GroupReference_Retrieve]    Script Date: 11/06/2014 10:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/03/2014
-- Description:	Retrieve IntrackReference information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_GroupReference_Retrieve]
(
	@pId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		v.[VehicleGroupId] [VehicleGroupId], 
		co.[Code] [CountryCode],
		c.[CustomerId] [CustomerId]
	FROM 
		[General].[VehicleGroups] v
		INNER JOIN [General].[Customers] c on v.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Countries] co on co.[CountryId] = c.[CountryId]
	WHERE 
		v.[VehicleGroupId] = @pId
	
			
    SET NOCOUNT OFF
END

GO
