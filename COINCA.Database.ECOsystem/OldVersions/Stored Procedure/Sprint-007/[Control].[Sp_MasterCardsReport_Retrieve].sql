USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_MasterCardsReport_Retrieve]    Script Date: 12/10/2014 10:44:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MasterCardsReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_MasterCardsReport_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_MasterCardsReport_Retrieve]    Script Date: 12/10/2014 10:44:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/21/2015
-- Description:	Retrieve Master Cards Report information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_MasterCardsReport_Retrieve]
(
	@pCustomerId INT = NULL,		--@pCustomerId: CustomerId
	@pCountryId INT = NULL,			--@pCountryId: CountryId
	@pYear INT = NULL,				--@pYear: Year
	@pMonth INT = NULL,				--@pMonth: Month
	@pStartDate DATETIME = NULL,	--@pStartDate: Start Date
	@pEndDate DATETIME = NULL		--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @pIssueForId INT

		SELECT 
			a.[CreditCardNumber],
			a.[CreditCardHolder],
			CONVERT(VARCHAR(2),a.[ExpirationMonth]) + ' / ' + CONVERT(VARCHAR(4),a.ExpirationYear) [Expiration],
			a.[CreditLimit],
			c.[Name] [CurrencyName],
			d.[Name] [StatusName],
			c.[Symbol] [CurrencySymbol]
		FROM [General].[CustomerCreditCards] a
			INNER JOIN [General].[Customers] b ON a.[CustomerId] = b.[CustomerId]
			INNER JOIN [Control].[Currencies] c ON b.[CurrencyId] = c.[CurrencyId]
			INNER JOIN [General].[Status] d ON a.[StatusId] = d.[StatusId]
		WHERE 
			a.[CustomerId] = @pCustomerId AND 
			b.[CountryId] = @pCountryId AND
				((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
				AND DATEPART(m,a.[InsertDate]) = @pMonth
				AND DATEPART(yyyy,a.[InsertDate]) = @pYear) OR
				(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
				AND a.[InsertDate] BETWEEN @pStartDate AND @pEndDate))
		ORDER BY CreditCardHolder
		
    SET NOCOUNT OFF
END


GO

