USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PasswordsHistory_Check]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PasswordsHistory_Check]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================================================================
-- Author:		Napole�n Alvarado
-- Create date: 12/05/2014
-- Description:	Check the new Password against the Password History 
--				0: Success, Greather Than 0: Fail (returns the number of passwords validated)
-- ==================================================================================================
CREATE PROCEDURE [General].[Sp_PasswordsHistory_Check]
(
	 @pUserId INT							--@pUserId: User Id related to Password
	,@pPassword VARCHAR(256)				--@pPassword: Password encrypted
)
AS
BEGIN	
	SET NOCOUNT ON	
	DECLARE @lPasswordsToValidate int
	
	Select @lPasswordsToValidate = PasswordsValidateRepetition 
	  from [General].[Parameters]
	  
	IF @lPasswordsToValidate is null or @lPasswordsToValidate = 0
		Set @lPasswordsToValidate = 5

	IF @pPassword in (Select Top(@lPasswordsToValidate) Password
					    from [General].[PasswordsHistory]
					   where [UserId] = @pUserId
					   order by [PasswordDate] desc)
	BEGIN
		Select @lPasswordsToValidate
	END
	ELSE
	BEGIN
		Select 0
	END
	 
    SET NOCOUNT OFF
END
GO