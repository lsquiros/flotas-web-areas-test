USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Types_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Types_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Types information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Types_Retrieve](
	@pUsage VARCHAR(50)
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT
		 a.[TypeId]
		,a.[Name]		
		,a.[Code]
    FROM [General].[Types] a
    WHERE a.[Usage] = @pUsage
      AND a.[IsActive] = 1
    ORDER BY a.[RowOrder]
	
    SET NOCOUNT OFF
END
