USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TrendAndConsumptionReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TrendAndConsumptionReport_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes.
-- Create date: 22/01/2015
-- Description:	Retrieve TrendAndConsumptionReport information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TrendAndConsumptionReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pKey VARCHAR(800) = NULL			--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
		DECLARE @lCurrencySymbol NVARCHAR(4) = ''
		
		SELECT
			@lCurrencySymbol = a.Symbol
		FROM [Control].[Currencies] a
			INNER JOIN [General].[Customers] b
				ON a.[CurrencyId] = b.[CurrencyId]
		WHERE b.[CustomerId] = @pCustomerId
		
		SELECT
			 a.[VehicleId]
			,a.[PlateId]
			,MAX(b.TrxReportedLiters) AS [MaxConsumption]
			,AVG(b.TrxReportedLiters) AS [AvgConsumption]
			,Min(b.TrxReportedLiters) AS [MinConsumption]
			,@lCurrencySymbol as [CurrencySymbol]
			,SUM(b.TrxReportedLiters) AS [TotalConsumption]
			,SUM(c.[FuelAmount]) AS [TotalFuelAmount]
		FROM [General].[Vehicles] a
			INNER JOIN [General].[PerformanceByVehicle] b
				ON a.[VehicleId] = b.[VehicleId]
			INNER JOIN [Control].[Transactions] c WITH(READUNCOMMITTED)
				ON b.[TransactionId] = c.[TransactionId]
		WHERE a.[CustomerId] = @pCustomerId
		  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,b.[TrxDate]) = @pMonth
								AND DATEPART(yyyy,b.[TrxDate]) = @pYear) OR
									(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND b.[TrxDate] BETWEEN @pStartDate AND @pEndDate))
		  AND (@pKey IS NULL 
				OR a.[PlateId] like '%'+@pKey+'%')
		GROUP BY a.[VehicleId]
				,a.[PlateId]
		
	SET NOCOUNT OFF
END
