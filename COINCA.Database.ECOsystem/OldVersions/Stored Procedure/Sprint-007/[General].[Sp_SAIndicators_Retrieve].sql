USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_SAIndicators_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_SAIndicators_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa H.
-- Create date: 11/14/2014
-- Description:	Retrieve Indicators for Super Admin Landing Page
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_SAIndicators_Retrieve]
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		(SELECT COUNT(1) FROM General.Partners) AS Partners, 
		(SELECT COUNT(1) FROM General.Customers where IsDeleted = 0) AS Customers, 
		(SELECT COUNT(1) FROM General.Vehicles) AS Vehicles, 
		(SELECT COUNT(1) FROM General.DriversUsers) AS Drivers
	
    SET NOCOUNT OFF
END
