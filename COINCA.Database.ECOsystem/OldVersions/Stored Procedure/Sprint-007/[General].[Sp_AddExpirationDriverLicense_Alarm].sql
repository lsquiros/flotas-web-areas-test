USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AddExpirationDriverLicense_Alarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_AddExpirationDriverLicense_Alarm]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [General].[Sp_AddExpirationDriverLicense_Alarm]
AS
BEGIN 
	DECLARE @lAlarmId INT, 
			@lUserId INT,
			@lCustomerId INT						
			
	DECLARE cDrivers CURSOR FOR
	
	SELECT [UserId], [CustomerId] FROM [General].[DriversUsers]
	WHERE [LicenseExpiration] < GETUTCDATE()
	
	OPEN cDrivers
	
	FETCH cDrivers INTO @lUserId, @lCustomerId
	WHILE (@@FETCH_STATUS = 0)
	BEGIN 
		SELECT @lAlarmId = [AlarmId]
		FROM [General].[Alarms]
		WHERE [AlarmTriggerId] = 504 
		  AND [EntityId] = @lUserId
		  AND [CustomerId] = @lCustomerId
		  AND [Active] = 'TRUE'
			
		IF @lAlarmId IS NOT NULL 
		BEGIN 
			PRINT @lUserId 
		END		
		
		FETCH cDrivers INTO @lUserId, @lCustomerId
	END 	
	CLOSE cDrivers
	
	DEALLOCATE cDrivers
	
END