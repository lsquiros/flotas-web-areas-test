USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_SendAlarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_SendAlarm]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Cristian Martínez 
-- Create date: 24/11/2014
-- Description:	Store Procedure 
-- ================================================================================================
create PROCEDURE [General].[Sp_SendAlarm]
	@pAlarmId INT = NULL,
	@pAlarmTriggerId INT = NULL,
	@pPeriodicityAlarm INT = NULL,
	@pCustomerId INT = NULL,
	@pPlate VARCHAR(10) = NULL,
	@pDescription VARCHAR(500) = NULL,	
	@pName VARCHAR(250) = NULL,
	@pTimeLastReport TIME = NULL, 
	@pPhone VARCHAR(100) = NULL, 
	@pEmail VARCHAR (500) = NULL, 
	@pInOut VARCHAR (10) = NULL, 
	@pRowVersion TIMESTAMP = NULL
AS
BEGIN 
	DECLARE @lErrorMessage NVARCHAR(4000)
    DECLARE @lErrorSeverity INT
    DECLARE @lErrorState INT
    DECLARE @lSubject VARCHAR(300)
	DECLARE @lMessage VARCHAR (MAX)
	DECLARE @lValue INT = 0
	DECLARE @lNextAlarm DATE
		
	BEGIN TRY 
		-- Get Message
		SELECT @lMessage = b.[Message] 
		FROM [General].[Types] a
			INNER JOIN [General].[Values] b
				  ON a.[TypeId] = b.[TypeId]
		WHERE a.[TypeId] = @pAlarmTriggerId
		
		IF @pPeriodicityAlarm IS NOT NULL
		BEGIN 
			-- Get Value		
			SELECT @lValue = b.[Value] 
			FROM [General].[Types] a
				INNER JOIN [General].[Values] b
		   			  ON a.[TypeId] = b.[TypeId]
			WHERE a.[TypeId] = @pPeriodicityAlarm
		END 
					
		IF (@lValue IS NOT NULL AND @lMessage IS NOT NULL)
		BEGIN 		
			SET @lNextAlarm = DATEADD(DAY, @lValue, DATEADD(Hour, -6,GETUTCDATE()))
			
			IF @pAlarmTriggerId = 500 
			BEGIN 
				SET @lSubject = 'Alarma Geocerca'
				SET @lMessage = REPLACE(@lMessage, '%vehicle%', @pPlate)
				SET @lMessage = REPLACE(@lMessage, '%InOut%', @pInOut)
				SET @lMessage = REPLACE(@lMessage, '%Description%', @pDescription)
				SET @lMessage = REPLACE(@lMessage, '%time%', CONVERT(VARCHAR(10), DATEADD(Hour, -6,GETUTCDATE()), 104) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, -6,GETUTCDATE()), 108))
				SET @lNextAlarm = NULL 
			END 
	
			IF @pAlarmTriggerId = 502 
			BEGIN 
				SET @lSubject = 'Alarma Vehículo Usado Fuera de Horario'
				SET @lMessage = REPLACE(@lMessage, '%vehicle%', @pPlate)
				SET @lMessage = REPLACE(@lMessage, '%time%', @pTimeLastReport)
				SET @lNextAlarm = DATEADD(HOUR, 1, DATEADD(Hour, -6,GETUTCDATE()))
			END 
	
			IF @pAlarmTriggerId  = 503
			BEGIN 
			SET @lSubject = 'Alarma de Mantenimiento Preventivo'
				SET @lMessage = REPLACE(@lMessage, '%Vehicle%', @pPlate)
				SET @lMessage = REPLACE(@lMessage, '%Description%', @pDescription)
			END 
			
			IF @pAlarmTriggerId = 504			
			BEGIN 
				SET @lSubject = 'Alarma por Vencimiento de Licencia'
				SET @lMessage = REPLACE(@lMessage, '%Driver%', @pName) 
			END 
		
			IF @pAlarmTriggerId = 505
			BEGIN 
				SET @lSubject = 'Alarma Ajuste de Odómetro'
				SET @lMessage = REPLACE(@lMessage, '%Vehicle%', @pPlate)
				SET @lNextAlarm = NULL 
			END 		
			
			INSERT INTO [General].[Emails] ([To], [Subject], [Message], [InsertDate])
			VALUES (@pEmail, @lSubject, @lMessage, DATEADD(Hour, -6,GETUTCDATE()))
			
		
			UPDATE [General].[Alarms]
			SET [NextAlarm] = @lNextAlarm
			WHERE [CustomerId] = @pCustomerId
			  AND [AlarmId] = @pAlarmId
			  AND [RowVersion] = @pRowVersion
			  
			INSERT INTO [General].[AlarmSend] (AlarmId, DateSend, InsertDate, InsertUserId)
			VALUES (@pAlarmId, DATEADD(Hour, -6,GETUTCDATE()), DATEADD(Hour, -6,GETUTCDATE()), 0)					
		END
	END TRY 
	BEGIN CATCH 
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH
END
