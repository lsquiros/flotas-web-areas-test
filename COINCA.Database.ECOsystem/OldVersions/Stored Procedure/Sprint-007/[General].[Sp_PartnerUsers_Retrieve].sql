USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerUsers_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PartnerUsers_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo.
-- Create date: 16/01/2015
-- Description:	Retrieve Customer User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PartnerUsers_Retrieve]
(
	  @pUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT
		  a.[PartnerUserId]
		 ,a.[PartnerId]
		 ,CASE  WHEN d.[Name]='PARTNER_USER' OR d.[Name]='INSURANCE_USER' THEN 'Usuario Regular'
						WHEN d.[Name]='PARTNER_ADMIN' OR d.[Name]='INSURANCE_ADMIN' THEN 'Administrador'
						ELSE '' END AS [RoleName]
		,a.[InsertDate]
		,e.CountryId
		,e.[PartnerGroupId]
    FROM [General].[PartnerUsers] a
		INNER JOIN [General].[Users] b
			ON b.[UserId] = a.[UserId]
		INNER JOIN [dbo].[AspNetUserRoles] c
			ON c.[UserId] = b.[AspNetUserId]
		INNER JOIN [dbo].[AspNetRoles] d
			ON d.[Id] = c.RoleId
		INNER JOIN [General].[Partners] e
			ON e.[PartnerId] = a.[PartnerId]
	WHERE a.[UserId] = @pUserId
	
    SET NOCOUNT OFF
END
