USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PartnerFuel_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_PartnerFuel_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Partner Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PartnerFuel_Retrieve]
(
	 @pPartnerFuelId INT = NULL				--@pPartnerFuelId: PK of the table
	,@pPartnerId INT						--@pPartnerId: FK of Partner Id
	,@pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT
		 a.[PartnerFuelId]
		,a.[FuelId]
		,b.[Name] AS [FuelName]
		,a.[PartnerId]
		,a.[LiterPrice]
		,CONVERT(VARCHAR(20),a.[StartDate],103) AS [StartDate]
		,CONVERT(VARCHAR(20),a.[EndDate],103) AS [EndDate]
		,a.[RowVersion]
    FROM [Control].[PartnerFuel] a
		INNER JOIN [Control].[Fuels] b
			ON a.[FuelId] = b.[FuelId]
	WHERE a.[PartnerId] = @pPartnerId
	  AND (@pPartnerFuelId IS NULL OR a.[PartnerFuelId] = @pPartnerFuelId)
	  AND (@pKey IS NULL 
				OR b.[Name] like '%'+@pKey+'%'
				OR CONVERT(VARCHAR(20),a.[LiterPrice]) like '%'+@pKey+'%')
	ORDER BY ISNULL(a.[EndDate], GETUTCDATE()) DESC
	
    SET NOCOUNT OFF
END
GO