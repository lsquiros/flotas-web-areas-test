USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]
GO

USE [ECOSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Danilo Hidalgo
-- Create date: 16/01/2015
-- Description:	Delete Preventive Maintenance Record By Vehicle Detail
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Delete]
	@pId INT, 
	@pLoggedUserId INT
AS
BEGIN	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
		DECLARE @lPreventiveMaintenanceRecordByVehicleId INT
		DECLARE @lTotalCost DECIMAL(16, 2)
		DECLARE @lTotalRecord DECIMAL(16, 2)

        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		SELECT @lPreventiveMaintenanceRecordByVehicleId = [PreventiveMaintenanceRecordByVehicleId] 
		FROM [General].[PreventiveMaintenanceRecordByVehicleDetail]
		WHERE [PreventiveMaintenanceRecordByVehicleDetailId] = @pId
		
		DELETE [General].[PreventiveMaintenanceRecordByVehicleDetail]
		WHERE [PreventiveMaintenanceRecordByVehicleDetailId] = @pId
		
		
			SELECT @lTotalCost = ISNULL(SUM(Cost), 0) FROM [General].[PreventiveMaintenanceRecordByVehicleDetail] 
			WHERE [PreventiveMaintenanceRecordByVehicleId] = @lPreventiveMaintenanceRecordByVehicleId
			
			SELECT @lTotalRecord = ISNULL(SUM(Record), 0) FROM [General].[PreventiveMaintenanceRecordByVehicleDetail] 
			WHERE [PreventiveMaintenanceRecordByVehicleId] = @lPreventiveMaintenanceRecordByVehicleId
			
			UPDATE [General].[PreventiveMaintenanceRecordByVehicle]
			SET 
				[Cost] = @lTotalCost, 
				[Record] = @lTotalRecord, 
				[ModifyUserId] = @pLoggedUserId,
				[ModifyDateId] = GETUTCDATE()
			WHERE 
				[PreventiveMaintenanceRecordByVehicleId] = @lPreventiveMaintenanceRecordByVehicleId


		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
