USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFences_Retrieve]    Script Date: 01/23/2015 13:26:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFences_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_GeoFences_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFences_Retrieve]    Script Date: 01/23/2015 13:26:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/13/2014
-- Description:	Retrieve GeoFences information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_GeoFences_Retrieve]
(
	 @pGeoFenceId INT = NULL
	,@pKey VARCHAR(800) = NULL
	,@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		co.[Code] CountryCode,
		g.[GeoFenceId]
		,g.[Name]
		,g.[Active]
		,g.[CustomerId]
		,g.[Alarm]
		,g.[AlarmId]
		,g.[Description]
		,g.[Polygon].STAsText() Polygon
		,g.[In]
		,g.[Out]
		,g.[RowVersion]
		,g.[CustomerId]
    FROM [Efficiency].[GeoFences] g
    INNER JOIN [General].[Customers] c ON g.[CustomerId] = c.[CustomerId]
    INNER JOIN [General].[Countries] co ON c.[CountryId] = co.[CountryId]
	WHERE g.[CustomerId] = @pCustomerId
	  AND (@pGeoFenceId IS NULL OR g.[GeofenceId] = @pGeoFenceId)
	  AND (@pKey IS NULL 
				OR UPPER(g.[Name]) like '%'+@pKey+'%'
				OR UPPER(g.[Description]) like '%'+@pKey+'%')
	ORDER BY g.[GeofenceId] DESC
	
    SET NOCOUNT OFF
END



GO


USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFences_AddOrEdit]    Script Date: 01/23/2015 13:24:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFences_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_GeoFences_AddOrEdit]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_GeoFences_AddOrEdit]    Script Date: 01/23/2015 13:24:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/13/2014
-- Description:	Insert or Update GeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_GeoFences_AddOrEdit]
(
	@pGeoFenceId int = NULL			--@pGeoFenceId: PK of the table
	,@pName varchar(50)					--@pName: GeoFence Name
	,@pActive bit						--@pActive:	If GeoFence Active
	,@pCustomerId int					--@pCustomerId: FK of Customer
	,@pAlarm bit = NULL					--@pAlarm: GeoFence has alarm
	,@pAlarmId int = NULL				--@pAlarmId: Id of alarm
	,@pDescription varchar(MAX)			--@pDescription: GeoFence Description
	,@pPolygon varchar(MAX)				--@pPolygon: GeoFence Polygon
	,@pIn bit							--@@pIn: If Geofence generate out alarm
	,@pOut bit							--@@pOut: If Geofence generate in alarm
	,@pLoggedUserId int					--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion timestamp				--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pGeoFenceId IS NULL)
			BEGIN
				INSERT INTO [Efficiency].[GeoFences]
						([Name]
						,[Active]
						,[CustomerId]
						,[Alarm]
						,[AlarmId]
						,[Description]
						,[Polygon]
						,[In]
						,[Out]
						,[InsertDate]
						,[InsertUserId]
						)
				VALUES	(@pName
						,@pActive
						,@pCustomerId
						,@pAlarm
						,@pAlarmId
						,@pDescription
						,(geometry::STGeomFromText(@pPolygon, 0))
						,@pIn
						,@pOut
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Efficiency].[GeoFences]
					SET  
						[CustomerId] = @pCustomerId
						,[Name] = @pName
						,[Active] = @pActive
						,[Alarm] = @pAlarm
						,[AlarmId] = @pAlarmId
						,[Description] = @pDescription
						,[Polygon] = @pPolygon
						,[In] = @pIn
						,[Out] = @pOut
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [GeoFenceId] = @pGeoFenceId
						  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END




GO


