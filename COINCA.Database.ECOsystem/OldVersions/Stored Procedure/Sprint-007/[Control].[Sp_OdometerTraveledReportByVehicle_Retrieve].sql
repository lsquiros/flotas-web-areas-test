USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 02/20/2015
-- Description:	Retrieve Odometer Traveled Report By Vehicle
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pKey VARCHAR(800) = NULL			--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		 a.[VehicleId]
		,b.PlateId
		,MIN(a.[Date]) AS [TrxMinDate]
		,MAX(a.[Date]) AS [TrxMaxDate]
		,MIN(COALESCE(a.FixedOdometer, a.Odometer, 0)) AS [TrxMinOdometer]
		,MAX(COALESCE(a.FixedOdometer, a.Odometer, 0)) AS [TrxMaxOdometer]
		,b.CustomerId
	FROM [Control].[Transactions] a WITH(READUNCOMMITTED)
			INNER JOIN [General].[Vehicles] b
		ON b.[VehicleId] = a.[VehicleId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND a.[IsFloating] = 0
	  AND a.[IsReversed] = 0
	  AND a.[IsDuplicated] = 0
	  AND b.[Active] = 1
	  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,a.[Date]) = @pMonth
								AND DATEPART(yyyy,a.[Date]) = @pYear) OR
									(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND a.[Date] BETWEEN @pStartDate AND @pEndDate))
	  AND (@pKey IS NULL 
				OR b.[PlateId] like '%'+@pKey+'%')
	GROUP BY a.[VehicleId]
			,b.PlateId
			,b.CustomerId
	
	
    SET NOCOUNT OFF
END
