USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleUnits_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleUnits_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleSubUnit information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleUnits_Retrieve]
(
	 @pUnitId INT = NULL						--@pUnitId: PK of the table
	,@pCustomerId INT							--@pCustomerId: FK of the table General.Customers
	,@pKey VARCHAR(800) = NULL					--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[UnitId]
		,a.[Name]
		,a.[RowVersion]
		,c.[Code] [CountryCode]
    FROM [General].[VehicleUnits] a 
		INNER JOIN [General].[Customers] b ON a.[CustomerId] = b.[CustomerId]
		INNER JOIN [General].[Countries] c ON b.[CountryId] = c.[CountryId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pUnitId IS NULL OR [UnitId] = @pUnitId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%')
	ORDER BY [UnitId] DESC
	
    SET NOCOUNT OFF
END
