USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartners_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_NewsByPartners_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes
-- Create date: 12/01/2015
-- Description:	Retrieve Partner News
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_NewsByPartners_Retrieve]
(
	 @pPartnerId INT=null,
	 @pNewsId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT
		a.[NewsId]
		,a.[PartnerId]
		,a.[Title]
		,a.[Content]
		,a.[Image]
		,a.[IsActive] 
		,a.[RowVersion]
    FROM [General].[NewsByPartner] a		
	WHERE (@pNewsId IS NULL OR [NewsId] = @pNewsId)
	  AND (@pKey IS NULL 
				OR a.[Title] like '%'+@pKey+'%'
				OR a.[Content] like '%'+@pKey+'%')
	  AND a.PartnerId = isnull(@pPartnerId,a.PartnerId)
	ORDER BY [NewsId] DESC
	
    SET NOCOUNT OFF
END
