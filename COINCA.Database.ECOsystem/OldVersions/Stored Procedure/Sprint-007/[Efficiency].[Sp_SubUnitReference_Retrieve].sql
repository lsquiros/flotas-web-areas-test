USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_SubUnitReference_Retrieve]    Script Date: 11/06/2014 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_SubUnitReference_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_SubUnitReference_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_SubUnitReference_Retrieve]    Script Date: 11/06/2014 10:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/03/2014
-- Description:	Retrieve IntrackReference information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_SubUnitReference_Retrieve]
(
	@pId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		s.[CostCenterId] [SubUnitId], 
		co.[Code] [CountryCode],
		c.[CustomerId] [CustomerId]
	FROM 
		[General].[VehicleCostCenters] s
		INNER JOIN [General].[VehicleUnits] u ON s.[UnitId] = u.[UnitId]
		INNER JOIN [General].[Customers] c on u.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Countries] co on c.[CountryId] = co.[CountryId]
	WHERE s.[CostCenterId] = @pId
		
    SET NOCOUNT OFF
END

GO
