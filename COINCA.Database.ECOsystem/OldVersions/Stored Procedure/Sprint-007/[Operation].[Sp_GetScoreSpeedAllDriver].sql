USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetScoreSpeedAllDriver]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_GetScoreSpeedAllDriver]
GO

/****** Object:  StoredProcedure [Operation].[Sp_GetScoreSpeedAllDriver]    Script Date: 7/28/2015 10:57:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Modified by:		Andr�s Oviedo
-- Modified date:	16/02/2015 

CREATE PROCEDURE [Operation].[Sp_GetScoreSpeedAllDriver] 
( @pCustomerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pScoreType char(1),			--A- Reporte Administrativo  , R- Reporte de  carretera (Route)  
  @pInvocationType char(1)		--J- Invocaci�n desde el Job, R-Invocaci�n desde un reporte 
)
AS
BEGIN
	--Declare @pStartDate datetime =  '20141019 03:45:02.000',
	--@pEndDate datetime = '20141128 11:30am'

	--Set Dates 
	If @pEndDate is null
		If @pStartDate is null
		begin
			If @pMonth is null or @pYear is null
			begin
				RAISERROR ('100', 16, 1)
				RETURN
			end
			else
			begin
				If @pInvocationType = 'J'
				begin
					--Para la invocaci�n de un mes completo desde el Job, ejecuta el c�lculo
					Set @pStartDate = [General].[Fn_GetFirstDayOfMonth](@pYear, @pMonth)
					Set @pEndDate = DATEADD(MM, 1, @pStartDate)
				end
				else
				begin
					--Para la invocaci�n por mes desde un reporte, recupera las calificaciones precalculadas
					Select 0 DriverScoreId, DS.UserId, U.Name as EncryptedName, D.Identification as EncryptedIdentification, DS.Score,
					DS.[KmTraveled] ,
					DS.[OverSpeed],
					DS.[OverSpeedAmount],
					DS.[OverSpeedDistancePercentage],
					DS.[OverSpeedAverage],
					DS.[OverSpeedWeihgtedAverage],
					DS.[StandardDeviation],
					DS.[Variance],
					DS.[VariationCoefficient],
					DS.OverSpeedDistance 				
					  from Operation.DriversScores DS
					  inner join General.Users U on DS.UserId = U.UserId
					  inner join General.DriversUsers D on DS.UserId = D.UserId
					 where DS.CustomerId = @pCustomerId
					   and DS.ScoreYear = @pYear
					   and DS.ScoreMonth = @pMonth
					   and DS.ScoreType = Case When @pScoreType = 'A' then 'SPADM' else 'SPROU' end
					 order by DS.Score
					   
					RETURN
				end
			end
		end
		else
		begin
			Set @pEndDate = DATEADD(DD, 1, @pStartDate)
		end
	
	IF(@pInvocationType = 'R')
	BEGIN
			Select 0 DriverScoreId, DS.UserId, U.Name as EncryptedName, D.Identification as EncryptedIdentification, 
					SUM(DS.Score) / COUNT(*) AS Score,
					SUM(DS.[KmTraveled]),
					SUM(DS.[OverSpeed]),
					SUM(DS.[OverSpeedAmount]),
					SUM(DS.[OverSpeedDistancePercentage]),
					SUM(DS.[OverSpeedAverage]),
					SUM(DS.[OverSpeedWeihgtedAverage]),
					SUM(DS.[StandardDeviation]),
					SUM(DS.[Variance]),
					SUM(DS.[VariationCoefficient]),
					SUM(DS.OverSpeedDistance) 				
					  from Operation.DriversScoresDaily DS
					  inner join General.Users U on DS.UserId = U.UserId
					  inner join General.DriversUsers D on DS.UserId = D.UserId
					 where DS.CustomerId = @pCustomerId
					   and DS.ScoreType = Case When @pScoreType = 'A' then 'SPADM' else 'SPROU' end
					   and DS.Date_Time >= @pStartDate and DS.Date_Time < @pEndDate
					 group by DS.UserId, U.Name, D.Identification
					 order by SUM(DS.Score) / COUNT(*)
			RETURN 
	END
	ELSE
	BEGIN

			--DECLARE @MyTable  dbo.TypeDeviceSpeed
			Declare @VehiclesByUser table
			(Device int,
			 Vehicle int,
			 StartOdometer int,
			 EndOdometer int,
			 StartDate datetime,
			 EndDate datetime)

			Create table #LocalDriverScore
			(DriverScoreId INT Identity(1,1),
			 UserId int,
			 Name  varchar (250),
			 Identification varchar(50),
			 Score Float,
			 KmTraveled float,
			 OverSpeed float,
			 OverSpeedAmount float,
			 OverSpeedDistancePercentage float,
			 OverSpeedAverage float,
			 OverSpeedWeihgtedAverage float,
			 StandardDeviation float,
			 Variance float,
			 VariationCoefficient float,
			 OverSpeedDistance float
			 )
	 
			Declare @UserId Int
			Declare @Name varchar(250),
					@Identification varchar(50)
			Declare  @Score Float

			Select @UserId = Min(U.UserId) 
			  from General.Users U
			  inner join General.DriversUsers D on U.UserId = D.UserId
			 Where U.IsActive =  1 
			   and D.CustomerId = @pCustomerId

			--Begin transaction
	 
			While  @UserId  is not null
			BEGIN
				Insert into @VehiclesByUser
					(Device, Vehicle, StartOdometer, EndOdometer, StartDate, EndDate)  
				--Select   V.IntrackReference As Device,CASE WHEN  VU.InitialOdometer IS NULL THEN 0 eLSE VU.InitialOdometer end  ,CASE WHEN VU.EndOdometer IS NULL THEN 0 eLSE EndOdometer end    , VU.InsertDate StartDate ,VU.LastDateDriving EndDate
				Select V.DeviceReference, V.IntrackReference, IsNull(VU.InitialOdometer, 0), IsNull(VU.EndOdometer,0), VU.InsertDate, IsNull(VU.LastDateDriving, GETDATE())
				from General.VehiclesByUser VU 
				inner Join General.Vehicles V ON  V.VehicleId = VU.VehicleId
				inner JOIN General.Users U  ON U.UserId = VU.UserId
				Where U.UserId =  @UserId 
				  and V.CustomerId = @pCustomerId
				  And V.DeviceReference is not null
				  and ((VU.LastDateDriving is null) or (@pEndDate<=ISNULL(VU.LastDateDriving,@pEndDate) and @pStartDate>=ISNULL(VU.InsertDate,@pStartDate)))
				order by V.DeviceReference, VU.InsertDate, VU.LastDateDriving

				--SELECT * from @VehiclesByUser
				if (select COUNT(*) FROM @VehiclesByUser ) > 0 
				BEGIN 
					DECLARE @VehiclesByUserString varchar(max);
					SET @VehiclesByUserString = (SElect * FROm  @VehiclesByUser AS Lista FOR XML AUTO)
					--SELECT  @VehiclesByUserString   
					declare @ScoreOutput nvarchar(max)
			
					DECLARE @SQL NVARCHAR(max)
				
						set @SQL=(SELECT ScoreDrivingFormulaSP FROM Insurance.ScoreDrivingFormula WHERE CustomerId=@pCustomerId AND IsActive=1)
						if @SQL is null
						begin
							set @SQL='[dbo].[Sp_GetScoreDriver]';
						end
					EXECUTE  @SQL @pStartDate, @pEndDate, @VehiclesByUserString, @pScoreType, @pScoreTable = @ScoreOutput OUTPUT
			
					DECLARE @pScoreOutputXML XML;
						SET  @pScoreOutputXML = CAST(@ScoreOutput  AS XML);
				
						DECLARE @ScoreTable dbo.TypeDriverScore;
				
						Insert into  @ScoreTable
						SELECT --DeviceSpeedId   = T.Item.value('@DeviceSpeedId ', 'int'),
							   KmTraveled = T.Item.value('@KmTraveled', 'float'),
							   OverSpeed = T.Item.value('@OverSpeed', 'float'),
							   OverSpeedAmount  = T.Item.value('@OverSpeedAmount',  'float'),
							   OverSpeedDistancePercentage = T.Item.value('@OverSpeedDistancePercentage', 'float'),
							   OverSpeedAverage = T.Item.value('@OverSpeedAverage', 'float'),
							   OverSpeedWeihgtedAverage = T.Item.value('@OverSpeedWeihgtedAverage', 'float'),
							   StandardDeviation = T.Item.value('@StandardDeviation', 'float'),
							   Variance = T.Item.value('@Variance', 'float'),
							   VariationCoefficient = T.Item.value('@VariationCoefficient', 'float'),
							   AverageScore = T.Item.value('@AverageScore', 'float'),
							   OverSpeedDistance= T.Item.value('@OverSpeedDistance', 'float')
						FROM   @pScoreOutputXML.nodes('Lista') AS T(Item)
				
						set @Score=(select AverageScore from @ScoreTable)
				
			
					--Select @Score
					If @Score is not null
					begin
						--Obtener nombre e identificaci�n del usuario
						Select @Name = U.Name,
							   @Identification = D.Identification 
						  from General.Users U
						  inner join General.DriversUsers D on U.UserId = D.UserId
						where U.UserId = @UserId
				 
						Insert into #LocalDriverScore (UserID, Name, Identification, Score,KmTraveled,
								OverSpeed ,
								OverSpeedAmount ,
								OverSpeedDistancePercentage ,
								OverSpeedAverage ,
								OverSpeedWeihgtedAverage ,
								StandardDeviation ,
								Variance ,
								VariationCoefficient,
								OverSpeedDistance)
								Values(@UserId, @Name, @Identification, @Score,(select KmTraveled from @ScoreTable),
								(select OverSpeed from @ScoreTable),(select OverSpeedAmount from @ScoreTable),(select OverSpeedDistancePercentage from @ScoreTable),
								(select OverSpeedAverage from @ScoreTable),(select OverSpeedWeihgtedAverage from @ScoreTable),(select StandardDeviation from @ScoreTable),
								(select Variance from @ScoreTable),(select VariationCoefficient from @ScoreTable),(select OverSpeedDistance from @ScoreTable))
				
						--Select * FROM #LocalDriverScore
						----Limpia la  tabla para  la nueva seleccion.
					end
			
					delete from @ScoreTable
					DELETE FROM @VehiclesByUser
				END
				--- Siguiente********************
				Select @UserId = Min(U.UserId) 
				  from General.Users U
				  inner join General.DriversUsers D on U.UserId = D.UserId
				 Where U.IsActive =  1 
				   and D.CustomerId = @pCustomerId
				   and U.UserId > @UserId
			END
	
			--Identification
			Select DriverScoreId , UserId , Name   , Identification, Score,KmTraveled,
								OverSpeed ,
								OverSpeedAmount ,
								OverSpeedDistancePercentage ,
								OverSpeedAverage ,
								OverSpeedWeihgtedAverage ,
								StandardDeviation ,
								Variance ,
								VariationCoefficient,OverSpeedDistance  FROM #LocalDriverScore
			order by Score asc
	
			Drop table #LocalDriverScore

	END -- END IF @pInvocationType
END



GO


