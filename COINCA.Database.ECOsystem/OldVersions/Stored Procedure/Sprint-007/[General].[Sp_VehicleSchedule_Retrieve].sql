USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleSchedule_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleSchedule_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleSchedule information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleSchedule_Retrieve]
(
	 @pVehicleId INT						--@pVehicleScheduleId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	SELECT
		 a.[VehicleId]
		,a.[JsonSchedule]
		,a.[RowVersion]
	FROM [General].[VehicleSchedule] a
	WHERE a.[VehicleId] = @pVehicleId	 
	
    SET NOCOUNT OFF
END
