USE [ECOSystem]
USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AlarmByOdometer_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_AlarmByOdometer_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Mart�nez H.
-- Create date: 21/Oct/2014
-- Description:	Insert or Update Alarm by odometer information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_AlarmByOdometer_AddOrEdit]
(
	 @pAlarmOdometerId INT = NULL
	,@pVehicleId INT
	,@pDate DATETIME 
	,@pExpectedOdometer INT
	,@pReportedOdometer INT
	,@pFixed BIT
	,@pLoggedUserId INT 
	,@pRowVersion TIMESTAMP = NULL
)
AS
BEGIN 
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0  
            DECLARE @lPlate VARCHAR(10)  
            DECLARE @lAlarmId INT
			DECLARE @lAlarmTriggerId INT
			DECLARE @lPhone VARCHAR(100)
			DECLARE @lEmail VARCHAR(500)
			DECLARE @lCustomerId INT  
			DECLARE @lRowVersion TIMESTAMP      
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pAlarmOdometerId IS NULL)
			BEGIN
				INSERT INTO [Control].[AlarmByOdometer]
						([VehicleID]
						,[Date]
						,[ExpectedOdometer]
						,[ReportedOdometer]
						,[Fixed]
						,[InsertUserId]
						,[InsertDate])
				VALUES (@pVehicleId
					   ,@pDate
					   ,@pExpectedOdometer
					   ,@pReportedOdometer
					   ,@pFixed
					   ,@pLoggedUserId
					   ,GETUTCDATE())
			
				SET @lRowCount = @@ROWCOUNT
					   
				SELECT @lPlate = [PlateId]
				FROM [General].[Vehicles]
				WHERE [VehicleId] = @pVehicleId
					   
				SET @pAlarmOdometerId = SCOPE_IDENTITY()
				
				SELECT @lAlarmTriggerId = a.[TypeId]
				FROM [General].[Types] a
				WHERE a.[Code] = 'CUSTOMER_ALARM_TRIGGER_ODOMETER'
				
				IF @lAlarmTriggerId IS NOT NULL
				BEGIN 
					SELECT TOP 1 @lAlarmId = a.[AlarmId],
					             @lPhone = a.[Phone], 
					             @lEmail = a.[Email], 
					             @lCustomerId = a.[CustomerId], 
					             @lRowVersion = a.[RowVersion]
					FROM [General].[Alarms] a
					WHERE a.[AlarmTriggerId] = @lAlarmTriggerId
					
					IF @lAlarmId IS NOT NULL 
					BEGIN 
						EXEC [General].[Sp_SendAlarm] @pAlarmId = @lAlarmId,
													  @pAlarmTriggerId = @lAlarmTriggerId,
									                  @pCustomerId = @lCustomerId,
									                  @pPlate = @lPlate,
									                  @pPhone = @lPhone, 
									                  @pEmail = @lEmail,
									                  @pRowVersion = @lRowVersion	
					END 
				END
			END 
			ELSE
			BEGIN 
				UPDATE [Control].[AlarmByOdometer]
					SET [VehicleId] = @pVehicleId
					   ,[Date] = @pDate
					   ,[ExpectedOdometer] = @pExpectedOdometer
					   ,[ReportedOdometer] = @pReportedOdometer
					   ,[Fixed] = @pFixed
					   ,[ModifyUserId] = @pLoggedUserId
					   ,[ModifyDate] = GETUTCDATE()
				WHERE [AlarmOdometerId] = @pAlarmOdometerId
				--AND [RowVersion] = @pRowVersion
				
				SET @lRowCount = @@ROWCOUNT
			END			
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO