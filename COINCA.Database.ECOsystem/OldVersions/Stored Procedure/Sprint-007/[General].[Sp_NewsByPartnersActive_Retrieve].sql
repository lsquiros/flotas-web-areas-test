USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartnersActive_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_NewsByPartnersActive_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes
-- Create date: 12/01/2014
-- Description:	Retrieve All Active Partner News
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_NewsByPartnersActive_Retrieve]
(
	 @pPartnerId INT = NULL,
	 @pActive INT = 1
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT
		a.[NewsId]
		,a.[PartnerId]
		,a.[Title]
		,a.[Content]
		,a.[Image]
		,a.[IsActive]
    FROM [General].[NewsByPartner] a		
	WHERE a.PartnerId = isnull(@pPartnerId,a.PartnerId) AND a.IsActive = @pActive
	ORDER BY [NewsId] DESC
	
    SET NOCOUNT OFF
END
