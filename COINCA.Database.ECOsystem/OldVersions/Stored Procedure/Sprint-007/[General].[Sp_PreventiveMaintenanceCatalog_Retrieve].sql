USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalog_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 06/Oct/2014
-- Description:	Preventive Maintenance information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_Retrieve]
	  @pCustomerId INT								--@pCustomerID: Customer ID PK from table Customer
	 ,@pPreventiveMaintenanceCatalogId INT = NULL	--
	 ,@pKey VARCHAR(250) = NULL
AS
BEGIN	
	SET NOCOUNT ON;
	SELECT 
		a.[PreventiveMaintenanceCatalogId],
		a.[Description],
		a.[FrequencyKm],
		a.[AlertBeforeKm],
		a.[FrequencyMonth],
		a.[AlertBeforeMonth],
		a.[FrequencyDate],
		a.[AlertBeforeDate],
		a.[Cost],
		a.[RowVersion],
		c.Symbol as [CurrencySymbol]
		FROM [General].[PreventiveMaintenanceCatalog] a
		
		INNER JOIN [General].[Customers] ct
		ON ct.CustomerId = a.CustomerId
		INNER JOIN [Control].[Currencies] c
		ON c.CurrencyId = ct.CurrencyId
		
		WHERE  a.[CustomerId] = @pCustomerId
				AND (@pPreventiveMaintenanceCatalogId IS NULL 
					OR a.[PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId)
				AND (@pKey IS NULL 
					OR a.[Description] LIKE '%'+@pKey+'%')				
	SET NOCOUNT OFF;
END
