USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerCredits_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CustomerCredits_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CustomerCredit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CustomerCredits_Retrieve]
(
	  @pCustomerId INT						--@pCustomerId: Customer Id
	 ,@pYear INT							--@pYear: Year
	 ,@pMonth INT							--@pMonth: Month
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
		 a.[CustomerCreditId]
		,a.[CustomerId]
		,a.[Year]
		,a.[Month]
		,a.[CreditAmount]
		,a.[CreditAssigned]
		,a.[CreditAvailable]
		,a.[CreditTypeId]
		,@lCurrencySymbol AS [CurrencySymbol]
		,a.[RowVersion]
    FROM [Control].[CustomerCredits] a
	WHERE a.[CustomerId] = @pCustomerId
	  AND a.[Year] = @pYear
	  AND a.[Month] = @pMonth
	
    SET NOCOUNT OFF
END
