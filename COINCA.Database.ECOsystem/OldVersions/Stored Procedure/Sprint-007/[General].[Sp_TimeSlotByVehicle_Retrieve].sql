USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_TimeSlotByVehicle_Retrieve]    Script Date: 12/04/2014 15:50:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_TimeSlotByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_TimeSlotByVehicle_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_TimeSlotByVehicle_Retrieve]    Script Date: 12/04/2014 15:50:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 12/04/2014
-- Description:	Retrieve VehicleTimeSlot information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_TimeSlotByVehicle_Retrieve]
(
	 @pVehicleId INT						--@pTimeSlotByVehicleId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	SELECT
		 a.[VehicleId]
		,a.[JsonTimeSlot]
		,a.[RowVersion]
	FROM [General].[TimeSlotByVehicle] a
	WHERE a.[VehicleId] = @pVehicleId	 
	
    SET NOCOUNT OFF
END

GO



SELECT * FROM [General].[TimeSlotByVehicle]