USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ClientVisitAddOrUpdate]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ClientVisitAddOrUpdate]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 06/07/2015
-- Description:	Insert or Update Visit Client information
-- ================================================================================================
CREATE PROCEDURE  [General].[Sp_ClientVisitAddOrUpdate]
(
	 @pUserId INT = NULL
	,@pRouteId INT = NULL
	,@pPointId INT = NULL
	,@pVisitId INT = NULL
	,@pName VARCHAR(300) = NULL
	,@pContact VARCHAR(300) = NULL
	,@pAddress VARCHAR(500) = NULL
	,@pPhone VARCHAR(50) = NULL
	,@pState INT = NULL
	,@pReason VARCHAR(150)
	,@pLatitude FLOAT = NULL
	,@pLongitude FLOAT = NULL
	,@pTime VARCHAR(50) = NULL
	,@pOnlyState BIT = 0
	,@pIsVisit BIT = 0
	,@pLoggedUserId INT = 0
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF ((@pVisitId IS NULL OR @pVisitId = 0) AND @pOnlyState = 0)
			BEGIN
				
				DECLARE @Order INT = 0

				-- GET THE ORDER VALUE
				SELECT @Order = COUNT(*)
				FROM [Efficiency].[RoutesPoints]
				WHERE [RouteId] = @pRouteId


				-- INSERT ROUTE POINT
				INSERT INTO [Efficiency].[RoutesPoints]
						([RouteId]
						,[PointReference]
						,[Order]
						,[Name]
						,[Time]
						,[Latitude]
						,[Longitude]
						,[InsertDate]
						,[ModifyDate])
					VALUES
						(@pRouteId
						,@Order
						,@Order
						,@pName
						,@pTime
						,@pLatitude
						,@pLongitude
						,GETUTCDATE()
						,GETUTCDATE())
				
				-- OBTENEMOS EL IDENTITY GENERADO EN EL ANTERIOR INSERT
				SELECT @pPointId = SCOPE_IDENTITY()


				-- VALIDAMOS SI HAY UN IDENTITY E INSERTAMOS EN LA TABLA CLIENT VISIT
				IF(@pPointId IS NOT NULL AND @pPointId > 0)
				BEGIN

					INSERT INTO [General].[ClientVisit]
							(
							 [UserId]
							,[RoutePointId]
							,[Name] 
							,[Contact]
							,[Address]
							,[Phone]
							,[State]
							,[Reason]
							,[IsVisit]
							,[InsertDate]
							,[InsertUserId])
					VALUES	(
							 @pUserId
							,@pPointId
							,@pName
							,@pContact
							,@pAddress
							,@pPhone
							,@pState
							,@pReason
							,@pIsVisit
							,GETUTCDATE()
							,@pLoggedUserId)
				END				
			END
			ELSE
			BEGIN

				IF(@pOnlyState = 0) 
				BEGIN
					
					-- ACTUALIZAMOS EL TIME DE LA TABLA [RoutesPoints]
					DECLARE @CurrentPoint int = 0

					SELECT @CurrentPoint = [RoutePointId]
					FROM [General].[ClientVisit]
					WHERE [VisitId] = @pVisitId

					UPDATE [Efficiency].[RoutesPoints]
						SET	[Time] = @pTime
						WHERE [PointId] = @CurrentPoint
					-- ///////////////////////////////////////////////

					-- ACTUALIZAMOS LOS DATOS DE LA VISITA
					UPDATE [General].[ClientVisit]
						SET	 [Name] = @pName
							,[Contact] = @pContact
							,[Address] = @pAddress
							,[Phone] = @pPhone
							,[State] = @pState
							,[IsVisit] = @pIsVisit
							,[ModifyDate] = GETUTCDATE()
							,[ModifyUserId] = @pLoggedUserId
						WHERE [VisitId] = @pVisitId
					-- ///////////////////////////////////////////////

				END
				ELSE
				BEGIN

					-- ACTUALIZAMOS EL ESTADO DE LA VISITA
					UPDATE [General].[ClientVisit]
						SET	 [State] = @pState
							,[Reason] = @pReason
							,[IsVisit] = @pIsVisit
							,[ModifyDate] = GETUTCDATE()
							,[ModifyUserId] = @pLoggedUserId
						WHERE [VisitId] = @pVisitId
					

					-- ACTUALIZAMOS LAS COORDENADAS DEL PUNTO
					DECLARE @tempPointId int

					SELECT @tempPointId = [RoutePointId]
					FROM [General].[ClientVisit]
					WHERE [VisitId] = @pVisitId

					IF(@tempPointId IS NOT NULL)
					BEGIN
						UPDATE [Efficiency].[RoutesPoints]
						SET [Latitude] = @pLatitude
							,[Longitude] = @pLongitude
						WHERE [PointId] = @tempPointId
					END
					--////////////////////////////////////////
				END

			END

            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO