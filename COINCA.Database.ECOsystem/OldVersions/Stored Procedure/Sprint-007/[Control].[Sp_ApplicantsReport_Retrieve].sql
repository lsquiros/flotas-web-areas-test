USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_ApplicantsReport_Retrieve]    Script Date: 12/10/2014 10:44:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ApplicantsReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_ApplicantsReport_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_ApplicantsReport_Retrieve]    Script Date: 12/10/2014 10:44:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 12/11/2014
-- Description:	Retrieve TransactionsReport information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_ApplicantsReport_Retrieve]
(
	@pUserId INT = NULL,						--@pUserId: InsrtUserId
	@pCountryId INT = NULL,					--@pCountryId: CountryId
	@pYear INT = NULL,					--@pYear: Year
	@pMonth INT = NULL,					--@pMonth: Month
	@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
	@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @pIssueForId INT

		SELECT 
			c.[Name] [CustomerName], 
			CONVERT(date, cc.[InsertDate]) Date, 
			CASE WHEN c.[CreditCardType] = 'C' THEN 'Cr�dito' ELSE 'D�bito' END [CreditCardType], 
			cu.[Name] [CurrencyName], 
			CASE WHEN c.[IssueForId] = 101 THEN 'Conductor' ELSE 'Veh�culo' END [FleetCard], 
			COUNT(*) [Quantity], 
			co.[Name] [CountryName]
		FROM 
			[Control].[CardRequest] cc
			INNER JOIN [General].[Customers] c ON cc.[CustomerId] = c.[CustomerId]
			INNER JOIN [General].[Countries] co ON c.[CountryId] = co.[CountryId] 
			INNER JOIN [Control].[Currencies] cu ON c.[CurrencyId] = cu.[CurrencyId] 
		WHERE 
			cc.[InsertUserId] = @pUserId AND 
			co.[CountryId] = @pCountryId AND 
			((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
				AND DATEPART(m,c.[InsertDate]) = @pMonth
				AND DATEPART(yyyy,c.[InsertDate]) = @pYear) OR
				(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
				AND c.[InsertDate] BETWEEN @pStartDate AND @pEndDate))
		GROUP BY
			cc.[CustomerId], 
			c.[Name], 
			CONVERT(date, cc.[InsertDate]), 
			c.[CreditCardType], 
			cu.[Name], 
			c.[IssueForId], 
			co.[Name]
		ORDER BY
			c.[Name]
	
    SET NOCOUNT OFF
END


GO

