USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreTypes_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_ScoreTypes_Retrieve]
GO

USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Operation].[Sp_ScoreTypes_Retrieve]    Script Date: 12/01/2014 10:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/01/2014
-- Description:	Retrieve Sp_Score Types information
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_ScoreTypes_Retrieve]
(
	 @pScoreTypeId INT = NULL,				--@pScoreTypeId: PK of the table
	 @pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[ScoreTypeId]
		,a.[Code]
		,a.[Name]
		,a.[RowVersion]
    FROM [Operation].[ScoreTypes] a
	WHERE (@pScoreTypeId IS NULL OR a.[ScoreTypeId] = @pScoreTypeId)	  
	  AND (@pKey IS NULL 
				OR a.[Code] like '%'+@pKey+'%'
				OR a.[Name] like '%'+@pKey+'%')
	ORDER BY a.[Code] ASC
	
    SET NOCOUNT OFF
END
