USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCard_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CreditCard information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCard_Retrieve]
(
	 @pCustomerId INT						--@pCustomerId: Customer Id
	,@pCreditCardId INT = NULL				--@pCreditCardId:Credit Card Id, PK of the table
	,@pKey VARCHAR(800) = NULL				--@pKey: Key to perform search operations
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		 a.[CreditCardId]
		,a.[CustomerId]
		,c.[Name] AS [EncryptedCustomerName]
		,a.[CreditCardNumber]
		,a.[ExpirationYear]
		,a.[ExpirationMonth]
		,a.[CreditLimit]
		,a.[CreditAvailable]
		,a.[CreditExtra]
		,a.[StatusId]
		,b.[RowOrder] AS [Step]
		,b.[Name] AS [StatusName]
		,d.[Symbol] AS [CurrencySymbol]
		,c.[IssueForId]		
		,e.[UserId]
		,g.[VehicleId]
		,c.[CreditCardType]
		,f.Name AS [EncryptedDriverName]
		,'Veh�culo: ' + h.[PlateId] AS [VehiclePlate]
		,a.CardRequestId
		,i.[EstimatedDelivery]
		,a.[RowVersion]
    FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [Control].[CreditCardByVehicle] g
			ON g.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] = g.[VehicleId]
		INNER JOIN [Control].[CardRequest] i
			ON a.[CardRequestId] = i.[CardRequestId]
	WHERE ((@pCustomerId = 0 AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')
				OR a.[CustomerId] = @pCustomerId)
	  AND (@pCreditCardId IS NULL OR a.[CreditCardId] = @pCreditCardId)
	  AND (@pKey IS NULL
				OR f.[Name] like '%'+@pKey+'%'
				OR h.[PlateId] like '%'+@pKey+'%'
				OR Convert(varchar(50),a.[CreditLimit]) like '%'+@pKey+'%'
				OR Convert(varchar(50),a.[CreditAvailable]) like '%'+@pKey+'%'
				OR Convert(varchar(50),a.[ExpirationYear]) like '%'+@pKey+'%'
				)
	  AND a.[StatusId] <> 9 -- filter the closed cards
	ORDER BY [CreditCardId] DESC
	
    SET NOCOUNT OFF
END
