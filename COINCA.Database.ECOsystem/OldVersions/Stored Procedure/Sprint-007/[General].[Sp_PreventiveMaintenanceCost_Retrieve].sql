USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCost_Retrieve]    Script Date: 01/05/2015 08:47:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCost_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCost_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCost_Retrieve]    Script Date: 01/05/2015 08:47:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Danilo Hidalgo Gonz�lez.
-- Create date: 12/31/2014
-- Description:	Preventive Maintenance Cost information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCost_Retrieve]
	 @pPreventiveMaintenanceCatalogId INT
AS
BEGIN	
	SET NOCOUNT ON;

		SELECT 
			co.[PreventiveMaintenanceCostId], 
			co.[PreventiveMaintenanceCatalogId], 
			co.[Description], 
			co.[Cost], 
			cur.[Symbol]
		FROM 
			[General].[PreventiveMaintenanceCost] co
			INNER JOIN [General].[PreventiveMaintenanceCatalog] ca ON co.[PreventiveMaintenanceCatalogId] = ca.[PreventiveMaintenanceCatalogId]
			INNER JOIN [General].[Customers] cu ON ca.[CustomerId] = cu.[CustomerId]
			INNER JOIN [Control].[Currencies] cur ON cu.[CurrencyId] = cur.[CurrencyId]
		WHERE 
			co.[PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId
		ORDER BY
			co.[Description]


	SET NOCOUNT OFF;
END

GO


