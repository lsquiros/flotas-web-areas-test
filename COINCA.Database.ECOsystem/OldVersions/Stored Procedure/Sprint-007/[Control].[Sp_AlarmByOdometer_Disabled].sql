USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AlarmByOdometer_Disabled]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_AlarmByOdometer_Disabled]
GO

USE [ECOSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Martinez H.
-- Create date: 23/Oct/2014
-- Description:	Disable Odometer Alert
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_AlarmByOdometer_Disabled]
(
	 @pAlarmOdometerId INT
	,@pVehicleId INT
	,@pNewOdometer INT 
) 
AS 
BEGIN		
	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lTransactionId INT
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			/*The original transaction must be recorded into table AlarmByOdometer to avoid this horrible process developed by cmartinez*/
			SELECT 
				TOP 1 @lTransactionId =  a.[TransactionId] 
			FROM [Control].[Transactions] a
			 INNER JOIN [Control].[AlarmByOdometer] b
				ON a.VehicleId = b.VehicleId 
			WHERE a.[VehicleId] = @pVehicleId
			  AND a.Odometer = b.ReportedOdometer
			  AND b.AlarmOdometerId = @pAlarmOdometerId
			ORDER BY [TransactionId] DESC
			
			IF (@lTransactionId IS NOT NULL)
			BEGIN 
				UPDATE [Control].[AlarmByOdometer]
				SET    [Fixed] = 1
				WHERE  [AlarmOdometerID] = @pAlarmOdometerId
				
				UPDATE [Control].[Transactions]
				SET    [FixedOdometer] = @pNewOdometer
				WHERE  [TransactionId] = @lTransactionId					
			END 
			
			SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
			
				RAISERROR ('Error updating the original transaction it doesn''t exists!', 16, 1)
			END			
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

