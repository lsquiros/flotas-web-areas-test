USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsReport_Retrieve]    Script Date: 12/10/2014 10:44:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_TransactionsReport_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsReport_Retrieve]    Script Date: 12/10/2014 10:44:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 05/12/2014
-- Description:	Retrieve TransactionsReport information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionsReport_Retrieve]
(
	@pCustomerId INT,					--@pCustomerId: CustomerId
	@pStatus INT=null,						--@pStatus: Status
	@pKey VARCHAR(800) = NULL,			--@pKey :Key
	@pYear INT = NULL,					--@pYear: Year
	@pMonth INT = NULL,					--@pMonth: Month
	@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
	@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @pIssueForId INT
		
	SET @pIssueForId = (SELECT [IssueForId] FROM [General].[Customers] WHERE [CustomerId] = @pCustomerId)

	IF @pIssueForId  = 100
		BEGIN
			--DRIVER
			SELECT 
				t.[TransactionId] [TransactionId], 
				u.[Name] [HolderName], 
				t.[Date] [Date], 
				f.[Name] [FuelName],
				t.[FuelAmount] [FuelAmount], 
				t.[Odometer] [Odometer], 
				t.[Liters] [Liters], 
				v.[PlateId] [PlateId],
				g.[Symbol] [CurrencySymbol]
			FROM 
				[Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v 
					ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c 
					ON v.[CustomerId] = c.[CustomerId]
				INNER JOIN [Control].[Fuels] f 
					ON t.[FuelId] = f.[FuelId]
				INNER JOIN [Control].[CreditCardByDriver] cd 
					ON t.[CreditCardId] = cd.[CreditCardId]
				INNER JOIN [General].[Users] u 
					ON cd.[UserId] = u.[UserId]
				INNER JOIN [Control].[Currencies] g 
					ON c.[CurrencyId] = g.[CurrencyId]
			WHERE 
				c.[CustomerId] = @pCustomerId AND 
				(@pStatus is null OR (ISNULL(t.[IsFloating], 0) = case WHEN @pStatus = 2 THEN 1 ELSE 0 END AND --Floating
				ISNULL(t.[IsReversed], 0) = CASE WHEN @pStatus = 3 THEN 1 ELSE 0 END AND --Reversed
				ISNULL(t.[IsDuplicated], 0) = CASE WHEN @pStatus = 4 THEN 1 ELSE 0 END AND --Duplicated
				ISNULL(t.[IsAdjustment], 0) = CASE WHEN @pStatus = 5 THEN 1 ELSE 0 END))  --Adjustment
				AND (@pKey IS NULL 
					OR u.[Name] like '%'+@pKey+'%'
					OR v.[PlateId] like '%'+@pKey+'%')
			    AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,t.[Date]) = @pMonth
						AND DATEPART(yyyy,t.[Date]) = @pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND t.[Date] BETWEEN @pStartDate AND @pEndDate))

			ORDER BY t.[Date]
		END
	ELSE
		BEGIN
			--VEHICLE
			SELECT 
				c.[CustomerId] [CustomerId],
				t.[CreditCardId] [CreditCardId],
				v.[PlateId] [HolderName], 
				t.[Date] [Date], 
				f.[Name] [FuelName],
				t.[FuelAmount] [FuelAmount], 
				t.[Odometer] [Odometer], 
				t.[Liters] [Liters], 
				v.[PlateId] [PlateId],
				g.[Symbol] [CurrencySymbol]
			FROM 
				[Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v 
					ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c 
					ON v.[CustomerId] = c.[CustomerId]
				INNER JOIN [Control].[Fuels] f 
					ON t.[FuelId] = f.[FuelId]
				INNER JOIN [Control].[CreditCardByVehicle] cv 
					ON t.[CreditCardId] = cv.[CreditCardId]
				INNER JOIN [Control].[Currencies] g 
					ON c.[CurrencyId] = g.[CurrencyId]
			WHERE
				c.[CustomerId] = @pCustomerId AND 
				(@pStatus is null OR (ISNULL(t.[IsFloating], 0) = case WHEN @pStatus = 2 THEN 1 ELSE 0 END AND --Floating
				ISNULL(t.[IsReversed], 0) = CASE WHEN @pStatus = 3 THEN 1 ELSE 0 END AND --Reversed
				ISNULL(t.[IsDuplicated], 0) = CASE WHEN @pStatus = 4 THEN 1 ELSE 0 END AND --Duplicated
				ISNULL(t.[IsAdjustment], 0) = CASE WHEN @pStatus = 5 THEN 1 ELSE 0 END))  --Adjustment
				AND (@pKey IS NULL 
					OR v.[PlateId] like '%'+@pKey+'%')
			    AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,t.[Date]) = @pMonth
						AND DATEPART(yyyy,t.[Date]) = @pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND t.[Date] BETWEEN @pStartDate AND @pEndDate))

			ORDER BY t.[Date]
		END
	
    SET NOCOUNT OFF
END


GO

