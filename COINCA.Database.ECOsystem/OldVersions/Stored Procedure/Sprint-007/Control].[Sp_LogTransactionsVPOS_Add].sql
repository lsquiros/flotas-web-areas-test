USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Control].[Sp_LogTransactionsVPOS_Add]    Script Date: 4/15/2015 8:09:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Alexander Aguero Venegas
-- Create date: 04/15/2015
-- Description:	Insert log de transaction.
-- ================================================================================================
Create PROCEDURE [Control].[Sp_LogTransactionsVPOS_Add]
(
		@pCCTransactionVPOSId INT 
		,@pResponseCode  varchar(250) = NULL
		,@pResponseCodeDescription Varchar(MAX)= Null
		,@pMessage Varchar(MAX)= Null
		,@pIsSuccess bit
		,@pIsTimeOut bit
		,@pIsCommunicationError bit
)
AS
BEGIN
	--SET NOCOUNT ON
	--   SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lErrorNumber INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
       
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
				INSERT INTO [Control].[LogTransactionsVPOS]
						([CCTransactionVPOSId]
						,[ResponseCode]
						,[ResponseCodeDescription]
						,[Message]
						,[IsSuccess]
						,[IsTimeOut]
						,[IsCommunicationError]
						,[InsertDate]
						,[ModifyDate]
						)
				VALUES	(@pCCTransactionVPOSId
						,@pResponseCode
						,@pResponseCodeDescription
						,@pMessage
						,@pIsSuccess
						,@pIsTimeOut
						,@pIsCommunicationError
						,GETUTCDATE()
						,GETUTCDATE()
					)	
			SET @lRowCount = @@ROWCOUNT
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
				SELECT @@IDENTITY AS 'Identity';
			 END
			
			IF @lRowCount = 0
			BEGIN
				SELECT -1 AS 'Identity';
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
         END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			SELECT -1 AS 'Identity';
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END    
            
