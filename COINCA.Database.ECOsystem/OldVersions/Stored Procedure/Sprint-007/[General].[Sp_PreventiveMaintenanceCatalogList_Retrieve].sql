USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalogList_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCatalogList_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================
-- Author:		Danilo Hidalgo
-- Create date: 02/23/2015
-- Description:	Preventive Maintenance Catalog not Asigned to Vehicle List information
-- ===========================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCatalogList_Retrieve]
	@pCustomerId INT,
	@pVehicleId INT
AS
BEGIN 
	SET NOCOUNT ON;
		SELECT 
			a.[PreventiveMaintenanceCatalogId], 
			a.[Description] [PreventiveMaintenanceDescription]
		FROM 
			[General].[PreventiveMaintenanceCatalog] a
		WHERE 
			a.CustomerId = @pCustomerId AND
			a.PreventiveMaintenanceCatalogId NOT IN 
			(SELECT DISTINCT b.PreventiveMaintenanceCatalogId
			FROM General.PreventiveMaintenanceByVehicle b
			WHERE 
				b.IsDeleted = 0 AND
				b.VehicleId = @pVehicleId)
	SET NOCOUNT OFF;
END
