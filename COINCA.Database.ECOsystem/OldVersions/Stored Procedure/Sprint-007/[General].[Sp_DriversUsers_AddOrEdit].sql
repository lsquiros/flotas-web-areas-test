USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversUsers_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriversUsers_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Add Or Edit Driver User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DriversUsers_AddOrEdit]
(
	 @pDriversUserId INT = NULL
	,@pUserId INT
	,@pCustomerId INT
	,@pIdentification NVARCHAR(MAX)
	,@pCode NVARCHAR(MAX) = NULL
	,@pLicense NVARCHAR(MAX)
	,@pLicenseExpiration DATE
	,@pDallas NVARCHAR(MAX) = NULL
	,@pDailyTransactionLimit INT
	,@pLoggedUserId INT
	,@pCardRequestId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		IF (@pDriversUserId IS NULL)
		BEGIN
			INSERT INTO [General].[DriversUsers]
					([UserId]
					,[CustomerId]
					,[Identification]
					,[Code]
					,[License]
					,[LicenseExpiration]
					,[Dallas]
					,[DailyTransactionLimit]
					,[InsertDate]
					,[InsertUserId]
					,[CardRequestId])
			VALUES	(@pUserId
					,@pCustomerId
					,@pIdentification
					,@pCode
					,@pLicense
					,@pLicenseExpiration
					,@pDallas
					,@pDailyTransactionLimit
					,GETUTCDATE()
					,@pLoggedUserId
					,@pCardRequestId)
		END
		ELSE
		BEGIN
			UPDATE [General].[DriversUsers]
				SET  [Identification] = @pIdentification
					,[Code] = @pCode
					,[License] = @pLicense
					,[LicenseExpiration] = @pLicenseExpiration
					,[Dallas] = @pDallas
					,[DailyTransactionLimit] = @pDailyTransactionLimit
					,[ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
					,[CardRequestId] = @pCardRequestId
			WHERE [DriversUserId] = @pDriversUserId
			
		END
        
        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
