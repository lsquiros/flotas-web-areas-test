USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleRoutePoints_Retrieve]    Script Date: 11/21/2014 10:24:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleRoutePoints_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleRoutePoints_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleRoutePoints_Retrieve]    Script Date: 11/21/2014 10:24:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	Retrieve RoutesSegments information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleRoutePoints_Retrieve]
(
	 @pRouteId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON

		SELECT 
			[PointId], 
			[RouteId], 
			[PointReference], 
			[Order], 
			[Name], 
			[Time], 
			[Latitude], 
			[Longitude], 
			[StopTime]
		FROM 
			[Efficiency].[RoutesPoints] 
		WHERE
			[RouteId] = @pRouteId
	
    SET NOCOUNT OFF
END




GO


