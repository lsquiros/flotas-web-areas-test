USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TrendFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TrendFuelsReportByVehicle_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Control].[Sp_TrendFuelsReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pVehiculoId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	;

	WITH [TmpValues] ( [Number]) as
	(
		SELECT 0
		UNION ALL
		SELECT [Number] + 1
		FROM [TmpValues]
		WHERE [Number] < 100
	)
	SELECT
  		 [T].[VehicleId]
		,[T].[PlateID] [PlateNumber]
		,[T].[Year]
		,[T].[Month]
		,Sum([T].[TotalOdometer]) [TotalOdometer]
		,Sum([T].[TotalLiters]) [TotalLiters]
		,Sum([T].[Samples]) [Samples]
		,Sum([T].[AVGPerformace]) [AVGPerformace]
		,Sum([T].[Interception]) [Interception]
		,Sum([T].[slope]) [slope]
	FROM
		(SELECT 
			[S].[VehicleId],
			[S].[PlateID],
			[S].[Month],
			[S].[Year],
			[S].[TotalOdometer],
			[S].[TotalLiters],
			[S].[N] [Samples],
			[S].[AVGPerformace],
			([S].[SUMY] * [S].[SUMXto2] - ([S].[SUMX] * [S].[SUMXY])) / ([N] * [S].[SUMXto2] - Power([S].[SUMX], 2)) [Interception], 
			([N] * [S].[SUMXY] - [S].[SUMX] * [S].[SUMY]) / ([N] * [S].[SUMXto2] - Power([S].[SUMX], 2)) [slope] 
		FROM 
			(SELECT 
				[p].[VehicleId], 
				[p].[PlateId], 
				DATEPART(m, [p].[TrxDate]) [Month],
				DATEPART(yyyy, [p].[TrxDate]) [Year],
				sum([p].[TrxReportedOdometer]) [TotalOdometer],
				sum([p].[TrxReportedLiters]) [TotalLiters],
				Count(*) [N], 
				AVG([Y]) [AVGPerformace], 
				SUM([p].[X]) [SUMX],
				SUM([Y]) SUMY, 
				SUM([p].[XY]) [SUMXY],
				SUM([p].[X2]) [SUMXto2],
				SUM([p].[Y2]) [SUMto2]
			FROM	
				(SELECT 
					[V].[VehicleId] [VehicleId] ,
					[V].[PlateId],
					[PV].[TrxReportedOdometer],
					[PV].[TrxReportedLiters], 
					[PV].[TrxDate],ROW_NUMBER() OVER(ORDER BY [PV].[TrxDate] ASC) [X], 
					[PV].[TrxPerformance] [Y], 
					ROW_NUMBER() OVER(ORDER BY [PV].[TrxDate]) * 
					[PV].[TrxPerformance] [XY],   
					POWER(ROW_NUMBER() OVER(ORDER BY [PV].[TrxDate] ASC),2) [X2],  
					POWER([PV].[TrxPerformance] , 2) [Y2]
				FROM [General].[PerformanceByVehicle] [PV] 
					INNER  JOIN [General].[Vehicles] [V] 	
						ON [PV].[VehicleId] = [V].[VehicleId]
				WHERE 
					((@pYear IS NOT NULL AND 
					@pMonth IS NOT NULL 
					AND DATEPART(m, [PV].[TrxDate]) = @pMonth
					AND DATEPART(yyyy, [PV].[TrxDate]) = @pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
					AND [PV].[TrxDate] BETWEEN @pStartDate AND @pEndDate))
					AND [PV].[TrxPerformance] > 0 and [V].[CustomerId] = @pCustomerId
					AND (@pVehiculoId IS NULL OR [V].[VehicleId] = @pVehiculoId)
				) [p]
		GROUP By [p].[VehicleId], [p].[PlateID], DATEPART(m, [p].[TrxDate]), DATEPART(yyyy, [P].[TrxDate])) [S]

		UNION --- Inclusion de  valores intermedios

		SELECT
				 [a].[VehicleId]
				,[a].[PlateId] AS [PlateNumber]
				,[r].[Month]
				,[r].[Year]
				,0 [TotalOdometer]
				,0 [TotalLiters]	
				,0 [Samples]	
				,0 [AVGPerformace]	
				,0 [Interception]	
				,0 [slope]
			FROM [General].[Vehicles] a
				CROSS JOIN(
						SELECT
							 DATEPART(m, DATEADD(m, x.[Number], @pStartDate)) [Month]
							,DATEPART(yyyy, DATEADD(m, x.[Number], @pStartDate)) [Year]
						FROM [TmpValues] [x]
						WHERE [x].[Number] <= DATEDIFF(m, @pStartDate, @pEndDate)
					UNION
						SELECT 
							@pMonth [Month], 
							@pYear [Year]
						WHERE 
							@pMonth IS NOT NULL AND 
							@pYear IS NOT NULL
				)[r]
			WHERE [a].[CustomerId] = @pCustomerId AND 
				(@pVehiculoId IS NULL OR [a].[VehicleId] = @pVehiculoId)
			
		)[T]
 GROUP BY [T].[VehicleId]
		 ,[T].[PlateID]
		 ,[T].[Year]
		 ,[T].[Month]
		
SET NOCOUNT OFF	
	END