USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_CreditCard_AddOrEdit]    Script Date: 6/24/2015 4:16:51 PM ******/
DROP PROCEDURE [Control].[Sp_CreditCard_AddOrEdit]
GO

/****** Object:  StoredProcedure [Control].[Sp_CreditCard_AddOrEdit]    Script Date: 6/24/2015 4:16:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update CreditCard information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCard_AddOrEdit]
(
	 @pCreditCardId INT = NULL				--@pCreditCardId:Credit Card Id, PK of the table
	,@pCustomerId INT = NULL				--@pCustomerId: Customer Id
	,@pCreditCardNumberId INT = NULL		--@pCreditCardNumberId: Credit Card Number Id
	,@pExpirationYear INT					--@pExpirationYear: Expiration Year
	,@pExpirationMonth INT					--@pExpirationMonth: Expiration Month
	,@pCreditLimit NUMERIC(16,2)			--@pCreditLimit: Credit Limit
	,@pStatusId INT	= NULL					--@pStatusId: Status Id
	,@pMonth INT							--@pMonth: Month
	,@pYear INT								--@pYear: Year
	,@pUserId INT = NULL					--@pUserId: User Id
	,@pVehicleId INT = NULL					--@pVehicleId: Vehicle Id
	,@pCardRequestId INT = NULL				--@pCardRequestId: Ref (RequestId)
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pPin VARCHAR(4) = NULL				--@pPin: Card Pin
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			--//// VALIDAMOS SI LA TARJETA YA ESTA CREADA /////////////////////////////
			IF (@pCreditCardId IS NULL)
			BEGIN
				DECLARE @lCreditCardNumber NVARCHAR(520)
				
				SELECT 
					@lCreditCardNumber = CreditCardNumber
				FROM [Control].[CreditCardNumbers]
				WHERE [CreditCardNumberId] = @pCreditCardNumberId
				
				UPDATE [Control].[CreditCardNumbers]
					SET  [IsUsed] = 1
						,[IsReserved] = 0
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CreditCardNumberId] = @pCreditCardNumberId
				
				INSERT INTO [Control].[CreditCard]
						([CustomerId]
						,[CreditCardNumber]
						,[ExpirationYear]
						,[ExpirationMonth]
						,[CreditLimit]
						,[CreditAvailable]
						,[StatusId]
						,[CardRequestId]
						,[Pin]
						,[InsertDate]
						,[InsertUserId])
				VALUES ( @pCustomerId
						,@lCreditCardNumber
						,@pExpirationYear
						,@pExpirationMonth
						,@pCreditLimit
						,@pCreditLimit -- Same when Insert
						,@pStatusId
						,@pCardRequestId
						,@pPin
						,GETUTCDATE()
						,@pLoggedUserId)
				SET @lRowCount = @@ROWCOUNT
				SET @pCreditCardId = SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
				DECLARE @lCreditLimit NUMERIC(16,2), @lCreditAvailable NUMERIC(16,2)
				
				SELECT @lCreditAvailable = @pCreditLimit - ([CreditLimit] - [CreditAvailable])
				FROM [Control].[CreditCard]
				WHERE [CreditCardId] = @pCreditCardId
				
				
				SELECT
					@pCustomerId = [CustomerId]
				FROM [Control].[CreditCard]
				WHERE @pCustomerId IS NULL -- When update comes from coinca admin
				  AND [CreditCardId] = @pCreditCardId
				
				UPDATE [Control].[CreditCard]
					SET  [CustomerId] = @pCustomerId
						,[ExpirationYear] = @pExpirationYear
						,[ExpirationMonth] = @pExpirationMonth
						,[CreditLimit] = @pCreditLimit
						,[CreditAvailable] = @lCreditAvailable
						,[Pin] = COALESCE(@pPin,[Pin])
						,[StatusId] = COALESCE(@pStatusId,[StatusId])
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CreditCardId] = @pCreditCardId
				  AND [RowVersion] = @pRowVersion
				  
				SET @lRowCount = @@ROWCOUNT
			END		
            --/////////////////////////////////////////////////////////////////////////////


            IF @lRowCount > 0  -- VALIDAMOS SI HAY UN UPDATE O UN INSERT DE LA TARJETA
            BEGIN
				
				-- //////// ACTUALIZAMOS EL CONDUCTOR LIGADO A LA TARJETA /////////////////
				IF @pUserId IS NOT NULL 
				BEGIN
					SELECT @lCount = COUNT(1) FROM [Control].[CreditCardByDriver] WHERE [CreditCardId] = @pCreditCardId
					IF @lCount = 0
					BEGIN
						INSERT INTO [Control].[CreditCardByDriver]
							([CreditCardId]
							,[UserId]
							,[InsertDate]
							,[InsertUserId])
						VALUES
							(@pCreditCardId
							,@pUserId
							,GETUTCDATE()
							,@pLoggedUserId)
					END ELSE BEGIN
						UPDATE [Control].[CreditCardByDriver]
						SET  [UserId] = @pUserId
							,[ModifyDate] = GETUTCDATE()
							,[ModifyUserId] = @pLoggedUserId
						WHERE [CreditCardId] = @pCreditCardId
					END
				END
				-- ////////////////////////////////////////////////////////////////////////

				 -- ///// ACTUALIZAMOS EL VEHICULO LIGADO A LA TARJETA ////////////////////
				IF @pVehicleId IS NOT NULL
				BEGIN
					SELECT @lCount = COUNT(1) FROM [Control].[CreditCardByVehicle] WHERE [CreditCardId] = @pCreditCardId
					IF @lCount = 0
					BEGIN
						INSERT INTO [Control].[CreditCardByVehicle]
							([CreditCardId]
							,[VehicleId]
							,[InsertDate]
							,[InsertUserId])
						VALUES
							(@pCreditCardId
							,@pVehicleId
							,GETUTCDATE()
							,@pLoggedUserId)
					END ELSE BEGIN
						UPDATE [Control].[CreditCardByVehicle]
						SET  [VehicleId] = @pVehicleId
							,[ModifyDate] = GETUTCDATE()
							,[ModifyUserId] = @pLoggedUserId
						WHERE [CreditCardId] = @pCreditCardId
					END
				END
				--///////////////////////////////////////////////////////////////////
				
				--///////// VALIDAMOS LA ASIGNACIÓN DE CREDITO DISPONIBLE A LA TARJETA //////					
				DECLARE @lAssigned DECIMAL(16,2) = 0
				DECLARE @lAvailable DECIMAL(16,2) = 0
									
				SET @lAssigned = COALESCE((SELECT SUM(a.[CreditLimit])+ SUM(a.[CreditExtra]) 
											FROM [Control].[CreditCard] a
											WHERE a.[CustomerId] = @pCustomerId
											  AND a.[StatusId] <> 8 AND a.[StatusId] <> 9),0) -- 8 = BLOQUEADA | 9 = CERRADA
				  
				SELECT
					--@lAvailable = a.[CreditAvailable]
					@lAvailable = a.[CreditAmount]
				FROM [Control].[CustomerCredits] a
				WHERE a.[Year] = @pYear
				  AND a.[Month] = @pMonth
				  AND a.[CustomerId] = @pCustomerId
				
				IF @lAssigned < = @lAvailable
				BEGIN
					UPDATE [Control].[CustomerCredits]
					SET [CreditAssigned] = @lAssigned
					WHERE [Year] = @pYear
					  AND [Month] = @pMonth
					  AND [CustomerId] = @pCustomerId
				END ELSE
				BEGIN
					IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
					BEGIN
						ROLLBACK TRANSACTION
						
						UPDATE [Control].[CreditCardNumbers]
						SET  [IsUsed] = 0, [IsReserved] = 0
						WHERE [CreditCardNumberId] = @pCreditCardNumberId
						
						Declare @strMessage varchar(500)
						set @strMessage = CONCAT('No amount available. ',  'Assigned: ',@lAssigned, ' > ', 'Available:', @lAvailable)

						RAISERROR (@strMessage, 16, 1)
					END
				END
				
				-- ////////////////////////////////////////////////////////////////////////

				--///////////////// VALIDAMOS EL ESTADO DE LA TARJETA ////////////////////

				-- HABILITAMOS CREDITO DISPONIBLE CUANDO LA TARJETA DE ESTABLECIÓ COMO ESTADO CERRADA

				--DECLARE @StateName varchar(50) = ''

				--SELECT @StateName = Name 
				--FROM [General].[Status]
				--WHERE StatusId = @pStatusId

				--IF(@StateName = 'Cerrada')  -- SE NECESITA VALIDAR SI TAMBIEN LAS BLOQUEADAS
				--BEGIN
				--	--@pCreditLimit
				--	SELECT
				--		--@lAvailable = a.[CreditAvailable],
				--		@lAssigned = a.[CreditAssigned]
				--	FROM [Control].[CustomerCredits] a
				--	WHERE a.[Year] = @pYear
				--	  AND a.[Month] = @pMonth
				--	  AND a.[CustomerId] = @pCustomerId
				
					
				--	UPDATE [Control].[CustomerCredits]
				--	SET 
				--		[CreditAssigned] = (@lAssigned - @pCreditLimit)
				--		--[CreditAvailable] = (@lAvailable + @pCreditLimit)
				--	WHERE [Year] = @pYear
				--		AND [Month] = @pMonth
				--		AND [CustomerId] = @pCustomerId
				--END

				-- ///////////////////////////////////////////////////////////////////////


				-- History control ///////////////////////////////////////////////////////
				INSERT INTO [Control].[CreditCardHx] 
				   ([CreditCardId]
				   ,[CustomerId]
				   ,[CreditCardNumber]
				   ,[ExpirationYear]
				   ,[ExpirationMonth]
				   ,[CreditLimit]
				   ,[CreditAvailable]
				   ,[CreditExtra]
				   ,[StatusId]
				   ,[CardRequestId]
				   ,[InsertDate]
				   ,[InsertUserId]
				   ,[ModifyDate]
				   ,[ModifyUserId])
				SELECT [CreditCardId]
				   ,[CustomerId]
				   ,[CreditCardNumber]
				   ,[ExpirationYear]
				   ,[ExpirationMonth]
				   ,[CreditLimit]
				   ,[CreditAvailable]
				   ,[CreditExtra]
				   ,[StatusId]
				   ,[CardRequestId]
				   ,[InsertDate]
				   ,[InsertUserId]
				   ,[ModifyDate]
				   ,[ModifyUserId] FROM [Control].[CreditCard] WHERE [CreditCardId] = @pCreditCardId
				-- History control --
            END
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
			
			UPDATE [Control].[CreditCardNumbers]
				SET  [IsUsed] = 0, [IsReserved] = 0
			WHERE [CreditCardNumberId] = @pCreditCardNumberId
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


