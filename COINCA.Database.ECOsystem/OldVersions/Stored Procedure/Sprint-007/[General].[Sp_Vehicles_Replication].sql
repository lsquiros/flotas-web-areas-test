USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_Vehicles_Replication]    Script Date: 02/10/2015 08:01:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_Replication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Vehicles_Replication]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_Vehicles_Replication]    Script Date: 02/10/2015 08:01:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:		Napole�n Alvarado
-- Create date: 11/19/2014
-- Description:	Vehicles specific data replication on Atrack DB
-- ===============================================================
Create Procedure [General].[Sp_Vehicles_Replication]
	@pVehicleId as int,							--@pVehicleId: PK of the table
	@pCustomerId as int,						--@pCustomerId: FK of Customer
	@pVehicleIntrackId as int,					--@pVehicleIntrackId: Reference to Intrack Vehicles
	@pDeviceId as int,							--@pDeviceId: Reference to Atrack Devices
	@pAdministrativeSpeedLimit as int,			--@pAdministrativeSpeedLimit: Maximum speed limit for this car
	@pResult as int = 0 output,					--@pResult: 0 Success, -1 Error
	@pErrorDesc as nvarchar(4000) = '' output	--@pErrorDesc: Error description
	
AS 
BEGIN
	BEGIN TRY
		Declare @lvehiculoIntrack int,
				@lCustomerId int
		
		Set @pResult = 0
		If @pVehicleId is not null
		begin
			--Verificar que la referencia a los veh�culos de Intrack sea v�lida
			If @pVehicleIntrackId is not null
			begin
				Select @lvehiculoIntrack = vehiculo
				  from IntrackV2.dbo.Vehiculos
				 where vehiculo = @pVehicleIntrackId
				
				If @@ROWCOUNT = 0
				begin
					--RAISERROR('Invalid Intrack Reference', 16, 1)
					Set @pResult = -1
					Set @pErrorDesc = 'Invalid Intrack Vehicle Reference'
					Return
				end
			end
		
			--Verificar si ya existe el registro
			Select @lCustomerId = CustomerId
			  from [172.22.220.38\BDS_WEB].ATrack.dbo.VehiclesECOSystem
			 where VehicleId = @pVehicleId
			 
			If @@ROWCOUNT > 0
			begin
				Update [172.22.220.38\BDS_WEB].ATrack.dbo.VehiclesECOSystem
				   set CustomerId = @pCustomerId,
					   VehicleIntrackId = @pVehicleIntrackId,
					   DeviceId = @pDeviceId,
					   AdministrativeSpeedLimit = @pAdministrativeSpeedLimit
				 where VehicleId = @pVehicleId
				 
				If @@ROWCOUNT = 0 
				begin
					--RAISERROR('Atrack table VehiclesECOSystem was not updated', 16, 1)
					Set @pResult = -1
					Set @pErrorDesc = 'Atrack table VehiclesECOSystem was not updated'
				end
			end
			else
			begin
				Insert into [172.22.220.38\BDS_WEB].ATrack.dbo.VehiclesECOSystem
					(VehicleId, CustomerId, VehicleIntrackId, DeviceId, AdministrativeSpeedLimit)
				values (@pVehicleId, @pCustomerId, @pVehicleIntrackId, @pDeviceId, @pAdministrativeSpeedLimit)

				If @@ROWCOUNT = 0 
				begin
					--RAISERROR('Record not included in Atrack table VehiclesECOSystem', 16, 1)
					Set @pResult = -1
					Set @pErrorDesc = 'Record not included in Atrack table VehiclesECOSystem'
				end
			end
		end
	END TRY
    BEGIN CATCH		
		Set @pResult = -1
		Select @pErrorDesc = ERROR_MESSAGE()		
	END CATCH	
	
END

GO


