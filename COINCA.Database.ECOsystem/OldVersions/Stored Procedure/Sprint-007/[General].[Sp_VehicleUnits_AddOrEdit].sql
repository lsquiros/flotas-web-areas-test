USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleUnits_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleUnits_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update VehicleSubUnit information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleUnits_AddOrEdit]
(
	 @pUnitId INT = NULL					--@pUnitId: PK of the table
	,@pName VARCHAR(250)					--@pName: VehicleSubUnit Name
	,@pCustomerId INT						--@pCustomerId: FK of the table General.Customers
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pUnitId IS NULL)
			BEGIN
				INSERT INTO [General].[VehicleUnits]
						([Name]
						,[CustomerId]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pName
						,@pCustomerId
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[VehicleUnits]
					SET  [Name] = @pName
						,[CustomerId] = @pCustomerId
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [UnitId] = @pUnitId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO