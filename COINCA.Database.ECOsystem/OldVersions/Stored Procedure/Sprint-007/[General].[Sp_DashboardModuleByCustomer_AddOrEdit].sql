USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DashboardModuleByCustomer_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DashboardModuleByCustomer_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo.
-- Create date: 02/12/2014
-- Description:	Insert or Update Dashboard Modules
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DashboardModuleByCustomer_AddOrEdit]
(
	 @pCustomerId INT
	,@pDashboardModuleId INT
	,@pOrder INT
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (EXISTS(SELECT 1 FROM [General].[DashboardModulesByCustomer] WHERE [CustomerId] = @pCustomerId AND [DashboardModuleId] = @pDashboardModuleId))
			BEGIN
				UPDATE [General].[DashboardModulesByCustomer]
					SET  [Order] = @pOrder
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CustomerId] = @pCustomerId
				  AND [DashboardModuleId] = @pDashboardModuleId
				  AND [RowVersion] = @pRowVersion							
			END
			ELSE
			BEGIN
				INSERT INTO [General].[DashboardModulesByCustomer]
						([CustomerId]
						,[DashboardModuleId]
						,[Order]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pDashboardModuleId
						,@pOrder
						,GETUTCDATE()
						,@pLoggedUserId)	
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO