USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_States_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_States_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Napole�n Alvarado
-- Create date: 12/17/2014
-- Description:	Retrieve States information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_States_Retrieve]
(
	 @pStateId INT = NULL,
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[StateId]
		,a.[Name]
		,a.[CountryId]
		,a.[Code]
		,a.[Latitude]
		,a.[Longitude]
		,a.[RowVersion]
    FROM [General].[States] a
	WHERE (@pStateId IS NULL OR [StateId] = @pStateId)
	  AND (@pCountryId IS NULL OR [CountryId] = @pCountryId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR a.[Code] like '%'+@pKey+'%')
	ORDER BY a.[Code]
	
    SET NOCOUNT OFF
END
