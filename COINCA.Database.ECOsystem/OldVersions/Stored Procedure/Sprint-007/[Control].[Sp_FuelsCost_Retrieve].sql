USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_FuelsCost_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_FuelsCost_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/03/2014
-- Description:	Retrieve Customer Credit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_FuelsCost_Retrieve]
(
	  @pCustomerId INT						--@pCustomerId: Customer Id
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lExchangeRate DECIMAL(16,8) = 1.0
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	
	SELECT
		@lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[PartnerId] = b.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = b.[CustomerId] AND c.[CurrencyId] = a.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND a.EndDate IS NULL
	
	SELECT
		 b.[FuelId]
		, COALESCE(CONVERT(DECIMAL(16,2),(t.LiterPrice * @lExchangeRate)), 0) AS LiterPrice
    FROM [Control].[Fuels] b
		LEFT JOIN (
				SELECT
					x.[FuelId], x.[LiterPrice] 
				FROM [Control].[PartnerFuel] x
					INNER JOIN [General].[CustomersByPartner] y
						ON x.[PartnerId] = y.[PartnerId]
					INNER JOIN [General].[Customers] z
						ON y.[CustomerId] = z.[CustomerId]
				WHERE z.CustomerId = @pCustomerId
				  AND x.[EndDate] IS NULL)t
			ON b.[FuelId] = t.FuelId
	
    SET NOCOUNT OFF
END
