USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerCreditCardByFleetCard_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerCreditCardByFleetCard_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/15/2014
-- Description:	Retrieve Customer CreditCards information by Fleet Card
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerCreditCardByFleetCard_Retrieve]
(
	  @pFleetCardNumber NVARCHAR(520)
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT TOP 1
			 a.[CreditCardNumber] AS [EncryptedCreditCardNumber]
			,a.[ExpirationYear]
			,a.[ExpirationMonth]
	FROM [General].[CustomerCreditCards] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[CreditCard] d
			ON d.[CustomerId] = c.[CustomerId]
	WHERE b.[StatusId] <> 102 -- Cancelada
	  AND d.[CreditCardNumber] = @pFleetCardNumber
	ORDER BY a.[CustomerCreditCardId] DESC
	
    SET NOCOUNT OFF
END
