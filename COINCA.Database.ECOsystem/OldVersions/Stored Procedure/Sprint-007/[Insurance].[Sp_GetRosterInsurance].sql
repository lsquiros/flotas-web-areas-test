USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetRosterInsurance]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Insurance].[Sp_GetRosterInsurance] 
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 09/02/2015
-- Description:	Retrieve Get Roster Insurance information
-- ================================================================================================
create PROCEDURE [Insurance].[Sp_GetRosterInsurance] 
(	@pPartnerId Int, 
	@pCustomerId int =Null
 )
AS
BEGIN
--Select * from  General.CustomersPartners   where PartnerId = @pParnetId 
Select c.CustomerId , c.Name from General.[CustomersByPartner] cbp INNER JOIN General.Customers c ON cbp.CustomerId=c.CustomerId where PartnerId=@pPartnerId and cbp.CustomerId=ISNULL(@pCustomerId,cbp.CustomerId)
END
