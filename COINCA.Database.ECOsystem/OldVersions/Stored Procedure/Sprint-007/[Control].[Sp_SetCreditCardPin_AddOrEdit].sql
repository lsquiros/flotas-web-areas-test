USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_SetCreditCardPin_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_SetCreditCardPin_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 03/10/2015
-- Description:	Set Credit Card Pin
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_SetCreditCardPin_Retrieve]
(
	@pRequestId INT,
	@pPin VARCHAR(4)
)
AS
BEGIN
	
	SET NOCOUNT ON

	UPDATE
		[Control].[CreditCard] 
	SET 
		[Pin] = @pPin 
	WHERE 
		[CardRequestId] = @pRequestId

    SET NOCOUNT OFF
END
