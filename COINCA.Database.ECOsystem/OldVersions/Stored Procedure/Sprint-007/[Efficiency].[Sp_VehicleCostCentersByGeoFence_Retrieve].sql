USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleCostCentersByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_VehicleCostCentersByGeoFence_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/18/2014
-- Description:	Retrieve VehicleSubUnitsByGeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleCostCentersByGeoFence_Retrieve]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		null [GeoFenceId],
		a.[Name] [Name],
		a.[CostCenterId] [CostCenterId],
		ROW_NUMBER() OVER (ORDER BY a.[CostCenterId]) AS [RowNumber] 
	FROM 
		[General].[VehicleCostCenters] a
		INNER JOIN [General].[VehicleUnits] b ON a.[UnitId] = b.[UnitId]
	WHERE
		b.[CustomerId] = @pCustomerId
		AND NOT EXISTS(
			SELECT 
				1 
			FROM 
				[Efficiency].[VehicleCostCenterByGeoFence] x
			WHERE 
				x.[CostCenterId] = a.[CostCenterId] AND 
				x.[GeoFenceId] = @pGeoFenceId
			)
	UNION
	SELECT 
		x.[GeoFenceId] [GeoFenceId],
		a.[Name] [Name],
		a.[CostCenterId] [CostCenterId],
		ROW_NUMBER() OVER (ORDER BY a.[CostCenterId]) AS [RowNumber] 
	FROM 
		[General].[VehicleCostCenters] a
		INNER JOIN [Efficiency].[VehicleCostCenterByGeoFence] x ON a.[CostCenterId] = x.[CostCenterId]
	WHERE x.[GeoFenceId] = @pGeoFenceId
	
    SET NOCOUNT OFF
END




