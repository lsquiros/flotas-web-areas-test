USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PAIndicators_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PAIndicators_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero
-- Create date: 11/14/2014
-- Description:	Retrieve Indicators for Partner Admin Landing Page
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PAIndicators_Retrieve]
(
	@pPartnerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		(SELECT COUNT(1) FROM [General].[Customers] c 
			INNER JOIN [General].[CustomersByPartner] cp 
			ON cp.CustomerId = c.CustomerId
			WHERE c.IsDeleted = 0 AND c.IsActive = 1 
			AND cp.PartnerId = @pPartnerId) 
		 AS Customers,
		(SELECT COUNT(1) FROM [Control].[CreditCard] cc
			INNER JOIN [General].[CustomersByPartner] cp 
			ON cp.CustomerId = cc.CustomerId
			WHERE cp.PartnerId = @pPartnerId
		) AS Cards, 
		(SELECT COUNT(1) FROM [General].[Vehicles] v
			INNER JOIN [General].[CustomersByPartner] cp 
			ON cp.CustomerId = v.CustomerId
			WHERE cp.PartnerId = @pPartnerId AND v.Active = 1) 
		 AS Vehicles,
		(SELECT COUNT(1) FROM [General].[PartnerUsers] pu 
			INNER JOIN [General].[Users] a 
			ON pu.UserId = a.UserId
			WHERE pu.PartnerId = @pPartnerId AND a.IsActive = 1 
			AND a.InsertDate > DATEADD(day,-7,GETUTCDATE())) 
		 AS Users
	
    SET NOCOUNT OFF
END
