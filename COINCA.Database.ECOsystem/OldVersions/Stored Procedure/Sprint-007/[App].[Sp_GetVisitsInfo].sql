USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetVisitsInfo]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_GetVisitsInfo]
GO

USE [ECOsystem]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 ----================================================================================================
 --Author:		Gerald Solano C.
 --Create date: 06/24/2015
 --Description:	Retrieve Visits from the current day
 ----================================================================================================
CREATE PROCEDURE [dbo].Sp_GetVisitsInfo
(
	@pUserId int,
	@pRouteId int
)
AS
BEGIN
	
	DECLARE @CurrentRouteName varchar(150) = ''
	DECLARE @VisitPoints nvarchar(max) = ''

	-- /////////////// OBTENEMOS LOS PUNTOS DE LA RUTA (LATITUD Y LONGITUD) //////////////
	SELECT 	@VisitPoints = CONCAT(@VisitPoints, [Name], ',', [Latitude], ',', [Longitude], '|')
	FROM [Efficiency].[RoutesPoints] 
	WHERE [RouteId] = @pRouteId

	--////////////////////////////////////////////////////////////////////////////////////

	-- RETORNAMOS LOS DATOS DE LA RUTA
	SELECT @pRouteId AS RouteId, 
		 --  @CurrentRouteName AS RouteName, 
		   @VisitPoints AS VisitPoints

END --End  Stored
GO

