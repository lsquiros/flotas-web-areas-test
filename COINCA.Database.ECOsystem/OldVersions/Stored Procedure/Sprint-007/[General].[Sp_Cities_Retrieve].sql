USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Cities_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Cities_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Napole�n Alvarado
-- Create date: 12/17/2014
-- Description:	Retrieve Cities information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Cities_Retrieve]
(
	 @pCityId INT = NULL,
	 @pCountyId INT = NULL,
	 @pStateId INT = NULL,
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[CityId]
		,a.[Name]
		,a.[CountyId]
		,a.[Code]
		,a.[Latitude]
		,a.[Longitude]
		,b.[StateId]
		,c.[CountryId]
		,a.[RowVersion]
    FROM [General].[Cities] a
		INNER JOIN [General].[Counties] b
			ON a.[CountyId] = b.[CountyId]
		INNER JOIN [General].[States] c
			ON b.[StateId] = c.[StateId]
	WHERE (@pCityId IS NULL OR a.[CityId] = @pCityId)
	  AND (@pCountyId IS NULL OR a.[CountyId] = @pCountyId)
	  AND (@pStateId IS NULL OR b.[StateId] = @pStateId)
	  AND (@pCountryId IS NULL OR c.[CountryId] = @pCountryId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR a.[Code] like '%'+@pKey+'%')
	ORDER BY a.[Code]
	
    SET NOCOUNT OFF
END
