USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleByRoute_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_VehicleByRoute_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/26/2014
-- Description:	Insert or Update Vehicle by Route information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleByRoute_AddOrEdit]
(
	 @pVehicleId INT						
	,@pXmlData VARCHAR(MAX)					
	,@pLoggedUserId INT						
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Efficiency].[VehicleByRoute]
			WHERE [VehicleId] = @pVehicleId
			
			INSERT INTO [Efficiency].[VehicleByRoute]
					([VehicleId]
					,[RouteId]
					,[Days]
					,[InsertDate]
					,[InsertUserId])
			SELECT 
					 @pVehicleId
					,Row.col.value('./@RouteId', 'INT') AS [UnitId]
					,Row.col.value('./@Days', 'VARCHAR(10)') AS [UnitId]
					,GETUTCDATE()
					,@pLoggedUserId
			FROM @lxmlData.nodes('/xmldata/VehicleByRoute') Row(col)
			WHERE Row.col.value('./@Days', 'VARCHAR(10)') <> ''
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO