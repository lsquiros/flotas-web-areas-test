USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetGlobalScoreDataSourceAllDriver]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_GetGlobalScoreDataSourceAllDriver]
GO
USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Operation].[Sp_GetScoreSpeedAllDriver]    Script Date: 11/25/2014 09:11:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified by:		Andrés Oviedo
-- Modified date:	16/02/2015 
CREATE PROCEDURE [Operation].[Sp_GetGlobalScoreDataSourceAllDriver] 
( @pCustomerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)			--Posibles valores S:Summarized, D:Detailed
)
AS
BEGIN

	--Declare @pStartDate datetime =  '20141019 03:45:02.000',
	--@pEndDate datetime = '20141128 11:30am'
	--Verificar configuración de la ponderación
	Declare @TotalWeight numeric(6,2), 
	        @CountWeight int,
	        @pCustomerName varchar(200)
	
	Select
		   @pCustomerName=Name
	  from General.Customers 
	 where CustomerId = @pCustomerId
	 
	Select @TotalWeight = IsNull(SUM(Weight),0),
		   @CountWeight = COUNT(1)
	 from Operation.WeightedScoreSettings w
	 where CustomerId = @pCustomerId
	
	--If @CountWeight > 0 and @TotalWeight <> 100
	--begin
		--Los pesos para la ponderación no suman 100
	--	RAISERROR ('101', 16, 1)
	--	RETURN
	--end

	--Set Dates 
	Declare @DataPerMonth bit = 0 
	If @pEndDate is null
		If @pStartDate is null
		begin
			If @pMonth is null or @pYear is null
			begin
				RAISERROR ('100', 16, 1)
				RETURN
			end
			else
			begin
				--Set @pStartDate = [General].[Fn_GetFirstDayOfMonth](@pYear, @pMonth)
				--Set @pEndDate = DATEADD(MM, 1, @pStartDate)
				Set @DataPerMonth = 1
			end
		end
		else
		begin
			Set @pEndDate = DATEADD(DD, 1, @pStartDate)
		end

	--DECLARE @MyTable  dbo.TypeDeviceSpeed
	Declare @VehiclesByUser table
	(Device int,
	 Vehicle int,
	 StartOdometer int,
	 EndOdometer int,
	 StartDate datetime,
	 EndDate datetime)

	Create table #LocalDriverScore
	(DriverScoreId INT Identity(1,1),
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 ScoreType varchar(5),
	 Score Float,
	 Photo varchar(max),
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float
	 )
	Declare @UserId Int
	Declare @Name varchar(250),
			@Identification varchar(50)
		
	Declare  @ScoreAdmOutput nvarchar(4000),
			 @ScoreRouteOutput nvarchar(4000)
			 	
	Declare  @ScoreAdm Float,
			 @ScoreRoute Float

	If @DataPerMonth = 0 
	begin
		Select @UserId = Min(U.UserId) 
		  from General.Users U
		  inner join General.DriversUsers D on U.UserId = D.UserId
		 Where U.IsActive =  1 
		 AND D.CustomerId=@pCustomerId
		--Begin transaction
		 
		While  @UserId  is not null
		BEGIN
			Insert into @VehiclesByUser
				(Device, Vehicle, StartOdometer, EndOdometer, StartDate, EndDate)  
			--Select   V.IntrackReference As Device,CASE WHEN  VU.InitialOdometer IS NULL THEN 0 eLSE VU.InitialOdometer end  ,CASE WHEN VU.EndOdometer IS NULL THEN 0 eLSE EndOdometer end    , VU.InsertDate StartDate ,VU.LastDateDriving EndDate
			Select V.DeviceReference, V.IntrackReference, IsNull(VU.InitialOdometer, 0), IsNull(VU.EndOdometer,0), VU.InsertDate, IsNull(VU.LastDateDriving, GETDATE())
			from General.VehiclesByUser VU 
			inner Join General.Vehicles V ON  V.VehicleId = VU.VehicleId
			inner JOIN General.Users U  ON U.UserId = VU.UserId
			Where U.UserId =  @UserId 
			  And V.DeviceReference is not null
			  and ((VU.InsertDate between @pStartDate and @pEndDate)
			   or  (IsNull(VU.LastDateDriving, GETDATE()) between @pStartDate and @pEndDate )
			   or  (VU.InsertDate <= @pStartDate and IsNull(VU.LastDateDriving, GETDATE()) >= @pEndDate))
			order by V.DeviceReference, VU.InsertDate, VU.LastDateDriving

			if (select COUNT(*) FROM @VehiclesByUser ) > 0 
			BEGIN 
				DECLARE @VehiclesByUserString varchar(1000);
				SET @VehiclesByUserString = (SElect * FROm  @VehiclesByUser AS Lista FOR XML AUTO)
				DECLARE @SQL NVARCHAR(1000)
				
				set @SQL=(SELECT ScoreDrivingFormulaSP FROM Insurance.ScoreDrivingFormula WHERE CustomerId=@pCustomerId AND IsActive=1)
				if @SQL is null
				begin
					set @SQL='[172.22.220.38\BDS_WEB].[Atrack].[dbo].[Sp_GetScoreDriver]';
				end
				
				EXECUTE  @SQL @pStartDate, @pEndDate, @VehiclesByUserString, 'A', @pScoreTable = @ScoreAdmOutput OUTPUT
				EXECUTE  @SQL @pStartDate, @pEndDate, @VehiclesByUserString, 'R', @pScoreTable = @ScoreRouteOutput OUTPUT
				DECLARE @pScoreAdmTableXML XML;
				SET  @pScoreAdmTableXML = CAST(@ScoreAdmOutput  AS XML);
				
				DECLARE @ScoreAdmTable dbo.TypeDriverScore;
				
				Insert into  @ScoreAdmTable
				SELECT --DeviceSpeedId   = T.Item.value('@DeviceSpeedId ', 'int'),
					   KmTraveled = T.Item.value('@KmTraveled', 'float'),
					   OverSpeed = T.Item.value('@OverSpeed', 'float'),
					   OverSpeedAmount  = T.Item.value('@OverSpeedAmount',  'float'),
					   OverSpeedDistancePercentage = T.Item.value('@OverSpeedDistancePercentage', 'float'),
					   OverSpeedAverage = T.Item.value('@OverSpeedAverage', 'float'),
					   OverSpeedWeihgtedAverage = T.Item.value('@OverSpeedWeihgtedAverage', 'float'),
					   StandardDeviation = T.Item.value('@StandardDeviation', 'float'),
					   Variance = T.Item.value('@Variance', 'float'),
					   VariationCoefficient = T.Item.value('@VariationCoefficient', 'float'),
					   AverageScore = T.Item.value('@AverageScore', 'float'),
					   OverSpeedDistance=T.Item.value('@OverSpeedDistance', 'float')
				FROM   @pScoreAdmTableXML.nodes('Lista') AS T(Item)
				
				DECLARE @pScoreRouteTableXML XML;
				SET  @pScoreRouteTableXML = CAST(@ScoreRouteOutput  AS XML);
				
				DECLARE @ScoreRouteTable dbo.TypeDriverScore;
				
				Insert into  @ScoreRouteTable
				SELECT --DeviceSpeedId   = T.Item.value('@DeviceSpeedId ', 'int'),
					   KmTraveled = T.Item.value('@KmTraveled', 'float'),
					   OverSpeed = T.Item.value('@OverSpeed', 'float'),
					   OverSpeedAmount  = T.Item.value('@OverSpeedAmount',  'float'),
					   OverSpeedDistancePercentage = T.Item.value('@OverSpeedDistancePercentage', 'float'),
					   OverSpeedAverage = T.Item.value('@OverSpeedAverage', 'float'),
					   OverSpeedWeihgtedAverage = T.Item.value('@OverSpeedWeihgtedAverage', 'float'),
					   StandardDeviation = T.Item.value('@StandardDeviation', 'float'),
					   Variance = T.Item.value('@Variance', 'float'),
					   VariationCoefficient = T.Item.value('@VariationCoefficient', 'float'),
					   AverageScore = T.Item.value('@AverageScore', 'float'),
					   OverSpeedDistance=T.Item.value('@OverSpeedDistance', 'float')
				FROM   @pScoreRouteTableXML.nodes('Lista') AS T(Item)
				
				set @ScoreAdm=(select AverageScore from @ScoreAdmTable)
				set @ScoreRoute=(select AverageScore from @ScoreRouteTable)
			
				If (@ScoreAdm is not null or @ScoreRoute is not null)
				begin
					--Obtener nombre e identificación del usuario
					Select @Name = U.Name,
						   @Identification = D.Identification 
					  from General.Users U
					  inner join General.DriversUsers D on U.UserId = D.UserId
					 where U.UserId = @UserId
					
					If  @ScoreAdm is not null
						Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score,
						KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
						Values(@UserId, @Name, @Identification, 'SPADM', @ScoreAdm,(select KmTraveled from @ScoreAdmTable),
						(select OverSpeed from @ScoreAdmTable),(select OverSpeedAmount from @ScoreAdmTable),(select OverSpeedDistancePercentage from @ScoreAdmTable),
						(select OverSpeedAverage from @ScoreAdmTable),(select OverSpeedWeihgtedAverage from @ScoreAdmTable),(select StandardDeviation from @ScoreAdmTable)
						,(select Variance from @ScoreAdmTable),(select VariationCoefficient from @ScoreAdmTable),(select OverSpeedDistance from @ScoreAdmTable))

					If  @ScoreRoute is not null
						Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score,
						KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
						Values(@UserId, @Name, @Identification, 'SPROU', @ScoreRoute,(select KmTraveled from @ScoreRouteTable),
						(select OverSpeed from @ScoreRouteTable),(select OverSpeedAmount from @ScoreRouteTable),(select OverSpeedDistancePercentage from @ScoreRouteTable),
						(select OverSpeedAverage from @ScoreRouteTable),(select OverSpeedWeihgtedAverage from @ScoreRouteTable),(select StandardDeviation from @ScoreRouteTable)
						,(select Variance from @ScoreRouteTable),(select VariationCoefficient from @ScoreRouteTable),(select OverSpeedDistance from @ScoreRouteTable))
				end
				
				delete from @ScoreAdmTable
				delete from @ScoreRouteTable
				--Limpia la  tabla para  la nueva seleccion.
				DELETE FROM @VehiclesByUser
			END
			--- Siguiente********************
			Select @UserId = Min(U.UserId) 
			  from General.Users U
			  inner join General.DriversUsers D on U.UserId = D.UserId
			 Where U.IsActive =  1 
			   and U.UserId > @UserId
			   AND D.CustomerId=@pCustomerId
		END --Fin del While
	end
	else
	begin
		--Para la invocación por mes, recupera las calificaciones precalculadas
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance	)
		Select DS.UserId, U.Name, D.Identification, DS.ScoreType, DS.Score ,DS.KmTraveled,
						DS.OverSpeed ,
						DS.OverSpeedAmount ,
						DS.OverSpeedDistancePercentage ,
						DS.OverSpeedAverage ,
						DS.OverSpeedWeihgtedAverage ,
						DS.StandardDeviation ,
						DS.Variance ,
						DS.VariationCoefficient,
						DS.OverSpeedDistance		
		  from Operation.DriversScores DS
		  inner join General.Users U on DS.UserId = U.UserId
		  inner join General.DriversUsers D on DS.UserId = D.UserId
		 where DS.CustomerId = @pCustomerId
		   and DS.ScoreYear = @pYear
		   and DS.ScoreMonth = @pMonth
	end

	If @CountWeight > 0 
	begin
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score, Photo,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
		Select DS.UserId, DS.Name, DS.Identification, ds.ScoreType, SUM(DS.Score * WS.Weight / 100), U.Photo,
						DS.KmTraveled,
						DS.OverSpeed ,
						DS.OverSpeedAmount ,
						DS.OverSpeedDistancePercentage ,
						DS.OverSpeedAverage ,
						DS.OverSpeedWeihgtedAverage ,
						DS.StandardDeviation ,
						DS.Variance ,
						DS.VariationCoefficient,
						DS.OverSpeedDistance	
	      from #LocalDriverScore DS
	     inner join Operation.ScoreTypes ST on 'TOTAL' = ST.Code 
		 inner join Operation.WeightedScoreSettings WS on ST.ScoreTypeId = WS.ScoreTypeId
		 inner join General.Users U on DS.UserId = U.UserId
		 where WS.CustomerId = @pCustomerId
		group by DS.UserId, DS.Name, DS.Identification, U.Photo,
						DS.KmTraveled,
						DS.OverSpeed ,
						DS.OverSpeedAmount ,
						DS.OverSpeedDistancePercentage ,
						DS.OverSpeedAverage ,
						DS.OverSpeedWeihgtedAverage ,
						DS.StandardDeviation ,
						DS.Variance ,
						DS.VariationCoefficient,
						DS.OverSpeedDistance,
		  	 			DS.ScoreType
	end
	else
	begin
	
		--No existe configuración de calificación por peso
		--Se asigna la calificación de carretera como la calificación global
		Insert into #LocalDriverScore (UserID, Name, Identification , ScoreType, Score, Photo,
						KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
		Select DS.UserId, DS.Name   , DS.Identification, ds.ScoreType, DS.Score, U.Photo,
						DS.KmTraveled,
						DS.OverSpeed ,
						DS.OverSpeedAmount ,
						DS.OverSpeedDistancePercentage ,
						DS.OverSpeedAverage ,
						DS.OverSpeedWeihgtedAverage ,
						DS.StandardDeviation ,
						DS.Variance ,
						DS.VariationCoefficient,
						DS.OverSpeedDistance	
	      from #LocalDriverScore DS
		 inner join General.Users U on DS.UserId = U.UserId
	     where DS.ScoreType = 'SPROU'
	end

	If @pReportType = 'D'
	begin
		Select @pCustomerId AS CustomerId,@pCustomerName as CustomerName, DS.DriverScoreId ,DS.UserId,Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo,
		DS.KmTraveled ,
		DS.OverSpeed ,
		DS.OverSpeedAmount ,
		DS.OverSpeedDistancePercentage ,
		DS.OverSpeedAverage ,
		DS.OverSpeedWeihgtedAverage ,
		DS.StandardDeviation ,
		DS.Variance ,
		DS.VariationCoefficient,	DS.OverSpeedDistance	   from #LocalDriverScore DS 
		order by DS.ScoreType, DS.Score asc 
	end
	else
	begin
		Select @pCustomerId AS CustomerId,@pCustomerName as CustomerName, DS.DriverScoreId ,DS.UserId,DS.Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo , IsNull(DSA.Score, 0) ScoreSpeedAdmin, IsNull(DSR.Score, 0) ScoreSpeedRoute
		,DS.KmTraveled ,
		DS.OverSpeed ,
		DS.OverSpeedAmount ,
		DS.OverSpeedDistancePercentage ,
		DS.OverSpeedAverage ,
		DS.OverSpeedWeihgtedAverage ,
		DS.StandardDeviation ,
		DS.Variance ,
		DS.VariationCoefficient
		,DS.OverSpeedDistance	
		
		from #LocalDriverScore DS
		 left join #LocalDriverScore DSA on DS.UserId = DSA.UserId and DSA.ScoreType = 'SPADM'
		 left join #LocalDriverScore DSR on DS.UserId = DSR.UserId and DSR.ScoreType = 'SPROU'
		
		order by DS.ScoreType, DS.Score asc
	end	
	
	Drop table #LocalDriverScore

end