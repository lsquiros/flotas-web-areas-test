USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleUnitsByGeoFence_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_VehicleUnitsByGeoFence_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/18/2014
-- Description:	Insert or Update Vehicle Groups by GeoFence information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleUnitsByGeoFence_AddOrEdit]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pXmlData VARCHAR(MAX)					--@pName: VehicleGroup Name
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [Efficiency].[VehicleUnitsByGeoFence]
			WHERE [GeoFenceId] = @pGeoFenceId
			
			INSERT INTO [Efficiency].[VehicleUnitsByGeoFence]
					([GeoFenceId]
					,[UnitId]
					,[InsertDate]
					,[InsertUserId])
			SELECT 
					 @pGeoFenceId
					,Row.col.value('./@VehicleUnitId', 'INT') AS [UnitId]
					,GETUTCDATE()
					,@pLoggedUserId
			FROM @lxmlData.nodes('/xmldata/VehicleUnit') Row(col)
			ORDER BY Row.col.value('./@Index', 'INT')
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO