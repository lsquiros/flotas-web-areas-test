USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_EfficiencyFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_EfficiencyFuelsReportBySubUnit_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Efficiency Fuels Report By Sub Unit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_EfficiencyFuelsReportBySubUnit_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	
	DECLARE @lTable TABLE
	(
		 [VehicleId] INT NOT NULL
		,[PlateNumber] VARCHAR(10) NOT NULL
		,[Year] INT NOT NULL
		,[Month] INT NOT NULL
		,[Liters] DECIMAL(16,2) NOT NULL
	)
	INSERT @lTable  EXEC [Control].[Sp_EfficiencyFuelsReportByVehicle_Retrieve] @pCustomerId = @pCustomerId
																				,@pYear = @pYear
																				,@pMonth = @pMonth
																				,@pStartDate = @pStartDate
																				,@pEndDate = @pEndDate
	
	SELECT
		 s.[CostCenterName]
		,t.[Month]
		,t.[Year]
		,SUM([Liters]) AS [Liters]
	FROM @lTable t
		INNER JOIN
			(SELECT
				 a.[VehicleId]
				,b.[Name] AS [CostCenterName]
			FROM [General].[Vehicles] a
				INNER JOIN [General].[VehicleCostCenters] b
					ON a.[CostCenterId] = b.[CostCenterId]
				INNER JOIN [General].[VehicleUnits] c
					ON b.[UnitId] = c.[UnitId]
			WHERE c.[CustomerId] = @pCustomerId) s
		ON t.[VehicleId] = s.[VehicleId]
	GROUP BY s.[CostCenterName], t.[Month], t.[Year]
	
	SET NOCOUNT OFF
END
