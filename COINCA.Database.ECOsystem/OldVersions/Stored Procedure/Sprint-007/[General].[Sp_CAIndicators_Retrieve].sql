USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CAIndicators_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CAIndicators_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa H.
-- Create date: 11/14/2014
-- Description:	Retrieve Indicators for Customer Admin Landing Page
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CAIndicators_Retrieve]
(
	@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		(SELECT count(1) FROM Efficiency.Routes WHERE CustomerId = @pCustomerId) AS Routes, 
		(SELECT COUNT(1) FROM General.Vehicles v WHERE v.CustomerId = @pCustomerId AND v.Active = 1) AS Vehicles, 
		(SELECT COUNT(1) FROM General.DriversUsers D INNER JOIN General.Users U ON D.UserID = U.UserId 
			WHERE D.CustomerId = @pCustomerId AND U.IsDeleted = 0 AND U.IsActive = 1) AS Drivers, 
		(SELECT COUNT(1) FROM Control.CreditCard WHERE CustomerId = @pCustomerId AND StatusId = 7) AS CreditCards
	
    SET NOCOUNT OFF
END
