USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_VehicleFuel_Delete]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Delete Vehicle Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuel_Delete]
(
	@pVehicleFuelId INT						--@pVehicleFuelId: PK of the table
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0            
            DECLARE @lFuelByCreditId INT = 0
            DECLARE @lAssigned DECIMAL(16,2) = 0
			DECLARE @lFuelId INT = 0
			DECLARE @pMonth INT
			DECLARE @pYear INT
			DECLARE @pCustomerId INT
			
			SELECT
				 @lFuelByCreditId = d.[FuelByCreditId]
				,@lFuelId = c.[DefaultFuelId]
				,@pMonth = a.[Month]
				,@pYear = a.[Year]
				,@pCustomerId = b.[CustomerId]
			FROM [Control].[VehicleFuel] a
				INNER JOIN [General].[Vehicles] b
					ON a.[VehicleId] = b.[VehicleId]
				INNER JOIN [General].[VehicleCategories] c
					ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
				INNER JOIN [Control].[FuelsByCredit] d
					ON a.[Month] = d.[Month]
				   AND a.[Year] = d.[Year]
				   AND b.[CustomerId] = d.[CustomerId]
				   AND c.[DefaultFuelId] = d.[FuelId]
			WHERE a.[VehicleFuelId] = @pVehicleFuelId
			
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END		
			
			
			DELETE [Control].[VehicleFuel]
			WHERE [VehicleFuelId] = @pVehicleFuelId
			
			
			SET @lAssigned = COALESCE((SELECT SUM(a.[Amount]) 
				FROM [Control].[VehicleFuel] a
					INNER JOIN [General].[Vehicles] b
						ON a.[VehicleId] = b.[VehicleId]
					INNER JOIN [General].[VehicleCategories] c
						ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
				WHERE a.[Year] = @pYear
				  AND a.[Month] = @pMonth
				  AND b.[CustomerId] = @pCustomerId
				  AND c.[DefaultFuelId] = @lFuelId),0)
				  
			UPDATE [Control].[FuelsByCredit]
			SET [Assigned] = @lAssigned
			WHERE [FuelByCreditId] = @lFuelByCreditId
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO