USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Parameters_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Parameters_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update Parameters information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Parameters_AddOrEdit]
(
	 @pSafeSpeedMargin INT = NULL
	,@pAVLOdometerErrorMargin INT = NULL
	,@pPOSOdometerErrorMargin INT = NULL
	,@pPointArrivalTimeTolerance INT = NULL
	,@pGeoPointErrorMargin INT = NULL
	,@pPasswordEffectiveDays INT = NULL
	,@pPasswordsValidateRepetition INT = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			DECLARE @lCount INT = 0
			SELECT @lCount = COUNT(1) FROM [General].[Parameters]
			
			IF (@lCount = 0)
			BEGIN
				INSERT INTO [General].[Parameters]
						([SafeSpeedMargin]
						,[AVLOdometerErrorMargin]
						,[POSOdometerErrorMargin]
						,[PointArrivalTimeTolerance]
						,[GeoPointErrorMargin]
						,[PasswordEffectiveDays]
						,[PasswordsValidateRepetition]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pSafeSpeedMargin
						,@pAVLOdometerErrorMargin
						,@pPOSOdometerErrorMargin
						,@pPointArrivalTimeTolerance
						,@pGeoPointErrorMargin
						,@pPasswordEffectiveDays
						,@pPasswordsValidateRepetition
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[Parameters]
					SET  [SafeSpeedMargin] = @pSafeSpeedMargin
						,[AVLOdometerErrorMargin] = @pAVLOdometerErrorMargin
						,[POSOdometerErrorMargin] = @pPOSOdometerErrorMargin
						,[PointArrivalTimeTolerance] = @pPointArrivalTimeTolerance
						,[GeoPointErrorMargin] = @pGeoPointErrorMargin
						,[PasswordEffectiveDays] = @pPasswordEffectiveDays
						,[PasswordsValidateRepetition] = @pPasswordsValidateRepetition
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO