USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCatalog_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_AddOrEdit]
GO

USE [ECOSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 03/Oct/2014
-- Description:	Insert or Update Preventive Maintenance information
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_AddOrEdit]
	 @pPreventiveMaintenanceCatalogId INT = NULL	--@pPreventiveMaintenanceID: PK of the table
	,@pCustomerId INT	= NULL				        --@pCustomer: FK of the table Control.Customer
	,@pDescription VARCHAR(500)						--@pDescription: Preventive Maintenance Description
	,@pFrequencyKm FLOAT = NULL						--@pFrequencyKm: Frequency Kilometers
	,@pAlertBeforeKm INT = NULL						--@pAlertBeforeKm: Alert Before Kilometers
	,@pFrequencyMonth FLOAT = NULL					--@pFrequencyMonth: Frequency Monts
	,@pAlertBeforeMonth INT = NULL					--@pAlertBeforeMonth: Alert Before Monts
	,@pFrequencyDate DATE = NULL				--@pFrequencyMonth: Frequency Date
	,@pAlertBeforeDate INT = NULL					--@pAlertBeforeMonth: Alert Before Date
	,@pLoggedUserId INT								--@pLoggedUserId: Logged UserId that executes the operationAS
	,@pRowVersion TIMESTAMP							--@pRowVersion: Timestamp of row to prevent bad updates
AS
BEGIN	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0
        DECLARE @lCode VARCHAR(400)
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END		

		IF (@pPreventiveMaintenanceCatalogID IS NULL)
		BEGIN
			INSERT INTO [General].[PreventiveMaintenanceCatalog]
				  ([CustomerId]
				   ,[Description]
				   ,[FrequencyKm]
				   ,[AlertBeforeKm]
				   ,[FrequencyMonth]
				   ,[AlertBeforeMonth]
				   ,[FrequencyDate]
				   ,[AlertBeforeDate]
				   ,[InsertUserId]
				   ,[InsertDate])
			VALUES(@pCustomerId
				  ,@pDescription
				   ,@pFrequencyKm
				   ,@pAlertBeforeKm
				   ,@pFrequencyMonth
				   ,@pAlertBeforeMonth
				   ,@pFrequencyDate
				   ,@pAlertBeforeDate
				  ,@pLoggedUserId
				  ,GETUTCDATE())		
		END
		ELSE
		BEGIN
			UPDATE [General].[PreventiveMaintenanceCatalog]
				SET [Description] = @pDescription
					,[FrequencyKm] = @pFrequencyKm
					,[AlertBeforeKm] = @pAlertBeforeKm
					,[FrequencyMonth] = @pFrequencyMonth
					,[AlertBeforeMonth] = @pAlertBeforeMonth
					,[FrequencyDate] = @pFrequencyDate
					,[AlertBeforeDate] = @pAlertBeforeDate
					,[ModifyUserId] = @pLoggedUserId
					,[ModifyDate] = GETUTCDATE()
			WHERE [PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId
			  AND [RowVersion] = @pRowVersion
		END
		
		SET @lRowCount = @@ROWCOUNT
            
        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
			
		IF @lRowCount = 0
		BEGIN
			RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
