USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_IntrackReference_Retrieve]    Script Date: 11/06/2014 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_IntrackReference_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_IntrackReference_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_IntrackReference_Retrieve]    Script Date: 11/06/2014 10:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/03/2014
-- Description:	Retrieve IntrackReference information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_IntrackReference_Retrieve]
(
	@pVeicleId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		c.[CustomerId],
		co.[Code] CountryCode, 
		v.[IntrackReference],
		c.[CustomerId] [CustomerId]
	FROM 
		[General].[Customers] c
		INNER JOIN [General].[Countries] co ON c.[CountryId] = co.[CountryId]
		INNER JOIN [General].[Vehicles] v ON c.[CustomerId] = v.[CustomerId]
	WHERE 
		v.[VehicleId] = @pVeicleId
		
    SET NOCOUNT OFF
END






GO


