USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerUsers_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerUsers_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Add Or Edit Customer User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerUsers_AddOrEdit]
(
	 @pCustomerUserId INT = NULL
	,@pUserId INT
	,@pCustomerId INT
	,@pChangeOdometer BIT = 0
	,@pIdentification VARCHAR(50)
	,@pLoggedUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		IF (@pCustomerUserId IS NULL)
		BEGIN
			INSERT INTO [General].[CustomerUsers]
					([UserId]
					,[CustomerId]
					,[Identification]
					,[InsertDate]
					,[InsertUserId])
			VALUES	(@pUserId
					,@pCustomerId
					,@pIdentification
					,GETUTCDATE()
					,@pLoggedUserId)				
		END
		ELSE
		BEGIN
			UPDATE [General].[CustomerUsers]
				SET  [Identification] = @pIdentification
					,[ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
			WHERE [CustomerUserId] = @pCustomerUserId
			
		END
        		
		DECLARE @RoleId NVARCHAR(128) 
		SELECT @RoleId = Id FROM [dbo].[AspNetRoles] WHERE Name = 'CUSTOMER_CHANGEODOMETER'

		DECLARE @AspNetUserId NVARCHAR(128) 
		SELECT @AspNetUserId = AspNetUserId FROM [General].[Users] WHERE UserId = @pUserId

		DECLARE @ExistsChangeOdometer BIT
		SELECT @ExistsChangeOdometer = 1 FROM [dbo].[AspNetUserRoles] UR WHERE UR.UserId = @AspNetUserId AND UR.RoleId = @RoleId
		
		IF @pChangeOdometer = 1 
		BEGIN
			IF @ExistsChangeOdometer = 1 
			BEGIN
				UPDATE [dbo].[AspNetUserRoles] SET RoleId = @RoleId WHERE UserId = @AspNetUserId
			END
			ELSE
			BEGIN
				INSERT INTO [dbo].[AspNetUserRoles] (UserId, RoleId) VALUES (@AspNetUserId,@RoleId)
			END
		END
		ELSE
		BEGIN
			DELETE FROM [dbo].[AspNetUserRoles] WHERE UserId = @AspNetUserId AND RoleId = @RoleId
		END

        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END



