USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByCountry_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomersByCountry_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo Gonz�alez
-- Create date: 19/01/2014
-- Description:	Retrieve Customers By Country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomersByCountry_Retrieve]
(
	@pCountryId INT = NULL, 
	@pPartnerId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT
		 a.[CustomerId]
		,a.[Name] AS [EncryptedName]
		,a.[CurrencyId]
		,b.[Name] AS [CurrencyName]
		,a.[Logo]
		,a.[IsActive]
		,a.[UnitOfCapacityId]
		,a.[IssueForId]
		,c.[Name] AS IssueForStr
		,a.[CreditCardType]
		,CASE a.[CreditCardType] WHEN 'C' THEN 'Cr�dito' WHEN 'D' THEN 'Debito' ELSE '' END AS CreditCardTypeStr
		,b.[Symbol] AS [CurrencySymbol]
		,a.[CountryId]
		,d.[Name] AS UnitOfCapacityName
		,a.[InsertDate]
		,a.[RowVersion]
		--,a.InsurancePartnerId
    FROM [General].[Customers] a
		INNER JOIN [Control].[Currencies] b
			ON a.[CurrencyId] = b.[CurrencyId]
		INNER JOIN [General].[Types] c
			ON c.[TypeId] = a.[IssueForId]		
		INNER JOIN [General].[Types] d
			ON d.[TypeId] = a.[UnitOfCapacityId]
		INNER JOIN [General].[CustomersByPartner] e
			ON e.[CustomerId] = a.[CustomerId]
	WHERE a.[IsDeleted] = 0
	  AND a.[CountryId] = @pCountryId
	  AND e.[PartnerId] = @pPartnerId
	 
    SET NOCOUNT OFF
END
