USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCategories_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleCategories_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update VehicleCategory information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCategories_AddOrEdit]
(
	 @pVehicleCategoryId INT = NULL
	,@pManufacturer VARCHAR(250)
	,@pType VARCHAR(250)
	,@pDefaultFuelId INT
	,@pLiters INT
	,@pVehicleModel VARCHAR(250)
	,@pCustomerId INT
	,@pIcon VARCHAR(MAX) = NULL
	,@pMaximumSpeed INT = NULL
	,@pMaximumRPM INT = NULL
	,@pYear INT = NULL
	,@pWeight NUMERIC(16,2) = NULL
	,@pLoadType INT = NULL
	,@pCylinderCapacity INT = NULL
	,@pFuelsListXml VARCHAR(MAX) = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
	,@pDefaultPerformance decimal
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lxmlData XML = CONVERT(XML,@pFuelsListXml)
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
				IF (@pVehicleCategoryId IS NULL)
			BEGIN
				INSERT INTO [General].[VehicleCategories]
						([Manufacturer]
						,[Type]
						,[DefaultFuelId]
						,[Liters]
						,[VehicleModel]
						,[CustomerId]
						,[Icon]
						,[MaximumSpeed]
						,[MaximumRPM]
						,[Year]
						,[Weight]
						,[LoadType]
						,[CylinderCapacity]
						,[InsertDate]
						,[InsertUserId]
						,DefaultPerformance)
				VALUES	(@pManufacturer
						,@pType
						,@pDefaultFuelId
						,@pLiters
						,@pVehicleModel
						,@pCustomerId
						,@pIcon
						,@pMaximumSpeed
						,@pMaximumRPM
						,@pYear
						,@pWeight
						,@pLoadType
						,@pCylinderCapacity
						,GETUTCDATE()
						,@pLoggedUserId
						,@pDefaultPerformance)
						
				SET @pVehicleCategoryId = SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
				UPDATE [General].[VehicleCategories]
					SET  [Manufacturer] = @pManufacturer
						,[Type] = @pType
						,[DefaultFuelId] = @pDefaultFuelId
						,[Liters] = @pLiters
						,[VehicleModel] = @pVehicleModel
						,[CustomerId] = @pCustomerId
						,[Icon] = @pIcon
						,[MaximumSpeed] = @pMaximumSpeed
						,[MaximumRPM] = @pMaximumRPM
						,[Year] = @pYear
						,[Weight] = @pWeight
						,[LoadType] = @pLoadType
						,[CylinderCapacity] = @pCylinderCapacity
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
						,DefaultPerformance=@pDefaultPerformance
				WHERE [VehicleCategoryId] = @pVehicleCategoryId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            DELETE
				FROM [General].[FuelsByVehicleCategory]
			WHERE [VehicleCategoryId] = @pVehicleCategoryId
	        
			INSERT INTO [General].[FuelsByVehicleCategory]([VehicleCategoryId],[FuelId],[InsertDate],[InsertUserId])
			SELECT 
				 @pVehicleCategoryId
				,RowData.col.value('./@FuelId', 'INT') AS [FuelId]
				,GETUTCDATE()
				,@pLoggedUserId
			FROM @lxmlData.nodes('/xmldata/Fuel') RowData(col)
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO