USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_HobbsMeterPerformanceReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_HobbsMeterPerformanceReportByVehicle_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 02/20/2015
-- Description:	Retrieve Hobbs meter Performance Report By Vehicle
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_HobbsMeterPerformanceReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pKey VARCHAR(800) = NULL			--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT
		 a.[VehicleId]
		,MIN(a.PlateId) AS [PlateId]
		,MIN(b.[ProcessedDate]) AS [MinProcessedDate]
		,MAX(b.[ProcessedDate]) AS [MaxProcessedDate]
		,ISNULL(MIN(b.[GpsStartOdometer]),0) AS [GpsStartOdometer]
		,ISNULL(MAX(b.[GpsEndOdometer]), 0) AS [GpsEndOdometer]
		,SUM(b.[GpsHoursOn]) AS [GpsHoursOn]
		,RIGHT('0' + CAST (FLOOR(SUM(b.[GpsHoursOn])) AS VARCHAR), 2) + ':' + 
			RIGHT('0' + CAST(FLOOR((((SUM(b.[GpsHoursOn]) * 3600) % 3600) / 60)) AS VARCHAR), 2) + ':' + 
			RIGHT('0' + CAST (FLOOR((SUM(b.[GpsHoursOn]) * 3600) % 60) AS VARCHAR), 2) AS [GpsHoursOnStr]
	FROM [General].[Vehicles] a
		INNER JOIN [General].[HobbsMeterByVehicle] b
			ON a.[VehicleId] = b.[VehicleId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND a.[Active] = 1
	  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,b.[ProcessedDate]) = @pMonth
								AND DATEPART(yyyy,b.[ProcessedDate]) = @pYear) OR
									(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND b.[ProcessedDate] BETWEEN @pStartDate AND @pEndDate))
	  AND (@pKey IS NULL 
				OR a.[PlateId] like '%'+@pKey+'%')
	GROUP BY a.[VehicleId]
	
	
    SET NOCOUNT OFF
END
