USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Fuels_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_Fuels_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Fuels_Retrieve]
(
	 @pFuelId INT = NULL,
	 @pKey VARCHAR(800) = NULL,
	 @pCountryId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
	SELECT
		 a.[FuelId]
		,a.[Name]
		,a.[CountryId]
		,a.[RowVersion]
		,b.[Name] AS [CountryName]
    FROM [Control].[Fuels] a
		INNER JOIN [General].[Countries] b
			ON a.[CountryId] = b.[CountryId]
	WHERE (@pFuelId IS NULL OR [FuelId] = @pFuelId)
	  AND (@pCountryId IS NULL OR a.[CountryId] = @pCountryId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR b.[Name] like '%'+@pKey+'%')
	ORDER BY [FuelId] DESC
	
    SET NOCOUNT OFF
END
