USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_ScoreDrivingFormula_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Insurance].[Sp_ScoreDrivingFormula_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes
-- Create date: 24/02/2015
-- Description:	Insert or Update Score Driving Formula information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_ScoreDrivingFormula_AddOrEdit]
(
	   @pScoreDrivingFormulaId INT=null
	  ,@pName varchar(300)
      ,@pScoreDrivingFormulaStr varchar(2000)
      ,@pScoreDrivingFormulaSP varchar(1000)=null
      --,@pIsActive bit=null
      ,@pCustomerId int
	  ,@pRowVersion TIMESTAMP
	  ,@pLoggedUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
           -- IF @pIsActive=1
            --BEGIN
			--	UPDATE [Insurance].ScoreDrivingFormula SET IsActive=0 WHERE CustomerId=@pCustomerId
            --END
            
			IF (@pScoreDrivingFormulaId IS NULL)
			BEGIN
			
				INSERT INTO [Insurance].ScoreDrivingFormula
						(Name,
						 ScoreDrivingFormulaStr,
						 ScoreDrivingFormulaSP,
						 IsPending,
						 IsActive,
					     CustomerId
						,[InsertDate]
						,[InsertUserId]
						)
				VALUES	(@pName
						,@pScoreDrivingFormulaStr
						,@pScoreDrivingFormulaSP
						,1
						,0
						,@pCustomerId
						,GETUTCDATE()
						,@pLoggedUserId
						)
			END
			ELSE
			BEGIN
				
				UPDATE [Insurance].ScoreDrivingFormula
					SET  Name=@pName,
						 ScoreDrivingFormulaStr=@pScoreDrivingFormulaStr,
						 ScoreDrivingFormulaSP=@pScoreDrivingFormulaSP,
						 IsPending=1,
						 IsApproved=0,
						 IsActive=0,
					     CustomerId=@pCustomerId
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId						
				WHERE [CustomerId] = @pCustomerId and ScoreDrivingFormulaId=@pScoreDrivingFormulaId
				  
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	
	SELECT @pCustomerId
	
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO