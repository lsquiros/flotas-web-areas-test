USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Operators_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Operators_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 02/Oct/2014
-- Description:	Operators information
-- =============================================
CREATE PROCEDURE [General].[Sp_Operators_Retrieve]
	  @pCustomerId INT
	 ,@pOperatorId INT = NULL
	 ,@pKey VARCHAR(250) = NULL
AS
BEGIN	
	SET NOCOUNT ON;
	SELECT 
		a.[OperatorId],
		a.[Name],
		a.[Rate],
		b.[CurrencyId],
		b.[Symbol] AS CurrencySymbol,
		a.[RowVersion]
		FROM
		General.Operators a
			INNER JOIN Control.Currencies b 
			ON a.[CurrencyId] = b.[CurrencyId]
		WHERE  (@pCustomerId IS NULL OR a.[CustomerId] = @pCustomerId)
				AND (@pOperatorId IS NULL OR 
				a.[OperatorId] = @pOperatorId)
				AND (@pKey IS NULL 
				OR a.[Name] LIKE '%'+@pKey+'%' 
				OR b.[Name] LIKE '%'+@pKey+'%')
		ORDER BY a.[Name] DESC	
	SET NOCOUNT OFF;
END
