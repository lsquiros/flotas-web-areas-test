USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Control].[Sp_TransactionsVPOSForReverse_Retrieve]    Script Date: 4/15/2015 12:03:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 04/14/2015
-- Description:	Retrieve Transactions VPOS Info For Reverse Operations
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_TransactionsVPOSForReverse_Retrieve]
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT ctv.*
		FROM [Control].[CreditCardTransactionVPOS] ctv
		WHERE ctv.TransactionType = 'REVERSE'
			  AND ctv.Invoice IS NOT NULL
			  AND (SELECT COUNT(*) 
					FROM [Control].[LogTransactionsVPOS] x
					WHERE CONVERT(nvarchar(50), x.CCTransactionVPOSId) = ctv.Invoice
						  AND x.IsSuccess = 1) = 0
	  
    SET NOCOUNT OFF
END
