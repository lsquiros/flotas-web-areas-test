USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Control].[Sp_CardRequestNoCustomer_Retrieve]    Script Date: 05/26/2015 14:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Kenneth Mora Flores
-- Create date: 26/05/2015
-- Description:	Retrieve CardRequest information without customer Id parameter
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CardRequestNoCustomer_Retrieve]
(
	 @pCardRequestId INT = NULL
	,@pPendingOnly BIT = 0
	,@pKey varchar(max)=null
)
AS
BEGIN
	SET NOCOUNT ON
		
	SELECT 
		 a.[CardRequestId]
		,a.[CustomerId]
		,e.[Name] as CustomerName
		,a.[PlateId]
		,a.[DriverName]
		,a.[DriverIdentification]
		,a.[AddressLine1]
		,a.[AddressLine2]
		,a.[PaymentReference]
		,a.[AuthorizedPerson]
		,a.[ContactPhone]
		,a.[EstimatedDelivery]
		,b.[StatusId]
		,CASE WHEN b.[StatusId] IS NULL 
				THEN (SELECT x.[Name]
						FROM [General].[Status] x
					 WHERE x.StatusId = 0)
			ELSE c.[Name] END AS [StatusName]
		,e.[IssueForId]
		,e.[CountryId]
		,a.[StateId]		
		,f.[Name] AS [StateName]
		,a.[CountyId]
		,g.[Name] AS [CountyName]
		,a.[CityId]
		,h.[Name] AS [CityName]
		,a.[DeliveryState]
		,a.[DeliveryCounty]
		,a.[DeliveryCity]
		,a.[RowVersion]
	FROM [Control].[CardRequest] a
		LEFT JOIN [Control].[CreditCard] b
			ON a.[CardRequestId] = b.[CardRequestId]
		   AND a.[CustomerId] = b.[CustomerId]
		LEFT JOIN [General].[Status] c
			ON c.[StatusId] = b.[StatusId]
		INNER JOIN [General].[Customers] e
			ON a.[CustomerId] = e.[CustomerId]
		LEFT JOIN [General].[States] f
			ON a.[StateId] = f.[StateId]
		LEFT JOIN [General].[Counties] g
			ON a.[CountyId] = g.[CountyId]
		LEFT JOIN [General].[Cities] h
			ON a.[CityId] = h.[CityId]		
	WHERE (@pCardRequestId IS NULL OR a.[CardRequestId] = @pCardRequestId)
	  AND (@pKey IS NULL
				OR a.[PlateId] like '%'+@pKey+'%'
				OR a.[AuthorizedPerson] like '%'+@pKey+'%'
				OR a.[ContactPhone] like '%'+@pKey+'%'
				)
	ORDER BY [CardRequestId] DESC
	
    SET NOCOUNT OFF
END
