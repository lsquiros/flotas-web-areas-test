USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NewsByPartners_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_NewsByPartners_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo.
-- Create date: 25/11/2014
-- Description:	Insert or Update New by Partner
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_NewsByPartners_AddOrEdit]
(
	@pNewsId INT = NULL
	,@pPartnerId INT
	,@pTitle VARCHAR(250)
	,@pContent VARCHAR(MAX)
	,@pImage VARCHAR(MAX) = NULL
	,@pIsActive INT
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pNewsId IS NULL)
			BEGIN
				INSERT INTO [General].[NewsByPartner]
						([PartnerId]
						,[Title]
						,[Content]
						,[Image]
						,[IsActive]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pPartnerId
						,@pTitle
						,@pContent
						,@pImage
						,@pIsActive
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[NewsByPartner]
					SET  [Title] = @pTitle
						,[Content] = @pContent
						,[Image] = @pImage
						,[IsActive] = @pIsActive
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [PartnerId] = @pPartnerId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO