USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CardRequest_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CardRequest_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update CustomerCreditCards information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CardRequest_AddOrEdit]
(
	 @pCardRequestId INT = NULL
	,@pCustomerId INT
	,@pPlateId VARCHAR(10) = NULL
	,@pDriverName VARCHAR(250) = NULL
	,@pDriverIdentification VARCHAR(50) = NULL
	,@pStateId INT = NULL
	,@pCountyId INT = NULL
	,@pCityId INT = NULL
	,@pDeliveryState VARCHAR(50) = NULL
	,@pDeliveryCounty VARCHAR(50) = NULL
	,@pDeliveryCity VARCHAR(50) = NULL
	,@pAddressLine1 VARCHAR(1024)
	,@pAddressLine2 VARCHAR(1024) = NULL 
	,@pPaymentReference VARCHAR(50) = NULL
	,@pAuthorizedPerson VARCHAR(520)
	,@pContactPhone VARCHAR(50)
	,@pEstimatedDelivery DATETIME = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCardRequestId IS NULL)
			BEGIN
				INSERT INTO [Control].[CardRequest]
						([CustomerId]
						,[PlateId]
						,[DriverName]
						,[DriverIdentification]
						,[StateId]
						,[CountyId]
						,[CityId]
						,[DeliveryState]
						,[DeliveryCounty]
						,[DeliveryCity]
						,[AddressLine1]
						,[AddressLine2]
						,[PaymentReference]
						,[AuthorizedPerson]
						,[ContactPhone]
						,[EstimatedDelivery]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pPlateId
						,@pDriverName
						,@pDriverIdentification
						,@pStateId
						,@pCountyId
						,@pCityId
						,@pDeliveryState
						,@pDeliveryCounty
						,@pDeliveryCity
						,@pAddressLine1
						,@pAddressLine2
						,@pPaymentReference
						,@pAuthorizedPerson
						,@pContactPhone
						,@pEstimatedDelivery
						,GETUTCDATE()
						,@pLoggedUserId)				
			END
			ELSE
			BEGIN
				UPDATE [Control].[CardRequest]
					SET	 [PlateId] = @pPlateId
						,[DriverName] = @pDriverName
						,[DriverIdentification] = @pDriverIdentification
						,[StateId] = @pStateId
						,[CountyId] = @pCountyId
						,[CityId] = @pCityId
						,[DeliveryState] = @pDeliveryState
						,[DeliveryCounty] = @pDeliveryCounty
						,[DeliveryCity] = @pDeliveryCity
						,[AddressLine1] = @pAddressLine1
						,[AddressLine2] = @pAddressLine2
						,[PaymentReference] = @pPaymentReference
						,[AuthorizedPerson] = @pAuthorizedPerson
						,[ContactPhone] = @pContactPhone
						,[EstimatedDelivery] = @pEstimatedDelivery
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CardRequestId] = @pCardRequestId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO