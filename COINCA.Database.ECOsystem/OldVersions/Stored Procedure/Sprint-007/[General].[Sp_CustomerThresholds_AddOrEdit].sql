USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerThresholds_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerThresholds_AddOrEdit]

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 29/01/2015
-- Description:	Insert or Update Thresholds information
-- ================================================================================================
Create PROCEDURE [General].[Sp_CustomerThresholds_AddOrEdit]
(
	 @pCodingThresholdsId INT=Null
	,@pCustomerId INT 
	,@pRedLower INT 
	,@pRedHigher INT 
	,@pYellowLower INT 
	,@pYellowHigher INT 
	,@pGreenLower INT 
	,@pGreenHigher INT 
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
	,@pCreditPercentageMin INT=NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
		
			IF (@pCodingThresholdsId IS NULL)
			BEGIN
				INSERT INTO [General].[CustomerThresholds]
						([CustomerId]
						,[RedLower]
						,[RedHigher]
						,[YellowLower]
						,[YellowHigher]
						,[GreenLower]
						,[GreenHigher]
						,[InsertDate]
						,[InsertUserId]
						,CreditPercentageMin)
				VALUES	(@pCustomerId
						,@pRedLower
						,@pRedHigher
						,@pYellowLower
						,@pYellowHigher
						,@pGreenLower
						,@pGreenHigher
						,GETUTCDATE()
						,@pLoggedUserId
						,@pCreditPercentageMin)
			END
			ELSE
			BEGIN
				UPDATE [General].[CustomerThresholds]
					SET  [RedLower] = @pRedLower
						,[RedHigher] = @pRedHigher
						,[YellowLower] = @pYellowLower
						,[YellowHigher] = @pYellowHigher
						,[GreenLower] = @pGreenLower
						,[GreenHigher] = @pGreenHigher
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
						,CreditPercentageMin=@pCreditPercentageMin
				WHERE CodingThresholdsId = @pCodingThresholdsId
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

