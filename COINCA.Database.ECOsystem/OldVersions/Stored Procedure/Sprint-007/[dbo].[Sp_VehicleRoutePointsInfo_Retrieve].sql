USE [ECOsystem]
GO

/****** Object:  StoredProcedure [dbo].[Sp_VehicleRoutePointsInfo_Retrieve]    Script Date: 6/30/2015 5:36:20 PM ******/
DROP PROCEDURE [dbo].[Sp_VehicleRoutePointsInfo_Retrieve]
GO

/****** Object:  StoredProcedure [dbo].[Sp_VehicleRoutePointsInfo_Retrieve]    Script Date: 6/30/2015 5:36:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/30/2014
-- Description:	Retrieve PassControl information
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_VehicleRoutePointsInfo_Retrieve]
(
	@pUnitID BIGINT,
	@pStartDate VARCHAR(21),
	@pEndDate VARCHAR(21),
	@pPoints VARCHAR(MAX),
	@pDistanceMin INT,
	@pTimeMin INT
)
AS
BEGIN

DECLARE @separator VARCHAR(MAX) = ','

DECLARE @Splited TABLE
(
	item VARCHAR(MAX)
)

DECLARE @PointsList TABLE
(
	Id BIGINT PRIMARY KEY IDENTITY(1,1),
	PointId BIGINT,
	PointLongitude FLOAT,
	PointLatitude FLOAT
)

DECLARE @ReportsList TABLE
(
	Id BIGINT PRIMARY KEY IDENTITY(1,1),
    Report DECIMAL(20, 0),
    GPSDATETIME DATETIME,
    Longitude FLOAT,
    Latitude FLOAT,
    VSSSpeed INT
)

DECLARE @StopReportsList TABLE
(
	Id BIGINT,
    Report DECIMAL(20, 0),
    GPSDATETIME DATETIME,
    Longitude FLOAT,
    Latitude FLOAT,
    VSSSpeed INT
)

DECLARE @TimeReportsList TABLE
(
	Id BIGINT,
    GPSDATETIME DATETIME,
    Longitude FLOAT,
    Latitude FLOAT,
    First BIT
)

DECLARE @ResultTemp TABLE
(
	Id1 BIGINT NULL,
	GPSDATETIME1 DATETIME NULL, 
	Longitude1 FLOAT NULL,
	Latitude1 FLOAT NULL,
	First1 BIT NULL,
	Id2 BIGINT NULL,
	GPSDATETIME2 DATETIME NULL,
	Longitude2 FLOAT NULL,
	Latitude2 FLOAT NULL,
	First2 BIT NULL,
	seconds int NULL,
	PointId int NULL,
	Distance int NULL,
	TotalDistance float NULL
)

DECLARE @Result TABLE
(
	Id1 BIGINT NULL,
	GPSDATETIME1 DATETIME NULL, 
	Longitude1 FLOAT NULL,
	Latitude1 FLOAT NULL,
	First1 BIT NULL,
	Id2 BIGINT NULL,
	GPSDATETIME2 DATETIME NULL,
	Longitude2 FLOAT NULL,
	Latitude2 FLOAT NULL,
	First2 BIT NULL,
	seconds INT NULL,
	PointId INT NULL,
	Distance INT NULL
)
 
 SET @pPoints = REPLACE(@pPoints,@separator,'''),(''')
 SET @pPoints = 'SELECT * from (values('''+@pPoints+''')) as V(a)' 
 INSERT INTO @Splited
 EXEC (@pPoints)
 INSERT INTO @PointsList 
	([PointId], 
	[PointLongitude], 
	[PointLatitude])
 SELECT	
	SUBSTRING([item], 1,CHARINDEX('*',[item])-1) [PointId],
	SUBSTRING([item], (CHARINDEX('*',[item])+1), ABS(CHARINDEX('*',[item]) - CHARINDEX(' ',[item]))) [PointLongitude],
	SUBSTRING([item], (CHARINDEX(' ',[item])+1), ABS(CHARINDEX(' ',[item]) - LEN([item]))) [PointLatitude]
FROM @Splited

INSERT INTO @ReportsList 
	([Report], 
	[GPSDATETIME], 
	[Longitude], 
	[Latitude], 
	[VSSSpeed])
SELECT 
	r.[Report],
	r.[GPSDATETIME],
	r.[Longitude],
	r.[Latitude],
	r.[VSSSpeed]
FROM 
	[dbo].[Reports] r 
	INNER JOIN [dbo].[Devices] d ON r.[Device] = d.[Device]
WHERE 
	d.[UnitID] = @pUnitID AND 
	r.[GPSDATETIME] >= @pStartDate AND 
	r.[GPSDATETIME] < @pEndDate 
ORDER BY 
	r.[GPSDATETIME] ASC
	
INSERT INTO @StopReportsList 
	([Id], 
	[Report], 
	[GPSDATETIME], 
	[Longitude], 
	[Latitude], 
	[VSSSpeed])
SELECT 
	[Id], 
	[Report], 
	[GPSDATETIME], 
	[Longitude], 
	[Latitude], 
	[VSSSpeed] 
FROM @ReportsList 
WHERE 
	[VSSSpeed] = 0

INSERT INTO @TimeReportsList 
	([Id], 
	[GPSDATETIME], 
	[Longitude], 
	[Latitude], 
	[First])
SELECT 
	MIN([Id]) [Id], 
	MIN([GPSDATETIME]) [GPSDATETIME], 
	[Longitude] [Longitude], 
	[Latitude] [Latitude], 
	1 [First]
FROM @StopReportsList 
GROUP BY 
	[Longitude], 
	[Latitude]
UNION 
SELECT 
	MAX([Id]) [Id], 
	MAX([GPSDATETIME]) [GPSDATETIME], 
	[Longitude] [Longitude], 
	[Latitude] [Latitude], 
	0 [First]
FROM @StopReportsList 
GROUP BY 
	[Longitude], 
	[Latitude]

INSERT INTO @ResultTemp 
	(Id1, 
	GPSDATETIME1, 
	Longitude1, 
	Latitude1, 
	First1, 
	Id2, 
	GPSDATETIME2, 
	Longitude2, 
	Latitude2, 
	First2, 
	seconds)
SELECT 
	a.[Id] [Id1], 
	a.[GPSDATETIME] [GPSDATETIME1], 
	a.[Longitude] [Longitude1], 
	a.[Latitude] [Latitude1], 
	a.[First] [First1], 
	b.[Id] [Id2], 
	b.[GPSDATETIME] [GPSDATETIME2], 
	b.[Longitude] [Longitude2], 
	b.[Latitude] [Latitude2], 
	b.[First] [First2], 
	DATEDIFF([second], a.[GPSDATETIME], b.[GPSDATETIME])
FROM 
	@TimeReportsList a 
	INNER JOIN @TimeReportsList b ON a.[Longitude] = b.[Longitude] AND a.[Latitude] = b.[Latitude] 
WHERE 
	a.First = 1 AND 
	b.First = 0
	and DATEDIFF([second], a.[GPSDATETIME], b.[GPSDATETIME]) > @pTimeMin
ORDER BY 
	[Id1] 

DECLARE @Iter int = 1
DECLARE @MaxRowNum int = (SELECT max([Id]) from @PointsList)

WHILE @Iter <= @MaxRowNum
	BEGIN
		DECLARE @PointId INT = (SELECT [PointId] FROM @PointsList WHERE [Id] = @Iter)
		DECLARE @PointLongitude FLOAT = (SELECT [PointLongitude] FROM @PointsList WHERE [Id] = @Iter)
		DECLARE @PointLatitude FLOAT = (SELECT [PointLatitude] FROM @PointsList WHERE [Id] = @Iter)
	INSERT INTO @Result 
		([Id1], 
		[GPSDATETIME1], 
		[Longitude1], 
		[Latitude1], 
		[First1], 
		[Id2], 
		[GPSDATETIME2], 
		[Longitude2], 
		[Latitude2], 
		[First2], 
		[seconds], 
		[PointId], 
		[Distance])
	SELECT TOP (1) 
		[Id1] [Id1], 
		[GPSDATETIME1] [GPSDATETIME1], 
		[Longitude1] [Longitude1], 
		[Latitude1] [Latitude1], 
		[First1] [First1], 
		[Id2] [Id2], 
		[GPSDATETIME2] [GPSDATETIME2], 
		[Longitude2] [Longitude2], 
		[Latitude2] [Latitude2], 
		[First2] [First2], 
		[seconds] [seconds], 
		@PointId [PointId],
		GEOGRAPHY::STPointFromText('POINT(' + CONVERT(VARCHAR(MAX),[Latitude1])  + ' ' + CONVERT(VARCHAR(MAX),[Longitude1]) + ')',4326).STDistance(GEOGRAPHY::STPointFromText('POINT(' + CONVERT(VARCHAR(MAX),@PointLatitude) + ' ' + CONVERT(VARCHAR(MAX),@PointLongitude) + ')',4326)) [Distance]
	FROM @ResultTemp 
		WHERE
			GEOGRAPHY::STPointFromText('POINT(' + CONVERT(VARCHAR(MAX),[Latitude1])  + ' ' + CONVERT(VARCHAR(MAX),[Longitude1]) + ')',4326).STDistance(GEOGRAPHY::STPointFromText('POINT(' + CONVERT(VARCHAR(MAX),@PointLatitude) + ' ' + CONVERT(VARCHAR(MAX),@PointLongitude) + ')',4326))  <= @pDistanceMin
		order BY 
			GEOGRAPHY::STPointFromText('POINT(' + CONVERT(VARCHAR(MAX),[Latitude1])  + ' ' + CONVERT(VARCHAR(MAX),[Longitude1]) + ')',4326).STDistance(GEOGRAPHY::STPointFromText('POINT(' + CONVERT(VARCHAR(max),@PointLatitude) + ' ' + CONVERT(VARCHAR(MAX),@PointLongitude) + ')',4326)) ASC, 
			[GPSDATETIME1] DESC
		set @Iter = @Iter + 1
	END
	
SELECT 
	[Id1] [Id], 
	[GPSDATETIME1] [Date], 
	[Longitude1] [Longitude], 
	[Latitude1] [Latitude], 
	[Seconds] [Time], 
	[PointId] [PointId], 
	[Distance] [Distance]
FROM @Result 

    SET NOCOUNT OFF
END

GO


