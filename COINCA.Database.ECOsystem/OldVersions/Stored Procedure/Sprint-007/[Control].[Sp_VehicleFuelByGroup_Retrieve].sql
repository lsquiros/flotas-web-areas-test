USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuelByGroup_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_VehicleFuelByGroup_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Vehicle Fuel information by group
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuelByGroup_Retrieve]
(
	 @pVehicleGroupId INT					--@pVehicleGroupId: PK of the table VehicleGroup
	,@pYear INT								--@pYear: Year
	,@pMonth INT							--@pMonth: Month	
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	SELECT
		 b.PlateId
		,b.[Name] AS  [VehicleName]
		,c.[Manufacturer] + ' - ' + c.[VehicleModel] AS VehicleModel
		,d.Name AS [FuelName]
		,f.[Symbol] AS [CurrencySymbol]
		,ISNULL(t.[Amount], CONVERT(DECIMAL(16,2),0)) AS [Amount]
		,ISNULL(t.[Liters],0) AS [Liters]
		,b.[VehicleId]
		,c.[DefaultFuelId]
		,t.[VehicleFuelId]
		,t.[RowVersion]		
	FROM [General].[VehiclesByGroup] a
		INNER JOIN [General].[Vehicles] b
			ON b.[VehicleId] = a.[VehicleId]
		INNER JOIN [General].[VehicleCategories] c
			ON c.[VehicleCategoryId] = b.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] d
			ON d.[FuelId] = c.[DefaultFuelId]
		INNER JOIN [General].[Customers] e
			ON e.[CustomerId] = b.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON f.[CurrencyId] = e.[CurrencyId]
		LEFT JOIN (
				SELECT
					 x.[VehicleId]
					,x.[Amount]
					,x.[Liters]
					,x.[VehicleFuelId]
					,x.[RowVersion]
				FROM [Control].[VehicleFuel] x
				WHERE x.[Year] = @pYear
				  AND x.[Month] = @pMonth) t
			ON t.VehicleId = b.VehicleId
	WHERE a.[VehicleGroupId] = @pVehicleGroupId
	  
    SET NOCOUNT OFF
END
