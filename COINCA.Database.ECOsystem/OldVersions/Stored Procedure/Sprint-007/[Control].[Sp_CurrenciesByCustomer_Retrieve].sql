USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrenciesByCustomer_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CurrenciesByCustomer_Retrieve]
GO
USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Martínez Hernández 
-- Create date: 09/Oct/2014
-- Description:	Retrieve country by customer information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CurrenciesByCustomer_Retrieve]
(
	@pCustomerId INT
)
AS 
BEGIN 
	SET NOCOUNT ON 
	SELECT 
		 a.[CurrencyId]
		,a.[Name]
		,a.[Symbol]
	FROM [Control].[Currencies] a
		INNER JOIN [Control].[PartnerCurrency] b
			ON a.CurrencyId = b.CurrencyId
		INNER JOIN [General].[CustomersByPartner] c
			ON b.[PartnerId] = c.[PartnerId]
		INNER JOIN [General].[Customers] d
			ON d.[CustomerId] = d.[CustomerId]
	WHERE d.[CustomerId] = @pCustomerId
	GROUP BY a.[CurrencyId], a.[Name], a.[Symbol]
	
	SET NOCOUNT OFF
END 