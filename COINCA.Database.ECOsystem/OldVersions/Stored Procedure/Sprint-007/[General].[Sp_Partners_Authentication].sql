USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Partners_Authentication]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Partners_Authentication]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Partner information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Partners_Authentication]
(
	 @pApiUserName VARCHAR(520),
	 @pApiPassword VARCHAR(520)
)
AS
BEGIN

	SET NOCOUNT ON	
	
	SELECT CONVERT( BIT,
			CASE WHEN (SELECT
							COUNT(1)
						FROM [General].[Partners] a
						WHERE a.[ApiUserName] = @pApiUserName
						  AND a.[ApiPassword] = @pApiPassword) = 1
				THEN 1
				ELSE 0
			END) AS [Authenticated]
	
    SET NOCOUNT OFF
END
