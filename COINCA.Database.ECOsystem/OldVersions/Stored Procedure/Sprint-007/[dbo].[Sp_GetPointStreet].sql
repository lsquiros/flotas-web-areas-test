USE [ECOsystem]
GO

/****** Object:  StoredProcedure [dbo].[Sp_GetPointStreet]    Script Date: 5/6/2015 10:49:44 AM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetPointStreet]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_GetPointStreet]
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[Sp_GetPointStreet]
    @Longitude as  decimal(13,10),
		@Latitude  decimal(13,10),	
		@DistancePoints integer,	
		@heading 	integer	
AS

BEGIN
      --TEST
 --    Declare @Latitude  decimal(13,10)=9.967517,
	--@Longitude  decimal(13,10)  = -84.134285 ,
	--@Cantidad  integer =7,
	--@DistancePoints integer =100,
	--@heading  integer =40
	
DECLARE @LatitudeRange1 decimal(13,10) = 0, 
		@LatitudeRange2 decimal(13,10) = 0, 
		@LongitudeRange1  decimal(13,10)  = 0,
		@LongitudeRange2  decimal(13,10)  = 0

SET @LatitudeRange1 = @Latitude - 0.01 
SET @LatitudeRange2 = @Latitude + 0.01
SET @LongitudeRange1 = @Longitude - 0.01
SET @LongitudeRange2 = @Longitude + 0.01

DECLARE @g geography = 'POINT(' + CAST( @Longitude  AS VARCHAR(MAX)) +' ' + CAST( @Latitude  AS VARCHAR(MAX)) + ')';
WITH puntos (pointID,distance,azimuth,Longitude,Latitude,SpeedMax,SpeedMin)
AS
(
	SELECT  top(5)  pointID,  geom.STDistance (@g)as distance, Azimuth ,Longitude,Latitude,SpeedMax,SpeedMin 
	FROM  dbo.RoadPoint--geom.ToString()
	WHERE 
		LATITUDE BETWEEN @LatitudeRange1 AND @LatitudeRange2 AND
		LONGITUDE BETWEEN @LongitudeRange1 AND @LongitudeRange2 AND
		geom.STDistance(@g)  < @DistancePoints --and  SpeedMax > 0 
	ORDER BY geom.STDistance(@g)
)
SELECT pointID,distance,Azimuth , ABS (  @heading - Azimuth) as azimuthdiff ,SpeedMax,SpeedMin FROM  puntos 
--where ABS(  @heading  - asimut )< 45
ORDER BY distance ,ABS(  @heading  - Azimuth ), Azimuth;

END












GO


