USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]    Script Date: 01/19/2015 11:41:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]    Script Date: 01/19/2015 11:41:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo Gonz�lez
-- Create date: 14/01/2015
-- Description:	Retrieve Preventive Maintenance Record By Vehicle Detail
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PreventiveMaintenanceRecordByVehicleDetail_Retrieve]
(
	 @pId INT
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @Id INT
	DECLARE @CatalogId INT
	
	SELECT @Id = PreventiveMaintenanceRecordByVehicleId FROM General.PreventiveMaintenanceRecordByVehicle WHERE PreventiveMaintenanceId = @pId

	IF @id IS NULL
		BEGIN

		SELECT @CatalogId = PreventiveMaintenanceCatalogId FROM General.PreventiveMaintenanceByVehicle WHERE PreventiveMaintenanceId = @pId

			SELECT 
				0 [PreventiveMaintenanceRecordByVehicleDetailId],
				0 [PreventiveMaintenanceRecordByVehicleId],
				[PreventiveMaintenanceCostId], 
				[Description], 
				[Cost],
				CAST(0.00 AS DECIMAL(16, 2)) [Record]
			FROM 
				[General].[PreventiveMaintenanceCost] 
			WHERE 
				[PreventiveMaintenanceCatalogId] = @CatalogId
			ORDER BY 
				Description
				
		END
	ELSE
		BEGIN
		
			SELECT 
				[PreventiveMaintenanceRecordByVehicleDetailId],
				[PreventiveMaintenanceRecordByVehicleId],
				[PreventiveMaintenanceCostId],
				[Description],
				[Cost],
				[Record]
			FROM 
				[General].[PreventiveMaintenanceRecordByVehicleDetail]
			WHERE 
				[PreventiveMaintenanceRecordByVehicleId] = @Id
			ORDER BY
				Description
	
		END


    SET NOCOUNT OFF
END

GO


