USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Customers_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 20/01/2014
-- Description:	Retrieve Customer information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Customers_Retrieve]
(
	@pCustomerId INT = NULL,
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL,
	 @pPartnerId INT=null
)
AS
BEGIN

	SET NOCOUNT ON
	

	SELECT distinct
		 a.[CustomerId]
		,a.[Name] AS [EncryptedName]
		,a.[CurrencyId]
		,b.[Name] AS [CurrencyName]
		,a.[Logo]
		,a.[IsActive]
		,a.[AccountNumber]
		,a.[UnitOfCapacityId]
		,d.[Name] AS [UnitOfCapacityName]
		,a.[IssueForId]
		,c.[Name] AS IssueForStr
		,a.[CreditCardType]
		,CASE a.[CreditCardType] WHEN 'C' THEN 'Crédito' WHEN 'D' THEN 'Débito' ELSE '' END AS CreditCardTypeStr
		,b.[Symbol] AS [CurrencySymbol]
		,a.[CountryId]
		,e.[Name] AS [CountryName]
		,a.[InsertDate]
		,a.[RowVersion]
		,cbp.IsDefault
		--,a.InsurancePartnerId
    FROM [General].[Customers] a
		INNER JOIN [Control].[Currencies] b
			ON a.[CurrencyId] = b.[CurrencyId]
		LEFT JOIN [General].[Types] c
			ON c.[TypeId] = a.[IssueForId]
		INNER JOIN [General].[Types] d
			ON d.[TypeId] = a.[UnitOfCapacityId]
		INNER JOIN [General].[Countries] e
			ON a.[CountryId] = e.[CountryId]
		INNER JOIN 	General.CustomersByPartner cbp
			ON a.CustomerId=cbp.CustomerId
	WHERE a.[IsDeleted]=CAST(0 as bit)
	  and a.IsActive=1
	  AND (@pCustomerId IS NULL OR a.[CustomerId] = @pCustomerId)
	  AND (@pCountryId IS NULL OR a.[CountryId] = @pCountryId)
	  AND (@pPartnerId IS NULL OR cbp.PartnerId=@pPartnerId)
	  /*AND (@pKey IS NULL 
				--OR a.[Name] like '%'+@pKey+'%' -- Encrypted --> Linq
				OR b.[Name] like '%'+@pKey+'%'
				)*/
	ORDER BY [CustomerId] DESC
	
    SET NOCOUNT OFF
END
