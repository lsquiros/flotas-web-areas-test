USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByGroup_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesByGroup_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update VehicleGroup information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesByGroup_AddOrEdit]
(
	 @pVehicleGroupId INT					--@pVehicleGroupId: PK of the table
	,@pXmlData VARCHAR(MAX)					--@pName: VehicleGroup Name
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [General].[VehiclesByGroup]
			WHERE [VehicleGroupId] = @pVehicleGroupId
			
			INSERT INTO [General].[VehiclesByGroup]
					([VehicleGroupId]
					,[VehicleId]
					,[InsertDate]
					,[InsertUserId])
			SELECT 
					 @pVehicleGroupId
					,Row.col.value('./@VehicleId', 'INT') AS [VehicleId]
					,GETUTCDATE()
					,@pLoggedUserId
			FROM @lxmlData.nodes('/xmldata/Vehicle') Row(col)
			ORDER BY Row.col.value('./@Index', 'INT')
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO