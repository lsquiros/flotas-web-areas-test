USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_LicenseExpiration_Alarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_LicenseExpiration_Alarm]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Cristian Martínez 
-- Create date: 24/11/2014
-- Description:	Add Alarm for Expiration of Driver  License 
-- ================================================================================================

create PROCEDURE [General].[Sp_LicenseExpiration_Alarm]
	@pAlarmId INT,
	@pUserId INT, 
	@pCustomerId INT, 
	@pAlarmTriggerId INT,
	@pPeriodicityAlarm INT,
	@pPhone VARCHAR(100), 
	@pEmail VARCHAR (500),
	@pRowVersion TIMESTAMP
	
AS
BEGIN 
	DECLARE @lErrorMessage NVARCHAR(4000)
    DECLARE @lErrorSeverity INT
    DECLARE @lErrorState INT
	DECLARE @lLicenseExpiration DATE
	DECLARE	@lValue INT
	DECLARE @lName VARCHAR(250)
	DECLARE @lMessage VARCHAR (700)
	DECLARE @lNextAlarm DATETIME			
	
	BEGIN TRY 
		SELECT @lLicenseExpiration = a.[LicenseExpiration],
			   @lName = b.[Name] 
		FROM [General].[DriversUsers] a
			INNER JOIN [General].[Users] b
				ON a.[UserId] = b.[UserId]
			INNER JOIN [General].[ParametersByCustomer] c
				ON a.[CustomerId] = c.[CustomerId]
		WHERE a.[UserId] = @pUserId
		  AND a.[CustomerId] = @pCustomerId
	      AND a.[LicenseExpiration] < CONVERT(DATE,DATEADD(Hour, CONVERT(INT, c.Value),GETUTCDATE()))
	      AND c.[ResuourceKey] = 'CUSTOMER_UTC'

	
		IF (@lLicenseExpiration IS NOT NULL)
		BEGIN 
			EXEC [General].[Sp_SendAlarm] @pAlarmId = @pAlarmId,
										  @pAlarmTriggerId = @pAlarmTriggerId,
										  @pPeriodicityAlarm = @pPeriodicityAlarm,
										  @pCustomerId = @pCustomerId,
										  @pName =	@lName,
										  @pPhone = @pPhone, 
										  @pEmail = @pEmail,
										  @pRowVersion = @pRowVersion
										  
		END 
	END TRY 
	BEGIN CATCH 
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH	
END 

			
