USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_LastPerformanceByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_LastPerformanceByVehicle_Retrieve]
GO

USE [ECOSystem]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 23/Oct/2014
-- Description:	Last Perfomance By Vehicle
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_LastPerformanceByVehicle_Retrieve]
(
	@pVehicleId INT
)
AS
BEGIN 
	SET NOCOUNT ON
	IF @pVehicleId IS NOT NULL 
	BEGIN 
		SELECT 
			TOP 1 [Performance]
		FROM 
			[General].[PerformanceByVehicle]
		WHERE [VehicleId] = @pVehicleId
		ORDER BY [PerformanceVehicleId] DESC
	END
	SET NOCOUNT OFF
END
