USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_TimeSlotByVehicle_AddOrEdit]    Script Date: 12/04/2014 15:50:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_TimeSlotByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_TimeSlotByVehicle_AddOrEdit]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_TimeSlotByVehicle_AddOrEdit]    Script Date: 12/04/2014 15:50:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 12/04/2014
-- Description:	Insert or Update TimeSlotByVehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_TimeSlotByVehicle_AddOrEdit]
(
	 @pVehicleId INT						--@pTimeSlotByVehicleId: PK of the table
	,@pJsonTimeSlot VARCHAR(MAX)			--@pJsonTimeSlot: Json TimeSlot
	,@pXmlTimeSlot VARCHAR(MAX)				--@pJsonTimeSlot: Json TimeSlot
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lTimeSlotByVehicleId INT
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			SELECT
				@lTimeSlotByVehicleId = a.[TimeSlotByVehicleId] 
			FROM [General].[TimeSlotByVehicle] a
			WHERE a.[VehicleId] = @pVehicleId
			
			IF (@lTimeSlotByVehicleId IS NULL)
			BEGIN
				INSERT INTO [General].[TimeSlotByVehicle]
						([VehicleId]
						,[JsonTimeSlot]
						,[XmlTimeSlot]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pVehicleId
						,@pJsonTimeSlot
						,@pXmlTimeSlot
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[TimeSlotByVehicle]
					SET  [JsonTimeSlot] = @pJsonTimeSlot
						,[XmlTimeSlot] = @pXmlTimeSlot
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [VehicleId] = @pVehicleId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
			
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


