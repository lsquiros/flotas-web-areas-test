USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerCreditCards_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerCreditCards_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Insert or Update CustomerCreditCards information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerCreditCards_AddOrEdit]
(
	 @pCustomerCreditCardId INT = NULL
	,@pCustomerId INT
	,@pCreditCardHolder VARCHAR(200)
	,@pCreditCardNumber NVARCHAR(520)
	,@pExpirationYear INT
	,@pExpirationMonth INT
	,@pCreditLimit NUMERIC(16,2)
	,@pStatusId INT
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCustomerCreditCardId IS NULL)
			BEGIN
				INSERT INTO [General].[CustomerCreditCards]
						([CustomerId]
						,[CreditCardHolder]
						,[CreditCardNumber]
						,[ExpirationYear]
						,[ExpirationMonth]
						,[CreditLimit]
						,[StatusId]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pCreditCardHolder
						,@pCreditCardNumber
						,@pExpirationYear
						,@pExpirationMonth
						,@pCreditLimit
						,@pStatusId
						,GETUTCDATE()
						,@pLoggedUserId)
						
				SET @lRowCount = @@ROWCOUNT
				SET @pCustomerCreditCardId = SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
				UPDATE [General].[CustomerCreditCards]
					SET  [CreditCardHolder] = @pCreditCardHolder
						,[CreditCardNumber] = @pCreditCardNumber
						,[ExpirationYear] = @pExpirationYear
						,[ExpirationMonth] = @pExpirationMonth
						,[CreditLimit] = @pCreditLimit
						,[StatusId] = @pStatusId
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CustomerCreditCardId] = @pCustomerCreditCardId
				  AND [RowVersion] = @pRowVersion
				  
				SET @lRowCount = @@ROWCOUNT
			END
			
			-----START - Insert || Update CustomerCredits---			
			DECLARE @lCustomerCreditId INT
			
			SET @lCustomerCreditId = COALESCE(
				(SELECT [CustomerCreditId]
					FROM [Control].[CustomerCredits]
				WHERE [CustomerId] = @pCustomerId
				  AND [Year] = DATEPART(year, GETUTCDATE())
				  AND [Month] = DATEPART(month, GETUTCDATE())), -1)
			
			IF( @lCustomerCreditId = -1)
			BEGIN
				INSERT INTO [Control].[CustomerCredits](
						 [CustomerId]
						,[Year]
						,[Month]
						,[CreditAmount]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,DATEPART(year, GETUTCDATE())
						,DATEPART(month, GETUTCDATE())
						,@pCreditLimit
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Control].[CustomerCredits]
				SET  [CreditAmount] = @pCreditLimit
					,[ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
				WHERE [CustomerCreditId] = @lCustomerCreditId
			END
			-----END - Insert || Update CustomerCredits---		
					
            
            -- History control --
			INSERT INTO [General].[CustomerCreditCardsHx]
			   ([CustomerCreditCardId]
			   ,[CustomerId]
			   ,[CreditCardHolder]
			   ,[CreditCardNumber]
			   ,[ExpirationYear]
			   ,[ExpirationMonth]
			   ,[CreditLimit]
			   ,[StatusId]
			   ,[InsertDate]
			   ,[InsertUserId]
			   ,[ModifyDate]
			   ,[ModifyUserId])
			SELECT 
				[CustomerCreditCardId]
			   ,[CustomerId]
			   ,[CreditCardHolder]
			   ,[CreditCardNumber]
			   ,[ExpirationYear]
			   ,[ExpirationMonth]
			   ,[CreditLimit]
			   ,[StatusId]
			   ,[InsertDate]
			   ,[InsertUserId]
			   ,[ModifyDate]
			   ,[ModifyUserId] FROM [General].[CustomerCreditCards] WHERE [CustomerCreditCardId] = @pCustomerCreditCardId
			-- History control --
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO