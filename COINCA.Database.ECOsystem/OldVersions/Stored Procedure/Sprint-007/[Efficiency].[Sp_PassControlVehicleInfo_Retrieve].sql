USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlVehicleInfo_Retrieve]    Script Date: 11/12/2014 11:04:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlVehicleInfo_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_PassControlVehicleInfo_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlVehicleInfo_Retrieve]    Script Date: 11/12/2014 11:04:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/12/2014
-- Description:	Retrieve PassControlVehicleInfo information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_PassControlVehicleInfo_Retrieve]
(
	 @pIntrackReference INT 
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		v.[VehicleId] [VehicleId], 
		v.[IntrackReference] [IntrackReference], 
		v.[PlateId] [PlateId], 
		v.[CostCenterId] [CostCenterId], 
		s.[Name] [CostCenterName]
	FROM 
		[General].[Vehicles] v
		INNER JOIN [General].[VehicleCostCenters] s ON v.[CostCenterId] = s.[CostCenterId]
	WHERE 
		[IntrackReference] in (@pIntrackReference)
		
    SET NOCOUNT OFF
END




GO


