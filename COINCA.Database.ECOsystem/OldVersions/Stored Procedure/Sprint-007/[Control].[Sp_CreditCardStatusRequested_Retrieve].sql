USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardStatusRequested_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardStatusRequested_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CreditCard information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardStatusRequested_Retrieve]
(
	 @pCreditCardId INT = NULL				--@pCreditCardId:Credit Card Id, PK of the table
	,@pKey VARCHAR(800) = NULL				--@pKey: Key to perform search operations
)
AS
BEGIN
	
	SET NOCOUNT ON
			
	SELECT
		 a.[CreditCardId]
		,a.[CustomerId]
		,a.[CreditCardNumber]
		,a.[ExpirationYear]
		,a.[ExpirationMonth]
		,a.[CreditLimit]
		,a.[CreditAvailable]
		,a.[CreditExtra]
		,a.[StatusId]
		,b.[RowOrder] AS [Step]
		,b.[Name] AS [StatusName]
		,d.[Symbol] AS [CurrencySymbol]
		,a.[RowVersion]
    FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
	WHERE (@pCreditCardId IS NULL OR a.[CreditCardId] = @pCreditCardId)
	  AND (@pKey IS NULL 
				--OR a.[Name] like '%'+@pKey+'%'
				--OR b.[Name] like '%'+@pKey+'%'
				)
	  AND b.Code in ('REQUESTED_STATUS_KEY','APPROVED_STATUS_KEY')
	ORDER BY [CreditCardId] DESC
	
    SET NOCOUNT OFF
END
