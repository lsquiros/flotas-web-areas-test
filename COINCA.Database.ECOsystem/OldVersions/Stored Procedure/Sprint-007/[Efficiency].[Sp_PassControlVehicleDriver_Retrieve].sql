USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlVehicleDriver_Retrieve]    Script Date: 11/12/2014 11:04:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_PassControlVehicleDriver_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_PassControlVehicleDriver_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_PassControlVehicleDriver_Retrieve]    Script Date: 11/12/2014 11:04:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/12/2014
-- Description:	Retrieve PassControlVehicleDriver information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_PassControlVehicleDriver_Retrieve]
(
	 @pId VARCHAR(MAX), 
	 @pStartDate VARCHAR(MAX),  
	 @pEndDate VARCHAR(MAX) 
)
AS
BEGIN
	
	SET NOCOUNT ON

    DECLARE @separator varchar(max) = ','
    DECLARE @Splited table(item varchar(max))
    SET @pId = REPLACE(@pId,@separator,'''),(''')
    SET @pId = 'select * from (values('''+@pId+''')) as V(A)' 
    INSERT INTO @Splited
    EXEC (@pId)
    

	SELECT 
		d.[VehicleId] [VehicleId], 
		d.[UserId] [DriverId], 
		u.[Name] [EncryptedDriverName],
		du.[Dallas] [EncryptedDallas],
		ISNULL(d.[InsertDate], GETUTCDATE()) [InDateTime], 
		ISNULL(d.[LastDateDriving], GETUTCDATE()) [OutDateTime]
	FROM 
		[General].[VehiclesByUser] d
		INNER JOIN [General].[Users] u ON d.[UserId] = u.[UserId]
		INNER JOIN [General].[DriversUsers] du ON u.[UserId] = du.[UserId]
	WHERE 
		d.[VehicleId] IN (select item from @Splited) AND 
		(
				(ISNULL(d.InsertDate, GETUTCDATE())  >= @pStartDate AND ISNULL(d.InsertDate, GETUTCDATE()) < @pEndDate) 
			OR
				(ISNULL(d.LastDateDriving, GETUTCDATE()) >= @pStartDate AND ISNULL(d.LastDateDriving, GETUTCDATE()) < @pEndDate)
			OR
				(ISNULL(d.InsertDate, GETUTCDATE()) < @pStartDate AND ISNULL(d.LastDateDriving, GETUTCDATE()) >= @pStartDate)
		)

    SET NOCOUNT OFF
END

GO
