USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_MonthlyClosing_WarningEmails]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_MonthlyClosing_WarningEmails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:		Napole�n Alvarado
-- Create date: 12/04/2014
-- Description:	Send the Monthly Closing Warning Emails
-- ===============================================================
Create Procedure [Control].[Sp_MonthlyClosing_WarningEmails]
(
	 @pYear INT								--@pYear: Year of monthly closing
	,@pMonth INT							--@pMonth: Month of monthly closing
)
AS
BEGIN
	DECLARE @lTo varchar(100) = '',
			@lSubject varchar(300),
			@lMessage nvarchar(1000),
			@lEmail varchar(100),
			@lFuelType varchar(50),
			@lCreditAmount decimal (18,2),
			@lTotal decimal (18,2),
			@lAssigned decimal (18,2),
			@lCustomerId int

    --1. VehicleFuel Amount Sum is greather than FuelByCreditTotal
    Set @lCustomerId = null
	Set @lSubject = 'El monto total de Cr�dito por tipo de Combustible es menor que el total asignado'
    
    Select @lCustomerId = min(FC.CustomerId)
      from Control.FuelsByCredit FC
	 where FC.Year = @pYear
	   and FC.Month = @pMonth
	   and FC.Total < Assigned
	   
	WHILE @lCustomerId is not null
	BEGIN
		Set @lMessage = ''
		--Send Email to Admin
		Set @lTo = General.Fn_GetEmailsRole('CUSTOMER_ADMIN', @lCustomerId, 100)
		
		IF LEN(@lTo) > 0
		BEGIN
			Set @lMessage = 'El monto de Cr�dito por Tipo de Combustible es menor que el monto total asignado para los siguientes combustibles:' + CHAR(13)
			
			Select top 1 @lFuelType = F.Name, @lTotal = FC.Total, @lAssigned = FC.Assigned
			  from Control.FuelsByCredit FC
			 inner join Control.Fuels F on FC.FuelId = F.FuelId
			 where FC.Year = @pYear
			   and FC.Month = @pMonth
			   and FC.CustomerId = @lCustomerId
			   and FC.Total < Assigned
			 order by F.Name
			 
			If @@ROWCOUNT = 0
				Set @lFuelType = null
			
			WHILE @lFuelType is not null
			BEGIN

				Set @lMessage = @lMessage + '- Tipo de Combustible: ' + @lFuelType + ', Monto total: ' + convert(varchar, cast(@lTotal as money), 1) + ', Monto Asignado: ' + convert(varchar, cast(@lAssigned as money), 1) + CHAR(13)
				--Next iteration
				Select top 1 @lFuelType = F.Name, @lTotal = FC.Total, @lAssigned = FC.Assigned
				  from Control.FuelsByCredit FC
				 inner join Control.Fuels F on FC.FuelId = F.FuelId
				 where FC.Year = @pYear
				   and FC.Month = @pMonth
				   and FC.Total < Assigned
				   and FC.CustomerId = @lCustomerId
				   and F.Name > @lFuelType
				 order by F.Name

				If @@ROWCOUNT = 0
					Set @lFuelType = null
			END
			 
			Select @lMessage			 
			Exec General.Sp_Email_AddOrEdit null, @lTo, null, null, @lSubject, @lMessage, 0, 0, null, null 
		END
					
		--Next iteration
		Select @lCustomerId = min(FC.CustomerId)
		  from Control.FuelsByCredit FC
		 where FC.Year = @pYear
		   and FC.Month = @pMonth
		   and FC.Total < Assigned
		   and FC.CustomerId > @lCustomerId
	END

	--2. FuelsByCredit Total Sum is greather than the Credit Amount    
	Set @lSubject = 'El monto del Cr�dito total es menor que la suma de Cr�ditos por tipo de Combustible'

	Select top 1 @lCustomerId = FC.CustomerId, @lCreditAmount = C.CreditAmount, @lTotal = Sum(FC.Total) 
	  from Control.FuelsByCredit FC
	 inner join Control.CustomerCredits C on FC.CustomerId = C.CustomerId and FC.Year = C.Year and FC.Month = C.Month
	 where FC.Year = @pYear
	   and FC.Month = @pMonth
	 group by FC.CustomerId, C.CreditAmount
	having Sum(FC.Total) > C.CreditAmount
	 order by FC.CustomerId
	 
	If @@ROWCOUNT = 0
		Set @lCustomerId = null
		
	WHILE @lCustomerId is not null
	BEGIN
		Set @lMessage = ''
		--Send Email to Admin
		Set @lTo = General.Fn_GetEmailsRole('CUSTOMER_ADMIN', @lCustomerId, 100)
		
		IF LEN(@lTo) > 0
		BEGIN
			Set @lMessage = 'El monto del cr�dito total: ' + convert(varchar, cast(@lCreditAmount as money), 1) + 
							', es menor que la sumatoria de cr�ditos por tipo de combustible: ' + convert(varchar, cast(@lTotal as money), 1)
						 
			Select @lMessage			 
			Exec General.Sp_Email_AddOrEdit null, @lTo, null, null, @lSubject, @lMessage, 0, 0, null, null 
		END
	
		--Next iteration
		Select top 1 @lCustomerId = FC.CustomerId, @lCreditAmount = C.CreditAmount, @lTotal = Sum(FC.Total) 
		  from Control.FuelsByCredit FC
		 inner join Control.CustomerCredits C on FC.CustomerId = C.CustomerId and FC.Year = C.Year and FC.Month = C.Month
		 where FC.Year = @pYear
		   and FC.Month = @pMonth
		   and C.CustomerId > @lCustomerId
		 group by FC.CustomerId, C.CreditAmount
		having Sum(FC.Total) > C.CreditAmount
		 order by FC.CustomerId
		 
		If @@ROWCOUNT = 0
			Set @lCustomerId = null
	END	 

	--3. Assigned in Credit Cards is greather than the Credit Amount    
	Set @lSubject = 'El monto del Cr�dito total es menor que el monto asignado a tarjetas de cr�dito'
	Select top 1 @lCustomerId = C.CustomerId, @lCreditAmount = C.CreditAmount, @lAssigned = C.CreditAssigned 
	  from Control.CustomerCredits C 
	 where C.Year = @pYear
	   and C.Month = @pMonth
	   and C.CreditAmount < C.CreditAssigned
	 order by C.CustomerId
	 
	If @@ROWCOUNT = 0
		Set @lCustomerId = null
		
	WHILE @lCustomerId is not null
	BEGIN
		Set @lMessage = ''
		Set @lTo = General.Fn_GetEmailsRole('CUSTOMER_ADMIN', @lCustomerId, 100)
		--Send Email to Admin
		
		IF LEN(@lTo) > 0
		BEGIN
			Set @lMessage = 'El monto del cr�dito total: ' + convert(varchar, cast(@lCreditAmount as money), 1) + 
							', es menor que el monto asignado a tarjetas de cr�dito: ' + convert(varchar, cast(@lAssigned as money), 1)
			Select @lMessage			 
			Exec General.Sp_Email_AddOrEdit null, @lTo, null, null, @lSubject, @lMessage, 0, 0, null, null 
		END
	
		--Next iteration
		Select top 1 @lCustomerId = C.CustomerId, @lCreditAmount = C.CreditAmount, @lAssigned = C.CreditAssigned 
		  from Control.CustomerCredits C 
		 where C.Year = @pYear
		   and C.Month = @pMonth
		   and C.CreditAmount < C.CreditAssigned
		   and C.CustomerId > @lCustomerId
		 order by C.CustomerId
		 
		If @@ROWCOUNT = 0
			Set @lCustomerId = null
	END	 

END