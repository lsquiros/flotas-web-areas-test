USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreSpeedPerMonthJob]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_ScoreSpeedPerMonthJob]
GO
USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [Operation].[Sp_ScoreSpeedPerMonthJob]
AS		
Begin
Declare @ldt_Start_Job datetime,
		@ldt_End_Job datetime,
		@lvc_Job varchar(30),
		@lc_status char(1),
		@li_error int = 0,
		@lvc_error_ref varchar(100),
		@li_CustomerId Int,
		@li_Month Int,
		@li_Year Int

Declare @Scores table
	(DriverScoreId int,
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 Score Float,
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float)	 

	Set @lvc_Job = 'ScoreSpeedPerMonth'
	Set @ldt_Start_Job = GETDATE()
	Set @li_Month = DATEPART(MM, @ldt_Start_Job)
	--Set @li_Month = 10 --Para Pruebas
	Set @li_Year = DATEPART(YY, @ldt_Start_Job)
	
	declare @pInitialDate datetime=  CAST(
							  CAST(@li_Year AS VARCHAR(4)) +
							  RIGHT('0' + CAST(@li_Month AS VARCHAR(2)), 2) +
							  RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
	declare @pFinalDate datetime=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	
	--Borrar registros previos del mes
	Delete Operation.DriversScores
	 where ScoreMonth = @li_Month
	   and ScoreYear = @li_Year
	   
	Select @li_CustomerId = MIN(D.CustomerId)
	  from General.DriversUsers D
	  inner join General.Users U on D.UserId = U.UserId
	 where U.IsActive = 1   

	While @li_CustomerId is not null
	begin
		--Generar registros de c�lculo por l�mite administrativo
		Insert into @Scores
			(DriverScoreId, UserId, Name, Identification, Score,KmTraveled,
						OverSpeed,
						OverSpeedAmount,
						OverSpeedDistancePercentage,
						OverSpeedAverage,
						OverSpeedWeihgtedAverage,
						StandardDeviation,
						Variance,
						VariationCoefficient
					   ,OverSpeedDistance)
		Exec Operation.Sp_GetScoreSpeedAllDriver @li_CustomerId, null, null, @pInitialDate, @pFinalDate, 'A', 'J'
		
		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end
		
		Insert into Operation.DriversScores
			(CustomerId, UserId, ScoreYear, ScoreMonth, ScoreType, Score,KmTraveled,
						OverSpeed,
						OverSpeedAmount,
						OverSpeedDistancePercentage,
						OverSpeedAverage,
						OverSpeedWeihgtedAverage,
						StandardDeviation,
						Variance,
						VariationCoefficient,OverSpeedDistance)
		Select @li_CustomerId, UserId, @li_Year, @li_Month, 'SPADM', Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,OverSpeedDistance
		  from @Scores

		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end

		Delete @Scores
		
		--Generar registros de c�lculo por l�mite de carretera
		Insert into @Scores
			(DriverScoreId, UserId, Name, Identification, Score,KmTraveled,
						OverSpeed,
						OverSpeedAmount ,
						OverSpeedDistancePercentage,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,OverSpeedDistance)
		Exec Operation.Sp_GetScoreSpeedAllDriver @li_CustomerId, null, null, @pInitialDate, @pFinalDate, 'R', 'J'
		
		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end
		
		Insert into Operation.DriversScores
			(CustomerId, UserId, ScoreYear, ScoreMonth, ScoreType, Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
		Select @li_CustomerId, UserId, @li_Year, @li_Month, 'SPROU', Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance
		  from @Scores

		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end

		Delete @Scores

		--Siguiente Customer
		Select @li_CustomerId = MIN(D.CustomerId)
		  from General.DriversUsers D
		  inner join General.Users U on D.UserId = U.UserId
		 where U.IsActive = 1   
		   and D.CustomerId > @li_CustomerId
	end

	If @li_error <> 0 
	begin
		Set @lc_status = 'E' --Error
		Set @lvc_error_ref = 'Customer Id: ' + @li_CustomerId
	end
	else
	begin
		Set @lc_status = 'T' --Terminado		
	end

	--Insertar datos en Log
	Set @ldt_End_Job = GETDATE()

	Insert into Operation.JobsLog
		(JobID, StartDate, EndDate, Status, ErrorNumber, ErrorReference)
	values
		(@lvc_Job, @ldt_Start_Job, @ldt_End_Job, @lc_status, @li_error, @lvc_error_ref)
		
end


GO


--MODIFICADO