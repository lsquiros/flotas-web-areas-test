USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCostCenters_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleCostCenters_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update VehicleCostCenters information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCostCenters_AddOrEdit]
(
	 @pCostCenterId INT = NULL				--@pCostCenterId: PK of the table
	,@pName VARCHAR(250)					--@pName: VehicleCostCenter Name
	,@pCode VARCHAR(20)						--@@pCode: VehicleCostCenter Code
	,@pUnitId INT							--@pUnitId: FK of the table General.VehicleUnits
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCostCenterId IS NULL)
			BEGIN
				INSERT INTO [General].[VehicleCostCenters]
						([Name]
						,[Code]
						,[UnitId]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pName
						,@pCode
						,@pUnitId
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [General].[VehicleCostCenters]
					SET  [Name] = @pName
						,[Code] = @pCode
						,[UnitId] = @pUnitId
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [CostCenterId] = @pCostCenterId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO