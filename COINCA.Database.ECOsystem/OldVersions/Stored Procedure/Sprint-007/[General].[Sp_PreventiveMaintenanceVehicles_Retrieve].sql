USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceVehicles_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceVehicles_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================
-- Author:		Danilo Hidalgo.
-- Create date: 07/01/2015
-- Description:	Preventive Maintenance Record By Vehicle information
-- ===========================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceVehicles_Retrieve]
	 @pCustomerId INT
	,@pPreventiveMaintenanceCatalogId INT = NULL
	,@pKey VARCHAR(MAX) = NULL
AS
BEGIN 
	SET NOCOUNT ON;
	
		SELECT DISTINCT
			d.[PreventiveMaintenanceCatalogId],
			a.[VehicleId], 
			a.[PlateId], 
			a.[Name] as [VehicleName], 
			c.[Name] as [CostCenterName], 
			b.[Type] as [TypeName]
		FROM [General].[Vehicles] a 
			INNER JOIN [General].[VehicleCategories] b ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
			INNER JOIN [General].[VehicleCostCenters] c ON a.[CostCenterId] = c.[CostCenterId]
			INNER JOIN [General].[PreventiveMaintenanceByVehicle] d ON a.[VehicleId] = d.[VehicleId]
		WHERE d.[PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId
			AND a.[CustomerId] = @pCustomerId
			AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR a.[PlateId] like '%'+@pKey+'%')
			ORDER BY a.[VehicleId] DESC
	
	SET NOCOUNT OFF;
END

