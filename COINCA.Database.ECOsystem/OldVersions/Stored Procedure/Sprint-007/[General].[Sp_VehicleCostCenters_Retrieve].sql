USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCostCenters_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleCostCenters_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleCostCenters information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCostCenters_Retrieve]
(
	 @pCostCenterId INT = NULL					--@pCostCenterId: PK of the table
	,@pCustomerId INT							--@pCustomerId: FK of the table General.Customers
	,@pKey VARCHAR(800) = NULL					--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[CostCenterId]
		,a.[Name]
		,a.[Code]
		,a.[UnitId]
		,b.[Name] AS [UnitName]
		,d.[Code] AS [CountryCode]
		,a.[RowVersion]
    FROM [General].[VehicleCostCenters] a
		INNER JOIN [General].[VehicleUnits] b
			ON a.[UnitId] = b.[UnitId]
		INNER JOIN [General].[Customers] c
			ON b.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Countries] d
			ON c.[CountryId] = d.[CountryId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND (@pCostCenterId IS NULL OR [CostCenterId] = @pCostCenterId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR a.[Code] like '%'+@pKey+'%'
				OR b.[Name] like '%'+@pKey+'%')
	ORDER BY [CostCenterId] DESC
	
    SET NOCOUNT OFF
END
