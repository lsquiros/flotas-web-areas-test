USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionLitersByCredit_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionLitersByCredit_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Martinez H.
-- Create date: 21/Oct/2014
-- Description:	Retrieve Transaction Credit information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionLitersByCredit_Retrieve]
(
	 @pCustomerId INT		--@pCustomerId: Customer Id 
	,@pAmount DECIMAL(16,2) --@pAmount: Amount
)
AS
BEGIN
	SET NOCOUNT ON
		DECLARE @lExchangeRate DECIMAL(16,8) = 1.0
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	
	SELECT
		@lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[PartnerId] = b.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = b.[CustomerId] AND c.[CurrencyId] = a.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND a.EndDate IS NULL
	  
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId

	SELECT 
		CONVERT(DECIMAL(16,2), @pAmount/(t.LiterPrice*@lExchangeRate)) AS Liters
	FROM [General].[Customers] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[CustomerId] = b.[CustomerId]
		INNER JOIN [Control].[PartnerFuel] c
		ON c.[PartnerId] = b.[PartnerId]
		LEFT JOIN (
					SELECT
						x.[FuelId], x.[LiterPrice] 
					FROM [Control].[PartnerFuel] x
						INNER JOIN [General].[CustomersByPartner] y
							ON x.[PartnerId] = y.[PartnerId]
						INNER JOIN [General].[Customers] z
							ON y.[CustomerId] = z.[CustomerId]
					WHERE z.CustomerId = @pCustomerId
					  AND x.[EndDate] IS NULL) t
		ON c.[FuelId] = t.[FuelId]
	WHERE a.CustomerId = @pCustomerId
	
	SET NOCOUNT OFF
END