USE [ECOSystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByPartners_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomersByPartners_Retrieve] 
GO
USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 10/02/2015
-- Description:	Retrieve Customers By Partner
-- ================================================================================================
create PROCEDURE [General].[Sp_CustomersByPartners_Retrieve] 
( @pPartnerId Int 
 )
AS
BEGIN

	Select DISTINCT  a.[CustomerId]
		,a.[Name] AS [EncryptedName]
		,a.[CurrencyId]
		,b.[Name] AS [CurrencyName]
		,a.[Logo]
		,a.[IsActive]
		,a.[AccountNumber]
		,a.[UnitOfCapacityId]
		,d.[Name] AS [UnitOfCapacityName]
		,a.[IssueForId]
		,c.[Name] AS IssueForStr
		,a.[CreditCardType]
		,CASE a.[CreditCardType] WHEN 'C' THEN 'Crédito' WHEN 'D' THEN 'Débito' ELSE '' END AS CreditCardTypeStr
		,b.[Symbol] AS [CurrencySymbol]
		,a.[CountryId]
		,e.[Name] AS [CountryName]
		,a.[InsertDate]
		,a.[RowVersion]
		,cbp.IsDefault
		
       from General.Customers a LEFT JOIN General.[CustomersByPartner] cbp  ON cbp.CustomerId=a.CustomerId
       INNER JOIN [Control].[Currencies] b
			ON a.[CurrencyId] = b.[CurrencyId]
		LEFT JOIN [General].[Types] c
			ON c.[TypeId] = a.[IssueForId]
		INNER JOIN [General].[Types] d
			ON d.[TypeId] = a.[UnitOfCapacityId]
		INNER JOIN [General].[Countries] e
			ON a.[CountryId] = e.[CountryId]

        where PartnerId=@pPartnerId and a.IsActive=1 and a.IsDeleted=0
END
