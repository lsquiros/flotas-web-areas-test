USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_PassControl_Process]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_PassControl_Process]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Improvement: Gerald Solano
-- Create date: 16/07/2015
-- Description:	Process and insert the PassControl information in DB
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_PassControl_Process]
(
	@pConsultDate Datetime null
)
AS
BEGIN
	
DECLARE @CurrentDayPoints TABLE
(
	Id bigint Primary Key IDENTITY(1,1),
	VehicleId int,
	Device int,
	UnitID decimal(20,0),
	PointId int,
	PointName varchar(250),
	CheckPointTime time,
	UserCheckPoint time,
	StoppingTime int,
	[State] varchar(20), -- VISITADO, NO VISITADO, PENDIENTE
	[PointLatitude] float,
	[PointLongitude] float,
	CurrentRouteDay char(1),
	ControlDay int,
	ControlMonth int,
	ControlYear int
)
CREATE TABLE #PassControlTemp (
	Id bigint Primary Key IDENTITY(1,1),
	[UnitID] VARCHAR(25) null,
	[Device] int null,
	[InDateTime] datetime null,
	[OutDateTime] datetime null
)

-- OBTENEMOS EL D�A ACTUAL O EL ESTABLECIDO EN LA CONSULTA
DECLARE @CurrentDate Datetime = GETDATE() 

IF @pConsultDate IS NOT NULL
BEGIN
	SET @CurrentDate = @pConsultDate
END 

DECLARE @CurrentRouteDay CHAR(1) 
DECLARE @DayName VARCHAR(100)
Declare @CurrentDay int = DATEPART(DD, @CurrentDate)
Declare @CurrentMonth int = DATEPART(MM, @CurrentDate)
Declare @CurrentYear int = DATEPART(YY, @CurrentDate)

SELECT @DayName = DATENAME(weekday, @CurrentDate)
--/////////////////////////

-- ESTABLECEMOS EL D�A A CONSULTAR EN LAS RUTAS
SELECT @CurrentRouteDay = 
	   CASE @DayName
         WHEN 'Monday' THEN 'M' 
         WHEN 'Tuesday' THEN 'K' 
         WHEN 'Wednesday' THEN 'M'
         WHEN 'Thursday' THEN 'J'
		 WHEN 'Friday' THEN 'V' 
		 WHEN 'Saturday' THEN 'S' 
		 WHEN 'Sunday' THEN 'D'
         ELSE ''
END
-- ///////////////////////////////////////////////


-- OBTENEMOS LOS PUNTOS DE RUTA CON LOS VEHICULOS ASIGNADOS PARA TODAS LAS RUTAS DEL D�A ACTUAL

INSERT INTO @CurrentDayPoints(VehicleId, Device, UnitID, PointId, PointName, CheckPointTime, PointLatitude, PointLongitude, 
							  CurrentRouteDay,ControlDay, ControlMonth,	ControlYear)
SELECT vr.[VehicleId], v.[DeviceReference], d.[UnitID], 
       rp.[PointId], rp.[Name], rp.[Time], rp.[Latitude], rp.[Longitude], 
	   @CurrentRouteDay, @CurrentDay, @CurrentMonth, @CurrentYear
FROM [Efficiency].[VehicleByRoute] vr
	INNER JOIN [Efficiency].[RoutesPoints] rp ON rp.[RouteId] = vr.[RouteId] 
	INNER JOIN [General].[Vehicles] v ON v.[VehicleId] = vr.[VehicleId]
	INNER JOIN [dbo].[Devices] d ON d.[Device] = v.[DeviceReference]
WHERE vr.[Days] LIKE '%'+ @CurrentRouteDay +'%' AND
      vr.[RouteId] IN(
			SELECT r.[RouteId] FROM [Efficiency].[Routes] r
			WHERE [Active] = 1 )
			--AND [CustomerId] = 10)
--//////////////////////////////////////////////////////////////////////////////////////////////

--ESTABLECEMOS LOS ESTADOS POR DEFECTO COMO PENDIENTES
UPDATE @CurrentDayPoints
SET [State] = 'PENDIENTE'
--////////////////////////////////////////////////////

-- OBTENEMOS LAS FECHAS DE CONSULTA 
DECLARE @CurrentDateFirstHour Datetime = DATEADD(dd, DATEDIFF(dd,0,@CurrentDate), 0)
DECLARE @CurrentDateWithDiff Datetime  = DATEADD(hour,-6, @CurrentDate)  -- -6 = Diferencia horaria 
DECLARE @CurrentTime time  = CONVERT (time, @CurrentDateWithDiff)
DECLARE @IndexRoutePoint int 

--RECORREMOS LOS PUNTOS QUE ANTERIORES A LA HORA DEL D�A ACTUAL Y VALIDAMOS SU POSICI�N SEGUN EL PUNTO DE RUTA Y LA HORA DE LLEGADA
SELECT @IndexRoutePoint = MIN(Id) FROM @CurrentDayPoints
where CheckPointTime <= @CurrentTime

WHILE @IndexRoutePoint IS NOT NULL
BEGIN
		-- OBTENEMOS LOS DATOS A CONSULTAR
		DECLARE @tempID int 
		DECLARE @tempPointLat float 
		DECLARE @tempPointLong float 
		DECLARE @tempUnitID decimal(20,0)
		DECLARE @tempCheckPointTime time

		SELECT @tempID = PointId,
			   @tempUnitID = UnitID,
			   @tempPointLat = PointLatitude,
			   @tempPointLong = PointLongitude,	
			   @tempCheckPointTime = CheckPointTime
		FROM @CurrentDayPoints
		WHERE Id = @IndexRoutePoint

		-- ESTABLECEMOS LOS TIPOS DE DATOS PARA CONSUMIR LA FUNCION Y VALIDAMOS LA INSRCI�N TEMPORAL
		DECLARE @varUnitID VARCHAR(max) = CAST(@tempUnitID AS VARCHAR(MAX))
		DECLARE @varDateINI VARCHAR(21) = CONVERT(VARCHAR, @CurrentDateFirstHour, 120)
		DECLARE @varDateFINI VARCHAR(21) = CONVERT(VARCHAR, @CurrentDate, 120)

		-- CONSUMIMOS LA FUNCION QUE PROCESA LAS HORAS DEL VEHICULO EN EL PUNTO RECORRIDO ACTUALMENTE
		INSERT INTO #PassControlTemp(UnitID, Device, InDateTime, OutDateTime)
		SELECT * FROM Fn_PassControl_Retrieve
					   (@tempPointLong, 
						@tempPointLat, 
						300, --- 300 mts = DISTANCIA DE CONSULTA ALREDEDOR DEL PUNTO RECORRIDO
						@varUnitID,
						@varDateINI, 
						@varDateFINI)	
		

		-- VALIDAMOS LOS DATOS DEVUELTOS POR LA FUNCI�N		
		DECLARE @CountRows int 
		SELECT @CountRows = COUNT(*) FROM #PassControlTemp	
		
		IF(@CountRows > 0) -- SI HAY DATOS VALIDAMOS LOS TIEMPOS DE LLEGADA 
		BEGIN
			
			DECLARE @InDateTime time
			DECLARE @OutDateTime time
			DECLARE @StopTime int
					
			SELECT TOP(1) @InDateTime = InDateTime,
				   @OutDateTime = OutDateTime,
				   @StopTime = DATEDIFF(minute, InDateTime, OutDateTime)
			FROM #PassControlTemp
			ORDER by DATEDIFF(minute, InDateTime, OutDateTime) DESC

			-- VALIDAMOS SI EL TIEMPO ES MENOR O IGUAL AL PUNTO DE RUTA
			IF(@InDateTime <= @tempCheckPointTime)
			BEGIN
				UPDATE @CurrentDayPoints
				SET [State] = 'VISITADO',
				    [StoppingTime] = @StopTime,
					[UserCheckPoint] = @InDateTime
				WHERE Id = @IndexRoutePoint
			END
			-- VALIDAMOS SI EL TIEMPO ES MAYOR AL PUNTO DE RUTA
			ELSE IF(@InDateTime > @tempCheckPointTime)
			BEGIN
				UPDATE @CurrentDayPoints
				SET [State] = 'TARDIO',
					[StoppingTime] = @StopTime,
					[UserCheckPoint] = @InDateTime
				WHERE Id = @IndexRoutePoint
			END
			-- SI NO ES DEFINIDO (NULL)
			ELSE IF(@InDateTime IS NULL)
			BEGIN
				UPDATE @CurrentDayPoints
				SET [State] = 'NO_VISITADO'
				WHERE Id = @IndexRoutePoint
			END

			-- LIMPIAMOS TABLA PARA LA SIGUIENTE ITERACI�N
			DELETE FROM #PassControlTemp
		END
		ELSE 
		BEGIN
		-- NO SE ENCONTRARON DATOS PARA ESTE ROUTE POINT
		-- ESTABLECEMOS EL PUNTO COMO NO VISITADO DEBIDO A QUE 
		-- A LA HORA ACTUAL YA DEBER�A DE HABER AL MENOS UN REGISTRO

			UPDATE @CurrentDayPoints
			SET [State] = 'NO_VISITADO'
			WHERE Id = @IndexRoutePoint

		-- //////////////////////////////////////////////
		END

	-- SIGUIENTE INDICE A RECORRER
	SELECT @IndexRoutePoint = MIN(Id) FROM @CurrentDayPoints
	WHERE CheckPointTime <= @CurrentTime AND Id > @IndexRoutePoint

END -- END WHILE //////////////////////////////////////////////////////////


-- FINALIZAMOS EL PROCESO REMOVIENDO EL PROCESO ANTERIOR DEL D�A CONSULTADO 
-- E INSERTANDO LOS NUEVOS ESTADOS

DECLARE @CountRows2 int 
SELECT @CountRows2 = COUNT(*) FROM @CurrentDayPoints	
		
IF(@CountRows2 > 0) -- VALIDAMOS SI HAY DATOS
BEGIN
	-- ELIMINAMOS LOS DATOS ANTERIORMENTE PROCESADOS
	DELETE FROM [Efficiency].[PassControl]
	WHERE [ControlDay] = @CurrentDay AND
		  [ControlMonth] = @CurrentMonth AND
		  [ControlYear] = @CurrentYear
	--///////////////////////////////////////////////

    -- INSERTAMOS LOS DATOS DEL D�A CONSULTADO
	INSERT INTO [Efficiency].[PassControl](
			[VehicleId], [Device], [UnitID],[PointId], [PointName],
			[CheckPointTime], [UserCheckPoint], [StoppingTime],
			[State], [PointLatitude], [PointLongitude], [CurrentRouteDay],
			[ControlDay], [ControlMonth], [ControlYear], [InsertDate])
	SELECT 	VehicleId, Device, UnitID, PointId,	PointName,
			CheckPointTime,	UserCheckPoint,	StoppingTime,
			[State], [PointLatitude], [PointLongitude],	CurrentRouteDay,
			ControlDay, ControlMonth, ControlYear, GETDATE()
	FROM @CurrentDayPoints
	-- //////////////////////////////////////////
END

DROP TABLE #PassControlTemp
--///////////////////////////////////////////////////////////////////////

END

GO


