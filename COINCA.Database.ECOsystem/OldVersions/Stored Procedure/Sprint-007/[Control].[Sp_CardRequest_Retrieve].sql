USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CardRequest_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CardRequest_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes
-- Create date: 09/01/2015
-- Description:	Retrieve CardRequest information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CardRequest_Retrieve]
(
	 @pCardRequestId INT = NULL
	,@pCustomerId INT
	,@pPendingOnly BIT = 0
	,@pKey varchar(max)=null
)
AS
BEGIN
	SET NOCOUNT ON
		
	SELECT 
		 a.[CardRequestId]
		,a.[CustomerId]
		,a.[PlateId]
		,a.[DriverName]
		,a.[DriverIdentification]
		,a.[AddressLine1]
		,a.[AddressLine2]
		,a.[PaymentReference]
		,a.[AuthorizedPerson]
		,a.[ContactPhone]
		,a.[EstimatedDelivery]
		,b.[StatusId]
		,CASE WHEN b.[StatusId] IS NULL 
				THEN (SELECT x.[Name]
						FROM [General].[Status] x
					 WHERE x.StatusId = 0)
			ELSE c.[Name] END AS [StatusName]
		,e.[IssueForId]
		,e.[CountryId]
		,a.[StateId]		
		,f.[Name] AS [StateName]
		,a.[CountyId]
		,g.[Name] AS [CountyName]
		,a.[CityId]
		,h.[Name] AS [CityName]
		,a.[DeliveryState]
		,a.[DeliveryCounty]
		,a.[DeliveryCity]
		,a.[RowVersion]
	FROM [Control].[CardRequest] a
		LEFT JOIN [Control].[CreditCard] b
			ON a.[CardRequestId] = b.[CardRequestId]
		   AND a.[CustomerId] = b.[CustomerId]
		LEFT JOIN [General].[Status] c
			ON c.[StatusId] = b.[StatusId]
		INNER JOIN [General].[Customers] e
			ON a.[CustomerId] = e.[CustomerId]
		LEFT JOIN [General].[States] f
			ON a.[StateId] = f.[StateId]
		LEFT JOIN [General].[Counties] g
			ON a.[CountyId] = g.[CountyId]
		LEFT JOIN [General].[Cities] h
			ON a.[CityId] = h.[CityId]		
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pCardRequestId IS NULL OR a.[CardRequestId] = @pCardRequestId)
	  AND (@pPendingOnly = 0 OR b.[CreditCardId] IS NULL)
	  AND (@pKey IS NULL
				OR a.[PlateId] like '%'+@pKey+'%'
				OR a.[AuthorizedPerson] like '%'+@pKey+'%'
				OR a.[ContactPhone] like '%'+@pKey+'%'
				)
	ORDER BY [CardRequestId] DESC
	
    SET NOCOUNT OFF
END
GO