USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Transactions_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_Transactions_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 20/Oct/2014
-- Description:	Retrieve Alarm By Odometer
-- Modified by:	Berman Romero
-- Description:	Was Changed all logic - previous version doesn't work at all
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Transactions_Retrieve]
(
	@pVehicleId INT							--@pVehicleId
)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM  [Control].[Transactions] a WITH(READUNCOMMITTED)
	WHERE a.[VehicleId] = @pVehicleId
	  AND a.[Date] > DATEADD(MONTH,-3,GETUTCDATE())
	  AND a.[IsAdjustment] = 0
	  AND a.[IsFloating] = 0
	  AND a.[IsReversed] = 0
	  AND a.[IsDuplicated] = 0
	  AND a.[FixedOdometer] IS NULL
	  
	  	
	SET NOCOUNT OFF
END
