USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerUsers_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerUsers_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Retrieve Customer User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerUsers_Retrieve]
(
	  @pUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @RoleId NVARCHAR(128) 
	SELECT @RoleId = Id FROM [dbo].[AspNetRoles] WHERE Name = 'CUSTOMER_CHANGEODOMETER'

	DECLARE @AspNetUserId NVARCHAR(128) 
	SELECT @AspNetUserId = AspNetUserId FROM [General].[Users] WHERE UserId = @pUserId

	DECLARE @ExistsChangeOdometer BIT
	SELECT @ExistsChangeOdometer = 1 FROM [dbo].[AspNetUserRoles] UR WHERE UR.UserId = @AspNetUserId AND UR.RoleId = @RoleId
		
	SELECT
		  a.[CustomerUserId]
		 ,a.[CustomerId]
		 ,a.[Identification] AS [EncryptedIdentification]
		 ,b.[CountryId]
		 ,ISNULL(@ExistsChangeOdometer,0) [ChangeOdometer]
    FROM [General].[CustomerUsers] a
		INNER JOIN [General].[Customers] b
			ON b.[CustomerId] = a.[CustomerId]		
	WHERE a.[UserId] = @pUserId
	
    SET NOCOUNT OFF
END
