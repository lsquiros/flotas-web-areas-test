USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_VehicleFuel_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Vehicle Fuel information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuel_Retrieve]
(
	 @pVehicleFuelId INT = NULL				--@pVehicleFuelId: PK of the table
	,@pVehicleId INT = NULL					--@pVehicleId: FK of Vehicle Id
	,@pYear INT = NULL						--@pYear: Year
	,@pMonth INT = NULL						--@pMonth: Month
	,@pCustomerId INT = NULL				--@pCustomerId: Customer Ids
	,@pKey VARCHAR(800) = NULL				--@pKey: Search Key
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[VehicleFuelId]
		,a.[VehicleId]
		,a.[Month]
		,a.[Year]		
		,a.[Liters]
		,a.[Amount]
		,d.[Name] AS FuelName
		,d.[FuelId]
		,b.[PlateId]
		,b.[Name] AS VehicleName
		,c.[VehicleModel]
		,f.[Symbol] AS [CurrencySymbol]
		,a.[RowVersion]
    FROM [Control].[VehicleFuel] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]
		INNER JOIN [General].[VehicleCategories] c
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] d
			ON c.[DefaultFuelId] = d.[FuelId]
		INNER JOIN [General].[Customers] e
			ON b.[CustomerId] = e.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON e.[CurrencyId] = f.[CurrencyId]
	WHERE (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
	  AND (@pVehicleFuelId IS NULL OR a.[VehicleFuelId] = @pVehicleFuelId)
	  AND (@pYear IS NULL OR a.[Year] = @pYear)
	  AND (@pMonth IS NULL OR a.[Month] = @pMonth)
	  AND (@pCustomerId IS NULL OR b.[CustomerId] = @pCustomerId)
	  AND (@pKey IS NULL 
				OR CONVERT(VARCHAR(20),a.[Month]) like '%'+@pKey+'%'
				OR CONVERT(VARCHAR(20),a.[Year]) like '%'+@pKey+'%'
				OR CONVERT(VARCHAR(20),a.[Liters]) like '%'+@pKey+'%')
	ORDER BY [VehicleFuelId] DESC
	
    SET NOCOUNT OFF
END
