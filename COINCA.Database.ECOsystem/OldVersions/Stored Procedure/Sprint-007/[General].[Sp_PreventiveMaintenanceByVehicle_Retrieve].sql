USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================
-- Author:		Danilo Hidalgo
-- Create date: 02/23/2015
-- Description:	Preventive Maintenance By Vehicle information
-- ===========================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]
	@pVehicleId INT
AS
BEGIN 
	SET NOCOUNT ON;

	SELECT 
		a.[PreventiveMaintenanceId],
		a.[PreventiveMaintenanceCatalogId], 
		b.[Description] [PreventiveMaintenanceDescription],
		a.[VehicleId], 
		a.[PreventiveMaintenanceCatalogId], 
		a.[LastReviewOdometer], 
		a.[NextReviewOdometer], 
		a.[LastReviewDate], 
		a.[NextReviewDate], 
		a.[Registred], 
		a.[AlarmSent], 
		a.[InsertUserId], 
		a.[InsertDate], 
		a.[ModifyUserId], 
		a.[ModifyDate], 
		a.[RowVersion] 
	FROM 
		[General].[PreventiveMaintenanceByVehicle] a
		INNER JOIN [General].[PreventiveMaintenanceCatalog] b ON a.[PreventiveMaintenanceCatalogId] = b.[PreventiveMaintenanceCatalogId]
	WHERE a.[Registred] = 0 AND 
		a.[IsDeleted] = 0 AND 
		a.[VehicleId] = @pVehicleId
	
	SET NOCOUNT OFF;
END







