USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_HobbsMeterByVehicle_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_HobbsMeterByVehicle_Add]
GO

-- ================================================================================================
-- Author:		Berman Romero
-- Create date: 01/18/2015
-- Description:	Add Hobbs meter By Vehicle -- Massive process --
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_HobbsMeterByVehicle_Add]
(
	 @pDate DATE = NULL						--@pDate: optional date to run the massive process
)
AS 
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
	BEGIN TRY
    
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        
        DECLARE @lVehicleId INT, @lIntrackReference INT, @lIndex INT, @lMaxIndex INT
        DECLARE @lStartDate DATETIME = NULL, @lEndDate DATETIME
        
        SET @pDate = ISNULL(@pDate, GETUTCDATE())
        SET @lStartDate = CONVERT(DATE,@pDate)
		SET @lEndDate = DATEADD(SECOND,-1,DATEADD(DAY,1,@lStartDate))
    
		IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		DECLARE @lTable TABLE
		(	 [Index] INT IDENTITY(1,1) NOT NULL
			,[VehicleId] INT NULL
			,[IntrackReference] INT NULL
			,[GpsHoursOn] [decimal](16, 4) NULL
			,[GpsHoursOnStr] [varchar](100) NULL
			,[GpsStartOdometer] [decimal](16, 2) NULL
			,[GpsEndOdometer] [decimal](16, 2) NULL
		)
		
		INSERT INTO @lTable(
			 [VehicleId]
			,[IntrackReference]
		)
		SELECT	 a.[VehicleId]
				,a.[IntrackReference]
		FROM [General].[Vehicles] a
		WHERE a.[Active] = 1
		  AND a.[IntrackReference] IS NOT NULL
		
		SELECT @lMaxIndex = COUNT(1) FROM @lTable t
		SET @lIndex = 1
		/*1. Update [GpsOdometer]*/
		WHILE(@lIndex <= @lMaxIndex)
		BEGIN	
			SELECT @lIntrackReference = t.[IntrackReference] FROM @lTable t WHERE [Index] = @lIndex
			
			UPDATE @lTable
				SET  [GpsHoursOn] = x.[HoursOn]
					,[GpsHoursOnStr] = x.[HoursOnStr]
					,[GpsStartOdometer] = x.[StartOdometer]
					,[GpsEndOdometer] = x.[EndOdometer]
			FROM [General].[Fn_GpsHobbsMeterByVehicle](	 NULL
														,NULL
														,@lStartDate
														,@lEndDate
														,@lIntrackReference) x
			WHERE [Index] = @lIndex
		  
			SET @lIndex = @lIndex + 1
		END
		
		/*2. Insert Fianal Data*/
		INSERT [General].[HobbsMeterByVehicle](
			 [VehicleId]
			,[ProcessedDate]
			,[GpsHoursOn]
			,[GpsHoursOnStr]
			,[GpsStartOdometer]
			,[GpsEndOdometer]
			,[InsertUserId]
			,[InsertDate]
		)
		SELECT
			 t.[VehicleId]
			,@pDate
			,t.[GpsHoursOn]
			,t.[GpsHoursOnStr]
			,t.[GpsStartOdometer]
			,t.[GpsEndOdometer]
			,0 AS [InsertUserId]
			,GETUTCDATE() AS [InsertDate]
		FROM @lTable t
    
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
    END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH
	
    SET XACT_ABORT OFF
	SET NOCOUNT OFF
    
END
GO
