USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetScoreDrivingFormulas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Insurance].[Sp_GetScoreDrivingFormulas]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andr�s Oviedo Brenes
-- Create date: 24/02/2015
-- Description:	Get Score Driving Formulas of the customer
-- =============================================
CREATE PROCEDURE [Insurance].[Sp_GetScoreDrivingFormulas]
	  @pScoreDrivingFormulaId INT=null
	 ,@pCustomerId INT =null
	 ,@pKey varchar(1000)=null
AS
BEGIN	
	SET NOCOUNT ON;
	SELECT 
	   [ScoreDrivingFormulaId]
	  ,[Name]
      ,[ScoreDrivingFormulaStr]
      ,[ScoreDrivingFormulaSP]
      ,[IsActive]
      ,IsPending
      ,IsApproved
      ,[CustomerId]
      ,[RowVersion]
		FROM Insurance.ScoreDrivingFormula
		WHERE  (@pCustomerId IS NULL OR [CustomerId] = @pCustomerId)
				AND (@pScoreDrivingFormulaId IS NULL OR 
				@pScoreDrivingFormulaId = ScoreDrivingFormulaId)
				AND (@pKey IS NULL 
				OR [Name] LIKE '%'+@pKey+'%' )
      ORDER BY [Name] DESC	
	SET NOCOUNT OFF;
END
