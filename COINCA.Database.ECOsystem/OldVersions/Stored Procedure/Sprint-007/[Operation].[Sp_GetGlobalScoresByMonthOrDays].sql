USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetGlobalScoresByMonthOrDays]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_GetGlobalScoresByMonthOrDays]
GO

/****** Object:  StoredProcedure [Operation].[Sp_GetGlobalScoresByMonthOrDays]    Script Date: 7/28/2015 10:55:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Modified by:	Gerald Solano
-- Modified date:	18/06/2015
CREATE PROCEDURE [Operation].[Sp_GetGlobalScoresByMonthOrDays]
( @pCustomerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)			--Posibles valores S:Summarized, D:Detailed
)
as
BEGIN

	--Verificar configuración de la ponderación
	Declare @TotalWeight numeric(6,2), 
	        @CountWeight int
	
	Select @TotalWeight = IsNull(SUM(Weight),0),
		   @CountWeight = COUNT(1)
	  from Operation.WeightedScoreSettings
	 where CustomerId = @pCustomerId

	--Set Dates 
	Declare @DataPerMonth bit = 0 
	If @pEndDate is null
		If @pStartDate is null
		begin
			If @pMonth is null or @pYear is null
			begin
				RAISERROR ('100', 16, 1)
				RETURN
			end
			else
			begin
				--Set @pStartDate = [General].[Fn_GetFirstDayOfMonth](@pYear, @pMonth)
				--Set @pEndDate = DATEADD(MM, 1, @pStartDate)
				Set @DataPerMonth = 1
			end
		end
		else
		begin
			Set @pEndDate = DATEADD(DD, 1, @pStartDate)
		end

	--DECLARE @MyTable  dbo.TypeDeviceSpeed

	Declare @VehiclesByUser table
	(
		Device int,
		Vehicle int,
		StartOdometer int,
		EndOdometer int,
		StartDate datetime,
		EndDate datetime
	)

	Create table #LocalDriverScore
	(DriverScoreId INT Identity(1,1),
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 ScoreType varchar(5),
	 Score Float,
	 Photo varchar(max)
	 )

	 -- Calificaciones temporales para consultarlas en el ultimo SELECT aplicando la Configuración global 
	--Create table #TempScoreDriving
	--(
	--	DriverScoreId INT,
	--	UserId  INT,
	--	SpeedScoreType varchar(5),
	--	SpeedScore  FLOAT
	--)

	Declare @UserId Int
	Declare @Name varchar(250),
			@Identification varchar(50)
		
	Declare  @ScoreAdmOutput nvarchar(4000),
			 @ScoreRouteOutput nvarchar(4000)
			 	
	Declare  @ScoreAdm Float,
			 @ScoreRoute Float
	

	-- //// CALIFICACIONES PRECALCULADAS ////////////////////////////////////////////////////////
	If @DataPerMonth = 0 
	begin
		--Para la invocación por día, recupera las calificaciones precalculadas
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score)
		Select DS.UserId, U.Name, D.Identification, DS.ScoreType, DS.Score 					
		  from Operation.DriversScoresDaily DS
		  inner join General.Users U on DS.UserId = U.UserId
		  inner join General.DriversUsers D on DS.UserId = D.UserId
		 where DS.CustomerId = @pCustomerId
		   --and DS.ScoreYear = @pYear
		   --and DS.ScoreMonth = @pMonth
		   and DS.Date_Time >= @pStartDate and DS.Date_Time < @pEndDate


		--Insert into #TempScoreDriving (DriverScoreId, UserId, SpeedScoreType, SpeedScore)
		--SELECT LD.DriverScoreId, LD.UserId, LD.ScoreType, LD.Score
		--FROM #LocalDriverScore LD	   
	end
	else
	begin
		--Para la invocación por mes, recupera las calificaciones precalculadas
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score)
		Select DS.UserId, U.Name, D.Identification, DS.ScoreType, DS.Score 					
		from Operation.DriversScores DS
		--from Operation.DriversScoresDaily DS
		  inner join General.Users U on DS.UserId = U.UserId
		  inner join General.DriversUsers D on DS.UserId = D.UserId
		 where DS.CustomerId = @pCustomerId
		   and DS.ScoreYear = @pYear
		   and DS.ScoreMonth = @pMonth
	END
	-- ///////////////////////////////////////////////////////////////////////////


	-- //////// CALIFICACION GLOBAL //////////////////////////////////////////////
	--SELECT @CountWeight AS COUNT_WEIGHT
	If @CountWeight > 0 
	begin

		IF(@DataPerMonth > 0)
		BEGIN
			Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score, Photo)
			Select DS.UserId, DS.Name, DS.Identification, 'TOTAL', SUM(DS.Score * WS.Weight / 100), U.Photo
			  from #LocalDriverScore DS
			 inner join Operation.ScoreTypes ST on DS.ScoreType = ST.Code 
			 inner join Operation.WeightedScoreSettings WS on ST.ScoreTypeId = WS.ScoreTypeId
			 inner join General.Users U on DS.UserId = U.UserId
			 where WS.CustomerId = @pCustomerId
			group by DS.UserId, DS.Name, DS.Identification, U.Photo
		END
		ELSE
		BEGIN
			Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score, Photo)
			Select DS.UserId, DS.Name, DS.Identification, 'TOTAL', ((SUM(DS.Score) / COUNT(*)) * WS.Weight / 100), U.Photo
			  from #LocalDriverScore DS
			 inner join Operation.ScoreTypes ST on DS.ScoreType = ST.Code 
			 inner join Operation.WeightedScoreSettings WS on ST.ScoreTypeId = WS.ScoreTypeId
			 inner join General.Users U on DS.UserId = U.UserId
			 where WS.CustomerId = 9--@pCustomerId
			group by DS.UserId, DS.Name, DS.Identification, DS.ScoreType, WS.Weight, U.Photo
		END		
		  	 	  
	end
	else
	begin
		--No existe configuración de calificación por peso
		--Se asigna la calificación de carretera como la calificación global

		IF(@DataPerMonth > 0)
		BEGIN
			Insert into #LocalDriverScore (UserID, Name, Identification , ScoreType, Score, Photo)
			Select DS.UserId, DS.Name, DS.Identification, 'TOTAL', DS.Score, U.Photo
			  from #LocalDriverScore DS
			 inner join General.Users U on DS.UserId = U.UserId
			 where DS.ScoreType = 'SPROU'
		END
		ELSE 
		BEGIN
			Insert into #LocalDriverScore (UserID, Name, Identification , ScoreType, Score, Photo)
			Select DS.UserId, DS.Name, DS.Identification, 'TOTAL', (SUM(DS.Score) / COUNT(*)), U.Photo
			  from #LocalDriverScore DS
			 inner join General.Users U on DS.UserId = U.UserId
			 where DS.ScoreType = 'SPROU'
			 group by DS.UserId, DS.Name, DS.Identification, U.Photo
		END
	end
	-- /////////////////////////////////////////////////////////////////////////


	-- //////// TIPO DE REPORTE //////////////////////////////////////////////
	If @pReportType = 'D'
	BEGIN
		IF(@DataPerMonth > 0)
		BEGIN
			Select DS.DriverScoreId ,
				   DS.UserId,Name as EncryptedName,
				   DS.Identification As EncryptedIdentification,
				   DS.ScoreType, 
				   DS.Score,
				   DS.Photo  
			from #LocalDriverScore DS
			order by DS.ScoreType, DS.Score asc
		END
		ELSE
		BEGIN
			Select MAX(DS.DriverScoreId), DS.UserId, DS.Name as EncryptedName, 
				 DS.Identification As EncryptedIdentification, 
				 DS.ScoreType, 
				 (SUM(DS.Score) / COUNT(*)), 
				 U.Photo
			  from #LocalDriverScore DS
			 inner join General.Users U on DS.UserId = U.UserId
			 where  DS.ScoreType <> 'TOTAL'
			 group by DS.UserId, DS.Name, DS.Identification, DS.ScoreType, U.Photo
		END

	END -- REPORT TYPE 'D'
	ELSE
	BEGIN
		--SELECT * FROM #LocalDriverScore
		
		IF(@DataPerMonth > 0)
		BEGIN

			Select DS.DriverScoreId ,
				   DS.UserId,DS.Name as EncryptedName,
				   DS.Identification As EncryptedIdentification,
				   DS.ScoreType, 
				   DS.Score,
				   DS.Photo, 
				   IsNull(DSA.Score, 0) ScoreSpeedAdmin, 
				   IsNull(DSR.Score, 0) ScoreSpeedRoute
			  from #LocalDriverScore DS
			 left join #LocalDriverScore DSA on DS.UserId = DSA.UserId and DSA.ScoreType = 'SPADM'
			 left join #LocalDriverScore DSR on DS.UserId = DSR.UserId and DSR.ScoreType = 'SPROU'
			 where DS.ScoreType = 'TOTAL'
			order by DS.ScoreType, DS.Score asc

		END 
		ELSE
		BEGIN

				Select MAX(DS.DriverScoreId),
				   DS.UserId,DS.Name as EncryptedName,
				   DS.Identification As EncryptedIdentification,
				   DS.ScoreType, 
				   SUM(DS.Score) AS Score,
				   DS.Photo,
				   0 AS ScoreSpeedAdmin, 
				   0 AS ScoreSpeedRoute 
				  -- (SELECT SpeedScore FROM #TempScoreDriving WHERE DriverScoreId = DS.DriverScoreId and UserId = DS.UserId and SpeedScoreType = 'SPADM') ScoreSpeedAdmin,
				  -- (SELECT SpeedScore FROM #TempScoreDriving WHERE DriverScoreId = DS.DriverScoreId and UserId = DS.UserId and SpeedScoreType = 'SPROU') ScoreSpeedRoute
			  from #LocalDriverScore DS
			 where DS.ScoreType = 'TOTAL'
			 GROUP BY  DS.UserId,DS.Name,
				   DS.Identification,
				   DS.ScoreType, 
				   DS.Photo
			order by DS.ScoreType, SUM(DS.Score) asc

			--SELECT * FROM #TempScoreDriving
		
		END 

	END -- END REPORT TYPE 'S'

	-- //////////////////////////////////////////////////////////////////

	Drop table #LocalDriverScore
END


GO


