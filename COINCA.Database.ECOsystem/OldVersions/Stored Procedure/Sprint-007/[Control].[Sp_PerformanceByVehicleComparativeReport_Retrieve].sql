USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PerformanceByVehicleComparativeReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_PerformanceByVehicleComparativeReport_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Rometro
-- Create date: 03/06/2015
-- Description:	Retrieve Performance By Vehicle Comparative Report information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PerformanceByVehicleComparativeReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pVehicleId INT = NULL				--@pVehicleId: Id of the vehicle
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pManufacturer VARCHAR(250) = NULL	--@pManufacturer: Manufacturer
	,@pVehicleModel VARCHAR(250) = NULL	--@pVehicleModel: Vehicle Model
	,@pVehicleYear INT = NULL			--@pVehicleYear: Vehicle Year
	,@pVehicleType VARCHAR(250) = NULL	--@pVehicleType: Vehicle Type
	,@pGroupBy INT						--@pGroupBy: Group By
	,@pCostCenterId int=null 
	,@pUnitId int=null
)
AS
BEGIN
	SET NOCOUNT ON
		
	IF(@pYear IS NOT NULL AND @pMonth IS NOT NULL)
	BEGIN
		SELECT   @pStartDate = DATEADD(MONTH,@pMonth-1,DATEADD(YEAR,@pYear-1900,0)) /*First*/
				,@pEndDate = DATEADD(SECOND,-1,DATEADD(MONTH,@pMonth,DATEADD(YEAR,@pYear-1900,0))) /*Last*/
	END

	IF(@pStartDate IS NULL OR @pEndDate IS NULL)
	BEGIN
		RETURN
	END
	
	;WITH TmpValues ( Number) as
	(
		  SELECT 0
		  UNION ALL
		  SELECT Number + 1
		  FROM TmpValues
		  WHERE Number < 5000
	)
	
	SELECT 
		 a.[VehicleId]
		,a.[PlateId]
		,a.[Name] AS [VehicleName]
		,b.[Manufacturer]
		,b.[Year] AS [VehicleYear]
		,b.[VehicleModel]
		,b.[Type] AS [VehicleType]
		,d.[Name] AS [UnitName]
		,c.[Name] AS [CostCenterName]
		,t.[TrxDate] AS [TrxDate]
		,DATEPART(wk, t.[TrxDate]) AS [TrxWeekNumber]
		,t.[SumTrxLiters] AS [TrxReportedLiters]
		,t.[SumTrxOdometer] AS [TrxTraveledOdometer]
		,t.[AvgTrxPerformance] AS [TrxPerformance]
		,t.[SumGpsOdometer] AS [GpsTraveledOdometer]
		,t.[AvgGpsPerformance] AS [GpsPerformance]
		,t.[SumGpsHoursOn] AS [GpsHoursOn]		
		,@pGroupBy AS [GroupBy]
	FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCategories] b
			ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
		INNER JOIN [General].[VehicleCostCenters] c
			ON a.[CostCenterId] = c.[CostCenterId]
		INNER JOIN [General].[VehicleUnits] d
			ON c.[UnitId] = d.[UnitId]
		INNER JOIN(
			SELECT
				 u.[VehicleId]
				,u.[TrxDate]
				,SUM([SumTrxLiters])AS [SumTrxLiters]
				,SUM([SumTrxOdometer])AS [SumTrxOdometer]
				,SUM([AvgTrxPerformance])AS [AvgTrxPerformance]
				,SUM([SumGpsOdometer])AS [SumGpsOdometer]
				,SUM([AvgGpsPerformance])AS [AvgGpsPerformance]
				,SUM([SumGpsHoursOn])AS [SumGpsHoursOn]
				FROM(
				SELECT	 a.[VehicleId]
						,CONVERT(DATE,a.[TrxDate]) AS [TrxDate]
						,SUM(a.[TrxReportedLiters]) AS [SumTrxLiters]
						,ABS(MAX(a.[TrxReportedOdometer]) - MAX(a.[TrxPreviousOdometer])) AS [SumTrxOdometer]
						,AVG(a.[TrxPerformance]) AS [AvgTrxPerformance]
						,ABS(MAX(a.[GpsReportedOdometer]) - MAX(a.[GpsPreviousOdometer])) AS [SumGpsOdometer]
						,AVG(a.[GpsPerformance]) AS [AvgGpsPerformance]
						,SUM(a.[GpsHoursOn]) AS [SumGpsHoursOn]
				FROM [General].[PerformanceByVehicle] a
					INNER JOIN [General].[Vehicles] b
						ON a.[VehicleId] = b.[VehicleId]
				WHERE b.[CustomerId] = @pCustomerId
				  AND b.[Active] = 1
				  AND (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
				  AND a.[TrxDate] BETWEEN @pStartDate AND @pEndDate				  
				GROUP BY 
						 a.[VehicleId]
						,CONVERT(DATE,a.[TrxDate])				
				UNION
				SELECT
					 a.[VehicleId]
					,CONVERT(DATE,r.[TrxDate]) AS [TrxDate]
					,NULL AS [SumTrxLiters]
					,NULL AS [SumTrxOdometer]
					,NULL AS [AvgTrxPerformance]
					,NULL AS [SumGpsOdometer]
					,NULL AS [AvgGpsPerformance]
					,NULL AS [SumGpsHoursOn]
				FROM [General].[Vehicles] a
					CROSS JOIN(
							SELECT
								 DATEADD(DAY, x.[Number], @pStartDate) AS [TrxDate]
							FROM [TmpValues] x
							WHERE x.[Number] <= DATEDIFF(DAY, @pStartDate, @pEndDate)
					)r
				WHERE a.[CustomerId] = @pCustomerId
				  AND a.[Active] = 1
				  AND (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId))u
				GROUP BY 
						 u.[VehicleId]
						,u.[TrxDate]
			)t
			ON a.[VehicleId] = t.[VehicleId]
		
	WHERE a.[CustomerId] = @pCustomerId
	  AND a.[Active] = 1
	  AND (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
	  AND (@pManufacturer IS NULL OR b.Manufacturer = @pManufacturer)
	  AND (@pVehicleModel IS NULL OR b.VehicleModel = @pVehicleModel)
	  AND (@pVehicleYear IS NULL OR b.[Year] = @pVehicleYear)
	  AND (@pVehicleType IS NULL OR b.[Type] = @pVehicleType)
	  AND (@pCostCenterId IS NULL OR C.CostCenterId = @pCostCenterId)
	  AND (@pUnitId IS NULL OR d.UnitId = @pUnitId)
	ORDER BY a.VehicleId, t.TrxDate	
	OPTION (MAXRECURSION 5000)
		
	
	SET NOCOUNT OFF
END
