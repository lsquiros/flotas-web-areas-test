USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardTransactionSALE_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardTransactionSALE_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 04/14/2015
-- Description:	Retrieve Transactions Sales for VOID Process
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardTransactionSALE_Retrieve]
(
	 @pCreditCardId INT	      --@pCreditCardId: Credit Card Id
)
AS
BEGIN
	
	SET NOCOUNT ON	
	SELECT top 20	
			cvp.CreditCardTransactionVPOS AS [CCTransactionVPOSId],
			cvp.[SystemTraceNumber] AS [TraceNumber],
			cvp.InsertDate AS [Date],
			cvp.TotalAmount AS [FuelAmount]
	FROM [Control].[CreditCardTransactionVPOS] cvp WITH(READUNCOMMITTED)
	
	WHERE cvp.CreditCardId = @pCreditCardId
		  AND cvp.TransactionType = 'SALE'
	
	ORDER BY cvp.InsertDate DESC
				
    SET NOCOUNT OFF
END
