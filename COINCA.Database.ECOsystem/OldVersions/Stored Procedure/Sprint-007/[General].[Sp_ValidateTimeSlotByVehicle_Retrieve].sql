USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_ValidateTimeSlotByVehicle_Retrieve]    Script Date: 12/09/2014 14:21:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ValidateTimeSlotByVehicle_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_ValidateTimeSlotByVehicle_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_ValidateTimeSlotByVehicle_Retrieve]    Script Date: 12/09/2014 14:21:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 12/09/2014
-- Description:	Retrieve ValidateVehicleTimeSlot information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ValidateTimeSlotByVehicle_Retrieve]
(
	 @pVehicleId INT						--@pTimeSlotByVehicleId: PK of the table
)
AS
BEGIN
	
	
	SET NOCOUNT ON	
	
	DECLARE @Date datetime = DATEADD(HOUR, -6, CONVERT(DATETIME, GETUTCDATE()))
	DECLARE @Time TIME = DATEADD(HOUR, -6, CONVERT(TIME, GETUTCDATE()))
	DECLARE @Day int = (DATEPART(dw, @Date)) - 1
	DECLARE @lxmlData xml

	select @lxmlData = XmlTimeSlot from General.TimeSlotByVehicle where VehicleId = @pVehicleId

	select 
		RowData.col.value('./@VehicleId', 'INT') AS [VehicleId]
		,RowData.col.value('./@Day', 'INT') AS [Day]
		,RowData.col.value('./@Time', 'VARCHAR(5)') AS [Time]
	FROM @lxmlData.nodes('/xmldata/TimeSlot') RowData(col)
	WHERE RowData.col.value('./@Day', 'INT')  = @Day
		AND @Time BETWEEN CONVERT(TIME, RowData.col.value('./@Time', 'VARCHAR(5)'))
		AND DATEADD(MINUTE, 30,CONVERT(TIME, RowData.col.value('./@Time', 'VARCHAR(5)')))											
	ORDER BY 2,3

    SET NOCOUNT OFF
END


GO


