USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetGlobalScoresDataSourceByMonthOrDays]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_GetGlobalScoresDataSourceByMonthOrDays]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Modified by:	Gerald Solano
-- Modified date:	18/06/2015 
CREATE PROCEDURE [Operation].[Sp_GetGlobalScoresDataSourceByMonthOrDays] 
( @pCustomerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)			--Posibles valores S:Summarized, D:Detailed
)
AS
BEGIN

	--Verificar configuración de la ponderación
	Declare @TotalWeight numeric(6,2), 
	        @CountWeight int,
	        @pCustomerName varchar(200)
	
	Select
		   @pCustomerName=Name
	  from General.Customers 
	 where CustomerId = @pCustomerId
	 
	Select @TotalWeight = IsNull(SUM(Weight),0),
		   @CountWeight = COUNT(1)
	 from Operation.WeightedScoreSettings w
	 where CustomerId = @pCustomerId


	--Set Dates 
	Declare @DataPerMonth bit = 0 
	If @pEndDate is null
		If @pStartDate is null
		begin
			If @pMonth is null or @pYear is null
			begin
				RAISERROR ('100', 16, 1)
				RETURN
			end
			else
			begin
				--Set @pStartDate = [General].[Fn_GetFirstDayOfMonth](@pYear, @pMonth)
				--Set @pEndDate = DATEADD(MM, 1, @pStartDate)
				Set @DataPerMonth = 1
			end
		end
		else
		begin
			Set @pEndDate = DATEADD(DD, 1, @pStartDate)
		end

	--DECLARE @MyTable  dbo.TypeDeviceSpeed
	Declare @VehiclesByUser table
	(Device int,
	 Vehicle int,
	 StartOdometer int,
	 EndOdometer int,
	 StartDate datetime,
	 EndDate datetime)

	Create table #LocalDriverScore
	(DriverScoreId INT Identity(1,1),
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 ScoreType varchar(5),
	 Score Float,
	 Photo varchar(max),
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float
	 )

	 Create table #ResultLocalDriverScore
	(DriverScoreId INT Identity(1,1),
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 ScoreType varchar(5),
	 Score Float,
	 Photo varchar(max),
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float
	 )

	Declare @UserId Int
	Declare @Name varchar(250),
			@Identification varchar(50)
		
	Declare  @ScoreAdmOutput nvarchar(4000),
			 @ScoreRouteOutput nvarchar(4000)
			 	
	Declare  @ScoreAdm Float,
			 @ScoreRoute Float
	
	-- //// CALIFICACIONES PRECALCULADAS ////////////////////////////////////////////////////////
	If @DataPerMonth = 0 
	begin
		--Para la invocación por día, recupera las calificaciones precalculadas
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score, KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance)
		Select DS.UserId, U.Name, D.Identification, DS.ScoreType, DS.Score ,DS.KmTraveled,
						DS.OverSpeed ,
						DS.OverSpeedAmount ,
						DS.OverSpeedDistancePercentage ,
						DS.OverSpeedAverage ,
						DS.OverSpeedWeihgtedAverage ,
						DS.StandardDeviation ,
						DS.Variance ,
						DS.VariationCoefficient,
						DS.OverSpeedDistance							
		  from Operation.DriversScoresDaily DS
		  inner join General.Users U on DS.UserId = U.UserId
		  inner join General.DriversUsers D on DS.UserId = D.UserId
		 where DS.CustomerId = @pCustomerId
		   and DS.Date_Time >= @pStartDate and DS.Date_Time < @pEndDate
	end
	else
	begin
		--Para la invocación por mes, recupera las calificaciones precalculadas
		Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,
						OverSpeedDistance	)
		Select DS.UserId, U.Name, D.Identification, DS.ScoreType, DS.Score ,DS.KmTraveled,
						DS.OverSpeed ,
						DS.OverSpeedAmount ,
						DS.OverSpeedDistancePercentage ,
						DS.OverSpeedAverage ,
						DS.OverSpeedWeihgtedAverage ,
						DS.StandardDeviation ,
						DS.Variance ,
						DS.VariationCoefficient,
						DS.OverSpeedDistance		
		  from Operation.DriversScores DS
		  inner join General.Users U on DS.UserId = U.UserId
		  inner join General.DriversUsers D on DS.UserId = D.UserId
		 where DS.CustomerId = @pCustomerId
		   and DS.ScoreYear = @pYear
		   and DS.ScoreMonth = @pMonth
	end
	-- ///////////////////////////////////////////////////////////////////////////

	-- //////// CALIFICACION GLOBAL //////////////////////////////////////////////

	DELETE #ResultLocalDriverScore

	--SELECT @CountWeight
	If @CountWeight > 0 
	begin
		IF(@DataPerMonth > 0)
		BEGIN
			Insert into #ResultLocalDriverScore--#LocalDriverScore 
					(UserID, Name, Identification, ScoreType, Score, Photo,KmTraveled,
							OverSpeed ,
							OverSpeedAmount ,
							OverSpeedDistancePercentage ,
							OverSpeedAverage ,
							OverSpeedWeihgtedAverage ,
							StandardDeviation ,
							Variance ,
							VariationCoefficient,
							OverSpeedDistance)
			Select DS.UserId, DS.Name, DS.Identification, ds.ScoreType, SUM(DS.Score * WS.Weight / 100), U.Photo,
							DS.KmTraveled,
							DS.OverSpeed ,
							DS.OverSpeedAmount ,
							DS.OverSpeedDistancePercentage ,
							DS.OverSpeedAverage ,
							DS.OverSpeedWeihgtedAverage ,
							DS.StandardDeviation ,
							DS.Variance ,
							DS.VariationCoefficient,
							DS.OverSpeedDistance	
			  from #LocalDriverScore DS
			 inner join Operation.ScoreTypes ST on DS.ScoreType = ST.Code  ---'TOTAL' = ST.Code 
			 inner join Operation.WeightedScoreSettings WS on ST.ScoreTypeId = WS.ScoreTypeId
			 inner join General.Users U on DS.UserId = U.UserId
			 where WS.CustomerId = @pCustomerId
			group by DS.UserId, DS.Name, DS.Identification, U.Photo,
							DS.KmTraveled,
							DS.OverSpeed ,
							DS.OverSpeedAmount ,
							DS.OverSpeedDistancePercentage ,
							DS.OverSpeedAverage ,
							DS.OverSpeedWeihgtedAverage ,
							DS.StandardDeviation ,
							DS.Variance ,
							DS.VariationCoefficient,
							DS.OverSpeedDistance,
		  	 				DS.ScoreType
		END 
		ELSE
		BEGIN
			Insert into #ResultLocalDriverScore---#LocalDriverScore 
					(UserID, Name, Identification, ScoreType, Score, Photo,KmTraveled,
							OverSpeed ,
							OverSpeedAmount ,
							OverSpeedDistancePercentage ,
							OverSpeedAverage ,
							OverSpeedWeihgtedAverage ,
							StandardDeviation ,
							Variance ,
							VariationCoefficient,
							OverSpeedDistance)
			Select DS.UserId, DS.Name, DS.Identification, ds.ScoreType, ((SUM(DS.Score) / COUNT(*)) * WS.Weight / 100), U.Photo,
							SUM(DS.KmTraveled),
							SUM(DS.OverSpeed),
							SUM(DS.OverSpeedAmount),
							SUM(DS.OverSpeedDistancePercentage),
							SUM(DS.OverSpeedAverage),
							SUM(DS.OverSpeedWeihgtedAverage),
							SUM(DS.StandardDeviation),
							SUM(DS.Variance),
							SUM(DS.VariationCoefficient),
							SUM(DS.OverSpeedDistance)	
			  from #LocalDriverScore DS
			 inner join Operation.ScoreTypes ST on DS.ScoreType = ST.Code  ---'TOTAL' = ST.Code 
			 inner join Operation.WeightedScoreSettings WS on ST.ScoreTypeId = WS.ScoreTypeId
			 inner join General.Users U on DS.UserId = U.UserId
			 where WS.CustomerId = 9--@pCustomerId
			group by DS.UserId, DS.Name, DS.Identification, U.Photo, WS.Weight, DS.ScoreType

		END
	end
	else
	begin
		--No existe configuración de calificación por peso
		--Se asigna la calificación de carretera como la calificación global

		IF(@DataPerMonth > 0)
		BEGIN	
			Insert into #ResultLocalDriverScore--#LocalDriverScore 
					(UserID, Name, Identification , ScoreType, Score, Photo,
							KmTraveled,
							OverSpeed ,
							OverSpeedAmount ,
							OverSpeedDistancePercentage ,
							OverSpeedAverage ,
							OverSpeedWeihgtedAverage ,
							StandardDeviation ,
							Variance ,
							VariationCoefficient,
							OverSpeedDistance)
			Select DS.UserId, DS.Name   , DS.Identification, ds.ScoreType, DS.Score, U.Photo,
							DS.KmTraveled,
							DS.OverSpeed ,
							DS.OverSpeedAmount ,
							DS.OverSpeedDistancePercentage ,
							DS.OverSpeedAverage ,
							DS.OverSpeedWeihgtedAverage ,
							DS.StandardDeviation ,
							DS.Variance ,
							DS.VariationCoefficient,
							DS.OverSpeedDistance	
			  from #LocalDriverScore DS
			 inner join General.Users U on DS.UserId = U.UserId
			 where DS.ScoreType = 'SPROU'
		END
		ELSE
		BEGIN
			Insert into #ResultLocalDriverScore--#LocalDriverScore 
					(UserID, Name, Identification , ScoreType, Score, Photo,
							KmTraveled,
							OverSpeed ,
							OverSpeedAmount ,
							OverSpeedDistancePercentage ,
							OverSpeedAverage ,
							OverSpeedWeihgtedAverage ,
							StandardDeviation ,
							Variance ,
							VariationCoefficient,
							OverSpeedDistance)
			Select DS.UserId, DS.Name, DS.Identification, ds.ScoreType, (SUM(DS.Score) / COUNT(*)), U.Photo,
							SUM(DS.KmTraveled),
							SUM(DS.OverSpeed),
							SUM(DS.OverSpeedAmount),
							SUM(DS.OverSpeedDistancePercentage),
							SUM(DS.OverSpeedAverage),
							SUM(DS.OverSpeedWeihgtedAverage),
							SUM(DS.StandardDeviation),
							SUM(DS.Variance),
							SUM(DS.VariationCoefficient),
							SUM(DS.OverSpeedDistance)	
			  from #LocalDriverScore DS
			 inner join General.Users U on DS.UserId = U.UserId
			 where DS.ScoreType = 'SPROU'
			 group by DS.UserId, DS.Name, DS.Identification, U.Photo, ds.ScoreType
		END
	end
	-- /////////////////////////////////////////////////////////////////////////

	-- //////// TIPO DE REPORTE //////////////////////////////////////////////
	If @pReportType = 'D'
	begin
		IF(@DataPerMonth > 0)
		BEGIN
			Select @pCustomerId AS CustomerId,@pCustomerName as CustomerName, DS.DriverScoreId ,DS.UserId,Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo,
			DS.KmTraveled ,
			DS.OverSpeed ,
			DS.OverSpeedAmount ,
			DS.OverSpeedDistancePercentage ,
			DS.OverSpeedAverage ,
			DS.OverSpeedWeihgtedAverage ,
			DS.StandardDeviation ,
			DS.Variance ,
			DS.VariationCoefficient,	DS.OverSpeedDistance	   
			from #ResultLocalDriverScore DS--#LocalDriverScore DS 
			order by DS.ScoreType, DS.Score asc 
		END
		ELSE
		BEGIN
			Select @pCustomerId AS CustomerId,@pCustomerName as CustomerName, DS.DriverScoreId ,DS.UserId,Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo,
			DS.KmTraveled ,
			DS.OverSpeed ,
			DS.OverSpeedAmount ,
			DS.OverSpeedDistancePercentage ,
			DS.OverSpeedAverage ,
			DS.OverSpeedWeihgtedAverage ,
			DS.StandardDeviation ,
			DS.Variance ,
			DS.VariationCoefficient,	DS.OverSpeedDistance	   
			from #ResultLocalDriverScore DS--#LocalDriverScore DS 
			order by DS.ScoreType, DS.Score asc 
		END
	end
	else
	begin

		IF(@DataPerMonth > 0)
		BEGIN
			Select @pCustomerId AS CustomerId,@pCustomerName as CustomerName, DS.DriverScoreId ,DS.UserId,DS.Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo , IsNull(DSA.Score, 0) ScoreSpeedAdmin, IsNull(DSR.Score, 0) ScoreSpeedRoute
			,DS.KmTraveled ,
			DS.OverSpeed ,
			DS.OverSpeedAmount ,
			DS.OverSpeedDistancePercentage ,
			DS.OverSpeedAverage ,
			DS.OverSpeedWeihgtedAverage ,
			DS.StandardDeviation ,
			DS.Variance ,
			DS.VariationCoefficient
			,DS.OverSpeedDistance	
		
			from #ResultLocalDriverScore DS--#LocalDriverScore DS
			 left join #LocalDriverScore DSA on DS.UserId = DSA.UserId and DSA.ScoreType = 'SPADM'
			 left join #LocalDriverScore DSR on DS.UserId = DSR.UserId and DSR.ScoreType = 'SPROU'
		
			order by DS.UserId, DS.ScoreType, DS.Score asc
		
		END
		ELSE
		BEGIN
			Select @pCustomerId AS CustomerId,@pCustomerName as CustomerName, DS.DriverScoreId ,DS.UserId,DS.Name as EncryptedName  ,DS.Identification As  EncryptedIdentification  ,DS.ScoreType ,	 DS.Score ,DS.Photo,
			0 ScoreSpeedAdmin, 
			0 ScoreSpeedRoute,
			DS.KmTraveled ,
			DS.OverSpeed ,
			DS.OverSpeedAmount ,
			DS.OverSpeedDistancePercentage ,
			DS.OverSpeedAverage ,
			DS.OverSpeedWeihgtedAverage ,
			DS.StandardDeviation ,
			DS.Variance ,
			DS.VariationCoefficient
			,DS.OverSpeedDistance	
			from #ResultLocalDriverScore DS--#LocalDriverScore DS
			--where DS.ScoreType = 'TOTAL'
			order by DS.UserId, DS.ScoreType, DS.Score asc
		END

	end	-- END REPORT TYPE 'S'
	-- //////////////////////////////////////////////////////////////////

	Drop table #LocalDriverScore
	Drop table #ResultLocalDriverScore

END

GO


