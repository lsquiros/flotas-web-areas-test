USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleByRoute_Retrieve]    Script Date: 11/06/2014 10:22:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleByRoute_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_VehicleByRoute_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_VehicleByRoute_Retrieve]    Script Date: 11/20/2014 10:22:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/20/2014
-- Description:	Retrieve VehicleByRoute information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehicleByRoute_Retrieve]
(
	 @pVehicleId INT = NULL
	,@pRouteId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		v.[VehicleId],
		v.[RouteId],
		r.[Name] [RouteName],
		v.[Days],
		v.[RowVersion]
	FROM 
		[Efficiency].[VehicleByRoute] v
		INNER JOIN [Efficiency].[Routes] r ON v.[RouteId] = r.[RouteId]	
	WHERE 
		v.[VehicleId] = @pVehicleId AND 
		(@pRouteId IS NULL OR v.[RouteId] = @pRouteId) 
	ORDER BY 
		r.[Name] ASC
	
    SET NOCOUNT OFF
END


GO

