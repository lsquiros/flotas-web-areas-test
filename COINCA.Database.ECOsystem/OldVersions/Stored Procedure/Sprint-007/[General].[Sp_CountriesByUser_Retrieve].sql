USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CountriesByUser_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CountriesByUser_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo Gonz�lez.
-- Create date: 01/19/2015
-- Description:	Retrieve country by user information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CountriesByUser_Retrieve]
(
	 @pLoggedUserId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		a.[CountryId],
		a.[Name]
	FROM [General].[Countries] a
		INNER JOIN [General].[PartnerUsersByCountry] b ON a.[CountryId] = b.[CountryId]
		INNER JOIN [General].[PartnerUsers] c ON b.[PartnerUserId] = c.[PartnerUserId]
		INNER JOIN [General].[Users] d ON c.[UserId] = d.[UserId]
	WHERE c.[UserId] = @pLoggedUserId 
	ORDER BY
		a.[Name]
	
    SET NOCOUNT OFF
END
