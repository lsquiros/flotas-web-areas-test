USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_OdometerVsRoute_Alarm]    Script Date: 01/30/2015 11:37:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_OdometerVsRoute_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_OdometerVsRoute_Alarm]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_OdometerVsRoute_Alarm]    Script Date: 01/30/2015 11:37:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/30/2015
-- Description:	Stored Procedure that execute the alarm Add Alarm for Expiration of Fleet Card
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_OdometerVsRoute_Alarm]
AS
BEGIN 

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @Message VARCHAR(MAX)
        DECLARE @RowNum INT
		
		DECLARE @tData TABLE
			(OdometerVsRouteAlarmId INT NULL,
			VehicleId INT,
			CustomerId INT,
			Active BIT,
			IntrackReference INT,
			Device INT,
			GPSDateTime DATETIME,
			Odometer FLOAT,
			LocalDate DATETIME)
		
		DECLARE @tEmails TABLE(
			[Id] INT IDENTITY(1,1) NOT NULL,
			OdometerVsRouteAlarmId INT,
			VehicleId INT,
			PlateId VARCHAR(10),
			CustomerId INT,
			kmRoute FLOAT,
			Email VARCHAR(MAX))

		--OBTIENE EL LISTADO DE VEHICULOS
		INSERT INTO	@tData
		(OdometerVsRouteAlarmId,
		VehicleId,
		CustomerId,
		Active,
		IntrackReference,
		Device,
		GPSDateTime,
		Odometer,
		LocalDate)
	SELECT DISTINCT
		COUNT(f.VehicleId),
		a.VehicleId,
		a.CustomerId,
		a.Active,
		a.IntrackReference,
		d.[Device],
		e.GPSDateTime,
		e.Odometer,
		DATEADD(HOUR, CONVERT(INT, g.Value), GETUTCDATE()) LocalDate
	FROM 
		General.Vehicles a
		INNER JOIN IntrackV2..Composiciones b ON a.IntrackReference = b.vehiculo
		INNER JOIN IntrackV2..DispositivosAVL c ON b.dispositivo = c.dispositivo
		INNER JOIN [172.22.220.38\BDS_WEB].[Atrack].[dbo].[Devices] d ON c.numeroimei = d.[UnitID]
		INNER JOIN [172.22.220.38\BDS_WEB].[Atrack].[dbo].[Report_Last] e ON d.[Device] = e.[Device]
		LEFT JOIN General.OdometerVsRouteAlarm f ON a.VehicleId = a.VehicleId AND 
		CONVERT(VARCHAR(2), MONTH(DATEADD(HOUR, -6, GETUTCDATE()))) + '-' +  CONVERT(VARCHAR(2), DAY(DATEADD(HOUR, -6, GETUTCDATE()))) + '-' + CONVERT(VARCHAR(2), DAY(DATEADD(HOUR, -6, GETUTCDATE()))) = 
		CONVERT(VARCHAR(2), MONTH(f.LocalStartDate)) + '-' +  CONVERT(VARCHAR(2), DAY(f.LocalStartDate)) + '-' + CONVERT(VARCHAR(2), DAY(f.LocalStartDate))
		INNER JOIN General.ParametersByCustomer g ON a.CustomerId = g.CustomerId
	WHERE
		g.ResuourceKey = 'CUSTOMER_UTC'
	group BY f.VehicleId,
		a.VehicleId,
		a.CustomerId,
		a.Active,
		a.IntrackReference,
		d.[Device],
		e.GPSDateTime,
		e.Odometer,
		g.Value

	--INSERTA LOS DATOS EN ODOMETRO POR TABLA
	INSERT INTO General.OdometerVsRouteAlarm
		(VehicleId, 
		CustomerId, 
		Active, 
		OdometerStart, 
		OdometerEnd, 
		kmRoute, 
		AlarmSent, 
		LocalStartDate, 
		InsertDate)
	SELECT 
		VehicleId, 
		CustomerId, 
		Active, 
		Odometer, 
		Odometer, 
		0, 
		0, 
		LocalDate, 
		GETUTCDATE() 
	FROM 
		@tData 
	WHERE 
		ISNULL(OdometerVsRouteAlarmId, 0) = 0

	--ACTUALIZA LOS DATOS EN ODOMETRO POR TABLA
	UPDATE b
	SET b.CustomerId = a.CustomerId,
		b.OdometerEnd = a.Odometer,
		b.LocalEndDate = a.LocalDate,
		b.ModifyDate = GETUTCDATE()
	 FROM @tData a
		INNER JOIN General.OdometerVsRouteAlarm b 
			ON a.VehicleId = b.VehicleId AND 
			CONVERT(VARCHAR(2), MONTH(a.LocalDate)) + '-' +  CONVERT(VARCHAR(2), DAY(a.LocalDate)) + '-' + CONVERT(VARCHAR(2), DAY(a.LocalDate)) = 
			CONVERT(VARCHAR(2), MONTH(b.LocalStartDate)) + '-' +  CONVERT(VARCHAR(2), DAY(b.LocalStartDate)) + '-' + CONVERT(VARCHAR(2), DAY(b.LocalStartDate)) 
	WHERE 
		ISNULL(a.OdometerVsRouteAlarmId, 0) <> 0

	--SE ACTUALIZA EL KILOMETRAJE DEFINIDO PARA LAS RUTAS DE CADA VEHICULO
	DECLARE @lDay VARCHAR(1)
	SELECT @lDay = CASE
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 1 THEN 'D'
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 2 THEN 'L'
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 3 THEN 'K'
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 4 THEN 'M'
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 5 THEN 'J'
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 6 THEN 'V'
			WHEN DATEPART(DW,DATEADD(HOUR, -6, GETUTCDATE())) = 7 THEN 'S'
		END

		UPDATE a 
		SET a.kmRoute = b.Distance
		FROM General.OdometerVsRouteAlarm a
			INNER JOIN 
				(SELECT a.VehicleId VehicleId, 
			sum(b.Distance) Distance
			 FROM Efficiency.VehicleByRoute a
			INNER JOIN Efficiency.Routes b ON a.RouteId = b.RouteId
		WHERE a.Days LIKE '%'+@lDay+'%'
		GROUP BY a.VehicleId) b
			ON a.VehicleId = b.VehicleId AND 
			CONVERT(VARCHAR(2), MONTH(DATEADD(HOUR, -6, GETUTCDATE()))) + '-' +  CONVERT(VARCHAR(2), DAY(DATEADD(HOUR, -6, GETUTCDATE()))) + '-' + CONVERT(VARCHAR(2), DAY(DATEADD(HOUR, -6, GETUTCDATE()))) = 
			CONVERT(VARCHAR(2), MONTH(a.LocalStartDate)) + '-' +  CONVERT(VARCHAR(2), DAY(a.LocalStartDate)) + '-' + CONVERT(VARCHAR(2), DAY(a.LocalStartDate))
			
	
	--CARGA LA TABLA CON EL LISTADO DE CORREOS QUE SE DEBEN ENVIAR
		INSERT INTO @tEmails (
			OdometerVsRouteAlarmId,
			VehicleId,
			PlateId,
			CustomerId,
			kmRoute,
			Email)
		SELECT DISTINCT
			a.OdometerVsRouteAlarmId,
			a.VehicleId,
			v.PlateId,
			a.CustomerId,
			a.kmRoute,
			g.Email	
		FROM 
			General.OdometerVsRouteAlarm a
			INNER JOIN General.Vehicles v ON a.VehicleId = v.VehicleId
			INNER JOIN [General].CustomerUsers c ON a.CustomerId = c.CustomerId
			INNER JOIN [General].[Users] d ON c.[UserId] = d.[UserId]
			INNER JOIN [AspNetUsers] g ON d.[AspNetUserId] = g.[Id]
			INNER JOIN [AspNetUserRoles] e ON g.[Id] = e.[UserId]
			INNER JOIN [AspNetRoles] f ON e.[RoleId] = f.[Id]
		WHERE AlarmSent = 0 AND
			a.kmRoute > 0 AND 
			a.OdometerEnd - OdometerStart > kmRoute AND 
			a.InsertDate > DATEADD(DAY, -2, GETUTCDATE())
			
		--RECORRE EL LISTADO DE CORREOS PARA ENVIAR ALARMAS
		DECLARE @lOdometerVsRouteAlarmId INT
		DECLARE @lVehicleId INT
		DECLARE @lPlateId VARCHAR(10)
		DECLARE @lCustomerId INT
		DECLARE @lkmRoute FLOAT
		DECLARE @lEmail VARCHAR(MAX)

		SELECT @RowNum = Count(*) From @tEmails
		
		SELECT @Message = a.[Message] FROM [General].[Values] a 
			INNER JOIN [General].[Types] b ON a.[TypeId] = b.[TypeId]
		WHERE 
			b.[Code] = 'CUSTOMER_ALARM_TRIGGER_ODOMETER_VS_ROUTE'

		WHILE @RowNum > 0
		BEGIN

			SELECT 
				@lOdometerVsRouteAlarmId = OdometerVsRouteAlarmId, 
				@lVehicleId = VehicleId, 
				@lPlateId = PlateId,
				@lCustomerId = CustomerId, 
				@lkmRoute = kmRoute, 
				@lEmail = Email 
			FROM @tEmails 
			WHERE Id = @RowNum
			
			DECLARE @lMessage VARCHAR(MAX) = @Message
			SET @lMessage = REPLACE(@lMessage, '%PlateId%', @lPlateId)
			SET @lMessage = REPLACE(@lMessage, '%KmRoute%', @lkmRoute)

			--SE INSERTA EL CORREO A ENVIAR
			INSERT INTO General.EmailEncryptedAlarms
				([To],
				Subject,
				Message,
				Sent,
				InsertDate)
			VALUES(@lEmail,
				'Alarma de Exceso de Kilometraje',
				@lMessage,
				0,
				GETUTCDATE())
			
			UPDATE General.OdometerVsRouteAlarm 
			SET AlarmSent = 1
			WHERE OdometerVsRouteAlarmId = @lOdometerVsRouteAlarmId

		END
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF

END

GO


