USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesStopsList_Retrieve]    Script Date: 01/29/2015 13:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesStopsList_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_RoutesStopsList_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesStopsList_Retrieve]    Script Date: 01/29/2015 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 04/02/2014
-- Description:	Retrieve Stops information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_RoutesStopsList_Retrieve]
(
	 @pVehicleId INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT 
		a.StopId,
		a.Longitude,
		a.Latitude
	 FROM Efficiency.Stops a
		INNER JOIN General.Vehicles b ON a.VehicleId = b.VehicleId 
	WHERE b.IntrackReference = @pVehicleId AND
		a.GPSDateTimeEnd >= @pStartDate AND
		a.GPSDateTimeStart <= @pEndDate
	
    SET NOCOUNT OFF
END

GO
