USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_FleetCard_Alarm]    Script Date: 01/28/2015 11:02:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_FleetCard_Alarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_FleetCard_Alarm]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [General].[Sp_FleetCard_Alarm]    Script Date: 01/28/2015 11:02:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/28/2015
-- Description:	Stored Procedure that execute the alarm Add Alarm for Expiration of Fleet Card
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_FleetCard_Alarm]
AS
BEGIN 

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
		DECLARE @RowNum INT
		DECLARE @CreditCardId INT
		DECLARE @CreditCardNumber VARCHAR(500)
		DECLARE @ExpirationMonth INT
		DECLARE @ExpirationYear INT
		DECLARE @Name VARCHAR(500)
		DECLARE @Message VARCHAR(500)

		DECLARE @tCards TABLE(
			[Id] INT IDENTITY(1,1) NOT NULL ,
			[CreditCardId] INT,
			[CreditCardNumber] VARCHAR(500),
			[ExpirationMonth] INT,
			[ExpirationYear] INT,
			[Email] VARCHAR(500))
		
		INSERT INTO @tCards(
			[CreditCardId],
			[CreditCardNumber],
			[ExpirationMonth],
			[ExpirationYear],
			[Email])
		SELECT 
			a.[CreditCardId],
			a.[CreditCardNumber],
			a.[ExpirationMonth],
			a.[ExpirationYear],
			g.[Email]
		FROM [Control].[CreditCard] a
			INNER JOIN [General].[Status] b ON a.[StatusId] = b.[StatusId]
			INNER JOIN [General].[CustomerUsers] c ON a.[CustomerId] = c.[CustomerId]
			INNER JOIN [General].[Users] d ON c.[UserId] = d.[UserId]
			INNER JOIN [AspNetUsers] g ON d.[AspNetUserId] = g.[Id]
			INNER JOIN [AspNetUserRoles] e ON g.[Id] = e.[UserId]
			INNER JOIN [AspNetRoles] f ON e.[RoleId] = f.[Id]
			INNER JOIN [General].[ParametersByCustomer] h ON a.[CustomerId] = h.[CustomerId]
		WHERE 
			b.[IsActive] = 1 AND 
			b.[Code] NOT IN ('CUSTOMER_CREDIT_CARD_REJECTED_KEY','CUSTOMER_CREDIT_CARD_CANCELLED_KEY') AND 
			ISNULL(a.[ExpirationAlarmSent], 0) <> 1 AND 
			DATEADD(MONTH, 1, DATEADD(HOUR, CONVERT(INT, h.Value), GETUTCDATE())) >= CONVERT(varchar(2), a.[ExpirationMonth]) + '-01-' + CONVERT(varchar(6), a.[ExpirationYear]) AND 
			d.[IsActive] = 1 AND 
			d.[IsDeleted] = 0 AND
			d.[IsLockedOut] = 0 AND
			f.[Name] = 'CUSTOMER_ADMIN' AND 
			h.ResuourceKey = 'CUSTOMER_UTC'

		SELECT @Message = a.[Message] FROM [General].[Values] a 
			INNER JOIN [General].[Types] b ON a.[TypeId] = b.[TypeId]
		WHERE 
			b.[Code] = 'CUSTOMER_ALARM_TRIGGER_FLEETCARD_EXPIRATION'

		Select @RowNum = Count(*) From @tCards

		WHILE @RowNum > 0
		BEGIN
			DECLARE @lMessage VARCHAR(500) = @Message
			DECLARE @lMonth VARCHAR(10)

			SELECT 
				@CreditCardId = [CreditCardId], 
				@CreditCardNumber = [CreditCardNumber], 
				@ExpirationMonth = [ExpirationMonth], 
				@ExpirationYear = [ExpirationYear], 
				@Name = [Email]
			FROM @tCards
			WHERE Id = @RowNum
		
			IF @ExpirationMonth = 1 SET @lMonth = 'Enero'
			ELSE IF @ExpirationMonth = 2 SET @lMonth = 'Febrero'
			ELSE IF @ExpirationMonth = 3 SET @lMonth = 'Marzo'
			ELSE IF @ExpirationMonth = 4 SET @lMonth = 'Abril'
			ELSE IF @ExpirationMonth = 5 SET @lMonth = 'Mayo'
			ELSE IF @ExpirationMonth = 6 SET @lMonth = 'Junio'
			ELSE IF @ExpirationMonth = 7 SET @lMonth = 'Julio'
			ELSE IF @ExpirationMonth = 8 SET @lMonth = 'Agosto'
			ELSE IF @ExpirationMonth = 9 SET @lMonth = 'Setiembre'
			ELSE IF @ExpirationMonth = 10 SET @lMonth = 'Octubre'
			ELSE IF @ExpirationMonth = 11 SET @lMonth = 'Noviembre'
			ELSE IF @ExpirationMonth = 12 SET @lMonth = 'Diciembre'
		
			SET @lMessage = REPLACE(@lMessage, '%ExpirationMonth%', @lMonth)
			SET @lMessage = REPLACE(@lMessage, '%ExpirationYear%', @ExpirationYear)
			
			INSERT INTO [General].[EmailFleetCardAlarms]
				([To], 
				[Subject], 
				[Message], 
				[Sent],
				[CreditCardNumber], 
				[InsertDate])
			VALUES
				(@Name,
				'Alarma de vencimiento de tarjeta',
				@lMessage,
				0,
				@CreditCardNumber,
				GETUTCDATE())

			UPDATE [Control].[CreditCard] 
			SET [ExpirationAlarmSent] = 1 
			WHERE [CreditCardId] = @CreditCardId

			SET @RowNum = @RowNum - 1
		END 

	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF

END
GO


