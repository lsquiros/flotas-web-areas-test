USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesDetail_AddOrEdit]    Script Date: 11/19/2014 11:42:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	AddOrEdit RouteDetail information
-- ================================================================================================
ALTER PROCEDURE [Efficiency].[Sp_RoutesDetail_AddOrEdit]
(
	 @pRouteId INT,
	 @pLoggedUserId INT,
	 @pXmlPoints XML,
	 @pXmlSegments XML
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			--delete points
			DELETE [Efficiency].[RoutesPoints]
			WHERE [RouteId] = @pRouteId
			
			--delete segments
			DELETE [Efficiency].[RoutesSegments]
			WHERE [RouteId] = @pRouteId
			
			--insert points
			INSERT INTO [Efficiency].[RoutesPoints]
				([RouteId]
				,[PointReference]
				,[Order]
				,[Name]
				,[Time]
				,[Latitude]
				,[Longitude]
				,[InsertDate]
				,[InsertUserId]
				,[ModifyDate]
				,[ModifyUserId]
				,[StopTime])
			SELECT
				@pRouteId as [RouteId],
				COALESCE([Table].[Column].value('PointReference [1]', 'int'), 0) as [PointReference],
				COALESCE([Table].[Column].value('Order [1]', 'int'), 0) as [Order],				
				COALESCE([Table].[Column].value('Name [1]', 'varchar(50)'), '') as [Name],
				COALESCE([Table].[Column].value('Time [1]', 'datetime'), '') as [Time],
				COALESCE([Table].[Column].value('Latitude [1]', 'float'), 0) as [Latitude],
				COALESCE([Table].[Column].value('Longitude [1]', 'float'), 0) as [Longitude],
				GETUTCDATE() as [InsertDate],
				@pLoggedUserId as [InsertUserId],
				GETUTCDATE() as [ModifyDate],
				@pLoggedUserId as [ModifyUserId],
				COALESCE([Table].[Column].value('StopTime [1]', 'int'), 0) as [StopTime]
			FROM @pXmlPoints.nodes('/ ArrayOfRoutesPoints / RoutesPoints ') as [Table]([Column])
	
			--insert segments
			INSERT INTO [Efficiency].[RoutesSegments]
				([RouteId]
				,[StartReference]
				,[EndReference]
				,[Poliyline]
				,[InsertDate]
				,[InsertUserId]
				,[ModifyDate]
				,[ModifyUserId]
				,[Time]
				,[Distance])
			SELECT
				@pRouteId as [RouteId],
				COALESCE([Table].[Column].value('StartReference [1]', 'int'), 0) as [StartReference],
				COALESCE([Table].[Column].value('EndReference [1]', 'int'), 0) as [EndReference],
				geometry::STGeomFromText(COALESCE([Table].[Column].value('Poliyline [1]', 'varchar(max)'), ''), 0) as [Poliyline],
				
				--,(geometry::STGeomFromText(@pPolygon, 0))
				
				GETUTCDATE() as [InsertDate],
				@pLoggedUserId as [InsertUserId],
				GETUTCDATE() as [ModifyDate],
				@pLoggedUserId as [ModifyUserId],
				COALESCE([Table].[Column].value('Time [1]', 'int'), 0) as [Time],
				COALESCE([Table].[Column].value('Distance [1]', 'float'), 0) as [Distance]
			FROM @pXmlSegments.nodes('/ ArrayOfRoutesSegments / RoutesSegments ') as [Table]([Column])

			declare @stopTime int 
			declare @time int 
			declare @distance float 
			SET @time = (SELECT SUM([StopTime]) FROM [Efficiency].[RoutesPoints] WHERE [RouteId] = @pRouteId) 
			SET @stopTime = (SELECT sum([Time]) FROM [Efficiency].[RoutesSegments] WHERE [RouteId] = @pRouteId)
			SET @distance = (SELECT SUM([Distance]) FROM [Efficiency].[RoutesSegments] WHERE [RouteId] = @pRouteId)
			SELECT @time + @stopTime
			SELECT @distance
			UPDATE Efficiency.Routes SET [Time] = @stopTime + @time, [Distance] = @distance WHERE RouteId = @pRouteId

            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END



GO
