USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_AddFromAPI]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Customers_AddFromAPI]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/19/2014
-- Description:	Insert Customer information from API
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Customers_AddFromAPI]
(
	 @pName VARCHAR(250)
	,@pCurrencyCode VARCHAR(10)
	,@pAccountNumber VARCHAR(50)
	,@pMasterCreditCard VARCHAR(50)
	,@pLoggedUserId INT
)
AS
BEGIN
	PRINT 'not implemented exception'
END
GO