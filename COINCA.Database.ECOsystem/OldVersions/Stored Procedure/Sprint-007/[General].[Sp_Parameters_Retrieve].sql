USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Parameters_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Parameters_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve Parameters information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Parameters_Retrieve]
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT
		 a.[SafeSpeedMargin]
		,a.[AVLOdometerErrorMargin]
		,a.[POSOdometerErrorMargin]
		,a.[PointArrivalTimeTolerance]
		,a.[GeoPointErrorMargin]
		,a.[PasswordEffectiveDays]
		,a.[PasswordsValidateRepetition]
		,a.[RowVersion]
    FROM [General].[Parameters] a	
	
    SET NOCOUNT OFF
END
