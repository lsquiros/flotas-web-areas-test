USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Counties_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Counties_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Napole�n Alvarado
-- Create date: 12/17/2014
-- Description:	Retrieve Counties information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Counties_Retrieve]
(
	 @pCountyId INT = NULL,
	 @pStateId INT = NULL,
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[CountyId]
		,a.[Name]
		,a.[StateId]
		,a.[Code]
		,a.[Latitude]
		,a.[Longitude]
		,b.[CountryId]
		,a.[RowVersion]
    FROM [General].[Counties] a
		INNER JOIN [General].[States] b
			ON a.[StateId] = b.[StateId]
	WHERE (@pCountyId IS NULL OR a.[CountyId] = @pCountyId)
	  AND (@pStateId IS NULL OR a.[StateId] = @pStateId)
	  AND (@pCountryId IS NULL OR b.[CountryId] = @pCountryId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR a.[Code] like '%'+@pKey+'%')
	ORDER BY a.[Code]
	
    SET NOCOUNT OFF
END
