USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Countries_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Countries_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/09/2014
-- Description:	Retrieve country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Countries_Retrieve]
(
	 @pCountryId INT = NULL,
	 @pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
		
	SELECT
		 a.[CountryId]
		,a.[CurrencyId]
		,a.[Name]
		,a.[Code]
		,b.[Name] AS [CurrencyName]
		,a.[RowVersion]
    FROM [General].[Countries] a
		INNER JOIN [Control].[Currencies] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE (@pCountryId IS NULL OR [CountryId] = @pCountryId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR a.[Code] like '%'+@pKey+'%'
				OR b.[Name] like '%'+@pKey+'%')
	ORDER BY [CountryId] DESC
	
    SET NOCOUNT OFF
END
