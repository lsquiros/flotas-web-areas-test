USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_PassControl_Process_Job]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_PassControl_Process_Job]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author: Gerald Solano
-- Create date: 16/07/2015
-- Description:	Execute process from JOB or Schedule task
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_PassControl_Process_Job]
AS
BEGIN

DECLARE @DATE DATETIME =  DATEADD(hour, -6, GETDATE())

EXEC [dbo].[Sp_PassControl_Process] @DATE

END