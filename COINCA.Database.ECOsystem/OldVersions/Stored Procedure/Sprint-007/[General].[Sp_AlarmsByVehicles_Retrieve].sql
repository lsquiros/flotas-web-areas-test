USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AlarmsByVehicles_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_AlarmsByVehicles_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==
-- ================================================================================================
-- Author:		Andr�s Oviedo B.
-- Create date: 14/01/2015
-- Description: Retrieve Alarm by Vehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_AlarmsByVehicles_Retrieve]
(
	@pAlarmId INT = NULL, 
	@pCustomerId INT=Null,
	@pAlarmTriggerId INT=Null, 
	@pEntityTypeId INT=Null, 
	@pEntityId INT = NULL,
	@pStartDate datetime,
	@pEndDate datetime
)
AS 
BEGIN 
		SET NOCOUNT ON
		
		SELECT 
			 a.[AlarmId]
			,a.[Phone]
			,a.[Email]
			,a.[AlarmTriggerId]
			,(Select Name FROM [General].[Types] WHERE a.[AlarmTriggerId]=TypeId) AS AlarmType
			,a.[EntityTypeId]
			,case [EntityTypeId] WHEN 400 THEN (Select  PlateId FROM [General].Vehicles WHERE VehicleId=a.[EntityId])
			  WHEN 401 THEN (Select Name FROM General.DriversUsers du INNER JOIN General.Users u ON du.UserId=u.UserId WHERE du.DriversUserId=a.[EntityId])
			  WHEN 402 THEN (Select Name FROM General.VehicleGroups WHERE VehicleGroupId=a.[EntityId])  
			  WHEN 403 THEN (Select Name FROM General.VehicleCostCenters WHERE CostCenterId=a.[EntityId])  
			  WHEN 404 THEN (Select Name FROM General.VehicleUnits WHERE UnitId=a.[EntityId]) 
			 end  as Entity
			 ,cast((case [EntityTypeId]
			  WHEN 401 THEN 1
			  else 0
			  end) as bit)as IsEncrypted
			,(Select Name FROM [General].[Types] WHERE a.[EntityTypeId]=TypeId) as EntityType
			,a.[EntityId]
			,a.[Active]
			,(Select Name FROM [General].[Types] WHERE a.[PeriodicityTypeId]=TypeId) AS Periodicity
			,a.[RowVersion]
			,a.NextAlarm
		FROM [General].[Alarms] a
		WHERE (@pAlarmId IS NULL OR a.[AlarmId] = @pAlarmId)
		  AND (@pEntityId IS NULL OR [a].[EntityId]  = @pEntityId)
		  AND [a].[CustomerId] = isnull(@pCustomerId,a.[CustomerId])
		  AND [a].[AlarmTriggerId] = isnull(@pAlarmTriggerId,a.AlarmTriggerId) 
		  AND [a].[EntityTypeId] =isnull(@pEntityTypeId,a.EntityTypeId)
		  AND a.NextAlarm is not null  
		  and a.SentDate between @pStartDate and @pEndDate
		ORDER BY [a].[AlarmId] DESC
		SET NOCOUNT OFF
END


