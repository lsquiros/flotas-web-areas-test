USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreSpeedDailyJob]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_ScoreSpeedDailyJob]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [ECOsystem]
GO

-- Created by:	Gerald Solano C.
-- Created date: 08/06/2015 
-- Description: Save Score Driving Daily

CREATE Procedure [Operation].[Sp_ScoreSpeedDailyJob]
AS		
Begin
Declare @ldt_Start_Job datetime,
		@ldt_End_Job datetime,
		@lvc_Job varchar(30),
		@lc_status char(1),
		@li_error int = 0,
		@lvc_error_ref varchar(100),
		@li_CustomerId Int,
		@li_Day Int,
		@li_Month Int,
		@li_Year Int

Declare @Scores table   -- TABLA TEMPORAL PARA LAS CALIFICACIONES TEMPORALES PRECALCULADAS
	(DriverScoreId int,
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 Score Float,
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float,
	 Date_Time datetime,
	 MaxOdometer int,
	 MinOdometer int,
	 Vec float,
	 Dt float,
	 De float,
	 Ce float)

	Set @lvc_Job = 'ScoreSpeedDaily' -- NOMBRE DEL SP EJECUTADO PARA REGISTRAR EN EL LOG DE PROCESOS
	Set @ldt_Start_Job = GETDATE() -- OBTENEMOS D�A ACTUAL
	Set @li_Day = DATEPART(DD, @ldt_Start_Job)  -- EXTRAEMOS D�A
	Set @li_Month = DATEPART(MM, @ldt_Start_Job) -- EXTRAEMOS MES
	Set @li_Year = DATEPART(YY, @ldt_Start_Job) -- EXTRAEMOS A�O
	
	-- OBTENEMOS D�A Y HORA INICIAL Y D�A Y HORA FINAL CON BASE AL D�A ACTUAL
	DECLARE @pInitialDate datetime=  CAST(
										  CAST(@li_Year AS VARCHAR(4)) +
										  RIGHT('0' + CAST(@li_Month AS VARCHAR(2)), 2) +
										  RIGHT('0' + CAST(@li_Day AS VARCHAR(2)), 2)
										  --RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
									AS DATETIME)

	DECLARE @pFinalDate datetime= DATEADD(day, 1, @pInitialDate) 
								  --DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0)) 
								  
	--SELECT @pInitialDate, @pFinalDate

	--BORRAMOS LOS REGISTROS PREVIOS DEL D�A A PROCESAR
	DELETE Operation.DriversScoresDaily
	WHERE 
	   ScoreDay = @li_Day and
	   ScoreMonth = @li_Month
	   and ScoreYear = @li_Year
	-- /////////////////////////////////////////////

	-- RECORREMOS LOS CLIENTES (CUSTOMERS)
	Select @li_CustomerId = MIN(D.CustomerId)  -- OBTENEMOS EL INDICE MINIMO PARA EL RECORRIDO
	  from General.DriversUsers D
	  inner join General.Users U on D.UserId = U.UserId
	 where U.IsActive = 1   
	
	--SET @li_CustomerId = 6

	While @li_CustomerId is not null
	begin
		
		-- ///////////////////////////////////////////////////////////////////////////////////
		--Generar registros de c�lculo por l�mite administrativo (A)
		--////////////////////////////////////////////////////////////////////////////////////
		Insert into @Scores
			(DriverScoreId, 
					UserID, Name, Identification, Score,
					KmTraveled,
					OverSpeed,
					OverSpeedAmount,
					OverSpeedDistancePercentage ,
					OverSpeedAverage ,
					OverSpeedWeihgtedAverage ,
					StandardDeviation ,
					Variance ,
					VariationCoefficient,
					OverSpeedDistance,
					Date_Time,
					MaxOdometer,
					MinOdometer,
					Vec,
					Dt,
					De,
					Ce)
		Exec Operation.Sp_ScoreSpeedDaily @li_CustomerId, null, null, @pInitialDate, @pFinalDate, 'A'
		
		--/////////////////////////////////////////////////////////////////////////////////////////


		Set @li_error = @@ERROR  -- ASIGNAMOS LA CANTIDAD DE ERRORES DETECTADOS EN LA TRANSACCI�N
		If @li_error <> 0 
		begin
			Break
		end
		

		-- INSERTAMOS LOS DATOS CALCULADOS POR VELOCIDAD ADMINISTRATIVA
		Insert into Operation.DriversScoresDaily
			(CustomerId, UserId, ScoreYear, ScoreMonth, ScoreDay, ScoreType, Score,KmTraveled,
						OverSpeed,
						OverSpeedAmount,
						OverSpeedDistancePercentage,
						OverSpeedAverage,
						OverSpeedWeihgtedAverage,
						StandardDeviation,
						Variance,
						VariationCoefficient,
						OverSpeedDistance,
						Date_Time,
						MaxOdometer,
						MinOdometer,
						Vec,
						Dt,
						De,
						Ce)
		Select @li_CustomerId, UserId, @li_Year, @li_Month, DATEPART(DD, Date_Time) , 'SPADM', Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,OverSpeedDistance,
						Date_Time,
						MaxOdometer,
						MinOdometer,
						Vec,
						Dt,
						De,
						Ce
		  from @Scores
		--////////////////////////////////////////////////////////////////////////////////
		

		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end

		Delete @Scores -- Borramos los scores anteriormente pre-calculados

		-- FIN DE C�lculo por l�mite administrativo
		-- ///////////////////////////////////////////////////////////////////////////////////


		-- ///////////////////////////////////////////////////////////////////////////////////
		--Generar registros de c�lculo por l�mite de carretera (R)
		--////////////////////////////////////////////////////////////////////////////////////
		Insert into @Scores
			(DriverScoreId, 
					UserID, Name, Identification, Score,
					KmTraveled,
					OverSpeed,
					OverSpeedAmount,
					OverSpeedDistancePercentage ,
					OverSpeedAverage ,
					OverSpeedWeihgtedAverage ,
					StandardDeviation ,
					Variance ,
					VariationCoefficient,
					OverSpeedDistance,
					Date_Time,
					MaxOdometer,
					MinOdometer,
					Vec,
					Dt,
					De,
					Ce)
		Exec Operation.Sp_ScoreSpeedDaily @li_CustomerId, null, null, @pInitialDate, @pFinalDate, 'R'
		
		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end
		
		--SELECT 'ROUTE',* FROM @Scores WHERE UserId = 1223

		Insert into Operation.DriversScoresDaily
			(CustomerId, UserId, ScoreYear, ScoreMonth, ScoreDay, ScoreType, Score,KmTraveled,
						OverSpeed,
						OverSpeedAmount,
						OverSpeedDistancePercentage,
						OverSpeedAverage,
						OverSpeedWeihgtedAverage,
						StandardDeviation,
						Variance,
						VariationCoefficient,
						OverSpeedDistance,
						Date_Time,
						MaxOdometer,
						MinOdometer,
						Vec,
						Dt,
						De,
						Ce)
		Select @li_CustomerId, UserId, @li_Year, @li_Month, DATEPART(DD, Date_Time), 'SPROU', Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,OverSpeedDistance,
						Date_Time,
						MaxOdometer,
						MinOdometer,
						Vec,
						Dt,
						De,
						Ce
		  from @Scores

		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end

		Delete @Scores -- Borramos los scores anteriormente pre-calculados

		-- FIN DE C�lculo por l�mite de carretera 
		-- ///////////////////////////////////////////////////////////////////////////////////

		
		--Siguiente Customer: OBTENEMOS EL INDICE MINIMO QUE SEA MAYOR AL QUE SE ACABA DE PROCESAR
		Select @li_CustomerId = MIN(D.CustomerId)
		  from General.DriversUsers D
		  inner join General.Users U on D.UserId = U.UserId
		 where U.IsActive = 1   
		   and D.CustomerId > @li_CustomerId


	END -- TERMINA WHILE //////////////////////////////////////////


	-- CONTROL DE EXCEPCI�N Y DEL PROCESO DEL SP
	If @li_error <> 0 
	BEGIN
		Set @lc_status = 'E' --Error
		Set @lvc_error_ref = CONCAT('Customer Id: ', @li_CustomerId)
	END
	ELSE
	BEGIN
		Set @lc_status = 'T' --Terminado		
	END

	--Insertar datos en Log de los JOBS ////////////////////////////////
	Set @ldt_End_Job = GETDATE()

	INSERT INTO Operation.JobsLog
		(JobID, StartDate, EndDate, Status, ErrorNumber, ErrorReference)
	VALUES
		(@lvc_Job, @ldt_Start_Job, @ldt_End_Job, @lc_status, @li_error, @lvc_error_ref)
		
END -- /// FIN DEL SP


