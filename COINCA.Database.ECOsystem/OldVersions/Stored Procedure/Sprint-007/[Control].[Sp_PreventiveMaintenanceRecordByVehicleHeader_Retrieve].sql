USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo Gonz�lez
-- Create date: 19/01/2015
-- Description:	Retrieve Preventive Maintenance Record Header By Vehicle Detail
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PreventiveMaintenanceRecordByVehicleHeader_Retrieve]
(
	 @pId INT
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @Id INT
	DECLARE @CatalogId INT
	
	SELECT @Id = PreventiveMaintenanceRecordByVehicleId FROM General.PreventiveMaintenanceRecordByVehicle WHERE PreventiveMaintenanceId = @pId

	IF @id IS NULL
		BEGIN

			SELECT 
				0 [PreventiveMaintenanceRecordByVehicleId],
				[PreventiveMaintenanceId] [PreventiveMaintenanceId],
				GETUTCDATE() [Date],
				0 [Odometer],
				CAST(0.00 AS DECIMAL(16, 2)) [Record]
			FROM [General].[PreventiveMaintenanceByVehicle] 
			WHERE 
				[PreventiveMaintenanceId] = @pId
        
		END
	ELSE
		BEGIN
		
			SELECT 
				[PreventiveMaintenanceRecordByVehicleId],
				[PreventiveMaintenanceId],
				[Date],
				[Odometer],
				[Record]
			FROM [General].[PreventiveMaintenanceRecordByVehicle]
			WHERE 
				[PreventiveMaintenanceId] = @pId
	
		END

    SET NOCOUNT OFF
END
GO