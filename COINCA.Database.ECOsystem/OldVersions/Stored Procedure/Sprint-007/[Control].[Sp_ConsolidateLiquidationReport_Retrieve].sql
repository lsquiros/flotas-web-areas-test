USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ConsolidateLiquidationReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_ConsolidateLiquidationReport_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andrés Oviedo Brenes.
-- Create date: 03/02/2015
-- Description:	Retrieve ConsolidateLiquidationReport information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_ConsolidateLiquidationReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	declare @ty int= (select top (1) t.TypeId from General.Customers c INNER JOIN General.[Types] t ON t.TypeId=c.IssueForId 
	where CustomerId=@pCustomerId)
	
	SELECT v.VehicleId,
		   v.PlateId,
		   du.Identification,
		   vf.Liters,
		   (SELECT top(1) TrxReportedOdometer FROM General.PerformanceByVehicle
		    where (MONTH(TrxDate)=@pMonth and YEAR(TrxDate)=@pYear) and VehicleId=v.VehicleId order by PerformanceVehicleId desc)		  
		    as TotalOdometer,
		   vc.Liters as TankCapacity,
		   f.FuelId,
		   f.Name as FuelName,
		   vu.UnitId,
		   vu.Name as UnitName,
		   vcc.CostCenterId,
		   vcc.Name as CostCenterName,
		   vc.Manufacturer,
		   vc.VehicleModel,
		   vc.[Year],
		   du.Code,
		   (case @ty when 100 then (SELECT top(1) cc.CreditCardNumber FROM [Control].CreditCardByDriver ccd INNER JOIN [Control].CreditCard cc  ON ccd.CreditCardId=cc.CreditCardId WHERE CCD.UserId=du.UserId)
		   ELSE
		   (SELECT top(1) cc.CreditCardNumber FROM [Control].CreditCardByVehicle ccd INNER JOIN [Control].CreditCard cc  ON ccd.CreditCardId=cc.CreditCardId WHERE CCD.VehicleId=V.VehicleId)
		   END) AS CreditCardNumber
		   FROM General.Vehicles v 
		   INNER JOIN [Control].VehicleFuel vf ON v.VehicleId=vf.VehicleId
		   INNER JOIN General.VehicleCategories vc ON v.VehicleCategoryId =vc.VehicleCategoryId
           INNER JOIN [Control].Fuels f ON vc.DefaultFuelId=f.FuelId
		   INNER JOIN General.VehicleCostCenters vcc ON V.CostCenterId=vcc.CostCenterId
		   INNER JOIN General.VehicleUnits vu ON vcc.UnitId=vu.UnitId
		   INNER JOIN General.VehiclesByUser vbu ON vbu.VehicleId=v.VehicleId
		   INNER JOIN General.DriversUsers du ON vbu.UserId=du.UserId
		   WHERE V.CustomerId=@pCustomerId and 
		   (vf.[Month]=@pMonth and vf.[Year]=@pYear)
		  
		  
	SET NOCOUNT OFF
END
