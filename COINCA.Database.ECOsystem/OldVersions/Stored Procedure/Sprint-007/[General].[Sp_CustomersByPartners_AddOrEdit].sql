USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByPartners_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomersByPartners_AddOrEdit]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo
-- Create date: 10/02/2015
-- Description:	Insert or Update Customer By Partner information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomersByPartners_AddOrEdit]
(
	 @pCustomerId INT						
	,@pPartnerId INT					
	,@pLoggedUserId INT						
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
       
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
		
			DECLARE @ExistRow INT = 0
			
			SELECT @ExistRow = COUNT(*)
					FROM [General].[CustomersByPartner] cp
					WHERE cp.CustomerId = @pCustomerId AND cp.PartnerId = @pPartnerId
			
			IF(@ExistRow = 0) BEGIN -- VERIFICA QUE UN CUSTOMER NO SE AGREGUE DUPLICADO A UN MISMO PARTNER			
			
				INSERT INTO [General].[CustomersByPartner]
							(CustomerId
							,PartnerId
							,[InsertDate]
							,[InsertUserId])
						VALUES
							(@pCustomerId,
							 @pPartnerId,
							 GETUTCDATE(),
							 @pLoggedUserId)
			END
		
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO