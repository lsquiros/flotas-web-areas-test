USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_NotActiveCustomersCount_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Insurance].[Sp_NotActiveCustomersCount_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes
-- Create date: 18/03/2015
-- Description:	Retrieve Not Active Customers Count information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_NotActiveCustomersCount_Retrieve]
(
	@pCustomerId INT = NULL,
	@pCountryId INT = NULL,
	@pPartnerId INT=null
)
AS
BEGIN

	SET NOCOUNT ON
	

	SELECT distinct
		   COUNT(*)
    FROM [General].[Customers] a
		INNER JOIN [Control].[Currencies] b
			ON a.[CurrencyId] = b.[CurrencyId]
		LEFT JOIN [General].[Types] c
			ON c.[TypeId] = a.[IssueForId]
		INNER JOIN [General].[Types] d
			ON d.[TypeId] = a.[UnitOfCapacityId]
		INNER JOIN [General].[Countries] e
			ON a.[CountryId] = e.[CountryId]
		INNER JOIN 	General.CustomersByPartner cbp
			ON a.CustomerId=cbp.CustomerId
	WHERE a.[IsDeleted]=CAST(0 as bit)
	  and a.IsActive=0
	  AND (@pCustomerId IS NULL OR a.[CustomerId] = @pCustomerId)
	  AND (@pCountryId IS NULL OR a.[CountryId] = @pCountryId)
	  AND (@pPartnerId IS NULL OR cbp.PartnerId=@pPartnerId)

	
    SET NOCOUNT OFF
END
