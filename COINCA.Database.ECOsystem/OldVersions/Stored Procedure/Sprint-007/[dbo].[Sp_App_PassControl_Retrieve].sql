USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_App_PassControl_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_App_PassControl_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Improvement: Gerald Solano
-- Create date: 10/30/2014
-- Description:	Retrieve PassControl information
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_App_PassControl_Retrieve]
(
	@pLongitude float,
	@pLatitude float,
	@pDistance float,
	@pUnitId VARCHAR(MAX),
	@pStartDate VARCHAR(21),
	@pEndDate VARCHAR(21)
)
AS
BEGIN
	DECLARE @i bigint = 0
	DECLARE @numrows int = 0
	DECLARE @LastUnitID Decimal(20, 0) = NULL
	DECLARE @LastDevice int = NULL
	DECLARE @UnitID Decimal(20, 0) = NULL
	DECLARE @InDateTime dateTime = NULL
	DECLARE @OutDateTime dateTime = NULL
	DECLARE @InRange int = NULL
	DECLARE @LastInRange int = NULL
	DECLARE @InsertData bit = 0
	DECLARE @ReportsList TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		UnitID Decimal(20, 0),
		Device int,
		GPSDateTime dateTime,
		InRange int
	)
	DECLARE @ReportsListFilter TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		UnitID Decimal(20, 0),
		Device int,
		GPSDateTime dateTime,
		InRange int
	)
	DECLARE @DevicesTemp TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		Device int,
		UnitID Decimal(20, 0)
	)
	DECLARE @ReportsTemp TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		Device int,
		UnitID Decimal(20, 0),
		GPSDateTime dateTime,
		Longitude float,
		Latitude float
	)
	DECLARE @Result TABLE
	(
		UnitID Decimal(20, 0),
		Device int,
		InDateTime dateTime NULL,
		OutDateTime dateTime NULL
	)

	-- REALIZAMOS UN SPLIT DE LOS UNIT_ID'S CONCATENADOS
    DECLARE @separator varchar(max) = ','
    DECLARE @Splited table(item varchar(max))
    SET @pUnitId = REPLACE(@pUnitId,@separator,'''),(''')
    SET @pUnitId = 'select * from (values('''+@pUnitId+''')) as V(A)' 
    INSERT INTO @Splited
    EXEC (@pUnitId)
	--/////////////////////////////////////////////////////////////////////////////////////////////
	SET NOCOUNT ON
	
	-- OBTENEMOS EL PUNTO GEOGRAFICO A COMPARAR
	DECLARE @p GEOGRAPHY = GEOGRAPHY::STGeomFromText('POINT(' + CONVERT(VARCHAR(MAX), @pLongitude) + ' ' + CONVERT(VARCHAR(MAX), @pLatitude) + ')', 4326);
	
	-- OBTENEMOS LOS DEVICES DESDE EL PARAMETRO CONSULTADO
	INSERT INTO @DevicesTemp (Device, UnitID)
		SELECT [Device], [UnitID] 
		FROM [dbo].[Devices]
		WHERE CONVERT(varchar(max), [UnitID]) in (select item from @Splited) 

	-- OBTENEMOS LOS DATOS DE REPORTS DESDE EL RANGO DE FECHAS CONSULTADO
	INSERT INTO @ReportsTemp (Device, UnitID, GPSDateTime, Longitude, Latitude)
		SELECT 
			r.[Device],
			d.[UnitID],
			r.[GPSDateTime],
			r.[Longitude],
			r.[Latitude]
		FROM 
			[dbo].[Reports] r 
			INNER JOIN @DevicesTemp d ON r.[Device] = d.[Device]
		WHERE 
			r.[GPSDateTime] >= @pStartDate AND 
			r.[GPSDateTime] < @pEndDate 
		ORDER BY r.[GPSDateTime]
	--/////////////////////////////////////////////////////////////////////////////////////////////

	--SELECT * FROM @ReportsTemp

	
	-- RECORREMOS LOS PUNTOS DE REPORTS TEMP PARA ESTABLECER SI UN UNIT_ID ESTUVO DENTRO DE UN RANGO
	INSERT INTO @ReportsList (UnitID, Device, GPSDateTime, InRange)
		SELECT 
			r.UnitID,
			r.Device,
			r.GPSDateTime, 
        GEOGRAPHY::STGeomFromText('POINT('+ 
            convert(nvarchar(20), r.Longitude)+' '+
            convert( nvarchar(20), r.Latitude)+')', 4326)
	        .STBuffer(@pDistance).STIntersects(@p) as [Intersects]
		FROM 
			@ReportsTemp r 
			--INNER JOIN [dbo].[Devices] d ON r.[Device] = d.[Device]
		--WHERE 
		--	r.[GPSDateTime] >= @pStartDate AND 
		--	r.[GPSDateTime] < @pEndDate AND 
		 	--CONVERT(varchar(max), d.[UnitID]) in (select item from @Splited) 
		ORDER BY r.[UnitID], r.[GPSDateTime]


	--SELECT * FROM @ReportsList where InRange = 1


	SET @UnitID = 0
	SET @i = 1
	SET @numrows = (SELECT COUNT(*) FROM @ReportsList)
	IF @numrows > 0
    
    INSERT INTO @ReportsListFilter (UnitID, Device, GPSDateTime, InRange)
    SELECT TOP(1) UnitID, Device, GPSDateTime, InRange FROM @ReportsList
    UNION
    SELECT x.UnitID, x.Device, x.GPSDateTime, x.InRange FROM @ReportsList x
    LEFT OUTER JOIN @ReportsList y ON x.Id = y.Id + 1 AND (x.InRange <> y.InRange OR x.UnitID <> y.UnitID) 
	WHERE y.Id IS NOT NULL 
	
    WHILE (@i <= (SELECT MAX(Id) FROM @ReportsListFilter))
    BEGIN
		IF @i > 1
		BEGIN
			SET @InsertData = 0
			SET @UnitID = (SELECT UnitID FROM @ReportsListFilter WHERE Id = @i)
			SET @LastUnitID = (SELECT UnitID FROM @ReportsListFilter WHERE Id = @i - 1)
			SET @LastDevice = (SELECT Device FROM @ReportsListFilter WHERE Id = @i - 1)

			IF (@UnitID = @LastUnitID)
			BEGIN

				SET @InRange = (SELECT InRange FROM @ReportsListFilter WHERE Id = @i)
				SET @LastInRange = (SELECT InRange FROM @ReportsListFilter WHERE Id = @i - 1)
				IF (@inRange != @LastInRange)
				BEGIN
					IF @InRange = 1
						BEGIN
							SET @InDateTime = (SELECT GPSDateTime FROM @ReportsListFilter WHERE Id = @i)
						END
					ELSE
						BEGIN
							SET @OutDateTime = (SELECT GPSDateTime FROM @ReportsListFilter WHERE Id = @i)
							SET @InsertData = 1
					END
				END
				IF @InsertData = 1
				BEGIN
					INSERT INTO @Result (UnitID, Device, InDateTime, OutDateTime)
					VALUES(@LastUnitID, @LastDevice, @InDateTime, @OutDateTime)
					SET @InDateTime = NULL
					SET @OutDateTime = NULL
				END
			END

		END
        SET @i = @i + 1
    END

	DECLARE @COUNT_ROWS INT 

	SELECT @COUNT_ROWS = COUNT(*) FROM @Result 

	IF(@COUNT_ROWS IS NOT NULL AND @COUNT_ROWS > 0)
	BEGIN 
		SELECT 
			CONVERT(varchar(25), [UnitID]) [UnitID],
			[Device], 
			[InDateTime], 
			[OutDateTime]
		FROM @Result 
	END
	ELSE
	BEGIN
		SELECT 
			NULL,
			0, 
			NULL, 
			NULL
	END

    SET NOCOUNT OFF
END

GO


