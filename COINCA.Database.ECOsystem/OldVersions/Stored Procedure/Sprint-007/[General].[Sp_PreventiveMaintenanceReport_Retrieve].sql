USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceReport_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_PreventiveMaintenanceReport_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 06/Oct/2014
-- Description:	Preventive Maintenance By Vehicle information
-- ===========================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceReport_Retrieve]
	 @pCustomerId INT
	,@pPreventiveMaintenanceId INT = NULL
	,@pPreventiveMaintenanceCatalogId INT
AS
BEGIN 
	SET NOCOUNT ON;
	
	SELECT 
		a.[PreventiveMaintenanceId],
		a.[Odometer] AS [OdometerValue], 
		(a.[NextReview]-c.[AlertBefore]) AS [Alert],
		a.[NextReview],
		a.[Day],
		a.[Month],
		a.[Year],
		c.[Frequency],
		c.[AlertBefore],
		b.[PlateId],
		b.[VehicleId],
		c.[FrequencyTypeId],
		d.[Name] AS Periodicity,
		e.[Value],
		c.Cost,
		f.Record,
		a.[RowVersion]
	FROM General.[PreventiveMaintenanceByVehicle] a
		INNER JOIN [General].[Vehicles] b
			ON b.[VehicleId] = a.[VehicleId]
		INNER JOIN [General].[PreventiveMaintenanceCatalog] c
			ON a.[PreventiveMaintenanceCatalogId] = c.[PreventiveMaintenanceCatalogId]
		LEFT JOIN [General].[Types] d
			ON c.[PeriodicityTypeId] = d.[TypeId]
		LEFT JOIN [General].[Values] e
			ON d.[TypeId] = e.[TypeId]
		LEFT JOIN General.PreventiveMaintenanceRecordByVehicle f 
			ON a.PreventiveMaintenanceId = f.PreventiveMaintenanceId
	WHERE   a.[CustomerId] = @pCustomerId
--		AND a.[PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId
		AND (@pPreventiveMaintenanceId IS NULL OR @pPreventiveMaintenanceId = a.[PreventiveMaintenanceId])	
	SET NOCOUNT OFF;
END
