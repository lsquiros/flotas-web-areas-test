USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_Points_Retrieve]    Script Date: 11/06/2014 10:22:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Points_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_Points_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_Points_Retrieve]    Script Date: 11/06/2014 10:22:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/27/2014
-- Description:	Retrieve Points information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_Points_Retrieve]
(
	 @pCustomerId INT = NULL
	,@pKey VARCHAR(800) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT 
		p.[Name] [description], 
		p.[Latitude] [lat], 
		p.[Longitude] [lon] 
	FROM 
		[Efficiency].[Routes] r 
		INNER JOIN [Efficiency].[RoutesPoints] p ON r.[RouteId] = p.[RouteId]
	WHERE 
		r.[CustomerId] = @pCustomerId
		
		AND (@pKey IS NULL 
		OR UPPER(p.[Name]) like '%'+@pKey+'%')


	ORDER BY p.Name
	
    SET NOCOUNT OFF
END


GO


