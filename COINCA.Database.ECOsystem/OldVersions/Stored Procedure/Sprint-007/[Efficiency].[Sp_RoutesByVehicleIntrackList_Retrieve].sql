USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]    Script Date: 01/29/2015 13:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]    Script Date: 01/29/2015 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 11/20/2014
-- Description:	Retrieve VehicleByRoute information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]
(
	 @pVehicleId INT = NULL
	,@pRouteId INT = NULL
	,@pCustomerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		vr.[RouteId] [Id],
		r.[Name]
	FROM 
		[Efficiency].[VehicleByRoute] vr
		INNER JOIN [General].[Vehicles] v ON vr.[VehicleId] = v.[VehicleId]
		INNER JOIN [Efficiency].[Routes] r ON vr.[RouteId] = r.[RouteId]	
	WHERE 
		v.[CustomerId] = r.[CustomerId] AND 
		v.[CustomerId] = @pCustomerId AND 
		v.[IntrackReference] = @pVehicleId AND 
		(@pRouteId IS NULL OR vr.[RouteId] = @pRouteId) 
	ORDER BY 
		r.[Name] DESC
	
    SET NOCOUNT OFF
END



GO

--SELECT * FROM [Efficiency].[VehicleByRoute]
--SELECT * FROM General.Vehicles
--SELECT * FROM [Efficiency].[Routes] 
