USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PerformanceByVehicleComparativeDetail_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_PerformanceByVehicleComparativeDetail_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Rometro
-- Create date: 03/06/2015
-- Description:	Retrieve Performance By Vehicle Comparative Report information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PerformanceByVehicleComparativeDetail_Retrieve]
(
	 @pVehicleId INT					--@pVehicleId: Id of the vehicle
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pWeek INT = NULL					--@pWeek: Week
	,@pDay VARCHAR(50) = NULL			--@pDay: Day
	,@pOption INT						--@pOption: Option -> 1 Year, 2 Month, 3 Day, 4 Week
	
)
AS
BEGIN
	SET NOCOUNT ON
	
	IF(@pYear IS NULL AND @pMonth IS NULL AND @pWeek IS NULL AND @pDay IS NULL)
	BEGIN
		RETURN
	END
	
	SELECT	 a.[VehicleId]
			,a.[TrxDate]
			,a.[TrxReportedLiters]
			,a.[TrxReportedOdometer]
			,a.[TrxPreviousOdometer]
			,a.[TrxPerformance]
			,a.[GpsReportedOdometer]
			,a.[GpsPreviousOdometer]
			,a.[GpsPerformance]
	FROM [General].[PerformanceByVehicle] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]
	WHERE b.[Active] = 1
	  AND a.[VehicleId] = @pVehicleId
	  AND (
		(@pOption = 1 AND @pYear IS NOT NULL AND DATEPART(YEAR, a.[TrxDate]) = @pYear)
		OR
		(@pOption = 2 AND @pYear IS NOT NULL AND @pMonth IS NOT NULL AND DATEPART(YEAR, a.[TrxDate]) = @pYear AND DATEPART(MONTH, a.[TrxDate]) = @pMonth)
		OR
		(@pOption = 3 AND @pDay IS NOT NULL AND CONVERT(DATE, a.[TrxDate]) = CONVERT(DATE,@pDay))
		OR
		(@pOption = 4 AND @pYear IS NOT NULL AND @pMonth IS NOT NULL AND @pWeek IS NOT NULL AND DATEPART(YEAR, a.[TrxDate]) = @pYear AND DATEPART(MONTH, a.[TrxDate]) = @pMonth AND DATEPART(WK, a.[TrxDate]) = @pWeek)
	  )
	  
	SET NOCOUNT OFF
END
