USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardNumbers_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardNumbers_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 11/14/2014
-- Description:	Retrieve Credit Card Numbers information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardNumbers_Retrieve]
(
	 --@pCreditCardNumberId INT				--@pCreditCardNumberId: Credit Card Number Id
	 @pCreditCardType CHAR(1)				--@pCreditCardType: Credit Card Type 'C' Or 'D'
	,@pPartnerId INT						--@pPartnerId: Partner Id
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @lCount INT
	DECLARE @currentCCIdNumber INT

	--Gerald: Cambio para obtener el top 1 de una tarjeta de disponible
	SELECT TOP(1) @currentCCIdNumber = ccn.[CreditCardNumberId]
	FROM [Control].[CreditCardNumbers] ccn
	WHERE ccn.[CreditCardType] = @pCreditCardType AND 
		  ccn.[IsUsed] = 0 AND
		  ccn.[IsReserved] = 0

	IF(@currentCCIdNumber = 0)
	BEGIN
		SELECT 0
	END ELSE
	IF(@currentCCIdNumber >= 1)
	BEGIN
		UPDATE [Control].[CreditCardNumbers]
			SET [IsReserved] = 1
		WHERE [CreditCardNumberId] = @currentCCIdNumber
		
		SELECT @currentCCIdNumber
	END
	
    SET NOCOUNT OFF
END

GO

/*

-----------------------SP INSERT-----------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardNumbers_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardNumbers_Add]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [Control].[Sp_CreditCardNumbers_Add]
(
	 @pCreditCardNumber NVARCHAR(520)
	,@pCreditCardType CHAR(1)
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	INSERT INTO [Control].[CreditCardNumbers](
		CreditCardNumber,CreditCardType,PartnerId,InsertDate,InsertUserId
	)
	VALUES(@pCreditCardNumber,@pCreditCardType,4,GETUTCDATE(),0)
	
    SET NOCOUNT OFF
END

*/