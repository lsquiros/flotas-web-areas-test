USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_WeightedScoreSettings_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_WeightedScoreSettings_AddOrEdit]
GO

USE [ECOSystem]
GO
/****** Object:  StoredProcedure [Operation].[Sp_WeightedScoreSettings_AddOrEdit]    Script Date: 12/01/2014 11:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/01/2014
-- Description:	Insert or Update Weighted Score Settings information
-- ================================================================================================
CREATE PROCEDURE [Operation].[Sp_WeightedScoreSettings_AddOrEdit]
(
	 @pWeightedScoreId INT = NULL			--@pWeightedScoreId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customers
	,@pScoreTypeId INT						--@pScoreTypeId: FK of Score Types
	,@pWeight NUMERIC(5,2)					--@pWeight: Weight of the Score Type
	,@pLoggedUserId INT						--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP					--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pWeightedScoreId IS NULL)
			BEGIN
				INSERT INTO [Operation].[WeightedScoreSettings]
						([CustomerId]
						,[ScoreTypeId]
						,[Weight]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@pCustomerId
						,@pScoreTypeId
						,@pWeight
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Operation].[WeightedScoreSettings]
					SET  [CustomerId] = @pCustomerId
						,[ScoreTypeId] = @pScoreTypeId
						,[Weight] = @pWeight
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [WeightedScoreId] = @pWeightedScoreId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
