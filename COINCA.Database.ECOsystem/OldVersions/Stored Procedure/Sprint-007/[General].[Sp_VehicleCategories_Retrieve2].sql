USE [ECOsystem]
GO

/****** Object:  StoredProcedure [General].[Sp_VehicleCategories_Retrieve2]    Script Date: 6/5/2015 2:41:59 PM ******/
DROP PROCEDURE [General].[Sp_VehicleCategories_Retrieve2]
GO

/****** Object:  StoredProcedure [General].[Sp_VehicleCategories_Retrieve2]    Script Date: 6/5/2015 2:41:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve VehicleCategory information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCategories_Retrieve2]
(
	 @pVehicleCategoryId INT = NULL
	,@pKey VARCHAR(800) = NULL
	,@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[VehicleCategoryId]
		,a.[Manufacturer]
		,a.[Type]
		,a.[VehicleModel]
		,b.[Name] AS [FuelName]
		FROM [General].[VehicleCategories] a
		INNER JOIN [Control].[Fuels] b
			ON a.[DefaultFuelId] = b.[FuelId]

	
    SET NOCOUNT OFF
END


GO


