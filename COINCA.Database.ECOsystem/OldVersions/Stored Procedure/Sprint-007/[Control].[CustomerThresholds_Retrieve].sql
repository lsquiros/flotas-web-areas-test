USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerThresholds_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerThresholds_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Alexander Aguero Venegas 
-- Create date: 14 enero 2015
-- Description:	Retrieve Thresholds by customer information
-- ================================================================================================
Create PROCEDURE [General].[Sp_CustomerThresholds_Retrieve]
(
	@pCustomerId INT=null
)
AS 
BEGIN 
SET NOCOUNT ON 

IF (SELECT COUNT(*) FROM  General.CustomerThresholds a 	WHERE a.[CustomerId] = @pCustomerId) > 0
	BEGIN
		SELECT 
			 a.[RedLower]
			,a.[RedHigher]
			,a.[YellowLower]
			,a.[YellowHigher]
			,a.[GreenLower]
			,a.[GreenHigher]
			,a.[RowVersion]
		FROM  General.CustomerThresholds a
		WHERE a.[CustomerId] = @pCustomerId
	END
ELSE
	SELECT   0 as [RedLower], 69 as [RedHigher], 70 AS [YellowLower], 84 as [YellowHigher], 85 as [GreenLower]	,100 as [GreenHigher], 0x00000000000B3FB7 as [RowVersion]

SET NOCOUNT OFF
END 