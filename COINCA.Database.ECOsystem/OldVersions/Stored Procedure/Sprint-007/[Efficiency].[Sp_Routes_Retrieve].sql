USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_Routes_Retrieve]    Script Date: 11/06/2014 10:23:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Routes_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_Routes_Retrieve]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_Routes_Retrieve]    Script Date: 11/06/2014 10:23:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/27/2014
-- Description:	Retrieve Routes information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_Routes_Retrieve]
(
	 @pRouteId INT = NULL
	,@pKey VARCHAR(800) = NULL
	,@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		co.[Code] CountryCode,
		g.[RouteId]
		,g.[Name]
		,g.[Active]
		,g.[CustomerId]
		,g.[Description]
		,g.[RowVersion]
        ,ISNULL(g.[Time], 0) [Time] 
        ,ISNULL(ROUND(g.[Distance], 2), 0) [Distance]
    FROM [Efficiency].[Routes] g
    INNER JOIN [General].[Customers] c ON g.[CustomerId] = c.[CustomerId]
    INNER JOIN [General].[Countries] co ON c.[CountryId] = co.[CountryId]
	WHERE g.[CustomerId] = @pCustomerId
	  AND (@pRouteId IS NULL OR g.[RouteId] = @pRouteId)
	  AND (@pKey IS NULL 
				OR UPPER(g.[Name]) like '%'+@pKey+'%'
				OR UPPER(g.[Description]) like '%'+@pKey+'%')
	ORDER BY g.[Name] ASC
	
    SET NOCOUNT OFF
END



GO


