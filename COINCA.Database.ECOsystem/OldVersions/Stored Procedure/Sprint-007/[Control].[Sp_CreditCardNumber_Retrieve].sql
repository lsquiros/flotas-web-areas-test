USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardNumber_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardNumber_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo
-- Create date: 05/12/2015
-- Description:	Retrieve CreditCard Transactions through the VPOS Process
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardNumber_Retrieve]
(
	 @pCreditCardId INT						--@pCreditCardId: Customer Id
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
			
	SELECT
		CreditCardNumber
	FROM [Control].[CreditCard]
	WHERE [CreditCardId] = @pCreditCardId
				
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO



