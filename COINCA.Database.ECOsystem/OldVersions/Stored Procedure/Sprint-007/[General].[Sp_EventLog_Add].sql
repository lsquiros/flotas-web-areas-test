USE [ECOsystem]
GO
/****** Object:  StoredProcedure [General].[Sp_EventLog_Add]    Script Date: 05/27/2015 10:49:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Albert Estrada
-- Create date: 12/Mayo/2015
-- Description:	Insert EventLog Information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_EventLog_Add]
(   
     @pTipoLog varchar (15) 
	,@pMensaje varchar(1000) 
	,@pUsuario varchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END

			
			INSERT INTO General.EventLog
						(TipoLog 
						,Mensaje 
						,Usuario )

				VALUES	(@pTipoLog
						,@pMensaje
						, @pUsuario
						)
						 
			 
            SET @lRowCount = @@ROWCOUNT
           
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (50001, 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
