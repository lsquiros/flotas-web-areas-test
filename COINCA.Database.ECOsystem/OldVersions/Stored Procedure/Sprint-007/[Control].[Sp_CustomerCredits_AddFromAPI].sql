USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerCredits_AddFromAPI]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CustomerCredits_AddFromAPI]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/30/2014
-- Description:	Insert Customer Credit information from API
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CustomerCredits_AddFromAPI]
(
	 @pAccountNumber VARCHAR(50)
	,@pYear INT
	,@pMonth INT
	,@pCreditAmount DECIMAL(16,2)
	,@pCreditTypeId INT
	,@pLoggedUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lCustomerId INT            
            DECLARE @lCustomerCreditId INT = 0
              
            SELECT @lCustomerId = a.[CustomerId]
				FROM [General].[Customers] a
            WHERE a.[AccountNumber] = @pAccountNumber
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			 
			INSERT INTO [Control].[CustomerCredits](
					 [CustomerId]
					,[Year]
					,[Month]
					,[CreditAmount]
					,[CreditTypeId]
					,[InsertDate]
					,[InsertUserId])
			VALUES	(@lCustomerId
					,@pYear
					,@pMonth
					,@pCreditAmount
					,@pCreditTypeId
					,GETUTCDATE()
					,@pLoggedUserId)
			
			SELECT @lCustomerCreditId = SCOPE_IDENTITY()
					
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	
	SELECT @lCustomerCreditId AS [CustomerCreditId] 
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO