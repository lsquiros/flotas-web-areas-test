USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_Routes_AddOrEdit]    Script Date: 11/06/2014 10:22:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_Routes_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Efficiency].[Sp_Routes_AddOrEdit]
GO

USE [ECOSystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_Routes_AddOrEdit]    Script Date: 11/06/2014 10:22:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/13/2014
-- Description:	Insert or Update Route information
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_Routes_AddOrEdit]
(
	@pRouteId int = NULL			--@pRouteId: PK of the table
	,@pName varchar(50)					--@pName: Route Name
	,@pActive bit						--@pActive:	If Route Active
	,@pCustomerId int					--@pCustomerId: FK of Customer
	,@pDescription varchar(MAX)			--@pDescription: Route Description
	,@pLoggedUserId int					--@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion timestamp				--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pRouteId IS NULL)
			BEGIN
				INSERT INTO [Efficiency].[Routes]
						([Name]
						,[Active]
						,[CustomerId]
						,[Description]
						,[InsertDate]
						,[InsertUserId]
						)
				VALUES	(@pName
						,@pActive
						,@pCustomerId
						,@pDescription
						,GETUTCDATE()
						,@pLoggedUserId)
			END
			ELSE
			BEGIN
				UPDATE [Efficiency].[Routes]
					SET  
						[CustomerId] = @pCustomerId
						,[Name] = @pName
						,[Active] = @pActive
						,[Description] = @pDescription
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [RouteId] = @pRouteId
						  --AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END



GO


