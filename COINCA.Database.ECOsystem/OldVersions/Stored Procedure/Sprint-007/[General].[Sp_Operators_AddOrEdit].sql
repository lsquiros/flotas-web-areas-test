USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Operators_AddOrEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [General].[Sp_Operators_AddOrEdit]
GO

USE [ECOSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 03/Oct/2014
-- Description:	Insert or Update Operators information
-- =============================================
CREATE PROCEDURE [General].[Sp_Operators_AddOrEdit]
	 @pOperatorId INT = NULL --@pOperatorID: PK of the table
	,@pCustomerId INT	= NULL	 --@pCustomer: FK of the table Control.Customer
	,@pName VARCHAR(250)     --@pName: Operators Name General.Customers
	,@pRate DECIMAL(14,2)	 --@pName: Operators Rate
	,@pCurrencyId INT        --@pCurrency: FK of the table Control.Currencies
	,@pLoggedUserId INT      --@pLoggedUserId: Logged UserId that executes the operation
	,@pRowVersion TIMESTAMP  --@pRowVersion: Timestamp of row to prevent bad updates
AS
BEGIN	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0
        
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END		
		
		IF (@pOperatorId IS NULL)
		BEGIN
			INSERT INTO [General].[Operators]
				  ([CustomerId]
				   ,[Name]
				   ,[Rate]
				   ,[InsertUserId]
				   ,[InsertDate]
				   ,[CurrencyId])
			VALUES(@pCustomerId
				  ,@pName
				  ,@pRate
				  ,@pLoggedUserId
				  ,GETUTCDATE()
				  ,@pCurrencyId)		
		END
		ELSE
		BEGIN
			UPDATE [General].[Operators]
				SET [Name] = @pName
				   ,[Rate] = @pRate 
				   ,[CurrencyId] = @pCurrencyId
				   ,[ModifyUserId] = @pLoggedUserId
				   ,[ModifyDate] = GETUTCDATE()
			WHERE [OperatorId] = @pOperatorId
			  AND [RowVersion] = @pRowVersion
		END
		
		SET @lRowCount = @@ROWCOUNT
            
        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
			
		IF @lRowCount = 0
		BEGIN
			RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
