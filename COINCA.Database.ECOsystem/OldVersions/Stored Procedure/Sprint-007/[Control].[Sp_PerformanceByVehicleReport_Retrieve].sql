USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_PerformanceByVehicleReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_PerformanceByVehicleReport_Retrieve]
GO

USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes.
-- Create date: 03/02/2015
-- Description:	Retrieve PerformanceByVehicleReport information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_PerformanceByVehicleReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pVehicleId INT = NULL				--@pVehicleId: Id of the vehicle
	,@pManufacturer varchar(500) = NULL	
	,@pVehicleModel varchar(500) = NULL	
	,@pVehicleYear int = NULL	
	,@pType varchar(500) = NULL		
	,@pCylinderCapacity int = NULL		
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	Declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	Declare @pInitialDate2 datetime
	declare @pFinalDate2 datetime
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT('0' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end

DECLARE @tTable2 Table(
	PlateId VARCHAR(10),
	VehicleId int,
	InitialOdometer decimal,
	FinalOdometer decimal,
	Liters decimal,
	Manufacturer varchar(300),
	VehicleModel varchar(300),
    [Year] int,
	[Type] varchar(300),
	CylinderCapacity int,
	VehicleCategoryId INT,
	TransactionMonth INT,
	TransactionYear INT
)	

DECLARE @tTable Table(
	cte_start_date datetime
)	

;WITH CTE AS
(
    SELECT @pInitialDate AS cte_start_date
    UNION ALL
    SELECT DATEADD(MONTH, 1, cte_start_date)
    FROM CTE
    WHERE DATEADD(MONTH, 1, cte_start_date) <= @pFinalDate   
)
insert INTO @tTable  SELECT * FROM CTE
 
 DECLARE @cte_sd datetime

	DECLARE myCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT cte_start_date AS 'ID' 
		FROM @tTable
	OPEN myCursor
	FETCH NEXT FROM myCursor INTO @cte_sd
	WHILE @@FETCH_STATUS = 0 BEGIN
		PRINT @cte_sd
		
		set @pInitialDate2=  CAST(
							  CAST(YEAR(@cte_sd) AS VARCHAR(4)) +
							  RIGHT('0' + CAST(MONTH(@cte_sd) AS VARCHAR(2)), 2) +
							  RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate2=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate2)+1,0))
		
			INSERT INTO @tTable2
			SELECT
			   v.PlateId,
			   v.VehicleId,
			   ISNULL((SELECT TOP 1 Odometer FROM [Control].Transactions  WHERE [DATE] <= @pInitialDate2 and VehicleId=v.VehicleId ORDER BY [DATE] DESC),0) as InitialOdometer,
			   ISNULL((SELECT TOP 1 Odometer FROM [Control].Transactions  WHERE [DATE] BETWEEN @pInitialDate2 AND @pFinalDate2 and VehicleId=v.VehicleId ORDER BY [DATE] ASC),0) as FinalOdometer,
			   ISNULL((SELECT SUM(Liters) FROM [Control].Transactions  WHERE [DATE] BETWEEN @pInitialDate2 AND @pFinalDate2 and VehicleId=v.VehicleId),0) as Liters,
				vc.Manufacturer,
				vc.VehicleModel,
				vc.[Year],
				vc.[Type],
				vc.CylinderCapacity,
				vc.VehicleCategoryId
				,MONTH(@pInitialDate2) as TransactionMonth
				,YEAR(@pFinalDate2) as TransactionYear
			   FROM General.Vehicles v 
			   INNER JOIN General.VehicleCategories vc ON vc.VehicleCategoryId=v.VehicleCategoryId
			   INNER JOIN Control.Transactions t ON v.VehicleId=t.VehicleId
			   WHERE V.CustomerId=@pCustomerId and v.VehicleId=isnull(@pVehicleId,v.VehicleId)
			   and MONTH(t.[Date])=@pInitialDate2 AND YEAR(t.[Date])=@pInitialDate2

		FETCH NEXT FROM myCursor INTO @cte_sd

	END

CLOSE myCursor
DEALLOCATE myCursor

SELECT VehicleId,PlateId,InitialOdometer,FinalOdometer,[TransactionMonth],[TransactionYear],cast((FinalOdometer-InitialOdometer) as decimal) AS KMTraveled,SUM(Liters) as Liters,
case when SUM(Liters)>0 
then 
	cast((SUM(FinalOdometer-InitialOdometer)/SUM(Liters)) as decimal) 
else 
	cast(0 as decimal)
end	
	 AS Performance,
	 @pCustomerId

FROM @tTable2 WHERE (@pManufacturer IS NULL OR Manufacturer = @pManufacturer)
			   AND (@pVehicleModel IS NULL OR VehicleModel= @pVehicleModel)
			   AND (@pVehicleYear IS NULL OR  [Year] =@pVehicleYear)
			   AND (@pType IS NULL OR [Type] =@pType)
			   AND (@pCylinderCapacity IS NULL OR CylinderCapacity=@pCylinderCapacity)
  GROUP BY VehicleId,PlateId,InitialOdometer,FinalOdometer,[TransactionMonth],[TransactionYear]
			  
		
	SET NOCOUNT OFF
END
