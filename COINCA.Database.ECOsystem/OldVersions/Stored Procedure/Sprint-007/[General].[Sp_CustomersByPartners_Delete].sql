USE [ECOSystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByPartners_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomersByPartners_Delete] 
GO
USE [ECOSystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andrés Oviedo Brenes
-- Create date: 11/02/2015
-- Description:	Deletes Customers By Partner
-- ================================================================================================
create PROCEDURE [General].[Sp_CustomersByPartners_Delete] 
( @pPartnerId Int 
 )
AS
BEGIN

	DELETE FROM [General].CustomersByPartner
    where PartnerId=@pPartnerId and (IsDefault <>1)
END
