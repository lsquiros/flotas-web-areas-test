USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByDriverCode_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesByDriverCode_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 19/08/2015
-- Description:	Retrieve Vehicles By Driver Code
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesByDriverCode_Retrieve]
(
	@pDriverCode NVARCHAR(MAX)	--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	--DECLARE @pDriverCode NVARCHAR(MAX) = 'EPwMLKBlz18='--''101'

	SELECT top(1)
		v.[VehicleId] AS [VehicleId],
	    v.[PlateId] AS [Plate],
		@pDriverCode AS [DriverCode]
		--, vu.[InsertDate]
	FROM [General].[DriversUsers] du
	INNER JOIN [General].[VehiclesByUser] vu
		ON du.[UserId] = vu.[UserId]
	INNER JOIN [General].[Vehicles] v
		ON v.[VehicleId] = vu.[VehicleId] 
	WHERE du.[Code] = @pDriverCode AND 
		  vu.[LastDateDriving] IS NULL
	ORDER BY vu.[InsertDate] DESC

    SET NOCOUNT OFF
END
GO


