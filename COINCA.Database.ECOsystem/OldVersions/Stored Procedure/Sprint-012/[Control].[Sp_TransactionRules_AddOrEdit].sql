USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionRules_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionRules_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano.
-- Create date: 17/08/2015
-- Description:	Add Or Edit Status for Transactions Rules 
-- ================================================================================================
Create PROCEDURE [Control].[Sp_TransactionRules_AddOrEdit]
(
	@pCustomerId INT,	--@pCustomerId: FK of Customer
	@pRuleCustomerId INT = NULL,
	@pRuleId INT,
	@pRuleActive BIT = 0,
	@pRuleValue FLOAT = NULL,
	@pUserId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
		
		IF(@pRuleCustomerId IS NOT NULL)
		BEGIN 
			UPDATE [Control].[TransactionsRulesByCustomer]
			SET
				[RuleActive] = @pRuleActive,
				[RuleValue] = @pRuleValue,
				[ModifyDate] = DATEADD(hour, -6, GETDATE()),
				[ModifyUserId] = @pUserId
			WHERE [Id] = @pRuleCustomerId
		END	
		ELSE
		BEGIN
			INSERT INTO [Control].[TransactionsRulesByCustomer]
			   ([CustomerId]
			   ,[RuleId]
			   ,[RuleActive]
			   ,[RuleValue]
			   ,[InsertDate]
			   ,[InsertUserId])
			VALUES
			   (@pCustomerId
			   ,@pRuleId
			   ,@pRuleActive
			   ,@pRuleValue
			   ,DATEADD(hour, -6, GETDATE())
			   ,@pUserId)
		END
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


