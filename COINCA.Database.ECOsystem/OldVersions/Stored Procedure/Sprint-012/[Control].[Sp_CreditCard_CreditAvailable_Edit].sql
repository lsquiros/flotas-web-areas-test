USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCard_CreditAvailable_Edit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCard_CreditAvailable_Edit]
GO

/****** Object:  StoredProcedure [Control].[Sp_CreditCard_CreditAvailable_Edit]    Script Date: 8/18/2015 4:59:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Napole�n Alvarado
-- Create date: 12/11/2014
-- Description:	Update CreditCard CreditAvailable amount
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCard_CreditAvailable_Edit]
(
	 @pCreditCardId INT						--@pCreditCardId:Credit Card Id, PK of the table
	,@pTransactionAmount NUMERIC(16,2)		--@pTransactionAmount: Transaction Amount
	,@pIsReversed BIT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF(@pIsReversed = 1) -- SI LA TRANSACCI�N FUE REVERSADA RESTAURAMOS EL MONTO DISPONIBLE
			BEGIN
				UPDATE [Control].[CreditCard]
					SET  [CreditAvailable] = [CreditAvailable] + @pTransactionAmount
				WHERE [CreditCardId] = @pCreditCardId
			END
			ELSE
			BEGIN
				UPDATE [Control].[CreditCard]
				SET  [CreditAvailable] = [CreditAvailable] - @pTransactionAmount
				WHERE [CreditCardId] = @pCreditCardId
			END  

			SET @lRowCount = @@ROWCOUNT
            				            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


