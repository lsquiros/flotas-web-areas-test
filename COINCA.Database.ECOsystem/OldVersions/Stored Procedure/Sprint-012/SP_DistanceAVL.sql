USE [ECOsystemDev]
GO

DROP PROCEDURE [dbo].[Sp_DistanceAVL]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kevin Pe�a
-- Create date: 18/08/2015
-- Description:	c�lculo de distancia entre el Reporte Actual y ReportLast
-- **STDistance devuelve el valor en metros
-- =============================================
CREATE PROCEDURE [dbo].[Sp_DistanceAVL]( 
    @pDevice [int] = NULL, 
	@pLatitude [float] = NULL,
    @pLongitude [float] = NULL
)

AS
BEGIN

	DECLARE @g geometry
    DECLARE @h geometry
    DECLARE @latLast float
    DECLARE @lonLast float

	SET @latLast =( select [Latitude]  from [Report_Last] WHERE Device =@pDevice) 
	SET @lonLast =( select [Longitude]  from [Report_Last] WHERE Device =@pDevice) 

    SET @g = geometry::Point(@pLatitude, @pLongitude, 4326) 
    SET @h = geometry::Point(@latLast, @lonLast, 4326) 
	SELECT ROUND(@g.STDistance(@h)*1000,1)   -- se devuelve la diferencia en kilometros
	
END

GO


