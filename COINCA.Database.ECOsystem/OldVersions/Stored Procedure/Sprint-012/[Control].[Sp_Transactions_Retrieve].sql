USE [ECOsystemDev]
GO

/****** Object:  StoredProcedure [Control].[Sp_Transactions_Retrieve]    Script Date: 8/21/2015 1:22:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Cristian Martínez Hernández.
-- Create date: 20/Oct/2014
-- Description:	Retrieve Alarm By Odometer
-- Modified by:	Berman Romero
-- Description:	Was Changed all logic - previous version doesn't work at all
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Transactions_Retrieve]
(
	@pVehicleId INT							--@pVehicleId
)
AS
BEGIN
	SET NOCOUNT ON
	
	--DECLARE @pVehicleId INT = 795

	SELECT
		*
	FROM  [Control].[Transactions] a WITH(READUNCOMMITTED)
	WHERE a.[VehicleId] = @pVehicleId
	  AND a.[Date] > DATEADD(MONTH,-3,GETUTCDATE())
	  AND (a.[IsAdjustment] = 0 OR a.[IsAdjustment] IS NULL)
	  AND (a.[IsFloating] = 0 OR a.[IsAdjustment] IS NULL)
	  AND (a.[IsReversed] = 0 OR a.[IsAdjustment] IS NULL)
	  AND (a.[IsDuplicated] = 0 OR a.[IsAdjustment] IS NULL)
	  AND a.[FixedOdometer] IS NULL
	  
	  	
	SET NOCOUNT OFF
END

GO


