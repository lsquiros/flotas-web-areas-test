USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerByCreditCardNumber_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CustomerByCreditCardNumber_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_CreditCardNumber_Retrieve]    Script Date: 7/23/2015 11:57:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Kevin Pe�a
-- Create date: 07/23/2015
-- Description:	Retrieve CustomerID from CreditCardNumber_Transactions
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CustomerByCreditCardNumber_Retrieve]
(
	 @pCreditCardNumber NVARCHAR(520)					--@pCreditCardId: Customer Id
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
				
    SELECT [CustomerId] 
	FROM [Control].[CreditCard]
    WHERE [CreditCardNumber] = @pCreditCardNumber

	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


