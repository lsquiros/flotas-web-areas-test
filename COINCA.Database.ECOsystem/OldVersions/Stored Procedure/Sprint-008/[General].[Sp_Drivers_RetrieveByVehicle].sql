USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Drivers_RetrieveByVehicle]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Drivers_RetrieveByVehicle]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano.
-- Create date: 23/07/2015
-- Description:	Retrieve Drivers information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Drivers_RetrieveByVehicle]
(
	  @pVehicleId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON	
		--DECLARE @pVehicleId INT = 1795

		SELECT 
		--vd.[VehicleId] AS [VehicleId],
		d.[UserId] AS [UserId]
		,u.[Name] AS [EncryptedName]
		,'' AS Photo
		,u.[IsActive]
		,b.[Email] AS [EncryptedEmail]
		--,d.[Identification]
		,b.[PhoneNumber] AS [EncryptedPhoneNumber]
		,d.[License] AS [EncryptedLicense]
		,d.[LicenseExpiration]
		,CONVERT(BIT,CASE WHEN (u.[IsLockedOut] = 1 OR b.[LockoutEndDateUtc] IS NOT NULL) THEN 1 ELSE 0 END) AS [IsLockedOut]
		,b.[UserName] AS [EncryptedUserName]
		,ar.[Name] AS [RoleName]
		,CONVERT(BIT,CASE WHEN d.[DriversUserId] IS NOT NULL THEN 1 ELSE 0 END) AS IsDriverUser
		,u.[PasswordExpirationDate]
		,CONVERT(BIT,CASE WHEN u.[PasswordExpirationDate] < GETDATE() THEN 1 ELSE 0 END) AS IsPasswordExpired
		,u.[RowVersion]
	FROM  [General].[DriversUsers] d
	INNER JOIN [General].[VehiclesDrivers] vd
		ON vd.[UserId] = d.[UserId]
	INNER JOIN [General].[Users] u 
		ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
	INNER JOIN [dbo].[AspNetUsers] b
				ON u.[AspNetUserId] = b.[Id]
	INNER JOIN [dbo].[AspNetUserRoles] c
				ON c.UserId = u.AspNetUserId
	INNER JOIN [dbo].[AspNetRoles] ar
				ON ar.[Id] = c.[RoleId]
	WHERE  
		(@pVehicleId IS NULL OR
		vd.[VehicleId] = @pVehicleId)

    SET NOCOUNT OFF
END
