USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ReportsRange_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_ReportsRange_Retrieve]
GO


/****** Object:  StoredProcedure [dbo].[Sp_ReportsRange_Retrieve]    Script Date: 7/24/2015 1:29:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/30/2014
-- Description:	Retrieve Reports information
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ReportsRange_Retrieve]
(
	 @pUnitID bigint = NULL,
	 @pStartDate VARCHAR(21),
	 @pEndDate VARCHAR(21)
)

AS
BEGIN
	
	SET NOCOUNT ON

		DECLARE @Device INT = 0

		--DECLARA LA TABLA PARA INGRESAR LOS REGISTROS DE REPORTS
		DECLARE @Reports TABLE
		(
			[Report] INT, 
			[Device] INT, 
			[GPSDateTime] DATETIME, 
			[RTCDateTime] DATETIME, 
			[RepDateTime] DATETIME, 
			[SvrDateTime] DATETIME, 
			[Longitude] FLOAT, 
			[Latitude] FLOAT, 
			[Heading] INT, 
			[ReportID] INT, 
			[Odometer] FLOAT, 
			[HDOP] FLOAT, 
			[InputStatus] TINYINT, 
			[OutputStatus] TINYINT, 
			[VSSSpeed] FLOAT, 
			[AnalogInput] FLOAT, 
			[DriverID] VARCHAR(16), 
			[Temperature1] FLOAT, 
			[Temperature2] FLOAT, 
			[MCC] INT, 
			[MNC] INT, 
			[LAC] INT, 
			[CellID] INT, 
			[RXLevel] INT, 
			[GSMQuality] INT, 
			[GSMStatus] INT, 
			[Satellites] INT, 
			[MainVolt] FLOAT, 
			[BatVolt] FLOAT,
			[Speeding] INT)
			
		DECLARE @Speeding TABLE
		(
			[Report] INT
		)
						
	--OBTIENE EL NUMERO DE DEVICE
	SELECT @Device = d.Device 
	FROM 
		[dbo].[Devices] [d] 
	WHERE 
		d.[UnitID] = @pUnitID 
		
	INSERT INTO @Reports (
		[Report], 
		[Device], 
		[GPSDateTime], 
		[RTCDateTime], 
		[RepDateTime], 
		[SvrDateTime], 
		[Longitude], 
		[Latitude], 
		[Heading], 
		[ReportID], 
		[Odometer], 
		[HDOP], 
		[InputStatus], 
		[OutputStatus], 
		[VSSSpeed], 
		[AnalogInput], 
		[DriverID], 
		[Temperature1], 
		[Temperature2], 
		[MCC], 
		[MNC], 
		[LAC], 
		[CellID], 
		[RXLevel], 
		[GSMQuality], 
		[GSMStatus], 
		[Satellites], 
		[MainVolt], 
		[BatVolt],
		[Speeding])	
	SELECT 
		r.[Report], 
		r.[Device], 
		r.[GPSDateTime], 
		r.[RTCDateTime], 
		r.[RepDateTime], 
		r.[SvrDateTime], 
		r.[Longitude], 
		r.[Latitude], 
		r.[Heading], 
		r.[ReportID], 
		r.[Odometer], 
		r.[HDOP], 
		r.[InputStatus], 
		r.[OutputStatus], 
		r.[VSSSpeed], 
		r.[AnalogInput], 
		r.[DriverID], 
		r.[Temperature1], 
		r.[Temperature2], 
		r.[MCC], 
		r.[MNC], 
		r.[LAC], 
		r.[CellID], 
		r.[RXLevel], 
		r.[GSMQuality], 
		r.[GSMStatus], 
		r.[Satellites], 
		r.[MainVolt], 
		r.[BatVolt],
--		CASE WHEN r.[VSSSpeed] > 80 THEN 1
--			ELSE 0
--		END
		0 [Speeding]
	FROM 
		[dbo].[Reports] r 
		INNER JOIN [dbo].[Devices] d ON r.[Device] = d.[Device]
	WHERE 
		d.[UnitID] = @pUnitID and 
		r.[GPSDateTime] >= @pStartDate and 
		r.[GPSDateTime] <= @pEndDate

	INSERT INTO @Speeding ([Report])
	SELECT [Report] 
	FROM [dbo].[Speeding] 
	WHERE Device = @Device AND 
		Date_Time >= @pStartDate AND 
		Date_Time <= @pEndDate

	UPDATE a SET a.[Speeding] = 1 
	FROM @Reports a
	INNER JOIN @Speeding b ON a.Report = b.Report

	SELECT 
		[Report], 
		[Device], 
		[GPSDateTime], 
		[RTCDateTime], 
		[RepDateTime], 
		[SvrDateTime], 
		[Longitude], 
		[Latitude], 
		[Heading], 
		[ReportID], 
		[Odometer], 
		[HDOP], 
		[InputStatus], 
		[OutputStatus], 
		[VSSSpeed], 
		[AnalogInput], 
		[DriverID], 
		[Temperature1], 
		[Temperature2], 
		[MCC], 
		[MNC], 
		[LAC], 
		[CellID], 
		[RXLevel], 
		[GSMQuality], 
		[GSMStatus], 
		[Satellites], 
		[MainVolt], 
		[BatVolt],
		[Speeding]
	FROM 
		@Reports
	ORDER BY 
		[GPSDateTime] ASC

    SET NOCOUNT OFF
END

GO
