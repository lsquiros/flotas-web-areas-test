USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByDriver_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesByDriver_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 22/07/2015
-- Description:	Insert or Update VehiclesByDriver information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesByDriver_AddOrEdit]
(
	 @pUserId INT			--@pVehicleId: PK of the table
	,@pXmlData VARCHAR(MAX)		--@pName: Vehicles Name
	,@pLoggedUserId INT			--@pLoggedUserId: Logged UserId that executes the operation
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			DELETE [General].[VehiclesDrivers]
			WHERE [UserId] = @pUserId
			
			INSERT INTO [General].[VehiclesDrivers]
					([VehicleId]
					,[UserId]
					,[InsertDate]
					,[InsertUserId])
			SELECT 
					Row.col.value('./@VehicleId', 'INT') AS [VehicleId]
					,@pUserId
					,GETUTCDATE()
					,@pLoggedUserId
			FROM @lxmlData.nodes('/xmldata/Vehicle') Row(col)
			ORDER BY Row.col.value('./@Index', 'INT')
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


