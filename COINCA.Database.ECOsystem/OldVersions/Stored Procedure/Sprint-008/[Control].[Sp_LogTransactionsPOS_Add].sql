USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_LogTransactionsPOS_Add]    Script Date: 7/23/2015 2:34:58 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_LogTransactionsPOS_Add]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Control].[Sp_LogTransactionsPOS_Add]
GO

/****** Object:  StoredProcedure [Control].[Sp_LogTransactionsPOS_Add]    Script Date: 7/23/2015 2:34:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 25/06/2015
-- Description:	Insert log de transaction.
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_LogTransactionsPOS_Add]
(
		@pCCTransactionPOSId INT = NULL
		,@pResponseCode  varchar(250) = NULL
		,@pResponseCodeDescription Varchar(MAX)= Null
		,@pMessage Varchar(MAX)= Null
		,@pTransportData Varchar(MAX)= Null
		,@pIsSuccess bit
		,@pIsFail bit
		,@pCustomerID INT
)
AS
BEGIN
	--SET NOCOUNT ON
	--   SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lErrorNumber INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
       
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
				INSERT INTO [Control].[LogTransactionsPOS]
						([CCTransactionPOSId]
						,[ResponseCode]
						,[ResponseCodeDescription]
						,[Message]
						,[TransportData]
						,[IsSuccess]
						,[IsFail]
						,[InsertDate]
						,[ModifyDate]
						,[CustomerId]
						)
				VALUES	(@pCCTransactionPOSId
						,@pResponseCode
						,@pResponseCodeDescription
						,@pMessage
						,@pTransportData
						,@pIsSuccess
						,@pIsFail
						,GETUTCDATE()
						,GETUTCDATE()
						,@pCustomerID
					)	
			SET @lRowCount = @@ROWCOUNT
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
				SELECT @@IDENTITY AS 'Identity';
			 END
			
			IF @lRowCount = 0
			BEGIN
				SELECT -1 AS 'Identity';
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
         END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END    
            



GO


