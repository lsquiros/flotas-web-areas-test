USE [ECOsystem]
GO

/****** Object:  StoredProcedure [dbo].[Sp_PassControl_Retrieve]    Script Date: 07/23/2015 10:38:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_PassControl_Retrieve]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_PassControl_Retrieve]
GO

USE [ECOsystem]
GO

/****** Object:  StoredProcedure [dbo].[Sp_PassControl_Retrieve]    Script Date: 07/23/2015 10:38:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/30/2014
-- Description:	Retrieve PassControl information
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_PassControl_Retrieve]
(
	@pLongitude float,
	@pLatitude float,
	@pDistance float,
	@pUnitId VARCHAR(MAX),
	@pStartDate VARCHAR(21),
	@pEndDate VARCHAR(21)
)
AS
BEGIN
	DECLARE @i bigint = 0
	DECLARE @numrows int = 0
	DECLARE @LastUnitID Decimal(20, 0) = NULL
	DECLARE @UnitID Decimal(20, 0) = NULL
	DECLARE @InDateTime dateTime = NULL
	DECLARE @OutDateTime dateTime = NULL
	DECLARE @InRange int = NULL
	DECLARE @LastInRange int = NULL
	DECLARE @InsertData bit = 0
	DECLARE @ReportsList TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		UnitID Decimal(20, 0),
		GPSDateTime dateTime,
		InRange int
	)
	DECLARE @ReportsListFilter TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		UnitID Decimal(20, 0),
		GPSDateTime dateTime,
		InRange int
	)
	DECLARE @DevicesTemp TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		Device int,
		UnitID Decimal(20, 0)
	)
	DECLARE @ReportsTemp TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		Device int,
		UnitID Decimal(20, 0),
		GPSDateTime dateTime,
		Longitude float,
		Latitude float
	)
	DECLARE @Result TABLE
	(
		UnitID Decimal(20, 0),
		InDateTime dateTime NULL,
		OutDateTime dateTime NULL
	)

	-- REALIZAMOS UN SPLIT DE LOS UNIT_ID'S CONCATENADOS
    DECLARE @separator varchar(max) = ','
    DECLARE @Splited table(item varchar(max))
    SET @pUnitId = REPLACE(@pUnitId,@separator,'''),(''')
    SET @pUnitId = 'select * from (values('''+@pUnitId+''')) as V(A)' 
    INSERT INTO @Splited
    EXEC (@pUnitId)
	--/////////////////////////////////////////////////////////////////////////////////////////////
	SET NOCOUNT ON
	
	-- OBTENEMOS EL PUNTO GEOGRAFICO A COMPARAR
	DECLARE @p GEOGRAPHY = GEOGRAPHY::STGeomFromText('POINT(' + CONVERT(VARCHAR(MAX), @pLongitude) + ' ' + CONVERT(VARCHAR(MAX), @pLatitude) + ')', 4326);
	
	-- OBTENEMOS LOS DEVICES DESDE EL PARAMETRO CONSULTADO
	INSERT INTO @DevicesTemp (Device, UnitID)
		SELECT [Device], [UnitID] 
		FROM [dbo].[Devices]
		WHERE CONVERT(varchar(max), [UnitID]) in (select item from @Splited) 

	-- OBTENEMOS LOS DATOS DE REPORTS DESDE EL RANGO DE FECHAS CONSULTADO
	INSERT INTO @ReportsTemp (Device, UnitID, GPSDateTime, Longitude, Latitude)
		SELECT 
			r.[Device],
			d.[UnitID],
			r.[GPSDateTime],
			r.[Longitude],
			r.[Latitude]
		FROM 
			[dbo].[Reports] r 
			INNER JOIN @DevicesTemp d ON r.[Device] = d.[Device]
		WHERE 
			r.[GPSDateTime] >= @pStartDate AND 
			r.[GPSDateTime] < @pEndDate 
		ORDER BY r.[GPSDateTime]
	--/////////////////////////////////////////////////////////////////////////////////////////////

	--SELECT * FROM @ReportsTemp

	
	-- RECORREMOS LOS PUNTOS DE REPORTS TEMP PARA ESTABLECER SI UN UNIT_ID ESTUVO DENTRO DE UN RANGO
	INSERT INTO @ReportsList (UnitID, GPSDateTime, InRange)
		SELECT 
			r.UnitID,
			r.GPSDateTime, 
        GEOGRAPHY::STGeomFromText('POINT('+ 
            convert(nvarchar(20), r.Longitude)+' '+
            convert( nvarchar(20), r.Latitude)+')', 4326)
	        .STBuffer(@pDistance).STIntersects(@p) as [Intersects]
		FROM 
			@ReportsTemp r 
		ORDER BY r.[UnitID], r.[GPSDateTime]


	--SELECT * FROM @ReportsList where InRange = 1


	SET @UnitID = 0
	SET @i = 1
	SET @numrows = (SELECT COUNT(*) FROM @ReportsList)
	IF @numrows > 0
    
    INSERT INTO @ReportsListFilter (UnitID, GPSDateTime, InRange)
    SELECT TOP(1) UnitID, GPSDateTime, InRange FROM @ReportsList
    UNION
    SELECT x.UnitID, x.GPSDateTime, x.InRange FROM @ReportsList x
    LEFT OUTER JOIN @ReportsList y ON x.Id = y.Id + 1 AND (x.InRange <> y.InRange OR x.UnitID <> y.UnitID) 
	WHERE y.Id IS NOT NULL 
	
    WHILE (@i <= (SELECT MAX(Id) FROM @ReportsListFilter))
    BEGIN
		IF @i > 1
		BEGIN
			SET @InsertData = 0
			SET @UnitID = (SELECT UnitID FROM @ReportsListFilter WHERE Id = @i)
			SET @LastUnitID = (SELECT UnitID FROM @ReportsListFilter WHERE Id = @i - 1)

			IF (@UnitID = @LastUnitID)
			BEGIN

				SET @InRange = (SELECT InRange FROM @ReportsListFilter WHERE Id = @i)
				SET @LastInRange = (SELECT InRange FROM @ReportsListFilter WHERE Id = @i - 1)
				IF (@inRange != @LastInRange)
				BEGIN
					IF @InRange = 1
						BEGIN
							SET @InDateTime = (SELECT GPSDateTime FROM @ReportsListFilter WHERE Id = @i)
							
							--Se valida si es el ultimo registro, para poder guardarlo como una entrada aun sin salida
							if not exists(select UnitID from @ReportsListFilter where Id = @i + 1)
								SET @InsertData = 1		
						END
					ELSE
						BEGIN
							SET @OutDateTime = (SELECT GPSDateTime FROM @ReportsListFilter WHERE Id = @i)
							SET @InsertData = 1
					END
				END
				IF @InsertData = 1
				BEGIN
					INSERT INTO @Result (UnitID, InDateTime, OutDateTime)
					VALUES(@LastUnitID, @InDateTime, @OutDateTime)
					SET @InDateTime = NULL
					SET @OutDateTime = NULL
				END
			END

		END
        SET @i = @i + 1
    END

	SELECT 
		CONVERT(varchar(25), [UnitID]) [UnitID], 
		[InDateTime], 
		[OutDateTime]
	FROM @Result 

    SET NOCOUNT OFF
END








GO


