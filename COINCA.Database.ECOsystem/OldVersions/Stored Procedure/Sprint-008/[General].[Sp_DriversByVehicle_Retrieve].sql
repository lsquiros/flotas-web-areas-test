USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriversByVehicle_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_DriversByVehicle_Retrieve]    Script Date: 7/21/2015 2:00:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 21/07/2015
-- Description:	Retrieve DriversByVehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DriversByVehicle_Retrieve]
(
	 @pVehicleId INT	--@pDriverId: PK of the table
	,@pCustomerId INT	--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	--DECLARE @pVehicleId INT = 788
	--DECLARE @pCustomerId INT = 10

	SELECT 
		NULL AS [VehicleId],
		d.[UserId] AS [UserId],
		u.[Name] AS [DriverName],
	    d.[Identification]
	FROM  [General].[DriversUsers] d
	LEFT JOIN [General].[VehiclesDrivers] vd
		ON vd.[UserId] = d.[UserId]
	INNER JOIN [General].[Users] u 
		ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1  AND u.[IsDeleted] = 0
	WHERE  vd.[UserId] IS NULL AND d.[CustomerId] = @pCustomerId

	UNION

	SELECT 
		vd.[VehicleId] AS [VehicleId],
		d.[UserId] AS [UserId],
		u.[Name] AS [DriverName],
		d.[Identification]
	FROM  [General].[DriversUsers] d
	INNER JOIN [General].[VehiclesDrivers] vd
		ON vd.[UserId] = d.[UserId]
	INNER JOIN [General].[Users] u 
		ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
	WHERE  vd.[VehicleId] = @pVehicleId AND d.[CustomerId] = @pCustomerId
	
    SET NOCOUNT OFF
END

GO
