USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_WService_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_WService_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 15/10/2015
-- Description:	Retrieve authorizator Web Service 
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_WService_Retrieve]
(
	 @pPartnerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
		
	SELECT [WSId]
		  ,[PartnerId]
		  ,[Name]
		  ,[Environment]
		  ,[UserAuth]
		  ,[PasswordAuth]
		  ,[WSType]
		  ,[UriService]
		  ,[PartnerCode]
		  ,[Terminal]
  FROM [General].[WServicesByPartner] wp
	WHERE 
		 wp.[PartnerId] = @pPartnerId AND
		 (wp.[IsDeleted] IS NULL OR wp.[IsDeleted] = 0)

    SET NOCOUNT OFF
END

GO