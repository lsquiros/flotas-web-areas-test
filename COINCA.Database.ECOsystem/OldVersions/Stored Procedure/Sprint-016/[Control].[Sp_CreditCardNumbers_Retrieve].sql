USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardNumbers_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardNumbers_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_CreditCardNumbers_Retrieve]    Script Date: 10/8/2015 2:21:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 11/14/2014
-- Description:	Retrieve Credit Card Numbers information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardNumbers_Retrieve]
(
	 --@pCreditCardNumberId INT				--@pCreditCardNumberId: Credit Card Number Id
	 @pCreditCardType CHAR(1)				--@pCreditCardType: Credit Card Type 'C' Or 'D'
	,@pCustomerId INT = null						--@pCustomerId: Customer Id
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @lCount INT
	DECLARE @currentCCIdNumber INT
	DECLARE @pPartnerId INT = 0

	--DECLARE @pCreditCardType CHAR(1) = 'C'
	--DECLARE @pCustomerId INT = 28

	-- Obtenemos los datos del Partner
	SELECT 
		@pPartnerId = cp.PartnerId 
	FROM [General].[CustomersByPartner] cp
	INNER JOIN [General].[Partners] p
		ON cp.PartnerId	= p.PartnerId	
		WHERE cp.CustomerId	= @pCustomerId
			  AND p.PartnerTypeId = 800 -- FINANCIERA

	IF (@pPartnerId IS NOT NULL AND @pPartnerId > 0)
	BEGIN
		--Gerald: Cambio para obtener el top 1 de una tarjeta de disponible
		SELECT TOP(1) @currentCCIdNumber = ccn.[CreditCardNumberId]
		FROM [Control].[CreditCardNumbers] ccn
		WHERE ccn.PartnerId	= @pPartnerId AND
			  ccn.[CreditCardType] = @pCreditCardType AND 
			  ccn.[IsUsed] = 0 AND
			  ccn.[IsReserved] = 0
	END

	IF(@currentCCIdNumber IS NULL OR @currentCCIdNumber = 0)
	BEGIN
		SELECT 0
	END ELSE
	IF(@currentCCIdNumber >= 1)
	BEGIN
		UPDATE [Control].[CreditCardNumbers]
			SET [IsReserved] = 1
		WHERE [CreditCardNumberId] = @currentCCIdNumber
		
		SELECT @currentCCIdNumber
	END
	
    SET NOCOUNT OFF
END
GO