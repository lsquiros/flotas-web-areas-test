USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionRules_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionRules_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano.
-- Create date: 17/08/2015
-- Description:	Retrieve Transactions Rules
-- ================================================================================================
Create PROCEDURE [Control].[Sp_TransactionRules_Retrieve]
(
	@pCustomerId INT = NULL	--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	--DECLARE @pCustomerId INT = 43

	SET NOCOUNT ON

	IF(@pCustomerId IS NOT NULL)
	BEGIN 
	
		DECLARE @Count INT = 0
		SELECT @Count = COUNT(*) FROM [Control].[TransactionsRulesByCustomer]
		WHERE CustomerId = @pCustomerId
	
		IF(@Count = 0)
		BEGIN
			SET @pCustomerId = NULL

			SELECT 
				ISNULL(tr.[Id], 0) AS RuleId,
				0 AS RuleCustomerId,
				0 AS CustomerId,
				tr.RuleName,
				tr.RuleDescription,
				tr.RuleType,
				CONVERT(bit,0) AS RuleActive,
				0 AS RuleValue
			FROM [Control].[TransactionsRules] tr	
			LEFT JOIN [Control].TransactionsRulesByCustomer rc
				ON tr.Id = rc.RuleId
			WHERE tr.[IsActive] = 1
			GROUP BY ISNULL(tr.[Id], 0), tr.RuleName, tr.RuleDescription, tr.RuleType
		END
		ELSE
		BEGIN
			
			SELECT 
				ISNULL(tr.[Id], 0) AS RuleId,
				ISNULL(rc.Id, 0) AS RuleCustomerId,
				ISNULL(rc.CustomerId, 0) AS CustomerId,
				tr.RuleName,
				tr.RuleDescription,
				tr.RuleType,
				ISNULL(rc.RuleActive,0) AS RuleActive,
				ISNULL(rc.RuleValue, 0) AS RuleValue
			FROM [Control].[TransactionsRules] tr	
			LEFT JOIN [Control].TransactionsRulesByCustomer rc
				ON tr.Id = rc.RuleId AND rc.CustomerId = @pCustomerId
			WHERE   tr.[IsActive] = 1 
					--((@pCustomerId IS NULL) OR (rc.CustomerId = @pCustomerId OR rc.CustomerId IS NULL))
		END
	END	
	ELSE
	BEGIN
		SELECT 
			ISNULL(tr.[Id], 0) AS RuleId,
			0 AS RuleCustomerId,
			0 AS CustomerId,
			--ISNULL(rc.Id, 0) AS RuleCustomerId,
			--ISNULL(rc.CustomerId, 0) AS CustomerId,
			tr.RuleName,
			tr.RuleDescription,
			tr.RuleType,
			--ISNULL(rc.RuleActive,0) AS RuleActive,
			CONVERT(bit,0) AS RuleActive,
			0 AS RuleValue
		FROM [Control].[TransactionsRules] tr	
		LEFT JOIN [Control].TransactionsRulesByCustomer rc
			ON tr.Id = rc.RuleId
		WHERE tr.[IsActive] = 1
		GROUP BY ISNULL(tr.[Id], 0), tr.RuleName, tr.RuleDescription, tr.RuleType
	END

    SET NOCOUNT OFF
END
GO