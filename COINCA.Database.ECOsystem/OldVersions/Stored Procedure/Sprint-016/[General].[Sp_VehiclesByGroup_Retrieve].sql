USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByGroup_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesByGroup_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleGroup information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesByGroup_Retrieve]
(
	 @pVehicleGroupId INT					--@pVehicleGroupId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	
	SELECT
		 null AS [VehicleGroupId]
		,a.[VehicleId]
		,a.[Name] AS VehicleName
		,b.[Manufacturer] + ' - ' + b.[VehicleModel] + ', ' + c.[Name] + ' ' + CONVERT(varchar(50),b.Liters) + 'L' AS VehicleType
		,a.[PlateId]
		,ROW_NUMBER() OVER (ORDER BY a.[VehicleId]) AS RowNumber
    FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCategories] b
			ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] c
			ON b.[DefaultFuelId] = c.[FuelId]
	WHERE b.[CustomerId] = @pCustomerId
		AND a.[IsDeleted] <> 1
		AND NOT EXISTS(
			SELECT 1
			FROM [General].[VehiclesByGroup] x
			WHERE x.[VehicleId] = a.[VehicleId]
			  AND x.[VehicleGroupId] = @pVehicleGroupId
		)
	UNION
	SELECT
		 a.[VehicleGroupId]
		,a.[VehicleId]
		,b.[Name] AS VehicleName
		,c.[Manufacturer] + ' - ' + c.[VehicleModel] + ', ' + d.[Name] + ' ' + CONVERT(varchar(50),c.Liters) + 'L' AS VehicleType
		,b.[PlateId]
		,ROW_NUMBER() OVER (ORDER BY a.[RowVersion]) AS RowNumber
    FROM [General].[VehiclesByGroup] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]
		INNER JOIN [General].[VehicleCategories] c
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] d
			ON c.[DefaultFuelId] = d.[FuelId]
	WHERE a.[VehicleGroupId] = @pVehicleGroupId
	  AND b.[CustomerId] = @pCustomerId AND b.[IsDeleted] <> 1
	 
	
    SET NOCOUNT OFF
END

GO


