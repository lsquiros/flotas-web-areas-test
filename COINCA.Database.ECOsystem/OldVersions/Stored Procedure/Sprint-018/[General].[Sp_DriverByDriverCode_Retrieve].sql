USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriverByDriverCode_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriverByDriverCode_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 11/10/2015
-- Description:	Retrieve Driver Identification By Driver Code
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DriverByDriverCode_Retrieve]
(
	@pDriverCode NVARCHAR(MAX),	--@pDriverCode
	@pCustomerId INT            --@pCustomerId
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT top(1) du.[Identification]		
	FROM [General].[DriversUsers] du	
	WHERE du.[Code] = @pDriverCode AND
		  du.[CustomerId] = @pCustomerId 
		 
    SET NOCOUNT OFF
END
GO


