USE [ECOsystemQA]
GO

/****** Object:  StoredProcedure [General].[Sp_FuelCurrencySettings_AddOrEdit]    Script Date: 11/5/2015 3:12:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Manuel Azofeifa Hidalgo.
-- Create date: 02/12/2014
-- Description:	Insert or Update Dashboard Modules
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_FuelCurrencySettings_AddOrEdit]
(
	 @pCustomerId INT
	,@pByCurrency BIT
	,@pByFuel BIT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (EXISTS(SELECT 1 FROM [General].[FuelCurrencySettings] WHERE [CustomerId] = @pCustomerId))
			BEGIN
				UPDATE [General].[FuelCurrencySettings]
					SET  [ByCurrency] = @pByCurrency,
						 [ByFuel] = @pByFuel
				WHERE [CustomerId] = @pCustomerId						
			END
			ELSE
			BEGIN
				INSERT INTO [General].[FuelCurrencySettings]
							([CustomerId], [ByCurrency], [ByFuel])
				VALUES		(@pCustomerId, @pByCurrency, @pByFuel)	
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END



GO


