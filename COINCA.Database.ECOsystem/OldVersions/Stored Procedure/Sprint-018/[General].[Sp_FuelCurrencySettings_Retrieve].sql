USE [ECOsystemDev]
GO

/****** Object:  StoredProcedure [General].[Sp_FuelCurrencySettings_Retrieve]    Script Date: 11/5/2015 3:13:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Melvin Salas 
-- Create date: 05/NOV/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [General].[Sp_FuelCurrencySettings_Retrieve]
(
	 @pCustomerId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;

    	SET NOCOUNT ON	
		
	SELECT 
		[CustomerId],
		[ByCurrency],
		[ByFuel]
	FROM 
		[General].[FuelCurrencySettings]
	WHERE
		[CustomerId] = @pCustomerId
	
    SET NOCOUNT OFF

END

GO


