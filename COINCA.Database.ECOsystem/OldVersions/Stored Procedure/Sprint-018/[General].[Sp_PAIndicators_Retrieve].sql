USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PAIndicators_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PAIndicators_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_PAIndicators_Retrieve]    Script Date: 11/9/2015 11:04:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--~ Commented out USE [database] statement because USE statement is not supported to another database.
-- ================================================================================================
-- Author:		Berman Romero
-- Create date: 11/14/2014
-- Description:	Retrieve Indicators for Partner Admin Landing Page
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PAIndicators_Retrieve]
(
	@pPartnerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	--DECLARE @pPartnerId INT = 1023

	SELECT 
		(SELECT COUNT(1) FROM [General].[Customers] c 
			INNER JOIN [General].[CustomersByPartner] cp 
			ON cp.CustomerId = c.CustomerId
			WHERE (c.IsDeleted IS NULL AND c.IsDeleted = 0) AND c.IsActive = 1 
			AND cp.PartnerId = @pPartnerId) 
		 AS Customers,
		(SELECT COUNT(1) FROM [Control].[CreditCard] cc
			INNER JOIN [General].[CustomersByPartner] cp 
			ON cp.CustomerId = cc.CustomerId
			WHERE cp.PartnerId = @pPartnerId
		) AS Cards, 
		(SELECT COUNT(1) FROM [General].[Vehicles] v
			INNER JOIN [General].[CustomersByPartner] cp 
			ON cp.CustomerId = v.CustomerId
			WHERE cp.PartnerId = @pPartnerId AND (v.IsDeleted IS NULL OR v.IsDeleted <> 1)) 
		 AS Vehicles,
		(SELECT COUNT(1) FROM [General].[PartnerUsers] pu 
			INNER JOIN [General].[Users] a 
			ON pu.UserId = a.UserId
			WHERE pu.PartnerId = @pPartnerId AND (a.IsDeleted IS NULL OR a.IsDeleted <> 1) 
			AND a.InsertDate > DATEADD(day,-7,GETUTCDATE())) 
		 AS Users
	
    SET NOCOUNT OFF
END

GO


