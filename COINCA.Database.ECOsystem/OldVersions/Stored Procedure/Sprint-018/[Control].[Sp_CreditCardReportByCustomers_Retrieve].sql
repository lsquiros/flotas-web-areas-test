USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardReportByCustomers_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardReportByCustomers_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 05/11/2015
-- Description:	Retrieve CreditCard information by Customers
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardReportByCustomers_Retrieve]
(
	 @pCustomerId INT		--@pCustomerId: Customer identificator
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT cc.CustomerId, 
		   c.Name AS [CustomerName], 
		   (
			CASE WHEN c.[IssueForId] = 100 --CONDUCTOR
					THEN 
						ISNULL((SELECT u.[Name] 
							FROM [General].[Users] u
								INNER JOIN [Control].[CreditCardByDriver] cd
									ON cd.[UserId] = u.[UserId]
									WHERE cd.[CreditCardId] = cc.[CreditCardId]), '------') 

				 WHEN c.[IssueForId] = 101 --VEHICULO
					THEN 
						ISNULL((SELECT 'Vehiculo: ' + v.[PlateId] 
							FROM [General].[Vehicles] v
								INNER JOIN [Control].[CreditCardByVehicle] cv
									ON cv.[VehicleId] = v.[VehicleId]
									WHERE cv.[CreditCardId] = cc.[CreditCardId]), '------')
				ELSE
					'------'
			END 
		   ) AS [HolderName],
		   cc.[CreditCardNumber], 
		   (SELECT TOP 1 cn.[CreditCardType] FROM [Control].[CreditCardNumbers] cn 
				WHERE cn.[CreditCardNumber] = cc.[CreditCardNumber]) AS [CreditCardType],
		   cc.[ExpirationYear],
		   cc.[ExpirationMonth]
	FROM [Control].[CreditCard] cc
	INNER JOIN [General].[Customers] c
		ON cc.CustomerId = c.CustomerId
	WHERE cc.StatusId = 7 AND cc.CustomerId = @pCustomerId
	ORDER BY cc.CustomerId ASC

    SET NOCOUNT OFF
END
GO