
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByDriver_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesByDriver_AddOrEdit]
GO

/****** Object:  StoredProcedure [General].[Sp_VehiclesByDriver_AddOrEdit]    Script Date: 11/19/2015 1:11:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 22/07/2015
-- Description:	Insert or Update VehiclesByDriver information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesByDriver_AddOrEdit]
(
	 @pUserId INT			--@pVehicleId: PK of the table
	,@pXmlData VARCHAR(MAX)		--@pName: Vehicles Name
	,@pLoggedUserId INT			--@pLoggedUserId: Logged UserId that executes the operation
	,@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END

			IF @pUserId = -1
			BEGIN
				
				DELETE	[General].[VehiclesDrivers]
				WHERE	VehicleId IN 
						(SELECT	Row.col.value('./@VehicleId', 'INT')
						FROM	@lxmlData.nodes('/xmldata/Vehicle') Row(col))

				IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
					TRUNCATE TABLE #TEMP
				ELSE
					CREATE TABLE #TEMP(VehicleId INT)

				INSERT	INTO #TEMP
				SELECT	Row.col.value('./@VehicleId', 'INT')
				FROM	 @lxmlData.nodes('/xmldata/Vehicle') Row(col)

				INSERT	INTO [General].[VehiclesDrivers]
						([VehicleId], [UserId], [InsertDate], [InsertUserId])

				SELECT	#TEMP.VehicleId
						,d.UserId
						,GETUTCDATE()
						,@pLoggedUserId
				FROM	[General].[DriversUsers] d
				LEFT	JOIN [General].[VehiclesDrivers] vd
				ON		vd.[UserId] = d.[UserId]
				INNER	JOIN [General].[Users] u 
				ON		u.[UserId] = d.[UserId] AND u.[IsActive] = 1  AND u.[IsDeleted] = 0
				CROSS	JOIN #TEMP
				WHERE	vd.[UserId] IS NULL AND d.[CustomerId] = @pCustomerId

				UNION

				SELECT	#TEMP.VehicleId
						,d.UserId
						,GETUTCDATE()
						,@pLoggedUserId
				FROM	[General].[DriversUsers] d
				INNER	JOIN [General].[VehiclesDrivers] vd
						ON	vd.[UserId] = d.[UserId]
				INNER	JOIN [General].[Users] u 
						ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
				CROSS	JOIN #TEMP
				WHERE  vd.[VehicleId] <> #TEMP.VehicleId AND 
						d.[CustomerId] = @pCustomerId AND 
						vd.[UserId] NOT IN 
						(SELECT d.[UserId] AS [UserId]
						FROM	[General].[DriversUsers] d
						INNER	JOIN [General].[VehiclesDrivers] vd
								ON vd.[UserId] = d.[UserId]
						INNER	JOIN [General].[Users] u 
								ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
						WHERE	vd.[VehicleId] = #TEMP.VehicleId AND d.[CustomerId] = @pCustomerId) 
			END
			ELSE
			BEGIN

				-- BORRA TODOS LOS VEHICULOS PARA ESE CONDUCTOR EN ESPECIFICO
				DELETE	[General].[VehiclesDrivers]
				WHERE	[UserId] = @pUserId
				
				-- INSERTA PARA ESE USUARIO TODOS LOS VEHICULOS QUE TIENE EL LISTADO
				INSERT	INTO [General].[VehiclesDrivers]
						([VehicleId], [UserId], [InsertDate], [InsertUserId])
				SELECT	Row.col.value('./@VehicleId', 'INT') AS [VehicleId]
						,@pUserId
						,GETUTCDATE()
						,@pLoggedUserId
				FROM	@lxmlData.nodes('/xmldata/Vehicle') Row(col)
				ORDER	BY Row.col.value('./@Index', 'INT')
			END
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO


