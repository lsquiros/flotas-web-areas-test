
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardHolder_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardHolder_Retrieve]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 30/11/2015
-- Description:	Retrieve Credit Card Holder information by Number
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardHolder_Retrieve]
(
	 @pCreditCardNumber nvarchar(520)		--@pCreditCardNumber: Credit Card Number encrypted
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	DECLARE @TipoEmision INT = 101 -- 101:VEHICULOS | 100:Conductores

	SELECT 
		@TipoEmision = cust.[IssueForId]
	FROM [Control].[CreditCard] cc
	INNER JOIN [General].[Customers] cust
		ON cust.CustomerId = cc.CustomerId
	WHERE cc.[CreditCardNumber] = @pCreditCardNumber

	IF (@TipoEmision = 101) --- CONSULTA POR LAS EMISIONES POR VEHICULO
	BEGIN
		SELECT
			 a.[CreditCardId]
			,a.[CustomerId]
			,a.[StatusId]
			,g.[VehicleId]
			,'Veh�culo: ' + h.[PlateId] AS [VehiclePlate]
			,h.[PlateId] AS [TransactionPlate]
			,a.CardRequestId
			,@TipoEmision AS [IssueForId]
		FROM [Control].[CreditCard] a
			LEFT JOIN [Control].[CreditCardByVehicle] g
				ON g.[CreditCardId] = a.[CreditCardId]
			LEFT JOIN [General].[Vehicles] h
				ON h.[VehicleId] = g.[VehicleId]
		WHERE a.[CreditCardNumber] = @pCreditCardNumber
		  AND a.[StatusId] <> 9 -- filter the closed cards
	
	END
	ELSE
	BEGIN  --- CONSULTA POR LAS EMISIONES POR CONDUCTOR
		
		--SELECT 'POR DRIVER'

		SELECT
			 a.[CreditCardId]
			,a.[CustomerId]
			,a.[StatusId]
			,e.[UserId]
			,f.Name AS [EncryptedDriverName]
			,a.CardRequestId
			,@TipoEmision AS [IssueForId]
		FROM [Control].[CreditCard] a
			LEFT JOIN [Control].[CreditCardByDriver] e
				ON e.[CreditCardId] = a.[CreditCardId]
			LEFT JOIN [General].[Users] f
				ON f.[UserId] = e.[UserId]
			LEFT JOIN [General].[VehiclesDrivers] vd
				ON vd.[UserId] = f.[UserId]
		WHERE a.[CreditCardNumber] = @pCreditCardNumber
		  AND a.[StatusId] <> 9 -- filter the closed cards
	END

    SET NOCOUNT OFF
END