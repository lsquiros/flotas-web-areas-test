
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_GetGlobalScoresByMonthOrDays]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_GetGlobalScoresByMonthOrDays]
GO

/****** Object:  StoredProcedure [Operation].[Sp_GetGlobalScoresByMonthOrDays]    Script Date: 11/17/2015 6:19:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	-- Modified by:	Gerald Solano
	-- Modified date:	18/06/2015
	CREATE PROCEDURE [Operation].[Sp_GetGlobalScoresByMonthOrDays]
	( @pCustomerId Int, 
	  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
	  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
	  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
	  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
	  @pReportType char(1)			--Posibles valores S:Summarized, D:Detailed
	)
	as
	BEGIN

		--Verificar configuración de la ponderación
		Declare @TotalWeight numeric(6,2), 
				@CountWeight int
	
		Select @TotalWeight = IsNull(SUM(Weight),0),
			   @CountWeight = COUNT(1)
		  from Operation.WeightedScoreSettings
		 where CustomerId = @pCustomerId

		--Set Dates 
		Declare @DataPerMonth bit = 0 
		If @pEndDate is null
			If @pStartDate is null
			begin
				If @pMonth is null or @pYear is null
				begin
					RAISERROR ('100', 16, 1)
					RETURN
				end
				else
				begin
					--Set @pStartDate = [General].[Fn_GetFirstDayOfMonth](@pYear, @pMonth)
					--Set @pEndDate = DATEADD(MM, 1, @pStartDate)
					Set @DataPerMonth = 1
				end
			end
			else
			begin
				Set @pEndDate = DATEADD(DD, 1, @pStartDate)
			end

		Declare @VehiclesByUser table
		(
			Device int,
			Vehicle int,
			StartOdometer int,
			EndOdometer int,
			StartDate datetime,
			EndDate datetime
		)

		Create table #LocalDriverScore
		(DriverScoreId INT Identity(1,1),
		 UserId int,
		 Name  varchar (250),
		 Identification varchar(50),
		 ScoreType varchar(5),
		 Score Float,
		 Photo varchar(max)
		 )

		Declare @UserId Int
		Declare @Name varchar(250),
				@Identification varchar(50)
		
		Declare  @ScoreAdmOutput nvarchar(4000),
				 @ScoreRouteOutput nvarchar(4000)
			 	
		Declare  @ScoreAdm Float,
				 @ScoreRoute Float
	

		-- //// CALIFICACIONES PRECALCULADAS ////////////////////////////////////////////////////////
		If @DataPerMonth = 0 
		BEGIN
			--Para la invocación por día, recupera las calificaciones precalculadas
			Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score)
			SELECT DS.UserId, U.Name, D.Identification, DS.ScoreType, DS.Score 					
			FROM Operation.DriversScoresDaily DS
			  inner join General.Users U on DS.UserId = U.UserId
			  inner join General.DriversUsers D on DS.UserId = D.UserId
			WHERE DS.CustomerId = @pCustomerId
			   --and DS.ScoreYear = @pYear
			   --and DS.ScoreMonth = @pMonth
			   and (U.IsDeleted IS NULL OR U.IsDeleted <> 1)
			   and DS.Date_Time >= @pStartDate and DS.Date_Time < @pEndDate
		END
		ELSE
		BEGIN
			--Para la invocación por mes, recupera las calificaciones precalculadas
			Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score)
			SELECT DS.UserId, U.Name, D.Identification, DS.ScoreType, DS.Score 					
			--from Operation.DriversScores DS
			FROM Operation.DriversScoresDaily DS
			  inner join General.Users U on DS.UserId = U.UserId
			  inner join General.DriversUsers D on DS.UserId = D.UserId
			WHERE DS.CustomerId = @pCustomerId
			   and DS.ScoreYear = @pYear
			   and DS.ScoreMonth = @pMonth
			   and (U.IsDeleted IS NULL OR U.IsDeleted <> 1)
		END
		-- ///////////////////////////////////////////////////////////////////////////


		-- //////// CALIFICACION GLOBAL //////////////////////////////////////////////
		--SELECT @CountWeight AS COUNT_WEIGHT
		If @CountWeight > 0 
		BEGIN
				Insert into #LocalDriverScore (UserID, Name, Identification, ScoreType, Score, Photo)
				SELECT DS.UserId, DS.Name, DS.Identification, 'TOTAL', ((SUM(DS.Score) / COUNT(*)) * WS.Weight / 100), U.Photo
				FROM #LocalDriverScore DS
				 inner join Operation.ScoreTypes ST on DS.ScoreType = ST.Code 
				 inner join Operation.WeightedScoreSettings WS on ST.ScoreTypeId = WS.ScoreTypeId
				 inner join General.Users U on DS.UserId = U.UserId
				WHERE WS.CustomerId = @pCustomerId and (U.IsDeleted IS NULL OR U.IsDeleted <> 1)
				GROUP BY DS.UserId, DS.Name, DS.Identification, DS.ScoreType, WS.Weight, U.Photo  
		END
		ELSE
		BEGIN
			--No existe configuración de calificación por peso
			--Se asigna la calificación de carretera como la calificación global

			Insert into #LocalDriverScore (UserID, Name, Identification , ScoreType, Score, Photo)
			SELECT DS.UserId, DS.Name, DS.Identification, 'TOTAL', (SUM(DS.Score) / COUNT(*)), U.Photo
			FROM #LocalDriverScore DS
				inner join General.Users U on DS.UserId = U.UserId
			WHERE DS.ScoreType = 'SPROU' and (U.IsDeleted IS NULL OR U.IsDeleted <> 1)
			GROUP BY DS.UserId, DS.Name, DS.Identification, U.Photo
		END
		-- /////////////////////////////////////////////////////////////////////////


		-- //////// TIPO DE REPORTE //////////////////////////////////////////////
		If @pReportType = 'D'
		BEGIN
				SELECT MAX(DS.DriverScoreId), DS.UserId, DS.Name as EncryptedName, 
					DS.Identification As EncryptedIdentification, 
					DS.ScoreType, 
					(SUM(DS.Score) / COUNT(*)), 
					U.Photo
				FROM #LocalDriverScore DS
				inner join General.Users U on DS.UserId = U.UserId
				WHERE  DS.ScoreType <> 'TOTAL'
				GROUP BY DS.UserId, DS.Name, DS.Identification, DS.ScoreType, U.Photo

		END -- REPORT TYPE 'D'
		ELSE
		BEGIN
			--SELECT * FROM #LocalDriverScore
		  IF(@DataPerMonth =1)
		  BEGIN
			SELECT MAX(DS.DriverScoreId),
				DS.UserId,DS.Name as EncryptedName,
				DS.Identification As EncryptedIdentification,
				DS.ScoreType, 
				(SUM(DS.Score) / COUNT(*)) AS Score,
				DS.Photo,
				--0 AS ScoreSpeedAdmin, 
				--0 AS ScoreSpeedRoute 
				(select Sum(Score)/Count(*) FROM Operation.DriversScoresDaily ld WHERE ld.UserId=DS.UserId  AND ScoreType='SPADM' AND ScoreYear = @pYear AND ScoreMonth = @pMonth ) as ScoreSpeedAdmin,
				(select Sum(Score)/Count(*) FROM Operation.DriversScoresDaily ld WHERE ld.UserId=DS.UserId  AND ScoreType='SPROU' AND ScoreYear = @pYear AND ScoreMonth = @pMonth ) as ScoreSpeedRoute
			   ,(select Sum(KmTraveled) FROM Operation.DriversScoresDaily ld WHERE ld.UserId=DS.UserId  AND ScoreYear= @pYear AND ScoreMonth = @pMonth  ) as TraveledDistance
    		FROM #LocalDriverScore DS
			WHERE DS.ScoreType = 'TOTAL'
			GROUP BY  DS.UserId,DS.Name,
				DS.Identification,
				DS.ScoreType, 
				DS.Photo
			ORDER BY DS.ScoreType, SUM(DS.Score) asc
			END

			ELSE
			BEGIN
		
				SELECT MAX(DS.DriverScoreId),
				DS.UserId,DS.Name as EncryptedName,
				DS.Identification As EncryptedIdentification,
				DS.ScoreType, 
				SUM(DS.Score) AS Score,
				DS.Photo,
				--0 AS ScoreSpeedAdmin, 
				--0 AS ScoreSpeedRoute 
				(select Sum(Score)/Count(*) FROM Operation.DriversScoresDaily ld WHERE ld.UserId=DS.UserId  AND ScoreType='SPADM' AND Date_Time between @pStartDate and @pEndDate ) as ScoreSpeedAdmin,
				(select Sum(Score)/Count(*) FROM Operation.DriversScoresDaily ld WHERE ld.UserId=DS.UserId  AND ScoreType='SPROU' AND Date_Time between @pStartDate and @pEndDate ) as ScoreSpeedRoute
			   ,(select Sum(KmTraveled) FROM Operation.DriversScoresDaily ld WHERE ld.UserId=DS.UserId  AND Date_Time between @pStartDate and @pEndDate  ) as TraveledDistance
    		FROM #LocalDriverScore DS
			WHERE DS.ScoreType = 'TOTAL'
			GROUP BY  DS.UserId,DS.Name,
				DS.Identification,
				DS.ScoreType, 
				DS.Photo
			ORDER BY DS.ScoreType, SUM(DS.Score) asc
		
			END
			--SELECT * FROM #TempScoreDriving

		END -- END REPORT TYPE 'S'

		-- //////////////////////////////////////////////////////////////////

		Drop table #LocalDriverScore
	END

GO


