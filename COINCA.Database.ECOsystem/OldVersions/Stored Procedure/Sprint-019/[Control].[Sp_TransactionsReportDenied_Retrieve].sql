
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReportDenied_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsReportDenied_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

	-- ================================================================================================
	-- Author:	Kevin Pe�a
	-- Modify by: Gerald Solano
	-- Create date: 07/24/2015
	-- Updated date: 11/30/2015 
	-- Description:	Retrieve DeniedTransactionsReport log
	-- ================================================================================================
	CREATE PROCEDURE [Control].[Sp_TransactionsReportDenied_Retrieve]
	(
		@pCustomerId INT,					--@pCustomerId: CustomerId		
		@pYear INT = NULL,					--@pYear: Year
		@pMonth INT = NULL,					--@pMonth: Month
		@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
		@pEndDate DATETIME = NULL			--@pEndDate: End Date
	)
	AS
	BEGIN

		SET NOCOUNT ON
		
		IF OBJECT_ID('tempdb..#tmp_transpordata') IS NOT NULL DROP TABLE #tmp_transpordata

		-- SACAR LA PLACA DE TRANSPORT DATA
		Create TABLE #tmp_transpordata  
		(
		  LogId INT,
		  TransportData nvarchar(MAX),
		  Plate VARCHAR(50),
		  CostCenterName VARCHAR(50) DEFAULT 'N/A'
		 )

		INSERT INTO #tmp_transpordata (LogId,TransportData,Plate)
		SELECT LogId,
			   TransportData, 
			   SUBSTRING ( TransportData ,CHARINDEX('"carTag":"', TransportData) +10, CHARINDEX('","kilometers"', TransportData) -CHARINDEX('"carTag":"', TransportData)-10)  
		FROM [Control].[LogTransactionsPOS]  
		WHERE CustomerId = @pCustomerId and 
			  ResponseCode = 05 AND 
			  ((@pYear IS NOT NULL AND 
				@pMonth IS NOT NULL AND 
				DATEPART(m,[InsertDate]) = @pMonth AND 
				DATEPART(yyyy,[InsertDate]) = @pYear) OR 
			  (@pStartDate IS NOT NULL AND 
				@pEndDate IS NOT NULL AND 
				[InsertDate] BETWEEN @pStartDate AND DATEADD(day,1,@pEndDate))) AND
			  TransportData like '%carTag%' and 
					CHARINDEX('"carTag":"', TransportData) > 0 and 
					CHARINDEX('","kilometers"', TransportData) > 0
		
		
		SELECT 
				cast(0 as INT) as [TransactionId]
				,'00000' AS [SystemTraceCode]
				,cast(null as char(10)) as [HolderName]
				,cast(null as char(10)) as [Date]
				,cast(null as char(10)) as [FuelName]
				,cast(0 as decimal) as [FuelAmount]
				,cast(0 as int) as [Odometer] 
				,cast(0 as decimal) as [Liters]
				,cast(null as char(10)) as [PlateId]
				,cast(null as char(10)) as [CurrencySymbol]
				,'Rechazada' as [State]
				,p.[Message]  
				,p.[TransportData]
				,DATEADD(hour,-6,p.[InsertDate]) AS [InsertDate] --UTC
				,p.[ResponseCode]
				,p.[ResponseCodeDescription]
				,cu.[Name] AS [CustomerName]
				--,c.[CostCenterName] 
				,(
					SELECT TOP 1 cc.[Name] FROM [General].[Vehicles] v 
									 INNER JOIN [General].[VehicleCostCenters] cc
										   ON v.CostCenterId = cc.CostCenterId 
									 WHERE CustomerId=@pCustomerId AND PlateId = c.Plate
				)AS [CostCenterName]
			FROM [Control].[LogTransactionsPOS] p		 
			INNER JOIN #tmp_transpordata c
				ON p.[LogId] = c.[LogId]
			INNER JOIN [General].[Customers] cu
				ON cu.CustomerId = p.CustomerId
			ORDER BY [InsertDate] DESC
		
		
		SET NOCOUNT OFF

	END	
	DROP TABLE #tmp_transpordata 	

GO


