
USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriversByVehicle_AddOrEdit]
GO

/****** Object:  StoredProcedure [General].[Sp_DriversByVehicle_AddOrEdit]    Script Date: 11/17/2015 8:25:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 21/07/2015
-- Description:	Insert or Update DriversByVehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DriversByVehicle_AddOrEdit]
(
	 @pVehicleId INT			--@pVehicleId: PK of the table
	,@pXmlData VARCHAR(MAX)		--@pName: Drivers Name
	,@pLoggedUserId INT			--@pLoggedUserId: Logged UserId that executes the operation
	,@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END

			IF @pVehicleId = -1
			BEGIN
				
				DELETE [General].[VehiclesDrivers]
				WHERE UserId IN (SELECT Row.col.value('./@UserId', 'INT')
								FROM @lxmlData.nodes('/xmldata/Driver') Row(col))

				IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
					TRUNCATE TABLE #TEMP
				ELSE
					CREATE TABLE #TEMP(UserId INT)

				INSERT INTO #TEMP
				SELECT Row.col.value('./@UserId', 'INT')
				FROM @lxmlData.nodes('/xmldata/Driver') Row(col)

				INSERT INTO [General].[VehiclesDrivers]
						([VehicleId]
						,[UserId]
						,[InsertDate]
						,[InsertUserId])
				
				SELECT	v.[VehicleId] AS [VehicleId], 
						#TEMP.UserId,
						GETUTCDATE(),
						@pLoggedUserId
				FROM	[General].[Vehicles] v
				LEFT	JOIN [General].[VehiclesDrivers] vd
						ON vd.[VehicleId] = v.[VehicleId]
				cross	JOIN #TEMP
				WHERE	vd.[UserId] IS NULL AND 
						v.[CustomerId] = @pCustomerId AND
						v.[Active] = 1
				UNION 
				SELECT	v.[VehicleId] AS [VehicleId],
						#TEMP.UserId,
						GETUTCDATE(),
						@pLoggedUserId
				FROM   [General].[Vehicles] v
				INNER	JOIN [General].[VehiclesDrivers] vd
						ON vd.[VehicleId] = v.[VehicleId]
				cross	JOIN #TEMP
				WHERE	vd.[UserId] <> #TEMP.UserId AND 
						v.[CustomerId] = @pCustomerId AND
						v.[Active] = 1 AND
						vd.[VehicleId] NOT IN 
						(SELECT v.[VehicleId]
						FROM   [General].[Vehicles] v
						INNER  JOIN [General].[VehiclesDrivers] vd
						ON		vd.[VehicleId] = v.[VehicleId]
						WHERE  vd.[UserId] = #TEMP.UserId AND 
								v.[CustomerId] = @pCustomerId AND
								v.[Active] = 1 )
						ORDER	BY v.[VehicleId]
	
			END
			ELSE
			BEGIN
			
				DELETE [General].[VehiclesDrivers]
				WHERE [VehicleId] = @pVehicleId
			
				INSERT INTO [General].[VehiclesDrivers]
						([VehicleId]
						,[UserId]
						,[InsertDate]
						,[InsertUserId])
				SELECT 
						 @pVehicleId
						,Row.col.value('./@UserId', 'INT') AS [UserId]
						,GETUTCDATE()
						,@pLoggedUserId
				FROM @lxmlData.nodes('/xmldata/Driver') Row(col)
				ORDER BY Row.col.value('./@Index', 'INT')

			END
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO


