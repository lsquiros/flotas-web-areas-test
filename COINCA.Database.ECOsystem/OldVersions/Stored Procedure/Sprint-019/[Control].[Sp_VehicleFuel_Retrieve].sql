
USE [ECOsystemQA]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_VehicleFuel_Retrieve]
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Vehicle Fuel information
-- Modify by : Stefano Quiros
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuel_Retrieve]
(
	 @pVehicleFuelId INT = NULL				--@pVehicleFuelId: PK of the table
	,@pVehicleId INT = NULL					--@pVehicleId: FK of Vehicle Id
	,@pYear INT = NULL						--@pYear: Year
	,@pMonth INT = NULL						--@pMonth: Month
	,@pCustomerId INT = NULL				--@pCustomerId: Customer Ids
	
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	Declare @tableFuelsTemp Table(
		 [FuelByCreditId] int null
		,[FuelId] int null
		,[Year] int null
		,[Month] int null
		,[Total] DECIMAL(16,2) null
		,[Assigned] DECIMAL(18,0) null
		,[Available] DECIMAL(21,2) null
		,[FuelName] varchar(50) null
		,[Liters] DECIMAL(16,2) null
		,[LiterPrice] DECIMAL(16,2) null
		,[CurrencySymbol] nvarchar (50)
		,[RowVersion] varchar(200)
	
	
	)

		insert into @tableFuelsTemp
		exec [Control].[Sp_FuelsByCredit_Retrieve] @pCustomerId, @pYear, @pMonth


	SELECT 
		 a.[VehicleFuelId]
		,a.[VehicleId]
		,a.[Month]
		,a.[Year]		
		,a.[Liters]
		,a.[Amount]
		,d.[Name] AS FuelName
		,d.[FuelId]
		,d.[FuelId] AS [DefaultFuelId]
		,b.[PlateId]
		,b.[Name] AS VehicleName
		,c.[VehicleModel]
		,f.[Symbol] AS [CurrencySymbol]
		,a.[RowVersion]
		,(SELECT Top 1 tmp.[LiterPrice] From @tableFuelsTemp tmp where tmp.[FuelId] = d.[FuelId])  As [LiterPrice]
    FROM [Control].[VehicleFuel] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]
		INNER JOIN [General].[VehicleCategories] c
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] d
			ON c.[DefaultFuelId] = d.[FuelId]
		INNER JOIN [General].[Customers] e
			ON b.[CustomerId] = e.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON e.[CurrencyId] = f.[CurrencyId]
	WHERE (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
	  AND (@pVehicleFuelId IS NULL OR a.[VehicleFuelId] = @pVehicleFuelId)
	  AND (@pCustomerId IS NULL OR b.[CustomerId] = @pCustomerId)
	  AND (@pMonth IS NULL OR a.[Month] = @pMonth)
	  AND (@pYear IS NULL OR a.[Year] = @pYear)
	ORDER BY [VehicleFuelId] DESC
	
    SET NOCOUNT OFF
END
GO
