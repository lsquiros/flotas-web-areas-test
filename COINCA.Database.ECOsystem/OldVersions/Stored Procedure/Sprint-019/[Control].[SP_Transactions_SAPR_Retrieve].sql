
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[SP_Transactions_SAPR_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[SP_Transactions_SAPR_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Henry Retana
-- Create date: 11/05/2015
-- Description:	Retrieve all the process transactions which are going to be displayed in the txt report. 
-- Update, sorting by merchant description, also there is a change when retrieve the merchant name
-- =============================================

CREATE PROCEDURE [Control].[SP_Transactions_SAPR_Retrieve]
(
		@pCustomerId INT,					--@pCustomerId: CustomerId
		@pKey VARCHAR(800) = NULL,			--@pKey :Key
		@pYear INT = NULL,					--@pYear: Year
		@pMonth INT = NULL,					--@pMonth: Month
		@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
		@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN

		SET NOCOUNT ON
		SELECT 
			cc.[CreditCardNumber],	
			t.[TransactionPOS] AS [SystemTraceCode],
			t.[Date],
			t.[ProcessorId] AS [TerminalId],
			ISNULL((SELECT tp.[Name] FROM [dbo].[TransactionsSAPServiceStationsTemp] AS tp 
			WHERE t.[ProcessorId] = tp.[TerminalId]), ISNULL(t.[MerchantDescription], '')) AS [MerchantDescription],	
			t.[Odometer], 
			t.[Liters],				
			t.[FuelAmount],	  
			(SELECT tps.[TransportData] 
			 FROM [Control].[LogTransactionsPOS] tps
			 WHERE tps.[CCTransactionPOSId] = t.[TransactionId]) AS [TransportData],			
			ISNULL(t.[Invoice], '1') AS [Invoice],			
			(SELECT m.[MCC_CODE_SAPR] FROM [General].[Parameters] m) as [MCC]
		FROM 
			[Control].[Transactions] t
			INNER JOIN [General].[Vehicles] v 
				ON t.[VehicleId] = v.[VehicleId]
			INNER JOIN [General].[Customers] c 
				ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN [Control].[Fuels] f 
				ON t.[FuelId] = f.[FuelId]
			INNER JOIN [Control].[CreditCardByVehicle] cv 
				ON t.[CreditCardId] = cv.[CreditCardId]			
			INNER JOIN [Control].[CreditCard] cc
				ON cc.CreditCardId = t.CreditCardId
			
		WHERE
			c.[CustomerId] = @pCustomerId   
			AND t.IsReversed = 0 
			AND t.IsFloating = 0
			AND t.IsDuplicated = 0 
			AND 
			1 > (
				SELECT COUNT(*) FROM Control.Transactions t2
				WHERE t2.[CreditCardId] = t.[CreditCardId] AND
					t2.[TransactionPOS] = t.[TransactionPOS] AND
					t2.[ProcessorId] = t.[ProcessorId] AND 
					t2.IsReversed = 1
				)
			AND (@pKey IS NULL 
				OR v.[PlateId] like '%'+@pKey+'%')
			AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
					AND DATEPART(m,t.[Date]) = @pMonth
					AND DATEPART(yyyy,t.[Date]) = @pYear) OR
				(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
					AND t.[Date] BETWEEN @pStartDate AND @pEndDate))
	
		ORDER BY [MerchantDescription] ASC, t.[Date] DESC

SET NOCOUNT OFF
	END

GO


