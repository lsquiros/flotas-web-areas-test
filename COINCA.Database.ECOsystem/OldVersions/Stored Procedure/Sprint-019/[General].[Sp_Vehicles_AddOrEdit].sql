
USE [ECOsystemQA]
GO

/****** Object:  StoredProcedure [General].[Sp_Vehicles_AddOrEdit]    Script Date: 7/22/2015 4:07:40 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Vehicles_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Vehicles_AddOrEdit]
GO

/****** Object:  StoredProcedure [General].[Sp_Vehicles_AddOrEdit]    Script Date: 7/22/2015 4:07:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Insert or Update Vehicle information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Vehicles_AddOrEdit]
(
	 @pVehicleId INT = NULL							--@pVehicleId: PK of the table
	,@pPlateId VARCHAR(10)							--@pPlateId: Vehicle Plate Number
	,@pName VARCHAR(250)							--@pName: Vehicle Name
	,@pCostCenterId INT								--@pCostCenterId: FK of CostCenter
	,@pCustomerId INT								--@pCustomerId: FK of Customer
	,@pUserId INT									--@pUserId: FK of Users
	,@pVehicleCategoryId INT						--@pVehicleCategoryId: FK of Vehicle Category
	,@pActive BIT									--@pActive: Is Active
	,@pColour VARCHAR(50) = NULL     				--@pColour: Vehicle Colour
	,@pChassis VARCHAR(50) = NULL					--@pChassis: Vehicle Chassis or VIN
	,@pLastDallas VARCHAR(50) = NULL				--@pLastDallas: Vehicle Last Dallas 
	,@pFreightTemperature INT = NULL    			--@pFreightTemperature: Vehicle Freight Temperature
	,@pFreightTemperatureThreshold1 INT	= NULL		--@pFreightTemperatureThreshold1: Vehicle First Freight Temperature Threshold
	,@pFreightTemperatureThreshold2 INT	= NULL		--@pFreightTemperatureThreshold2: Vehicule Second Freight Temperature Threshold
	,@pPredictive BIT								--@pPredictive: Is Predictive
	,@pAVL VARCHAR(50) = NULL						--@PAVL: Vehicle AVL 
	,@pPhoneNumber VARCHAR(10) = NULL				--@pPhoneNumber: Phone Number
	,@pInsurance VARCHAR(20) = NULL					--@pInsurance: Insurance of Vehicle
	,@pDateExpirationInsurance DATE = NULL			--@pDateExpirationInsurance: Date Expiration Insurance
	,@pCoverageType VARCHAR(20)= NULL				--@pCoverageType: Coverage Type of Insurance
	,@pCabinPhone VARCHAR(50)= NULL					--@pCabinPhone: Cabin Phone Number
	,@pNameEnterpriseInsurance VARCHAR(50) = NULL	--@pNameEnterpriseInsurance: Company Name of Insurance
	,@pPeriodicityId INT = 0						--@@pTypeId: Periodicity of Insurance
	,@pCost DECIMAL(16,2) = NULL					--@pCost: Cost 
	,@pOdometer INT = NULL							--@pInitialOdometer: Odometer of Vehicles By Users
	,@pLoggedUserId INT								--@pLoggedUserId: Logged UserId that executes the operation
	,@pAdministrativeSpeedLimit INT					--@pAdministrativeSpeedLimit: Maximum speed limit for this car
	,@pIntrackReference INT = NULL					--@pIntrackReference: Vehicle Number in Intrack System to get AVL
	,@pImei DECIMAL = NULL							--@pImei: Imei number of AVL, use to validate IntrackReference number
	,@pRowVersion TIMESTAMP							--@pRowVersion: Timestamp of row to prevent bad updates
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lUserIdByVehicle INT 
            DECLARE @lVehiclesByUserId INT
            DECLARE @lResult INT
			DECLARE @lIntrackReference INT
			DECLARE @lDeviceReference INT							
			DECLARE @lAdmSpeedLimitPrev INT
            DECLARE @lCustomerPlateCount INT = 0

            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END			

			IF (@pVehicleId IS NULL)
			BEGIN
				-- VALIDAMOS EN LA INSERCI�N SI YA EXISTE LA PLACA
				SELECT @lCustomerPlateCount = COUNT(1)				  
				FROM [General].[Vehicles]
				WHERE [CustomerId] = @pCustomerId AND [PlateId] = @pPlateId 
					  AND [IsDeleted] <> 1

				--Validate vehicle plate is not duplicate for the customer
				IF @lCustomerPlateCount > 0
				BEGIN
					RAISERROR ('PLATE_IS_EXIST', 16, 1)
				END
				ELSE
				BEGIN
					INSERT INTO [General].[Vehicles]
							([PlateId]
							,[Name]
							,[CostCenterId]
							,[CustomerId]
							,[VehicleCategoryId]
							,[Active]
							,[Colour]
							,[Chassis]
							,[LastDallas]
							,[FreightTemperature]
							,[FreightTemperatureThreshold1]
							,[FreightTemperatureThreshold2]
							,[Predictive]
							,[AVL]
							,[PhoneNumber]
							,[Insurance]
							,[DateExpirationInsurance]
							,[CoverageType]
							,[NameEnterpriseInsurance]
							,[PeriodicityId]
							,[Cost]
							,[AdministrativeSpeedLimit]
							,[CabinPhone]
							,[IntrackReference]
							,[Imei]
							,[InsertDate]
							,[InsertUserId])
					VALUES	(UPPER(@pPlateId)
							,@pName
							,@pCostCenterId
							,@pCustomerId
							,@pVehicleCategoryId
							,@pActive
							,@pColour
							,@pChassis
							,@pLastDallas
							,@pFreightTemperature
							,@pFreightTemperatureThreshold1
							,@pFreightTemperatureThreshold2
							,@pPredictive
							,@pAVL
							,@pPhoneNumber
							,@pInsurance
							,@pDateExpirationInsurance
							,@pCoverageType
							,@pNameEnterpriseInsurance
							,@pPeriodicityId
							,@pCost
							,@pAdministrativeSpeedLimit
							,@pCabinPhone
							,@pIntrackReference
							,@pImei
							,GETUTCDATE()
							,@pLoggedUserId)
					
					declare @tmpvehicleid int = SCOPE_IDENTITY()

					IF (@pUserId IS NOT NULL)
					BEGIN 
						INSERT INTO [General].[VehiclesByUser]
									([VehicleId]
									,[UserId]
									,[InitialOdometer]
									,[InsertDate]
									,[InsertUserId])
								VALUES (@tmpvehicleid
									,@pUserId
									,@pOdometer
									,GETUTCDATE()
									,@pLoggedUserId)
					END

					--Inserta registro en distribucion de combustible por defect, solamente la primera vez
					DECLARE @pMonth INT = MONTH(GETDATE()); 
					DECLARE @pYear INT = YEAR(GETDATE()); 
										
					INSERT INTO [Control].[VehicleFuel]
										([VehicleId]
										,[Month]
										,[Year]
										,[Liters]
										,[Amount]
										,[InsertDate]
										,[InsertUserId])
								VALUES	(@tmpvehicleid
										,@pMonth
										,@pYear
										,0.00
										,0.00
										,GETUTCDATE()
										,@pLoggedUserId)

				END -- END DUPLICATE PLATE VALIDATION
				
			END
			ELSE
			BEGIN
				
				-- VALIDAMOS EN LA ACTUALIZACI�N SI YA EXISTE OTRO VEHICULO CON LA MISMA PLACA
				SELECT @lCustomerPlateCount = COUNT(1)				  
				FROM [General].[Vehicles]
				WHERE [CustomerId] = @pCustomerId AND [PlateId] = @pPlateId AND 
					  [VehicleId] <> @pVehicleId AND [IsDeleted] <> 1

				--Validate vehicle plate is not duplicate for the customer
				IF @lCustomerPlateCount > 0
				BEGIN
					RAISERROR ('PLATE_IS_EXIST', 16, 1)
				END
				ELSE
				BEGIN
					IF (@pUserId IS NOT NULL)
					BEGIN 
						SELECT 
							TOP 1 @lUserIdByVehicle = a.[UserId]
						   ,@lVehiclesByUserId = a.[VehiclesByUserId]
						FROM [General].[VehiclesByUser] a
						WHERE a.[VehicleId] = @pVehicleId
						ORDER BY a.[InsertDate] DESC
					
						IF (@lUserIdByVehicle <> @pUserId)
						BEGIN 
							UPDATE [General].[VehiclesByUser]
							SET [LastDateDriving] = GETUTCDATE()
								,[ModifyDate] = GETUTCDATE()
								,[ModifyUserId] = @pLoggedUserId
								,[EndOdometer] = @pOdometer
							WHERE  [VehiclesByUserId] = @lVehiclesByUserId
						
							INSERT INTO [General].[VehiclesByUser]
										([VehicleId]
										,[UserId]
										,[InitialOdometer]
										,[InsertDate]
										,[InsertUserId])
								 VALUES (@pVehicleId
										,@pUserId
										,@pOdometer
										,GETUTCDATE()
										,@pLoggedUserId)
						END 
					END 
				
					--Get Previous AdministrativeSpeedLimit to verify if changed
					SELECT @lAdmSpeedLimitPrev = [AdministrativeSpeedLimit]
						  ,@lIntrackReference = [IntrackReference]
						  ,@lDeviceReference = [DeviceReference]					  
					  FROM [General].[Vehicles]
					 WHERE [VehicleId] = @pVehicleId
				
					UPDATE [General].[Vehicles]
						SET  [PlateId] = UPPER(@pPlateId)
							,[Name] = @pName
							,[CostCenterId] = @pCostCenterId
							,[CustomerId] = @pCustomerId
							,[VehicleCategoryId] = @pVehicleCategoryId
							,[Active] = @pActive
							,[Colour] = @pColour
							,[Chassis] = @pChassis
							,[LastDallas] = @pLastDallas
							,[FreightTemperature] = @pFreightTemperature
							,[FreightTemperatureThreshold1] = @pFreightTemperatureThreshold1
							,[FreightTemperatureThreshold2] = @pFreightTemperatureThreshold2
							,[Predictive] = @pPredictive
							,[AVL] = @pAVL
							,[PhoneNumber] = @pPhoneNumber
							,[Insurance] = @pInsurance
							,[DateExpirationInsurance] = @pDateExpirationInsurance
							,[CoverageType] = @pCoverageType
							,[NameEnterpriseInsurance] = @pNameEnterpriseInsurance
							,[PeriodicityId] = @pPeriodicityId
							,[Cost] = @pCost
							,[AdministrativeSpeedLimit] = @pAdministrativeSpeedLimit
							,[CabinPhone] = @pCabinPhone
							,[IntrackReference] = @pIntrackReference
							,[Imei] = @pImei
							,[ModifyDate] = GETUTCDATE()
							,[ModifyUserId] = @pLoggedUserId
					WHERE [VehicleId] = @pVehicleId
					  AND [RowVersion] = @pRowVersion

				END --END DUPLICATE PLATE VALIDATION 
			END
            
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
				--Replicate data in Atrack Database
				IF @pVehicleId IS NOT NULL AND @pAdministrativeSpeedLimit IS NOT NULL --and IsNull(@lAdmSpeedLimitPrev, 0) <> IsNull(@pAdministrativeSpeedLimit, 0)
				BEGIN
					IF @lIntrackReference = 0 
						Set @lIntrackReference = null
					IF @lDeviceReference = 0 
						Set @lDeviceReference = null
					--Aunque hubiera Intrack References, si no hay DeviceRefence no se ejecuta la replicaci�n
					IF @lDeviceReference is not null
					BEGIN
						Exec General.Sp_Vehicles_Replication @pVehicleId, @pCustomerId, @lIntrackReference, @lDeviceReference, @pAdministrativeSpeedLimit, @pResult = @lResult output, @pErrorDesc = @lErrorMessage output
						IF @lResult = -1 
						BEGIN
							--Send Email to Admin
							DECLARE @lTo varchar(100) = '',
									@lSubject varchar(300),
									@lMessage varchar(1000),
									@lEmail varchar(100)
							
							Set @lTo = General.Fn_GetEmailsRole('SUPER_ADMIN', 0, 100)
							
							IF LEN(@lTo) = 0
								Set @lTo = 'coinca@coinca.tv'
								
							Set @lSubject = 'Error replicando datos de Veh�culo en Base de Datos Atrack'
							Set @lMessage = 'Durante la replicaci�n del Veh�culo Id ' + convert(varchar, @pVehicleId) + ' Placa No ' + UPPER(@pPlateId) + 
											' se present� el siguiente error: ' + @lErrorMessage + '. Los datos en la base de datos Atrack quedaron desactualizados.'
							
							Exec General.Sp_Email_AddOrEdit null, @lTo, null, null, @lSubject, @lMessage, 0, 0, @pLoggedUserId, null 
						END
					END
				END --End Replication
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END


			IF @pIntrackReference IS NOT NULL 
				BEGIN
					--VERIFICA QUE EL IMEI PERTENEZCA AL NUMERO DE VEHICULO
					DECLARE @RowNum INT = 0

					SELECT @RowNum = Count(*)
					FROM 
						[dbo].[Vehiculos] [a]
						INNER JOIN [dbo].[Composiciones] [b] ON [a].[Vehiculo] = [b].[Vehiculo]
						INNER JOIN [dbo].[DispositivosAVL] [c] ON [b].[Dispositivo] = [c].[Dispositivo]
						INNER JOIN [dbo].[Devices] [d] ON [c].[Numeroimei] = [d].[UnitID]
						INNER JOIN [General].[Vehicles] [e] ON [a].[Vehiculo] = [e].[IntrackReference]
					WHERE 
						[a].[Vehiculo] = @pIntrackReference AND 
						[d].[UnitID] = @pImei


					IF @RowNum = 0
						BEGIN
							UPDATE [General].[Vehicles]
								SET  [IntrackReference] = NULL
									,[Imei] = NULL
							WHERE [VehicleId] = @pVehicleId

							RAISERROR ('IMEI_NO_CORRESPONDE', 16, 1)
						END
					ELSE
						BEGIN
							UPDATE [e]
								SET [e].[DeviceReference] = [d].[Device]
							FROM 
								[dbo].[Vehiculos] [a]
								INNER JOIN [dbo].[Composiciones] [b] ON [a].[Vehiculo] = [b].[Vehiculo]
								INNER JOIN [dbo].[DispositivosAVL] [c] ON [b].[Dispositivo] = [c].[Dispositivo]
								INNER JOIN [dbo].[Devices] [d] ON [c].[Numeroimei] = [d].[UnitID]
								INNER JOIN [General].[Vehicles] [e] ON [a].[Vehiculo] = [e].[IntrackReference]
							WHERE 
								[a].[Vehiculo] = @pIntrackReference
						END
				END

    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


