
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerTerminal_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerTerminal_AddOrEdit]
GO

/****** Object:  StoredProcedure [General].[Sp_CustomerTerminal_AddOrEdit]    Script Date: 11/10/2015 4:52:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Stefano Quiros Ruiz.
-- Create date: 30/10/2015
-- Description:	Insert or Update CustomerTerminal information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerTerminal_AddOrEdit]
(
	 @pCustomerId INT,
	 @pTerminalId Varchar(250),
	 @pMerchantDescription varchar(250) = NULL
	 
)
AS
BEGIN
declare @pTemp varchar(250)

SELECT @pTemp = [TerminalId]
FROM [General].[Terminal] 
WHERE [TerminalId] = @pTerminalId;	               
					
IF (@pTemp IS NULL)
	BEGIN
		INSERT INTO [General].[Terminal]
				([CustomerId]
				,[TerminalId]
				,[MerchantDescription])
		VALUES	(@pCustomerId
					,@pTerminalId
					,@pMerchantDescription)
	END
ELSE
	BEGIN
		UPDATE [General].[Terminal]
			SET [MerchantDescription] = @pMerchantDescription						
		WHERE [TerminalId] = @pTerminalId
	END
END	

GO