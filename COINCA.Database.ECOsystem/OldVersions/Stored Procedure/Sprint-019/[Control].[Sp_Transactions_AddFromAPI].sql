
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Transactions_AddFromAPI]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_Transactions_AddFromAPI]
GO

/****** Object:  StoredProcedure [Control].[Sp_Transactions_AddFromAPI]    Script Date: 11/12/2015 2:27:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/30/2014
-- Description:	Insert Transaction information from API
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_Transactions_AddFromAPI]
(
	 @pPlateId VARCHAR(10)	
	,@pFuelId INT
	,@pCreditCardNumber NVARCHAR(520)
	,@pDate DATETIME
	,@pOdometer INT
	,@pLiters DECIMAL(16,2)
	,@pFuelAmount DECIMAL(16,2)	
	,@pInvoice VARCHAR(12) = NULL
	,@pTransactionPOS VARCHAR(250) = NULL
	,@pSchemePOS VARCHAR(MAX) = NULL
	,@pMerchantDescription VARCHAR(50) = NULL
	,@pProcessorId VARCHAR(250) = NULL
	,@pIsFloating BIT = NULL
	,@pIsReversed BIT = NULL
	,@pIsDuplicated BIT = NULL
	,@pIsVoid BIT = NULL
	,@pLoggedUserId INT
	,@pCustomerId INT
	,@pDriverCode VARCHAR(MAX)
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lVehicleId INT            
            DECLARE @lTransactionId INT = 0
            DECLARE @lCreditCardId INT
			DECLARE @IsInternal BIT

			SET @IsInternal =  CASE WHEN EXISTS (SELECT * FROM	[General].[Terminal] WHERE [TerminalId] = @pProcessorId AND [CustomerId] = @pCustomerId) 
									THEN 1
									ELSE 0
								END
            
            SELECT @lVehicleId = a.[VehicleId]
				FROM [General].[Vehicles] a
            WHERE a.PlateId = @pPlateId
            
            SELECT @lCreditCardId =  [CreditCardId]
				FROM [Control].[CreditCard]
            WHERE [CreditCardNumber] = @pCreditCardNumber
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			 
			INSERT INTO [Control].[Transactions](
					 [VehicleId]
					,[FuelId]
					,[CreditCardId]
					,[Date]
					,[Odometer]
					,[Liters]
					,[FuelAmount]					
					,[TransactionPOS]
					,[SchemePOS]
					,[ProcessorId]
					,[IsFloating]
					,[IsReversed]
					,[IsDuplicated]
					,[IsVoid]
					,[InsertDate]
					,[InsertUserId]
					,[Invoice]
					,[MerchantDescription]
					,[IsInternal]
					,[DriverCode])
			VALUES	(@lVehicleId
					,@pFuelId
					,@lCreditCardId
					,@pDate
					,@pOdometer
					,@pLiters
					,@pFuelAmount					
					,@pTransactionPOS
					,@pSchemePOS
					,@pProcessorId
					,@pIsFloating
					,@pIsReversed
					,@pIsDuplicated
					,@pIsVoid
					,GETUTCDATE()
					,@pLoggedUserId
					,@pInvoice
					,@pMerchantDescription
					,@IsInternal
					,@pDriverCode)
			
			SELECT @lTransactionId = SCOPE_IDENTITY()
			
			--IF (@pIsReversed = 0 and @pIsDuplicated = 0)
			IF (@pIsDuplicated = 0 AND @IsInternal = 0)
			BEGIN
				Exec [Control].[Sp_CreditCard_CreditAvailable_Edit] @lCreditCardId, @pFuelAmount, @pIsReversed
			END
			
			-- History control --
			INSERT INTO [Control].[TransactionsHx]
			   ([TransactionId]
			   ,[CreditCardId]
			   ,[VehicleId]
			   ,[FuelId]
			   ,[Date]
			   ,[Odometer]
			   ,[FuelAmount]
			   ,[TransactionPOS]
			   ,[SchemePOS]
			   ,[ProcessorId]
			   ,[IsAdjustment]
			   ,[IsFloating]
			   ,[IsReversed]
			   ,[IsDuplicated]
			   ,[IsVoid]
			   ,[FixedOdometer]
			   ,[Liters]
			   ,[InsertDate]
			   ,[InsertUserId]
			   ,[ModifyDate]
			   ,[ModifyUserId]
			   ,[Invoice]
			   ,[MerchantDescription]
			   ,[DriverCode])
			SELECT 
				[TransactionId]
			   ,[CreditCardId]
			   ,[VehicleId]
			   ,[FuelId]
			   ,[Date]
			   ,[Odometer]
			   ,[FuelAmount]
			   ,[TransactionPOS]
			   ,[SchemePOS]
			   ,[ProcessorId]
			   ,[IsAdjustment]
			   ,[IsFloating]
			   ,[IsReversed]
			   ,[IsDuplicated]
			   ,[IsVoid]
			   ,[FixedOdometer]
			   ,[Liters]
			   ,[InsertDate]
			   ,[InsertUserId]
			   ,[ModifyDate]
			   ,[ModifyUserId]
			   ,[Invoice]
			   ,[MerchantDescription] 
			   ,[DriverCode] FROM [Control].[Transactions] WHERE [TransactionId] = @lTransactionId
			-- History control --
					
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	
	SELECT @lTransactionId AS [TransactionId], @IsInternal AS [IsInternal]
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO