
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByDriver_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesByDriver_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_VehiclesByDriver_Retrieve]    Script Date: 11/19/2015 12:46:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 22/07/2015
-- Description:	Retrieve VehiclesByDriver information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesByDriver_Retrieve]
(
	 @pUserId INT	--@pUserId: PK of the table
	,@pCustomerId INT	--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	
	SET NOCOUNT ON
	--DECLARE @pUserId INT = -1
	--DECLARE @pCustomerId INT = 10

	IF @pUserId = -1
	BEGIN 

		IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
			DROP TABLE #TEMP
		ELSE
			CREATE TABLE #TEMP 
				(UserId INT
				,VehicleId INT
				,VehicleName VARCHAR(250)
				,Plate VARCHAR(250))

		INSERT INTO #TEMP

		SELECT -- VEHICULOS SIN ASIGNAR
			NULL AS [UserId],
			v.[VehicleId] AS [VehicleId],
			v.[Name] AS [VehicleName],
			v.[PlateId] AS [Plate] 
		FROM  [General].[Vehicles] v
		LEFT JOIN [General].[VehiclesDrivers] vd
			ON vd.[VehicleId] = v.[VehicleId]
		WHERE vd.[UserId] IS NULL AND 
			  v.[CustomerId] = @pCustomerId AND
			  v.[Active] = 1
		
		UNION

		SELECT -- VEHICULOS YA ASIGNADOS A OTROS CONDUCTORES
			NULL AS [UserId],
			v.[VehicleId] AS [VehicleId],
			v.[Name] AS [VehicleName],
			v.[PlateId] AS [Plate] 
		FROM   [General].[Vehicles] v
		INNER JOIN [General].[VehiclesDrivers] vd
			ON vd.[VehicleId] = v.[VehicleId]
		WHERE  vd.[UserId] <> @pUserId AND 
			   v.[CustomerId] = @pCustomerId AND
			   v.[Active] = 1 AND
			   vd.[VehicleId] NOT IN (
					SELECT 
						v.[VehicleId]
					FROM   [General].[Vehicles] v
					INNER JOIN [General].[VehiclesDrivers] vd
						ON vd.[VehicleId] = v.[VehicleId]
					WHERE  vd.[UserId] = @pUserId AND 
						   v.[CustomerId] = @pCustomerId AND
						   v.[Active] = 1 )
		UNION

		SELECT -- VEHICULOS ASIGNADOS AL CONDUCTOR CONSULTADO
			vd.[UserId] AS [UserId],
			v.[VehicleId] AS [VehicleId],
			v.[Name] AS [VehicleName],
			v.[PlateId] AS [Plate] 
		FROM   [General].[Vehicles] v
		INNER JOIN [General].[VehiclesDrivers] vd
			ON vd.[VehicleId] = v.[VehicleId]
		WHERE  vd.[UserId] = @pUserId AND 
			   v.[CustomerId] = @pCustomerId AND
			   v.[Active] = 1

		UPDATE #TEMP
		SET UserId =	(SELECT	COUNT(*)
						FROM	[General].[DriversUsers] d
						LEFT	JOIN [General].[VehiclesDrivers] vd
								ON vd.[UserId] = d.[UserId]
						INNER	JOIN [General].[Users] u 
								ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1  AND u.[IsDeleted] = 0
						WHERE	vd.[UserId] IS NULL AND d.[CustomerId] = @pCustomerId) +

						(SELECT	COUNT(*)   -- CONDUCTORES YA ASIGNADOS A OTROS VEHICULOS
						FROM	[General].[DriversUsers] d
						INNER	JOIN [General].[VehiclesDrivers] vd
								ON vd.[UserId] = d.[UserId]
						INNER	JOIN [General].[Users] u 
								ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
						WHERE	vd.[VehicleId] <> #TEMP.VehicleId AND 
								d.[CustomerId] = @pCustomerId AND 
								vd.[UserId] NOT IN
								(SELECT	d.[UserId] AS [UserId]
								FROM	[General].[DriversUsers] d
								INNER	JOIN [General].[VehiclesDrivers] vd
										ON vd.[UserId] = d.[UserId]
								INNER	JOIN [General].[Users] u 
										ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
								WHERE	vd.[VehicleId] = #TEMP.VehicleId AND d.[CustomerId] = @pCustomerId) )

		UPDATE	#TEMP 
		SET		UserId = CASE WHEN UserId = 0 THEN -1 ELSE NULL END;

		SELECT	UserId
				,VehicleId
				,VehicleName
				,Plate 
		FROM	#TEMP
	END
	ELSE
	BEGIN

		SELECT -- VEHICULOS SIN ASIGNAR
			NULL AS [UserId],
			v.[VehicleId] AS [VehicleId],
			v.[Name] AS [VehicleName],
			v.[PlateId] AS [Plate] 
		FROM  [General].[Vehicles] v
		LEFT JOIN [General].[VehiclesDrivers] vd
			ON vd.[VehicleId] = v.[VehicleId]
		WHERE vd.[UserId] IS NULL AND 
			  v.[CustomerId] = @pCustomerId AND
			  v.[Active] = 1
		
		UNION

		SELECT  -- VEHICULOS YA ASIGNADOS A OTROS CONDUCTORES
			NULL AS [UserId],
			v.[VehicleId] AS [VehicleId],
			v.[Name] AS [VehicleName],
			v.[PlateId] AS [Plate] 
		FROM   [General].[Vehicles] v
		INNER JOIN [General].[VehiclesDrivers] vd
			ON vd.[VehicleId] = v.[VehicleId]
		WHERE  vd.[UserId] <> @pUserId AND 
			   v.[CustomerId] = @pCustomerId AND
			   v.[Active] = 1 AND
			   vd.[VehicleId] NOT IN (
					SELECT 
						v.[VehicleId]
					FROM   [General].[Vehicles] v
					INNER JOIN [General].[VehiclesDrivers] vd
						ON vd.[VehicleId] = v.[VehicleId]
					WHERE  vd.[UserId] = @pUserId AND 
						   v.[CustomerId] = @pCustomerId AND
						   v.[Active] = 1 )
		UNION

		SELECT -- VEHICULOS ASIGNADOS AL CONDUCTOR CONSULTADO
			vd.[UserId] AS [UserId],
			v.[VehicleId] AS [VehicleId],
			v.[Name] AS [VehicleName],
			v.[PlateId] AS [Plate] 
		FROM   [General].[Vehicles] v
		INNER JOIN [General].[VehiclesDrivers] vd
			ON vd.[VehicleId] = v.[VehicleId]
		WHERE  vd.[UserId] = @pUserId AND 
			   v.[CustomerId] = @pCustomerId AND
			   v.[Active] = 1

	END
	
    SET NOCOUNT OFF
END


GO


