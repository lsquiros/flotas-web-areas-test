
USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ServiceStations_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ServiceStations_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 20/11/2015
-- Description:	Add Or Edit Service Stations
-- ================================================================================================
Create PROCEDURE [General].[Sp_ServiceStations_AddOrEdit]
(
	@pServiceStationId INT = NULL,	
	@pBacId VARCHAR(100) = NULL,
	@pName VARCHAR(300) =  NULL,
	@pAddress VARCHAR(MAX) =  NULL,
	@pCantonId INT = NULL,
	@pProvinceId INT = NULL,
	@pLatitude FLOAT = NULL,
	@pLongitude FLOAT = NULL,
	@pUserId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
		
		IF(@pServiceStationId IS NOT NULL)
		BEGIN 
			UPDATE [General].[ServiceStations]
			SET				
				[BacId] = @pBacId
				,[Name] = @pName
				,[Address] = @pAddress
				,[CantonId] = @pCantonId
				,[ProvinceId] = @pProvinceId				
				,[ModifyUserId] = @pUserId
				,[ModifyDate] = (DATEADD(hour, -6, GETDATE()))				
				,[Latitude] = @pLatitude
				,[Longitude] = @pLongitude
			WHERE [ServiceStationId] = @pServiceStationId
		END	
		ELSE
		BEGIN
			INSERT INTO [General].[ServiceStations]
			   ([BacId]
				,[Name] 
				,[Address] 
				,[CantonId] 
				,[ProvinceId] 
				,[InsertUserId] 
				,[InsertDate]
				,[Latitude] 
				,[Longitude])
			VALUES
			   (@pBacId
				,@pName
				,@pAddress
				,@pCantonId
				,@pProvinceId
				,@pUserId
				,DATEADD(hour, -6, GETDATE())
				,@pLatitude
				,@pLongitude				
			   )			   
		END
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO
