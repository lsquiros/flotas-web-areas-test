
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReportDeniedByPartner_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsReportDeniedByPartner_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

	-- ================================================================================================
	-- Author:		Kevin Pe�a
	-- Create date: 11/02/2015
	-- Modify by:	Gerald Solano
	-- Modify date: 11/30/2015
	-- Description:	Retrieve DeniedTransactionsReport log
	-- ================================================================================================
	CREATE PROCEDURE [Control].[Sp_TransactionsReportDeniedByPartner_Retrieve]
	(
		@pPartnerId INT,					--@pPartnerId: PartnerId		
		@pYear INT = NULL,					--@pYear: Year
		@pMonth INT = NULL,					--@pMonth: Month
		@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
		@pEndDate DATETIME = NULL			--@pEndDate: End Date
	)
	AS
	BEGIN

		SET NOCOUNT ON
		
		DECLARE @pCustomerId INT = null

		IF OBJECT_ID('tempdb..#tmp_transpordata') IS NOT NULL DROP TABLE #tmp_transpordata
		IF OBJECT_ID('tempdb..#tmp_customersdata') IS NOT NULL DROP TABLE #tmp_customersdata

		-- OBTENER LOS CUSTOMERS DEL PARTNER
		Create TABLE #tmp_customersdata  
		(
			CustomerId INT
		)
		
		INSERT INTO #tmp_customersdata  (CustomerId)
		SELECT CustomerId
		FROM [General].[CustomersByPartner] WHERE PartnerId =@pPartnerId


		-- EXTRAER PLACA DEL TRANSAPORT DATA
		Create TABLE #tmp_transpordata  
		(
			LogId INT,
			CustomerId INT,		
			TransportData nvarchar(MAX),
			Plate VARCHAR(50),
			CostCenterName VARCHAR(50) DEFAULT 'N/A'
		)

		SELECT @pCustomerId = MIN(CustomerId)
		FROM #tmp_customersdata

		WHILE(@pCustomerId IS NOT NULL) -- RECORREMOS CADA CUSTOMER
		BEGIN
			INSERT INTO #tmp_transpordata (LogId,CustomerId,TransportData,Plate)
			SELECT LogId,
				   @pCustomerId,
				   TransportData, 
				   SUBSTRING ( TransportData ,CHARINDEX('"carTag":"', TransportData) +10, CHARINDEX('","kilometers"', TransportData) -CHARINDEX('"carTag":"', TransportData)-10)  
			FROM [Control].[LogTransactionsPOS]  
			WHERE CustomerId = @pCustomerId and 
				  ResponseCode =05 AND 
				  ((@pYear IS NOT NULL AND 
					@pMonth IS NOT NULL AND 
					DATEPART(m,[InsertDate]) = @pMonth AND 
					DATEPART(yyyy,[InsertDate]) = @pYear) OR 
				  (@pStartDate IS NOT NULL AND 
					@pEndDate IS NOT NULL AND 
					[InsertDate] BETWEEN @pStartDate AND DATEADD(day,1,@pEndDate))) AND
				   TransportData like '%carTag%' and 
						CHARINDEX('"carTag":"', TransportData) > 0 and 
						CHARINDEX('","kilometers"', TransportData) > 0

			-- NEXT INDEX
			SELECT @pCustomerId = MIN(CustomerId)
			FROM #tmp_customersdata	
			WHERE CustomerId > @pCustomerId

		END -- TERMINA RECORRIDO DE CUSTOMER
		
		
		--SELECT * FROM #tmp_transpordata	
			
		SELECT 
			cast(0 as INT) as [TransactionId]
			,'00000' AS [SystemTraceCode]
			,cast(null as char(10)) as [HolderName]
			,cast(null as char(10)) as [Date]
			,cast(null as char(10)) as [FuelName]
			,cast(0 as decimal) as [FuelAmount]
			,cast(0 as int) as [Odometer] 
			,cast(0 as decimal) as [Liters]
			,cast(null as char(10)) as [PlateId]
			,cast(null as char(10)) as [CurrencySymbol]
			,'Rechazada' as [State]
			,p.[Message]  
			,p.[TransportData]
			,DATEADD(hour,-6,p.[InsertDate]) AS [InsertDate] --UTC
			,p.[ResponseCode]
			,p.[ResponseCodeDescription]
			--,c.[CostCenterName] 
			,(
				SELECT TOP 1 cc.[Name] FROM [General].[Vehicles] v 
										INNER JOIN [General].[VehicleCostCenters] cc
												ON v.CostCenterId = cc.CostCenterId 
										WHERE CustomerId=@pCustomerId AND PlateId = c.Plate
			)AS [CostCenterName]
			,c.CustomerId
			,cu.Name AS [CustomerName]
		FROM #tmp_transpordata c		 
		INNER JOIN [Control].[LogTransactionsPOS] p
			ON p.[LogId] = c.[LogId]
		INNER JOIN [General].[Customers] cu
			ON cu.CustomerId = c.CustomerId
		ORDER BY [InsertDate] DESC

		SET NOCOUNT OFF
		
		DROP TABLE #tmp_transpordata 
		DROP TABLE #tmp_customersdata

	END
	
GO


