
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CostCentersByCredit_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CostCentersByCredit_Retrieve]
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Cost Centers By Credit
-- Modify by: Stefano Quiros
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CostCentersByCredit_Retrieve]
(
	 @pCostCenterId INT						--@pCostCenterId: PK of the table VehicleGroup
	,@pYear INT								--@pYear: Year
	,@pMonth INT	                         --@pMonth: Month	
	,@pCustomerId INT = NULL						
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	
	Declare @tableFuelsTemp Table(
		 [FuelByCreditId] int null
		,[FuelId] int null
		,[Year] int null
		,[Month] int null
		,[Total] DECIMAL(16,2) null
		,[Assigned] DECIMAL(18,0) null
		,[Available] DECIMAL(21,2) null
		,[FuelName] varchar(50) null
		,[Liters] DECIMAL(16,2) null
		,[LiterPrice] DECIMAL(16,2) null
		,[CurrencySymbol] nvarchar (50)
		,[RowVersion] varchar(200)
	
	
	)

	insert into @tableFuelsTemp
	exec [Control].[Sp_FuelsByCredit_Retrieve] @pCustomerId, @pYear, @pMonth

	SELECT
		 a.PlateId
		,a.[Name] AS  [VehicleName]
		,b.[Manufacturer] + ' - ' + b.[VehicleModel] AS VehicleModel
		,f.[Symbol] AS [CurrencySymbol]
		,ISNULL(t.[Amount], CONVERT(DECIMAL(16,2),0)) AS [Amount]
		,ISNULL(t.[Liters],0) AS [Liters]
		,cf.[Name] AS FuelName
		,a.[VehicleId]
		,b.[DefaultFuelId]
		,c.[Code] + ' - ' + c.[Name] AS CostCenter
		,t.[VehicleFuelId]
		,t.[RowVersion]	
		,(SELECT Top 1 tmp.[LiterPrice] From @tableFuelsTemp tmp where tmp.[FuelId] = cf.[FuelId])  As [LiterPrice]	
	FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCategories] b
			ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
	    INNER JOIN [Control].[Fuels] cf
		    ON b.[DefaultFuelId] = cf.[FuelId]
		INNER JOIN [General].[VehicleCostCenters] c
			ON a.[CostCenterId] = c.[CostCenterId]
		INNER JOIN [General].[Customers] e
			ON e.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON f.[CurrencyId] = e.[CurrencyId]
		LEFT JOIN (
				SELECT 
					x.[VehicleId]
					,x.[Amount]
					,x.[Year]
					,x.[Month]
					,x.[Liters]
					,x.[VehicleFuelId]
					,x.[RowVersion]
				FROM [Control].[VehicleFuel] x	
					INNER JOIN (
						SELECT	VehicleId, max(VehicleFuelID) AS VehicleFuelID
						FROM	[Control].[VehicleFuel]
						WHERE [Year] = @pYear
						  AND [Month] = @pMonth
						GROUP BY VehicleId
					) AS y ON x.VehicleId = y.VehicleId AND x.VehicleFuelID = y.VehicleFuelID
				WHERE x.[Year] = @pYear
				  AND x.[Month] = @pMonth) t
			ON t.VehicleId = a.VehicleId
	WHERE c.[CostCenterId] = @pCostCenterId
	  
    SET NOCOUNT OFF
END



GO


