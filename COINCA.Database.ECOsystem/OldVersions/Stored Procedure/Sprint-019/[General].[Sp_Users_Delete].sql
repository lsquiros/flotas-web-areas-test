
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Users_Delete]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Delete User information. Update 11/11/2015, Henry Retana, Edit Email and User Name 
-- in the [dbo].[AspNetUsers] so the email and user name can be reused.
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Users_Delete]
(
	 @pUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			UPDATE [General].[Users]
				SET [IsDeleted] = 1
			WHERE [UserId] = @pUserId
			
			DECLARE @pAspUserId VARCHAR(200) 
			SET @pAspUserId = (SELECT [AspNetUserId] FROM [General].[Users] WHERE [UserId] = @pUserId)

			DECLARE @pEmail VARCHAR(200) 
			SET @pEmail = (SELECT [Email] FROM [dbo].[AspNetUsers] WHERE [Id] = @pAspUserId)

			UPDATE [dbo].[AspNetUsers] 
			SET [Email] = @pEmail + '1'
			    ,[UserName] = @pEmail + '1'
			WHERE [Id] = @pAspUserId

            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
            
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


