
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ConsolidateLiquidationReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_ConsolidateLiquidationReport_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes.
-- Create date: 03/02/2015
-- Description:	Retrieve ConsolidateLiquidationReport information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_ConsolidateLiquidationReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	--DECLARE @pCustomerId INT = 30
	--DECLARE @pYear INT = 2015
	--DECLARE @pMonth INT = 11	

	declare @ty int= (select top (1) t.TypeId from General.Customers c INNER JOIN General.[Types] t ON t.TypeId=c.IssueForId 
	where CustomerId=@pCustomerId)
	
	SELECT v.VehicleId,
		   v.PlateId,
		   du.Identification,
		   vf.Liters,
		   ISNULL((SELECT top(1) pv.TrxReportedOdometer 
				FROM General.PerformanceByVehicle pv
				where (MONTH(TrxDate)=@pMonth 
					   AND YEAR(TrxDate)=@pYear)
					   AND VehicleId=v.VehicleId 
					   AND 1 > (
								SELECT ISNULL(Count(1), 0)
								FROM (
									SELECT TOP 1 [CreditCardId],[TransactionPOS],[ProcessorId] 
									FROM [Control].[Transactions]
									WHERE [TransactionId] = pv.TransactionId
								)tx
								INNER JOIN  [Control].[Transactions] tx2
									ON  tx2.[CreditCardId] = tx.[CreditCardId] AND
										tx2.[TransactionPOS] = tx.[TransactionPOS] AND
										tx2.[ProcessorId] = tx.[ProcessorId]
								WHERE tx2.IsReversed = 1
							)
				order by PerformanceVehicleId desc), 0)		  
		    as TotalOdometer,
		   vc.Liters as TankCapacity,
		   f.FuelId,
		   f.Name as FuelName,
		   vu.UnitId,
		   vu.Name as UnitName,
		   vcc.CostCenterId,
		   vcc.Name as CostCenterName,
		   vc.Manufacturer,
		   vc.VehicleModel,
		   vc.[Year],
		   du.Code,
		   (case @ty when 100 then 
				(SELECT top(1) cc.CreditCardNumber FROM [Control].CreditCardByDriver ccd INNER JOIN [Control].CreditCard cc  ON ccd.CreditCardId=cc.CreditCardId WHERE CCD.UserId=du.UserId)
			ELSE
				(SELECT top(1) cc.CreditCardNumber FROM [Control].CreditCardByVehicle ccd INNER JOIN [Control].CreditCard cc  ON ccd.CreditCardId=cc.CreditCardId WHERE CCD.VehicleId=V.VehicleId)
			END) AS CreditCardNumber
	FROM General.Vehicles v 
	INNER JOIN [Control].VehicleFuel vf ON v.VehicleId=vf.VehicleId
	INNER JOIN General.VehicleCategories vc ON v.VehicleCategoryId =vc.VehicleCategoryId
    INNER JOIN [Control].Fuels f ON vc.DefaultFuelId=f.FuelId
	INNER JOIN General.VehicleCostCenters vcc ON V.CostCenterId=vcc.CostCenterId
	INNER JOIN General.VehicleUnits vu ON vcc.UnitId=vu.UnitId
	INNER JOIN General.VehiclesByUser vbu ON vbu.VehicleId=v.VehicleId
	INNER JOIN General.DriversUsers du ON vbu.UserId=du.UserId
	WHERE V.CustomerId=@pCustomerId and 
		 (vf.[Month]=@pMonth and vf.[Year]=@pYear)
		  
	SET NOCOUNT OFF
END
GO


