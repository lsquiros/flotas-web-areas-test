
USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_TransactionsSAPServiceStationsTemp_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_TransactionsSAPServiceStationsTemp_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
	-- ================================================================================================
	-- Author:		Henry Retana
	-- Create date: 11/24/2015
	-- Description:	Retrieve Merchant Description
	-- ================================================================================================
	CREATE PROCEDURE [dbo].[SP_TransactionsSAPServiceStationsTemp_Retrieve]
	(
		@pTerminalId VARCHAR(100) 
	)
	AS
	BEGIN

		SET NOCOUNT ON
		
		SELECT [Name] 
		FROM [dbo].[TransactionsSAPServiceStationsTemp]
		WHERE [TerminalId] =  @pTerminalId

		SET NOCOUNT OFF
	END
GO