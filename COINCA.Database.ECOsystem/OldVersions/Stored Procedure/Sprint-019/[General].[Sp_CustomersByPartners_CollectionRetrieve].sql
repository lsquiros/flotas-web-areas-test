
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomersByPartners_CollectionRetrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomersByPartners_CollectionRetrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes
-- Create date: 10/02/2015
-- Description:	Retrieve Customers By Partner
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomersByPartners_CollectionRetrieve] 
( 
	@pPartnerId Int 
 )
AS
BEGIN
	   SELECT DISTINCT  a.[CustomerId]
		,a.[Name] AS [EncryptedName]
		,a.[InsertDate]
		,a.[RowVersion]
       FROM [General].[Customers] a 
	   LEFT JOIN General.[CustomersByPartner] cbp  
			ON cbp.CustomerId=a.CustomerId
       WHERE PartnerId = @pPartnerId AND 
			 a.IsActive= 1 AND 
			(a.IsDeleted IS NULL OR a.IsDeleted = 0)
END
GO