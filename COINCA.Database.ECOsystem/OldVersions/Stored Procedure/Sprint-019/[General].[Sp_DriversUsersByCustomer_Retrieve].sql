
USE [ECOsystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversUsersByCustomer_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriversUsersByCustomer_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_DriversUsersByCustomer_Retrieve]    Script Date: 11/18/2015 2:34:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

	-- ================================================================================================
	-- Author:		Danilo Hidalgo G.
	-- Create date: 04/11/2015
	-- Description:	Retrieve Customer User information by CustomerId
	-- ================================================================================================
	CREATE PROCEDURE [General].[Sp_DriversUsersByCustomer_Retrieve]
	(
		  @pCustomerId INT
	)
	AS
	BEGIN

		SET NOCOUNT ON
	
		SELECT
			  a.[UserId]
			 ,a.[DriversUserId]
			 ,a.[CustomerId]
			 ,a.[Identification] AS [EncryptedIdentification]
			 ,a.[Code] AS [EncryptedCode]
			 ,a.[License] AS [EncryptedLicense]
			 ,a.[LicenseExpiration]
			 ,a.[Dallas] AS EncryptedDallas
			 ,b.[CountryId]
			 ,a.[DailyTransactionLimit]
			 ,a.[CardRequestId]
			 ,NULL AS [DallasId]
		FROM [General].[DriversUsers] a
			INNER JOIN [General].[Customers] b
				ON b.[CustomerId] = a.[CustomerId]		
		WHERE a.[CustomerId] = @pCustomerId
	
		SET NOCOUNT OFF
	END




GO


