USE [ECOSystem]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsSAPReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsSAPReport_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 2/2/2016
-- Description:	Retrieve TransactionsSAPReport information
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_TransactionsSAPReport_Retrieve]
(
	@pCustomerId INT,					--@pCustomerId: CustomerId		
	@pYear INT = NULL,					--@pYear: Year
	@pMonth INT = NULL,					--@pMonth: Month
	@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
	@pEndDate DATETIME = NULL			--@pEndDate: End Date	
)
AS
BEGIN

	SET NOCOUNT ON	

	SELECT 		
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'DEBE' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [Debe],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'HABER' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [Haber],
		DATEADD(HOUR, -6, t.[InsertDate]) [DocumentDate],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'CLASE_DOCUMENTO' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [DocumentClass],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'SOCIEDAD' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [Sociedad],
		DATEADD(HOUR, -6, GETDATE()) [CountDate],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'MONEDA' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [Currency],
		t.[TransactionPOS] AS [Reference],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'CABECERA_CARGA' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [TextoCabecera],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'DEBE' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [ClaveContabilizacionDebe],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'HABER' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [ClaveContabilizacionHaber],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'CUENTA_DEBE' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [Account],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'IND_CME' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [IND_CME],
		t.[FuelAmount] [Importe],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'INDICADOR_IMPUESTO' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [IndicadorImpuesto],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'CALC_IMPUESTO' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [CalcImpuesto],
		DATEADD(HOUR, -6, t.[InsertDate]) [FechaValor],
		ISNULL(t.[Invoice], '-') AS [Asignacion],
		CONCAT('V:', v.[Name],';P:',v.[PlateId],';L:',t.[Odometer],';K:',t.[Liters]) [Texto],		
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'BANCO_PROPIO' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [BancoPropio],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'VIA_PAGO_DEBE' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [ViaPagoDebe],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'VIA_PAGO_HABER' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [ViaPagoHaber],
		e.[Code] AS [CentroCosto],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'CENTRO_BENEFICIO' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [CentroBeneficio],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'ORDEN' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [Orden],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'PEP' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [PEP],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'POSICION_PRESUPUEST' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [PosicionPresupuesto],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'CENTRO_GESTOR' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [CentroGestor],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'PROGRAMA_PRESUPUEST' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [ProgramaPresupuesto],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'FONDO' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [Fondo],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'CENTRO' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [Centro],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'BLOQUEA_PAGO_DEBE' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [BloqueaPagoDebe],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'BLOQUEA_PAGO_HABER' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [BloqueaPagoHaber],
		CONVERT(VARCHAR(50), ISNULL((SELECT ss.[SAPProv] FROM [General].[ServiceStations] AS ss
		                  WHERE ss.[BacId] IN (SELECT tp.[BacId] FROM [dbo].[TransactionsSAPServiceStationsTemp] AS tp 
														         WHERE t.[ProcessorId] = tp.[TerminalId] )), '')) AS [SAPProv],
		(SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterId] = 'PAGADOR_ALTERNATIVO' AND [Description] = 'Reporte Concolidado SAP Dos Pinos') [PagadorAlternativo],
		cc.[CreditCardNumber]
	FROM 
		[Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v 
			ON t.[VehicleId] = v.[VehicleId]
		INNER JOIN [General].[Customers] c 
			ON v.[CustomerId] = c.[CustomerId]
		INNER JOIN [Control].[Fuels] f 
			ON t.[FuelId] = f.[FuelId]
		INNER JOIN [Control].[CreditCardByVehicle] cv 
			ON t.[CreditCardId] = cv.[CreditCardId]
		INNER JOIN [Control].[Currencies] g 
			ON c.[CurrencyId] = g.[CurrencyId]
		INNER JOIN [Control].[CreditCard] cc
			ON cc.CreditCardId = t.CreditCardId
		INNER JOIN [General].[VehicleCostCenters] e
		ON v.CostCenterId = e.CostCenterId
	WHERE	
	    c.[CustomerId] = @pCustomerId AND 
		1 > (
			SELECT ISNULL(COUNT(1), 0) FROM Control.Transactions t2
					WHERE t2.[CreditCardId] = t.[CreditCardId] AND
						t2.[TransactionPOS] = t.[TransactionPOS] AND
						t2.[ProcessorId] = t.[ProcessorId] AND 
						t2.IsReversed = 1
		)		
		AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
				AND DATEPART(m,DATEADD(HOUR, -6, t.[InsertDate])) = @pMonth
				AND DATEPART(yyyy,DATEADD(HOUR, -6, t.[InsertDate])) = @pYear) OR
			(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
					AND DATEADD(HOUR, -6, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))

	ORDER BY t.[InsertDate] DESC

	SET NOCOUNT OFF
END

GO
