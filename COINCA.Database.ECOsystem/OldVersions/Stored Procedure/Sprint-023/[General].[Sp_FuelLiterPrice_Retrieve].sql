USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_FuelPrice_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_FuelPrice_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 26/01/2015
-- Description:	Retrieve Fuel  Price information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_FuelPrice_Retrieve]
(
	 @pVehicleId INT
)
AS
BEGIN
	SET NOCOUNT ON	
	
	SELECT TOP 1  c.LiterPrice
	FROM 
		General.Vehicles a
		INNER JOIN General.CustomersByPartner b ON a.CustomerId = b.CustomerId
		INNER JOIN Control.PartnerFuel c ON b.PartnerId = c.PartnerId
		INNER JOIN General.VehicleCategories d ON a.VehicleCategoryId = d.VehicleCategoryId AND c.FuelId = d.DefaultFuelId
		INNER JOIN General.Partners e ON b.PartnerId = e.PartnerId
	WHERE 
		a.VehicleId = @pVehicleId AND 
		b.IsDefault = 1 AND 
		c.EndDate IS NULL

    SET NOCOUNT OFF
END

GO


