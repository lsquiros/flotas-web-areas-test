USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriversByVehicle_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 21/07/2015
-- Description:	Retrieve DriversByVehicle information
-- Modify by Henry Retana - Stefano Quiros - Returns the number of driver who have all the vehicles assigned
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DriversByVehicle_Retrieve]
(
	 @pVehicleId INT	--@pDriverId: PK of the table
	,@pCustomerId INT	--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	--If vehicle Id = -1 means that the query is asking for the drivers who have all the vehicles assigned
	IF @pVehicleId = -1 
	BEGIN
			DECLARE @VehicleCount INT

			SET @VehicleCount = (SELECT COUNT(1) FROM [General].[Vehicles] v 
										WHERE v.[CustomerId] = @pCustomerId AND (v.[IsDeleted] = 0 or v.[IsDeleted] is null))

			SELECT  COUNT(vd.[VehicleId]) AS [VehicleId],
					d.[UserId] AS [UserId],
					u.[Name] AS [DriverName],
					d.[Identification]
			FROM  [General].[DriversUsers] d
			INNER JOIN [General].[VehiclesDrivers] vd
				ON vd.[UserId] = d.[UserId]
			INNER JOIN [General].[Users] u 
				ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
			WHERE  d.[CustomerId] = @pCustomerId 
			GROUP BY d.[UserId],
						u.[Name],
						d.[Identification]
			HAVING COUNT(vd.[VehicleId]) = @VehicleCount
			UNION
			SELECT  NULL AS [VehicleId],
					d.[UserId] AS [UserId],
					u.[Name] AS [DriverName],
					d.[Identification]
			FROM  [General].[DriversUsers] d
			LEFT OUTER JOIN [General].[VehiclesDrivers] vd
				ON vd.[UserId] = d.[UserId]
			INNER JOIN [General].[Users] u 
				ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
			WHERE  d.[CustomerId] = @pCustomerId 
			GROUP BY d.[UserId],
						u.[Name],
						d.[Identification]
			HAVING COUNT(vd.[VehicleId]) <> @VehicleCount
			
	END
	ELSE
	BEGIN
			SELECT  -- CONDUCTORES SIN ASIGNAR
				NULL AS [VehicleId],
				d.[UserId] AS [UserId],
				u.[Name] AS [DriverName],
				d.[Identification]
			FROM  [General].[DriversUsers] d
			LEFT JOIN [General].[VehiclesDrivers] vd
				ON vd.[UserId] = d.[UserId]
			INNER JOIN [General].[Users] u 
				ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1  AND u.[IsDeleted] = 0
			WHERE  vd.[UserId] IS NULL AND d.[CustomerId] = @pCustomerId

			UNION

			SELECT   -- CONDUCTORES YA ASIGNADOS A OTROS VEHICULOS
				NULL AS [VehicleId],
				d.[UserId] AS [UserId],
				u.[Name] AS [DriverName],
				d.[Identification]
			FROM  [General].[DriversUsers] d
			INNER JOIN [General].[VehiclesDrivers] vd
				ON vd.[UserId] = d.[UserId]
			INNER JOIN [General].[Users] u 
				ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
			WHERE  vd.[VehicleId] <> @pVehicleId AND d.[CustomerId] = @pCustomerId AND
				   vd.[UserId] NOT IN (
						SELECT 
							d.[UserId] AS [UserId]
						FROM  [General].[DriversUsers] d
						INNER JOIN [General].[VehiclesDrivers] vd
							ON vd.[UserId] = d.[UserId]
						INNER JOIN [General].[Users] u 
							ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
						WHERE  vd.[VehicleId] = @pVehicleId AND d.[CustomerId] = @pCustomerId
				   ) 
			UNION

			SELECT  -- CONDUCTORES ASIGNADOS AL VEHICULO CONSULTADO
				vd.[VehicleId] AS [VehicleId],
				d.[UserId] AS [UserId],
				u.[Name] AS [DriverName],
				d.[Identification]
			FROM  [General].[DriversUsers] d
			INNER JOIN [General].[VehiclesDrivers] vd
				ON vd.[UserId] = d.[UserId]
			INNER JOIN [General].[Users] u 
				ON u.[UserId] = d.[UserId] AND u.[IsActive] = 1 AND u.[IsDeleted] = 0
			WHERE  vd.[VehicleId] = @pVehicleId AND d.[CustomerId] = @pCustomerId
	END

    SET NOCOUNT OFF
END
GO


