USE [ECOsystem]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ParameterSAPReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ParameterSAPReport_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 2/2/2016
-- Description:	Retrieve Parameters for SAP Report
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_ParameterSAPReport_Retrieve]

AS
BEGIN

	SET NOCOUNT ON

	DECLARE @ParametersValuesTable Table (  Debe VARCHAR(800),
											Haber VARCHAR(800),
											BancoPropio VARCHAR(800),
											BloqueoPagoHaber VARCHAR(800),
											CabeceraCarga VARCHAR(800),
											CalcImpuesto VARCHAR(800),
											Centro VARCHAR(800),
											CentroBeneficio VARCHAR(800),
											CentroCosto VARCHAR(800),
											CentroGestor VARCHAR(800),
											ClaseDocumento VARCHAR(800),
											CuentaDebe VARCHAR(800),	
											Fondo VARCHAR(800),
											IndCme VARCHAR(800),
											IndicadorImpuesto VARCHAR(800),
											Moneda VARCHAR(800),
											Orden VARCHAR(800),
											Pep VARCHAR(800),
											PosicionPresupuesto VARCHAR(800),
											ProgramaPresupuesto VARCHAR(800),
											Sociedad VARCHAR(800),
											ViaPagoHaber VARCHAR(800))

	INSERT INTO @ParametersValuesTable (Debe)	SELECT [Value] FROM [dbo].[GeneralParameters] 
														WHERE [ParameterId] = 'DEBE' 
														AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE @ParametersValuesTable SET Haber =	(SELECT [Value] FROM [dbo].[GeneralParameters] 
												 WHERE [ParameterId] = 'HABER' 
												 AND [Description] =  'Reporte Concolidado SAP Dos Pinos')
	
	UPDATE @ParametersValuesTable SET BancoPropio =	(SELECT [Value] FROM [dbo].[GeneralParameters] 
													 WHERE [ParameterId] = 'BANCO_PROPIO' 
													 AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET BloqueoPagoHaber =  (SELECT [Value] FROM [dbo].[GeneralParameters] 
														   WHERE [ParameterId] = 'BLOQUEA_PAGO_HABER' 
														   AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

	UPDATE @ParametersValuesTable SET CabeceraCarga  =  (SELECT [Value] FROM [dbo].[GeneralParameters] 
														 WHERE [ParameterId] = 'CABECERA_CARGA' 
														 AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET CalcImpuesto  = (SELECT [Value] FROM [dbo].[GeneralParameters] 
													   WHERE [ParameterId] = 'CALC_IMPUESTO' 
													   AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET Centro =  (SELECT [Value] FROM [dbo].[GeneralParameters] 
												 WHERE [ParameterId] = 'CENTRO' 
												 AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET CentroBeneficio =  (SELECT [Value] FROM [dbo].[GeneralParameters] 
														  WHERE [ParameterId] = 'CENTRO_BENEFICIO' 
														  AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET CentroCosto  = (SELECT [Value] FROM [dbo].[GeneralParameters] 
													  WHERE [ParameterId] = 'CENTRO_COSTO' 
													  AND [Description] =  'Reporte Concolidado SAP Dos Pinos')


    UPDATE @ParametersValuesTable SET CentroGestor  =  (SELECT [Value] FROM [dbo].[GeneralParameters] 
													    WHERE [ParameterId] = 'CENTRO_GESTOR' 
													    AND [Description] =  'Reporte Concolidado SAP Dos Pinos')


    UPDATE @ParametersValuesTable SET ClaseDocumento =  (SELECT [Value] FROM [dbo].[GeneralParameters] 
														 WHERE [ParameterId] = 'CLASE_DOCUMENTO' 
														 AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET CuentaDebe  =  (SELECT [Value] FROM [dbo].[GeneralParameters] 
													  WHERE [ParameterId] = 'CUENTA_DEBE' 
													  AND [Description] =  'Reporte Concolidado SAP Dos Pinos')	

    UPDATE @ParametersValuesTable SET Fondo =  (SELECT [Value] FROM [dbo].[GeneralParameters] 
												WHERE [ParameterId] = 'FONDO' 
												AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET IndCme =  (SELECT [Value] FROM [dbo].[GeneralParameters] 
												 WHERE [ParameterId] = 'IND_CME' 
												 AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET IndicadorImpuesto =    (SELECT [Value] FROM [dbo].[GeneralParameters] 
														      WHERE [ParameterId] = 'INDICADOR_IMPUESTO' 
															  AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET Moneda =  (SELECT [Value] FROM [dbo].[GeneralParameters] 
												 WHERE [ParameterId] = 'MONEDA' 
												 AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET Orden =   (SELECT [Value] FROM [dbo].[GeneralParameters] 
											   	 WHERE [ParameterId] = 'ORDEN' 
												 AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET Pep =   (SELECT [Value] FROM [dbo].[GeneralParameters] 
											   WHERE [ParameterId] = 'PEP' 
											   AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET PosicionPresupuesto =   (SELECT [Value] FROM [dbo].[GeneralParameters] 
														       WHERE [ParameterId] = 'POSICION_PRESUPUEST' 
															   AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET ProgramaPresupuesto =   (SELECT [Value] FROM [dbo].[GeneralParameters] 
														       WHERE [ParameterId] = 'PROGRAMA_PRESUPUEST' 
															   AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET Sociedad =   (SELECT [Value] FROM [dbo].[GeneralParameters] 
												    WHERE [ParameterId] = 'SOCIEDAD' 
												    AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

    UPDATE @ParametersValuesTable SET ViaPagoHaber =   (SELECT [Value] FROM [dbo].[GeneralParameters] 
													    WHERE [ParameterId] = 'VIA_PAGO_HABER' 
													    AND [Description] =  'Reporte Concolidado SAP Dos Pinos')

     --****************************************************************************************************************

    SELECT	 Debe
	        ,Haber
	        ,BancoPropio
			,BloqueoPagoHaber
			,CabeceraCarga AS [TextoCabecera]
			,CalcImpuesto
			,Centro
			,CentroBeneficio
			,CentroCosto
			,CentroGestor
			,ClaseDocumento AS [DocumentClass]
			,CuentaDebe AS [Account]
			,Fondo
			,IndCme AS [IND_CME]
			,IndicadorImpuesto
			,Moneda AS [Currency]
			,Orden
			,Pep AS [PEP]
			,PosicionPresupuesto
			,ProgramaPresupuesto
			,Sociedad
			,ViaPagoHaber
	FROM @ParametersValuesTable
	
	SET NOCOUNT OFF
END

GO