USE [ECOsystem]
GO

/****** Object:  StoredProcedure [dbo].[Sp_ASPNetRolesByMenu_Retrieve]    Script Date: 2/1/2016 11:36:56 AM ******/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetRolesByMenu_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetRolesByMenu_Retrieve]
GO

/****** Object:  StoredProcedure [dbo].[Sp_ASPNetRolesByMenu_Retrieve]    Script Date: 2/1/2016 11:36:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 23/12/2015
-- Description:	Roles Retrieve by Menu
-- Vehicle Filter - Feb/01/2016 - Melvin Salas
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ASPNetRolesByMenu_Retrieve]
(    
	@pRoleName varchar(50) = null,
	@pCreatedBy int	
)	
AS
BEGIN	
	SET NOCOUNT ON;

	DECLARE @SuperIds TABLE (id int)
	DECLARE @RoleParent VARCHAR(128)
	DECLARE @Role VARCHAR(50)

	-- Get customer id in same company
	INSERT INTO @SuperIds SELECT		DISTINCT u.userid 
						  FROM			general.users u
			  			  INNER JOIN	aspnetroles r ON u.userid = r.createdby
				  		  WHERE			u.UserId IN (SELECT	c.Userid
													 FROM	general.customerusers c
													 WHERE	c.Customerid IN (SELECT	cr.customerid 
																			 FROM	general.customerusers cr 
																			 WHERE cr.userid = @pCreatedBy))     
									    AND r.createdby IN (SELECT	c.userid 
															FROM	general.customerusers c
															WHERE	c.customerid = (SELECT	t.customerid 
																					FROM	general.customerusers t 
																					WHERE	t.userid = @pCreatedBy))

	-- Get partner id of the company
	INSERT INTO @SuperIds	SELECT		DISTINCT u.userid 
							FROM		general.users u
							INNER JOIN	aspnetroles r ON u.userid = r.createdby
							WHERE		u.UserId IN (SELECT c.Userid
													 FROM general.partnerusers c
													 WHERE c.PartnerId IN (SELECT cr.PartnerId 
																		   FROM general.partnerusers cr 
																		   WHERE cr.userid = @pCreatedBy))
	
	--Verifies if the role asociated with the user is a master role, if it is gets the information about the role to retrieve all the roles
	IF NOT EXISTS (SELECT TOP 1 * FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName AND [Master] = 1) 
	BEGIN		
		SET @RoleParent = (SELECT [Parent] FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName)
		SET @Role = (SELECT [Name] FROM [dbo].[AspNetRoles] WHERE [Id] = @RoleParent)
	END	
	ELSE
	BEGIN
		SET @RoleParent = (SELECT [Id] FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName)
		SET @Role = (SELECT [Name] FROM [dbo].[AspNetRoles] WHERE [Id] = @RoleParent)
	END

	IF @Role = 'SUPER_ADMIN'
	BEGIN			
		SELECT [Id] as [RoleId], [Name] 
		FROM [dbo].[AspNetRoles] 
		WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END 

	IF @Role = 'SUPER_USER'
	BEGIN			
		SELECT [Id] as [RoleId], [Name] FROM [dbo].[AspNetRoles] WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END 

	IF @Role = 'PARTNER_ADMIN'
	BEGIN		
		SELECT [dbo].[AspNetRoles].[Id] as [RoleId], [Name] 
		FROM [dbo].[AspNetRoles] 
		WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END	

	IF @Role = 'PARTNER_USER'
	BEGIN		
		SELECT [dbo].[AspNetRoles].[Id] as [RoleId], [Name] 
		FROM [dbo].[AspNetRoles] 
		WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END	

	IF @Role = 'CUSTOMER_ADMIN'
	BEGIN		
		SELECT [dbo].[AspNetRoles].[Id] as [RoleId], [Name] 
		FROM [dbo].[AspNetRoles] 
		JOIN @SuperIds AS superids ON [dbo].[AspNetRoles].[CreatedBy] = superids.id
		WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END	

	IF @Role = 'CUSTOMER_USER'
	BEGIN		
		SELECT [dbo].[AspNetRoles].[Id] as [RoleId], [Name] 
		FROM [dbo].[AspNetRoles] 
		JOIN @SuperIds AS superids ON [dbo].[AspNetRoles].[CreatedBy] = superids.id
		WHERE [Active] = 1 AND ([Parent] = @RoleParent OR [CreatedBy] = @pCreatedBy)
		ORDER BY [Name]
	END	
		
	SET NOCOUNT OFF
END


GO


