USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversByVehicle_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriversByVehicle_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 21/07/2015
-- Description:	Insert or Update DriversByVehicle information
-- Modify by Henry Retana - Stefano Quiros, modifies the way the drivers by vehicle are being save
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DriversByVehicle_AddOrEdit]
(
	 @pVehicleId INT			--@pVehicleId: PK of the table
	,@pXmlData VARCHAR(MAX)		--@pName: Drivers Name
	,@pLoggedUserId INT			--@pLoggedUserId: Logged UserId that executes the operation
	,@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            DECLARE @lxmlData XML = CONVERT(XML,@pXmlData)
               
			DECLARE @DriverUsers TABLE ([UserId] INT)
			

			INSERT INTO @DriverUsers
			SELECT [UserId] FROM [General].[DriversUsers] 
			WHERE [CustomerId] = @pCustomerId

			--IF THEY ARE USING ALL TO ALL 
			IF @pVehicleId = -1
			BEGIN				

				IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
					TRUNCATE TABLE #TEMP
				ELSE
				BEGIN
					CREATE TABLE #TEMP ([UserId] INT)
				END
				

				INSERT INTO #TEMP
				SELECT Row.col.value('./@UserId', 'INT') FROM @lxmlData.nodes('/xmldata/Driver') Row(col)

				DECLARE @COUNTUSERS INT
				DECLARE @USERSTABLETEMP TABLE ( [UserId] INT	)				

				SET @COUNTUSERS = (SELECT Count(gvd.UserId) FROM [General].[VehiclesDrivers] gvd 
															INNER JOIN @DriverUsers du
															ON gvd.UserId = du.UserId
															LEFT OUTER JOIN #TEMP usid
															ON gvd.UserId = usid.UserId
															WHERE usid.UserId IS NULL)

				IF @COUNTUSERS = 0	
				BEGIN
					SET @COUNTUSERS = (SELECT Count(usid.UserId) FROM #TEMP usid 
																 INNER JOIN @DriverUsers du
																 ON usid.UserId = du.UserId
																 LEFT OUTER JOIN [General].[VehiclesDrivers] VD
																 ON VD.UserId = usid.UserId
																 WHERE VD.UserId IS NULL)

					IF @COUNTUSERS > 0
					BEGIN

						INSERT INTO @USERSTABLETEMP
						SELECT usid.UserId FROM #TEMP usid 
										   INNER JOIN @DriverUsers du
										   ON usid.UserId = du.UserId
										   LEFT OUTER JOIN [General].[VehiclesDrivers] VD
										   ON VD.UserId = usid.UserId
										   WHERE VD.UserId IS NULL
						

						DECLARE @VehIds TABLE ( [VehicleId] int	)						

						INSERT INTO @VehIds
						SELECT v.[VehicleId]
								FROM   [General].[Vehicles] v		
								WHERE  	v.[CustomerId] = @pCustomerId 
										AND (v.isdeleted = 0 OR v.isdeleted IS NULL)

						INSERT INTO [General].[VehiclesDrivers]
										([VehicleId]
										,[UserId]
										,[InsertDate]
										,[InsertUserId])
						SELECT  vid.[VehicleId],
								utt.UserId,
								GETUTCDATE(),
								@pLoggedUserId
						FROM @VehIds vid CROSS JOIN @USERSTABLETEMP utt

					END
				END
				ELSE
				BEGIN
					DECLARE @counter INT
					DECLARE @Batch INT = 1

					IF ((Select Count(1) FROM #TEMP) = 0)		
					BEGIN	

						DELETE FROM [General].[VehiclesDrivers]
							   FROM [General].[VehiclesDrivers] VD
						       LEFT OUTER JOIN @DriverUsers DU
							   ON VD.UserId = DU.UserId
							   WHERE DU.UserId IS NOT NULL						
						
					END
					ELSE
					BEGIN 					

						INSERT INTO @USERSTABLETEMP
						SELECT gvd.UserId FROM [General].[VehiclesDrivers] gvd
										  INNER JOIN @DriverUsers du
										  ON gvd.UserId = du.UserId
										  LEFT OUTER JOIN #TEMP usid
										  ON gvd.UserId = usid.UserId
										  WHERE usid.UserId IS NULL	
					
						SET @counter = (SELECT COUNT(VD.UserId) FROM [General].[VehiclesDrivers] VD
																LEFT OUTER JOIN @USERSTABLETEMP utt
																ON utt.UserId = VD.UserId 
																WHERE utt.UserId IS NOT NULL)														    
					
						WHILE(@counter > 0 )
						BEGIN
							DELETE TOP(@Batch) FROM [General].[VehiclesDrivers]
											   FROM [General].[VehiclesDrivers] VD
											   LEFT OUTER JOIN @USERSTABLETEMP utt
											   ON VD.UserId = utt.UserId
									    	   WHERE utt.UserId IS NOT NULL	

							SET @counter = (SELECT COUNT(VD.UserId) FROM [General].[VehiclesDrivers] VD
																	LEFT OUTER JOIN @USERSTABLETEMP utt
																	ON utt.UserId = VD.UserId 
																	WHERE utt.UserId IS NOT NULL)	
						END
					END
					
				END
	
			END
			ELSE
			BEGIN
			
				DELETE [General].[VehiclesDrivers]
				WHERE [VehicleId] = @pVehicleId
			
				INSERT INTO [General].[VehiclesDrivers]
						([VehicleId]
						,[UserId]
						,[InsertDate]
						,[InsertUserId])
				SELECT 
						 @pVehicleId
						,Row.col.value('./@UserId', 'INT') AS [UserId]
						,GETUTCDATE()
						,@pLoggedUserId
				FROM @lxmlData.nodes('/xmldata/Driver') Row(col)
				ORDER BY Row.col.value('./@Index', 'INT')

			END	
			
    END TRY
    BEGIN CATCH		
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO


