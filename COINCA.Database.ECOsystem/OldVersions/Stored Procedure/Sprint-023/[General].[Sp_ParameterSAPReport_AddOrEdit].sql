USE [ECOsystem]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ParameterSAPReport_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ParameterSAPReport_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 2/2/2016
-- Description:	Edit Parameters for SAP Report
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_ParameterSAPReport_AddOrEdit]
(
	@pDebe VARCHAR(800) = NULL,
	@pHaber VARCHAR(800) = NULL,
	@pBancoPropio VARCHAR(800) = NULL,
	@pBloqueoPagoHaber VARCHAR(800) = NULL,
	@pCabeceraCarga VARCHAR(800) = NULL,
	@pCalcImpuesto VARCHAR(800) = NULL,
	@pCentro VARCHAR(800) = NULL,
	@pCentroBeneficio VARCHAR(800) = NULL,
	@pCentroCosto VARCHAR(800) = NULL,
	@pCentroGestor VARCHAR(800) = NULL,
	@pClaseDocumento VARCHAR(800) = NULL,
	@pCuentaDebe VARCHAR(800) = NULL,	
	@pFondo VARCHAR(800) = NULL,
	@pIndCme VARCHAR(800) = NULL,
	@pIndicadorImpuesto VARCHAR(800) = NULL,
	@pMoneda VARCHAR(800) = NULL,
	@pOrden VARCHAR(800) = NULL,
	@pPep VARCHAR(800) = NULL,
	@pPosicionPresupuesto VARCHAR(800) = NULL,
	@pProgramaPresupuesto VARCHAR(800) = NULL,
	@pSociedad VARCHAR(800) = NULL,
	@pViaPagoHaber VARCHAR(800) = NULL

)
AS
BEGIN

	SET NOCOUNT ON

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pDebe, '') 
	WHERE [ParameterId] = 'DEBE' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pHaber, '') 
	WHERE [ParameterId] = 'HABER' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pBancoPropio, '') 
	WHERE [ParameterId] = 'BANCO_PROPIO' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pBloqueoPagoHaber, '') 
	WHERE [ParameterId] = 'BLOQUEA_PAGO_HABER' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pCabeceraCarga, '')
	WHERE [ParameterId] = 'CABECERA_CARGA' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pCalcImpuesto, '') 
	WHERE [ParameterId] = 'CALC_IMPUESTO' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pCentro, '')
	WHERE [ParameterId] = 'CENTRO' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pCentroBeneficio, '') 
	WHERE [ParameterId] = 'CENTRO_BENEFICIO' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pCentroCosto, '') 
	WHERE [ParameterId] = 'CENTRO_COSTO' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pCentroGestor, '') 
	WHERE [ParameterId] = 'CENTRO_GESTOR' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pClaseDocumento, '') 
	WHERE [ParameterId] = 'CLASE_DOCUMENTO' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pCuentaDebe, '') 
	WHERE [ParameterId] = 'CUENTA_DEBE' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pFondo, '') 
	WHERE [ParameterId] = 'FONDO' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pClaseDocumento, '') 
	WHERE [ParameterId] = 'CLASE_DOCUMENTO' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pIndCme, '') 
	WHERE [ParameterId] = 'IND_CME' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pIndicadorImpuesto, '') 
	WHERE [ParameterId] = 'INDICADOR_IMPUESTO' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pMoneda, '') 
	WHERE [ParameterId] = 'MONEDA' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pOrden, '') 
	WHERE [ParameterId] = 'ORDEN' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pPep, '') 
	WHERE [ParameterId] = 'PEP' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pPosicionPresupuesto, '') 
	WHERE [ParameterId] = 'POSICION_PRESUPUEST' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pProgramaPresupuesto, '') 
	WHERE [ParameterId] = 'PROGRAMA_PRESUPUEST' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pSociedad, '') 
	WHERE [ParameterId] = 'SOCIEDAD' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	UPDATE [dbo].[GeneralParameters]
	SET [Value] = ISNULL(@pViaPagoHaber, '') 
	WHERE [ParameterId] = 'VIA_PAGO_HABER' AND [Description] =  'Reporte Concolidado SAP Dos Pinos'

	SET NOCOUNT OFF
END

GO