USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ValidateRangeTimeSlotByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ValidateRangeTimeSlotByVehicle_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 26/01/2015
-- Description:	Retrieve ValidateRangeVehicleTimeSlot information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ValidateRangeTimeSlotByVehicle_Retrieve]
(
	 @pVehicleId INT
)
AS
BEGIN
	SET NOCOUNT ON	
	
	DECLARE @Date datetime = DATEADD(HOUR, -6, CONVERT(DATETIME, GETUTCDATE()))
	DECLARE @Time TIME = DATEADD(HOUR, -6, CONVERT(TIME, GETUTCDATE()))
	DECLARE @Day int = (DATEPART(dw, @Date)) - 1
	DECLARE @lxmlData xml
	DECLARE @MINTime VARCHAR(10)
	DECLARE @MAXTime VARCHAR(10)
	DECLARE @ReturnDay INT

	select @lxmlData = XmlTimeSlot from General.TimeSlotByVehicle where VehicleId = @pVehicleId

	SET @MINTime = (SELECT TOP 1
		MIN(RowData.col.value('./@Time', 'VARCHAR(5)')) AS [Time]
	FROM @lxmlData.nodes('/xmldata/TimeSlot') RowData(col)
	WHERE RowData.col.value('./@Day', 'INT')  = @Day)											
	
	SET @MAXTime = (SELECT TOP 1
		MAX(RowData.col.value('./@Time', 'VARCHAR(5)')) AS [Time]
	FROM @lxmlData.nodes('/xmldata/TimeSlot') RowData(col)
	WHERE RowData.col.value('./@Day', 'INT')  = @Day)		

	SET @ReturnDay = @Day

	IF @MINTime IS NULL AND @MAXTime IS NULL
	BEGIN 
		
		SET @MINTime = (SELECT TOP 1
			MIN(RowData.col.value('./@Time', 'VARCHAR(5)')) AS [Time]
		FROM @lxmlData.nodes('/xmldata/TimeSlot') RowData(col)
		WHERE RowData.col.value('./@Day', 'INT')  = (@Day + 1))											
	
		SET @MAXTime = (SELECT TOP 1
			MAX(RowData.col.value('./@Time', 'VARCHAR(5)')) AS [Time]
		FROM @lxmlData.nodes('/xmldata/TimeSlot') RowData(col)
		WHERE RowData.col.value('./@Day', 'INT')  = (@Day + 1))	

		SET @ReturnDay = (@Day + 1) 

	END 

	DECLARE @Results TABLE ([Time] VARCHAR(10), [Day] INT) 

	INSERT INTO @Results VALUES (@MINTime, @ReturnDay)
	INSERT INTO @Results VALUES (@MAXTime, @ReturnDay)

	SELECT [Time], [Day]  FROM @Results

    SET NOCOUNT OFF
END

GO


