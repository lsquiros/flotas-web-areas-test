USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceCost_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PreventiveMaintenanceCost_Delete]
GO

/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCost_Delete]    Script Date: 9/30/2015 10:02:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Danilo Hidalgo.
-- Create date: 01/05/2015
-- Description:	Delete Preventive Maintenance Cost information
-- =============================================
-- Modified By:	Kevin Pe�a
-- Create date: 25/09/2015
-- =============================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceCost_Delete]
	@pPreventiveMaintenanceCostId INT
AS
BEGIN	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		  
		DECLARE @TempItem TABLE
        (
         [PreventiveMaintenanceCatalogId] INT,
		 [Cost] DECIMAL(16,2)
        );
		
		INSERT INTO @TempItem 
		      SELECT [PreventiveMaintenanceCatalogId],[Cost]
	          FROM [General].[PreventiveMaintenanceCost]
		      WHERE [PreventiveMaintenanceCostId] = @pPreventiveMaintenanceCostId

	    DECLARE @TotalCost DECIMAL(16,2)
	
	    SELECT @TotalCost = [Cost] 
        FROM [General].[PreventiveMaintenanceCatalog]
        WHERE [PreventiveMaintenanceCatalogId] = (SELECT [PreventiveMaintenanceCatalogId] FROM @TempItem)

		DELETE [General].[PreventiveMaintenanceCost]
		WHERE [PreventiveMaintenanceCostId] = @pPreventiveMaintenanceCostId

		--UPDATE COST VALUE 
		UPDATE [General].[PreventiveMaintenanceCatalog]
		SET [Cost] = @TotalCost - (SELECT [Cost] FROM @TempItem) 
		WHERE [PreventiveMaintenanceCatalogId] = (SELECT [PreventiveMaintenanceCatalogId] FROM @TempItem)


		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
        
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO


