USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Countries_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Countries_AddOrEdit]
GO

/****** Object:  StoredProcedure [General].[Sp_Countries_AddOrEdit]    Script Date: 10/2/2015 12:13:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/09/2014
-- Description:	Insert or Update country information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Countries_AddOrEdit]
(
	 @pCountryId INT = NULL					--
	,@pName VARCHAR(250)
	,@pCurrencyId INT
	,@pCode CHAR(3)
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
	,@pGeopoliticalLevel1 VARCHAR(50)
	,@pGeopoliticalLevel2 VARCHAR(50)
	,@pGeopoliticalLevel3 VARCHAR(50)
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCountryId IS NULL)
			BEGIN
				INSERT INTO [General].[Countries]
						([Name]
						,[CurrencyId]
						,[Code]
						,[InsertDate]
						,[InsertUserId]
						,[GeopoliticalLevel1]
						,[GeopoliticalLevel2]
						,[GeopoliticalLevel3])
				VALUES	(@pName
						,@pCurrencyId
						,UPPER(@pCode)
						,GETUTCDATE()
						,@pLoggedUserId
						,@pGeopoliticalLevel1
						,@pGeopoliticalLevel2
						,@pGeopoliticalLevel3)
			END
			ELSE
			BEGIN
				UPDATE [General].[Countries]
					SET  [Name] = @pName
						,[CurrencyId] = @pCurrencyId
						,[Code] = UPPER(@pCode)
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
						,[GeopoliticalLevel1] = @pGeopoliticalLevel1
						,[GeopoliticalLevel2] = @pGeopoliticalLevel2
						,[GeopoliticalLevel3] = @pGeopoliticalLevel3
				WHERE [CountryId] = @pCountryId
				  AND [RowVersion] = @pRowVersion
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


