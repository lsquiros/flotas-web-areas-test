USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardBins_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardBins_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_Countries_Retrieve]    Script Date: 10/2/2015 11:45:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 05/10/2015
-- Description:	Retrieve credit card bins information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardBins_Retrieve]
(
	 @pPartnerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
		
	SELECT
		 ccb.[BinId],
		 ccb.[PartnerId],
		 p.[Name] AS [PartnerName],
		 ccb.[CardType],
		 ccb.[BinData] AS [BindDataEncrypted]
    FROM [Control].[CreditCardBin] ccb
	INNER JOIN [General].[Partners] p 
		ON p.PartnerId = ccb.[PartnerId]
	WHERE 
		 ccb.[PartnerId] = @pPartnerId

    SET NOCOUNT OFF
END

GO