USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardNumbers_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardNumbers_Add]
GO

/****** Object:  StoredProcedure [General].[Sp_Countries_AddOrEdit]    Script Date: 10/2/2015 12:13:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 06/10/2015
-- Description:	Insert Credit Card Numbers information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardNumbers_Add]
(
	 @pPartnerId INT
	,@pCreditCardType CHAR(1)
	,@pCreditCardNumber nvarchar(520)
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
				
				INSERT INTO [Control].[CreditCardNumbers]
					   ([CreditCardNumber]
					   ,[IsUsed]
					   ,[IsReserved]
					   ,[CreditCardType]
					   ,[PartnerId]
					   ,[InsertDate]
					   ,[InsertUserId]
					   ,[ModifyDate]
					   ,[ModifyUserId])
				 VALUES
					   (@pCreditCardNumber
					   ,0
					   ,0
					   ,@pCreditCardType
					   ,@pPartnerId
					   ,DATEADD(HOUR, -6, GETDATE())
					   ,0
					   ,DATEADD(HOUR, -6, GETDATE())
					   ,0)

            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


