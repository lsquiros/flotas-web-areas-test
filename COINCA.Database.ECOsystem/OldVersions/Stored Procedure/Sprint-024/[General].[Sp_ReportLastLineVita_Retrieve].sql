USE [ECOsystemDev]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ReportLastLineVita_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ReportLastLineVita_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/02/2016
-- Description:	Retrieve Report Last LineVita ECO
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ReportLastLineVita_Retrieve]
(
	@pPlateId VARCHAR(MAX)
)
AS
BEGIN
	
	SELECT rl.[GPSDateTime]
		   ,rl.[Longitude]
		   ,rl.[Latitude]
		   ,rl.[Odometer]
		   ,rl.[InputStatus]
		   ,rl.[OutputStatus]
	       ,rl.[VSSSpeed]
		   ,rl.[MainVolt]
		   ,rl.[BatVolt]
	FROM [dbo].[DispositivosAVL] av 
	INNER JOIN [dbo].[Composiciones] comp 
	   ON av.dispositivo = comp.dispositivo
	INNER JOIN [dbo].[Devices] d 
	   ON CAST(d.UnitID as varchar) = av.terminalID
	INNER JOIN [dbo].[Report_Last] rl 
	   ON rl.Device = d.Device
	INNER JOIN [dbo].[Vehiculos] vi 
	   ON vi.vehiculo = comp.vehiculo
	WHERE vi.placa = @pPlateId
	   AND d.Activo = 1

END
GO
