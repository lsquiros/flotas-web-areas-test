USE [ECOsystemDev]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_LineVitaRole_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_LineVitaRole_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 11/02/2016
-- Description:	Retrieve Role LineVita
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_LineVitaRole_Retrieve]
AS
BEGIN
	SELECT Value
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'LineVitaRole' AND [Property] = 'ROLE'
END
GO


