USE [ECOsystemDev]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleLineVita_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleLineVita_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_VehicleLineVita_Retrieve]    Script Date: 2/16/2016 4:37:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/02/2016
-- Description:	Retrieve VehicleList LineVita ECO
-- Modified by: Kevin Pe�a
-- Description:	Change Inner JOIN table
-- Modified by: Gerald Solano
-- Description:	Select default data from [General].[VehiclesByUser]
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleLineVita_Retrieve]
(
	@pUserId INT	
)
AS
BEGIN	
	
	DECLARE @Results INT = 0

	SELECT @Results = COUNT(1) FROM [General].[VehiclesDrivers] vd
	WHERE vd.[UserId] = @pUserId

	IF(@Results > 0)
	BEGIN
		--GET THE DATE ONE DAY BEFORE THE PARAMETER DATE
		SELECT v.[VehicleId]
			  ,v.[PlateId]
			  ,v.[Name]
		FROM [General].[Vehicles] v
		INNER JOIN [General].[VehiclesDrivers] vd
			ON v.[VehicleId] = vd.[VehicleId]
		WHERE vd.[UserId] = @pUserId
	END
	ELSE 
	BEGIN
		SELECT v.[VehicleId]
			  ,v.[PlateId]
			  ,v.[Name]
		FROM [General].[Vehicles] v
		INNER JOIN [General].[VehiclesByUser] vu
			ON v.[VehicleId] = vu.[VehicleId]
		WHERE vu.[UserId] = @pUserId AND vu.[LastDateDriving] IS NULL
	END

END

GO


