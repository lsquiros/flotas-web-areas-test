USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriversUsers_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriversUsers_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_DriversUsers_Retrieve]    Script Date: 8/28/2015 4:58:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Retrieve Customer User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DriversUsers_Retrieve]
(
	  @pUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT
		  a.[DriversUserId]
		 ,a.[CustomerId]
		 ,a.[Identification] AS [EncryptedIdentification]
		 ,a.[Code] AS [EncryptedCode]
		 ,a.[License] AS [EncryptedLicense]
		 ,a.[LicenseExpiration]
		 ,a.[Dallas] AS EncryptedDallas
		 ,b.[CountryId]
		 ,a.[DailyTransactionLimit]
		 ,a.[CardRequestId]
		 ,(SELECT TOP 1 dkd.[DallasId] 
			FROM [General].[DallasKeysByDriver] dkd 
			WHERE dkd.[UserId] = @pUserId) AS [DallasId]
    FROM [General].[DriversUsers] a
		INNER JOIN [General].[Customers] b
			ON b.[CustomerId] = a.[CustomerId]		
	WHERE a.[UserId] = @pUserId
	
    SET NOCOUNT OFF
END

GO


