USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetDrivingScoreFleetDataSourceReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Insurance].[Sp_GetDrivingScoreFleetDataSourceReport]
GO

/****** Object:  StoredProcedure [Insurance].[Sp_GetDrivingScoreFleetDataSourceReport]    Script Date: 9/2/2015 4:07:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Andr�s Oviedo
-- Create date: 18/02/2015
-- Description:	Retrieve Score Driving Fleet Data Source information
-- Modified by: Kevin Pe�a
-- date: 2/09/2015
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_GetDrivingScoreFleetDataSourceReport] 
( @pPartnerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)=null,		--Posibles valores S:Summarized, D:Detailed
  @pCustomerId int=null,
  @pScoreOpc VARCHAR(10) = null
)
AS BEGIN

DECLARE @tableCustomers TABLE (CustomerId int, Name varchar(250))
DECLARE @li_CustomerId INT
DECLARE @NameCustomer varchar(250)
DECLARE @tableDriversScore TABLE(
[CustomerId] int
,[CustomerName] varchar(250)
,[DriverScoreId] int
,[UserId] int
,[Name]  varchar (250)
,[Identification]  varchar(50)	
,[ScoreType] varchar(5)
,[Score] Float
,[Photo] varchar(max)
,[ScoreSpeedAdmin] Float
,[ScoreSpeedRoute] Float
,[KmTraveled] float
,[OverSpeed] float
,[OverSpeedAmount] float
,[OverSpeedDistancePercentage] float
,[OverSpeedAverage] float
,[OverSpeedWeihgtedAverage] float
,[StandardDeviation] float
,[Variance] float
,[VariationCoefficient] float,
 OverSpeedDistance float
)




	Declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT('0' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end
		
DECLARE @Dates TABLE(
	Id INT
	,d_month int
	,d_year	int
)


insert @Dates values(1,MONTH(@pInitialDate),YEAR(@pFinalDate))

INSERT INTO @tableCustomers
EXEC  [Insurance].[Sp_GetRosterInsurance]
		@pPartnerId,@pCustomerId
---SELECT * from @tableCustomers 

DECLARE @count INT=(select count(*) from @Dates)

WHILE @count>0
begin

Select @li_CustomerId = MIN(CustomerId)
from @tableCustomers

WHILE @li_CustomerId is not null
begin

declare @MONTH INT=(select d_month from @Dates where id=@count)
declare @YEAR INT=(select d_year from @Dates where id=@count)


INSERT   @tableDriversScore(
 [CustomerId]
,[CustomerName]
,[DriverScoreId]
,[UserId]
,[Name]
,[Identification]
,[ScoreType]
,[Score]
,[Photo] 
,[ScoreSpeedAdmin]
,[ScoreSpeedRoute]
,[KmTraveled] 
,[OverSpeed] 
,[OverSpeedAmount] 
,[OverSpeedDistancePercentage] 
,[OverSpeedAverage] 
,[OverSpeedWeihgtedAverage] 
,[StandardDeviation] 
,[Variance] 
,[VariationCoefficient], OverSpeedDistance )
EXEC [Operation].[Sp_GetGlobalScoresDataSourceByMonthOrDays]
		@li_CustomerId ,
		@pYear,			
		@pMonth,
		@pStartDate,
		@pEndDate,	
		@pReportType

-----------------------------
--Siguiente Customer
		Select @li_CustomerId = MIN(CustomerId)
		  from @tableCustomers 
		 where CustomerId > @li_CustomerId
END  -- End While
set @count=@count-1
end

IF(@pScoreOpc IS NOT NULL)
BEGIN

SELECT 
	[CustomerId],
	[CustomerName] [EncryptedCustomerName],
	[UserId],
	[Name] [EncryptedName],
	[Identification] [EncryptedIdentification],
	[ScoreType],
	[Score] [AverageScore],
	[KmTraveled],
	[OverSpeed],
	[OverSpeedAmount],
	[OverSpeedDistancePercentage],
	[OverSpeedAverage],
	[OverSpeedWeihgtedAverage],
	[StandardDeviation],
	[Variance],
	[VariationCoefficient],
	[OverSpeedDistance] 
FROM 
	@tableDriversScore 
	WHERE [ScoreType] = @pScoreOpc
GROUP BY
[CustomerId],
	[CustomerName],
	[UserId],
	[Name],
	[Identification],
	[ScoreType],
	[Score],
	[KmTraveled],
	[OverSpeed],
	[OverSpeedAmount],
	[OverSpeedDistancePercentage],
	[OverSpeedAverage],
	[OverSpeedWeihgtedAverage],
	[StandardDeviation],
	[Variance],
	[VariationCoefficient],
	[OverSpeedDistance]
ORDER BY 
	[AverageScore]

END
ELSE
 BEGIN

SELECT 
	[CustomerId],
	[CustomerName] [EncryptedCustomerName],
	[UserId],
	[Name] [EncryptedName],
	[Identification] [EncryptedIdentification],
	[ScoreType],
	[Score] [AverageScore],
	[KmTraveled],
	[OverSpeed],
	[OverSpeedAmount],
	[OverSpeedDistancePercentage],
	[OverSpeedAverage],
	[OverSpeedWeihgtedAverage],
	[StandardDeviation],
	[Variance],
	[VariationCoefficient],
	[OverSpeedDistance] 
FROM 
	@tableDriversScore 
GROUP BY
[CustomerId],
	[CustomerName],
	[UserId],
	[Name],
	[Identification],
	[ScoreType],
	[Score],
	[KmTraveled],
	[OverSpeed],
	[OverSpeedAmount],
	[OverSpeedDistancePercentage],
	[OverSpeedAverage],
	[OverSpeedWeihgtedAverage],
	[StandardDeviation],
	[Variance],
	[VariationCoefficient],
	[OverSpeedDistance]
ORDER BY 
	[AverageScore]

END 


END

GO


