USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DallasKeys_Remove]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DallasKeys_Remove]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 09/Septiembre/2015
-- Description:	Remove assign User with Dallas Key
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DallasKeys_Remove]
(   
	@pUserId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			--VALIDAMOS SI EL USER ID NO ES NULL //////////////////////
			IF(@pUserId IS NOT NULL)
			BEGIN
				
				DECLARE @DallasKeyId INT
				
				SELECT @DallasKeyId = [DallasId] 
				FROM [General].[DallasKeysByDriver]
				WHERE [UserId] = @pUserId

				IF(@DallasKeyId IS NOT NULL)
				BEGIN
					UPDATE [General].[DallasKeys]
					SET [IsUsed] = 0
					WHERE [DallasId] = @DallasKeyId

					DELETE FROM [General].[DallasKeysByDriver]
					WHERE [UserId] = @pUserId
				END

				SELECT 1 -- SUCCESS RETURN 1
			END
			ELSE
			BEGIN
				SELECT 0 -- DEFAULT RETURN 0
			END

			--//////////////////////////////////////////// 

            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (50001, 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO