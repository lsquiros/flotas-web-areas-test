USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EventLog_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_EventLog_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 26/Agosto/2015
-- Description:	Retrieve EventLog Information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_EventLog_Retrieve]
(   
	@pEventType VARCHAR(15) = NULL
    ,@pController VARCHAR(50) = NULL
    ,@pAction VARCHAR(50) = NULL
    ,@pIsError BIT = 0
    ,@pIsInfo BIT = 0
    ,@pIsWarning BIT = 0
    ,@pUserId INT = NULL
    ,@pCustomerId INT = NULL
    ,@pPartnerId INT = NULL
	,@pLogDateTime DATETIME = NULL
)
AS
BEGIN
	
	SELECT 
	   el.EventId,
	   el.EventType, 
	   el.UserId, 
	   el.CustomerId, 
	   el.PartnerId, 
	   el.Controller, 
	   el.[Action], 
	   el.Mensaje, 
	   el.IsInfo, 
	   el.IsWarning, 
	   el.IsError, 
	   el.LogDateTime 
	FROM General.EventLog el
	WHERE
		(@pEventType IS NULL OR el.EventType = @pEventType) AND
		(@pController IS NULL OR el.Controller = @pController) AND
		(@pAction IS NULL OR el.[Action] = @pAction) AND
		(@pIsError IS NULL OR el.IsError = @pIsError) AND
		(@pIsInfo IS NULL OR el.IsInfo = @pIsInfo) AND
		(@pIsWarning IS NULL OR el.IsWarning = @pIsWarning) AND
		(@pUserId IS NULL OR el.UserId = @pUserId) AND
		(@pCustomerId IS NULL OR el.CustomerId = @pCustomerId) AND
		(@pPartnerId IS NULL OR el.PartnerId = @pPartnerId) AND
		(@pLogDateTime IS NULL OR (el.LogDateTime >= @pLogDateTime AND el.LogDateTime <= @pLogDateTime))
END

GO


