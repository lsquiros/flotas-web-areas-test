USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DallasKeys_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DallasKeys_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 27/Agosto/2015
-- Description:	Add or Edit Dallas Keys maintenance
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DallasKeys_AddOrEdit]
(   
	@pDallasKey INT = NULL
    ,@pCustomerId INT = NULL
	,@pDallasCode VARCHAR(100) = NULL
	,@pIsDeleted BIT = 0
	,@pLogUserId INT = NULL
	,@pRowVersion TIMESTAMP
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			--VALIDAMOS SI ES UN UPDATE //////////////////////
			IF(@pDallasKey IS NOT NULL)
			BEGIN
				UPDATE [General].[DallasKeys]
				SET
					[DallasCode] = @pDallasCode,
					[IsDeleted]	= @pIsDeleted,
					[ModifyUserId] = @pLogUserId,	
					[ModifyDate] = DATEADD(hour, -6, GETDATE())	
				WHERE 
					[DallasId] = @pDallasKey --AND [RowVersion] = @pRowVersion		
			END
			ELSE
			BEGIN
				--INSERTAMOS UN NUEVO REGISTRO
				INSERT INTO [General].[DallasKeys]
					([CustomerId]
					,[DallasCode]
					,[InsertUserId])
				VALUES
					(@pCustomerId
					,@pDallasCode
					,@pLogUserId)
			END

			--//////////////////////////////////////////// 

            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (50001, 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO