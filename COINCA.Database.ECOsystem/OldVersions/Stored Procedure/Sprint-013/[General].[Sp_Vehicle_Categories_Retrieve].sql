USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCategories_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleCategories_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_VehicleCategories_Retrieve]    Script Date: 9/1/2015 4:04:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve VehicleCategory information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCategories_Retrieve]
(
	 @pVehicleCategoryId INT = NULL
	,@pKey VARCHAR(800) = NULL
	,@pCustomerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[VehicleCategoryId]
		,a.[Manufacturer]
		,a.[Type]
		,a.[DefaultFuelId]
		,a.[Liters]
		,a.[VehicleModel]
		,b.[Name] AS [FuelName]
		,a.[Icon]
		,a.[MaximumSpeed]
		,a.[MaximumRPM]
		,a.[Year]
		,a.[Weight]
		,a.[LoadType]
		,a.[CylinderCapacity]
		,a.[RowVersion]
		,a.[DefaultPerformance]
		,a.[SpeedDelimited]
		,(SELECT '[' + STUFF((
			SELECT 
				',{"FuelByVehicleCategoryId":' + FuelByVehicleCategoryId
				+ ',"VehicleCategoryId":' + CAST(VehicleCategoryId AS VARCHAR(MAX))
				+ ',"FuelId":' + CAST(FuelId  AS VARCHAR(MAX))
				+ ',"FuelName": "' + FuelName + '"'
				+ ',"IsFuelChecked":' + CAST(IsFuelChecked AS VARCHAR(MAX))
				+'}'

			FROM (SELECT
					 CAST(x.FuelByVehicleCategoryId  AS VARCHAR(MAX)) AS FuelByVehicleCategoryId
					,x.VehicleCategoryId
					,x.FuelId
					,y.Name AS FuelName
					,CONVERT(BIT, 1) AS IsFuelChecked 
				FROM [General].[FuelsByVehicleCategory] x
					INNER JOIN [Control].[Fuels] y
						ON y.FuelId = x.FuelId
				WHERE x.VehicleCategoryId = a.VehicleCategoryId
				UNION 
				SELECT
					 'null' AS FuelByVehicleCategoryId
					,a.VehicleCategoryId AS VehicleCategoryId
					,x.[FuelId]
					,x.Name AS FuelName
					,CONVERT(BIT, 0) AS IsFuelChecked 
				FROM [Control].[Fuels] x
					INNER JOIN [General].[Customers] y
						ON y.[CountryId] = x.[CountryId]
				WHERE NOT EXISTS (	SELECT 1 
									FROM [General].[FuelsByVehicleCategory] q
									WHERE q.[VehicleCategoryId] = a.VehicleCategoryId
									  AND q.[FuelId] = x.[FuelId])
				  AND y.CustomerId = a.CustomerId) t
			        
			FOR XML PATH(''), TYPE
		).value('.', 'varchar(max)'), 1, 1, '') + ']') AS JsonFuelsList
	FROM [General].[VehicleCategories] a
		INNER JOIN [Control].[Fuels] b
			ON a.[DefaultFuelId] = b.[FuelId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pVehicleCategoryId IS NULL OR [VehicleCategoryId] = @pVehicleCategoryId)
	  AND (@pKey IS NULL 
				OR a.[Manufacturer] like '%'+@pKey+'%'
				OR a.[Type] like '%'+@pKey+'%'
				OR CONVERT(VARCHAR(20),a.[Liters]) like '%'+@pKey+'%'
				OR b.[Name] like '%'+@pKey+'%')
	ORDER BY [VehicleCategoryId] DESC
	
    SET NOCOUNT OFF
END

GO


