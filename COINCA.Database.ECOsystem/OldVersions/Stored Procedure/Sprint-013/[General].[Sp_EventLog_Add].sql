USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_EventLog_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_EventLog_Add]
GO

/****** Object:  StoredProcedure [General].[Sp_EventLog_Add]    Script Date: 8/26/2015 5:11:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 26/Agosto/2015
-- Description:	Insert EventLog Information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_EventLog_Add]
(   
	@pEventType VARCHAR(15) = NULL
    ,@pMessage VARCHAR(1000) = NULL
    ,@pController VARCHAR(50) = NULL
    ,@pAction VARCHAR(50) = NULL
    ,@pIsError BIT = 0
    ,@pIsInfo BIT = 0
    ,@pIsWarning BIT = 0
    ,@pUserId INT = NULL
    ,@pCustomerId INT = NULL
    ,@pPartnerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END

			INSERT INTO [General].[EventLog]
				   ([EventType]
				   ,[UserId]
				   ,[CustomerId]
				   ,[PartnerId]
				   ,[Controller]
				   ,[Action]
				   ,[Mensaje]
				   ,[IsInfo]
				   ,[IsWarning]
				   ,[IsError])
			 VALUES
				  (
		   		    @pEventType
					,@pUserId
					,@pCustomerId
					,@pPartnerId
					,@pController
					,@pAction
					,@pMessage
					,@pIsInfo
					,@pIsWarning
					,@pIsError
				   )
			 
            SET @lRowCount = @@ROWCOUNT
           
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (50001, 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


