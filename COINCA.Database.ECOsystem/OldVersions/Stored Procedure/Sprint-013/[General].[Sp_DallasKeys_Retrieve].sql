USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DallasKeys_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DallasKeys_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 27/Agosto/2015
-- Description:	Retrieve Dallas Keys Information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DallasKeys_Retrieve]
(   
     @pCustomerId INT = NULL
	,@pDallasId INT = NULL
	,@pKey VARCHAR(800) = NULL
	,@pStartDateTime DATETIME = NULL
	,@pEndDateTime DATETIME = NULL
	,@pIsUsed BIT = NULL
)
AS
BEGIN
	
	SELECT 
		dk.DallasId, 
		dk.CustomerId, 
		dk.DallasCode, 
		dk.IsUsed,
		dk.IsDeleted,
		(SELECT TOP 1 dkd.[UserId]
		   FROM [General].[DallasKeysByDriver] dkd
		    WHERE dkd.[DallasId] = dk.[DallasId]) AS [UserId]
	FROM [General].[DallasKeys] dk
	WHERE dk.IsDeleted = 0 AND
		(@pDallasId	IS NULL OR dk.DallasId = @pDallasId) AND
		(@pCustomerId IS NULL OR dk.CustomerId = @pCustomerId) AND
		((@pStartDateTime IS NULL AND @pEndDateTime IS NULL) OR (dk.InsertDate >= @pStartDateTime AND dk.InsertDate <= @pEndDateTime)) AND
		(@pIsUsed IS NULL OR dk.IsUsed = @pIsUsed) AND
		(@pKey IS NULL OR dk.DallasCode LIKE '%'+ @pKey +'%')
END

GO


