USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DallasKeysByDriver_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DallasKeysByDriver_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 27/Agosto/2015
-- Description:	Add or Edit Dallas Keys by Driver
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_DallasKeysByDriver_AddOrEdit]
(   
     @pDallasId INT = NULL
	,@pUserId INT = NULL
	,@pLogUserId INT = NULL 
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			-- VALIDAMOS SI EL CONDUCTOR TIENE ASIGNADA UNA LLAVE DALLAS
			DECLARE @DallasKeyIdOld INT = 0
			
			SELECT TOP 1 
				@DallasKeyIdOld = [DallasId]
			FROM [General].[DallasKeysByDriver]
			WHERE [UserId] = @pUserId

			IF(@DallasKeyIdOld IS NOT NULL AND @DallasKeyIdOld > 0)
			BEGIN
				-- ACTUALIZAMOS EL ESTADO DE LA LLAVE DALLAS ANTERIORMENTE ASIGANDA
				UPDATE [General].[DallasKeys]
				SET
				    [IsUsed] = 0, -- bit
				    [ModifyDate] = DATEADD(hour, -6, GETDATE())	
				WHERE [DallasId] = @DallasKeyIdOld

				-- ACTUALIZAMOS EL REGISTRO ACTUAL EN [DallasKeysByDriver]
				UPDATE [General].[DallasKeysByDriver]
				SET 
					[DallasId] = @pDallasId,	
					[ModifyUserId] = @pLogUserId,
					[ModifyDate] = DATEADD(hour, -6, GETDATE())		
				WHERE 
					[UserId] = @pUserId AND
					[DallasId] = @DallasKeyIdOld
				
				-- ACTUALIZAMOS EL ESTADO DE LA NUEVA LLAVE DALLAS ASIGANDA
				UPDATE [General].[DallasKeys]
				SET
				    [IsUsed] = 1, -- bit
				    [ModifyDate] = DATEADD(hour, -6, GETDATE())	
				WHERE [DallasId] = @pDallasId
			END
			ELSE
			BEGIN
				
				-- INSERTAMOS UNA NUEVA ASIGNACIÓN
				INSERT INTO [General].[DallasKeysByDriver]
					([DallasId]
					,[UserId]
					,[InsertUserId])
				VALUES
					(@pDallasId	
					,@pUserId
					,@pLogUserId)

				-- ACTUALIZAMOS EL ESTADO DE LA LLAVE DALLAS ASIGANDA
				UPDATE [General].[DallasKeys]
				SET
				    [IsUsed] = 1, -- bit
				    [ModifyDate] = DATEADD(hour, -6, GETDATE())	
				WHERE [DallasId] = @pDallasId
			END

			--//////////////////////////////////////////////////////////
			 
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR (50001, 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO