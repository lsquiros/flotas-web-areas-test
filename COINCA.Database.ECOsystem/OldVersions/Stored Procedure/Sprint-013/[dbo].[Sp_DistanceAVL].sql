USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_DistanceAVL]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_DistanceAVL]
GO

/****** Object:  StoredProcedure [dbo].[Sp_DistanceAVL]    Script Date: 9/10/2015 3:26:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Pe�a
-- Create date: 18/08/2015
-- Description:	c�lculo de distancia entre el Reporte Actual y ReportLast
-- **STDistance devuelve el valor en metros
-- =============================================
CREATE PROCEDURE [dbo].[Sp_DistanceAVL]( 
    @pDevice [int] = NULL, 
	@pLatitude [float] = NULL,
    @pLongitude [float] = NULL,
	@ptime2 [datetime] = NULL
)

AS
BEGIN

	DECLARE @g geography
    DECLARE @h geography
    DECLARE @latLast float
    DECLARE @lonLast float
	DECLARE @ptime1 datetime
	DECLARE @pdifftime int

	SET @latLast =( select [Latitude]  from [Report_Last] WHERE Device =@pDevice) 
	SET @lonLast =( select [Longitude]  from [Report_Last] WHERE Device =@pDevice) 
	SET @ptime1 =( select [GPSDateTime]  from [Report_Last] WHERE Device =@pDevice) 
	
	DECLARE @point1 geography = 'POINT(' + CAST( @pLongitude  AS VARCHAR(MAX)) +' ' + CAST( @pLatitude  AS VARCHAR(MAX)) + ')'
	DECLARE @point2 geography = 'POINT(' + CAST( @lonLast  AS VARCHAR(MAX)) +' ' + CAST( @latLast  AS VARCHAR(MAX)) + ')'

    SET @g = geography::STGeomFromText(@point1.ToString(), 4326);
    SET @h = geography::STGeomFromText(@point2.ToString(), 4326); 
	
	SET @pdifftime =  DATEDIFF (second, @ptime1 , @ptime2 )
   
   --RETURN
    SELECT Round(@g.STDistance(@h),2)/@pdifftime
END
GO