USE [ECOsystemDev]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreSpeedDaily]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_ScoreSpeedDaily]
GO

/****** Object:  StoredProcedure [Operation].[Sp_ScoreSpeedDaily]    Script Date: 9/1/2015 11:22:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- Created by:		Gerald Solano C.
-- Created date:	08/06/2015 
-- Description:	Retrieve Score Driving Daily

CREATE PROCEDURE [Operation].[Sp_ScoreSpeedDaily] 
( @pCustomerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pScoreType char(1)			--A- Reporte Administrativo  , R- Reporte de  carretera (Route)
)
AS
BEGIN

	--DECLARE @MyTable  dbo.TypeDeviceSpeed
	Declare @VehiclesByUser table
	(Device int,
	 Vehicle int,
	 StartOdometer int,
	 EndOdometer int,
	 StartDate datetime,
	 EndDate datetime)

	Create table #LocalDriverScore
	(DriverScoreId INT Identity(1,1),
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 Score Float,
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float,
	 Date_Time datetime,
	 MaxOdometer int,
	 MinOdometer int,
	 Vec float,
	 Dt float,
	 De float,
	 Ce float)
	 
	Declare @UserId Int
	Declare @Name varchar(250),
			@Identification varchar(50)
	Declare  @Score Float

	Select @UserId = Min(U.UserId) 
	  from General.Users U
	  inner join General.DriversUsers D on U.UserId = D.UserId
	 Where 
	   U.IsActive =  1 
	   and D.CustomerId = @pCustomerId

	--Declare @pCustomerId Int
	--Declare @pStartDate datetime  = '2015-09-01 00:00:00'
	--DECLARE @pEndDate datetime = '2015-09-01 23:59:59'
	--SET @pCustomerId = 28
	--SET @UserId = 1240

	--Begin transaction
	While  @UserId  is not null
	BEGIN
		
		-- VARIBLE PARA MANTENER EL CICLO ACTUAL PARA CUANDO EL USER ID SE ACTUALIZA POR 
		-- LA VALIDACIÓN DEL DALLAS KEY
		DECLARE @TEMPUserId INT = @UserId
		
		-- INSERTAMOS LOS DEVICES DE LOS VEHCIULOS LIGADOS AL USUARIO Y QUE ESTA USANDO
		Insert into @VehiclesByUser
			(Device, Vehicle, StartOdometer, EndOdometer, StartDate, EndDate)  
		--Select   V.IntrackReference As Device,CASE WHEN  VU.InitialOdometer IS NULL THEN 0 eLSE VU.InitialOdometer end  ,CASE WHEN VU.EndOdometer IS NULL THEN 0 eLSE EndOdometer end    , VU.InsertDate StartDate ,VU.LastDateDriving EndDate
		Select V.DeviceReference, V.IntrackReference, IsNull(VU.InitialOdometer, 0), IsNull(VU.EndOdometer,0), VU.InsertDate, IsNull(VU.LastDateDriving, GETDATE())
		from General.VehiclesByUser VU 
		inner Join General.Vehicles V ON  V.VehicleId = VU.VehicleId
		inner JOIN General.Users U  ON U.UserId = VU.UserId
		Where U.UserId =  @UserId 
		  and V.CustomerId = @pCustomerId
		  And V.DeviceReference is not null
		  and ((VU.LastDateDriving is null) or (@pEndDate<=ISNULL(VU.LastDateDriving,@pEndDate) and @pStartDate>=ISNULL(VU.InsertDate,@pStartDate)))
		order by V.DeviceReference, VU.InsertDate, VU.LastDateDriving


		-- VALIDATE AND GET DALLAS KEY //////////////////////////////////////////////////////////////
		DECLARE @CurrentDallasKey VARCHAR(100) -- DALLAS KEY EN REPORT_LAST 
			--DECLARE @CurrentUserId INT 
			--DECLARE @RegisterDallasKey VARCHAR(100) -- DALLAS KEY EN DALLAS KEY BY DRIVER TABLE

		DECLARE @DeviceVehiclesByUser INT -- MIN DEVICE PARA RECORRERLO
		SELECT @DeviceVehiclesByUser = MIN(Device)
		FROM @VehiclesByUser 

		-- RECORREMOS LOS DEVICES ASIGNADOS
		WHILE  @DeviceVehiclesByUser IS NOT NULL
		BEGIN

			SELECT @CurrentDallasKey = dl.[DriverID] 
				FROM [dbo].[Driver_Last] dl	
				WHERE [Device] = @DeviceVehiclesByUser AND 
					  [DriverID] IS NOT NULL 
					  --AND [RegisteredDate] >= @pStartDate

			IF(@CurrentDallasKey IS NULL)
			BEGIN 
				SELECT @CurrentDallasKey = [DriverID] 
				FROM [dbo].[Report_Last]
				WHERE [Device] = @DeviceVehiclesByUser AND 
					  [DriverID] IS NOT NULL	
			END

			-- SIGUIENTE ITERACION
			SELECT @DeviceVehiclesByUser = MIN(Device)
			FROM @VehiclesByUser 
			WHERE Device > @DeviceVehiclesByUser
		END 

		-- SI LAS LLAVES DALLAS SON DIFERENTES ASIGNAMOS EL USER ID REGISTRADO EN REPORT LAST
		IF(@CurrentDallasKey IS NOT NULL)
		BEGIN
			-- SETEAMOS EL NUEVO USER ID QUE SE REFERENCIA CON 
			-- EL DALLAS KEY REGISTRADO EN REPORT LAST
			DECLARE @DallasUserId INT
				
			SELECT @DallasUserId = dkd.[UserId] 
			FROM [General].[DallasKeysByDriver] dkd
			INNER JOIN [General].[DallasKeys] dk ON dkd.[DallasId] = dk.[DallasId]
			WHERE dk.[DallasCode] = @CurrentDallasKey

			IF(@DallasUserId IS NOT NULL AND @DallasUserId <> @UserId)
			BEGIN
				SET @UserId = @DallasUserId
			END

			--SELECT @RegisterDallasKey = dk.[DallasCode] 
			--FROM [General].[DallasKeysByDriver] dkd
			--INNER JOIN [General].[DallasKeys] dk ON dkd.[DallasId] = dk.[DallasId]
			--WHERE dkd.[UserId] = @UserId

			--IF(@RegisterDallasKey IS NOT NULL AND @CurrentDallasKey <> @RegisterDallasKey) 
			--BEGIN
				
			--END
		END
		--/////////////////////////////////////////////////////////////////////////////////////////

		--SELECT * from @VehiclesByUser

		------------------------------------------

		IF (SELECT COUNT(*) FROM @VehiclesByUser ) > 0 
		BEGIN 
			
			DECLARE @VehiclesByUserString varchar(max);
			SET @VehiclesByUserString = (SElect * FROm  @VehiclesByUser AS Lista FOR XML AUTO)
			--SELECT  @VehiclesByUserString   

			DECLARE @ScoreOutput nvarchar(max)
			DECLARE @SQL NVARCHAR(max)
				
			--SET @SQL=(SELECT ScoreDrivingFormulaSP FROM Insurance.ScoreDrivingFormula WHERE CustomerId=@pCustomerId AND IsActive=1)
			--IF @SQL is null
			--BEGIN
				--set @SQL='[dbo].[Sp_GetScoreDriver]';
			SET @SQL='[dbo].[Sp_GetScoreDriverDaily]';
			--END

			EXECUTE  @SQL @pStartDate, @pEndDate, @VehiclesByUserString, @pScoreType, @pScoreTable = @ScoreOutput OUTPUT
			
			DECLARE @pScoreOutputXML XML;
			SET  @pScoreOutputXML = CAST(@ScoreOutput  AS XML);
				
			--DECLARE @ScoreTable dbo.TypeDriverScore;
			DECLARE @ScoreTable dbo.TypeDriverScoreDaily;
				
			Insert into  @ScoreTable
			SELECT --DeviceSpeedId   = T.Item.value('@DeviceSpeedId ', 'int'),
					--IdDailyScore = IDENTITY(INT,1,1),
					KmTraveled = T.Item.value('@KmTraveled', 'float'),
					OverSpeed = T.Item.value('@OverSpeed', 'float'),
					OverSpeedAmount  = T.Item.value('@OverSpeedAmount',  'float'),
					OverSpeedDistancePercentage = T.Item.value('@OverSpeedDistancePercentage', 'float'),
					OverSpeedAverage = T.Item.value('@OverSpeedAverage', 'float'),
					OverSpeedWeihgtedAverage = T.Item.value('@OverSpeedWeihgtedAverage', 'float'),
					StandardDeviation = T.Item.value('@StandardDeviation', 'float'),
					Variance = T.Item.value('@Variance', 'float'),
					VariationCoefficient = T.Item.value('@VariationCoefficient', 'float'),
					AverageScore = T.Item.value('@AverageScore', 'float'),
					OverSpeedDistance= T.Item.value('@OverSpeedDistance', 'float'),
					Date_Time= T.Item.value('@Date_Time', 'datetime'),
					MaxOdometer= T.Item.value('@MaxOdometer', 'int'),
					MinOdometer= T.Item.value('@MinOdometer', 'int'),
					Vec= T.Item.value('@Vec', 'float'),
					Dt= T.Item.value('@Dt', 'float'),
					De= T.Item.value('@De', 'float'),
					Ce= T.Item.value('@Ce', 'float')
			FROM   @pScoreOutputXML.nodes('Lista') AS T(Item)
				
			--SELECT @VehiclesByUserString
			--SELECT * FROM @ScoreTable

			--RECORREMOS LOS RESULTADOS DEL XML DEVUELTO POR EL Sp_GetScoreDriverDaily
			DECLARE @minIndice int = NULL 
			DECLARE @maxIndice int = NULL

			SELECT @minIndice = MIN(IdDailyScore), @maxIndice = MAX(IdDailyScore) FROM @ScoreTable
				
			WHILE @minIndice <= @maxIndice
			BEGIN
				--SELECT @minIndice
				set @Score=(select AverageScore from @ScoreTable WHERE IdDailyScore = @minIndice)
				--Select @Score
				
				If @Score is not null
				BEGIN
					--Obtener nombre e identificación del usuario
					Select @Name = U.Name,
						   @Identification = D.Identification 
					  from General.Users U
					  inner join General.DriversUsers D on U.UserId = D.UserId
					where U.UserId = @UserId
				 
					Insert into #LocalDriverScore (UserID, Name, Identification, Score,
								KmTraveled,
								OverSpeed,
								OverSpeedAmount,
								OverSpeedDistancePercentage ,
								OverSpeedAverage ,
								OverSpeedWeihgtedAverage ,
								StandardDeviation ,
								Variance ,
								VariationCoefficient,
								OverSpeedDistance,
								Date_Time,
								MaxOdometer,
								MinOdometer,
								Vec,
								Dt,
								De,
								Ce)
					Values(@UserId, @Name, @Identification, @Score,
							(select KmTraveled from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select OverSpeed from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select OverSpeedAmount from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select OverSpeedDistancePercentage from @ScoreTable  WHERE IdDailyScore = @minIndice),
							(select OverSpeedAverage from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select OverSpeedWeihgtedAverage from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select StandardDeviation from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select Variance from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select VariationCoefficient from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select OverSpeedDistance from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select Date_Time from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select MaxOdometer from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select MinOdometer from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select Vec from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select Dt from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select De from @ScoreTable WHERE IdDailyScore = @minIndice),
							(select Ce from @ScoreTable WHERE IdDailyScore = @minIndice))
				
					--Select * FROM #LocalDriverScore
				----Limpia la  tabla para  la nueva seleccion.
				END

				-- Siguiente Indice
				SELECT @minIndice = MIN(IdDailyScore) 
				FROM @ScoreTable
				WHERE IdDailyScore > @minIndice
			END
			
			delete from @ScoreTable
			DELETE FROM @VehiclesByUser
		END

		-- RESETEA EL USER ID RECORRIDO SI ESTE FUE ALTERADO EN EL DALLAS KEY
		SET @UserId = @TEMPUserId
		--///////////////////////////////////////////////////////////////////

		--- Siguiente********************
		Select @UserId = Min(U.UserId) 
		  from General.Users U
		  inner join General.DriversUsers D on U.UserId = D.UserId
		 Where U.IsActive =  1 
		   and D.CustomerId = @pCustomerId
		   and U.UserId > @UserId
	END
	
	--RETURN SCORES AND DATA FOR REGISTER EN DATA BASE
	Select DriverScoreId, 
			UserId, 
			Name, 
			Identification, 
			Score,KmTraveled,
			OverSpeed ,
			OverSpeedAmount,
			OverSpeedDistancePercentage,
			OverSpeedAverage,
			OverSpeedWeihgtedAverage,
			StandardDeviation,
			Variance,
			VariationCoefficient,
			OverSpeedDistance,
		    Date_Time,
			MaxOdometer,
			MinOdometer,
			Vec,
			Dt,
			De,
			Ce
	FROM #LocalDriverScore
	ORDER BY Score ASC
	
	Drop table #LocalDriverScore
	
END



GO


