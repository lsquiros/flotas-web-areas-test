USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReportDenied_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsReportDenied_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsReportDenied_Retrieve]    Script Date: 9/9/2015 11:02:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Kevin Pe�a
-- Create date: 07/24/2015
-- Description:	Retrieve DeniedTransactionsReport log
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionsReportDenied_Retrieve]
(
	@pCustomerId INT,					--@pCustomerId: CustomerId		
	@pYear INT = NULL,					--@pYear: Year
	@pMonth INT = NULL,					--@pMonth: Month
	@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
	@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN

	--DECLARE @pCustomerId INT = 22
	--DECLARE @pYear INT = 2015
	--DECLARE @pMonth INT = 8		
	--DECLARE @pStartDate DATETIME = NULL
	--DECLARE @pEndDate DATETIME = NULL

	SET NOCOUNT ON

SELECT 
       cast(0 as INT) as [TransactionId]
	  ,'00000' AS [SystemTraceCode]
      ,cast(null as char(10)) as [HolderName]
	  ,cast(null as char(10)) as [Date]
      ,cast(null as char(10)) as [FuelName]
	  ,cast(0 as decimal) as [FuelAmount]
	   ,cast(0 as int) as [Odometer] 
	  ,cast(0 as decimal) as [Liters]
	  ,cast(null as char(10)) as [PlateId]
	  ,cast(null as char(10)) as [CurrencySymbol]
	  ,'Rechazada' as [State]
	  ,[Message]  
	  ,[TransportData]
	  ,DATEADD(hour,-6,[InsertDate]) AS [InsertDate]
      ,[ResponseCode]
      ,[ResponseCodeDescription]
      ,[CustomerId]
  FROM [Control].[LogTransactionsPOS]

  WHERE [CustomerId] =@pCustomerId
   AND [IsFail] = 1
   AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,[InsertDate]) = @pMonth
						AND DATEPART(yyyy,[InsertDate]) = @pYear) OR
		(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND [InsertDate] BETWEEN @pStartDate AND @pEndDate))

  ORDER BY [InsertDate] DESC

END	

SET NOCOUNT OFF
GO