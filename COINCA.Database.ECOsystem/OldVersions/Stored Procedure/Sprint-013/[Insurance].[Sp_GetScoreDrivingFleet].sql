USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetScoreDrivingFleet]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Insurance].[Sp_GetScoreDrivingFleet]
GO

/****** Object:  StoredProcedure [Insurance].[Sp_GetScoreDrivingFleet]    Script Date: 9/3/2015 4:26:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Andr�s Oviedo
-- Create date: 09/02/2015
-- Description:	Retrieve Score Driving Fleet] information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_GetScoreDrivingFleet] 
( @pPartnerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)=null,	--Posibles valores S:Summarized, D:Detailed
  @pCustomerId int=Null	
)
AS BEGIN

DECLARE @tableCustomers TABLE (CustomerId int, Name varchar(250)) 
DECLARE @li_CustomerId INT
DECLARE @NameCustomer varchar(250)
DECLARE @tableDriversScore TABLE(
	DriverScoreId int
	,UserId	int
	,Name  varchar (250)
	,Identification  varchar(50)	
	,ScoreType	 varchar(5)
	,Score Float
	,Photo varchar(max)
	,ScoreSpeedAdmin Float
	,ScoreSpeedRoute Float
	,TraveledDistance float
)

DECLARE @ScoreDrivingFleet TABLE(
CustomerId int
,EncryptedName	varchar (1000)
,AverageScore  FLOAT
)

INSERT INTO @tableCustomers
EXEC  [Insurance].[Sp_GetRosterInsurance]
		@pPartnerId,@pCustomerId
---SELECT * from @tableCustomers 

Select @li_CustomerId = MIN(CustomerId)
from @tableCustomers

WHILE @li_CustomerId is not null
BEGIN

INSERT into   @tableDriversScore  (DriverScoreId,UserId,Name,Identification,ScoreType,Score,Photo ,ScoreSpeedAdmin,ScoreSpeedRoute,TraveledDistance)
--EXEC [Operation].[Sp_GetGlobalScoreAllDriver]
EXEC [Operation].[Sp_GetGlobalScoresByMonthOrDays]
		@li_CustomerId ,
		@pYear ,
		@pMonth ,
		@pStartDate ,
		@pEndDate ,
		@pReportType

Select @NameCustomer = Name FROM  @tableCustomers where CustomerId=@li_CustomerId

insert into @ScoreDrivingFleet SELECT @li_CustomerId CustomerId, @NameCustomer  as EncryptedName ,CASE  WHEN AVG(Score) IS NULL THEN 0 ELSE AVG(Score)  END AS  AverageScore
FROM  @tableDriversScore 
Delete  FROM  @tableDriversScore 

-----------------------------
--Siguiente Customer
		Select @li_CustomerId = MIN(CustomerId)
		  from @tableCustomers 
		 where CustomerId > @li_CustomerId
END  -- End While
select * from @ScoreDrivingFleet
END




GO


