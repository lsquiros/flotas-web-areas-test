USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[ProcessReport]
GO

/****** Object:  StoredProcedure [dbo].[ProcessReport]    Script Date: 9/10/2015 2:45:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[ProcessReport] (
	 @dUnitID AS DECIMAL(20, 0)
	,@dtGPSdatetime AS DATETIME
	,@dtRTCdatetime AS DATETIME
	,@dtRepdatetime AS DATETIME
	,@fLongitude AS FLOAT
	,@fLatitude AS FLOAT
	,@fHeading AS FLOAT
	,@iReportID AS INT
	,@fOdometer AS FLOAT
	,@fHDOP AS FLOAT
	,@tAllInputs AS TINYINT
	,@tAllOutputs AS TINYINT
	,@fVSSSpeed AS FLOAT
	,@fAnalogInput AS FLOAT
	,@sDriverID AS VARCHAR(16) = ''
	,@fTemperature1 AS FLOAT = 2000
	,@fTemperature2 AS FLOAT = 2000
	,@iMCC AS INT = 0
	,@iMNC AS INT = 0
	,@iLAC AS INT = 0
	,@iCellID AS INT = 0
	,@iRXLevel AS INT = 0
	,@iGSMQ AS INT = 0
	,@iGSMS AS INT = 0
	,@iSatellites AS INT = 0
	,@bRealTime AS BIT = 0
	,@fMainVolt AS FLOAT
	,@fBatVolt AS FLOAT
	,@iRPM AS INT = NULL
	,@sNeighboring AS VARCHAR(72) = ''
	,@sTextMessage AS VARCHAR(1024) = ''
	,@iOBDMalfunctionIndLamp AS INT = NULL
	,@fOBDFuelUsed AS FLOAT = NULL
	,@iOBDFuelLevel AS INT = NULL
	,@iOBDEngineRPM AS INT = NULL
	,@iOBDEngineLoad AS INT = NULL
	,@iOBDAccelerator AS INT = NULL
	,@iOBDEngineCoolantTemp AS INT = NULL
	,@fOBDMassAirFlowRate AS FLOAT = NULL
	,@sOBDVIN AS VARCHAR(19) = NULL
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @iDevice AS INT
		,@sUnitID AS VARCHAR(20)
		,@vsDriverID AS VARCHAR(16)
		,@vfTemperature1 AS FLOAT
		,@vfTemperature2 AS FLOAT
		,@viMCC AS INT
		,@viMNC AS INT
		,@viLAC AS INT
		,@viCellID AS INT
		,@viRXLevel AS INT
		,@viGSMQ AS INT
		,@viGSMS AS INT
		,@viRegistro AS INT
		,@viCantidad AS INT
		,@vdtGPSdatetime AS DATETIME
		,@vbActualizarTexto AS BIT
		,@iRPMCalc AS INT
		,@dFuelVolumeGalCalc AS FLOAT
		,@viDeviceStatus AS INT
		,@cTransaccion AS VARCHAR(16)

	EXEC UnitIDLookUp @dUnitID = @dUnitID
		,@iDevice = @iDevice OUTPUT

	IF isnull(@iDevice, 0) > 0
	BEGIN
		SET @cTransaccion = 'T' + CONVERT(VARCHAR(16), @iDevice) + 'T' + REPLACE(convert(VARCHAR(24), getdate(), 114), ':', '')

		BEGIN TRAN @cTransaccion

		SELECT @vsDriverID = NULL
			,@vfTemperature1 = NULL
			,@vfTemperature2 = NULL
			,@viMCC = NULL
			,@viMNC = NULL
			,@viLAC = NULL
			,@viCellID = NULL
			,@viRXLevel = NULL
			,@viGSMQ = NULL
			,@viGSMS = NULL

		IF len(ltrim(rtrim(@sDriverID))) > 0
		BEGIN
			SET @vsDriverID = @sDriverID
		END

		IF @fTemperature1 <> 2000
		BEGIN
			SET @vfTemperature1 = @fTemperature1
		END

		IF @fTemperature2 <> 2000
		BEGIN
			SET @vfTemperature2 = @fTemperature2
		END

		IF @iMCC > 0
			AND @iMNC > 0
		BEGIN
			SELECT @viMCC = @iMCC
				,@viMNC = @iMNC
				,@viGSMQ = @iGSMQ
				,@viGSMS = @iGSMS
		END

		IF @iLAC > 0
		BEGIN
			SELECT @viLAC = @iLAC
		END

		IF @iCellID > 0
		BEGIN
			SELECT @viCellID = @iCellID
		END

		IF @iRXLevel >= - 111
			AND @iRXLevel <= - 48
		BEGIN
			SELECT @viRXLevel = @iRXLevel
		END

		-- VALIDACI�N DE SALTOS   -- Kevin Pe�a 20/08/2015				  
		DECLARE @inserte_normal BIT = 1 --Por defecto todos los reportes deben insertarse normal 
		DECLARE @t_distance FLOAT

		EXEC @t_distance = [dbo].[Sp_DistanceAVL] @pDevice = @iDevice
			,@pLatitude = @fLatitude
			,@pLongitude = @fLongitude
			,@ptime2 =  @vdtGPSdatetime

		IF (@t_distance > 44.4) -- si mayor a 44,4 m/s
		BEGIN
			SET @inserte_normal = 0
		END

		IF (@inserte_normal = 0)
		BEGIN
			INSERT INTO SaltosAVL (
				Device
				,GPSDateTime
				,RTCDateTime
				,RepDateTime
				,SvrDateTime
				,Longitude
				,Latitude
				,Heading
				,ReportID
				,Odometer
				,HDOP
				,InputStatus
				,OutputStatus
				,VSSSpeed
				,AnalogInput
				,DriverID
				,Temperature1
				,Temperature2
				,MCC
				,MNC
				,LAC
				,CellID
				,RXLevel
				,GSMQuality
				,GSMStatus
				,Satellites
				,RealTime
				,MainVolt
				,BatVolt
				)
			VALUES (
				@iDevice
				,@dtGPSdatetime
				,@dtRTCdatetime
				,@dtRepdatetime
				,GETDATE()
				,@fLongitude
				,@fLatitude
				,@fHeading
				,@iReportID
				,@fOdometer
				,@fHDOP
				,@tAllInputs
				,@tAllOutputs
				,@fVSSSpeed
				,@fAnalogInput
				,@vsDriverID
				,@vfTemperature1
				,@vfTemperature2
				,@viMCC
				,@viMNC
				,@viLAC
				,@viCellID
				,@viRXLevel
				,@viGSMQ
				,@viGSMS
				,@iSatellites
				,@bRealTime
				,@fMainVolt
				,@fBatVolt
				)
		END
		ELSE --si no es salto el proceso continua normal
		BEGIN
			INSERT INTO Reports (
				Device
				,GPSDateTime
				,RTCDateTime
				,RepDateTime
				,SvrDateTime
				,Longitude
				,Latitude
				,Heading
				,ReportID
				,Odometer
				,HDOP
				,InputStatus
				,OutputStatus
				,VSSSpeed
				,AnalogInput
				,DriverID
				,Temperature1
				,Temperature2
				,MCC
				,MNC
				,LAC
				,CellID
				,RXLevel
				,GSMQuality
				,GSMStatus
				,Satellites
				,RealTime
				,MainVolt
				,BatVolt
				)
			VALUES (
				@iDevice
				,@dtGPSdatetime
				,@dtRTCdatetime
				,@dtRepdatetime
				,GETDATE()
				,@fLongitude
				,@fLatitude
				,@fHeading
				,@iReportID
				,@fOdometer
				,@fHDOP
				,@tAllInputs
				,@tAllOutputs
				,@fVSSSpeed
				,@fAnalogInput
				,@vsDriverID
				,@vfTemperature1
				,@vfTemperature2
				,@viMCC
				,@viMNC
				,@viLAC
				,@viCellID
				,@viRXLevel
				,@viGSMQ
				,@viGSMS
				,@iSatellites
				,@bRealTime
				,@fMainVolt
				,@fBatVolt
				)
		END

		IF @@error <> 0
		BEGIN
			ROLLBACK TRAN @cTransaccion

			RAISERROR (
					'Error al insertar registro de Reports.'
					,16
					,1
					)

			RETURN - 1
		END

		SELECT @viRegistro = SCOPE_IDENTITY()

		/* 2014-01-15. WAB/JMBS. Procesamiento conductor m�s reciente */
		IF (@iReportID = 102)
		BEGIN
			DELETE
			FROM Driver_Last
			WHERE Device = @iDevice

			IF @@error <> 0
			BEGIN
				ROLLBACK TRAN @cTransaccion

				RAISERROR (
						'Error al borrar registro de Driver_Last.'
						,16
						,2
						)

				RETURN - 1
			END
		END

		IF (@iReportID = 10)
		BEGIN
			SELECT @viCantidad = count(1)
			FROM Driver_Last
			WHERE Device = @iDevice

			IF (@viCantidad = 0)
			BEGIN
				INSERT INTO Driver_Last (
					Device
					,DriverID
					,RegisteredDate
					)
				VALUES (
					@iDevice
					,@vsDriverID
					,GETDATE()
					)

				IF @@error <> 0
				BEGIN
					ROLLBACK TRAN @cTransaccion

					RAISERROR (
							'Error al insertar registro de Driver_Last.'
							,16
							,3
							)

					RETURN - 1
				END
			END
		END

		/* Fin de: 2014-01-15. WAB/JMBS. Procesamiento conductor m�s reciente */
		/* 2013-06-17. JMBS. Ajuste para OBD */
		IF (@inserte_normal = 1)
		BEGIN
			IF (
					(@iOBDMalfunctionIndLamp IS NOT NULL)
					OR (@fOBDFuelUsed IS NOT NULL)
					OR (@iOBDFuelLevel IS NOT NULL)
					OR (@iOBDEngineRPM IS NOT NULL)
					OR (@iOBDEngineLoad IS NOT NULL)
					OR (@iOBDAccelerator IS NOT NULL)
					OR (@iOBDEngineCoolantTemp IS NOT NULL)
					OR (@fOBDMassAirFlowRate IS NOT NULL)
					OR (@sOBDVIN IS NOT NULL)
					)
			BEGIN
				INSERT INTO Reports_OBD (
					Report
					,MalfunctionIndLamp
					,FuelUsed
					,FuelLevel
					,EngineRPM
					,EngineLoad
					,Accelerator
					,EngineCoolantTemp
					,MassAirFlowRate
					,VIN
					)
				VALUES (
					@viRegistro
					,@iOBDMalfunctionIndLamp
					,@fOBDFuelUsed
					,@iOBDFuelLevel
					,@iOBDEngineRPM
					,@iOBDEngineLoad
					,@iOBDAccelerator
					,@iOBDEngineCoolantTemp
					,@fOBDMassAirFlowRate
					,@sOBDVIN
					)

				IF @@error <> 0
				BEGIN
					ROLLBACK TRAN @cTransaccion

					RAISERROR (
							'Error al insertar registro de Reports_OBD.'
							,16
							,4
							)

					RETURN - 1
				END
			END
		END

		/* Fin de: 2013-06-17. JMBS. Ajuste para OBD */
		/* 2013-10-31. JMBS. Ajuste para RPM */
		IF (@inserte_normal = 1)
		BEGIN
			SET @iRPMCalc = NULL

			IF (@iRPM IS NOT NULL)
			BEGIN
				/* 2013-11-14. JMBS. Obtenci�n valor estimado real del RPM */
				EXEC ConvertRPMReading @iDevice = @iDevice
					,@iRPM = @iRPM
					,@iRPMCalculado = @iRPMCalc OUTPUT

				/* Fin de: 2013-11-14. JMBS. Obtenci�n valor estimado real del RPM */
				INSERT INTO Reports_RPM (
					Report
					,RPM
					,RPMReal
					)
				VALUES (
					@viRegistro
					,@iRPM
					,@iRPMCalc
					)

				IF @@error <> 0
				BEGIN
					ROLLBACK TRAN @cTransaccion

					RAISERROR (
							'Error al insertar registro de Reports_RPM.'
							,16
							,5
							)

					RETURN - 1
				END
			END
		END

		/* Fin de: 2013-10-31. JMBS. Ajuste para RPM */
		/* 2013-11-14. JMBS. Ajuste para Volumen Combustible */
		IF (@inserte_normal = 1)
		BEGIN
			SET @dFuelVolumeGalCalc = NULL

			IF (@fAnalogInput > 0)
			BEGIN
				EXEC ConvertAVoltageReading @iDevice = @iDevice
					,@dAVoltage = @fAnalogInput
					,@dFuelVolume = @dFuelVolumeGalCalc OUTPUT

				IF (@dFuelVolumeGalCalc IS NOT NULL)
				BEGIN
					INSERT INTO Reports_FuelLevel (
						Report
						,AnalogInput
						,FuelVolumeGallons
						)
					VALUES (
						@viRegistro
						,@fAnalogInput
						,@dFuelVolumeGalCalc
						)

					IF @@error <> 0
					BEGIN
						ROLLBACK TRAN @cTransaccion

						RAISERROR (
								'Error al insertar registro de Reports_FuelLevel.'
								,16
								,5
								)

						RETURN - 1
					END
				END
			END
		END

		/* Fin de: 2013-11-14. JMBS. Ajuste para Volumen Combustible */
		IF (@inserte_normal = 1)
		BEGIN
			IF (len(ltrim(rtrim(@sTextMessage))) > 0)
				AND (@viRegistro > 0)
			BEGIN
				INSERT INTO Reports_TextMessage (
					Report
					,TextMessage
					)
				VALUES (
					@viRegistro
					,ltrim(rtrim(@sTextMessage))
					)

				IF @@error <> 0
				BEGIN
					ROLLBACK TRAN @cTransaccion

					RAISERROR (
							'Error al insertar registro de Reports_TextMessage.'
							,16
							,6
							)

					RETURN - 1
				END
			END
		END

		IF (@inserte_normal = 1)
		BEGIN
			SET @vbActualizarTexto = 0

			SELECT @viCantidad = count(1)
			FROM Report_Last
			WHERE Device = @iDevice

			IF @viCantidad = 0
			BEGIN
				SET @vbActualizarTexto = 1

				INSERT INTO Report_Last (
					Device
					,GPSDateTime
					,RTCDateTime
					,RepDateTime
					,SvrDateTime
					,Longitude
					,Latitude
					,Heading
					,ReportID
					,Odometer
					,HDOP
					,InputStatus
					,OutputStatus
					,VSSSpeed
					,AnalogInput
					,DriverID
					,Temperature1
					,Temperature2
					,MCC
					,MNC
					,LAC
					,CellID
					,RXLevel
					,GSMQuality
					,GSMStatus
					,Satellites
					,RealTime
					,MainVolt
					,BatVolt
					)
				VALUES (
					@iDevice
					,@dtGPSdatetime
					,@dtRTCdatetime
					,@dtRepdatetime
					,GETDATE()
					,@fLongitude
					,@fLatitude
					,@fHeading
					,@iReportID
					,@fOdometer
					,@fHDOP
					,@tAllInputs
					,@tAllOutputs
					,@fVSSSpeed
					,@fAnalogInput
					,@vsDriverID
					,@vfTemperature1
					,@vfTemperature2
					,@viMCC
					,@viMNC
					,@viLAC
					,@viCellID
					,@viRXLevel
					,@viGSMQ
					,@viGSMS
					,@iSatellites
					,@bRealTime
					,@fMainVolt
					,@fBatVolt
					)

				IF @@error <> 0
				BEGIN
					ROLLBACK TRAN @cTransaccion

					RAISERROR (
							'Error al insertar registro de Report_Last.'
							,16
							,7
							)

					RETURN - 1
				END
			END
			ELSE
			BEGIN
				SELECT @vdtGPSdatetime = GPSDateTime
				FROM Report_Last
				WHERE Device = @iDevice

				/* 2013-07-04. JMBS. Se eval�a que reporte no est� m�s de 6 horas (UTC) en el futuro, partiendo del supuesto que zona horaria servidor es UTC-6 */
				/* 2013-11-19. JMBS. Se eval�a que reporte est� dentro de las coordenadas v�lidas de WGS84. */
				/* 2013-12-19. JMBS. Se filtan reportes con coordenadas 0, 0. */
				/* 2014-01-15. JMBS. Se permite actualiza si nuevo reporte es mayor o igual que reporte actual. */
				IF (
						(@dtGPSdatetime >= @vdtGPSdatetime)
						AND (@dtGPSdatetime < dateadd(hh, 12, GETDATE()))
						AND (
							@fLongitude >= - 180
							AND @fLongitude <= 180
							)
						AND (
							@fLatitude >= - 90
							AND @fLatitude <= 90
							)
						AND (
							@fLongitude <> 0
							AND @fLatitude <> 0
							)
						)
				BEGIN
					SET @vbActualizarTexto = 1

					UPDATE Report_Last
					SET GPSDateTime = @dtGPSdatetime
						,RTCDateTime = @dtRTCdatetime
						,RepDateTime = @dtRepdatetime
						,SvrDateTime = GETDATE()
						,Longitude = @fLongitude
						,Latitude = @fLatitude
						,Heading = @fHeading
						,ReportID = @iReportID
						,Odometer = @fOdometer
						,HDOP = @fHDOP
						,InputStatus = @tAllInputs
						,OutputStatus = @tAllOutputs
						,VSSSpeed = @fVSSSpeed
						,AnalogInput = @fAnalogInput
						,DriverID = @vsDriverID
						,Temperature1 = @vfTemperature1
						,Temperature2 = @vfTemperature2
						,MCC = @viMCC
						,MNC = @viMNC
						,LAC = @viLAC
						,CellID = @viCellID
						,RXLevel = @viRXLevel
						,GSMQuality = @viGSMQ
						,GSMStatus = @viGSMS
						,Satellites = @iSatellites
						,RealTime = @bRealTime
						,MainVolt = @fMainVolt
						,BatVolt = @fBatVolt
					WHERE Device = @iDevice

					IF @@error <> 0
					BEGIN
						ROLLBACK TRAN @cTransaccion

						RAISERROR (
								'Error al actualizar registro de Report_Last.'
								,16
								,8
								)

						RETURN - 1
					END
				END
			END
		END

		/* 2014-01-17. Proceso para actualizar el Status (Estado) del AVL */
		IF (@inserte_normal = 1)
		BEGIN
			IF @vbActualizarTexto = 1
			BEGIN
				SET @viDeviceStatus = NULL

				SELECT @viCantidad = count(1)
				FROM Report_Last_Status
				WHERE Device = @iDevice

				SELECT TOP 1 @viDeviceStatus = STATUS
				FROM DeviceStatus
				WHERE (
						Ignition = (@tAllInputs & 1)
						AND ReportID IS NULL
						)
					OR (
						Ignition = (@tAllInputs & 1)
						AND ReportID = @iReportID
						)
				ORDER BY Priority DESC

				IF (@viDeviceStatus IS NULL)
				BEGIN
					SELECT @viDeviceStatus = STATUS
					FROM DeviceStatus
					WHERE UNKNOWN = 1
				END

				IF (@viDeviceStatus IS NOT NULL)
				BEGIN
					IF (@viCantidad > 0)
					BEGIN
						UPDATE Report_Last_Status
						SET STATUS = @viDeviceStatus
						WHERE Device = @iDevice

						IF @@error <> 0
						BEGIN
							ROLLBACK TRAN @cTransaccion

							RAISERROR (
									'Error al actualizar registro en Report_Last_Status.'
									,16
									,9
									)

							RETURN - 1
						END
					END
					ELSE
					BEGIN
						INSERT INTO Report_Last_Status (
							Device
							,STATUS
							)
						VALUES (
							@iDevice
							,@viDeviceStatus
							)

						IF @@error <> 0
						BEGIN
							ROLLBACK TRAN @cTransaccion

							RAISERROR (
									'Error al insertar registro en Report_Last_Status.'
									,16
									,10
									)

							RETURN - 1
						END
					END
				END
			END
		END

		/* Fin de: 2014-01-17. Proceso para actualizar el Status (Estado) del AVL */
		IF (@inserte_normal = 1)
		BEGIN
			IF @vbActualizarTexto = 1
			BEGIN
				IF len(ltrim(rtrim(@sTextMessage))) > 0
				BEGIN
					SELECT @viCantidad = count(1)
					FROM Report_Last_TextMessage
					WHERE Device = @iDevice

					IF @viCantidad = 0
					BEGIN
						INSERT INTO Report_Last_TextMessage (
							Device
							,TextMessage
							)
						VALUES (
							@iDevice
							,ltrim(rtrim(@sTextMessage))
							)

						IF @@error <> 0
						BEGIN
							ROLLBACK TRAN @cTransaccion

							RAISERROR (
									'Error al insertar registro de Report_Last_TextMessage.'
									,16
									,11
									)

							RETURN - 1
						END
					END
					ELSE
					BEGIN
						UPDATE Report_Last_TextMessage
						SET TextMessage = ltrim(rtrim(@sTextMessage))
						WHERE Device = @iDevice

						IF @@error <> 0
						BEGIN
							ROLLBACK TRAN @cTransaccion

							RAISERROR (
									'Error al actualizar registro de Report_Last_TextMessage.'
									,16
									,12
									)

							RETURN - 1
						END
					END
				END
				ELSE
				BEGIN
					DELETE
					FROM Report_Last_TextMessage
					WHERE Device = @iDevice

					IF @@error <> 0
					BEGIN
						ROLLBACK TRAN @cTransaccion

						RAISERROR (
								'Error al borrar registro de Report_Last_TextMessage.'
								,16
								,13
								)

						RETURN - 1
					END
				END
			END
		END

		/* 2013-06-17. JMBS. Ajuste para OBD */
		IF (@inserte_normal = 1)
		BEGIN
			DELETE
			FROM Report_Last_OBD
			WHERE Device = @iDevice

			IF @@error <> 0
			BEGIN
				ROLLBACK TRAN @cTransaccion

				RAISERROR (
						'Error al borrar registro de Report_Last_OBD.'
						,16
						,14
						)

				RETURN - 1
			END

			IF (
					(@iOBDMalfunctionIndLamp IS NOT NULL)
					OR (@fOBDFuelUsed IS NOT NULL)
					OR (@iOBDFuelLevel IS NOT NULL)
					OR (@iOBDEngineRPM IS NOT NULL)
					OR (@iOBDEngineLoad IS NOT NULL)
					OR (@iOBDAccelerator IS NOT NULL)
					OR (@iOBDEngineCoolantTemp IS NOT NULL)
					OR (@fOBDMassAirFlowRate IS NOT NULL)
					OR (@sOBDVIN IS NOT NULL)
					)
			BEGIN
				SELECT @viCantidad = count(1)
				FROM Report_Last_OBD
				WHERE Device = @iDevice

				IF @viCantidad = 0
				BEGIN
					INSERT INTO Report_Last_OBD (
						Device
						,MalfunctionIndLamp
						,FuelUsed
						,FuelLevel
						,EngineRPM
						,EngineLoad
						,Accelerator
						,EngineCoolantTemp
						,MassAirFlowRate
						,VIN
						)
					VALUES (
						@iDevice
						,@iOBDMalfunctionIndLamp
						,@fOBDFuelUsed
						,@iOBDFuelLevel
						,@iOBDEngineRPM
						,@iOBDEngineLoad
						,@iOBDAccelerator
						,@iOBDEngineCoolantTemp
						,@fOBDMassAirFlowRate
						,@sOBDVIN
						)

					IF @@error <> 0
					BEGIN
						ROLLBACK TRAN @cTransaccion

						RAISERROR (
								'Error al insertar registro de Report_Last_OBD.'
								,16
								,15
								)

						RETURN - 1
					END
				END
				ELSE
				BEGIN
					UPDATE Report_Last_OBD
					SET MalfunctionIndLamp = @iOBDMalfunctionIndLamp
						,FuelUsed = @fOBDFuelUsed
						,FuelLevel = @iOBDFuelLevel
						,EngineRPM = @iOBDEngineRPM
						,EngineLoad = @iOBDEngineLoad
						,Accelerator = @iOBDAccelerator
						,EngineCoolantTemp = @iOBDEngineCoolantTemp
						,MassAirFlowRate = @fOBDMassAirFlowRate
						,VIN = @sOBDVIN
					WHERE Device = @iDevice

					IF @@error <> 0
					BEGIN
						ROLLBACK TRAN @cTransaccion

						RAISERROR (
								'Error al actualizar registro de Report_Last_OBD.'
								,16
								,16
								)

						RETURN - 1
					END
				END
			END
		END

		/* Fin de: 2013-06-17. JMBS. Ajuste para OBD */
		/* 2013-10-31. JMBS. Ajuste para RPM */
		IF (@inserte_normal = 1)
		BEGIN
			DELETE
			FROM Report_Last_RPM
			WHERE Device = @iDevice

			IF @@error <> 0
			BEGIN
				ROLLBACK TRAN @cTransaccion

				RAISERROR (
						'Error al borrar registro de Report_Last_RPM.'
						,16
						,17
						)

				RETURN - 1
			END

			IF (@iRPM IS NOT NULL)
			BEGIN
				SELECT @viCantidad = count(1)
				FROM Report_Last_RPM
				WHERE Device = @iDevice

				IF @viCantidad = 0
				BEGIN
					INSERT INTO Report_Last_RPM (
						Device
						,RPM
						,RPMReal
						)
					VALUES (
						@iDevice
						,@iRPM
						,@iRPMCalc
						)

					IF @@error <> 0
					BEGIN
						ROLLBACK TRAN @cTransaccion

						RAISERROR (
								'Error al insertar registro de Report_Last_RPM.'
								,16
								,18
								)

						RETURN - 1
					END
				END
				ELSE
				BEGIN
					UPDATE Report_Last_RPM
					SET RPM = @iRPM
						,RPMReal = @iRPMCalc
					WHERE Device = @iDevice

					IF @@error <> 0
					BEGIN
						ROLLBACK TRAN @cTransaccion

						RAISERROR (
								'Error al actualizar registro de Report_Last_RPM.'
								,16
								,19
								)

						RETURN - 1
					END
				END
			END
		END

		/* Fin de: 2013-10-31. JMBS. Ajuste para RPM */
		/* 2013-11-14. JMBS. Ajuste para Volumen Combustible */
		IF (@inserte_normal = 1)
		BEGIN
			DELETE
			FROM Report_Last_FuelLevel
			WHERE Device = @iDevice

			IF @@error <> 0
			BEGIN
				ROLLBACK TRAN @cTransaccion

				RAISERROR (
						'Error al borrar registro de Report_Last_FuelLevel.'
						,16
						,20
						)

				RETURN - 1
			END

			IF (@dFuelVolumeGalCalc IS NOT NULL)
			BEGIN
				SELECT @viCantidad = count(1)
				FROM Report_Last_FuelLevel
				WHERE Device = @iDevice

				IF @viCantidad = 0
				BEGIN
					INSERT INTO Report_Last_FuelLevel (
						Device
						,AnalogInput
						,FuelVolumeGallons
						)
					VALUES (
						@iDevice
						,@fAnalogInput
						,@dFuelVolumeGalCalc
						)

					IF @@error <> 0
					BEGIN
						ROLLBACK TRAN @cTransaccion

						RAISERROR (
								'Error al insertar registro de Report_Last_FuelLevel.'
								,16
								,21
								)

						RETURN - 1
					END
				END
				ELSE
				BEGIN
					UPDATE Report_Last_FuelLevel
					SET AnalogInput = @fAnalogInput
						,FuelVolumeGallons = @dFuelVolumeGalCalc
					WHERE Device = @iDevice

					IF @@error <> 0
					BEGIN
						ROLLBACK TRAN @cTransaccion

						RAISERROR (
								'Error al actualizar registro de Report_Last_FuelLevel.'
								,16
								,22
								)

						RETURN - 1
					END
				END
			END

			/* Fin de: 2013-11-14. JMBS. Ajuste para Volumen Combustible */
			COMMIT TRAN @cTransaccion
		END
	END
	ELSE
	BEGIN
		SET @sUnitID = CONVERT(VARCHAR(20), @dUnitID)

		RAISERROR (
				'No se encontr� Dispositivo referenciado por UnidID %s.'
				,16
				,23
				,@sUnitID
				)

		RETURN - 1
	END

	SET NOCOUNT OFF
END


GO


