USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetScoreDriverDaily]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_GetScoreDriverDaily]
GO

/****** Object:  StoredProcedure [dbo].[Sp_GetScoreDriverDaily]    Script Date: 8/3/2015 4:20:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano C.
-- Create date: 06/05/2015
-- Description:	Retrieve Daily Scores an date range
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_GetScoreDriverDaily]
(@pStartDate datetime,				--@pStartDate: Start date for report
 @pEndDate datetime,				--@pEndDate: End date for report
 @pListDevicesUsed varchar(max),	--@pListDevicesUsed: List of device used by an especific User (XML format)
 @pScoreType char(1),				--@pScoreType: A - Administrative Limits, R - Road Limits 
 @pScoreTable nvarchar(max) OUTPUT)
AS
BEGIN

--DECLARE @pStartDate datetime = '2015-08-01 00:00:00'				--@pStartDate: Start date for report
--DECLARE @pEndDate datetime = '2015-08-31 23:59:59'				--@pEndDate: End date for report
--DECLARE @pListDevicesUsed varchar(max) = 
--'<Lista Device="572" Vehicle="382" StartOdometer="0" EndOdometer="0" StartDate="2015-04-27T23:30:02.560" EndDate="2015-08-06T22:51:52.413"/><Lista Device="573" Vehicle="383" StartOdometer="0" EndOdometer="0" StartDate="2015-04-27T23:30:50.957" EndDate="2015-08-06T22:51:52.413"/>'
--DECLARE @pScoreType char(1)	= 'R'			--@pScoreType: A - Administrative Limits, R - Road Limits 

Create table #pOutput
(KmTraveled float,
 OverSpeed float,
 OverSpeedAmount float, 
 OverSpeedDistancePercentage float,
 OverSpeedAverage float, 
 OverSpeedWeihgtedAverage float,
 StandardDeviation float,
 Variance float,
 --SumVariance float,
 VariationCoefficient float,
 AverageScore  float,
 OverSpeedDistance float,
 Date_Time datetime,
 MaxOdometer int,
 MinOdometer int,
 Vec float,
 Dt float,
 De float,
 Ce float);
  
DECLARE @pListDevicesUsedXML XML;

SET  @pListDevicesUsedXML = CAST(@pListDevicesUsed  AS XML);
DECLARE @ListEvaluate   TypeDeviceSpeed ;

Insert into  @ListEvaluate
SELECT --DeviceSpeedId   = T.Item.value('@DeviceSpeedId ', 'int'),
       Device = T.Item.value('@Device', 'int'),
       StartOdometer = T.Item.value('@StartOdometer', 'int'),
       EndOdometer  = T.Item.value('@EndOdometer',  'int'),
       StartDate = T.Item.value('@StartDate', 'datetime'),
       EndDate = T.Item.value('@EndDate', 'datetime')
FROM   @pListDevicesUsedXML.nodes('Lista') AS T(Item)

--Select * from @ListEvaluate

---------------------------Constantes  parametrizadas-----------------------------
DECLARE @timebetweexcess int --= 30 --segundos
DECLARE @tiempopenalizado_exceso  int-- = 25
Set @timebetweexcess = dbo.GetNumericParameter('MaxSecsBetweenPoints', 'NA')
Set @tiempopenalizado_exceso = dbo.GetNumericParameter('PenaltySecsExcessSpeed', 'NA')
----------------------Locals------------------------------------------------------
Create table #Speedingtemp
(SpeedingId INT Identity(1,1),
 device int,
 SpeedLimit int not null,
 Variance float, 
 ActualSpeed int,
 Date_Time datetime not null, 
 Longitude float not null,
 Latitude  float	 not null,
 report bigint,
 PointID int,
 AzimuthDiff  int,
 Azimuth  int,
 Odometer float,
 Heading int,
 Distance float
  );

-- TABLA QUE AGRUPA LAS VELOCIDADES DE EXCESO POR D�A, HORA Y MINUTO
DECLARE @TmpTableSpeeding AS TABLE(
	[Id] INT IDENTITY(1,1),
	[device] INT,
	[ActualSpeed] FLOAT,
	[PermittedSpeed] FLOAT,
	[Odometer] FLOAT,
	[Date] DATE,
	[Hour] INT,
	[Minute] INT
)
-- /////////////////////////////////////////////////////////////////

-- TABLA CON LAS VELOCIDADES EN EXCESO FILTRADAS POR LA TABLA DE AGRUPACI�N
DECLARE @TmpTableSpeedingFINAL AS TABLE(
	[Id] INT,
	[device] INT,
	[ActualSpeed] FLOAT,
	[PermittedSpeed] FLOAT,
	[Odometer] FLOAT,
	[Date] DATE
	,[Hour] INT
	,[Minute] INT
)
-- /////////////////////////////////////////////////////////////////////////
 
DECLARE @ActualStartDateTime DATETiME ,
		@ActualEndDateTime DATETiME  ,
		@ActualDevice integer, 
		@DeviceSpeedId  int,
		@fechadeviceini DATETiME,
		@fechadevicefin DATETiME,
		@CantExcesoVel as int,
		@SumVelocidadExceso decimal,
		@PromedioVelExceso  decimal,
		@vdate_time1  DATETIME,
		@vLatitude1  FLOAT,
		@vLongitude1   FLOAT,
		@vdate_time2  DATETIME,
		@vLatitude2  FLOAT,
		@vLongitude2   FLOAT,
		@ActualSpeed1 FLOAT,
		@ActualSpeed2 FLOAT,
		@resultdistance FLOAT,
		@DistanceOverSpeedLimitTotal FLOAT,
		@Contador INT,
  		@Regs INT,
  		@OdometerMin float,
  		@OdometerMax float,
		@OdometerMinTOTAL float,
  		@OdometerMaxTOTAL float,
  		@DistanceTotal float,
		@IndVeloLimite Float,
		@Vehicle int,
		@pScore float,
		@pOverSpeedDistancePercentage float
	         
--Carga de los excesos para todos los vehiculos que tenia asignado el chofer  tomando en cuenta la fecha de asiginacion del vehiculo
--Lo  rangos de fechas ya debe de venir ajustados por lo que no importa el orden que se llamen

SELECT  @DeviceSpeedId  = min(DeviceSpeedId) FROM  @ListEvaluate 

SET @DistanceOverSpeedLimitTotal  =0
SET @DistanceTotal =0
SET @Contador = 1 
SET @OdometerMaxTOTAL = 0
SET @OdometerMinTOTAL = 0

Select @ActualStartDateTime = Min(StartDate),  
	   @ActualEndDateTime = Max(EndDate) 
	FROM  @ListEvaluate  

--Si la fecha de inicio de asignaci�n del veh�culo es menor a la fecha inicial del reporte, entonces se usa la fecha inicial del reporte
If @ActualStartDateTime < @pStartDate
	Set @ActualStartDateTime = @pStartDate
--Si la fecha final de asignaci�n del veh�culo es mayor a la fecha final del reporte, entonces se usa la fecha final del reporte
If @ActualEndDateTime > @pEndDate
	Set @ActualEndDateTime = @pEndDate	

--RECORRIDO POR D�A		
DECLARE @loopStartDate DATETIME = @ActualStartDateTime 

--- Obtiene  todos los exesos  de velocidad para ese  vehiculo entre esas  fechas.
While @loopStartDate <= @ActualEndDateTime 
Begin 
	DECLARE @loopEndDate DATETIME = DateAdd(Day, 1, @loopStartDate)
	--Select @loopStartDate, @loopEndDate

	While   @DeviceSpeedId   is not null 
	BEGIN
		--- El Device   es  en realidad el   vehicle por  lo que es    requerido obtener  el Device
		SELECT @ActualDevice = Device 
		FROM  @ListEvaluate  
		where DeviceSpeedId = @DeviceSpeedId

			Insert into  #Speedingtemp
			Select device , Case When @pScoreType = 'A' then AdministrativeSpeed else PermittedSpeed end, --Usar el l�mite administrativo si el tipo de calificaci�n es A, sino usar el l�mite de carretera 
				   0, ActualSpeed, Date_Time, Longitude, Latitude, report, PointID, AzimuthDiff, Azimuth, Odometer, Heading, Distance
			  FROM dbo.Speeding  
			 where device = @ActualDevice  
			   and Date_Time >= @loopStartDate 
			   and Date_Time < @loopEndDate  
			   and ActualSpeed > Case When @pScoreType = 'A' then AdministrativeSpeed else PermittedSpeed end
			 order by Date_Time  ASC

			  -- Extraemos el Maximo Odometro y el Minimo Odometro de de todo el d�a
		Select @OdometerMin = IsNull(min(Odometer), 0),  
				@OdometerMax = IsNull(Max(Odometer), 0) 
			FROM Reports  
			WHERE GPSDateTime >= @loopStartDate--@ActualStartDateTime 
			and GPSDateTime <=  @loopEndDate --@ActualEndDateTime 
			and Device = @ActualDevice
			and Odometer > 0
	
		--SELECT @OdometerMax, @OdometerMin, @DistanceTotal, @loopStartDate, @loopEndDate	   
		Set @DistanceTotal +=  @OdometerMax - @OdometerMin --@DistanceTotal +(  @OdometerMax-  @OdometerMin)

		SET @OdometerMinTOTAL += @OdometerMin  -- Acumula la sumatoria de todos los Odometros Minimos
		SET @OdometerMaxTOTAL += @OdometerMax  -- Acumula la sumatoria de todos los Odometros Maximos

		---- Siguiente iteraci�n  DeviceSpeedId > al Minimo
		SELECT @DeviceSpeedId = min(DeviceSpeedId)FROM  @ListEvaluate where  DeviceSpeedId >  @DeviceSpeedId
			 
	 END  --- END While DeviceSpeedId

	-- ASIGNAMOS Vec = Velocidad en Exceso y Ce = Cantidad en Exceso
	--AGRUPAMOS LAS VELOCIDADES DE EXCESO POR D�A, HORA Y MINUTO
	INSERT INTO @TmpTableSpeeding([device], [ActualSpeed], [PermittedSpeed], [Odometer], [Date], [Hour], [Minute])
	SELECT device, ActualSpeed, SpeedLimit, Odometer, Cast(Date_Time AS DATE), DATEPART(HOUR, Date_Time),  DATEPART(MINUTE, Date_Time) 
	FROM #Speedingtemp
	GROUP BY device, ActualSpeed, SpeedLimit, Odometer, Cast(Date_Time AS DATE), DATEPART(HOUR, Date_Time),  DATEPART(MINUTE, Date_Time) 
	ORDER BY  Cast(Date_Time AS DATE)  ASC, DATEPART(HOUR, Date_Time) ASC, DATEPART(MINUTE, Date_Time) ASC 
	
	--EXTRAER LOS EXCESOS DE VELOCIDAD ENTRE UN LAPSO DE CADA 5 MINUTOS DURANTE EL D�A RECORRIDO
	DECLARE @HOURMin INT
	DECLARE @HOURMax INT
	DECLARE @MINUTEMin INT
	DECLARE @MINUTEMax INT

	SELECT @HOURMin = MIN([Hour]),
		   @HOURMax = MAX([Hour])
	FROM @TmpTableSpeeding	

	WHILE @HOURMin <= @HOURMax -- RECORRE CADA HORA DEL D�A RECORRIDO
	BEGIN
		
		SELECT @MINUTEMin = MIN([Minute]),
			   @MINUTEMax = MAX([Minute])
		FROM @TmpTableSpeeding	
		WHERE [Hour] = @HOURMin

		WHILE @MINUTEMin <= @MINUTEMax  -- RECORRE CADA MINUTO DEL D�A Y HORA RECORRIDO
		BEGIN
			
			DECLARE @MinuteFini INT = @MINUTEMin + 5 -- CADA 5 MINUTOS EXTRAE UN EXCESO DE VELOCIDAD
			
			INSERT INTO @TmpTableSpeedingFINAL
			SELECT TOP 1 *
			FROM @TmpTableSpeeding	
			WHERE [Hour] = @HOURMin AND
				  [Minute] >= @MINUTEMin AND [Minute] <= @MinuteFini

			SET @MINUTEMin = @MinuteFini

		END -- FIN RECORRIDO CADA MINUTO DEL D�A Y HORA RECORRIDO

		SET @MINUTEMin = 0
		SET @MINUTEMax = 0

		SELECT @HOURMin = MIN([Hour])
		FROM @TmpTableSpeeding	
		WHERE [Hour] > @HOURMin

	END -- FIN RECORRIDO CADA HORA DEL D�A

	SET @HOURMin = 0
	SET @HOURMax = 0

	-- ASIGNAMOS LA VELOCIDAD EN EXCESO (Ve) Y LA CANTIDAD DE EXCESOS (Ce)
	SELECT  @CantExcesoVel = count(*), --- Nce
		@SumVelocidadExceso = SUM(ActualSpeed - [PermittedSpeed])
		--@DistanceOverSpeedLimitTotal = SUM(Distance)
	FROM  @TmpTableSpeedingFINAL
	
	--////////////////////////////////////////////////////////////////////

	-- RECORRIDO EN EXCESO /////////////////////////////
	DECLARE @TmpSumOdometerExcess AS TABLE(
		device int,
		Odometer float
	)
	
	INSERT INTO @TmpSumOdometerExcess(device, Odometer) 
	SELECT device, MAX(Odometer) - MIN(Odometer)  -- AGRUPAMOS LAS DISTANCIAS RECORRIDAS EN EXCESO POR DEVICE
	FROM  @TmpTableSpeedingFINAL
	GROUP BY device

	SELECT @DistanceOverSpeedLimitTotal = SUM(Odometer)
	FROM @TmpSumOdometerExcess

	DELETE FROM @TmpSumOdometerExcess
	--//////////////////////////////////////////////

	--SELECT @CantExcesoVel, @SumVelocidadExceso, @DistanceOverSpeedLimitTotal, @DistanceTotal

	-- LIMPIAMOS LAS TEMPORALES
	DELETE FROM @TmpTableSpeeding  
	DELETE FROM @TmpTableSpeedingFINAL

	--//////////////////////////////////////////////////////////////////////
	
	If @CantExcesoVel = 0   -- Validamos si tiene excesos de velocidad
	BEGIN
		If @DistanceTotal > 0
			--No hay excesos de velocidad pero si hay distancia recorrida
			Set @pScore = 100	
			-------------------------------------------
		ELSE
			--No hay excesos de velocidad ni distancia recorrida
			Set @pScore = null	
			-------------------------------------------
	END
	ELSE   -- Si tiene excesos de velocidad aplicamos la formula para la calificaci�n
	BEGIN
		--  Promedio de velocidad en  exeso sobre limite  XVc =SUM (VEC)/Nce
		Declare @Xvc FLOAT
		SET @Xvc =@SumVelocidadExceso / @CantExcesoVel
		-----------------------------------------------

		 -- Update Variance de la tabla temporal 
		DECLARE @SpeedingId int = 0
		SELECT @SpeedingId = MIN(SpeedingId) FROM #Speedingtemp

		WHILE @SpeedingId IS NOT NULL
		BEGIN
			
			UPDATE #Speedingtemp
			SET Variance = Power((ActualSpeed - SpeedLimit)-@Xvc, 2)
			WHERE SpeedingId = @SpeedingId

			---- Siguiente iteraci�n  @SpeedingId > al Minimo
			SELECT @SpeedingId = min(SpeedingId)FROM #Speedingtemp WHERE SpeedingId > @SpeedingId
		END
		--------------------------------------------------

		--Indice sobre  recorrido sobre limite Irc= SUM(Dec)/SUM(DT)  osea suma Distancia velocidad en exeso/ suma Distancia en exeso Total
		Declare @Irc FLOAT
		IF @DistanceTotal = 0
			SET @Irc = 0
		ELSE
			SET @Irc = @DistanceOverSpeedLimitTotal / @DistanceTotal 
		------------------------------------------------------------

		--Indice Ponderado de velocidad sobre limite de carretera.
		Declare @Ipc FLOAT
		SET @Ipc = @Xvc  * @Irc  
		--Select 'Distacia Total:', @DistanceTotal , 'Distacia exeso:',  @DistanceOverSpeedLimitTotal ,'sum velocidad execeso',  @SumVelocidadExceso , 'Promedio exceso:', @Ipc
		------------------------------------------------------------
		
		Declare @Spc float-- Desviaci�n est�ndar
		Declare @sumvarianza  float = 0
		Declare @S2 float --Varianza 
		Declare @CV FLOAT --CV= coeficinete de   variacion= velocidad sobre  limite de  carretera.
		
		---Desviacion estandar= Velocidad sobre  limite de  carretera | Hacerlo por medio de un SUM
		SELECT @sumvarianza = SUM(Variance) --SUM(Power((ActualSpeed - SpeedLimit)-@Xvc, 2))
		FROM #Speedingtemp
		----------------------------------------------

		-- CALCULO PARA LA Varianza 
		Set @sumvarianza = ISNULL(@sumvarianza, 0)
		set @S2 = @sumvarianza / @CantExcesoVel
        ----------------------------------------------
						
		--Desviacion estadar = velocida sobre limite de carretera
		-- La raiz  cuadrada de Para  cada  (velocidad en exceso X)  resto (elpromedio de   velocida en exceso XVC) elevado a  2 entre la  cantidad de  veces que  dio  velocidad  @CantExcesoVel
		set @Spc = SQRT(@S2)
		-----------------------------------------------
				
		--CV= coeficinete de   variacion= velocidad sobre  limite de  carretera.
		SET @CV= @Spc / @Xvc
		-----------------------------------------------

		--F�rmula de calificaci�n
		SELECT  @pScore  =ROUND(((99) * RAND() + 1), 0)
		Set @pScore = 100 - (@Irc * 100) - @Xvc - @Spc
		------------------------------------------------
				    
		-- DATOS QUE SE DEVOLVER�AN EN EL XML POR CADA D�A
			--SELECT  @OdometerMax AS MAX_ODO,
			--       @OdometerMin AS MIN_ODO,
			--	   @CantExcesoVel  AS CANT_EXC, 
			--	   @SumVelocidadExceso  AS VEL_EXC, 
			--	   @DistanceOverSpeedLimitTotal AS DIST_OVER_SPEED, 
			--	   @DistanceTotal AS DIST_TOTAL,
			--	   @Xvc AS XVC,
			--	   @Irc AS IRC,
			--	   @Ipc AS IPC,
			--	   @sumvarianza AS VARIANZA,
			--	   @Spc AS SPC,
			--	   @CV AS CV,
			--	   @pScore AS SCORE
	END
	 
	-- Calculamos el promedio para la velocidad en exceso
	SET @PromedioVelExceso=@SumVelocidadExceso/@CantExcesoVel
	-----------------------------------------------

	-- Insertamos los datos del d�a con su calificaci�n
	INSERT INTO #pOutput VALUES
	(@DistanceTotal,
		@SumVelocidadExceso,
		@CantExcesoVel, 
		@Irc/100,
		@PromedioVelExceso, 
		@Ipc,
		@Spc,
		@S2,
		--@sumvarianza,
		@CV,
		@pScore,
		@DistanceOverSpeedLimitTotal,
		@loopStartDate,
		@OdometerMaxTOTAL,--@OdometerMax,
		@OdometerMinTOTAL,--@OdometerMin,
		@SumVelocidadExceso,
		@DistanceTotal,
		@DistanceOverSpeedLimitTotal,
		@CantExcesoVel)
	

	--SELECT @loopStartDate, @Xvc as XVC
	--SELECT * FROM #Speedingtemp

	-- Set iNFO a 0 para cada d�a
	SET @DistanceTotal = 0
	SET @OdometerMaxTOTAL = 0
	SET @OdometerMinTOTAL = 0

	-- Set Variables para Calculo para el siguiente d�a
	SET @CV = 0
	SET @Spc = 0
	SET @S2 = 0
	SET @Ipc = 0
	SET @Irc = 0
	SET @Xvc = 0

	-- Limpiamos tabla temporal para la siguiente iteraci�n
	DELETE #Speedingtemp

	-- Set DeviceSpeedId para recorrer de nuevo los Devices para otro d�a
	SELECT  @DeviceSpeedId  = min(DeviceSpeedId) FROM  @ListEvaluate 

	-- Siguiente iteraci�n D�a + 1
	Set @loopStartDate = DateAdd(Day, 1, @loopStartDate) 

End ---While LoopDate

--SELECT * FROM #pOutput

-- LISTA XML QUE SE DEVOLVER�A PARA CADA D�A
Set @pScoreTable=(SELECT * FROM #pOutput  AS Lista FOR XML AUTO)
	
--SELECT @pScoreTable 

Drop table #pOutput
Drop table #Speedingtemp

END --End  Stored