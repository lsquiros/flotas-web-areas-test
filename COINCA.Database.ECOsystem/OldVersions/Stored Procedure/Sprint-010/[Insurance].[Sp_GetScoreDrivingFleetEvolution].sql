USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Insurance].[Sp_GetScoreDrivingFleetEvolution]    Script Date: 8/7/2015 12:17:55 PM ******/
DROP PROCEDURE [Insurance].[Sp_GetScoreDrivingFleetEvolution]
GO

/****** Object:  StoredProcedure [Insurance].[Sp_GetScoreDrivingFleetEvolution]    Script Date: 8/7/2015 12:17:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ================================================================================================
-- Author:		Andr�s Oviedo
-- Create date: 12/02/2015
-- Description:	Retrieve Score Driving Fleet Evolution information
-- Modified By:	Kevin Pe�a
-- Create date: 06/08/2015
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_GetScoreDrivingFleetEvolution] 
( @pPartnerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)=null,	--Posibles valores S:Summarized, D:Detailed
  @pMonthsQuantity int,
  @pCustomerId int=Null
)
AS BEGIN

DECLARE @tableCustomers TABLE (CustomerId int, Name varchar(250)) 
DECLARE  @li_CustomerId INT
DECLARE @NameCustomer varchar(250)
DECLARE @tableDriversScore TABLE(
DriverScoreId int
,UserId	int
,Name  varchar (250)
,Identification  varchar(50)	
,ScoreType	 varchar(5)
,Score Float
,Photo varchar(max)
,ScoreSpeedAdmin Float
,ScoreSpeedRoute Float
)

DECLARE @ScoreDrivingFleet TABLE(
CustomerId int
,EncryptedName	varchar (1000)
,AverageScore  FLOAT
,[Month] INT
,[Year] INT

)
	Declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT('0' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end
		
DECLARE @Dates TABLE(
	Id INT
	,d_month int
	,d_year	int
)

SET @pMonthsQuantity=@pMonthsQuantity-1

while @pMonthsQuantity>0
begin
	declare @pM INT =MONTH(DATEADD(mm,-@pMonthsQuantity,@pInitialDate))
	declare @pY INT =YEAR(DATEADD(mm,-@pMonthsQuantity,@pInitialDate))
	
	insert @Dates values(@pMonthsQuantity+1,@pM,@pY)
	set @pMonthsQuantity=@pMonthsQuantity-1
	
end

insert into @Dates values(1,MONTH(@pInitialDate),YEAR(@pFinalDate))

INSERT INTO @tableCustomers
EXEC  [Insurance].[Sp_GetRosterInsurance]
		@pPartnerId,@pCustomerId
---SELECT * from @tableCustomers 

DECLARE @count INT=(select count(*) from @Dates)

WHILE @count>0
begin

Select @li_CustomerId = MIN(CustomerId)
from @tableCustomers

WHILE @li_CustomerId is not null
begin

declare @MONTH INT=(select d_month from @Dates where id=@count)
declare @YEAR INT=(select d_year from @Dates where id=@count)


IF(@pReportType = 'R') --ScoreSpeedRoute
   BEGIN
     
	 SELECT @NameCustomer = Name FROM  @tableCustomers where CustomerId=@li_CustomerId
     INSERT INTO @ScoreDrivingFleet --(CustomerId,EncryptedName,AVG(Score) as AverageScore,@MONTH,@YEAR)
     SELECT @li_CustomerId,@NameCustomer as EncryptedName,AVG(ds.Score) as AverageScore,@MONTH,@YEAR
     FROM  [Operation].[DriversScoresDaily] ds, [General].[Customers] c
     WHERE c.CustomerId = ds.CustomerId AND c.CustomerId = @li_CustomerId AND ds.ScoreType ='SPROU' AND ds.ScoreMonth = @MONTH AND ds.ScoreYear = @YEAR
	 END
IF(@pReportType = 'S' OR @pReportType ='D') -- Procedimiento Original
   BEGIN
     INSERT into   @tableDriversScore  (DriverScoreId,UserId,Name,Identification,ScoreType,Score,Photo ,ScoreSpeedAdmin,ScoreSpeedRoute)
     EXEC [Operation].[Sp_GetGlobalScoresByMonthOrDays]
		@li_CustomerId ,
		@YEAR ,
		@MONTH ,
		NULL ,
		NULL ,
		@pReportType

      Select @NameCustomer = Name FROM  @tableCustomers where CustomerId=@li_CustomerId
      
	  insert into @ScoreDrivingFleet SELECT @li_CustomerId CustomerId, @NameCustomer  as EncryptedName ,
      CASE  WHEN AVG(Score) IS NULL THEN 0 ELSE AVG(Score)  END AS  AverageScore,@MONTH,@YEAR
      FROM  @tableDriversScore 
      Delete  FROM  @tableDriversScore 
   END

-----------------------------
--Siguiente Customer
		Select @li_CustomerId = MIN(CustomerId)
		  from @tableCustomers 
		 where CustomerId > @li_CustomerId
END  -- End While
set @count=@count-1
end

select * from @ScoreDrivingFleet group by CustomerId,EncryptedName,AverageScore,[Month],[Year]
END




GO


