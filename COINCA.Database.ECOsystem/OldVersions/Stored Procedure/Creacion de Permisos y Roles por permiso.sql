USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_AddPermissionsByRole]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_AddPermissionsByRole]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 1/25/2016
-- Description:	Add Permissions by Roles
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_AddPermissionsByRole] 
(
	 @pName VARCHAR(100) 				
	,@pResource VARCHAR(100) 							
	,@pAction VARCHAR(100) 				
	,@pDescription VARCHAR(200) = NULL		
	,@pRole VARCHAR(200) = NULL						    
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	IF @pRole IS NULL
	BEGIN
		PRINT 'MUST CONTAIN A ROLE OR A PROFILE TO CONTINUE'
	END
	ELSE
	BEGIN
			--Variables
			DECLARE @LastId VARCHAR(200) 
			DECLARE @RoleId VARCHAR(200)

			--Insert the permission in the database 
			INSERT INTO [dbo].[AspNetPermissions] VALUES (NEWID(), @pName, @pResource, @pAction, @pDescription, GETDATE())
	
			
			--Get the last Id added in the database
			SET @LastId = (SELECT TOP 1 Id FROM [dbo].[AspNetPermissions] ORDER BY [InsertionDate] DESC)

			PRINT '--Save the permission'
			PRINT 'INSERT INTO [dbo].[AspNetPermissions] VALUES (''' + @LastId + ''', ''' + @pName +''' , ''' + @pResource + ''' , ''' + @pAction + ''' , ''' + @pDescription + ''', GETDATE())'

			--********************************************************************************************
			SET  @RoleId = (SELECT [Id] FROM [dbo].[AspNetRoles] WHERE [Name] = @pRole)

			INSERT  INTO [dbo].[AspNetRolePermissions] VALUES  (@RoleId,  @LastId)		
				
			PRINT '--Save the permission by Role'
			PRINT 'INSERT INTO [dbo].[AspNetRolePermissions] VALUES  (''' + @RoleId + ''',  '''+ @LastId + ''')'
			
			IF @pRole = 'CUSTOMER_ADMIN'
			BEGIN
				PRINT 'INSERT INTO [dbo].[AspNetRolePermissions] VALUES  (''F024C5BD-C5F4-408C-8A86-B5DBA55435E0'',  '''+ @LastId + ''')'
			END

			IF @pRole = 'PARTNER_ADMIN'
			BEGIN
				PRINT 'INSERT INTO [dbo].[AspNetRolePermissions] VALUES  (''10703CAB-999E-4293-A423-28C94620EE6D'',  '''+ @LastId + ''')'
			END

			IF @pRole = 'SUPER_ADMIN'
			BEGIN
				PRINT 'INSERT INTO [dbo].[AspNetRolePermissions] VALUES  (''21C5EB03-6C0D-4B16-8F0A-A10C994A8157'',  '''+ @LastId + ''')'
			END
				
			--********************************************************************************************
	END --END main if 

    SET NOCOUNT OFF
END

GO


