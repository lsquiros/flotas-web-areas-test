USE [ECOsystem]
GO

/****** Object:  StoredProcedure [General].[Sp_ImageServiceStations_AddOrEdit]    Script Date: 7/28/2015 10:29:23 AM ******/
DROP PROCEDURE [General].[Sp_ImageServiceStations_AddOrEdit]
GO

/****** Object:  StoredProcedure [General].[Sp_ImageServiceStations_AddOrEdit]    Script Date: 7/28/2015 10:29:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Kevin Pe�a
-- Create date: 28/07/2015
-- Description:	Insert or Update Service Stations Image from Partners
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ImageServiceStations_AddOrEdit]
(
	 @pPartnerId INT = NULL
	,@pImageStation VARCHAR(MAX) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			BEGIN
	
						UPDATE [General].[Partners]
						SET [StationImage] = @pImageStation						
						WHERE [PartnerId] = @pPartnerId
					END

            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO


