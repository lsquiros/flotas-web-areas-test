USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Customers_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Customers_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Modified By:		Gerald Solano
-- Modified date: 28/07/2015
-- Description:	Insert or Update Customer information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Customers_AddOrEdit]
(
	 @pCustomerId INT = NULL
	,@pName VARCHAR(250)
	,@pCurrencyId INT
	,@pLogo VARCHAR(MAX) = NULL
	,@pAccountNumber VARCHAR(50)=null
	,@pIsActive BIT
	,@pUnitOfCapacityId INT
	,@pIssueForId INT=null
	,@pCreditCardType CHAR(1)=null
	,@pAvailableModules VARCHAR(100)
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
	,@pPartnerId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            
            DECLARE @pCountryId INT =  (SELECT 
											[CountryId]
										FROM [General].[Partners] p 
										WHERE p.[PartnerId] = @pPartnerId)
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			IF (@pCustomerId IS NULL)
			BEGIN
			
				INSERT INTO [General].[Customers]
						([Name]
						,[CurrencyId]
						,[Logo]
						,[IsActive]
						,[UnitOfCapacityId]
						,[IssueForId]
						,[CreditCardType]
						,[InsertDate]
						,[InsertUserId]
						,[CountryId]
						,[AccountNumber]
						,[AvailableModules]
						)
				VALUES	(@pName
						,@pCurrencyId
						,@pLogo
						,@pIsActive
						,@pUnitOfCapacityId
						,@pIssueForId
						,@pCreditCardType
						,GETUTCDATE()
						,@pLoggedUserId
						,@pCountryId
						,@pAccountNumber
						,@pAvailableModules
						)
						
				SET @pCustomerId = SCOPE_IDENTITY()
				INSERT INTO General.CustomersByPartner(CustomerId,PartnerId,IsDefault)VALUES(@pCustomerId,@pPartnerId,1)
				
			END
			ELSE
			BEGIN
				UPDATE [General].[Customers]
					SET  [Name] = @pName
						,[CurrencyId] = @pCurrencyId
						,[Logo] = @pLogo
						,[IsActive] = @pIsActive
						,[UnitOfCapacityId] = @pUnitOfCapacityId
						,[IssueForId] = @pIssueForId
						,[AccountNumber] = @pAccountNumber
						,[CreditCardType] = @pCreditCardType
						,[AvailableModules] = @pAvailableModules
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId						
				WHERE [CustomerId] = @pCustomerId
				  AND [RowVersion] = @pRowVersion
				  
			END
            SET @lRowCount = @@ROWCOUNT
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	
	SELECT @pCustomerId
	
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO