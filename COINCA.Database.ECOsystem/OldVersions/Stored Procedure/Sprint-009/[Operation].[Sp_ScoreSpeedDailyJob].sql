USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operation].[Sp_ScoreSpeedDailyJob]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Operation].[Sp_ScoreSpeedDailyJob]
GO

/****** Object:  StoredProcedure [Operation].[Sp_ScoreSpeedDailyJob]    Script Date: 7/29/2015 3:00:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Created by:	Gerald Solano C.
-- Created date: 08/06/2015 
-- Description: Save Score Driving Daily

CREATE Procedure [Operation].[Sp_ScoreSpeedDailyJob]
AS		
Begin
Declare @ldt_Start_Job datetime,
		@ldt_End_Job datetime,
		@lvc_Job varchar(30),
		@lc_status char(1),
		@li_error int = 0,
		@lvc_error_ref varchar(100),
		@li_CustomerId Int,
		@li_Day Int,
		@li_Month Int,
		@li_Year Int

Declare @Scores table
	(DriverScoreId int,
	 UserId int,
	 Name  varchar (250),
	 Identification varchar(50),
	 Score Float,
	 KmTraveled float,
	 OverSpeed float,
     OverSpeedAmount float,
	 OverSpeedDistancePercentage float,
	 OverSpeedAverage float,
	 OverSpeedWeihgtedAverage float,
	 StandardDeviation float,
	 Variance float,
	 VariationCoefficient float,
	 OverSpeedDistance float,
	 Date_Time datetime,
	 MaxOdometer int,
	 MinOdometer int,
	 Vec float,
	 Dt float,
	 De float,
	 Ce float)

	Set @lvc_Job = 'ScoreSpeedDaily' -- NOMBRE DEL SP EJECUTADO PARA REGISTRAR EN EL LOG DE PROCESOS
	Set @ldt_Start_Job = GETDATE() -- OBTENEMOS D�A ACTUAL
	Set @li_Day = DATEPART(DD, @ldt_Start_Job)  -- EXTRAEMOS D�A
	Set @li_Month = DATEPART(MM, @ldt_Start_Job) -- EXTRAEMOS MES
	Set @li_Year = DATEPART(YY, @ldt_Start_Job) -- EXTRAEMOS A�O
	
	-- OBTENEMOS D�A Y HORA INICIAL Y D�A Y HORA FINAL CON BASE AL D�A ACTUAL
	DECLARE @pInitialDate datetime=  CAST(
										  CAST(@li_Year AS VARCHAR(4)) +
										  RIGHT('0' + CAST(@li_Month AS VARCHAR(2)), 2) +
										  RIGHT('0' + CAST(@li_Day AS VARCHAR(2)), 2)
										  --RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
									AS DATETIME)

	DECLARE @pFinalDate datetime= DATEADD(day, 1, @pInitialDate) 
								  --DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0)) 
								  
	--SELECT @pInitialDate, @pFinalDate

	----Borrar registros previos del d�a
	--DELETE Operation.DriversScoresDaily
	--WHERE 
	--   ScoreDay = @li_Day and
	--   ScoreMonth = @li_Month
	--   and ScoreYear = @li_Year
	   
	Select @li_CustomerId = MIN(D.CustomerId)
	  from General.DriversUsers D
	  inner join General.Users U on D.UserId = U.UserId
	 where U.IsActive = 1   
	
	--SET @li_CustomerId = 6

	While @li_CustomerId is not null
	begin
		
		-- ///////////////////////////////////////////////////////////////////////////////////
		--Generar registros de c�lculo por l�mite administrativo (A)
		--////////////////////////////////////////////////////////////////////////////////////
		Insert into @Scores
			(DriverScoreId, 
					UserID, Name, Identification, Score,
					KmTraveled,
					OverSpeed,
					OverSpeedAmount,
					OverSpeedDistancePercentage ,
					OverSpeedAverage ,
					OverSpeedWeihgtedAverage ,
					StandardDeviation ,
					Variance ,
					VariationCoefficient,
					OverSpeedDistance,
					Date_Time,
					MaxOdometer,
					MinOdometer,
					Vec,
					Dt,
					De,
					Ce)
		Exec Operation.Sp_ScoreSpeedDaily @li_CustomerId, null, null, @pInitialDate, @pFinalDate, 'A'
		
		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end
		
		--SELECT 'ADM',* FROM @Scores WHERE UserId = 1223
	    
		-- Borrar registros de calificaci�n de velocidad administrativa ('SPADM') 
		-- previos del d�a del customer actual recorrido y procesado
		DELETE Operation.DriversScoresDaily
		WHERE 
		   ScoreDay = @li_Day 
		   and ScoreMonth = @li_Month
		   and ScoreYear = @li_Year
		   and CustomerId = @li_CustomerId
		   and ScoreType = 'SPADM'
		--//////////////////////////////////////////////////////////////////////////

		-- Inserci�n de datos de calificaci�n de velocidad administrativa ('SPADM')
		Insert into Operation.DriversScoresDaily
			(CustomerId, UserId, ScoreYear, ScoreMonth, ScoreDay, ScoreType, Score,KmTraveled,
						OverSpeed,
						OverSpeedAmount,
						OverSpeedDistancePercentage,
						OverSpeedAverage,
						OverSpeedWeihgtedAverage,
						StandardDeviation,
						Variance,
						VariationCoefficient,
						OverSpeedDistance,
						Date_Time,
						MaxOdometer,
						MinOdometer,
						Vec,
						Dt,
						De,
						Ce)
		Select @li_CustomerId, UserId, @li_Year, @li_Month, DATEPART(DD, Date_Time) , 'SPADM', Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,OverSpeedDistance,
						Date_Time,
						MaxOdometer,
						MinOdometer,
						Vec,
						Dt,
						De,
						Ce
		  from @Scores
		--////////////////////////////////////////////////////////////////////////////////////////////////////////
		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end

		Delete @Scores
		-- ///////////////////////////////////////////////////////////////////////////////////


		-- ///////////////////////////////////////////////////////////////////////////////////
		--Generar registros de c�lculo por l�mite de carretera (R)
		--////////////////////////////////////////////////////////////////////////////////////
		Insert into @Scores
			(DriverScoreId, 
					UserID, Name, Identification, Score,
					KmTraveled,
					OverSpeed,
					OverSpeedAmount,
					OverSpeedDistancePercentage ,
					OverSpeedAverage ,
					OverSpeedWeihgtedAverage ,
					StandardDeviation ,
					Variance ,
					VariationCoefficient,
					OverSpeedDistance,
					Date_Time,
					MaxOdometer,
					MinOdometer,
					Vec,
					Dt,
					De,
					Ce)
		Exec Operation.Sp_ScoreSpeedDaily @li_CustomerId, null, null, @pInitialDate, @pFinalDate, 'R'
		
		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end
		
		--SELECT 'ROUTE',* FROM @Scores WHERE UserId = 1223

		-- Borrar registros de calificaci�n de velocidad ruta ('SPROU') 
		-- previos del d�a del customer actual recorrido y procesado
		DELETE Operation.DriversScoresDaily
		WHERE 
		   ScoreDay = @li_Day 
		   and ScoreMonth = @li_Month
		   and ScoreYear = @li_Year
		   and CustomerId = @li_CustomerId
		   and ScoreType = 'SPROU'
		--//////////////////////////////////////////////////////////////////////////

		-- Inserci�n de datos de calificaci�n de velocidad ruta ('SPROU')
		Insert into Operation.DriversScoresDaily
			(CustomerId, UserId, ScoreYear, ScoreMonth, ScoreDay, ScoreType, Score,KmTraveled,
						OverSpeed,
						OverSpeedAmount,
						OverSpeedDistancePercentage,
						OverSpeedAverage,
						OverSpeedWeihgtedAverage,
						StandardDeviation,
						Variance,
						VariationCoefficient,
						OverSpeedDistance,
						Date_Time,
						MaxOdometer,
						MinOdometer,
						Vec,
						Dt,
						De,
						Ce)
		Select @li_CustomerId, UserId, @li_Year, @li_Month, DATEPART(DD, Date_Time), 'SPROU', Score,KmTraveled,
						OverSpeed ,
						OverSpeedAmount ,
						OverSpeedDistancePercentage ,
						OverSpeedAverage ,
						OverSpeedWeihgtedAverage ,
						StandardDeviation ,
						Variance ,
						VariationCoefficient,OverSpeedDistance,
						Date_Time,
						MaxOdometer,
						MinOdometer,
						Vec,
						Dt,
						De,
						Ce
		  from @Scores
		-- //////////////////////////////////////////////////////////////////////////////////////////////////////
		
		Set @li_error = @@ERROR
		If @li_error <> 0 
		begin
			Break
		end

		Delete @Scores
		-- ///////////////////////////////////////////////////////////////////////////////////

		--Siguiente Customer
		Select @li_CustomerId = MIN(D.CustomerId)
		  from General.DriversUsers D
		  inner join General.Users U on D.UserId = U.UserId
		 where U.IsActive = 1   
		   and D.CustomerId > @li_CustomerId

	END -- TERMINA WHILE


	If @li_error <> 0 
	BEGIN
		Set @lc_status = 'E' --Error
		Set @lvc_error_ref = CONCAT('Customer Id: ', @li_CustomerId)
	END
	ELSE
	BEGIN
		Set @lc_status = 'T' --Terminado		
	END

	--Insertar datos en Log ///////////////////////////////////////////////////////////
	Set @ldt_End_Job = GETDATE()

	INSERT INTO Operation.JobsLog
		(JobID, StartDate, EndDate, Status, ErrorNumber, ErrorReference)
	VALUES
		(@lvc_Job, @ldt_Start_Job, @ldt_End_Job, @lc_status, @li_error, @lvc_error_ref)
		
END



GO


