USE [ECOsystem]
GO

/****** Object:  StoredProcedure [dbo].[Sp_GetDriverScoreData]    Script Date: 7/30/2015 9:14:51 AM ******/
DROP PROCEDURE [dbo].[Sp_GetDriverScoreData]
GO

/****** Object:  StoredProcedure [dbo].[Sp_GetDriverScoreData]    Script Date: 7/30/2015 9:14:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 ----================================================================================================
 --Author:		Gerald Solano C.
 --Create date: 06/19/2015
 --Description:	Retrieve Driver Scores from Months or days
 ----================================================================================================
CREATE PROCEDURE [dbo].[Sp_GetDriverScoreData]
(
	@pUserId int,
	@pType Char(1) = null, -- M - Month, D- Day
	@pConsultDate datetime = null
)
AS
BEGIN
	
	IF(@pConsultDate IS NULL)
	BEGIN
		SET @pConsultDate = GETDATE()
	END

	Declare @lInitDateForDays datetime = DATEADD(day, -4, @pConsultDate) 
    Declare @lCurrentDay int = DATEPART(DD, @pConsultDate)
	Declare @lCurrentMonth int = DATEPART(MM, @pConsultDate)
    Declare @lCurrentYear int = DATEPART(YY, @pConsultDate)

	-- ////// Obtenemos los datos de los �ltimos 5 d�as a partir del d�a actual
	IF(@pType = 'D')
	BEGIN
		SELECT  Concat([ScoreDay], '/',[ScoreMonth], '/', [ScoreYear]) AS DayOrMonth,
				 SUM([Score]) / COUNT(*) AS Score
		FROM [Operation].[DriversScoresDaily]
		WHERE [UserId] = @pUserId AND
			  [Date_Time] >= @lInitDateForDays AND 
			  [Date_Time] <= @pConsultDate AND [Score] > 0
		GROUP BY [UserId], [ScoreDay], [ScoreMonth], [ScoreYear]
	END
	-- ////// Obtenemos los datos de los �ltimos 3 Meses a partir del mes actual
	ELSE
	BEGIN
		SELECT Concat([ScoreMonth], '/', [ScoreYear]) AS DayOrMonth,
			   SUM([Score]) / COUNT(*) AS Score
		FROM [Operation].[DriversScores]
		WHERE [UserId] = @pUserId AND
			  [ScoreYear] = @lCurrentYear AND
			  [ScoreMonth] >= (@lCurrentMonth - 3) AND [ScoreMonth] <= @lCurrentMonth
		GROUP BY [UserId], [ScoreMonth], [ScoreYear]
	END
END --End  Stored

GO


