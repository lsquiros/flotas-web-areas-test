DROP PROCEDURE [General].[Sp_FixUsers_AddOrEdit]

CREATE PROCEDURE [General].[Sp_FixUsers_AddOrEdit]
(
	@pOldUserName VARCHAR(250),
	@pNewUserName VARCHAR(250),
	@pUserEmail VARCHAR(250)
)
AS
BEGIN
	UPDATE [dbo].[AspNetUsers]
		SET  [Email] = @pUserEmail
			,[UserName] = @pNewUserName
	WHERE [UserName] = @pOldUserName

	SELECT 1
END
