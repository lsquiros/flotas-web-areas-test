USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_GetWightedIndexScoreDrivingFleet]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Insurance].[Sp_GetWightedIndexScoreDrivingFleet]
GO

/****** Object:  StoredProcedure [Insurance].[Sp_GetWightedIndexScoreDrivingFleet]    Script Date: 7/31/2015 11:21:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Andr�s Oviedo
-- Create date: 16/02/2015
-- Description:	Retrieve Wighted Index Score Driving Fleet information
-- ================================================================================================
CREATE PROCEDURE [Insurance].[Sp_GetWightedIndexScoreDrivingFleet] 
( @pPartnerId Int, 
  @pYear int = null,			--Se incluye null en caso  de que el parametro no se agregue
  @pMonth int  = null ,			--Se incluye null en caso  de que el parametro no se agregue
  @pStartDate datetime  = null, --Se incluye null en caso  de que el parametro no se agregue
  @pEndDate datetime = null,	--Se incluye null en caso  de que el parametro no se agregue
  @pReportType char(1)=null,	--Posibles valores S:Summarized, D:Detailed
  @pCustomerId int =null
)
AS BEGIN

DECLARE @tableCustomers TABLE (CustomerId int, Name varchar(250)) 
DECLARE  @li_CustomerId INT
DECLARE @NameCustomer varchar(250)
DECLARE @tableDriversScore TABLE(
DriverScoreId int
,UserId	int
,Name  varchar (250)
,Identification  varchar(50)	
,ScoreType	 varchar(5)
,Score Float
,Photo varchar(max)
,ScoreSpeedAdmin Float
,ScoreSpeedRoute Float
)

DECLARE @performanceTable TABLE(
VehicleId INT,
PlateId VARCHAR(300),
[TrxMinDate] datetime,
[TrxMaxDate] datetime,
[TrxMinOdometer] INT,
[TrxMaxOdometer] INT,
CustomerId int
)

DECLARE @ScoreDrivingFleet TABLE(
CustomerId int
,EncryptedName	varchar (1000)
,SumScore  FLOAT
)

INSERT INTO @tableCustomers
EXEC  [Insurance].[Sp_GetRosterInsurance]
		@pPartnerId,@pCustomerId
---SELECT * from @tableCustomers 

Select @li_CustomerId = MIN(CustomerId)
from @tableCustomers

WHILE @li_CustomerId is not null
BEGIN

INSERT INTO @performanceTable exec [Control].[Sp_OdometerTraveledReportByVehicle_Retrieve] @li_CustomerId,@pYear,@pMonth,@pStartDate ,@pEndDate 

INSERT into   @tableDriversScore  (DriverScoreId,UserId,Name,Identification,ScoreType,Score,Photo ,ScoreSpeedAdmin,ScoreSpeedRoute)
--EXEC	 [Operation].[Sp_GetGlobalScoreAllDriver]
EXEC [Operation].[Sp_GetGlobalScoresByMonthOrDays]
		@li_CustomerId ,
		@pYear ,
		@pMonth ,
		@pStartDate ,
		@pEndDate ,
		@pReportType

Select @NameCustomer = Name FROM  @tableCustomers where CustomerId=@li_CustomerId

insert into @ScoreDrivingFleet SELECT @li_CustomerId CustomerId, @NameCustomer  as EncryptedName ,CASE  WHEN SUM(Score) IS NULL THEN 0 ELSE SUM(Score)  END AS  SumScore
FROM  @tableDriversScore 
Delete  FROM  @tableDriversScore 

-----------------------------
--Siguiente Customer
		Select @li_CustomerId = MIN(CustomerId)
		  from @tableCustomers 
		 where CustomerId > @li_CustomerId
END  -- End While

select t.*,
	  (t.KmTraveled/(CASE WHEN t.SumScore IS NULL OR t.SumScore=0 then 1 else t.SumScore end))  as WightedIndex 
	from	
	   (select sdf.*,
			(select sum(pt.[TrxMaxOdometer]-pt.[TrxMinOdometer]) 
				from @performanceTable pt 
				where sdf.CustomerId=pt.CustomerId) as KmTraveled 
	       from @ScoreDrivingFleet sdf) as t 
		   order by WightedIndex
END

GO


