USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CostCentersByCredit_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CostCentersByCredit_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_CostCentersByCredit_Retrieve]    Script Date: 07/31/2015 11:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Cost Centers By Credit
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CostCentersByCredit_Retrieve]
(
	 @pCostCenterId INT						--@pCostCenterId: PK of the table VehicleGroup
	,@pYear INT								--@pYear: Year
	,@pMonth INT							--@pMonth: Month	
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	SELECT
		 a.PlateId
		,a.[Name] AS  [VehicleName]
		,b.[Manufacturer] + ' - ' + b.[VehicleModel] AS VehicleModel
		,f.[Symbol] AS [CurrencySymbol]
		,ISNULL(t.[Amount], CONVERT(DECIMAL(16,2),0)) AS [Amount]
		,ISNULL(t.[Liters],0) AS [Liters]
		,a.[VehicleId]
		,b.[DefaultFuelId]
		,c.[Code] + ' - ' + c.[Name] AS CostCenter
		,t.[VehicleFuelId]
		,t.[RowVersion]		
	FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCategories] b
			ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
		INNER JOIN [General].[VehicleCostCenters] c
			ON a.[CostCenterId] = c.[CostCenterId]
		INNER JOIN [General].[Customers] e
			ON e.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON f.[CurrencyId] = e.[CurrencyId]
		LEFT JOIN (
				SELECT 
					x.[VehicleId]
					,x.[Amount]
					,x.[Year]
					,x.[Month]
					,x.[Liters]
					,x.[VehicleFuelId]
					,x.[RowVersion]
				FROM [Control].[VehicleFuel] x	
					INNER JOIN (
						SELECT	VehicleId, max(VehicleFuelID) AS VehicleFuelID
						FROM	[Control].[VehicleFuel]
						WHERE [Year] = @pYear
						  AND [Month] = @pMonth
						GROUP BY VehicleId
					) AS y ON x.VehicleId = y.VehicleId AND x.VehicleFuelID = y.VehicleFuelID
				WHERE x.[Year] = @pYear
				  AND x.[Month] = @pMonth) t
			ON t.VehicleId = a.VehicleId
	WHERE c.[CostCenterId] = @pCostCenterId
	  
    SET NOCOUNT OFF
END
