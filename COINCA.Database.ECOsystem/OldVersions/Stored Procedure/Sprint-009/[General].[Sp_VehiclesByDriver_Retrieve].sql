USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByDriver_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesByDriver_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 22/07/2015
-- Description:	Retrieve VehiclesByDriver information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesByDriver_Retrieve]
(
	 @pUserId INT	--@pUserId: PK of the table
	,@pCustomerId INT	--@pCustomerId: FK of Customer
)
AS
BEGIN
	
	SET NOCOUNT ON
	--DECLARE @pUserId INT = 1060
	--DECLARE @pCustomerId INT = 10

	SELECT 
		NULL AS [UserId],
		v.[VehicleId] AS [VehicleId],
		v.[Name] AS [VehicleName],
	    v.[PlateId] AS [Plate] 
	FROM  [General].[Vehicles] v
	LEFT JOIN [General].[VehiclesDrivers] vd
		ON vd.[VehicleId] = v.[VehicleId]
	WHERE vd.[UserId] IS NULL AND 
		  v.[CustomerId] = @pCustomerId AND
		  v.[Active] = 1
		
	UNION

	SELECT 
		NULL AS [UserId],
		v.[VehicleId] AS [VehicleId],
		v.[Name] AS [VehicleName],
	    v.[PlateId] AS [Plate] 
	FROM   [General].[Vehicles] v
	INNER JOIN [General].[VehiclesDrivers] vd
		ON vd.[VehicleId] = v.[VehicleId]
	WHERE  vd.[UserId] <> @pUserId AND 
		   v.[CustomerId] = @pCustomerId AND
		   v.[Active] = 1 AND
		   vd.[VehicleId] NOT IN (
				SELECT 
					v.[VehicleId]
				FROM   [General].[Vehicles] v
				INNER JOIN [General].[VehiclesDrivers] vd
					ON vd.[VehicleId] = v.[VehicleId]
				WHERE  vd.[UserId] = @pUserId AND 
					   v.[CustomerId] = @pCustomerId AND
					   v.[Active] = 1 )
	UNION

	SELECT 
		vd.[UserId] AS [UserId],
		v.[VehicleId] AS [VehicleId],
		v.[Name] AS [VehicleName],
	    v.[PlateId] AS [Plate] 
	FROM   [General].[Vehicles] v
	INNER JOIN [General].[VehiclesDrivers] vd
		ON vd.[VehicleId] = v.[VehicleId]
	WHERE  vd.[UserId] = @pUserId AND 
		   v.[CustomerId] = @pCustomerId AND
		   v.[Active] = 1
	
    SET NOCOUNT OFF
END

GO
