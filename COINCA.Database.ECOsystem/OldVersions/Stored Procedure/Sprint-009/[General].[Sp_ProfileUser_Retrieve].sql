USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ProfileUser_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ProfileUser_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 30/07/2015
-- Description:	Retrieve Profile User information
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ProfileUser_Retrieve]
(
	@pUserId INT
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT u.[Photo]
    FROM [General].[Users] u
	WHERE u.[UserId] = @pUserId
	
    SET NOCOUNT OFF
END
