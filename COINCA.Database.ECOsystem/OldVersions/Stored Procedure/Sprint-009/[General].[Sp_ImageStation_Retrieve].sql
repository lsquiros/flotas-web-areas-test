USE [ECOsystem]
GO

/****** Object:  StoredProcedure [General].[Sp_ImageStation_Retrieve]    Script Date: 7/28/2015 10:52:47 AM ******/
DROP PROCEDURE [General].[Sp_ImageStation_Retrieve]
GO

/****** Object:  StoredProcedure[General].[Sp_ImageStation_Retrieve]   Script Date: 7/28/2015 10:52:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Kevin Pe�a
-- Create date: 28/07/2015
-- Description:	Retrieve Partner ServiceStation Image
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ImageStation_Retrieve]
(
	 @pPartnerId INT=NULL	
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT [StationImage]
    FROM [General].[Partners]
	WHERE [PartnerId] = @pPartnerId
	
    SET NOCOUNT OFF
END

GO


