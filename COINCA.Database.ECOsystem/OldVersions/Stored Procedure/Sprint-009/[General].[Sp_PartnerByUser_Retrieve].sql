USE [ECOsystem]
GO

/****** Object:  StoredProcedure [General].[Sp_PartnerByUser_Retrieve]    Script Date: 7/31/2015 5:30:40 PM ******/
DROP PROCEDURE [General].[Sp_PartnerByUser_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_PartnerByUser_Retrieve]    Script Date: 7/31/2015 5:30:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ================================================================================================
-- Author:		Kevin Pe�a
-- Create date: 31/07/2015
-- Description:	Retrieve Partner from UserId
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PartnerByUser_Retrieve]
(
	 @pUserId INT = NULL	
)
AS
BEGIN

	SET NOCOUNT ON	
		
	SELECT TOP 1 u.[PartnerId]
    FROM [General].[CustomerUsers] p,[General].[CustomersByPartner] u
    WHERE  p.[CustomerId] = u.[CustomerId] AND p.[UserId]=@pUserId
	
    SET NOCOUNT OFF
END



GO


