USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsReportDeniedByPartner_Retrieve]    Script Date: 1/21/2016 2:30:52 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReportDeniedByPartner_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsReportDeniedByPartner_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsReportDeniedByPartner_Retrieve]    Script Date: 1/21/2016 2:30:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





	-- ================================================================================================
	-- Author:		Kevin Pe�a
	-- Create date: 11/02/2015
	-- Description:	Retrieve DeniedTransactionsReport log
	-- ================================================================================================
	CREATE PROCEDURE [Control].[Sp_TransactionsReportDeniedByPartner_Retrieve]
	(
		@pPartnerId INT,					--@pPartnerId: PartnerId		
		@pYear INT = NULL,					--@pYear: Year
		@pMonth INT = NULL,					--@pMonth: Month
		@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
		@pEndDate DATETIME = NULL			--@pEndDate: End Date
		,@pUserId INT						--@pUserId: User Id
	)
	AS
	BEGIN

		SET NOCOUNT ON

		-- DYNAMIC FILTER
		DECLARE	@Results TABLE (items INT)
		DECLARE @count INT
		INSERT	@Results
		EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
		SET		@count = (SELECT COUNT(*) FROM	@Results)
		-- END

IF OBJECT_ID('tempdb..#tmp_transpordata') IS NOT NULL DROP TABLE #tmp_transpordata
IF OBJECT_ID('tempdb..#tmp_CustomerByPartner') IS NOT NULL DROP TABLE #tmp_CustomerByPartner

-- SACAR LA PLACA DE TRANSPORT DATA
Create TABLE #tmp_transpordata  
(
  LogId INT,
  ResponseCode VARCHAR(250),
  ResponseCodeDescription NVARCHAR(MAX),
  [Message] NVARCHAR(MAX),
  TransportData nvarchar(MAX),
  InsertDate datetime,
  CustomerId INT,
  Plate VARCHAR(50),
  CostCenterName VARCHAR(50) DEFAULT 'N/A'
 )

 Create TABLE #tmp_CustomerByPartner  
(
  RowId int identity(1,1) NOT NULL,
  CustomerId INT
 )

INSERT INTO #tmp_CustomerByPartner SELECT [CustomerId] FROM [General].[CustomersByPartner] WHERE PartnerId =@pPartnerId

 declare @TempID int  
 declare @pCustomerId int

 WHILE EXISTS (SELECT * FROM #tmp_CustomerByPartner)
    BEGIN
		SELECT TOP 1 @pCustomerId = [CustomerId],@TempID = [RowId] FROM #tmp_CustomerByPartner
		insert into #tmp_transpordata (LogId,ResponseCode,ResponseCodeDescription,[Message],TransportData,InsertDate,CustomerId)
        SELECT LogId,ResponseCode,ResponseCodeDescription,[Message],TransportData,InsertDate,CustomerId from [Control].[LogTransactionsPOS] 
		where CustomerId = @pCustomerId and ResponseCode =05
        AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL AND DATEPART(m,[InsertDate]) = @pMonth 
		AND DATEPART(yyyy,[InsertDate]) = @pYear) OR (@pStartDate IS NOT NULL AND @pEndDate 
		IS NOT NULL AND [InsertDate] BETWEEN @pStartDate AND DATEADD(day,1,@pEndDate)))

		DECLARE @tmpcount int, @tmpNum int, @tmprow int  =0;

		Select @tmpcount = Count(*) From #tmp_transpordata 

      WHILE ( @tmpNum  < @tmprow)
        BEGIN

		  select @tmprow =  LogId from #tmp_transpordata
						               WHERE ROW_NUMBER()OVER (ORDER BY LogId) = tmpnum

            DECLARE @INI_INDEX INT = CHARINDEX('cartag',(SELECT [TransportData] from #tmp_transpordata   where LogId =@tmprow))+9
            DECLARE @FIN_INDEX INT = CHARINDEX('kilometers',(SELECT [TransportData] from #tmp_transpordata  where LogId =@tmprow))-3

            declare @diff_index int = @FIN_INDEX - @INI_INDEX

            UPDATE #tmp_transpordata 
            SET Plate = SUBSTRING( (SELECT [TransportData] from #tmp_transpordata where LogId =@tmprow) , @INI_INDEX,@diff_index ) 
            WHERE LogId = @tmprow

            DECLARE @tmpPlate VARCHAR(20)
            SELECT @tmpPlate =  Plate from #tmp_transpordata where LogId = @tmprow
            Declare @CCName VARCHAR(50)   
            SELECT @CCName =  cc.[Name] FROM [General].[Vehicles] v 
                               INNER JOIN [General].[VehicleCostCenters] cc
		                               ON v.CostCenterId = cc.CostCenterId 
							   WHERE	(@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER 
										AND CustomerId=@pCustomerId AND PlateId = @tmpPlate
            IF @CCName IS NOT NULL
            BEGIN 
            UPDATE #tmp_transpordata 
            SET CostCenterName = @CCName 
            WHERE LogId = @tmprow
            END
    
	      SELECT @tmpNum = @tmpNum +1;
        END -- enn while
           

   DELETE FROM #tmp_CustomerByPartner WHERE RowId =@TempID 
  END

   
		SELECT 
			   cast(0 as INT) as [TransactionId]
			  ,'00000' AS [SystemTraceCode]
			  ,cast(null as char(10)) as [HolderName]
			  ,cast(null as char(10)) as [Date]
			  ,cast(null as char(10)) as [FuelName]
			  ,cast(0 as decimal) as [FuelAmount]
			  ,cast(0 as int) as [Odometer] 
			  ,cast(0 as decimal) as [Liters]
			  ,cast(null as char(10)) as [PlateId]
			  ,cast(null as char(10)) as [CurrencySymbol]
			  ,'Rechazada' as [State]
			  ,c.[Message]  
			  ,c.[TransportData]
			  ,DATEADD(hour,-6,c.[InsertDate]) AS [InsertDate]
			  ,c.[ResponseCode]
			  ,c.[ResponseCodeDescription]
			  ,c.[CustomerId]
		      ,c.[CostCenterName] AS [CostCenterName]
			  ,cu.Name AS [CustomerName]		 	 
		  FROM #tmp_transpordata c
		  INNER JOIN [General].[Customers] cu
		     ON c.CustomerId = cu.CustomerId

	  ORDER BY [InsertDate] DESC

	DROP TABLE #tmp_transpordata 
	DROP TABLE #tmp_CustomerByPartner
	END
	SET NOCOUNT OFF




GO


