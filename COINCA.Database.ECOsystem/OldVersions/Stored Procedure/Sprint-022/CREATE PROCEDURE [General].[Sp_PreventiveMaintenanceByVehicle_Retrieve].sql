USE [ECOsystem]
GO

/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]    Script Date: 1/19/2016 4:47:53 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]    Script Date: 1/19/2016 4:47:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:		Danilo Hidalgo
-- Create date: 02/23/2015
-- Description:	Preventive Maintenance By Vehicle information
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- ===========================================================
CREATE PROCEDURE [General].[Sp_PreventiveMaintenanceByVehicle_Retrieve]
	@pVehicleId INT
	,@pUserId INT							--@pUserId: User Id
AS
BEGIN 
	SET NOCOUNT ON;

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	SELECT 
		a.[PreventiveMaintenanceId],
		a.[PreventiveMaintenanceCatalogId], 
		b.[Description] [PreventiveMaintenanceDescription],
		a.[VehicleId], 
		a.[PreventiveMaintenanceCatalogId], 
		a.[LastReviewOdometer], 
		a.[NextReviewOdometer], 
		a.[LastReviewDate], 
		a.[NextReviewDate], 
		a.[Registred], 
		a.[AlarmSent], 
		a.[InsertUserId], 
		a.[InsertDate], 
		a.[ModifyUserId], 
		a.[ModifyDate], 
		a.[RowVersion] 
	FROM 
		[General].[PreventiveMaintenanceByVehicle] a
		INNER JOIN [General].[PreventiveMaintenanceCatalog] b ON a.[PreventiveMaintenanceCatalogId] = b.[PreventiveMaintenanceCatalogId]
	WHERE a.[Registred] = 0 AND 
		a.[IsDeleted] = 0 AND 
		a.[VehicleId] = @pVehicleId
		AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	
	SET NOCOUNT OFF;
END

GO


