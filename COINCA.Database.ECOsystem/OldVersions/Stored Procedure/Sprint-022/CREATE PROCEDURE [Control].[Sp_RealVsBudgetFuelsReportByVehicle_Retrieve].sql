USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]    Script Date: 1/19/2016 3:47:31 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]    Script Date: 1/19/2016 3:47:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--~ Commented out USE [database] statement because USE statement is not supported to another database.
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Vehicle information
-- Update 1/22/2016 Melvin Salas - Dinamic Filter
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportByVehicle_Retrieve]
(
	@pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pUserId INT						--@pUserId: User Id
)
AS
BEGIN
	
	SET NOCOUNT ON	

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT
	a.[VehicleId],
			a.[PlateId] AS [PlateNumber]
		,ISNULL(t.[RealAmount],0) AS [RealAmount]
		,s.[AssignedAmount]
		,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) AS [RealAmountPct]
		,@lCurrencySymbol AS [CurrencySymbol]
	FROM [General].[Vehicles] a
		LEFT JOIN (	SELECT	y.[VehicleId]
							,SUM(x.[FuelAmount]) AS [RealAmount]
					FROM	[Control].[Transactions] x
							INNER JOIN [General].[Vehicles] y ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
						AND x.[IsFloating] = 0
						AND x.[IsReversed] = 0
						AND x.[IsDuplicated] = 0
						AND y.[Active] = 1
						AND 1 > (
							SELECT COUNT(*) FROM Control.Transactions t2
								WHERE t2.[CreditCardId] = x.[CreditCardId] AND
								t2.[TransactionPOS] = x.[TransactionPOS] AND
								t2.[ProcessorId] = x.[ProcessorId] AND 
								t2.IsReversed = 1
						)
						AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,x.[Date]) = @pMonth
								AND DATEPART(yyyy,x.[Date]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
					GROUP BY y.[VehicleId])t
			ON a.[VehicleId] = t.[VehicleId]
		INNER JOIN (SELECT
							y.[VehicleId]
						,SUM(x.[Amount]) AS [AssignedAmount]
					FROM [Control].[VehicleFuel] x
						INNER JOIN [General].[Vehicles] y
							ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
						AND y.[Active] = 1
						AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND x.[Month] = @pMonth
								AND x.[Year] = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
								AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
						AND x.[Amount] > 0  -- GSOLANO: Excluye los valores en 0 para evitar la division entre 0
					GROUP BY y.[VehicleId])s
			ON a.[VehicleId] = s.[VehicleId]
			WHERE (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	ORDER BY 4 desc	
	
	SET NOCOUNT OFF
END

GO


