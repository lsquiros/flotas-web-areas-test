USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_VehiclesByGeoFence_Retrieve]    Script Date: 1/20/2016 1:45:22 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehiclesByGeoFence_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_VehiclesByGeoFence_Retrieve]
GO

/****** Object:  StoredProcedure [Efficiency].[Sp_VehiclesByGeoFence_Retrieve]    Script Date: 1/20/2016 1:45:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 11/17/2014
-- Description:	Retrieve VehicleGeoFence information
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_VehiclesByGeoFence_Retrieve]
(
	 @pGeoFenceId INT						--@pGeoFenceId: PK of the table
	,@pCustomerId INT						--@pCustomerId: FK of Customer
	,@pUserId INT							--@pUserId: User Id
)
AS
BEGIN
	
	SET NOCOUNT ON

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	SELECT
		 null AS [GeoFenceId]
		,a.[VehicleId]
		,a.[Name] AS VehicleName
		,b.[Manufacturer] + ' - ' + b.[VehicleModel] + ', ' + c.[Name] + ' ' + CONVERT(varchar(50),b.Liters) + 'L' AS VehicleType
		,a.[PlateId]
		,ROW_NUMBER() OVER (ORDER BY a.[VehicleId]) AS RowNumber
    FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCategories] b
			ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] c
			ON b.[DefaultFuelId] = c.[FuelId]
	WHERE b.[CustomerId] = @pCustomerId
	AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	  AND NOT EXISTS(
			SELECT 1
			FROM [Efficiency].[VehiclesByGeoFence] x
			WHERE x.[VehicleId] = a.[VehicleId]
			  AND x.[GeoFenceId] = @pGeoFenceId
		)
	UNION
	SELECT
		 a.[GeoFenceId]
		,a.[VehicleId]
		,b.[Name] AS VehicleName
		,c.[Manufacturer] + ' - ' + c.[VehicleModel] + ', ' + d.[Name] + ' ' + CONVERT(varchar(50),c.Liters) + 'L' AS VehicleType
		,b.[PlateId]
		,ROW_NUMBER() OVER (ORDER BY a.[RowVersion]) AS RowNumber
    FROM [Efficiency].[VehiclesByGeoFence] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]
		INNER JOIN [General].[VehicleCategories] c
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] d
			ON c.[DefaultFuelId] = d.[FuelId]
	WHERE a.[GeoFenceId] = @pGeoFenceId
	  AND b.[CustomerId] = @pCustomerId
	 
	
    SET NOCOUNT OFF
END

GO


