USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetMasterRole_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetMasterRole_Retrieve]
GO

/****** Object:  StoredProcedure [dbo].[Sp_ASPNetMasterRole_Retrieve]    Script Date: 1/27/2016 11:54:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 23/12/2015
-- Description:	Master Roles Retrieve
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ASPNetMasterRole_Retrieve]
(   
	@pRoleName varchar(MAX) = null	
)	
AS
BEGIN	
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT TOP 1 [Name] FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName AND [Master] = 1)	
		SELECT @pRoleName	
	ELSE
	BEGIN
		DECLARE @RoleId VARCHAR(128)
		SET @RoleId = (SELECT [Parent] FROM [dbo].[AspNetRoles] WHERE [Name] = @pRoleName AND [Active] = 1)
		SELECT [Name] FROM [dbo].[AspNetRoles] WHERE [Id] = @RoleId
	END	
	
	SET NOCOUNT OFF
END
