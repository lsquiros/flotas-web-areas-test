USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleGroups_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleGroups_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleGroup information
-- Update 1/22/2016 Henry Retana - Dinamic Filter
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleGroups_Retrieve] 
(
	 @pVehicleGroupId INT = NULL				--@pVehicleGroupId: PK of the table
	,@pCustomerId INT							--@pCustomerId: FK of Customer
	,@pKey VARCHAR(800) = NULL					--@pKey: Search Key
	,@pUserId INT							    --@pUserId: User Id
)
AS
BEGIN
	
	SET NOCOUNT ON	
		
    -- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	SELECT DISTINCT
		 a.[VehicleGroupId]
		,a.[Name]
		,a.[Description]
		,a.[RowVersion]
		,c.[Code] CountryCode

    FROM [General].[VehicleGroups] a		
		INNER JOIN [General].[Customers] b ON a.[CustomerId] = b.[CustomerId]
		INNER JOIN [General].[Countries] c ON b.[CountryId] = c.[CountryId]
		LEFT OUTER JOIN [General].[VehiclesByGroup] d ON d.[VehicleGroupId] = a.[VehicleGroupId]
		LEFT OUTER JOIN [General].[Vehicles] v ON v.[VehicleId] = d.[VehicleId]
		

	WHERE a.[CustomerId] = @pCustomerId
		  AND (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
		  AND (@pVehicleGroupId IS NULL OR a.[VehicleGroupId] = @pVehicleGroupId)
		  AND (@pKey IS NULL 
					OR a.[Name] like '%'+@pKey+'%'
					OR a.[Description] like '%'+@pKey+'%')
	ORDER BY [VehicleGroupId] DESC
	
    SET NOCOUNT OFF
END

GO


