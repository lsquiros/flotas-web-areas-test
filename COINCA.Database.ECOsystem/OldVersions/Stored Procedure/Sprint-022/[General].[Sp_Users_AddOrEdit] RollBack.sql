USE [ECOsystem]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Users_AddOrEdit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Insert or Update User information - Update 11/11/2015 Henry Retana, update the [IsLockedOut] 
-- in the User table, so the user can be blocked from the maintenance. 
 -- Update 11/13/2015 Henry Retana, update the @pEmailConfimation 
-- in the AspNetUsers table, so the email can be confirmed. 
-- Update 01/04/2016 change in the save user and roleid, if going to use the roleId now on.
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Users_AddOrEdit]
(
	 @pUserId INT = NULL	
	,@pName VARCHAR(250)
	,@pChangePassword BIT = NULL
	,@pPhoto VARCHAR(MAX) = NULL
	,@pIsActive BIT = 1
	,@pEmail VARCHAR(256)
	,@pPhoneNumber VARCHAR(256) = NULL
	,@pUserName VARCHAR(256)
	,@pIsLockedOut BIT = NULL
	,@pRoleId NVARCHAR(256) = NULL
	,@pRoleName NVARCHAR(256) = NULL
	,@pPasswordExpirationDate DATETIME = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
	,@pEmailConfimation BIT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lAspNetUserId NVARCHAR(128)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
	
			SELECT @lAspNetUserId = a.[Id]
				FROM [dbo].[AspNetUsers] a
			WHERE [UserName] = @pUserName
			
			IF (@pUserId IS NULL)
			BEGIN
								
				INSERT INTO [General].[Users]
						([AspNetUserId]
						,[Name]
						,[ChangePassword]
						,[Photo]
						,[InsertDate]
						,[InsertUserId])
				VALUES	(@lAspNetUserId
						,@pName
						,@pChangePassword
						,@pPhoto
						,GETUTCDATE()
						,@pLoggedUserId)

				SET @pUserId = SCOPE_IDENTITY()

				IF @pEmailConfimation IS NOT NULL 
				BEGIN
					UPDATE [dbo].[AspNetUsers]
					SET [EmailConfirmed] = 1
					WHERE [Email] = @pEmail
				END

			END
			ELSE
			BEGIN
				DECLARE @lIsLockedOut BIT, @lAccessFailedCount INT = NULL
				SELECT 
					@lIsLockedOut = CASE WHEN a.[IsLockedOut] = 1 OR b.[LockoutEndDateUtc] IS NOT NULL THEN 1 ELSE 0 END
				FROM [General].[Users] a INNER JOIN [dbo].[AspNetUsers] b ON a.[AspNetUserId] = b.[Id]
				WHERE a.[UserId] = @pUserId
				
				IF(@lIsLockedOut = 1 AND @pIsLockedOut = 0)
					SET @lAccessFailedCount = 0
				
				UPDATE [dbo].[AspNetUsers]
					SET  [Email] = @pEmail
						,[PhoneNumber] = @pPhoneNumber
						,[AccessFailedCount] = ISNULL(@lAccessFailedCount, [AccessFailedCount])
						,[LockoutEndDateUtc] = CASE WHEN @lAccessFailedCount = 0 THEN NULL ELSE [LockoutEndDateUtc] END
				WHERE [UserName] = @pUserName

				IF @pIsLockedOut = 1
				BEGIN
					UPDATE [dbo].[AspNetUsers]
						SET  [LockoutEndDateUtc] = GETUTCDATE()
					WHERE [UserName] = @pUserName
						AND LockoutEndDateUtc IS NULL
				END
				
				UPDATE [General].[Users]
					SET  [Name] = @pName
						,[ChangePassword] = ISNULL(@pChangePassword,[ChangePassword])
						,[Photo] = @pPhoto
						,[PasswordExpirationDate] = ISNULL(@pPasswordExpirationDate,[PasswordExpirationDate])
						,[IsActive] = @pIsActive
						,[IsLockedOut] = @pIsLockedOut
						,[ModifyDate] = GETUTCDATE()
						,[ModifyUserId] = @pLoggedUserId
				WHERE [UserId] = @pUserId
				  AND [RowVersion] = @pRowVersion
			END
            
            SET @lRowCount = @@ROWCOUNT
            			
			DELETE [dbo].[AspNetUserRoles] WHERE [UserId] = @lAspNetUserId

			IF @pRoleName IS NULL
			BEGIN 
				INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId])
				(SELECT 
					@lAspNetUserId, a.[Id]
				FROM [dbo].[AspNetRoles] a
				WHERE a.[Id] = @pRoleId)
			END
			ELSE
			BEGIN
				INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId])
				(SELECT 
					@lAspNetUserId, a.[Id]
				FROM [dbo].[AspNetRoles] a
				WHERE a.[Name] = @pRoleName)
			END
			
            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
			SELECT @pUserId
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO


