USE [ECOsystem]
GO

/****** Object:  StoredProcedure [General].[Sp_VehiclesReferences_Retrieve]    Script Date: 1/19/2016 4:41:57 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesReferences_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesReferences_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_VehiclesReferences_Retrieve]    Script Date: 1/19/2016 4:41:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Napole�n Alvarado
-- Create date: 12/02/2014
-- Description:	Retrieve Vehicle References information
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehiclesReferences_Retrieve]
(
	 @pVehicleId INT = NULL					--@pVehicleId: PK of the table
	,@pKey VARCHAR(800) = NULL				--@pKey: Search Key
	,@pUserId INT							--@pUserId: User Id
)
AS
BEGIN
	
	SET NOCOUNT ON

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT
		 a.[VehicleId]
		,a.[PlateId]
		,a.[Name]
		,a.[CustomerId]
		,a.[AdministrativeSpeedLimit]
		,a.[IntrackReference]
		,a.[DeviceReference]
		,a.[RowVersion]
    FROM [General].[Vehicles] a
	WHERE (@pVehicleId IS NULL OR [VehicleId] = @pVehicleId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
                OR a.[PlateId] like '%'+@pKey+'%')
	  AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	ORDER BY [VehicleId] DESC
	
    SET NOCOUNT OFF
END

GO


