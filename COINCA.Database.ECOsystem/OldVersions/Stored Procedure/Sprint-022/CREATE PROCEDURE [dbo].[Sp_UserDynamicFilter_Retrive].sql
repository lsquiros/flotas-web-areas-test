USE [ECOsystem]
GO

/****** Object:  StoredProcedure [dbo].[Sp_UserDynamicFilter_Retrive]    Script Date: 1/19/2016 10:14:51 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_UserDynamicFilter_Retrive]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_UserDynamicFilter_Retrive]
GO

/****** Object:  StoredProcedure [dbo].[Sp_UserDynamicFilter_Retrive]    Script Date: 1/19/2016 10:14:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ================================================================================================
-- Author:		Melvin Salas
-- Create date: JAN/18/2016	
-- Description:	Retrieve Users Dynamic Filter information
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_UserDynamicFilter_Retrive]
	@pUserId INT,
	@pType NVARCHAR(2)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	items
	FROM	dbo.Fn_Split ((	SELECT	[Value]
								FROM	dbo.[UsersDynamicFilter]
								WHERE	[UserId] = @pUserId
										AND [Type] = @pType), ',')
END



GO


