USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AspDinamicFilters_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_AspDinamicFilters_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO 
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 1/15/2015
-- Description:	Retrieve Dinamic Filters
-- ================================================================================================
CREATE PROCEDURE [dbo].[SP_AspDinamicFilters_Retrieve]
(	
	@pCustomerId	INT = null,
	@pUserId	INT = null,
	@pFilterType VARCHAR(20) = null
)	
AS
BEGIN	
	SET NOCOUNT ON;

	IF @pFilterType = 'CC'
	BEGIN

		 SELECT VHCC.CostCenterId
			   ,VHCC.Name AS CostCenterName
			   ,VH.VehicleId
			   ,VH.Name AS VehicleName
			   ,VH.PlateId AS PlateId
		 FROM [General].[Vehicles] AS VH 
			  INNER JOIN [General].[VehicleCostCenters] AS VHCC
		 ON VH.CostCenterId = VHCC.CostCenterId
		 WHERE VH.CustomerId =  @pCustomerId
		 ORDER BY VHCC.CostCenterId, VHCC.Name

	END	 

	IF @pFilterType = 'GP'
	BEGIN

		SELECT ISNULL(VG.VehicleGroupId, 99) AS VehicleGroupId
		   ,ISNULL(VG.Name, 'Indefinido') AS VehicleGroupName
		   ,VH.VehicleId
		   ,VH.Name AS VehicleName	   
		   ,VH.PlateId AS PlateId
		FROM [General].[Vehicles] AS VH 
			LEFT OUTER JOIN [General].[VehiclesByGroup] AS VBG
			ON VH.VehicleId = VBG.VehicleId
			LEFT OUTER JOIN [General].[VehicleGroups] AS  VG
			ON VBG.VehicleGroupId = VG.VehicleGroupId
		WHERE VH.CustomerId = @pCustomerId
		ORDER BY VG.VehicleGroupId, VG.Name

	END 

	IF @pFilterType = 'VH'
	BEGIN

		SELECT VH.VehicleId
			  ,VH.Name AS VehicleName
			  ,VH.PlateId AS PlateId
		FROM [General].[Vehicles] AS VH
		WHERE VH.CustomerId = @pCustomerId
		ORDER BY VH.VehicleId, VH.Name

	END

	SET NOCOUNT OFF
END
GO