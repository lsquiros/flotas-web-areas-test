USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CostCentersByCredit_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CostCentersByCredit_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_CostCentersByCredit_Retrieve]    Script Date: 2/17/2016 1:08:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Cost Centers By Credit
-- Modify by: Stefano Quiros
-- Modify by : Melvin Salas - 1/22/2015
-- Summary: Dinamic Filter
-- Modify by : Gerald Solano - 2/17/2016
-- Summary: Se agregó cantidad de decimales permitidos en la variable @Equival para no redondear la equivalencia
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_CostCentersByCredit_Retrieve]
(
	 @pCostCenterId INT						--@pCostCenterId: PK of the table VehicleGroup
	,@pYear INT								--@pYear: Year
	,@pMonth INT	                         --@pMonth: Month	
	,@pCustomerId INT = NULL	
	,@pUserId INT							--@pUserId: User Id					
)
AS
BEGIN
	
	SET NOCOUNT ON	

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	Declare @tableFuelsTemp Table(
		 [FuelByCreditId] int null
		,[FuelId] int null
		,[Year] int null
		,[Month] int null
		,[Total] DECIMAL(16,2) null
		,[Assigned] DECIMAL(18,0) null
		,[Available] DECIMAL(21,2) null
		,[FuelName] varchar(50) null
		,[Liters] DECIMAL(16,2) null
		,[LiterPrice] DECIMAL(16,2) null
		,[CurrencySymbol] nvarchar (50)
		,[RowVersion] varchar(200)
	
	
	)

	insert into @tableFuelsTemp
	exec [Control].[Sp_FuelsByCredit_Retrieve] @pCustomerId, @pYear, @pMonth
	
	DECLARE @Tipo INT
	DECLARE @Equival DECIMAL(18,8) = 3.78541178 -- CUSTOMER WITH GALLONS
        
	Select @Tipo = [UnitOfCapacityId] FROM [General].[Customers] WHERE [CustomerId] = @pCustomerId
	
	IF(@Tipo = 0)
	BEGIN
		SET @Equival = 1  -- CUSTOMER WITH LITERS
	END	  

	SELECT
		 a.PlateId
		,a.[Name] AS  [VehicleName]
		,b.[Manufacturer] + ' - ' + b.[VehicleModel] AS VehicleModel
		,f.[Symbol] AS [CurrencySymbol]
		,ISNULL(t.[Amount], CONVERT(DECIMAL(16,2),0)) AS [Amount]
		,ISNULL(t.[Liters],0) AS [Liters]
		,cf.[Name] AS FuelName
		,a.[VehicleId]
		,b.[DefaultFuelId]
		,c.[Code] + ' - ' + c.[Name] AS CostCenter
		,t.[VehicleFuelId]
		,t.[RowVersion]	
		,(SELECT Top 1 tmp.[LiterPrice] * @Equival From @tableFuelsTemp tmp where tmp.[FuelId] = cf.[FuelId])  As [LiterPrice]	
	FROM [General].[Vehicles] a
		INNER JOIN [General].[VehicleCategories] b
			ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
	    INNER JOIN [Control].[Fuels] cf
		    ON b.[DefaultFuelId] = cf.[FuelId]
		INNER JOIN [General].[VehicleCostCenters] c
			ON a.[CostCenterId] = c.[CostCenterId]
		INNER JOIN [General].[Customers] e
			ON e.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON f.[CurrencyId] = e.[CurrencyId]
		LEFT JOIN (
				SELECT 
					x.[VehicleId]
					,x.[Amount]
					,x.[Year]
					,x.[Month]
					,x.[Liters]
					,x.[VehicleFuelId]
					,x.[RowVersion]
				FROM [Control].[VehicleFuel] x	
					INNER JOIN (
						SELECT	VehicleId, max(VehicleFuelID) AS VehicleFuelID
						FROM	[Control].[VehicleFuel]
						WHERE [Year] = @pYear
						  AND [Month] = @pMonth
						GROUP BY VehicleId
					) AS y ON x.VehicleId = y.VehicleId AND x.VehicleFuelID = y.VehicleFuelID
				WHERE x.[Year] = @pYear
				  AND x.[Month] = @pMonth) t
			ON t.VehicleId = a.VehicleId
	WHERE c.[CostCenterId] = @pCostCenterId
	AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	  
    SET NOCOUNT OFF
END

GO


