USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleFuel_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_VehicleFuel_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_VehicleFuel_Retrieve]    Script Date: 2/17/2016 1:08:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve Vehicle Fuel information
-- Dinamic Filter - 1/22/2016 - Melvin Salas
-- Modify by : Stefano Quiros - 1/27/2016
-- Modify by : Gerald Solano - 2/17/2016
-- Summary: Se agregó cantidad de decimales permitidos en la variable @Equival para no redondear la equivalencia
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleFuel_Retrieve]
(
	 @pVehicleFuelId INT = NULL				--@pVehicleFuelId: PK of the table
	,@pVehicleId INT = NULL					--@pVehicleId: FK of Vehicle Id
	,@pYear INT = NULL						--@pYear: Year
	,@pMonth INT = NULL						--@pMonth: Month
	,@pCustomerId INT = NULL				--@pCustomerId: Customer Ids
	,@pUserId INT = NULL					--@pUserId: User Id
)
AS
BEGIN
	
	SET NOCOUNT ON

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	Declare @tableFuelsTemp Table(
		 [FuelByCreditId] int null
		,[FuelId] int null
		,[Year] int null
		,[Month] int null
		,[Total] DECIMAL(16,2) null
		,[Assigned] DECIMAL(18,0) null
		,[Available] DECIMAL(21,2) null
		,[FuelName] varchar(50) null
		,[Liters] DECIMAL(16,2) null
		,[LiterPrice] DECIMAL(16,2) null
		,[CurrencySymbol] nvarchar (50)
		,[RowVersion] varchar(200)
	
	
	)

	insert into @tableFuelsTemp
	exec [Control].[Sp_FuelsByCredit_Retrieve] @pCustomerId, @pYear, @pMonth

	DECLARE @Tipo INT
	DECLARE @Equival DECIMAL(18,8) = 3.78541178 -- CUSTOMER WITH GALLONS
        
	Select @Tipo = [UnitOfCapacityId] FROM [General].[Customers] WHERE [CustomerId] = @pCustomerId
	
	IF(@Tipo = 0)
	BEGIN
		SET @Equival = 1  -- CUSTOMER WITH LITERS
	END	  

	SELECT
		a.[VehicleFuelId]
		,a.[VehicleId]
		,a.[Month]
		,a.[Year]		
		,a.[Liters]
		,a.[Amount]
		,d.[Name] AS FuelName
		,d.[FuelId]
		,d.[FuelId] AS [DefaultFuelId]
		,b.[PlateId]
		,b.[Name] AS VehicleName
		,c.[VehicleModel]
		,f.[Symbol] AS [CurrencySymbol]
		,a.[RowVersion]
		,(SELECT Top 1 tmp.[LiterPrice] * @Equival From @tableFuelsTemp tmp where tmp.[FuelId] = d.[FuelId])  As [LiterPrice]
	FROM [Control].[VehicleFuel] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]
		INNER JOIN [General].[VehicleCategories] c
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] d
			ON c.[DefaultFuelId] = d.[FuelId]
		INNER JOIN [General].[Customers] e
			ON b.[CustomerId] = e.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON e.[CurrencyId] = f.[CurrencyId]
	WHERE (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
		AND (@pVehicleFuelId IS NULL OR a.[VehicleFuelId] = @pVehicleFuelId)
		AND (@pCustomerId IS NULL OR b.[CustomerId] = @pCustomerId)
		AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
		AND (@pMonth IS NULL OR a.[Month] = @pMonth)
		AND (@pYear IS NULL OR a.[Year] = @pYear)
	  
	ORDER BY [VehicleFuelId] DESC
	
    SET NOCOUNT OFF
END
GO
