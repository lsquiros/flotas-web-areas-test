USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_VehicleLowPerformanceReport_Retrieve]    Script Date: 1/19/2016 4:53:13 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_VehicleLowPerformanceReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_VehicleLowPerformanceReport_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_VehicleLowPerformanceReport_Retrieve]    Script Date: 1/19/2016 4:53:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes.
-- Create date: 26/01/2015
-- Description:	Retrieve VehicleLowPerformanceReport information
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_VehicleLowPerformanceReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pVehicleId INT = NULL			    --@pVehicleId: End Date
	,@pUserId INT						--@pUserId: User Id
)
AS
BEGIN
	
	SET NOCOUNT ON	

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT('0' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end
	;WITH T(VehicleId,PlateId,[Type],VehicleModel,Manufacturer,[Year],Performance,DefaultPerformance) AS
	(
	   SELECT v.VehicleId,
				   v.PlateId,
				   vc.[Type],
				   vc.VehicleModel,
				   vc.Manufacturer,
				   vc.[Year],
				   (SELECT TOP 1 TrxPerformance FROM General.PerformanceByVehicle WHERE  TrxDate <@pFinalDate AND PerformanceVehicleId=p.PerformanceVehicleId order by TrxDate desc) as Performance,
				   vc.DefaultPerformance
				   FROM General.Vehicles v
					INNER JOIN General.PerformanceByVehicle p ON (p.VehicleId=v.VehicleId and p.PerformanceVehicleId=(SELECT TOP 1 PerformanceVehicleId FROM General.PerformanceByVehicle WHERE  TrxDate < @pFinalDate and VehicleId=v.VehicleId order by TrxDate desc))
					INNER JOIN General.VehicleCategories vc ON v.VehicleCategoryId=vc.VehicleCategoryId
					
					WHERE p.TrxDate BETWEEN @pInitialDate AND @pFinalDate AND
					v.VehicleId=ISNULL(@pVehicleId,v.VehicleId) AND v.CustomerId=@pCustomerId 
					AND (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	)
	SELECT * FROM T
	WHERE Performance<DefaultPerformance
	 	
	SET NOCOUNT OFF
END

GO


