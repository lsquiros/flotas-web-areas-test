USE [ECOsystem]
GO

/****** Object:  StoredProcedure [General].[Sp_VehicleByPlate_Retrieve]    Script Date: 1/19/2016 4:39:16 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleByPlate_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleByPlate_Retrieve]
GO

/****** Object:  StoredProcedure [General].[Sp_VehicleByPlate_Retrieve]    Script Date: 1/19/2016 4:39:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ================================================================================================
-- Author:		Cristian Mart�nez H.
-- Create date: 20/Oct/2014
-- Description:	Retrieve Vehicle By Plate information
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleByPlate_Retrieve]
(
	@pPlateId VARCHAR(10), --@pPlateId: Plate of the vehicle
	@pCustomerId INT
	,@pUserId INT = NULL   --@pUserId: User Id
)
AS
BEGIN 
	SET NOCOUNT ON
		
	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

		--DECLARE @pPlateId VARCHAR(10) = 'MTD345'

		SELECT
			 a.[VehicleId]
			,a.[PlateId]
			,a.[Name]
			,a.[CostCenterId]
			,c.[Name] AS CostCenterName
			,a.[CustomerId]
			,f.[Name] AS UserName
			,e.[LicenseExpiration] AS DriverLicenseExpiration
			,e.[DailyTransactionLimit]
			,a.[VehicleCategoryId]
			,d.[Type] AS CategoryType
			,d.[Liters]
			,d.[DefaultFuelId]
			,b.[Name] AS FuelName
			,a.[Active]
			,a.[Colour]
			,a.[Chassis]
			,a.[LastDallas]
			,a.[FreightTemperature]
			,a.[FreightTemperatureThreshold1]
			,a.[FreightTemperatureThreshold2]
			,a.[Predictive]
			,a.[AVL]
			,a.[PhoneNumber]
			,h.[PartnerId]
			,h.[CapacityUnitId] AS PartnerCapacityUnitId
			,a.[RowVersion]
		FROM [General].[Vehicles] a
			INNER JOIN [General].[VehicleCostCenters] c
				ON a.[CostCenterId] = c.[CostCenterId]
			INNER JOIN [General].[VehicleCategories] d
				ON a.[VehicleCategoryId] = d.[VehicleCategoryId]
			INNER JOIN [Control].[Fuels] b
				ON d.[DefaultFuelId] = b.[FuelId]
			LEFT JOIN [General].[DriversUsers] e  -- 2015-10-30 GSOLANO: Se aplic� Left JOIN - Problemas cuando varios vehiculos comparten un mismo conductor
				ON e.[UserId] = (SELECT
									TOP 1 [UserId]
								 FROM [General].[VehiclesByUser]
								 WHERE [VehicleId] = a.[VehicleId] AND [LastDateDriving] IS NULL
								 ORDER BY [InsertDate] DESC)
			LEFT JOIN [General].[Users] f   -- 2015-10-30 GSOLANO: Se aplic� Left JOIN - Problemas cuando varios vehiculos comparten un mismo conductor
				ON e.[UserId] = f.[UserId]
			INNER JOIN [General].[Customers] g
				ON a.[CustomerId] = g.[CustomerId]
			INNER JOIN [General].[CustomersByPartner] i
				ON i.[CustomerId] = g.[CustomerId]
			INNER JOIN [General].[Partners] h
				ON i.[PartnerId] = h.[PartnerId]
		WHERE 
			 a.CustomerId = @pCustomerId AND
			(a.[PlateId] IS NULL OR a.[PlateId] = @pPlateId OR a.[PlateId] LIKE '%'+@pPlateId+'%')
			AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
		ORDER BY [VehicleId] DESC
	SET NOCOUNT OFF
END




GO


