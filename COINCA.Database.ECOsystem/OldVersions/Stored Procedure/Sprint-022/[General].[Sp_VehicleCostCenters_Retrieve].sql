USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehicleCostCenters_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehicleCostCenters_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/23/2014
-- Description:	Retrieve VehicleCostCenters information
-- Update 1/22/2016 Henry Retana - Dinamic Filter
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_VehicleCostCenters_Retrieve]
(
	 @pCostCenterId INT = NULL					--@pCostCenterId: PK of the table
	,@pCustomerId INT							--@pCustomerId: FK of the table General.Customers
	,@pKey VARCHAR(800) = NULL					--@pKey: Search Key
	,@pUserId INT							    --@pUserId: User Id
)
AS
BEGIN
	
	SET NOCOUNT ON

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END


	IF @pKey IS NOT NULL SET @pKey = UPPER(@pKey)
		
	SELECT DISTINCT 
		 a.[CostCenterId]
		,a.[Name]
		,a.[Code]
		,a.[UnitId]
		,b.[Name] AS [UnitName]
		,d.[Code] AS [CountryCode]
		,a.[RowVersion]
    FROM [General].[VehicleCostCenters] a
		INNER JOIN [General].[VehicleUnits] b
			ON a.[UnitId] = b.[UnitId]		
		INNER JOIN [General].[Customers] c
			ON b.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Countries] d
			ON c.[CountryId] = d.[CountryId]
		LEFT OUTER JOIN [General].[Vehicles] v
		    ON v.[CostCenterId] = a.[CostCenterId]
		
	WHERE b.[CustomerId] = @pCustomerId
	  AND (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	  AND (@pCostCenterId IS NULL OR a.[CostCenterId] = @pCostCenterId)
	  AND (@pKey IS NULL 
				OR a.[Name] like '%'+@pKey+'%'
				OR a.[Code] like '%'+@pKey+'%'
				OR b.[Name] like '%'+@pKey+'%')
	ORDER BY [CostCenterId] DESC
	
    SET NOCOUNT OFF
END

GO


