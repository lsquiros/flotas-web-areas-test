USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]    Script Date: 1/20/2016 9:41:26 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]    Script Date: 1/20/2016 9:41:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

	-- ================================================================================================
	-- Author:		Berman Romero L.
	-- Create date: 10/21/2014
	-- Description:	Retrieve Current Fuels Report By Sub Unit information
	-- Dinamic Filter - 1/22/2015 - Melvin Salas
	-- ================================================================================================
	CREATE PROCEDURE [Control].[Sp_CurrentFuelsReportBySubUnit_Retrieve]
	(
		 @pCustomerId INT					--@pCustomerId: Customer Id
		,@pYear INT = NULL					--@pYear: Year
		,@pMonth INT = NULL					--@pMonth: Month
		,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
		,@pEndDate DATETIME = NULL			--@pEndDate: End Date
		,@pUserId INT						--@pUserId: User Id
	)
	AS
	BEGIN
	
		--DECLARE @pCustomerId INT = 22	
		--DECLARE @pYear INT = 2015	
		--DECLARE @pMonth INT = 9	
		--DECLARE @pStartDate DATETIME = NULL	
		--DECLARE @pEndDate DATETIME = NULL	

		SET NOCOUNT ON	
	
		DECLARE @lCurrencySymbol NVARCHAR(4) = ''

		-- DYNAMIC FILTER
		DECLARE	@Results TABLE (items INT)
		DECLARE @count INT
		INSERT	@Results
		EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
		SET		@count = (SELECT COUNT(*) FROM	@Results)
		-- END
	
		SELECT
			@lCurrencySymbol = a.Symbol
		FROM [Control].[Currencies] a
			INNER JOIN [General].[Customers] b
				ON a.[CurrencyId] = b.[CurrencyId]
		WHERE b.[CustomerId] = @pCustomerId
	
		SELECT
			 a.[Name] AS [CostCenterName]
			,ISNULL(t.[RealAmount],0) AS [RealAmount]
			,s.[AssignedAmount]
			,CONVERT(DECIMAL(16,2),ROUND( ISNULL( t.[RealAmount] / NULLIF(s.[AssignedAmount],0), 0), 2)*100) AS [RealAmountPct]
			,@lCurrencySymbol AS [CurrencySymbol]
			,a.CostCenterId
		FROM [General].[VehicleCostCenters] a
			LEFT JOIN (SELECT
							 y.[CostCenterId]
							,SUM(x.[FuelAmount]) AS [RealAmount]
						FROM [Control].[Transactions] x
							INNER JOIN [General].[Vehicles] y
								ON x.[VehicleId] = y.[VehicleId]
						WHERE y.[CustomerId] = @pCustomerId
						AND (@count = 0 OR x.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
						  AND x.[IsFloating] = 0
						  AND x.[IsReversed] = 0
						  AND x.[IsDuplicated] = 0
						  AND 1 > (
								SELECT COUNT(*) FROM Control.Transactions t2
									WHERE t2.[CreditCardId] = x.[CreditCardId] AND
										  t2.[TransactionPOS] = x.[TransactionPOS] AND
										  t2.[ProcessorId] = x.[ProcessorId] AND 
										  t2.IsReversed = 1
							)
						  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
									AND DATEPART(m,x.[Date]) = @pMonth
									AND DATEPART(yyyy,x.[Date]) = @pYear) OR
								(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
									AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
						GROUP BY y.[CostCenterId])t
				ON a.CostCenterId = t.CostCenterId
			INNER JOIN (SELECT
							 y.[CostCenterId]
							,SUM(x.[Amount]) AS [AssignedAmount]
						FROM [Control].[VehicleFuel] x
							INNER JOIN [General].[Vehicles] y
								ON x.[VehicleId] = y.[VehicleId]
						WHERE y.[CustomerId] = @pCustomerId
						AND (@count = 0 OR x.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
						  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
									AND x.[Month] = @pMonth
									AND x.[Year] = @pYear) OR
								(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
									AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
									AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
						GROUP BY y.[CostCenterId])s
				ON a.[CostCenterId] = s.[CostCenterId]
		
	
		SET NOCOUNT OFF
	END


GO


