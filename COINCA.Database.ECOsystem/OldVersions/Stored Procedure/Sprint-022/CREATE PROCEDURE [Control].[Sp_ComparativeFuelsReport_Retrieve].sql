USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_ComparativeFuelsReport_Retrieve]    Script Date: 1/20/2016 10:57:32 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ComparativeFuelsReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_ComparativeFuelsReport_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_ComparativeFuelsReport_Retrieve]    Script Date: 1/20/2016 10:57:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Andr�s Oviedo Brenes.
-- Create date: 05/02/2015
-- Description:	Retrieve ComparativeFuelsReport_Retrieve information
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_ComparativeFuelsReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pUserId INT						--@pUserId: User Id
	
)
AS
BEGIN
	
	SET NOCOUNT ON	

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	
	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	Declare @pInitialDate datetime
	declare @pFinalDate datetime
	
	Declare @pInitialDate2 datetime
	declare @pFinalDate2 datetime
	
	
	if @pMonth is not null
	begin
		set @pInitialDate=  CAST(
							  CAST(@pYear AS VARCHAR(4)) +
							  RIGHT('0' + CAST(@pMonth AS VARCHAR(2)), 2) +
							  RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate)+1,0))
	end
	else
	begin
		set @pInitialDate=@pStartDate
		set @pFinalDate=@pEndDate
	end
	
DECLARE @tTable2 Table(
	PlateId VARCHAR(10),
	VehicleId int,
	InitialOdometer decimal,
	FinalOdometer decimal,
	Liters decimal,
	Manufacturer varchar(300),
	VehicleModel varchar(300),
    [Year] int,
	[Type] varchar(300),
	CylinderCapacity int,
	VehicleCategoryId INT,
	TransactionMonth INT,
	TransactionYear INT,
	[Routes] varchar(500),
	DefaultPerformance decimal
)	

DECLARE @tTable Table(
	cte_start_date datetime
)	

;WITH CTE AS
(
    SELECT @pInitialDate AS cte_start_date
    UNION ALL
    SELECT DATEADD(MONTH, 1, cte_start_date)
    FROM CTE
    WHERE DATEADD(MONTH, 1, cte_start_date) <= @pFinalDate   
)
insert INTO @tTable  SELECT * FROM CTE
 
 DECLARE @cte_sd datetime

	DECLARE myCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT cte_start_date AS 'ID' 
		FROM @tTable
	OPEN myCursor
	FETCH NEXT FROM myCursor INTO @cte_sd
	WHILE @@FETCH_STATUS = 0 BEGIN
		PRINT @cte_sd
		
		set @pInitialDate2=  CAST(
							  CAST(YEAR(@cte_sd) AS VARCHAR(4)) +
							  RIGHT('0' + CAST(MONTH(@cte_sd) AS VARCHAR(2)), 2) +
							  RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
						   AS DATETIME)
		set @pFinalDate2=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@pInitialDate2)+1,0))
		
			INSERT INTO @tTable2
			SELECT
			   v.PlateId,
			   v.VehicleId,
			   ISNULL((SELECT TOP 1 Odometer FROM [Control].Transactions  WHERE [DATE] <= @pInitialDate2 and VehicleId=v.VehicleId ORDER BY [DATE] DESC),0) as InitialOdometer,
			   ISNULL((SELECT TOP 1 Odometer FROM [Control].Transactions  WHERE [DATE] BETWEEN @pInitialDate2 AND @pFinalDate2 and VehicleId=v.VehicleId ORDER BY [DATE] ASC),0) as FinalOdometer,
			   ISNULL((SELECT SUM(Liters) FROM [Control].Transactions  WHERE [DATE] BETWEEN @pInitialDate2 AND @pFinalDate2 and VehicleId=v.VehicleId),0) as Liters,
				vc.Manufacturer,
				vc.VehicleModel,
				vc.[Year],
				vc.[Type],
				vc.CylinderCapacity,
				vc.VehicleCategoryId
				,MONTH(@pInitialDate2) as TransactionMonth
				,YEAR(@pFinalDate2) as TransactionYear
				,Control.GetRouteConcat(v.VehicleId) As [Routes]
				,vc.DefaultPerformance
			   FROM General.Vehicles v 
			   INNER JOIN General.VehicleCategories vc ON vc.VehicleCategoryId=v.VehicleCategoryId
			   INNER JOIN Control.Transactions t ON v.VehicleId=t.VehicleId
			    WHERE V.CustomerId=@pCustomerId and MONTH(t.[Date])=@pMonth AND YEAR(t.[Date])=@pYear

		FETCH NEXT FROM myCursor INTO @cte_sd

	END

CLOSE myCursor
DEALLOCATE myCursor

	DECLARE @lTable TABLE
	(
	     VehicleId		INT NOT NULL
	    ,PlateNumber		varchar(10)
		,[year]				INT NOT NULL
		,[month]			INT NOT NULL
		,Totalodometer		DECIMAL(16,2) NOT NULL
		,Totalliters		DECIMAL(16,2) NOT NULL
		,[Samples]			INT NOT NULL
		,[AVGPerformace]	DECIMAL(16,2) NOT NULL
		,Interception		DECIMAL(16,2) NOT NULL
		,slope				DECIMAL(16,2) NOT NULL
	
	)
	INSERT @lTable  EXEC [Control].[Sp_TrendFuelsReportByVehicle_Retrieve] @pCustomerId = @pCustomerId
																				,@pYear = @pYear
																				,@pMonth = @pMonth
																				,@pStartDate = null
																				,@pEndDate = null
																				,@pVehiculoId =null

SELECT t.VehicleId,t.DefaultPerformance,t.PlateId as PlateNumber,t.[TransactionMonth] as [Month],t.[TransactionYear] as [Year],cast((t.FinalOdometer-t.InitialOdometer) as decimal) AS TotalOdometer,SUM(t.Liters) as TotalLiters,t.[Routes],t.Manufacturer, t.[Type],t.VehicleModel,t.[Year] as Model,t.CylinderCapacity,
case when SUM(t.Liters)>0 
then 
	cast((SUM(t.FinalOdometer-t.InitialOdometer)/SUM(t.Liters)) as decimal) 
else 
	cast(0 as decimal)
end	
	 AS Performance,
	t2.slope,
	t2.Samples

FROM @tTable2 t INNER JOIN @lTable t2 ON t.VehicleId=t2.VehicleId
WHERE (@count = 0 OR t.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
  GROUP BY t.VehicleId,t.PlateId,t.[TransactionMonth],t.[TransactionYear],t.InitialOdometer,t.FinalOdometer,t.[Routes],t.Manufacturer,t.[Type],t.VehicleModel,t.CylinderCapacity,t.[Year],t2.slope,
	t2.Samples,t.DefaultPerformance
		
	SET NOCOUNT OFF
END

GO


