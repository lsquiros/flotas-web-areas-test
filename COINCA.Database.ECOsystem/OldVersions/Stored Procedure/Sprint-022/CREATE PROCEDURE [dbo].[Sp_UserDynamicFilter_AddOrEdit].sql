USE [ECOsystem]
GO

/****** Object:  StoredProcedure [dbo].[Sp_UserDynamicFilter_AddOrEdit]    Script Date: 1/19/2016 10:14:23 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_UserDynamicFilter_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_UserDynamicFilter_AddOrEdit]
GO

/****** Object:  StoredProcedure [dbo].[Sp_UserDynamicFilter_AddOrEdit]    Script Date: 1/19/2016 10:14:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ================================================================================================
-- Author:		Melvin Salas
-- Create date: JAN/18/2016
-- Description:	Insert or Update User Dynamic Filter information
-- ================================================================================================

CREATE PROCEDURE [dbo].[Sp_UserDynamicFilter_AddOrEdit]
	 @pUserId INT
	,@pType NVARCHAR(2)
	,@pValue NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @count INT
	
	SET @count = (SELECT	COUNT(*) 
				  FROM		dbo.[UsersDynamicFilter] 
				  WHERE		[UserId] = @pUserId 
				  AND		[Type] = @pType)

	IF @count > 0
	BEGIN
		
		-- CUANDO EL CAMPO YA EXISTE, LO ACTUALIZA
		UPDATE	dbo.[UsersDynamicFilter]
		SET		[Value] = @pValue
		WHERE	[UserId] = @pUserId 
				AND [Type] = @pType
	
	END
	ELSE
	BEGIN
		
		-- CUANDO EL CAMPO NO EXISTE, LO AGREGA
		INSERT	INTO [dbo].[UsersDynamicFilter]
				([UserId], [Type], [Value])
		VALUES	(@pUserId, @pType, @pValue)


	END
		
END 



GO


