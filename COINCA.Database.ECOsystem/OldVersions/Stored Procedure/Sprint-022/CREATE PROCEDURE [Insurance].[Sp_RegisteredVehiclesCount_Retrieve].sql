USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Insurance].[Sp_RegisteredVehiclesCount_Retrieve]    Script Date: 1/19/2016 4:40:49 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Insurance].[Sp_RegisteredVehiclesCount_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Insurance].[Sp_RegisteredVehiclesCount_Retrieve]
GO

/****** Object:  StoredProcedure [Insurance].[Sp_RegisteredVehiclesCount_Retrieve]    Script Date: 1/19/2016 4:40:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Dinamic Filter - 1/22/2015 - Melvin Salas
CREATE PROCEDURE [Insurance].[Sp_RegisteredVehiclesCount_Retrieve]
(
	@pPartnerId INT=null,
	@pCustomerId INT = NULL
	,@pUserId INT							--@pUserId: User Id
)
AS
BEGIN

	SET NOCOUNT ON

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	;WITH WITH_TABLE
	AS
	-- Define the CTE query.
	(
		Select c.CustomerId , c.Name from General.[CustomersByPartner] cbp INNER JOIN General.Customers c ON cbp.CustomerId=c.CustomerId where PartnerId=@pPartnerId and cbp.CustomerId=ISNULL(@pCustomerId,cbp.CustomerId)
	)
	SELECT distinct
		   COUNT(V.VehicleId)
    FROM General.Vehicles v
        INNER JOIN [General].[Customers] a ON v.CustomerId=A.CustomerId
        INNER JOIN WITH_TABLE w ON w.CustomerId=a.CustomerId
		
	WHERE a.[IsDeleted]=CAST(0 as bit)
	  and a.IsActive=1
	  AND (@pCustomerId IS NULL OR a.[CustomerId] = @pCustomerId)
	  AND (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	
	SET NOCOUNT OFF
END




GO


