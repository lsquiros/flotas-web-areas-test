USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Vehicle information
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pUserId INT						--@pUserId: User Id
	,@pFlag INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lExchangeRate DECIMAL(16,8) = 1.0

	DECLARE @count INT = NULL

	--Verifies if the SP is nested or is not
	IF @pFlag = 1
	BEGIN
		-- DYNAMIC FILTER
		DECLARE	@Results TABLE (items INT)	
		INSERT	@Results
		EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
		SET		@count = (SELECT COUNT(*) FROM	@Results)
		-- END
	END
	
	SELECT
		@lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
		INNER JOIN [General].[CustomersByPartner] b
			ON a.[PartnerId] = b.[PartnerId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = b.[CustomerId] AND c.[CurrencyId] = a.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	  AND a.EndDate IS NULL
	;
	WITH TmpValues ( Number) as
	(
		  SELECT 0
		  UNION ALL
		  SELECT Number + 1
		  FROM TmpValues
		  WHERE Number < 100
	)
	SELECT
		  q.[VehicleId]
		 ,q.[PlateNumber]
		 ,q.[Year]
		 ,q.[Month]
		 ,SUM(q.[Liters]) AS [Liters]
	FROM
		(SELECT
			 a.[VehicleId]
			,a.[PlateId] AS [PlateNumber]
			,s.[Month]
			,s.[Year]
			,CONVERT(DECIMAL(16,2),ISNULL(s.[RealAmount],0)/(t.LiterPrice * @lExchangeRate)) AS Liters
		FROM [General].[Vehicles] a
			INNER JOIN [General].[VehicleCategories] b
				ON b.[VehicleCategoryId] = a.[VehicleCategoryId]
			INNER JOIN (
					SELECT
						x.[FuelId], x.[LiterPrice] 
					FROM [Control].[PartnerFuel] x
						INNER JOIN [General].[CustomersByPartner] y
							ON x.[PartnerId] = y.[PartnerId]
						INNER JOIN [General].[Customers] z
							ON y.[CustomerId] = z.[CustomerId]
					WHERE z.CustomerId = @pCustomerId
					  AND x.[EndDate] IS NULL)t
				ON b.[DefaultFuelId] = t.FuelId
			INNER JOIN (SELECT
							 y.[VehicleId]
							,DATEPART(m,x.[Date]) AS [Month]
							,DATEPART(yyyy,x.[Date]) AS [Year]
							,SUM(x.[FuelAmount]) AS [RealAmount]
						FROM [Control].[Transactions] x
							INNER JOIN [General].[Vehicles] y
								ON x.[VehicleId] = y.[VehicleId]
						WHERE y.[CustomerId] = @pCustomerId
						  AND (@count = 0 OR x.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
						  AND x.[IsFloating] = 0
						  AND x.[IsReversed] = 0
						  AND x.[IsDuplicated] = 0
						  AND 1 > (
								SELECT COUNT(*) FROM Control.Transactions t2
									WHERE t2.[CreditCardId] = x.[CreditCardId] AND
									t2.[TransactionPOS] = x.[TransactionPOS] AND
									t2.[ProcessorId] = x.[ProcessorId] AND 
									t2.IsReversed = 1
							)
						  AND y.[Active] = 1
						  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
									AND DATEPART(m,x.[Date]) = @pMonth
									AND DATEPART(yyyy,x.[Date]) = @pYear) OR
								(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
									AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
									
						GROUP BY y.[VehicleId], DATEPART(m,x.[Date]), DATEPART(yyyy,x.[Date]))s
				ON s.[VehicleId] = a.[VehicleId]			
			UNION		
			SELECT
				 a.[VehicleId]
				,a.[PlateId] AS [PlateNumber]
				,r.[Month]
				,r.[Year]
				,0 AS Liters
			FROM [General].[Vehicles] a
				CROSS JOIN(
						SELECT
							 DATEPART(m, DATEADD(m, x.[Number], @pStartDate)) AS [Month]
							,DATEPART(yyyy, DATEADD(m, x.[Number], @pStartDate)) AS [Year]
						FROM TmpValues x
						WHERE x.[Number] <= DATEDIFF(m, @pStartDate, @pEndDate)
					UNION
						SELECT @pMonth AS [Month], @pYear AS [Year]
						WHERE @pMonth IS NOT NULL AND @pYear IS NOT NULL
				)r
			WHERE a.[CustomerId] = @pCustomerId
			AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
    
			  AND a.[Active] = 1)q
		GROUP BY q.[VehicleId], q.[PlateNumber], q.[Year], q.[Month]
		
	SET NOCOUNT OFF
END


GO


