USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]    Script Date: 1/27/2016 1:50:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	--~ Commented out USE [database] statement because USE statement is not supported to another database.
	-- ================================================================================================
	-- Author:		Berman Romero L.
	-- Create date: 10/21/2014
	-- Description:	Retrieve Current Fuels Report By Vehicle information
	-- ================================================================================================
	CREATE PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]
	(
		 @pCustomerId INT					--@pCustomerId: Customer Id
		,@pYear INT = NULL					--@pYear: Year
		,@pMonth INT = NULL					--@pMonth: Month
		,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
		,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	)
	AS
	BEGIN
	
		--DECLARE @pCustomerId INT = 34	
		--DECLARE @pYear INT = 2016	
		--DECLARE @pMonth INT = 1	
		--DECLARE @pStartDate DATETIME = NULL	
		--DECLARE @pEndDate DATETIME = NULL	

		SET NOCOUNT ON	
	
		DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	
		SELECT
			@lCurrencySymbol = a.Symbol
		FROM [Control].[Currencies] a
			INNER JOIN [General].[Customers] b
				ON a.[CurrencyId] = b.[CurrencyId]
		WHERE b.[CustomerId] = @pCustomerId
	
		SELECT
			 a.[PlateId] AS [PlateNumber]
			,ISNULL(t.[RealAmount],0) AS [RealAmount]
			,ISNULL(s.[AssignedAmount], 0) AS [AssignedAmount]
			,ISNULL(CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) , 0) AS [RealAmountPct]
			,@lCurrencySymbol AS [CurrencySymbol]
			,a.CostCenterId
		FROM [General].[Vehicles] a
			LEFT JOIN (SELECT
							 y.[VehicleId]
							,SUM(x.[FuelAmount]) AS [RealAmount]
						FROM [Control].[Transactions] x
							INNER JOIN [General].[Vehicles] y
								ON x.[VehicleId] = y.[VehicleId]
						WHERE y.[CustomerId] = @pCustomerId
						  AND x.[IsFloating] = 0
						  AND x.[IsReversed] = 0
						  AND x.[IsDuplicated] = 0
						  --AND y.[Active] = 1
						  AND 1 > (
								SELECT ISNULL(COUNT(1), 0) FROM Control.Transactions t2
									WHERE t2.[CreditCardId] = x.[CreditCardId] AND
										t2.[TransactionPOS] = x.[TransactionPOS] AND
										t2.[ProcessorId] = x.[ProcessorId] AND 
										t2.IsReversed = 1
							)
						  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
									AND DATEPART(m,x.[Date]) = @pMonth
									AND DATEPART(yyyy,x.[Date]) = @pYear) OR
								(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
									AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
								
						GROUP BY y.[VehicleId])t
				ON a.[VehicleId] = t.[VehicleId]
			LEFT JOIN (SELECT
							 y.[VehicleId]
							,SUM(x.[Amount]) AS [AssignedAmount]
						FROM [Control].[VehicleFuel] x
							INNER JOIN [General].[Vehicles] y
								ON x.[VehicleId] = y.[VehicleId]
						WHERE y.[CustomerId] = @pCustomerId
						  AND y.[Active] = 1
						  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
									AND x.[Month] = @pMonth
									AND x.[Year] = @pYear) OR
								(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
									AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
									AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
								AND x.Amount > 0 -- GSOLANO: Exluye los valores en 0 para evitar realizar una division entre 0	
						GROUP BY y.[VehicleId])s
				ON a.[VehicleId] = s.[VehicleId]
			WHERE a.CustomerId = @pCustomerId 
			      AND (a.IsDeleted = NULL OR a.IsDeleted = 0)
		
		SET NOCOUNT OFF
	END