USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetRolesByUser_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetRolesByUser_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 21/01/2016
-- Description:	Roles Retrieve by Users
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ASPNetRolesByUser_Retrieve]
(  
	@pUserId INT = NULL	
)	
AS
BEGIN	
	SET NOCOUNT ON;	

	SELECT ANR.[Id] AS [RoleId]
		   ,ANR.[Name]		   
	FROM [dbo].[AspNetRoles] AS ANR
	     INNER JOIN [dbo].[AspNetUserRoles] AS UR
		 ON ANR.[Id] = UR.[RoleId]
		 INNER JOIN [General].[Users] AS GU
		 ON UR.[UserId] = GU.[AspNetUserId]		
	WHERE (GU.[UserId] = @pUserId)
	     AND [Active] = 1

	ORDER BY anr.[Id]

	SET NOCOUNT OFF
END


GO


