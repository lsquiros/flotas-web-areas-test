USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsForReverse_Retrieve]    Script Date: 1/21/2016 2:19:52 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsForReverse_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsForReverse_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsForReverse_Retrieve]    Script Date: 1/21/2016 2:19:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--~ Commented out USE [database] statement because USE statement is not supported to another database.
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 07/01/2014
-- Description:	Retrieve Transactions Info For Reverse Operations
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionsForReverse_Retrieve]
(
	 @pTransactionPOS VARCHAR(250)			--@pTransactionPOS: TransactionPOS (External System TraceNumber)
	,@pProcessorId VARCHAR(250)				--@pProcessorId: ProcessorId (External terminalId)
	,@pUserId INT							--@pUserId: User Id
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
			
	SELECT	 a.[CreditCardId]
			,a.[VehicleId]
			,a.[FuelId]
			,a.[Odometer]
			,a.[FuelAmount]
			,a.[Liters]
			,b.[CreditCardNumber] AS [EncryptedCreditCardNumber]
			,c.[PlateId]

    FROM	[Control].[Transactions] a
			INNER JOIN [Control].[CreditCard] b
				ON b.[CreditCardId] = a.[CreditCardId]
			INNER JOIN [General].[Vehicles] c
				ON c.[VehicleId] = a.[VehicleId]

	WHERE	a.[TransactionPOS] = @pTransactionPOS
			AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
			AND a.[ProcessorId] = @pProcessorId
			AND a.[IsFloating] = 0
			AND a.[IsReversed] = 0
			AND a.[IsDuplicated] = 0
			AND a.[IsVoid] = 0
			AND COALESCE(a.[IsAdjustment],0) = 0
	
    SET NOCOUNT OFF

END

GO


