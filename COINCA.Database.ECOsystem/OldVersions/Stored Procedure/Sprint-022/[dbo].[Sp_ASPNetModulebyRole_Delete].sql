USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ASPNetModulebyRole_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ASPNetModulebyRole_Delete]
GO

/****** Object:  StoredProcedure [dbo].[Sp_ASPNetModulebyRole_Delete]    Script Date: 1/27/2016 11:47:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Juan Jos� Arias Solano	
-- Create date: 04/01/2016
-- Description:	Module by Role Delete
-- ================================================================================================

CREATE PROCEDURE [dbo].[Sp_ASPNetModulebyRole_Delete]
(
	@pModuleId int, 
	@pRole  varchar(120)
)
AS 
BEGIN

	DECLARE @IdRol varchar(120);


	SET @IdRol = (SELECT [Id]
				  FROM [dbo].[AspNetRoles]
				  WHERE [Name] = @pRole AND [Active] = 1);
	
	DELETE [dbo].[AspModulesByRoles]
	WHERE ModuleId = @pModuleId AND RoleId = @IdRol;

	DELETE [dbo].[AspMenusByRoles]
	WHERE RoleId = @IdRol AND MenuId IN (SELECT Id
										 FROM [dbo].[AspMenus]
										 WHERE Header = @pModuleId);
END;

GO


