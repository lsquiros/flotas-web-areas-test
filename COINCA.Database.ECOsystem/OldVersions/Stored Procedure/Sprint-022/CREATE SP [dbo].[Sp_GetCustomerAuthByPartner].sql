USE [ECOsystem]
GO

/****** Object:  StoredProcedure [dbo].[Sp_GetCustomerAuthByPartner]    Script Date: 12/29/2015 09:10:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetCustomerAuthByPartner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_GetCustomerAuthByPartner]
GO

/****** Object:  StoredProcedure [dbo].[Sp_GetCustomerAuthByPartner]    Script Date: 12/29/2015 09:10:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================
-- Author:		Oscar Herrera
-- Create date: 29/12/2015
-- Description:	SP para obtener los Customer por rol del usuario 
-- ===============================================================
CREATE PROCEDURE [dbo].[Sp_GetCustomerAuthByPartner]
(
	 @pUserID int = NULL,
	 @pRoleName NVARCHAR(256) = NULL
)

AS
BEGIN
	
  SET NOCOUNT ON

	SELECT cabp.CustomerId, c.Name as CustomerName, c.CountryId , 'CUSTOMER_ADMIN' as RoleName ,c.Logo as CustomerLogo  
	FROM General.Users u 	
	INNER JOIN CustomerAuthByPartner cabp ON cabp.PartnerUserId = u.UserId 
	INNER JOIN General.Customers c ON c.CustomerId = cabp.CustomerId
	INNER JOIN AspNetRoles anr ON anr.Id = cabp.RoleId
	WHERE u.UserId = @pUserID AND (REPLACE(anr.Name,' ','_') = @pRoleName OR anr.Name = @pRoleName);

  SET NOCOUNT OFF
  
END



GO


