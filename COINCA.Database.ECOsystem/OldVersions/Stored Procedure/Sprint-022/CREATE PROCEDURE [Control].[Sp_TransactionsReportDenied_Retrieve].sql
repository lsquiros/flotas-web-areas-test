USE [ECOsystem]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsReportDenied_Retrieve]    Script Date: 1/21/2016 2:38:04 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReportDenied_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsReportDenied_Retrieve]
GO

/****** Object:  StoredProcedure [Control].[Sp_TransactionsReportDenied_Retrieve]    Script Date: 1/21/2016 2:38:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




	-- ================================================================================================
	-- Author:		Kevin Pe�a
	-- Create date: 07/24/2015
	-- Updated date: 10/28/2015 
	-- Description:	Retrieve DeniedTransactionsReport log
	-- Dinamic Filter - 1/22/2015 - Melvin Salas
	-- ================================================================================================
	CREATE PROCEDURE [Control].[Sp_TransactionsReportDenied_Retrieve]
	(
		@pCustomerId INT,					--@pCustomerId: CustomerId		
		@pYear INT = NULL,					--@pYear: Year
		@pMonth INT = NULL,					--@pMonth: Month
		@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
		@pEndDate DATETIME = NULL			--@pEndDate: End Date
		,@pUserId INT						--@pUserId: User Id
	)
	AS
	BEGIN

		SET NOCOUNT ON

		-- DYNAMIC FILTER
		DECLARE	@Results TABLE (items INT)
		DECLARE @count INT
		INSERT	@Results
		EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
		SET	@count = (SELECT COUNT(*) FROM	@Results)
		-- END

IF OBJECT_ID('tempdb..#tmp_transpordata') IS NOT NULL DROP TABLE #tmp_transpordata

-- SACAR LA PLACA DE TRANSPORT DATA
Create TABLE #tmp_transpordata  
(
  LogId INT,
  TransportData nvarchar(MAX),
  Plate VARCHAR(50),
  CostCenterName VARCHAR(50) DEFAULT 'N/A'
 )

insert into #tmp_transpordata (LogId,TransportData)
SELECT LogId,TransportData from [Control].[LogTransactionsPOS]  where CustomerId = @pCustomerId and ResponseCode =05
AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL AND DATEPART(m,[InsertDate]) = @pMonth AND DATEPART(yyyy,[InsertDate]) = @pYear) 
					OR (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL AND [InsertDate] BETWEEN @pStartDate AND DATEADD(day,1,@pEndDate)))


DECLARE @tmpcount int, @tmpNum int, @tmprow int = 0;

Select @tmpcount = Count(*) From #tmp_transpordata 

 WHILE ( @tmpNum  < @tmprow)
 BEGIN

select @tmprow =  LogId from #tmp_transpordata WHERE ROW_NUMBER()OVER (ORDER BY LogId) = tmpnum

DECLARE @INI_INDEX INT = CHARINDEX('cartag',(SELECT [TransportData] from #tmp_transpordata   where LogId =@tmprow))+9
DECLARE @FIN_INDEX INT = CHARINDEX('kilometers',(SELECT [TransportData] from #tmp_transpordata  where LogId =@tmprow))-3

declare @diff_index int = @FIN_INDEX - @INI_INDEX

UPDATE #tmp_transpordata 
SET Plate = SUBSTRING( (SELECT [TransportData] from #tmp_transpordata where LogId =@tmprow) , @INI_INDEX,@diff_index ) 
WHERE LogId = @tmprow

DECLARE @tmpPlate VARCHAR(20)
SELECT @tmpPlate =  Plate from #tmp_transpordata where LogId = @tmprow

Declare @CCName VARCHAR(50)   
SELECT @CCName =  cc.[Name] FROM [General].[Vehicles] v 
                               INNER JOIN [General].[VehicleCostCenters] cc
		                               ON v.CostCenterId = cc.CostCenterId 
							   WHERE (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
							   AND CustomerId=@pCustomerId AND PlateId = @tmpPlate
IF @CCName IS NOT NULL
BEGIN 
UPDATE #tmp_transpordata 
SET CostCenterName = @CCName 
WHERE LogId = @tmprow
END

 SELECT @tmpNum = @tmpNum +1;

END -- end while

		SELECT 
			   cast(0 as INT) as [TransactionId]
			  ,'00000' AS [SystemTraceCode]
			  ,cast(null as char(10)) as [HolderName]
			  ,cast(null as char(10)) as [Date]
			  ,cast(null as char(10)) as [FuelName]
			  ,cast(0 as decimal) as [FuelAmount]
			  ,cast(0 as int) as [Odometer] 
			  ,cast(0 as decimal) as [Liters]
			  ,cast(null as char(10)) as [PlateId]
			  ,cast(null as char(10)) as [CurrencySymbol]
			  ,'Rechazada' as [State]
			  ,p.[Message]  
			  ,p.[TransportData]
			  ,DATEADD(hour,-6,p.[InsertDate]) AS [InsertDate] --UTC
			  ,p.[ResponseCode]
			  ,p.[ResponseCodeDescription]
			  ,cu.[Name] AS [CustomerName]
		      ,c.[CostCenterName] AS [CostCenterName]
		  FROM [Control].[LogTransactionsPOS] p		 
		  INNER JOIN #tmp_transpordata c
		     ON p.[LogId] = c.[LogId]
			 INNER JOIN [General].[Customers] cu
			 ON cu.CustomerId = p.CustomerId

	  ORDER BY [InsertDate] DESC

	END	
	DROP TABLE #tmp_transpordata 
	SET NOCOUNT OFF






GO


