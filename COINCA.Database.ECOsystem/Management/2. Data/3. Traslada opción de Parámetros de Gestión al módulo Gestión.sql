USE [ECOsystem]
GO

-- ================================================================================
-- Author:		Stefano Quir�s
-- Create date: 21/09/2018
-- Description:	Traslada opci�n de Par�metros de Gesti�n al m�dulo correspondiente
-- ================================================================================

DECLARE @pAspMenuId INT = (SELECT [Id] 
						   FROM [AspMenus] 
						   WHERE [Name] = 'Par�metros de Gesti�n')

UPDATE [AspMenus]
SET [Header] = 22
   ,[Parent] = @pAspMenuId
   ,[Order] = 100
   ,[Ico] = '~/Content/Images/customizer.png'
   ,[Name] = 'Par�metros'
WHERE [Id] = @pAspMenuId

UPDATE [AspNetPermissions] 
SET [Resource] = 'Parameters' 
   ,[Name] = 'View_Parameters'
WHERE [Resource] = 'ManagementParameters'