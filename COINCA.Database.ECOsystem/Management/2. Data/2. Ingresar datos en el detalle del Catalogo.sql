USE [ECOsystem]
GO

DECLARE @lCatalogId INT,
		@lParent INT

--------------------------
--- TIPOS DE EVENTOS ---
--------------------------
SET @lCatalogId = (SELECT Id FROM [Management].[Catalog] WHERE [Name] = 'Tipos de Eventos Principales')

INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Iniciar Jornada' ,'CUSTOMERS_JOURNEY_BEGIN' ,'Inicia la jornada del agente' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
SET @lParent = @@IDENTITY
--INSERT INTO GENERALPARAMETERS
INSERT INTO [dbo].[GeneralParameters] VALUES ('CUSTOMERS_BEGIN_JOURNEY', 'NA', 'Evento de Inicio de Jornada', @lParent, 0)

INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Finalizar Jornada' ,'CUSTOMERS_JOURNEY_END' ,'Finaliza la jornada del agente' ,@lParent ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Iniciar Gesti�n' ,'CUSTOMERS_CLIENT_VISIT_BEGIN' ,'Inicia la visita al cliente' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
SET @lParent = @@IDENTITY
INSERT INTO [dbo].[GeneralParameters] VALUES ('CUSTOMERS_CLIENT_BEGIN_VISIT', 'NA',	'Evento de Inicio de Gestion',	@lParent,	0)

INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Finalizar Gesti�n' ,'CUSTOMERS_CLIENT_VISIT_END' ,'Finaliza la visita al cliente' ,@lParent ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Inicio Permiso' ,'CUSTOMERS_PERMISSION_TIME_BEGIN' ,'Inicia Tiempo de permiso para el agente' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
SET @lParent = @@IDENTITY
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Finaliza Permiso' ,'CUSTOMERS_PERMISSION_TIME_END' ,'Finaliza Tiempo de permiso para el agente' ,@lParent ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Iniciar Almuerzo' ,'CUSTOMERS_LUNCH_TIME_BEGIN' ,'Inicia el tiempo de almuerzo para el agente' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
SET @lParent = @@IDENTITY
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Finalizar Almuerzo' ,'CUSTOMERS_LUNCH_TIME_END' ,'Finaliza el tiempo de almuerzo para el agente' ,@lParent ,1 ,GETDATE() ,NULL ,NULL ,NULL)


SET @lCatalogId = (SELECT Id FROM [Management].[Catalog] WHERE [Name] = 'Tipos de Eventos Secundarios')
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Traslado' ,'AGENT_LOCATION_REAL_TIME' ,'Transmite en tiempo real la ubicaci�n del Agente' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
SET @lParent = @@IDENTITY
INSERT INTO [dbo].[GeneralParameters] VALUES ('AGENT_LOCATION_REAL_TIME', 'NA',	'Evento de Traslado',	@lParent,	0)

INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Pinear' ,'CUSTOMERS_COMMERCE_PIN' ,'Identifica un punto de inter�s para el agente' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Iniciar Otro' ,'CUSTOMERS_OTHER_BEGIN' ,'Evento generico' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Parada' ,'AGENT_STOP' ,'El agente realizo una parada' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Pierde Se�al' ,'AGENT_LOSES_SIGNAL' ,'El agente pierde la se�al' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Recupera Se�al' ,'AGENT_RESTORE_SIGNAL' ,'El agente restablece la se�al' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)


--------------------------
--- TIPOS DE PREGUNTAS ---
--------------------------
SET @lCatalogId = (SELECT Id FROM [Management].[Catalog] WHERE [Name] = 'Tipos de Preguntas')

INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Texto' ,'QUESTIONS_TYPE_TEXT' ,'Campo de texto para escribir un comentario u observacion' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Lista de selecci�n �nica' ,'QUESTIONS_TYPE_RADIO' ,'Listado con las opciones a escoger' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Lista Desplegable' ,'QUESTIONS_TYPE_DROPDOWN' ,'Lista despegable con las opciones a elegir' ,0 ,0 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Lista de selecci�n m�ltiple' ,'QUESTIONS_TYPE_CHECK' ,'Lista de seleccion multiple con las opciones a elegir' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Imagen' ,'QUESTIONS_TYPE_IMAGE' ,'Selecci�n de una imagen' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Firma' ,'QUESTIONS_TYPE_SIGN' ,'Selecci�n de Firma' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Resumen' ,'QUESTIONS_TYPE_SUMMARY' ,'Resumen de las presuntas del formulario' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)


--------------------------
--- TIPOS DE ICONOS ---
--------------------------
SET @lCatalogId = (SELECT Id FROM [Management].[Catalog] WHERE [Name] = 'Iconos')

INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-account' ,'account' ,'mdi-account' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-cat' ,'cat' ,'mdi-cat' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-home' ,'home' ,'Lugar de residencia' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-domain' ,'domain' ,'Lugar de trabajo' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-store' ,'store' ,'Comercio' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)

GO
