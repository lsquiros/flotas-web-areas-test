USE [Management]
GO

-- ================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 26/09/2018
-- Description:	Update name of opcions for Management
-- ================================================================================

DECLARE @pAspMenuId INT = (SELECT [Id] 
						   FROM [AspMenus] 
						   WHERE [Name] = 'Formularios')

UPDATE [AspMenus]
SET [Name] = 'Mantenimientos'
WHERE [Id] = @pAspMenuId

----------------------------------------------------------------------

SET @pAspMenuId = ( SELECT [Id] 
					FROM [AspMenus] 
					WHERE [Name] = 'Mantenimiento de Formularios')

UPDATE [AspMenus]
SET [Name] = 'Formularios'
WHERE [Id] = @pAspMenuId

UPDATE [dbo].[AspNetPermissions] 
SET [Resource] = 'Survey'
WHERE  Id = (SELECT [PermissionId]
			FROM [AspMenus]
			WHERE [Id] = @pAspMenuId )
----------------------------------------------------------------------

SET @pAspMenuId = ( SELECT [Id] 
					FROM [AspMenus] 
					WHERE [Name] = 'Mantenimiento de Eventos')

UPDATE [AspMenus]
SET [Name] = 'Eventos'
WHERE [Id] = @pAspMenuId

UPDATE [dbo].[AspNetPermissions] 
SET [Resource] = 'Event'
WHERE  Id = (SELECT [PermissionId]
			FROM [AspMenus]
			WHERE [Id] = @pAspMenuId )

----------------------------------------------------------------------

UPDATE [AspMenus]
SET [Name] = 'Agenda'
WHERE [Name] = 'Mantenimiento de Agenda'


----------------------------------------------------------------------
-- Eliminar opcion del menu
----------------------------------------------------------------------
GO
DECLARE @pAspMenuId INT = (SELECT [Id] 
						   FROM [AspMenus] 
						   WHERE [Name] = 'Preguntas por Formulario')

DELETE FROM [dbo].[AspNetRolePermissions]
WHERE [PermissionId] = (SELECT [Id] 
						FROM[dbo].[AspNetPermissions] 
						WHERE [Id] = (SELECT [PermissionId]
										FROM [AspMenus] 
										WHERE [Id] = @pAspMenuId))
DELETE FROM [dbo].[AspNetPermissions] 
	WHERE [Id] = (SELECT [PermissionId]
						   FROM [AspMenus] 
						   WHERE [Id] = @pAspMenuId)

DELETE FROM [dbo].[AspMenusByRoles] 
	WHERE [MenuId] = (SELECT [Id]
						   FROM [AspMenus] 
						   WHERE [Id] = @pAspMenuId)

DELETE FROM [dbo].[AspMenus] 
	WHERE [Id] = @pAspMenuId

------------------------------------------
--UPDATE SURVEY REPORT
------------------------------------------

UPDATE [dbo].[AspMenus]
SET [Name] = 'Formularios'
WHERE [Name] = 'Formulario de Visitas a Comercios'
AND [Header] = 22

UPDATE [dbo].[AspNetPermissions]
SET [Name] = 'View_SurveyReport',
	[Resource] = 'SurveyReport'
WHERE [Id] = '7C77976D-7261-4C75-A308-77325E4F5982'
