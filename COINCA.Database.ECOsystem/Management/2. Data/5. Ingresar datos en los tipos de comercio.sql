
-- Actualizar el valor para indicar que tipo de comercio requiere datos extra en el App. 
UPDATE [General].[CommercesTypes] SET [RequiredData] = 1 WHERE [Name] = 'Comercio' 

GO

UPDATE [General].[CommercesTypes] SET [RequiredData] = 0 WHERE [Name] != 'Comercio'

GO

-- Actualizar el valor para el icono de cada negocio 
UPDATE [General].[CommercesTypes]
	SET [IconId] = (SELECT [Id] 
					FROM [Management].[CatalogDetail]
					WHERE [Name] = 'mdi-store')
	WHERE [Name] = 'Comercio'
	
GO

UPDATE [General].[CommercesTypes]
	SET [IconId] = (SELECT [Id] 
					FROM [Management].[CatalogDetail]
					WHERE [Name] = 'mdi-home')
	WHERE [Name] = 'Casa'
	
GO

UPDATE [General].[CommercesTypes]
	SET [IconId] = (SELECT [Id] 
					FROM [Management].[CatalogDetail]
					WHERE [Name] = 'mdi-domain')
	WHERE [Name] = 'Trabajo'
