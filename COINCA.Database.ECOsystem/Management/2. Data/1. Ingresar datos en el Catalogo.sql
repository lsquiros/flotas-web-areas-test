USE [Management]
GO

DELETE FROM [Management].[Catalog]
GO

SET IDENTITY_INSERT [Management].[Catalog] ON
GO

INSERT INTO [Management].[Catalog]
           ([Name] ,[Description] ,[Active] ,[InsertDate] ,[InsertUserId] ,[ModifyDate] ,[ModifyUserId])
     VALUES('Tipos de Eventos Principales', 'Tipado para lo eventos principales.', 1, GETDATE(), NULL, NULL, NULL)
GO

INSERT INTO [Management].[Catalog]
           ([Name] ,[Description] ,[Active] ,[InsertDate] ,[InsertUserId] ,[ModifyDate] ,[ModifyUserId])
     VALUES('Tipos de Eventos Secundarios', 'Tipado para lo eventos secundarios.', 1, GETDATE(), NULL, NULL, NULL)

GO

INSERT INTO [Management].[Catalog]
           ([Name] ,[Description] ,[Active] ,[InsertDate] ,[InsertUserId] ,[ModifyDate] ,[ModifyUserId])
     VALUES('Tipos de Preguntas', 'Listado de tipos para la creaci�n de preguntas.', 1, GETDATE(), NULL, NULL, NULL)
GO

INSERT INTO [Management].[Catalog] VALUES ( 'Iconos', 'Listado de Iconos para la creaci�n de Eventos.', 1, GETDATE(), NULL, NULL, NULL )

GO

SET IDENTITY_INSERT [Management].[Catalog] OFF
GO
