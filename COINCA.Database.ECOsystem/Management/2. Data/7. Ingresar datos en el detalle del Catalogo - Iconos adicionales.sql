--USE [ECOsystem]
GO
------------------------------------------------------------------------------
--  Ingresar datos en el detalle del Catalogo - Iconos adicionales
------------------------------------------------------------------------------

DECLARE @lCatalogId INT

SET @lCatalogId = (SELECT Id FROM [Management].[Catalog] WHERE [Name] = 'Iconos')

INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-account-clock' ,'account-clock' ,'mdi-account-clock' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-alert-circle-outline' ,'alert-circle-outline' ,'mdi-alert-circle-outline' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-briefcase' ,'briefcase' ,'mdi-briefcase' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-calendar-check' ,'calendar-check' ,'mdi-calendar-check' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-camera' ,'camera' ,'mdi-camera' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-checkbox-marked' ,'checkbox-marked' ,'mdi-checkbox-marked' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-circle-slice-5' ,'circle-slice-5' ,'mdi-circle-slice-5' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-clock' ,'clock' ,'mdi-clock' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-clock-start' ,'clock-start' ,'mdi-clock-start' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-clock-end' ,'clock-end' ,'mdi-clock-end' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-factory' ,'factory' ,'mdi-factory' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-map-marker' ,'map-marker' ,'mdi-map-marker' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-message-text' ,'message-text' ,'mdi-message-text' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-nature-people' ,'nature-people' ,'mdi-nature-people' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-play' ,'play' ,'mdi-play' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-star' ,'star' ,'mdi-star' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-stop' ,'stop' ,'mdi-stop' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-tag' ,'tag' ,'mdi-tag' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'mdi-timer' ,'timer' ,'mdi-timer' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)

GO
