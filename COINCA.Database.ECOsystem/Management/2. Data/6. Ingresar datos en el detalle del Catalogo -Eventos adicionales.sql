USE [ECOsystem]
GO

DECLARE @lCatalogId INT,
		@lParent INT


SET @lCatalogId = (SELECT Id FROM [Management].[Catalog] WHERE [Name] = 'Tipos de Eventos Secundarios')

INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Pierde Se�al GPS' ,'AGENT_LOSES_SIGNAL_GPS' ,'El agente pierde la se�al de GPS' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)
INSERT INTO [Management].[CatalogDetail] VALUES(@lCatalogId ,'Recupera Se�al GPS' ,'AGENT_RESTORE_SIGNAL_GPS' ,'El agente restablece la se�al de GPS' ,0 ,1 ,GETDATE() ,NULL ,NULL ,NULL)

GO
