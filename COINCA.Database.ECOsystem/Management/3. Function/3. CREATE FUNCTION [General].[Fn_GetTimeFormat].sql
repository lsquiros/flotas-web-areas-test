use [ECOsystem]
GO

IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Fn_GetTimeFormat]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [General].[Fn_GetTimeFormat]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Henry Retana
-- Create date: 13/10/2018
-- Description:	Returns Time Format
-- =============================================

CREATE FUNCTION [General].[Fn_GetTimeFormat]
(
	 @pHours VARCHAR(5)
	,@pMinutes VARCHAR(5)
	,@pSeconds VARCHAR(5)
)
RETURNS VARCHAR(20)
AS
BEGIN
	DECLARE @lResult VARCHAR(20)

	IF CAST(@pMinutes AS INT) < 0 SET @pMinutes = CAST(@pMinutes AS INT) * -1
	IF CAST(@pSeconds AS INT) < 0 SET @pSeconds = CAST(@pSeconds AS INT) * -1

	SET @lResult = CASE WHEN @pHours = '0'
						THEN '00'
						WHEN LEN(@pHours) = 1
						THEN '0' + @pHours
						WHEN @pHours  IS NULL
						THEN '00'
						ELSE @pHours
				   END + ':' + CASE WHEN @pMinutes = '0'
									THEN '00'
									WHEN LEN(@pMinutes) = 1
									THEN '0' + @pMinutes
									WHEN @pMinutes IS NULL 
									THEN '00'
									ELSE @pMinutes
								END + ':' + CASE WHEN @pSeconds = '0'
												 THEN '00'
												 WHEN LEN(@pSeconds) = 1
												 THEN '0' + @pSeconds
												 WHEN @pSeconds IS NULL 
												 THEN '00'
												 ELSE @pSeconds
											END
	RETURN @lResult
END