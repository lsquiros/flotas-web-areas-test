USE [ECOsystem]
GO

IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Fn_GetDistanceFromPointsByDates]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [Management].[Fn_GetDistanceFromPointsByDates]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Henry Retana
-- Create date: 05/07/2017
-- Description:	Get the distance from points
-- =============================================

CREATE FUNCTION [Management].[Fn_GetDistanceFromPointsByDates]
(
	@pUserId INT,
	@pStartDate DATETIME,
	@pEndDate DATETIME
)
RETURNS DECIMAL(16, 2)
AS
BEGIN

	DECLARE @ResultOutput DECIMAL(16, 2) = 0
	       ,@lCount INT
		   ,@lLatitude FLOAT
		   ,@lLongitude FLOAT
		   ,@lLatitudeLast FLOAT
		   ,@lLongitudeLast FLOAT

	DECLARE @Data TABLE 
	(		
		[Id] INT IDENTITY,
		[Longitude] FLOAT,
		[Latitude] FLOAT		
	)

	INSERT INTO @Data
	SELECT [Longitude]
	      ,[Latitude]
	FROM [Management].[UserEvent] 
	WHERE [UserId] = @pUserId
	AND [EventDate] BETWEEN @pStartDate AND @pEndDate
	AND [Latitude] IS NOT NULL
	ORDER BY [EventDate] ASC

	SELECT @lCount = MIN([Id])
	FROM @Data

	WHILE @lCount IS NOT NULL 
	BEGIN
		
		SELECT @lLatitude = [Latitude]
			  ,@lLongitude = [Longitude]
		FROM @Data
		WHERE [Id] = @lCount

		SELECT @lLatitudeLast = [Latitude]
			  ,@lLongitudeLast = [Longitude]
		FROM @Data
		WHERE [Id] = @lCount + 1

		IF @lLatitudeLast IS NOT NULL 
		BEGIN
			DECLARE @lSource GEOGRAPHY = GEOGRAPHY::Point(@lLatitude, @lLongitude, 4326)
			DECLARE @lTarget GEOGRAPHY = GEOGRAPHY::Point(@lLatitudeLast, @lLongitudeLast, 4326)

			SELECT @ResultOutput = (@ResultOutput + (@lSource.STDistance(@lTarget) / 1000))
		END

		SELECT @lCount = MIN([Id])
		FROM @Data
		WHERE [Id] > @lCount
	END

	RETURN @ResultOutput
END