USE [Management]
GO

IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Fn_HasAgendaByCommerce]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [Management].[Fn_HasAgendaByCommerce]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 02/10/2018
-- Description:	Get the agenda by commerce
-- =============================================

CREATE FUNCTION [Management].[Fn_HasAgendaByCommerce]
(
	@pCommerceId INT
)
RETURNS BIT
AS
BEGIN
	DECLARE @Result BIT

	IF EXISTS(SELECT COUNT([Id]) 
				FROM [Management].[Agenda]
				WHERE [CommerceId] = @pCommerceId)
	BEGIN
			SET @Result = 1
	END
	ELSE
	BEGIN
			SET @Result = 0
	END

	RETURN @Result
END