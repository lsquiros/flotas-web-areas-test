USE [Management]
GO
/****** Object:  StoredProcedure [Management].[Spu_DeleteAgenda]    Script Date: 26/09/2018 11:41:10 a.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Survey_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Survey_Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================  
-- Author:		Marjorie Garbanzo Morales
-- Create date: 11/10/2018
-- Description: Delete item ot the Survey
-- ========================================================================================================  

CREATE PROCEDURE [Management].[Sp_Survey_Delete]
(
	@pId INT,
	@pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON  
	SET XACT_ABORT ON  
  
	BEGIN TRY  
		DECLARE @lErrorMessage VARCHAR(MAX), 
				@lErrorSeverity INT,
				@lErrorState INT,
                @lLocalTran BIT = 0   

        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		----Elimina las opciones a las preguntas asociadas al formulario a elimina
		UPDATE [Management].[Option]
			SET [Delete] = 1,
				[ModifyUserId] = @pUserId,
				[ModifyDate] = GETDATE()
			WHERE [QuestionId] IN (SELECT [Id]
									FROM [Management].[Question]
									WHERE [SurveyId] = @pId)

		----Elimina las preguntas asociadas al formulario a elimina
		UPDATE [Management].[Question]
			SET [Delete] = 1,
				[ModifyUserId] = @pUserId,
				[ModifyDate] = GETDATE()
			WHERE [SurveyId] = @pId
			
		----Elimina el formulario
		UPDATE [Management].[Survey]
			SET [Delete] = 1,
				[ModifyUserId] = @pUserId,
				[ModifyDate] = GETDATE()
			WHERE [Id] = @pId
		
        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH  
  
	SET NOCOUNT OFF  
	SET XACT_ABORT OFF  	
END
