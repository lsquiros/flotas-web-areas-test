USE [Management]
GO
/****** Object:  StoredProcedure [Management].[Sp_Commerces_Retrieve]    Script Date: 19/09/2018 09:39:22 a.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Commerce_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Commerce_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 19/09/2018
-- Description:	Retrieve Commerces 
-- ================================================================================================
CREATE PROCEDURE [Management].[Sp_Commerce_Retrieve]
(
	 @pLatitude FLOAT
	,@pLongitude FLOAT					
	,@pCustomerId INT	
)
AS
BEGIN
	SET NOCOUNT ON	

	DECLARE @lDistance INT  
	DECLARE @lTop INT
	DECLARE @lStringPoint GEOGRAPHY

	SELECT @lDistance = [NumericValue] FROM [dbo].[GeneralParameters]
						WHERE ParameterId = 'DISTANCE_FOR_COMMERCES'	

	SELECT @lTop = [NumericValue] FROM [dbo].[GeneralParameters]
				   WHERE ParameterId = 'TOP_OF_THE_COMMERCES'	

	SET @lStringPoint = geography::STPointFromText('POINT(' + CAST(@pLongitude AS VARCHAR(20)) + ' ' + 	CAST(@pLatitude AS VARCHAR(20)) + ')', 4326)

	SELECT TOP (@lTop) [Id]
					  ,[Name]
					  ,[Description]
					  ,[Latitude]
					  ,[Longitude]
					  ,[Address]
	FROM [General].[Commerces] 
	WHERE @lStringPoint.STDistance(geography::STPointFromText('POINT(' + CAST([Longitude] AS VARCHAR(20)) + ' ' + CAST([Latitude] AS VARCHAR(20)) + ')', 4326)) <= @lDistance   
	AND [CustomerId] = @pCustomerId
	
	SET NOCOUNT OFF
END
