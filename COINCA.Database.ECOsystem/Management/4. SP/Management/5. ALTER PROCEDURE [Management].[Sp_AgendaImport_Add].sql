USE [Management]
GO
/****** Object:  StoredProcedure [Management].[Sp_AgendaImport_Add]    Script Date: 18/09/2018 02:59:33 p.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_AgendaImport_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_AgendaImport_Add]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================  
-- Author:		Henry Retana
-- Create date: 23/01/2018
-- Description: Add Information from the import
-- Modify by:   Marjorie Garbanzo - 18/09/2018 - Change AgentId by UserId
-- ========================================================================================================  

CREATE PROCEDURE [Management].[Sp_AgendaImport_Add]
(
	@pCustomerId INT,
	@pXmlData VARCHAR(MAX),
	@pCreateCommerce BIT = NULL, 
	@pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON  
	SET XACT_ABORT ON  
  
	BEGIN TRY  
		DECLARE @lErrorMessage VARCHAR(MAX)  
		DECLARE @lErrorSeverity INT  
		DECLARE @lErrorState INT  
		DECLARE @lLocalTran BIT = 0           
		DECLARE @lxmlData XML = CONVERT(XML, @pXmlData)
		            
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		CREATE TABLE #AgendaImport
		(
			[Id] INT IDENTITY,
			[Date] DATETIME,
			[Code] VARCHAR(50),
			[CommerceName] VARCHAR(200),
			[Latitude] FLOAT, 
			[Longitude] FLOAT, 
			[AgentCode] VARCHAR(50)
		)
	
		INSERT INTO #AgendaImport
		SELECT m.c.value('Date[1]', 'DATETIME'),
			   m.c.value('Code[1]', 'VARCHAR(50)'),
			   m.c.value('CommerceName[1]', 'VARCHAR(200)'),
			   m.c.value('Latitude[1]', 'FLOAT'),
			   m.c.value('Longitude[1]', 'FLOAT'),
			   m.c.value('AgentCode[1]', 'VARCHAR(50)')
		FROM @lxmlData.nodes('//AgendaImport') AS m(c)	
		
		IF @pCreateCommerce = 1 
		BEGIN
			--Create the new commerces
			INSERT INTO [General].[Commerces]
			(
				[Name],
				[Latitude],
				[Longitude],
				[Address],
				[Code],
				[InsertDate],
				[InsertUserId]
			)
			SELECT a.[CommerceName],
				   a.[Latitude], 
				   a.[Longitude],
				   [Efficiency].[Fn_GetAddressByLatLon_Retrieve](a.[Longitude], a.[Latitude], null),
				   a.[Code],
				   GETDATE(), 
				   @pUserId
			FROM #AgendaImport a
			LEFT JOIN [General].[Commerces] c
				ON a.[Code] = c.[Code]
			WHERE a.[Latitude] <> 0
			AND a.[Longitude] <> 0 
			AND c.[Code] IS NULL
		END 

	
 
		--Add the Data into the Agenda Table 
		INSERT INTO [Management].[Agenda]
		(
			[CommerceId],
			[UserId],
			[DateOfAgenda],
			[InsertDate],
			[LoggedUserId]
		)
		SELECT c.[Id],
			   d.[UserId],
			   a.[Date],
			   GETDATE(), 
			   @pUserId
		FROM #AgendaImport a
		INNER JOIN [General].[Commerces] c
			ON a.[Code] = c.[Code]
		INNER JOIN [General].[DriversUsers] d
			ON a.[AgentCode] = d.[Code]
			AND d.[CustomerId] = @pCustomerId
						
		--Return the not saved data 
		SELECT DISTINCT a.[Date],
						a.[Code],
						a.[CommerceName],
						a.[Latitude], 
						a.[Longitude], 
						a.[AgentCode]						
		FROM #AgendaImport a
		WHERE a.[Id] NOT IN (SELECT a.[Id]
							 FROM #AgendaImport a
							 INNER JOIN [General].[Commerces] c
							 	ON a.[Code] = c.[Code]
							 INNER JOIN [General].[DriversUsers] d
							 	ON a.[AgentCode] = d.[Code]
							 	AND d.[CustomerId] = @pCustomerId)
	   AND a.[Date] IS NOT NULL
		        
        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION					
	END TRY  
	BEGIN CATCH  
		IF (@@TRANCOUNT > 0 AND XACT_STATE() > 0)	ROLLBACK TRANSACTION 
  
		SELECT @lErrorMessage = ERROR_MESSAGE()  
			  ,@lErrorSeverity = ERROR_SEVERITY()  
			  ,@lErrorState = ERROR_STATE()  
  
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)  
	END CATCH  
  
	SET NOCOUNT OFF  
	SET XACT_ABORT OFF  	
END