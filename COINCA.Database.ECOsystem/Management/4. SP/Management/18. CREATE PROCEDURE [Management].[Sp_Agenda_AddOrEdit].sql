USE [Management]
GO
/****** Object:  StoredProcedure [Management].[Sp_Agenda_AddOrEdit]    Script Date: 26/09/2018 07:05:07 a.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Agenda_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Agenda_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================  
-- Author:		Albert Estrada Ponce
-- Create date: Enero/29/2018
-- Description: Add Agenda
-- Modify By: Marjorie Garbanzo - 26/09/2018 - Change name and AgentId for UserId
-- ======================================================================================================== 
CREATE PROCEDURE [Management].[Sp_Agenda_AddOrEdit]
(
	@pCommerceID INT,
	@pUserId INT,
	@pDateOfAgenda DATE,
	@pLoggedUserId INT,
	@pCustomerId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON  
	SET XACT_ABORT ON  
  
	BEGIN TRY  
		DECLARE @lErrorMessage VARCHAR(MAX)  
		DECLARE @lErrorSeverity INT  
		DECLARE @lErrorState INT  
		DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0             
		            
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		------------------------------------------------------------------
		INSERT INTO [Management].[Agenda] ([CommerceId], [UserId], [DateOfAgenda], [Status], [LoggedUserId], [InsertDate], [InsertUserId])
								VALUES (@pCommerceID, @pUserId, @pDateOfAgenda, 0, @pLoggedUserId, getdate(), @pLoggedUserId)

		EXEC [Management].[Sp_Agenda_Retrieve] @pUserId, @pDateOfAgenda, @pCustomerId
		------------------------------------------------------------------

        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION
		END TRY  
		BEGIN CATCH  
		IF (@@TRANCOUNT > 0 AND XACT_STATE() > 0)	ROLLBACK TRANSACTION 
  
		SELECT @lErrorMessage = ERROR_MESSAGE()  
			  ,@lErrorSeverity = ERROR_SEVERITY()  
			  ,@lErrorState = ERROR_STATE()  
  
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)  
	END CATCH  
  
	SET NOCOUNT OFF  
	SET XACT_ABORT OFF  	
END