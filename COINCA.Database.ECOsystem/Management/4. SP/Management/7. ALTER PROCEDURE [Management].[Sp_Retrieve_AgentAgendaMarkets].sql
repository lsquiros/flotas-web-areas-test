USE [Management]
GO
/****** Object:  StoredProcedure [Management].[Sp_Retrieve_AgentAgendaMarkets]    Script Date: 18/09/2018 03:30:13 p.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_AgentAgendaMarkets]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_AgentAgendaMarkets]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 30/01/2016
-- Description:	Retrieve Agent Agenda Markets for Management Map
-- Modify by:   Marjorie Garbanzo - 18/09/2018 - Change AgentId by UserId
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_AgentAgendaMarkets]
(
	  @pUserId INT = NULL,
	  @pStartDate DATETIME = NULL,
	  @pEndDate DATETIME = NULL
)
AS
BEGIN	
	SET NOCOUNT ON	

	SELECT c.[Id],
	       c.[Latitude],
		   c.[Longitude],
		   c.[Name] [CommerceName]
	FROM [Management].[Agenda] a
	INNER JOIN [General].[Commerces] c
		ON a.[CommerceId] = c.[Id]
	WHERE a.[UserId] = @pUserId
	AND a.[DateOfAgenda] BETWEEN @pStartDate AND @pEndDate
	ORDER BY a.[DateOfAgenda]

    SET NOCOUNT OFF
END
