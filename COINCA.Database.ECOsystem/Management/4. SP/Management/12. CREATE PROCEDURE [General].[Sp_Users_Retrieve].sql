USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Users_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Andres Oviedo B.
-- Create date: Sep/01/2015
-- Description:	Retrieve User information
-- ================================================================================================
-- Modify by:	Stefano Quiros Ruiz & Gerald Solano
-- Modify date: Jun/20/2016
-- Description:	Mejoras de rendimiento
-- ================================================================================================
-- Modify by:	Melvin Salas
-- Modify date:	Jun/27/2016
-- Description:	Delete key validation because use encrypted email and name
-- ================================================================================================
-- Modify by:	Cindy Vargas
-- Modify date:	Jul/19/2016
-- Description:	Add new parameter @pbindObtPhoto for obtain the current Photo
-- CASE WHEN @pbindObtPhoto = 1 THEN a.[Photo] WHEN @pbindObtPhoto = 0 THEN ''   END AS Photo
-- Exec [General].[Sp_Users_Retrieve] 6649,'', NULL, 23, NULL, NULL, 0
-- Modify Henry Retana 7/21/16 - Create local variable
-- ================================================================================================
-- Modify by:	Stefano Quirós
-- Modify date:	Nov/16/2016
-- Description:	Add new Filter, User IsActive or not and new value on the retrieve, isLockedOut
-- @iscustomer 
-- ================================================================================================
-- Modify by:	Kevin Peña
-- Modify date:	Mar/30/2017
-- Description:	Quit Logo Value, convert to CAST on iscustomerUser and IsPartnerUser
-- ================================================================================================
-- Modify by:	Henry Retana
-- Modify date:	18/05/2017
-- Description:	Order by PartnerId 
-- ================================================================================================
-- Modify by:	Henry Retana
-- Modify date:	27/10/2017
-- Description:	Add show news to the retrieve
-- ================================================================================================
-- Modify by:	Esteban Solís
-- Modify date:	17/04/2018
-- Description:	Add TypeId to retrieve for PUMA Partner
-- ================================================================================================
-- Modify by:	Stefano Quirós
-- Modify date:	19/09/2018
-- Description:	If User is not a DriverUser return the CustomerId from CustomerUser table and add
-- the IsAgent to the retrieve
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_Users_Retrieve] 
(
	  @pUserId INT = NULL
	 ,@pKey VARCHAR(800) = NULL
	 ,@pUserName VARCHAR(800) = NULL
	 ,@pCustomerId INT = NULL
	 ,@pCustomerId2 INT = NULL
	 ,@pPartnerId INT = NULL
	 ,@pbindObtPhoto BIT = NULL
	 ,@pUserStatus INT = NULL
	 ,@pIsCustomer BIT = 1
	 ,@pLoadSingleUser BIT = NULL
)
AS
BEGIN

	SET NOCOUNT ON	
	
	DECLARE @RoleId NVARCHAR(128) 
	SELECT @RoleId = Id FROM [dbo].[AspNetRoles] WHERE Name = 'CUSTOMER_CHANGEODOMETER'

	DECLARE @AspNetUserId NVARCHAR(128) 
	SELECT @AspNetUserId = AspNetUserId FROM [General].[Users] WHERE UserId = @pUserId

	DECLARE @ExistsChangeOdometer BIT
	SELECT @ExistsChangeOdometer = 1 FROM [dbo].[AspNetUserRoles] UR WHERE UR.UserId = @AspNetUserId AND UR.RoleId = @RoleId

	IF @pCustomerId2 IS NULL   ---Provisional se arregla en 2.12 definitivo
	BEGIN
		SELECT   a.[UserId]
				,a.[Name] AS [EncryptedName]
				,a.[ChangePassword]
				,CASE @pLoadSingleUser WHEN 1 
									   THEN a.[Photo] 
									   ELSE '' 
				 END [Photo]
				,a.[IsActive]
				,b.[Email] [EncryptedEmail]
				,b.[PhoneNumber] [EncryptedPhoneNumber]
				,CONVERT(BIT, CASE WHEN (a.[IsLockedOut] = 1 OR b.[LockoutEndDateUtc] IS NOT NULL) 
								   THEN 1 
								   ELSE 0 
							  END) [IsLockedOut]
				,b.[UserName] [EncryptedUserName]
				,d.[Id] [RoleId]
				,d.[Name] [RoleName]
				,CONVERT(BIT, CASE WHEN e.[CustomerUserId] IS NOT NULL 
								   THEN 1 
								   ELSE 0 
							  END) [IsCustomerUser]
				,CONVERT(BIT, CASE WHEN ((SELECT anur.[Id] 
										  FROM [dbo].[AspNetRoles] anur 
										  WHERE d.[Parent] = anur.[Id]) = (SELECT [Id] 
																		   FROM [dbo].[AspNetRoles] 
																		   WHERE [Name] = 'SERVICESTATION_ADMIN') 
										  OR d.[Name] = 'SERVICESTATION_ADMIN') 
								  THEN 1 
								  ELSE 0 
							 END) [IsServiceStationUser]
				,CONVERT(BIT, CASE WHEN g.[PartnerUserId] IS NOT NULL 
								   THEN 1 
								   ELSE 0 
							  END) [IsPartnerUser]
				,a.[PasswordExpirationDate]
				,CONVERT(BIT, CASE WHEN a.[PasswordExpirationDate] < GETDATE() 
								   THEN 1 
								   ELSE 0 
							  END) [IsPasswordExpired]
				,e.[CustomerUserId]
				,e.[CustomerId]
				,g.[PartnerUserId]
				,g.[PartnerId]
				,g.[InsertDate]
				,e.[Identification] [EncryptedIdentification]
				,CASE WHEN i.[CountryId] IS NULL 
					  THEN h.[CountryId] 
					  ELSE i.[CountryId] 
				 END AS [CountryId]
				,h.[PartnerGroupId]
				,a.[IsAgent]
				,a.[RowVersion]
				,ISNULL(a.[ShowNews], 0) [ShowNews]
				,h.[TypeId]
				,CONVERT(BIT, CASE WHEN su.[UserId] IS NOT NULL
								   THEN 1 
								   ELSE 0 
							  END) [IsSupportUser]
			   ,a.[AddCustomers]
		FROM [General].[Users] a
		INNER JOIN [dbo].[AspNetUsers] b
			ON a.[AspNetUserId] = b.[Id]
		INNER JOIN [dbo].[AspNetUserRoles] c
			ON c.[UserId] = a.[AspNetUserId]
		INNER JOIN [dbo].[AspNetRoles] d
			ON d.[Id] = c.[RoleId]
		LEFT JOIN [General].[CustomerUsers] e
			ON e.[UserId] = a.[UserId]			
		LEFT JOIN [General].[Customers] i
			ON i.[CustomerId] = e.[CustomerId]
		LEFT JOIN [General].[PartnerUsers] g
			ON g.[UserId] = a.[UserId]
		LEFT JOIN [General].[Partners] h
			ON g.PartnerId = h.PartnerId
		LEFT JOIN [Stations].[ServiceStationsUsersByPartner] ssp
			ON ssp.[PartnerId] = @pPartnerId
				AND	ssp.[UserId] = a.[UserId]
		LEFT JOIN [General].[SupportUsers] su
			ON su.[UserId] = a.[UserId]
		WHERE a.[IsDeleted] = 0
		AND (@pUserId IS NULL OR a.[UserId] = @pUserId)
		AND (@pUserName IS NULL OR b.[UserName] = @pUserName)
		AND (@pCustomerId IS NULL
				OR e.[CustomerId] = @pCustomerId)
		AND (@pPartnerId IS NULL
				OR h.[PartnerId] = @pPartnerId 
				OR ssp.[PartnerId] = @pPartnerId
				OR su.[PartnerId] = @pPartnerId)
		AND d.[Name] NOT IN ('CUSTOMER_CHANGEODOMETER', 'VPOS_USER')
		AND (@pUserStatus IS NULL OR a.[IsActive] = @pUserStatus)
		ORDER BY g.[PartnerId] DESC			
	END
	ELSE
	BEGIN
		SELECT	 a.[UserId]
				,a.[Name] [EncryptedName]
				,a.[ChangePassword]
				,f.[DriversUserId]
				,ISNULL(f.[CustomerId], i.[CustomerId]) [CustomerId]
				,f.[Identification] [EncryptedIdentification]
				,f.[Code] [EncryptedCode]
				,f.[License] [EncryptedLicense]
				,f.[LicenseExpiration]
				,f.[Dallas] [EncryptedDallas]
				,i.[CountryId]
				,f.[DailyTransactionLimit]
				,f.[CardRequestId]
				,(SELECT TOP 1 dkd.[DallasId] 
				  FROM [General].[DallasKeysByDriver] dkd 
				  WHERE dkd.[UserId] = @pUserId) [DallasId]
				,a.[IsActive]
				,b.[Email] [EncryptedEmail]
				,b.[PhoneNumber] [EncryptedPhoneNumber]
				,CONVERT(BIT, CASE WHEN (a.[IsLockedOut] = 1 OR b.[LockoutEndDateUtc] IS NOT NULL) 
								   THEN 1 
								   ELSE 0 
							  END) [IsLockedOut]
				,b.[UserName] [EncryptedUserName]
				,d.[Id] [RoleId]
				,d.[Name] [RoleName]
				,a.[PasswordExpirationDate]
				,CONVERT(BIT, CASE WHEN a.[PasswordExpirationDate] < GETDATE() 
								   THEN 1 
								   ELSE 0 
						      END) [IsPasswordExpired]		
				,a.[IsAgent]	
				,a.[RowVersion]
				,CONVERT(BIT,1) [IsDriverUser]
				,ISNULL(a.[ShowNews], 0) [ShowNews]
		FROM [General].[Users] a
		INNER JOIN [dbo].[AspNetUsers] b
			ON a.[AspNetUserId] = b.[Id]
		INNER JOIN [dbo].[AspNetUserRoles] c
			ON c.UserId = a.AspNetUserId
		INNER JOIN [dbo].[AspNetRoles] d
			ON d.[Id] = c.[RoleId]			
		LEFT JOIN [General].[DriversUsers] f
			ON f.[UserId] = a.[UserId]
		LEFT JOIN [General].[CustomerUsers] e
			ON e.[UserId] = a.[UserId]
		LEFT JOIN [General].[Customers] i
			ON i.CustomerId = e.CustomerId			
		WHERE a.[IsDeleted] = 0
		AND (@pUserId IS NULL OR f.[UserId] = @pUserId)
		AND (@pUserName IS NULL OR b.[UserName] = @pUserName)
		AND ((@pCustomerId2 IS NULL OR @pCustomerId2 = 0) 
			  OR f.[CustomerId] = @pCustomerId2)		  
		AND (@pUserStatus IS NULL OR a.[IsActive] = @pUserStatus)
	END

	SET NOCOUNT OFF
END
