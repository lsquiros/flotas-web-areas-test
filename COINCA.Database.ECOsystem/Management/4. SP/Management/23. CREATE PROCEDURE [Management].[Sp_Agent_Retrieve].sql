USE [Management]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Agent_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Agent_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Modify by: Henry Retana 
-- Modify date: 10/04/2018
-- Description: Agent Retrieve 
-- ================================================================================================  
  
CREATE PROCEDURE [Management].[Sp_Agent_Retrieve]  
(  
	 @pUserId INT = NULL  
	,@pCustomerId INT = NULL  	
)  
AS  
BEGIN    
	SET NOCOUNT ON   
 
	SELECT	 a.[UserId]  
			,a.[Name] AS [EncryptName]  			
	FROM [General].[Users] a  
	INNER JOIN [dbo].[AspNetUsers] b  
		ON a.[AspNetUserId] = b.[Id]
	LEFT JOIN [General].[DriversUsers] f  
		ON f.[UserId] = a.[UserId]  
	LEFT JOIN [General].[CustomerUsers] e  
		ON e.[UserId] = a.[UserId]  
	LEFT JOIN [General].[Customers] i  
		ON i.[CustomerId] = e.[CustomerId]     
	WHERE a.[IsDeleted] = 0  
	AND (@pUserId IS NULL OR f.[UserId] = @pUserId)  
	AND (@pCustomerId IS NULL OR f.[CustomerId] = @pCustomerId)        
	AND a.[IsAgent] = 1 
  
	SET NOCOUNT OFF  
END  