--USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Answer_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Answer_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 19/09/2018
-- Description:	Add or Edit the UserAnswer
-- Madify by:   Marjorie Garbanzo Morales - Add managed the temporarily Commerce ID
-- ================================================================================================
CREATE PROCEDURE [Management].[Sp_Answer_AddOrEdit] 
(
	  @pSurveyId INT					
	 ,@pEventDetailId INT						
	 ,@pCommerceId INT = NULL
	 ,@pTempCommerceId BIGINT = NULL
	 ,@pQuestionsXML VARCHAR(MAX)					
	 ,@pCustomerId INT					
	 ,@pUserId INT					
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0               
			   ,@lRowCount INT = 0 
			   ,@pUserEventId INT = 0 
			   ,@lxmlQuestions XML = CONVERT(XML, @pQuestionsXML)
			    
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		IF ((@pCommerceId IS NULL OR @pCommerceId = 0) AND (@pTempCommerceId IS NOT NULL))
		BEGIN
			SELECT @pCommerceId = [CommerceId] 
			FROM [Management].[TemporaryRelationshipCommerce]
			WHERE [TempCommerceId] = @pTempCommerceId
		END

		SET @pUserEventId = (SELECT MAX(ue.[Id]) 
							FROM [Management].[UserEvent] ue
								INNER JOIN [Management].[EventDetail] ed
									ON ue.[EventTypeId] = ed.[EventTypeId]
							WHERE ed.[Id] = @pEventDetailId
								AND ue.[CustomerId] = @pCustomerId
								AND ue.[CommerceId] = @pCommerceId
								AND ue.[UserId] = @pUserId)

		SELECT m.c.value('../../QuestionId[1]', 'int') AS [QuestionId],
			   m.c.value('.[1]', 'varchar(MAX)') AS [OptionId]
		INTO #ANSWERS
		FROM @lxmlQuestions.nodes('//Answer/Answers/int') AS m(c)

		SELECT m.c.value('QuestionId[1]', 'int') AS [QuestionId]
			  ,'' AS [Answers]
			  ,m.c.value('Data[1]', 'varchar(MAX)') AS [Data]	
		INTO #TEMP 
		FROM @lxmlQuestions.nodes('//Answer') AS m(c) 

		INSERT INTO [Management].[UserAnswer] (
			 [UserEventId]
			,[SurveyId]
			,[QuestionId]
			,[OptionId]
			,[Data] 
			,[Delete]
			,[InsertUserId]
			,[InsertDate]
		)
		SELECT  @pUserEventId [UserEventId]
			   ,@pSurveyId [SurveyId]
			   ,t.QuestionId [QuestionId]
			   ,a.OptionId [OptionId]
			   ,t.Data [Data] 
			   ,0 [Delete]
			   ,@pUserId [InsertUserId]
			   ,GETDATE() [InsertDate]
		FROM #TEMP t	
		LEFT JOIN #ANSWERS a
			ON t.[QuestionId] = a.[QuestionId]

		DROP TABLE #TEMP, #ANSWERS 
       
        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION			
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF 
END
