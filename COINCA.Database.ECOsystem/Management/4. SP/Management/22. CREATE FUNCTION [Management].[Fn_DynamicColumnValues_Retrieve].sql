USE [Management]
GO
/****** Object:  UserDefinedFunction [Management].[Sp_DynamicColumnValues_Retrieve]    Script Date: 01/10/2018 11:29:24 a.m. ******/

IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Fn_DynamicColumnValues_Retrieve]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [Management].[Fn_DynamicColumnValues_Retrieve]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Sebastian Quesada
-- Create date: 05/06/2018
-- Description:	xml columnas dinamicas y valores, consulta para modificar mantenimiento de agenda 
-- Modify by: Marjorie Garbanzo - 02/10/2018 - Change name and return data with CustomerId = NULL
-- ===============================================================================================

CREATE FUNCTION [Management].[Fn_DynamicColumnValues_Retrieve] 
(
	@pAgendaId INT,
    @pCustomerId INT = NULL	
)
RETURNS VARCHAR(MAX)
AS
BEGIN 
    DECLARE @lxmlData XML
	DECLARE @lResult VARCHAR(MAX)

    DECLARE @lColumns TABLE 
	(
		[Id] INT,
		[Title] VARCHAR(500)
	)
	DECLARE @lValues TABLE 
	(
		[Id] INT,
		[Value] VARCHAR(500)
	)

	--GET THE COLUMNS VALUES
	IF (@pCustomerId IS NULL)
	BEGIN
		
		SET @lxmlData = CONVERT(XML, (SELECT [AdditionalColumns] 
									  FROM [Management].[Agenda] 
									  WHERE [Id] = @pAgendaId))
									  
		INSERT INTO @lColumns
		SELECT m.c.value('Id[1]', 'INT')
			  ,m.c.value('Title[1]', 'VARCHAR(MAX)') 
		FROM @lxmlData.nodes('//AditionalColumns') AS m(c)

	END
	ELSE
	BEGIN
		SET @lxmlData = CONVERT(XML, (SELECT [DynamicColumns] 
									  FROM [Management].[Parameters] 
									  WHERE CustomerId = @pCustomerId))
    
		INSERT INTO @lColumns
		SELECT m.c.value('Id[1]', 'INT')
			  ,m.c.value('Value[1]', 'VARCHAR(500)') 
		FROM @lxmlData.nodes('//AdittionalAgendaColumn') AS m(c)
		
	END

    
	--GET THE ANSWERS 
    SET @lxmlData = CONVERT(XML, (SELECT [AdditionalColumns] 
								  FROM [Management].[Agenda] 
								  WHERE [Id] = @pAgendaId))

    INSERT INTO @lValues
    SELECT m.c.value('Id[1]', 'INT') [Id]
        ,m.c.value('Value[1]', 'VARCHAR(500)') [Value]
    FROM @lxmlData.nodes('//AditionalColumns') AS m(c)
	
	SELECT @lResult =  CONVERT(VARCHAR(MAX), ISNULL((SELECT  c.[Id]
															,c.[Title]
															,ISNULL(v.[Value], '') [Value]
															,@pAgendaId [AgendaId]
													FROM @lColumns c
													LEFT JOIN @lValues v
														ON c.[Id] = v.[Id]
													FOR XML PATH ('AditionalColumns'), Root('AditionalColumns'), ELEMENTS XSINIL), ''))

	RETURN @lResult
END