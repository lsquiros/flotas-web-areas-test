--USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_UserStop_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_UserStop_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana 
-- Create date: 17/10/2018
-- Description:	Add or Edit the User Stop
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_UserStop_AddOrEdit]
(
	  @pEventTypeId INT
	 ,@pUserId INT		
	 ,@pCustomerId INT
	 ,@pLatitude FLOAT
	 ,@pLongitude FLOAT
	 ,@pBatteryLevel INT = NULL
	 ,@pHeading VARCHAR(20) = NULL
	 ,@pSpeed INT = NULL
	 ,@pAltitude INT = NULL
	 ,@pEventDateTime DATETIME
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0
			   ,@lLocation INT 
			   ,@lStopHistory BIT = 0

		DECLARE @lResult TABLE 
		(
			[Lapse] INT, 
			[IsStop] BIT,
			[Update] BIT
		)
			    
        IF @@TRANCOUNT = 0
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END

		--VALIDATES IF THERE IS STOP HISTORY
		SELECT @lStopHistory = CAST(COUNT(1) AS BIT)
		FROM [Management].[UserStop]
		WHERE [UserId] = @pUserId
		AND [CustomerId] = @pCustomerId
				
		--GET THE LOCATION EVENT TYPE
		SELECT @lLocation = [Value]
		FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'
		
		--CHECK IF THE CURRENT EVENT IS LOCATION
		IF @lLocation = @pEventTypeId
		BEGIN 
			--VALIDATES IF THE EVENT IS A STOP
			INSERT INTO @lResult
			SELECT *
			FROM [Management].[Fn_GetStatusOfStop] (
													  @pUserId 
													 ,@pCustomerId 
													 ,@pLatitude  
													 ,@pLongitude 
													 ,@pSpeed
													 ,@pEventDateTime  
													 ,@lStopHistory    
												  )

			--IF THE EVENT IS NOT A STOP UPDATE THE DATA FOR THE USER
			IF EXISTS (
						SELECT *
						FROM @lResult
						WHERE [IsStop] = 0
						AND [Update] = 1
					  )
			BEGIN									
				INSERT INTO [Management].[UserStop]
				(
					[UserId],
					[CustomerId],
					[Latitude],
					[Longitude],
					[BatteryLevel],
					[Heading],
					[Speed],
					[Altitude],
					[EventDate],
					[InitStopDate]	
				)	
				VALUES
				(
						@pUserId			
					,@pCustomerId 
					,@pLatitude 
					,@pLongitude 
					,@pBatteryLevel 
					,@pHeading 
					,@pSpeed 
					,@pAltitude 
					,NULL
					,@pEventDateTime
				)
			END
			ELSE IF EXISTS (
								SELECT *
								FROM @lResult
								WHERE [IsStop] = 1
								AND [Update] = 1
							)
			BEGIN
				UPDATE [Management].[UserStop]
				SET [BatteryLevel] = @pBatteryLevel,
					[Heading] = @pHeading,
					[Speed] = @pSpeed,
					[Altitude] = @pAltitude,
					[EventDate] = @pEventDateTime
				WHERE [UserId] = @pUserId
				AND [CustomerId] = @pCustomerId
			END
			ELSE IF EXISTS (
								SELECT *
								FROM @lResult
								WHERE [IsStop] = 0
								AND [Update] = 0
							)
			BEGIN
				--REMOVE HISTORIC
				DELETE FROM [Management].[UserStop]
				WHERE [UserId] = @pUserId
				AND [CustomerId] = @pCustomerId
			END 
			
		END 
		ELSE 
		BEGIN 
			--VALIDATES IF THERE IS STOP DETAIL
			IF @lStopHistory = 1
			BEGIN 
				INSERT INTO @lResult
				SELECT *
				FROM [Management].[Fn_GetStatusOfStop] (
														  @pUserId 
														 ,@pCustomerId 
														 ,@pLatitude  
														 ,@pLongitude 
														 ,@pSpeed
														 ,@pEventDateTime  
														 ,@lStopHistory    
													  )

				IF EXISTS (
							SELECT *
							FROM @lResult
							WHERE [IsStop] = 0
						  )
				BEGIN 
					--REMOVE HISTORIC
					DELETE FROM [Management].[UserStop]
					WHERE [UserId] = @pUserId
					AND [CustomerId] = @pCustomerId
				END
				ELSE
				BEGIN 
					--SET THE UPDATE DEFAULT 0
					UPDATE @lResult
					SET [Update] = 0
				END 
			END
		END

		--IF THE EVENT IS A STOP, GET THE DATA FOR THE EVENT INSERTED
		IF EXISTS (
					SELECT *
					FROM @lResult
					WHERE [IsStop] = 1
					AND [Update] = 0
				  )
		BEGIN 
			DECLARE @lEventStopId INT 

			SELECT @lEventStopId = [Id] 
			FROM [Management].[CatalogDetail]
			WHERE [Value] = 'AGENT_STOP'

			--INSERT THE EVENT AS STOP
			INSERT INTO [Management].[UserEvent]
			(				 
				 [EventTypeId]
				,[UserId]
				,[CustomerId]
				,[Latitude]
				,[Longitude]
				,[BatteryLevel]
				,[Heading]
				,[Speed]
				,[Altitude]
				,[Lapse]
				,[EventDate]
				,[InsertDate]
				,[InsertUserId]
			)
			SELECT @lEventStopId,
				   [UserId],
				   [CustomerId],
				   [Latitude],
				   [Longitude],
				   [BatteryLevel],
				   [Heading],
				   [Speed],
				   [Altitude],
				   (SELECT TOP 1 [Lapse]
					FROM @lResult),
				   @pEventDateTime,
				   GETDATE(),
				   [UserId]
			FROM [Management].[UserStop]
			WHERE [UserId] = @pUserId
			AND [CustomerId] = @pCustomerId
			
			--REMOVE HISTORIC
			DELETE FROM [Management].[UserStop]
			WHERE [UserId] = @pUserId
			AND [CustomerId] = @pCustomerId
		END
								     
        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION			
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF 
END
