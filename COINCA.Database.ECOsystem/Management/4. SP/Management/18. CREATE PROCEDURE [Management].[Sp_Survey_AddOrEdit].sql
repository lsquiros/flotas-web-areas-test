USE [Management]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Survey_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Survey_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 26/09/2018
-- Description:	Add or Edit the Survey
-- ================================================================================================
CREATE PROCEDURE [Management].[Sp_Survey_AddOrEdit]
(
	@pId INT = NULL,
	@pName VARCHAR(200),
	@pDescription VARCHAR(500) = NULL,
	@pQuestionsStr VARCHAR(MAX),
	@pCustomerId INT,
	@pUserId INT					
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
   BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0
			   ,@lQuestions XML = CONVERT(XML, @pQuestionsStr)
			   ,@lCountId INT 
			    
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
				
		IF @pId IS NULL 
		BEGIN 
			INSERT INTO [Management].[Survey]
			(
				[CustomerId],
				[Name],
				[Description],
				[InsertUserId],
				[InsertDate]
			)
			VALUES
			(
				@pCustomerId,
				@pName,
				@pDescription,
				@pUserId,
				GETDATE()
			)
			SET @pId = @@IDENTITY
		END
		ELSE 
		BEGIN 
			UPDATE [Management].[Survey]
			SET [Name] = @pName, 
				[Description] = @pDescription
			WHERE [Id] = @pId
		END

		--SET ALL QUESTIONS DELETED
		UPDATE [Management].[Question]
		SET [Delete] = 1
		WHERE [SurveyId] = @pId
		
		--GET THE QUESTIONS INTO THE TABLE 
		SELECT m.c.value('Id[1][not(@xsi:nil = "true")]', 'INT') [Id],			  
			   @pId [SurveyId],
			   m.c.value('QuestionTypeId[1][not(@xsi:nil = "true")]', 'INT') [QuestionTypeId],
			   m.c.value('Text[1]', 'VARCHAR(200)') [Text],
			   m.c.value('Description[1]', 'VARCHAR(200)') [Description],
			   m.c.value('OptionsStr[1]', 'VARCHAR(MAX)') [OptionsStr],
			   m.c.value('Order[1]', 'INT') [Order],
			   m.c.value('Delete[1]', 'BIT') [Delete]
		INTO #QUESTIONS
		FROM @lQuestions.nodes('//Question') AS m(c) 
		WHERE m.c.value('Id[1][not(@xsi:nil = "true")]', 'INT') IS NOT NULL

		SELECT @lCountId = MIN([Order])
		FROM #QUESTIONS

		--GO THROUGH THE LIST OF QUESTIONS
		WHILE @lCountId IS NOT NULL 
		BEGIN 
			DECLARE @lOptions XML,
					@lQuestionId INT, 
					@lId INT 

			SELECT	@lQuestionId = [Id],
					@lOptions = CONVERT(XML, [OptionsStr])
			FROM #QUESTIONS
			WHERE [Order] = @lCountId

			--INSERT THE QUESTIONS IN THE TABLE
			INSERT INTO [Management].[Question]
			(
				[SurveyId], 
				[QuestionTypeId],
				[Text],
				[Description],
				[Order],
				[Delete],
				[InsertUserId],
				[InsertDate]
			)
			SELECT [SurveyId], 
				   [QuestionTypeId],
				   [Text],
				   [Description],
				   [Order],
				   [Delete],
				   @pUserId,
				   GETDATE()
			FROM #QUESTIONS
			WHERE [Id] = @lQuestionId

			SET @lId = @@IDENTITY

			--SET THE OPTIONS AS DELETE 
			UPDATE [Management].[Option]
			SET [Delete] = 1
			WHERE [QuestionId] = @lQuestionId

			--UPDATE THE OPTIONS RELATED TO THE NEW NEXT QUESTION IN THE NEW OPTIONS
			UPDATE [Management].[Option]
			SET [NextQuestionId] = @lId			
			WHERE [NextQuestionId] = @lQuestionId
			AND [Delete] = 0

			INSERT INTO [Management].[Option]
			(
				[QuestionId],
				[Text],
				[Order],
				[NextQuestionId],	
				[Delete],			
				[InsertUserId],
				[InsertDate]
			)
			SELECT @lId,
				   m.c.value('Text[1]', 'VARCHAR(200)'),
				   m.c.value('Order[1]', 'VARCHAR(200)'),
				   m.c.value('NextQuestionId[1][not(@xsi:nil = "true")]', 'INT'),
				   m.c.value('Delete[1]', 'BIT'),
				   @pUserId,
				   GETDATE()
			FROM @lOptions.nodes('//Option') AS m(c) 
			WHERE m.c.value('Id[1][not(@xsi:nil = "true")]', 'INT') IS NOT NULL

			SELECT @lCountId = MIN([Order])
			FROM #QUESTIONS
			WHERE [Order] > @lCountId
		END 

        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION			
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF 
END
