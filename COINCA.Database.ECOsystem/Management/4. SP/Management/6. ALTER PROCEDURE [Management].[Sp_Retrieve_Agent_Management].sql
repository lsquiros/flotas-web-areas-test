USE [Management]
GO
/****** Object:  StoredProcedure [Management].[Sp_Retrieve_Agent_Management]    Script Date: 18/09/2018 03:26:45 p.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_Agent_Management]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_Agent_Management]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Stefano Quirós
-- Create date: 02/02/2018
-- Description:	Retrieve Agent Management
-- Modify by:   Albert Estrada Date:28/02/2018 Add AdditionalColumns
-- Modify by:   Marjorie Garbanzo - 18/09/2018 - Change AgentId by UserId
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_Agent_Management]
(
	  @pUserId INT = NULL,
	  @pStartDate DATETIME = NULL,
	  @pEndDate DATETIME = NULL
)
AS
BEGIN	
	SET NOCOUNT ON	

	SELECT c.[Id],	       
		   c.[Name],
		   c.[Description],
		   c.[Address],
		   c.[Latitude],
		   c.[Longitude],
		   a.[AdditionalColumns]
	FROM [Management].[Agenda] a
	INNER JOIN [General].[Commerces] c
		ON a.[CommerceId] = c.[Id]
	WHERE a.[UserId] = @pUserId
	AND a.[DateOfAgenda] BETWEEN @pStartDate AND @pEndDate
	ORDER BY a.[DateOfAgenda]

    SET NOCOUNT OFF
END
 