USE [Management]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_CommerceByAgentsAndDate]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_CommerceByAgentsAndDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:	Henry Retana
-- Create date: 12/10/2018
-- Description:	Retrieve Information of Commerces
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_CommerceByAgentsAndDate] 
(
	  @pUserId INT = NULL,	
      @pDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON 

	DECLARE @lDateStart DATETIME = DATEADD(dd, 0, DATEDIFF(dd, 0, @pDate))
	DECLARE @lEndStart DATETIME = DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, 1, @pDate) ))
	
	SELECT DISTINCT c.[Id], 
					c.[Name]
	FROM [Management].[UserEvent] u
	INNER JOIN [General].[commerces] c 
		ON u.[CommerceId] = c.[Id]
	WHERE  u.[UserId] = @pUserId 
	and u.[EventDate] BETWEEN @lDateStart AND @lEndStart

	SET NOCOUNT OFF
END
 

