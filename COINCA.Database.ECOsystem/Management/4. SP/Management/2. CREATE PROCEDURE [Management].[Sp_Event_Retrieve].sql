USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Event_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Event_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 18/09/2018
-- Description:	Event Retrieve
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Event_Retrieve]
(	
	@pId INT = NULL,
	@pCustomerId INT
)
AS
BEGIN	
	SET NOCOUNT ON

    SELECT [Id],
		   [Tracking],
		   [Lapse],
		   [CommerceRequired],
		   [Parent],
		   CAST((SELECT [SurveyId],
						(SELECT [Name]
						 FROM [Management].[Survey]
						 WHERE [Id] = [SurveyId]) [SurveyName],
					    [Id],
						[EventTypeId],
						(SELECT [Name]
					     FROM [Management].[CatalogDetail]
					     WHERE [Id] = [EventTypeId]) [EventTypeName],
					    [Name],
					    [Color],
						[IconId],
					    (SELECT [Value]
					     FROM [Management].[CatalogDetail]
					     WHERE [Id] = [IconId]) [Icon]
				 FROM [Management].[EventDetail]
				 WHERE [Id] = e.[EventDetailInitId] FOR XML PATH('EventDetail'), ROOT('EventDetail'), ELEMENTS XSINIL) AS VARCHAR(MAX)) [InitStr],
			CAST((SELECT [SurveyId],
						 (SELECT [Name]
						  FROM [Management].[Survey]
						  WHERE [Id] = [SurveyId]) [SurveyName],
						 [Id],
						 [EventTypeId],
						 (SELECT [Name]
					      FROM [Management].[CatalogDetail]
					      WHERE [Id] = [EventTypeId]) [EventTypeName],
						 [Name],
						 [Color],
						 [IconId],
					     (SELECT [Value]
					      FROM [Management].[CatalogDetail]
					      WHERE [Id] = [IconId]) [Icon]
				 FROM [Management].[EventDetail]
				 WHERE [Id] = e.[EventDetailFinishId] FOR XML PATH('EventDetail'), ROOT('EventDetail'), ELEMENTS XSINIL) AS VARCHAR(MAX)) [FinishStr]
	FROM [Management].[Event] e
	WHERE e.[CustomerId] = @pCustomerId
	AND (@pId IS NULL OR e.[Id] = @pId)
	AND (e.[Delete] IS NULL OR e.[Delete] = 0)
	ORDER BY e.[Parent]

	SET NOCOUNT OFF
END
