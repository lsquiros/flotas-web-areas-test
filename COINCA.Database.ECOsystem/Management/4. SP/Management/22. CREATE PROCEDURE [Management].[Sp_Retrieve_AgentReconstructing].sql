USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_AgentReconstructing]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_AgentReconstructing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 19/09/2018
-- Description:	Agent Reconstructing Retrieve
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_AgentReconstructing]
(	
	@pUserId INT,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL
)
AS
BEGIN	
	SET NOCOUNT ON

	DECLARE @lBeginJourneyEvent INT,
			@lLocation INT,
			@lLocationName VARCHAR(200), 
			@lMainCatalog INT 

	--Get Begin Journey 
	SELECT @lBeginJourneyEvent = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'

	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'

	SELECT @lMainCatalog = [Id]
	FROM [Management].[Catalog]
	WHERE [Name] = 'Tipos de Eventos Principales'

	SELECT @lLocationName = p.[MovementName]
	FROM [Management].[Parameters] p 
	INNER JOIN [Management].[UserEvent] u
		ON p.[CustomerId] = u.[CustomerId]
	WHERE u.[UserId] = @pUserId
	
	IF @pStartDate IS NULL SET @pStartDate = GETDATE()
	IF @pEndDate IS NULL SET @pEndDate = DATEADD(DAY, 1, @pStartDate)

	SELECT  u.[Id],
			u.[UserId],
			u.[EventDetailId],
			ed.[Name] [EventDetailName],
			u.[EventTypeId],
			c.[Name] [EventName],
			c.[Parent],
			u.[CommerceId],
			u.[Latitude],
			u.[Longitude],
			u.[BatteryLevel],
			u.[Speed],
			u.[EventDate]
	INTO #REPORTS
	FROM [Management].[UserEvent] u
	INNER JOIN [Management].[CatalogDetail] c
		ON u.[EventTypeId] = c.[Id]
	LEFT JOIN [Management].[EventDetail] ed
		ON u.[EventDetailId] = ed.[Id]
	WHERE u.[UserId] = @pUserId 
	AND (c.[CatalogId] = @lMainCatalog OR c.[Id] = @lLocation)
	AND u.[EventDate] BETWEEN @pStartDate AND @pEndDate
	ORDER BY u.[EventDate];

	WITH Temp AS
	(
		SELECT TOP 1 * 
		FROM #REPORTS
		UNION
		SELECT r.*
		FROM #REPORTS r,
				#REPORTS r2
		WHERE r.[Id] = (r2.[Id] + 1) 
		AND r.[EventTypeId] <> r2.[EventTypeId]
	)
	SELECT  t.[Id],
			t.[UserId],
			t.[EventTypeId],
			t.[EventDetailName],
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN ISNULL(@lLocationName, t.[EventName])
				 ELSE ISNULL(t.[EventDetailName], t.[EventName]) 
			END [EventName],
			c.[Name] [CommerceName],
			t.[Parent],
			t.[Latitude],
			t.[Longitude],
			t.[BatteryLevel],
			t.[Speed],
			t.[EventDate],
			t.[EventDate] [StartDate], 
			(SELECT TOP 1 r.[EventDate] 
			 FROM temp r
			 WHERE r.[Id] > t.[Id]
			 ORDER BY r.[Id]) [EndDate],
			 [General].[Fn_GetTimeFormat](ISNULL(DATEDIFF(S, t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																			 FROM temp r
																			 WHERE r.[Id] > t.[Id]
																			 ORDER BY r.[Id])) / 3600, 0),
		  			RIGHT('0' + CAST(ISNULL(DATEDIFF(S, t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																FROM temp r
																WHERE r.[Id] > t.[Id]
																ORDER BY r.[Id])) % 3600 / 60, 0) AS VARCHAR(2)), 2),
					RIGHT('0' + CAST(ISNULL(DATEDIFF(S, t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																FROM temp r
																WHERE r.[Id] > t.[Id]
																ORDER BY r.[Id])) % 60, 0) AS VARCHAR(2)), 2)) [Lapse],
			CASE WHEN t.[Latitude] IS NULL 
				 THEN 0
				 ELSE [Management].[Fn_GetDistanceFromPointsByDates] (@pUserId,  t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																								 FROM temp r
																								 WHERE r.[Id] > t.[Id]
																								 ORDER BY r.[Id])) 
			END [Distance]
	FROM temp t
	LEFT JOIN [General].[Commerces] c
		ON t.[CommerceId] = c.[Id]
	WHERE [Parent] = 0	
	AND [EventTypeId] <> @lBeginJourneyEvent 
	UNION 
	SELECT  [Id],
			[UserId],	
			[EventTypeId],	
			[EventDetailName],
			ISNULL([EventDetailName], [EventName]) [EventName],
			NULL [CommerceName],
			[Parent],
			[Latitude],
			[Longitude],
			[BatteryLevel],
			[Speed],			
			[EventDate],
			[EventDate] [StartDate],
			DATEADD(SECOND, 1, [EventDate]) [EndDate],
			NULL [Lapse],
			NULL [Distance]
	FROM #REPORTS
	WHERE ([EventTypeId] = @lBeginJourneyEvent OR [Parent] = @lBeginJourneyEvent)
	ORDER BY [EventDate]

	DROP TABLE #REPORTS

	SET NOCOUNT OFF
END