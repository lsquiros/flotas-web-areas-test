USE [Management]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_UserEvent_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_UserEvent_Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================      
-- Author:  Marjorie Garbanzo Morales      
-- Create date: 19/09/2018      
-- Description: Delete UserEvent
-- ================================================================================================      
CREATE PROCEDURE [Management].[Sp_UserEvent_Delete] 
(		
	  @pUserId INT
	 ,@pEventDetailId INT = NULL	
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0 
               ,@pEventTypeId INT = 0
			    
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		SET @pEventTypeId = (SELECT [EventTypeId] FROM [Management].[EventDetail]
														WHERE [Id] = @pEventDetailId)
		IF(@pEventDetailId IS NULL)
		BEGIN
			UPDATE [Management].[UserEvent]
			SET  [Delete] = 1
				,[ModifyUserId] = @pUserId
				,[ModifyDate] = GETDATE()
			WHERE [Id] = (SELECT max([Id]) 
						  FROM [Management].[UserEvent]
						  WHERE [UserId] = @pUserId)
		END
		ELSE
		BEGIN
			UPDATE [Management].[UserEvent]
				SET  [Delete] = 1
					,[ModifyUserId] = @pUserId
					,[ModifyDate] = GETDATE()
			WHERE [EventTypeId] = @pEventTypeId 
				AND [UserId] = @pUserId
				AND [Id] = (SELECT max([Id]) 
						    FROM [Management].[UserEvent]
						    WHERE [EventTypeId] = @pEventTypeId 
								AND [UserId] = @pUserId)
		END

        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION			
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF 
END
