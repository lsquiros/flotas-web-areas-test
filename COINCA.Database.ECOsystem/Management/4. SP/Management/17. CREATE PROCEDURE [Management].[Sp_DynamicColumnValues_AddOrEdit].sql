USE [Management]
GO
/****** Object:  StoredProcedure [Management].[Sp_DynamicColumnValues_AddOrEdit]    Script Date: 27/09/2018 11:39:35 a.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_DynamicColumnValues_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_DynamicColumnValues_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:		Sebastian Quesada
-- Create date: 05/06/2018
-- Description:	Modifica el valor de las columnas dinamicas, mantenimiento de agenda, Modulo Gestión
-- Modify by:   Change size of pXml parameter
-- ======================================================================================================

CREATE PROCEDURE [Management].[Sp_DynamicColumnValues_AddOrEdit]
(
	@pId INT,
	@pXml VARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END

            UPDATE [Management].[Agenda]
	        SET [AdditionalColumns] = @pXml
	        WHERE [Id] = @pId

			IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH

	SET NOCOUNT OFF
END	    