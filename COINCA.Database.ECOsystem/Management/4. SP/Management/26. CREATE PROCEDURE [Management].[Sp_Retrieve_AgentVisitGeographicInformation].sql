USE [Management]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_AgentVisitGeographicInformation]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_AgentVisitGeographicInformation]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 07/07/2017
-- Description:	Retrieve Visit Location Count Retrieve
-- Modify: Henry Retana - 05/10/2018
-- Use new tables 
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_AgentVisitGeographicInformation]
(
	  @pId INT = NULL,
	  @pCustomerId INT = NULL, 		
      @pStartDate DATETIME = NULL,
	  @pEndDate DATETIME = NULL,
	  @pCountryCode VARCHAR(3)
)
AS
BEGIN
	
	SET NOCOUNT ON	

	DECLARE @lCount INT
	DECLARE @lUserName VARCHAR(200)
	DECLARE @lAgentVisit INT

	SELECT @lAgentVisit = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'CUSTOMERS_CLIENT_BEGIN_VISIT'
	
	IF @pId = -1 SET @pId = NULL
	IF @pId IS NOT NULL SET @pCustomerId = NULL

	DECLARE @lUserIds TABLE ([Id] INT) 
		
	INSERT INTO @lUserIds
	SELECT [UserId]
	FROM [General].[DriversUsers]
	WHERE (@pId IS NULL OR [UserId] = @pId)
		AND (@pCustomerId IS NULL OR [CustomerId] = @pCustomerId)	 
	
	CREATE TABLE #Data
	(
		[Id] INT IDENTITY,		
		[Longitude] FLOAT,
		[Latitude] FLOAT
	 )

	CREATE TABLE #Result
	(
	    [Id] INT IDENTITY,
		[GeopoliticalLevel1] VARCHAR(200), 
		[GeopoliticalLevel2] VARCHAR(200),
		[GeopoliticalLevel3] VARCHAR(200)
	)

	INSERT INTO #Data
	SELECT [Longitude],
		   [Latitude]		   
	FROM [Management].[UserEvent] 	
	WHERE [UserId] IN (
							SELECT [Id]
							FROM @lUserIds
						)
	AND [EventDate] BETWEEN @pStartDate AND @pEndDate
	AND [EventTypeId] = @lAgentVisit
	ORDER BY [EventDate] ASC
	
	SELECT @lCount = MIN([Id]) 
	FROM #Data

	WHILE @lCount IS NOT NULL
	BEGIN
		DECLARE @lLatitude FLOAT,
				@lLongitude FLOAT

		SELECT @lLatitude = [Latitude],
			   @lLongitude = [Longitude]
		FROM #Data
		WHERE [Id] = @lCount

		INSERT INTO #Result
		EXEC [General].[Sp_Retrieve_GeographicInformation] @lLatitude, @lLongitude, @pCountryCode
		
		SELECT @lCount = MIN([Id]) 
		FROM #Data
		WHERE [Id] > @lCount
	END
	
	IF @pId IS NOT NULL
	BEGIN 
		SELECT @lUserName = [Name]
		FROM  [General].[Users]
		WHERE [UserId] = @pId	
	END 
	ELSE
	BEGIN
		SET @lUserName = 'bcAp9tgFmCw='
	END	

	SELECT COUNT(1) [VisitCount],
		   r.[GeopoliticalLevel1],
		   r.[GeopoliticalLevel2],
		   r.[GeopoliticalLevel3],
		   @lUserName [EncryptName]
	FROM #Result r	
	GROUP BY r.[GeopoliticalLevel1],
			 r.[GeopoliticalLevel2],
			 r.[GeopoliticalLevel3]

	DROP TABLE #Data
	DROP TABLE #Result
    SET NOCOUNT OFF
END