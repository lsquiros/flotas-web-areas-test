USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_ReportForms]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_ReportForms]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author: Henry Retana
-- Create date: 09/10/2018
-- Description:	Retrieve Information of Survey
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_ReportForms]
(
	@pUserId INT = NULL,
	@pCommerceId INT = NULL, 		
	@pDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	 @pDateStart DATETIME = DATEADD(dd, 0, DATEDIFF(dd, 0, @pDate)),
			 @pEndStart DATETIME = DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, 1, @pDate))),
			 @lImageEvent INT, 
			 @lSingImage INT

	SELECT @lImageEvent = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'QUESTIONS_TYPE_IMAGE'

	SELECT @lSingImage = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'QUESTIONS_TYPE_SIGN'	
	
	SELECT	s.[Name] [Survey],
			q.[Text] [Question],
			CASE WHEN ua.[Data] IS NOT NULL
					  AND q.[QuestionTypeId] <> @lImageEvent
					  AND q.[QuestionTypeId] <> @lSingImage
				THEN ua.[Data]
				ELSE o.[Text]
			END [Option],
			CASE WHEN q.[QuestionTypeId] = @lImageEvent 
					OR q.[QuestionTypeId] = @lSingImage
				 THEN ua.[Data]
				 ELSE ''
			END [Data],
			u.[Latitude],
			u.[Longitude],
			us.[Name] [UserName],
			u.[UserId],
			co.[Name] [CommerceName],
			u.[EventDate],
			[Efficiency].[Fn_GetAddressByLatLon_Retrieve] (u.[Longitude], u.[Latitude], u.[CustomerId]) [Address],
			ISNULL(ed.[Name], cd.[Name]) [EventName] 
	FROM [Management].[UserAnswer] ua
	INNER JOIN [Management].[UserEvent] u
		ON ua.[UserEventId] = u.[Id]
	INNER JOIN [Management].[Survey] s
		ON ua.[SurveyId] = s.[Id]
	INNER JOIN [Management].[Question] q
		ON ua.[QuestionId] = q.[Id]	
	INNER JOIN [General].[Users] us
		ON us.[UserId] = u.[UserId]
	INNER JOIN [Management].[CatalogDetail] cd
		ON u.[EventTypeId] = cd.[Id]
	LEFT JOIN [General].[Commerces] co
		ON u.[CommerceId] = co.[Id]
	LEFT JOIN [Management].[Option] o
		ON ua.[OptionId] = o.[Id]
	LEFT JOIN [Management].[EventDetail] ed
		ON u.[EventDetailId] = ed.[Id]

	WHERE u.[UserId] = @pUserId
	AND u.[CommerceId] = @pCommerceId
	AND u.[EventDate] BETWEEN @pDateStart AND @pEndStart	
	ORDER BY q.[Order]

	SET NOCOUNT OFF
END
 