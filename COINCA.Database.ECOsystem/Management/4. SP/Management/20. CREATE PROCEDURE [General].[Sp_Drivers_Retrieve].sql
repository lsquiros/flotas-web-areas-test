USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Drivers_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Drivers_Retrieve]  
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Modify by: Stefano Quirós  
-- Modify date: Marzo/27/2017  
-- Description: New Sp that retrieve the drivers data  
-- Modify: Henry Retana - 06/09/2017  
-- Add Cost Center Id to the retrieve  
-- Modify: Esteban Solís - 06/09/2017  
-- Add [PullPreviousBudget] field 
-- Modify By: Albert Estrada - 17/01/2018 -- Add [pAgent] field 
-- ================================================================================================  
  
CREATE PROCEDURE [General].[Sp_Drivers_Retrieve]  
(  
	 @pUserId INT = NULL  
	,@pKey VARCHAR(800) = NULL  
	,@pUserName VARCHAR(800) = NULL  
	,@pCustomerId INT = NULL  
	,@pUserStatus INT = NULL  
	,@pAgent bit = null
)  
AS  
BEGIN    
	SET NOCOUNT ON   
 
	SELECT	 a.[UserId]  
			,a.[Name] AS [EncryptedName]  
			,a.[ChangePassword]  
			,f.[DriversUserId]  
			,f.[CustomerId]  
			,f.[Identification] AS [EncryptedIdentification]  
			,f.[Code] AS [EncryptedCode]  
			,f.[License] AS [EncryptedLicense]  
			,f.[LicenseExpiration]  
			,f.[Dallas] AS EncryptedDallas  
			,i.[CountryId]  
			,f.[DailyTransactionLimit]  
			,f.[CardRequestId]  
			,(
				SELECT TOP 1 dkd.[DallasId]   
				FROM [General].[DallasKeysByDriver] dkd   
				WHERE dkd.[UserId] = @pUserId
			) AS [DallasId]  
			,a.[IsActive]  
			,b.[Email] AS [EncryptedEmail]  
			,b.[PhoneNumber] AS [EncryptedPhoneNumber]  
			,CONVERT(BIT, CASE WHEN (a.[IsLockedOut] = 1 OR b.[LockoutEndDateUtc] IS NOT NULL) 
								THEN 1 
								ELSE 0 
							END) AS [IsLockedOut]  
			,b.[UserName] AS [EncryptedUserName]  
			,d.[Id] AS [RoleId]  
			,d.[Name] AS [RoleName]  
			,a.[PasswordExpirationDate]  
			,CONVERT(BIT, CASE WHEN a.[PasswordExpirationDate] < GETDATE() 
								THEN 1 
								ELSE 0 
							END) AS [IsPasswordExpired]     		  
			,CONVERT(BIT,1) AS [IsDriverUser]  
			,f.[CostCenterId] 
			,f.[PullPreviousBudget]
			,a.[IsAgent]
            ,a.[Photo]
	FROM [General].[Users] a  
	INNER JOIN [dbo].[AspNetUsers] b  
		ON a.[AspNetUserId] = b.[Id]  
	INNER JOIN [dbo].[AspNetUserRoles] c  
		ON c.[UserId] = a.[AspNetUserId]  
	INNER JOIN [dbo].[AspNetRoles] d  
		ON d.[Id] = c.[RoleId]     
	LEFT JOIN [General].[DriversUsers] f  
		ON f.[UserId] = a.[UserId]  
	LEFT JOIN [General].[CustomerUsers] e  
		ON e.[UserId] = a.[UserId]  
	LEFT JOIN [General].[Customers] i  
		ON i.[CustomerId] = e.[CustomerId]     
	WHERE a.[IsDeleted] = 0  
	AND (@pUserId IS NULL OR f.[UserId] = @pUserId)  
	AND (@pUserName IS NULL OR b.[UserName] = @pUserName)  
	AND (@pCustomerId IS NULL OR f.[CustomerId] = @pCustomerId)      
	AND (@pUserStatus IS NULL OR a.[IsActive] = @pUserStatus)  
	AND (@pAgent IS NULL OR a.[IsAgent] = @pAgent)  
  
	SET NOCOUNT OFF  
END  