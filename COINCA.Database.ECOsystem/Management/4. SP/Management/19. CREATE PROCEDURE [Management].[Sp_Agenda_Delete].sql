USE [Management]
GO
/****** Object:  StoredProcedure [Management].[Spu_DeleteAgenda]    Script Date: 26/09/2018 11:41:10 a.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Agenda_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Agenda_Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================  
-- Author:		Albert Estrada Ponce
-- Create date: Enero/29/2018
-- Description: Delete item ot the agenda
-- Modify By: Stefano Quirós 
-- Description: Add the CustomerId to retrieve the data well
-- Modify By: Marjorie Garbanzo - 26/09/2018 - Change name and AgentId for UserId
-- ========================================================================================================  

CREATE PROCEDURE [Management].[Sp_Agenda_Delete]
(
	@pAgendaId INT
   ,@pCustomerId INT
)
AS
BEGIN
	SET NOCOUNT ON  
	SET XACT_ABORT ON  
  
	BEGIN TRY  
		DECLARE @lErrorMessage VARCHAR(MAX)  
		DECLARE @lErrorSeverity INT  
		DECLARE @lErrorState INT  
		DECLARE @lLocalTran BIT = 0            
		
		DECLARE @pUserId INT 
		DECLARE @pDateOfAgenda DATE

        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		-----------------------------------------------------------------
		SELECT @pUserId = UserId, @pDateOfAgenda = DateOfAgenda from [Management].[Agenda] WHERE [Id] = @pAgendaId
		
		UPDATE [Management].[Agenda]
			SET [Delete] = 1
		WHERE [Id] = @pAgendaId

		EXEC [Management].[Sp_Agenda_Retrieve] @pUserId, @pDateOfAgenda, @pCustomerId
		------------------------------------------------------------------
 
        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION
		END TRY  
		BEGIN CATCH  
		IF (@@TRANCOUNT > 0 AND XACT_STATE() > 0)	ROLLBACK TRANSACTION 
  
		SELECT @lErrorMessage = ERROR_MESSAGE()  
			  ,@lErrorSeverity = ERROR_SEVERITY()  
			  ,@lErrorState = ERROR_STATE()  
  
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)  
	END CATCH  
  
	SET NOCOUNT OFF  
	SET XACT_ABORT OFF  	
END