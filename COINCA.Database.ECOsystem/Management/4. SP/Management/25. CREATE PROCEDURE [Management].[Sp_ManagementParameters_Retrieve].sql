USE [EcoSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_ManagementParameters_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_ManagementParameters_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 21/07/2017
-- Description:	Retrieve management parameters
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_ManagementParameters_Retrieve]
(
	@pCustomerId INT
)
AS
BEGIN	
	SET NOCOUNT ON;

	SELECT [ClientMinutes],
		   [LostMinutes],
		   [MovingMinutes],
		   [TopMargin],
		   [LowMargin],
		   [TopPercent],
		   [LowPercent],
		   [MiddlePercent],
		   [GeofenceDistance],
		   [TimeBeginJourney],
		   [TimeFinishJourney],
		   [DataSync],
		   [MovementName], 
		   [ReconstructionType],
		   [StopMinimumTime],
		   [StopMinimumDistance],
		   [StopMinimumSpeed]
	FROM [Management].[Parameters]
	WHERE [CustomerId] = @pCustomerId	
		
	SET NOCOUNT OFF
END
