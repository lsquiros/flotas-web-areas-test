USE [Management]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Event_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Event_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Marjorie Garbanzo - Henry Retana 
-- Create date: 20/09/2018
-- Description:	Add or Edit the Event
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Event_AddOrEdit]
(	
	@pId INT = NULL,
    @pEventTypeId INT,
    @pLapse INT = NULL,
    @pTracking BIT,
    @pCommerceRequired BIT,
	@pParent INT = NULL,
    @pInit VARCHAR(500),
    @pFinish VARCHAR(500),
    @pCustomerId INT,
    @pUserId INT					
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0 
			   ,@lInit XML = CONVERT(XML, @pInit)
			   ,@lFinish XML = CONVERT(XML, @pFinish)			   
			   ,@lEventFinishId INT = NULL
			   ,@lBeginJourneyEvent INT 
			    
        IF @@TRANCOUNT = 0
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		--Get Begin Journey 
		SELECT @lBeginJourneyEvent = [Value]
		FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'

		SELECT @lEventFinishId = [Id]
		FROM [Management].[CatalogDetail] 
		WHERE [Parent] = @pEventTypeId

		IF @pId IS NULL 
		BEGIN 		
			DECLARE @lInitId INT
				   ,@lFinishId INT = NULL

			--INSERT DETAILS	
			INSERT INTO [Management].[EventDetail]
			(
				[SurveyId],
				[EventTypeId],
				[IconId],
				[Name],
				[Color],
				[Active],
				[InsertUserId],
				[InsertDate]
			)
			SELECT m.c.value('SurveyId[1][not(@xsi:nil = "true")]', 'INT')
				  ,@pEventTypeId
				  ,m.c.value('IconId[1][not(@xsi:nil = "true")]', 'INT')
				  ,m.c.value('Name[1]', 'VARCHAR(200)')
			      ,m.c.value('Color[1]', 'VARCHAR(50)')			      
				  ,1
				  ,@pUserId
				  ,GETDATE()
			FROM @lInit.nodes('//EventDetail') AS m(c) 

			SET @lInitId = @@IDENTITY

			--IF THERE IS INFORMATION FOR THE FINISH EVENT 
			IF EXISTS (
						SELECT m.c.value('Name[1]', 'VARCHAR(200)')
						FROM @lFinish.nodes('//EventDetail') AS m(c) 
					  )
			BEGIN 
				INSERT INTO [Management].[EventDetail]
				(
					[SurveyId],
					[EventTypeId],
					[IconId],
					[Name],
					[Color],
					[Active],
					[InsertUserId],
					[InsertDate]
				)
				SELECT m.c.value('SurveyId[1][not(@xsi:nil = "true")]', 'INT')
					  ,@lEventFinishId
					  ,m.c.value('IconId[1][not(@xsi:nil = "true")]', 'INT')
					  ,m.c.value('Name[1]', 'VARCHAR(200)')
					  ,m.c.value('Color[1]', 'VARCHAR(50)')			      	      
					  ,1
					  ,@pUserId
					  ,GETDATE()
				FROM @lFinish.nodes('//EventDetail') AS m(c) 

				SET @lFinishId = @@IDENTITY
			END

			INSERT INTO [Management].[Event]
			(
				[EventDetailInitId],
				[EventDetailFinishId],
				[CustomerId],
				[Tracking],
				[Lapse],
				[CommerceRequired],
				[Parent],
				[Active],				
				[InsertUserId],
				[InsertDate]
			)
			VALUES
			(
				@lInitId,
				@lFinishId,
				@pCustomerId,
				@pTracking,
				@pLapse,				
				@pCommerceRequired,
				@pParent,
				1,				
				@pUserId,
				GETDATE()
			)
			SET @pId = @@IDENTITY
		END 
		ELSE 
		BEGIN 
			UPDATE [Management].[Event]
			SET [Tracking] = @pTracking,
				[Lapse] = @pLapse,
				[CommerceRequired] = @pCommerceRequired,
				[Parent] = @pParent
			WHERE [Id] = @pId

			--UPDATE DETAILS 
			UPDATE [Management].[EventDetail]
			SET [SurveyId] = m.c.value('SurveyId[1][not(@xsi:nil = "true")]', 'INT'),
				[EventTypeId] = @pEventTypeId,
				[IconId] = m.c.value('IconId[1][not(@xsi:nil = "true")]', 'INT'),
				[Name] =  m.c.value('Name[1]', 'VARCHAR(200)'),
			    [Color] = m.c.value('Color[1]', 'VARCHAR(50)'),			      
				[ModifyUserId] = @pUserId,
				[ModifyDate] = GETDATE()
			FROM [Management].[EventDetail]
			INNER JOIN @lInit.nodes('//EventDetail') AS m(c) 
				ON [Management].[EventDetail].[Id] = m.c.value('Id[1]', 'INT')

			--IF THE FINISH EVENTS EXISTS IN THE EDITION AND THE TABLE, UPDATE THE DETAIL 
			IF EXISTS (
						SELECT m.c.value('Name[1]', 'VARCHAR(200)')
						FROM @lFinish.nodes('//EventDetail') AS m(c) 
					  )
			   AND EXISTS (
							 SELECT *
							 FROM [Management].[Event]
							 WHERE [Id] = @pId
							 AND [EventDetailFinishId] IS NOT NULL
						  )
			BEGIN 
				UPDATE [Management].[EventDetail]
				SET [SurveyId] = m.c.value('SurveyId[1][not(@xsi:nil = "true")]', 'INT'),
					[EventTypeId] = @lEventFinishId,
					[IconId] = m.c.value('IconId[1][not(@xsi:nil = "true")]', 'INT'),
					[Name] =  m.c.value('Name[1]', 'VARCHAR(200)'),
					[Color] = m.c.value('Color[1]', 'VARCHAR(50)'),
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				FROM [Management].[EventDetail]
				INNER JOIN @lFinish.nodes('//EventDetail') AS m(c) 
					ON [Management].[EventDetail].[Id] = m.c.value('Id[1]', 'INT')
			END
			--IF THE FINISH EVENT EXISTS IN THE EDITION AND NOT IN  THE TABLE, INSERT THE DETAIL
			ELSE IF EXISTS (
							SELECT m.c.value('Name[1]', 'VARCHAR(200)')
							FROM @lFinish.nodes('//EventDetail') AS m(c) 
						  )
				   AND NOT EXISTS (
								 SELECT *
								 FROM [Management].[Event]
								 WHERE [Id] = @pId
								 AND [EventDetailFinishId] IS NOT NULL
							  )
			BEGIN 
				INSERT INTO [Management].[EventDetail]
				(
					[SurveyId],
					[EventTypeId],
					[IconId],
					[Name],
					[Color],
					[Active],
					[InsertUserId],
					[InsertDate]
				)
				SELECT m.c.value('SurveyId[1][not(@xsi:nil = "true")]', 'INT')
					  ,@lEventFinishId
					  ,m.c.value('IconId[1][not(@xsi:nil = "true")]', 'INT')
					  ,m.c.value('Name[1]', 'VARCHAR(200)')
					  ,m.c.value('Color[1]', 'VARCHAR(50)')			      	      
					  ,1
					  ,@pUserId
					  ,GETDATE()
				FROM @lFinish.nodes('//EventDetail') AS m(c) 

				UPDATE [Management].[Event]
				SET [EventDetailFinishId] = @@IDENTITY 
				WHERE [Id] = @pId
			END
			ELSE 
			BEGIN 
				--REMOVE THE DETAIL IN THE EVENT
				UPDATE [Management].[Event]
				SET [EventDetailFinishId] = NULL
				WHERE [Id] = @pId
			END
		END 

		--UPDATE THE PARENT FOR THE OTHER EVENTS
		IF @lBeginJourneyEvent = @pEventTypeId
		BEGIN 
			UPDATE [Management].[Event]
			SET [Parent] = @pId
			WHERE [CustomerId] = @pCustomerId
			AND [Id] NOT IN (@pId)
			AND [Delete] = 0
		END 
		ELSE IF EXISTS (
							SELECT *
							FROM [Management].[Event] e
							INNER JOIN [Management].[EventDetail] ed
								ON e.[EventDetailInitId] = ed.[Id]
							WHERE ed.[EventTypeId] = @lBeginJourneyEvent
							AND e.[CustomerId] = @pCustomerId
							AND e.[Delete] = 0
						)
		BEGIN 
			SELECT @pParent = e.[Id]
			FROM [Management].[Event] e
			INNER JOIN [Management].[EventDetail] ed
				ON e.[EventDetailInitId] = ed.[Id]
			WHERE ed.[EventTypeId] = @lBeginJourneyEvent
			AND e.[CustomerId] = @pCustomerId
			AND e.[Delete] = 0

			UPDATE [Management].[Event]
			SET [Parent] = @pParent
			WHERE [CustomerId] = @pCustomerId
			AND [Id] NOT IN (@pParent)
			AND [Delete] = 0
		END
		ELSE 
		BEGIN 
			UPDATE [Management].[Event]
			SET [Parent] = NULL
			WHERE [CustomerId] = @pCustomerId
			AND [Delete] = 0
		END


        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION					
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF 
END