USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_MeasuringAgentsMonthlyReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_MeasuringAgentsMonthlyReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:		Henry Retana
-- Create date: 21/07/2017
-- Description:	Retrieve data for Report Measuring Agents Monthly
-- ======================================================================================================
CREATE PROCEDURE [Management].[Sp_Retrieve_MeasuringAgentsMonthlyReport]
(
	@pCustomerId INT,
	@pStartDate DATETIME, 
	@pEndDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lCount INT,
			@lLogIn INT,
			@lLogOut INT,
			@lStartVisit INT,
			@lEndVisit INT,
			@lLocation INT,
			@lWorkTime VARCHAR(20),						
			@lClientTime VARCHAR(20),
			@lMovingTime VARCHAR(20),			
			@lWorkMinutes INT,						
			@lClientMinutes INT,
			@lMovingMinutes INT,
			@lUserId INT

	--GET THE IDS FOR THE EVENTS--
	SELECT @lLogout = c.[Id],
		   @lLogin = c.[Parent]
	FROM [Management].[CatalogDetail] c
	INNER JOIN [dbo].[GeneralParameters] p
		ON c.[Parent] = p.[Value]
	WHERE p.[ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'	

	SELECT @lEndVisit= c.[Id],
		   @lStartVisit = c.[Parent]
	FROM [Management].[CatalogDetail] c
	INNER JOIN [dbo].[GeneralParameters] p
		ON c.[Parent] = p.[Value]
	WHERE p.[ParameterID] = 'CUSTOMERS_CLIENT_BEGIN_VISIT'
	
	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'
	--**************************--

	CREATE TABLE #Data
	(
		[Id] INT IDENTITY,
		[EventTypeId] INT,
		[EventDate] DATETIME
	)

	CREATE TABLE #TempData
	(
		[UserId] INT,
		[EventTypeId] INT,
		[Minutes] INT
	)

	CREATE TABLE #LOGINS
	(
		[UserId] INT,
		[LogIn] DATETIME,
		[LogOut] DATETIME
	)

	CREATE TABLE #Users
	(
		[Id] INT IDENTITY,
		[UserId] INT
	)

	CREATE TABLE #Result
	(
		[AgentWorkTime] VARCHAR(20), 
		[AgentClientTime] VARCHAR(20), 
		[AgentMovingTime] VARCHAR(20),
		[AgentLostTime] VARCHAR(20),
		[AgentLogIn] DATETIME,
		[AgentLogOut] DATETIME,
		[ClientDifference] INT,
		[MovingDifference] INT, 
		[LostDifference] INT, 
		[LogInDifference] INT,
		[LogOutDifference] INT,
		[ClientTime] VARCHAR(20),
		[LostTime] VARCHAR(20),
		[MovingTime] VARCHAR(20),
		[StartTime] INT,
		[EndTime] INT,
		[EncryptDriverName] VARCHAR(500),
		[TopMargin] INT,
		[LowMargin] INT,
		[TopPercent] INT,
		[LowPercent] INT,
		[MiddlePercent] INT,
		[UserId] INT
	)

	INSERT INTO #Users
	SELECT DISTINCT du.[UserId]
	FROM [General].[DriversUsers] du
	INNER JOIN [Management].[UserEvent] m
		ON du.[UserId] = m.[UserId]
	WHERE du.[CustomerId] = @pCustomerId
	AND m.[EventDate] BETWEEN @pStartDate AND @pEndDate

	SELECT @lCount = MIN([Id])
	FROM #Users

	WHILE @lCount IS NOT NULL 
	BEGIN
		DECLARE @lRowId INT

		SELECT @lUserId = [UserId]
		FROM #Users
		WHERE [Id] = @lCount

		INSERT INTO #Data
		SELECT [EventTypeId],
			   [EventDate]
		FROM [Management].[UserEvent]
		WHERE [UserId] = @lUserId
		AND [EventDate] BETWEEN @pStartDate AND @pEndDate

		SELECT @lRowId = MIN([Id])
		FROM #Data
	
		--GO THROUGH THE DATA 
		WHILE @lRowId IS NOT NULL 
		BEGIN 
			DECLARE @lEventTypeId INT,
					@lStartTime DATETIME,
					@lEndTime DATETIME,
					@lFinishId INT															

			SELECT @lEventTypeId = [EventTypeId], 
				   @lStartTime = [EventDate]
			FROM #Data
			WHERE [Id] = @lRowId

			--VALIDATE EVENTS 
			--Login
			IF @lEventTypeId = @lLogIn
			BEGIN 				
				INSERT INTO #LOGINS
				(
					[UserId],
					[LogIn]
				)				
				SELECT TOP 1 @lUserId,
							 [EventDate]
				FROM #Data
				WHERE [Id] = @lRowId

				UPDATE 	#LOGINS
				SET [LogOut] = (SELECT TOP 1 [EventDate]
								FROM #Data
								WHERE [EventTypeId] = @lLogOut 
								AND   [Id] > @lRowId)
				WHERE [UserId] = @lUserId

				SELECT TOP 1 @lFinishId = [Id],  
							 @lEndTime = [EventDate]
				FROM #Data
				WHERE [EventTypeId] = @lLogOut 
				AND   [Id] > @lRowId
				ORDER BY [EventDate] DESC

				DELETE FROM #Data
				WHERE [EventTypeId] = @lLogIn
				AND [Id] <> @lRowId

				DELETE FROM #Data
				WHERE [EventTypeId] = @lLogOut
			END
			--Visit
			ELSE IF @lEventTypeId = @lStartVisit
			BEGIN 
				SELECT TOP 1 @lFinishId = [Id],  
							 @lEndTime = [EventDate]
				FROM #Data
				WHERE [EventTypeId] = @lEndVisit
				AND   [Id] > @lRowId

				DELETE FROM #Data
				WHERE [Id] = @lFinishId
			END
			--Moving
			ELSE IF @lEventTypeId = @lLocation
			BEGIN 
				SELECT TOP 1 @lFinishId = [Id]
				FROM #Data
				WHERE [EventTypeId] <> @lLocation
				AND   [Id] > @lRowId
			
				IF @lFinishId < @lRowId
				BEGIN 
					SELECT TOP 1 @lFinishId = [Id]
					FROM #Data
					WHERE [EventTypeId] = @lLocation
					ORDER BY [Id] DESC

					SELECT TOP 1 @lFinishId = [Id],  
								 @lEndTime = [EventDate]
					FROM #Data
					WHERE [Id] = @lFinishId
				END 
				ELSE 
				BEGIN 
					SELECT TOP 1 @lFinishId = [Id],  
								 @lEndTime = [EventDate]
					FROM #Data
					WHERE [Id] = @lFinishId - 1
				END

				DELETE FROM #Data
				WHERE [Id] BETWEEN @lRowId AND @lFinishId
			END

			--Insert data in temp table
			INSERT INTO #TempData
			VALUES 
			(
				@lUserId,
				@lEventTypeId,
				DATEDIFF(MINUTE, @lStartTime, @lEndTime)
			)

			SELECT @lRowId = MIN([Id])
			FROM #Data
			WHERE [Id] > @lRowId
		END 

		TRUNCATE TABLE #Data

		SELECT @lCount = MIN([Id])
		FROM #Users
		WHERE [Id] > @lCount
	END 

	SELECT @lUserId = MIN([UserId])
	FROM #TempData 

	WHILE @lUserId IS NOT NULL
	BEGIN
		DECLARE @lStartHour INT,
				@lEndHour INT,
				@lLogInTime DATETIME = NULL,
				@lLogOutTime DATETIME = NULL

		SELECT @lLogInTime = ISNULL([LogIn], @pStartDate),
			   @lLogOutTime = ISNULL([LogOut], @pEndDate)
		FROM #LOGINS 
		WHERE [UserId] = @lUserId

		--Get the final Times result 
		SELECT @lWorkTime = CAST(SUM([Minutes]) / 60 AS VARCHAR) + ':' + RIGHT('0' + CAST(SUM([Minutes]) % 60 AS VARCHAR), 2),
			   @lWorkMinutes = SUM([Minutes])
		FROM #TempData
		WHERE [EventTypeId] = @lLogIn
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]

		SELECT @lClientTime = CAST(SUM([Minutes]) / 60 AS VARCHAR) + ':' + RIGHT('0' + CAST(SUM([Minutes]) % 60 AS VARCHAR), 2),
			   @lClientMinutes = SUM([Minutes])		   
		FROM #TempData
		WHERE [EventTypeId] = @lStartVisit
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]

		SELECT @lMovingTime = CAST(SUM([Minutes]) / 60 AS VARCHAR) + ':' + RIGHT('0' + CAST(SUM([Minutes]) % 60 AS VARCHAR), 2),
			   @lMovingMinutes = SUM([Minutes])
		FROM #TempData
		WHERE [EventTypeId] = @lLocation
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]

		SELECT @lStartHour = ISNULL((SELECT [Value] 
								     FROM [General].[ParametersByCustomer]
								     WHERE [CustomerId] = @pCustomerId
								     AND [Name] = 'StartTime'
								     AND [ResuourceKey] = 'NORMALVEHICLESCHEDULE'), 0)
		SELECT @lEndHour = ISNULL((SELECT [Value] 
								   FROM [General].[ParametersByCustomer]
								   WHERE [CustomerId] = @pCustomerId
								   AND [Name] = 'EndTime'
								   AND [ResuourceKey] = 'NORMALVEHICLESCHEDULE'), 0)
		INSERT INTO #Result
		SELECT ISNULL(@lWorkTime, 0),
			   ISNULL(@lClientTime, 0),
			   ISNULL(@lMovingTime, 0),
			   ISNULL(CAST((@lWorkMinutes - @lClientMinutes - @lMovingMinutes) / 60 AS VARCHAR) + ':' + RIGHT('0' + CAST((@lWorkMinutes - @lClientMinutes - @lMovingMinutes) % 60 AS VARCHAR), 2), 0),
			   ISNULL(@lLogInTime, 0),
			   ISNULL(@lLogOutTime, 0),
			   [ClientMinutes] - @lClientMinutes,
			   [MovingMinutes] - @lMovingMinutes, 
			   [LostMinutes] - (@lWorkMinutes - @lClientMinutes - @lMovingMinutes),
			   ISNULL(DATEDIFF(MINUTE, DATEADD(HOUR, @lStartHour, CONVERT(DATETIME, DATEDIFF(DAY, 0, @lLogInTime))), @lLogInTime), 0), 
			   ISNULL(DATEDIFF(MINUTE, DATEADD(HOUR, @lEndHour, CONVERT(DATETIME, DATEDIFF(DAY, 0, @lLogOutTime))), @lLogOutTime), 0),
			   CAST([ClientMinutes] / 60 AS VARCHAR) + ':' + RIGHT('0' + CAST([ClientMinutes] % 60 AS VARCHAR), 2),
			   CAST([LostMinutes] / 60 AS VARCHAR) + ':' + RIGHT('0' + CAST([LostMinutes] % 60 AS VARCHAR), 2),
			   CAST([MovingMinutes] / 60 AS VARCHAR) + ':' + RIGHT('0' + CAST([MovingMinutes] % 60 AS VARCHAR), 2),
			   @lStartHour,
			   @lEndHour,
			   (SELECT [Name]
				FROM [General].[DriversUsers] du
				INNER JOIN [General].[Users] u
				ON du.[UserId] = u.[UserId]
				WHERE [DriversUserId] = @lUserId),
			   [TopMargin],
			   [LowMargin],
			   [TopPercent],
			   [LowPercent],
			   [MiddlePercent],
			   @lUserId
		FROM [Management].[Parameters]
		WHERE [CustomerId] = @pCustomerId

		SELECT @lUserId = MIN([UserId])
		FROM #TempData
		WHERE [UserId] > @lUserId
	END

	--Returns Results with all the information
	SELECT r.[AgentWorkTime], 
		   r.[AgentClientTime], 
		   r.[AgentMovingTime],
		   r.[AgentLostTime],
		   r.[AgentLogIn],
		   r.[AgentLogOut],
		   r.[ClientDifference],
		   r.[MovingDifference], 
		   r.[LostDifference], 
		   r.[LogInDifference],
		   r.[LogOutDifference],
		   r.[ClientTime],
		   r.[LostTime],
		   r.[MovingTime],
		   r.[StartTime],
		   r.[EndTime],
		   u.[Name] [EncryptDriverName],
		   r.[TopMargin],
		   r.[LowMargin],
		   r.[TopPercent],
		   r.[LowPercent],
		   r.[MiddlePercent]
	FROM #Result r
	INNER JOIN [General].[Users] u
		ON r.[UserId] = u.[UserId]

	DROP TABLE #Data
	DROP TABLE #Users
	DROP TABLE #TempData
	DROP TABLE #Result
	DROP TABLE #LOGINS

	SET NOCOUNT OFF
END