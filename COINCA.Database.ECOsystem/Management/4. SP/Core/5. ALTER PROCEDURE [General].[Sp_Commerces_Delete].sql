USE [Management]
GO
/****** Object:  StoredProcedure [General].[Sp_Commerces_Delete]    Script Date: 02/10/2018 10:40:05 a.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Commerces_Delete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Commerces_Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/06/2016
-- Description:	Delete the Commerces
-- Modify by:   Marjorie Garbanzo - 02/10/2018 - Add delete agenda
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Commerces_Delete]
(
	@pId INT = NULL,
	@pCustomerId INT,
	@pUserId INT
)
AS
BEGIN	
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT COUNT([Id]) 
				FROM [Management].[Agenda]
				WHERE [CommerceId] = @pId)
	BEGIN
		DELETE FROM [Management].[Agenda]
		WHERE [DateOfAgenda] >= GETDATE()
			AND [CommerceId] = @pId
	END

	UPDATE [General].[Commerces]
	SET [IsDeleted] = 1,
		[ModifyDate] = GETDATE(),
		[ModifyUserId] = @pUserId
	WHERE [Id] = @pId
		
	SET NOCOUNT OFF
END
