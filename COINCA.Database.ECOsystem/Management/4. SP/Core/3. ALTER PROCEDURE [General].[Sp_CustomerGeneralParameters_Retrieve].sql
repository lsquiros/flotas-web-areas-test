USE [Management]
GO
/****** Object:  StoredProcedure [General].[Sp_CustomerGeneralParameters_Retrieve]    Script Date: 24/09/2018 01:59:57 p.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerGeneralParameters_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerGeneralParameters_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================    
-- Author:  Henry Retana    
-- Create date: 18/04/2017    
-- Description: Retorna parametros para clientes    
-- Henry Retana - 17/08/2017    
-- Add allow process to the retrieve    
-- Modify By: Stefano Quirós - 17/10/2017 - Add to the retrieve the TypeOfDistribution parameter    
-- Modify By: Esteban Solís - 23/11/2017 - Add to the retrieve the AlarmTemperatureDelayMinutes parameter  
-- Modify By: Stefano Quirós - 19/12/2017 - Add the DefaultEmail to the parameters    
-- Modify By: Esteban Solís - 28/12/2017 - Add the MinimumStopTime to the parameters    
-- Modify By: Henry Retana - 30/04/2018 - Add the HasInternalServiceStation to the retrieve
-- Modify By: Sebastian Quesada - 10/07/2018 - Add the RadioDistance to the retrieve and active radio distance by commerce
-- Modify By: Henry Retana - 16/07/2018 - Add the [VehicleStopLapse] to the retrieve
-- Modify By: Marjorie Garbanzo - 24/09/2018 - Add the [TransmissionInterval] and [LocationInterval] to the retrieve
-- ================================================================================================    

CREATE PROCEDURE [General].[Sp_CustomerGeneralParameters_Retrieve] 
(
	@pCustomerId INT
)
AS
BEGIN
	SET NOCOUNT ON

	SELECT ISNULL((
				SELECT [Value]
				FROM [General].[ParametersByCustomer]
				WHERE [CustomerId] = @pCustomerId
					AND [Name] = 'StartTime'
					AND [ResuourceKey] = 'NORMALVEHICLESCHEDULE'
				), 0) [StartTime]
		,ISNULL((
				SELECT [Value]
				FROM [General].[ParametersByCustomer]
				WHERE [CustomerId] = @pCustomerId
					AND [Name] = 'EndTime'
					AND [ResuourceKey] = 'NORMALVEHICLESCHEDULE'
				), 0) [EndTime]
		,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'AllowProcess'
						AND [ResuourceKey] = 'ALLOWINPROCESSTRANSACTIONS'
					), 0) AS BIT) [AllowInProcessCard]
		,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'ActivateImport'
						AND [ResuourceKey] = 'ALLOWAPPROVALTRANSACTIONSIMPORT'
					), 0) AS BIT) [AllowApprovalTransactiosImport]
		,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'ActivateCostCenter'
						AND [ResuourceKey] = 'COSTCENTERBYDRIVER'
					), 0) AS BIT) [CostCenterByDriver]
		,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'TypeOdometer'
						AND [ResuourceKey] = 'TYPEODOMETERALARM'
					), 0) AS BIT) [TypeOdometer]
		,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'TypeOfDistribution'
						AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
					), 0) AS INT) [TypeBudgetDistribution]
		,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'AlarmTemperatureDelayMinutes'
						AND [ResuourceKey] = 'ALARMTEMPERATUREDELAY'
					), 0) AS INT) [AlarmTemperatureDelay]
		,ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'DefaultEmail'
						AND [ResuourceKey] = 'DEFAULTEMAIL'
					), '') [DefaultEmail]
		,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'MinimumStopTime'
						AND [ResuourceKey] = 'REC_PARAMETERS'
					), 0) AS INT) AS [MinimumStopTime]
		,[General].[Fn_HasInternalServiceStation_Retrieve] (@pCustomerId) [HasInternalServiceStation]
		,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'NotApprove'
						AND [ResuourceKey] = 'APPROVALTRANSACTIONS'
					), 0) AS BIT) AS [NotApproveTransactions]
		,CAST(ISNULL((
		               SELECT [VALUE]
					   FROM [General].[ParametersByCustomer]
					   WHERE [CustomerId] = @pCustomerId
					       AND [Name] = 'RadioDistanceByCommerce'  
                           AND [ResuourceKey] = 'RADIODISTANCE'
		            ),0) AS INT) AS [RadioDistance]
	    ,CAST(ISNULL((
		           SELECT [VALUE]
				   FROM [General].[ParametersByCustomer]
				   WHERE [CustomerId] = @pCustomerId
					       AND [Name] = 'Active_RadioDistanceByCustomer'  
                           AND [ResuourceKey] = 'ACTIVE_RADIODISTANCE'
		            ),0) AS BIT) AS [ActiveRadioDistance]
		,CAST(ISNULL((	
						SELECT [Value]
						FROM [General].[ParametersByCustomer]
						WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'VehicleStopLapse'
						AND [ResuourceKey] = 'VEHICLESTOPLAPSE')
				    ,0) AS INT) [VehicleStopLapse]
		,CAST(ISNULL((	
						SELECT [Value]
						FROM [General].[ParametersByCustomer]
						WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'TransmissionInterval'
						AND [ResuourceKey] = 'TRANSMISSIONINTERVAL')
				    ,0) AS INT) [TransmissionInterval]
		,CAST(ISNULL((	
						SELECT [Value]
						FROM [General].[ParametersByCustomer]
						WHERE [CustomerId] = @pCustomerId
						AND [Name] = 'LocationInterval'
						AND [ResuourceKey] = 'LOCATIONINTERVAL')
				    ,0) AS INT) [LocationInterval]
	SET NOCOUNT OFF
END
