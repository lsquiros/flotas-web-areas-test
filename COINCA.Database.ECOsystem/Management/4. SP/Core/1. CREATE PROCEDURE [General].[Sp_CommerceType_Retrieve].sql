USE [Management]
GO
/****** Object:  StoredProcedure [General].[Sp_CommerceType_Retrieve]    Script Date: 19/09/2018 10:02:18 a.m. ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CommerceType_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CommerceType_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 19/09/2018
-- Description:	Retrieve the CommercesTypes
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CommerceType_Retrieve]
AS
BEGIN	
	SET NOCOUNT ON;

	SELECT ct.[Id] 
		  ,ct.[Name] 
		  ,ct.[Description]
		  ,cd.[Value] [Icon]
		  ,ct.[RequiredData]
	FROM [General].[CommercesTypes] ct   
		 LEFT JOIN [MAnagement].[CatalogDetail] cd
			ON ct.[IconId] = cd.Id
		
	SET NOCOUNT OFF
END
