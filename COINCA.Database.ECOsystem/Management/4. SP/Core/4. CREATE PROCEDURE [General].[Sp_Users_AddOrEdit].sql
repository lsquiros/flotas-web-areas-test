USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Users_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Users_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/16/2014
-- Description:	Insert or Update User information - Update 11/11/2015 Henry Retana, update the [IsLockedOut] 
-- in the User table, so the user can be blocked from the maintenance. 
 -- Update 11/13/2015 Henry Retana, update the @pEmailConfimation 
-- in the AspNetUsers table, so the email can be confirmed. 
-- Update 01/04/2016 change in the save user and roleid, if going to use the roleId now on.
-- Update 16/09/2016 - Henry Retana - Delete the roles only for the profile
-- Update 29/09/2016 - Gerald Solano - Validamos que el RoleID si viene NULL que obtenga la info con el Role Name
-- Update 07/08/2017 - Marco Cabrera - Avoid the duplication of roles when exist severals rols with the same name
-- Update 11/08/2017 - Henry Retana - Changes the duplication of the roles 
-- Update 08/11/2017 - Gerald Solano - Se valida la variable @lAspNetUserId y se asegura que el valor se obtenga
-- Modify By: Albert Estrada - 17/01/2018 -- Add [pAgent] field 
-- Modify By: Henry Retana - 15/05/2018 
-- Validates if the user add customers and if this is a Support User
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_Users_AddOrEdit]
(
	 @pUserId INT = NULL	
	,@pName VARCHAR(250)
	,@pChangePassword BIT = NULL
	,@pPhoto VARCHAR(MAX) = NULL
	,@pIsActive BIT = 1
	,@pEmail VARCHAR(256)
	,@pPhoneNumber VARCHAR(256) = NULL
	,@pUserName VARCHAR(256)
	,@pIsLockedOut BIT = NULL
	,@pRoleId NVARCHAR(256) = NULL
	,@pRoleName NVARCHAR(256) = NULL
	,@pPasswordExpirationDate DATETIME = NULL
	,@pLoggedUserId INT
	,@pRowVersion TIMESTAMP
	,@pEmailConfimation BIT = NULL
	,@pIsAgent BIT = 1
	,@pAddCustomers BIT = 0
	,@pIsSupportUser BIT = 0
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lAspNetUserId NVARCHAR(128)
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
	
			--GSOLANO:VALIDAMOS QUE EL ROLE_ID NO VENGA NULL
			IF @pRoleId IS NULL
			BEGIN
				SELECT @pRoleId = [Id]
				FROM [dbo].[AspNetRoles]
				WHERE [Name] = @pRoleName
			END
			
			SELECT @lAspNetUserId = a.[Id]
			FROM [dbo].[AspNetUsers] a
			WHERE [UserName] = @pUserName
			
			IF @pUserId IS NULL
			BEGIN								
				INSERT INTO [General].[Users]
				(
					 [AspNetUserId]
					,[Name]
					,[ChangePassword]
					,[Photo]
					,[InsertDate]
					,[InsertUserId]
					,[AddCustomers]
					,[IsAgent]
				)
				VALUES	
				(
					 @lAspNetUserId
					,@pName
					,@pChangePassword
					,@pPhoto
					,GETUTCDATE()
					,@pLoggedUserId
					,@pAddCustomers
					,@pIsAgent
				)

				SET @pUserId = SCOPE_IDENTITY()

				IF @pEmailConfimation IS NOT NULL 
				BEGIN
					UPDATE [dbo].[AspNetUsers]
					SET [EmailConfirmed] = 1
					WHERE [Email] = @pEmail
				END

			END
			ELSE
			BEGIN
				DECLARE @lIsLockedOut BIT, 
						@lAccessFailedCount INT = NULL

				--GSOLANO: Se valida la variable @lAspNetUserId y se asegura que el valor se obtenga
				IF @lAspNetUserId IS NULL
				BEGIN 
					SELECT @lAspNetUserId = u.[AspNetUserId]
					FROM [General].[Users] u
					WHERE u.[UserId] = @pUserId
				END

				SELECT @lIsLockedOut = CASE WHEN a.[IsLockedOut] = 1 OR b.[LockoutEndDateUtc] IS NOT NULL 
											THEN 1 
											ELSE 0 
									   END
				FROM [General].[Users] a 
				INNER JOIN [dbo].[AspNetUsers] b 
					ON a.[AspNetUserId] = b.[Id]
				WHERE a.[UserId] = @pUserId
				
				IF @lIsLockedOut = 1 AND @pIsLockedOut = 0 SET @lAccessFailedCount = 0
				
				UPDATE [dbo].[AspNetUsers]
				SET  [Email] = @pEmail
					,[PhoneNumber] = @pPhoneNumber
					,[AccessFailedCount] = ISNULL(@lAccessFailedCount, [AccessFailedCount])
					,[LockoutEndDateUtc] = CASE WHEN @lAccessFailedCount = 0 
												THEN NULL ELSE [LockoutEndDateUtc] 
										   END
				WHERE [UserName] = @pUserName

				IF @pIsLockedOut = 1
				BEGIN
					UPDATE [dbo].[AspNetUsers]
					SET  [LockoutEndDateUtc] = GETUTCDATE()
					WHERE [UserName] = @pUserName
					AND LockoutEndDateUtc IS NULL
				END
				
				UPDATE [General].[Users]
				SET  [Name] = @pName
					,[ChangePassword] = ISNULL(@pChangePassword,[ChangePassword])
					,[Photo] = @pPhoto
					,[PasswordExpirationDate] = ISNULL(@pPasswordExpirationDate,[PasswordExpirationDate])
					,[IsActive] = @pIsActive
					,[IsLockedOut] = @pIsLockedOut
					,[IsAgent] = @pIsAgent
					,[ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
					,[AddCustomers] = @pAddCustomers
				WHERE [UserId] = @pUserId
			END
            
            SET @lRowCount = @@ROWCOUNT
            
			--UPDATE THE INFORMATION FOR THE ROLE WHEN THIS IS NOT A SUPPORT USER
			IF @pIsSupportUser = 0 
			BEGIN 
				EXEC [General].[Sp_RolesByUser_Update] @pRoleId, @lAspNetUserId
				
				IF @pAddCustomers = 1 EXEC [General].[Sp_AddAllCustomerToUser_Add] @pUserId, NULL
			END
            
            IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION
			
			IF @lRowCount = 0 	RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)

			SELECT @pUserId			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END



