USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Commerces_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Commerces_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana, Stefano Quirós
-- Create date: 09/06/2016
-- Description:	Add or edit Commerces
-- Modify by: Henry Retana - 22-02-2018
-- Change var TypeId to Type
-- Modify By: Sebastian Quesada - 19/07/2018 - AddorEdit column Geom type GEOGRAPHY Location by commerce for the customer
-- Modify by: Marjorie Garbanzo - 26-07-2018 - Change to associate the CustomerId
-- Modify by: Marjorie Garbanzo - 19-09-2018 - Change the @pAgentName for @pUserName
-- Modify by: Marjorie Garbanzo - 19-10-2018 - Add TempCommerceId and the insert in the TemporaryRelationshipCommerce table
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Commerces_AddOrEdit]
(
	@pId INT = NULL,
	@pTempCommerceId BIGINT = NULL, 
    @pName VARCHAR(500),
    @pDescription VARCHAR(MAX) = NULL,
    @pLatitude FLOAT = NULL, 
    @pLongitude FLOAT = NULL, 
    @pAddress VARCHAR(MAX) = NULL,
    @pCommerceType INT = NULL,
    @pCode VARCHAR(100) = NULL,
	@pCustomerId INT = NULL,
	@pUserId INT = NULL,
	@pUserName VARCHAR(500) = NULL
)
AS
BEGIN	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0

		 IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		

		DECLARE @lCount INT
			   ,@lGeofenceDistance INT
			   ,@lResultLat FLOAT
			   ,@lResultLong FLOAT
			   ,@lFinalLat FLOAT
			   ,@lFinalLong FLOAT
			   ,@lPolygon VARCHAR(500) = 'POLYGON (('
			   ,@lGeofenceId INT
			   ,@lDriverId INT
			   ,@lGeom GEOGRAPHY = GEOGRAPHY::STGeomFromText('POINT(' + CAST(@pLongitude AS VARCHAR(MAX)) + ' ' + CAST(@pLatitude AS VARCHAR(MAX)) + ')', 4326)
		   
		-- Como parámetro recibimos el DriverUserId, necesitamos el UserId
		SELECT @lDriverId = @pUserId

		IF @pCustomerId IS NULL
		BEGIN
			SELECT @pCustomerId = [CustomerId]
			FROM [General].[DriversUsers] 
			WHERE [UserId] = @pUserId
		END 

		SELECT @lCount = COUNT(1) 
		FROM [General].[Commerces] 
		WHERE [Code] = @pCode 
		OR ([InsertUserId] = @pUserId AND @pCommerceType <> 1 AND [TypeId] = @pCommerceType)

		SELECT @lGeofenceDistance = [NumericValue] 
		FROM [GeneralParameters] 
		WHERE [ParameterId] = 'DISTANCE_DEFAULT_GEOFENCE'	

		IF @pCommerceType = 0
		BEGIN
			SELECT @pCommerceType = [Id] FROM [General].[CommercesTypes] WHERE [Name] = 'Comercio'
		END

		IF @pId IS NULL AND @lCount = 0
		BEGIN
			IF @pAddress IS NULL SET @pAddress = [Efficiency].[Fn_GetAddressByLatLon_Retrieve](@pLongitude, @pLatitude,null)

			IF @pTempCommerceId IS NULL OR @pTempCommerceId = 0
			BEGIN
				INSERT INTO [General].[Commerces]
					(
						[Name],
						[Description],
						[Latitude],
						[Longitude],
						[Address],
						[TypeId],
						[Code],
						[InsertDate],
						[InsertUserId],
						[CustomerId],
						[Geom]
					)
					VALUES 
					(
						@pName
					   ,@pDescription
					   ,@pLatitude 
					   ,@pLongitude 
					   ,@pAddress
					   ,@pCommerceType
					   ,@pCode
					   ,GETDATE()
					   ,@pUserId
					   ,@pCustomerId 		
					   ,@lGeom
					)
				
				SELECT @pId = SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT * FROM [General].[Commerces]
								WHERE [Code] = @pCode
									OR ([Latitude] = @pLatitude
										AND [Longitude] = @pLongitude ))
				BEGIN
					SELECT @pId= Id 
					FROM [General].[Commerces]
					WHERE [Code] = @pCode
						OR ([Latitude] = @pLatitude
							AND [Longitude] = @pLongitude)
				END
				ELSE
				BEGIN
					INSERT INTO [General].[Commerces]
					(
						[Name],
						[Description],
						[Latitude],
						[Longitude],
						[Address],
						[TypeId],
						[Code],
						[InsertDate],
						[InsertUserId],
						[CustomerId],
						[Geom]
					)
					VALUES 
					(
						@pName
					   ,@pDescription
					   ,@pLatitude 
					   ,@pLongitude 
					   ,@pAddress
					   ,@pCommerceType
					   ,@pCode
					   ,GETDATE()
					   ,@pUserId
					   ,@pCustomerId 		
					   ,@lGeom
					)
				
					SELECT @pId = SCOPE_IDENTITY()
				END

				INSERT INTO [Management].[TemporaryRelationshipCommerce]
				(
					[TempCommerceId],
					[CommerceId]
				)
				VALUES 
				(
					@pTempCommerceId,
					@pId
				)
			END
		END 
		ELSE 
		BEGIN
			UPDATE [General].[Commerces]
			SET [Name] = @pName,
				[Description] = @pDescription,
				[Latitude] = @pLatitude,
				[Longitude] = @pLongitude,
				[Address] = @pAddress,
				[TypeId] = @pCommerceType,
				[Code] = @pCode,
				[ModifyDate] = GETDATE(),
				[ModifyUserId] = @pUserId,
				[CustomerId] = 	@pCustomerId,
				[Geom] = @lGeom
			WHERE [Id] = @pId 
			OR [Code] = @pCode 
			OR ([InsertUserId] = @pUserId AND [TypeId] = @pCommerceType)

			SELECT @pId = [Id]
			FROM [General].[Commerces]
			WHERE [Id] = @pId 
			OR [Code] = @pCode 
			OR ([InsertUserId] = @pUserId AND [TypeId] = @pCommerceType)
						
			IF @pTempCommerceId IS NOT NULL  OR @pTempCommerceId <> 0
			BEGIN
				INSERT INTO [Management].[TemporaryRelationshipCommerce]
				(
					[TempCommerceId],
					[CommerceId]
				)
				VALUES 
				(
					@pTempCommerceId,
					@pId
				)
			END
		END

		--Inicia Proceso de Creacion Geocerca para casa del Agente
		--IF @pCommerceType = 2
		--BEGIN	
		--	SELECT @lResultLat = [ResultLat], @lResultLong = [ResultLong] 
		--	FROM [Management].[Fn_GetPointsFromDistance] (@pLongitude, @pLatitude, (@lGeofenceDistance * 0.10), 90)
	
		--	--SELECT @pLatitude, @pLongitude, @lGeofenceDistance, @lPolygon
	
		--	SET @lPolygon = @lPolygon + CAST(@lResultLat AS VARCHAR(100)) + ' ' + CAST(@lResultLong AS VARCHAR(100)) + ', '
		--	SET @lFinalLat = @lResultLat
		--	SET @lFinalLong = @lResultLong
	
		--	SELECT @lResultLat = [ResultLat], @lResultLong = [ResultLong]
		--	FROM [Management].[Fn_GetPointsFromDistance] (@pLongitude, @pLatitude, @lGeofenceDistance, 180)
	
		--	SET @lPolygon = @lPolygon + CAST(@lResultLat AS VARCHAR(100)) + ' ' + CAST(@lResultLong AS VARCHAR(100)) + ', '
	
		--	SELECT @lResultLat = [ResultLat], @lResultLong = [ResultLong] FROM
		--	[Management].[Fn_GetPointsFromDistance] (@pLongitude, @pLatitude, (@lGeofenceDistance * 0.10), 270)
	
		--	SET @lPolygon = @lPolygon + CAST(@lResultLat AS VARCHAR(100)) + ' ' + CAST(@lResultLong AS VARCHAR(100)) + ', '
	
		--	SELECT @lResultLat = [ResultLat], @lResultLong = [ResultLong] FROM
		--	[Management].[Fn_GetPointsFromDistance] (@pLongitude, @pLatitude, @lGeofenceDistance, 360)
	
		--	SET @lPolygon = @lPolygon + CAST(@lResultLat AS VARCHAR(100)) + ' ' + CAST(@lResultLong AS VARCHAR(100)) + ', ' + CAST(@lFinalLat AS VARCHAR(100)) + ' ' + CAST(@lFinalLong AS VARCHAR(100)) + '))'
		
		--	IF NOT EXISTS(SELECT * FROM [Efficiency].[GeoFenceByDriver]  gfd
		--				  INNER JOIN [Efficiency].[GeoFences] g
		--					ON gfd.[GeofenceId] = g.[GeofenceId]
		--				  WHERE gfd.[DriverId] = @lDriverId AND g.[GeofenceTypeId] = @pCommerceType)
		--	BEGIN
		--		INSERT INTO [Efficiency].[GeoFences]
		--		(
		--			 [Name]
		--			,[Active]
		--			,[CustomerId]
		--			,[Description]
		--			,[Polygon]
		--			,[In]
		--			,[Out]
		--			,[GeoFenceTypeId]
		--			,[InsertDate]
		--			,[InsertUserId]
		--		)
		--		VALUES	
		--		(
		--			'Geocerca Casa: Nombre del Agente: ' + @pUserName 
		--			,1
		--			,@pCustomerId
		--			,'Esta Geocerca fue hecha para el Usuario: ' + @pUserName
		--			,@lPolygon
		--			,1
		--			,1
		--			,@pCommerceType
		--			,GETUTCDATE()
		--			,@pUserId
		--		)	
	
		--		SELECT @lGeofenceId = SCOPE_IDENTITY()
	
	
		--		INSERT INTO [Efficiency].[GeoFenceByDriver]
		--		(
		--			[GeofenceId]
		--		   ,[DriverId]
		--		   ,[InsertUserId]
		--		   ,[InsertDate]
		--		)
		--		VALUES
		--		(
		--			@lGeofenceId
		--		   ,@lDriverId
		--		   ,@pUserId
		--		   ,GETDATE()
		--	   )

		--		SET @pId	= 0
		--	END	
		--	ELSE
		--	BEGIN
		--	--SELECT @pUserName
		--		UPDATE g				
		--		SET g.[Polygon] = @lPolygon
		--		   ,g.[ModifyDate] = GETDATE()
		--		   ,g.[ModifyUserId] = @pUserId
		--		   ,g.[Name] = 'Geocerca Casa: Nombre del Agente: ' + @pUserName
		--		   ,g.[Description] = 'Esta Geocerca fue hecha para el Usuario: ' + @pUserName
		--		FROM [Efficiency].[GeoFences] g
		--		INNER JOIN [Efficiency].[GeoFenceByDriver]  gfd
		--			ON gfd.[GeofenceId] = g.[GeofenceId]
		--		WHERE gfd.[DriverId] = @lDriverId AND g.[GeofenceTypeId] = @pCommerceType
				
		--		SET @pId	= 0
		--	END	
		--END	

		--RETURN IDENTITY
		SELECT @pId			

		IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION			
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF 
END
