USE [Management]
GO

-- ================================================================================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 18/09/2018
-- Description:	ADD IconId column to CommercesTypes table
-- ================================================================================================

ALTER TABLE [General].[CommercesTypes]
ADD [IconId] INT

GO

ALTER TABLE [General].[CommercesTypes] WITH CHECK ADD CONSTRAINT [FK_IconId_CatalogDetail] FOREIGN KEY([IconId])
REFERENCES [Management].[CatalogDetail] ([Id])

GO

ALTER TABLE [General].[CommercesTypes]
ADD [RequiredData] BIT

