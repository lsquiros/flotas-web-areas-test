USE [Management]
GO

IF OBJECT_ID (N'[Management].[UserAnswer]', N'U') IS NOT NULL DROP TABLE [Management].[UserAnswer]
GO
 
 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 14/09/2018
-- Description:	Create table UserAnswer
-- ================================================================================================

CREATE TABLE [Management].[UserAnswer]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[UserEventId] INT,
	[SurveyId] INT,
	[QuestionId] INT,
	[OptionId] INT,
	[Data] VARCHAR(MAX),
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME	
) 
GO

ALTER TABLE [Management].[UserAnswer] WITH CHECK ADD CONSTRAINT [FK_UserEventId_UserEvent] FOREIGN KEY([UserEventId])
REFERENCES [Management].[UserEvent] ([Id])

ALTER TABLE [Management].[UserAnswer] WITH CHECK ADD CONSTRAINT [FK_SurveyId_Survey] FOREIGN KEY([SurveyId])
REFERENCES [Management].[Survey] ([Id])

ALTER TABLE [Management].[UserAnswer] WITH CHECK ADD CONSTRAINT [FK_QuestionId_Question] FOREIGN KEY([QuestionId])
REFERENCES [Management].[Question] ([Id])

ALTER TABLE [Management].[UserAnswer] WITH CHECK ADD CONSTRAINT [FK_OptionId_Option] FOREIGN KEY([OptionId])
REFERENCES [Management].[Option] ([Id])

GO