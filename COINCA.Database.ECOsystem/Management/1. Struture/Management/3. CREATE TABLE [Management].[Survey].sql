USE [Management]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'[Management].[Survey]', N'U') IS NOT NULL DROP TABLE [Management].[Survey]
GO
 
 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 08/08/2018
-- Description:	Create table Survey
-- ================================================================================================

CREATE TABLE [Management].[Survey]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[CustomerId] INT,
	[Name] VARCHAR(200),
	[Description] VARCHAR(MAX),
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME	
) 
GO

ALTER TABLE [Management].[Survey] WITH CHECK ADD CONSTRAINT [FK_CustomerId_Customers] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])

GO