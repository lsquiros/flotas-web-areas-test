USE [Management]
GO

IF OBJECT_ID (N'[Management].[EventDetail]', N'U') IS NOT NULL DROP TABLE [Management].[EventDetail]
GO
 
 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 14/09/2018
-- Description:	Create table EventDetail
-- ================================================================================================

CREATE TABLE [Management].[EventDetail]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[SurveyId] INT,
	[EventTypeId] INT,
	[IconId] INT,
	[Name] VARCHAR(200),
	[Color] VARCHAR(20),
	[Active] BIT,
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME	
) 
GO

ALTER TABLE [Management].[EventDetail] WITH CHECK ADD CONSTRAINT [FK_SurveyId_Survey] FOREIGN KEY([SurveyId])
REFERENCES [Management].[Survey] ([Id])

ALTER TABLE [Management].[EventDetail] WITH CHECK ADD CONSTRAINT [FK_EventTypeId_CatalogDetail] FOREIGN KEY([EventTypeId])
REFERENCES [Management].[CatalogDetail] ([Id])

ALTER TABLE [Management].[EventDetail] WITH CHECK ADD CONSTRAINT [FK_IconID_CatalogDetail] FOREIGN KEY([IconId])
REFERENCES [Management].[CatalogDetail] ([Id])

GO