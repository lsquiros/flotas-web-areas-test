USE [Management]
GO

IF OBJECT_ID (N'[Management].[Question]', N'U') IS NOT NULL DROP TABLE [Management].[Question]
GO
 
 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 08/08/2018
-- Description:	Create table Question
-- ================================================================================================

CREATE TABLE [Management].[Question]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[SurveyId] INT,
	[QuestionTypeId] INT,
	[Text] VARCHAR(MAX),
	[Description] VARCHAR(MAX),
	[Order] INT,
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME	
) 
GO

ALTER TABLE [Management].[Question] WITH CHECK ADD CONSTRAINT [FK_SurveyId_Survey] FOREIGN KEY([SurveyId])
REFERENCES [Management].[Survey] ([Id])

GO

ALTER TABLE [Management].[Question] WITH CHECK ADD CONSTRAINT [FK_QuestionTypeId_CatalogDetail] FOREIGN KEY([QuestionTypeId])
REFERENCES [Management].[CatalogDetail] ([Id])

GO