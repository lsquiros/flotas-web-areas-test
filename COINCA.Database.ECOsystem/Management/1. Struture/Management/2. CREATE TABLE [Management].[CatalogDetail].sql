USE [Management]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'[Management].[CatalogDetail]', N'U') IS NOT NULL DROP TABLE [Management].[CatalogDetail]
GO

 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 14/09/2018
-- Description:	Create table CatalogDetail
-- ================================================================================================

CREATE TABLE [Management].[CatalogDetail]
(
    [Id] INT IDENTITY PRIMARY KEY,
    [CatalogId] INT,
    [Name] VARCHAR(100),
    [Value] VARCHAR(MAX),
    [Description] VARCHAR(500),
	[Parent] INT,
    [Active] BIT DEFAULT(1),
    [InsertDate] DATETIME,
    [InsertUserId] INT, 
    [ModifyDate] DATETIME,
    [ModifyUserId] INT
)
GO

ALTER TABLE [Management].[CatalogDetail]  WITH CHECK ADD  CONSTRAINT [FK_CatalogId_Catalog] FOREIGN KEY([CatalogId])
REFERENCES [Management].[Catalog] ([Id])
GO

