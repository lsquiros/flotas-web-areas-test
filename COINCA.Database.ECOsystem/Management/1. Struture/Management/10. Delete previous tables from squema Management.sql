USE [ECOsystemDev]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 17/08/2018
-- Description:	Delete previous tables from squema Management
-- ================================================================================================

--Delete constraint
ALTER TABLE [Management].[AgentAnswers]
DROP CONSTRAINT FK_AgentAnswers_ProgramProcotol 
				
ALTER TABLE [Management].[AgentAnswers]
DROP CONSTRAINT FK_AgentAnswers_ProcotolAnswer

ALTER TABLE [Management].[AnswersByProtocol]
DROP CONSTRAINT FK_AnswersByProtocol_Customer

ALTER TABLE [Management].[CustomersByManagementTypes]
DROP CONSTRAINT FK_CustomersByManagementTypes_Customers
				
ALTER TABLE [Management].[CustomersByManagementTypes]
DROP CONSTRAINT FK_CustomersByManagementTypes_ManagementTypes

ALTER TABLE [Management].[ManagementEvents]
DROP CONSTRAINT FK_ManagementEvents_ManagementTypes
				
ALTER TABLE [Management].[ManagementEvents]
DROP CONSTRAINT FK_ManagementEvents_DriverUsers

ALTER TABLE [Management].[ProgramProtocols]
DROP CONSTRAINT FK_QuestionsByProtocol_Type
				
ALTER TABLE [Management].[ProgramProtocols]
DROP CONSTRAINT FK_ProgramProtocols_Protocols
				
ALTER TABLE [Management].[ProgramProtocols]
DROP CONSTRAINT FK_ProgramProtocols_Answers
				
ALTER TABLE [Management].[ProgramProtocols]
DROP CONSTRAINT FK_ProgramProtocols_Question

ALTER TABLE [Management].[ProtocolAnswers]
DROP CONSTRAINT FK_ProtocolAnswers_Events
				
ALTER TABLE [Management].[ProtocolAnswers]
DROP CONSTRAINT FK_ProtocolAnswers_Drivers

ALTER TABLE [Management].[Protocols]
DROP CONSTRAINT FK_Protocols_Customer

ALTER TABLE [Management].[ProtocolsByManagementTypes]
DROP CONSTRAINT FK_ProtocolsByManagementTypes_Protocols
				
ALTER TABLE [Management].[ProtocolsByManagementTypes]
DROP CONSTRAINT FK_ProtocolsByManagementTypes_ManagementTypes

ALTER TABLE [Management].[QuestionsByProtocol]
DROP CONSTRAINT FK_QuestionsByProtocol_Protocols
				
ALTER TABLE [Management].[QuestionsByProtocol]
DROP CONSTRAINT FK_QuestionsByProtocol_Customer

--Delete tables 
DROP TABLE [Management].[Protocols]
DROP TABLE [Management].[ManagementTypes]
DROP TABLE [Management].[AnswersByProtocol]
DROP TABLE [Management].[QuestionsByProtocol]
DROP TABLE [Management].[ProgramProtocols]
DROP TABLE [Management].[ManagementEvents]
DROP TABLE [Management].[ProtocolAnswers]
DROP TABLE [Management].[AgentAnswers]
DROP TABLE [Management].[CustomersByManagementTypes]
DROP TABLE [Management].[ProtocolsByManagementTypes]