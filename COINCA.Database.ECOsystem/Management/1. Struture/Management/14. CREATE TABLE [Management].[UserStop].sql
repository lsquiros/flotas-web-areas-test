USE [Management]
GO

IF OBJECT_ID (N'[Management].[UserStop]', N'U') IS NOT NULL DROP TABLE [Management].[UserStop]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 17/10/2018
-- Description:	Create table UserStop
-- ================================================================================================

CREATE TABLE [Management].[UserStop]
(
	[UserId] INT,
	[CustomerId] INT,
	[Latitude] FLOAT,
	[Longitude] FLOAT,
	[BatteryLevel] INT,
	[Heading] VARCHAR(20),
	[Speed] INT,
	[Altitude] INT,
	[EventDate] DATETIME,
	[InitStopDate] DATETIME	 
) 
GO

ALTER TABLE [Management].[UserStop] WITH CHECK ADD CONSTRAINT [FK_UserId_UserStops] FOREIGN KEY([UserId])
REFERENCES [General].[Users] ([UserId])

ALTER TABLE [Management].[UserStop] WITH CHECK ADD CONSTRAINT [FK_CustomerId_UserStops] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])
GO