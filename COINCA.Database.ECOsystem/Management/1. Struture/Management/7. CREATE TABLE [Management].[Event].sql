USE [Management]
GO

IF OBJECT_ID (N'[Management].[Event]', N'U') IS NOT NULL DROP TABLE [Management].[Event]
GO
 
 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 14/09/2018
-- Description:	Create table Event
-- ================================================================================================

CREATE TABLE [Management].[Event]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[EventDetailInitId] INT,
	[EventDetailFinishId] INT,
	[CustomerId] INT,
	[Tracking] BIT,
	[Lapse] INT,
	[CommerceRequired] BIT,
	[Parent] INT,
	[Active] BIT,
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME	
) 
GO

ALTER TABLE [Management].[Event] WITH CHECK ADD CONSTRAINT [FK_EventDetailInitId_EventDetail] FOREIGN KEY([EventDetailInitId])
REFERENCES [Management].[EventDetail] ([Id])

ALTER TABLE [Management].[Event] WITH CHECK ADD CONSTRAINT [FK_EventDetailFinishId_EventDetail] FOREIGN KEY([EventDetailFinishId])
REFERENCES [Management].[EventDetail] ([Id])

ALTER TABLE [Management].[Event] WITH CHECK ADD CONSTRAINT [FK_CustomerId_Customers] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])
GO