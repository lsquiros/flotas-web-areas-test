USE [Management]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'[Management].[Catalog]', N'U') IS NOT NULL DROP TABLE [Management].[Catalog]
GO

 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 14/09/2018
-- Description:	Create table Catalog
-- ================================================================================================

CREATE TABLE [Management].[Catalog]
(
    [Id] INT IDENTITY PRIMARY KEY,
    [Name] VARCHAR(100),
    [Description] VARCHAR(500),
    [Active] BIT DEFAULT(1),
    [InsertDate] DATETIME,
    [InsertUserId] INT, 
    [ModifyDate] DATETIME,
    [ModifyUserId] INT
)
GO