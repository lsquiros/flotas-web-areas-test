USE [EcoSystem]
GO

-- ================================================================================================
-- Author:		Stefano Quiros Ruiz
-- Create date: 04/10/2018
-- Description:	Add parameters to management parameters table
-- ================================================================================================

--Delete existing Columns
IF EXISTS (SELECT  *
           FROM   syscolumns
           WHERE  id = OBJECT_ID('Management.Parameters')
           AND name = 'TimeBegJourney' ) 
	ALTER TABLE [Management].[Parameters]
	DROP COLUMN [TimeBegJourney]

IF EXISTS (SELECT  *
           FROM   syscolumns
           WHERE  id = OBJECT_ID('Management.Parameters')
           AND name = 'TimeFinJourney' ) 
	ALTER TABLE [Management].[Parameters]
	DROP COLUMN [TimeFinJourney]

IF EXISTS (SELECT  *
           FROM   syscolumns
           WHERE  id = OBJECT_ID('Management.Parameters')
           AND name = 'DataSync' ) 
	ALTER TABLE [Management].[Parameters]
	DROP COLUMN [DataSync]

IF EXISTS (SELECT  *
           FROM   syscolumns
           WHERE  id = OBJECT_ID('Management.Parameters')
           AND name = 'MovemName' ) 
	ALTER TABLE [Management].[Parameters]
	DROP COLUMN [MovemName]

IF EXISTS (SELECT  *
           FROM   syscolumns
           WHERE  id = OBJECT_ID('Management.Parameters')
           AND name = 'ReconsType' ) 
	ALTER TABLE [Management].[Parameters]
	DROP COLUMN [ReconsType]

ALTER TABLE [Management].[Parameters]
ADD [TimeBeginJourney] INT

ALTER TABLE [Management].[Parameters]
ADD [TimeFinishJourney] INT

ALTER TABLE [Management].[Parameters]
ADD [DataSync] INT

ALTER TABLE [Management].[Parameters]
ADD [MovementName] VARCHAR(500)

ALTER TABLE [Management].[Parameters]
ADD [ReconstructionType] INT

--Stop Parameters 
ALTER TABLE [Management].[Parameters]
ADD [StopMinimumTime] INT

ALTER TABLE [Management].[Parameters]
ADD [StopMinimumDistance] INT

ALTER TABLE [Management].[Parameters]
ADD [StopMinimumSpeed] INT

