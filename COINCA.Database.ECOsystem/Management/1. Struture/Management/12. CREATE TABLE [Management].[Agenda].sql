USE [Management]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'[Management].[Agenda]', N'U') IS NOT NULL DROP TABLE [Management].[Agenda]
GO

 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 19/09/2018
-- Description:	Create table CatalogDetail
-- ================================================================================================

CREATE TABLE [Management].[Agenda]
(
    [Id] INT IDENTITY PRIMARY KEY,
    [CommerceId] INT,
    [UserId] INT,
    [LoggedUserId] INT,
    [DateOfAgenda] DATETIME,
    [AdditionalColumns] VARCHAR(MAX),
	[Status] INT,
	[Delete] BIT DEFAULT(0),
    [InsertDate] DATETIME,
    [InsertUserId] INT, 
    [ModifyDate] DATETIME,
    [ModifyUserId] INT
)
GO
