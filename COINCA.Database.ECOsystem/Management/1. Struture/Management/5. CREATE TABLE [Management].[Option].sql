USE [Management]
GO

IF OBJECT_ID (N'[Management].[Option]', N'U') IS NOT NULL DROP TABLE [Management].[Option]
GO
 
 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 08/08/2018
-- Description:	Create table Option
-- ================================================================================================

CREATE TABLE [Management].[Option]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[QuestionId] INT,
	[Text] VARCHAR(MAX),
	[Order] INT,
	[NextQuestionId] INT,
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME	
) 
GO

ALTER TABLE [Management].[Option] WITH CHECK ADD CONSTRAINT [FK_QuestionId_Question] FOREIGN KEY([QuestionId])
REFERENCES [Management].[Question] ([Id])

GO

/*ALTER TABLE [Management].[Option] WITH CHECK ADD CONSTRAINT [FK_NextQuestionId_Option] FOREIGN KEY([NextQuestionId])
REFERENCES [Management].[Question] ([Id])

GO*/