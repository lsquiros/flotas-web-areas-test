USE [ECOsystem]
GO

IF OBJECT_ID (N'[Management].[TemporaryRelationshipCommerce]', N'U') IS NOT NULL DROP TABLE [Management].[TemporaryRelationshipCommerce]
GO
 
 -- ================================================================================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 19/10/2018
-- Description:	Create table TemporaryRelationshipCommerce
-- ================================================================================================

CREATE TABLE [Management].[TemporaryRelationshipCommerce]
(
	[TempCommerceId] BIGINT,
	[CommerceId] INT
) 
GO

ALTER TABLE [Management].[TemporaryRelationshipCommerce] WITH CHECK ADD CONSTRAINT [FK_Commerce_TemporaryRelationshipCommerce] FOREIGN KEY([CommerceId])
REFERENCES [General].[Commerces] ([Id])
GO