USE [Management]
GO

IF OBJECT_ID (N'[Management].[UserEvent]', N'U') IS NOT NULL DROP TABLE [Management].[UserEvent]
GO
 
 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 14/09/2018
-- Description:	Create table UserEvent
-- ================================================================================================

CREATE TABLE [Management].[UserEvent]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[EventDetailId] INT,
	[EventTypeId] INT,
	[UserId] INT,
	[CommerceId] INT,
	[CustomerId] INT,
	[Latitude] FLOAT,
	[Longitude] FLOAT,
	[BatteryLevel] INT,
	[Heading] VARCHAR(20),
	[Speed] INT,
	[Altitude] INT,
	[Lapse] INT,
	[EventDate] DATETIME,
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME	
) 
GO

ALTER TABLE [Management].[UserEvent] WITH CHECK ADD CONSTRAINT [FK_EventDetailId_EventDetail] FOREIGN KEY([EventDetailId])
REFERENCES [Management].[EventDetail] ([Id])

ALTER TABLE [Management].[UserEvent] WITH CHECK ADD CONSTRAINT [FK_EventTypeId_CatalogDetail] FOREIGN KEY([EventTypeId])
REFERENCES [Management].[CatalogDetail] ([Id])

ALTER TABLE [Management].[UserEvent] WITH CHECK ADD CONSTRAINT [FK_UserId_Users] FOREIGN KEY([UserId])
REFERENCES [General].[Users] ([UserId])

ALTER TABLE [Management].[UserEvent] WITH CHECK ADD CONSTRAINT [FK_CommerceId_Commerces] FOREIGN KEY([CommerceId])
REFERENCES [General].[Commerces] ([Id])

ALTER TABLE [Management].[UserEvent] WITH CHECK ADD CONSTRAINT [FK_CustomerId_Customer] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])
GO