USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 31/08/2017
-- Description:	Add table Blocked [General].[ReportIdsToExport]
-- ================================================================================================

IF OBJECT_ID (N'[dbo].[ReportIdsToExportLog]', N'U') IS NOT NULL DROP TABLE [dbo].[ReportIdsToExportLog]

CREATE TABLE [dbo].[ReportIdsToExportLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Report] [int] NULL,
	[IsDeleted] [int] DEFAULT(0),
	[InsertDate] [datetime] NULL
) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IX_Reports] ON [dbo].[ReportIdsToExportLog]
(
	[Report] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

