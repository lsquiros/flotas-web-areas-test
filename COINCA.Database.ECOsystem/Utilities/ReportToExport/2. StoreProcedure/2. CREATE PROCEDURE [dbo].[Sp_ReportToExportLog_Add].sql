USE [ECO_HIS];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 06/10/2017
-- Description:	Report to Export Log Add
-- ================================================================================================
CREATE PROCEDURE [dbo].[Sp_ReportToExportLog_Add]
(
	@pXml VARCHAR(MAX) = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT	
		DECLARE @lxmlData XML = CONVERT(XML, @pXml)	
		
		CREATE TABLE #Reports
		(
			[Report] BIGINT,
			[Device] INT NOT NULL,
			[GPSDateTime] DATETIME NOT NULL,
			[RTCDateTime] DATETIME NOT NULL,
			[RepDateTime] DATETIME NOT NULL,
			[SvrDateTime] DATETIME NOT NULL,
			[Longitude] FLOAT NOT NULL,
			[Latitude] FLOAT NOT NULL,
			[Heading] INT NOT NULL,
			[ReportID] INT NOT NULL,
			[Odometer] FLOAT NOT NULL,
			[HDOP] FLOAT NOT NULL,
			[InputStatus] INT NOT NULL,
			[OutputStatus] INT NOT NULL,
			[VSSSpeed] FLOAT NOT NULL,
			[AnalogInput] FLOAT NOT NULL,
			[DriverID] VARCHAR(16) NULL,
			[Temperature1] FLOAT NULL,
			[Temperature2] FLOAT NULL,
			[MCC] INT NULL,
			[MNC] INT NULL,
			[LAC] INT NULL,
			[CellID] INT NULL,
			[RXLevel] INT NULL,
			[GSMQuality] INT NULL,
			[GSMStatus] INT NULL,
			[Satellites] INT NULL,
			[RealTime] BIT NULL,
			[MainVolt] FLOAT NULL,
			[BatVolt] FLOAT NULL
		 )

		INSERT INTO #Reports
		SELECT m.c.value('Report[1]', 'BIGINT'),				
			   m.c.value('Device[1]', 'INT'),
			   m.c.value('GPSDateTime[1]', 'DATETIME'),
			   m.c.value('RTCDateTime[1]', 'DATETIME'),
			   m.c.value('RepDateTime[1]', 'DATETIME'),
			   m.c.value('SvrDateTime[1]', 'DATETIME'),
			   m.c.value('Longitude[1]', 'FLOAT'),
			   m.c.value('Latitude[1]', 'FLOAT'),
			   m.c.value('Heading[1]', 'INT'),
			   m.c.value('ReportID[1]', 'INT'),
			   m.c.value('Odometer[1]', 'FLOAT'),
			   m.c.value('HDOP[1]', 'FLOAT'),
			   m.c.value('InputStatus[1]', 'INT'),
			   m.c.value('OutputStatus[1]', 'INT'),
			   m.c.value('VSSSpeed[1]', 'FLOAT'),
			   m.c.value('AnalogInput[1]', 'FLOAT'),
			   m.c.value('DriverID[1]', 'VARCHAR(16)'),
			   m.c.value('Temperature1[1]', 'FLOAT'),			
			   m.c.value('Temperature2[1]', 'FLOAT'),
			   m.c.value('MCC[1]', 'INT'),
			   m.c.value('MNC[1]', 'INT'),			
			   m.c.value('LAC[1]', 'INT'),
			   m.c.value('CellID[1]', 'INT'),
			   m.c.value('RXLevel[1]', 'INT'),
			   m.c.value('GSMQuality[1]', 'INT'),
			   m.c.value('GSMStatus[1]', 'INT'),
			   m.c.value('Satellites[1]', 'INT'),
			   m.c.value('RealTime[1]', 'BIT'),
			   m.c.value('MainVolt[1]', 'FLOAT'),
			   m.c.value('BatVolt[1]', 'FLOAT')
		FROM @lxmlData.nodes('//ReportToExport') AS m(c)		
		
		SET IDENTITY_INSERT [dbo].[Reports] ON
		
		INSERT INTO [dbo].[Reports]		
		(
			[Report],
			[Device],
			[GPSDateTime],
			[RTCDateTime],
			[RepDateTime],
			[SvrDateTime],
			[Longitude],
			[Latitude],
			[Heading],
			[ReportID],
			[Odometer],
			[HDOP],
			[InputStatus],
			[OutputStatus],
			[VSSSpeed],
			[AnalogInput],
			[DriverID],
			[Temperature1],
			[Temperature2],
			[MCC],
			[MNC],
			[LAC],
			[CellID],
			[RXLevel],
			[GSMQuality],
			[GSMStatus],
			[Satellites],
			[RealTime],
			[MainVolt],
			[BatVolt]
		)
		SELECT *
		FROM #Reports		
		
		SET IDENTITY_INSERT [dbo].[Reports] OFF
		
		DROP TABLE #Reports
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO