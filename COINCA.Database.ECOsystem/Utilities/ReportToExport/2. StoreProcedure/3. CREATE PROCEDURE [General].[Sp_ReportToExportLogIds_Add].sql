USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ReportToExportLogIds_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ReportToExportLogIds_Add] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 06/10/2017
-- Description:	Report to Export Log Ids Add
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ReportToExportLogIds_Add]
(
	@pXml VARCHAR(MAX) = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT	
		DECLARE @lxmlData XML = CONVERT(XML, @pXml)	
		DECLARE @ReportIds TABLE ([Report] INT)

		INSERT INTO	@ReportIds
		SELECT	m.c.query('.').value('int[1]', 'INT')
		FROM	@lxmlData.nodes('//ArrayOfInt/int') AS m(c)
		
		INSERT INTO [dbo].[ReportIdsToExportLog]
		(
			[Report],	
			[InsertDate]
		)
		SELECT [Report],			   
			   GETDATE()
		FROM @ReportIds		
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

