USE [ECOSystem]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ReportsToDelete]') AND type in (N'P', N'PC'))	
     DROP PROCEDURE [General].[Sp_ReportsToDelete]
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 19/10/2017
-- Description:	Reports to Delete 
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ReportsToDelete]
AS
BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT	
		
		DECLARE @Report BIGINT = 0
		DECLARE @MinReport BIGINT = 0
		DECLARE @MaxReport BIGINT = 0
		
		SELECT @Report = MIN(Report) FROM [dbo].[ReportIdsToExportLog] WHERE IsDeleted = 0
		SET @MinReport = @Report
		
		--Disable CONSTRAINT temporarily for Reports
		IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_Reports_TextMessage_Reports_22022017]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reports_TextMessage]'))
		BEGIN
			ALTER TABLE [dbo].[Reports_TextMessage] NOCHECK CONSTRAINT [FK_Reports_TextMessage_Reports_22022017]	
		END

		IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_Reports_RPM_Reports_22022017]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reports_RPM]'))
		BEGIN
			ALTER TABLE [dbo].[Reports_RPM] NOCHECK CONSTRAINT [FK_Reports_RPM_Reports_22022017]
		END

		IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_Reports_OBD_Reports_22022017]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reports_OBD]'))
		BEGIN
			ALTER TABLE [dbo].[Reports_OBD] NOCHECK CONSTRAINT [FK_Reports_OBD_Reports_22022017]
		END

		IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_Reports_FuelLevel_Reports_22022017]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reports_FuelLevel]'))
		BEGIN
			ALTER TABLE [dbo].[Reports_FuelLevel] NOCHECK CONSTRAINT [FK_Reports_FuelLevel_Reports_22022017]
		END
		
		--Delete Reports
		WHILE(@Report IS NOT NULL)
		BEGIN
			
			/*DELETE FROM [dbo].[Reports_TextMessage]
			WHERE [Report] = @Report 
			
			DELETE FROM [dbo].[Reports_OBD]
			WHERE [Report] = @Report 
			
			DELETE FROM [dbo].[Reports_RPM]
			WHERE [Report] = @Report*/

			DELETE FROM [dbo].[Reports] 
			WHERE [Report] = @Report
			
			SET @MaxReport = @Report
			
			--Next ID
			SELECT @Report = MIN(Report) FROM [dbo].[ReportIdsToExportLog]
			WHERE Report > @Report AND IsDeleted = 0
		END
		
		--Enable CONSTRAINT for Reports
		IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_Reports_TextMessage_Reports_22022017]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reports_TextMessage]'))
		BEGIN
			ALTER TABLE [dbo].[Reports_TextMessage] CHECK CONSTRAINT [FK_Reports_TextMessage_Reports_22022017]
		END

		IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_Reports_RPM_Reports_22022017]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reports_RPM]'))
		BEGIN
			ALTER TABLE [dbo].[Reports_RPM] CHECK CONSTRAINT [FK_Reports_RPM_Reports_22022017]
		END

		IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_Reports_OBD_Reports_22022017]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reports_OBD]'))
		BEGIN
			ALTER TABLE [dbo].[Reports_OBD] CHECK CONSTRAINT [FK_Reports_OBD_Reports_22022017]
		END

		IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_Reports_FuelLevel_Reports_22022017]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reports_FuelLevel]'))
		BEGIN
			ALTER TABLE [dbo].[Reports_FuelLevel] CHECK CONSTRAINT [FK_Reports_FuelLevel_Reports_22022017]
		END
		
		--Update reports deleted
		UPDATE [dbo].[ReportIdsToExportLog]
		SET IsDeleted = 1
		WHERE Report BETWEEN @MinReport AND @MaxReport
		
		SELECT 1
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO