USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_ReportToExport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_ReportToExport_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 06/10/2017
-- Description:	Retrieve Report Data to Export
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_ReportToExport_Retrieve]
(
	 @pLapse INT = NULL,
	 @pMinLapse INT = NULL,	--Indicates the amount of months the job has allowed to import
	 @pTopNumber INT = NULL
)
AS
BEGIN	
	SET NOCOUNT ON

	DECLARE @lReportId INT,
			@lDate DATETIME,
			@lLastReportId INT

	IF NOT EXISTS (SELECT *
				   FROM [dbo].[ReportIdsToExportLog] WHERE IsDeleted = 0)
	BEGIN
		SELECT @lReportId = MIN([Report])
		FROM [dbo].[Reports]

		SELECT @lDate = [SvrDateTime]
		FROM [dbo].[Reports]
		WHERE [Report] = @lReportId

		IF DATEADD(HOUR, @pLapse, @lDate) <= DATEADD(MONTH, -@pMinLapse, GETDATE())
		BEGIN
			IF @pTopNumber IS NULL
			BEGIN 
				SELECT TOP 1 @lLastReportId = [Report]
				FROM [dbo].[Reports]
				WHERE [SvrDateTime] <= DATEADD(HOUR, @pLapse, @lDate)
				ORDER BY [SvrDateTime] DESC

				SELECT *
				FROM [dbo].[Reports]
				WHERE [Report] BETWEEN @lReportId AND @lLastReportId
			END
			ELSE 
			BEGIN
				SELECT TOP (@pTopNumber) *
				FROM [dbo].[Reports]
				WHERE [Report] >= @lReportId
				ORDER BY [Report] ASC
			END
		END
	END

    SET NOCOUNT OFF
END