USE [ECOsystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriverCodesInTransaction_Edit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriverCodesInTransaction_Edit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 04/10/2017
-- Description:	Edit Driver Codes for the Transactions
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_DriverCodesInTransaction_Edit]
(
	@pTransactionId INT,
	@pDriverCode VARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON	

	UPDATE [Control].[Transactions]
	SET [DriverCode] = @pDriverCode
	WHERE [TransactionId] = @pTransactionId

	SET NOCOUNT OFF
END

