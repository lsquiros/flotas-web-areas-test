SELECT name, modify_date
FROM sys.objects
WHERE type = 'P'
AND modify_date > '2015-06-30'
order by modify_date desc