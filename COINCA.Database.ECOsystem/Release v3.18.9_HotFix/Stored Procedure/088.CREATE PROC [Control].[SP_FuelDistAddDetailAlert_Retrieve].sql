
IF EXISTS (SELECT * FROM SYS.OBJECTS WHERE NAME = 'SP_FuelDistAddDetailAlert_Retrieve')
	DROP PROC [Control].[SP_FuelDistAddDetailAlert_Retrieve]
GO

-- ==========================================================================================
-- Author:		Juan Carlos Santamaria
-- Create date: 21-07-20200
-- Description:	Get aditional data with alert
-- ==========================================================================================
CREATE PROCEDURE [Control].[SP_FuelDistAddDetailAlert_Retrieve]--null, null, 8603, null, null, 0, 17848, 101
(		
--declare
	@pVehicleId INT = NULL,
	@pUserId INT = NULL,	
	@pCustomerId INT = NULL,	
	@pPlateId VARCHAR(50) = NULL,
	@pVehicleGroupId INT = NULL,
	@pCostCenterId INT = NULL,
	@pLoggedUserId INT = NULL,
	@pIssueForId INT = NULL

--select 
-- @pVehicleId       = null
--,@pUserId		   = null
--,@pCustomerId	   = 106
--,@pPlateId		   = null
--,@pVehicleGroupId  = null
--,@pCostCenterId	   = null
--,@pLoggedUserId	   = 1
--,@pIssueForId	   = 101
)
AS
BEGIN
	SET NOCOUNT ON	
	
	DECLARE	 @Results TABLE (items INT)
	DECLARE  @count INT
			,@lInsertDate DATETIME = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)
			,@lId INT
			,@lTimeZoneParameter INT
	
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pLoggedUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) 
					  FROM	@Results)
	
	SELECT @lTimeZoneParameter = [TimeZone],
		   @pIssueForId = ISNULL(@pIssueForId, cu.[IssueForId])
	FROM [General].[Countries] c
	INNER JOIN [General].[Customers] cu
		ON c.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId
	
	IF @pCostCenterId = 0 OR @pCostCenterId = -1 SET @pCostCenterId = NULL

	/*************< WE ONLY ADD THOSE VEHICLES THAT HAVE ALERT >******************/	
	DECLARE	  @pPorcent INT = NULL
	
    --GET THE PARAMETER BASED ON THE CUSTOMER INFORMATION    
    SELECT @pPorcent = ISNULL([ParameterValue], @pPorcent)
    FROM [General].[AlertsByCustomer]
    WHERE [AlertId] = 1
          AND [CustomerId] = @pCustomerId;

    SELECT TOP 201 a.[VehicleId]
	INTO #Vehicles
    FROM [Control].[FuelDistribution]				a WITH(NOLOCK) 
         INNER JOIN [General].[Vehicles]			b WITH(NOLOCK) ON a.[VehicleId] = b.[VehicleId]
         LEFT JOIN (
						SELECT y.[VehicleId], 
							   SUM(x.[FuelAmount]) AS [RealAmount], 
							   SUM(x.[Liters]) AS [RealLiters]
						FROM [Control].[Transactions] x WITH(NOLOCK)
							 INNER JOIN [General].[Vehicles] y WITH(NOLOCK) ON x.[VehicleId] = y.[VehicleId]
						WHERE y.[CustomerId] = @pCustomerId
							  AND x.[IsFloating] = 0
							  AND x.[IsReversed] = 0
							  AND x.[IsDuplicated] = 0
							  AND (x.[IsDenied] IS NULL
								   OR x.[IsDenied] = 0)
							  AND y.[Active] = 1
							  AND 1 >
						(
							SELECT COUNT(*)
							FROM [Control].[Transactions] t2 WITH(NOLOCK)
							WHERE t2.[CreditCardId] = x.[CreditCardId]
								  AND t2.[TransactionPOS] = x.[TransactionPOS]
								  AND t2.[ProcessorId] = x.[ProcessorId]
								  AND (t2.IsReversed = 1
									   OR t2.IsVoid = 1)
						)
							  AND CONVERT(DATE, DATEADD(HOUR, @lTimeZoneParameter, x.[InsertDate])) >= @lInsertDate
						GROUP BY y.[VehicleId]
					) t ON a.[VehicleId] = t.[VehicleId]
    WHERE(@pCustomerId IS NULL
          OR b.[CustomerId] = @pCustomerId)
         AND CONVERT(DATETIME, a.[InsertDate]) >= @lInsertDate
         AND a.[Amount] > 0
         AND CONVERT(DECIMAL(16, 2), ROUND(ISNULL(t.[RealAmount], 0) / (a.[Amount] + ISNULL(a.[AdditionalAmount], 0)), 2) * 100) >= @pPorcent
    ORDER BY a.[VehicleId] DESC;

/***********************************************************************************************/

	CREATE TABLE #Ids
	(
		[Id] INT,
		[FuelId] INT
	)	

	CREATE TABLE #Results 
	(	
		 [Id] INT IDENTITY
		,[VehicleId] INT
		,[DriverId] INT
		,[Available] DECIMAL(16, 2)
		,[Assigned] DECIMAL(16, 2)
		,[AssignedLiters] DECIMAL(16, 3)
		,[RealAmount] DECIMAL(16, 2)
		,[RealLiters] DECIMAL(16, 3)
		,[TotalAssigned] DECIMAL(16, 2)
		,[Total] DECIMAL(16, 2)
		,[CreditCardId] INT
		,[PaymentIntrumentId] INT
		,[AvailableLiters] DECIMAL(16, 3)
	)

	SELECT @pUserId = CASE WHEN COUNT(1) = 0
                         THEN NULL
                         ELSE @pUserId
                      END
    FROM [General].[DriversUsers] d
    LEFT JOIN [General].[VehiclesByUser] v
        ON v.[UserId] = d.[UserId]
    WHERE (v.[UserId] IS NOT NULL AND v.[UserId]= @pUserId)
    OR (d.[UserId] IS NOT NULL AND d.[UserId]= @pUserId)

	IF @pIssueForId = 101
	BEGIN 

		INSERT INTO #Ids
		SELECT DISTINCT TOP 201	a.[VehicleId]
						       ,d.[FuelId]								   
		FROM [Control].[FuelDistribution] a WITH(NOLOCK)
		INNER JOIN [General].[Vehicles] b WITH(NOLOCK)
			ON a.[VehicleId] = b.[VehicleId]	
		INNER JOIN #Vehicles ve 
			ON b.[VehicleId] = ve.[VehicleId]
		LEFT JOIN [General].[VehiclesByGroup] vg WITH(NOLOCK)
			ON b.[VehicleId] = vg.[VehicleId]  
		INNER JOIN [General].[VehicleCategories] c WITH(NOLOCK)
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [General].[VehicleCostCenters] cc  WITH(NOLOCK)
			ON b.[CostCenterId] = cc.[CostCenterId]		
		INNER JOIN [General].[Customers] e WITH(NOLOCK)
			ON b.[CustomerId] = e.[CustomerId]
		INNER JOIN [Control].[Fuels] d WITH(NOLOCK)
			ON c.[DefaultFuelId] = d.[FuelId]			  
		WHERE (b.[IsDeleted] IS NULL OR b.[IsDeleted] = 0)		
		AND (@pPlateId IS NULL OR b.[PlateId] = @pPlateId)		  
		AND (@pCustomerId IS NULL OR b.[CustomerId] = @pCustomerId)
		AND (@pCostCenterId IS NULL OR cc.[CostCenterId] = @pCostCenterId)
		AND (@pVehicleGroupId IS NULL OR vg.[VehicleGroupId] = @pVehicleGroupId)
		AND (@count = 0 OR a.[VehicleId] IN (SELECT [items] 
											 FROM @Results))
		AND a.[InsertDate] >= @lInsertDate 

  	END 
	ELSE 
	BEGIN 
		INSERT INTO #Ids
		SELECT DISTINCT TOP 201	a.[DriverId]
								,NULL [FuelId]
		FROM [Control].[FuelDistribution] a WITH(NOLOCK)
		INNER JOIN [General].[Users] u WITH(NOLOCK)
			ON u.[UserId] = a.[DriverId]        
		INNER JOIN [General].[DriversUsers] du WITH(NOLOCK)
			ON du.[UserId] = a.[DriverId]		
		WHERE (@pUserId IS NULL OR a.[DriverId] = @pUserId)
		AND (@pCustomerId IS NULL OR du.[CustomerId] = @pCustomerId)
		AND (@pCostCenterId IS NULL OR du.[CostCenterId] = @pCostCenterId)            
		AND a.[InsertDate] >= @lInsertDate
	END 

	SELECT @lId = MIN([Id]) 
	FROM #Ids

	WHILE @lId IS NOT NULL
	BEGIN
	
		DECLARE @lFUelId INT = (SELECT [FuelId]
								FROM #Ids
								WHERE [Id] = @lId)

		IF @pIssueForId = 101
		BEGIN			
			INSERT INTO #Results
			(
				 [Available]
				,[Assigned]
				,[AssignedLiters]
				,[RealAmount]
				,[RealLiters]
				,[TotalAssigned]
				,[Total]
				,[CreditCardId]
				,[PaymentIntrumentId]  /* Esto hay que comentarlo y habilitarlo con */
				,[AvailableLiters]
			)
			EXEC [Control].[Sp_GetDistributionData] @pVehicleId         = @lId
												   ,@pCustomerId        = @pCustomerId
												   ,@pFuelId	        = @lFUelId
												   ,@pTimeZoneParameter = @lTimeZoneParameter
			UPDATE #Results 
			SET [VehicleId] = @lId
			WHERE [Id] = SCOPE_IDENTITY()
		END 
		ELSE 
		BEGIN
			INSERT INTO #Results
			(
				 [Available]
				,[Assigned]
				,[AssignedLiters]
				,[RealAmount]
				,[RealLiters]
				,[TotalAssigned]
				,[Total]
				,[CreditCardId]
				,[PaymentIntrumentId]  /* Esto hay que comentarlo */
				,[AvailableLiters]
			)
			EXEC [Control].[Sp_GetDistributionData] @pDriverId = @lId
												   ,@pCustomerId = @pCustomerId												   
												   ,@pTimeZoneParameter = @lTimeZoneParameter
			UPDATE #Results 
			SET [DriverId] = @lId
			WHERE [Id] = SCOPE_IDENTITY()
		END

		SELECT @lId = MIN([Id])
		FROM #Ids
		WHERE [Id] > @lId
	END /*End While*/

	SELECT [VehicleId] 
		  ,[DriverId]
		  ,ISNULL([Available], 0) [Available]
		  ,ISNULL([Assigned], 0) [Assigned]
		  ,ISNULL([RealAmount], 0) [RealAmount]
		  ,ISNULL([TotalAssigned], 0) [TotalAssigned]
		  ,ISNULL([Total], 0) [Total]
		  ,ISNULL([AvailableLiters], 0) [AvailableLiters]
	FROM #Results

	DROP TABLE #Ids, #Results
	DROP TABLE #Vehicles

	SET NOCOUNT OFF
END

GO
