USE [ECOsystem]
GO

IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Fn_GetEventColor]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [Management].[Fn_GetEventColor]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Henry Retana - Marjorie Garbanzo
-- Create date: 25/10/2018
-- Description:	Set color for event
-- =============================================

CREATE FUNCTION [Management].[Fn_GetEventColor]
(
	@pEventTypeId INT = NULL,
	@pStopEvent INT = NULL	
)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @lResult VARCHAR(100) = NULL,
			@lIsStop BIT = 0,
			@lLocation BIT = 0,
			@lCatalogId INT = NULL 

	SELECT @lCatalogId = [CatalogId]
	FROM [Management].[CatalogDetail]
	WHERE [Id] = @pEventTypeId

	SELECT @lIsStop = CAST(COUNT(1) AS bit)
	FROM [Management].[CatalogDetail]
	WHERE [Id] = @pStopEvent	
	AND [Value] = 'AGENT_STOP'

	SELECT @lLocation = CAST(COUNT(1) AS bit)
	FROM [Management].[CatalogDetail]
	WHERE [Id] = @pStopEvent	
	AND [Value] = 'AGENT_LOCATION_REAL_TIME'
	
	SELECT @lResult = CASE WHEN @lCatalogId = 2 AND @lIsStop = 0 AND @lLocation = 0 
						   THEN 'NavajoWhite'
						   WHEN @lCatalogId = 2 AND @lIsStop = 1 AND @lLocation = 0
						   THEN 'DarkGray'
						   ELSE NULL
					  END 

	RETURN @lResult
END