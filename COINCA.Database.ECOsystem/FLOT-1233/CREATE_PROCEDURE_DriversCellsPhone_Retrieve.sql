
IF EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE NAME = 'DriversCellsPhone_Retrieve')
	DROP PROC [General].[DriversCellsPhone_Retrieve] --30
GO

-- =============================================
-- Author:      Juan Carlos Santamaria
-- Create Date: 2020-06-29
-- Description: Get Driver Cellphone
-- =============================================
CREATE PROCEDURE [General].[DriversCellsPhone_Retrieve]    
(
	@pCustomerId INT = NULL     
)    
AS   
	--SELECT @pCustomerId = 30
BEGIN      
 SET NOCOUNT ON     

 DECLARE @lUserStatus INT = NULL
   
 SELECT  
	a.[UserId]    d
   ,a.[Name]					[EncryptedName]    
   ,f.[DriversUserId]    
   ,f.[CustomerId]    
   ,f.[Identification]			[EncryptedIdentification]    
   ,b.[Email] [EncryptedEmail]    
   ,ISNULL(b.[PhoneNumber],'')	[EncryptedPhoneNumber]    
   ,b.[UserName]				[EncryptedUserName]
   ,a.[HasWhatsapp]				[Whatsapp]
 FROM [General].[Users] a WITH(NOLOCK)   
 INNER JOIN [dbo].[AspNetUsers] b WITH(NOLOCK)   
  ON a.[AspNetUserId] = b.[Id]    
 INNER JOIN [dbo].[AspNetUserRoles] c WITH(NOLOCK)    
  ON c.[UserId] = a.[AspNetUserId]
 INNER JOIN [dbo].[AspNetRoles] d  WITH(NOLOCK)
  ON d.[Id] = c.[RoleId]
 INNER JOIN [General].[DriversUsers] f WITH(NOLOCK)
  ON f.[UserId] = a.[UserId]         
 WHERE a.[IsDeleted] = 0  
 AND (b.[PhoneNumber] IS NOT NULL)
 AND LEN(RTRIM(LTRIM(b.[PhoneNumber]))) > 0
 AND (@pCustomerId IS NULL OR f.[CustomerId] = @pCustomerId)        
 AND a.[IsActive] = 1
 
    
 SET NOCOUNT OFF    

END 

go

