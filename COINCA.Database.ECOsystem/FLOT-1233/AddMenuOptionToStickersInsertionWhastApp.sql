--USE [ECOsystem]
--GO

-- ================================================================================================
-- Author:		Juan Carlos Santamaria V.
-- Create date: 26-06-2020
-- Description:	Third party management
-- ================================================================================================

DECLARE @NewPermID NVARCHAR(128) = NEWID()
	   ,@lParentId INT

INSERT INTO [dbo].[AspNetPermissions]
VALUES
(
	@NewPermID
	,'View_ThirdPartyManagement'--'View_Stickers'
	,'ThirdPartyManagement'
	,'Index'
	,''
	,GETDATE()
)

INSERT INTO [dbo].[AspMenus]
VALUES
(
	'Gesti�n de terceros',
	'',--'~/Content/Images/menu_credit_cards.png',
	@NewPermID,
	87,
	8,
	116,
	GETDATE()
)

DECLARE @NewMenuID INT = SCOPE_IDENTITY()

--INSERT PERMISSIONS TO THE MENU AND ROLE
INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('F024C5BD-C5F4-408C-8A86-B5DBA55435E0', @NewMenuID) -- Super Usuario Cliente
INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('454df528-3a24-4361-9425-25426682fa70', @NewMenuID) -- CUSTOMER_USER
INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('9baac678-473a-4f96-9d67-9f2a5af778f3', @NewMenuID) -- CUSTOMER_ADMIN

INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('F024C5BD-C5F4-408C-8A86-B5DBA55435E0', @NewPermID) -- Super Usuario Cliente
INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('454df528-3a24-4361-9425-25426682fa70', @NewPermID) -- CUSTOMER_USER
INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('9baac678-473a-4f96-9d67-9f2a5af778f3', @NewPermID) -- CUSTOMER_ADMIN


----select * from [dbo].[AspNetRoles] where customerId = 30
----select * from AspNetRoles where customerId is null and PartnerId is null and active = 1 and parent is null
--select * from AspNetPermissions where id = '41CC12F7-59E2-4E1E-B3A3-DC6012224BDD'
--select * from AspNetRolePermissions where PermissionId = '41CC12F7-59E2-4E1E-B3A3-DC6012224BDD'
--select * from AspNetRoles where id = '4b356447-c286-45d6-bace-d1cc1ae298db'
--select * from aspMenus where id =  239
--select * from AspmenusByRoles where menuid = 239

--select * 
--from [dbo].[AspNetUserRoles] a
--inner join [dbo].[AspNetRoles] b on b.id = a.RoleId
--where UserId = 'b5f929d2-cfb4-4f21-93ee-e705ef7baa4a' --JuanKita
--select * from AspNetRoles where active = 1 and Parent is null