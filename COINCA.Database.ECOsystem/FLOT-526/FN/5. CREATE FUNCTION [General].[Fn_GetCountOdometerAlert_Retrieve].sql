USE [ECOsystem]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Fn_GetCountOdometerAlert_Retrieve]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [General].[Fn_GetCountOdometerAlert_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:  Henry Retana
-- Create date: 10/10/2016
-- Description: Returns the Odometer - Logic from [Control].[Sp_Setting_Odometers_Retrieve]
-- Modify By: Stefano Quirós - Add the Time Zone Parameter - 14/11/2017 - Add Void Transactions Validation
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- ======================================================================
CREATE FUNCTION [General].[Fn_GetCountOdometerAlert_Retrieve]
(
	 @pCustomerId INT
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pUserId INT
)
RETURNS INT
AS
BEGIN
	
	DECLARE @ReturnCount INT	
	DECLARE @lParameterO DECIMAL(16,2)
	DECLARE @lTimeZoneParameter INT 

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	SELECT	items
	FROM	dbo.Fn_Split((SELECT [Value]
						  FROM	dbo.[UsersDynamicFilter]
						  WHERE	[UserId] = @pUserId
						    AND [CustomerId] = @pCustomerId
							AND [Type] = 'VH'), ',')
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu
	ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	SELECT @lParameterO = [ParameterValue]
	FROM [General].[AlertsByCustomer]
	WHERE [AlertId] = 2 
	  AND [CustomerId] = @pCustomerId;	

	IF @lParameterO IS NULL 
		SET @lParameterO = 0.15
	ELSE
		SET @lParameterO = (@lParameterO/100);

	WITH TempTable AS 
	(
		SELECT ((t.[Odometer] - (SELECT TOP 1 TT.[Odometer] FROM(
				 SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
				 WHERE NT.[VehicleId] = t.[VehicleId] 
				 AND NT.[Date] <= t.[Date] 
				 AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
				 AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
				 AND 1 > (  SELECT ISNULL(COUNT(1), 0) 
			   				FROM [Control].[Transactions] t2
			   				WHERE t2.[CreditCardId] = NT.[CreditCardId] 
			   				AND   t2.[TransactionPOS] = NT.[TransactionPOS] 
			   				AND   t2.[ProcessorId] = NT.[ProcessorId] 
			   				AND  (t2.IsReversed = 1 OR t2.IsVoid = 1)
			   			)
				 ORDER BY NT.[Date] DESC ) TT ORDER BY TT.[Date] ASC)) / t.[Liters]) [Performance]
			   ,vc.[DefaultPerformance]
		FROM 
			[Control].[Transactions] t
			INNER JOIN [General].[Vehicles] v 
				ON t.[VehicleId] = v.[VehicleId]
			INNER JOIN [General].[Customers] c 
				ON v.[CustomerId] = c.[CustomerId]	
			INNER JOIN [General].[VehicleCategories] vc	
				ON v.[VehicleCategoryId] = vc.[VehicleCategoryId] 				
		WHERE
			(@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
			AND c.[CustomerId] = @pCustomerId 
			AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
			AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
			AND	1 > (
					SELECT ISNULL(COUNT(1), 0) FROM Control.Transactions t2
							WHERE t2.[CreditCardId] = t.[CreditCardId] AND
								t2.[TransactionPOS] = t.[TransactionPOS] AND
								t2.[ProcessorId] = t.[ProcessorId] AND 
							   (t2.IsReversed = 1 OR t2.IsVoid = 1)
				)			
				AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
						AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear))
	)
	SELECT @ReturnCount = ISNULL(COUNT(1), 0)
	FROM TempTable t
	WHERE t.[Performance] NOT BETWEEN (t.[DefaultPerformance] - (t.[DefaultPerformance] * @lParameterO)) 
									   AND (t.[DefaultPerformance] + (t.[DefaultPerformance] * @lParameterO))	

	RETURN @ReturnCount
END

