USE [ECOsystem]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Fn_GetCountBudgetAlert_Retrieve]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [General].[Fn_GetCountBudgetAlert_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:  Henry Retana
-- Create date: 10/10/2016
-- Description: Returns the Budget - Logic from [Control].[Sp_AlarmCreditCount_Retrieve]
-- Modify by Stefano Quirós - 14/11/2017
-- Add Void Transactions Validation
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- ======================================================================
CREATE FUNCTION [General].[Fn_GetCountBudgetAlert_Retrieve]
(
	 @pCustomerId INT = NULL
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pUserId INT = NULL	
)
RETURNS INT
AS
BEGIN
	
	DECLARE @ReturnCount INT	

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	SELECT	items
	FROM	dbo.Fn_Split((SELECT [Value]
						  FROM	dbo.[UsersDynamicFilter]
						  WHERE	[UserId] = @pUserId
							AND [CustomerId] = @pCustomerId
							AND [Type] = 'VH'), ',')
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
		
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	DECLARE @lParameterP INT = 0	

	DECLARE @lInsertDate DATE
   
    SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)

	SELECT @lParameterP = [ParameterValue]
	FROM [General].[AlertsByCustomer]
	WHERE [AlertId] = 1 
	  AND [CustomerId] = @pCustomerId

	SELECT @lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
	INNER JOIN [General].[Customers] b
		ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId

	SELECT @ReturnCount = ISNULL(SUM(1), 0)
	FROM [General].[Vehicles] a
	LEFT JOIN (	SELECT	y.[VehicleId]
						,SUM(x.[FuelAmount]) AS [RealAmount]
				FROM	[Control].[Transactions] x
						INNER JOIN [General].[Vehicles] y ON x.[VehicleId] = y.[VehicleId]
				WHERE y.[CustomerId] = @pCustomerId
				AND x.[IsFloating] = 0
				AND x.[IsReversed] = 0
				AND x.[IsDuplicated] = 0
				AND (x.[IsDenied] IS NULL OR x.[IsDenied] = 0)
				AND y.[Active] = 1
				AND 1 > (
					SELECT COUNT(*) FROM Control.Transactions t2
						WHERE t2.[CreditCardId] = x.[CreditCardId] AND
						t2.[TransactionPOS] = x.[TransactionPOS] AND
						t2.[ProcessorId] = x.[ProcessorId]  
						AND (t2.IsReversed = 1 OR t2.IsVoid = 1)
				)
				AND CONVERT(DATE, x.[Date]) >= @lInsertDate						
				GROUP BY y.[VehicleId]) t
			ON a.[VehicleId] = t.[VehicleId]			
		INNER JOIN (SELECT y.[VehicleId]
						  ,SUM(x.[Amount]) AS [AssignedAmount]
						  ,SUM(x.[AdditionalAmount]) AS [AdditionalAmount]
					FROM [Control].[FuelDistribution] x
					INNER JOIN [General].[Vehicles] y
						ON x.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId
					AND y.[Active] = 1
					AND CONVERT(DATE, x.[InsertDate]) >= @lInsertDate						
					AND x.[Amount] > 0  
					GROUP BY y.[VehicleId]) s
			ON a.[VehicleId] = s.[VehicleId]		
			WHERE (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
			 AND  CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / (s.[AssignedAmount] + ISNULL(s.[AdditionalAmount], 0)), 2)*100) >= @lParameterP

	RETURN @ReturnCount
END

