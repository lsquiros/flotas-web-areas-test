USE [ECOsystem]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Fn_FuelDistribution_TransactionsAmount]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [Control].[Fn_FuelDistribution_TransactionsAmount]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:		Stefano Quirós
-- Create date: 08-03-2018
-- Description:	Get fuel amount of the transactions
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- =================================================================

CREATE FUNCTION [Control].[Fn_FuelDistribution_TransactionsAmount] 
(
	 @pCreditCardId INT = NULL
	,@pTimeZoneParameter INT = NULL
	,@pInsertDate DATE = NULL
	,@pFinalDate DATE = NULL
)
RETURNS DECIMAL(16, 6)
AS
BEGIN

	DECLARE @RealAmount DECIMAL(16, 6) = 0	
	
	SET @RealAmount = ISNULL((
				SELECT SUM(x.[FuelAmount])
				FROM [Control].[Transactions] x
				WHERE x.[CreditCardId] = @pCreditCardId
					AND x.[IsFloating] = 0
					AND x.[IsReversed] = 0
					AND x.[IsDuplicated] = 0
					AND (x.[IsDenied] IS NULL OR x.[IsDenied] = 0)
					AND 1 > (
						SELECT COUNT(1)
						FROM CONTROL.Transactions t2
						WHERE t2.[CreditCardId] = x.[CreditCardId]
							AND t2.[TransactionPOS] = x.[TransactionPOS]
							AND t2.[ProcessorId] = x.[ProcessorId]
							AND (
								t2.IsReversed = 1
								OR t2.IsVoid = 1
								)
						)
					--NUEVO FILTRO DONDE LA FECHA DE INSERCION SEA IGUAL A LA FECHA DEL ULTIMO CIERRE  
					AND ((@pFinalDate IS NOT NULL
							AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) >= @pInsertDate
							AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) <= @pFinalDate)
						 OR (@pFinalDate IS NULL AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) >= @pInsertDate))
				), 0.0)

	RETURN @RealAmount;

END

