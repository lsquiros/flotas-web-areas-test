USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_SaleTransaction_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_SaleTransaction_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 10/11/2017
-- Description:	Retrieve Sale Transaction
-- Modify by Marjorie Garbanzo - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_SaleTransaction_Retrieve] 
(
	 @pTransactionId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON	
		
	SELECT TOP 1 t.[TransactionId] [CCTransactionVPOSId]	
				,t.[CreditCardId]
				,cc.[CustomerId]			
				,t.[ProcessorId] [TerminalId]
				,t.[Invoice]
				,cc.[CreditCardNumber] [AccountNumber]
				,t.[FuelAmount] [TotalAmount]
				,CAST(t.[Odometer] AS DECIMAL(12, 2)) [Odometer]
				,t.[Liters]
				,v.[PlateId] [Plate]
				,t.[AuthorizationNumber]			
				,t.[TransactionPOS] [SystemTraceNumber]
				,p.[PartnerId]
	FROM [Control].[Transactions] t
	INNER JOIN [Control].[CreditCard] cc
		ON t.[CreditCardId] = cc.[CreditCardId]
	INNER JOIN [General].[Vehicles] v
		ON t.[VehicleId] = v.[VehicleId]
	INNER JOIN [General].[CustomersByPartner] p
		ON cc.[CustomerId] = p.[CustomerId]
	WHERE t.[TransactionId] = @pTransactionId
	AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)
	
    SET NOCOUNT OFF
END

