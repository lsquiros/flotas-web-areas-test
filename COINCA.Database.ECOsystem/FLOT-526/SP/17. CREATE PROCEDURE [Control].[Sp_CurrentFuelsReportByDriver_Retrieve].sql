USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByDriver_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CurrentFuelsReportByDriver_Retrieve] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================    
-- Author:  Albert EStrada  
-- Create date: 11/24/2017    
-- Description: Retrieve Current Fuels Report By Driver information    
-- Modify by Esteban Solís - 30-01-2018 -  Add column Litters and change calculation of real litters
-- Modify by Esteban Solís - 03-05-2018 - Added logic to get liters from transactions
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================   

CREATE PROCEDURE [Control].[Sp_CurrentFuelsReportByDriver_Retrieve]
(
	 @pCustomerId INT --@pCustomerId: Customer Id    
	,@pYear INT = NULL --@pYear: Year    
	,@pMonth INT = NULL --@pMonth: Month    
	,@pStartDate DATETIME = NULL --@pStartDate: Start Date    
	,@pEndDate DATETIME = NULL --@pEndDate: End Date    
	,@pUserId INT --@pUserId: User Id    
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lInsertDate DATE = CASE WHEN @pStartDate IS NULL 
									THEN DATEADD(YEAR, @pYear-1900, DATEADD(MONTH, @pMonth-1, DATEADD(day, 1-1, 0)))
								ELSE @pStartDate
								END

	DECLARE @lFinalDate DATE = CASE WHEN @pEndDate IS NULL 
									THEN DATEADD(YEAR, @pYear-1900, DATEADD(MONTH, (@pMonth + 1)-1, DATEADD(day, 1-1, 0)))
								ELSE @pEndDate
								END


	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	-- DYNAMIC FILTER    
	DECLARE @Results TABLE (items INT)
	DECLARE @count INT
	DECLARE @lTimeZoneParameter INT = 0 	
	DECLARE @lCustomerCapacityUnit INT
	DECLARE @lEquival DECIMAL(18,8) = 3.78541178

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu 
	ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId

	SELECT @lCustomerCapacityUnit = c.[UnitOfCapacityId]
	FROM [General].[Customers] c
    WHERE c.[CustomerId] = @pCustomerId

	
	IF (@lCustomerCapacityUnit = 0) SET @lEquival = 1

	INSERT @Results
	EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId

	SET @count = (
			SELECT COUNT(1)
			FROM @Results
			)

	-- END    
DECLARE @drivers TABLE (
		CreditCardId INT
		,Active BIT
		--,PlateNumber VARCHAR(500)
		,VehicleName VARCHAR(500)
		,RealAmount DECIMAL(16, 2)
		,AssignedAmount DECIMAL(16, 2)
		,RealAmountPct DECIMAL(16, 2)
		,CurrencySymbol VARCHAR(10)
		,CostCenterId INT
		,AssignedLiters DECIMAL(16, 2)
		,RealLiters DECIMAL(16, 2)
		,Liters DECIMAL(16, 2)
		,AditionalAmount DECIMAL(16, 2)
		,AditionalLiters DECIMAL(16, 2)
		,FuelName VARCHAR(500)
		,DriverID INT
		)

	SELECT @lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
	INNER JOIN [General].[Customers] b ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId

	INSERT INTO @drivers
	SELECT cc.CreditCardId
		,1
		--,v.[PlateId] AS [PlateNumber]
		,u.[Name] AS [VehicleName]
		,ISNULL(CONVERT(DECIMAL (16, 2), [Control].[Fn_FuelDistribution_TransactionsAmount](cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate), 2), 0) [RealAmount]
		,(SELECT ISNULL(Convert(Decimal(16,2), SUM([Amount]), 2), 0) FROM [Control].[FuelDistribution] WHERE [DriverId] = du.[UserId] AND [InsertDate] >= @lInsertDate AND [InsertDate] < @lFinalDate) [AssignedAmount]
		,CASE WHEN (SELECT Convert(Decimal(16,2), ISNULL(SUM([Amount]), 0) + ISNULL(SUM([AdditionalAmount]), 0), 2)
					 FROM [Control].[FuelDistribution] 
					 WHERE [DriverId] = du.[UserId] AND [InsertDate] >= @lInsertDate AND [InsertDate] < @lFinalDate) = 0
		 THEN 0
		 ELSE ISNULL(CONVERT(DECIMAL(16, 2), ROUND(ISNULL([Control].[Fn_FuelDistribution_TransactionsAmount](cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate), 0) / (SELECT Convert(Decimal(16,2), ISNULL(SUM([Amount]), 0) + ISNULL(SUM([AdditionalAmount]), 0), 2)
																																														  FROM [Control].[FuelDistribution] 
																																														  WHERE [DriverId] = du.[UserId] AND [InsertDate] >= @lInsertDate AND [InsertDate] < @lFinalDate), 2) * 100), 0) 
		END [RealAmountPct] 
		,@lCurrencySymbol AS [CurrencySymbol]
		,'' AS CostCenterId

		,(SELECT ISNULL(Convert(Decimal(16,2), SUM([Liters]) / @lEquival, 2), 0) 
		  FROM [Control].[FuelDistribution] 
		  WHERE [DriverId] = du.[UserId] AND [InsertDate] >= @lInsertDate 
		  AND [InsertDate] < @lFinalDate)  [AssignedLiters] 
		   
		,ISNULL(CONVERT(DECIMAL (16, 2), [Control].[Fn_FuelDistribution_TransactionsLiters](cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate, 0), 2),0) [RealLiters]   
		,ISNULL(CONVERT(DECIMAL (16, 2), [Control].[Fn_FuelDistribution_TransactionsLiters](cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate, 1), 2),0) [Liters]  
		,(SELECT ISNULL(Convert(Decimal(16,2), SUM([AdditionalAmount]), 2), 0) FROM [Control].[FuelDistribution] WHERE [DriverId] = du.[UserId] AND [InsertDate] >= @lInsertDate AND [InsertDate] < @lFinalDate) [AditionalAmount]      
		
		,(SELECT ISNULL(Convert(Decimal(16,2), SUM([AdditionalLiters]) / @lEquival, 2), 0) 
		  FROM [Control].[FuelDistribution] 
		  WHERE [DriverId] = du.[UserId] AND [InsertDate] >= @lInsertDate 
		  AND [InsertDate] < @lFinalDate) [AditionalLiters]   
		   
		,f.[Name] [FuelName]
		,du.[UserId]
	FROM [General].[Users] u 
	INNER JOIN [General].[DriversUsers] du 
		ON du.[UserId] = u.[UserId]
	INNER JOIN [General].[Customers] c
		ON du.[CustomerId] = c.[CustomerId]
	INNER JOIN [Control].[CreditCardByDriver] ccb 
		ON ccb.Userid = du.[UserId]
	INNER JOIN [Control].[CreditCard] cc
		ON cc.[CreditCardId] = ccb.[CreditCardId]
			AND cc.[StatusId] IN (7, 8)
	INNER JOIN [Control].[Transactions] t
		ON t.[CreditCardId] = cc.[CreditCardId]
	INNER JOIN [control].[fuels] f 
		ON f.FuelId = t.[FuelId]
	WHERE du.[CustomerId] = @pCustomerId
		  AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		  AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
		  AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				   FROM [Control].[Transactions] t2
				   WHERE t2.[CreditCardId] = t.[CreditCardId] 
				   AND t2.[TransactionPOS] = t.[TransactionPOS] 
				   AND t2.[ProcessorId] = t.[ProcessorId] 
				   AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
		AND (
			u.isDeleted = 0
			OR u.isDeleted IS NULL
			)	
	GROUP BY du.[UserId], u.[Name], cc.CreditCardId, f.[Name]

	SELECT DISTINCT --PlateNumber
		VehicleName
		,RealAmount
		,AssignedAmount
		,RealAmountPct
		,CurrencySymbol
		,CostCenterId
		,AssignedLiters
		,RealLiters
		,AditionalAmount
		,AditionalLiters
		,FuelName
	FROM @drivers
	ORDER BY RealAmountPct DESC

	SET NOCOUNT OFF
END
