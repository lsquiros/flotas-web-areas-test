USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 02/20/2015
-- Description:	Retrieve Odometer Traveled Report By Vehicle
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- Modify by: Stefano Quiros 12/04/2016
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_OdometerTraveledReportByVehicle_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pKey VARCHAR(800) = NULL			--@pKey: Search Key
	,@pUserId INT						--@pUserId: User Id
)
AS
BEGIN
	
	SET NOCOUNT ON

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	SELECT   a.[VehicleId]
			,b.PlateId
			,b.[Name] AS [VehicleName]
			,MIN(a.[Date]) AS [TrxMinDate]
			,MAX(a.[Date]) AS [TrxMaxDate]
			,MIN(COALESCE(a.FixedOdometer, a.Odometer, 0)) AS [TrxMinOdometer]
			,MAX(COALESCE(a.FixedOdometer, a.Odometer, 0)) AS [TrxMaxOdometer]
			,b.CustomerId
	FROM [Control].[Transactions] a WITH(READUNCOMMITTED)
	INNER JOIN [General].[Vehicles] b
		ON b.[VehicleId] = a.[VehicleId]
	WHERE b.[CustomerId] = @pCustomerId
	AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	AND a.[IsReversed] = 0
	AND a.[IsDuplicated] = 0
	AND (a.[IsDenied] IS NULL OR a.[IsDenied] = 0)
	AND b.[Active] = 1
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
							AND DATEPART(m,a.[Date]) = @pMonth
							AND DATEPART(yyyy,a.[Date]) = @pYear) OR
								(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
							AND a.[Date] BETWEEN @pStartDate AND @pEndDate))
	AND (@pKey IS NULL 
			OR b.[PlateId] like '%'+@pKey+'%')
	AND (a.[IsFloating] IS NULL OR a.[IsFloating] = 0) 
	AND 1 > (SELECT ISNULL(COUNT(1), 0) 
			 FROM [Control].[Transactions] t2
			 WHERE t2.[CreditCardId] = a.[CreditCardId] 
			 AND t2.[TransactionPOS] = a.[TransactionPOS] 
			 AND t2.[ProcessorId] = a.[ProcessorId] 
			 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
	GROUP BY a.[VehicleId]
			,b.PlateId
			,b.[Name]
			,b.CustomerId	
	
    SET NOCOUNT OFF
END