USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriverCodesInTransaction_Edit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriverCodesInTransaction_Edit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 04/10/2017
-- Description:	Edit Driver Codes for the Transactions
-- Modify by Marjorie Garbanzo - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_DriverCodesInTransaction_Edit]
(
	@pTransactionId INT,
	@pDriverCode VARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON	

	UPDATE [Control].[Transactions]
	SET [DriverCode] = @pDriverCode
	WHERE [TransactionId] = @pTransactionId
	AND ([IsDenied] = 0 OR [IsDenied] IS NULL)

	SET NOCOUNT OFF
END

