USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ServiceStationsPOSCheck_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_ServiceStationsPOSCheck_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 08/12/2017
-- Description:	Retrieve the information for the ServiceStations Validate
-- Modify by Marjorie Garbanzo - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_ServiceStationsPOSCheck_Retrieve]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lPartnerId INT

	CREATE TABLE #Parameters
	(
		[Id] INT IDENTITY,
		[PartnerId] INT,
		[MasterCardNumber] VARCHAR(500), 
		[ExpirationDate] VARCHAR(20),
		[TotalAmount] DECIMAL(16, 2)
	)

	CREATE TABLE #Result
	(
		[PartnerId] INT,
		[MasterCardNumber] VARCHAR(500), 
		[ExpirationDate] VARCHAR(20),
		[TotalAmount] DECIMAL(16, 2),
		[TerminalId] VARCHAR(30),
		[MerchantDescription] VARCHAR(500)
	)

	INSERT INTO #Parameters
	SELECT p.[PartnerId],
		   c.[CreditCardNumber],
		   CONCAT(RIGHT(c.[ExpirationYear], 2), RIGHT('0' + RTRIM(c.[ExpirationMonth]), 2)),												  
		   p.[Amount]
	FROM [General].[ServiceStationsPOSCheckParameters] p
	INNER JOIN [General].[CustomerCreditCards] c
		ON p.[MasterCardId] = c.[CustomerCreditCardId]
	WHERE [IsActive] = 1

	SELECT @lPartnerId = MIN([PartnerId])
	FROM #Parameters

	WHILE @lPartnerId IS NOT NULL
	BEGIN
		DECLARE @lCreditCardIds TABLE ([CreditCardId] INT)

		INSERT INTO @lCreditCardIds
		SELECT [CreditCardId]
		FROM [Control].[CreditCard]
		WHERE [CustomerId] IN (SELECT [CustomerId]
								FROM [General].[CustomersByPartner]
								WHERE [PartnerId] = @lPartnerId);

		WITH noservicestations AS 
		(
			SELECT DISTINCT ts.[ServiceStationId]
			FROM [General].[TerminalsByServiceStations] ts
			WHERE ts.[TerminalId] NOT IN (SELECT t.[ProcessorId]
										  FROM [Control].[Transactions] t
										  WHERE t.[Date] > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
										  AND t.[CreditCardId] IN (SELECT [CreditCardId]
																   FROM @lCreditCardIds)
										  AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL))
		), servicestations AS 
		(
			SELECT DISTINCT ts.[ServiceStationId]
			FROM [General].[TerminalsByServiceStations] ts
			WHERE ts.[TerminalId] IN (SELECT t.[ProcessorId]
									  FROM [Control].[Transactions] t
									  WHERE t.[Date] > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
									  AND t.[CreditCardId] IN (SELECT [CreditCardId]
																   FROM @lCreditCardIds)
									  AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL))
		)
		INSERT INTO #Result
		SELECT p.[PartnerId],
			   p.[MasterCardNumber],
			   p.[ExpirationDate],
			   p.[TotalAmount],
			   t.[TerminalId],
			   t.[MerchantDescription]
		FROM #Parameters p
		INNER JOIN (
					SELECT ter.[TerminalId],
						   ss.[Name] [MerchantDescription],
						   @lPartnerId [PartnerId]
					FROM noservicestations ts
					INNER JOIN [General].[ServiceStations] ss
						ON ss.[ServiceStationId] = ts.[ServiceStationId]
					INNER JOIN [General].[TerminalsByServiceStations] ter
						ON ss.[ServiceStationId] = ter.[ServiceStationId]
					WHERE ts.[ServiceStationId] NOT IN (SELECT s.[ServiceStationId]
														FROM servicestations s)
					) t
			ON p.[PartnerId] = t.[PartnerId]
		WHERE p.[PartnerId] = @lPartnerId

		SELECT @lPartnerId = MIN([PartnerId])
		FROM #Parameters
		WHERE [PartnerId] > @lPartnerId
	END

	SELECT  [PartnerId],
			[MasterCardNumber], 
			[ExpirationDate],
			[TotalAmount],
			[TerminalId],
			[MerchantDescription]
	FROM #Result

	DROP TABLE #Parameters
	DROP TABLE #Result

	SET NOCOUNT OFF	
END