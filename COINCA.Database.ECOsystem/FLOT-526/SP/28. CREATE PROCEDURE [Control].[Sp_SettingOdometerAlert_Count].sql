USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_SettingOdometerAlert_Count]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_SettingOdometerAlert_Count]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana 
-- Create date: 19/12/2016
-- Description:	Update Odometers alert count information
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_SettingOdometerAlert_Count]
(
	 @pCustomerId INT,
	 @pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON
    	
	DECLARE @ReturnCount INT,
	        @lParameterO DECIMAL(16,2),
	        @lTimeZoneParameter INT,
			@lOdometerDistance INT,
		    @lFlag INT 

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH',@pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END	

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu
	ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	--Get the parameter information for the customer
	SELECT @lParameterO = [ParameterValue],
		   @lOdometerDistance = [OdometerDifference],
		   @lFlag = [OdometerDifferenceValidation]
	FROM [General].[AlertsByCustomer]
	WHERE [AlertId] = 2 
	  AND [CustomerId] = @pCustomerId

	IF @lFlag <> 0
	BEGIN
		IF @lParameterO IS NULL 
			SET @lParameterO = 0.15
		ELSE
			SET @lParameterO = (@lParameterO/100)

		SET @lOdometerDistance = NULL
	END
	ELSE 
	BEGIN 
		SET @lParameterO = NULL 

		IF @lOdometerDistance IS NULL SET @lOdometerDistance = 1000
	END;

	WITH TempTable AS 
	(
		SELECT  CASE WHEN @lParameterO IS NOT NULL 
					 THEN  ((t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
											 FROM(
													SELECT TOP 2 NT.* 
													FROM [Control].[Transactions] NT
													WHERE NT.[VehicleId] = t.[VehicleId] 
													AND NT.[Date] <= t.[Date] 
													AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
													AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
													AND 1 > (SELECT ISNULL(COUNT(1), 0) 
															 FROM [Control].[Transactions] t2
															 WHERE t2.[CreditCardId] = nt.[CreditCardId] 
															 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
															 AND t2.[ProcessorId] = nt.[ProcessorId] 
															 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
													ORDER BY NT.[Date] DESC ) TT 
											ORDER BY TT.[Date] ASC)) / t.[Liters])
					ELSE (t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
										  FROM(
												SELECT TOP 2 NT.* 
												FROM [Control].[Transactions] NT
												WHERE NT.[VehicleId] = t.[VehicleId] 
												AND NT.[Date] <= t.[Date] 
												AND 1 > (  SELECT ISNULL(COUNT(1), 0) 
			   											FROM [Control].[Transactions] t2
			   											WHERE t2.[CreditCardId] = NT.[CreditCardId] 
			   											AND   t2.[TransactionPOS] = NT.[TransactionPOS] 
			   											AND   t2.[ProcessorId] = NT.[ProcessorId] 
			   											AND   t2.IsReversed = 1
			   										)
												ORDER BY NT.[Date] DESC ) TT 
										 ORDER BY TT.[Date] ASC)) END [Performance]
			   ,vc.[DefaultPerformance]
		FROM [Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v 
			ON t.[VehicleId] = v.[VehicleId]
		INNER JOIN [General].[Customers] c 
			ON v.[CustomerId] = c.[CustomerId]	
		INNER JOIN [General].[VehicleCategories] vc	
			ON v.[VehicleCategoryId] = vc.[VehicleCategoryId] 				
		WHERE(@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
		AND c.[CustomerId] = @pCustomerId 
		AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
		AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				 FROM [Control].[Transactions] t2
				 WHERE t2.[CreditCardId] = t.[CreditCardId] 
				 AND t2.[TransactionPOS] = t.[TransactionPOS] 
				 AND t2.[ProcessorId] = t.[ProcessorId] 
				 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))			
		AND (t.[ShowAlert] IS NULL OR t.[ShowAlert] = 0)
	)
	SELECT @ReturnCount = ISNULL(COUNT(*), 0)
	FROM TempTable t
	WHERE (@lOdometerDistance IS NOT NULL AND ([Performance] < 0 OR [Performance] > @lOdometerDistance))
	OR (@lParameterO IS NOT NULL AND(t.[Performance] NOT BETWEEN (t.[DefaultPerformance] - (t.[DefaultPerformance] * @lParameterO)) 
									   AND (t.[DefaultPerformance] + (t.[DefaultPerformance] * @lParameterO))))

	UPDATE [General].[AlertOdometerCountByCustomer]
	SET	[SettingOdometerCount] = @ReturnCount,
		[ModifyDate] = GETUTCDATE()
	WHERE [CustomerId] = @pCustomerId
	
	SELECT @ReturnCount

	SET NOCOUNT OFF
END

