USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsForReverse_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsForReverse_Retrieve]  
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 07/01/2014
-- Description:	Retrieve Transactions Info For Reverse Operations
-- Stefano Quirós - Comment the IsFloating validation because the transaction will be processed when 
-- we receive the RESULT_ADVICE from BAC - 28/11/2018
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_TransactionsForReverse_Retrieve]
(
	 @pTransactionPOS VARCHAR(250)			--@pTransactionPOS: TransactionPOS (External System TraceNumber)
	,@pProcessorId VARCHAR(250)				--@pProcessorId: ProcessorId (External terminalId)
)
AS
BEGIN
	
	SET NOCOUNT ON
			
	SELECT TOP 1
	     a.[TransactionId]
		 ,a.[CreditCardId]
		 ,a.[VehicleId]
		 ,a.[FuelId]
		 ,a.[Odometer]
		 ,a.[FuelAmount]
		 ,a.[Liters]
		 ,b.[CreditCardNumber] AS [EncryptedCreditCardNumber]
		 ,c.[PlateId]
    FROM [Control].[Transactions] a
		INNER JOIN [Control].[CreditCard] b
			ON b.[CreditCardId] = a.[CreditCardId]
		INNER JOIN [General].[Vehicles] c
			ON c.[VehicleId] = a.[VehicleId]
	WHERE a.[TransactionPOS] = @pTransactionPOS
	  AND a.[ProcessorId] = @pProcessorId
	  --AND a.[IsFloating] = 0
	  AND a.[IsReversed] = 0
	  AND a.[IsDuplicated] = 0
	  AND a.[IsVoid] = 0
	  AND (a.[IsDenied] IS NULL OR a.[IsDenied] = 0)
	  AND COALESCE(a.[IsAdjustment],0) = 0
	ORDER BY a.[InsertDate] DESC
	
    SET NOCOUNT OFF
END