USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Stations].[Sp_StationTransactionsReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Stations].[Sp_StationTransactionsReport_Retrieve] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 06/04/2017
-- Description:	Obtiene la información para las transacciones en estación de servicio
-- Modify by Henry Retana - 19/09/2017
-- Use function to get the driver id and driver name
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Stefano Quirós - Add validation of the ServiceStationId because if the service station is created 
-- twice on more than one partner the select duplicate the transactions
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================

CREATE PROCEDURE [Stations].[Sp_StationTransactionsReport_Retrieve] 
(
	@pCustomerId INT = NULL,
	@pServiceStationId INT = NULL,
	@pXMLServiceStationIds VARCHAR(MAX) = NULL,
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL	
)
AS
BEGIN

	SET NOCOUNT ON	
	
	DECLARE @lxmlData XML = CONVERT(XML, @pXMLServiceStationIds)
	CREATE TABLE #ServiceStations ([ServiceStationId] INT)

	IF @pCustomerId < 0 SET @pCustomerId = NULL

	--Valida si el id de la estacion de servicio viene individual o de forma masiva
	IF @pServiceStationId IS NULL
	BEGIN
		INSERT INTO #ServiceStations
		SELECT Row.col.value('./@ServiceStationId', 'INT') 
		FROM @lxmlData.nodes('/xmldata/ServiceStations') Row(col)
	END
	ELSE
	BEGIN
		INSERT INTO #ServiceStations
		VALUES ( @pServiceStationId )
	END	

	SELECT   c.[CustomerId]
		    ,t.[ProcessorId] [TerminalId]
			,[General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0) [ServiceStationName]			
			,DATEADD(HOUR, co.[TimeZone], t.[InsertDate]) [Date]
			,v.[Name] [VehicleName]
			,v.[PlateId]			
			,ISNULL((SELECT TOP 1 u.[Name]
					FROM [General].[Users] u
					INNER JOIN [General].[DriversUsers] du
					ON u.[UserId] = du.[UserId]
					WHERE du.[CustomerId] = c.[CustomerId] 
					AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 0)) [DriverName]
			,ISNULL((SELECT TOP 1 du.[Identification]
					FROM [General].[Users] u
					INNER JOIN [General].[DriversUsers] du
					ON u.[UserId] = du.[UserId]
					WHERE du.[CustomerId] = c.[CustomerId] 
					AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 1)) [DriverId]
			,t.[Liters]
			,ISNULL(t.[Invoice], '-') AS [Invoice]
			,t.[FuelAmount]
			,f.[Name] AS [FuelName]
			,ISNULL([Control].[Fn_FuelPriceBySSOrPartner_Retrieve](t.[InsertDate], f.[FuelId], tss.[ServiceStationId], NULL),
					[Control].[Fn_FuelPriceBySSOrPartner_Retrieve](t.[InsertDate], f.[FuelId], NULL, cp.[PartnerId])) [FuelPrice]
	FROM [Control].[Transactions] t
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]
	INNER JOIN [General].[VehicleCategories] vc
		ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]
	INNER JOIN [General].[CustomersByPartner] cp
		ON c.[CustomerId] = cp.[CustomerId]
	INNER JOIN [Control].[Fuels] f 
		ON t.[FuelId] = f.[FuelId]	
	INNER JOIN [General].[TerminalsByServiceStations] tss
		ON tss.[TerminalId] = t.[ProcessorId] 
	LEFT OUTER JOIN [General].[Countries] co
		ON t.[CountryCode] = co.[Code]
	WHERE (@pCustomerId IS NULL OR c.[CustomerId] = @pCustomerId)
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
	AND 1 > (SELECT ISNULL(COUNT(1), 0) 
			 FROM [Control].[Transactions] t2
			 WHERE t2.[CreditCardId] = t.[CreditCardId] 
			 AND t2.[TransactionPOS] = t.[TransactionPOS] 
			 AND t2.[ProcessorId] = t.[ProcessorId] 
			 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))			
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, co.[TimeZone], t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, co.[TimeZone], t.[InsertDate])) = @pYear) OR
		(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
				AND DATEADD(HOUR, co.[TimeZone], t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
	AND t.[ProcessorId] IN (SELECT tss.[TerminalId]
							FROM [General].[ServiceStations] ss
							INNER JOIN [General].[TerminalsByServiceStations] tss
								ON ss.[ServiceStationId] = tss.[ServiceStationId]
							WHERE ss.[ServiceStationId] IN (SELECT [ServiceStationId]
															FROM #ServiceStations))
	AND tss.[ServiceStationId] IN (SELECT [ServiceStationId]
								   FROM #ServiceStations)
	ORDER BY t.[Date] DESC

	DROP TABLE #ServiceStations
	SET NOCOUNT OFF
END


