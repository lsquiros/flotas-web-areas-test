USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DriverCodesInTransaction_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DriverCodesInTransaction_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 04/10/2017
-- Description:	Retrieve Driver Codes for the Transactions
-- Modify by Marjorie Garbanzo - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_DriverCodesInTransaction_Retrieve]
AS
BEGIN
	SET NOCOUNT ON	

	SELECT t.[TransactionId],
		   t.[DriverCode] [DriverCodeDecrypt]
	FROM [Control].[Transactions] t 
	LEFT JOIN [General].[DriversUsers] d
		ON t.[DriverCode] = d.[Code]
	WHERE [SchemePOS] = 'SchemeVPOS'
	AND d.[Code] IS NULL
	AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)
	ORDER BY t.[DriverCode]

	SET NOCOUNT OFF
END