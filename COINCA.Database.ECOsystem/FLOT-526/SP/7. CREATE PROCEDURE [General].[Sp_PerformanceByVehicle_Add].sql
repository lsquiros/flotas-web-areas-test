USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PerformanceByVehicle_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PerformanceByVehicle_Add]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero
-- Create date: 01/18/2015
-- Description:	Add Performance By Vehicle -- Massive process --
-- Modify By: Stefano Quirós - Add validation that only process when IntrackReference is different
-- to 0 or null
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PerformanceByVehicle_Add]
AS 
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
	    
	BEGIN TRY    
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
        DECLARE @lLocalTran BIT = 0
        
        DECLARE @lVehicleId INT, @lTrxDate DATETIME, @lPreviousTrxDate DATETIME, @lIntrackReference INT 
		DECLARE @lIndex INT, @lMaxIndex INT
    
		IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		DECLARE @lTable TABLE
		(	 [Index] INT IDENTITY(1,1) NOT NULL
			,[TransactionId] INT NULL
			,[VehicleId] INT NULL
			,[IntrackReference] INT NULL
			,[TrxDate] DATETIME NULL
			,[TrxLiters] DECIMAL(16,2) NULL
			,[TrxOdometer] DECIMAL(16,2) NULL
			,[GpsOdometer] DECIMAL(16,2) NULL
			,[PreviousTrxOdometer] DECIMAL(16,2) NULL
			,[PreviousGpsOdometer] DECIMAL(16,2) NULL
			,[PreviousTrxDate] DATETIME NULL
			,[GpsHoursOn] DECIMAL(16,2) NULL
			,[GpsHoursOnStr] VARCHAR(100)
		)
		
		INSERT INTO @lTable(
			 [TransactionId]
			,[VehicleId]
			,[TrxDate]
			,[TrxOdometer]
			,[TrxLiters]
		)    
		SELECT
			 a.[TransactionId]
			,a.[VehicleId]
			,a.[Date]
			,COALESCE(a.FixedOdometer, a.Odometer, 0) AS [TrxOdometer]
			,a.[Liters]
		FROM [Control].[Transactions] a WITH(READUNCOMMITTED)
		WHERE (a.[IsAdjustment] = 0 OR a.[IsAdjustment] IS NULL)
		  AND a.[IsFloating] = 0
		  AND a.[IsReversed] = 0
		  AND a.[IsDuplicated] = 0
		  AND (a.[IsDenied] IS NULL OR a.[IsDenied] = 0)	
		  AND 1 > (
					SELECT ISNULL(COUNT(1), 0)
					FROM CONTROL.Transactions t2
					WHERE t2.[CreditCardId] = a.[CreditCardId]
						AND t2.[TransactionPOS] = a.[TransactionPOS]
						AND t2.[ProcessorId] = a.[ProcessorId]
						AND (
							t2.IsReversed = 1
							OR t2.IsVoid = 1
							)
					)
		  AND NOT EXISTS(SELECT 1
							FROM [General].[PerformanceByVehicle] x
							WHERE x.TransactionId = a.TransactionId)
		ORDER BY a.[VehicleId], a.[TransactionId]
		
		/*1. Update first [PreviousTrxOdometer] to -1*/
		UPDATE @lTable
			SET [PreviousTrxOdometer] = -1	
		FROM(SELECT t.[VehicleId], MIN(t.[Index]) AS [MinIndex]
				FROM @lTable t
			GROUP BY t.[VehicleId])t
		WHERE [Index] = t.[MinIndex]
		
		/*2. Update [IntrackReference]*/
		UPDATE @lTable
			SET [IntrackReference] = t.[IntrackReference]
		FROM(SELECT x.[VehicleId] AS [VehicleIdWithIntrackRef], x.[IntrackReference]
				FROM [General].[Vehicles] x					
					INNER JOIN @lTable y
						ON x.[VehicleId] = y.[VehicleId])t
		WHERE [VehicleId] = t.[VehicleIdWithIntrackRef]		
		
		SELECT @lMaxIndex = COUNT(1) FROM @lTable t
		
		SET @lIndex = 1
		/*3. Update [GpsOdometer]*/
		WHILE(@lIndex <= @lMaxIndex)
		BEGIN	
			SELECT @lIntrackReference = t.[IntrackReference], @lTrxDate = t.TrxDate FROM @lTable t WHERE [Index] = @lIndex
			
			IF @lIntrackReference <> 0 OR @lIntrackReference IS NOT NULL
			BEGIN
				UPDATE @lTable
					SET [GpsOdometer] = (SELECT
											x.[GpsOdometer] 
										FROM [General].[Fn_GpsOdometerByVehicle](@lTrxDate, @lIntrackReference) x)
				WHERE [Index] = @lIndex
				  AND [PreviousTrxOdometer] IS NULL
				  AND [IntrackReference] IS NOT NULL
		    END
			SET @lIndex = @lIndex + 1
		END
		
		SET @lIndex = 1
		/*4. Update [PreviousTrxOdometer] from Previous Row*/
		WHILE(@lIndex <= @lMaxIndex)
		BEGIN	
			SELECT @lVehicleId = t.[VehicleId] FROM @lTable t WHERE [Index] = @lIndex
			
			UPDATE @lTable
				SET  [PreviousTrxOdometer] = x.[TrxOdometer]
					,[PreviousTrxDate] = x.[TrxDate]
			FROM 
			(SELECT t.[TrxOdometer], t.[TrxDate]
				FROM @lTable t
				INNER JOIN
					(SELECT t.[VehicleId], MAX(t.[Index]) AS [Index]
						FROM @lTable t
					WHERE t.[PreviousTrxOdometer] IS NOT NULL
					  AND t.[VehicleId] = @lVehicleId
					GROUP BY t.[VehicleId])u
				ON t.[Index] = u.[Index])x
			WHERE [Index] = @lIndex
			  AND [PreviousTrxOdometer] IS NULL
			
			SET @lIndex = @lIndex + 1
		END
		
		/*5. Update from last Odometer*/
		SET @lIndex = 1
		WHILE(@lIndex <= @lMaxIndex)
		BEGIN	
			SELECT @lVehicleId = t.[VehicleId] FROM @lTable t WHERE [Index] = @lIndex
			
			UPDATE @lTable
				SET  [PreviousTrxOdometer] = x.[TrxReportedOdometer]
					,[PreviousGpsOdometer] = x.[GpsReportedOdometer]
					,[PreviousTrxDate] = x.[TrxDate]
			FROM (SELECT s.[TrxReportedOdometer]
						,s.[GpsReportedOdometer]
						,s.[TrxDate]
					FROM [General].[PerformanceByVehicle] s
						INNER JOIN (SELECT
										x.[VehicleId], MAX(TransactionId) AS [TransactionId]
									FROM [General].[PerformanceByVehicle] x
									WHERE x.[VehicleId] = @lVehicleId
									GROUP BY x.[VehicleId])t
					ON s.TransactionId = t.TransactionId)x
			WHERE [Index] = @lIndex
			  AND [PreviousTrxOdometer] = -1
			  
			SET @lIndex = @lIndex + 1
		END
		
		/*6. Update from last Odometer when not exist Previous Trx Odometer*/
		UPDATE @lTable
			SET  [PreviousTrxOdometer] = NULL
				,[PreviousGpsOdometer] = NULL
		WHERE [PreviousTrxOdometer] = -1		
		
		/*7. Update Gps Hours On*/
		SET @lIndex = 1
		WHILE(@lIndex <= @lMaxIndex)
		BEGIN	
			SELECT	 @lIntrackReference = t.[IntrackReference]
					,@lTrxDate = t.TrxDate
					,@lPreviousTrxDate = t.[PreviousTrxDate] 
			FROM @lTable t WHERE [Index] = @lIndex		
			
			IF @lIntrackReference <> 0 OR @lIntrackReference IS NOT NULL
			BEGIN
				UPDATE @lTable
					SET  [GpsHoursOn] = x.[HoursOn]
						,[GpsHoursOnStr] = x.[HoursOnStr]
				FROM [General].[Fn_GpsHobbsMeterByVehicle](	 NULL
															,NULL
															,@lPreviousTrxDate
															,@lTrxDate
															,@lIntrackReference) x
				WHERE [Index] = @lIndex
				  AND [IntrackReference] IS NOT NULL
			END			  
			SET @lIndex = @lIndex + 1
		END
		
		/*8. Insert Fianal Data*/
		INSERT [General].[PerformanceByVehicle](
			 [VehicleId]
			,[TransactionId]
			,[TrxDate]
			,[TrxPreviousDate]
			,[TrxReportedLiters]
			,[TrxPreviousOdometer]
			,[TrxReportedOdometer]
			,[TrxPerformance]
			,[GpsPreviousOdometer]
			,[GpsReportedOdometer]
			,[GpsPerformance]
			,[GpsHoursOn]
			,[GpsHoursOnStr]
			,[InsertUserId]
			,[InsertDate]
		)
		SELECT
			 t.[VehicleId]
			,t.[TransactionId]
			,t.[TrxDate]
			,t.[PreviousTrxDate]
			,t.[TrxLiters]
			,t.[PreviousTrxOdometer]
			,t.[TrxOdometer]
			,ABS(t.[TrxOdometer] - CASE WHEN t.[PreviousTrxOdometer] IS NULL THEN t.[TrxOdometer] ELSE t.[PreviousTrxOdometer] END)
					/ CASE WHEN t.[TrxLiters] = 0 THEN 1 ELSE t.[TrxLiters] END
			 AS [PerformanceByOdometer]
			,t.[PreviousGpsOdometer]
			,t.[GpsOdometer]
			,ABS(ISNULL(t.[GpsOdometer],0) - CASE WHEN t.[PreviousGpsOdometer] IS NULL THEN ISNULL(t.[GpsOdometer],0) 
														ELSE ISNULL(t.[PreviousGpsOdometer],0) END)
					/ CASE WHEN t.[TrxLiters] = 0 THEN 1 ELSE t.[TrxLiters] END
			 AS [GpsPerformance]
			,t.[GpsHoursOn]
			,t.[GpsHoursOnStr]
			
			,0 AS [InsertUserId]
			,GETUTCDATE() AS [InsertDate]
		FROM @lTable t
    
		IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
		
    END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH
	
    SET XACT_ABORT OFF
	SET NOCOUNT OFF
    
END
