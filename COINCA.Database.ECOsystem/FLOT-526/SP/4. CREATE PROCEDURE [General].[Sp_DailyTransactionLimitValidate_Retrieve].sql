USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_DailyTransactionLimitValidate_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_DailyTransactionLimitValidate_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author     :	Henry Retana
-- Create date: 10/07/2018
-- Description:	Retrieve Daily Transaction Limit Validate
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_DailyTransactionLimitValidate_Retrieve]
(
	@pCreditCardNumber VARCHAR(200)
)   
AS	
BEGIN	
	SET NOCOUNT ON
	
	DECLARE @lCustomerId INT,
			@lCreditCardId INT,
			@lAmount DECIMAL(16, 2),
			@lNumber INT = NULL,
			@lTxAmount DECIMAL(16, 2),
			@lTxNumber INT = NULL,
			@lTRName VARCHAR(100) = 'DailyTransactionLimit',
			@lxmlData XML,
			@lTimeZoneParameter INT,
			@lSymbol VARCHAR(10)
	
	--GET THE CARD INFORMATION
	SELECT @lCreditCardId = c.[CreditCardId],
		   @lCustomerId = c.[CustomerId],
		   @lTimeZoneParameter = co.[TimeZone],
		   @lSymbol = cur.[Symbol]
	FROM [Control].[CreditCard] c 	
	INNER JOIN [General].[Customers] cu 
		ON c.[CustomerId] = cu.[CustomerId]
	INNER JOIN [General].[Countries] co
		ON co.[CountryId] = cu.[CountryId]
	INNER JOIN [Control].[Currencies] cur
		ON co.[Code] = cur.[Code]
	WHERE c.[CreditCardNumber] = @pCreditCardNumber
   
	--RETRIEVE THE PARAMETERS FOR THE TR
	SELECT TOP 1 @lxmlData = CONVERT(XML, [Value])
	FROM [General].[ParametersByCustomer] 
	WHERE [Name] = @lTRName 
	AND [CustomerId] = @lCustomerId
	AND [ResuourceKey] = 'TR_PARAMETERS'

	--VALIDATES IF THE CREDITCARD IS BY DRIVER
	SELECT @lNumber = [DailyTransactionLimit]
	FROM [General].[DriversUsers] du 
	INNER JOIN [Control].[CreditCardByDriver] c
		ON du.[UserId] = c.[UserId]
	WHERE c.[CreditCardId] = @lCreditCardId	

	SELECT @lNumber = CASE WHEN @lNumber IS NULL 
						   THEN ISNULL(m.c.value('Number[1]', 'INT'), 0)
						   ELSE ISNULL(@lNumber, 0)
					  END,
		   @lAmount = ISNULL(m.c.value('Amount[1]', 'INT'), 0)
	FROM @lxmlData.nodes('//DailyTransactionLimitAddOrEdit/DailyTransactionLimitAddOrEdit') AS m(c)

	--GET THE DATA FROM THE TRANSACTIONS
	SELECT @lTxNumber = COUNT(1),
		   @lTxAmount = SUM([FuelAmount])
	FROM [Control].[Transactions] t	
	INNER JOIN [Control].[CreditCard] cc 
		ON cc.[CreditCardId] = t.[CreditCardId]
	WHERE cc.[CustomerId] = @lCustomerId
	AND t.[CreditCardId] = @lCreditCardId
	AND 1 > (
				SELECT ISNULL(COUNT(1), 0)
				FROM [Control].[Transactions] t2
				WHERE t2.[CreditCardId] = t.[CreditCardId]
				AND t2.[TransactionPOS] = t.[TransactionPOS]
				AND t2.[ProcessorId] = t.[ProcessorId]
				AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)
			)
	AND (t.[TransactionOffline] IS NULL OR t.[TransactionOffline] = 0)
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)	
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)	
	AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) 
		BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, @lTimeZoneParameter, GETDATE())), 0) 
		AND DATEADD(HOUR, @lTimeZoneParameter, GETDATE())

	--RETURN THE VALIDATION 
	IF @lTxNumber > @lNumber
	BEGIN 
		SELECT CAST(0 AS BIT) [Approved],
			   @lNumber [Number],
			   NULL [Amount],
			   @lSymbol [Symbol] 
	END 
	ELSE IF @lTxAmount > @lAmount
	BEGIN
		SELECT CAST(0 AS BIT) [Approved],
			   NULL [Number],
			   @lAmount [Amount],
			   @lSymbol [Symbol]
	END 
	ELSE 
	BEGIN 
		SELECT CAST(1 AS BIT) [Approved],
			   NULL [Number],
			   NULL [Amount],
			   @lSymbol [Symbol]
	END 

    SET NOCOUNT OFF
END