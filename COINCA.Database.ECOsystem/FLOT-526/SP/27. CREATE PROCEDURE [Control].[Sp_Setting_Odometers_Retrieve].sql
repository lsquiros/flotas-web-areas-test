USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_Setting_Odometers_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_Setting_Odometers_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Stefano Quiros
-- Create date: 3/31/2016
-- Description:	Retrieve Setting_Odometers information
-- Modify By: Stefano Quirós - 11/25/2016 - Fix: Inside the Subquery of the Odometer and Liters Last, 
-- i put a validation that ignored the reversed transactions.
-- Modify By: Stefano Quirós - Add the Time Zone Parameter - 12/06/2016
-- Modify: Henry Retana - 12/14/2016
-- Add Odometer Before Last and ShowAlert Validation
-- Modify By: Marco Cabrera
-- Add a validation in case of liters is 0 and cause a division between 0 error
-- Modify: Henry Retana - 06/06/2017
-- Add ExportReport parameter, it will return all the rows for the filters and not only the 100
-- Modify: Henry Retana - 05/09/2017
-- Add Driver Name and driver identification to the retrieve
-- Modify: Henry Retana - 11/09/2017
-- Add second validation
-- Modify: Henry Retana - 19/09/2017
-- Get the driver information
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_Setting_Odometers_Retrieve]
(
	@pCustomerId INT,
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL,
	@pKey VARCHAR(800) = NULL,
	@pUserId INT,
	@pAlert BIT = NULL,
	@pFilterType INT = NULL,
	@pExportReport BIT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	DECLARE @lTimeZoneParameter INT 
		   ,@lParameterO DECIMAL(16,2)
		   ,@lTop BIGINT = 0X7FFFFFFFFFFFFFFF --Hexadecimal number, max number of rows available in sql
		   ,@lOdometerDistance INT
		   ,@lFlag INT

	--Get the parameter information for the customer
	SELECT @lParameterO = [ParameterValue],
		   @lOdometerDistance = [OdometerDifference],
		   @lFlag = [OdometerDifferenceValidation]
	FROM [General].[AlertsByCustomer]
	WHERE [AlertId] = 2 
	  AND [CustomerId] = @pCustomerId

	IF @lFlag <> 1
	BEGIN
		IF @lParameterO IS NULL 
			SET @lParameterO = 0.15
		ELSE
			SET @lParameterO = (@lParameterO/100)

		SET @lOdometerDistance = NULL
	END
	ELSE 
	BEGIN 
		SET @lParameterO = NULL 

		IF @lOdometerDistance IS NULL SET @lOdometerDistance = 1000
	END

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	SET @lTop = CASE WHEN @pExportReport = 0 OR @pExportReport IS NULL THEN 100 ELSE @lTop END

	IF @pAlert = 0
	BEGIN
		SELECT TOP (@lTop)		
			DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date],
			c.customerid,
			v.[Name] [VehicleName],
			v.[PlateId],

			CASE WHEN (SELECT COUNT(*)
					   FROM [Control].[Transactions] t
					   WHERE t.[VehicleId] = v.[VehicleId]
					   AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
					   AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
					   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					   		    FROM [Control].[Transactions] t2
					   		    WHERE t2.[CreditCardId] = t.[CreditCardId] 
					   		    AND t2.[TransactionPOS] = t.[TransactionPOS] 
					   		    AND t2.[ProcessorId] = t.[ProcessorId] 
					   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))) > 2 
			THEN (SELECT TOP 1 TT.[Odometer] 
				  FROM(SELECT TOP 3 NT.* 
					   FROM [Control].[Transactions] NT
					   WHERE NT.[VehicleId] = t.[VehicleId] 
					   AND NT.[Date] <= t.[Date] 
					   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
					   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0) 
					   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					   		    FROM [Control].[Transactions] t2
					   		    WHERE t2.[CreditCardId] = nt.[CreditCardId] 
					   		    AND t2.[TransactionPOS] = nt.[TransactionPOS] 
					   		    AND t2.[ProcessorId] = nt.[ProcessorId] 
					   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
					            ORDER BY NT.[Date] DESC ) TT 
					   ORDER BY TT.[Date] ASC)
			ELSE 0
			END AS [OdometerBeforeLast],

			CASE WHEN (SELECT COUNT(*)
					   FROM [Control].[Transactions] t
					   WHERE t.[VehicleId] = v.[VehicleId]
					   AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
					   AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
					   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					   		    FROM [Control].[Transactions] t2
					   		    WHERE t2.[CreditCardId] = t.[CreditCardId] 
					   		    AND t2.[TransactionPOS] = t.[TransactionPOS] 
					   		    AND t2.[ProcessorId] = t.[ProcessorId] 
					   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))) > 1
			THEN (SELECT TOP 1 TT.[Odometer] 
				  FROM(SELECT TOP 2 NT.* 
					   FROM [Control].[Transactions] NT
					   WHERE NT.[VehicleId] = t.[VehicleId] 
					   AND NT.[Date] <= t.[Date] 
					   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
					   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
					   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					   			FROM [Control].[Transactions] t2
					   			WHERE t2.[CreditCardId] = nt.[CreditCardId] 
					   			AND t2.[TransactionPOS] = nt.[TransactionPOS] 
					   			AND t2.[ProcessorId] = nt.[ProcessorId] 
					   			AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
					            ORDER BY NT.[Date] DESC ) TT 
					  ORDER BY TT.[Date] ASC) 
			ELSE 0
			END AS [OdometerLast],

			t.[Odometer],
			(SELECT TOP 1 TT.[Liters] 
				FROM(SELECT TOP 2 NT.* 
					FROM [Control].[Transactions] NT
					WHERE NT.[VehicleId] = t.[VehicleId] 
					AND NT.[DATE] <= t.[InsertDate] 
					AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
					AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
					AND 1 > (SELECT ISNULL(COUNT(1), 0) 
							 FROM [Control].[Transactions] t2
							 WHERE t2.[CreditCardId] = nt.[CreditCardId] 
							 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
							 AND t2.[ProcessorId] = nt.[ProcessorId] 
							 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
					ORDER BY NT.[Date] DESC ) TT 
					ORDER BY TT.[Date] ASC) AS [LitersLast],	
			t.[Liters],				
			t.[FuelAmount],	
			vc.[DefaultPerformance],	
			t.[TransactionId],
			ISNULL(t.[ShowAlert], 0) [ShowAlert],
			(t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
			   					FROM(SELECT TOP 2 NT.* 
			   						FROM [Control].[Transactions] NT
			   						WHERE NT.[VehicleId] = t.[VehicleId] 
			   						AND NT.[Date] <= t.[Date] 
			   						AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
									AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
									AND 1 > (SELECT ISNULL(COUNT(1), 0) 
											 FROM [Control].[Transactions] t2
											 WHERE t2.[CreditCardId] = nt.[CreditCardId] 
											 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
											 AND t2.[ProcessorId] = nt.[ProcessorId] 
											 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
			   						ORDER BY NT.[Date] DESC ) TT 
			   						ORDER BY TT.[Date] ASC)) [Travel],
			   (CASE WHEN t.[Liters] <> 0 THEN (CONVERT(DECIMAL(12,2), ((t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
														FROM( SELECT TOP 2 NT.* 
															 FROM [Control].[Transactions] NT
															 WHERE NT.[VehicleId] = t.[VehicleId] 
															 AND NT.[Date] <= t.[Date] 
															 AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
															 AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
															 AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																	 FROM [Control].[Transactions] t2
																	 WHERE t2.[CreditCardId] = nt.[CreditCardId] 
																	 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
																	 AND t2.[ProcessorId] = nt.[ProcessorId] 
																	 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
															 ORDER BY NT.[Date] DESC ) TT 
															 ORDER BY TT.[Date] ASC)) / t.[Liters])))
															 ELSE 0 END) [Performance],
			  ISNULL((SELECT TOP 1 u.[Name]
					  FROM [General].[Users] u
					  INNER JOIN [General].[DriversUsers] du
					  	ON u.[UserId] = du.[UserId]
					  WHERE du.[CustomerId] = c.[CustomerId] 
					  AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 0)) [DriverName],
			  ISNULL((SELECT TOP 1 du.[Identification]
					  FROM [General].[Users] u
					  INNER JOIN [General].[DriversUsers] du
					  	ON u.[UserId] = du.[UserId]
					  WHERE du.[CustomerId] = c.[CustomerId] 
					  AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 1)) [DriverIdentification]
		FROM [Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v 
			ON t.[VehicleId] = v.[VehicleId]
		INNER JOIN [General].[Customers] c 
			ON v.[CustomerId] = c.[CustomerId]	
		INNER JOIN [General].[VehicleCategories] vc	
			ON v.[VehicleCategoryId] = vc.[VehicleCategoryId] 				
		WHERE (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
		AND c.[CustomerId] = @pCustomerId 
		AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
		AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				 FROM [Control].[Transactions] t2
				 WHERE t2.[CreditCardId] = t.[CreditCardId] 
				 AND t2.[TransactionPOS] = t.[TransactionPOS] 
				 AND t2.[ProcessorId] = t.[ProcessorId] 
				 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))	
		AND (@pKey IS NULL 
			OR v.[Name] like '%'+@pKey+'%'			
			OR v.[PlateId] like '%'+@pKey+'%')			
		AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear) 
			OR (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
			AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
		AND (@pFilterType IS NULL OR ((@lParameterO IS NOT NULL AND (CASE WHEN t.[Liters] <> 0  
																		 THEN ((t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
																								FROM( SELECT TOP 2 NT.* 
																										FROM [Control].[Transactions] NT
																										WHERE NT.[VehicleId] = t.[VehicleId] 
																										AND NT.[Date] <= t.[Date] 
																										 AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
																										 AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
																										 AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																												 FROM [Control].[Transactions] t2
																												 WHERE t2.[CreditCardId] = nt.[CreditCardId] 
																												 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
																												 AND t2.[ProcessorId] = nt.[ProcessorId] 
																												 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
																										ORDER BY NT.[Date] DESC ) TT 
																										ORDER BY TT.[Date] ASC)) / t.[Liters]) 
																			ELSE 0 END)
																	NOT BETWEEN (vc.[DefaultPerformance] - (vc.[DefaultPerformance] * @lParameterO)) 
																	AND (vc.[DefaultPerformance] + (vc.[DefaultPerformance] * @lParameterO)))
										OR (@lOdometerDistance IS NOT NULL AND ((t.[Odometer] < (SELECT TOP 1 TT.[Odometer] 
																								 FROM(SELECT TOP 2 NT.* 
																									  FROM [Control].[Transactions] NT
																									  WHERE NT.[VehicleId] = t.[VehicleId] 
																									  AND NT.[Date] <= t.[Date] 
																									  AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
																									  AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
																									  AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																												FROM [Control].[Transactions] t2
																												WHERE t2.[CreditCardId] = nt.[CreditCardId] 
																												AND t2.[TransactionPOS] = nt.[TransactionPOS] 
																												AND t2.[ProcessorId] = nt.[ProcessorId] 
																												AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
																										ORDER BY NT.[Date] DESC ) TT 
																								ORDER BY TT.[Date] ASC))
												OR ((t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
																	 FROM(SELECT TOP 2 NT.* 
																	      FROM [Control].[Transactions] NT
																		  WHERE NT.[VehicleId] = t.[VehicleId] 
																		  AND NT.[Date] <= t.[Date] 
																		  AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
																		  AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
																		  AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																					FROM [Control].[Transactions] t2
																					WHERE t2.[CreditCardId] = nt.[CreditCardId] 
																					AND t2.[TransactionPOS] = nt.[TransactionPOS] 
																					AND t2.[ProcessorId] = nt.[ProcessorId] 
																					AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
																			ORDER BY NT.[Date] DESC ) TT 
																			ORDER BY TT.[Date] ASC)) > @lOdometerDistance))))
			 )
		ORDER BY t.[InsertDate] DESC
	END
	ELSE 
	BEGIN 

		;WITH TempTable AS 
		(
			SELECT 
				   DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date],
				   c.CustomerId,
				   v.[Name] [VehicleName],
				   v.[PlateId],
				   t.InsertDate,

				   CASE WHEN (SELECT COUNT(*)
					   FROM [Control].[Transactions] t
					   WHERE t.[VehicleId] = v.[VehicleId]
					   AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
					   AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
					   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					   		    FROM [Control].[Transactions] t2
					   		    WHERE t2.[CreditCardId] = t.[CreditCardId] 
					   		    AND t2.[TransactionPOS] = t.[TransactionPOS] 
					   		    AND t2.[ProcessorId] = t.[ProcessorId] 
					   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))) > 2 
				   THEN (SELECT TOP 1 TT.[Odometer] 
				   	  FROM(SELECT TOP 3 NT.* 
				   		   FROM [Control].[Transactions] NT
				   		   WHERE NT.[VehicleId] = t.[VehicleId] 
				   		   AND NT.[Date] <= t.[Date] 
				   		   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
						   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
				   		   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				   		   		    FROM [Control].[Transactions] t2
				   		   		    WHERE t2.[CreditCardId] = nt.[CreditCardId] 
				   		   		    AND t2.[TransactionPOS] = nt.[TransactionPOS] 
				   		   		    AND t2.[ProcessorId] = nt.[ProcessorId] 
				   		   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   		            ORDER BY NT.[Date] DESC ) TT 
				   		   ORDER BY TT.[Date] ASC)
				   ELSE 0
				   END AS [OdometerBeforeLast],
				   
				   CASE WHEN (SELECT COUNT(*)
				   		   FROM [Control].[Transactions] t
				   		   WHERE t.[VehicleId] = v.[VehicleId]
				   		   AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
						   AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
				   		   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				   		   		    FROM [Control].[Transactions] t2
				   		   		    WHERE t2.[CreditCardId] = t.[CreditCardId] 
				   		   		    AND t2.[TransactionPOS] = t.[TransactionPOS] 
				   		   		    AND t2.[ProcessorId] = t.[ProcessorId] 
				   		   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))) > 1
				   THEN (SELECT TOP 1 TT.[Odometer] 
				   	  FROM(SELECT TOP 2 NT.* 
				   		   FROM [Control].[Transactions] NT
				   		   WHERE NT.[VehicleId] = t.[VehicleId] 
				   		   AND NT.[Date] <= t.[Date] 
				   		   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
						   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
				   		   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				   		   			FROM [Control].[Transactions] t2
				   		   			WHERE t2.[CreditCardId] = nt.[CreditCardId] 
				   		   			AND t2.[TransactionPOS] = nt.[TransactionPOS] 
				   		   			AND t2.[ProcessorId] = nt.[ProcessorId] 
				   		   			AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   		            ORDER BY NT.[Date] DESC ) TT 
				   		  ORDER BY TT.[Date] ASC) 
				   ELSE 0
				   END AS [OdometerLast],

				   t.[Odometer],
				   (SELECT TOP 1 TT.[Liters] 
				   	FROM(SELECT TOP 2 NT.* 
				   		FROM [Control].[Transactions] NT
				   		WHERE NT.[VehicleId] = t.[VehicleId] 
				   		AND NT.[DATE] <= t.[InsertDate] 
				   		AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
						AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
						AND 1 > (SELECT ISNULL(COUNT(1), 0) 
								FROM [Control].[Transactions] t2
								WHERE t2.[CreditCardId] = nt.[CreditCardId] 
								AND t2.[TransactionPOS] = nt.[TransactionPOS] 
								AND t2.[ProcessorId] = nt.[ProcessorId] 
								AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   		ORDER BY NT.[Date] DESC ) TT 
				   		ORDER BY TT.[Date] ASC) AS [LitersLast],	
				   t.[Liters],				
				   t.[FuelAmount],	
				   vc.[DefaultPerformance],	
				   t.[TransactionId],
				   ISNULL(t.[ShowAlert], 0) [ShowAlert],
				   (t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
				   					FROM(SELECT TOP 2 NT.* 
				   						FROM [Control].[Transactions] NT
				   						WHERE NT.[VehicleId] = t.[VehicleId] 
				   						AND NT.[Date] <= t.[Date] 
				   						AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
										AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
										AND 1 > (SELECT ISNULL(COUNT(1), 0) 
												FROM [Control].[Transactions] t2
												WHERE t2.[CreditCardId] = nt.[CreditCardId] 
												AND t2.[TransactionPOS] = nt.[TransactionPOS] 
												AND t2.[ProcessorId] = nt.[ProcessorId] 
												AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   						ORDER BY NT.[Date] DESC ) TT 
				   						ORDER BY TT.[Date] ASC)) [Travel],
				   CASE WHEN @lParameterO IS NOT NULL 
							 THEN  ((t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
													 FROM(
															SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
															WHERE NT.[VehicleId] = t.[VehicleId] 
															AND NT.[Date] <= t.[Date] 
															AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
															AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
															AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																	 FROM [Control].[Transactions] t2
																	 WHERE t2.[CreditCardId] = nt.[CreditCardId] 
																	 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
																	 AND t2.[ProcessorId] = nt.[ProcessorId] 
																	 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
															ORDER BY NT.[Date] DESC ) TT 
													ORDER BY TT.[Date] ASC)) / t.[Liters])
							ELSE (t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
												  FROM(
														SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
														WHERE NT.[VehicleId] = t.[VehicleId] 
														AND NT.[Date] <= t.[Date] 
														AND 1 > (  SELECT ISNULL(COUNT(1), 0) 
					   											FROM [Control].[Transactions] t2
					   											WHERE t2.[CreditCardId] = NT.[CreditCardId] 
					   											AND   t2.[TransactionPOS] = NT.[TransactionPOS] 
					   											AND   t2.[ProcessorId] = NT.[ProcessorId] 
					   											AND   t2.IsReversed = 1
					   										)
														ORDER BY NT.[Date] DESC ) TT 
												 ORDER BY TT.[Date] ASC)) END [Performance],
				  ISNULL((SELECT TOP 1 u.[Name]
						  FROM [General].[Users] u
						  INNER JOIN [General].[DriversUsers] du
						  	ON u.[UserId] = du.[UserId]
						  WHERE du.[CustomerId] = c.[CustomerId] 
						  AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 0)) [DriverName],
				  ISNULL((SELECT TOP 1 du.[Identification]
						  FROM [General].[Users] u
						  INNER JOIN [General].[DriversUsers] du
						  	ON u.[UserId] = du.[UserId]
						  WHERE du.[CustomerId] = c.[CustomerId] 
						  AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 1)) [DriverIdentification]
			FROM [Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v 
					ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c 
					ON v.[CustomerId] = c.[CustomerId]	
				INNER JOIN [General].[VehicleCategories] vc	
					ON v.[VehicleCategoryId] = vc.[VehicleCategoryId] 				
				WHERE(@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
				AND c.[CustomerId] = @pCustomerId 
				AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
				AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
				AND 1 > (SELECT ISNULL(COUNT(1), 0) 
						 FROM [Control].[Transactions] t2
						 WHERE t2.[CreditCardId] = t.[CreditCardId] 
						 AND t2.[TransactionPOS] = t.[TransactionPOS] 
						 AND t2.[ProcessorId] = t.[ProcessorId] 
						 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))			
				AND (t.[ShowAlert] IS NULL OR t.[ShowAlert] = 0)
        )

		SELECT * FROM TempTable t
		WHERE (@lOdometerDistance IS NOT NULL AND ([Performance] < 0 OR [Performance] > @lOdometerDistance))
		OR (@lParameterO IS NOT NULL AND(t.[Performance] NOT BETWEEN (t.[DefaultPerformance] - (t.[DefaultPerformance] * @lParameterO)) 
										   AND (t.[DefaultPerformance] + (t.[DefaultPerformance] * @lParameterO))))
		ORDER BY t.[InsertDate] DESC
	END	

	SET NOCOUNT OFF
END


