USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReportNoServiceStationsByPartner_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsReportNoServiceStationsByPartner_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 07/09/2017
-- Description:	Transactions with no service stations
-- ================================================================================================
-- Modify: Henry Retana - 06/11/2017 - Change the retrieve for the issue 100
-- Modify: Henry Retana - 14/11/2017 - Add Void Transactions Validation
-- Modify: Maria de los Angeles Jimenez Chavarria - Jan/01/2018 - Invoice lenght
-- Modify by Marjorie Garbanzo - 21/05/2019 - Add filter IsDenied to the retrieve
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionsReportNoServiceStationsByPartner_Retrieve]
(
	@pPartnerId INT,	
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL,
	@pCustomerId INT = NULL,
	@pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @lTimeZoneParameter INT 

	SELECT @lTimeZoneParameter = [TimeZone]	
	FROM [General].[Countries] c 
	INNER JOIN [General].[Partners] p
	ON c.[CountryId] = p.[CountryId] 
	WHERE p.[PartnerId] = @pPartnerId

	IF @pCustomerId = -1 SET @pCustomerId = NULL

	IF OBJECT_ID('tempdb..#CustomersByPartner') IS NOT NULL DROP TABLE #CustomersByPartner
	IF OBJECT_ID('tempdb..#TransactionsReports') IS NOT NULL DROP TABLE #TransactionsReports

	CREATE TABLE #CustomersByPartner 
	(
		[Id] INT IDENTITY,
		[CustomerId] INT
	)

	CREATE TABLE #TransactionsReports 
	(
		 [CustomerName] VARCHAR(250)
		,[CreditCardId] INT
		,[CreditCardNumber] NVARCHAR(520)
		,[Invoice] VARCHAR(20) NULL
		,[MerchantDescription] VARCHAR(75) NULL
		,[SystemTraceCode] VARCHAR(250) NULL
		,[HolderName] VARCHAR(250) NULL
		,[Date] DATETIME
		,[FuelName] VARCHAR(50)
		,[FuelAmount] DECIMAL(16, 2)
		,[Odometer] INT
		,[Liters] DECIMAL(16, 2)
		,[PlateId] VARCHAR(10)
		,[CurrencySymbol] NVARCHAR(50) NULL
		,[State] VARCHAR(250) NULL
		,[CostCenterName] VARCHAR(250) NULL
		,[TerminalId] VARCHAR(20) NULL
		,[VehicleName] VARCHAR(50) NULL
		,[CountryName] VARCHAR(250) NULL
		,[ExchangeValue] NVARCHAR(50)
		,[RealAmount] DECIMAL(16, 2)
		,[IsInternational] BIT NULL
	)	

	INSERT INTO #CustomersByPartner
	SELECT [CustomerId]
	FROM [General].[CustomersByPartner]
	WHERE [PartnerId] = @pPartnerId
	AND (@pCustomerId IS NULL OR [CustomerId] = @pCustomerId)

	DECLARE @TempID INT

	WHILE EXISTS (SELECT * 
				  FROM #CustomersByPartner)
	BEGIN
		SELECT TOP 1 @pCustomerId = [CustomerId],
					 @TempID = [Id]
		FROM #CustomersByPartner

		DELETE FROM #CustomersByPartner 
		WHERE [Id] = @TempID

		DECLARE @pIssueForId INT

		SET @pIssueForId = (SELECT [IssueForId]
						    FROM [General].[Customers]
							WHERE [CustomerId] = @pCustomerId)

		IF @pIssueForId = 100
		BEGIN
			INSERT INTO #TransactionsReports
			SELECT c.[Name] [CustomerName]
				  ,t.[CreditCardId] [CreditCardId]
				  ,cr.[CreditCardNumber] AS [CreditCardNumber]
				  ,ISNULL(t.[Invoice], '-') AS [Invoice]
				  ,'' AS [MerchantDescription]
				  ,t.[TransactionPOS] AS [SystemTraceCode]
				  ,v.[PlateId] [HolderName]
				  ,t.[Date] [Date]
				  ,f.[Name] [FuelName]
				  ,t.[FuelAmount] [FuelAmount]
				  ,t.[Odometer] [Odometer]
				  ,t.[Liters] [Liters]
				  ,v.[PlateId] [PlateId]
				  ,g.[Symbol] [CurrencySymbol]
				  ,(CASE WHEN t.[IsReversed] = 1 AND t.[IsVoid] = 1 THEN 'Reversada' 
						 WHEN t.[IsDuplicated] = 1 THEN 'Duplicada' 
						 WHEN t.[IsFloating] = 1 THEN 'Flotante'
						 WHEN t.[IsReversed] = 0 AND t.[IsVoid] = 1 THEN 'Anulada'
						 ELSE 'Procesada' END) AS [State]
				  ,e.[Name] AS [CostCenterName]
				  ,t.[ProcessorId] AS [TerminalId]
				  ,v.[Name] AS [VehicleName]
				  ,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
				  					  FROM [General].[Countries] coun
				  					  WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName]
				  ,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
				  		WHEN t.[ExchangeValue] IS NOT NULL 
				  		THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
				  					(SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
				  					FROM [Control].[ExchangeRates] er
				  					INNER JOIN [Control].[Currencies] cue 
				  						ON er.[CurrencyCode] = cue.[CurrencyCode]
				  					INNER JOIN [General].[Countries] coun 
				  						ON coun.[CountryId] = c.[CountryId]
				  					WHERE cue.[Code] = coun.[Code]
				  						AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
				  							OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
				  	END) AS [ExchangeValue]
				  ,(CASE WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL 
					     THEN [Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
				  		 WHEN t.[IsInternational] = 0 
						 THEN t.[FuelAmount]
				  	ELSE t.FuelAmount END) AS [RealAmount]
				  ,ccc.[InternationalCard] [IsInternational]
			FROM [Control].[Transactions] t
			INNER JOIN [General].[Vehicles] v 
				ON t.[VehicleId] = v.[VehicleId]
			INNER JOIN [General].[Customers] c 
				ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN [General].[CustomerCreditCards] ccc 
				ON ccc.[CustomerId] = c.[CustomerId]
			INNER JOIN [Control].[Fuels] f 
				ON t.[FuelId] = f.[FuelId]
			INNER JOIN [Control].[CreditCardByDriver] cd 
				ON t.[CreditCardId] = cd.[CreditCardId]
			INNER JOIN [Control].[CreditCard] cr 
				ON cd.CreditCardId = cr.CreditCardId
			INNER JOIN [General].[Users] u 
				ON cd.[UserId] = u.[UserId]
			INNER JOIN [Control].[Currencies] g 
				ON c.[CurrencyId] = g.[CurrencyId]
			INNER JOIN [General].[VehicleCostCenters] e 
				ON t.CostCenterId = e.CostCenterId			
			LEFT OUTER JOIN [General].[Countries] co 
				ON t.[CountryCode] = co.[Code]			
			WHERE c.[CustomerId] = @pCustomerId
			AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)
		    AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)
			AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					 FROM [Control].[Transactions] t2
					 WHERE t2.[CreditCardId] = t.[CreditCardId]
					 AND t2.[TransactionPOS] = t.[TransactionPOS] 
					 AND t2.[ProcessorId] = t.[ProcessorId] 
					 AND (t2.IsReversed = 1 OR t2.IsVoid = 1))
			AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL
				  AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
				  AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear)
				 and (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
					 AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
			AND t.[ProcessorId] NOT IN (SELECT [TerminalId]
										FROM [General].[TerminalsByServiceStations])
			ORDER BY t.[Date] DESC
		END
		ELSE
		BEGIN			
			INSERT INTO #TransactionsReports
			SELECT c.[Name] [CustomerName]
				,t.[CreditCardId] [CreditCardId]
				,cc.[CreditCardNumber] AS [CreditCardNumber]
				,ISNULL(t.[Invoice], '-') AS [Invoice]
				,'' AS [MerchantDescription]
				,t.[TransactionPOS] AS [SystemTraceCode]
				,v.[PlateId] [HolderName]
				,t.[Date] [Date]
				,f.[Name] [FuelName]
				,t.[FuelAmount] [FuelAmount]
				,t.[Odometer] [Odometer]
				,t.[Liters] [Liters]
				,v.[PlateId] [PlateId]
				,g.[Symbol] [CurrencySymbol]
				,(CASE WHEN t.[IsReversed] = 1 AND t.[IsVoid] = 1 THEN 'Reversada' 
					   WHEN t.[IsDuplicated] = 1 THEN 'Duplicada' 
					   WHEN t.[IsFloating] = 1 THEN 'Flotante'
					   WHEN t.[IsReversed] = 0 AND t.[IsVoid] = 1 THEN 'Anulada'
					   ELSE 'Procesada' END) AS [State]
				,e.[Name] AS [CostCenterName]
				,t.[ProcessorId] AS [TerminalId]
				,v.[Name] AS [VehicleName]
				,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
						FROM [General].[Countries] coun
						WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName]
				,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
						WHEN t.[ExchangeValue] IS NOT NULL 
						THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
									(SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
									FROM [Control].[ExchangeRates] er
									INNER JOIN [Control].[Currencies] cue 
										ON er.[CurrencyCode] = cue.[CurrencyCode]
									INNER JOIN [General].[Countries] coun 
										ON coun.[CountryId] = c.[CountryId]
									WHERE cue.[Code] = coun.[Code]
										AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
											OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
					END) AS [ExchangeValue]
				,(CASE 
					WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL THEN 
					[Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
					WHEN t.[IsInternational] = 0 THEN
					t.FuelAmount
					ELSE t.FuelAmount END) AS [RealAmount]
				,ccc.[InternationalCard] [IsInternational]
			FROM [Control].[Transactions] t
			INNER JOIN [General].[Vehicles] v 
				ON t.[VehicleId] = v.[VehicleId]
			INNER JOIN [General].[Customers] c 
				ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN [General].[CustomerCreditCards] ccc 
				ON ccc.[CustomerId] = c.[CustomerId]
			INNER JOIN [Control].[Fuels] f 
				ON t.[FuelId] = f.[FuelId]
			INNER JOIN [Control].[CreditCardByVehicle] cv 
				ON t.[CreditCardId] = cv.[CreditCardId]
			INNER JOIN [Control].[Currencies] g 
				ON c.[CurrencyId] = g.[CurrencyId]
			INNER JOIN [Control].[CreditCard] cc 
				ON cc.CreditCardId = cv.CreditCardId
			INNER JOIN [General].[VehicleCostCenters] e 
				ON t.CostCenterId = e.CostCenterId				
			LEFT OUTER JOIN [General].[Countries] co 
				ON t.[CountryCode] = co.[Code]
			WHERE c.[CustomerId] = @pCustomerId
		    AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)
		    AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)
			AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					 FROM [Control].[Transactions] t2
					 WHERE t2.[CreditCardId] = t.[CreditCardId]
					 AND t2.[TransactionPOS] = t.[TransactionPOS] 
					 AND t2.[ProcessorId] = t.[ProcessorId] 
					 AND (t2.IsReversed = 1 OR t2.IsVoid = 1))
			AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL
				  AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
				  AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear)
				 and (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
					 AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
			AND t.[ProcessorId] NOT IN (SELECT [TerminalId]
										FROM [General].[TerminalsByServiceStations])
			ORDER BY t.[Date] DESC	
		END
	END

	SET NOCOUNT OFF
	SELECT * 
	FROM #TransactionsReports

	DROP TABLE #TransactionsReports
	DROP TABLE #CustomersByPartner	
END

