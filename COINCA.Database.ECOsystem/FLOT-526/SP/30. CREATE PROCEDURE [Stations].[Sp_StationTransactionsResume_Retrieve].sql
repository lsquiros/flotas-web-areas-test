USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Stations].[Sp_StationTransactionsResume_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Stations].[Sp_StationTransactionsResume_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Marco Cabrera
-- Create date: 06/04/2017
-- Description:	Get the resume info of the transactions in a station service
-- Modify by Henry Retana - 29/9/2017
-- Add new function to get the service station info
-- Modify by Marjorie Garbanzo - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================

CREATE PROCEDURE [Stations].[Sp_StationTransactionsResume_Retrieve]
(
	@pServiceStationId INT = NULL,
	@pXMLServiceStationIds VARCHAR(MAX) = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL	
)
AS
BEGIN

	SET NOCOUNT ON	
		
	DECLARE @lxmlData XML = CONVERT(XML, @pXMLServiceStationIds)
	CREATE TABLE #ServiceStations 
	(
		[ServiceStationId] INT
	)

	--Valida si el id de la estacion de servicio viene individual o de forma masiva
	IF @pServiceStationId IS NULL
	BEGIN
		INSERT INTO #ServiceStations
		SELECT Row.col.value('./@ServiceStationId', 'INT') 
		FROM @lxmlData.nodes('/xmldata/ServiceStations') Row(col)
	END
	ELSE
	BEGIN
		INSERT INTO #ServiceStations
		VALUES ( @pServiceStationId )
	END	

	SELECT   v.CustomerId as Customers
			,COUNT(1) AS Quantity
			,SUM(t.FuelAmount) AS Amount
			,cu.Symbol as CurrencySymbol
	INTO #Results
	FROM [Control].[Transactions] t
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]
	LEFT OUTER JOIN [General].[Countries] co
		ON t.[CountryCode] = co.[Code]
	LEFT OUTER JOIN [Control].[Currencies] cu
		ON co.[CurrencyId] = cu.CurrencyId
	WHERE (@pStartDate IS NOT NULL 
		   AND @pEndDate IS NOT NULL
		   AND DATEADD(HOUR, co.[TimeZone], t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate)
		   AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)
		   AND t.[ProcessorId] IN (SELECT tss.[TerminalId]
							FROM [General].[ServiceStations] ss
							INNER JOIN [General].[TerminalsByServiceStations] tss
								ON ss.[ServiceStationId] = tss.[ServiceStationId]
							WHERE ss.[ServiceStationId] IN (SELECT [ServiceStationId]
															FROM #ServiceStations))
	GROUP BY v.CustomerId, cu.Symbol

	SELECT	COUNT(1) AS Customers
			,SUM(Quantity) AS Quantity
			,SUM(Amount) AS Amount
			,CurrencySymbol
	FROM #Results 
	GROUP BY CurrencySymbol

	DROP TABLE #ServiceStations
	DROP TABLE #Results
	SET NOCOUNT OFF
END

