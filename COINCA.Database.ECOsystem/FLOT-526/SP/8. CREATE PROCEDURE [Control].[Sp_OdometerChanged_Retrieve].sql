USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_OdometerChanged_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_OdometerChanged_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:	 Albert Estrada
-- Create date: set/28/2017
-- Description:	Retrieve Setting_Odometer Changed information
-- Modify By: Stefano Quirós -- Change te VehicleId to VehicleName
-- Stefano Quirós - Add filters to the retrieve - 21/05/2019
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_OdometerChanged_Retrieve]
(
	@pCustomerId INT,
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL 
)
AS
BEGIN
	SET NOCOUNT ON

	SELECT 
			v.[VehicleId]  AS [Vehiculo]
			,v.[PlateId] AS [Placa]
			,CONVERT (char(10), t.[Date], 103) AS [FechaTransaccion]
			,convert(char(8), t.[Date], 108) AS [HoraTransaccion]
			,CONVERT (char(10), t.ModifyDate, 103) AS [FechaCambio]
			,convert(char(8), t.ModifyDate, 108) AS [HoraCambio]
			,t.[Odometer] AS [OdometroFinal]
			,h.[Odometer] AS [OdometroOriginal]
			,u.[Name] as [UsuarioEncripted]
			,'Desde ' + CONVERT (char(10), @pStartDate, 103) + ' Hasta '+ CONVERT (char(10), @pEndDate, 103) AS [Encabezado]
	 FROM	[Control].[Transactions]  t
			INNER JOIN [Control].[TransactionsHx] h 
				ON h.[TransactionId] = t.[TransactionId]
			INNER JOIN [General].[Vehicles] v 
				ON v.[VehicleId] = t.[VehicleId]
			LEFT JOIN [General].[users] u 
				ON u.[UserId] = t.[ModifyUserId]
	WHERE (t.[TransactionOffline] IS NULL OR t.[TransactionOffline] = 0)
		  AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)	
	      AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)	
		  AND 1 > (
				   SELECT ISNULL(COUNT(1), 0)
				   FROM [Control].[Transactions] t2
				   WHERE t2.[CreditCardId] = t.[CreditCardId]
				   AND t2.[TransactionPOS] = t.[TransactionPOS]
				   AND t2.[ProcessorId] = t.[ProcessorId]
				   AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)
			      )
		  AND v.CustomerID = @pCustomerId AND h.[Odometer] <> t.[Odometer]  AND 
			(
				(@pYear IS NOT NULL AND @pMonth IS NOT NULL AND DATEPART(m,t.[Date]) = @pMonth AND DATEPART(yyyy,t.[Date]) = @pYear) 
				OR 
				(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL AND t.[Date] BETWEEN @pStartDate AND @pEndDate)
			)
 
	SET NOCOUNT OFF
END

 