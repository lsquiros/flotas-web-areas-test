USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_NoShowAlert_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_NoShowAlert_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 15/12/2016
-- Description:	No Show Alerts Add or Edit
-- Modify by Marjorie Garbanzo - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_NoShowAlert_AddOrEdit]
(
	@pXmlDataNoShowAlert VARCHAR(MAX)
	,@pXmlDataShowAlert VARCHAR(MAX)
	,@pCustomerId INT
	,@pUserId INT	
)
AS
BEGIN
	SET NOCOUNT ON	
	
	--**************** No Alert ****************
	DECLARE @lxmlData XML = CONVERT(XML, @pXmlDataNoShowAlert)
	
	UPDATE [Control].[Transactions]
	SET [ShowAlert] = 1
	WHERE [TransactionId] IN (SELECT Row.col.value('./@TransactionId', 'INT') 
							  FROM @lxmlData.nodes('/xmldata/NoAlert') ROW(col))
		   AND ([IsDenied] = 0 OR [IsDenied] IS NULL)

	--**************** Show Alert ****************
	SET @lxmlData = CONVERT(XML, @pXmlDataShowAlert)

	UPDATE [Control].[Transactions]
	SET [ShowAlert] = 0
	WHERE [TransactionId] IN (SELECT Row.col.value('./@TransactionId', 'INT') 
							  FROM @lxmlData.nodes('/xmldata/Alert') ROW(col))
		   AND ([IsDenied] = 0 OR [IsDenied] IS NULL)

	--UPDATE THE COUNT FOR THE SETTING ODOMETERS	
	EXEC [Control].[Sp_SettingOdometerAlert_Count] @pCustomerId, @pUserId

	SET NOCOUNT OFF
END

