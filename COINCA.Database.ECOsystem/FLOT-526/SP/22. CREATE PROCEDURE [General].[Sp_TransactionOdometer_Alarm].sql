USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_TransactionOdometer_Alarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_TransactionOdometer_Alarm]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 03/09/2015
-- Description:	Stored Procedure that validate odometer transaction and sent informative email
-- Modify: Henry Retana 12/7/2016
-- Change the way the range is created
-- Modify By: Stefano Quirós - Add the Time Zone Parameter - 12/06/2016
-- Modify: Henry Retana 8/2/2017
-- Validates if the email must be send
-- Modify By:	Marco Cabrera 19-06-2017
-- Add a validation in the odometer to ignore the reverse transactions
-- Modify: Henry Retana 13/10/2017
-- Validates if the vehicle has previous transactions
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_TransactionOdometer_Alarm]
(
	@pVehicleId INT,
	@pOdometer FLOAT,
	@pLiters FLOAT,
	@pRuleId INT
)
AS
BEGIN 

	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRY

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT
		DECLARE @lLastOdometer FLOAT
		DECLARE @lMaxOdometer FLOAT
		DECLARE @lMinOdometer FLOAT
		DECLARE @RowNum INT
		DECLARE @Message VARCHAR(MAX)
		DECLARE @PlateId VARCHAR(MAX)
		DECLARE @Email VARCHAR(MAX)
		DECLARE @SetAlarm INT
		DECLARE @lValue VARCHAR(10) 
		DECLARE @lStart DECIMAL(4,2) 
		DECLARE @lEnd DECIMAL(4,2)
		DECLARE @lPerOdometer FLOAT
		DECLARE @lTimeZoneParameter INT 
		DECLARE @lCustomerId INT
		DECLARE @lSendEmail BIT

		SELECT @SetAlarm = 0

		SELECT @lSendEmail = [TransactionRuleOdometerSendEmail]
		FROM [General].[Parameters]
		
		--GET THE PARAMETERS
		SELECT @lValue = [value]		 
		FROM [General].[ParametersByCustomer] p
		INNER JOIN [General].[Vehicles] v
			ON p.[CustomerId] = v.[CustomerId]
		WHERE v.[VehicleId] = @pVehicleId 
		AND p.[Name] = (SELECT r.[RuleName]
						FROM [Control].[TransactionsRules] r
						WHERE r.[Id] = @pRuleId)

		SELECT @lStart = CONVERT(DECIMAL(4,2), SUBSTRING(@lValue, 1, CHARINDEX('-', @lValue, 1) -1))/100,
			   @lEnd = CONVERT(DECIMAL(4,2), SUBSTRING(@lValue, CHARINDEX('-', @lValue, 1) +1, LEN(@lValue)))/100

		--GET THE LAST ODOMETERS
		IF EXISTS (SELECT TOP 1 ISNULL([Odometer], 0) [TrxReportedOdometer]
				   FROM [Control].[Transactions] NT		
				   WHERE [VehicleId] = @pVehicleId
				   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
				   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
				   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
							 FROM [Control].[Transactions] t2
							 WHERE t2.[CreditCardId] = nt.[CreditCardId] 
							 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
							 AND t2.[ProcessorId] = nt.[ProcessorId] 
							 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   ORDER BY [InsertDate] DESC)
		BEGIN 
			SELECT @lLastOdometer = ISNULL([a].[TrxReportedOdometer], 0)
			FROM (SELECT TOP 1 ISNULL([Odometer], 0) [TrxReportedOdometer]
				  FROM [Control].[Transactions]	NT		
				  WHERE [VehicleId] = @pVehicleId
				  AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
				  AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
				  AND 1 > (SELECT ISNULL(COUNT(1), 0) 
							 FROM [Control].[Transactions] t2
							 WHERE t2.[CreditCardId] = nt.[CreditCardId] 
							 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
							 AND t2.[ProcessorId] = nt.[ProcessorId] 
							 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				  ORDER BY [InsertDate] DESC) [a]
		END
		ELSE 
			SELECT @lLastOdometer = 0
			
		-- Calculate the expected odometer based in the performance
		SELECT	@lPerOdometer = (@pLiters * c.[DefaultPerformance])									  
		FROM [General].[Vehicles] b 
		INNER JOIN [General].[VehicleCategories] c 
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		WHERE b.[VehicleId] = @pVehicleId

		--GET THE MIN AND MAX ODOMETER
		SELECT @lMinOdometer = @lLastOdometer + @lPerOdometer - (@lStart * @lPerOdometer)
		SELECT @lMaxOdometer = @lLastOdometer + @lPerOdometer + (@lEnd * @lPerOdometer)
			
		IF @lLastOdometer = 0 SET @pOdometer = (@lMinOdometer + @lMaxOdometer) / 2

		--VALIDATES IF THE ODOMETER IS WITHING THE RANGE	
		IF @pOdometer <= @lMinOdometer
		BEGIN
			SELECT @SetAlarm = 1
		END
		ELSE
		BEGIN
			IF @pOdometer > @lMaxOdometer 
			BEGIN
				SELECT @SetAlarm = 2
			END
		END
		
		--***** Alarm 	
		IF @SetAlarm >= 1 AND @lSendEmail = 1
		BEGIN
			DECLARE @tEmails TABLE
			(
				[Id] INT IDENTITY(1,1) NOT NULL,
				[PlateId] VARCHAR(10),
				[Email] VARCHAR(MAX),
				[CustomerId] INT
			)

			--CARGA LA TABLA CON EL LISTADO DE CORREOS QUE SE DEBEN ENVIAR
			INSERT INTO @tEmails 
			(
				[PlateId],
				[Email],
				[CustomerId]
			)
			SELECT [a].[PlateId], 
				   [d].[Email],
				   [a].[CustomerId]
			FROM [General].[Vehicles] [a]
			INNER JOIN [General].[CustomerUsers] [b]	
				ON [a].[CustomerId] = [b].[CustomerId]
			INNER JOIN [General].[Users] [c] 
				ON [b].[UserId] = [c].[UserId]
			INNER JOIN [AspNetUsers] [d] 
				ON [c].[AspNetUserId] = [d].[Id]
			INNER JOIN [AspNetUserRoles] [e] 
				ON [d].[Id] = [e].[UserId]
			INNER JOIN [AspNetRoles] [f] 
			ON [e].[RoleId] = [f].[Id]
			WHERE [f].[Name] = 'CUSTOMER_ADMIN' 
			AND [a].[VehicleId] = @pVehicleId		
		
			SELECT @RowNum = Count(*) From @tEmails
				
			SELECT @Message = [a].[Message] 
			FROM [General].[Values] [a] 
			INNER JOIN [General].[Types] [b] 
				ON [a].[TypeId] = [b].[TypeId]
			WHERE [b].[Code] = 'CUSTOMER_ALARM_TRIGGER_ODOMETER_TRANSACTION'
		
			WHILE @RowNum > 0
			BEGIN
				SELECT	@PlateId = [PlateId],
						@Email = [Email],
						@lCustomerId = [CustomerId]
				FROM @tEmails 
				WHERE Id = @RowNum
		
				SELECT @lTimeZoneParameter = [TimeZone]	
				FROM [General].[Countries] co
				INNER JOIN [General].[Customers] cu
					ON co.[CountryId] = cu.[CountryId] 
				WHERE cu.[CustomerId] = @lCustomerId

				DECLARE @lMessage VARCHAR(MAX) = @Message
				SET @lMessage = REPLACE(@lMessage, '%PlateId%', @PlateId)
				SET @lMessage = REPLACE(@lMessage, '%LastOdometer%', @lLastOdometer)
				SET @lMessage = REPLACE(@lMessage, '%Odometer%', @pOdometer)
				SET @lMessage = REPLACE(@lMessage, '%time%', CONVERT(VARCHAR(10), DATEADD(Hour, @lTimeZoneParameter,GETUTCDATE()), 111) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @lTimeZoneParameter,GETUTCDATE()), 108))
		
				INSERT INTO [General].[EmailEncryptedAlarms]
				(
					[To],
					[Subject],
					[Message],
					[Sent],
					[InsertDate]
				)
				VALUES
				(
					@Email,
					'Alarma de Inconsistencias en Odómetro',
					@lMessage,
					0,
					GETUTCDATE()
				)
		
				SET @RowNum = @RowNum - 1
			END
		END

		SELECT @SetAlarm AS [SetAlarm],
			   @pOdometer AS [Odometer],
			   @lMinOdometer AS [MinOdometer],
			   @lMaxOdometer AS [MaxOdometer]			   	
		
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
    SET NOCOUNT OFF
    SET XACT_ABORT OFF

END

