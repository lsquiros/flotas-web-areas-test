USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByVehicleGroup_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicleGroup_Retrieve] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- ================================================================================================  
 -- Author:  Berman Romero L.  
 -- Create date: 10/21/2014  
 -- Description: Retrieve Current Fuels Report By Vehicle Group information  
 -- Dinamic Filter - 1/22/2015 - Melvin Salas
 -- Export to Excel - 11/08/2017 - Esteban Solis  
 -- Modify by: Stefano Quiros 13/04/2016 - 14/11/2017 - Add Void Transactions Validation
-- Modify by Marjorie Garbanzo - 21/05/2019 - Add filter IsDenied to the retrieve
 -- ================================================================================================   
 CREATE PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicleGroup_Retrieve]  
 (  
   @pCustomerId INT     --@pCustomerId: Customer Id  
  ,@pYear INT = NULL     --@pYear: Year  
  ,@pMonth INT = NULL     --@pMonth: Month  
  ,@pStartDate DATETIME = NULL  --@pStartDate: Start Date  
  ,@pEndDate DATETIME = NULL   --@pEndDate: End Date  
  ,@pUserId INT      --@pUserId: User Id  
 )  
 AS  
 BEGIN  
   
  --DECLARE @pCustomerId INT = 59   
  --DECLARE @pYear INT = 2017   
  --DECLARE @pMonth INT = 11 
  --DECLARE @pStartDate DATETIME = NULL   
  --DECLARE @pEndDate DATETIME = NULL   
  --DECLARE @pUserId INT = 7097  
  
  SET NOCOUNT ON   
   
  DECLARE @lCurrencySymbol NVARCHAR(4) = ''  
  
  -- DYNAMIC FILTER  
  DECLARE @Results TABLE (items INT)  
  DECLARE @count INT  
  INSERT @Results  
  EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId  
  SET  @count = (SELECT COUNT(*) FROM @Results)  
  -- END  
   
  SELECT  
   @lCurrencySymbol = a.Symbol  
  FROM [Control].[Currencies] a  
   INNER JOIN [General].[Customers] b  
    ON a.[CurrencyId] = b.[CurrencyId]  
  WHERE b.[CustomerId] = @pCustomerId  
   
  SELECT	a.VehicleGroupId,
			a.[Name] AS [VehicleGroupName]  
			,ISNULL(t.[RealAmount],0) AS [RealAmount]  
			,s.[AssignedAmount]  
			,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / NULLIF(s.[AssignedAmount],0), 2)*100) AS [RealAmountPct]  
			,@lCurrencySymbol AS [CurrencySymbol]  
  FROM		[General].[VehicleGroups] a  
  LEFT		 
  JOIN		(
				SELECT	z.[VehicleGroupId]  
						,SUM(x.[FuelAmount]) AS [RealAmount]  
				FROM	[Control].[Transactions] x  
				JOIN	[General].[Vehicles] y	ON x.[VehicleId] = y.[VehicleId]  
				JOIN	[General].[VehiclesByGroup] z ON z.VehicleId = y.VehicleId  
				WHERE	y.[CustomerId] = @pCustomerId  
				AND		(@count = 0 OR x.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER  
				AND		x.[IsFloating] = 0  
				AND x.[IsReversed] = 0  
				AND x.[IsDuplicated] = 0  
				AND (x.[IsFloating] IS NULL OR x.[IsFloating] = 0) 
				AND 1 > (  
							SELECT	COUNT(1) 
							FROM	Control.Transactions t2  
							WHERE	t2.[CreditCardId] = x.[CreditCardId] 
							AND		t2.[TransactionPOS] = x.[TransactionPOS] 
							AND		t2.[ProcessorId] = x.[ProcessorId] 
							AND	   (t2.IsReversed = 1 OR t2.IsVoid = 1) 
						)  
				AND		(
							(
								@pYear IS NOT NULL 
							AND @pMonth IS NOT NULL   
							AND DATEPART(m,x.[Date]) = @pMonth  
							AND DATEPART(yyyy,x.[Date]) = @pYear
							) 
						OR	(
								@pStartDate IS NOT NULL 
							AND @pEndDate IS NOT NULL  
							AND x.[Date] BETWEEN @pStartDate AND @pEndDate
							)
						)            
				GROUP 
				BY		z.[VehicleGroupId]
			) t	ON a.[VehicleGroupId] = t.[VehicleGroupId]  
   JOIN		(
				SELECT	z.[VehicleGroupId]  
						,SUM(x.[Amount]) AS [AssignedAmount]
						,SUM(x.[Liters]) AS [AssignedLiters]    
				FROM	[Control].[FuelDistribution] x  
				JOIN	[General].[Vehicles] y	ON x.[VehicleId] = y.[VehicleId]  
				JOIN	[General].[VehiclesByGroup] z ON z.VehicleId = y.VehicleId  
				WHERE	y.[CustomerId] = @pCustomerId  
				AND		(
							@count = 0 
						OR x.[VehicleId] IN	(
												SELECT	items 
												FROM	@Results
											)
						) -- DYNAMIC FILTER  
				AND		(
							(
								@pYear IS NOT NULL 
							AND @pMonth IS NOT NULL   
							AND x.[Month] = @pMonth  
							AND x.[Year] = @pYear
							) 
						OR	(
								@pStartDate IS NOT NULL 
							AND @pEndDate IS NOT NULL  
							AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)  
							AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)
							)
						)  
				GROUP 
				BY				z.[VehicleGroupId]
			) s ON a.[VehicleGroupId] = s.[VehicleGroupId]     
  SET NOCOUNT OFF  
 END

