USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_ApprovalTransaction_Count]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_ApprovalTransaction_Count]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Melvin Salas
-- Create date: April 8, 2016
-- Description:	Get count transactions by criteria
-- Modify 21/10/2016 - Henry Retana - Modify the table for the terminals
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- =============================================

CREATE PROCEDURE [Control].[Sp_ApprovalTransaction_Count]
(
	@pInvoice		VARCHAR(250),
	@pBacId			VARCHAR(100),
	@pCustomerId	INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	
	-- COSTCENTER TABLE FILTER
	DECLARE @tCostCenters TABLE 
	(
		[CostCenterId] INT
	)
	
	INSERT INTO @tCostCenters
	SELECT DISTINCT a.[CostCenterId]
	FROM [General].[VehicleCostCenters] a
	INNER JOIN [General].[VehicleUnits] b	
		ON a.[UnitId] = b.[UnitId]		
	WHERE b.[CustomerId] = @pCustomerId

	-- COUNTER
	SELECT	COUNT(*) 
	FROM [Control].[Transactions] t
	INNER JOIN @tCostCenters tcc 
	ON tcc.[CostCenterId] = t.[CostCenterId]
	WHERE t.[ProcessorId] = @pBacId
	AND t.[Invoice] = @pInvoice	
	AND t.[IsReversed] = 0 
	AND t.[IsFloating] = 0
	AND t.[IsDuplicated] = 0 
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)

END
