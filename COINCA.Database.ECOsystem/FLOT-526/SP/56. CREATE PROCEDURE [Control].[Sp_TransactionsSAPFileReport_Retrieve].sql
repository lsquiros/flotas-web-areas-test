USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsSAPFileReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsSAPFileReport_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 11/08/2017
-- Description:	Retrieve Transactions SAP File Report information
-- Modify: Henry Retana - 04/09/2017
-- Add the SAP Prov for the customer
-- Modify 09/28/2017 - Esteban Solis - Modifications related to serviceStationsCustomName table
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_TransactionsSAPFileReport_Retrieve]
(	
	@pMonth INT = NULL,
	@pYear INT = NULL,	
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL,
	@pCustomerId INT,	
	@pUserId INT 
)
AS
BEGIN

	SET NOCOUNT ON	
	
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	DECLARE @lTimeZoneParameter INT

	-- DYNAMIC FILTER
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	
	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId
	
	SELECT CONVERT(VARCHAR(100), [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 1)) AS [SAPProv],
		   CONVERT(VARCHAR(100),(SELECT TOP 1 ss2.legal_id 
		   						   FROM [General].[ServiceStations] ss2 
		   						   WHERE ss2.[ServiceStationId] = tss.[ServiceStationId])) AS [LegalId],
		   CASE  
				WHEN COALESCE(t.[MerchantDescription],'') = ''
				  THEN [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0)
				ELSE t.[MerchantDescription] 
			END AS [ServiceStation],
		   DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [InvoiceDate],
		   t.[Invoice],
		   t.[FuelAmount] [Amount],
		   e.[Name] [CostCenter],
		   vu.[Name] [Unit],
		   v.[Name] [Vehicle],
		   v.[PlateId]
	FROM [Control].[Transactions] t
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]	
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]		
	INNER JOIN [General].[VehicleCostCenters] e
	    ON t.CostCenterId = e.CostCenterId
	INNER JOIN [General].[VehicleUnits] vu
		ON e.[UnitId] = vu.[UnitId] 
	LEFT OUTER JOIN [General].[ServiceStations] ss
		ON t.[ProcessorId] = ss.[Terminal]
	LEFT OUTER JOIN [General].[TerminalsByServiceStations] tss
		ON t.[ProcessorId] = tss.[TerminalId]	
	WHERE (@count = 0 OR v.[VehicleId] IN (SELECT items 
										   FROM @Results)) 
	AND c.[CustomerId] = @pCustomerId 
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
	AND 1 > (SELECT ISNULL(COUNT(1), 0) 
			 FROM [Control].[Transactions] t2
			 WHERE t2.[CreditCardId] = t.[CreditCardId] 
			 AND t2.[TransactionPOS] = t.[TransactionPOS] 
			 AND t2.[ProcessorId] = t.[ProcessorId] 
			 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))			
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear) 
			OR (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
	ORDER BY t.[Date] DESC

	SET NOCOUNT OFF
END


