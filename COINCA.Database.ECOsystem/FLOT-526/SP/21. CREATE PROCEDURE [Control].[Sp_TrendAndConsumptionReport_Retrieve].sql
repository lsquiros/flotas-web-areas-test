USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TrendAndConsumptionReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TrendAndConsumptionReport_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Andrs Oviedo Brenes.
-- Create date: 22/01/2015
-- Description:	Retrieve TrendAndConsumptionReport information
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- Modify By: Stefano Quiros 12/04/2016
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_TrendAndConsumptionReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pKey VARCHAR(800) = NULL			--@pKey: Search Key
	,@pUserId INT						--@pUserId: User Id
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
		
	SELECT	@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
	INNER JOIN [General].[Customers] b
		ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
		
	SELECT	 a.[VehicleId]
			,a.[PlateId]
			,a.[Name] AS [VehicleName]
			,MAX(b.TrxReportedLiters) AS [MaxConsumption]
			,AVG(b.TrxReportedLiters) AS [AvgConsumption]
			,Min(b.TrxReportedLiters) AS [MinConsumption]
			,@lCurrencySymbol as [CurrencySymbol]
			,SUM(b.TrxReportedLiters) AS [TotalConsumption]
			,SUM(c.[FuelAmount]) AS [TotalFuelAmount]
	FROM [General].[Vehicles] a
	INNER JOIN [General].[PerformanceByVehicle] b
		ON a.[VehicleId] = b.[VehicleId]
	INNER JOIN [Control].[Transactions] c WITH(READUNCOMMITTED)
		ON b.[TransactionId] = c.[TransactionId]
	WHERE a.[CustomerId] = @pCustomerId
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,b.[TrxDate]) = @pMonth
						AND DATEPART(yyyy,b.[TrxDate]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND b.[TrxDate] BETWEEN @pStartDate AND @pEndDate))
	AND (@pKey IS NULL 
		OR a.[PlateId] like '%'+@pKey+'%')			
	AND (c.[IsFloating] IS NULL OR c.[IsFloating] = 0) 
	AND (c.[IsDenied] IS NULL OR c.[IsDenied] = 0)
	AND 1 > (SELECT ISNULL(COUNT(1), 0) 
			 FROM [Control].[Transactions] t2
			 WHERE t2.[CreditCardId] = c.[CreditCardId] 
			 AND t2.[TransactionPOS] = c.[TransactionPOS] 
			 AND t2.[ProcessorId] = c.[ProcessorId] 
			 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))		
	AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	GROUP BY a.[VehicleId]
			,a.[PlateId]
			,a.[Name]
		
	SET NOCOUNT OFF
END

