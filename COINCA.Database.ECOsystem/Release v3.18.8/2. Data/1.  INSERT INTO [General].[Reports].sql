USE [ECOSystemdev]
GO

-- ================================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Add Report to MasterCard Available
-- ================================================

 INSERT INTO [General].[Reports]
 (
	[ReportName]
   ,[DownloadName]
   ,[InsertDate]
   ,[IsPartner]
   ,[SheetName]
   ,[Module]
   ,[SpName]
 )
 VALUES
 (
	'MasterCardAvailableReport'
   ,'Reporte de consumo Tarjetas Maestras'
   ,GETDATE()
   ,1
   ,'Reporte de Consumo Tarjetas Maestras'
   ,'P'
   ,'[General].[Sp_MasterCardsAvailable_Retrieve]'
 )