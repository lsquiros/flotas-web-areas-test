/****** Object:  StoredProcedure [General].[Sp_GeneralParameterByPartner_Retrieve]    Script Date: 4/21/2020 1:30:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 18/11/2016
-- Description:	Retrieve parameters for the partner
-- ================================================================================================
-- Modify by Henry Retana - 14/02/2018 - Add Preauthorized Days
-- Modify by Henry Retana - 18/06/2018 - Add the prices edit
-- Modify by Henry Retana - 07/08/2018 - Add Contract Information
-- Modify by Maria de los Angeles Jimenez Chavarria - 02/Nov/2018 - Add StationTolerance, CreditCardNumber and Amount in the select stament.
-- Modify by Maria de los Angeles Jimenez Chavarria - 11/Dic/2018 - Add CardAlarmEmail in the select stament.
-- Modify by Gerald Solano - 06/08/2019 - Add column called PilotModeNotificationEmail for the Pilote Mode.
-- Modify by Henry Retana - 29/07/2019 - Cancel Credit card parameters
-- Modify by Stefano Quirós - 2/12/2019 - Expire CreditCardParameters
-- ================================================================================================

ALTER PROCEDURE [General].[Sp_GeneralParameterByPartner_Retrieve]
(	
	 @pPartnerId INT	
)
AS
BEGIN	
	SET NOCOUNT ON	
	
	DECLARE @lXmlResult VARCHAR(MAX) = NULL,
			@lSMSPriceId INT = 0

	SELECT @lSMSPriceId = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'CUSTOMER_PRICE_SMS'

	EXEC [General].[Sp_CustomerPrices_Retrieve] NULL, @pPartnerId, @lSMSPriceId, @pXMLResult = @lXmlResult OUTPUT

	SELECT g.[Id]
		  ,g.[AlarmDays]
		  ,g.[RTN]
		  ,g.[PhoneNumber]
		  ,g.[Address]
		  ,g.[BacService]
		  ,g.[BacName]
		  ,g.[StationTolerance]
		  ,g.[CardAlarmEmail]
		  ,g.[PreauthorizedDays]	
		  ,g.[PreauthorizedEmail]
		  ,@lXmlResult [SMSPriceXML]
		  ,CAST(ISNULL(p.[SMSInternalModem], 1) AS BIT) [SMSInternalModem]
		  ,g.[ContractYears]
		  ,g.[SelectAllVehicles]
		  ,g.[ContractAlertDays]
		  ,s.[CreditCardNumber]
		  ,s.[Amount]
		  ,g.[PilotModeNotificationEmail]
		  ,g.[CancelCreditCardNumber]
		  ,g.[CancelCreditCardEmails]
		  ,g.[CancelCreditCardActive]
		  ,g.[ExpireCreditCardBefore] [AlertBefore]
          ,g.[ExpireCreditCardPeriodicity] [Periodicity]
          ,g.[ExpireCreditCardNameDay] [NameDay]
          ,g.[ExpireCreditCardDay] [NameDate]
          ,g.[ExpireCreditCardRepeat] [Repeat]
          ,g.[ExpireCreditCardEmails]
          ,g.[ExpireCreditCardActive]
		  ,g.[MasterCardEmails]
		  ,g.[MasterCardAlertActive]
		  ,g.[MasterCardPercent]
		  ,g.[MasterCardAlertPeriodicity]
		  ,g.[MasterCardAlertNameDay]
		  ,g.[MasterCardAlertNameDate]
		  ,g.[MasterCardAlertRepeat]
	FROM [General].[GeneralParametersByPartner] g
	INNER JOIN [General].[Partners] p 
		ON g.[PartnerId] = p.[PartnerId]
	LEFT JOIN [General].[ServiceStationsPOSCheckParameters] s
		ON p.[PartnerId] = s.[PartnerId]
	WHERE g.[PartnerId] = @pPartnerId

    SET NOCOUNT OFF
END

