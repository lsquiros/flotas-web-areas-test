/****** Object:  StoredProcedure [Control].[Sp_ValidateAvailable]    Script Date: 6/10/2020 5:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================
-- Author:		Stefano Quirós
-- Create date: March 06, 2018
-- Description:	Update Available of the CreditCard and return true or false
-- =========================================================================

ALTER PROCEDURE [Control].[Sp_ValidateAvailable] 
(	
    @pDriverId	INT = NULL
   ,@pAssigned DECIMAL(16,2)
   ,@pRealAmount DECIMAL(16,2)
   ,@pTotalAssigned DECIMAL(16,2)
   ,@pTotal DECIMAL(16, 2)
   ,@pAvailable DECIMAL(16,2)
   ,@pCreditCardId INT
   ,@pResult INT = NULL OUTPUT
)
AS
BEGIN
SET NOCOUNT ON;
	BEGIN TRY	
	
		IF (@pAssigned < @pRealAmount)
		BEGIN
			SELECT @pResult = 50003
		END
		ELSE IF @pDriverId IS NULL AND (@pTotalAssigned > @pTotal)  
		BEGIN
			SELECT @pResult = 50002
		END
		ELSE IF (@pAvailable < 0)
		BEGIN
			SELECT @pResult = 50004
		END

		IF @pResult IS NULL
		BEGIN
			SELECT @pResult = 0
		END

		RETURN @pResult
	END TRY
	BEGIN CATCH
		SELECT @pResult = ERROR_NUMBER()

		RETURN @pResult
	END CATCH
END

