USE [ECOSystemDev]
GO
IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_ClassifficationTypes_Retrieve' AND TYPE = 'P')
	DROP PROC [General].[Sp_ClassifficationTypes_Retrieve]
GO
-- ===================================================
-- Author:		Stefano Quir�s
-- Create date: 27/04/2020
-- Description:	Retrieve Classiffication Types
-- ===================================================
CREATE PROCEDURE [General].[Sp_ClassifficationTypes_Retrieve]
AS
BEGIN	
	SET NOCOUNT ON	

		SELECT [Id]
		      ,[Name]
		FROM [General].[ClassificationType]
		WHERE [Active] = 1

	SET NOCOUNT OFF
END