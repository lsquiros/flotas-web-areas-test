/****** Object:  StoredProcedure [Control].[Sp_GeneralParametersByPartner_AddOrEdit]    Script Date: 4/21/2020 12:54:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 18/11/2016
-- Description:	Insert or Update General Parameters By Partner
-- ================================================================================================
-- Modify by Henry Retana - 11/12/2017 - Add Service Station Parameters
-- Modify by Henry Retana - 14/02/2018 - Add Preauthorized Days
-- Modify by Henry Retana - 18/06/2018 - Add the prices edit
-- Modify by Henry Retana - 07/08/2018 - Add Contract Information
-- Modify by Maria de los Angeles Jimenez Chavarria - 02/Nov/2018 - Include @pStationTolerance parameter.
-- Modify by Maria de los Angeles Jimenez Chavarria - 11/Dic/2018 - Add CardAlarmEmail in the select stament.
-- Modify by Gerald Solano - 06/08/2019 - Add PilotModeNotificationEmail column for Pilote Mode Process 
-- Modify by Henry Retana - 24/07/2019 - Add cancel credit card alert
-- Modify By Stefano Quirós - 2/12/2019 - Add the new notificaction of ExpireCreditCards
-- ================================================================================================

ALTER PROCEDURE [Control].[Sp_GeneralParametersByPartner_AddOrEdit]
(
	 @pId INT = NULL
	,@pAlarmDays INT = NULL
	,@pRTN VARCHAR(100) = NULL
	,@pPhoneNumber VARCHAR(100) = NULL
	,@pAddress VARCHAR(MAX) = NULL
	,@pBacService VARCHAR(100) = NULL
	,@pBacName VARCHAR(200) = NULL
	,@pPartnerId INT = NULL
	,@pStationTolerance INT = NULL
	,@pCreditCardNumber VARCHAR(500) = NULL
	,@pAmount DECIMAL(16, 2) = NULL
	,@pActive BIT = NULL
	,@pCardAlarmEmail VARCHAR(MAX) = NULL
	,@pPreauthorizedDays INT = NULL
	,@pPreauthorizedEmail VARCHAR(MAX) = NULL
	,@pUserId INT
	,@pSMSPriceXML VARCHAR(MAX) = NULL
	,@pSMSInternalModem BIT = NULL
	,@pContractYears INT = NULL
	,@pSelectAllVehicles BIT = NULL
    ,@pContractAlertDays INT = NULL
    ,@pPilotModeNotificationEmail VARCHAR(MAX) = NULL
	,@pCancelCreditCardNumber INT = NULL
	,@pCancelCreditCardEmails VARCHAR(1500) = NULL
    ,@pCancelCreditCardActive INT = NULL
	,@pAlertBefore INT = NULL
	,@pPeriodicity INT = NULL
	,@pNameDay INT = NULL
	,@pNameDate INT = NULL
	,@pRepeat INT = NULL
	,@pExpireCreditCardEmails VARCHAR(5000) = NULL
	,@pExpireCreditCardActive BIT = NULL
	,@pMasterCardEmails VARCHAR(1500) = NULL
	,@pMasterCardAlertActive BIT = NULL
	,@pMasterCardPercent INT = NULL
	,@pMasterCardAlertPeriodicity INT = NULL
    ,@pMasterCardAlertNameDay INT = NULL
    ,@pMasterCardAlertNameDate INT = NULL
	,@pMasterCardAlertRepeat INT = NULL
)
AS
BEGIN	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT                        
			   ,@lSMSPriceId INT = 0

		SELECT @lSMSPriceId = [Value]
		FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'CUSTOMER_PRICE_SMS'

		DECLARE @lReportId INT = (SELECT TOP 1 [Id] 
								  FROM [General].[Reports]
								  WHERE [ReportName] = 'ExpireCreditCardReport')

		DECLARE @lReportIdMasterCard INT = (SELECT TOP 1 [Id] 
								  FROM [General].[Reports]
								  WHERE [ReportName] = 'MasterCardAvailableReport')
			
		--USE INTERNAL MODEM
		UPDATE  [General].[Partners]
		SET [SMSInternalModem] = @pSMSInternalModem
		WHERE [PartnerId] = @pPartnerId

		--PARAMETERS FOR SERVICE STATION VALIDATION
		IF @pCreditCardNumber IS NOT NULL 
			AND @pAmount IS NOT NULL 			  			   
		BEGIN
			DECLARE @lMasterCardId INT = NULL

			SELECT @lMasterCardId = [CustomerCreditCardId]
			FROM [Control].[CreditCard] cc
			INNER JOIN [General].[CustomerCreditCards] c
				ON cc.[CustomerId] = c.[CustomerId]
			WHERE cc.[CreditCardNumber] = @pCreditCardNumber
					
			IF @lMasterCardId IS NULL  RAISERROR ('La tarjeta no existe en Flotas.', 16, 1)

			IF EXISTS(SELECT *
						FROM [General].[ServiceStationsPOSCheckParameters]
						WHERE [PartnerId] = @pPartnerId)
			BEGIN
				UPDATE [General].[ServiceStationsPOSCheckParameters]
				SET [CreditCardNumber] = @pCreditCardNumber,
					[MasterCardId] = @lMasterCardId,
					[Amount] = @pAmount,
					[IsActive] = @pActive,
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				WHERE [PartnerId] = @pPartnerId
			END
			ELSE
			BEGIN
				INSERT INTO [General].[ServiceStationsPOSCheckParameters]
				(
					[PartnerId], 
					[CreditCardNumber],	
					[MasterCardId],
					[Amount],
					[IsActive],
					[InsertUserId],
					[InsertDate]
				)
				VALUES
				(
					@pPartnerId, 
					@pCreditCardNumber,
					@lMasterCardId,
					@pAmount,
					@pActive,
					@pUserId,
					GETDATE()
				)
			END
		END
		ELSE 
		BEGIN
			IF EXISTS(SELECT *
						FROM [General].[ServiceStationsPOSCheckParameters]
						WHERE [PartnerId] = @pPartnerId)
			BEGIN
				UPDATE [General].[ServiceStationsPOSCheckParameters]
				SET [IsActive] = 0,
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				WHERE [PartnerId] = @pPartnerId
			END
		END

		--REMOVE SPACES IN EMAIL 
		SET @pCancelCreditCardEmails = REPLACE(@pCancelCreditCardEmails, ' ', '')

		IF @pId IS NULL
		BEGIN
			INSERT INTO [General].[GeneralParametersByPartner]
			(
				[PartnerId],
				[InsertDate],
				[AlarmDays],
				[RTN],
				[PhoneNumber],
				[Address],
				[BacService],
				[BacName],
				[StationTolerance],
				[CardAlarmEmail],
				[PreauthorizedDays],
				[PreauthorizedEmail],
				[ContractYears],
				[SelectAllVehicles],
				[ContractAlertDays],
				[CancelCreditCardNumber],
				[CancelCreditCardEmails],
				[CancelCreditCardActive],
				[PilotModeNotificationEmail],
				[ExpireCreditCardBefore],
				[ExpireCreditCardPeriodicity],
				[ExpireCreditCardNameDay],
				[ExpireCreditCardDay],
				[ExpireCreditCardRepeat],
				[ExpireCreditCardEmails],
				[ExpireCreditCardActive],
				[MasterCardEmails],
				[MasterCardAlertActive],
				[MasterCardPercent],
				[MasterCardAlertPeriodicity],
				[MasterCardAlertNameDay],
				[MasterCardAlertNameDate],
				[MasterCardAlertRepeat]
			)
			VALUES	
			(	
				@pPartnerId,					
				GETUTCDATE(),
				@pAlarmDays,
				@pRTN,
				@pPhoneNumber,
				@pAddress,
				@pBacService,
				@pBacName,
				@pStationTolerance,
				@pCardAlarmEmail,
				@pPreauthorizedDays,
				@pPreauthorizedEmail,
				@pContractYears,
				@pSelectAllVehicles,
				@pContractAlertDays,
				@pCancelCreditCardNumber,
				@pCancelCreditCardEmails,
				@pCancelCreditCardActive,
				@pPilotModeNotificationEmail,
				@pAlertBefore,
				@pPeriodicity,
				@pNameDay,
				@pNameDate,
				@pRepeat,
				@pExpireCreditCardEmails,
				@pExpireCreditCardActive,
				@pMasterCardEmails,
				@pMasterCardAlertActive,
				@pMasterCardPercent,
				@pMasterCardAlertPeriodicity,
				@pMasterCardAlertNameDay,
				@pMasterCardAlertNameDate,
				@pMasterCardAlertRepeat
			)

			--CreditCardExpire Alert			
			IF (@pExpireCreditCardEmails IS NOT NULL)
			BEGIN
				IF EXISTS(SELECT * 
						  FROM [General].[SendProgramReports] 
						  WHERE [ReportId] = @lReportId 
						        AND [PartnerId] = @pPartnerId)
				BEGIN
					UPDATE [General].[SendProgramReports]
					SET [Emails] = @pExpireCreditCardEmails
					   ,[Elapse] = @pPeriodicity
					   ,[Days] = CASE WHEN @pPeriodicity = 7 
								 THEN @pNameDay
								 ELSE @pNameDate
								 END
					   ,[Active] = @pExpireCreditCardActive
					   ,[ModifyDate] = GETDATE()
					WHERE [ReportId] = @lReportId 
						  AND [PartnerId] = @pPartnerId

				END
				ELSE
				BEGIN
					INSERT INTO [General].[SendProgramReports]
					(
						[ReportId]
					   ,[Emails]
					   ,[PartnerId]
					   ,[Elapse]
					   ,[Days]
					   ,[Active]
					   ,[InsertDate]
					   ,[InsertUserId]
					   ,[Module]
					)
					VALUES
					(
						@lReportId
					   ,@pExpireCreditCardEmails
					   ,@pPartnerId
					   ,@pPeriodicity
					   ,CASE WHEN @pPeriodicity = 7 
					    THEN @pNameDay
						ELSE @pNameDate
						END
					   ,@pExpireCreditCardActive
					   ,GETDATE()
					   ,@pUserId
					   ,'P'
					)
				END
			END	

			--MasterCardAvailable Alert			
			IF (@pMasterCardEmails IS NOT NULL)
			BEGIN
				IF EXISTS(SELECT * 
						  FROM [General].[SendProgramReports] 
						  WHERE [ReportId] = @lReportIdMasterCard 
						        AND [PartnerId] = @pPartnerId)
				BEGIN
					UPDATE [General].[SendProgramReports]
					SET [Emails] = @pMasterCardEmails
					   ,[Elapse] = @pMasterCardAlertPeriodicity
					   ,[Days] = CASE WHEN @pMasterCardAlertPeriodicity = 7 
								 THEN @pMasterCardAlertNameDay
								 ELSE @pMasterCardAlertNameDate
								 END
					   ,[Active] = @pMasterCardAlertActive
					   ,[ModifyDate] = GETDATE()
					WHERE [ReportId] = @lReportIdMasterCard 
						  AND [PartnerId] = @pPartnerId

				END
				ELSE
				BEGIN
					INSERT INTO [General].[SendProgramReports]
					(
						[ReportId]
					   ,[Emails]
					   ,[PartnerId]
					   ,[Elapse]
					   ,[Days]
					   ,[Active]
					   ,[InsertDate]
					   ,[InsertUserId]
					   ,[Module]
					)
					VALUES
					(
						@lReportIdMasterCard
					   ,@pMasterCardEmails
					   ,@pPartnerId
					   ,@pMasterCardAlertPeriodicity
					   ,CASE WHEN @pMasterCardAlertPeriodicity = 7 
					    THEN @pMasterCardAlertNameDay
						ELSE @pMasterCardAlertNameDate
						END
					   ,@pMasterCardAlertActive
					   ,GETDATE()
					   ,@pUserId
					   ,'P'
					)
				END
			END						
		END
		ELSE
		BEGIN				
			UPDATE [General].[GeneralParametersByPartner]
			SET [InsertDate] = GETUTCDATE(),
				[AlarmDays] = @pAlarmDays,
				[RTN] = @pRTN,
				[PhoneNumber] = @pPhoneNumber,
				[Address] = @pAddress,
				[BacService] = @pBacService,
				[BacName] = @pBacName,
				[StationTolerance] = @pStationTolerance,
				[CardAlarmEmail] = @pCardAlarmEmail,
				[PreauthorizedDays] = @pPreauthorizedDays,
				[PreauthorizedEmail] = @pPreauthorizedEmail,
				[ContractYears] = @pContractYears,
				[SelectAllVehicles] = @pSelectAllVehicles,
				[ContractAlertDays] = @pContractAlertDays,
				[CancelCreditCardNumber] = @pCancelCreditCardNumber,
				[CancelCreditCardEmails] = @pCancelCreditCardEmails,
				[CancelCreditCardActive] = @pCancelCreditCardActive,
				[PilotModeNotificationEmail] = @pPilotModeNotificationEmail,
				[ExpireCreditCardBefore] = @pAlertBefore,
				[ExpireCreditCardPeriodicity] = @pPeriodicity,
				[ExpireCreditCardNameDay] = @pNameDay,
				[ExpireCreditCardDay] = @pNameDate,
				[ExpireCreditCardRepeat] = @pRepeat,
				[ExpireCreditCardEmails] = @pExpireCreditCardEmails,
				[ExpireCreditCardActive] = @pExpireCreditCardActive,
				[MasterCardEmails] = @pMasterCardEmails,	
				[MasterCardAlertActive] = @pMasterCardAlertActive,
				[MasterCardPercent] = @pMasterCardPercent,
				[MasterCardAlertPeriodicity] = @pMasterCardAlertPeriodicity,				
				[MasterCardAlertNameDay] = @pMasterCardAlertNameDay,
				[MasterCardAlertNameDate] = @pMasterCardAlertNameDate,
				[MasterCardAlertRepeat] = @pMasterCardAlertRepeat
			WHERE [PartnerId] = @pPartnerId
			AND [Id] = @pId	
			
			IF (@pExpireCreditCardEmails IS NOT NULL)
			BEGIN
				
				IF EXISTS(SELECT * 
						  FROM [General].[SendProgramReports] 
						  WHERE [ReportId] = @lReportId 
						        AND [PartnerId] = @pPartnerId)
				BEGIN
					UPDATE [General].[SendProgramReports]
					SET [Emails] = @pExpireCreditCardEmails
					   ,[Elapse] = @pPeriodicity
					   ,[Days] = CASE WHEN @pPeriodicity = 7 
								 THEN @pNameDay
								 ELSE @pNameDate
								 END
					   ,[Active] = @pExpireCreditCardActive
					   ,[ModifyDate] = GETDATE()
					WHERE [ReportId] = @lReportId 
						  AND [PartnerId] = @pPartnerId

				END
				ELSE
				BEGIN
					INSERT INTO [General].[SendProgramReports]
					(
						[ReportId]
					   ,[Emails]
					   ,[PartnerId]
					   ,[Elapse]
					   ,[Days]
					   ,[Active]
					   ,[InsertDate]
					   ,[InsertUserId]
					   ,[Module]
					)
					VALUES
					(
						@lReportId
					   ,@pExpireCreditCardEmails
					   ,@pPartnerId
					   ,@pPeriodicity
					   ,CASE WHEN @pPeriodicity = 7 
					    THEN @pNameDay
						ELSE @pNameDate
						END
					   ,@pExpireCreditCardActive
					   ,GETDATE()
					   ,@pUserId
					   ,'P'
					)
				END
			END	
			
			--MasterCardAvailable Alert			
			IF (@pMasterCardEmails IS NOT NULL)
			BEGIN
				IF EXISTS(SELECT * 
						  FROM [General].[SendProgramReports] 
						  WHERE [ReportId] = @lReportIdMasterCard 
						        AND [PartnerId] = @pPartnerId)
				BEGIN
					UPDATE [General].[SendProgramReports]
					SET [Emails] = @pMasterCardEmails
					   ,[Elapse] = @pMasterCardAlertPeriodicity
					   ,[Days] = CASE WHEN @pMasterCardAlertPeriodicity = 7 
								 THEN @pMasterCardAlertNameDay
								 ELSE @pMasterCardAlertNameDate
								 END
					   ,[Active] = @pMasterCardAlertActive
					   ,[ModifyDate] = GETDATE()
					WHERE [ReportId] = @lReportIdMasterCard 
						  AND [PartnerId] = @pPartnerId

				END
				ELSE
				BEGIN
					INSERT INTO [General].[SendProgramReports]
					(
						[ReportId]
					   ,[Emails]
					   ,[PartnerId]
					   ,[Elapse]
					   ,[Days]
					   ,[Active]
					   ,[InsertDate]
					   ,[InsertUserId]
					   ,[Module]
					)
					VALUES
					(
						@lReportIdMasterCard
					   ,@pMasterCardEmails
					   ,@pPartnerId
					   ,@pMasterCardAlertPeriodicity
					   ,CASE WHEN @pMasterCardAlertPeriodicity = 7 
					    THEN @pMasterCardAlertNameDay
						ELSE @pMasterCardAlertNameDate
						END
					   ,@pMasterCardAlertActive
					   ,GETDATE()
					   ,@pUserId
					   ,'P'
					)
				END
			END							
		END		
            
		--INSERT DATA FOR SMS PRICES
		EXEC [General].[Sp_CustomerRangePrices_AddOrEdit] NULL, @pPartnerId, @lSMSPriceId, @pSMSPriceXML, @pUserId

		COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
