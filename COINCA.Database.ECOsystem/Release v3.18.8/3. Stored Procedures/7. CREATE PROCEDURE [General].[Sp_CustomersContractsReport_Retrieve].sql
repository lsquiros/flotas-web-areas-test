USE [ECOSystem]
GO
IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_CustomersContractsReport_Retrieve' AND TYPE = 'P')
	DROP PROC [General].[Sp_CustomersContractsReport_Retrieve]
GO
-- ===================================================
-- Author:		Stefano Quir�s
-- Create date: 27/04/2020
-- Description:	Retrieve Customers Contracts Retrieve
-- ===================================================
CREATE PROCEDURE [General].[Sp_CustomersContractsReport_Retrieve] --1037, -1, 1, 2020, null, null
(
	@pPartnerId INT
   ,@pClassification INT = NULL
   ,@pMonth INT = NULL
   ,@pYear INT = NULL
   ,@pStartDate DATETIME = NULL
   ,@pEndDate DATETIME = NULL
)
AS
BEGIN	
	SET NOCOUNT ON	

	SET @pClassification = NULLIF(@pClassification, -1)

	SELECT DISTINCT MIN(p.[Name]) [PartnerName]
				   ,MIN(c.[Name]) [CustomerName]
				   ,asp.[Email]
				   ,ca.[LegalDocument]
				   ,ca.[MainPhone]
				   ,ca.[SecundaryPhone]
				   ,ca.[SupportTicket]
				   ,v.[PlateId]
				   ,v.[AVL]
				   ,s.[NumeroCelular] [CellPhone]
				   ,v.[Chassis]
				   ,v.[OperationBAC]
				   ,MIN(c.[InsertDate]) [CreateDate]
				   ,ISNULL(MIN(ct.[Name]), 'Sin Clasificaci�n') [Classiffication]
				   ,MAX([Since]) [Since]
				   ,MAX([To]) [To]	 
				   ,CASE WHEN MAX([Since]) IS NULL AND MAX([To]) IS NULL 
						 THEN 'Sin Contrato'
						 WHEN GETDATE() > MAX([To])
						 THEN 'Contrato Vencido'
						 ELSE 'Contrato Activo'
						 END [ContractStatus]
				   ,CASE WHEN v.[DeviceReference] IS NOT NULL 
						 THEN 'Si'
					     ELSE 'No'
				   END [HasGPS]
				  ,ISNULL((SELECT COUNT(1) 
						   FROM [General].[CustomerContracts] 
						   WHERE [CustomerId] = c.[CustomerId]
						   GROUP BY CustomerId), 0) [Renovations]
					,ISNULL((SELECT TOP 1 'Contrato Activo'
							 FROM [General].[CustomerContractsByVehicle] ccv			 
							 INNER JOIN [General].[CustomerContracts] cc
								ON ccv.[CustomerContractsId] = cc.[Id]
							 WHERE cc.[Active] = 1 
							 AND ccv.[VehicleId] = v.[VehicleId] 
							 AND GETDATE() BETWEEN [Since] AND [To]), 'Contrato Inactivo') [VehicleContractStatus]	
					,ISNULL(@pStartDate, datefromparts(@pYear, @pMonth, 1)) [StartDate]
					,ISNULL(@pEndDate, EOMONTH(datefromparts(@pYear, @pMonth, 1))) [EndDate]
					,CONVERT(VARCHAR(20), CONVERT(DATE, (SELECT MAX([Since])
														 FROM [General].[CustomerContracts] 
														 WHERE [CustomerId] = c.[CustomerId]
														 GROUP BY [CustomerId]))) [LastRenovation]
					,CONVERT(VARCHAR(20), CONVERT(DATE, (SELECT MAX([ModifyDate])
														 FROM [General].[CustomerContracts] 
														 WHERE [CustomerId] = c.[CustomerId]
														       AND [Active] = 0
														 GROUP BY [CustomerId]))) [LastCancelation]
	FROM [General].[Customers] c
	LEFT JOIN [General].[CustomerContracts] cc
		ON cc.[CustomerId] = c.[CustomerId]
		   AND [Active] = 1
	LEFT JOIN [General].[CustomerAdditionalInformation] ca
		ON ca.[CustomerId] = c.[CustomerId]
	LEFT JOIN [General].[Vehicles] v
		ON v.[CustomerId] = c.[CustomerId]
	INNER JOIN [General].[CustomersByPartner] cp
		ON c.[CustomerId] = CP.[CustomerId]
	INNER JOIN [General].[Countries] coun
		ON coun.[CountryId] = c.[CountryId]
	INNER JOIN [General].[Partners] p
		ON p.[PartnerId] = cp.[PartnerId]
	INNER JOIN [General].[CustomerUsers] cu
		ON cu.[CustomerId] = c.[CustomerId]
	INNER JOIN [General].[Users] u
		ON u.[UserId] = cu.[UserId]
	INNER JOIN [AspNetUsers] asp
		ON asp.[Id] = u.[AspNetUserId]
	LEFT JOIN [Composiciones] co
		ON co.[Vehiculo] = v.[IntrackReference]
	LEFT JOIN [Sims] s
		ON s.[Sim] = co.[Sim]
	LEFT JOIN [General].[VehicleClassification] vc
		ON vc.[VehicleId] = v.[VehicleId]
	LEFT JOIN [General].[ClassificationType] ct
		ON ct.[Id] = vc.[ClassificationId]
	WHERE p.[PartnerId] = @pPartnerId
		  AND (@pClassification IS NULL
			   OR ct.[Id] = @pClassification)	
	      AND (
					(
						@pYear IS NOT NULL
						AND @pMonth IS NOT NULL
						AND DATEPART(m, DATEADD(HOUR, coun.[TimeZone], [Since])) = @pMonth
						AND DATEPART(yyyy, DATEADD(HOUR, coun.[TimeZone], [Since])) = @pYear
						)
					OR (
						@pStartDate IS NOT NULL
						AND @pEndDate IS NOT NULL
						AND DATEADD(HOUR, coun.[TimeZone], [Since]) >= @pStartDate
						AND DATEADD(HOUR, coun.[TimeZone], [Since]) <= @pEndDate
						)
					) 

	AND (c.[Isdeleted] = 0 
		 OR c.[IsDeleted] IS NULL)
	AND c.[IsActive] = 1
	GROUP BY c.[CustomerId]
			,asp.[Email]
			,v.[PlateId]
			,v.[AVL]
			,v.[Chassis]
			,s.[NumeroCelular]
			,v.[DeviceReference]
			,v.[VehicleId]
			,ca.[LegalDocument]
			,ca.[MainPhone]
			,ca.[SecundaryPhone]
			,ca.[SupportTicket]
			,v.[OperationBAC]

SET NOCOUNT OFF
END