USE [ECOSystem]
GO

-- ================================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Add Parameters for MasterCard Alert
-- ================================================

ALTER TABLE [General].[GeneralParametersByPartner]
ADD [MasterCardEmails] VARCHAR(1500)

ALTER TABLE [General].[GeneralParametersByPartner]
ADD [MasterCardAlertActive] BIT

ALTER TABLE [General].[GeneralParametersByPartner]
ADD [MasterCardPercent] INT

ALTER TABLE [General].[GeneralParametersByPartner]
ADD [MasterCardAlertPeriodicity] INT

ALTER TABLE [General].[GeneralParametersByPartner]
ADD [MasterCardAlertNameDay] INT

ALTER TABLE [General].[GeneralParametersByPartner]
ADD [MasterCardAlertNameDate] INT

ALTER TABLE [General].[GeneralParametersByPartner]
ADD [MasterCardAlertRepeat] INT

ALTER TABLE [General].[GeneralParametersByPartner]
ADD [LastMasterCardAlertNotificationSent] DATE