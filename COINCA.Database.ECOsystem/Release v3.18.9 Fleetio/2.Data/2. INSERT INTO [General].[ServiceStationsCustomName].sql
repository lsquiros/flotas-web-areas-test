USE [ECOSystem]
GO

-- ========================================================
-- Author:		Stefano Quir�s
-- Create date: 09/06/2020
-- Description:	Insert Service Stations that have FleetioId
-- ========================================================

INSERT INTO [General].[ServiceStationsCustomName] 
(
	[ServiceStationId]
   ,[Name]
   ,[CustomerId]
   ,[InsertDate]
   ,[FleetioId]
)
SELECT [ServiceStationId]
      ,[Name]
	  ,11761
	  ,GETDATE()
	  ,[FleetioId]
FROM [General].[ServiceStations] 
WHERE [FleetioId] IS NOT NULL