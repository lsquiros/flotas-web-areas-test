/****** Object:  StoredProcedure [General].[Sp_ServiceStationsFleetio_AddOrEdit]    Script Date: 6/9/2020 9:07:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Update Vehicles set FleetioId
-- ================================================================================================
 ALTER PROCEDURE [General].[Sp_ServiceStationsFleetio_AddOrEdit]
 (
	@pServiceStationId INT
   ,@pServiceStationFleetioId BIGINT
   ,@pCustomerId INT
 )
 AS
 BEGIN	
	SET NOCOUNT ON	

    IF EXISTS(SELECT * 
			  FROM [General].[ServiceStationsCustomName]
			  WHERE [ServiceStationId] = @pServiceStationId
				    AND [CustomerId] = @pCustomerId)
	BEGIN
		UPDATE [General].[ServiceStationsCustomName]
		SET [FleetioId] = @pServiceStationFleetioId
		WHERE [ServiceStationId] = @pServiceStationId
		      AND [CustomerId] = @pCustomerId
	END
	ELSE
	BEGIN
		INSERT INTO [General].[ServiceStationsCustomName] 
		(
			[ServiceStationId]
		   ,[Name]
		   ,[CustomerId]
		   ,[InsertDate]
		   ,[FleetioId]
		)
		SELECT [ServiceStationId]
		      ,[Name]
			  ,@pCustomerId
			  ,GETDATE()
			  ,@pServiceStationFleetioId
		FROM [General].[ServiceStations] 
		WHERE [ServiceStationId] = @pServiceStationId
	END

    SET NOCOUNT OFF
 END
