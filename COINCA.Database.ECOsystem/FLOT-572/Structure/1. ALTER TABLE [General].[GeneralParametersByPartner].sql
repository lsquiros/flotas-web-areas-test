USE [ECOsystemDev]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 24/07/2019
-- Description:	Alter table [General].[GeneralParametersByPartner]
-- ================================================================================================

ALTER TABLE [General].[GeneralParametersByPartner]
ADD [CancelCreditCardNumber] INT,
	[CancelCreditCardEmails] VARCHAR(1500),
    [CancelCreditCardActive] BIT