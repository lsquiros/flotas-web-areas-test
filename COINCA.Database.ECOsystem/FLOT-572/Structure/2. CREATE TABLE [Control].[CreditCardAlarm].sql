USE [ECOsystemDev]
GO

IF OBJECT_ID (N'[Control].[CreditCardAlarm]', N'U') IS NOT NULL DROP TABLE [Control].[CreditCardAlarm]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 01/02/2019
-- Description:	Create table [Control].[CreditCardAlarm]
-- ================================================================================================

CREATE TABLE [Control].[CreditCardAlarm]
(
	[Id] INT IDENTITY,
	[CustomerId] INT,
	[StatusId] INT,
	[MonthlyCount] INT,
	[AlertCount] INT,
	[InsertDate] DATETIME,
	[InsertUserId] INT,
	CONSTRAINT [PK_CreditCardAlarm] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) 
GO

ALTER TABLE [Control].[CreditCardAlarm] WITH CHECK ADD CONSTRAINT [FK_CreditCardAlarm_CustomerId] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])

GO
