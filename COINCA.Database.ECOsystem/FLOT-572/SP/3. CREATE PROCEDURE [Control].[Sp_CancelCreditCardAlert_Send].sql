USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CancelCreditCardAlert_Send]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CancelCreditCardAlert_Send]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 29/07/2019
-- Description:	Retrieve parameters for the partner
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_CancelCreditCardAlert_Send]
(	
	 @pCustomerId INT,
	 @pCustomerName VARCHAR(500),
	 @pStatusId INT, 
	 @pCreditCardCount INT,
	 @pUserId INT
)
AS
BEGIN	
	SET NOCOUNT ON	

	IF @pStatusId = 9 
	BEGIN 
		DECLARE @lCreditCardNumber INT, 
				@lCreditCardEmails VARCHAR(1500),
				@lCancelCreditCardSend DATETIME,
				@lCount INT

		SELECT @lCreditCardNumber = gp.[CancelCreditCardNumber], 
			   @lCreditCardEmails = gp.[CancelCreditCardEmails]
		FROM [General].[GeneralParametersByPartner] gp
		INNER JOIN [General].[CustomersByPartner] cp
			ON gp.[PartnerId] = cp.[PartnerId]
		WHERE gp.[CancelCreditCardActive] = 1
		AND cp.[CustomerId] = @pCustomerId

		IF @lCreditCardEmails IS NOT NULL
		BEGIN 
			SELECT @lCount = COUNT(1)
			FROM [Control].[CreditCard] 
			WHERE [CustomerId] = @pCustomerId
			AND DATEPART(MONTH, [ModifyDate]) = DATEPART(MONTH, GETDATE())				
			AND DATEPART(YEAR, [ModifyDate]) = DATEPART(YEAR, GETDATE())
			AND [StatusId] = 9

			IF @pCreditCardCount >= 10 OR @lCount >= @lCreditCardNumber			
			BEGIN
				IF	@pCreditCardCount >= 10
					OR NOT EXISTS 
					(
						SELECT *
						FROM [Control].[CreditCardAlarm]
						WHERE [CustomerId] = @pCustomerId
						AND [MonthlyCount] >= @lCreditCardNumber
						AND DATEPART(MONTH, [InsertDate]) = DATEPART(MONTH, GETDATE())	 
						AND DATEPART(YEAR, [InsertDate]) = DATEPART(YEAR, GETDATE())
					)
				BEGIN 
					EXEC [General].[Sp_SendAlarm]	@pAlarmTriggerId = 651,
													@pEmail = @lCreditCardEmails, 
													@pCreditCards = @lCount,
													@pCustomerName = @pCustomerName
				
					--Log the alert
					INSERT INTO [Control].[CreditCardAlarm]
					(
						[CustomerId],
						[StatusId],
						[MonthlyCount],
						[AlertCount],
						[InsertDate],
						[InsertUserId]
					)
					VALUES 
					(
						 @pCustomerId,
						 @pStatusId,
						 @lCount,
						 @pCreditCardCount,
						 GETDATE(),
						 @pUserId
					)	
				END
			END
		END
	END	
	
	SET NOCOUNT OFF
END
