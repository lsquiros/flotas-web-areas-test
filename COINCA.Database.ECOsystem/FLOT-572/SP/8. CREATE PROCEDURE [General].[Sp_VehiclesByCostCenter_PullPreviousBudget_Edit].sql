USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_VehiclesByCostCenter_PullPreviousBudget_Edit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_VehiclesByCostCenter_PullPreviousBudget_Edit]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Melvin Salas
-- Create date: Aug/05/2016
-- Description:	Change PullPreviousBudget by CostCenter
-- Stefano Quirós - Update PullPrevious to Drivers
-- =============================================
CREATE PROCEDURE [General].[Sp_VehiclesByCostCenter_PullPreviousBudget_Edit]
	 @pCostCenterId INT				--@pCostCenterId: PK of the table VehicleGroup
	,@pPullPreviousBudget BIT = 0	--@pPullPreviousBudget: PullPreviousBudget Value
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT  

		UPDATE	[General].[Vehicles]
		SET		PullPreviousBudget = @pPullPreviousBudget
		WHERE	CostCenterId = @pCostCenterId

		UPDATE	[General].[DriversUsers]
		SET		PullPreviousBudget = @pPullPreviousBudget
		WHERE	CostCenterId = @pCostCenterId

		COMMIT TRANSACTION			
    END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
