-- ================================================================================================  
-- Author:  Henry Retana
-- Create date: 29/07/2019
-- Description: Update footer in the emails send to customers
-- =================================================================================================== 

UPDATE v 
SET [Message] = REPLACE(v.[Message], 'COINCA Telematics', 'Namutek')
FROM [General].[Values] v
WHERE v.[Message] IS NOT NULL

UPDATE v 
SET [Message] = REPLACE(v.[Message], 'www.flotas.baccredomatic.com', 'flotas.baccredomatic.com')
FROM [General].[Values] v
WHERE v.[Message] IS NOT NULL