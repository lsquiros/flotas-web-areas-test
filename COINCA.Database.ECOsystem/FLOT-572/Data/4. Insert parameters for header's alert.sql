USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 12/08/2019
-- Description:	Insert parameters for header's alert
-- ================================================================================================

BEGIN TRY
	BEGIN TRANSACTION	

	INSERT INTO [dbo].[GeneralParameters]
	VALUES 
	(
		'ALERT_CONTROL_CAR_HEADER',
		'NA',
		'ALERT_CONTROL_CAR_HEADER',
		'http://www.bisqa.com/Namutek/controlcar-header_mail.png',
		0
	)

	INSERT INTO [dbo].[GeneralParameters]
	VALUES 
	(
		'ALERT_FLOTAS_HEADER',
		'NA',
		'ALERT_FLOTAS_HEADER',
		'http://www.bisqa.com/Namutek/flotas-header_mail.png',
		0
	)

	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION		
END CATCH