--USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 29/07/2019
-- Description:	Insert data for creditcard cancel alarm
-- ================================================================================================

DECLARE @lTypeId INT

INSERT INTO [General].[Types]
(
	[TypeId],
	[Usage],
	[Name],
	[Description],
	[Code],
	[RowOrder],
	[IsActive],
	[InsertDate]
)
VALUES 
(
	651,
	'CREDITCARDCANCEL_ALARM',
	'Alerta por tarjetas canceladas',
	'Alerta por tarjetas canceladas',
	'CREDITCARDCANCEL_ALARM',
	1,
	1,
	GETDATE()
)

SET @lTypeId = 651

INSERT INTO [General].[Values]
(
	[TypeId],
	[Value],
	[InsertDate],
	[Message]
)
VALUES 
(
	@lTypeId,
	0,
	GETDATE(),
	'<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta name="viewport" content="width=device-width">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title></title>
		<style>
			.container {
				display: block !important;
				max-width: 600px !important;
				margin: 0 auto !important; /* makes it centered */
				clear: both !important;
			}

			.content {
				padding: 10px 0 0 0;
				max-width: 100%;
				margin: 0 auto;
				display: block;
			}

				.content table {
					width: 100%;
				}

			.column-bottom {
				font-size: 26px;
				line-height: 1px;
				background-color: #dddddd;
			}
		</style>
	</head>
	<body bgcolor="#f6f6f6" cz-shortcut-listen="true">
		<p>&nbsp;</p>   <!-- body -->   <table class="body-wrap">   <tbody>   <tr>   <td>&nbsp;</td>   <td class="container" bgcolor="#FFFFFF"><!-- content -->   <div class="content">   <table style="margin-top: -13px;">   <tbody>   <tr>   <td style="padding: 0; vertical-align: top; padding-left: 6px; padding-right: 6px; word-break: break-word; word-wrap: break-word;" height="1">   <h1 style="margin-top: 27px; height: 23px; font-style: normal; font-weight: bold; font-size: 18px; text-align: center; line-height: 44px; margin-bottom: 24px; font-family: Lato,Tahoma,sans-serif; color: #565656;">Notificaci&oacute;n por Tarjetas Canceladas</h1>   </td>   </tr>   </tbody>   </table>   <div class="column-bottom" style="font-size: 26px; line-height: 1px; background-color: #dddddd;">&nbsp;</div>   <table style="padding: 0px 105px 0px 0px; margin-top: 20px; font-style: normal; font-weight: 400; line-height: 24px; margin-bottom: 24px; font-family: Cabin,Avenir,sans-serif; color: #565656; font-size: 16px;">   <tbody>   <tr>   <td style="text-align: right; padding-right: 20px;" height="1">Nombre de la Empresa:</td>   <td height="1"><strong>%customerName% </strong></td>   </tr>   <tr>   <td style="text-align: right; padding-right: 20px;" height="1">N&uacute;meros de Tarjetas:</td>   <td height="1"><strong>%creditCardNumber% </strong></td>   </tr>   <tr>   <td style="text-align: right; padding-right: 20px;" height="1">Fecha:</td>   <td height="1"><strong>%date% </strong></td>   </tr>   </tbody>   </table>   </div>   <!-- /content --></td>   <td>&nbsp;</td>   </tr>   </tbody>   </table>   <p>&nbsp;</p>   <!-- footer -->   <table class="footer-wrap">   <tbody>   <tr>   <td>&nbsp;</td>   <td class="container"><!-- content -->   <div class="content">   <table>   <tbody>   <tr>   <td align="center">   <p>Namutek | <a style="color: #929497;" href="flotas.baccredomatic.com">flotas.baccredomatic.com</a></p>   </td>   </tr>   </tbody>   </table>   </div>   <!-- /content --></td>   <td>&nbsp;</td>   </tr>   </tbody>   </table>   <!-- /footer -->
	</body>
	</html>'
)