USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 12/08/2019
-- Description:	Changes for the alerts
-- ================================================================================================

BEGIN TRY
	BEGIN TRANSACTION	

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificación Fuera de Horario </span></h2>
					<p>El vehículo <strong>%vehicle%</strong> esta siendo utilizando fuera de horario permitido.</p>
            
					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1" colspan="2" class="center-div"><h2>Detalles&nbsp;</h2></td>                        
							</tr>
							<tr>
								<td><strong style="font-weight: bold">Fecha: &nbsp;</strong></td>
								<td>%time%</td>
							</tr>
							<tr>
								<td><strong style="font-weight: bold">Veh&#237;culo:</strong></td>
								<td>%vehicle%</td>
							</tr>
							<tr>
								<td><strong>Latitud:</strong></td>
								<td>%latitude% </td>
							</tr>
							<tr>
								<td><strong>Longitud:</strong></td>
								<td>%longitude% </td>
							</tr>
							<tr>
								<td><strong>Direcci&oacute;n:</strong></td>
								<td style="max-width: 185px;">%location% </td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&zoom=15&size=800x400&&markers=color:red%7C%latitude%,%longitude%&key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 502

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 820px;
					max-width: 820px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n de Mantenimiento Preventivo</span></h2>
					<table>
						<tbody>
							<tr style="height: 1px;">
								<td><strong style="font-weight: bold;">Fecha: &nbsp;</strong></td>
								<td>%time%</td>
							</tr>
							<tr style="height: 1px;">
								<td><strong style="font-weight: bold;">Veh&iacute;culo:</strong></td>
								<td>%vehicle%</td>
							</tr>
							<tr style="height: 1px;">
								<td><strong style="font-weight: bold;">Placa:</strong></td>
								<td>%plateid%</td>
							</tr>
							<tr style="height: 1px;">
								<td><strong style="font-weight: bold;">Kilometraje:</strong></td>
								<td>%odometer%</td>
							</tr>
							<tr style="height: 1px;">
								<td><strong style="font-weight: bold;">Nombre del Mantenimiento:</strong></td>
								<td>%maintenancename%</td>
							</tr>
							<tr style="height: 1px;">
								<td><strong style="font-weight: bold;">Fecha para realizar el mantenimiento:</strong></td>
								<td>%nextreviewdate% - ( %expireddays% d&iacute;as )</td>
							</tr>
							<tr style="height: 1px;">
								<td><strong style="font-weight: bold;">Kilometraje para realizar el mantenimiento :</strong></td>
								<td>%nextreviewodometer% - ( %expiredodometer% kms )</td>
							</tr>
							<tr style="height: 1px;">
								<td><em>*%descriptiondate%%descriptionodometer%</em></td>
							</tr>
						</tbody>
					</table>
					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 503

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n de Licencia Vencida</span></h2>
            
					<table>
						<tbody>
							<tr>
								<td><strong style="font-weight: bold;">Fecha: &nbsp;</strong></td>
								<td>%time%</td>
							</tr>
							<tr>
								<td><strong style="font-weight: bold;">Fecha de Vencimiento:</strong></td>
								<td>%expirationdate%</td>
							</tr>
							<tr>
								<td><strong style="font-weight: bold;">D&iacute;as Faltantes:</strong></td>
								<td>%remaining%</td>
							</tr>
							<tr>
								<td><strong style="font-weight: bold;">Conductor:</strong></td>
								<td>%Driver%</td>
							</tr>
							<tr>
								<td><strong style="font-weight: bold;">Observaciones:</strong></td>
								<td>El conductor <strong>%Driver%</strong> tiene la licencia vencida o Pronta a Vencer.</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 504

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">        
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">            
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n de Od&oacute;metro</span></h2>

					<table>
						<tbody>
							<tr>
								<td><strong style="font-weight: bold;">Fecha: &nbsp;</strong></td>
								<td>%time%</td>
							</tr>                   
							<tr>
								<td><strong style="font-weight: bold;">Veh&iacute;culo:</strong></td>
								<td>%vehicle%</td>
							</tr>
							<tr>
								<td><strong style="font-weight: bold;">Observaciones:</strong></td>
								<td>El Od&oacute;metro esperado no corresponde al registrado en la transacci&oacute;n para el veh&iacute;culo con placa <strong>%Vehicle%</strong>.</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 505

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">        
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">            
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n Vencimiento de Tarjeta</span></h2>
            
					<table>
						<tbody>
							<tr>
								<td><strong style="font-weight: bold;">Fecha: &nbsp;</strong></td>
								<td>%time%</td>
							</tr>
							<tr>
								<td><strong style="font-weight: bold;">Tarjeta:</strong></td>
								<td><strong>%CreditCardNumber%</strong></td>
							</tr>
							<tr>
								<td><strong style="font-weight: bold;">Observaciones:</strong></td>
								<td>La tarjeta n&uacute;mero <strong>%CreditCardNumber%</strong> vence el mes de %ExpirationMonth% del a&ntilde;o %ExpirationYear%. %RemainingTimeMessage%</td>
							</tr>
						</tbody>
					</table>
            
					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 506

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">        
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">            
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n Exceso Kilometraje Diario</span></h2>
            
					<table>
						<tbody>
							<tr>
								<td height="1"><strong>Fecha: </strong></td>
								<td height="1">%time%</td>
							</tr>
							<tr>
								<td height="1"><strong>Veh&iacute;culo:</strong></td>
								<td height="1">%PlateId%</td>
							</tr>
							<tr>
								<td height="1"><strong>Observaciones:</strong></td>
								<td height="1">El veh&iacute;culo con la placa <strong>%PlateId%</strong> ha excedido la cantidad de kil&oacute;metros definidos para el d&iacute;a de hoy (%KmRoute% km).</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 507

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n Tiempo de Parada</span></h2>

					<table>
						<tbody>
							<tr>
								<td><strong>Fecha: </strong></td>
								<td>%time%</td>
							</tr>                    
							<tr>
								<td><strong>Placa:</strong></td>
								<td>%PlateId%</td>
							</tr>
							<tr>
								<td><strong>Tiempo Detenido:</strong></td>
								<td>%lapse%</td>
							</tr>
							<tr>
								<td><strong>Ubicaci&oacute;n:</strong></td>
								<td> %location%</td>
							</tr>
							<tr>
								<td><strong>Latitud:</strong></td>
								<td>%latitude%</td>
							</tr>
							<tr>
								<td><strong>Longitud:</strong></td>
								<td>%longitude%</td>
							</tr>
							<tr>
								<td><strong>Observaciones:</strong></td>
								<td>El veh&iacute;culo ha excedido el tiempo de parada establecido en la configuraci&oacute;n de la alarma.</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 508

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">        
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">            
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n Inconsistencia de Od&oacute;metro</span></h2>
            
					<table>
						<tbody>
							<tr>
								<td height="1"><strong>Fecha: </strong></td>
								<td height="1">%time%</td>
							</tr>                    
							<tr>
								<td height="1"><strong>Placa:</strong></td>
								<td height="1">%PlateId%</td>
							</tr>
							<tr>
								<td height="1"><strong>&Uacute;ltimo Od&oacute;metro:</strong></td>
								<td height="1">%LastOdometer%</td>
							</tr>
							<tr>
								<td height="1"><strong>Od&oacute;metro Actual:</strong></td>
								<td height="1">%Odometer%</td>
							</tr>
							<tr>
								<td height="1"><strong>Observaciones:</strong></td>
								<td height="1">El veh&iacute;culo placa <strong>%PlateId%</strong> ha reportado inconsistencias de od&oacute;metro al recargar combustible.</td>
							</tr>
						</tbody>
					</table>
            
					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 509

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n de Exceso de Velocidad</span></h2>

					<table style="width:100%">
						<tbody>                   
							<tr>
								<td height="1"><strong>Fecha y Hora: </strong></td>
								<td height="1">%time%</td>
							</tr>
							<tr>
								<td height="1"><strong>Velocidad M&aacute;xima: </strong></td>
								<td height="1">%maxspeed%</td>
							</tr>
							<tr>
								<td height="1"><strong>Promedio: </strong></td>
								<td height="1">%agvspeed%</td>
							</tr>
							<tr>
								<td height="1"><strong>Velocidad M&iacute;nima: </strong></td>
								<td height="1">%minspeed%</td>
							</tr>
							<tr>
								<td height="1"><strong>Veh&iacute;culo:</strong></td>
								<td height="1">%vehicle%</td>
							</tr>
							<tr>
								<td height="1"><strong>Placa:</strong></td>
								<td height="1">%plate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Conductor:</strong></td>
								<td height="1">&amp;&amp;%driver%-/&amp;/-</td>
							</tr>
							<tr>
								<td height="1"><strong>Latitud:</strong></td>
								<td height="1">%latitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Longitud:</strong></td>
								<td height="1">%longitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Ubicaci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
						</tbody>
					</table>
					<br />
					<table style="width:100%">
						<thead>
							<tr>
								<td colspan="3"><center><strong>Velocidades Anteriores</strong></center></td>
							</tr>
							<tr>
								<td height="1"><strong>Fecha</strong></td>
								<td height="1"><strong>Velocidad</strong></td>
								<td height="1"><strong>Detalle</strong></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td height="1">%GPSDateTime1%</td>
								<td height="1">%Speed1%</td>
								<td height="1">%Comments1%</td>
							</tr>
							<tr>
								<td height="1">%GPSDateTime2%</td>
								<td height="1">%Speed2%</td>
								<td height="1">%Comments2%</td>
							</tr>
							<tr>
								<td height="1">%GPSDateTime3%</td>
								<td height="1">%Speed3%</td>
								<td height="1">%Comments3%</td>
							</tr>
							<tr>
								<td height="1">%GPSDateTime4%</td>
								<td height="1">%Speed4%</td>
								<td height="1">%Comments4%</td>
							</tr>
							<tr>
								<td height="1">%GPSDateTime5%</td>
								<td height="1">%Speed5%</td>
								<td height="1">%Comments5%</td>
							</tr>
							<tr>
								<td colspan="3" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 510

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">        
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">            
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n de&nbsp;Cambio de Precio</span></h2>
            
					<table>
						<tbody>
							<tr>
								<td height="1"><strong>Fecha y Hora: </strong></td>
								<td height="1">%time%</td>
							</tr>
							<tr>
								<td height="1"><strong>Tipo de Combustible:</strong></td>
								<td height="1">%fuelname%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nuevo Precio:</strong></td>
								<td height="1">%literprice%</td>
							</tr>
							<tr>
								<td height="1"><strong>Fecha del cambio:</strong></td>
								<td height="1">%changetime%</td>
							</tr>
							<tr>
								<td height="1"><strong>Observaciones:</strong></td>
								<td height="1">
									<p>Faltan %alarmdays% dias para el cambio de precio.</p>
									<p>%programalarm%</p>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 515

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">        
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">            
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Envio de Reporte Programado</span></h2>
            
					<table>
						<tbody>
							<tr>
								<td height="1"><strong>%body% </strong></td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 1000

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Alerta Bot&oacute;n de P&aacute;nico</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha de Alarma:</strong></td>
								<td height="1">%dateAlarm% </td>
							</tr>
							<tr>
								<td height="1"><strong>Placa Veh&iacute;culo:</strong></td>
								<td height="1">%plate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Veh&iacute;culo:</strong></td>
								<td height="1">%name%</td>
							</tr>
							<tr>
								<td height="1"><strong>Latitud:</strong></td>
								<td height="1">%latitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Longitud:</strong></td>
								<td height="1">%longitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 511

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">        
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">            
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n Transacciones Offline</span></h2>
            
					<table>
						<tbody>
							<tr>
								<td height="1"><strong>Fecha: </strong></td>
								<td height="1">%time%</td>
							</tr>
							<tr>
								<td height="1"><strong>Evento:</strong></td>
								<td height="1">Se %event% el Proceso de Transacciones Offline</td>
							</tr>
							<tr>
								<td height="1"><strong>Socio:</strong></td>
								<td height="1">%partnerid%</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 802

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n de Geocerca</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha: </strong></td>
								<td height="1">%time%</td>
							</tr>
							<tr>
								<td height="1"><strong>Evento:</strong></td>
								<td height="1">Ha %InOut% la Geocerca &ndash; %Description%.</td>
							</tr>
							<tr>
								<td height="1"><strong>Veh&iacute;culo:</strong></td>
								<td height="1">%vehicle%</td>
							</tr>
							<tr>
								<td height="1"><strong>Observaciones:</strong></td>
								<td height="1">El veh&iacute;culo <strong>%vehicle%</strong> ha %InOut% la geocerca %Description%.</td>
							</tr>
							<tr>
								<td height="1"><strong>Latitud:</strong></td>
								<td height="1">%latitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Longitud:</strong></td>
								<td height="1">%longitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>            
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 500

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Desconexi&oacute;n del GPS</span></h2>

					<table style="width: 100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha de Alarma:</strong></td>
								<td height="1">%dateAlarm%</td>
							</tr>
							<tr>
								<td height="1"><strong>Placa de Veh&iacute;culo:</strong></td>
								<td height="1">%plate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Veh&iacute;culo:</strong></td>
								<td height="1">%name%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td height="1"><strong>Latitud:</strong></td>
								<td height="1">%latitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Longitud:</strong></td>
								<td height="1">%longitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 12

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Alerta de Bater&iacute;a Baja</span></h2>

					<table style="width: 100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha de Alarma:</strong></td>
								<td height="1">%dateAlarm%</td>
							</tr>
							<tr>
								<td height="1"><strong>Placa de Veh&iacute;culo:</strong></td>
								<td height="1">%plate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Veh&iacute;culo:</strong></td>
								<td height="1">%name%</td>
							</tr>
							<tr>
								<td height="1"><strong>Conductor:</strong></td>
								<td height="1">%driver%</td>
							</tr>
							<tr>
								<td height="1"><strong>Latitud:</strong></td>
								<td height="1">%latitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Longitud:</strong></td>
								<td height="1">%longitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2"  class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 10

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Veh&iacute;culo Apagado por Comando</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha Alerta:</strong></td>
								<td height="1">%dateAlarm%</td>
							</tr>
							<tr>
								<td height="1"><strong>Matr&iacute;cula: </strong></td>
								<td height="1">%plate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Veh&iacute;culo: </strong></td>
								<td height="1">%name%</td>
							</tr>
							<tr>
								<td height="1"><strong>Latitud:</strong></td>
								<td height="1">%latitude% </td>
							</tr>
							<tr>
								<td height="1"><strong>Longitud:</strong></td>
								<td height="1">%longitude% </td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 25

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Temperatura M&aacute;xima Excedida</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha Alerta:</strong></td>
								<td height="1">%dateAlarm%</td>
							</tr>
							<tr>
								<td height="1"><strong>Temperatura Limite:</strong></td>
								<td height="1">%tempReportLim%</td>
							</tr>
							<tr>
								<td height="1"><strong>Temperatura Registrada: </strong></td>
								<td height="1">%tempReport%</td>
							</tr>
							<tr>
								<td height="1"><strong>Matr&iacute;cula: </strong></td>
								<td height="1">%plate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Veh&iacute;culo: </strong></td>
								<td height="1">%name%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 31

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Temperatura por debajo del m&iacute;nimo permitido</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha Alerta:</strong></td>
								<td height="1">%dateAlarm%</td>
							</tr>
							<tr>
								<td height="1"><strong>Temperatura Limite:</strong></td>
								<td height="1">%tempReportLim%</td>
							</tr>
							<tr>
								<td height="1"><strong>Temperatura Registrada: </strong></td>
								<td height="1">%tempReport%</td>
							</tr>
							<tr>
								<td height="1"><strong>Matr&iacute;cula: </strong></td>
								<td height="1">%plate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Veh&iacute;culo: </strong></td>
								<td height="1">%name%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 32

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Error en Sensor de Temperatura</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha Alerta:</strong></td>
								<td height="1">%dateAlarm%</td>
							</tr>
							<tr>
								<td height="1"><strong>Sensor(es) con Error</strong></td>
								<td height="1">%sensor%</td>
							</tr>
							<tr>
								<td height="1"><strong>Matr&iacute;cula: </strong></td>
								<td height="1">%plate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Veh&iacute;culo: </strong></td>
								<td height="1">%name%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 33

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Alerta de Plataforma de Gr&uacute;a Activa</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha de Alarma:</strong></td>
								<td height="1">%dateAlarm% </td>
							</tr>
							<tr>
								<td height="1"><strong>Placa Veh&iacute;culo:</strong></td>
								<td height="1">%plate% </td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Veh&iacute;culo:</strong></td>
								<td height="1">%name%</td>
							</tr>
							<tr>
								<td height="1"><strong>Latitud:</strong></td>
								<td height="1">%latitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Longitud:</strong></td>
								<td height="1">%longitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 280

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Alerta de Plataforma de Gr&uacute;a Inactiva</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha de Alarma:</strong></td>
								<td height="1">%dateAlarm% </td>
							</tr>
							<tr>
								<td height="1"><strong>Placa Veh&iacute;culo:</strong></td>
								<td height="1">%plate% </td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Veh&iacute;culo:</strong></td>
								<td height="1">%name%</td>
							</tr>
							<tr>
								<td height="1"><strong>Latitud:</strong></td>
								<td height="1">%latitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Longitud:</strong></td>
								<td height="1">%longitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 281

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Alerta de Cliente en Modo Piloto</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Socio:</strong></td>
								<td height="1">%partner%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre de la Empresa:</strong></td>
								<td height="1">%customerName%</td>
							</tr>
							<tr>
								<td height="1"><strong>Ingreso de la Empresa:</strong></td>
								<td height="1">%startDate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Vencimiento del Piloto:</strong></td>
								<td height="1">%isDemoAlert%</td>
							</tr>
							<tr>
								<td height="1"><strong>Cantidad de Tarjetas:</strong></td>
								<td height="1">%creditCards%</td>
							</tr>
							<tr>
								<td height="1"><strong>Cantidad de Veh&iacute;culos:</strong></td>
								<td height="1">%vehicles%</td>
							</tr>
							<tr></tr>
							<tr>
								<td colspan="2" class="center-div">** Este mensaje se enviar&aacute; continuamente, si desea dejar de recibirlo cambiar la fecha del Piloto o activar la empresa **</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 517

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Facturaci&oacute;n de Orden de Trabajo</span></h2>

					<table>
						<tbody>
							<tr>
								<td height="1"><strong>Fecha Emisi&oacute;n:</strong></td>
								<td height="1">%billingDate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Cliente:</strong></td>
								<td height="1">%clientName%</td>
							</tr>
							<tr>
								<td height="1"><strong>Placa Veh&iacute;culo:</strong></td>
								<td height="1">%plate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Tipo de Orden:</strong></td>
								<td height="1">%orderType%</td>
							</tr>
							<tr>
								<td height="1"><strong>Zona de Cobro:</strong></td>
								<td height="1">%zoneName%</td>
							</tr>
							<tr>
								<td height="1"><strong>Detalle:</strong></td>
								<td height="1">DescriptionCant.Precio%billingDetail%</td>
							</tr>
							<tr>
								<td height="1"><strong>Observaciones:</strong></td>
								<td height="1">%billingComments%</td>
							</tr>
						</tbody>
					</table>                   

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 1001

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Alarma de Veh&iacute;culos Sin Transmitir</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Fecha de Alarma:</strong></td>
								<td height="1">%dateAlarm%</td>
							</tr>
							<tr>
								<td height="1"><strong>Placa de Veh&iacute;culo:</strong></td>
								<td height="1">%plate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre Veh&iacute;culo:</strong></td>
								<td height="1">%name%</td>
							</tr>
							<tr>
								<td height="1"><strong>Fecha de la &Uacute;ltima Transmisi&oacute;n:</strong></td>
								<td height="1">%dateLastReport%</td>
							</tr>
							<tr>
								<td height="1"><strong>Latitud:</strong></td>
								<td height="1">%latitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>Longitud:</strong></td>
								<td height="1">%longitude%</td>
							</tr>
							<tr>
								<td height="1"><strong>&Uacute;ltima Ubicaci&oacute;n:</strong></td>
								<td height="1">%location%</td>
							</tr>
							<tr>
								<td colspan="2" class="center-div">
									<a href="https://www.google.com/maps/?q=%latitude%,%longitude%">
										<img src="https://maps.googleapis.com/maps/api/staticmap?center=%latitude%,%longitude%&amp;zoom=15&amp;size=800x400&amp;&amp;markers=color:red%7C%latitude%,%longitude%&amp;key=AIzaSyDmofzVN07-9vuM_ir2Gu5KmUYJnfdl3D0" alt="" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 13

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Solicitud Orden de %OrderName%</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td colspan="2" height="1" class="center-div"><strong>Informaci&oacute;n de Cliente</strong></td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre:</strong></td>
								<td height="1">%customerName%</td>
							</tr>
							<tr>
								<td height="1"><strong>Contacto:</strong></td>
								<td height="1">%contact%</td>
							</tr>
							<tr>
								<td height="1"><strong>Tel&eacute;fono 1:</strong></td>
								<td height="1">%phone1%</td>
							</tr>
							<tr>
								<td height="1"><strong>Tel&eacute;fono 2:</strong></td>
								<td height="1">%phone2%</td>
							</tr>
							<tr>
								<td height="1"><strong>Correo:</strong></td>
								<td height="1">%email%</td>
							</tr>
							<tr>
								<td height="1"><strong>Direcci&oacute;n:</strong></td>
								<td height="1">%address%</td>
							</tr>
						</tbody>
					</table>
					<br />
					<table style="width:100%">
						<tbody>
							<tr>
								<td colspan="2" height="1" class="center-div"><strong>Informaci&oacute;n de Veh&iacute;culo</strong></td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre:</strong></td>
								<td height="1">%vehicleName%</td>
							</tr>
							<tr>
								<td height="1"><strong>Placa:</strong></td>
								<td height="1">%plateid%</td>
							</tr>
							<tr>
								<td height="1"><strong>Chasis:</strong></td>
								<td height="1">%chassis%</td>
							</tr>
							<tr>
								<td height="1"><strong>Fabricante:</strong></td>
								<td height="1">%manufacturer%</td>
							</tr>
							<tr>
								<td height="1"><strong>Modelo:</strong></td>
								<td height="1">%Model%</td>
							</tr>
							<tr>
								<td height="1"><strong>Tipo:</strong></td>
								<td height="1">%Type%</td>
							</tr>
							<tr>
								<td height="1"><strong>Color:</strong></td>
								<td height="1">%Colour%</td>
							</tr>
							<tr>
								<td height="1"><strong>A&ntilde;o:</strong></td>
								<td height="1">%year%</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 650

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n de Vencimiento de Contrato</span></h2>

					<table style="width:100%">
						<tbody>
							<tr>
								<td height="1"><strong>Socio:</strong></td>
								<td height="1">%partner% </td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre de la Empresa:</strong></td>
								<td height="1">%customerName%</td>
							</tr>
							<tr>
								<td height="1"><strong>Inicio del Contrato:</strong></td>
								<td height="1">%startDate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Fin del Contrato:</strong></td>
								<td height="1">%endDate%</td>
							</tr>
							<tr>
								<td height="1"><strong>Contrato vence en:</strong></td>
								<td height="1">%daysLeft% d&iacute;as</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 518

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">        
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">            
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Notificaci&oacute;n por Tarjetas Canceladas </span></h2>
					<p>Se le informa que el cliente: <b>%customerName%</b>, ha cancelado <b>%creditCardNumber% tarjetas</b> durante el mes al d&iacute;a <b>%date%</b>.</p>
					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 651

	UPDATE [General].[Values]
	SET [Message] = '<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
			<style>
				body {
					color: #666;
					background-color: #fff;
					font-family: "Open Sans",Arial,sans-serif;
					font-size: 14px;
					font-weight: 500;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					line-height: 1.7em;
					width: 800px;
					max-width: 800px;
				}

				.center-div {
					text-align: center;
				}

				.container {
					width: 800px;
					max-width: 800px;
				}

				.content {
					margin: 40px;
				}
			</style>
		</head>
		<body cz-shortcut-listen="true">
			<div class="container">
				<div class="center-div">
					<img src="%pictureHeader%" alt="Namutek" id="logo" style="width:100%">
				</div>
				<div class="content center-div">
					<h2><span style="color: rgb(33, 29, 112);">Alerta de Cliente en Modo Prueba</span></h2>

					<table>
						<tbody>
							<tr>
								<td height="1"><strong>Socio: </strong></td>
								<td height="1">%partner%</td>
							</tr>
							<tr>
								<td height="1"><strong>Nombre del Cliente: </strong></td>
								<td height="1">%customerName%</td>
							</tr>
							<tr>
								<td height="1"><strong>Fecha de Ingreso: </strong></td>
								<td height="1">%startDate% </td>
							</tr>
							<tr>
								<td height="1"><strong>Vencimiento de la Prueba: </strong></td>
								<td height="1">%isDemoAlert%</td>
							</tr>
							<tr></tr>
							<tr>
								<td colspan="2" class="center-div">** Este correo se enviar&aacute; cuando la prueba haya expirado, si desea dejar de recibirlo cambie la fecha del Modo Prueba en la ficha del Cliente **</td>
							</tr>
						</tbody>
					</table>

					<h2>Estamos para servirle!</h2>
					<a href="https://flotas.baccredomatic.com" style="color:#929497;">flotas.baccredomatic.com</a>
					<br />
				</div>
				<div class="center-div">
					<img src="http://www.bisqa.com/Namutek/namutek-footer_mail.png" alt="Namutek" id="logo" style="width:100%">
				</div>
			</div>
		</body>
		</html>'
	WHERE [TypeId] = 519

	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION		
END CATCH