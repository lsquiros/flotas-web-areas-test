/****** Object:  StoredProcedure [General].[Sp_Vehicles_AddOrEdit]    Script Date: 07/08/2020 3:22:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================  
-- Author:  Berman Romero L.  
-- Create date: Sep/23/2014  
-- Description: Insert or Update Vehicle information  
-- ================================================================================================  
-- Modify date: 2/3/2015  
-- Description: Modifications in the Intrack Query in order to validate the Device Information  
-- ================================================================================================  
-- Modify by: Melvin Salas  
-- Modify date: Jun/29/2016  
-- Description: Allow Null UserId and Insert into VehiclesDrivers  
-- ================================================================================================  
-- Modify by: Melvin Salas  
-- Modify date: Aug/04/2016  
-- Description: Add PullPreviousBudget  
-- ================================================================================================  
-- Modify by: Gerald Solano  
-- Modify date: Agos/11/2016  
-- Description: Update process to retrieve device reference from Composiciones  
-- ================================================================================================  
-- Modify by: Stefano Quirós  
-- Modify date: Octubre/03/2016  
-- Description: Update the sp, now insert always into ECOsystemQAVehicles  
-- ================================================================================================  
-- Modify by: Marco Cabrera  
-- Modify date: 24/02/2017  
-- Description: Now insert or update the ExternalId in Vehicles  
-- ================================================================================================   
-- Modify by: Albert Estrada  
-- Modify date: sep/22/2017  
-- Description: Insert insert or update in General.Vehicles   
-- ================================================================================================  
-- Modify by: Esteban Solis
-- Modify date: Oct/20/2017  
-- Description: Save HasCooler and  TempSensorCount Fields
-- ================================================================================================  
-- Henry Retana - 04/06/2018
-- Add the Sp for replication in Intrack and remove the edit for the Intrack Data
-- ================================================================================================  
-- Modify by:	 Gerald Solano
-- Modify date:  11/01/2019
-- Description:	 Se pasan los valores anteriores a la actualizacion de la placa y el chasis en Intrack
 -- ================================================================================================  
 -- Modify by:	 Karina Cruz
-- Modify date:  11/01/2019
-- Description:	 Update @pIntrackReference in case it is null return the current value of @pIntrackReference.
 -- ================================================================================================  
 -- Modify by:	 Juan C. Santamaria V.
-- Modify date:  2020-03-05
-- Description:	 Insert the first odometer the first time
 -- ================================================================================================   
 -- Modify by:	 Juan C. Santamaria V.
-- Modify date:  2020-04-06
-- Description:	 Update the field in case this null--FuelID in FuelDistribution Table
 -- ================================================================================================ 
 -- Modify by:	 Jason Bonilla
 -- Modify date:  2020-04-06
 -- Description:	 Added Field FleetioId
 -- ================================================================================================ 
 -- Modify by:	 Jason Bonilla
 -- Modify date:  2020-08-07
 -- Description:	 Added Field Depreciation
 -- ================================================================================================ 


ALTER PROCEDURE [General].[Sp_Vehicles_AddOrEdit] --@pVehicleId = 31847, @pName = 'Eric de La Goublayeeee de Menorva', @pCustomerId = 13569, @pLoggedUserId = 39579
(  
	  @pVehicleId INT = NULL       --@pVehicleId: PK of the table  
	 ,@pPlateId VARCHAR(10) = NULL     --@pPlateId: Vehicle Plate Number  
	 ,@pName VARCHAR(250)       --@pName: Vehicle Name  
	 ,@pCostCenterId INT = NULL       --@pCostCenterId: FK of CostCenter  
	 ,@pCustomerId INT        --@pCustomerId: FK of Customer  
	 ,@pUserId INT = NULL       --@pUserId: FK of Users  
	 ,@pVehicleCategoryId INT = NULL      --@pVehicleCategoryId: FK of Vehicle Category  
	 ,@pActive BIT = NULL         --@pActive: Is Active  
	 ,@pColour VARCHAR(50) = NULL         --@pColour: Vehicle Colour  
	 ,@pChassis VARCHAR(50) = NULL     --@pChassis: Vehicle Chassis or VIN  
	 ,@pLastDallas VARCHAR(50) = NULL    --@pLastDallas: Vehicle Last Dallas   
	 ,@pFreightTemperature INT = NULL       --@pFreightTemperature: Vehicle Freight Temperature  
	 ,@pFreightTemperatureThreshold1 INT = NULL  --@pFreightTemperatureThreshold1: Vehicle First Freight Temperature Threshold  
	 ,@pFreightTemperatureThreshold2 INT = NULL  --@pFreightTemperatureThreshold2: Vehicule Second Freight Temperature Threshold  
	 ,@pPredictive BIT = NULL        --@pPredictive: Is Predictive  
	 ,@pAVL VARCHAR(50) = NULL      --@PAVL: Vehicle AVL   
	 ,@pPhoneNumber VARCHAR(10) = NULL    --@pPhoneNumber: Phone Number  
	 ,@pInsurance VARCHAR(20) = NULL     --@pInsurance: Insurance of Vehicle  
	 ,@pDateExpirationInsurance DATE = NULL   --@pDateExpirationInsurance: Date Expiration Insurance  
	 ,@pCoverageType VARCHAR(20)= NULL    --@pCoverageType: Coverage Type of Insurance  
	 ,@pCabinPhone VARCHAR(50)= NULL     --@pCabinPhone: Cabin Phone Number  
	 ,@pNameEnterpriseInsurance VARCHAR(50) = NULL --@pNameEnterpriseInsurance: Company Name of Insurance  
	 ,@pPeriodicityId INT = 0      --@@pTypeId: Periodicity of Insurance  
	 ,@pCost DECIMAL(16,2) = NULL     --@pCost: Cost   
	 ,@pOdometer INT = NULL       --@pInitialOdometer: Odometer of Vehicles By Users  
	 ,@pLoggedUserId INT        --@pLoggedUserId: Logged UserId that executes the operation  
	 ,@pAdministrativeSpeedLimit INT = NULL     --@pAdministrativeSpeedLimit: Maximum speed limit for this car  
	 ,@pIntrackReference INT = NULL     --@pIntrackReference: Vehicle Number in Intrack System to get AVL  
	 ,@pImei DECIMAL = NULL       --@pImei: Imei number of AVL, use to validate IntrackReference number  
	 ,@pRowVersion TIMESTAMP  = NULL     --@pRowVersion: Timestamp of row to prevent bad updates  
	 ,@pPullPreviousBudget BIT = 0     --@pPullPreviousBudget: Pull Previous Budget  
	 ,@pExternalId INT = NULL      --@pExternalId: Identification for external systems  
	 ,@pOdometerVehicle FLOAT = NULL      --@OdometerVehicle: Identification for Odometer   
	 ,@pHasCooler BIT = 0
	 ,@pTempSensorCount INT = 0
	 ,@pMinTemperature DECIMAL(18,2) = 0
	 ,@pMaxTemperature DECIMAL(18,2) = 0
	 ,@pFleetioId BIGINT = 0
	 ,@pDepreciation DECIMAL(18,3) = 0
	 ,@pVehiclePrice DECIMAL(18, 2) = 0
)  
AS  
BEGIN  
   
	SET NOCOUNT ON  
    SET XACT_ABORT ON  
      
    BEGIN TRY  
		DECLARE @lErrorMessage NVARCHAR(4000)  
		DECLARE @lErrorSeverity INT  
		DECLARE @lErrorState INT  
		DECLARE @lLocalTran BIT = 0  
		DECLARE @lUserIdByVehicle INT   
		DECLARE @lVehiclesByUserId INT  
		DECLARE @lResult INT  
		DECLARE @lIntrackReference INT  
		DECLARE @lDeviceReference INT         
		DECLARE @lAdmSpeedLimitPrev INT  
        DECLARE @lCustomerPlateCount INT = 0  
		DECLARE @lIssueForId INT,  
				@lManufacturer VARCHAR(200),
				@lYear INT,
				@lFuelId INT
		DECLARE @tmpvehicleid INT
		DECLARE @pIsAClientLinked INT = 0
		DECLARE @OldPlate VARCHAR(10) = NULL
		DECLARE @OldChasis VARCHAR(50) = NULL
		DECLARE @OldDefaultFuelId INT

        IF (@@TRANCOUNT = 0)  
	    BEGIN  
			BEGIN TRANSACTION  
			SET @lLocalTran = 1  
	    END    
		
		--Replicate information in intrack              
		SELECT @lManufacturer = [Manufacturer],
			   @lYear = [Year],
			   @lFuelId = [DefaultFuelId]
		FROM [General].[VehicleCategories]
		WHERE [VehicleCategoryId] = @pVehicleCategoryId 
  
	    IF (@pVehicleId IS NULL)  
	    BEGIN  
			-- VALIDAMOS EN LA INSERCIÓN SI YA EXISTE LA PLACA  
			SELECT @lCustomerPlateCount = COUNT(1)        
			FROM [General].[Vehicles]  
			WHERE [CustomerId] = @pCustomerId 
			AND [PlateId] = @pPlateId   
			AND [IsDeleted] <> 1  
  
			--Validate vehicle plate is not duplicate for the customer  
			IF @lCustomerPlateCount > 0  
			BEGIN  
				RAISERROR ('PLATE_IS_EXIST', 16, 1)  
			END  
			ELSE  
			BEGIN  
				INSERT INTO [General].[Vehicles]  
				 (
					 [PlateId]  
					,[Name]  
					,[CostCenterId]  
					,[CustomerId]  
					,[VehicleCategoryId]  
					,[Active]  
					,[Colour]  
					,[Chassis]  
					,[LastDallas]  
					,[FreightTemperature]  
					,[FreightTemperatureThreshold1]  
					,[FreightTemperatureThreshold2]  
					,[Predictive]  
					,[AVL]  
					,[PhoneNumber]  
					,[Insurance]  
					,[DateExpirationInsurance]  
					,[CoverageType]  
					,[NameEnterpriseInsurance]  
					,[PeriodicityId]  
					,[Cost]  
					,[AdministrativeSpeedLimit]  
					,[CabinPhone]  
					,[IntrackReference]  
					,[Imei]  
					,[InsertDate]  
					,[InsertUserId]  
					,[PullPreviousBudget]  
					,[ExternalId]  
					,[Odometer]
					,[HasCooler]
					,[TempSensorCount]
					,[MinTemperature]
					,[MaxTemperature]
					,[FirstOdometer]
					,[FleetioId]
					,[Depreciation]
					,[VehiclePrice]
				 )  
				 VALUES 
				 (
					 UPPER(@pPlateId)  
					,@pName  
					,@pCostCenterId  
					,@pCustomerId  
					,@pVehicleCategoryId  
					,@pActive  
					,@pColour  
					,@pChassis  
					,@pLastDallas  
					,@pFreightTemperature  
					,@pFreightTemperatureThreshold1  
					,@pFreightTemperatureThreshold2  
					,@pPredictive  
					,@pAVL  
					,@pPhoneNumber  
					,@pInsurance  
					,@pDateExpirationInsurance  
					,@pCoverageType  
					,@pNameEnterpriseInsurance  
					,isnull(@pPeriodicityId,0)
					,@pCost  
					,@pAdministrativeSpeedLimit  
					,@pCabinPhone  
					,@pIntrackReference  
					,@pImei  
					,GETUTCDATE()  
					,@pLoggedUserId  
					,@pPullPreviousBudget  
					,@pExternalId  
					,@pOdometerVehicle  
					,@pHasCooler
					,@pTempSensorCount
					,@pMinTemperature
					,@pMaxTemperature
					,@pOdometerVehicle
					,@pFleetioId
					,@pDepreciation
					,@pVehiclePrice
				 )  
         
				SET @tmpvehicleid = SCOPE_IDENTITY()  
  
				IF (@pUserId IS NOT NULL)  
				BEGIN   
					INSERT INTO [General].[VehiclesByUser]  
					  (
						  [VehicleId]  
						 ,[UserId]  
						 ,[InitialOdometer]  
						 ,[InsertDate]  
						 ,[InsertUserId]
					  )  
					  VALUES 
					  (
						  @tmpvehicleid  
						 ,@pUserId  
						 ,@pOdometer  
						 ,GETUTCDATE()  
						 ,@pLoggedUserId
					  )  
					          
					-- SE AGREGA EL NUEVO USUARIO ACTIVO DEL VEHICULO  
					INSERT INTO [General].[VehiclesDrivers]  
					(
						[UserId]  
						,[VehicleId]  
						,[InsertDate]  
						,[InsertUserId]
					)  
					VALUES 
					(
						@pUserId  
						,@tmpvehicleid  
						,GETUTCDATE()  
						,@pLoggedUserId
					)  
				END  
       
     			SELECT @lIssueForId = [IssueForId] 
				FROM [General].[Customers] 
				WHERE [CustomerId] = @pCustomerId  
       
				IF @lIssueForId = 101  
				BEGIN  
					--Inserta registro en distribucion de combustible por defect, solamente la primera vez  
					DECLARE @pMonth INT = MONTH(GETDATE());   
					DECLARE @pYear INT = YEAR(GETDATE());   
             
					INSERT INTO [Control].[FuelDistribution]  
					(
						[VehicleId]  
						,[Month]  
						,[Year]  
						,[Liters]  
						,[Amount]  
						,[AdditionalAmount]
						,[AdditionalLiters]
						,[InsertDate]  
						,[InsertUserId]
						,[FuelId]
					)  
					VALUES 
					(
						 @tmpvehicleid  
						,@pMonth  
						,@pYear  
						,0.00  
						,0.00  
						,0.00  
						,0.00  
						,GETUTCDATE()  
						,@pLoggedUserId
						,@lFuelId
					)
				END                
			END -- END DUPLICATE PLATE VALIDATION  
      
		END  
	    ELSE  
	    BEGIN 
			-- VALIDAMOS EN LA ACTUALIZACIÓN SI YA EXISTE OTRO VEHICULO CON LA MISMA PLACA  
			SELECT @lCustomerPlateCount = COUNT(1)        
			FROM [General].[Vehicles]  
			WHERE [CustomerId] = @pCustomerId 
			AND [PlateId] = @pPlateId 
			AND [VehicleId] <> @pVehicleId 
			AND [IsDeleted] <> 1  
  
			--Validate vehicle plate is not duplicate for the customer  
			IF @lCustomerPlateCount > 0  
			BEGIN  
				RAISERROR ('PLATE_IS_EXIST', 16, 1)  
			END  
			ELSE  
			BEGIN 
				--Validamos si un vehiculo esta siendo actualizado por un cliente que esta asociado y no por el cliente original del vehiculo
				--Esto evitara que se le actualicen las referencias del vehiculo y por ende evitara errores en los flujos existentes
				IF EXISTS(SELECT 1 FROM [General].[VehiclesByCustomers] WHERE CustomerId = @pCustomerId AND VehicleId = @pVehicleId )	
				BEGIN 
					SET @pIsAClientLinked = 1
				END
				
				IF(@pIsAClientLinked = 0)
				BEGIN 
			 
					IF (@pUserId IS NOT NULL)  
					BEGIN   
						SELECT TOP 1 @lUserIdByVehicle = a.[UserId]  
									,@lVehiclesByUserId = a.[VehiclesByUserId]  
						FROM [General].[VehiclesByUser] a  
						WHERE a.[VehicleId] = @pVehicleId  
						ORDER BY a.[InsertDate] DESC  
       
						IF (@lUserIdByVehicle IS NULL OR @lUserIdByVehicle <> @pUserId)  
						BEGIN   
							UPDATE [General].[VehiclesByUser]  
							SET  [LastDateDriving] = GETUTCDATE()  
								,[ModifyDate] = GETUTCDATE()  
								,[ModifyUserId] = @pLoggedUserId  
								,[EndOdometer] = @pOdometer  
							WHERE  [VehiclesByUserId] = @lVehiclesByUserId  
  
							INSERT INTO [General].[VehiclesByUser]  
							(
								[VehicleId]  
								,[UserId]  
								,[InitialOdometer]  
								,[InsertDate]  
								,[InsertUserId]
							)  
							VALUES 
							(
								@pVehicleId  
								,@pUserId  
								,@pOdometer  
								,GETUTCDATE()  
								,@pLoggedUserId
							)  
						END   
					END   
      
					--Get Previous AdministrativeSpeedLimit to verify if changed  
					DECLARE @OLDOdometer FLOAT  
					SELECT @lAdmSpeedLimitPrev = [AdministrativeSpeedLimit]  
						,@lIntrackReference = [IntrackReference]  
						,@lDeviceReference = [DeviceReference]  
						,@OLDOdometer =  [Odometer]       
					FROM [General].[Vehicles]  
					WHERE [VehicleId] = @pVehicleId  
      
					IF @pOdometerVehicle IS NULL
					BEGIN  
						SET @pOdometerVehicle = @OLDOdometer  
					END  
					
					--Obtenemos los valores anteriores de la Placa y el Chasis
					SELECT 
						@OldPlate = [PlateId],
						@OldChasis = [Chassis],
						@OldDefaultFuelId = [DefaultFuelId]
					FROM [General].[Vehicles] v
					INNER JOIN [General].[VehicleCategories] vc
						ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
					WHERE [VehicleId] = @pVehicleId

					UPDATE [General].[Vehicles]  
					SET  [PlateId] = ISNULL(UPPER(@pPlateId), [PlateId])
						,[Name] = @pName  
						,[CostCenterId] = ISNULL(@pCostCenterId, [CostCenterId])
						,[CustomerId] = @pCustomerId  
						,[VehicleCategoryId] = ISNULL(@pVehicleCategoryId, [VehicleCategoryId])
						,[Active] = @pActive  
						,[Colour] = @pColour  
						,[Chassis] = @pChassis  
						,[LastDallas] = @pLastDallas  
						,[FreightTemperature] = @pFreightTemperature  
						,[FreightTemperatureThreshold1] = @pFreightTemperatureThreshold1  
						,[FreightTemperatureThreshold2] = @pFreightTemperatureThreshold2  
						,[Predictive] = @pPredictive  
						,[AVL] = @pAVL  
						,[PhoneNumber] = @pPhoneNumber  
						,[Insurance] = @pInsurance  
						,[DateExpirationInsurance] = @pDateExpirationInsurance  
						,[CoverageType] = @pCoverageType  
						,[NameEnterpriseInsurance] = @pNameEnterpriseInsurance  
						,[PeriodicityId] = @pPeriodicityId  
						,[Cost] = @pCost  
						,[AdministrativeSpeedLimit] = @pAdministrativeSpeedLimit  
						,[CabinPhone] = @pCabinPhone  
						,[IntrackReference] =ISNULL(@pIntrackReference, [IntrackReference])
						,[Imei] = @pImei  
						,[ModifyDate] = GETUTCDATE()  
						,[ModifyUserId] = @pLoggedUserId  
						,[PullPreviousBudget] = @pPullPreviousBudget  
						,[ExternalId] = @pExternalId  
						--,[Odometer] = @pOdometerVehicle
						,[HasCooler] = @pHasCooler 
						,[TempSensorCount] = @pTempSensorCount 
						,[MinTemperature] = @pMinTemperature
						,[MaxTemperature] = @pMaxTemperature
						,[FleetioId] = @pFleetioId
						,[Depreciation] = @pDepreciation
						,[VehiclePrice] = @pVehiclePrice
					WHERE [VehicleId] = @pVehicleId  
					--AND [RowVersion] = @pRowVersion

					/* UPDATE THE OLD FUELID FIELD WITH THE NEW VALUE */
					UPDATE [Control].[FuelDistribution]
					SET [FuelId] = @lFuelId
					WHERE [VehicleId] = @pVehicleId
					AND [FuelId] = @OldDefaultFuelId

					/* UPDATE THE FUELID FIELD IN CASE THIS NULL */
					UPDATE [Control].[FuelDistribution]
					SET [FuelId] = @lFuelId
					WHERE [VehicleId] = @pVehicleId
					AND [FuelId] is null
					AND [YEAR] = DATEPART(YEAR,GETDATE())
					AND [MONTH] = DATEPART(MONTH,GETDATE())


				END
				ELSE BEGIN
					DECLARE @LogMessage VARCHAR(500) = ''

					SET @LogMessage = CONCAT('[UPDATE_LINKED_VEHICLE] VehicleId = ', CONVERT(VARCHAR(100), @pVehicleId), 
									'| CustomerId = ', CONVERT(VARCHAR(100), @pCustomerId),
									'| ModifyByUserId = ', CONVERT(VARCHAR(100), @pLoggedUserId), 
									'| Message: Vehiculo no se puede actualizar debido a que la operacion la realiza un Cliente asociado, no el Cliente que pertenece al vehiculo.')

					--Inserta el control de logs
					INSERT INTO [General].[EventDBLog] ([ProcessName],[Message],[IsInfo],[IsWarning],[IsError],[LogUTCDateTime])
					VALUES ('[General].[Sp_Vehicles_AddOrEdit]',@LogMessage,0,0,1,GETDATE())

				END -- END @pIsAClientLinked VALIDATION

			END --END DUPLICATE PLATE VALIDATION   
		END 

		SET @pVehicleId = (SELECT ISNULL(@pVehicleId,@tmpvehicleid))		

        IF @@TRANCOUNT > 0 AND @lLocalTran = 1  
		BEGIN  
			COMMIT TRANSACTION			
		END  

		IF @pPlateId IS NULL
		BEGIN
			SELECT @pPlateId = [PlateId]
			FROM [General].[Vehicles]
			WHERE [VehicleId] = @pVehicleId
		END

		EXEC [dbo].[IntrackService_AddOrEditVehicleToIntrack]
			 @pVehicleId = @pVehicleId, 
			 @pCustomerId = @pCustomerId,
			 @pPlate = @pPlateId, 
			 @pChasis = @pChassis, 
			 @pBrandName = @lManufacturer, 
			 @pColor = @pColour, 
			 @pYearModel = @lYear,
			 @pOldPlate = @OldPlate, 
			 @pOldChasis = @OldChasis

    END TRY  
    BEGIN CATCH  
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)  
		BEGIN  
			ROLLBACK TRANSACTION  
		END  
    
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()  
   
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)      
	END CATCH  
          
	SET NOCOUNT OFF  
    SET XACT_ABORT OFF  
END
