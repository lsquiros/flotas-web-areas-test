USE [ECOSystem]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_ConsolidateMaintenanceReport_Retrieve' AND TYPE = 'P')
	DROP PROC [General].[Sp_ConsolidateMaintenanceReport_Retrieve]
GO

-- ================================================================
-- Author:  Stefano Quir�s  
-- Create date: 16/07/20  
-- Description: Retrieve of the New Consolidate Maintenance Report
-- ================================================================
CREATE PROCEDURE [General].[Sp_ConsolidateMaintenanceReport_Retrieve]  --15829, NULL, NULL, 2020, 2, -1
(
	 @pCustomerId INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pVehicleId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON

		DECLARE @lCoversionValue DECIMAL(16,6)= 3.78541
		       ,@lDays INT = CASE WHEN @pYear IS NOT NULL
								       AND @pMonth IS NOT NULL
							 THEN 30
							 ELSE DATEDIFF(DAY, @pStartDate, @pEndDate)
							 END
			   ,@lTimeZoneParameter INT = (SELECT [TimeZone]
									   FROM [General].[Countries] co
									   INNER JOIN [General].[Customers] cu ON co.[CountryId] = cu.[CountryId]
									   WHERE cu.[CustomerId] = @pCustomerId)
		

		SELECT v.[Name] [VehicleName]
		      ,v.[PlateId]
			  ,vc.[Manufacturer] + '/' + vc.[VehicleModel] + '/' + CONVERT(VARCHAR(4), vc.[Year]) [Description]
			  ,MAX(t.[Odometer]) - MIN(t.[Odometer]) [TravelKilometers]
			  ,SUM(t.[Liters]) [Liters]
			  ,CONVERT(DECIMAL(16,2), MAX(t.[Odometer]) - MIN(t.[Odometer]) / SUM(t.[Liters])) [Performance]
			  ,CONVERT(DECIMAL(16,2), (MAX(t.[Odometer]) - MIN(t.[Odometer]) / SUM(t.[Liters])) / @lCoversionValue) [GallonsPerformance]
			  ,(SELECT AVG(pmrv2.[Record]) 
				FROM [General].[PreventiveMaintenanceByVehicle] pmv2
				INNER JOIN [General].[PreventiveMaintenanceCatalog] pmc2
					ON pmv2.[PreventiveMaintenanceCatalogId] = pmc2.[PreventiveMaintenanceCatalogId]
				INNER JOIN [General].[PreventiveMaintenanceRecordByVehicle] pmrv2
					ON pmv2.[PreventiveMaintenanceId] = pmrv2.[PreventiveMaintenanceId]
				INNER JOIN [General].[MaintenanceCatalogTypes] mct2
					ON mct2.[MaintenanceCatalogTypeId] = pmc2.[FilterType]
				WHERE pmv2.[VehicleId] = v.[VehicleId]
				      AND mct2.[MaintenanceCatalogTypeId] = 1
					  AND pmrv2.[Date] BETWEEN @pStartDate AND @pEndDate
				GROUP BY pmrv2.[PreventiveMaintenanceId]
			   ) [PreventiveMaintenanceTotal]
			,(SELECT AVG(pmrv2.[Record]) 
				FROM [General].[PreventiveMaintenanceByVehicle] pmv2
				INNER JOIN [General].[PreventiveMaintenanceCatalog] pmc2
					ON pmv2.[PreventiveMaintenanceCatalogId] = pmc2.[PreventiveMaintenanceCatalogId]
				INNER JOIN [General].[PreventiveMaintenanceRecordByVehicle] pmrv2
					ON pmv2.[PreventiveMaintenanceId] = pmrv2.[PreventiveMaintenanceId]
				INNER JOIN [General].[MaintenanceCatalogTypes] mct2
					ON mct2.[MaintenanceCatalogTypeId]  = pmc2.[FilterType]
				WHERE pmv2.[VehicleId] = v.[VehicleId]
				      AND mct2.[MaintenanceCatalogTypeId] = 2
					  AND pmrv2.[Date] BETWEEN @pStartDate AND @pEndDate
				GROUP BY pmrv2.[PreventiveMaintenanceId]
			   ) [CorrectiveMaintenanceTotal]
			,(SELECT AVG(pmrv2.[Record]) 
				FROM [General].[PreventiveMaintenanceByVehicle] pmv2
				INNER JOIN [General].[PreventiveMaintenanceCatalog] pmc2
					ON pmv2.[PreventiveMaintenanceCatalogId] = pmc2.[PreventiveMaintenanceCatalogId]
				INNER JOIN [General].[PreventiveMaintenanceRecordByVehicle] pmrv2
					ON pmv2.[PreventiveMaintenanceId] = pmrv2.[PreventiveMaintenanceId]
				INNER JOIN [General].[MaintenanceCatalogTypes] mct2
					ON mct2.[MaintenanceCatalogTypeId]  = pmc2.[FilterType]
				WHERE pmv2.[VehicleId] = v.[VehicleId]
				      AND pmrv2.[Date] BETWEEN @pStartDate AND @pEndDate
					  AND pmc2.[Description] = 'Costo de Llantas'
				GROUP BY pmrv2.[PreventiveMaintenanceId]
			   ) [TireCost]
			,(SELECT AVG(pmrv2.[Record]) 
				FROM [General].[PreventiveMaintenanceByVehicle] pmv2
				INNER JOIN [General].[PreventiveMaintenanceCatalog] pmc2
					ON pmv2.[PreventiveMaintenanceCatalogId] = pmc2.[PreventiveMaintenanceCatalogId]
				INNER JOIN [General].[PreventiveMaintenanceRecordByVehicle] pmrv2
					ON pmv2.[PreventiveMaintenanceId] = pmrv2.[PreventiveMaintenanceId]
				INNER JOIN [General].[MaintenanceCatalogTypes] mct2
					ON mct2.[MaintenanceCatalogTypeId]  = pmc2.[FilterType]
				WHERE pmv2.[VehicleId] = v.[VehicleId]
				      AND pmrv2.[Date] BETWEEN @pStartDate AND @pEndDate
					  AND pmc2.[Description] = 'Costo de Bater�as'
				GROUP BY pmrv2.[PreventiveMaintenanceId]
			   ) [BatteryCost]
			,CONVERT(DECIMAL(16,2), CASE WHEN ty.[Name] = 'Quincenal'
									THEN (v.[Cost] / 15) * @lDays
									WHEN ty.[Name] = 'Mensual'
									THEN (v.[Cost] / 30) * @lDays
									WHEN ty.[Name] = 'Bimestral' 
									THEN (v.[Cost] / 60) * @lDays
									WHEN ty.[Name] = 'Trimestral' 
									THEN (v.[Cost] / 90) * @lDays
									WHEN ty.[Name] = 'Semestral' 
									THEN (v.[Cost] / 180) * @lDays
									WHEN ty.[Name] = 'Anual' 
									THEN (v.[Cost] / 360) * @lDays
									END) [Insurance]
		    ,CASE WHEN (DATEPART(YEAR, GETDATE())) - vc.[Year] <= 5
			 THEN (v.[VehiclePrice] / 5) * ((DATEPART(YEAR, GETDATE())) - vc.[Year])
			 ELSE 0
			 END [Depreciation] --v.[Depreciation] * 0.10 * (DATEPART(YEAR, GETDATE())) - vc.[Year] [Depreciation]  --Falta agregar campo depreciation en veh�culos y parametro de depreciacion anual, por ahora pondremos 10%
			,SUM(t.[FuelAmount]) [FuelAmount]
			,c.[Name] [CustomerName]
		    ,pmrv.[Date]
		INTO #Result
		FROM [General].[Vehicles] v
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = v.[CustomerId]
		INNER JOIN [General].[VehicleCategories] vc
			ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
		INNER JOIN [Control].[Transactions] t
			ON t.[VehicleId] = v.[VehicleId]
		LEFT JOIN [General].[PreventiveMaintenanceByVehicle] pmv
			ON pmv.[VehicleId] = v.[VehicleId]
		LEFT JOIN [General].[PreventiveMaintenanceCatalog] pmc
			ON pmv.[PreventiveMaintenanceCatalogId] = pmc.[PreventiveMaintenanceCatalogId]
		LEFT JOIN [General].[PreventiveMaintenanceRecordByVehicle] pmrv
			ON pmv.[PreventiveMaintenanceId] = pmrv.[PreventiveMaintenanceId]
		LEFT JOIN [General].[PreventiveMaintenanceRecordByVehicleDetail] pmrvd
			ON pmrv.[PreventiveMaintenanceRecordByVehicleId] = pmrvd.[PreventiveMaintenanceRecordByVehicleId]
		LEFT JOIN [General].[Types] ty
			ON ty.[TypeId] = v.[PeriodicityId]
		WHERE v.[CustomerId] = @pCustomerId
			  AND (@pVehicleId = -1
				   OR v.[VehicleId] = @pVehicleId)
			  AND (
					t.[IsFloating] IS NULL
					OR t.[IsFloating] = 0
					)
				AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
				AND 1 > (
					SELECT ISNULL(COUNT(1), 0)
					FROM CONTROL.Transactions t2
					WHERE t2.[CreditCardId] = t.[CreditCardId]
						AND t2.[TransactionPOS] = t.[TransactionPOS]
						AND t2.[ProcessorId] = t.[ProcessorId]
						AND (
							t2.IsReversed = 1
							OR t2.IsVoid = 1
							)
					)
				AND (
					t.[TransactionOffline] IS NULL
					OR t.[TransactionOffline] = 0
					)
			  AND (
					(
						@pYear IS NOT NULL
						AND @pMonth IS NOT NULL
						AND DATEPART(m, pmrv.[Date]) = @pMonth
						AND DATEPART(yyyy, pmrv.[Date]) = @pYear
						)
					OR (
						@pStartDate IS NOT NULL
						AND @pEndDate IS NOT NULL
						AND pmrv.[Date] BETWEEN @pStartDate
							AND @pEndDate
						)
					)
        GROUP BY  v.[VehicleId]
		         ,v.[PlateId]
				 ,v.[Name]
				 ,v.[Cost]
				 ,v.[VehiclePrice]
			     ,vc.[Manufacturer]
				 ,vc.[VehicleModel]
				 ,vc.[Year]
				 ,ty.[Name]
				 ,c.[Name]
				 ,pmrv.[Date]

			SELECT *
			      ,CONVERT(DECIMAL(16,2), [PreventiveMaintenanceTotal] + [CorrectiveMaintenanceTotal] + [TireCost] + [BatteryCost] + [Insurance] + [Depreciation]) [CostWithoutFuel]
				  ,CONVERT(DECIMAL(16,2), [PreventiveMaintenanceTotal] + [CorrectiveMaintenanceTotal] + [TireCost] + [BatteryCost] + [Insurance] + [Depreciation] + [FuelAmount]) [TotalCost]
				  ,CONVERT(DECIMAL(16,2), CASE WHEN [TravelKilometers] = 0
										  THEN 0
										  ELSE ([PreventiveMaintenanceTotal] + [CorrectiveMaintenanceTotal] + [TireCost] + [Insurance]) / [TravelKilometers] 
										  END) [OperationCostByKm]
				  ,CONVERT(DECIMAL(16,2), CASE WHEN [TravelKilometers] = 0
										  THEN 0
										  ELSE ([Insurance] + [Depreciation]) / [TravelKilometers] 
										  END) [FinanceCostByKm]
				  ,CONVERT(DECIMAL(16,2), CASE WHEN [TravelKilometers] = 0
										  THEN 0
										  ELSE [FuelAmount] / [TravelKilometers] 
										  END) [FuelCostByKm]
				  ,CONVERT(DECIMAL(16,2), CASE WHEN [TravelKilometers] = 0
										  THEN 0
										  ELSE ([PreventiveMaintenanceTotal] + [CorrectiveMaintenanceTotal] + [TireCost] + [BatteryCost] + [Insurance] + [Depreciation] + [FuelAmount]) / [TravelKilometers] 
										  END) [TotalCostByKm]
			FROM #Result
					
	SET NOCOUNT OFF
	SET XACT_ABORT OFF
END
