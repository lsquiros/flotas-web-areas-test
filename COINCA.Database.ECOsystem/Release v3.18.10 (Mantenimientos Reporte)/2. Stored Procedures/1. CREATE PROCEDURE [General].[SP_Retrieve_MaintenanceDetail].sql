USE ECOsystem
-- =============================================
-- Author:      Jason Bonilla Soto
-- Create Date: 26/06/2020
-- Description: Retrieve Vehicle Maintenance Detail
-- =============================================
CREATE PROCEDURE [General].[SP_Retrieve_MaintenanceDetail]
(
    @pxml VARCHAR(1000)
)
AS
BEGIN		
	DECLARE @lxmlData XML = CONVERT(XML,@pxml)

	SELECT 
		Row.col.value('./@VehicleId', 'INT')[VehicleId],
		Row.col.value('./@CatalogId', 'INT')[CatalogId]
	INTO #data
	FROM @lxmlData.nodes('/xmldata/PreventiveMaintenance') Row(col)

	SELECT
			[PreventiveMaintenanceRecordByVehicleDetailId]
		   ,[PreventiveMaintenanceRecordByVehicleId]
		   ,[PreventiveMaintenanceCostId]
		   ,ISNULL([Description],'') [Description]
		   ,[Cost]
		   ,[Record]
		   ,[InsertUserId]
		   ,[InsertDateId]
		   ,[ModifyUserId]
		   ,[ModifyDateId]
		   ,ISNULL([ServiceProvider],'') [ServiceProvider]
		   ,ISNULL([InvoiceNumber],'') [InvoiceNumber]
		   ,ISNULL([MaintenanceTime],'') [MaintenanceTime]
		   ,ISNULL([Comments],'') [Comments]
		   ,[RecordDollar] as CostDollar
		FROM [General].[PreventiveMaintenanceRecordByVehicleDetail]
		WHERE [PreventiveMaintenanceRecordByVehicleId] IN
		(
			SELECT 
				[PreventiveMaintenanceRecordByVehicleId] 
			FROM [General].[PreventiveMaintenanceRecordByVehicle]
			WHERE [PreventiveMaintenanceId] IN
			(
				SELECT 
					[PreventiveMaintenanceId] 
				FROM [General].[PreventiveMaintenanceByVehicle] 
				WHERE [VehicleId] IN(
										SELECT [VehicleId] 
										FROM #data
									) 
					  AND [PreventiveMaintenanceCatalogId] IN(
																SELECT [CatalogId] 
																FROM #data
															 ) 
			)
		)

END