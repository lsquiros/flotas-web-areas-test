/****** Object:  StoredProcedure [General].[Sp_PreventiveMaintenanceCatalog_Retrieve]    Script Date: 8/10/2020 11:21:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================================================
-- Author:		Cristian Martínez Hernández.
-- Create date: Oct/06/2014
-- Description:	Preventive Maintenance information
-- ====================================================================================================
-- Stefano Quirós Ruiz - OCT/06/2016 - Add Filter to the retrieve (Preventive or Corrective)
-- Gerald Solano - OCT/22/2018 - Add new columns for manage the max range for Odometers and Frecuency Date
-- María de los Ángeles Jiménez - FEB/22/2019 - Include start and end date
-- ====================================================================================================
ALTER PROCEDURE [General].[Sp_PreventiveMaintenanceCatalog_Retrieve]
	  @pCustomerId INT								--@pCustomerID: Customer ID PK from table Customer
	 ,@pFilterType INT = NULL
	 ,@pPreventiveMaintenanceCatalogId INT = NULL	--
	 ,@pKey VARCHAR(250) = NULL
AS
BEGIN	
	SET NOCOUNT ON;
	IF @pFilterType = 0
	BEGIN
		SET @pFilterType = NULL
	END

	IF @pFilterType IS NULL		
	BEGIN
		SELECT 
			a.[PreventiveMaintenanceCatalogId],
			a.[Description],
			a.[FilterType],
			a.[FrequencyKm],
			a.[AlertBeforeKm],
			a.[FrequencyMonth],
			a.[AlertBeforeMonth],
			a.[FrequencyDate],
			a.[AlertBeforeDate],
			a.[Cost],
			a.[RowVersion],
			c.Symbol as [CurrencySymbol],
			mct.[Name] [FilterName],
			a.[HasMaxRange],	
			a.[MaxRangeDate],
			a.[MaxRangeKM],
			a.[StartDate],
			a.[EndDate],
			ct.[Name] [CustomerName],
			a.[DontAllowDelete]
			FROM [General].[PreventiveMaintenanceCatalog] a			
			INNER JOIN [General].[Customers] ct
			ON ct.CustomerId = a.CustomerId
			INNER JOIN [Control].[Currencies] c
			ON c.CurrencyId = ct.CurrencyId
			INNER JOIN [General].[MaintenanceCatalogTypes] mct
			ON a.[FilterType] = mct.[MaintenanceCatalogTypeId]	

			WHERE  a.[CustomerId] = @pCustomerId
					AND (@pPreventiveMaintenanceCatalogId IS NULL 
						OR a.[PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId)
					AND (@pKey IS NULL 
					OR a.[Description] LIKE '%'+@pKey+'%')				
	END
	ELSE	
	BEGIN
		SELECT 
			a.[PreventiveMaintenanceCatalogId],
			a.[Description],
			a.[FilterType],
			a.[FrequencyKm],
			a.[AlertBeforeKm],
			a.[FrequencyMonth],
			a.[AlertBeforeMonth],
			a.[FrequencyDate],
			a.[AlertBeforeDate],
			a.[Cost],
			a.[RowVersion],
			c.Symbol as [CurrencySymbol],
			mct.[Name] [FilterName],
			a.[HasMaxRange],	
			a.[MaxRangeDate],
			a.[MaxRangeKM],
			a.[StartDate],
			a.[EndDate],
			ct.[Name] [CustomerName],
			a.[DontAllowDelete]
			FROM [General].[PreventiveMaintenanceCatalog] a			
			INNER JOIN [General].[Customers] ct
			ON ct.CustomerId = a.CustomerId
			INNER JOIN [Control].[Currencies] c
			ON c.CurrencyId = ct.CurrencyId
			INNER JOIN [General].[MaintenanceCatalogTypes] mct
			ON a.[FilterType] = mct.[MaintenanceCatalogTypeId]	
						
			WHERE  a.[CustomerId] = @pCustomerId
					AND (@pPreventiveMaintenanceCatalogId IS NULL 
						OR a.[PreventiveMaintenanceCatalogId] = @pPreventiveMaintenanceCatalogId)
					AND (@pKey IS NULL 
					OR a.[Description] LIKE '%'+@pKey+'%')	
					AND a.FilterType = @pFilterType		
	END
	SET NOCOUNT OFF;
END


