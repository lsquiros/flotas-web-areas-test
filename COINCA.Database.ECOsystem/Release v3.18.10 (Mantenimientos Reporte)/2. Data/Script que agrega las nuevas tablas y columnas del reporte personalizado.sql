--Insert PreventiveMaintenanceByVehicle

 INSERT INTO [General].[ReportSources]
 VALUES
 (
	'Mantenimientos por Veh�culos'
   ,'General.PreventiveMaintenanceByVehicle'
   ,'PreventiveMaintenanceId'
   ,5
   ,'VehicleId'
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,1
 )

 INSERT INTO [General].[ReportColumns]
 VALUES
 (
	'Pr�xima Revisi�n (Km)'
   ,'NextReviewOdometer'
   ,14
   ,56
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,NULL
   ,NULL
   ,14
   ,0
 )

 INSERT INTO [General].[ReportColumns]
 VALUES
 (
	'Pr�xima Revisi�n (Fecha)'
   ,'NextReviewOdometer'
   ,14
   ,56
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,NULL
   ,NULL
   ,14
   ,0
 )

 --Insert PreventiveMaintenanceCatalog

 INSERT INTO [General].[ReportSources]
 VALUES
 (
	'Mantenimientos'
   ,'General.PreventiveMaintenanceCatalog'
   ,'PreventiveMaintenanceCatalogId'
   ,14
   ,NULL
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,1
 )

 INSERT INTO [General].[ReportColumns]
 VALUES
 (
	'Mantenimiento'
   ,'Description'
   ,15
   ,167
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,NULL
   ,NULL
   ,15
   ,0
 )

 INSERT INTO [General].[ReportColumns]
 VALUES
 (
	'Frecuencia (Km)'
   ,'FrequencyKm'
   ,15
   ,167
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,NULL
   ,NULL
   ,15
   ,0
 )

  INSERT INTO [General].[ReportColumns]
 VALUES
 (
	'Frecuencia (Meses)'
   ,'FrequencyMonth'
   ,15
   ,167
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,NULL
   ,NULL
   ,15
   ,0
 )

   INSERT INTO [General].[ReportColumns]
 VALUES
 (
	'Frecuencia (A�o)'
   ,'FrequencyDate'
   ,15
   ,167
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,NULL
   ,NULL
   ,15
   ,0
 )

    INSERT INTO [General].[ReportColumns]
 VALUES
 (
	'Costo Real'
   ,'Cost'
   ,15
   ,167
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,NULL
   ,NULL
   ,15
   ,0
 )

 --Insert PreventiveMaintenanceCatalogTypes


  INSERT INTO [General].[ReportSources]
 VALUES
 (
	'Tipos de Mantenimientos'
   ,'General.MaintenanceCatalogTypes'
   ,'MaintenanceCatalogTypeId'
   ,15
   ,'FilterType'
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,1
 )

 INSERT INTO [General].[ReportColumns]
 VALUES
 (
	'Tipo de Mantenimiento'
   ,'Name'
   ,16
   ,167
   ,GETDATE()
   ,1
   ,NULL
   ,NULL
   ,NULL
   ,NULL
   ,16
   ,0
 )