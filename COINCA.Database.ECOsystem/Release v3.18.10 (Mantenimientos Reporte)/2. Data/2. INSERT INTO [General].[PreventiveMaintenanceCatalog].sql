USE [ECOSystem]
GO

-- ===============================================================
-- Author:		Stefano Quir�s
-- Create date: 07/08/2020
-- Description:	Insert Corrective Maintenance DontAllowDelete = 1
-- ===============================================================


INSERT INTO [General].[PreventiveMaintenanceCatalog]
(
	[CustomerId]
   ,[Description]
   ,[InsertUserId]
   ,[InsertDate]
   ,[FilterType]
   ,[DontAllowDelete]
)
SELECT c.[CustomerId]
      ,'Costo de Llantas'
	  ,1
	  ,GETDATE()
	  ,2
	  ,1
FROM [General].[Customers] c
INNER JOIN [General].[CustomersByPartner] cp
	ON cp.[CustomerId] = c.[CustomerId]
INNER JOIN [General].[Partners] p
	ON p.[PartnerId] = cp.[PartnerId]
WHERE p.[Name] like '%Flota%'
AND (c.[IsDeleted] = 0
	 OR c.[IsDeleted] IS NULL)




INSERT INTO [General].[PreventiveMaintenanceCatalog]
(
	[CustomerId]
   ,[Description]
   ,[InsertUserId]
   ,[InsertDate]
   ,[FilterType]
   ,[DontAllowDelete]
)
SELECT c.[CustomerId]
      ,'Costo de Bater�as'
	  ,1
	  ,GETDATE()
	  ,2
	  ,1
FROM [General].[Customers] c
INNER JOIN [General].[CustomersByPartner] cp
	ON cp.[CustomerId] = c.[CustomerId]
INNER JOIN [General].[Partners] p
	ON p.[PartnerId] = cp.[PartnerId]
WHERE p.[Name] like '%Flota%'
AND (c.[IsDeleted] = 0
	 OR c.[IsDeleted] IS NULL)