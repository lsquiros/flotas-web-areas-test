USE [ECOSystem]
GO

-- ================================================
-- Author:		Stefano Quir�s
-- Create date: 07/08/2020
-- Description:	Add Columns
-- ================================================

ALTER TABLE [General].[PreventiveMaintenanceCatalog] 
ADD [DontAllowDelete] BIT
