USE [ECOSystem]
GO

-- ================================================
-- Author:		Stefano Quir�s
-- Create date: 19/05/2020
-- Description:	Add Columns
-- ================================================

ALTER TABLE [General].[Vehicles] 
ADD [Depreciation] DECIMAL(16, 3)

GO

ALTER TABLE [General].[Vehicles] 
ADD [VehiclePrice] DECIMAL(16, 2)

