USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardsReportDetail_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardsReportDetail_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author     :	Gerald Solamo
-- Create date: 07/08/2016
-- Description:	Retrieve Credit Cards Report Detailed
-- Henry Retana - 05/24/2019 - Validates Credit Card for Drivers
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_CreditCardsReportDetail_Retrieve]
(
	@pCustomerId INT = NULL,
	@pCountryId INT = NULL,
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL
)
AS
BEGIN	
	SET NOCOUNT ON

	IF @pEndDate IS NOT NULL
	BEGIN
		SET @pEndDate =  @pEndDate + '23:59:59' 
	END 
	
	SELECT DISTINCT a.[CreditCardId],
					a.[CreditCardNumber],
					CASE WHEN v.[PlateId] IS NOT NULL
						 THEN 'Placa: ' + v.[PlateId]
						 ELSE u.[Name] 
					END [Holder],
					c.[Name] [ApplicantName],
					d.[Name] [StatusName],
					a.[InsertDate] [RequestDate],
					a.[ModifyDate] [ModifyDate],
					ISNULL(e.[InsertDate], a.[InsertDate]) [DeliveryDate],
					b.[CardRequestId] [RequestNumber],
					(
						SELECT COUNT(1) 
						FROM [Control].[CreditCardHx] ccx 
						WHERE ccx.[CreditCardId] = a.[CreditCardId] 
						AND ccx.[StatusId] = 7
					) [HasBeenActive]					
	FROM [Control].[CreditCard] a
	INNER JOIN [Control].[CardRequest] b 
		ON a.[CardRequestId] = b.[CardRequestId]
	INNER JOIN [General].[Users] c    
		ON b.[InsertUserId] = c.[UserId]
	INNER JOIN [General].[Status] d      
		ON a.[StatusId] = d.[StatusId]
	LEFT JOIN [Control].[CreditCardHx] e 
		ON a.[CreditCardId] = e.[CreditCardId] AND e.[StatusId] = 5
	INNER JOIN [General].[Customers] f   
		ON a.[CustomerId] = f.[CustomerId]
	LEFT JOIN 	[Control].[CreditCardByVehicle]	 ccv
		ON ccv.[CreditCardId] = a.[CreditCardId]
	LEFT JOIN 	[General].[Vehicles] v
		ON ccv.[VehicleId] = v.[VehicleId]	
	LEFT JOIN 	[Control].[CreditCardByDriver]	ccd
		ON ccd.[CreditCardId] = a.[CreditCardId]
	LEFT JOIN [General].[Users]	u
		ON ccd.[UserId] = u.[UserId]
	WHERE a.[CustomerId] = @pCustomerId
	AND d.[Name] IN 
	(
		'Activa',
		'Bloqueada',
		'Cerrada',
		'Entregada'
	) 
	AND (@pCountryId IS NULL OR f.[CountryId] = @pCountryId)
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,ISNULL(e.[InsertDate], a.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,ISNULL(e.[InsertDate], a.[InsertDate])) = @pYear) 
			OR (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
				AND ISNULL(e.[InsertDate], a.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))

    SET NOCOUNT OFF
END