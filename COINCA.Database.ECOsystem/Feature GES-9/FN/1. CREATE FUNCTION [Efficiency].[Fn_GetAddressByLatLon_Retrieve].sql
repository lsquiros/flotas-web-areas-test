USE [ECOSystemQA]
GO

IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Fn_GetAddressByLatLon_Retrieve]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [Efficiency].[Fn_GetAddressByLatLon_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================================
-- Author:  Henry Retana
-- Create date: 03/10/2016
-- Description: Returns the Location
-- Modify by: Sebastian Quesada - 13/07/2018 - opcion de que retorne la localizacion por comercio
-- Modify by: Marjorie Garbanzo - 17/01/2019 - Add address for stops with reference to nearby commerces.
-- ===============================================================================================
CREATE FUNCTION [Efficiency].[Fn_GetAddressByLatLon_Retrieve]
(
	 @pLongitude DECIMAL(18,12)
	,@pLatitude DECIMAL(18,12)	
	,@pCustomerId INT = NULL
	,@pEventTypeId INT = NULL
)
RETURNS VARCHAR(500)
AS
BEGIN

	DECLARE @lResultLocation  VARCHAR(500)	
	       ,@lLatitudeRange1  DECIMAL(13,10) = 0
	       ,@lLatitudeRange2  DECIMAL(13,10) = 0
	       ,@lLongitudeRange1 DECIMAL(13,10) = 0
	       ,@lLongitudeRange2 DECIMAL(13,10) = 0
		   ,@lRadioDistance INT	= 0
		   ,@lValidateCustomer INT = 0
		   ,@lEventStop INT = 0
		   ,@lCloseCommerceDistance INT = 0
		   	
	SELECT @lRadioDistance = [Value] 
	FROM [General].[ParametersByCustomer] p 
	WHERE p.[CustomerId] = @pCustomerId
	AND p.[Name] = 'RadioDistanceByCustomer'
	AND p.[ResuourceKey] = 'RADIODISTANCE'

	SELECT @lValidateCustomer = p.[Value] 
	FROM [General].[ParametersByCustomer] p 
	WHERE [CustomerId] = @pCustomerId	                                                                         
	AND p.[Name] = 'Active_RadioDistanceByCustomer'
	AND p.[ResuourceKey] = 'ACTIVE_RADIODISTANCE'

	SELECT @lEventStop = [Id] 
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'AGENT_STOP'
	
	SELECT @lCloseCommerceDistance = [NumericValue] 
	FROM [dbo].[GeneralParameters]
	WHERE ParameterId = 'DISTANCE_FOR_COMMERCES'

	DECLARE @tPuntos TABLE 	
						(
						    [Distance] DECIMAL(12,2)
						   ,[Point]   VARCHAR(500)
						   ,[Longitude]	DECIMAL(13,10)
						   ,[Latitude] DECIMAL(13,10)
						)

	SET @lLatitudeRange1 = @pLatitude - 0.01 
	SET @lLatitudeRange2 = @pLatitude + 0.01
	SET @lLongitudeRange1 = @pLongitude - 0.01
	SET @lLongitudeRange2 = @pLongitude + 0.01

	DECLARE @g GEOGRAPHY = 'POINT(' + CAST(@pLongitude AS VARCHAR(MAX)) +' ' + CAST(@pLatitude  AS VARCHAR(MAX)) + ')';	 


	IF @lValidateCustomer = 1 OR (@pEventTypeId IS NOT NULL AND @pEventTypeId = @lEventStop)
    BEGIN
		IF @lValidateCustomer = 1 
			BEGIN
			INSERT INTO @tPuntos
			SELECT TOP(1) ROUND([Geom].STDistance(@g), 0) AS [Distance]
						 ,[Name][Point] 					
						 ,[Longitude]
						 ,[Latitude]					
			FROM [General].[Commerces]
			WHERE [Latitude] BETWEEN @lLatitudeRange1 AND @lLatitudeRange2 
			AND [Longitude] BETWEEN @lLongitudeRange1 AND @lLongitudeRange2 
			AND [Geom].STDistance(@g) <= @lRadioDistance
			AND [Name] IS NOT NULL
			AND [Latitude] IS NOT NULL 
			AND [Longitude] IS NOT NULL
			AND [CustomerId] = @pCustomerId
			ORDER BY [Geom].STDistance(@g)
		END

		IF @pEventTypeId = @lEventStop
		BEGIN
			INSERT INTO @tPuntos
			SELECT TOP(1) ROUND([Geom].STDistance(@g), 0) AS [Distance]
						 ,[Name][Point] 					
						 ,[Longitude]
						 ,[Latitude]					
			FROM [General].[Commerces]
			WHERE [Latitude] BETWEEN @lLatitudeRange1 AND @lLatitudeRange2 
			AND [Longitude] BETWEEN @lLongitudeRange1 AND @lLongitudeRange2 
			AND [Geom].STDistance(@g) <= @lCloseCommerceDistance
			AND [Name] IS NOT NULL
			AND [Latitude] IS NOT NULL 
			AND [Longitude] IS NOT NULL
			AND [CustomerId] = @pCustomerId
			ORDER BY [Geom].STDistance(@g)
		END

		IF NOT EXISTS (SELECT * FROM @tPuntos)
		BEGIN
			INSERT INTO @tPuntos
			SELECT TOP(1)  ROUND(geom.STDistance(@g), 0) AS [Distance]
	  					  ,[Point] 					
	  					  ,[Longitude]
	  					  ,[Latitude]					
			FROM  [dbo].[MapsPointsByCountry]
			WHERE [LATITUDE] BETWEEN @lLatitudeRange1 AND @lLatitudeRange2 
  			AND [LONGITUDE] BETWEEN @lLongitudeRange1 AND @lLongitudeRange2 
			AND geom.STDistance(@g)  < 1000
			ORDER BY geom.STDistance(@g)
		END
	END
	ELSE
    BEGIN
		INSERT INTO @tPuntos
	    SELECT TOP(1)  ROUND(geom.STDistance(@g), 0) AS [Distance]
	  			      ,[Point] 					
	  			      ,[Longitude]
	  			      ,[Latitude]					
	    FROM  [dbo].[MapsPointsByCountry]
	    WHERE [LATITUDE] BETWEEN @lLatitudeRange1 AND @lLatitudeRange2 
  	    AND [LONGITUDE] BETWEEN @lLongitudeRange1 AND @lLongitudeRange2 
		AND geom.STDistance(@g)  < 1000
	    ORDER BY geom.STDistance(@g)
	END

	IF EXISTS (SELECT * FROM @tPuntos)
	BEGIN	
		SELECT @lResultLocation = CONCAT(CASE WHEN ([Distance] / 1000) = 0 THEN CONCAT('','Está al ') END
										,CASE WHEN ([Distance] / 1000) <  1 THEN CONCAT(CAST([Distance] AS DECIMAL(12,0)), ' mts ') 
											  ELSE CONCAT(CAST(([Distance] / 1000) AS DECIMAL(12,1)), ' kms ')
											  END
										,CASE WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 337 AND
												   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 22	THEN 'Norte de '
											  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 23 AND
						   						   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 68	THEN 'Noreste de '
											  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 69 AND
						   						   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 114	THEN 'Este de '
											  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 115 AND
						   						   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 160	THEN 'Sureste de '
											  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 161 AND
						   						   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 206	THEN 'Sur de '
											  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 207 AND
						   						   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 252	THEN 'Suroeste de '
 											  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 253 AND
						   						   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 298	THEN 'Oeste de '
 											  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 299 AND
						   						   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 336	THEN 'Noroeste de '
											  ELSE 'de '
										END										
									  ,[Point])
		FROM  @tPuntos p
		ORDER BY p.[Distance]
	END
	ELSE
	BEGIN
		SET @lLatitudeRange1 = @pLatitude - 0.03 
		SET @lLatitudeRange2 = @pLatitude + 0.03
		SET @lLongitudeRange1 = @pLongitude - 0.03
		SET @lLongitudeRange2 = @pLongitude + 0.03

		INSERT INTO @tPuntos
		SELECT TOP(1)   ROUND(geom.STDistance(@g), 0) AS [Distance]
						,[Point] 					
						,[Longitude]
						,[Latitude]					
		FROM  [dbo].[MapsPointsByCountry]
		WHERE [LATITUDE] BETWEEN @lLatitudeRange1 AND @lLatitudeRange2 
		AND [LONGITUDE] BETWEEN @lLongitudeRange1 AND @lLongitudeRange2 
		AND geom.STDistance(@g)  < 100000
		ORDER BY geom.STDistance(@g)

		SELECT @lResultLocation = CONCAT(CASE WHEN ([Distance] / 1000) < 1 THEN CONCAT(CAST([Distance] AS DECIMAL(12,0)), ' mts ') 
							ELSE CONCAT(CAST(([Distance] / 1000) AS DECIMAL(12,1)), ' kms ')END
						,CASE WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 337 AND
								   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 22	THEN 'Norte de '
							  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 23 AND
						   		   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 68	THEN 'Noreste de '
							  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 69 AND
						   		   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 114	THEN 'Eeste de '
							  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 115 AND
						   		   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 160	THEN 'Sureste de '
							  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 161 AND
						   		   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 206	THEN 'Sur de '
							  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 207 AND
						   		   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 252	THEN 'Suroeste de '
 							  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 253 AND
						   		   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 298	THEN 'Oeste de '
 							  WHEN [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) > 299 AND
						   		   [Efficiency].[CalculateBearingByPoints] ([Latitude], [Longitude], @pLatitude, @pLongitude) < 336	THEN 'Noroeste de '
							  ELSE 'de '
						END
					,[Point])
		FROM  @tPuntos p
		ORDER BY p.[Distance]
	END

	RETURN @lResultLocation
END