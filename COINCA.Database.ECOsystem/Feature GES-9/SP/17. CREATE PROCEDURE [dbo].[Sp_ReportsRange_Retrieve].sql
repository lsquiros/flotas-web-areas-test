USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ReportsRange_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ReportsRange_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/30/2014
-- Description:	Retrieve Reports information
-- Modified by Kevin Pe�a
-- Table var @Reports changed to Temp Table #Reports, final select its already order ASC from first Select from Reports
-- Modify By: Stefano Quir�s - Add the Time Zone Parameter - 12/06/2016
-- Modify: Henry Retana - 20/10/2017
-- Add Temperature to the data information
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to GPSDateTime
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================

CREATE PROCEDURE [dbo].[Sp_ReportsRange_Retrieve]
(
	 @pUnitID bigint = NULL,
	 @pStartDate VARCHAR(21),
	 @pEndDate VARCHAR(21)
)
AS
BEGIN
	
SET NOCOUNT ON

	DECLARE @Device INT = 0
	DECLARE @TimeZone INT = 0
	
	--OBTIENE EL NUMERO DE DEVICE
	SELECT @Device = d.Device 
	FROM  [dbo].[Devices] [d] 
	WHERE  d.[UnitID] = @pUnitID 

	Select @TimeZone = [TimeZone] 
	FROM [General].[Countries] co					   
	INNER JOIN [General].[Customers] cu 
		ON co.[CountryId] = cu.[CountryId] 
	INNER JOIN [General].[Vehicles] v
		ON cu.[CustomerId] = v.[CustomerId]
	WHERE v.DeviceReference = @Device		

	SELECT r.[Report], 
		   r.[Device], 
		   DATEADD(HOUR, @TimeZone, r.[GPSDateTime]) [GPSDateTime],
		   r.[RTCDateTime], 
		   DATEADD(HOUR, @TimeZone, r.[GPSDateTime]) [GPSDateTime],
		   r.[SvrDateTime], 
		   r.[Longitude], 
		   r.[Latitude], 
		   r.[Heading], 
		   r.[ReportID], 
		   r.[Odometer], 
		   r.[HDOP], 
		   r.[InputStatus], 
		   r.[OutputStatus], 
		   r.[VSSSpeed], 
		   r.[AnalogInput], 
		   r.[DriverID], 
		   r.[Temperature1], 
		   r.[Temperature2], 
		   r.[MCC], 
		   r.[MNC], 
		   r.[LAC], 
		   r.[CellID], 
		   r.[RXLevel], 
		   r.[GSMQuality], 
		   r.[GSMStatus], 
		   r.[Satellites], 
		   r.[MainVolt], 
		   r.[BatVolt],	
		   [Efficiency].[Fn_GetOverSpeedByReport] (r.[Report]) [Speeding],
		   [Efficiency].[Fn_GetAddressByLatLon_Retrieve](r.[Longitude], r.[Latitude], v.[CustomerId], NULL) [Direction],
		   CASE WHEN [Efficiency].[Fn_GetVehicleHasCooler](v.VehicleId) = 0
				THEN NULL
				ELSE CAST((CASE WHEN r.[Temperature1] = '999' 
							    THEN 'No Disponible'
								ELSE ISNULL(CAST(CAST(r.[Temperature1] AS DECIMAL(12,2)) AS VARCHAR(100)), 'No Disponible')
						   END) AS VARCHAR(100)) 
		   END AS [Temperature],
		   [Efficiency].[Fn_GetVehicleHasCooler](v.VehicleId) AS [HasCooler],
		   r.[ReportId],
		   CASE WHEN [Efficiency].[Fn_GetVehicleHasCooler](v.VehicleId) = 0
				THEN NULL
				ELSE [Efficiency].[Fn_GetTemperatureSensorsToXML] (r.[Report], v.[DeviceReference])
		   END [SensorsXML],
		   (SELECT [Image]
			FROM [dbo].[ReportsIdAdditionalInformation]
			WHERE [ReportId] = r.[ReportId]) [ReportIdImage],
		   (SELECT [Description]
			FROM [dbo].[ReportsIdAdditionalInformation]
			WHERE [ReportId] = r.[ReportId]) [ReportIdDescription]
	FROM [dbo].[Reports] r
	INNER JOIN [General].[Vehicles] v
		ON 	r.[Device] = v.[DeviceReference]	
	WHERE  r.[Device] = @Device 
	AND	DATEADD(HOUR, @TimeZone, r.[RepDateTime]) >= @pStartDate 
	AND DATEADD(HOUR, @TimeZone, r.[RepDateTime]) <= @pEndDate
	ORDER BY r.[GPSDateTime] ASC

    SET NOCOUNT OFF
END