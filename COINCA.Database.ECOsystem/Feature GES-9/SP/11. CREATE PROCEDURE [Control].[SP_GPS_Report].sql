USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[SP_GPS_Report]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[SP_GPS_Report]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Marco Cabrera
-- Create date: 16/06/2016
-- Description:	Retrieve GPS Report information
-- Modify by:	Gerald Solano
-- Modify date: 25/08/2016
-- Description:	Fix the registers when the state is "Apagado" the property MaxSpeed is Zero
-- Modify by: Henry Retana  03/10/16
-- Add the location function to the resulset. 
-- Modify By: Stefano Quir�s - Add the Time Zone Parameter - 12/06/2016
-- Modify By: Marco Cabrera - Add the last unfinished change event 01/11/2017
-- Modify By: Henry Retana - Change the way to identify the stops 06/02/2017
-- Modify By: Albert EStrada - add hightemperature, lowtemperature, panicbutton Oct/23/2017
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to RepDateTime
-- Modify: Esteban Sol�s - 03/04/2018  
-- Added NO COUNT ON/OFF
-- Modify: Henry Retana - 03/04/2018
-- Change Reports when get max speed
-- Modifica: �lvaro Zamora - 17/07/2018
-- Cambio: Se le inyecta una funci�n como columna, para que calcule el total de paradas que cumplen
--			con el criterio del tiempo.
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================

CREATE PROCEDURE [Control].[SP_GPS_Report] 
(
	@pVehicleId INT,
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL
) 
AS
BEGIN
	
	SET NOCOUNT ON

	DECLARE @vIterator INT,
			@vLastIterator INT = 1,
			@vDevice INT,
			@vDateIni Datetime,
			@vDateFin Datetime,
			@lTimeZoneParameter INT, 
			@pCounter INT,
			@pIsAttrack INT = 0,
			@lStopDetail INT = NULL,
			@lSuddenStops INT = NULL,
			@lAbruptTurns VARCHAR(20) = NULL,
			@lQuickAccelerations INT = NULL,			
			@lPanicButtons INT = NULL,
			@lLowTemperaturas INT = NULL,
			@lHighTemperatures INT = NULL,
			@lOnDesc VARCHAR(10) = 'Encendido',
			@lOffDesc VARCHAR(10) = 'Apagado',
			@lStopTime INT = 2,
			@lCustomerId INT=NULL

	SELECT @pIsAttrack = COUNT(*) 
	FROM [General].[Vehicles]
	WHERE VehicleId = @pVehicleId
		AND DeviceReference IN (SELECT [Device]
								FROM [dbo].[Devices] 
								WHERE [UnitId] IN (SELECT [TerminalId]
												   FROM [dbo].[DispositivosAVL] 
												   WHERE [Modelo] = 18))

	SELECT @lStopDetail = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'STOPDETAIL_PARAMETER'
	SELECT @lSuddenStops = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'SUDDENSTOPS_PARAMETER'
	SELECT @lAbruptTurns = [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'ABRUPTTURNS_PARAMETER'
	SELECT @lQuickAccelerations = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'QUICKACCELERATIONS_PARAMETER'

	SELECT @lPanicButtons = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'PANIC_BUTTON'
	SELECT @lLowTemperaturas = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'LOW_TEMPERATURE'
	SELECT @lHighTemperatures = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'HIGT_TEMPERATURE'
		
	CREATE TABLE #Data
	(
		[Id] INT IDENTITY NOT NULL,
		[RepDateTime] DATETIME,
		[Latitude] FLOAT,
		[Longitude] FLOAT,
		[Odometer] FLOAT,
		[InputStatus] TINYINT,
		[VSSSpeed] FLOAT, 
		[ReportId] INT
	)

	CREATE TABLE #Result
	(
		[Id] INT IDENTITY,
		[Evento] VARCHAR(20),
		[FechaInicial] DATETIME,
		[FechaFinal] DATETIME,
		[Latitud] FLOAT,
		[Longitud] FLOAT,
		[Recorrido] FLOAT,
		[VeloMax] FLOAT,
		[Stops] INT,
		[SuddenStops] INT, 
		[AbruptTurns] INT,
		[QuickAccelerations] INT,
		[LowTemperature] INT,
		[HighTemperature] INT,
		[PanicButton] INT
	)

	SELECT
		@vDevice = [DeviceReference],
		@lCustomerId = [CustomerId]
	FROM [General].[Vehicles] 
	WHERE [VehicleId] = @pVehicleId

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	INNER JOIN [General].[Vehicles] v
		ON cu.[CustomerId] = v.[CustomerId]
	WHERE v.[VehicleId] = @pVehicleId
								 

	IF (@pYear IS NOT NULL AND @pMonth IS NOT NULL) 
	BEGIN 
		SET @vDateIni = DATEADD(hh, @lTimeZoneParameter * -1, CAST(CAST(@pYear AS VARCHAR) + '-' + CAST(@pMonth AS VARCHAR) + '-' + CAST(1 AS VARCHAR) AS DATETIME))
		SET @vDateFin = DATEADD(hh, @lTimeZoneParameter * -1, DATEADD(m, 1, @vDateIni))
	END
	ELSE
	BEGIN
		SET @vDateIni = DATEADD(hh, @lTimeZoneParameter * -1, @pStartDate)
		SET @vDateFin = DATEADD(hh, @lTimeZoneParameter * -1, @pEndDate)
	END
	
	IF(@vDateIni IS NOT NULL AND @vDateFin IS NOT NULL)
	BEGIN
		INSERT INTO #Data
		(
			[RepDateTime],
			[Latitude],
			[Longitude],
			[Odometer],
			[InputStatus],
			[VSSSpeed], 
			[ReportId] 			
		) 
		SELECT	[RepDateTime],
				[Latitude],
				[Longitude],
				[Odometer],
				[InputStatus],
				[VSSSpeed], 
				[ReportId] 			
		FROM [dbo].[Reports] 
		WHERE [Device] = @vDevice 
		AND [RepDateTime] BETWEEN @vDateIni AND @VDateFin
		ORDER BY [RepDateTime] ASC

		SELECT @pCounter = COUNT(1) 
		FROM #Data

		IF @pCounter = 0 RETURN

		SELECT t1.[Id],
			   t1.[RepDateTime], 
			   t1.[Latitude], 
			   t1.[Longitude], 
			   t1.[InputStatus] 
		INTO #ChangeEvent 
		FROM #Data t1, #Data t2 
		WHERE t1.[Id] = (t2.[Id] + 1) 
		AND (t1.[InputStatus] % 2) != (t2.[InputStatus] % 2)

		SELECT @vIterator = MIN([Id]) 
		FROM #ChangeEvent

		WHILE (@vIterator IS NOT NULL)
		BEGIN
			DECLARE @vIdentidad INT

			INSERT INTO #Result
			(
				[FechaInicial], 
				[FechaFinal], 
				[Recorrido], 
				[VeloMax]
			)
			SELECT MIN([RepDateTime]),
				   MAX([RepDateTime]), 
				   (MAX([Odometer]) - MIN([Odometer])), 
				   MAX([VSSSpeed])
			FROM #Data 
			WHERE [Id] BETWEEN @vLastIterator AND @vIterator

			SET @vIdentidad = @@Identity

			IF @pIsAttrack = 0
			BEGIN 
				
				UPDATE #Result 
				SET #Result.[SuddenStops] = v.[SuddenStops]
				FROM (
						SELECT COUNT([VSSSpeed]) [SuddenStops] 
						FROM #Data 
						WHERE [ReportId] = @lSuddenStops 
						AND [Id] BETWEEN @vLastIterator AND @vIterator
					  ) v
				WHERE #Result.[Id] = @vIdentidad

				UPDATE #Result 
				SET #Result.[AbruptTurns] = v.[AbruptTurns]
				FROM (
						SELECT COUNT([VSSSpeed]) [AbruptTurns] 
						FROM #Data 
						WHERE [ReportId] IN (SELECT [items] 
										     FROM [dbo].[Fn_Split](@lAbruptTurns, ','))
						AND [Id] BETWEEN @vLastIterator AND @vIterator
					  ) v
				WHERE #Result.[Id] = @vIdentidad

				UPDATE #Result 
				SET #Result.[QuickAccelerations] = v.[QuickAccelerations]
				FROM (
						SELECT COUNT([VSSSpeed]) [QuickAccelerations] 
						FROM #Data 
						WHERE [ReportId] = @lQuickAccelerations 
						AND [Id] BETWEEN @vLastIterator AND @vIterator
					  ) v
				WHERE #Result.[Id] = @vIdentidad

				--TEMPERATURES
				UPDATE #Result 
				SET #Result.[HighTemperature] = (
													SELECT COUNT([VSSSpeed]) 
													FROM #Data 
													WHERE [ReportId] = @lHighTemperatures 
													AND [Id] BETWEEN @vLastIterator AND @vIterator
											     )
				WHERE #Result.[Id] = @vIdentidad

				UPDATE #Result 
				SET #Result.[LowTemperature] = (
													SELECT COUNT([VSSSpeed]) 
													FROM #Data 
													WHERE [ReportId] = @lLowTemperaturas 
													AND [Id] BETWEEN @vLastIterator AND @vIterator
											  )
				WHERE #Result.[Id] = @vIdentidad

				UPDATE #Result 
				SET #Result.[PanicButton] = (
											  SELECT COUNT([VSSSpeed]) 
											  FROM #Data 
											  WHERE [ReportId] = HighTemperature 
											  AND [Id] BETWEEN @vLastIterator AND @vIterator
										   )
			END

			UPDATE #Result 
			SET #Result.[Evento] = v.[Evento], 
				#Result.[Latitud] = v.[Latitude], 
				#Result.[Longitud] = v.[Longitude]
			FROM (
					SELECT (CASE [InputStatus] % 2 
								WHEN 0 
								THEN @lOnDesc
								ELSE @lOffDesc
							END) [Evento],							
							[Latitude], 
							[Longitude]
					FROM #ChangeEvent 
					WHERE [Id] = @vIterator
				) v
			WHERE #Result.[Id] = @vIdentidad

			DELETE FROM #ChangeEvent 
			WHERE [Id] = @vIterator

			SET @vLastIterator = @vIterator

			SELECT @vIterator = MIN([Id]) 
			FROM #ChangeEvent
		END

		SELECT @vIterator = MAX(Id) 
		FROM #Data

		INSERT INTO #Result
		(
			[FechaInicial], 
			[FechaFinal], 
			[Recorrido], 
			[VeloMax]
		)
		SELECT MIN([RepDateTime]),
				MAX([RepDateTime]), 
				(MAX([Odometer]) - MIN([Odometer])), 
				MAX([VSSSpeed])
		FROM #Data 
		WHERE [Id] BETWEEN @vLastIterator AND @vIterator

		SET @vIdentidad = @@Identity

		IF @pIsAttrack = 0
		BEGIN 
						
			UPDATE #Result 
			SET #Result.[SuddenStops] = v.[SuddenStops]
			FROM (
					SELECT COUNT([VSSSpeed]) [SuddenStops] 
					FROM #Data 
					WHERE [ReportId] = @lSuddenStops 
					AND [Id] BETWEEN @vLastIterator AND @vIterator
					) v
			WHERE #Result.[Id] = @vIdentidad

			UPDATE #Result 
			SET #Result.[AbruptTurns] = v.[AbruptTurns]
			FROM (
					SELECT COUNT([VSSSpeed]) [AbruptTurns] 
					FROM #Data 
					WHERE [ReportId] IN (SELECT [items] 
										    FROM [dbo].[Fn_Split](@lAbruptTurns, ','))
					AND [Id] BETWEEN @vLastIterator AND @vIterator
					) v
			WHERE #Result.[Id] = @vIdentidad

			UPDATE #Result 
			SET #Result.[QuickAccelerations] = v.[QuickAccelerations]
			FROM (
					SELECT COUNT([VSSSpeed]) [QuickAccelerations] 
					FROM #Data 
					WHERE [ReportId] = @lQuickAccelerations 
					AND [Id] BETWEEN @vLastIterator AND @vIterator
				 ) v
			WHERE #Result.[Id] = @vIdentidad

			--TEMPERATURES
			UPDATE #Result 
			SET #Result.[HighTemperature] = (
												SELECT COUNT([VSSSpeed]) 
												FROM #Data 
												WHERE [ReportId] = @lHighTemperatures 
												AND [Id] BETWEEN @vLastIterator AND @vIterator
										    )
			WHERE #Result.[Id] = @vIdentidad

			UPDATE #Result 
			SET #Result.[LowTemperature] = (
												SELECT COUNT([VSSSpeed]) 
												FROM #Data 
												WHERE [ReportId] = @lLowTemperaturas 
												AND [Id] BETWEEN @vLastIterator AND @vIterator
											)
			WHERE #Result.[Id] = @vIdentidad

			UPDATE #Result 
			SET #Result.[PanicButton] = (
											SELECT COUNT([VSSSpeed]) 
											FROM #Data 
											WHERE [ReportId] = HighTemperature 
											AND [Id] BETWEEN @vLastIterator AND @vIterator
										)
		END

		UPDATE #Result 
		SET #Result.[Evento] = v.[Evento], 
			#Result.[Latitud] = v.[Latitude], 
			#Result.[Longitud] = v.[Longitude]
		FROM (
				SELECT (CASE [InputStatus] % 2 
							WHEN 0 
							THEN @lOnDesc
							ELSE @lOffDesc
						END) [Evento],							
						[Latitude], 
						[Longitude]
				FROM #ChangeEvent 
				WHERE [Id] = @vIterator
			) v
		WHERE #Result.[Id] = @vIdentidad
	END
			
	UPDATE #Result 
	SET [Stops] = 0, 
		[VeloMax] = 0 
	WHERE [Evento] = @lOffDesc

	SELECT  [Id] [GPSId]
		   ,DATEADD(hh, @lTimeZoneParameter, [FechaInicial]) [StartDate]
	       ,DATEADD(hh, @lTimeZoneParameter, [FechaFinal]) [EndDate]
		   ,[Latitud]
		   ,[Longitud]
		   ,[Evento] [Status]
		   ,[VeloMax] [MaxSpeed]
		   ,[Recorrido] [Kilometers]
		   ,ISNULL([Efficiency].[Fn_GetStopsByLapse_Retrieve](@vDevice, @vDateIni, @vDateFin, @lCustomerId), '') AS [Stops]
		   ,[SuddenStops]
		   ,[AbruptTurns]
		   ,[QuickAccelerations]
		   ,ISNULL([Efficiency].[Fn_GetAddressByLatLon_Retrieve]([Longitud], [Latitud], NULL, NULL), '') [Location]
		   ,[LowTemperature]
		   ,[HighTemperature]
		   ,[PanicButton]
	FROM #Result
	ORDER BY 2

	--REMOVE TABLES
	DROP TABLE #Data
	DROP TABLE #ChangeEvent
	DROP TABLE #Result

	SET NOCOUNT OFF
END