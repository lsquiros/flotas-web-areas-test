USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleGPSAlertReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_VehicleGPSAlertReport_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 14/11/2017
-- Description:	Retrieve Vehicle information
-- Modify 11/15/2017 - Esteban Solis - Added order by date to result set
-- Stefano Quir�s - Add the Detail Stops to the Alert Report - 10/11/18
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================

CREATE PROCEDURE [Efficiency].[Sp_VehicleGPSAlertReport_Retrieve]
(
	 @pCustomerId INT
    ,@pVehicleId INT = NULL
    ,@pCostCenterId INT = NULL
    ,@pStartDate DATETIME
    ,@pEndDate DATETIME
    ,@pTemperature BIT = NULL
    ,@pPanicButton BIT = NULL
	,@pStopsDetail BIT = NULL
    ,@pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON

	--DECLARE VARIABLES
	DECLARE @lDevices TABLE ([Device] BIGINT) 
	DECLARE @lReports TABLE ([ReportId] INT)
	DECLARE	@Results TABLE (items INT)
	DECLARE @lTimeZoneParameter INT
	DECLARE @count INT
	DECLARE @lStopTime INT = 2
	DECLARE @lStopId INT

	--DYNAMIC FILTERS
	INSERT INTO	@Results
	EXEC [dbo].[Sp_UserDynamicFilter_Retrive] @pUserId, 'VH', @pCustomerId
	SET  @count = (SELECT COUNT(1) 
				   FROM	@Results)

	--GET THE TIMEZONE FOR THE CUSTOMERS
	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	SELECT @lStopTime = ISNULL([Value], 2)
	FROM [General].[ParametersByCustomer] p
	INNER JOIN [General].[Vehicles] v
		ON p.[CustomerId] = v.[CustomerId]
	WHERE v.[VehicleId] = @pVehicleId
	AND p.[Name] = 'VehicleStopLapse'
	AND p.[ResuourceKey] = 'VEHICLESTOPLAPSE'


	--VALIDATES TEMPERATURE FLAG 
	IF @pTemperature  = 1 
	BEGIN
		INSERT INTO @lReports
		SELECT CONVERT(INT, [Value]) 
		FROM [dbo].[GeneralParameters] 
		WHERE [ParameterID] = 'HIGT_TEMPERATURE'

		INSERT INTO @lReports
		SELECT CONVERT(INT, [Value]) 
		FROM [dbo].[GeneralParameters] 
		WHERE [ParameterID] = 'LOW_TEMPERATURE'
	END	

	--VALIDATES PANICBUTTON FLAG
	IF @pPanicButton = 1 
	BEGIN
		INSERT INTO @lReports
		SELECT CONVERT(INT, [Value]) 
		FROM [dbo].[GeneralParameters] 
		WHERE [ParameterID] = 'PANIC_BUTTON'
	END

	IF @pStopsDetail = 1
	BEGIN
		SELECT @lStopId = CONVERT(INT, [Value]) 
		FROM [dbo].[GeneralParameters] 
		WHERE [ParameterID] = 'STOPDETAIL_PARAMETER'
	END
	
	--GET THE DEVICES BASED ON THE COSTCENTER OR THE VEHICLE ID 
	IF @pCostCenterId = -1 OR @pCostCenterId IS NOT NULL
	BEGIN 
		INSERT INTO @lDevices
		SELECT [DeviceReference]
		FROM [General].[Vehicles]
		WHERE [CustomerId] = @pCustomerId
		AND (@count = 0 OR [VehicleId] IN (SELECT [items] 
						 				   FROM @Results))
		AND (@pCostCenterId = -1 OR [CostCenterId] = @pCostCenterId) 
		AND ([DeviceReference] IS NOT NULL AND [DeviceReference] > 0) 
		AND [Active] = 1
		AND [IsDeleted] = 0
	END
	ELSE 
	BEGIN
		INSERT INTO @lDevices
		SELECT [DeviceReference]
		FROM [General].[Vehicles]
		WHERE [CustomerId] = @pCustomerId
		AND (@count = 0 OR [VehicleId] IN (SELECT [items] 
						 				   FROM @Results))
		AND [VehicleId] = @pVehicleId
		AND ([DeviceReference] IS NOT NULL AND [DeviceReference] > 0) 
		AND [Active] = 1
		AND [IsDeleted] = 0
	END

	SELECT  [Device]
		   ,DATEADD(HOUR, @lTimeZoneParameter, [RepDateTime]) AS [Date]
		   ,[ReportID]
		   ,[Longitude]
		   ,[Latitude]
	INTO #Reports
	FROM [dbo].[Reports] r
	WHERE r.[Device] IN (SELECT [Device]
						 FROM @lDevices)
	AND r.[ReportId] IN (SELECT [ReportId] 
						 FROM @lReports)
	AND r.[RepDateTime] BETWEEN DATEADD(HOUR, @lTimeZoneParameter * -1, @pStartDate) 
						  AND DATEADD(DAY, 1, DATEADD(HOUR, @lTimeZoneParameter * -1, @pEndDate))
	UNION
	SELECT  [Device]
		   ,DATEADD(HOUR, @lTimeZoneParameter, [RepDateTime]) AS [Date]
		   ,[ReportID]
		   ,[Longitude]
		   ,[Latitude]	
		    --INTO #Reports
			FROM [dbo].[Reports] r
			WHERE r.[Device] IN (SELECT [Device]
						 FROM @lDevices)
			AND r.[ReportId] = @lStopId
			AND r.[RepDateTime] BETWEEN DATEADD(HOUR, @lTimeZoneParameter * -1, @pStartDate) 
						  AND DATEADD(DAY, 1, DATEADD(HOUR, @lTimeZoneParameter * -1, @pEndDate))
			AND DATEDIFF(MI, r.[RepDateTime], (SELECT TOP 1 [RepDateTime] 
										   FROM [dbo].[Reports] 
										   WHERE ([RepDateTime] > r.[RepDateTime]) 
										   AND [ReportId] = 104
										   AND [Device] = r.[Device]
										   ORDER BY [RepDateTime] ASC)) >= @lStopTime

	--RETURN FINAL DATA 
	SELECT  v.[Name]
		   ,r.[Date]
		   ,[General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], r.[Date], NULL) [EncryptDriverName]
		   ,[Efficiency].[Fn_GetAddressByLatLon_Retrieve](r.[Longitude], r.[Latitude], @pCustomerId, NULL) [Address]
		   ,r.[ReportId]
		   ,ra.[Description]
		   ,r.[Latitude]
		   ,r.[Longitude]
		   ,cc.[CostCenterId] 
		   ,cc.[Name] [CostCenterName]
	FROM #Reports r
	INNER JOIN [General].[Vehicles] v
		ON r.[Device] = [DeviceReference]
	INNER JOIN [General].[VehicleCostCenters] cc
		ON v.[CostCenterId] = cc.[CostCenterId]
	LEFT JOIN [dbo].[ReportsIdAdditionalInformation] ra
		ON r.[ReportId] = ra.[ReportId]
	ORDER BY r.[Date]
	DROP TABLE #Reports	
	
    SET NOCOUNT OFF
END
