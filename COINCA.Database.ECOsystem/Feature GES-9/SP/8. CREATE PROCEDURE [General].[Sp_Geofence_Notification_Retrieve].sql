USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_Geofence_Notification_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_Geofence_Notification_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_Geofence_Notification_Retrieve] 
(
	@pVehicleId INT
   ,@pCustomerId INT
   ,@pGeofenceId INT
   ,@pInOrOut BIT
   ,@pPoint GEOMETRY
   ,@pLongitude DECIMAL(16, 13)
   ,@pLatitude DECIMAL(16, 13)
   ,@pVSSSpeed FLOAT
   ,@pGPSDateTime DATETIME
)
AS
BEGIN
	SET NOCOUNT ON		

		DECLARE @lDistanceBetweenPoints FLOAT = 0
		DECLARE @lGeofenceINMinimunSpeed FLOAT = 0
		DECLARE @lTimeZoneParameter INT

		--Data used on the return  
		DECLARE @lAlarmId INT = NULL
		DECLARE @lAlarmTriggerId INT = NULL

		DECLARE @lPlateId VARCHAR(20) = NULL
		DECLARE @lDescription VARCHAR(500) = NULL
		DECLARE @lTimeLastReport DATETIME = NULL
		DECLARE @lPhone VARCHAR(100) = NULL
		DECLARE @lEmail VARCHAR (MAX) = NULL
		DECLARE @lInOut VARCHAR(50) = NULL
		DECLARE @lIn BIT = NULL
		DECLARE @lOut BIT = NULL
		DECLARE @lGeofenceType VARCHAR(100) = NULL 
		DECLARE @lLastLongitude DECIMAL(16, 10)
		DECLARE @lLastLatitude DECIMAL(16, 10)
		DECLARE @lLocation VARCHAR (MAX) = NULL
		DECLARE @lSendEmail BIT = 0

		SELECT @lPlateId = [PlateId] 
		FROM [General].[Vehicles] 
		WHERE [VehicleId] = @pVehicleId
				
		SELECT @lDistanceBetweenPoints = [NumericValue] 
		FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'GEOFENCE_IN_DISTANCE'

		SELECT @lGeofenceINMinimunSpeed = [NumericValue] 
		FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'GEOFENCE_MINIMUN_SPEED'

		SELECT @lTimeZoneParameter = [TimeZone] 
		FROM [General].[Countries] co 
		INNER JOIN [General].[Customers] cu
			ON co.[CountryId] = cu.[CountryId]		
		WHERE [CustomerId] = @pCustomerId

		--Selecciona la informacion de la alerta configurada para las GEOCERCAS. Obtiene la alarma, el tipo (GEOCERCA) y la configuracion de la GEOCERCA
	
		SELECT @lAlarmId = b.[AlarmId],
			   @lAlarmTriggerId = a.[TypeId],
			   @lPhone = b.[Phone],
			   @lEmail = b.[Email],
			   @lDescription = c.[Name],						   
			   @lGeofenceType = d.[Name],
			   @lLocation = [Efficiency].[Fn_GetAddressByLatLon_Retrieve](@pLongitude, @pLatitude, @pCustomerId, NULL),
			   @lIn = c.[In],
			   @lOut = c.[Out],
			   @lInOut = CASE WHEN @pInOrOut = 1 
							THEN 'Entrado'
						 ELSE 'Salido'
						 END
		FROM [General].[Types] a 
		INNER JOIN [General].[Alarms] b 
		ON a.[TypeId] = b.[AlarmTriggerId]
		INNER JOIN [Efficiency].[GeoFences] c 
		ON b.[EntityId] = c.[GeoFenceId]
		LEFT JOIN [General].[GeoFencesTypes] d
		ON d.[Id] = c.[GeoFenceTypeId]
		WHERE c.[GeofenceId] = @pGeofenceId     
		AND @pVSSSpeed > @lGeofenceINMinimunSpeed		

		IF @pVSSSpeed > @lGeofenceINMinimunSpeed	
		BEGIN
			-- MEJORA: SE LIGA EL VEHICULO CON LA GEOCERCA.
			IF EXISTS(SELECT * FROM [Efficiency].[GeoFenceReportAlarm] WHERE [VehicleId] = @pVehicleId AND [GeofenceId] = @pGeofenceId)
			BEGIN

				IF EXISTS(SELECT * FROM [Efficiency].[GeoFenceReportAlarm] 
						  WHERE [VehicleId] = @pVehicleId
						  AND [GeofenceId] = @pGeofenceId --> Liga el veh�culo y la geocerca.
						  AND @pInOrOut <> [InOrOut]
						  AND geography::STPointFromText(CONVERT(VARCHAR(200), @pPoint), 4326)
						   				.STDistance(
						   					geography::STPointFromText('POINT(' + CAST([Longitude] AS VARCHAR(20)) + ' ' + CAST([Latitude] AS VARCHAR(20)) + ')', 4326)
						   		) >= @lDistanceBetweenPoints)

				BEGIN
					UPDATE [Efficiency].[GeoFenceReportAlarm] 
					SET [Longitude] = @pLongitude
					   ,[Latitude] = @pLatitude
					   ,[GPSDateTime] = @pGPSDateTime
					   ,[LastLongitude] = @pLongitude
					   ,[LastLatitude] = @pLatitude
					   ,[LastGPSDateTime] = @pGPSDateTime
					   ,[ModifyDate] = GETDATE()
					   ,[InOrOut] = @pInOrOut
			        WHERE [VehicleId] = @pVehicleId
					AND [GeofenceId] = @pGeofenceId --> Liga el veh�culo y la geocerca.

					SET @lSendEmail = 1
				END
			 END
			 ELSE
			 BEGIN
				IF (@pInOrOut = 1)
				BEGIN
					INSERT INTO [Efficiency].[GeoFenceReportAlarm] 
						(
							[VehicleId],
							[GeofenceId],
							[GPSDateTime], 
							[Longitude], 
							[Latitude], 
							[LastGPSDateTime], 
							[LastLongitude], 
							[LastLatitude],
							[InsertDate],
							[InOrOut]
						 )
						VALUES
						(
							@pVehicleId
							,@pGeofenceId
							,@pGPSDateTime
							,@pLongitude
							,@pLatitude
							,@pGPSDateTime
							,@pLongitude
							,@pLatitude	
							,GETDATE()
							,@pInOrOut
						)
					SET @lSendEmail = 1
				END				
			END

			IF @lSendEmail = 1 AND (@pInOrOut = @lIn
									OR (@lOut = 1 
										AND @pInOrOut = 0))
			BEGIN
				EXEC [General].[Sp_SendAlarm] 
				@pAlarmId = @lAlarmId
			   ,@pAlarmTriggerId = @lAlarmTriggerId
			   ,@pCustomerId = @pCustomerId
			   ,@pPlate = @lPlateId
			   ,@pDescription = @lDescription
			   ,@pTimeLastReport = @lTimeLastReport
			   ,@pPhone = @lPhone
			   ,@pEmail = @lEmail
			   ,@pInOut = @lInOut
			   ,@pValue = 400
			   ,@plongitude = @pLongitude
			   ,@platitude = @pLatitude
			   ,@pLocation = @lLocation	
			END
		END		

	SET NOCOUNT OFF
END
