USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_RoutesPoints_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_RoutesPoints_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/29/2014
-- Description:	Retrieve RoutesPoints information
-- Henry Retana - 17/10/17
-- Add address and description to the retrieve
-- Henry Retana - 23/03/18
-- Add Commerce Information
--  Modify By : Sebastian Quesada - 16/07/2018 - add parameter customerId for function [Efficiency].[Fn_GetAddressByLatLon_Retrieve]
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================

CREATE PROCEDURE [Efficiency].[Sp_RoutesPoints_Retrieve]  
(
	 @pRouteId INT = NULL,
	 @pPointId int = null
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT r.[PointId]
		  ,r.[RouteId]
		  ,r.[PointReference]
		  ,r.[Order]
		  ,r.[Name]
		  ,CONVERT(varchar(20), r.[Time]) as [Time]
		  ,CONVERT(varchar(50), r.[Latitude]) as [Latitude]
		  ,CONVERT(varchar(50), r.[Longitude]) as [Longitude]
		  ,r.[StopTime]
		  ,'' [Description]
		  ,[Efficiency].[Fn_GetAddressByLatLon_Retrieve] (r.[Longitude], r.[Latitude], cu.[CustomerId], NULL) [Address]
		  ,[Efficiency].[Fn_RetrieveCountCommerces] (r.[Longitude], r.[Latitude],r.[PointId]) [IsCommerce]
		  ,c.[Id] [CommerceId]
		  ,c.[Name] [CommerceName]
		  ,cu.[CustomerId]
	FROM [Efficiency].[RoutesPoints] r
	LEFT JOIN [General].[Commerces] c
		ON r.[CommerceId] = c.[Id]
    LEFT JOIN [General].[Customers] cu
	    ON cu.[CustomerId] = c.[CustomerId]
	WHERE (@pRouteId IS NULL OR r.[RouteId] = @pRouteId  ) 
	AND (@pPointId IS NULL OR r.pointid = @pPointId)
	
    SET NOCOUNT OFF
END
 
  
