USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_PassControl_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_PassControl_Retrieve] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/30/2014
-- Description:	Retrieve PassControl information
-- Modify By: Stefano Quir�s - Delete a validation that retrieve data only when the vehicle out of
-- the marked zone in the map - 01/04/17  
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to GPSDateTime
-- Modify: Marjorie Garbanzo - 31/05/2018
-- Identificar los eventos de encendio y apagado dentro de la zona determinada.
-- Modify : Sebastian Quesada - 17/04/2018 - add parameter of customerId to function [Efficiency].[Fn_GetAddressByLatLon_Retrieve]
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve]
-- ================================================================================================

CREATE PROCEDURE [dbo].[Sp_PassControl_Retrieve]
(
	@pLongitude FLOAT,
	@pLatitude FLOAT,
	@pDistance FLOAT,
	@pUnitId VARCHAR(MAX),
	@pStartDate VARCHAR(21),
	@pEndDate VARCHAR(21),
	@pEventOn BIT,
	@pEventOff BIT
)
AS
BEGIN
	DECLARE @i bigint = 0
	DECLARE @numrows int = 0
	DECLARE @LastUnitID Decimal(20, 0) = NULL
	DECLARE @UnitID Decimal(20, 0) = NULL
	DECLARE @InDateTime dateTime = NULL
	DECLARE @OutDateTime dateTime = NULL
	DECLARE @InRange int = NULL
	DECLARE @LastInRange int = NULL
	DECLARE @InsertData bit = 0
	DECLARE @lCustomerId INT = NULL
	DECLARE @Fecha DATETIME
	DECLARE @Longitude FLOAT 
	DECLARE @Latitude FLOAT
	DECLARE @InputStatus TINYINT
	DECLARE @Direccion NVARCHAR(MAX)
	DECLARE @NameDriver VARCHAR(250)
	DECLARE @Evento VARCHAR(20)
	
	DECLARE @DevicesTemp TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		Device int,
		UnitID Decimal(20, 0)
	)

	DECLARE @ReportsTemp TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		UnitID Decimal(20, 0),
		Device int,
		GPSDateTime dateTime,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
		Longitude float,
		Latitude float, 
		InputStatus tinyint,
		ReportID int,
		[CustomerId] INT NULL
	)
	
	DECLARE @ReportsList TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		UnitID Decimal(20, 0),
		Device int,
		GPSDateTime dateTime,
		InRange int,  
		Longitude float,
		Latitude float, 
		InputStatus tinyint,
		ReportID int,
		[CustomerId] INT NULL
	)
	
	DECLARE @ReportsListFilter TABLE
	(
		Id bigint Primary Key IDENTITY(1,1),
		UnitID Decimal(20, 0),
		Device int,
		GPSDateTime dateTime,
		InRange int,  
		Longitude float,
		Latitude float, 
		InputStatus tinyint,
		ReportID int,
		[CustomerId] INT NULL
	)
	
	DECLARE @Result TABLE
	(
		Id int Primary Key IDENTITY(1,1),
		UnitID Decimal(20, 0),
		InDateTime dateTime NULL,
		OutDateTime dateTime NULL, 
		Longitude float,
		Latitude float, 
		InputStatus tinyint,
		Dir NVARCHAR(MAX),
		EventOnOff VARCHAR(20),
		ParentID int
	)

	-- REALIZAMOS UN SPLIT DE LOS UNIT_ID'S CONCATENADOS
    DECLARE @separator varchar(max) = ','
    DECLARE @Splited table(item varchar(max))
    SET @pUnitId = REPLACE(@pUnitId,@separator,'''),(''')
    SET @pUnitId = 'select * from (values('''+@pUnitId+''')) as V(A)' 
    INSERT INTO @Splited
    EXEC (@pUnitId)
	--/////////////////////////////////////////////////////////////////////////////////////////////
	SET NOCOUNT ON
	
	-- OBTENEMOS EL PUNTO GEOGRAFICO A COMPARAR
	DECLARE @p GEOGRAPHY = GEOGRAPHY::STGeomFromText('POINT(' + CONVERT(VARCHAR(MAX), @pLongitude) + ' ' + CONVERT(VARCHAR(MAX), @pLatitude) + ')', 4326);
	
	-- OBTENEMOS LOS DEVICES DESDE EL PARAMETRO CONSULTADO
	INSERT INTO @DevicesTemp (Device, UnitID)
		SELECT [Device], [UnitID] 
		FROM [dbo].[Devices]
		WHERE CONVERT(varchar(max), [UnitID]) in (select item from @Splited) 

	-- OBTENEMOS LOS DATOS DE REPORTS DESDE EL RANGO DE FECHAS CONSULTADO
		INSERT INTO @ReportsTemp (Device, UnitID, GPSDateTime, Longitude, Latitude, InputStatus, ReportID, [CustomerId])
		SELECT 
			r.[Device],
			d.[UnitID],
			r.[RepDateTime],
			r.[Longitude],
			r.[Latitude],
			r.[InputStatus], 
			r.[ReportID],
			v.[CustomerId]
		FROM [dbo].[Reports] r 
		INNER JOIN @DevicesTemp d 
			ON r.[Device] = d.[Device]
		INNER JOIN [General].[Vehicles] v
		    ON v.[DeviceReference] = r.[Device]
		WHERE 
			r.[RepDateTime] >= @pStartDate AND 
			r.[RepDateTime] < @pEndDate 
		ORDER BY r.[RepDateTime]

	--SELECT * FROM @ReportsTemp

	
	-- RECORREMOS LOS PUNTOS DE REPORTS TEMP PARA ESTABLECER SI UN UNIT_ID ESTUVO DENTRO DE UN RANGO		
	INSERT INTO @ReportsList (UnitID, Device, GPSDateTime, InRange, Longitude, Latitude, InputStatus, ReportID, CustomerId)
		SELECT 
			r.UnitID,
			r.[Device],
			r.GPSDateTime, 
        GEOGRAPHY::STGeomFromText('POINT('+ 
            convert(nvarchar(20), r.Longitude)+' '+
            convert( nvarchar(20), r.Latitude)+')', 4326)
	        .STBuffer(@pDistance).STIntersects(@p) as [Intersects],
			r.[Longitude],
			r.[Latitude],
			r.[InputStatus], 
			r.[ReportID],
			r.[CustomerId]
		FROM 
			@ReportsTemp r 
		ORDER BY r.[UnitID], r.[GPSDateTime]

	--SELECT * FROM @ReportsList where InRange = 1


	SET @UnitID = 0
	SET @i = 1
	SET @numrows = (SELECT COUNT(*) FROM @ReportsList)
	IF @numrows > 0
    
	INSERT INTO @ReportsListFilter (UnitID, Device, GPSDateTime, InRange, Longitude, Latitude, InputStatus, ReportID, CustomerId)
    SELECT TOP(1) UnitID, Device, GPSDateTime, InRange, Longitude, Latitude, InputStatus, ReportID, CustomerId FROM @ReportsList
    UNION
    SELECT x.UnitID, x.Device, x.GPSDateTime, x.InRange, x.Longitude, x.Latitude, x.InputStatus, x.ReportID, x.CustomerId FROM @ReportsList x
    LEFT OUTER JOIN @ReportsList y ON x.Id = y.Id + 1 AND (x.InRange <> y.InRange OR x.UnitID <> y.UnitID) 
	WHERE y.Id IS NOT NULL 
	
	--SELECT * FROM @ReportsListFilter
	
    WHILE (@i <= (SELECT MAX(Id) FROM @ReportsListFilter))
    BEGIN
	--Select @i
		IF @i >= 1
		BEGIN
			SET @InsertData = 0
			SET @UnitID = (SELECT UnitID FROM @ReportsListFilter WHERE Id = @i)
			SET @LastUnitID = (SELECT UnitID FROM @ReportsListFilter WHERE Id = @i - 1)
			
			--Select @UnitID, @LastUnitID	

			-- Nota: Se comenta este If ya que si al procesar los datos de control de paso el 
			-- veh�culo en cuesti�n no tiene datos de que sali� de la zona marcada, s�lo de
			-- entrada el procedimiento no devolv�a nada, ahora devuelve aunque no haya registro
			-- de salida en la zona marcada en el mapa, basta con que haya ingresado.
			 
			--IF (@UnitID = @LastUnitID) 
			--BEGIN

				SET @InRange = (SELECT InRange FROM @ReportsListFilter WHERE Id = @i)
				SET @LastInRange = ISNULL((SELECT InRange FROM @ReportsListFilter WHERE Id = @i - 1), 0)

				--Select @InRange, @LastInRange

				IF (@inRange != @LastInRange)
				BEGIN
					IF @InRange = 1
						BEGIN
							SET @InDateTime = (SELECT GPSDateTime FROM @ReportsListFilter WHERE Id = @i)
							
							--Select @InDateTime
							
							--Se valida si es el ultimo registro, para poder guardarlo como una entrada aun sin salida
							if not exists(select UnitID from @ReportsListFilter where Id = @i + 1)
								SET @InsertData = 1		
						END
					ELSE
						BEGIN
							SET @OutDateTime = (SELECT GPSDateTime FROM @ReportsListFilter WHERE Id = @i)
							SET @InsertData = 1
						END
				END
				IF @InsertData = 1
				BEGIN
				
					SET @Longitude = (SELECT Longitude FROM @ReportsListFilter WHERE Id = @i)
					SET @Latitude = (SELECT Latitude FROM @ReportsListFilter WHERE Id = @i)
					SET @lCustomerId = (SELECT [CustomerId] FROM @ReportsListFilter WHERE Id = @i)
					SET @InputStatus = (SELECT InputStatus FROM @ReportsListFilter WHERE Id = @i)
					SET @Direccion =  [Efficiency].[Fn_GetAddressByLatLon_Retrieve](@Longitude, @Latitude, @lCustomerId, NULL)

					INSERT INTO @Result (UnitID, InDateTime, OutDateTime, Longitude, Latitude, InputStatus, Dir, EventOnOff, ParentID)
					VALUES(@UnitID,DATEADD(hour,-6,@InDateTime), DATEADD(hour,-6,@OutDateTime), @Longitude, @Latitude, @InputStatus, @Direccion, 'Ingreso/Salida', 0)				
					SET @InDateTime = NULL
					SET @OutDateTime = NULL

					--Select * from @Result
				END	
			--END
		END
        SET @i = @i + 1
    END
	
	DECLARE @j bigint = 1
	SET @i = 1

   
    WHILE (@i <= (SELECT MAX(Id) FROM @Result))
    BEGIN
	print @i
		
		WHILE (@j <= (SELECT MAX(Id) FROM @ReportsListFilter))
		BEGIN
			IF((SELECT GPSDateTime FROM @ReportsListFilter WHERE Id = @J) BETWEEN (SELECT InDateTime FROM @Result WHERE Id = @i) AND (SELECT OutDateTime FROM @Result WHERE Id = @i))
			BEGIN 
				SET @Fecha = (SELECT GPSDateTime FROM @ReportsListFilter WHERE Id = @J)
				SET @Longitude = (SELECT Longitude FROM @ReportsListFilter WHERE Id = @J)
				SET @Latitude = (SELECT Latitude FROM @ReportsListFilter WHERE Id = @J)
				SET @lCustomerId = (SELECT [CustomerId] FROM @ReportsListFilter WHERE Id = @i)
				SET @InputStatus = (SELECT InputStatus FROM @ReportsListFilter WHERE Id = @J)
				SET @Direccion =  [Efficiency].[Fn_GetAddressByLatLon_Retrieve](@Longitude, @Latitude, @lCustomerId, NULL)
							
				IF(@pEventOn = 1)
				BEGIN
					IF((SELECT ReportID FROM @ReportsListFilter WHERE Id = @J) != 25)
					BEGIN
						IF((SELECT ReportID % 2 FROM @ReportsListFilter WHERE Id = @J) = 0)--Encendido
						BEGIN
							SET @Evento = 'Encendido'
										
							INSERT INTO @Result (UnitID, InDateTime, OutDateTime, Longitude, Latitude, InputStatus, Dir, EventOnOff, ParentID)
							VALUES(@UnitID, @Fecha, '', @Longitude, @Latitude, @InputStatus, @Direccion, @Evento, (SELECT Id FROM @Result WHERE Id = @i) )				
							
						END
					END
				END
				IF(@pEventOff = 1)
				BEGIN
					IF((SELECT ReportID FROM @ReportsListFilter WHERE Id = @J) != 25)
					BEGIN
						IF((SELECT ReportID % 2 FROM @ReportsListFilter WHERE Id = @J) != 0)--Apagado
						BEGIN
							SET @Evento = 'Apagado'
										
							INSERT INTO @Result (UnitID, InDateTime, OutDateTime, Longitude, Latitude, InputStatus, Dir, EventOnOff, ParentID)
							VALUES(@UnitID, @Fecha, '', @Longitude, @Latitude, @InputStatus, @Direccion, @Evento, (SELECT Id FROM @Result WHERE Id = @i) )				
							
						END
					END
				END

			END

			SET @J = @J + 1
		END
		
		SET @j = 1
		SET @i = @i + 1
    END

	SELECT 
		[Id],
		CONVERT(varchar(25), [UnitID]) [UnitID], 
		[InDateTime], 
		[OutDateTime],
		[Longitude],
		[Latitude],
		[InputStatus],
		[Dir],
		[EventOnOff],
		[ParentID]
	FROM @Result 
	

    SET NOCOUNT OFF
END