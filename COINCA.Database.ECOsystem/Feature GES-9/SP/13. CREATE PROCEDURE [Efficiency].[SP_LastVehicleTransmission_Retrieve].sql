USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[SP_LastVehicleTransmission_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[SP_LastVehicleTransmission_Retrieve] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================================================
-- Author:			�lvaro Zamora
-- Create date: 21/08/2018
-- Description:	Recupera la �ltima ubicaci�n de un veh�culo en un d�a espec�fico.
-- Henry Retana - 04/09/2018 - reestructura del sp.
-- Stefano Quir�s - 26/09/2018 - Add the TimeZone Parameter
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ======================================================================================================

CREATE PROCEDURE [Efficiency].[SP_LastVehicleTransmission_Retrieve] 
(
	@pDate AS DATE,
	@pCostCenterId  AS INT = NULL,
	@pCustomerId AS INT,
	@pDates AS VARCHAR(100)
) 
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lTimeZoneParameter  INT 

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 	
	WHERE cu.[CustomerId] = @pCustomerId

	IF (CAST(DATEADD(HOUR, @lTimeZoneParameter, GETDATE()) AS DATE) <= @pDate)
	BEGIN
		DECLARE @Today AS DATETIME = DATEADD(d, 0, DATEDIFF(d, 0, DATEADD(HOUR, @lTimeZoneParameter, GETDATE()))) 
		
		SELECT	CASE WHEN DATEDIFF (HOUR, r.[RepDateTime], @Today) > 24 
						THEN 1
						ELSE 0 
				END [Check]
				,v.[VehicleId]
				,v.[Name] [VehicleName]
				,v.[PlateId]
				,ISNULL((SELECT TOP 1 u.[Name]
						 FROM [General].[Users] u
						 INNER JOIN [General].[VehiclesDrivers] vd 
						 	ON u.[UserId] = vd.[UserId]
						 INNER JOIN [General].[VehiclesByUser] vu
						 	ON u.[UserId] = vu.[UserId]  AND vd.[VehicleId] = vu.[VehicleId]
						 WHERE vd.[VehicleId] = MIN(v.[VehicleId])
						 AND (vu.[LastDateDriving] BETWEEN MIN(r.[RepDateTime]) 
						 AND MAX(r.[RepDateTime]) OR vu.[LastDateDriving] IS NULL)), '') [EncryptDriverName]
				,DATEADD(HOUR, @lTimeZoneParameter, r.[RepDateTime]) [LastTransmissionDate]
				,r.[Latitude]
				,r.[Longitude]
				,[Efficiency].[Fn_GetAddressByLatLon_Retrieve](r.[Longitude], r.[Latitude], @pCustomerId, NULL) [Location]
				,r.[ReportID]
				,c.[Name] [EncryptCustomerName]
				,cc.[Name] [CostCenterName]
				,@pDates [Dates]
		FROM [dbo].[Report_Last] r
		INNER JOIN [General].[Vehicles] v
			ON r.[Device] = v.[DeviceReference]
		INNER JOIN [General].[VehicleCostCenters] cc
			ON v.[CostCenterId] = cc.[CostCenterId]
		INNER JOIN [General].[Customers] c
			ON v.[CustomerId] = c.CustomerId
		WHERE v.[CustomerId] = @pCustomerId
		AND (@pCostCenterId IS NULL OR v.[CostCenterId] = @pCostCenterId)
		AND (v.[IsDeleted] IS NULL OR v.[IsDeleted] = 0)
		AND v.[Active] = 1
		GROUP BY v.[VehicleId], 
				 v.[Name], 
				 v.[PlateId], 
				 r.[RepDateTime], 
				 r.[Latitude], 
				 r.[Longitude], 
				 r.[ReportID], 
				 cc.[Name], 
				 c.[Name]
		ORDER BY cc.[Name] ASC
		
	END
	ELSE
	BEGIN		
		SELECT	 0 [Check]
				,v.[VehicleId]
				,v.[Name] [VehicleName]
				,v.[PlateId]
				,ISNULL((SELECT TOP 1 u.[Name]
						 FROM [General].[Users] u
						 INNER JOIN [General].[VehiclesDrivers] vd 
						 	ON u.[UserId] = vd.[UserId]
						 INNER JOIN [General].[VehiclesByUser] vu
						 	ON u.[UserId] = vu.[UserId]  AND vd.[VehicleId] = vu.[VehicleId]
						 WHERE vd.[VehicleId] = v.[VehicleId]
					     AND (vu.[LastDateDriving] BETWEEN r.[RepDateTime] AND r.[RepDateTime] 
						 OR vu.[LastDateDriving] IS NULL)), '') [EncryptDriverName]
				,DATEADD(HOUR, @lTimeZoneParameter, r.[RepDateTime]) [LastTransmissionDate]
				,r.[Latitude]
				,r.[Longitude]
				,[Efficiency].[Fn_GetAddressByLatLon_Retrieve](r.[Longitude], r.[Latitude], @pCustomerId, NULL) [Location]
				,r.[ReportID]
				,c.[Name] [EncryptCustomerName]
				,cc.[Name] [CostCenterName]
				,@pDates [Dates]
		FROM 
		(
			SELECT MAX([Report]) [Report], 
				   ROW_NUMBER() OVER (PARTITION BY MAX([Device]) ORDER BY [RepDateTime] DESC) rn
			FROM [dbo].[Reports] 
			WHERE [Device] IN (
								   SELECT [DeviceReference] 
								   FROM [General].[Vehicles]
								   WHERE [CustomerId] = @pCustomerId
								   AND ([DeviceReference] IS NOT NULL AND [DeviceReference] > 0)
								   AND ([IntrackReference] IS NOT NULL AND [IntrackReference] > 0)
								   AND ([IsDeleted] IS NULL OR [IsDeleted] = 0)
								   AND [Active] = 1
							  )
			AND [RepDateTime] < DATEADD(DAY, 1, @pDate) 
			GROUP BY [Device], 
					 [RepDateTime]
		) re
		INNER JOIN [dbo].[Reports] r
			ON r.[Report] = re.[Report]
			AND re.[rn] = 1
		INNER JOIN [General].[Vehicles] v
			ON r.[Device] = v.[DeviceReference]			
		INNER JOIN [General].[VehicleCostCenters] cc
			ON v.[CostCenterId] = cc.[CostCenterId]
		INNER JOIN [General].[Customers] c
			ON v.[CustomerId] = c.CustomerId
		WHERE v.[CustomerId] = @pCustomerId
		AND (@pCostCenterId IS NULL OR v.[CostCenterId] = @pCostCenterId)		
		ORDER BY cc.[Name] ASC
	END
	SET NOCOUNT OFF
END
