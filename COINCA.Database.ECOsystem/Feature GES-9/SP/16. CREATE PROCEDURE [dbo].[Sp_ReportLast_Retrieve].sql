USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ReportLast_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ReportLast_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Danilo Hidalgo G.
-- Create date: 10/30/2014
-- Description:	Retrieve ReportLast information
-- Modify by: Sebastian Quesada - 17/07/2018 - a�adir parametros CustomerId para la funcion [Efficiency].[Fn_GetAddressByLatLon_Retrieve]
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================

CREATE PROCEDURE [dbo].[Sp_ReportLast_Retrieve]
(
	 @pUnitsID VARCHAR(MAX) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON

	    DECLARE @separator varchar(max) = ','
		DECLARE @Splited table(item varchar(max))
	    SET @pUnitsID = REPLACE(@pUnitsID,@separator,'''),(''')
		SET @pUnitsID = 'select * from (values('''+@pUnitsID+''')) as V(A)' 
	    INSERT INTO @Splited
		EXEC (@pUnitsID)

		SELECT 
			d.[UnitID],
			r.[Device], 
			DATEADD(HOUR, -6, r.[GPSDateTime]) [GPSDateTime],
			r.[RTCDateTime], 
			r.[RepDateTime], 
			r.[SvrDateTime],
			r.[Longitude], 
			r.[Latitude], 
			r.[Heading], 
			r.[ReportID], 
			r.[Odometer], 
			r.[HDOP], 
			r.[InputStatus], 
			r.[OutputStatus], 
			r.[VSSSpeed], 
			r.[AnalogInput], 
			r.[DriverID], 
			r.[Temperature1], 
			r.[Temperature2], 
			r.[MCC],
			r.[MNC], 
			r.[LAC], 
			r.[CellID], 
			r.[RXLevel],
			r.[GSMQuality],
			r.[GSMStatus],
			r.[Satellites],
			r.[RealTime],
			r.[MainVolt],
			r.[BatVolt],
			[Efficiency].[Fn_GetAddressByLatLon_Retrieve]([r].[Longitude], [r].[Latitude],v.[CustomerId], NULL) [Direction]				
		FROM dbo.[Devices] d
		  INNER JOIN dbo.[Report_Last] r 
		    ON d.[Device] = r.[Device]
		  INNER JOIN [General].[Vehicles] v
		    ON v.[DeviceReference] = r.[Device]
		WHERE d.[UnitID] IN (select item from @Splited)

    SET NOCOUNT OFF

END