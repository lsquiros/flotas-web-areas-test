USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[SP_GeofencesAlarmsReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[SP_GeofencesAlarmsReport_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================

CREATE PROCEDURE [Efficiency].[SP_GeofencesAlarmsReport_Retrieve]
(
	@pCustomerId INT,
	@pStartDate DATETIME,
	@pEndDate DATETIME,
	@pCostCenterId INT = NULL,
	@pGeofencesId INT = NULL,
	@pDates VARCHAR (100)
) 
AS
BEGIN

	SET NOCOUNT ON

    SELECT
		 v.[VehicleId]
		,v.[Name] [VehicleName]
		,v.[PlateId]
		,ISNULL((SELECT TOP 1 u.[Name]
			FROM [General].[Users] u
			INNER JOIN [General].[VehiclesDrivers] vd 
			  ON u.[UserId] = vd.[UserId]
			INNER JOIN [General].[VehiclesByUser] vu
			  ON u.[UserId] = vu.[UserId]  AND vd.[VehicleId] = vu.[VehicleId]
		WHERE vd.[VehicleId] = MIN(v.[VehicleId])
		  AND (vu.[LastDateDriving] BETWEEN MIN(r.[GPSDateTime]) 
		  AND MAX(r.[GPSDateTime]) OR vu.[LastDateDriving] IS NULL)), '') [EncryptDriverName]
		,CASE
			WHEN r.[InOrOut] = 1 THEN 'Entrada'
			ELSE 'Salida'
		END [EventType]
		,DATEADD(hh, -6, r.[GPSDateTime]) [Date]
		,r.[Longitude]
		,r.[Latitude]
		,[Efficiency].[Fn_GetAddressByLatLon_Retrieve](r.[Longitude], r.[Latitude], @pCustomerId, NULL) [Location]
		,g.[Name] [GeofenceName]
		,c.[Name] [EncryptCustomerName]
		,@pDates [Dates]
	 FROM [Efficiency].[AlarmGeofencesReport] r
	 INNER JOIN [General].[Vehicles] v
		ON v.[VehicleId] = r.[VehicleId]
	 INNER JOIN [Efficiency].[GeoFences] g
		ON r.GeofenceId = g.GeofenceId
	 INNER JOIN [General].[Customers] c
		ON r.CustomerId = c.CustomerId
	 WHERE r.[GPSDateTime] BETWEEN @pStartDate AND @pEndDate
		AND (@pGeofencesId IS NULL OR r.[GeofenceId] = @pGeofencesId)
		AND (@pCostCenterId IS NULL OR v.[CostCenterId] = @pCostCenterId)
	 GROUP BY
		v.[VehicleId], v.[Name], v.[PlateId], r.[InOrOut], r.[GPSDateTime], r.[Latitude], r.[Longitude], g.[Name], c.[Name]
	ORDER BY
		[Date] DESC

	 SET NOCOUNT OFF

END
