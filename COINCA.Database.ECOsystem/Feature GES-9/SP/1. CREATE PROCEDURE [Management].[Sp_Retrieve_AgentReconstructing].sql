USE [ECOSystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_AgentReconstructing]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_AgentReconstructing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 19/09/2018
-- Description:	Agent Reconstructing Retrieve
-- Modify by: Marjorie Garbanzo - 27/11/2018 - Do not take into account deleted transmissions
-- Modify by: Marjorie Garbanzo - 15/01/2019 - Include events without a parent
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_AgentReconstructing]
(	
	@pUserId INT,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL
)
AS
BEGIN	
	SET NOCOUNT ON

	DECLARE @lBeginJourneyEvent INT,
			@lLocation INT,
			@lLocationName VARCHAR(200), 
			@lMainCatalog INT,
			@lTimeZone INT, 
			@lAgentStop INT,
			@lBetweenManagement VARCHAR(200), 
			@lClientBegin INT 

	SELECT @lTimeZone = co.[TimeZone]
	FROM [General].[DriversUsers] d
	INNER JOIN [General].[Customers] c
		ON d.[CustomerId] = c.[CustomerId]
	INNER JOIN [General].[Countries] co
		ON co.[CountryId] = c.[CountryId]
	WHERE d.[UserId] = @pUserId 

	--Get Begin Journey 
	SELECT @lBeginJourneyEvent = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'
	
	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'

	SELECT @lClientBegin = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'CUSTOMERS_CLIENT_VISIT_BEGIN'

	SELECT @lMainCatalog = [Id]
	FROM [Management].[Catalog]
	WHERE [Name] = 'Tipos de Eventos Principales'

	SELECT @lAgentStop = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'AGENT_STOP'

	SELECT @lLocationName = p.[MovementName]
	FROM [Management].[Parameters] p 
	INNER JOIN [Management].[UserEvent] u
		ON p.[CustomerId] = u.[CustomerId]
	WHERE u.[UserId] = @pUserId
	
	SELECT @lBetweenManagement = p.[BetweenManagement]
	FROM [Management].[Parameters] p 
	INNER JOIN [Management].[UserEvent] u
		ON p.[CustomerId] = u.[CustomerId]
	WHERE u.[UserId] = @pUserId

	IF @pStartDate IS NULL SET @pStartDate = GETDATE()
	IF @pEndDate IS NULL SET @pEndDate = DATEADD(DAY, 1, @pStartDate)

	SELECT  CAST(ROW_NUMBER() OVER(ORDER BY u.[EventDate] ASC) AS INT) [Id],
			u.[UserId],
			u.[EventDetailId],
			ed.[Name] [EventDetailName],
			u.[EventTypeId],
			c.[Name] [EventName],
			c.[Parent],
			u.[CommerceId],
			u.[Latitude],
			u.[Longitude],
			u.[BatteryLevel],
			CAST(u.[Speed] AS INT)[Speed],
			u.[EventDate],
			u.[Lapse]
	INTO #REPORTS
	FROM [Management].[UserEvent] u
	INNER JOIN [Management].[CatalogDetail] c
		ON u.[EventTypeId] = c.[Id]
	LEFT JOIN [Management].[EventDetail] ed
		ON u.[EventDetailId] = ed.[Id]
	WHERE u.[UserId] = @pUserId 
	AND (c.[CatalogId] = @lMainCatalog OR c.[Id] IN (@lLocation, @lAgentStop))
	AND u.[EventDate] BETWEEN @pStartDate AND @pEndDate
	AND u.[Delete] = 0
	ORDER BY u.[EventDate];

	;WITH Temp AS
	(
		SELECT TOP 1 * 
		FROM #REPORTS
		UNION
		SELECT r.*
		FROM #REPORTS r,
			 #REPORTS r2
		WHERE r.[Id] = (r2.[Id] + 1) 
		AND r.[EventTypeId] <> r2.[EventTypeId]
	)
	SELECT  t.[Id],
			t.[UserId],
			t.[EventTypeId],
			t.[EventDetailName],
			CASE WHEN t.[EventTypeId] = @lLocation	
				 THEN CASE WHEN (	
									SELECT TOP 1 [EventTypeId]
									FROM Temp
									WHERE [Id] < t.[Id]
									ORDER BY [Id] DESC 
								) = @lClientBegin	
							AND (	
									SELECT TOP 1 [Parent]
									FROM Temp
									WHERE [Id] > t.[Id]
									ORDER BY [Id] ASC 
								) = @lClientBegin	
						THEN ISNULL(@lBetweenManagement, t.[EventName])
					    ELSE ISNULL(@lLocationName, t.[EventName])
						END
				 ELSE ISNULL(t.[EventDetailName], t.[EventName]) 
			END [EventName],
			c.[Name] [CommerceName],
			t.[Parent],
			t.[Latitude],
			t.[Longitude],
			t.[BatteryLevel],
			t.[Speed],
			t.[EventDate],
			t.[EventDate] [StartDate], 
			ISNULL((
						SELECT TOP 1 r.[EventDate] 
						FROM temp r
						WHERE r.[Id] > t.[Id]
						ORDER BY r.[Id]
				   ), DATEADD(HOUR, @lTimeZone, GETDATE())) [EndDate],

			 CASE WHEN t.[Parent] = 0 AND t.[EventTypeId] NOT IN (@lLocation, @lAgentStop)
				  THEN '00:00:00'
				  WHEN t.[EventTypeId] = @lLocation
				  THEN [General].[Fn_GetTimeFormat](ISNULL(DATEDIFF(S, t.[EventDate], ISNULL((SELECT TOP 1 r.[EventDate] 
																							  FROM temp r
																							  WHERE r.[Id] > t.[Id]
																							  ORDER BY r.[Id]), DATEADD(HOUR, @lTimeZone, GETDATE()))) / 3600, 0),				  
		  					RIGHT('0' + CAST(ISNULL(DATEDIFF(S, t.[EventDate], ISNULL((SELECT TOP 1 r.[EventDate] 
																						FROM temp r
																						WHERE r.[Id] > t.[Id]
																						ORDER BY r.[Id]), DATEADD(HOUR, @lTimeZone, GETDATE()))) % 3600 / 60, 0) AS VARCHAR(2)), 2),
							RIGHT('0' + CAST(ISNULL(DATEDIFF(S, t.[EventDate], ISNULL((SELECT TOP 1 r.[EventDate] 
																						FROM temp r
																						WHERE r.[Id] > t.[Id]
																						ORDER BY r.[Id]), DATEADD(HOUR, @lTimeZone, GETDATE()))) % 60, 0) AS VARCHAR(2)), 2)) 
				  WHEN t.[EventTypeId] = @lAgentStop
				  THEN [General].[Fn_GetTimeFormat](ISNULL((t.[Lapse] % (24 * 60 * 60)) / (60 * 60), '00'),
													ISNULL(((t.[Lapse] % (24 * 60 * 60)) % (60 * 60)) / 60, '00'),
													ISNULL(((t.[Lapse] % (24 * 60 * 60)) % (60 * 60)) % 60, '00'))
				  WHEN t.[Parent] > 0
				  THEN [General].[Fn_GetTimeFormat](ISNULL(DATEDIFF(S, ISNULL((SELECT TOP 1 r.[EventDate] 
																		      FROM temp r
																			  WHERE r.[EventTypeId] = t.[Parent]
																			  AND r.[Id] < t.[Id]
																			  ORDER BY r.[EventDate] DESC), t.[EventDate]), t.[EventDate]) / 3600, 0),				  
		  					RIGHT('0' + CAST(ISNULL(DATEDIFF(S, ISNULL((SELECT TOP 1 r.[EventDate] 
																		FROM temp r
																		WHERE r.[EventTypeId] = t.[Parent]
																		AND r.[Id] < t.[Id]
																		ORDER BY r.[EventDate] DESC), t.[EventDate]), t.[EventDate]) % 3600 / 60, 0) AS VARCHAR(2)), 2),
							RIGHT('0' + CAST(ISNULL(DATEDIFF(S, ISNULL((SELECT TOP 1 r.[EventDate] 
																		FROM temp r
																		WHERE r.[EventTypeId] = t.[Parent]
																		AND r.[Id] < t.[Id]
																		ORDER BY r.[EventDate] DESC), t.[EventDate]), t.[EventDate]) % 60, 0) AS VARCHAR(2)), 2)) 
		    END [Lapse],			 
			CASE WHEN t.[Latitude] IS NULL 
				 THEN 0
				 WHEN t.[Parent] = 0 AND t.[EventTypeId] <> @lLocation
				 THEN 0 
				 WHEN t.[EventTypeId] = @lLocation
				 THEN [Management].[Fn_GetDistanceFromPointsByDates] (@pUserId,  t.[EventDate], ISNULL((SELECT TOP 1 r.[EventDate] 
																										FROM temp r
																										WHERE r.[Id] > t.[Id]
																										ORDER BY r.[Id]), DATEADD(HOUR, @lTimeZone, GETDATE()))) 
				 WHEN t.[Parent] > 0
				 THEN [Management].[Fn_GetDistanceFromPointsByDates] (@pUserId, ISNULL((SELECT TOP 1 r.[EventDate] 
																						FROM temp r
																						WHERE r.[EventTypeId] = t.[Parent]
																						AND r.[Id] < t.[Id]
																						ORDER BY r.[EventDate] DESC), t.[EventDate]), t.[EventDate])
			END [Distance]
	FROM temp t
	LEFT JOIN [General].[Commerces] c
		ON t.[CommerceId] = c.[Id]
	WHERE [EventTypeId]<> @lBeginJourneyEvent 
	AND ([Parent] <> @lBeginJourneyEvent OR t.[Parent] IS NULL)
	UNION 
	SELECT  [Id],
			[UserId],	
			[EventTypeId],	
			[EventDetailName],
			ISNULL([EventDetailName], [EventName]) [EventName],
			NULL [CommerceName],
			[Parent],
			[Latitude],
			[Longitude],
			[BatteryLevel],
			[Speed],			
			[EventDate],
			[EventDate] [StartDate],
			DATEADD(SECOND, 1, [EventDate]) [EndDate],
			'00:00:00' [Lapse],
			0 [Distance]
	FROM #REPORTS
	WHERE ([EventTypeId] = @lBeginJourneyEvent OR [Parent] = @lBeginJourneyEvent)
	ORDER BY [EventDate]

	DROP TABLE #REPORTS
		
	SET NOCOUNT OFF
END