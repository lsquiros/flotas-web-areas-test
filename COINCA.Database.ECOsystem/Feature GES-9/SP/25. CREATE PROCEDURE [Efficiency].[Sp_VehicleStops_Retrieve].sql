USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleStops_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_VehicleStops_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 02/27/2017
-- Description:	Retrieve Stops Reports per Vehicles 
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to RepDateTime
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================

CREATE PROCEDURE [Efficiency].[Sp_VehicleStops_Retrieve]
(
	@pVehicleId INT,
	@pParameter INT				
)
AS
BEGIN
	SET NOCOUNT ON	

	DECLARE @lDevice INT
		   ,@Longitude DECIMAL(13,10)
		   ,@Latitude DECIMAL(13,10)
		   ,@lGPSStopDateTime DATETIME
		   ,@lTimeZoneParameter INT
		   ,@lIsAttrack INT
		   ,@lStopsDetail INT
		   ,@date1 DATETIME
		   ,@date2 DATETIME
		   ,@lapse VARCHAR(100)
           ,@years INT
		   ,@months INT
		   ,@weeks INT
		   ,@days INT
		   ,@hours INT
		   ,@minutes INT

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	INNER JOIN [General].[Vehicles] v
		ON cu.[CustomerId] = v.[CustomerId]
	WHERE v.[VehicleId] = @pVehicleId

    --GET THE DEVICE
	SELECT @lDevice = v.[DeviceReference]
	FROM [General].[Vehicles] v
	WHERE [VehicleId] = @pVehicleId
	

	--IF THE TEMP TABLES EXIST, DELETE THEM
	IF OBJECT_ID('tempdb..#temp') IS NOT NULL DROP TABLE #temp
	IF OBJECT_ID('tempdb..#ResultRecords') IS NOT NULL DROP TABLE #ResultRecords

	--CREATE TEMP TABLES 
	CREATE TABLE #temp 
	(
		 [GPSDateTime] DATETIME
		,[Longitude] DECIMAL(13,10)
		,[Latitude] DECIMAL(13,10)
	);

	CREATE TABLE #ResultRecords 
	(
		 [GPSDateTime] DATETIME
		,[Longitude] DECIMAL(13,10)
		,[Latitude] DECIMAL(13,10)
		,[Lapse] VARCHAR(100)
		,[Location] VARCHAR(MAX)
	);	

	SELECT @lIsAttrack = COUNT(*) FROM [General].[Vehicles]
	WHERE VehicleId = @pVehicleId
    AND DeviceReference IN (SELECT [Device]
							FROM [dbo].[Devices] 
							WHERE CONVERT(VARCHAR,[UnitId]) IN (SELECT [TerminalId]
									FROM [dbo].[DispositivosAVL] 
									WHERE [Modelo] = 18))
    
	IF (@lIsAttrack = 0)
	BEGIN
		
		Select @lStopsDetail = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'STOPDETAIL_PARAMETER'					   		

		--GET THE RECORDS BASED ON THE FILTERS	
		SELECT TOP 1 @lGPSStopDateTime = r.[RepDateTime]
					 ,@Longitude = r.[Longitude]
					 ,@Latitude = r.[Latitude]
		FROM [dbo].[Reports] r
		WHERE r.[Device] = @lDevice 
		AND r.[ReportId] = @lStopsDetail
		ORDER BY r.[RepDateTime] DESC  

		IF (SELECT COUNT(1) 
			FROM [dbo].[Reports] 
			WHERE [Device] = @lDevice 
			AND [ReportId] = 104	
			AND [RepDateTime] > @lGPSStopDateTime) = 0
		BEGIN
			INSERT INTO #temp
			SELECT   r.[RepDateTime]
					,r.[Longitude]
					,r.[Latitude]
			FROM [dbo].[Report_Last] r
			WHERE r.[Device] = @lDevice 
			ORDER BY r.[RepDateTime] DESC 
		END
	END
	ELSE
	BEGIN
		--GET THE RECORDS BASED ON THE FILTERS	
		SELECT TOP 1 @lGPSStopDateTime = r.[RepDateTime]
					 ,@Longitude = r.[Longitude]
					 ,@Latitude = r.[Latitude]
		FROM [dbo].[Reports] r
		WHERE r.[Device] = @lDevice 
		AND r.[VSSSpeed] = 0
		ORDER BY r.[RepDateTime] DESC  

		IF (SELECT COUNT(1) 
			FROM [dbo].[Reports] 
			WHERE [Device] = @lDevice 
			AND [VSSSpeed] > 0 
			AND [RepDateTime] > @lGPSStopDateTime) = 0
		BEGIN
			INSERT INTO #temp
			SELECT   r.[RepDateTime]
					,r.[Longitude]
					,r.[Latitude]
			FROM [dbo].[Report_Last] r
			WHERE r.[Device] = @lDevice 
			ORDER BY r.[RepDateTime] DESC
		END
	END	

	IF ((Select Count(1) FROM #temp) > 0)
	BEGIN
		SET @date1 = @lGPSStopDateTime	
		SELECT @date2 = [GPSDateTime] FROM #temp

		Select @years=DATEDIFF(yy,@date1,@date2)
		if DateAdd(yy,-@years,@date2)<@date1 Select @years=@years-1
		Set @date2= DateAdd(yy,-@years,@date2)

		Select @months=DATEDIFF(mm,@date1,@date2)
		if DateAdd(mm,-@months,@date2)<@date1 Select @months=@months-1
		Set @date2= DateAdd(mm,-@months,@date2)

		Select @weeks=DATEDIFF(wk,@date1,@date2)
		if DateAdd(wk,-@weeks,@date2)<@date1 Select @weeks=@weeks-1
		Set @date2= DateAdd(wk,-@weeks,@date2)        

		Select @days=DATEDIFF(dd,@date1,@date2)
		if DateAdd(dd,-@days,@date2)<@date1 Select @days=@days-1
		Set @date2= DateAdd(dd,-@days,@date2)
		
		Select @hours=DATEDIFF(hh,@date1,@date2)
		if DateAdd(hh,-@hours,@date2)<@date1 Select @hours=@hours-1
		Set @date2= DateAdd(hh,-@hours,@date2)
		
		Select @minutes=DATEDIFF(mi,@date1,@date2)		

		IF ((@years <> NULL) OR (@months <> NULL) OR (@weeks <> NULL) OR (@days <> NULL) OR (@hours <> NULL) OR (@minutes > @pParameter))
		BEGIN

		Select @lapse= ISNULL(CAST(NULLIF(@years,0) as varchar(10)) + ' A�o','')
		     + ISNULL(' ' + CAST(NULLIF(@months,0) as varchar(10)) + ' Mes','')
		     + ISNULL(' ' + CAST(NULLIF(@weeks,0) as varchar(10)) + ' Semana','')     
		     + ISNULL(' ' + CAST(NULLIF(@days,0) as varchar(10)) + ' D�a','')
		     + ISNULL(' ' + CAST(NULLIF(@hours,0) as varchar(10)) + ' Horas','')
		     + ISNULL(' ' + CAST(@minutes as varchar(10)) + ' Minutos','')		

			INSERT INTO #ResultRecords
			SELECT @lGPSStopDateTime
			  ,@Longitude
			  ,@Latitude
			  ,@lapse
			  ,[Efficiency].[Fn_GetAddressByLatLon_Retrieve](@Longitude, @Latitude, NULL, NULL) AS [Location]
			FROM #temp	
	    END
	END		


	--RETRIEVE THE RECORDS
	SELECT	 DATEADD(HOUR, @lTimeZoneParameter, [GPSDateTime]) [GPSDateTime] 
			,[Longitude]
			,[Latitude]
			,[Lapse]
			,[Location]
    FROM #ResultRecords

	SET NOCOUNT OFF
END