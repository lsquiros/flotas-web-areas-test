USE [ECOSystemQA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[GPS_CreateAlarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[GPS_CreateAlarm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================  
-- Author:  Marco Cabrera  
-- Create date: 09-05-2017  
-- Description: Send a alarm in case of the reportId match with any id in GeneralParameters - Property GPS  
-- Modify By:   Jose Ramirez   
-- 31/10/2017 - Agregar alarma de temperatura  
-- Modify By:   Esteban Solís   
-- 31/10/2017 - Logica para usar configuracion de tiempo de espera entre alarmas
-- Modify By:   Jose Julian   
-- 31/01/2018 - Se añade el voltaje a la alarma
-- Modify By:   Gerald Solano
-- 12/02/2018 - Se añaden validaciones en temas de buzon de correo y composiciones válidas
-- 15/02/2018 - Se añaden validaciones para días de expiración en alarmas
-- Modify By:   Manuel Corea
-- 18/04/2018 - Envía una alarma para el evento de uso de grúa
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================  
  
CREATE PROCEDURE [General].[GPS_CreateAlarm]
	@pDevice INT,
	@pLatitude FLOAT,
	@pLongitude FLOAT,
	@pDateReport DATETIME,
	@pReportId INT,
	@pTemperature FLOAT = NULL,
	@pSensor  VARCHAR(20) = NULL,
	@pLimiteTemperatura FLOAT = NULL,
	@pMainVolt  FLOAT = NULL
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @lEntityId INT
	DECLARE	@lPlate VARCHAR(20)
	DECLARE @lCustomerId INT
	DECLARE @lCode VARCHAR(MAX)
	DECLARE @lAlarmTriggerId INT
	DECLARE @lEmail VARCHAR(MAX)
	DECLARE @lTimeZone INT
	DECLARE @lAlarmId INT
	DECLARE @lCountryCode VARCHAR(3)
	DECLARE @lVehicleName VARCHAR(MAX)
	DECLARE @lDriverName VARCHAR(MAX)
	DECLARE @lLocation VARCHAR(MAX)
	DECLARE @tbl_splitPartners TABLE (PartnerId int)
	DECLARE @str_Partners varchar(max)=''
	DECLARE @xml_strPartners XML
	DECLARE @int_PartnerId int = 0
	DECLARE @emailCopy VARCHAR(MAX) = NULL
	DECLARE @vehicleID INT
	DECLARE @psite varchar(200)
	DECLARE @pFrom VARCHAR (500) = NULL
	DECLARE @lDebug BIT
	DECLARE @lMessage NVARCHAR(MAX)
	DECLARE @lResourceKey VARCHAR(400)
	DECLARE @doSendAlarm BIT
	DECLARE @lastAlarmDate DATETIME
	DECLARE @sendDelayMinutes INT = NULL
	DECLARE @slapsedTime INT
	DECLARE @disconnect_gps_inbox NVARCHAR(100) = ''
	DECLARE @disconnect_gps_expire INT = 0
	DECLARE @IsEnableAlertsToClients INT = 0
	DECLARE @FalsePositve BIT = 0
	DECLARE @DaysForExpiration INT = 0
									
	-- SET TO 1 FOR DEBUGGING PURPOSES
	SET @lDebug = 0
	
	--Creamos tabla para registrar los vehículos relacionados a un solo Device
	DECLARE @tbl_ClientsWithDevice TABLE(
		Id INT IDENTITY(1,1),
		Plate VARCHAR(20),
		CustomerId INT,
		VehicleName VARCHAR(MAX),
		DriverName VARCHAR(MAX),
		VehicleID INT
	)
 
	--Obtenemos el codigo de alarma relacionado a un ReportID del GPS
	SELECT	@lCode = [Description]
	FROM	[dbo].[GeneralParameters]
	WHERE	[Property] = 'GPS'
	AND		[Value] = @pReportId

	IF @lCode IS NOT NULL
	BEGIN
		--Obtenemos parametros de configuracion
		SELECT @disconnect_gps_inbox = [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'GPS_DISCONNECT_INBOX'
		SELECT @disconnect_gps_expire = [NumericValue] FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'EXPIRE_ALARM_GPS_DISCONNECT'
		
		SELECT @IsEnableAlertsToClients = [NumericValue] FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'ALERT_CLIENT_CONTROLCAR'			
		SELECT @str_Partners = [Value] FROM [dbo].[GeneralParameters] WHERE[ParameterID] = 'PARTNER_APP_NOTIFICATION' -- Se obtiene los Socios que requieren notificaciones via App
		
		--Obtenemos el tipo de alarma
		SELECT	@lAlarmTriggerId = [TypeId]
		FROM	[General].[Types]
		WHERE	Code = @lCode
		
		--Obtenemos los datos de clientes y vehiculos relacionados con el Device -- Pueden haber vehiculos con relacion a otros Socios
		INSERT INTO @tbl_ClientsWithDevice(Plate, CustomerId, VehicleName, DriverName, VehicleID)
		SELECT TOP 1 PlateId [Plate], CustomerId [CustomerId], V.Name [VehicleName], U.Name [DriverName], v.vehicleID [VehicleID]
		FROM	[General].[Vehicles] V
		LEFT JOIN [General].[VehiclesByUser] VU ON VU.VehicleId = V.VehicleId AND [LastDateDriving] IS NULL
		LEFT JOIN [General].[Users] U ON U.UserId = VU.UserId
		WHERE	DeviceReference = @pDevice
		ORDER BY VU.VehiclesByUserId DESC 
		
		--Recorremos los registros de clientes / vehiculos relacionados al Device
		DECLARE @Min_Id INT = 0		
		SELECT @Min_Id = MIN(Id) FROM @tbl_ClientsWithDevice
		
		WHILE (@Min_Id IS NOT NULL)
		BEGIN
			-- Obtenemos el cliente, placa y vehicle ID relacionado con el Device
			SELECT	@lPlate = Plate, 
					@lCustomerId = CustomerId, 
					@lVehicleName = VehicleName, 
				    @lDriverName = DriverName,
					@vehicleID = VehicleID
			FROM @tbl_ClientsWithDevice
			WHERE Id = @Min_Id
			
			--=======================================================
			-- VALIDACIÓN DE CORREO DE NOTIFICACION POR TIPO DE SOCIO
			--=======================================================
			
			/*IF EXISTS (SELECT  1 FROM dbo.ReportsIdAdditionalInformation WHERE ReportId = @pReportId AND FilterByVehicle = 1)
				BEGIN*/
			SELECT	@lEmail = Email, @lAlarmId = AlarmId
			FROM	[General].[Alarms]
			WHERE	AlarmTriggerId = @lAlarmTriggerId AND EntityId = @vehicleID

			--Si la alarma es de Botón de Pánico / Alarma de Plataforma de Grua se debe filtrar el Entity ID por CustomerId 
			--Debido a que está configurado por empresa y no por Vehículo
			IF(@lAlarmTriggerId IN (511,280,281)) -- Alarma Botón de Pánico / Alarma de Plataforma de Grua - Activa / Inactiva
			BEGIN
				SELECT TOP 1
					@lEmail = Email, 
					@lAlarmId = (CASE WHEN @lAlarmTriggerId = 511 THEN AlarmId ELSE 0 END)
				FROM [General].[Alarms]
				WHERE	--AlarmTriggerId = @lAlarmTriggerId AND 
					    EntityId = @lCustomerId
				ORDER BY AlarmId DESC
			END

			/*	END
			ELSE
				BEGIN
					SELECT	@lEmail = Email, @lAlarmId = AlarmId
					FROM	[General].[Alarms]
					WHERE	AlarmTriggerId = @lAlarmTriggerId
				END*/

			--Convertimos el string plano de los valores a un valor XML
			SET @xml_strPartners = CAST(('<row><col>' + REPLACE(@str_Partners,',','</col></row><row><col>') + '</col></row>') AS XML)

			--Insertamos en una tabla tipo variable para facilitar la consulta de los Socios 
			INSERT INTO @tbl_splitPartners
			SELECT
			   t.n.value('col[1]','int')
			FROM
				@xml_strPartners.nodes ('/row') AS t(n)

			--Obtenemos el Socio relacionado al Cliente 
			SELECT @int_PartnerId = PartnerId FROM [General].[CustomersByPartner] WHERE CustomerId = @lCustomerId
			
			-- Socio LineVita / Control Car
			IF EXISTS(SELECT 1 FROM @tbl_splitPartners WHERE PartnerId IN (@int_PartnerId))
			BEGIN
				SET @psite = 'http://www.controlcar.com/'
				SET @pFrom = 'info@controlcar.com'
			END
			ELSE
			BEGIN -- Socio BacFlota
				SET @psite = 'https://flotas.baccredomatic.com'
			END
			
			-- Validamos el correo para la notificacion
			IF @lEmail IS NULL
			BEGIN 
					-- Socio LineVita / Control Car
					IF EXISTS(SELECT 1 FROM @tbl_splitPartners WHERE PartnerId IN (@int_PartnerId))
						BEGIN
							INSERT INTO [General].[Alarms]
							  ([CustomerId]
							  ,[Phone]
							  ,[Email]
							  ,[AlarmTriggerId]
							  ,[EntityTypeId]
							  ,[EntityId]
							  ,[Active]
							  ,[InsertDate]
							  ,[InsertUserId])
						  SELECT @lCustomerId, 
							   '00000000' [Phone],
							   anu.[Email], 
							   @lAlarmTriggerId [AlarmTriggerId], 
							   0 [EntityTypeId], 
							   @vehicleID [EntityId], 
							   1 [Active], 
							   GETDATE() [InsertDate], 
							   0 [InsertUserId]
								FROM [dbo].[AspNetUsers] anu
								INNER JOIN [General].[Users] u
								 ON u.[AspNetUserId] = anu.Id
								INNER JOIN [General].[CustomerUsers] cu
								 ON cu.UserId = u.[UserId]
								WHERE cu.CustomerId = @lCustomerId
						END

						SELECT	@lEmail = Email, @lAlarmId = AlarmId
						FROM	[General].[Alarms]
						WHERE	AlarmTriggerId = @lAlarmTriggerId
								AND	EntityId = @vehicleID								
			END
			
			--=======================================================
			-- TERMINA VALIDACIÓN DE CORREO DE NOTIFICACION POR TIPO DE SOCIO
			--=======================================================
			
			--==================================================
			-- VALIDACIÓN DE REPORTES DE GPS
			--==================================================
			
			--Validamos el reporte de desconexion de GPS
			IF(@pReportId = 12)
			BEGIN
				-- Seteamos los días de expiración para esta alarma
				SET @DaysForExpiration = @disconnect_gps_expire
			
				--Se valida que el reporte de desconexión esté trabajando con el voltaje del dispositivo y no del vehículo, 
				--esto es un indicativo que el GPS fue desconectado de su fuente principal, con esta premisa evitamos los falsos positivos
				IF(@pMainVolt IS NOT NULL AND @pMainVolt <= 3) 
				BEGIN
					SET @FalsePositve = 0
					
					IF EXISTS(SELECT 1 FROM @tbl_splitPartners WHERE PartnerId IN (@int_PartnerId)) -- LineVitaECO / ControlCar
					BEGIN				
						SET @lEmail = @lEmail + ';' + @disconnect_gps_inbox
						
						--Se valida si el cliente tiene habilitado recibir notificaciones
						IF(@IsEnableAlertsToClients = 0)
						BEGIN
							SET @lEmail = @disconnect_gps_inbox
						END
					END
					ELSE BEGIN -- Es un socio BAC FLOTA

						SET @lEmail = @disconnect_gps_inbox
						
					END			
					
				END
				ELSE BEGIN 
					
					-- Alerta como falso positivo
					SET @FalsePositve = 1
				END
			END
			
			-- Se valida el tipo de Alarma Sensor de Temperatura
			IF @lAlarmTriggerId = 512
			BEGIN
				SET	@lAlarmTriggerId = @pReportId
			END
			
			--==================================================
			-- TERMINA VALIDACIÓN DE REPORTES DE GPS
			--==================================================
			
			-- Se valida los destinatarios de la notificación
			SELECT @emailCopy =  EmailFrom FROM [GENERAL].[Emails_NotificationCoinca] WHERE CustomerId = @lCustomerId

			IF @emailCopy IS NOT NULL
			BEGIN 
				SET @lEmail = @lEmail + ';'+ @emailCopy
			END  
						
			--Obtenemos la zona horaria del cliente
			SELECT @lTimeZone = [TimeZone], @lCountryCode = Code
			FROM [General].[Countries] C
			INNER JOIN [General].[Customers] CU
				ON CU.CountryId = C.CountryId
			WHERE CU.CustomerId = @lCustomerId

			SET @pDateReport = DATEADD(hh, @lTimeZone, @pDateReport)
			
			--Obtenemos la direccion del GPS
			SELECT @lLocation = [Efficiency].[Fn_GetAddressByLatLon_Retrieve] (@pLongitude, @pLatitude, @lCustomerId, NULL)
			
			
			SET @doSendAlarm = 1

			IF EXISTS(	
						SELECT	1 
						FROM	[General].[AlarmConfiguration] 
						WHERE	[AlarmTriggerId] = @lAlarmTriggerId
					)
			BEGIN
				-- por default no se envia alarma
				SET @doSendAlarm = 0;

				-- inicializar configuracion default 
				SELECT	@sendDelayMinutes = [DefaultSendDelayMinutes],
						@lResourceKey = [ResourceKey]
				FROM	[General].[AlarmConfiguration] WHERE [AlarmTriggerId] = @lAlarmTriggerId

				-- buscar configuracion del cliente
				SELECT	@sendDelayMinutes = CONVERT(INT, Value)
				FROM	[General].[ParametersByCustomer] 
				WHERE	[CustomerId] = @lCustomerId
				AND		[ResuourceKey] = @lResourceKey

				-- buscar ultima alarma
				SET @lastAlarmDate = (
										SELECT	[LastSendDate] 
										FROM	[General].[LastAlarmSend] 
										WHERE	[AlarmTriggerId] = @lAlarmTriggerId 
										AND		[CustomerId] = @lCustomerId 
										AND		[VehicleId] = @vehicleID
									)

				-- setear valor true/false
				IF @lastAlarmDate IS NULL
				BEGIN
					SET @doSendAlarm = 1
					INSERT INTO [General].[LastAlarmSend]
					(AlarmTriggerId,CustomerId,VehicleId,LastSendDate)
					VALUES
					(@lAlarmTriggerId,@lCustomerId,@vehicleID,@pDateReport)
				END
				ELSE 
				BEGIN
					SET @slapsedTime = DATEDIFF(MINUTE, @lastAlarmDate, @pDateReport)
					IF @slapsedTime >= @sendDelayMinutes
					BEGIN
						SET @doSendAlarm = 1
						UPDATE	[General].[LastAlarmSend]
						SET		[LastSendDate] = @pDateReport
						WHERE	[AlarmTriggerId] = @lAlarmTriggerId 
						AND		[CustomerId] = @lCustomerId 
						AND		[VehicleId] = @vehicleID
					END
				END
			END
		
			-- Este flag indica que se utiliza en Modo Pruebas
			IF @lDebug = 1 
			  BEGIN
				SET @lMessage = 'Email = ' +  ISNULL(@lEmail, '[Null]') +
						' | @doSendAlarm = ' + CONVERT(VARCHAR(100), ISNULL(@doSendAlarm, 0)) + ', @lastAlarmDate = ' + CONVERT(VARCHAR(100),ISNULL(@lastAlarmDate, '2000-01-01')) + 
						', @slapsedTime = ' + CONVERT(VARCHAR(100), ISNULL(@slapsedTime, '0')) + ', @sendDelayMinutes = '+ CONVERT(VARCHAR(100), ISNULL(@sendDelayMinutes, '0')) + 
						', @doSendAlarm = '+ CONVERT(VARCHAR(100), ISNULL(@doSendAlarm, 0)) + ', @pDateReport = ' + CONVERT(VARCHAR(100),ISNULL(@pDateReport, '2000-01-01')) 
				EXEC [General].[Sp_EventDBLog_Add] 
					 @pProcessName = '[General].[GPS_CreateAlarm]'
					,@pMessage = @lMessage
					,@pType = 'I'
			END 
			ELSE BEGIN 
				
				--Validamos que el Device esté ligado a un vehículo en composición, de lo contrario no se procede a comunicar la alerta
				DECLARE @compositionResult INT = 0
				
				--Validamos que la clasificación del vehículo sea de un producto válido (ControlCar, Lexus, PurdyConnect, BAC Flota)
				DECLARE @isValidProduct INT = 0				
				
				SELECT TOP 1 @compositionResult = c.composicion
				FROM Composiciones AS c
					INNER JOIN DispositivosAVL d ON d.dispositivo = c.dispositivo
					INNER JOIN Devices de ON d.terminalId = Cast(de.UnitID AS VARCHAR)
				WHERE de.Device = @pDevice AND c.sim IS NOT NULL AND c.vehiculo IS NOT NULL
				
				SELECT @isValidProduct = COUNT(1)
				FROM Composiciones AS c
					INNER JOIN DispositivosAVL d ON d.dispositivo = c.dispositivo
					INNER JOIN Devices de ON d.terminalId = Cast(de.UnitID AS VARCHAR)
					INNER JOIN Vehiculos v ON v.vehiculo = c.vehiculo
					INNER JOIN VehiculosxProductos vp ON vp.Vehiculo = v.vehiculo 
				WHERE de.Device = @pDevice AND vp.[ProductId] IN (20, 21, 12, 42, 43, 48)
								
				
				-- Validamos resultado de si tiene composicion y es un producto válido
				IF(@compositionResult > 0 AND @isValidProduct > 0)
				BEGIN
				
					-- Si el correo existe se procede a enviar la notificación
					IF (@lEmail IS NOT NULL)  
					BEGIN   

						IF @doSendAlarm = 1
						BEGIN
								
							DECLARE @IsAlarmInProcess BIT = 0
							DECLARE @LastDateReport DATETIME = @pDateReport
							
							--Validamos si hay una alarma que se haya enviado días atrás y que esté en proceso
							
							--TEMPORAL -- Se permite solo al reporte de Desconexión de GPS (Report = 12) de acceder a este proceso por motivo de pruebas y seguimiento
							IF(@pReportId = 12) 
							BEGIN		
								IF EXISTS(SELECT 1 FROM [General].[GPS_TypeAlarmByDevice] WHERE Device = @pDevice AND TypeAlarmId = @lAlarmTriggerId)
								BEGIN 								
									
									SELECT @LastDateReport = [ReportDate]
									FROM [General].[GPS_TypeAlarmByDevice] 
									WHERE Device = @pDevice AND TypeAlarmId = @lAlarmTriggerId
									
									-- Validamos si la fecha de expiración se cumple
									IF(@pDateReport > DATEADD(DAY, @DaysForExpiration, @LastDateReport))
									BEGIN 
										-- Si no hay alarmas en proceso / no hay falsos positivos, actualizamos las fechas de reporte y cantidad de repeticiones a 1
										IF(@FalsePositve = 0)
										BEGIN
											UPDATE [General].[GPS_TypeAlarmByDevice]
											SET [ReportDate] = @pDateReport,
												[CantReports] = 1
											WHERE Device = @pDevice AND TypeAlarmId = @lAlarmTriggerId
										END
										ELSE BEGIN
											-- Sumamos una repetición más
											UPDATE [General].[GPS_TypeAlarmByDevice]
											SET [CantReports] = [CantReports] + 1
											WHERE Device = @pDevice AND TypeAlarmId = @lAlarmTriggerId
										END
									END
									ELSE BEGIN
										
										--Activamos la bandera que indica que hay una alarma en proceso
										SET @IsAlarmInProcess = 1
										
										-- Si hay alarmas en proceso actualizamos las fechas de reporte y subimos la cantidad de repeticiones
										
										UPDATE [General].[GPS_TypeAlarmByDevice]
										SET [ReportDate] = @pDateReport,
											[CantReports] = [CantReports] + 1
										WHERE Device = @pDevice AND TypeAlarmId = @lAlarmTriggerId										
									END
								END
								ELSE BEGIN
									
									--Apagamos la bandera que indica que NO hay una alarma en proceso
									SET @IsAlarmInProcess = 0									
																
									--Insertamos un nuevo registro de control de alarma si ésta no existe
									
									IF(@pDevice IS NOT NULL AND @lAlarmTriggerId IS NOT NULL)
									BEGIN
										INSERT INTO [General].[GPS_TypeAlarmByDevice]
											   ([Device]
											   ,[TypeAlarmId]
											   ,[ReportDate]
											   ,[CantReports])
										 VALUES
											   (@pDevice
											   ,@lAlarmTriggerId
											   ,@pDateReport
											   ,0)
									END
									
								END
							END
														
							-- ENVÍO DE ALARMA ==================================
														
							-- Si NO hay alarma en proceso y NO hay falsos positivos se envía la notificación
							IF(@IsAlarmInProcess = 0 AND @FalsePositve = 0)
							BEGIN
								EXEC [General].[Sp_SendAlarm] @pEmail = @lEmail, @pEndDate = @pDateReport, @pPlate = @lPlate, @pLatitude = @pLatitude, @pLongitude = @pLongitude, @pAlarmTriggerId = @lAlarmTriggerId,   
										@pAlarmId = @lAlarmId, @pName = @lDriverName, @pLocation = @lLocation, @pCountryCode = @lCountryCode, @pVehicleName = @lVehicleName, @pCustomerId =@lCustomerId,  
										@pTemperature = @pTemperature, @pSensor = @pSensor, @pLimiteTemperatura = @pLimiteTemperatura,
										@psite = @psite, @pFrom  =@pFrom ,  @pMainVolt = @pMainVolt
							END
							-- TERMINA ENVÍO DE ALARMA ==========================
							
						END  
					END
				END
				ELSE BEGIN
					SET @lMessage = '[GPS_Alarm] No se procede a enviar la alerta, el Device: '+ Cast(@pDevice AS VARCHAR) +' no tiene una composición válida / no es un producto válido.' 
					
					EXEC [General].[Sp_EventDBLog_Add] 
						 @pProcessName = '[General].[GPS_CreateAlarm]'
						,@pMessage = @lMessage
						,@pType = 'I'
				END
			END
			
			
			--Siguiente cliente / vehiculo
			SELECT @Min_Id = MIN(Id) FROM @tbl_ClientsWithDevice
			WHERE Id > @Min_Id
		END
		
		SET NOCOUNT OFF  
	END  
END