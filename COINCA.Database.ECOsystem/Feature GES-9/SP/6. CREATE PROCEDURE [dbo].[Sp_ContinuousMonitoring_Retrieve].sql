USE [ECOSystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_ContinuousMonitoring_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Sp_ContinuousMonitoring_Retrieve] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Author:  Danilo Hidalgo G.  
-- Create date: 03/14/2016  
-- Description: Retrieve ReportLast information  
-- Modify by: Danilo Hidalgo G.  
-- Date Modify: 06/14/2016  
-- Add column [Distance]  
-- Modify By: Stefano Quirós  
-- Date: 06/09/2016  
-- Description: Add -6 to the hours  
-- Modify By: Stefano Quirós - Add the Time Zone Parameter - 12/06/2016  
-- Modify By: Henry Retana -19/10/2017  
-- Add Temperature to the retrieve  
-- Modify: Esteban Solis - 20/10/2017  
-- Add [Efficiency].[Fn_GetVehicleHasCooler] function call
-- Modify: Esteban Solis - 11/01/2017  
-- Add [Efficiency].[Fn_GetVehicleStatusConf] function call to get image an status html format
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to GPSDateTime
-- Modify: Sebastian Quesada - 16/07/2018 - add new parameters CustomerId in function [Efficiency].[Fn_GetAddressByLatLon_Retrieve]
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================  

CREATE PROCEDURE [dbo].[Sp_ContinuousMonitoring_Retrieve] 
(
	 @pDate VARCHAR(MAX) = NULL
	,@pUTC INT = NULL
	,@pVehicles VARCHAR(MAX) = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SplitedVehicles TABLE (item VARCHAR(max))
	DECLARE @separator VARCHAR(max) = ','
	DECLARE @lTimeZoneParameter INT
	DECLARE @InactiveTransmitionTime INT = NULL

	-- get transmition time
	SET @InactiveTransmitionTime = (
										SELECT [TransmissionTime]
										FROM [General].[Parameters]
									)

	--SEPARA LAS CONFIGURACIONES UTILIZADAS Y LAS INSERTA EN UNA TABLA  
	SET @pVehicles = REPLACE(@pVehicles, @separator, '''),(''')
	SET @pVehicles = 'SELECT * FROM (VALUES(''' + @pVehicles + ''')) AS V(A)'

	INSERT INTO @SplitedVehicles
	EXEC (@pVehicles)

	DECLARE @list TABLE 
	(
		[Id] INT IDENTITY(1, 1)
		,[VehicleId] INT
		,[PlateId] VARCHAR(MAX)
		,[Name] VARCHAR(MAX)
		,[GPSDateTime] DATETIME
		,[Longitude] FLOAT
		,[Latitude] FLOAT
		,[Heading] INT
		,[Status] INT
		,[VSSSpeed] FLOAT
		,[ActualDate] DATETIME
		,[Device] INT
		,[StarOdometer] FLOAT
		,[EndOdometer] FLOAT
		,[Distance] FLOAT
		,[Direction] VARCHAR(MAX)
		,[Temperature] VARCHAR(100)
		,[HasCooler] BIT
		,[ReportId] INT
		,[SensorsXML] VARCHAR(MAX)
		,[ReportIdImage] VARCHAR(300)
		,[ReportIdDescription] VARCHAR(300)
	);

	DECLARE @devicesOdometer TABLE 
	(
		[Device] INT
		,[StarOdometer] FLOAT
	);

	DECLARE @devicesDate TABLE 
	(
		[Device] INT
		,[StarOdometerDate] DATETIME
	);

	INSERT INTO @list 
	(
		[VehicleId]
		,[PlateId]
		,[Name]
		,[GPSDateTime]
		,[Longitude]
		,[Latitude]
		,[Heading]
		,[Status]
		,[VSSSpeed]
		,[ActualDate]
		,[Device]
		,[StarOdometer]
		,[EndOdometer]
		,[Distance]
		,[Direction]
		,[Temperature]
		,[HasCooler]
		,[ReportId]
		,[SensorsXML]
		,[ReportIdImage]
		,[ReportIdDescription]
	)
	SELECT v.[VehicleId]
		,v.[PlateId]
		,v.[Name]
		,r.[GPSDateTime]
		,r.[Longitude]
		,r.[Latitude]
		,r.[Heading]
		,r.[InputStatus] % 2 [Status]
		,r.[VSSSpeed]
		,GETUTCDATE() [ActualDate]
		,r.[Device]
		,0 [StarOdometer]
		,r.[Odometer]
		,0 [Distance]
		,[Efficiency].[Fn_GetAddressByLatLon_Retrieve](r.[Longitude], r.[Latitude],v.[CustomerId], NULL) [Direction]
		,CASE WHEN [Efficiency].[Fn_GetVehicleHasCooler](v.VehicleId) = 0
			  THEN NULL
			  ELSE CAST((
							CASE WHEN r.[Temperature1] = 999
								 THEN 'No Disponible'
								 ELSE ISNULL(CAST(CAST(r.[Temperature1] AS DECIMAL(12, 2)) AS VARCHAR(100)), 'No Disponible')
							END
						) AS VARCHAR(100))
		 END AS [Temperature]
		,[Efficiency].[Fn_GetVehicleHasCooler](v.VehicleId) AS [HasCooler]
		,r.[ReportID]
		,CASE WHEN [Efficiency].[Fn_GetVehicleHasCooler](v.[VehicleId]) = 0
			THEN NULL
			ELSE [Efficiency].[Fn_GetTemperatureSensorsToXML](NULL, v.[DeviceReference])
			END [SensorsXML]
		,(
			SELECT [Image]
			FROM [dbo].[ReportsIdAdditionalInformation]
			WHERE [ReportId] = r.[ReportId]
			) [ReportIdImage]
		,(
			SELECT [Description]
			FROM [dbo].[ReportsIdAdditionalInformation]
			WHERE [ReportId] = r.[ReportId]
		 ) [ReportIdDescription]
	FROM [General].[Vehicles] v
	INNER JOIN [dbo].[Composiciones] c 
		ON v.[IntrackReference] = c.[Vehiculo]
	INNER JOIN [dbo].[DispositivosAVL] d 
		ON c.[dispositivo] = d.[dispositivo]
	INNER JOIN [dbo].[Devices] de 
		ON d.[numeroimei] = de.[UnitID]
	INNER JOIN [dbo].[Report_Last] r 
		ON de.[Device] = r.[Device]
	WHERE ISNULL(v.[IntrackReference], 0) <> 0
		AND v.[VehicleId] IN (
								SELECT *
								FROM @SplitedVehicles
							  )

	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu 
		ON co.[CountryId] = cu.[CountryId]
	INNER JOIN [General].[Vehicles] v 
		ON cu.[CustomerId] = v.[CustomerId]
	WHERE v.[VehicleId] IN (
								SELECT *
								FROM @SplitedVehicles
							)

	DECLARE @i INT = 1
	DECLARE @max INT = (
							SELECT COUNT(*)
							FROM @list
						)
	DECLARE @device INT = 0
	DECLARE @odometer FLOAT = 0
	DECLARE @date DATETIME = NULL
	DECLARE @UTCDate DATETIME = DATEADD(HOUR, @pUTC, @pDate)

	INSERT INTO @devicesDate 
	(
		[Device]
		,[StarOdometerDate]
	)
	SELECT DISTINCT a.[Device]
				   ,MIN(b.[GPSDateTime])
	FROM @list a
	INNER JOIN [dbo].[Reports] b 
		ON a.[Device] = b.[Device]
	WHERE b.[GPSDateTime] >= @UTCDate
	GROUP BY a.[Device]

	INSERT INTO @devicesOdometer 
	(
		[Device]
		,[StarOdometer]
	)
	SELECT DISTINCT a.[Device]
				   ,b.[Odometer]
	FROM @devicesDate a
	INNER JOIN [dbo].[Reports] b 
		ON a.[Device] = b.[Device]
		AND a.[StarOdometerDate] = b.[GPSDateTime]

	UPDATE a
	SET a.[StarOdometer] = b.[StarOdometer]
	FROM @list a
	INNER JOIN @devicesOdometer b 
		ON a.[Device] = b.[Device]

	UPDATE @list
	SET [Distance] = ROUND([EndOdometer] - [StarOdometer], 0)
	WHERE [EndOdometer] <> 0
	AND [StarOdometer] <> 0
	AND [EndOdometer] > [StarOdometer]

	SELECT [VehicleId]
		  ,[PlateId]
		  ,[Name]
		  ,DATEADD(hh, @lTimeZoneParameter, [GPSDateTime]) [GPSDateTime]
		  ,[Longitude]
		  ,[Latitude]
		  ,[Heading]
		  ,[Status]
		  ,[VSSSpeed]
		  ,DATEADD(hh, @lTimeZoneParameter, [ActualDate]) [ActualDate]
		  ,[Distance]
		  ,[Device]
		  ,[Direction]
		  ,[Temperature]
		  ,[HasCooler]
		  ,[ReportId]
		  ,[SensorsXML]
		  ,[ReportIdImage]
		  ,[ReportIdDescription]
		  ,conf.[StatusStr]
		  ,conf.[ImageURL]
	FROM @list l
	CROSS APPLY (
		SELECT StatusStr
			  ,ImageURL
		FROM [Efficiency].[Fn_GetVehicleStatusConf](l.[VehicleId], l.[Status], DATEADD(hh, @lTimeZoneParameter, l.[ActualDate]), DATEADD(hh, @lTimeZoneParameter, l.[GPSDateTime]), @InactiveTransmitionTime, l.[VSSSpeed])
		) conf

	SET NOCOUNT OFF
END
