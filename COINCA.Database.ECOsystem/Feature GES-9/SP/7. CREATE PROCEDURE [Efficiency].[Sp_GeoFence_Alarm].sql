USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GeoFence_Alarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_GeoFence_Alarm] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 01/26/2015
-- Description:	Add alarm by geofence
-- Modify by:	Gerald Solano
-- Modify date: 05/08/2016
-- Description:	Evita los saltos peque�os de un veh�culo est�tico validando solo los puntos con velocidad.
-- Modify by:	Gerald Solano
-- Modify date: 27/10/2016
-- Description:	Se agrega un margen de distancia al validar dispositivos fuera de una Geocerca, 
-- adem�s cuando un dispositivo entra a la Geocerca se toma en cuenta la velocidad y distancia del dispositivo
-- Modify date: 02/11/2016 - Henry Retana
-- Description:	Se modifica el SP para el uso en la base de datos de solo lectura
-- Modify date: 01/31/2017 - Henry Retana
-- Description:	Se modifica el SP para utilizar archivos desde un xml
-- Modify date: 07/07/2017 - Marco Cabrera
-- Description: Se agrega el campo de tipo de geocerca
-- Modify date: 07/09/2017 - Albert Estrada
-- Description: Se agrega el campo para indicar si se env�a SMS
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to RepDateTime
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================

CREATE PROCEDURE [Efficiency].[Sp_GeoFence_Alarm]
(
	@pXmlData VARCHAR(MAX)
)
AS
BEGIN 

	BEGIN TRY 

		DECLARE @RowNum INT = 0
		DECLARE @lErrorMessage NVARCHAR(4000)
		DECLARE @lErrorSeverity INT
		DECLARE @lErrorState INT

		-- PARAMETERS
		DECLARE @meter_per_foot FLOAT = 0.3048
		DECLARE @FeetsForOutside FLOAT = 0
		DECLARE @DistanceBetweenPoints FLOAT = 0
		DECLARE @GeofenceINMinimunSpeed FLOAT = 0
					
		SELECT @FeetsForOutside = [NumericValue] FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'GEOFENCE_BUFFER_DISTANCE'
					
		SELECT @DistanceBetweenPoints = [NumericValue] FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'GEOFENCE_IN_DISTANCE'

		SELECT @GeofenceINMinimunSpeed = [NumericValue] FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'GEOFENCE_MINIMUN_SPEED'
		--//////////////////////////////////		
		DECLARE @lxmlData XML = CONVERT(XML, @pXmlData)						

		DECLARE @VehicleReportLast TABLE
		(
			[VehicleId] INT,
			[Device] INT,
			[RepDateTime] DATETIME,
			[Longitude] DECIMAL(13, 10),
			[Latitude] DECIMAL(13, 10),
			[VSSSpeed] FLOAT
		)

		INSERT INTO @VehicleReportLast
		SELECT m.c.value('VehicleId[1]', 'INT'),
			   m.c.value('Device[1]', 'INT'),
			   m.c.value('GPSDateTime[1]', 'DATETIME'),
			   m.c.value('Longitude[1]', 'DECIMAL(13, 10)'),
			   m.c.value('Latitude[1]', 'DECIMAL(13, 10)'),
			   m.c.value('VSSSpeed[1]', 'FLOAT')
		FROM @lxmlData.nodes('//VehicleReportLast') AS m(c)

		DECLARE @GeoFencesList TABLE
		(
			[Id] INT IDENTITY(1,1) NOT NULL ,
			[AlarmId] INT,
			[TypeId] INT,
			[CustomerId] INT,
			[AlarmPhone] VARCHAR(100),
			[AlarmEmail] VARCHAR(500),
			[GeoFenceId] INT,
			[GeoFenceName] VARCHAR(50),
			[Polygon] GEOMETRY,
			[In] BIT,
			[Out] BIT,
			[GeoFenceInsertDate] DATETIME,
			[Point] GEOGRAPHY,
			[GeoFenceType] VARCHAR(100)
		)

		DECLARE @VehicleByGeoFence TABLE
		(
			[GeoFenceId] INT,
			[TypeId] INT,
			[VehicleId] INT,
			[PlateId] VARCHAR(10),
			[GeofenceIn] BIT,
			[GeofenceOut] BIT
		)

		DECLARE @ChangedRecords TABLE
		(
			[GeoFenceReportAlarmId] INT,
			[VehicleId] INT,
			[RepDateTime] DATETIME,
			[Longitude] FLOAT,
			[Latitude] FLOAT,
			[LastRepDateTime] DATETIME,
			[LastLongitude] FLOAT,
			[LastLatitude] FLOAT,
			[Distance] FLOAT,
			[VSSSpeed] FLOAT,
			[InsertDate] DATETIME,
			[ModifyDate] DATETIME
		)	

		DECLARE @InOutRecordsByVehicle TABLE
		(
			[Id] INT IDENTITY(1,1) NOT NULL ,
			[AlarmId] INT,
			[TypeId] INT,
			[CustomerId] INT,
			[AlarmPhone] VARCHAR(MAX),
			[AlarmEmail] VARCHAR(MAX),
			[GeoFenceId] INT,
			[GeoFenceName] VARCHAR(50),
			[In] BIT,
			[Out] BIT,
			[GeofenceIn] BIT,
			[GeofenceOut] BIT,
			[VehicleId] INT,
			[PlateId] VARCHAR(10),
			[GeoFenceType] VARCHAR(100),
			[LastRepDateTime] DATETIME,
			[Longitude] DECIMAL(16, 10),
			[Latitude] DECIMAL(16, 10),
			[LastLongitude] DECIMAL(16, 10),
			[LastLatitude] DECIMAL(16, 10)
		)	
	
		DECLARE @GeoFenceReportAlarm TABLE
		(   
			[VehicleId] INT, 
			[RepDateTime] DATETIME, 
			[Longitude] DECIMAL(13, 10), 
			[Latitude] DECIMAL(13, 10), 
			[LastRepDateTime] DATETIME, 
			[LastLongitude] DECIMAL(13, 10), 
			[LastLatitude] DECIMAL(13, 10),
			[InsertDate] DATETIME,
			[ModifyDate] DATETIME
		)
	
		DECLARE @ResultList TABLE 
		(
			[AlarmId] INT,
			[AlarmTriggerId] INT,
			[CustomerId] INT,
			[Plate] VARCHAR(10),
			[Description] VARCHAR(500),
			[TimeLastReport] DATETIME,
			[Phone] VARCHAR(100),
			[Email] VARCHAR (MAX),
			[InOut] VARCHAR(10),
			[GeoFenceType] VARCHAR(100),
			[SendSMS] bit,
			[Longitude] DECIMAL(16, 10),
			[Latitude] DECIMAL(16, 10),
			[LastLongitude] DECIMAL(16, 10),
			[LastLatitude] DECIMAL(16, 10)
		)		
		--Selecciona la informacion de la alerta configurada para las GEOCERCAS. Obtiene la alarma, el tipo (GEOCERCA) y la configuracion de la GEOCERCA
		INSERT INTO @GeoFencesList
		(
			[AlarmId],
			[TypeId],
			[CustomerId],
			[AlarmPhone],
			[AlarmEmail],
			[GeoFenceId],
			[GeoFenceName],
			[Polygon],
			[In],
			[Out],
			[GeoFenceInsertDate],
			[GeoFenceType]
		)
		SELECT b.[AlarmId],
			a.[TypeId],
			b.[CustomerId],
			b.[Phone] [AlarmPhone],
			b.[Email] [AlarmEmail],
			c.[GeoFenceId],
			c.[Name] [GeoFenceName],
			c.[Polygon],
			c.[In],
			c.[Out],
			c.[InsertDate] [GeoFenceInsertDate],
			d.[Name] [GeoFenceType]
		FROM [General].[Types] a 
		INNER JOIN [General].[Alarms] b 
		ON a.[TypeId] = b.[AlarmTriggerId]
		INNER JOIN [Efficiency].[GeoFences] c 
		ON b.[EntityId] = c.[GeoFenceId]
		LEFT JOIN [General].[GeoFencesTypes] d
		ON d.[Id] = c.[GeoFenceTypeId]
		WHERE a.[Code] = 'CUSTOMER_ALARM_TRIGGER_GEOFENCE' AND 
			a.[IsActive] = 1 AND
			b.[Active] = 1 AND 
			c.[Active] = 1

		--Obtiene la informacion de quienes utilizan las GEOCERCAS configuradas (vehiculos, grupos, unidades, costos)
		INSERT INTO @VehicleByGeoFence
		(
			[GeoFenceId],
			[VehicleId],
			[PlateId],
			[GeofenceIn],
			[GeofenceOut]
		)
		(SELECT a.[GeoFenceId], 
			a.[VehicleId],
			c.[PlateId],
			b.[In],
			b.[Out]
		FROM [Efficiency].[VehiclesByGeoFence] a
		INNER JOIN @GeoFencesList b 
		ON a.[GeoFenceId] = b.[GeoFenceId]
		INNER JOIN [General].[Vehicles] c 
		ON a.[VehicleId] = c.[VehicleId]
		UNION
		SELECT a.[GeoFenceId], 
			c.[VehicleId],
			d.[PlateId] ,
			b.[In],
			b.[Out]
		FROM [Efficiency].[VehicleGroupsByGeoFence] a
		INNER JOIN @GeoFencesList b 
		ON a.[GeoFenceId] = b.[GeoFenceId]
		INNER JOIN [General].[VehiclesByGroup] c 
		ON a.[VehicleGroupId] = c.[VehicleGroupId]
		INNER JOIN [General].[Vehicles] d 
		ON c.[VehicleId] = d.[VehicleId]
		UNION
		SELECT a.[GeoFenceId], 
			e.[VehicleId], 
			e.[PlateId],
			b.[In],
			b.[Out]
		FROM [Efficiency].[VehicleUnitsByGeoFence] a
		INNER JOIN @GeoFencesList b 
		ON a.[GeoFenceId] = b.[GeoFenceId]
		INNER JOIN [General].[VehicleUnits] c 
		ON a.[UnitId] = c.[UnitId]
		INNER JOIN [General].[VehicleCostCenters] d 
		ON c.[UnitId] = d.[UnitId]
		INNER JOIN [General].[Vehicles] e 
		ON e.[CostCenterId] = d.[CostCenterId]
		UNION
		SELECT a.[GeoFenceId], 
			c.[VehicleId],
			c.[PlateId],
			b.[In],
			b.[Out]
		FROM [Efficiency].[VehicleCostCenterByGeoFence] a
		INNER JOIN @GeoFencesList b 
		ON a.[GeoFenceId] = b.[GeoFenceId]
		INNER JOIN [General].[Vehicles] c 
		ON a.[CostCenterId] = c.[CostCenterId])

		INSERT INTO @GeoFenceReportAlarm
		( 
			[VehicleId], 
			[RepDateTime], 
			[Longitude], 
			[Latitude], 
			[LastRepDateTime], 
			[LastLongitude], 
			[LastLatitude],
			[InsertDate]
		)
		SELECT [VehicleId], 
			   [GPSDateTime], 
			   [Longitude], 
			   [Latitude], 
			   [LastGPSDateTime], 
			   [LastLongitude], 
			   [LastLatitude],
			   [InsertDate]
		FROM [Efficiency].[GeoFenceReportAlarm]

		UPDATE @GeoFenceReportAlarm 
		SET [RepDateTime] = [LastRepDateTime],
			[Longitude] = [LastLongitude],
			[Latitude] = [LastLatitude]

		INSERT INTO @GeoFenceReportAlarm 
		( 
			[VehicleId], 
			[RepDateTime], 
			[Longitude], 
			[Latitude], 
			[LastRepDateTime], 
			[LastLongitude], 
			[LastLatitude],
			[InsertDate]
		)
		SELECT [VehicleId], 
			   [RepDateTime], 
			   [Longitude], 
			   [Latitude], 
			   [RepDateTime], 
			   [Longitude], 
			   [Latitude],
			   GETUTCDATE()
		FROM @VehicleReportLast
		WHERE [VehicleId] NOT IN (SELECT [VehicleId] 
								  FROM [Efficiency].[GeoFenceReportAlarm])

		UPDATE a
		SET a.[LastRepDateTime] = b.[RepDateTime], 
			a.[LastLongitude] = b.[Longitude], 
			a.[LastLatitude] = b.[Latitude], 
			a.[ModifyDate] = GETUTCDATE()
		FROM @GeoFenceReportAlarm a
		INNER JOIN @VehicleReportLast b 
			ON a.[VehicleId] = b.[VehicleId]
		WHERE a.[LastRepDateTime] <> b.[RepDateTime]
		
		INSERT INTO @ChangedRecords 
		(
			[VehicleId],
			[RepDateTime],
			[Longitude],
			[Latitude],
			[LastRepDateTime],
			[LastLongitude],
			[LastLatitude],
			[InsertDate],
			[ModifyDate],
			[VSSSpeed],
			[Distance]
		)
		SELECT gf.[VehicleId],
			   gf.[RepDateTime],
			   gf.[Longitude],
			   gf.[Latitude],
			   gf.[LastRepDateTime],
			   gf.[LastLongitude],
			   gf.[LastLatitude],
			   gf.[InsertDate],
			   gf.[ModifyDate],
			   ISNULL((SELECT vr.[VSSSpeed] 
			           FROM @VehicleReportLast vr 
					   WHERE vr.[VehicleId] = gf.[VehicleId]), 0) AS [VSSSpeed],
			   geography::STPointFromText('POINT(' + CAST(gf.[LastLongitude] AS VARCHAR(20)) + ' ' + 	CAST(gf.[LastLatitude] AS VARCHAR(20)) + ')', 4326)
			   				.STDistance(
			   					geography::STPointFromText('POINT(' + CAST(gf.[Longitude] AS VARCHAR(20)) + ' ' + CAST(gf.[Latitude] AS VARCHAR(20)) + ')', 4326)
			   				)[Distance]
		FROM  @GeoFenceReportAlarm gf
		WHERE gf.[RepDateTime] <> gf.[LastRepDateTime] AND 
			  gf.[Longitude] <> gf.[LastLongitude] AND 
			  gf.[Latitude] <> gf.[LastLatitude]

		SELECT @RowNum = Count(*) 
		FROM @GeoFencesList
	
		WHILE @RowNum > 0
		BEGIN
			DECLARE @Id INT
			DECLARE @AlarmId INT
			DECLARE @TypeId INT
			DECLARE @CustomerId INT
			DECLARE @AlarmPhone VARCHAR(MAX)
			DECLARE @AlarmEmail varchar(max)
			DECLARE @GeoFenceId INT
			DECLARE @GeoFenceName VARCHAR(50)
			DECLARE @Polygon GEOGRAPHY
			DECLARE @PolygonForOutSide GEOGRAPHY
			DECLARE @In BIT
			DECLARE @Out BIT
			DECLARE @GeoFenceInsertDate DATETIME
			DECLARE @valuePolygon geometry
			DECLARE @valuePolygon2 geometry
			DECLARE @GeoFenceType varchar(100)
								
			SELECT  @Id = [Id], 
					@AlarmId = [AlarmId], 
					@TypeId = [TypeId], 
					@CustomerId = [CustomerId], 
					@AlarmPhone = [AlarmPhone], 
					@AlarmEmail = [AlarmEmail], 
					@GeoFenceId = [GeoFenceId], 
					@GeoFenceName = [GeoFenceName], 			
					@valuePolygon = [Polygon],
					@In = [In],
					@Out = [Out],
					@GeoFenceInsertDate = [GeoFenceInsertDate],
					@GeoFenceType = [GeoFenceType]
			FROM @GeoFencesList 
			WHERE [Id] = @RowNum				
		
			--// VALIDATE POLYGONS ///////////////
			SELECT @valuePolygon = [Polygon] 
			FROM @GeoFencesList 
			WHERE [Id] = @RowNum

			EXEC [General].[Helper_GetGeographyValue] @valuePolygon, @Polygon out
			SET @PolygonForOutSide =  @Polygon.STBuffer(@FeetsForOutside * @meter_per_foot)
			SET @valuePolygon2 = GEOMETRY::STGeomFromText(@PolygonForOutSide.ToString(), 4326)
			--///////////////////////////////////

			IF @In = 1
			BEGIN
				BEGIN TRY 
					INSERT INTO @InOutRecordsByVehicle
					(
						[AlarmId],
						[TypeId],
						[CustomerId],
						[AlarmPhone],
						[AlarmEmail],
						[GeoFenceId],
						[GeoFenceName],
						[In],
						[Out],
						[GeofenceIn],
						[GeofenceOut],
						[VehicleId],
						[PlateId],
						[GeoFenceType],
						[LastRepDateTime],
						[Longitude],
						[Latitude],
						[LastLongitude],
						[LastLatitude]
					)
					SELECT @AlarmId,
						   @TypeId,
						   @CustomerId,
						   @AlarmPhone,
						   @AlarmEmail,
						   @GeoFenceId,
						   @GeoFenceName,
						   1,
						   0,
						   b.GeofenceIn,
						   b.GeofenceOut,
						   a.[VehicleId],
						   b.[PlateId],
						   @GeoFenceType,
						   a.[LastRepDateTime],
						   a.[Longitude],
						   a.[Latitude],
						   a.[LastLongitude],
						   a.[LastLatitude]
					FROM @ChangedRecords a
					INNER JOIN @VehicleByGeoFence b 
						ON a.[VehicleId] = b.[VehicleId]
					WHERE b.[GeoFenceId] = @GeoFenceId 
					AND @Polygon.STIntersects(geography::Point([Latitude], [Longitude], 4326)) <> @Polygon.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326))
					AND @Polygon.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326)) = 1 
					AND (
						  	 a.[VSSSpeed] >= @GeofenceINMinimunSpeed OR
						  	 a.[Distance] >= @DistanceBetweenPoints
						)
				END TRY 
				BEGIN CATCH 
					--RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
					--ERROR EN LA INSERCI�N
				END CATCH 	
			END	

			IF @Out = 1 
			BEGIN						
				BEGIN TRY 
					INSERT INTO @InOutRecordsByVehicle
					(
						[AlarmId],
						[TypeId],
						[CustomerId],
						[AlarmPhone],
						[AlarmEmail],
						[GeoFenceId],
						[GeoFenceName],
						[In],
						[Out],
						[GeofenceIn],
						[GeofenceOut],
						[VehicleId],
						[PlateId],
						[GeoFenceType],
						[LastRepDateTime]
					)
					SELECT @AlarmId,
						   @TypeId,
						   @CustomerId,
						   @AlarmPhone,
						   @AlarmEmail,
						   @GeoFenceId,
						   @GeoFenceName,
						   0,
						   1,
						   b.GeofenceIn,
						   b.GeofenceOut,
						   a.[VehicleId],
						   b.[PlateId], 
						   @GeoFenceType,
						   a.[LastRepDateTime]
					FROM @ChangedRecords a
					INNER JOIN @VehicleByGeoFence b 
						ON a.[VehicleId] = b.[VehicleId]
					WHERE b.[GeoFenceId] = @GeoFenceId AND
						  @PolygonForOutSide.STIntersects(geography::Point([Latitude], [Longitude], 4326)) <> @PolygonForOutSide.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326))AND 
						  @PolygonForOutSide.STIntersects(geography::Point([LastLatitude], [LastLongitude], 4326)) = 0 AND
						  a.[VSSSpeed] > 1
				END TRY 
				BEGIN CATCH 
					--RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
					--ERROR EN LA INSERCI�N
				END CATCH 		
			END
	
			SET @RowNum = @RowNum - 1
		END

		SELECT @RowNum = Count(*) 
		FROM @InOutRecordsByVehicle

		WHILE @RowNum > 0
		BEGIN
			DECLARE @lAlarmId INT
			DECLARE @lAlarmTriggerId INT
			DECLARE @lCustomerId INT
			DECLARE @lPlate VARCHAR(10)
			DECLARE @lDescription VARCHAR(500)
			DECLARE @lTimeLastReport DATETIME
			DECLARE @lPhone VARCHAR(100)
			DECLARE @lEmail VARCHAR (MAX)
			DECLARE @lIn INT
			DECLARE @lOut INT
			DECLARE @lGeofenceIn INT
			DECLARE @lGeofenceOut INT
			DECLARE @lInOut VARCHAR(10)
			DECLARE @lGeoFenceType VARCHAR(100)
			DECLARE @lSMSSend bit
			DECLARE @lTimeZoneParameter INT
			DECLARE @lLongitude DECIMAL(16, 10)
			DECLARE @lLatitude DECIMAL(16, 10)
			DECLARE @lLastLongitude DECIMAL(16, 10)
			DECLARE @lLastLatitude DECIMAL(16, 10)
			
			SELECT @lAlarmId = [AlarmId], 
				   @lAlarmTriggerId = [TypeId],
				   @lCustomerId =[CustomerId],
				   @lPlate = [PlateId],
				   @lDescription = [GeoFenceName],
				   @lPhone = [AlarmPhone],
				   @lEmail = [AlarmEmail],
				   @lIn = [In],
				   @lOut = [Out],
				   @lGeofenceIn = [GeofenceIn],
				   @lGeofenceOut = [GeofenceOut],
				   @lGeoFenceType = [GeoFenceType],
				   @lTimeLastReport = [LastRepDateTime]	,
				   @lLongitude = [Longitude],
				   @lLatitude = [Latitude],
				   @lLastLongitude = [LastLongitude],
				   @lLastLatitude  = [LastLatitude]	
			FROM  @InOutRecordsByVehicle 
			WHERE  [Id] = @RowNum				
			
			SELECT @lSMSSend = SendSMSAlarm FROM [General].[Customers] 
			WHERE customerid = @lCustomerId
		  
			SELECT @lTimeZoneParameter = [TimeZone] FROM [General].[Countries] co 
			INNER JOIN [General].[Customers] cu
			ON co.[CountryId] = cu.[CountryId]
			WHERE cu.[CustomerId] = @lCustomerId


			IF @lIn = 1 AND @lGeofenceIn = 1
			BEGIN
				SET @lInOut = 'Entrado'

				INSERT INTO @ResultList
				VALUES 
				(
				   @lAlarmId,
				   @lAlarmTriggerId,
				   @lCustomerId,
				   @lPlate,
				   @lDescription,
				   @lTimeLastReport,
				   @lPhone,
				   @lEmail,
				   @lInOut,
				   @lGeoFenceType,
				   @lSMSSend,
				   @lLongitude,
				   @lLatitude,
				   @lLastLongitude, 
				   @lLastLatitude  
				)
				
			END

			IF @lOut = 1 AND @lGeofenceOut = 1
			BEGIN
				SET @lInOut = 'Salido'

				INSERT INTO @ResultList
				VALUES 
				(
				   @lAlarmId,
				   @lAlarmTriggerId,
				   @lCustomerId,
				   @lPlate,
				   @lDescription,
				   @lTimeLastReport,
				   @lPhone,
				   @lEmail,
				   @lInOut,
				   @lGeoFenceType,
				   @lSMSSend,
				   @lLongitude,
				   @lLatitude,
				   @lLastLongitude, 
				   @lLastLatitude  
				)

			END
			SET @RowNum = @RowNum - 1
		END		

		--Retorna los datos almacenados en la tabla de resultados
		SELECT [AlarmId],
			   [AlarmTriggerId],
			   [CustomerId],
			   [Plate],
			   [Description],
			   DATEADD(hour, @lTimeZoneParameter , [TimeLastReport]) AS [TimeLastReport],
			   [Phone],
			   [Email],
			   [InOut],
			   [GeoFenceType],
			   [SendSMS],
			   [Longitude],
			   [Latitude],
			   [LastLongitude],
			   [LastLatitude],
			   [Efficiency].[Fn_GetAddressByLatLon_Retrieve]([Longitude], [Latitude],[CustomerId], NULL) AS [Location]  
		FROM   @ResultList

	END TRY 
	BEGIN CATCH 
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH 		
END
