USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_VehicleOverSpeed_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_VehicleOverSpeed_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================  
-- Author:  Henry Retana  
-- Create date: 07/09/2016  
-- Description: Retrieve Vehicles Over Speed   
-- Modify: Henry Retana, 03/10/16  
-- Add the location to the retrive information  
-- Modify: Henry Retana, 10/11/16  
-- Add the last datetime for the lapse  
-- Modify By: Stefano Quir�s - Add the Time Zone Parameter - 12/06/2016  
-- Modify: Marco Cabrera  
-- Only show the driver assign to the vehicle  
-- Modify By: Stefano Quir�s - 27/04/2017  
-- Delete the conversion * -1 on the Start and EndDate   
-- Henry Retana - Add the timezone parameter in the retrieve - 26/05/2017  
-- Modify Esteban Sol�s - 29/11/2016  
-- Added last 5 speed records for over speed alarm
-- Modify: Henry Retana - 25/01/2018
-- Change RepDateTime to RepDateTime
-- Modify by: Sebastian Quesada - 17/07/2018 - add parameter for the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve]
-- Modify By: Stefano Quir�s Ruiz - 27-08-2018 - Add a Group By clause on the SpeedRecord select, 
-- because doesn�t work until 07-08-2018 
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================  

CREATE PROCEDURE [Efficiency].[Sp_VehicleOverSpeed_Retrieve] 
(  
	 @pVehicleId INT,  
	 @pStartDate DATETIME,    
	 @pEndDate DATETIME,  
	 @pFilterType INT      
)  
AS  
BEGIN  
	SET NOCOUNT ON   
  
	DECLARE @lDevice INT  
		   ,@lMaxSpeed INT  
		   ,@Longitude DECIMAL(13,10)  
		   ,@Latitude DECIMAL(13,10)   
		   ,@VSSSpeed DECIMAL(16,2)   
		   ,@Heading INT  
		   ,@ResultOutput INT  
		   ,@RCOUNT INT  
		   ,@lGPSDateTime DATETIME  
		   ,@lOdometer DECIMAL(16,2)   
		   ,@lPlateId VARCHAR(50)  
		   ,@lVehicleName VARCHAR(100)  
		   ,@lTimeZoneParameter INT  
  
	SELECT @lTimeZoneParameter = [TimeZone] FROM [General].[Countries] co   
	INNER JOIN [General].[Customers] cu  
		ON co.[CountryId] = cu.[CountryId]   
	INNER JOIN [General].[Vehicles] v  
		ON cu.[CustomerId] = v.[CustomerId]  
	WHERE v.[VehicleId] = @pVehicleId  
  
	--GET THE DEVICE AND MAX SPEED   
	SELECT @lDevice = v.[DeviceReference],  
		   @lMaxSpeed = vc.[SpeedDelimited],  
		   @lPlateId = v.[PlateId],  
		   @lVehicleName = v.[Name]  
	FROM [General].[Vehicles] v  
	INNER JOIN [General].[VehicleCategories] vc  
		ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]  
	WHERE [VehicleId] = @pVehicleId  
   
	--GET THE PARAMETER FOR THE MINIMUN SPEED  
	DECLARE @lMinimunSpeed INT = (SELECT [NumericValue]   
								  FROM [dbo].[GeneralParameters]  
								  WHERE [ParameterID] = 'SPEED_MINIMUN'  
								  AND [Description] = 'SPEED_MINIMUN')  
  
	--IF THE TEMP TABLES EXIST, DELETE THEM  
	IF OBJECT_ID('tempdb..#temp') IS NOT NULL DROP TABLE #temp  
	IF OBJECT_ID('tempdb..#SpeedRecords') IS NOT NULL DROP TABLE #SpeedRecords  
	IF OBJECT_ID('tempdb..#ResultRecords') IS NOT NULL DROP TABLE #ResultRecords  
  
	--CREATE TEMP TABLES   
	CREATE TABLE #temp   
	(  
		 [Id] INT IDENTITY (1,1)
		,[ReportId] BIGINT
		,[RepDateTime] DATETIME  
		,[Longitude] DECIMAL(13,10)  
		,[Latitude] DECIMAL(13,10)  
		,[Odometer] DECIMAL(16,2)  
		,[VSSSpeed] DECIMAL(16,2)  
		,[Heading] INT
		,[CustomerId] INT  NULL  
	);  
  
	CREATE TABLE #SpeedRecords   
	(  
		 [ReportId]	BIGINT
		,[RepDateTime] DATETIME  
		,[Longitude] DECIMAL(13,10)  
		,[Latitude] DECIMAL(13,10)  
		,[Odometer] DECIMAL(16,2)  
		,[VSSSpeed] DECIMAL(16,2)
		,[CustomerId] INT NULL
	);  
  
	CREATE TABLE #ResultRecords   
	(   
		 [ReportId]	BIGINT
		,[RepDateTime] DATETIME  
		,[LastDateTime] DATETIME  
		,[ElapsedTime] VARCHAR(40)  
		,[Longitude] DECIMAL(13,10)  
		,[Latitude] DECIMAL(13,10)  
		,[Odometer] DECIMAL(16,2)  
		,[MINSpeed] DECIMAL(16,2)  
		,[MAXSpeed] DECIMAL(16,2)  
		,[AVGSpeed] DECIMAL(16,2)  
		,[DriverName] VARCHAR(500)  
		,[PlateId] VARCHAR(500)  
		,[VehicleName] VARCHAR(100) 
		,[CustomerId] INT  NULL   
	);
 
	DECLARE @PreviousSpeeds TABLE
	(
		 [Id]			INT IDENTITY (1,1)
		,[ReportId]		BIGINT
		,[RepDateTime]	DATETIME  
		,[Speed]		DECIMAL(16,2) 
		,[Comments]		VARCHAR(400)
	)

	DECLARE @VEHICLE_MOVING INT = 50
	DECLARE @XmlSpeeds XML  
   
	DECLARE @lStartUTCDateTime DATETIME  
	DECLARE @lUTCEndDate DATETIME  
  
	SET @lStartUTCDateTime = @pStartDate--DATEADD(HOUR, @lTimeZoneParameter * -1, @pStartDate)   
	SET @lUTCEndDate = @pEndDate--DATEADD(HOUR, @lTimeZoneParameter * -1 , @pEndDate)  
  
	--GET THE RECORDS BASED ON THE FILTERS AND DATES  
	INSERT INTO #temp  
	SELECT r.[Report]
		  ,r.[RepDateTime]    
		  ,r.[Longitude]  
		  ,r.[Latitude]  
		  ,r.[Odometer]  
		  ,r.[VSSSpeed]  
		  ,r.[Heading]
		  ,v.[CustomerId]  
	FROM [dbo].[Reports] r 
	INNER JOIN [General].[Vehicles] v
	  ON v.[DeviceReference] = r.[Device]
	WHERE r.[Device] = @lDevice   
	AND r.[RepDateTime] > @lStartUTCDateTime AND r.[RepDateTime] < @lUTCEndDate   
	AND r.[VSSSpeed] > @lMinimunSpeed
  
	--SET THE COUNT ID FOR THE LOOP    
	SET @RCOUNT = (SELECT MIN([Id])   
				   FROM #Temp)  
  
	--**********************FILTER TYPE 1 = ADMIN SPEED**********************  
	IF @pFilterType = 1  
	BEGIN
		--START LOOP  
		WHILE (@RCOUNT IS NOT NULL)  
		BEGIN  
			--RETRIEVE THE SPEED FROM THE RECORS  
			SELECT @VSSSpeed = [VSSSpeed]  
				  ,@lGPSDateTime = [RepDateTime]  
				  ,@lOdometer = [Odometer]  
			FROM #Temp  
			WHERE [Id] = @RCOUNT  
  
			--IF THE SPEED IS HIGHER THAN THE ADMIN SPEED SAVE THE DATE IN THE #SpeedRecords TEMP TABLE  
			IF @VSSSpeed > @lMaxSpeed  
			BEGIN
				INSERT INTO #SpeedRecords  
				SELECT 
					[ReportId]
					,[RepDateTime]  
					,[Longitude]  
					,[Latitude]  
					,[Odometer]  
					,[VSSSpeed]
					,[CustomerId]  
				FROM #temp   
				WHERE [Id] = @RCOUNT  
			END  
			ELSE   
			BEGIN         
				--ONCE THE SPEED IS NOT HIGHER IT WILL SAVE THE RECORDS IN THE RESULT TABLE AND DELETE THE #SpeedRecords RECORDS  
				IF EXISTS(SELECT 1 FROM #SpeedRecords)  
				BEGIN
					INSERT INTO #ResultRecords  
					SELECT MIN([ReportId]),
					    MIN([RepDateTime]),  
						MAX([RepDateTime]),  
						CONVERT(VARCHAR(40), (RIGHT('0' + CAST(DATEDIFF(S, min([RepDateTime]), @lGPSDateTime) / 3600 AS VARCHAR(2)), 2) + ':'  
						+ RIGHT('0' + CAST(DATEDIFF(S, min([RepDateTime]), @lGPSDateTime) % 3600 / 60 AS VARCHAR(2)), 2) + ':'  
						+ RIGHT('0' + CAST(DATEDIFF(S,min([RepDateTime]), @lGPSDateTime) % 60 AS VARCHAR(2)), 2))),  
						(SELECT TOP 1 [Longitude] FROM #SpeedRecords),  
						(SELECT TOP 1 [Latitude] FROM #SpeedRecords),  
						(@lOdometer - MIN([Odometer])),  
						MIN([VSSSpeed]),  
						MAX([VSSSpeed]),  
						AVG([VSSSpeed]),  
						(  
							SELECT TOP 1 u.[Name]  
							FROM [General].[Users] u  
							INNER JOIN [General].[VehiclesDrivers] vd  
								ON u.[UserId] = vd.[UserId]  
							INNER JOIN [General].[VehiclesByUser] vu  
								ON u.[UserId] = vu.[UserId]   
								AND vd.[VehicleId] = vu.[VehicleId]  
							WHERE vd.[VehicleId] = @pVehicleId  
								AND (vu.[LastDateDriving] BETWEEN @pStartDate AND MIN(DATEADD(HOUR, @lTimeZoneParameter, [RepDateTime]))   
									OR vu.[LastDateDriving] IS NULL)  
						),  
						@lPlateId,   
						@lVehicleName,
						[CustomerId]  
					FROM  #SpeedRecords 
					GROUP BY [CustomerId]   

					DELETE FROM #SpeedRecords 
				END  
			END  
  
			--If the count is the last one verifies if there is any record in the speed recors table   
			IF @RCOUNT = (SELECT MAX([Id]) FROM #Temp)  
			BEGIN       
				--ONCE THE SPEED IS NOT HIGHER IT WILL SAVE THE RECORDS IN THE RESULT TABLE AND DELETE THE #SpeedRecords RECORDS  
				IF EXISTS(SELECT 1 FROM #SpeedRecords)  
				BEGIN   
       
					INSERT INTO #ResultRecords  
					SELECT MIN([ReportId]),
					MIN([RepDateTime]),  
						MAX([RepDateTime]),  
						CONVERT(VARCHAR(40), (RIGHT('0' + CAST(DATEDIFF(S, min([RepDateTime]), @lGPSDateTime) / 3600 AS VARCHAR(2)), 2) + ':'  
						+ RIGHT('0' + CAST(DATEDIFF(S, min([RepDateTime]), @lGPSDateTime) % 3600 / 60 AS VARCHAR(2)), 2) + ':'  
						+ RIGHT('0' + CAST(DATEDIFF(S,min([RepDateTime]), @lGPSDateTime) % 60 AS VARCHAR(2)), 2))),  
						(SELECT TOP 1 [Longitude] FROM #SpeedRecords),  
						(SELECT TOP 1 [Latitude] FROM #SpeedRecords),  
						(@lOdometer - MIN([Odometer])),  
						MIN([VSSSpeed]),  
						MAX([VSSSpeed]),  
						AVG([VSSSpeed]),  
						(  
					SELECT TOP 1 u.[Name]  
					FROM [General].[Users] u  
					INNER JOIN [General].[VehiclesDrivers] vd  
						ON u.[UserId] = vd.[UserId]  
					INNER JOIN [General].[VehiclesByUser] vu  
						ON u.[UserId] = vu.[UserId]   
						AND vd.[VehicleId] = vu.[VehicleId]  
					WHERE vd.[VehicleId] = @pVehicleId  
						AND (vu.[LastDateDriving] BETWEEN @pStartDate AND MIN(DATEADD(HOUR, @lTimeZoneParameter, [RepDateTime]))   
							OR vu.[LastDateDriving] IS NULL)  
						),  
						@lPlateId,   
						@lVehicleName,
						[CustomerId]  
					FROM  #SpeedRecords  
					GROUP BY [CustomerId]   
  
					DELETE FROM #SpeedRecords    
				END  
			END  
  
			--GET THE NEXT RECORD FOR THE LOOP  
			SELECT @RCOUNT = MIN([Id])   
			FROM #temp   
			WHERE [Id] > @RCOUNT  
			END  
  
	END  
	--**********************FILTER TYPE 2 = LAW SPEED**********************  
	ELSE IF @pFilterType = 2  
	BEGIN  
		--START LOOP    
		WHILE (@RCOUNT IS NOT NULL)  
		BEGIN 
			--GET THE INFORMATION NEEDED TO GET THE LAW SPEED  
			SELECT   @Longitude = [Longitude]  
					,@Latitude = [Latitude]  
					,@VSSSpeed = [VSSSpeed]  
					,@Heading = [Heading]  
					,@lGPSDateTime = [RepDateTime]  
					,@lOdometer = [Odometer]  
			FROM #Temp  
			WHERE [Id] = @RCOUNT  
  
			--***********GET THE MAX LAW SPEED - LOGIC FROM dbo.[Sp_GetPointStreet]***********  
			DECLARE @LatitudeRange1   DECIMAL(13,10) = 0  
				   ,@LatitudeRange2   DECIMAL(13,10) = 0  
				   ,@LongitudeRange1  DECIMAL(13,10) = 0  
				   ,@LongitudeRange2  DECIMAL(13,10) = 0  
  
			SET @LatitudeRange1  = @Latitude  - 0.01   
			SET @LatitudeRange2  = @Latitude  + 0.01  
			SET @LongitudeRange1 = @Longitude - 0.01  
			SET @LongitudeRange2 = @Longitude + 0.01  
  
			DECLARE @g GEOGRAPHY = 'POINT(' + CAST(@Longitude AS VARCHAR(MAX)) +' ' + CAST(@Latitude  AS VARCHAR(MAX)) + ')';  
			WITH Puntos AS  
			(  
				SELECT TOP(1)  geom.STDistance(@g) AS [Distance]   
						,[Azimuth]  
						,[SpeedMax]  
				FROM  [dbo].[RoadPoint]  
				WHERE [LATITUDE] BETWEEN @LatitudeRange1 AND @LatitudeRange2   
					AND [LONGITUDE] BETWEEN @LongitudeRange1 AND @LongitudeRange2   
					AND geom.STDistance(@g)  < 100    
				ORDER BY geom.STDistance(@g)  
			)  
			SELECT TOP 1 @ResultOutput = [SpeedMax]  
			FROM  Puntos p  
			ORDER BY p.[Distance], ABS(@Heading - p.[Azimuth]), p.[Azimuth];  

			--******************************************************************************************  
			--IF THE SPEED IN THE RECORD IS HIGHER THAN THE LAW SPEED INSERT THE INFORMATION IN THE RESULT TEMP TABLE  
			IF @VSSSpeed > @ResultOutput  
			BEGIN     
				INSERT INTO #SpeedRecords  
				SELECT [ReportId]
					  ,[RepDateTime]  
					  ,[Longitude]  
					  ,[Latitude]  
					  ,[Odometer]  
					  ,[VSSSpeed]
					  ,[CustomerId]  
				FROM #temp   
				WHERE [Id] = @RCOUNT    
			END  
		ELSE  
		BEGIN  
		--IF THE SPEED IS NOT HIGHER IT WILL SAVE THE RECORDS IN THE RESULT TABLE AND DELETE THE TEMP HIGHER RECORDS  
		IF EXISTS(SELECT 1 FROM #SpeedRecords)  
		BEGIN   
       
			INSERT INTO #ResultRecords  
			SELECT MIN([ReportId]),
			MIN([RepDateTime]),  
				MAX([RepDateTime]),  
				CONVERT(VARCHAR(40), (RIGHT('0' + CAST(DATEDIFF(S, min([RepDateTime]), @lGPSDateTime) / 3600 AS VARCHAR(2)), 2) + ':'  
				+ RIGHT('0' + CAST(DATEDIFF(S, min([RepDateTime]), @lGPSDateTime) % 3600 / 60 AS VARCHAR(2)), 2) + ':'  
				+ RIGHT('0' + CAST(DATEDIFF(S,min([RepDateTime]), @lGPSDateTime) % 60 AS VARCHAR(2)), 2))),  
				(SELECT TOP 1 [Longitude] FROM #SpeedRecords),  
				(SELECT TOP 1 [Latitude] FROM #SpeedRecords),  
				(@lOdometer - MIN([Odometer])),  
				MIN([VSSSpeed]),  
				MAX([VSSSpeed]),  
				AVG([VSSSpeed]),  
				(  
			SELECT TOP 1 u.[Name]  
			FROM [General].[Users] u  
			INNER JOIN [General].[VehiclesDrivers] vd  
				ON u.[UserId] = vd.[UserId]  
			INNER JOIN [General].[VehiclesByUser] vu  
				ON u.[UserId] = vu.[UserId]   
				AND vd.[VehicleId] = vu.[VehicleId]  
			WHERE vd.[VehicleId] = @pVehicleId  
				AND (vu.[LastDateDriving] BETWEEN @pStartDate AND MIN(DATEADD(HOUR, @lTimeZoneParameter, [RepDateTime]))   
					OR vu.[LastDateDriving] IS NULL)  
				),  
				@lPlateId,   
				@lVehicleName,
				[CustomerId]  
			FROM  #SpeedRecords  
			GROUP BY [CustomerId]   
  
			DELETE FROM #SpeedRecords    
		END  
		END  
  
			--If the count is the last one verifies if there is any record in the speed recors table   
			IF @RCOUNT = (SELECT MAX([Id]) FROM #Temp)  
			BEGIN       
				--ONCE THE SPEED IS NOT HIGHER IT WILL SAVE THE RECORDS IN THE RESULT TABLE AND DELETE THE #SpeedRecords RECORDS  
				IF EXISTS(SELECT 1 FROM #SpeedRecords)  
				BEGIN
					INSERT INTO #ResultRecords  
					SELECT MIN([ReportId]),
						MIN([RepDateTime]),  
						MAX([RepDateTime]),  
						CONVERT(VARCHAR(40), (RIGHT('0' + CAST(DATEDIFF(S, min([RepDateTime]), @lGPSDateTime) / 3600 AS VARCHAR(2)), 2) + ':'  
						+ RIGHT('0' + CAST(DATEDIFF(S, min([RepDateTime]), @lGPSDateTime) % 3600 / 60 AS VARCHAR(2)), 2) + ':'  
						+ RIGHT('0' + CAST(DATEDIFF(S,min([RepDateTime]), @lGPSDateTime) % 60 AS VARCHAR(2)), 2))),  
						(SELECT TOP 1 [Longitude] FROM #SpeedRecords),  
						(SELECT TOP 1 [Latitude] FROM #SpeedRecords),  
						(@lOdometer - MIN([Odometer])),  
						MIN([VSSSpeed]),  
						MAX([VSSSpeed]),  
						AVG([VSSSpeed]),  
						(  
							SELECT TOP 1 u.[Name]  
							FROM [General].[Users] u  
							INNER JOIN [General].[VehiclesDrivers] vd  
								ON u.[UserId] = vd.[UserId]  
							INNER JOIN [General].[VehiclesByUser] vu  
								ON u.[UserId] = vu.[UserId]   
								AND vd.[VehicleId] = vu.[VehicleId]  
							WHERE vd.[VehicleId] = @pVehicleId  
								AND (vu.[LastDateDriving] BETWEEN @pStartDate AND MIN(DATEADD(HOUR, @lTimeZoneParameter, [RepDateTime]))   
									OR vu.[LastDateDriving] IS NULL)  
						),  
						@lPlateId,   
						@lVehicleName,
						[CustomerId]  
					FROM  #SpeedRecords 
					GROUP BY [CustomerId]    
  
					DELETE FROM #SpeedRecords  
  
				END  
			END  
  
			--GET THE NEXT RECORD FOR THE LOOP  
			SELECT @RCOUNT = MIN([Id])   
			FROM #temp   
			WHERE [Id] > @RCOUNT  
			END        
	END  

	-- CREATE RECORD OF previous speeds 
	INSERT INTO @PreviousSpeeds
	SELECT	rr.[ReportId],
			r.[RepDateTime],
			r.[VSSSpeed],
			r.[Comment]
	FROM	#ResultRecords rr
	CROSS APPLY (
			SELECT TOP 5  [RepDateTime],
							[VSSSpeed],
							CASE WHEN r.[Satellites] < 5 AND r.[HDOP] > 3
 								THEN 'GPS con poca conectividad'
 								ELSE 'GPS con buena se�al'
							END AS [Comment]
			FROM	[dbo].[Reports] r 
			WHERE	r.[report] < rr.[ReportId] 
			AND	r.[Device] = @lDevice  
			AND	r.[ReportId] = @VEHICLE_MOVING
			ORDER BY [RepDateTime] DESC
	) r

	--RETRIEVE THE RECORDS  
	SELECT   DATEADD(HOUR, @lTimeZoneParameter, [RepDateTime]) [GPSDateTime]   
			,DATEADD(HOUR, @lTimeZoneParameter, [LastDateTime]) [LastDateTime]  
			,[ElapsedTime]  
			,[Odometer]  
			,[MINSpeed]  
			,[MAXSpeed]  
			,[AVGSpeed]  
			,[DriverName]  
			,[PlateId]  
			,[VehicleName]  
			,[Longitude]  
			,[Latitude]  
			,[Efficiency].[Fn_GetAddressByLatLon_Retrieve]([Longitude], [Latitude],[CustomerId], NULL) AS [Location]  
			,CONVERT(XML,
						(SELECT	[Id],
								[RepDateTime],
								[Speed],
								[Comments]
						FROM	@PreviousSpeeds AS speed
						WHERE	speed.[ReportId] = rr.[ReportId]
						FOR XML AUTO, ROOT('xmldata') 
			)) AS [XmlSpeeds]
	FROM #ResultRecords rr
  
	SET NOCOUNT OFF  
END