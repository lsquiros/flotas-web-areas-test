USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Sp_GetAddress_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[Sp_GetAddress_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================

CREATE PROCEDURE [Efficiency].[Sp_GetAddress_Retrieve]
(
	 @pLatitude DECIMAL(18, 12),
	 @pLongitude DECIMAL(18, 12),
	 @pCustomerId INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT ISNULL([Efficiency].[Fn_GetAddressByLatLon_Retrieve] (@pLongitude, @pLatitude,@pCustomerId, NULL), '')
		
    SET NOCOUNT OFF
END