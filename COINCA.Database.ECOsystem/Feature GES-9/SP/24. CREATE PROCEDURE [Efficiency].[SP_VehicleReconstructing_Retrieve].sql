USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[SP_VehicleReconstructing_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Efficiency].[SP_VehicleReconstructing_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================      
-- Author:  Henry Retana      
-- Create date: 01/03/2017      
-- Description: Retrieve Vehicle Reconstructing      
-- Modify the Summary Report, Henry Retana - 1/6/2017      
-- Modify: Creacion de rutas desde recorrido, Esteban Solis, 2017-10-12  
-- Modify: Se agregan parametros de distancia minima y tiempo minimo de parada, Esteban Solis, 2017-12-28  
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to RepDateTime
-- Modify by: Marjorie Garbanzo - 18/01/2019 - Add the EventTypeId parameter to NULL, in the function [Efficiency].[Fn_GetAddressByLatLon_Retrieve].
-- ================================================================================================      

CREATE PROCEDURE [Efficiency].[SP_VehicleReconstructing_Retrieve] 
(
	 @pVehicleId INT
	,@pStartDate DATETIME
	,@pEndDate DATETIME
)
AS
BEGIN
	DECLARE @vIterator INT
		   ,@vLastIterator INT = 1
		   ,@vDevice INT
		   ,@vDateIni DATETIME
		   ,@vDateFin DATETIME
		   ,@lTimeZoneParameter INT
		   ,@pCounter INT
		   ,@pIsAttrack INT = 0
		   ,@lStopDetail INT = NULL
		   ,@lMinimumDistance DECIMAL(16)
		   ,@lMinimumStopTime DECIMAL(16)
		   ,@lCustomerId INT = NULL

	SELECT @pIsAttrack = COUNT(*)
	FROM [General].[Vehicles]
	WHERE VehicleId = @pVehicleId
		AND DeviceReference IN (
			SELECT [Device]
			FROM [dbo].[Devices]
			WHERE [UnitId] IN (
					SELECT [TerminalId]
					FROM [dbo].[DispositivosAVL]
					WHERE [Modelo] = 18
					)
			)

	SELECT @lStopDetail = CONVERT(INT, [Value])
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'STOPDETAIL_PARAMETER'

	CREATE TABLE #Data 
	(
		 [Id] INT IDENTITY NOT NULL
		,[RepDatetime] DATETIME
		,[Latitude] FLOAT
		,[Longitude] FLOAT
		,[Odometer] FLOAT
		,[InputStatus] TINYINT
		,[VSSSpeed] FLOAT
		,[ReportId] INT
	)

	CREATE TABLE #Result 
	(
		 [Id] INT IDENTITY
		,[Evento] VARCHAR(20)
		,[FechaInicial] DATETIME
		,[FechaFinal] DATETIME
		,[Latitud] FLOAT
		,[Longitud] FLOAT
		,[Recorrido] FLOAT
		,[VeloMax] FLOAT
		,[Stops] INT
	)

	SELECT @vDevice = [DeviceReference],
		   @lCustomerId = [CustomerId]
	FROM [General].[Vehicles]
	WHERE [VehicleId] = @pVehicleId

	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu 
		ON co.[CountryId] = cu.[CountryId]
	INNER JOIN [General].[Vehicles] v 
		ON cu.[CustomerId] = v.[CustomerId]
	WHERE v.[VehicleId] = @pVehicleId

	SET @vDateIni = DATEADD(hh, @lTimeZoneParameter * - 1, @pStartDate)
	SET @vDateFin = DATEADD(hh, @lTimeZoneParameter * - 1, @pEndDate)

	IF (@vDateIni IS NOT NULL AND @vDateFin IS NOT NULL)
	BEGIN
		INSERT INTO #Data 
		(
			 [RepDatetime]
			,[Latitude]
			,[Longitude]
			,[Odometer]
			,[InputStatus]
			,[vssspeed]
			,[ReportId]
		)
		SELECT [RepDatetime]
			  ,[Latitude]
			  ,[Longitude]
			  ,[Odometer]
			  ,[InputStatus]
			  ,[vssspeed]
			  ,[ReportId]
		FROM [dbo].[Reports]
		WHERE [Device] = @vDevice
		AND [RepDatetime] BETWEEN @vDateIni AND @VDateFin
		ORDER BY [RepDatetime] ASC

		SELECT @pCounter = COUNT(1)
		FROM #Data

		IF @pCounter = 0 RETURN

		SELECT t1.id
			,t1.RepDatetime
			,t1.Latitude
			,t1.Longitude
			,t1.inputStatus
		INTO #ChangeEvent
		FROM #Data t1
			,#Data t2
		WHERE t1.id = (t2.id + 1)
			AND (t1.inputStatus % 2) != (t2.inputStatus % 2)

		SELECT @vIterator = MIN(id)
		FROM #ChangeEvent

		WHILE (@vIterator IS NOT NULL)
		BEGIN
			DECLARE @vIdentidad INT

			INSERT INTO #Result (
				FechaInicial
				,FechaFinal
				,Recorrido
				,VeloMax
				)
			SELECT MIN(RepDatetime) AS FechaIni
				,MAX(RepDatetime) AS FechaFin
				,(MAX(Odometer) - MIN(Odometer)) AS Distancia
				,MAX(VSSSpeed) AS Velocidad
			FROM #Data
			WHERE Id BETWEEN @vLastIterator
					AND @vIterator

			SET @vIdentidad = @@Identity

			IF @pIsAttrack = 0
			BEGIN
				UPDATE #Result
				SET #Result.Stops = v.Stops
				FROM (
					SELECT COUNT(VSSSpeed) Stops
					FROM #Data
					WHERE ReportId = @lStopDetail
						AND Id BETWEEN @vLastIterator
							AND @vIterator
					) v
				WHERE #Result.Id = @vIdentidad
			END
			ELSE
			BEGIN
				UPDATE #Result
				SET #Result.Stops = v.Stops
				FROM (
					SELECT COUNT(VSSSpeed) Stops
					FROM #Data
					WHERE VSSSpeed = 0
						AND Id BETWEEN @vLastIterator
							AND @vIterator
					) v
				WHERE #Result.Id = @vIdentidad
			END

			UPDATE #Result
			SET #Result.Evento = v.Evento
				,#Result.Latitud = v.Latitude
				,#Result.Longitud = v.Longitude
			FROM (
				SELECT (
						CASE inputStatus % 2
							WHEN 0
								THEN 'Encendido'
							ELSE 'Apagado'
							END
						) AS Evento
					,Latitude
					,Longitude
				FROM #ChangeEvent
				WHERE id = @vIterator
				) AS v
			WHERE #Result.Id = @vIdentidad

			DELETE
			FROM #ChangeEvent
			WHERE id = @vIterator

			SET @vLastIterator = @vIterator

			SELECT @vIterator = MIN(id)
			FROM #ChangeEvent
		END

		SELECT @vIterator = MAX(Id)
		FROM #Data

		INSERT INTO #Result (
			FechaInicial
			,FechaFinal
			,Recorrido
			,VeloMax
			)
		SELECT MIN(RepDatetime) AS FechaIni
			,NULL AS FechaFin
			,(MAX(Odometer) - MIN(Odometer)) AS Distancia
			,MAX(VSSSpeed) AS Velocidad
		FROM #Data
		WHERE Id BETWEEN @vLastIterator
				AND @vIterator

		SET @vIdentidad = @@Identity

		IF @pIsAttrack = 0
		BEGIN
			UPDATE #Result
			SET #Result.Stops = v.Stops
			FROM (
				SELECT COUNT(VSSSpeed) Stops
				FROM #Data
				WHERE ReportId = @lStopDetail
					AND Id BETWEEN @vLastIterator
						AND @vIterator
				) v
			WHERE #Result.Id = @vIdentidad
		END
		ELSE
		BEGIN
			UPDATE #Result
			SET #Result.Stops = v.Stops
			FROM (
				SELECT COUNT(VSSSpeed) Stops
				FROM #Data
				WHERE VSSSpeed = 0
					AND Id BETWEEN @vLastIterator
						AND @vIterator
				) v
			WHERE #Result.Id = @vIdentidad
		END

		UPDATE #Result
		SET #Result.Evento = v.Evento
			,#Result.Latitud = v.Latitude
			,#Result.Longitud = v.Longitude
		FROM (
			SELECT (
					CASE inputStatus % 2
						WHEN 1
							THEN 'Encendido'
						ELSE 'Apagado'
						END
					) AS Evento
				,Latitude
				,Longitude
			FROM #Data
			WHERE id = @vIterator
			) AS v
		WHERE #Result.Id = @vIdentidad
	END

	UPDATE #Result
	SET Stops = 0
		,VeloMax = 0
	WHERE Evento = 'Apagado'

	DROP TABLE #Data

	DROP TABLE #ChangeEvent

	SET @lMinimumDistance = (
			SELECT pc.[Value]
			FROM [General].[Vehicles] v
			LEFT JOIN [General].[ParametersByCustomer] pc ON pc.CustomerId = v.CustomerId
			WHERE v.VehicleId = @pVehicleId
				AND pc.Name = 'MinimumDistanceBetweenPoints'
			)
	SET @lMinimumStopTime = (
			SELECT pc.[Value]
			FROM [General].[Vehicles] v
			LEFT JOIN [General].[ParametersByCustomer] pc ON pc.CustomerId = v.CustomerId
			WHERE v.VehicleId = @pVehicleId
				AND pc.Name = 'MinimumStopTime'
			)

	SELECT [Id] [GPSId]
		,@pVehicleId [VehicleId]
		,DATEADD(hh, @lTimeZoneParameter, FechaInicial) AS StartDate
		,ISNULL(DATEADD(hh, @lTimeZoneParameter, FechaFinal), @pEndDate) AS EndDate
		,CASE Evento
			WHEN 'Encendido'
				THEN ISNULL([Efficiency].[Fn_GetOverSpeedSummaryByVehicle](@pVehicleId, FechaInicial, FechaFinal, 0), '')
			ELSE ''
			END OverSpeedSummary
		,Evento AS [Status]
		,CONVERT(INT, VeloMax) AS MaxSpeed
		,Recorrido AS Odometer
		,Stops
		,ISNULL([Efficiency].[Fn_GetAddressByLatLon_Retrieve]([Longitud], [Latitud], @lCustomerId, NULL), '') AS [Location]
		,CONVERT(VARCHAR(256), [Longitud]) AS [Longitud]
		,CONVERT(VARCHAR(256), [Latitud]) AS [Latitud]
		,ISNULL(@lMinimumDistance, 0) AS 'MinimumDistanceBetweenPoints'
		,ISNULL(@lMinimumStopTime, 0) AS 'MinimumStopTime'
	FROM #Result
	ORDER BY 3

	DROP TABLE #Result
END
