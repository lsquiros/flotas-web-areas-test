-- ====================================  
-- Created By: Stefano Quir�s  
-- Create Date: 25/06/2020  
-- Description: Add Column HasWhatsApp 
-- ==================================== 

ALTER TABLE [General].[Users]
ADD [HasWhatsApp] BIT