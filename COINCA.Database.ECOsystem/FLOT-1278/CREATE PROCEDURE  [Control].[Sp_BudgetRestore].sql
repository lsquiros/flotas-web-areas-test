

IF EXISTS (SELECT * FROM SYS.OBJECTS WHERE NAME = 'Sp_BudgetRestore')
	DROP PROC [Control].[Sp_BudgetRestore]
GO

-- =============================================
-- Author:      Juan Carlos Santamaria V
-- Create Date: 2020-06-02
-- Description: update the budgets of the vehicles or drivers both liters and balances
-- =============================================
CREATE PROC [Control].[Sp_BudgetRestore]
AS
BEGIN

	/* FOR TESTING
	DECLARE @CustomerId int = 11761
	--*/
	
	CREATE TABLE #Customers
	(
		[CustomerId] INT
	   ,[IssueId] INT 
	)

	CREATE TABLE #CreditCards
	(
		[CreditCardId] INT 
	)

	CREATE TABLE #Result
	(
		[CustomerId] INT
	   ,[VehicleOrDriverId] INT
	   ,[CreditCardId] INT
	   ,[CreditCardAvailable] DECIMAL(16, 2)
	   ,[CreditCardAvailableLiters] DECIMAL(16,3)
	   ,[RealAvailable] DECIMAL(16,2)
	   ,[RealAvailableLiters] DECIMAL(16,3)
	   ,[RealSpend] DECIMAL(16,2)
	   ,[RealSpendLiters] DECIMAL(16,3)
	   ,[InsertDateVF] DATETIME
	   ,[ModifyDateVF] DATETIME
	   ,[InsertDateAVF] DATETIME
	   ,[ModifyDateAVF] DATETIME
	   ,[AmountVF] DECIMAL(16, 2)
	   ,[AdditionalAmountAVF] DECIMAL(16, 2)
	   ,[LastCustomerClosing] DATETIME
	   ,[DistributionType] INT  NULL /*Driver or Vehicle*/
	)


	BEGIN TRY

		INSERT INTO #Customers
		SELECT DISTINCT c.[CustomerId], c.[IssueForId]
		FROM [General].[Customers] c
		INNER JOIN [General].[CustomersByPartner] cp
			ON c.[CustomerId] = cp.[CustomerId]
		WHERE [IsActive] = 1 AND [IsDeleted] IS NOT NULL AND [IsDeleted] <> 1
		AND [PartnerId] IN (1023, 1026, 1029, 1031, 1032, 1033, 1035)
		---------------------------------------
		--AND C.CustomerId = @CustomerId   /*JSA FOR TESTING*/
		---------------------------------------


		WHILE EXISTS (SELECT * FROM #Customers)
		BEGIN

			DECLARE @lInsertDate DATETIME
			DECLARE @lCustomerId INT
			DECLARE @lIssueId INT 

			SELECT @lCustomerId = MIN(CustomerId) FROM #Customers
			SELECT @lIssueId = [IssueId] FROM #Customers WHERE [CustomerId] = @lCustomerId
			SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve](@lCustomerId)

			INSERT INTO #CreditCards
			SELECT [CreditCardId] 
			FROM [Control].[CreditCard] 
			WHERE [CustomerId] = @lCustomerId 
			AND [StatusId] IN (7, 8)
			------------------------------
			--and CreditCardId = 6679        /*JSA FOR TESTING*/
			------------------------------

			WHILE EXISTS (SELECT * FROM #CreditCards)
			BEGIN
		
				DECLARE @lAssignedAmount DECIMAL(16,2) = 0
				DECLARE @lAdditionalAmount DECIMAL(16,2) = 0
				DECLARE @lAssignedLiters DECIMAL(16, 3) = 0
				DECLARE @lAdditionalLiters DECIMAL(16, 3) = 0
				DECLARE @lInsertDateVF DATETIME
				DECLARE @lModifyDateVF DATETIME
				DECLARE @lInsertDateAVF DATETIME
				DECLARE @lModifyDateAVF DATETIME
				DECLARE @lRealAmount DECIMAL(16,2) = 0
				DECLARE @lRealLiters DECIMAL(16,3) = 0
				DECLARE @lCreditCardId INT		

				SELECT @lCreditCardId = MIN([CreditCardId]) FROM #CreditCards

				SET @lRealAmount = [Control].[Fn_FuelDistribution_TransactionsAmount](null,@lCreditCardId,-6, @lInsertDate, null, null)
				SET @lRealLiters = [Control].[Fn_FuelDistribution_TransactionsLiters](null,@lCreditCardId, -6, @lInsertDate, NULL, 1, null)							

				IF @lIssueId = 101
				BEGIN

					DECLARE @lVehicleId INT = NULL
					SELECT @lVehicleId = [VehicleId] 
					FROM [Control].[CreditCardByVehicle]
					WHERE [CreditCardId] = @lCreditCardId

					IF @lVehicleId IS NOT NULL
					BEGIN
						SELECT TOP 1 @lAssignedAmount =  SUM(ISNULL(vf.[Amount], 0))
							,@lAdditionalAmount =  SUM(ISNULL(vf.[AdditionalAmount], 0))
							,@lAssignedLiters = SUM(ISNULL(vf.[Liters], 0))
							,@lAdditionalLiters = SUM(ISNULL(vf.[AdditionalLiters], 0))
							,@lInsertDateVF = MIN(vf.[InsertDate])	
							,@lModifyDateVF = MIN(vf.[ModifyDate])			
						FROM [Control].[FuelDistribution] vf
						WHERE vf.[VehicleId] = @lVehicleId 
						AND [InsertDate] >= @lInsertDate
						GROUP BY vf.[VehicleId]	

						INSERT INTO #Result
						select cc.[CustomerId]
							  ,@lVehicleId
							  ,cc.[CreditCardId]
							  ,cc.[CreditAvailable]
							  ,cc.[AvailableLiters]
							  ,[RealAvailable]			= (@lAssignedAmount + @lAdditionalAmount) - @lRealAmount
							  ,[RealAvailableLiters]	= (@lAssignedLiters + @lAdditionalLiters) - @lRealLiters
							  ,[RealSpend]				= @lRealAmount
							  ,[RealSpendLiters]		= @lRealLiters
							  ,[InsertDateVF]			= @lInsertDateVF
							  ,[ModifyDateVF]			= @lModifyDateVF
							  ,[InsertDateAVF]			= @lInsertDateAVF
							  ,[ModifyDateAVF]			= @lModifyDateAVF
							  ,[AmountVF]				= @lAssignedAmount
							  ,[AdditionalAmountAVF]	= @lAdditionalAmount
							  ,[LastCustomerClosing]	= @lInsertDate
							  ,[DistributionType]		= @lIssueId
						FROM [Control].[CreditCard] cc
						WHERE cc.[CreditCardId] = @lCreditCardId AND cc.[StatusId] IN (7, 8)
						--(CONVERT(INT, cc.[CreditAvailable]) <>  CONVERT(INT, (@lAssignedAmount + @lAdditionalAmount) - @lRealAmount)
							   --OR CONVERT(INT, cc.[AvailableLiters]) <>  CONVERT(INT, (@lAssignedLiters + @lAdditionalLiters) - @lRealLiters)) 
						--AND 
					END			
				END			
				ELSE
				BEGIN
					DECLARE @lDriverId INT = NULL

					SELECT @lDriverId = [UserId] 
					FROM [Control].[CreditCardByDriver]
					WHERE [CreditCardId] = @lCreditCardId
			
					IF @lDriverId IS NOT NULL
					BEGIN 
						SELECT TOP 1 @lAssignedAmount =  ISNULL(vf.[Amount], 0)
								,@lAdditionalAmount =  ISNULL(vf.[AdditionalAmount], 0)
								,@lInsertDateVF = vf.[InsertDate]	
								,@lModifyDateVF = vf.[ModifyDate]				
						FROM [Control].[FuelDistribution] vf
						WHERE vf.[DriverId] = @lDriverId AND [InsertDate] >= @lInsertDate
						ORDER BY vf.[InsertDate] DESC	
				
						INSERT INTO #Result
						select cc.[CustomerId]
							  ,@lVehicleId
							  ,cc.[CreditCardId]
							  ,cc.[CreditAvailable]
							  ,cc.[AvailableLiters]
							  ,[RealAvailable]			= (@lAssignedAmount + @lAdditionalAmount) - @lRealAmount
							  ,[RealAvailableLiters]	= (@lAssignedLiters + @lAdditionalLiters) - @lRealLiters
							  ,[RealSpend]				= @lRealAmount
							  ,[RealSpendLiters]		= @lRealLiters
							  ,[InsertDateVF]			= @lInsertDateVF
							  ,[ModifyDateVF]			= @lModifyDateVF
							  ,[InsertDateAVF]			= @lInsertDateAVF
							  ,[ModifyDateAVF]			= @lModifyDateAVF
							  ,[AmountVF]				= @lAssignedAmount
							  ,[AdditionalAmountAVF]	= @lAdditionalAmount
							  ,[LastCustomerClosing]	= @lInsertDate
							  ,[DistributionType]		= @lIssueId
						FROM [Control].[CreditCard] cc		
						WHERE cc.[CreditCardId] = @lCreditCardId AND cc.[StatusId] IN (7, 8)
						--(CONVERT(INT, cc.[CreditAvailable]) <>  CONVERT(INT, (@lAssignedAmount + @lAdditionalAmount) - @lRealAmount) 
							   --OR CONVERT(INT, cc.[AvailableLiters]) <>  CONVERT(INT, (@lAssignedLiters + @lAdditionalLiters) - @lRealLiters))
						--AND 
					END				
				END	

				DELETE FROM #CreditCards WHERE [CreditCardId] = @lCreditCardId
			END

			DELETE FROM #Customers WHERE [CustomerId] = @lCustomerId
	
		END

		/****** BUDGET AND LITER UPDATES ARE INITIALIZED ******/
		UPDATE cc
		SET cc.[CreditAvailable] = [REALAVAILABLE]
		   ,cc.[AvailableLiters] = r.[rEALaVAILABLELITERS]
		FROM [Control].[CreditCard] cc
		INNER JOIN #Result r
			ON cc.[CreditCardId] = r.[CreditCardId]
		WHERE [REALAVAILABLE] <> cc.[CreditAvailable]
				OR r.[RealAvailableLiters] <> cc.[AvailableLiters]
		AND r.[RealAvailableLiters] > 0
		AND [REALAVAILABLE] > 0
		AND ABS((r.[RealAvailableLiters] - cc.[AvailableLiters])) > 1


		/*-------------------------------<for testing porpuse >--------------------------------------------------

		SELECT r.CustomerId
			 , r.VehicleOrDriverId
			 , cc.CreditCardId
			 , r.[DistributionType]
			 , r.[RealAvailableLiters]
			 , r.[RealAvailable]
			 , cc.[CreditAvailable]
			 , cc.[AvailableLiters]
			 ,'Dif --->' Dif
			 , LiterDifference = (r.[RealAvailableLiters] - cc.[AvailableLiters])
			 , AmountDifference = (r.[REALAVAILABLE] - cc.[CreditAvailable] )
		into #Listado
		FROM [Control].[CreditCard] cc
		INNER JOIN #Result r ON cc.[CreditCardId] = r.[CreditCardId]
		WHERE r.[REALAVAILABLE] <> cc.[CreditAvailable] 
		AND [REALAVAILABLE] > 0
		 OR (r.[RealAvailableLiters] <> cc.[AvailableLiters] 
			AND r.[RealAvailableLiters] > 0)
		--and abs((r.[RealAvailableLiters] - cc.[AvailableLiters])) > 1


		select 
		b.Name as CustomerName
		,c.PlateId
		,pa.Name as Partner
		,a.CustomerId
		,a.VehicleOrDriverId
		,a.CreditCardId
		,a.[DistributionType]
		,a.[RealAvailableLiters]
		,a.[RealAvailable]
		,a.[CreditAvailable]
		,a.[AvailableLiters]
		,a.Dif
		,a.LiterDifference 
		,a.AmountDifference 
		from #Listado a
		Inner Join General.Customers b on a.CustomerId = b.CustomerId
		inner join General.Vehicles c on a.VehicleOrDriverId = c.VehicleId
		Inner Join General.CustomersByPartner d on b.CustomerId = d.CustomerId
		inner join General.partners pa on d.PartnerId = pa.PartnerId
		order by c.PlateId

		DROP TABLE #Listado	
	
		--*/

		DROP TABLE #Result
		DROP TABLE #CreditCards
		DROP TABLE #Customers
	END TRY
	BEGIN CATCH	
		DECLARE @lErrorMessage NVARCHAR(MAX)
		DECLARE @ErrorSeverity INT;  
		DECLARE @ErrorState INT; 
         
		SELECT
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  

		SELECT @lErrorMessage = CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE(), ', ERROR_SEVERITY:',@ErrorSeverity,', ERROR_STATE:',@ErrorState)  		
		
		INSERT INTO [General].[EventDBLog] (
			 [ProcessName]
			,[Message]
			,[IsInfo]
			,[IsWarning]
			,[IsError]
			,[LogUTCDateTime])  
		VALUES ('[Control].[Sp_BudgetRestore]'
				,@lErrorMessage
				,0
				,0
				,1
				,GETDATE())
	END CATCH

END

GO
