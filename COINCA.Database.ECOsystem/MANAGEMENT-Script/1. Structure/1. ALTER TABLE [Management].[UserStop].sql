USE [ECOsystemQA]
GO

 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 07/03/2019
-- Description:	Changes in the structure of the table of stops, so that there can only be one record per agent
-- =================================================================================================
ALTER TABLE [Management].[UserStop]
ALTER COLUMN [UserId] INT NOT NULL

GO

ALTER TABLE [Management].[UserStop]
ALTER COLUMN [CustomerId] INT NOT NULL

GO

ALTER TABLE [Management].[UserStop]
ADD CONSTRAINT UserStop_PK PRIMARY KEY ([UserId], [CustomerId]);