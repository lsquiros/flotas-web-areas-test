USE [ECOsystemQA]
GO

 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 07/03/2019
-- Description:	Add flag to identify location in stop
-- =================================================================================================

ALTER TABLE [Management].[UserEvent] 
ADD [Stoped] BIT DEFAULT 0