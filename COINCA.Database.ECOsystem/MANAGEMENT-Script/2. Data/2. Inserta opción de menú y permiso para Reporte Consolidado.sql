--USE [ECOsystemQA]
GO

-- ================================================================================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 19/03/2019
-- Description:	Menu option added for Consolidated Report
-- ================================================================================================

DECLARE @NewPermID NVARCHAR(128) = NEWID()
	   ,@lParentId INT
	   ,@lOrder INT
	   
INSERT INTO [dbo].[AspNetPermissions]
VALUES
(
	@NewPermID
	,'View_ManagementConsolidatedReport'
	,'ManagementConsolidatedReport'
	,'Index'
	,''
	,GETDATE()
)

SELECT @lParentId = [Id]
FROM [dbo].[AspMenus]
WHERE [Name] = 'Reportes'
AND [Header] = 22

--UPDATE [dbo].[AspMenus]
--SET [Order] = [Order] + 1
--WHERE [Parent] = @lParentId 
--AND [Order] >= 95 
--AND [Order] < 100

SELECT @lOrder = MAX([Order])
FROM [dbo].[AspMenus]
WHERE [Parent] = @lParentId
AND [Header] = 22

INSERT INTO [dbo].[AspMenus]
VALUES
(
	'Consolidado',
	'',
	@NewPermID,
	@lParentId,
	22,
	@lOrder,
	GETDATE()
)

DECLARE @NewMenuID INT = SCOPE_IDENTITY()

CREATE TABLE #Roles
(
	[Id] INT IDENTITY
   ,[RoleId] VARCHAR(MAX)
)

INSERT INTO #Roles
SELECT [Id] FROM [AspNetRoles] 
WHERE [Name] = 'Super Usuario Cliente'
AND [Active] = 1

DECLARE @lRoleId VARCHAR(MAX)
	   ,@lId INT

WHILE EXISTS(SELECT * FROM #Roles)
BEGIN

	SELECT @lId = MIN([Id]) 
	FROM #Roles

	SELECT @lRoleId = [RoleId] 
	FROM #Roles 
	WHERE [Id] = @lId

	--Insert the menu and the permission to all 'Super Usuario Cliente' created on the platform
	INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES(@lRoleId, @NewMenuID) 

	INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES(@lRoleId, @NewPermID) 

	DELETE FROM #Roles
	WHERE [RoleId] = @lRoleId
END

DROP TABLE #Roles


--INSERT PERMISSIONS TO THE MENU AND ROLE

INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('454df528-3a24-4361-9425-25426682fa70', @NewMenuID) 
INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('9baac678-473a-4f96-9d67-9f2a5af778f3', @NewMenuID) 

INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('454df528-3a24-4361-9425-25426682fa70', @NewPermID) 
INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('9baac678-473a-4f96-9d67-9f2a5af778f3', @NewPermID) 