USE [ECOsystemQA]
GO

-- ================================================================================
-- Author:       Marjorie Grabanzo Morales
-- Create date:  08/03/2019
-- Description:  Add static parameters for management
-- ================================================================================

INSERT INTO [dbo].[GeneralParameters]
VALUES
(
	'GRESTER_ACCURACY_GPS',
	'NA',
	'Exactitud maxima para aceptar los puntos del GPS',
	100,
	100
)

INSERT INTO [dbo].[GeneralParameters]
VALUES
(
	'DISTANCE_WITHIN_MANAGEMENT',
	'NA',
	'Distancia maxima sin movimiento dentro de una gestion',
	0.030,
	0.030
)