--USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_MeasuringAgentsMonthlyReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_MeasuringAgentsMonthlyReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:		Henry Retana
-- Create date: 21/07/2017
-- Description:	Retrieve data for Report Measuring Agents Monthly
-- Modify by:   Marjorie Garbanzo - 06/04/2019 - Update data received from the SP [Management].[Sp_ReconstructionCleanData_Retrieve]
-- ======================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_MeasuringAgentsMonthlyReport]
(
	@pCustomerId INT,
	@pStartDate DATETIME, 
	@pEndDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON

	--set @pStartDate = cast(@pStartDate as date)
	--set @pEndDate = dateadd(day, 1, cast(@pEndDate as date))
	--set @pEndDate = dateadd(s, -1, @pEndDate)


	DECLARE @lCount INT,
			@lLogIn INT,
			@lLogOut INT,
			@lStartVisit INT,
			@lEndVisit INT,
			@lLocation INT,			
			@lUserId INT,
			@lMainEvents INT,
			@lStopEvent INT,
			@lInitLunch INT, 
			@lTimeZone INT



	SELECT @lTimeZone = co.[TimeZone]
	FROM [General].[Customers] c
	INNER JOIN [General].[Countries] co
		ON co.[CountryId] = c.[CountryId]
	WHERE c.[CustomerId] = @pCustomerId 

	--GET THE IDS FOR THE EVENTS--
	SELECT @lLogout = c.[Id],
		   @lLogin = c.[Parent]
	FROM [Management].[CatalogDetail] c
	INNER JOIN [dbo].[GeneralParameters] p
		ON c.[Parent] = p.[Value]
	WHERE p.[ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'	

	SELECT @lEndVisit= c.[Id],
		   @lStartVisit = c.[Parent]
	FROM [Management].[CatalogDetail] c
	INNER JOIN [dbo].[GeneralParameters] p
		ON c.[Parent] = p.[Value]
	WHERE p.[ParameterID] = 'CUSTOMERS_CLIENT_BEGIN_VISIT'
	
	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'

	SELECT @lMainEvents = [Id] 
	FROM [Management].[Catalog]
	WHERE [Name] = 'Tipos de Eventos Principales'

	SELECT @lStopEvent = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'AGENT_STOP'

	SELECT @lInitLunch = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'CUSTOMERS_LUNCH_TIME_BEGIN'
	--**************************--

	CREATE TABLE #DATA 
	(	
		[Id] INT,
		[UserId] INT,
		[UserEventId] INT,
		[EventDetailId] INT,
		[EventDetailName] VARCHAR(200),
		[EventTypeId] INT,
		[EventName] VARCHAR(200),
		[Parent] INT,
		[CommerceId] INT,
		[Latitude] FLOAT,
		[Longitude] FLOAT,
		[BatteryLevel] INT,
		[Speed] DECIMAL(4, 0),
		[EventDate] DATETIME,
		[Lapse] INT,
		[Distance] DECIMAL(16, 2)
	)

	CREATE TABLE #TempData
	(
		[UserId] INT,
		[EventTypeId] INT,
		[Seconds] INT
	)

	CREATE TABLE #LOGINS
	(
		[UserId] INT,
		[LogIn] DATETIME,
		[LogOut] DATETIME
	)

	CREATE TABLE #Users
	(
		[Id] INT IDENTITY,
		[UserId] INT
	)

	CREATE TABLE #Result
	(
		[AgentWorkTime] VARCHAR(20), 
		[AgentClientTime] VARCHAR(20), 
		[AgentMovingTime] VARCHAR(20),
		[AgentLostTime] VARCHAR(20),
		[AgentLogIn] DATETIME,
		[AgentLogOut] DATETIME,
		[ClientDifference] INT,
		[MovingDifference] INT, 
		[LostDifference] INT, 
		[LogInDifference] INT,
		[LogOutDifference] INT,
		[ClientTime] VARCHAR(20),
		[LostTime] VARCHAR(20),
		[MovingTime] VARCHAR(20),
		[StartTime] INT,
		[EndTime] INT,
		[LogInScore] INT,
		[LogOutScore] INT,
		[ClientScore] INT,
		[MovingScore] INT,
		[LostScore] INT,		
		[EncryptDriverName] VARCHAR(500),
		[TopMargin] INT,
		[LowMargin] INT,
		[TopPercent] INT,
		[LowPercent] INT,
		[MiddlePercent] INT,
		[UserId] INT
	)

	INSERT INTO #Users
	SELECT DISTINCT du.[UserId]
	FROM [General].[DriversUsers] du
	INNER JOIN [Management].[UserEvent] m
		ON du.[UserId] = m.[UserId]
	WHERE du.[CustomerId] = @pCustomerId
	AND m.[EventDate] BETWEEN @pStartDate AND @pEndDate
	
	SELECT @lCount = MIN([Id])
	FROM #Users

	WHILE @lCount IS NOT NULL 
	BEGIN
		DECLARE @lRowId INT = NULL

		SELECT @lUserId = [UserId]
		FROM #Users
		WHERE [Id] = @lCount
				
		INSERT INTO #DATA
		EXEC [Management].[Sp_ReconstructionCleanData_Retrieve] @lUserId, @pStartDate, @pEndDate, 0
		
		SELECT @lRowId = MIN([Id])
		FROM #Data
	
		--GO THROUGH THE DATA 
		WHILE @lRowId IS NOT NULL 
		BEGIN 
			DECLARE @lEventTypeId INT = NULL,
					@lStartTime DATETIME = NULL,
					@lEndTime DATETIME = NULL,
					@lFinishId INT = NULL															

			SELECT @lEventTypeId = [EventTypeId], 
				   @lStartTime = [EventDate]
			FROM #Data
			WHERE [Id] = @lRowId

			--VALIDATE EVENTS 
			--Login
			IF @lEventTypeId = @lLogIn
			BEGIN 
				INSERT INTO #LOGINS
				(
					[UserId],
					[LogIn]
				)				
				SELECT @lUserId,
					   CONVERT(CHAR(8),CAST(AVG(CAST([EventDate] AS FLOAT) - FLOOR(CAST([EventDate] AS FLOAT))) AS DATETIME), 108)
				FROM #Data
				WHERE [EventTypeId] = @lLogIn

				SELECT @lStartTime = CONVERT(CHAR(8),CAST(AVG(CAST([EventDate] AS FLOAT) - FLOOR(CAST([EventDate] AS FLOAT))) AS DATETIME), 108)
				FROM #Data
				WHERE [EventTypeId] = @lLogIn

				UPDATE 	#LOGINS
				SET [LogOut] = (SELECT CONVERT(CHAR(8),CAST(AVG(CAST([EventDate] AS FLOAT) - FLOOR(CAST([EventDate] AS FLOAT))) AS DATETIME), 108)
								FROM #Data
								WHERE [EventTypeId] = @lLogOut)
				WHERE [UserId] = @lUserId
								
				SELECT TOP 1 @lEndTime = (SELECT CONVERT(CHAR(8),CAST(AVG(CAST([EventDate] AS FLOAT) - FLOOR(CAST([EventDate] AS FLOAT))) AS DATETIME), 108)
										  FROM #Data
										  WHERE [EventTypeId] = @lLogOut)
				FROM #Data
				WHERE [EventTypeId] = @lLogOut 
				AND   [Id] > @lRowId
				ORDER BY [EventDate] DESC

				--VALIDATES NULL
				SELECT @lEndTime = ISNULL(@lEndTime, CAST(@pEndDate AS TIME)),
					   @lStartTime = ISNULL(@lStartTime, CAST(@pStartDate AS TIME))
				
				DELETE FROM #Data
				WHERE [EventTypeId] = @lLogIn
				AND [Id] <> @lRowId
			END
			--Visit
			ELSE IF @lEventTypeId = @lStartVisit
			BEGIN 
				SELECT TOP 1 @lFinishId = [Id],  
							 @lEndTime = [EventDate]
				FROM #Data
				WHERE [EventTypeId] = @lEndVisit
				AND   [Id] > @lRowId
				ORDER BY [Id] 
			END
			--Moving
			ELSE IF @lEventTypeId = @lLocation
			BEGIN 
				DECLARE @lEndMovement DATETIME = NULL 

				SET @lEndMovement = CASE WHEN @pEndDate = DATEADD(S, -1, DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, @lTimeZone, GETDATE())), 1))   
										 THEN DATEADD(HOUR, @lTimeZone, GETDATE())
										 ELSE @pEndDate
									END

				SELECT TOP 1 @lFinishId = [Id],
							 @lEndTime = [EventDate]
				FROM #Data
				WHERE [EventTypeId] <> @lLocation
				AND   [Id] > @lRowId				
				ORDER BY [Id]	
				
				SELECT @lEndTime = CASE WHEN @lEndTime IS NULL THEN @lEndMovement ELSE @lEndTime END 

			END			
			ELSE IF EXISTS (
								SELECT [Id]
								FROM [Management].[CatalogDetail]
								WHERE [Id] NOT IN (@lStartVisit, @lLogIn)
								AND [Id] = @lEventTypeId
								AND [CatalogId] = @lMainEvents
								AND [Parent] = 0
						   )	
			BEGIN 
				SELECT TOP 1 @lFinishId = [Id],  
							 @lEndTime = [EventDate]
				FROM #Data
				WHERE [EventTypeId] IN (
											SELECT [Id]
											FROM [Management].[CatalogDetail]
											WHERE [Parent] = @lEventTypeId
									   )
				AND   [Id] > @lRowId
				ORDER BY [Id]
			END
					
			--Insert data in temp table
			INSERT INTO #TempData
			VALUES 
			(
				@lUserId,
				@lEventTypeId,
				DATEDIFF(SECOND, @lStartTime, @lEndTime)
			)

			SELECT @lRowId = MIN([Id])
			FROM #Data
			WHERE [Id] > @lRowId
		END 

		TRUNCATE TABLE #Data

		--GET STOP TIME
		INSERT INTO #DATA
		EXEC [Management].[Sp_ReconstructionCleanData_Retrieve] @lUserId, @pStartDate, @pEndDate, 1

		INSERT INTO #TempData
		SELECT @lUserId,
			   @lStopEvent,
			   SUM([Lapse])
		FROM #DATA
		WHERE [EventTypeId] = @lStopEvent
		
		TRUNCATE TABLE #Data

		SELECT @lCount = MIN([Id])
		FROM #Users
		WHERE [Id] > @lCount
	END 

	SELECT @lUserId = MIN([UserId])
	FROM #TempData 
	
	WHILE @lUserId IS NOT NULL
	BEGIN
		DECLARE @lLogInTime DATETIME = NULL,
				@lLogOutTime DATETIME = NULL,
				@lLostTime INT = 0,
				@lWorkTime VARCHAR(20) = NULL,						
				@lClientTime VARCHAR(20) = NULL,
				@lMovingTime VARCHAR(20) = NULL,			
				@lWorkMinutes INT = NULL,						
				@lClientMinutes INT = NULL,
				@lMovingMinutes INT = NULL,
				@lStopTime INT = NULL, 
				@lLunchTime INT = NULL,
				@lLogInDifference INT = NULL,
				@lLogOutDifference INT = NULL

		SELECT @lLogInTime = ISNULL([LogIn], CAST(@pStartDate AS TIME)),
			   @lLogOutTime = ISNULL([LogOut], CAST(@pEndDate AS TIME))
		FROM #LOGINS 
		WHERE [UserId] = @lUserId

		--Get the final Times result 
		SELECT @lWorkTime = [General].[Fn_GetTimeFormat] (CAST(ROUND(SUM([Seconds]) / 3600, 0, 1) AS VARCHAR), RIGHT('0' + CAST(ROUND(SUM([Seconds]) / 60, 0, 1) % 60 AS VARCHAR), 2), 0),
			   @lWorkMinutes = ROUND(SUM([Seconds]) / 60, 0, 1)
		FROM #TempData
		WHERE [EventTypeId] = @lLogIn
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]

		SELECT @lClientTime = [General].[Fn_GetTimeFormat] (CAST(ROUND(SUM([Seconds]) / 3600, 0, 1) AS VARCHAR), RIGHT('0' + CAST(ROUND(SUM([Seconds]) / 60, 0, 1) % 60 AS VARCHAR), 2), 0),
			   @lClientMinutes = ROUND(SUM([Seconds]) / 60, 0, 1)  
		FROM #TempData
		WHERE [EventTypeId] = @lStartVisit
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]

		--GET LUNCH TIME 
		SELECT @lLunchTime = ROUND(SUM([Seconds]) / 60, 0, 1)   
		FROM #TempData
		WHERE [EventTypeId] = @lInitLunch
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]

		SELECT @lMovingMinutes = ROUND(SUM([Seconds]) / 60, 0, 1)
		FROM #TempData
		WHERE [EventTypeId] = @lLocation
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]
		
		--GET STOP TIME 
		SELECT @lStopTime = ROUND(SUM([Seconds]) / 60, 0, 1)					 		
		FROM #TempData
		WHERE [EventTypeId] = @lStopEvent
		AND   [UserId] = @lUserId 
		
		SELECT @lMovingMinutes =  @lMovingMinutes - ISNULL(@lStopTime, 0) - ISNULL(@lLunchTime, 0)
		SELECT @lMovingMinutes = CASE WHEN @lMovingMinutes < 0  THEN 0 ELSE @lMovingMinutes END 
				
		SELECT @lMovingTime = [General].[Fn_GetTimeFormat] (CAST(@lMovingMinutes / 60 AS VARCHAR), RIGHT('0' + CAST(@lMovingMinutes % 60 AS VARCHAR), 2), 0)

		--GET LOST TIME 
		SELECT @lLogInDifference = ISNULL(DATEDIFF(MINUTE, @lLogInTime, DATEADD(HOUR, [TimeBeginJourney], CONVERT(DATETIME, DATEDIFF(DAY, 0, @lLogInTime)))), 0)
		FROM [Management].[Parameters]
		WHERE [CustomerId] = @pCustomerId

		SELECT @lLogOutDifference = ISNULL(DATEDIFF(MINUTE, DATEADD(HOUR, [TimeFinishJourney], CONVERT(DATETIME, DATEDIFF(DAY, 0, @lLogOutTime))), @lLogOutTime), 0)
		FROM [Management].[Parameters]
		WHERE [CustomerId] = @pCustomerId

		--LOGIN LATE
		SELECT @lLostTime = @lLostTime + CASE WHEN @lLogInDifference < 0 
											  THEN @lLogInDifference * -1
											  ELSE 0
										 END
		--LOGOUT EARLY
		SELECT @lLostTime = @lLostTime + CASE WHEN @lLogOutDifference < 0 
											  THEN @lLogOutDifference * -1
											  ELSE 0
										 END						 										 		
		--STOP TIME
		SELECT @lLostTime = @lLostTime + ISNULL(@lStopTime, 0) --+ ISNULL(@lLunchTime, 0)
		--**************************************************************************************************

		INSERT INTO #Result
		SELECT ISNULL(@lWorkTime, '00:00:00'),
			   ISNULL(@lClientTime, '00:00:00'),
			   ISNULL(@lMovingTime, '00:00:00'),
			   ISNULL([General].[Fn_GetTimeFormat] (CAST(@lLostTime / 60 AS VARCHAR), RIGHT('0' + CAST(@lLostTime % 60 AS VARCHAR), 2), 0), 0),
			   ISNULL(@lLogInTime, 0),
			   ISNULL(@lLogOutTime, 0),
			   ISNULL(@lClientMinutes, 0) - [ClientMinutes],
			   ISNULL(@lMovingMinutes, 0) - [MovingMinutes], 
			   ISNULL([LostMinutes] - @lLostTime, 0),
			   @lLogInDifference, 
			   @lLogOutDifference,
			   [General].[Fn_GetTimeFormat] (CAST([ClientMinutes] / 60 AS VARCHAR), RIGHT('0' + CAST([ClientMinutes] % 60 AS VARCHAR), 2), 0),
			   [General].[Fn_GetTimeFormat] (CAST([LostMinutes] / 60 AS VARCHAR), RIGHT('0' + CAST([LostMinutes] % 60 AS VARCHAR), 2), 0),
			   [General].[Fn_GetTimeFormat] (CAST([MovingMinutes] / 60 AS VARCHAR), RIGHT('0' + CAST([MovingMinutes] % 60 AS VARCHAR), 2), 0),
			   [TimeBeginJourney],
			   [TimeFinishJourney],
			   --GET SCORES
			   [Management].[Fn_GetManagementScoreByParameters] 
			   (
					@lLogInDifference, 
					NULL, 
					NULL, 
					NULL, 
					NULL, 
					[TopMargin], 
					[LowMargin], 
					[TopPercent], 
					[MiddlePercent], 
					[LowPercent]
			    ),
				[Management].[Fn_GetManagementScoreByParameters] 
				(
					NULL, 
					@lLogOutDifference,
					NULL, 
					NULL, 
					NULL, 
					[TopMargin], 
					[LowMargin], 
					[TopPercent], 
					[MiddlePercent], 
					[LowPercent]
				),
				[Management].[Fn_GetManagementScoreByParameters] (NULL, NULL, ISNULL(@lClientMinutes, 0) - [ClientMinutes], NULL, NULL, [TopMargin], [LowMargin], [TopPercent], [MiddlePercent], [LowPercent]),
				[Management].[Fn_GetManagementScoreByParameters] (NULL, NULL, NULL, ISNULL(@lMovingMinutes, 0) - [MovingMinutes], NULL, [TopMargin], [LowMargin], [TopPercent], [MiddlePercent], [LowPercent]),
				[Management].[Fn_GetManagementScoreByParameters] (NULL, NULL, NULL, NULL, ISNULL(@lLostTime, 0) - [LostMinutes], [TopMargin], [LowMargin], [TopPercent], [MiddlePercent], [LowPercent]),
			    ---------------------
				(
					SELECT [Name]
					FROM [General].[DriversUsers] du
					INNER JOIN [General].[Users] u
						ON du.[UserId] = u.[UserId]
					WHERE [DriversUserId] = @lUserId
				),
			   [TopMargin],
			   [LowMargin],
			   [TopPercent],
			   [LowPercent],
			   [MiddlePercent],
			   @lUserId
		FROM [Management].[Parameters]
		WHERE [CustomerId] = @pCustomerId

		SELECT @lUserId = MIN([UserId])
		FROM #TempData
		WHERE [UserId] > @lUserId
	END

	--Returns Results with all the information
	SELECT r.userid, r.[AgentWorkTime], 
		   r.[AgentClientTime], 
		   r.[AgentMovingTime],
		   r.[AgentLostTime],
		   r.[AgentLogIn],
		   r.[AgentLogOut],
		   r.[ClientDifference],
		   r.[MovingDifference], 
		   r.[LostDifference], 
		   r.[LogInDifference],
		   r.[LogOutDifference],
		   r.[ClientTime],
		   r.[LostTime],
		   r.[MovingTime],
		   r.[StartTime],
		   r.[EndTime],
		   r.[LogInScore],
		   r.[LogOutScore],
		   r.[ClientScore],
		   r.[MovingScore],
		   r.[LostScore],
		   u.[Name] [EncryptDriverName],
		   r.[TopMargin],
		   r.[LowMargin],
		   r.[TopPercent],
		   r.[LowPercent],
		   r.[MiddlePercent]
	FROM #Result r
	INNER JOIN [General].[Users] u
		ON r.[UserId] = u.[UserId]

	DROP TABLE #Data
			  ,#Users
			  ,#TempData
			  ,#Result
			  ,#LOGINS

	SET NOCOUNT OFF
END