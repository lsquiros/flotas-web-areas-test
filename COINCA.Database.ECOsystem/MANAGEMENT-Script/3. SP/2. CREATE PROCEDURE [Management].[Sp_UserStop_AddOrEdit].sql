--USE [ECOSystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_UserStop_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_UserStop_AddOrEdit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana 
-- Create date: 17/10/2018
-- Description:	Add or Edit the User Stop
-- Modify by:   Marjorie Garbanzo - 30/10/2018 - Change the conditions 
-- Modify by:   Marjorie Garbanzo - 23/11/2018 - Delete the condition of Location
-- Modify by:   Marjorie Garbanzo - 07/03/2019 - Change to insert the stop as soon as it starts
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_UserStop_AddOrEdit]
(
	  @pEventTypeId INT
	 ,@pUserId INT		
	 ,@pCustomerId INT
	 ,@pLatitude FLOAT
	 ,@pLongitude FLOAT
	 ,@pBatteryLevel INT = NULL
	 ,@pHeading DECIMAL(4,0) = NULL
	 ,@pSpeed DECIMAL(4,0) = NULL
	 ,@pAltitude DECIMAL(4,0) = NULL
	 ,@pEventDateTime DATETIME
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0
			   ,@lJourneyEnd INT 
			   ,@lEventStopId INT

		DECLARE @lResult TABLE 
		(
			[Lapse] INT, 
			[IsStop] BIT,
			[Update] BIT
		)
			    
        IF @@TRANCOUNT = 0
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END

		--GET THE STOP EVENT TYPE
		SELECT @lEventStopId = [Id] 
		FROM [Management].[CatalogDetail]
		WHERE [Value] = 'AGENT_STOP'
		
		--VALIDATES IF THE EVENT IS A STOP
		INSERT INTO @lResult
		SELECT *
		FROM [Management].[Fn_GetStatusOfStop] (
													@pUserId 
													,@pCustomerId 
													,@pLatitude  
													,@pLongitude 
													,@pSpeed
													,@pEventDateTime  
													,(SELECT CAST(COUNT(1) AS BIT)
														FROM [Management].[UserStop]
														WHERE [UserId] = @pUserId
															AND [CustomerId] = @pCustomerId)    
												)

		--IF THE EVENT IS NOT A STOP UPDATE THE DATA FOR THE USER
		IF EXISTS (
					SELECT *
					FROM @lResult
					WHERE [IsStop] = 0
					AND [Update] = 1
					)
		BEGIN--Posible parada
			IF (SELECT CAST(COUNT(1) AS BIT)
				FROM [Management].[UserStop]
				WHERE [UserId] = @pUserId
					AND [CustomerId] = @pCustomerId) = 0 
			BEGIN
				INSERT INTO [Management].[UserStop]
					(
						[UserId],
						[CustomerId],
						[Latitude],
						[Longitude],
						[BatteryLevel],
						[Heading],
						[Speed],
						[Altitude],
						[EventDate],
						[InitStopDate]	
					)	
					VALUES
					(
						 @pUserId			
						,@pCustomerId 
						,@pLatitude 
						,@pLongitude 
						,@pBatteryLevel 
						,@pHeading 
						,@pSpeed 
						,@pAltitude 
						,NULL
						,@pEventDateTime
					)
			END
			ELSE
			BEGIN
				UPDATE [Management].[UserStop]
				SET [BatteryLevel] = @pBatteryLevel,
					[Heading] = @pHeading,
					[Speed] = @pSpeed,
					[Altitude] = @pAltitude--,
					--[EventDate] = @pEventDateTime
				WHERE [UserId] = @pUserId
				AND [CustomerId] = @pCustomerId
			END
		END
		ELSE IF EXISTS (
							SELECT *
							FROM @lResult
							WHERE [IsStop] = 1
							AND [Update] = 1
						)
		BEGIN --Dentro de parada
			UPDATE [Management].[UserStop]
			SET [BatteryLevel] = @pBatteryLevel,
				[Heading] = @pHeading,
				[Speed] = @pSpeed,
				[Altitude] = @pAltitude,
				[EventDate] = @pEventDateTime
			WHERE [UserId] = @pUserId
			AND [CustomerId] = @pCustomerId


			IF (SELECT CAST(COUNT(1) AS BIT)
				FROM [Management].[UserEvent]
				WHERE [UserId] = @pUserId
					AND [CustomerId] = @pCustomerId
				AND [EventTypeId] = @lEventStopId
				AND [EventDate] = (SELECT [InitStopDate]
									FROM [Management].[UserStop]
									WHERE [UserId] = @pUserId
										AND [CustomerId] = @pCustomerId)) = 0 
			BEGIN
			
				--actualizar de la fecha inicio de parada hasta actual Stoped = 1
				UPDATE [Management].[UserEvent]
				SET [Stoped] = 1,
				    [ModifyDate] = GETDATE(),
				    [ModifyUserId] = [UserId]
				WHERE [UserId] = @pUserId
				AND [CustomerId] = @pCustomerId
				AND [EventDate] >= (SELECT [InitStopDate]
									FROM [Management].[UserStop]
									WHERE [UserId] = @pUserId
										AND [CustomerId] = @pCustomerId)

				--INSERT THE EVENT AS STOP
				INSERT INTO [Management].[UserEvent]
				(				 
					 [EventTypeId]
					,[UserId]
					,[CustomerId]
					,[Latitude]
					,[Longitude]
					,[BatteryLevel]
					,[Heading]
					,[Speed]
					,[Altitude]
					,[Lapse]
					,[EventDate]
					,[InsertDate]
					,[InsertUserId]
				)
				SELECT @lEventStopId,
					   [UserId],
					   [CustomerId],
					   [Latitude],
					   [Longitude],
					   [BatteryLevel],
					   [Heading],
					   [Speed],
					   [Altitude],
					   DATEDIFF(SECOND, [InitStopDate], @pEventDateTime) [Lapse],
					   [InitStopDate],
					   GETDATE(),
					   [UserId]
				FROM [Management].[UserStop]
				WHERE [UserId] = @pUserId
				AND [CustomerId] = @pCustomerId
			END
			ELSE
			BEGIN
				UPDATE [Management].[UserEvent]
				SET [BatteryLevel] = @pBatteryLevel,
					[Heading] = @pHeading,
					[Speed] = @pSpeed,
					[Altitude] = @pAltitude,
					--[EventDate] = @pEventDateTime
					[Lapse] = DATEDIFF(SECOND, [EventDate], @pEventDateTime),
				    [ModifyDate] = GETDATE(),
				    [ModifyUserId] = [UserId]
				WHERE [UserId] = @pUserId
				AND [CustomerId] = @pCustomerId
				AND [EventTypeId] = @lEventStopId
				AND [EventDate] = (SELECT [InitStopDate]
									FROM [Management].[UserStop]
									WHERE [UserId] = @pUserId
										AND [CustomerId] = @pCustomerId)
			END

		END
		ELSE IF EXISTS (
							SELECT *
							FROM @lResult
							WHERE [IsStop] = 0
							AND [Update] = 0
						)
		BEGIN --No es parada 
			--REMOVE HISTORIC
			DELETE FROM [Management].[UserStop]
			WHERE [UserId] = @pUserId
			AND [CustomerId] = @pCustomerId
		END 

		--IF THE EVENT IS A STOP, GET THE DATA FOR THE EVENT INSERTED
		ELSE IF EXISTS (
					SELECT *
					FROM @lResult
					WHERE [IsStop] = 1
					AND [Update] = 0
				  )
		BEGIN --Termina parada
			UPDATE [Management].[UserEvent]
				SET [BatteryLevel] = @pBatteryLevel,
					--[Heading] = @pHeading,
					--[Speed] = @pSpeed,
					--[Altitude] = @pAltitude,
					--[EventDate] = @pEventDateTime
					[Lapse] = DATEDIFF(SECOND, [EventDate], @pEventDateTime),
				    [ModifyDate] = GETDATE(),
				    [ModifyUserId] = [UserId]
				WHERE [UserId] = @pUserId
				AND [CustomerId] = @pCustomerId
				AND [EventTypeId] = @lEventStopId
				AND [EventDate] = (SELECT [InitStopDate]
									FROM [Management].[UserStop]
									WHERE [UserId] = @pUserId
										AND [CustomerId] = @pCustomerId)

			--REMOVE HISTORIC
			DELETE FROM [Management].[UserStop]
			WHERE [UserId] = @pUserId
			AND [CustomerId] = @pCustomerId

		END
			     
        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION			
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF 
END
