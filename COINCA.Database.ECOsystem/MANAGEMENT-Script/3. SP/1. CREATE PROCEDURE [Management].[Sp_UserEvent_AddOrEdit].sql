--USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_UserEvent_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_UserEvent_AddOrEdit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 18/09/2018
-- Description:	Add or Edit the UserEvent
-- Modify by:   Marjorie Garbanzo - 08/11/2018 - Do not validate stops within events
-- Modify by:   Marjorie Garbanzo - 21/11/2018 - Add Accuracy validate
-- Modify by:   Marjorie Garbanzo - 24/11/2018 - Improvement in the condition of entry and exit of the stop
-- Modify by:   Henry Retana - 29/11/2018 - Add validate to the distance
-- Modify by:   Marjorie Garbanzo - 30/11/2018 - Delete history in the Journey Begin event
-- Modify by:   Marjorie Garbanzo - 14/12/2018 - Modify @lGreaterAccuracy value of 75 the 100
-- Modify by:   Marjorie Garbanzo - 29/01/2019 - Add the value for location point accuracy
-- Modify by:   Marjorie Garbanzo - 07/03/2019 - Change to insert the stop as soon as it starts
-- Modify by:   Marjorie Garbanzo - 06/04/2019 - Change to insert the events and the responses of the form in a single transaction
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_UserEvent_AddOrEdit] 
(
	  @pEventDetailId INT = NULL						
	 ,@pCommerceId INT = NULL
	 ,@pTempCommerceId BIGINT = NULL
	 ,@pLatitude FLOAT = NULL
	 ,@pLongitude FLOAT = NULL
	 ,@pBatteryLevel INT = NULL
	 ,@pAccuracy DECIMAL(4,0) = NULL
	 ,@pHeading DECIMAL(4,0) = NULL
	 ,@pSpeed DECIMAL(4,0) = NULL
	 ,@pAltitude DECIMAL(4,0) = NULL
	 ,@pLapse INT = NULL
	 ,@pEventDateTime DATETIME	
	 ,@pEventType VARCHAR(20) = NULL					
	 ,@pCustomerId INT	 				
	 ,@pUserId INT	
	 ,@pSurveyId INT = NULL
	 ,@pQuestionsXML VARCHAR(MAX) = NULL		
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0
			   ,@lEventTypeId INT = 0
			   ,@lJourneyBegin INT = 0
			   ,@lJourneyEnd INT = 0
			   ,@lEventStopId INT = 0
			   ,@lEventLocationId INT = 0
			   ,@lParentMainEventLast INT = -1
			   ,@lGreaterAccuracy DECIMAL(4,0) = 0 --100 --------------------PARAMETERS
			   ,@lDistance DECIMAL(4,3) = 0 --0.030 --------------------PARAMETERS
               ,@lDelete BIT = 0
               ,@lStoped BIT = 0
			   ,@lUserEventId INT = 0
			    
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1 
		END

		IF ((@pCommerceId IS NULL OR @pCommerceId = 0) AND (@pTempCommerceId IS NOT NULL))
		BEGIN
			SELECT @pCommerceId = [CommerceId] 
			FROM [Management].[TemporaryRelationshipCommerce]
			WHERE [TempCommerceId] = @pTempCommerceId
		END

		-- GET THE Location Event Id
		SELECT @lEventLocationId = [Id] 
		FROM [Management].[CatalogDetail] 
		WHERE [Value] = 'AGENT_LOCATION_REAL_TIME'

		IF @pEventDetailId = 0 SET @pEventDetailId = NULL
		
		SET @lEventTypeId = (CASE WHEN @pEventType = 'location' 
								THEN (
										@lEventLocationId
									)
								WHEN @pEventType = 'pin' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'CUSTOMERS_COMMERCE_PIN'
									)
								WHEN @pEventType = 'signal-off' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_LOSES_SIGNAL'
									)
								WHEN @pEventType = 'signal-on' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_RESTORE_SIGNAL'
									)
								WHEN @pEventType = 'gps-off' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_LOSES_SIGNAL_GPS'
									)
								WHEN @pEventType = 'gps-on' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_RESTORE_SIGNAL_GPS'
									)
								ELSE (
										SELECT [EventTypeId] 
										FROM [Management].[EventDetail]
										WHERE [Id] = @pEventDetailId
									)
							END )
				
		-- GET THE GRESTER ACCURACY FOR THE GPS
		SELECT @lGreaterAccuracy = [Value]
		FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'GRESTER_ACCURACY_GPS'
			
		-- GET THE DISTANCE WITHIN A MANAGEMENT
		SELECT @lDistance = [Value]
		FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'DISTANCE_WITHIN_MANAGEMENT'

		-- GET THE JOURNEY BEGIN EVENT TYPE
		SELECT @lJourneyBegin = [Id]
		FROM [Management].[catalogDetail]
		WHERE [Value] = 'CUSTOMERS_JOURNEY_BEGIN'
			
		--GET THE JOURNEY END EVENT TYPE
		SELECT @lJourneyEnd = [Id]
		FROM [Management].[catalogDetail]
		WHERE [Value] = 'CUSTOMERS_JOURNEY_END'

		--GET THE STOP EVENT TYPE
		SELECT @lEventStopId = [Id] 
		FROM [Management].[CatalogDetail]
		WHERE [Value] = 'AGENT_STOP'

		-- IF THE CURRENT EVENT IS JOURNEY BEGIN, CLEAN THE HISTORY OF STOPS
		IF  @lEventTypeId = @lJourneyBegin 
		BEGIN
			--REMOVE HISTORY
			DELETE FROM [Management].[UserStop]
			WHERE [UserId] = @pUserId
			AND [CustomerId] = @pCustomerId
			
			--SET @lDelete = 0 
		END
		ELSE
		BEGIN	
			-- If the event is location, the Accuracy should be considered
			IF ((@lEventTypeId <> @lEventLocationId) OR (@lEventTypeId = @lEventLocationId AND @pAccuracy <= @lGreaterAccuracy))
			BEGIN
				--SET @lDelete = 0 
			
				--GET THE PARENT OF THE MAIN EVENTS (Excluding JOURNEY BEGIN EVENT)
				SELECT TOP(1) @lParentMainEventLast = cd.[Parent]
				FROM [Management].[UserEvent] ue
					INNER JOIN [Management].[CatalogDetail] cd
						ON ue.[EventTypeId] = cd.[Id]
				WHERE cd.[CatalogId] = (SELECT Id FROM [Management].[Catalog]
												WHERE [Name] = 'Tipos de Eventos Principales')
					AND [EventTypeId] <> @lJourneyBegin
					AND [UserID] = @pUserId
					AND [CustomerId] = @pCustomerId
				ORDER BY [EventDate] DESC
		
				--EXECUTE STOP 
				IF NOT EXISTS(SELECT * FROM [Management].[UserEvent] 
								WHERE [UserID] = @pUserId
									AND [CustomerId] = @pCustomerId) 
					OR
					(@pEventDateTime > (SELECT MAX([EventDate])
											FROM [Management].[UserEvent] 
											WHERE [UserID] = @pUserId
												AND [CustomerId] = @pCustomerId))
				BEGIN					
					IF ((@pLatitude IS NOT NULL AND @pLongitude IS NOT NULL) 
						AND ((EXISTS( SELECT * FROM [Management].[CatalogDetail] cd
									INNER JOIN [Management].[Catalog] c 
										ON cd.[CatalogId] = c.[Id]
								WHERE c.[Name] = 'Tipos de Eventos Secundarios'
									AND cd.[Id] = @lEventTypeId)
							AND
							(NOT EXISTS(SELECT TOP(1)* FROM [Management].[UserEvent] ue
											INNER JOIN [Management].[CatalogDetail] cd ON ue.[EventTypeId] = cd.[Id]
										WHERE cd.[CatalogId] = (SELECT Id FROM [Management].[Catalog] WHERE [Name] = 'Tipos de Eventos Principales')
											AND [EventTypeId] <> @lJourneyBegin
											AND [UserID] = @pUserId
											AND [CustomerId] = @pCustomerId
											AND cd.[Parent] = 0
										ORDER BY [EventDate] DESC))
						)
						OR 
						(@lParentMainEventLast <> 0 OR @lParentMainEventLast IS NULL))
					)
					BEGIN
						EXEC [Management].[Sp_UserStop_AddOrEdit] @lEventTypeId
																 ,@pUserId
																 ,@pCustomerId
																 ,@pLatitude
																 ,@pLongitude
																 ,@pBatteryLevel
																 ,@pHeading 
																 ,@pSpeed 
																 ,@pAltitude 
																 ,@pEventDateTime

						-- IF IS STOP
						IF (SELECT CAST(COUNT(1) AS BIT)
							FROM [Management].[UserStop]
							WHERE [UserId] = @pUserId
								AND [CustomerId] = @pCustomerId) = 1
							AND
							(SELECT CAST(COUNT(1) AS BIT)
							FROM [Management].[UserEvent]
							WHERE [UserId] = @pUserId
								AND [CustomerId] = @pCustomerId
							AND [EventTypeId] = @lEventStopId
							AND [EventDate] = (SELECT [InitStopDate]
												FROM [Management].[UserStop]
												WHERE [UserId] = @pUserId
													AND [CustomerId] = @pCustomerId)) = 1 
						BEGIN
							IF (@lEventTypeId = @lEventLocationId)
							BEGIN
								SET @lStoped = 1
							END
						END 
					END
					ELSE
					BEGIN
						--TERMINAR PARADA SI LA HAY
						IF (SELECT CAST(COUNT(1) AS BIT)
							FROM [Management].[UserEvent]
							WHERE [UserId] = @pUserId
								AND [CustomerId] = @pCustomerId
							AND [EventTypeId] = @lEventStopId
							AND [EventDate] = (SELECT [InitStopDate]
												FROM [Management].[UserStop]
												WHERE [UserId] = @pUserId
													AND [CustomerId] = @pCustomerId)) = 1 
						BEGIN
							UPDATE [Management].[UserEvent]
							SET [BatteryLevel] = @pBatteryLevel,
								--[Heading] = @pHeading,
								--[Speed] = @pSpeed,
								--[Altitude] = @pAltitude,
								--[EventDate] = @pEventDateTime
								--[Lapse] = DATEDIFF(SECOND, [EventDate], @pEventDateTime),
								[ModifyDate] = GETDATE(),
								[ModifyUserId] = [UserId]
							WHERE [UserId] = @pUserId
							AND [CustomerId] = @pCustomerId
							AND [EventTypeId] = @lEventStopId
							AND [EventDate] = (SELECT [InitStopDate]
												FROM [Management].[UserStop]
												WHERE [UserId] = @pUserId
													AND [CustomerId] = @pCustomerId)
													
							SET @lStoped = 0
						END
						--REMOVE HISTORY
						DELETE FROM [Management].[UserStop]
						WHERE [UserId] = @pUserId
						AND [CustomerId] = @pCustomerId
					END
				END -- EXECUTE STOP
			END	-- If the Accuracy should be considered or not
		END --ELSE THE CURRENT EVENT IS JOURNEY BEGIN 
		
		-- IF THE CURRENT EVENT IS JOURNEY END, CLEAN THE HISTORY OF STOPS
		IF  @lEventTypeId = @lJourneyEnd 
		BEGIN
			--TERMINAR PARADA SI LA HAY
			IF (SELECT CAST(COUNT(1) AS BIT)
				FROM [Management].[UserEvent]
				WHERE [UserId] = @pUserId
					AND [CustomerId] = @pCustomerId
				AND [EventTypeId] = @lEventStopId
				AND [EventDate] = (SELECT [InitStopDate]
									FROM [Management].[UserStop]
									WHERE [UserId] = @pUserId
										AND [CustomerId] = @pCustomerId)) = 1 
			BEGIN
				UPDATE [Management].[UserEvent]
				SET [BatteryLevel] = @pBatteryLevel,
					--[Heading] = @pHeading,
					--[Speed] = @pSpeed,
					--[Altitude] = @pAltitude,
					--[EventDate] = @pEventDateTime
					--[Lapse] = DATEDIFF(SECOND, [EventDate], @pEventDateTime),
					[ModifyDate] = GETDATE(),
					[ModifyUserId] = [UserId]
				WHERE [UserId] = @pUserId
				AND [CustomerId] = @pCustomerId
				AND [EventTypeId] = @lEventStopId
				AND [EventDate] = (SELECT [InitStopDate]
									FROM [Management].[UserStop]
									WHERE [UserId] = @pUserId
										AND [CustomerId] = @pCustomerId)
										
				SET @lStoped = 0
			END
			--REMOVE HISTORY
			DELETE FROM [Management].[UserStop]
			WHERE [UserId] = @pUserId
			AND [CustomerId] = @pCustomerId
		END

		IF (@lEventTypeId = @lEventLocationId AND @pAccuracy > @lGreaterAccuracy)
			SET @lDelete = 1

		--Validates the range around the last point 
		IF @lEventTypeId = @lEventLocationId AND @lDelete = 0
		BEGIN 
			SELECT TOP 1 *
			INTO #TEMPRECORD
			FROM [Management].[UserEvent]			
			WHERE [CustomerId] = @pCustomerId
			AND [UserId] = @pUserId	
			AND ([Delete] = 0 OR [Delete] IS NULL)		
			ORDER BY [Id] DESC

			--CHECK IF THE RECORD IS MOVEMENT 
			IF EXISTS (
						 SELECT TOP 1 *
						 FROM #TEMPRECORD
						 WHERE [EventTypeId] IN (@lEventLocationId)
					 )				
			BEGIN 
				DECLARE @lLastLat FLOAT = NULL,
						@lLastLon FLOAT = NULL,
						@lResult DECIMAL(12, 3) = NULL

				SELECT @lLastLat = [Latitude],
					   @lLastLon = [Longitude]
				FROM #TEMPRECORD
			
				DECLARE @lSource GEOGRAPHY = GEOGRAPHY::Point(@pLatitude, @pLongitude, 4326)
				DECLARE @lTarget GEOGRAPHY = GEOGRAPHY::Point(@lLastLat, @lLastLon, 4326)

				SET @lResult = @lSource.STDistance(@lTarget) / 1000

				IF @lResult < @lDistance
				BEGIN
					SET @lDelete = 1
				END 
			END

			DROP TABLE #TEMPRECORD
		END

		------ If the event is location
		----IF ((@lEventTypeId = @lEventLocationId) AND (@lStoped = 0))
		----BEGIN
		----	IF (SELECT TOP(1) EventTypeId 
		----		FROM [Management].[UserEvent]
		----		WHERE [UserId] = @pUserId
		----		ORDER BY [EventDate] DESC) = @lEventLocationId
		----		AND
		----		(SELECT TOP(1) Stoped 
		----		FROM [Management].[UserEvent]
		----		WHERE [UserId] = @pUserId
		----		ORDER BY [EventDate] DESC) = 0
		----	BEGIN
		----		SET @pLapse = (ISNULL(DATEDIFF(S, (SELECT TOP(1) [EventDate] 
		----											FROM [Management].[UserEvent]
		----											WHERE [UserId] = @pUserId
		----											ORDER BY [EventDate] DESC), @pEventDateTime), 0))
		----		IF (SELECT TOP(1) Lapse 
		----			FROM [Management].[UserEvent]
		----			WHERE [UserId] = @pUserId
		----			ORDER BY [EventDate] DESC) > 0
		----		BEGIN
		----			SET @pLapse = (SELECT TOP(1) Lapse 
		----							FROM [Management].[UserEvent]
		----							WHERE [UserId] = @pUserId
		----							ORDER BY [EventDate] DESC) + @pLapse

		----		END
		----	END
		----END

		INSERT INTO [Management].[UserEvent]
		(
			[EventDetailId]
			,[EventTypeId]
			,[UserId]
			,[CommerceId]
			,[CustomerId]
			,[Latitude]
			,[Longitude]
			,[BatteryLevel]
			,[Heading]
			,[Speed]
			,[Altitude]
			,[Lapse]
			,[Accuracy]
			,[EventDate]
			,[Delete]
			,[Stoped]
			,[InsertDate]
			,[InsertUserId]
		)
		VALUES
		(
			 @pEventDetailId
			,@lEventTypeId
			,@pUserId
			,@pCommerceId
			,@pCustomerId
			,@pLatitude
			,@pLongitude
			,@pBatteryLevel
			,@pHeading 
			,@pSpeed 
			,@pAltitude 
			,@pLapse 
			,@pAccuracy
			,@pEventDateTime
			,@lDelete
			,@lStoped
			,GETDATE()
			,@pUserId
		)

		IF (@pSurveyId IS NOT NULL)
		BEGIN
			SET @lUserEventId = CAST(scope_identity() AS int) 
			EXEC [Management].[Sp_Answer_AddOrEdit] @lUserEventId, @pSurveyId, @pEventDetailId, @pCommerceId, @pTempCommerceId, @pQuestionsXML, @pCustomerId, @pUserId
		END

        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION	
						
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF 
END
