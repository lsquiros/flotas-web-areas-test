--USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_AgendaConsolidated_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_AgendaConsolidated_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================  
-- Author:		Marjorie Garbanzo
-- Create date: 21/06/2018
-- Description: Retrieve Agenda whit UserEventId
-- ========================================================================================================  

CREATE PROCEDURE [Management].[Sp_AgendaConsolidated_Retrieve] 
( 
	@pUserId INT,
	@pDateOfAgenda DATETIME,
	@pEndDate DATETIME = NULL,
	@pCustomerId INT = NULL 
)
AS
BEGIN 
	DECLARE @UserName VARCHAR(200)

	IF @pEndDate IS NULL
		SET @pEndDate = DATEADD(DAY, 1, @pDateOfAgenda)

	SELECT @UserName = [Name]
	FROM [General].[Users]
	WHERE [UserId] = @pUserId

	SELECT @pCustomerId = ISNULL(@pCustomerId, [CustomerId])
	FROM [General].[DriversUsers] 
	WHERE [UserId] = @pUserId

	SELECT  CAST(ROW_NUMBER() OVER(ORDER BY c.[Name]) AS INT) [Order],
		    a.[Id], 
			a.[CommerceId],  
			a.[UserId], 
			a.[DateOfAgenda], 
			a.[Status],
			c.[Name] [CommerceName],
			ISNULL(c.[Code],'') [Code],
			c.[Description],
			c.[Address],
			a.[LoggedUserId],
			@UserName [UserName],
			[Management].[Fn_DynamicColumnValues_Retrieve] (a.[Id], @pCustomerId) [DynamicColumnsStr],
			(SELECT TOP(1)Id FROM [Management].[UserEvent] WHERE [UserId] = a.[UserId]
														AND [CommerceId] = a.[CommerceId]
														AND [EventDate] BETWEEN a.[DateOfAgenda] AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, a.[DateOfAgenda]), 0))
														ORDER BY [EventDate]) [UserEventId]
	FROM [Management].[Agenda] a  
	INNER JOIN [General].[Commerces] c 
		ON c.[Id] =  a.[CommerceId]
	WHERE a.[DateOfAgenda] BETWEEN @pDateOfAgenda AND @pEndDate --= @pDateOfAgenda 
	AND a.[UserId] = @pUserId
	AND a.[Delete] = 0
	AND ((SELECT TOP(1)Id FROM [Management].[UserEvent] WHERE [UserId] = a.[UserId]
														AND [CommerceId] = a.[CommerceId]
														AND [EventDate] BETWEEN a.[DateOfAgenda] AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, a.[DateOfAgenda]), 0))
														ORDER BY [EventDate]) IS NOT NULL)
	ORDER BY c.[Name]
END
 