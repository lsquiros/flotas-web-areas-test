--USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_FormsAnswers]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_FormsAnswers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author: Marjorie Garbanzo Morales
-- Create date: 18/03/2019
-- Description:	Retrieve Information of Survey and its answres
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_FormsAnswers] --25908, '2018-10-23 14:16:20.477', '2018-11-22 14:24:03.737' --25908
(
	@pUserId INT,	
	@pDate DATETIME,
	@pEndDate DATETIME = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@lImageEvent INT, 
			@lSingImage INT
			 
	IF @pEndDate IS NULL
		SET @pEndDate = DATEADD(DAY, 1, @pDate)

	SELECT @lImageEvent = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'QUESTIONS_TYPE_IMAGE'

	SELECT @lSingImage = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'QUESTIONS_TYPE_SIGN'	
	
	SELECT	s.[Id] [SurveyId],
			s.[Name] [Survey],
			q.[Text] [Question],
			CASE WHEN ua.[Data] IS NOT NULL
					  AND q.[QuestionTypeId] <> @lImageEvent
					  AND q.[QuestionTypeId] <> @lSingImage
				THEN ua.[Data]
				ELSE o.[Text]
			END [Option],
			CASE WHEN q.[QuestionTypeId] = @lImageEvent 
					OR q.[QuestionTypeId] = @lSingImage
				 THEN ua.[Data]
				 ELSE ''
			END [Data],
			q.[QuestionTypeId],
			--u.[Latitude],
			--u.[Longitude],
			us.[Name] [UserName],
			u.[UserId],
			co.[Name] [CommerceName],
			u.[EventDate],
			--[Efficiency].[Fn_GetAddressByLatLon_Retrieve] (u.[Longitude], u.[Latitude], u.[CustomerId]) [Address],
			ISNULL(ed.[Name], cd.[Name]) [EventName],
			ua.[UserEventId]
	FROM [Management].[UserAnswer] ua
	INNER JOIN [Management].[UserEvent] u
		ON ua.[UserEventId] = u.[Id]
	INNER JOIN [Management].[Survey] s
		ON ua.[SurveyId] = s.[Id]
	INNER JOIN [Management].[Question] q
		ON ua.[QuestionId] = q.[Id]	
	INNER JOIN [General].[Users] us
		ON us.[UserId] = u.[UserId]
	INNER JOIN [Management].[CatalogDetail] cd
		ON u.[EventTypeId] = cd.[Id]
	LEFT JOIN [General].[Commerces] co
		ON u.[CommerceId] = co.[Id]
	LEFT JOIN [Management].[Option] o
		ON ua.[OptionId] = o.[Id]
	LEFT JOIN [Management].[EventDetail] ed
		ON u.[EventDetailId] = ed.[Id]
	WHERE u.[UserId] = @pUserId
		AND u.[EventDate] BETWEEN @pDate AND @pEndDate	
	ORDER BY u.[Id], q.[Order]
	
	SET NOCOUNT OFF
END