--USE [ECOsystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_AgentReconstructing_Report]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_AgentReconstructing_Report] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 19/09/2018
-- Description:	Agent Reconstructing Retrieve
-- Modify by:   Marjorie Garbanzo - 08/10/2018 - Add MovementName and EventKey
-- Modify by:   Marjorie Garbanzo - 08/10/2018 - Add Stop Event in the report
-- Modify by:   Marjorie Garbanzo - 29/10/2018 - Remove column of speed
--                                             - Change the data order by eventdate when generating the ID in the temporary table
-- Modify by: Henry Retana - 06/11/2018 - New data source added
-- Modify by: Marjorie Garbanzo - 17/01/2019 - Obtain and add the necessary parameters to obtain the address of the commerces according to the nearby stores..
-- Modify by: Henry Retana - 02/01/2019 - Validates Lapse, use events through the day
-- Modify by: Marjorie Garbanzo - 21/03/2019 - Change so that the transfers do not consider the location events that are in stop.
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_AgentReconstructing_Report] 
(	
	@pUserId INT,
	@pStartDate DATETIME,
	@pEndDate DATETIME = NULL
)
AS
BEGIN	
	SET NOCOUNT ON

	DECLARE @lBeginJourneyEvent INT,
			@lStopEvent INT,
			@lLocation INT,
			@lLocationName VARCHAR(200),
			@lDeletedCommerceDesc VARCHAR(100),
			@lSecundaryEvent INT, 
			@lCountyCode VARCHAR(10),
			@lCustomerId INT

	CREATE TABLE #DATA 
	(	
		[Id] INT,
		[UserId] INT,
		[UserEventId] INT,
		[EventDetailId] INT,
		[EventDetailName] VARCHAR(200),
		[EventTypeId] INT,
		[EventName] VARCHAR(200),
		[Parent] INT,
		[CommerceId] INT,
		[Latitude] FLOAT,
		[Longitude] FLOAT,
		[BatteryLevel] INT,
		[Speed] INT,
		[EventDate] DATETIME,
		[Lapse] INT,
		[Distance] DECIMAL(16,2)
	)

	--Get Begin Journey 
	SELECT @lBeginJourneyEvent = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'

	SELECT @lDeletedCommerceDesc = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'

	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'
	
	SELECT @lStopEvent = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'AGENT_STOP'

	SELECT @lLocationName = p.[MovementName]
	FROM [Management].[Parameters] p 
	INNER JOIN [Management].[UserEvent] u
		ON p.[CustomerId] = u.[CustomerId]
	WHERE u.[UserId] = @pUserId

	--GET SECUNDARY EVENTS
	SELECT @lSecundaryEvent = [Id]
	FROM [Management].[Catalog]
	WHERE [Name] = 'Tipos de Eventos Secundarios'

	SELECT [Id]
	INTO #SECUNDARYEVENTS
	FROM [Management].[CatalogDetail]
	WHERE [CatalogId] = @lSecundaryEvent
	AND [Id] NOT IN (@lStopEvent, @lLocation)

	SELECT @lCountyCode = co.[Code]
	FROM [General].[Customers] c
		INNER JOIN [General].[DriversUsers] d
		ON c.[CustomerId] = d.[CustomerId]
		INNER JOIN [General].[Countries] co
		ON co.[CountryId] = c.[CountryId]
	WHERE d.[UserId] = @pUserId

	SELECT @lCustomerId = c.CustomerId
	FROM [General].[Customers] c
		INNER JOIN [General].[DriversUsers] d
		ON c.[CustomerId] = d.[CustomerId]
	WHERE d.[UserId] = @pUserId
	---------------------------------------

	IF @pEndDate IS NULL SET @pEndDate = DATEADD(DAY, 1, @pStartDate)

	INSERT INTO #DATA
	EXEC [Management].[Sp_ReconstructionCleanData_Retrieve] @pUserId, @pStartDate, @pEndDate
	
	;WITH FinalResult AS 
	(	
		SELECT  t.[Id],
				t.[UserId],
				t.[UserEventId],
				t.[EventTypeId],
				t.[EventDetailName],
				CASE WHEN t.[EventTypeId] = @lLocation
					 THEN ISNULL(@lLocationName, t.[EventName])
					 ELSE ISNULL(t.[EventDetailName], t.[EventName]) 
				END [EventName],
				CASE WHEN c.[IsDeleted] = 1 
					 THEN CONCAT(@lDeletedCommerceDesc, c.[Name]) 
					 ELSE c.[Name] 
				END [CommerceName],
				
				CASE WHEN t.[EventTypeId] = @lLocation
					 THEN ''
					 WHEN t.[EventTypeId] = @lStopEvent
					 THEN (
							  SELECT TOP 1 [items]
							  FROM [dbo].[Fn_Split] (ISNULL(c.[Address], [Efficiency].[Fn_GetAddressByLatLon_Retrieve](t.[Longitude], t.[Latitude], NULL)), ',')
						  )					 
					 ELSE (
							  SELECT TOP 1 [items]
							  FROM [dbo].[Fn_Split] (ISNULL(c.[Address], [Efficiency].[Fn_GetAddressByLatLon_Retrieve](t.[Longitude], t.[Latitude], NULL)), ',')
						  )					 
				END [Address],

				CASE WHEN t.[EventTypeId] = @lLocation
					 THEN ''
					 ELSE [Efficiency].[Fn_ConcatGeopoliticalInformation_Retrieve] (t.[Latitude], t.[Longitude], @lCountyCode)
				END [GeopoliticalInformation],

				t.[Parent],
			
				CASE WHEN t.[EventTypeId] = @lLocation
					 THEN ''
					 ELSE t.[Latitude] 
				END [Latitude],

				CASE WHEN t.[EventTypeId] = @lLocation
					 THEN ''
					 ELSE t.[Longitude] 
				END [Longitude],

				t.[BatteryLevel],
				t.[EventDate],
				t.[EventDate]  [StartDate],
			
				CASE WHEN t.[Parent] > 0 
					 THEN t.[EventDate]
					 ELSE (SELECT TOP 1 r.[EventDate] 
							FROM #DATA r
							WHERE r.[Id] > t.[Id]
							ORDER BY r.[Id]) 
				END  [EndDate],

			   CASE WHEN t.[EventTypeId] = @lStopEvent
					THEN [General].[Fn_GetTimeFormat](ISNULL((t.[Lapse] % (24 * 60 * 60)) / (60 * 60), '00'),
													  ISNULL(((t.[Lapse] % (24 * 60 * 60)) % (60 * 60)) / 60, '00'),
													  ISNULL(((t.[Lapse] % (24 * 60 * 60)) % (60 * 60)) % 60, '00'))
					WHEN (
							SELECT COUNT(1)
							FROM #SECUNDARYEVENTS
							WHERE [Id] = t.[EventTypeId]
						 ) = 1
					THEN '00:00:00'
					ELSE [General].[Fn_GetTimeFormat](ISNULL(DATEDIFF(S, CASE WHEN t.[Parent] > 0 
																			  THEN (
																						SELECT TOP 1 r.[EventDate] 
																						FROM #DATA r
																						WHERE r.[EventTypeId] = t.[Parent]
																						AND r.[Id] < t.[Id]
																						AND r.[EventDate] BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0) 
																										  AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0))
																						ORDER BY r.[Id] DESC
																					)
																			  ELSE t.[EventDate]
																		 END, CASE WHEN t.[Parent] > 0 
																				   THEN t.[EventDate]
																				   ELSE (
																							SELECT TOP 1 r.[EventDate] 
																							FROM #DATA r
																							WHERE r.[Id] > t.[Id]
																							AND r.[EventDate] BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0) 
																											  AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0))
																							ORDER BY r.[Id]
																						) 
																			  END) / 3600, '00'),
		  											  ISNULL (DATEDIFF(S, CASE WHEN t.[Parent] > 0 
																			   THEN (
																						SELECT TOP 1 r.[EventDate] 
																						FROM #DATA r
																						WHERE r.[EventTypeId] = t.[Parent]
																						AND r.[Id] < t.[Id]
																						AND r.[EventDate] BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0) 
																										  AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0))
																						ORDER BY r.[Id] DESC
																					)
																				ELSE t.[EventDate]
																		  END, CASE WHEN t.[Parent] > 0 
																					THEN t.[EventDate]
																					ELSE (
																							SELECT TOP 1 r.[EventDate] 
																							FROM #DATA r
																							WHERE r.[Id] > t.[Id]
																							AND r.[EventDate] BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0) 
																											  AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0))
																							ORDER BY r.[Id]
																						 ) 
																			   END) % 3600 / 60, '00'),
													  ISNULL (DATEDIFF(S, CASE WHEN t.[Parent] > 0 
																			   THEN (
																						SELECT TOP 1 r.[EventDate] 
																						FROM #DATA r
																						WHERE r.[EventTypeId] = t.[Parent]
																						AND r.[Id] < t.[Id]
																						AND r.[EventDate] BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0) 
																										  AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0))
																						ORDER BY r.[Id] DESC
																					 )
																			   ELSE t.[EventDate]
																		  END, CASE WHEN t.[Parent] > 0 
																					THEN t.[EventDate]
																					ELSE (
																							SELECT TOP 1 r.[EventDate] 
																							FROM #DATA r
																							WHERE r.[Id] > t.[Id]
																							AND r.[EventDate] BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0) 
																											  AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, t.[EventDate]), 0))
																							ORDER BY r.[Id]
																						 ) 
																				END) % 60, '00')) 
				END [Lapse],

				CASE WHEN t.[EventTypeId] = @lLocation
					 THEN [Management].[Fn_GetDistanceFromPointsByDates] (@pUserId,  t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																									 FROM #DATA r
																									 WHERE r.[Id] > t.[Id]
																									 ORDER BY r.[Id]))
					 WHEN t.[EventTypeId] = @lStopEvent
					 THEN [Management].[Fn_GetDistanceFromPointsByDates] (@pUserId,  t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																									 FROM #DATA r
																									 WHERE r.[Id] > t.[Id]
																									 ORDER BY r.[Id]))
					 WHEN (
							 SELECT COUNT(1)
							 FROM #SECUNDARYEVENTS
							 WHERE [Id] = t.[EventTypeId]
						  ) = 1
					 THEN 0															
					 ELSE 0.00 
				END [Distance],

				CASE WHEN t.[EventTypeId] = @lLocation
					 THEN (SELECT AVG(r.[Speed])
						   FROM #DATA r
						   WHERE r.[Id] > t.[Id])
					 WHEN t.[EventTypeId] = @lStopEvent
					 THEN CAST(t.[Speed] AS INT)
					 ELSE 0 
				END [Speed],
				[Management].[Fn_GetEventColor] (t.[EventTypeId], t.[EventTypeId]) [RowColor]
		FROM #DATA t
		LEFT JOIN [General].[Commerces] c
			ON t.[CommerceId] = c.[Id]
		WHERE [EventTypeId]<> @lBeginJourneyEvent 
		AND ([Parent] <> @lBeginJourneyEvent OR t.[Parent] IS NULL) 
		UNION 
		SELECT  rt.[Id],
				rt.[UserId],
				rt.[UserEventId],	
				rt.[EventTypeId],
				rt.[EventDetailName],
				ISNULL(rt.[EventDetailName], rt.[EventName]) [EventName],
				CASE WHEN cc.[IsDeleted] = 1 
					 THEN CONCAT(@lDeletedCommerceDesc, cc.[Name]) 
					 ELSE cc.[Name] 
				END [CommerceName],
				(
					SELECT TOP 1 [items]
					FROM [dbo].[Fn_Split] (ISNULL(cc.[Address], [Efficiency].[Fn_GetAddressByLatLon_Retrieve](rt.[Longitude], rt.[Latitude], NULL)), ',')
				) [Address],

				[Efficiency].[Fn_ConcatGeopoliticalInformation_Retrieve] (rt.[Latitude], rt.[Longitude], @lCountyCode) [GeopoliticalInformation],

				rt.[Parent],
				rt.[Latitude],
				rt.[Longitude],
				rt.[BatteryLevel],			
				rt.[EventDate],
				rt.[EventDate] [StartDate],
				DATEADD(SECOND, 1, rt.[EventDate]) [EndDate],
			
				CASE WHEN rt.[EventTypeId]<> @lBeginJourneyEvent
					THEN  [General].[Fn_GetTimeFormat](ISNULL (DATEDIFF(S, (SELECT TOP 1 r.[EventDate] 
																			FROM #DATA r
																			WHERE r.[EventTypeId] = @lBeginJourneyEvent
																			AND r.[EventDate] BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, rt.[EventDate]), 0) 
																							  AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, rt.[EventDate]), 0))
																			ORDER BY r.[Id]), rt.[EventDate]) / 3600, '00'),
		  											   ISNULL (DATEDIFF(S, (SELECT TOP 1 r.[EventDate] 
																			FROM #DATA r
																			WHERE r.[EventTypeId] = @lBeginJourneyEvent
																			AND r.[EventDate] BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, rt.[EventDate]), 0) 
																							  AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, rt.[EventDate]), 0))
																			ORDER BY r.[Id]), rt.[EventDate]) % 3600 / 60, '00'),
													   ISNULL (DATEDIFF(S, (SELECT TOP 1 r.[EventDate] 
																			FROM #DATA r
																			WHERE r.[EventTypeId] = @lBeginJourneyEvent
																			AND r.[EventDate] BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, rt.[EventDate]), 0) 
																							  AND DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, rt.[EventDate]), 0))
																			ORDER BY r.[Id]), rt.[EventDate]) % 60, '00')) 
					ELSE '00:00:00' 
				END [Lapse],
				0.00 [Distance],
				0 [Speed],
				NULL [RowColor]
		FROM #DATA rt
		LEFT JOIN [General].[Commerces] cc
			ON rt.[CommerceId] = cc.[Id]	
		WHERE ([EventTypeId] = @lBeginJourneyEvent OR [Parent] = @lBeginJourneyEvent) 
	)
	,LocationNoLapse AS 
	(
		SELECT *
		FROM FinalResult 
		WHERE ([EventTypeId] = @lLocation AND [Lapse] = '00:00:00')
	)
	SELECT *
	FROM FinalResult 
	WHERE [Id] NOT IN (
						 SELECT [Id]
						 FROM LocationNoLapse
					  )
	ORDER BY [EventDate]

	DROP TABLE #DATA,
			   #SECUNDARYEVENTS

	SET NOCOUNT OFF
END