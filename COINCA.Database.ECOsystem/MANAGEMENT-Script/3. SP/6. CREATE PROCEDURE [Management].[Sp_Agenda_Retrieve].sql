--USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Agenda_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Agenda_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================  
-- Author:		Henry Retana
-- Create date: 06/06/2018
-- Description: Retrieve Agenda
-- Modify by:   Marjorie Garbanzo - 18/09/2018 - Change AgentId by UserId
-- Modify by:   Henry Retana - 18/12/2018 - Set the CX Id for the dinamic columns 
-- Modify by:   Marjorie Garbanzo - 05/02/2019 - Filter the date so that it verifies all the day, thus it takes into account all the hours.
-- Modify by:   Marjorie Garbanzo - 04/04/2019 - Change @pEndDate value.
-- ========================================================================================================  

CREATE PROCEDURE [Management].[Sp_Agenda_Retrieve] 
( 
	@pUserId INT,
	@pDateOfAgenda DATETIME,
	@pEndDate DATETIME = NULL,
	@pCustomerId INT = NULL 
)
AS
BEGIN 
	DECLARE @UserName VARCHAR(200)

	IF @pEndDate IS NULL
		SET @pEndDate = DATEADD(DAY, DATEDIFF(DAY, 0, @pDateOfAgenda), 0) 

	SELECT @UserName = [Name]
	FROM [General].[Users]
	WHERE [UserId] = @pUserId

	SELECT @pCustomerId = ISNULL(@pCustomerId, [CustomerId])
	FROM [General].[DriversUsers] 
	WHERE [UserId] = @pUserId

	SELECT  CAST(ROW_NUMBER() OVER(ORDER BY c.[Name]) AS INT) [Order],
		    a.[Id], 
			a.[CommerceId],  
			a.[UserId], 
			a.[DateOfAgenda], 
			a.[Status],
			c.[Name] [CommerceName],
			ISNULL(c.[Code],'') [Code],
			c.[Description],
			c.[Address],
			a.[LoggedUserId],
			@UserName [UserName],
			[Management].[Fn_DynamicColumnValues_Retrieve] (a.[Id], @pCustomerId) [DynamicColumnsStr]
	FROM [Management].[Agenda] a  
	INNER JOIN [General].[Commerces] c 
		ON c.[Id] =  a.[CommerceId]
	WHERE a.[DateOfAgenda] BETWEEN @pDateOfAgenda AND @pEndDate --= @pDateOfAgenda 
	AND a.[UserId] = @pUserId
	AND a.[Delete] = 0
	ORDER BY c.[Name]
END
 