USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionRuleBinnacle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionRuleBinnacle_Retrieve]
GO

-- ================================================================================================
-- Author:		María de los Ángeles Jiménez Chavarría
-- Create date: FEB/20/2019
-- Description:	Retrieve the transactions rules binnacle - The last 5 changes (omit repeated status)

--***************************************************************************************************
-- Modify by:	Stefano Quiros
-- Create date: ABR/2/2019
-- Description:	Change the Logic of the retrieve
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionRuleBinnacle_Retrieve] --30, 2
(
	@pCustomerId INT,
	@pRuleId INT
)
AS
BEGIN		
	SET NOCOUNT ON;
	
		CREATE TABLE #Temp
		(
			[Id] INT IDENTITY
		   ,[UserId] INT
		   ,[Active] VARCHAR(20)
		   ,[LogDateTime] DATETIME
		   ,[Name] VARCHAR(500)
		   ,[RowNumber] INT
		)

	    CREATE TABLE #Result
		(
			[Id] INT
		   ,[UserId] INT
		   ,[Active] VARCHAR(20)
		   ,[LogDateTime] DATETIME
		   ,[EncryptedName] VARCHAR(500)
		)

	    INSERT INTO #Temp
		SELECT
		     e.[UserId]
			,CASE WHEN JSON_VALUE(REPLACE(REPLACE(e.[Mensaje], 'Modificaciones en las reglas de negocio: ', ''), '[DataChanges]', ''),'$.RuleActive') = 'true'
			 THEN 'Activada'
			 ELSE 'Desactivada'
			 END AS [Active]
			,e.[LogDateTime]
			,u.[Name]
			,ROW_NUMBER()
			OVER (ORDER BY [LogDateTime] DESC)		
		FROM [General].[EventLog] e
		INNER JOIN [General].[Users] u
		ON  e.[CustomerId] = @pCustomerId AND
			e.[Controller] = 'TransactionRules' AND
			e.[Action] = 'AddOrEditTransactionRules' AND
			e.[IsError] = 0 AND
			u.[UserId] = e.[UserId]
		WHERE JSON_VALUE(REPLACE(REPLACE(e.[Mensaje], 'Modificaciones en las reglas de negocio: ', ''), '[DataChanges]', ''),'$.RuleId') = @pRuleId
		ORDER BY [LogDateTime]
	

	INSERT INTO #Result
	SELECT t2.[Id]
		  ,t2.[UserId]
		  ,t2.[Active]
		  ,t2.[LogDateTime]
		  ,t2.[Name]
    FROM #Temp t1, #Temp t2
	WHERE t1.[RowNumber] = t2.[RowNumber] + 1
	AND (t1.[Active] <> t2.[Active])
	UNION
	SELECT TOP 1 [Id]
				,[UserId]
				,[Active]
				,[LogDateTime]
				,[Name] [EncryptedName] 
	FROM #Temp 
	ORDER BY [Id]

	SELECT TOP 5 [UserId]
				,[Active]
				,[LogDateTime]
				,[EncryptedName]
	FROM #Result 
	ORDER BY [LogDateTime] DESC

	DROP TABLE #Result

    SET NOCOUNT OFF
END
