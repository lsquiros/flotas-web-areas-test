USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerSurvey_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PartnerSurvey_AddOrEdit]
GO

-- ================================================================================================
-- Author:		Maria de los Angeles Jimenez 
-- Create date: JUL/02/2019
-- Description:	Add or Edit the a partner survey.
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PartnerSurvey_AddOrEdit]
(
	 @pId INT = NULL
	,@pPartnerId INT = NULL
	,@pName VARCHAR(300) = NULL
	,@pTargetId INT = NULL
	,@pDescription VARCHAR(500) = NULL
	,@pMandatory BIT = NULL
	,@pActive BIT = NULL
	,@pDelete BIT
	,@pQuestionsXML VARCHAR(MAX) = NULL
	,@pUserId INT	
)
AS BEGIN	
	BEGIN TRY
		BEGIN TRANSACTION
		DECLARE @tSurveyQuestions TABLE 
		(
			 [Id] INT
			,[QuestionTypeId] INT
			,[Text] VARCHAR(600)
			,[Order] INT
			,[Delete] BIT
		)

		DECLARE @tSurveyAnswer TABLE 
		(
			[Id] INT,
			[PartnerSurveyQuestionId] INT,
			[Text] VARCHAR(600),
			[Order] INT,
			[NextSurveyQuestionId] INT,
			[Delete] BIT
		)
		
		DECLARE @tTempQuestion TABLE 
		(
			[Id] INT 
		)

		DECLARE @lQuestionsXML XML = CONVERT(XML, @pQuestionsXML)

		INSERT INTO @tSurveyQuestions 
		(
			[Id], 
			[QuestionTypeId], 
			[Text], 
			[Order],
			[Delete]
		)
		SELECT	m.c.value('Id[1]', 'int'),
				m.c.value('QuestionTypeId[1]', 'int'),
				m.c.value('Text[1]', 'VARCHAR(600)'),
				m.c.value('Order[1]', 'int'),
				m.c.value('Delete[1]', 'bit')
		FROM @lQuestionsXML.nodes('//PartnerSurveyQuestion') AS m(c)
		WHERE m.c.value('Text[1]', 'VARCHAR(600)') IS NOT NULL

		--INSERT OPERATION
		IF @pId IS NULL OR @pId = 0
		BEGIN	
			INSERT INTO [General].[PartnerSurvey] 
			(
				 [PartnerId]
				,[Name]
				,[Description]
				,[TargetId]
				,[Active]
				,[Mandatory]
				,[Delete]
				,[InsertUserId]
				,[InsertDate]
			) 
			VALUES 
			(
				@pPartnerId,
				@pName,
				@pDescription,
				@pTargetId,
				@pActive,
				@pMandatory,
				0,
				@pUserId,
				GETDATE()
			)
			SET @pId = SCOPE_IDENTITY()

			--INSERT QUESTIONS
			INSERT INTO [General].[PartnerSurveyQuestion] 
			(
				 [PartnerSurveyId]
				,[QuestionTypeId]
				,[Text]
				,[Order]
				,[Delete]
				,[InsertUserId]
				,[InsertDate]
			)
			SELECT	@pId, 
					[QuestionTypeId],
					[Text],
					[Order],
					0,
					@pUserId,
					GETDATE()
			FROM @tSurveyQuestions
		
			INSERT INTO @tSurveyAnswer 
			(
				[Id], 
				[PartnerSurveyQuestionId], 
				[Text], 
				[Order], 
				[NextSurveyQuestionId], 
				[Delete]
			)
			SELECT	m.c.value('Id[1]', 'int'),
					g.[Id],
					m.c.value('Text[1]', 'VARCHAR(600)'),
					m.c.value('Order[1]', 'int'),
					m.c.value('NextSurveyQuestionId[1]', 'int'),
					m.c.value('Delete[1]', 'bit')
			FROM @lQuestionsXML.nodes('//PartnerSurveyQuestion/Answers/PartnerSurveyAnswer') AS m(c)
			INNER JOIN [General].[PartnerSurveyQuestion] g
				ON g.[Text] = m.c.value('../../Text[1]', 'VARCHAR(600)')
			WHERE g.[PartnerSurveyId] = @pId

			--INSERT ANSWERS
			INSERT INTO [General].[PartnerSurveyAnswer] 
			(
				[PartnerSurveyQuestionId]
			   ,[Text]
			   ,[Order]
			   ,[NextSurveyQuestionId]
			   ,[Delete]
			   ,[InsertUserId]
			   ,[InsertDate]
			)
			SELECT	[PartnerSurveyQuestionId],
					[Text],
					[Order],
					[NextSurveyQuestionId],
					0,
					@pUserId,
					GETDATE()
			FROM @tSurveyAnswer
		END
		ELSE
		BEGIN
			--DELETE OPERATION
			IF @pDelete = 1
			BEGIN
				UPDATE [General].[PartnerSurvey]
				SET	[Delete] = 1,
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				WHERE [Id] = @pId

				UPDATE [General].[PartnerSurveyQuestion]
				SET [Delete] = 1,
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				WHERE [PartnerSurveyId] = @pId

				UPDATE [General].[PartnerSurveyAnswer] 
				SET [Delete] = 1,
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				WHERE [PartnerSurveyQuestionId] IN (SELECT [Id] 
													FROM [General].[PartnerSurveyQuestion] 
													WHERE [PartnerSurveyId] = @pId)
			END
			ELSE
			BEGIN
				UPDATE [General].[PartnerSurvey]
				SET [Name] = @pName,
					[TargetId] = @pTargetId,
					[Description] = @pDescription,
					[Mandatory] = @pMandatory,
					[Active] = @pActive,
					[Delete] = 0,
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				WHERE [Id] = @pId

				--QUESTIONS UPDATE START
				--DELETE CHANGED QUESTION
				UPDATE [General].[PartnerSurveyQuestion] 
				SET [Delete] = 1,
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				WHERE [PartnerSurveyId] = @pId 
				AND [Text] IN 
				(
					SELECT [Text] 
					FROM (
							SELECT	[Text], 
									[Order],
									[QuestionTypeId]
							FROM [General].[PartnerSurveyQuestion]
							WHERE [PartnerSurveyId] = @pId 
							AND [Delete] = 0
							EXCEPT
							SELECT [Text], 
								   [Order],
								   [QuestionTypeId]
							FROM @tSurveyQuestions 
							WHERE [Delete] = 0
					) AS q
				)

				--ADD NEW QUESTIONS
				INSERT INTO [General].[PartnerSurveyQuestion] 
				(
				  	[PartnerSurveyId],
					[QuestionTypeId],
					[Text],
					[Order],					
					[Delete],
					[InsertUserId],
					[InsertDate]
				)
				SELECT	@pId, 
						[QuestionTypeId],
						[Text],
						[Order],
						[Delete],
						@pUserId,
						GETDATE()
				FROM @tSurveyQuestions
				WHERE [Text] IN 
				(
					SELECT [Text] 
					FROM 
					(
						SELECT	[Text], 
								[Order] 
						FROM @tSurveyQuestions 
						WHERE [Delete] = 0
						EXCEPT
						SELECT	[Text],
								[Order] 
						FROM [General].[PartnerSurveyQuestion]
						WHERE [PartnerSurveyId] = @pId 
						AND [Delete] = 0
					) AS q
				)
				--QUESTIONS UPDATE END

				INSERT INTO @tTempQuestion 
				(
					[Id]
				)
				SELECT [Id] 
				FROM [General].[PartnerSurveyQuestion] 
				WHERE [PartnerSurveyId] = @pId

				INSERT INTO @tSurveyAnswer
				(
					[Id], 
					[PartnerSurveyQuestionId], 
					[Text], 
					[Order], 
					[NextSurveyQuestionId], 
					[Delete]
				)
				SELECT   m.c.value('Id[1]', 'int')
						,g.[Id]
						,m.c.value('Text[1]', 'VARCHAR(600)')
						,m.c.value('Order[1]', 'int')
						,m.c.value('NextSurveyQuestionId[1]', 'int')
						,m.c.value('Delete[1]', 'bit')
				FROM @lQuestionsXML.nodes('//PartnerSurveyQuestion/Answers/PartnerSurveyAnswer') AS m(c)
				INNER JOIN [General].[PartnerSurveyQuestion] g
					ON g.[Text] = m.c.value('../../Text[1]', 'VARCHAR(600)')
				WHERE g.[PartnerSurveyId] = @pId

				--ANSWERS UPDATE START
				--DELETE CHANGED ANSWERS
				UPDATE [General].[PartnerSurveyAnswer] 
				SET	[Delete] = 1,
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				WHERE [Text] IN 
				(
					SELECT [Text] 
					FROM 
					(
						SELECT	[PartnerSurveyQuestionId], 
								[Text], 
								[Order]
						FROM [General].[PartnerSurveyAnswer]
						WHERE [PartnerSurveyQuestionId] 
						IN (SELECT [Id] 
							FROM @tTempQuestion)
						AND [Delete] = 0
						EXCEPT
						SELECT [PartnerSurveyQuestionId], 
							   [Text], 
							   [Order] 
						FROM @tSurveyAnswer 
						WHERE [Delete] = 0
					) AS q
				) 
				AND [PartnerSurveyQuestionId] IN (SELECT [Id] FROM @tTempQuestion)

				--ADD NEW ANSWERS
				INSERT INTO [General].[PartnerSurveyAnswer] 
				(
					[PartnerSurveyQuestionId]
				   ,[Text]
				   ,[Order]
				   ,[NextSurveyQuestionId]
				   ,[Delete]
				   ,[InsertUserId]
				   ,[InsertDate]
				)
				SELECT	[PartnerSurveyQuestionId],
						[Text],
						[Order],
						[NextSurveyQuestionId],
						0,
						@pUserId,
						GETDATE()
				FROM @tSurveyAnswer
				WHERE [Text] IN 
				(
					SELECT [Text] 
					FROM 
					(
						SELECT [PartnerSurveyQuestionId], 
							   [Text], 
							   [Order]
						FROM @tSurveyAnswer 
						WHERE [Delete] = 0
						EXCEPT
						SELECT	[PartnerSurveyQuestionId], 
								[Text], 
								[Order]
						FROM [General].[PartnerSurveyAnswer]
						WHERE [PartnerSurveyQuestionId] IN (SELECT [Id] FROM @tTempQuestion)
						AND [Delete] = 0
					) AS q
				)
				--ANSWERS UPDATE END
			END

		END

		--INACTIVE OTHER SURVEYS
		IF @pActive = 1
		BEGIN 
			UPDATE [General].[PartnerSurvey]
			SET [Active] = 0,
				[ModifyUserId] = @pUserId,
				[ModifyDate] = GETDATE()
			WHERE [PartnerId] = @pPartnerId
			AND [TargetId] = @pTargetId
			AND [Active] = @pActive 
			AND [Id] <> @pId 
			AND ([Delete] IS NULL OR [Delete] = 0)
		END

		COMMIT TRANSACTION
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION		
		DECLARE	@lErrorMessage NVARCHAR(4000),
				@lErrorSeverity INT,
				@lErrorState INT

		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
END