/****** Object:  StoredProcedure [Control].[Sp_FuelsByCredit_Retrieve]    Script Date: 7/12/2019 6:56:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:	Berman Romero L.
-- Create date: 10/03/2014
-- Description:	Retrieve Customer Credit information
-- Modify by: Gerald Solano
-- Create date: 20/06/2016
-- Description:	Mejora de Redimiento
-- Modify By: Sinndy Vargas  - 10/08/2016
-- Summary: Se modifica la cantidad de decimales obtenidos a 3 en el campo: [Liters], "CONVERT(DECIMAL(16,3)"
-- Stefano Quirós - Add the GetDate Function to the FuelByCredit select
-- Henry Retana - 8/6/2017
-- Modify the retrieve to avoid divized by 0 
--+++++++ MISSING LAST MODIFICATION+++++++++++++
-- Henry Retana - 28/11/2017
-- Validates dates in the select fuels
-- Gerald Solano - 23/04/2017
-- Se valida el combustible asignado 
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_FuelsByCredit_Retrieve] --8352
(
	  @pCustomerId INT,
	  @pYear INT = NULL,
	  @pMonth INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	--DECLARE @pCustomerId INT = 7867
	--DECLARE @pYear INT = NULL
	--DECLARE @pMonth INT = NULL

	DECLARE @lExchangeRate DECIMAL(16,8) = 1.0
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	DECLARE @lFuelsCount INT 
	DECLARE @lTimeZoneParameter INT 
	DECLARE @lInsertDate DATE
	DECLARE @tblAssignedAmountByFuel TABLE(
		FuelId INT,
		Assigned DECIMAL(16,3)
	)

	DECLARE @lConvertionValue DECIMAL(16, 6) = 3.78541
	DECLARE @lCapacityUnit INT 

	SELECT @lCapacityUnit = [CapacityUnitId] 
	FROM [General].[CustomersByPartner] pc
	INNER JOIN [General].[Partners] p
		ON p.[PartnerId] = pc.[PartnerId]
	WHERE [CustomerId] = @pCustomerId

	-- Se obtiene el ultimo mes en curso de distribucion
	SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)

	SELECT @lTimeZoneParameter = [TimeZone]	
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	SELECT @lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
	INNER JOIN [General].[CustomersByPartner] b
		ON a.[PartnerId] = b.[PartnerId]
	INNER JOIN [General].[Customers] c
		ON c.[CustomerId] = b.[CustomerId] AND c.[CurrencyId] = a.[CurrencyId]
	WHERE c.[CustomerId] = @pCustomerId
	AND a.EndDate IS NULL
	
	SELECT @lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
	INNER JOIN [General].[Customers] b
		ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId

	--Obtenemos la sumatoria de la distribución del mes por combustible	
	INSERT INTO @tblAssignedAmountByFuel (FuelId, Assigned)
	SELECT s.[FuelId], SUM(ISNULL(s.[Amount], 0)) Assigned 
	FROM (SELECT DISTINCT 
					b.VehicleId
					,d.[FuelId]
					,ISNULL(a.[Amount], 0) AS [Amount]				
			FROM [Control].[FuelDistribution] a
			INNER JOIN [General].[Vehicles] b
				ON a.[VehicleId] = b.[VehicleId]			
			INNER JOIN [General].[VehicleCategories] c
				ON b.[VehicleCategoryId] = c.[VehicleCategoryId]	
			INNER JOIN [Control].[CreditCardByVehicle] cv
				ON cv.[VehicleId] = b.[VehicleId]
			INNER JOIN [Control].[CreditCard] ccc
				ON cv.[CreditCardId] = ccc.[CreditCardId]
			INNER JOIN [Control].[Fuels] d
				ON c.[DefaultFuelId] = d.[FuelId]
			INNER JOIN [General].[Customers] e
				ON b.[CustomerId] = e.[CustomerId]
			INNER JOIN [Control].[Currencies] f
				ON e.[CurrencyId] = f.[CurrencyId]	
			WHERE (b.[IsDeleted] IS NULL OR b.[IsDeleted] = 0)		
					AND (@pCustomerId IS NULL OR b.[CustomerId] = @pCustomerId)
					AND CONVERT(DATE, a.[InsertDate]) >= @lInsertDate 
					AND ccc.[StatusId] IN (7, 8)
	) s
	GROUP BY s.[FuelId]

	--Obtenemos la distribucion asignada y disponible para cada combustible
	SELECT 
		 MAX(a.[FuelByCreditId]) [FuelByCreditId]
		,b.[FuelId]
		,ISNULL(a.[Year], DATEPART(YEAR, @lInsertDate)) [Year]
		,ISNULL(a.[Month], DATEPART(MONTH, @lInsertDate)) [Month]
		,MAX(a.[Total]) [Total]
		,ISNULL(af.[Assigned], 0) AS [Assigned]
		,(MAX(a.[Total]) - ISNULL(af.[Assigned], 0)) AS Available
		,b.[Name] AS [FuelName]
		,CASE WHEN t.LiterPrice = 0 
			THEN NULL 
		 ELSE CONVERT(DECIMAL(16,3),MAX(a.[Total])/(t.LiterPrice * @lExchangeRate)) 
		 END AS Liters

		,CASE WHEN t.LiterPrice = 0 
			THEN NULL
		 ELSE CONVERT(DECIMAL(16,2),(t.LiterPrice * @lExchangeRate)) 
		 END AS LiterPrice	

		,@lCurrencySymbol AS [CurrencySymbol]
		,MAX(a.[RowVersion]) [RowVersion]
    FROM [Control].[Fuels] b
	LEFT JOIN [Control].[FuelsByCredit] a
		ON a.[FuelId] = b.[FuelId]
		AND a.[InsertDate] >= @lInsertDate
		AND a.[CustomerId] = @pCustomerId
	LEFT JOIN @tblAssignedAmountByFuel af 
		ON af.FuelId = b.FuelId
	LEFT JOIN (
				SELECT x.[FuelId], x.[LiterPrice] 
				FROM [Control].[PartnerFuel] x
				INNER JOIN [General].[CustomersByPartner] y
					ON x.[PartnerId] = y.[PartnerId]
				INNER JOIN [General].[Customers] z
					ON y.[CustomerId] = z.[CustomerId]
					AND z.CustomerId = y.CustomerId
				WHERE z.CustomerId = @pCustomerId
				AND y.IsDefault = 1
				AND x.[EndDate] > DATEADD(HOUR, @lTimeZoneParameter, GETDATE())
				AND x.[ScheduledId] IS NULL
			) t
		ON b.[FuelId] = t.FuelId
	WHERE  t.[LiterPrice] IS NOT NULL		
	GROUP BY b.[FuelId], a.[Year], a.[Month], af.[Assigned], b.[Name], t.LiterPrice 
	ORDER BY b.[FuelId]
	
    SET NOCOUNT OFF
END
GO
