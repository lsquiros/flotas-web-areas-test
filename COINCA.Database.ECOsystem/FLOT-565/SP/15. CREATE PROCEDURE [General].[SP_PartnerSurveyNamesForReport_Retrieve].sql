USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_PartnerSurveyNamesForReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_PartnerSurveyNamesForReport_Retrieve]
GO

-- ================================================================================================
-- Author:		Henry Retana 
-- Create date: 23/03/2019
-- Description:	Retrieve the partner survey for report
-- =======================================================================================================================================

CREATE PROCEDURE [General].[SP_PartnerSurveyNamesForReport_Retrieve]
(
	@pPartnerId INT
)
AS
BEGIN
	SET NOCOUNT ON

	SELECT s.[Id],
		   s.[Name]	
	FROM [General].[PartnerSurvey] s
	WHERE s.[PartnerId] =	@pPartnerId				
	
	SET NOCOUNT OFF
END

