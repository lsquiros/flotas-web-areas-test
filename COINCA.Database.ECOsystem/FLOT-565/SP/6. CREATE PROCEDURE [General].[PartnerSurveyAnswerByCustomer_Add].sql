USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PartnerSurveyAnswerByCustomer_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PartnerSurveyAnswerByCustomer_Add]
GO

-- ================================================================================================
-- Author:		María de los Ángeles Jiménez 
-- Create date: JUL/03/2019
-- Description:	Add or edit a customer's answer for a survey.
-- Modify by Henry Retana 
-- Date: 23/07/2019 - Add Survey By Customer
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_PartnerSurveyAnswerByCustomer_Add]
(
	@pSurveyId INT, 
	@pElements INT,
	@pAnswersXML VARCHAR(MAX) = NULL,
	@pCustomerId INT,
	@pUserId INT
)
AS BEGIN
	BEGIN TRY
		BEGIN TRANSACTION	
		
		DECLARE @lSurveyAnswerId INT
		DECLARE @lAnswersXML XML = CONVERT(XML, @pAnswersXML)

		INSERT INTO [General].[PartnerSurveyByCustomer]
		(
			[SurveyId],
			[Elements],
			[CustomerId],
			[InsertUserId],
			[InsertDate]
		)
		VALUES 
		(
			@pSurveyId, 
			@pElements,			
			@pCustomerId,
			@pUserId,
			GETDATE()
		)

		SET @lSurveyAnswerId = @@IDENTITY		

		INSERT INTO [General].[PartnerSurveyAnswerByCustomer] 
		(
			[SurveyByCustomerId],
			[PartnerSurveyQuestionId],
			[PartnerSurveyAnswerId],
			[CustomerId],
			[Text],
			[InsertUserId],
			[InsertDate]
		)
		SELECT	@lSurveyAnswerId,
				m.c.value('PartnerSurveyQuestionId[1]', 'int'),
				CASE WHEN m.c.value('Id[1]', 'int') = 0 
					 THEN NULL 
					 ELSE m.c.value('Id[1]', 'int') 
				END,
				@pCustomerId,
				m.c.value('AnswerText[1]', 'VARCHAR(1200)'),
				@pUserId,
				GETDATE()
		FROM @lAnswersXML.nodes('/PartnerSurveyAnswer/PartnerSurveyAnswer') AS m(c)

		COMMIT TRANSACTION
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE	@lErrorMessage NVARCHAR(4000),
				@lErrorSeverity INT,
				@lErrorState INT

		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
END