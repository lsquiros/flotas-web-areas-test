USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_CustomersByPartnerSurvey_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_CustomersByPartnerSurvey_Retrieve]
GO

-- ================================================================================================
-- Author:		Henry Retana 
-- Create date: 23/07/2019
-- Description:	Retrieve the customers linked to Survey
-- =======================================================================================================================================

CREATE PROCEDURE [General].[SP_CustomersByPartnerSurvey_Retrieve]
(
	@pPartnerId INT
)
AS
BEGIN
	SET NOCOUNT ON	

	SELECT DISTINCT	c.[CustomerId],
					c.[Name] [EncryptedName]
	FROM [General].[PartnerSurveyByCustomer] sa	
	INNER JOIN [General].[Customers] c
		ON c.[CustomerId] = sa.[CustomerId]
	INNER JOIN 
	(
		SELECT [CustomerId]
		FROM [General].[CustomersByPartner]
		WHERE [PartnerId] =	@pPartnerId				
	) cp
		ON cp.[CustomerId] = sa.[CustomerId]

	SET NOCOUNT OFF
END

