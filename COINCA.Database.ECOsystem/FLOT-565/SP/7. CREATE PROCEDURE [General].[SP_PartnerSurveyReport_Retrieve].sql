USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_PartnerSurveyReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_PartnerSurveyReport_Retrieve]
GO

-- ================================================================================================
-- Author:		Maria de los Angeles Jimenez 
-- Create date: JUL/09/2019
-- Description:	Retrieve the partner survey report
-- =======================================================================================================================================

CREATE PROCEDURE [General].[SP_PartnerSurveyReport_Retrieve]
(
	@pPartnerId INT,
	@pSurveyId  INT = NULL,
	@pCustomerId INT = NULL,
	@pStartDate DATE,
	@pEndDate DATE
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @lTimeZone INT

	SELECT @lTimeZone = [TimeZone]
	FROM [General].[Countries] co
	INNER JOIN [General].[Partners] p 
		ON co.[CountryId] = p.[CountryId]
	WHERE p.[PartnerId] =	@pPartnerId

	SELECT	sa.[Id],
			s.[Name],
			CASE WHEN st.[Description] = 'Eliminar Tarjeta de Crédito'
				 THEN 'Número de Tarjetas Eliminadas'
				 ELSE st.[Description]
			END [Target],
			sa.[Elements],
			c.[Name] [CustomerNameEncrypt],	    
			DATEADD(HOUR, @lTimeZone, sa.[InsertDate]) [Date],
			CAST((
					SELECT	sc.[SurveyByCustomerId],
							sq.[Text] [Question],
							sq.[Order] [QuestionOrder],
							ISNULL(a.[Text], sc.[Text]) [Answer],			
							u.[Name] [UserNameEncrypt]			
					FROM [General].[PartnerSurveyQuestion] sq
					INNER JOIN [General].[PartnerSurveyAnswerByCustomer] sc
						ON sc.[PartnerSurveyQuestionId] = sq.[Id]
					LEFT JOIN [General].[PartnerSurveyAnswer] a
						ON sc.[PartnerSurveyAnswerId] = a.[Id]
					LEFT JOIN [General].[Users] u
						ON u.[UserId] = sc.[InsertUserId]
					WHERE sq.[PartnerSurveyId] = sa.[SurveyId]
					AND sc.[SurveyByCustomerId] = sa.[Id]
					ORDER BY sq.[Order]
					FOR XML PATH('PartnerSurveyReportDetail'), 
					ROOT('PartnerSurveyReportDetail'), ELEMENTS XSINIL
			) AS VARCHAR(MAX)) [ReportDetailStr]
	FROM [General].[PartnerSurveyByCustomer] sa
	INNER JOIN [General].[PartnerSurvey] s
		ON sa.[SurveyId] = s.[Id]
	INNER JOIN [General].[Customers]  c
		ON sa.[CustomerId] = c.[CustomerId]
	INNER JOIN [General].[PartnerSurveyTargets] st
		ON s.[TargetId] = st.[Id]	
	INNER JOIN 
	(
		SELECT [CustomerId]
		FROM [General].[CustomersByPartner]
		WHERE [PartnerId] =	@pPartnerId				
	) cp
		ON cp.[CustomerId] = c.[CustomerId]
	WHERE (@pSurveyId IS NULL OR s.[Id] = @pSurveyId)
	AND (@pCustomerId IS NULL OR cp.[CustomerId] = @pCustomerId)
	AND DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, @lTimeZone, sa.[InsertDate])), 0) BETWEEN @pStartDate AND @pEndDate
	
	SET NOCOUNT OFF
END

