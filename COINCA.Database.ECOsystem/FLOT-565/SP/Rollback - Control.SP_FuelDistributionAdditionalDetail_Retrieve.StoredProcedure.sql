/****** Object:  StoredProcedure [Control].[SP_FuelDistributionAdditionalDetail_Retrieve]    Script Date: 7/12/2019 6:56:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==========================================================================================
-- Author:		Henry Retana
-- Create date: 08/03/2018
-- Description:	Obtiene la información detallada para la distribucion adicional  
-- Modify: Henry Retana - 17/04/2018
-- Validates IssuedForId
-- Modify By:	Gerald Solano - 
-- Modify Date: 08/11/2018 
-- Description: Validate Customer User when the user not exist in General.VehicleByUser table
-- Stefano Quirós - Add the AvailableLiters to the retrieve - 15/01/2019 - Modify the 
-- InsertDate that respect the Datetime value and not only the Date
-- ===========================================================================================

CREATE PROCEDURE [Control].[SP_FuelDistributionAdditionalDetail_Retrieve]
(		
	@pVehicleId INT = NULL,
	@pUserId INT = NULL,	
	@pCustomerId INT = NULL,	
	@pPlateId VARCHAR(50) = NULL,
	@pVehicleGroupId INT = NULL,
	@pCostCenterId INT = NULL,
	@pLoggedUserId INT = NULL,
	@pIssueForId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON	
	
	DECLARE	 @Results TABLE (items INT)
	DECLARE  @count INT
			,@lInsertDate DATETIME = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)
			,@lId INT
			,@lTimeZoneParameter INT
	
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pLoggedUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) 
					  FROM	@Results)
	
	SELECT @lTimeZoneParameter = [TimeZone],
		   @pIssueForId = ISNULL(@pIssueForId, cu.[IssueForId])
	FROM [General].[Countries] c
	INNER JOIN [General].[Customers] cu
		ON c.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId
	
	IF @pCostCenterId = 0 SET @pCostCenterId = NULL

	CREATE TABLE #Ids
	(
		[Id] INT,
		[FuelId] INT
	)	

	CREATE TABLE #Results 
	(	
		 [Id] INT IDENTITY
		,[VehicleId] INT
		,[DriverId] INT
		,[Available] DECIMAL(16, 2)
		,[Assigned] DECIMAL(16, 2)
		,[AssignedLiters] DECIMAL(16, 3)
		,[RealAmount] DECIMAL(16, 2)
		,[RealLiters] DECIMAL(16, 3)
		,[TotalAssigned] DECIMAL(16, 2)
		,[Total] DECIMAL(16, 2)
		,[CreditCardId] INT
		,[AvailableLiters] DECIMAL(16, 3)
	)

	SELECT @pUserId = CASE WHEN COUNT(1) = 0
                         THEN NULL
                         ELSE @pUserId
                      END
    FROM [General].[DriversUsers] d
    LEFT JOIN [General].[VehiclesByUser] v
        ON v.[UserId] = d.[UserId]
    WHERE (v.[UserId] IS NOT NULL AND v.[UserId]= @pUserId)
    OR (d.[UserId] IS NOT NULL AND d.[UserId]= @pUserId)

	IF @pIssueForId = 101
	BEGIN 
		INSERT INTO #Ids
		SELECT DISTINCT TOP 201	a.[VehicleId]
						       ,d.[FuelId]								   
		FROM [Control].[FuelDistribution] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]	
		LEFT JOIN [General].[VehiclesByGroup] vg    
			ON b.[VehicleId] = vg.[VehicleId]  
		INNER JOIN [General].[VehicleCategories] c
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [General].[VehicleCostCenters] cc 
			ON b.[CostCenterId] = cc.[CostCenterId]		
		INNER JOIN [General].[Customers] e
			ON b.[CustomerId] = e.[CustomerId]
		INNER JOIN [Control].[Fuels] d
			ON c.[DefaultFuelId] = d.[FuelId]			  
		WHERE (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
		AND (b.[IsDeleted] IS NULL OR b.[IsDeleted] = 0)		
		AND (@pPlateId IS NULL OR b.[PlateId] = @pPlateId)		  
		AND (@pCustomerId IS NULL OR b.[CustomerId] = @pCustomerId)
		AND (@pCostCenterId IS NULL OR cc.[CostCenterId] = @pCostCenterId)
		AND (@pVehicleGroupId IS NULL OR vg.[VehicleGroupId] = @pVehicleGroupId)
		AND (@count = 0 OR a.[VehicleId] IN (SELECT [items] 
											 FROM @Results))
		AND a.[InsertDate] >= @lInsertDate 
  	END 
	ELSE 
	BEGIN 
		INSERT INTO #Ids
		SELECT DISTINCT TOP 201	a.[DriverId]
								,NULL [FuelId]
		FROM [Control].[FuelDistribution] a
		INNER JOIN [General].[Users] u      
			ON u.[UserId] = a.[DriverId]        
		INNER JOIN [General].[DriversUsers] du      
			ON du.[UserId] = a.[DriverId]		
		WHERE (@pUserId IS NULL OR a.[DriverId] = @pUserId)
		AND (@pCustomerId IS NULL OR du.[CustomerId] = @pCustomerId)
		AND (@pCostCenterId IS NULL OR du.[CostCenterId] = @pCostCenterId)            
		AND a.[InsertDate] >= @lInsertDate
	END 

	SELECT @lId = MIN([Id]) 
	FROM #Ids

	WHILE @lId IS NOT NULL
	BEGIN		
		DECLARE @lFUelId INT = (SELECT [FuelId]
								FROM #Ids
								WHERE [Id] = @lId)
		IF @pIssueForId = 101
		BEGIN			
			INSERT INTO #Results
			(
				 [Available]
				,[Assigned]
				,[AssignedLiters]
				,[RealAmount]
				,[RealLiters]
				,[TotalAssigned]
				,[Total]
				,[CreditCardId]
				,[AvailableLiters]
			)
			EXEC [Control].[Sp_GetDistributionData] @pVehicleId = @lId
												   ,@pCustomerId = @pCustomerId
												   ,@pFuelId = @lFUelId
												   ,@pTimeZoneParameter = @lTimeZoneParameter
			UPDATE #Results 
			SET [VehicleId] = @lId
			WHERE [Id] = SCOPE_IDENTITY()
		END 
		ELSE 
		BEGIN
			INSERT INTO #Results
			(
				 [Available]
				,[Assigned]
				,[AssignedLiters]
				,[RealAmount]
				,[RealLiters]
				,[TotalAssigned]
				,[Total]
				,[CreditCardId]
				,[AvailableLiters]
			)
			EXEC [Control].[Sp_GetDistributionData] @pDriverId = @lId
												   ,@pCustomerId = @pCustomerId												   
												   ,@pTimeZoneParameter = @lTimeZoneParameter
			UPDATE #Results 
			SET [DriverId] = @lId
			WHERE [Id] = SCOPE_IDENTITY()
		END

		SELECT @lId = MIN([Id])
		FROM #Ids
		WHERE [Id] > @lId
	END

	SELECT [VehicleId] 
		  ,[DriverId]
		  ,ISNULL([Available], 0) [Available]
		  ,ISNULL([Assigned], 0) [Assigned]
		  ,ISNULL([RealAmount], 0) [RealAmount]
		  ,ISNULL([TotalAssigned], 0) [TotalAssigned]
		  ,ISNULL([Total], 0) [Total]
		  ,ISNULL([AvailableLiters], 0) [AvailableLiters]
	FROM #Results

	DROP TABLE #Ids, #Results

	SET NOCOUNT OFF
END

GO
