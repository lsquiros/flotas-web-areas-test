/****** Object:  StoredProcedure [Control].[Sp_TransactionRuleBinnacle_Retrieve]    Script Date: 7/12/2019 6:56:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		María de los Ángeles Jiménez Chavarría
-- Create date: FEB/20/2019
-- Description:	Retrieve the transactions rules binnacle - The last 5 changes (omit repeated status)

--***************************************************************************************************
--Modify by:	Stefano Quiros
-- Create date: ABR/2/2019
-- Description:	Cambio para liberar DB 
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_TransactionRuleBinnacle_Retrieve]
(
	@pCustomerId INT,
	@pRuleId INT
)
AS
BEGIN		
	SET NOCOUNT ON;
	
	--WITH x AS
	--(
	--	SELECT  
	--		 e.[UserId]
	--		,CASE WHEN JSON_VALUE(REPLACE(REPLACE(e.[Mensaje], 'Modificaciones en las reglas de negocio: ', ''), '[DataChanges]', ''),'$.RuleActive') = 'true'
	--		 THEN 'Activada'
	--		 ELSE 'Desactivada'
	--		 END AS [Active]
	--		,e.[LogDateTime]
	--		,u.[Name] [EncryptedName]
	--		,rn = ROW_NUMBER()
	--		OVER (ORDER BY [LogDateTime])
	--	FROM [General].[EventLog] e
	--	INNER JOIN [General].[Users] u
	--	ON  e.[CustomerId] = @pCustomerId AND
	--		e.[Controller] = 'TransactionRules' AND
	--		e.[Action] = 'AddOrEditTransactionRules' AND
	--		e.[IsError] = 0 AND
	--		u.[UserId] = e.[UserId]
	--	WHERE JSON_VALUE(REPLACE(REPLACE(e.[Mensaje], 'Modificaciones en las reglas de negocio: ', ''), '[DataChanges]', ''),'$.RuleId') = @pRuleId
	--)

	----Delete repeated status! Do a simple SELECT * FROM x if you want to see the diference.
	--SELECT TOP 5 x.[UserId], x.[Active], x.[LogDateTime], x.[EncryptedName]
	--FROM x LEFT JOIN x AS y
	--ON (x.rn = y.rn + 1
	--AND x.[Active] <> y.[Active])
	--WHERE y.[Active] IS NOT NULL OR x.rn = 1
	--ORDER BY [LogDateTime] DESC

	SELECT 0 [UserId]
           ,'La información solicitada no esta disponible en este momento' [Active]
           ,GETDATE() [LogDateTime]
           ,'M6cGktcgEkajW2DlZDjxLWlageVf/e8eGR7XiTzBIb7fuWL6i6rtXw==' [EncryptedName]
    SET NOCOUNT OFF
END
GO
