USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomerCreditCards_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomerCreditCards_Retrieve]
GO

-- ===================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/15/2014
-- Description:	Retrieve Customer CreditCards information
-- ===================================================================================================
-- Modify: 03/08/2016 - Henry Retana - International Flag retrieve
-- Modify: JAN/17/2019 - María de los Ángeles Jiménez Chavarría - AssignedCredit
-- Stefano Quirós - 20/05/2019 - Add the SpendAmount by Customer and last BudgetClosingDate
-- ===================================================================================================
CREATE PROCEDURE [General].[Sp_CustomerCreditCards_Retrieve] 
(
	  @pCustomerId INT
	 ,@pCustomerCreditCardId INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @temp TABLE (
		[FuelByCreditId] INT,
		[FuelId] INT,
		[Year] INT,
		[Month] INT,
		[Total] DECIMAL (16,2),
		[Assigned] DECIMAL (16,3),
		[Available] DECIMAL(16,3),
		[FuelName] VARCHAR(50),
		[AssignedLiters] DECIMAL(16,3),
		[Liters] DECIMAL(16,3),
		[LiterPrice]  DECIMAL (16,2),
		[CurrencySymbol] NVARCHAR(4),
		[RowVersion] VARCHAR(1000),
		[UnitOfCapacityId] INT,
		[UnitOfCapacity] VARCHAR(100)
	)

	INSERT INTO @temp
		EXEC ('[Control].[Sp_FuelsByCredit_Retrieve] ' + @pCustomerId) 
	
	SELECT	 a.[CustomerCreditCardId]
			,a.[CustomerId]
			,a.[CreditCardNumber]
			,a.[ExpirationYear]
			,a.[ExpirationMonth]
			,a.[CreditLimit]
			,a.[StatusId]
			,b.[Name] AS [StatusName]
			,a.[CreditCardHolder]
			,d.[Symbol] AS [CurrencySymbol]
			,d.[Name] AS [CurrencyName]
			,a.[RowVersion]
			,a.[InternationalCard]
			,(SELECT SUM([Total]) FROM @temp) [AssignedCredit]			
	FROM [General].[CustomerCreditCards] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		INNER JOIN [General].[Countries] coun
			ON coun.[CountryId] = c.[CountryId]
	WHERE a.[CustomerId] = @pCustomerId
	  AND (@pCustomerCreditCardId IS NULL OR a.[CustomerCreditCardId] = @pCustomerCreditCardId)
	ORDER BY [CustomerId] DESC
	
    SET NOCOUNT OFF
END
