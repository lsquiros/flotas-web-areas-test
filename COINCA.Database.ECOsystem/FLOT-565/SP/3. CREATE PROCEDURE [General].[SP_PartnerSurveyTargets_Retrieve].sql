USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_PartnerSurveyTargets_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_PartnerSurveyTargets_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================
-- Author:		Henry Retana
-- Create date: 03/07/2019
-- Description:	Retrieve the Partner Survey Targets
-- =======================================================================================================================================

CREATE PROCEDURE [General].[SP_PartnerSurveyTargets_Retrieve]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT	[Id],
			[Description]
	FROM [General].[PartnerSurveyTargets] 
	WHERE [Active] = 1 
	AND [Delete] = 0

	SET NOCOUNT OFF
END

