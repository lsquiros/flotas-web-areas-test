/****** Object:  StoredProcedure [Control].[Sp_CustomerCredits_Retrieve]    Script Date: 7/12/2019 6:56:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CustomerCredit information
-- Modify by:	Gerald Solano
-- Modify date: 25/08/2016
-- Description:	Retrieve CustomerCredit Default information
-- Stefano Quiros - Make a new calculation when call the assigned credit from FuelsByCredit
-- Modify By: Stefano Quirós - Add the Time Zone Parameter - 12/06/2016
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_CustomerCredits_Retrieve]
(
	  @pCustomerId INT						--@pCustomerId: Customer Id
	 ,@pYear INT							--@pYear: Year
	 ,@pMonth INT							--@pMonth: Month
)
AS
BEGIN
	
	SET NOCOUNT ON
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	DECLARE @lAssignedSum DECIMAL(16,2)
	DECLARE @lAvailableSum DECIMAL(16,2)
	DECLARE @lTimeZoneParameter INT 
	DECLARE @lFuelsCount INT
	DECLARE @lCustomerCreditCardId INT 

	CREATE TABLE #FuelsPerCustomer
	(
		[CustomerId] INT
	   ,[FuelId] INT
	   ,[Assigned] DECIMAL
	)

	SELECT @lTimeZoneParameter = [TimeZone]	FROM [General].[Countries] co
								 INNER JOIN [General].[Customers] cu
								 ON co.[CountryId] = cu.[CountryId] 
								 WHERE cu.[CustomerId] = @pCustomerId


	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	-- VALIDA LA FECHA ENTRANTE PARA QUE COINCIDA CON LA FECHA LOCAL, 
	-- Y EVITAR QUE ENVIE DATOS EN 0 ANTES DEL CIERRE MENSUAL
	DECLARE @LOCAL_DATE DATETIME = DATEADD(HOUR, @lTimeZoneParameter, GETDATE())
	DECLARE @LOCAL_MONTH INT = DATEPART(mm,@LOCAL_DATE)
	DECLARE @LOCAL_YEAR INT =  DATEPART(yyyy,@LOCAL_DATE) 

	IF @pYear = @LOCAL_YEAR AND @pMonth <> @LOCAL_MONTH
	BEGIN
		 SET @pYear = @LOCAL_YEAR
		 SET @pMonth = @LOCAL_MONTH
	END 
	--//////////////////////////////////////////////////////////

	IF EXISTS(
		SELECT 1
		FROM [Control].[CustomerCredits] a
		WHERE a.[CustomerId] = @pCustomerId
	)
	BEGIN
		
		SELECT @lFuelsCount = COUNT([FuelId]) FROM [Control].[PartnerFuel] pf
									  INNER JOIN [General].[CustomersByPartner] cp
										ON pf.[PartnerId] = cp.[PartnerId]
									  WHERE cp.[CustomerId] = @pCustomerId 
									  AND [EndDate] > GETDATE() AND [ScheduledId] IS NULL

		INSERT INTO #FuelsPerCustomer
		SELECT TOP (@lFuelsCount) [CustomerId]
								 ,[FuelId] 
								 ,[Assigned]
		FROM [Control].[FuelsByCredit] 		
		WHERE [CustomerId] = @pCustomerId	
		Order By [InsertDate] DESC

	
		Select @lAssignedSum = SUM([Assigned]) FROM #FuelsPerCustomer

		SELECT TOP 1 @lCustomerCreditCardId = [CustomerCreditId] 
		FROM [Control].[CustomerCredits] 
		WHERE [CustomerId] = @pCustomerId
		ORDER BY [InsertDate] DESC

		UPDATE [Control].[CustomerCredits] 
		SET [CreditAssigned] = ISNULL(@lAssignedSum,0)
		WHERE [CustomerId] = @pCustomerId
		AND [CustomerCreditId] = @lCustomerCreditCardId

		SELECT TOP 1
			 a.[CustomerCreditId]
			,a.[CustomerId]
			,a.[Year]
			,a.[Month]
			,a.[CreditAmount]
			,a.[CreditAssigned]
			,a.[CreditAvailable]
			,a.[CreditTypeId]
			,@lCurrencySymbol AS [CurrencySymbol]
			,a.[RowVersion]
		FROM [Control].[CustomerCredits] a
		WHERE a.[CustomerId] = @pCustomerId
		ORDER BY [InsertDate] DESC
	END	
    SET NOCOUNT OFF
END
GO
