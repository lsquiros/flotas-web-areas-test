USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_PartnerSurveyQuestionTypes_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_PartnerSurveyQuestionTypes_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================
-- Author:		Henry Retana
-- Create date: 01/07/2019
-- Description:	Retrieve the Partner Survey Question Types
-- =======================================================================================================================================

CREATE PROCEDURE [General].[SP_PartnerSurveyQuestionTypes_Retrieve]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT	[Id],
			[Name],
			CAST([OneOption] AS BIT) [OneOption],
			CAST([ShowOptions] AS BIT) [ShowOptions]
	FROM [General].[QuestionTypes] 
	WHERE [Active] = 1 
	AND [Delete] = 0

	SET NOCOUNT OFF
END

