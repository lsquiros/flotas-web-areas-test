USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_SurveyToCustomer_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_SurveyToCustomer_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================
-- Author:		Henry Retana
-- Create date: 07/09/2019
-- Description:	Retrieve the Partner Survey to Customer
-- =======================================================================================================================================

CREATE PROCEDURE [General].[Sp_SurveyToCustomer_Retrieve]
(
	@pController VARCHAR(100),
    @pAction VARCHAR(100),
    @pOption VARCHAR(50) = NULL,
    @pCustomerId INT
)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT s.[Id]
	FROM [General].[PartnerSurvey] s
	INNER JOIN [General].[PartnerSurveyTargets] pt 
		ON s.[TargetId] = pt.[Id]
	INNER JOIN [General].[CustomersByPartner] cp
		ON s.[PartnerId] = cp.[PartnerId]
	WHERE s.[Delete] = 0
	AND s.[Active] = 1
	AND cp.[CustomerId] = @pCustomerId
	AND pt.[Controller] = @pController
	AND pt.[Action] = @pAction
	AND pt.[Options] = @pOption		

	SET NOCOUNT OFF
END

