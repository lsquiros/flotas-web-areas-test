USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[SP_FuelDistribution_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[SP_FuelDistribution_Retrieve]
GO

-- =============================================================================================================
-- Author:		Henry Retana
-- Create date: MAR/06/2018
-- Description:	Obtiene la información de vehiculos o conductores para la distribucion
-- ========================================================================================
-- Henry Retana   - APR/17/2018 - Add CustomerCapacityUnit
-- Gerald Solano  - APR/26/2018 - En distribucion por conductor se obtiene el ultimo registro aplicado
-- Stefano Quirós - JUN/13/2018 - Add Filter Active = 1 to the retrieve
-- Gerald Solano  - NOV/08/2018 - Validate Customer User when the user not exist in General.VehicleByUser table
-- Mari Jiménez   - JUN/27/2019 - Include UnitOfCapacityId, UnitOfCapacity, IssueForId, IssueFor, 
-- TypeOfDistributionId and TypeOfDistribution.
-- =============================================================================================================
CREATE PROCEDURE [Control].[SP_FuelDistribution_Retrieve]
(	
	@pId INT = NULL,
	@pVehicleId INT = NULL,
	@pUserId INT = NULL,	
	@pCustomerId INT = NULL,	
	@pPlateId VARCHAR(50) = NULL,
	@pVehicleGroupId INT = NULL,
	@pCostCenterId INT = NULL,
	@pLoggedUserId INT = NULL,
	@pIssueForId INT = NULL,
	@pIsSum BIT = NULL
)
AS
BEGIN
	SET NOCOUNT ON	

	DECLARE @lInsertDate DATE,			
			@lEquival DECIMAL(18,8) = 3.78541178,
			@lPartnerCapacityUnit INT, 
			@lCustomerCapacityUnitId INT,
			@lTop INT = 2147483647,
            @lTypeOfDistributionId VARCHAR(MAX),
            @lTypeOfDistribution VARCHAR(25),
            @lCustomerCapacityUnit VARCHAR(100),
            @lIssueFor VARCHAR(100)


	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pLoggedUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) 
					  FROM	@Results)
	-- END
	
	DECLARE @tableFuelsTemp TABLE
	(
		 [FuelByCreditId] INT
		,[FuelId] INT
		,[Year] INT
		,[Month] INT
		,[Total] DECIMAL(16, 2)
		,[Assigned] DECIMAL(18, 0)
		,[Available] DECIMAL(21, 2)
		,[FuelName] VARCHAR(50)
		,[AssignedLiters] DECIMAL(16, 2)
		,[Liters] DECIMAL(16, 2)
		,[LiterPrice] DECIMAL(16, 2)
		,[CurrencySymbol] VARCHAR(50)
		,[RowVersion] VARCHAR(200)
		,[UnitOfCapacityId] INT
		,[UnitOfCapacity] VARCHAR(100)
	)

    SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)

	IF @pCostCenterId = 0 SET @pCostCenterId = NULL
	IF @pIsSum = 0 OR @pIsSum IS NULL SET @lTop = 201

	INSERT INTO @tableFuelsTemp
	EXEC [Control].[Sp_FuelsByCredit_Retrieve] @pCustomerId
	    
	SELECT @lCustomerCapacityUnitId = c.[UnitOfCapacityId],
		   @pIssueForId = ISNULL(@pIssueForId, c.[IssueForId])
	FROM [General].[Customers] c
    WHERE c.[CustomerId] = @pCustomerId

    SELECT @lTypeOfDistributionId = [Value] 
    FROM [General].[ParametersByCustomer] 
    WHERE [Name] = 'TypeOfDistribution' 
    AND [CustomerId] = @pCustomerId
    
    SELECT @lCustomerCapacityUnit = [Name]
    FROM [General].[Types]
    WHERE [TypeId] = @lCustomerCapacityUnitId
    
    SELECT @lIssueFor = [Name] 
    FROM [General].[Types] 
    WHERE [TypeId] = @pIssueForId

    IF @lTypeOfDistributionId = '1'
    BEGIN
        SET @lTypeOfDistribution = 'DISTRIBUTION_BY_LITERS'
    END
    ELSE 
    BEGIN
        SET @lTypeOfDistributionId = '0'
        SET @lTypeOfDistribution = 'DISTRIBUTION_BY_AMOUNT'
    END

	IF (@lCustomerCapacityUnitId = 0) SET @lEquival = 1

	--Validate CustomerUsers when not exist in VehicleByUser
	SELECT @pUserId = CASE WHEN COUNT(1) = 0
                         THEN NULL
                         ELSE @pUserId
                      END
    FROM [General].[DriversUsers] d
    LEFT JOIN [General].[VehiclesByUser] v
        ON v.[UserId] = d.[UserId]
    WHERE (v.[UserId] IS NOT NULL AND v.[UserId]= @pUserId)
    OR (d.[UserId] IS NOT NULL AND d.[UserId] = @pUserId)

	IF @pIssueForId = 101 -- by vehicles
	BEGIN 
		SELECT DISTINCT TOP (@lTop)	 a.[Id]
                                    ,a.[VehicleId]
                                    ,a.[Month]
                                    ,a.[Year]									   
                                    ,ISNULL(a.[Liters], 0) [Liters]
                                    ,ISNULL(a.[Amount], 0) [Amount]
                                    ,ISNULL(a.[AdditionalLiters], 0) [AdditionalLiters]
                                    ,ISNULL(a.[AdditionalAmount], 0) [AdditionalAmount]
                                    ,d.[Name] [FuelName]
                                    ,d.[FuelId]						   
                                    ,b.[PlateId]
                                    ,b.[Name] [VehicleName]
                                    ,c.[VehicleModel]							   
                                    ,f.[Symbol] [CurrencySymbol]
                                    ,ISNULL((SELECT Top 1 ISNULL(tmp.[LiterPrice] * @lEquival, 0) 
                                                FROM @tableFuelsTemp tmp 
                                                WHERE tmp.[FuelId] = d.[FuelId]), 0) [LiterPrice]
                                    ,b.[CostCenterId]
                                    ,@lCustomerCapacityUnitId AS [UnitOfCapacityId]
                                    ,@lCustomerCapacityUnit AS [UnitOfCapacity]
                                    ,@pIssueForId [IssueForId]
                                    ,@lIssueFor AS [IssueFor]
                                    ,@lTypeOfDistributionId [TypeOfDistributionId]
                                    ,@lTypeOfDistribution [TypeOfDistribution]
		FROM [Control].[FuelDistribution] a
		INNER JOIN [General].[Vehicles] b
			ON a.[VehicleId] = b.[VehicleId]
			   AND a.Id = (SELECT TOP 1 fd.Id FROM [Control].[FuelDistribution] fd 
							WHERE fd.[VehicleId] = a.[VehicleId] AND 
								  CONVERT(DATE, fd.[InsertDate]) >= @lInsertDate 
							ORDER BY fd.Id DESC)	
		LEFT JOIN [General].[VehiclesByGroup] vg    
			ON b.[VehicleId] = vg.[VehicleId]  
		INNER JOIN [General].[VehicleCategories] c
			ON b.[VehicleCategoryId] = c.[VehicleCategoryId]
		INNER JOIN [General].[VehicleCostCenters] cc 
			ON b.[CostCenterId] = cc.[CostCenterId]
		INNER JOIN [Control].[CreditCardByVehicle] cv
			ON cv.[VehicleId] = b.[VehicleId]
		INNER JOIN [Control].[CreditCard] ccc
			ON cv.[CreditCardId] = ccc.[CreditCardId]
		INNER JOIN [Control].[Fuels] d
			ON c.[DefaultFuelId] = d.[FuelId]
		INNER JOIN [General].[Customers] e
			ON b.[CustomerId] = e.[CustomerId]
		INNER JOIN [Control].[Currencies] f
			ON e.[CurrencyId] = f.[CurrencyId]	
		WHERE (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
		AND b.[Active] = 1
		AND (b.[IsDeleted] IS NULL OR b.[IsDeleted] = 0)		
		AND (@pPlateId IS NULL OR b.[PlateId] = @pPlateId)		  
		AND (@pId IS NULL OR a.[Id] = @pId)
		AND (@pCustomerId IS NULL OR b.[CustomerId] = @pCustomerId)
		AND (@pCostCenterId IS NULL OR cc.[CostCenterId] = @pCostCenterId)
		AND (@pVehicleGroupId IS NULL OR vg.[VehicleGroupId] = @pVehicleGroupId)
		AND (@count = 0 OR a.[VehicleId] IN (SELECT [items] 
											 FROM @Results))
		AND CONVERT(DATE, a.[InsertDate]) >= @lInsertDate 
		AND ccc.[StatusId] IN (7, 8)
		ORDER BY a.[Id] ASC
  	END 
	ELSE -- by drivers
	BEGIN 
		SELECT DISTINCT TOP (@lTop)  a.[Id]
                                    ,a.[DriverId]
                                    ,a.[Month]
                                    ,a.[Year]		
                                    ,ISNULL(a.[Liters], 0) [Liters]
                                    ,ISNULL(a.[Amount], 0) [Amount]
                                    ,ISNULL(a.[AdditionalLiters], 0) [AdditionalLiters]
                                    ,ISNULL(a.[AdditionalAmount], 0) [AdditionalAmount]
                                    ,u.[Name] [EncryptedName]  
                                    ,du.[Identification] [EncryptedIdentification] 
                                    ,f.[Symbol] [CurrencySymbol]									       				   
                                    ,(SELECT TOP 1 v.[PlateId]      
                                        FROM [General].[Vehicles] v      
                                        INNER JOIN [General].[VehiclesByUser] vbu      
                                            ON vbu.[VehicleId] = v.[VehicleId]      
                                        WHERE vbu.[UserId] = @pUserId      
                                        AND vbu.[LastDateDriving] IS NULL) [PlateId] 
                                    ,(CASE WHEN (@lCustomerCapacityUnitId = 0 
                                                    AND @lPartnerCapacityUnit = 1) 
                                            THEN ISNULL((SELECT TOP 1 tmp.[LiterPrice] / @lEquival
                                                        FROM @tableFuelsTemp tmp), 0)
                                            ELSE ISNULL((SELECT TOP 1 tmp.[LiterPrice] / @lEquival
                                                        FROM @tableFuelsTemp tmp), 0)
                                    END) [LiterPrice]
                                    ,@lCustomerCapacityUnitId AS [UnitOfCapacityId]
                                    ,@lCustomerCapacityUnit AS [UnitOfCapacity]
                                    ,@pIssueForId [IssueForId]
                                    ,@lIssueFor AS [IssueFor]
                                    ,@lTypeOfDistributionId [TypeOfDistributionId]
                                    ,@lTypeOfDistribution [TypeOfDistribution]
		FROM [Control].[FuelDistribution] a
		INNER JOIN [General].[Users] u      
			ON u.[UserId] = a.[DriverId] 
			  AND a.Id = (SELECT TOP 1 fd.Id FROM [Control].[FuelDistribution] fd 
							WHERE fd.DriverId = a.[DriverId] AND 
								  CONVERT(DATE, fd.[InsertDate]) >= @lInsertDate 
							ORDER BY fd.Id DESC)       
		INNER JOIN [General].[DriversUsers] du      
			ON du.[UserId] = a.[DriverId]  
		LEFT JOIN [General].[VehiclesByUser] vbu    
			ON du.[UserId] = vbu.[UserId]    
		LEFT JOIN [General].[Vehicles] v    
			ON vbu.[VehicleId] = v.[VehicleId]
		LEFT JOIN [Control].[CreditCardByDriver] cd
			ON cd.[UserId] = u.[UserId]
		LEFT JOIN [Control].[CreditCard] ccc
			ON cd.[CreditCardId] = ccc.[CreditCardId]		
		LEFT JOIN [General].[Customers] c
			ON c.[CustomerId] = du.[CustomerId]
		LEFT JOIN [Control].[Currencies] f
			ON c.[CurrencyId] = f.[CurrencyId]
		WHERE (@pUserId IS NULL OR du.[UserId] = @pUserId)
		AND u.[IsActive] = 1
		AND (u.[IsDeleted] IS NULL OR u.[IsDeleted] = 0)
		AND (@pId IS NULL OR a.[Id] = @pId)   
		AND (@pCostCenterId IS NULL OR du.[CostCenterId] = @pCostCenterId)   
		AND (@pCustomerId IS NULL OR du.[CustomerId] = @pCustomerId)          
		AND CONVERT(DATE, a.[InsertDate]) >= @lInsertDate 
		AND ccc.[StatusId] IN (7, 8)
		ORDER BY a.[Id] ASC
	END 

	SET NOCOUNT OFF
END
GO
