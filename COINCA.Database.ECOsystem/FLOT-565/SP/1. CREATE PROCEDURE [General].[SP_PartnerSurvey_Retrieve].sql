USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_PartnerSurvey_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_PartnerSurvey_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================
-- Author:		Henry Retana
-- Create date: 28/06/2019
-- Description:	Retrieve the Partner Survey
-- =======================================================================================================================================

CREATE PROCEDURE [General].[SP_PartnerSurvey_Retrieve]
(
	@pPartnerId INT = NULL,
	@pCustomerId INT = NULL,
	@pId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT  sv.[Id],
		    sv.[Name],
			sv.[Description],
			sv.[TargetId],
			sv.[TargetDescription],
			sv.[Active],
			sv.[Mandatory],
			sv.[Delete],			
		    sv.[QuestionsStr]
	FROM 
	(
		SELECT  s.[Id],
				s.[Name],
				s.[Description],
				s.[TargetId],
				pt.[Description] [TargetDescription],
				s.[Active],
				s.[Mandatory],
				s.[Delete],
				CAST((
						SELECT	q.[Id],
								q.[Text],
								q.[QuestionTypeId],
								qt.[Name] AS [QuestionTypeText],
								q.[PartnerSurveyId],
								q.[Order],								
								(
									SELECT [OneOption]
									FROM [General].[QuestionTypes]
									WHERE [Id] = q.[QuestionTypeId]
								) [OneOption],
								(
									SELECT	o.[Id],
											o.[Text],
											o.[Order],
											o.[PartnerSurveyQuestionId],
											o.[NextSurveyQuestionId]
									FROM [General].[PartnerSurveyAnswer] o
									WHERE o.[Delete] = 0
									AND o.[PartnerSurveyQuestionId] = q.[Id] 
									ORDER BY o.[Order]
									FOR XML PATH('PartnerSurveyAnswer'), 
									ROOT('PartnerSurveyAnswer'), ELEMENTS XSINIL
								)  [AnswersStr]
							FROM [General].[PartnerSurveyQuestion] q
							INNER JOIN [General].[QuestionTypes] qt
								ON q.[QuestionTypeId] = qt.[Id]
							WHERE q.[Delete] = 0
							AND q.[PartnerSurveyId] = s.[Id] 
							ORDER BY q.[Order]
							FOR XML PATH('PartnerSurveyQuestion'), 
							ROOT('PartnerSurveyQuestion'), ELEMENTS XSINIL) AS VARCHAR(MAX)
					) [QuestionsStr]
		FROM [General].[PartnerSurvey] s
		INNER JOIN [General].[PartnerSurveyTargets] pt
			ON s.[TargetId] = pt.[Id]
		WHERE s.[Delete] = 0
		AND (@pId IS NULL OR s.[Id] = @pId)
		AND (
				(@pPartnerId IS NULL OR s.[PartnerId] = @pPartnerId)
				AND ( 
					@pCustomerId IS NULL 
					OR s.[PartnerId] = (
											SELECT [PartnerId]
											FROM [General].[CustomersByPartner]
											WHERE [CustomerId] = @pCustomerId
								       )
				)
		   )
		AND (@pCustomerId IS NULL OR s.[Active] = 1)
	) sv

	SET NOCOUNT OFF
END

