USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CustomerCredits_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CustomerCredits_Retrieve]
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: NOV/09/2014
-- Description:	Retrieve CustomerCredit information
-- ================================================================================================
-- Gerald Solano  - AUG/25/2016 - Retrieve CustomerCredit Default information
-- Stefano Quiros - Make a new calculation when call the assigned credit from FuelsByCredit
-- Stefano Quirós - JUN/12/2016 - Add the Time Zone Parameter
-- Mari Jiménez   - JUN/25/2019 - Include CreditAllocated
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_CustomerCredits_Retrieve]
(
	  @pCustomerId INT
	 ,@pYear INT
	 ,@pMonth INT
)
AS
BEGIN
	-- DECLARE @pCustomerId INT = 6431	--@pCustomerId: Customer Id
	-- ,@pYear INT	= 2019 --@pYear: Year
	-- ,@pMonth INT	= 5

	SET NOCOUNT ON
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	DECLARE @lAssignedSum DECIMAL(16,2)
    DECLARE @lTotalSum DECIMAL(16,2)
	DECLARE @lAvailableSum DECIMAL(16,2)
	DECLARE @lTimeZoneParameter INT 
	DECLARE @lFuelsCount INT
	DECLARE @lCustomerCreditCardId INT 

	CREATE TABLE #FuelsPerCustomer
	(
		[CustomerId] INT
	   ,[FuelId] INT
	   ,[Assigned] DECIMAL
       ,[Total] DECIMAL
	)

	SELECT @lTimeZoneParameter = [TimeZone]	FROM [General].[Countries] co
								 INNER JOIN [General].[Customers] cu
								 ON co.[CountryId] = cu.[CountryId] 
								 WHERE cu.[CustomerId] = @pCustomerId


	SELECT
		@lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
		INNER JOIN [General].[Customers] b
			ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	-- VALIDA LA FECHA ENTRANTE PARA QUE COINCIDA CON LA FECHA LOCAL, 
	-- Y EVITAR QUE ENVIE DATOS EN 0 ANTES DEL CIERRE MENSUAL
	DECLARE @LOCAL_DATE DATETIME = DATEADD(HOUR, @lTimeZoneParameter, GETDATE())
	DECLARE @LOCAL_MONTH INT = DATEPART(mm,@LOCAL_DATE)
	DECLARE @LOCAL_YEAR INT =  DATEPART(yyyy,@LOCAL_DATE) 

	IF @pYear = @LOCAL_YEAR AND @pMonth <> @LOCAL_MONTH
	BEGIN
		 SET @pYear = @LOCAL_YEAR
		 SET @pMonth = @LOCAL_MONTH
	END 
	--//////////////////////////////////////////////////////////

	IF EXISTS(
		SELECT 1
		FROM [Control].[CustomerCredits] a
		WHERE a.[CustomerId] = @pCustomerId
	)
	BEGIN
		
		SELECT @lFuelsCount = COUNT([FuelId]) FROM [Control].[PartnerFuel] pf
									  INNER JOIN [General].[CustomersByPartner] cp
										ON pf.[PartnerId] = cp.[PartnerId]
									  WHERE cp.[CustomerId] = @pCustomerId 
									  AND [EndDate] > GETDATE() AND [ScheduledId] IS NULL

		INSERT INTO #FuelsPerCustomer
		SELECT TOP (@lFuelsCount) [CustomerId]
								 ,[FuelId] 
								 ,[Assigned]
                                 ,[Total]
		FROM [Control].[FuelsByCredit]
		WHERE [CustomerId] = @pCustomerId	
		Order By [InsertDate] DESC

	
		Select @lAssignedSum = SUM([Assigned]) FROM #FuelsPerCustomer

        SELECT @lTotalSum = SUM([Total]) FROM #FuelsPerCustomer

		SELECT TOP 1 @lCustomerCreditCardId = [CustomerCreditId] 
		FROM [Control].[CustomerCredits] 
		WHERE [CustomerId] = @pCustomerId
		ORDER BY [InsertDate] DESC

		UPDATE [Control].[CustomerCredits] 
		SET [CreditAssigned] = ISNULL(@lAssignedSum,0)
		WHERE [CustomerId] = @pCustomerId
		AND [CustomerCreditId] = @lCustomerCreditCardId

		SELECT TOP 1
			 a.[CustomerCreditId]
			,a.[CustomerId]
			,a.[Year]
			,a.[Month]
			,a.[CreditAmount]
			,a.[CreditAssigned] --spent money
			,a.[CreditAvailable] --available money
            ,@lTotalSum [CreditAllocated] --allocated money (assigned money)
            ,(a.[CreditAssigned] + a.[CreditAvailable]) [CreditCardLimit] --limit
			,a.[CreditTypeId]
			,@lCurrencySymbol AS [CurrencySymbol]
			,a.[RowVersion]
		FROM [Control].[CustomerCredits] a
		WHERE a.[CustomerId] = @pCustomerId
		ORDER BY [InsertDate] DESC
	END	

    DROP TABLE #FuelsPerCustomer

    SET NOCOUNT OFF
END