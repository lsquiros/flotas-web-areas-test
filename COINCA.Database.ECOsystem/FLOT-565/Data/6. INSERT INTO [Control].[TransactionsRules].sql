USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Stefano Quir�s Ruiz
-- Create date: 27/06/2019
-- Description:	Insert new TransactionRule to validate UID
-- ================================================================================================


INSERT INTO [Control].[TransactionsRules]
(
	[RuleName]
   ,[RuleDescription]
   ,[RuleType]
   ,[IsActive]
   ,[InsertDate]
   ,[TransactionRuleType]
   ,[TransactionCode]
   ,[Order]
   ,[Description]
)
VALUES
(
	'UIDVehicle'
   ,'Validar UID (Sticker) vinculado a la placa de un Veh�culo'
   ,0
   ,1
   ,GETDATE()
   ,'V'
   ,'F2'
   ,18
   ,'Se valida que el UID correspondiente al Sticker, se encuentre correctamente ligado al veh�culo.'
)
		
