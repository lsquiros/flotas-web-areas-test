USE [ECOsystem]
GO

-- =======================================================================================================================================
-- Author:		Henry Retana
-- Create date: 28/06/2019
-- Description:	UPDATE [dbo].[AspMenus] 
-- =======================================================================================================================================

UPDATE [dbo].[AspMenus] 
SET [Name] = 'Reporte de Encuestas'
WHERE [Name] = 'Resultados de las Encuestas'