USE [ECOsystem]
GO

-- =======================================================================================================================================
-- Author:		Henry Retana
-- Create date: 28/06/2019
-- Description:	Insert Question Types Data
-- =======================================================================================================================================

INSERT INTO [General].[QuestionTypes] VALUES ('Selecci�n �nica', 1, 1,	1, 0, 1, GETDATE(),	NULL, NULL)
INSERT INTO [General].[QuestionTypes] VALUES ('Selecci�n multiple', 1, 1,	1, 0, 1, GETDATE(),	NULL, NULL)
INSERT INTO [General].[QuestionTypes] VALUES ('Texto', 1, 0, 1, 0, 1, GETDATE(),	NULL, NULL)
