USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Stefano Quir�s Ruiz
-- Create date: 08/07/2019
-- Description:	Insert new Payment Instrument
-- ================================================================================================

INSERT INTO [Control].[PaymentInstrumentsTypes]
(
	[Name]
   ,[Active] 
   ,[Delete]
   ,[InsertUserId]
   ,[InsertDate]
)
VALUES
(
	'Sticker'
   ,1
   ,0
   ,1
   ,GETDATE()
)