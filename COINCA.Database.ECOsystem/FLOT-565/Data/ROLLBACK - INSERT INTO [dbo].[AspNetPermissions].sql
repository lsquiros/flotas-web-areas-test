DECLARE @lPermission VARCHAR(100),
		@lAspMenu INT 

SELECT @lPermission = [Id]
FROM [dbo].[AspNetPermissions] 
WHERE [Name] = 'View_PartnerSurveyReport'

SELECT @lAspMenu = [Id]
FROM [dbo].[AspMenus]
WHERE [Name] = 'Resultados de las Encuestas' 
AND [Header] = 14

DELETE FROM [dbo].[AspMenusByRoles]
WHERE [MenuId] = @lAspMenu

DELETE FROM [dbo].[AspNetRolePermissions]
WHERE [PermissionId] = @lPermission

DELETE FROM [dbo].[AspNetPermissions] 
WHERE [Id] = @lPermission

DELETE FROM [dbo].[AspMenus] 
WHERE [Id] = @lAspMenu
