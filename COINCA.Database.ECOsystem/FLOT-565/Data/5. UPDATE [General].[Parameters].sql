USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Stefano Quir�s Ruiz
-- Create date: 26/06/2019
-- Description:	Update TransactionCode Flag to the Transaction Rules
-- ================================================================================================

UPDATE [General].[Parameters]
SET [TransactionCodeFlag] = 1
		
