USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 26/06/2019
-- Description:	Add Partner Survery Targes
-- ================================================================================================

INSERT INTO [General].[PartnerSurveyTargets] 
VALUES 
(
	'Eliminar Tarjeta de Cr�dito',
	'CreditCard',
	'EditCreditCard',
	'StatusId=9',
	1,
	0,
	NULL,	
	GETDATE(),
	NULL,
	NULL
)