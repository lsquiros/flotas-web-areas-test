DECLARE @lPermission VARCHAR(100),
		@lAspMenu INT 

SELECT @lPermission = [Id]
FROM [dbo].[AspNetPermissions] 
WHERE [Name] = 'View_PartnerSurvey'

SELECT @lAspMenu = [Id]
FROM [dbo].[AspMenus]
WHERE [Name] = 'Encuestas' 
AND [Header] = 17

DELETE FROM [dbo].[AspMenusByRoles]
WHERE [MenuId] = @lAspMenu

DELETE FROM [dbo].[AspNetRolePermissions]
WHERE [PermissionId] = @lPermission

DELETE FROM [dbo].[AspNetPermissions] 
WHERE [Id] = @lPermission

DELETE FROM [dbo].[AspMenus] 
WHERE [Id] = @lAspMenu
