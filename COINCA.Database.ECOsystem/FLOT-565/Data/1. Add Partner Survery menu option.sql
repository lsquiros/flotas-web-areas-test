USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 26/06/2019
-- Description:	Add Partner Survery menu option
-- ================================================================================================

DECLARE @NewPermID NVARCHAR(128) = NEWID()
	   ,@lParentId INT

INSERT INTO [dbo].[AspNetPermissions]
VALUES
(
	@NewPermID
	,'View_PartnerSurvey'
	,'PartnerSurvey'
	,'Index'
	,''
	,GETDATE()
)

SELECT @lParentId = [Id]
FROM [dbo].[AspMenus]
WHERE [Name] = 'Parámetros'
AND [Header] = 17

INSERT INTO [dbo].[AspMenus]
VALUES
(
	'Encuestas',
	'',
	@NewPermID,
	@lParentId,
	17,
	1,
	GETDATE()
)

DECLARE @NewMenuID INT = SCOPE_IDENTITY()

--INSERT PERMISSIONS TO THE MENU AND ROLE
INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('10703CAB-999E-4293-A423-28C94620EE6D', @NewMenuID) --Super Usuario Socio
INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('41f92f6b-84ff-4c74-bde6-0f15b67845b9', @NewMenuID) --PARTNER_USER
INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('A0465644-860E-4B8E-8779-7719B65D5E6E', @NewMenuID) --PARTNER_ADMIN

INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('10703CAB-999E-4293-A423-28C94620EE6D', @NewPermID) --Super Usuario Socio
INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('41f92f6b-84ff-4c74-bde6-0f15b67845b9', @NewPermID) --PARTNER_USER
INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('A0465644-860E-4B8E-8779-7719B65D5E6E', @NewPermID) --PARTNER_ADMIN