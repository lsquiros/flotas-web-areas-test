USE [ECOsystem]
GO

-- =======================================================================================================================================
-- Author:		Henry Retana
-- Create date: 28/06/2019
-- Description:	Insert Question Types Data
-- =======================================================================================================================================

UPDATE [General].[QuestionTypes] 
SET [Name] = 'Selecci�n m�ltiple'
WHERE [Id] = 2 
AND [Name] = 'Selecci�n multiple'
