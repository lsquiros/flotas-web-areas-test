USE [ECOsystem]
GO

IF OBJECT_ID (N'[General].[QuestionTypes]', N'U') IS NOT NULL DROP TABLE [General].[QuestionTypes]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 27/06/2019
-- Description:	Create table for Question Types
-- ================================================================================================

CREATE TABLE [General].[QuestionTypes]
(
	[Id] INT IDENTITY,
	[Name] VARCHAR(300),   
	[OneOption]BIT DEFAULT(0), 
	[ShowOptions]BIT DEFAULT(0),
	[Active] BIT DEFAULT(1),
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME,
	CONSTRAINT [PK_QuestionTypes] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) 
GO

