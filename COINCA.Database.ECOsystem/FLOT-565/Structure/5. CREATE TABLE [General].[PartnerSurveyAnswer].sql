USE [ECOsystem]
GO

IF OBJECT_ID (N'[General].[PartnerSurveyAnswer]', N'U') IS NOT NULL DROP TABLE [General].[PartnerSurveyAnswer]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 27/06/2019
-- Description:	Create table for Partner Survey Answer
-- ================================================================================================

CREATE TABLE [General].[PartnerSurveyAnswer]
(
	[Id] INT IDENTITY,
	[PartnerSurveyQuestionId] INT,
	[Text] VARCHAR(600),
	[Order] INT,
	[NextSurveyQuestionId] INT,
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME,
	CONSTRAINT [PK_PartnerSurveyAnswer] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) 
GO

ALTER TABLE [General].[PartnerSurveyAnswer] WITH CHECK ADD CONSTRAINT [FK_PartnerSurveyAnswer_PartnerSurveyQuestionId] FOREIGN KEY([PartnerSurveyQuestionId])
REFERENCES [General].[PartnerSurveyQuestion] ([Id])

GO

