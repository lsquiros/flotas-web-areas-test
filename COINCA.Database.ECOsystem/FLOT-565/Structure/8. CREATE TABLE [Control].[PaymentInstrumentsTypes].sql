USE [ECOsystem]
GO

IF OBJECT_ID (N'[Control].[PaymentInstrumentsTypes]', N'U') IS NOT NULL DROP TABLE [Control].[PaymentInstrumentsTypes]
GO
 
 -- ================================================================================================
-- Author:		Stefano Quir�s 
-- Create date: 16/07/2019
-- Description:	Create table for PaymentsInstrumentsTypes
-- ================================================================================================


CREATE TABLE [Control].[PaymentInstrumentsTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
	[Active] [bit] NULL,
	[Delete] [bit] NULL,
	[InsertUserId] [int] NULL,
	[InsertDate] [datetime] NULL,
	[ModifyUserId] [int] NULL,
	[ModifyDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


