USE [ECOsystem]
GO

IF OBJECT_ID (N'[General].[PartnerSurveyTargets]', N'U') IS NOT NULL DROP TABLE [General].[PartnerSurveyTargets]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 03/07/2019
-- Description:	Create table for Partner Survey Targets
-- ================================================================================================

CREATE TABLE [General].[PartnerSurveyTargets]
(
	[Id] INT IDENTITY,
	[Description] VARCHAR(300),
	[Controller] VARCHAR(300),
	[Action] VARCHAR(300),
	[Options] VARCHAR(300),	
	[Active] BIT DEFAULT(1),
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,
	[ModifyUserId] INT,
	[ModifyDate] DATETIME,
	CONSTRAINT [PK_PartnerSurveyTargets] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) 
GO