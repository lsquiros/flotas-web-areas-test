USE [ECOsystem]
GO

IF OBJECT_ID (N'[General].[PartnerSurveyQuestion]', N'U') IS NOT NULL DROP TABLE [General].[PartnerSurveyQuestion]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 27/06/2019
-- Description:	Create table for Partner Survey Question
-- ================================================================================================

CREATE TABLE [General].[PartnerSurveyQuestion]
(
	[Id] INT IDENTITY,
	[PartnerSurveyId] INT,
	[QuestionTypeId] INT,
	[Text] VARCHAR(600),
	[Order] INT,
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME,
	CONSTRAINT [PK_PartnerSurveyQuestion] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) 
GO

ALTER TABLE [General].[PartnerSurveyQuestion] WITH CHECK ADD CONSTRAINT [FK_PartnerSurveyQuestion_PartnerSurveyId] FOREIGN KEY([PartnerSurveyId])
REFERENCES [General].[PartnerSurvey] ([Id])

GO

ALTER TABLE [General].[PartnerSurveyQuestion] WITH CHECK ADD CONSTRAINT [FK_PartnerSurveyQuestion_QuestionTypeId] FOREIGN KEY([QuestionTypeId])
REFERENCES [General].[QuestionTypes] ([Id])

GO
