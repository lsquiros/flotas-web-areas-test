USE [ECOsystem]
GO

IF OBJECT_ID (N'[General].[PartnerSurveyAnswerByCustomer]', N'U') IS NOT NULL DROP TABLE [General].[PartnerSurveyAnswerByCustomer]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 27/06/2019
-- Description:	Create table for Partner Survey Answer By Customer
-- ================================================================================================

CREATE TABLE [General].[PartnerSurveyAnswerByCustomer]
(
	[Id] INT IDENTITY,
	[SurveyByCustomerId] INT,
	[PartnerSurveyQuestionId] INT,
	[PartnerSurveyAnswerId] INT,
	[CustomerId] INT,
	[Text] VARCHAR(1200),	
	[InsertUserId] INT,
	[InsertDate] DATETIME,
	CONSTRAINT [PK_PartnerSurveyAnswerByCustomer] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) 
GO

ALTER TABLE [General].[PartnerSurveyAnswerByCustomer] WITH CHECK ADD CONSTRAINT [FK_PartnerSurveyAnswerByCustomer_SurveyByAnswerId] FOREIGN KEY([SurveyByCustomerId])
REFERENCES [General].[PartnerSurveyByCustomer] ([Id])

GO

ALTER TABLE [General].[PartnerSurveyAnswerByCustomer] WITH CHECK ADD CONSTRAINT [FK_PartnerSurveyAnswerByCustomer_PartnerSurveyQuestionId] FOREIGN KEY([PartnerSurveyQuestionId])
REFERENCES [General].[PartnerSurveyQuestion] ([Id])

GO

ALTER TABLE [General].[PartnerSurveyAnswerByCustomer] WITH CHECK ADD CONSTRAINT [FK_PartnerSurveyAnswerByCustomer_PartnerSurveyAnswerId] FOREIGN KEY([PartnerSurveyAnswerId])
REFERENCES [General].[PartnerSurveyAnswer] ([Id])

GO

