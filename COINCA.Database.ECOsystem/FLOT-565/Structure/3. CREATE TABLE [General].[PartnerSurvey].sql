USE [ECOsystem]
GO

IF OBJECT_ID (N'[General].[PartnerSurvey]', N'U') IS NOT NULL DROP TABLE [General].[PartnerSurvey]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 27/06/2019
-- Description:	Create table for Partner Surveys
-- ================================================================================================

CREATE TABLE [General].[PartnerSurvey]
(
	[Id] INT IDENTITY,
	[PartnerId] INT,
	[Name] VARCHAR(300),
	[Description] VARCHAR(1000),
	[TargetId] INT,
	[Active] BIT DEFAULT(1),
	[Mandatory] BIT DEFAULT(1),
	[Delete] BIT DEFAULT(0),
	[InsertUserId] INT,
	[InsertDate] DATETIME,	
	[ModifyUserId] INT,
	[ModifyDate] DATETIME,
	CONSTRAINT [PK_PartnerSurvey] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) 
GO

ALTER TABLE [General].[PartnerSurvey] WITH CHECK ADD CONSTRAINT [FK_PartnerSurvey_PartnerId] FOREIGN KEY([PartnerId])
REFERENCES [General].[Partners] ([PartnerId])

GO

ALTER TABLE [General].[PartnerSurvey] WITH CHECK ADD CONSTRAINT [FK_PartnerSurvey_TargetId] FOREIGN KEY([TargetId])
REFERENCES [General].[PartnerSurveyTargets] ([Id])

GO