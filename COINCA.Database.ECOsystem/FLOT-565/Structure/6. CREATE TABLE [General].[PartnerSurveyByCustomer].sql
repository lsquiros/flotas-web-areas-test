USE [ECOsystem]
GO

IF OBJECT_ID (N'[General].[PartnerSurveyByCustomer]', N'U') IS NOT NULL DROP TABLE [General].[PartnerSurveyByCustomer]
GO
 
 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 23/07/2019
-- Description:	Create table for Partner Survey By Customer
-- ================================================================================================

CREATE TABLE [General].[PartnerSurveyByCustomer]
(
	[Id] INT IDENTITY,
	[SurveyId] INT,
	[Elements] INT,
	[CustomerId] INT,
	[InsertUserId] INT,
	[InsertDate] DATETIME,
	CONSTRAINT [PK_PartnerSurveyByCustomer] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) 
GO

ALTER TABLE [General].[PartnerSurveyByCustomer] WITH CHECK ADD CONSTRAINT [FK_PartnerSurveyByCustome_SurveyId] FOREIGN KEY([SurveyId])
REFERENCES [General].[PartnerSurvey] ([Id])

GO