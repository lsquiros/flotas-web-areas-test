--USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_SurveysToDownload_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_SurveysToDownload_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Author:  Henry Retana
-- Create date: 09/11/2018
-- Description: Retrieve Surveys to Download
-- Modify by: Marjorie Garbanzo - 04/12/2018 - Fix in the tables relationships
-- ================================================================================================  

CREATE PROCEDURE [Management].[Sp_SurveysToDownload_Retrieve]
(
	@pUserId INT = NULL,		
	@pDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	 @pDateStart DATETIME = DATEADD(dd, 0, DATEDIFF(dd, 0, @pDate)),
			 @pEndStart DATETIME = DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, 1, @pDate)))
			
	SELECT DISTINCT	s.[Id] [SurveyId],
					co.[Id] [CommerceId],
					u.[Id] [UserEventId],
					s.[Name] [Survey],
					co.[Name] [CommerceName],
					ISNULL(ed.[Name], cd.[Name]) [EventName],
					u.[EventDate] 
	FROM [Management].[UserAnswer] ua
	INNER JOIN [Management].[UserEvent] u
		ON ua.[UserEventId] = u.[Id]
	LEFT JOIN [Management].[EventDetail] ed
		ON u.[EventDetailId] = ed.[Id]
	INNER JOIN [Management].[Survey] s
		ON ed.[SurveyId] = s.[Id]	
	INNER JOIN [Management].[CatalogDetail] cd
		ON u.[EventTypeId] = cd.[Id]
	LEFT JOIN [General].[Commerces] co
		ON u.[CommerceId] = co.[Id]	
	WHERE u.[UserId] = @pUserId
	AND u.[EventDate] BETWEEN @pDateStart AND @pEndStart	
	ORDER BY u.[EventDate]

	SET NOCOUNT OFF
END
 