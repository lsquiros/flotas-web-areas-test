USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_ManagementParameters_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_ManagementParameters_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Stefano Quirós
-- Create date: 04/10/2018
-- Description:	Add or edit management parameters for the APP
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_ManagementParameters_AddOrEdit]
(
	@pCustomerId INT,
	@pDataSync INT = NULL,
	@pMovementName VARCHAR(500) = NULL,
	@pReconstructionType INT = NULL,
	@pTopMargin INT = 0,
	@pLowMargin INT = 0,
	@pTopPercent INT = 0,
	@pLowPercent INT = 0,
	@pMiddlePercent INT = 0,
	@pClientMinutes INT = 0,
	@pLostMinutes INT = 0,
	@pMovingMinutes INT = 0,
	@pTimeBeginJourney INT = 0,
	@pTimeFinishJourney INT = 0,
	@pStopMinimumTime INT = 0,
	@pStopMinimumDistance INT = 0,
	@pStopMinimumSpeed INT = 0,
	@pUserId INT
)
AS
BEGIN	
	SET NOCOUNT ON;

	IF EXISTS (SELECT * 
			   FROM [Management].[Parameters]
			   WHERE [CustomerId] = @pCustomerId)
	BEGIN
		UPDATE [Management].[Parameters]
		SET [DataSync] = @pDataSync,
			[MovementName] = @pMovementName,
			[ReconstructionType] = @pReconstructionType,
			[TopMargin] = @pTopMargin,
			[LowMargin] = @pLowMargin,
			[TopPercent] = @pTopPercent,
			[LowPercent] = @pLowPercent,
			[MiddlePercent] = @pMiddlePercent,
			[ClientMinutes] = @pClientMinutes,
			[LostMinutes] = @pLostMinutes,
			[MovingMinutes] = @pMovingMinutes,	
			[TimeBeginJourney] = @pTimeBeginJourney,
			[TimeFinishJourney] = @pTimeFinishJourney,
		    [StopMinimumTime] = @pStopMinimumTime,
		    [StopMinimumDistance] = @pStopMinimumDistance,
		    [StopMinimumSpeed] = @pStopMinimumSpeed,
			[ModifyDate] = GETDATE(),
			[ModifyUserId] = @pUserId
		WHERE [CustomerId] = @pCustomerId
	END
	ELSE
	BEGIN
		INSERT INTO [Management].[Parameters]
		(
			[CustomerId],
			[DataSync],
			[MovementName], 
			[ReconstructionType],
			[TopMargin],
			[LowMargin],
			[TopPercent],
			[LowPercent],
			[MiddlePercent],
			[ClientMinutes],
			[LostMinutes],
			[MovingMinutes],
			[TimeBeginJourney],
			[TimeFinishJourney],
		    [StopMinimumTime],
		    [StopMinimumDistance],
		    [StopMinimumSpeed],
			[InsertDate],
			[InsertUserId]
		)
		VALUES
		(
			@pCustomerId,
			@pDataSync,
			@pMovementName, 
			@pReconstructionType,	
			@pTopMargin,
			@pLowMargin,
			@pTopPercent,
			@pLowPercent,
			@pMiddlePercent,
			@pClientMinutes,
			@pLostMinutes,
			@pMovingMinutes,
			@pTimeBeginJourney,
			@pTimeFinishJourney,	
			@pStopMinimumTime,
		    @pStopMinimumDistance,
		    @pStopMinimumSpeed,				
			GETDATE(),
			@pUserId
		)
	END
		
	SET NOCOUNT OFF
END
