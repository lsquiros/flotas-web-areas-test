--USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[UpdateImportDrivers]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[UpdateImportDrivers]
GO

-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 25/09/2017
-- Description:	Actualiza el usuario de acuerdo a la informaci�n de 
-- ================================================================================================
-- Modify by:	Maria de los Angeles Jimenez Chavarria - Dic/03/2018 - Indicar la cartera a la cu�l pertenece el email.
-- Modify by:	Maria de los Angeles Jimenez Chavarria - Dic/05/2018 - Incluir el parametro para indicar si es un conductor agente. 
--																	 - Guardar correctamente la llave Dallas
-- ================================================================================================
CREATE PROCEDURE [General].[UpdateImportDrivers]
(	
	@pUserName VARCHAR(250) = NULL
   ,@pIdentification VARCHAR(250) = NULL
   ,@pLicense VARCHAR(250) = NULL
   ,@pCode VARCHAR(250) = NULL
   ,@pIsActive BIT = NULL
   ,@pEmail VARCHAR(250) = NULL
   ,@pPhoneNumber VARCHAR(250) = NULL
   ,@pLicenseExpiration DATETIME = NULL
   ,@pDallas INT = NULL
   ,@pDailyTransactionLimit INT
   ,@pCustomerId INT = NULL
   ,@pUserId INT = NULL
   ,@pIsAgent BIT = NULL
)
AS
BEGIN
	SET NOCOUNT ON	
	
	DECLARE @lStatus BIT
		   ,@lUserId INT
	
	SELECT @lStatus = u.[IsActive]
		  ,@lUserId = u.[UserId]
	FROM [General].[Users] u
	INNER JOIN [General].[DriversUsers] du
		ON du.[UserId] = u.[UserId]
	INNER JOIN [AspNetUsers] asp
		ON u.[AspNetUserId] = asp.[Id]
	WHERE asp.[Email] = @pEmail
	AND du.[CustomerId] = @pCustomerId
	
	IF @lUserId IS NOT NULL
	BEGIN
		UPDATE [General].[Users] 
		SET [Name] = @pUserName  
		   ,[IsActive] = @pIsActive
		   ,[IsAgent] = @pIsAgent
		   ,[ModifyDate] = GETUTCDATE()
		   ,[ModifyUserId] = @pUserId
		WHERE [UserId] = @lUserId

		UPDATE [General].[DriversUsers]  
		SET [Identification] = @pIdentification  
		   ,[Code] = @pCode
		   ,[License] = @pLicense
		   ,[LicenseExpiration] = @pLicenseExpiration
		   --,[Dallas] = @pDallas
		   ,[DailyTransactionLimit] = @pDailyTransactionLimit
		   ,[ModifyDate] = GETDATE()
		   ,[ModifyUserId] = @pUserId
		WHERE [UserId] = @lUserId

		UPDATE [AspNetUsers] 
		SET [PhoneNumber] = @pPhoneNumber
		WHERE [Email] = @pEmail

		IF (@pDallas IS NOT NULL)
        BEGIN
			EXEC [General].[Sp_DallasKeysByDriver_AddOrEdit] @pDallas, @lUserId, @pUserId
        END
        ELSE
        BEGIN
			DECLARE @DallasKeyId INT
				
			SELECT @DallasKeyId = [DallasId] 
			FROM [General].[DallasKeysByDriver]
			WHERE [UserId] = @lUserId

			IF(@DallasKeyId IS NOT NULL)
			BEGIN
				UPDATE [General].[DallasKeys]
				SET [IsUsed] = 0
				WHERE [DallasId] = @DallasKeyId

				DELETE FROM [General].[DallasKeysByDriver]
				WHERE [UserId] = @lUserId
			END
        END

		IF @lStatus = @pIsActive
		BEGIN
			SELECT 'actualiz�'
		END
		ELSE
		BEGIN
			IF @pIsActive = 1
			BEGIN
				SELECT 'activ�'	
			END
			ELSE
			BEGIN
				SELECT 'desactiv�'
			END		
		END
	END
	ELSE
	BEGIN
		SELECT c.[Name] 
		FROM [General].[Users] u
		INNER JOIN [General].[DriversUsers] du
			ON du.[UserId] = u.[UserId]
		INNER JOIN [AspNetUsers] asp
			ON u.[AspNetUserId] = asp.[Id]
		INNER JOIN [General].[Customers] c
			ON du.[CustomerId] = c.[CustomerId]
		WHERE asp.[Email] = @pEmail

		--SELECT 'no pertenece'
	END
	SET NOCOUNT OFF
END


