USE [ECOSystemQA]

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 30/11/2018
-- Description:	Add parameter to table
-- ================================================================================================

ALTER TABLE [Management].[Parameters]
ADD [BetweenManagement] VARCHAR(200) 