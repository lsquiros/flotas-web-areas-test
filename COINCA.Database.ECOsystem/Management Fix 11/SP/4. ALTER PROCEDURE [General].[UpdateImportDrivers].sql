--USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[UpdateImportDrivers]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[UpdateImportDrivers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Stefano Quirós
-- Create date: 25/09/2017
-- Description:	Actualiza el usuario de acuerdo a la información de 
-- Modify by:	Maria de los Angeles Jimenez Chavarria - Dic/03/2018 - Indicar la cartera a la cuál pertenece el email
-- ================================================================================================
CREATE PROCEDURE [General].[UpdateImportDrivers]
(	
	@pUserName VARCHAR(250) = NULL
   ,@pIdentification VARCHAR(250) = NULL
   ,@pLicense VARCHAR(250) = NULL
   ,@pCode VARCHAR(250) = NULL
   ,@pIsActive BIT = NULL
   ,@pEmail VARCHAR(250) = NULL
   ,@pPhoneNumber VARCHAR(250) = NULL
   ,@pLicenseExpiration DATETIME = NULL
   ,@pDallas VARCHAR(250) = NULL
   ,@pDailyTransactionLimit INT
   ,@pCustomerId INT = NULL
   ,@pUserId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON	
	
	DECLARE @lStatus BIT
		   ,@lUserId INT
	
	SELECT @lStatus = u.[IsActive]
		  ,@lUserId = u.[UserId]
	FROM [General].[Users] u
	INNER JOIN [General].[DriversUsers] du
		ON du.[UserId] = u.[UserId]
	INNER JOIN [AspNetUsers] asp
		ON u.[AspNetUserId] = asp.[Id]
	WHERE asp.[Email] = @pEmail
	AND du.[CustomerId] = @pCustomerId
	
	IF @lUserId IS NOT NULL
	BEGIN
		UPDATE [General].[Users] 
		SET [Name] = @pUserName  
		   ,[IsActive] = @pIsActive
		   ,[ModifyDate] = GETUTCDATE()
		   ,[ModifyUserId] = @pUserId
		WHERE [UserId] = @lUserId

		UPDATE [General].[DriversUsers]  
		SET [Identification] = @pIdentification  
		   ,[Code] = @pCode
		   ,[License] = @pLicense
		   ,[LicenseExpiration] = @pLicenseExpiration
		   ,[Dallas] = @pDallas
		   ,[DailyTransactionLimit] = @pDailyTransactionLimit
		   ,[ModifyDate] = GETDATE()
		   ,[ModifyUserId] = @pUserId
		WHERE [UserId] = @lUserId

		UPDATE [AspNetUsers] 
		SET [PhoneNumber] = @pPhoneNumber
		WHERE [Email] = @pEmail

		IF @lStatus = @pIsActive
		BEGIN
			SELECT 'actualizó'
		END
		ELSE
		BEGIN
			IF @pIsActive = 1
			BEGIN
				SELECT 'activó'	
			END
			ELSE
			BEGIN
				SELECT 'desactivó'
			END		
		END
	END
	ELSE
	BEGIN
		SELECT c.[Name] 
		FROM [General].[Users] u
		INNER JOIN [General].[DriversUsers] du
			ON du.[UserId] = u.[UserId]
		INNER JOIN [AspNetUsers] asp
			ON u.[AspNetUserId] = asp.[Id]
		INNER JOIN [General].[Customers] c
			ON du.[CustomerId] = c.[CustomerId]
		WHERE asp.[Email] = @pEmail

		--SELECT 'no pertenece'
	END
	SET NOCOUNT OFF
END


