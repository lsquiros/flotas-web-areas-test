--USE [ECOSystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_AgendaImport_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_AgendaImport_Add]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================  
-- Author:		Henry Retana
-- Create date: 23/01/2018
-- Description: Add Information from the import
-- Modify by:   Marjorie grbanzo Morales - 23/10/2018 - Change AgentCode to UserEmail.
-- Modify by:   Marjorie grbanzo Morales - 30/11/2018 - Add return ErrorMessage.
-- Modify by:   Marjorie grbanzo Morales - 03/12/2018 - Do not insert repeated.
-- ========================================================================================================  

CREATE PROCEDURE [Management].[Sp_AgendaImport_Add] 
(
	@pCustomerId INT,
	@pXmlData VARCHAR(MAX),
	@pCreateCommerce BIT = NULL, 
	@pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON  
	SET XACT_ABORT ON  
  
	BEGIN TRY  
		DECLARE @lErrorMessage VARCHAR(MAX)  
		DECLARE @lErrorSeverity INT  
		DECLARE @lErrorState INT  
		DECLARE @lLocalTran BIT = 0
        DECLARE @lRowCount INT = 0            
		DECLARE @lxmlData XML = CONVERT(XML, @pXmlData)
		            
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		CREATE TABLE #AgendaImport
		(
			[Id] INT IDENTITY,
			[Date] DATETIME,
			[Code] VARCHAR(50),
			[CommerceName] VARCHAR(200),
			[Latitude] FLOAT, 
			[Longitude] FLOAT, 
			[UserEmail] VARCHAR(300)
		)
	
		INSERT INTO #AgendaImport
		SELECT m.c.value('Date[1]', 'DATETIME'),
			   m.c.value('Code[1]', 'VARCHAR(50)'),
			   m.c.value('CommerceName[1]', 'VARCHAR(200)'),
			   m.c.value('Latitude[1]', 'FLOAT'),
			   m.c.value('Longitude[1]', 'FLOAT'),
			   m.c.value('UserEmail[1]', 'VARCHAR(300)')
		FROM @lxmlData.nodes('//AgendaImport') AS m(c)	

		IF @pCreateCommerce = 1 
		BEGIN
			--Create the new commerces
			INSERT INTO [General].[Commerces]
			(
				[Name],
				[Latitude],
				[Longitude],
				[Address],
				[Code],
				[InsertDate],
				[InsertUserId]
			)
			SELECT a.[CommerceName],
				   a.[Latitude], 
				   a.[Longitude],
				   [Efficiency].[Fn_GetAddressByLatLon_Retrieve](a.[Longitude], a.[Latitude], null),
				   a.[Code],
				   GETDATE(), 
				   @pUserId
			FROM #AgendaImport a
			LEFT JOIN [General].[Commerces] c
				ON a.[Code] = c.[Code]
			WHERE a.[Latitude] <> 0
			AND a.[Longitude] <> 0 
			AND c.[Code] IS NULL
		END 

		
		DECLARE @intFlag INT,
				@lEndFlag INT,
				@lUserId INT,
				@lCommerceId VARCHAR(50),
				@lDateAgenda DATETIME

		SET @intFlag = 1
		SET @lEndFlag = (SELECT COUNT(*) FROM #AgendaImport)
		
		WHILE (@intFlag <= @lEndFlag)
		BEGIN
			--PRINT @intFlag
			SET @lUserId = (SELECT d.[UserId]
							FROM #AgendaImport a
							INNER JOIN [dbo].[AspNetUsers] au
								ON a.[UserEmail] = au.[Email]
							INNER JOIN [General].[Users] u
								ON au.[Id] = u.[AspNetUserId]
							INNER JOIN [General].[DriversUsers] d
								ON u.[UserId] = d.[UserId]
							WHERE 
								 d.[CustomerId] = @pCustomerId
								 AND a.[Id] = @intFlag)

			SET @lCommerceId = (SELECT c.[Id]
								FROM #AgendaImport a
								INNER JOIN [General].[Commerces] c
									ON a.[Code] = c.[Code]
								WHERE a.[Id] = @intFlag)
								 			
			SET @lDateAgenda = (SELECT a.[Date]
								FROM #AgendaImport a
								WHERE a.[Id] = @intFlag)

			IF @lUserId IS NOT NULL
			BEGIN
				IF NOT EXISTS(SELECT * FROM [Management].[Agenda]
							WHERE [CommerceId] = @lCommerceId
								AND [UserId] = @lUserId
								AND [DateOfAgenda] = @lDateAgenda)
				BEGIN
					--Add the Data into the Agenda Table 
					INSERT INTO [Management].[Agenda]
					(
						[CommerceId],
						[UserId],
						[DateOfAgenda],
						[InsertDate],
						[LoggedUserId]
					)
					VALUES
					(
						@lCommerceId,
						@lUserId,
						@lDateAgenda,
						GETDATE(), 
						@pUserId
					)
				END
			END

			SET @intFlag = @intFlag + 1
		END
	
		--Return the not saved data 
		SELECT DISTINCT a.[Date],
						a.[Code],
						a.[CommerceName],
						a.[Latitude], 
						a.[Longitude], 
						a.[UserEmail],
						CASE WHEN (SELECT [Email] FROM [dbo].[AspNetUsers] WHERE [Email] = a.[UserEmail]) != NULL
							 THEN '' 
							 ELSE 'El correo electrónico no se encuentra registrado.' 
						END [ErrorMessage]				
		FROM #AgendaImport a
		WHERE a.[Id] NOT IN (SELECT a.[Id]
							 FROM #AgendaImport a
							 INNER JOIN [General].[Commerces] c
							 	ON a.[Code] = c.[Code]
							 INNER JOIN [dbo].[AspNetUsers] au
								ON a.[UserEmail] = au.[Email]
							INNER JOIN [General].[Users] u
								ON au.[Id] = u.[AspNetUserId]
							INNER JOIN [General].[DriversUsers] d
								ON u.[UserId] = d.[UserId]
							WHERE 
								 d.[CustomerId] = @pCustomerId)
	   AND a.[Date] IS NOT NULL
		        
        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION					
	END TRY  
	BEGIN CATCH  
		IF (@@TRANCOUNT > 0 AND XACT_STATE() > 0)	ROLLBACK TRANSACTION 
  
		SELECT @lErrorMessage = ERROR_MESSAGE()  
			  ,@lErrorSeverity = ERROR_SEVERITY()  
			  ,@lErrorState = ERROR_STATE()  
  
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)  
	END CATCH  
  
	SET NOCOUNT OFF  
	SET XACT_ABORT OFF  	
END