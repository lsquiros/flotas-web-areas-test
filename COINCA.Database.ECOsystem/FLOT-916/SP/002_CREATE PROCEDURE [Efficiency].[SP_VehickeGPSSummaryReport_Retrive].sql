IF EXISTS(SELECT 1 FROM sys.objects WHERE name = 'SP_VehicleGPSSummaryReport_Retrieve' AND type = 'P')
	DROP PROC [Efficiency].[SP_VehicleGPSSummaryReport_Retrieve]
GO

-- =============================================================================================================
-- Author:		Henry Retana
-- Create date: 16/06/2016
-- Description:	Retrieve Vehicle GPS Summary Report
-- =============================================================================================================
-- Modify: Henry Retana - 04/04/2017 - Modify the way it gets the lapsed
-- Modify: Henry Retana - 17/04/2017 - Add the Admin Speed
-- Modify: Marco Cabrera - Fix a problem with the total hours
-- Modify: Marco Cabrera - 25/05/2017 - Fix a problem with the sum of authorize and unauthorize hours
-- Modify: Henry Retana - Marco Cabrera - Stefano Quiros - 29/05/2017 - Fix a problem with the sum of authorize and unauthorize hours, Kilometers, and AdminSpeed
-- Modify: Albert Estrada - Nov/21/2017 Corrección primeros registros
-- Modify: Henry Retana - 25/01/2018 - Change GPSDateTime to RepDateTime
-- Modify: Henry Retana - 06/04/2018 - Add CustomerId to the Retrieve
-- Modify: Henry Retana - 17/05/2018 - Add SlowMotion Retrieve
-- Modify: Henry Retana - 02/07/2018 - Add TemperatureAVG Retrieve
-- Modify: Stefano Quirós Ruiz - 24/07/2018 - Re-Arm the procedure and make the calculates by ReportId 0 and 1, add a group by day on the result table
-- Modify: María de los Ángeles Jiménez Chavarría - JAN/07/2019 - Fix SlowMotion calculation
-- Modify: Juan Carlos Santamaria V. - 18/11/2019 - Fix EngineOn EngineOFF - JSA-001
-- ==============================================================================================================
CREATE PROCEDURE [Efficiency].[SP_VehicleGPSSummaryReport_Retrieve]
(
	@pCustomerId INT,
	@pStartDate DATETIME,
	@pEndDate DATETIME,
	@pUserId INT,
	@pCostCenterId INT = NULL,
	@pFromJob BIT = 1
) 
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT INTO	@Results
	EXEC [dbo].[Sp_UserDynamicFilter_Retrive] @pUserId, 'VH', @pCustomerId
	SET	@count = (SELECT COUNT(*) 
				  FROM	@Results);


	DECLARE @vIterator INT,
			@vLastIterator INT = 1,
			@LastId INT,
			@vDevice INT,
			@lDeviceCount INT,
			@vDateIni Datetime,
			@vDateFin Datetime,
			@lTimeZoneParameter INT, 
			@pCounter INT,
			@pIsAttrack INT = 0,
			@lStopDetail INT = NULL,
			@lSuddenStops INT = NULL,
			@lAbruptTurns VARCHAR(20) = NULL,
			@lQuickAccelerations INT = NULL,
			@lVehicleName VARCHAR(100),
			@lVehicleId INT,
			@lPlateId VARCHAR(50),
			@lStartDate DATETIME,
			@lEndDate DATETIME,
			@vIteratorUpdate INT = 0,
			@lMaxSpeed FLOAT,
			@lAvgSpeed FLOAT,
			@lDefaultReportId INT = 50,
			@lLastEvent BIT,
			@lDefaultDate DATETIME = '1900-01-01'

	SELECT @lStopDetail = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'STOPDETAIL_PARAMETER'
	SELECT @lSuddenStops = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'SUDDENSTOPS_PARAMETER'
	SELECT @lAbruptTurns = [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'ABRUPTTURNS_PARAMETER'
	SELECT @lQuickAccelerations = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'QUICKACCELERATIONS_PARAMETER'

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId

	CREATE TABLE #Devices
	(
		[Id] INT IDENTITY(1, 1),
		[Device] INT,
		[VehicleId] INT,
		[VehicleName] VARCHAR(100),
		[PlateId] VARCHAR(50)
	)

	CREATE TABLE #Result
	(
		[Id] INT IDENTITY,
		[VehicleId] INT,
		[VehicleName] VARCHAR(100),	
		[PlateId] VARCHAR(50),	
		[VeloMax] DECIMAL(16, 2),
		[AvgSpeed] DECIMAL(16, 2),
		[Stops] INT,
		[SuddenStops] INT, 
		[AbruptTurns] INT,
		[QuickAccelerations] INT,
		[Event] INT,
		[StartOn] DATETIME,
		[EndOff] DATETIME,
		[StartDate] DATETIME, 
		[EndDate] DATETIME,
		[HoursOn] INT, 
		[MinutesOn] INT,
		[SecondsOn] INT,
		[HoursOff] INT, 
		[MinutesOff] INT,
		[SecondsOff] INT,
		[TotalHoursUnAuthorized] INT,
		[TotalMinutesUnAuthorized] INT,
		[TotalSecondsUnAuthorized] INT,
		[TotalHoursAuthorized] INT,
		[TotalMinutesAuthorized] INT,
		[TotalSecondsAuthorized] INT,
		[SlowMotion] VARCHAR(30),
		[OnCount] INT,
		[FirstDate] DATETIME,
		[LastDate] DATETIME
	)

	CREATE TABLE #AuthorizedValues
	(
		[TotalHoursUnAuthorized] INT,
		[TotalMinutesUnAuthorized] INT,
		[TotalSecondsUnAuthorized] INT,
		[TotalHoursAuthorized] INT,
		[TotalMinutesAuthorized] INT,
		[TotalSecondsAuthorized] INT
	)

	SET @vDateIni = DATEADD(hh, @lTimeZoneParameter * -1, @pStartDate)
	SET @vDateFin = DATEADD(hh, @lTimeZoneParameter * -1, @pEndDate)

	INSERT INTO #Devices
	SELECT [DeviceReference]
		  ,[VehicleId]
		  ,[Name]
		  ,[PlateId]
	FROM [General].[Vehicles]
	WHERE [CustomerId] = @pCustomerId
	AND [Active] = 1
	AND ([IsDeleted] = 0 
			OR [IsDeleted] IS NULL)
	AND ([DeviceReference] IS NOT NULL 
			AND [DeviceReference] > 0)
	AND (@pCostCenterId IS NULL 
			OR [CostCenterId] = @pCostCenterId)

	SET @lDeviceCount = (SELECT MIN([Id]) FROM #Devices)

	WHILE @lDeviceCount IS NOT NULL
	BEGIN	
	
		CREATE TABLE #GeneralData
		(
			[Id] int IDENTITY not null,
			[RepDatetime] DATETIME,			
			[InputStatus] TINYINT,
			[VSSSpeed] FLOAT, 
			[ReportId] INT,
			[Device] INT,
			[Odometer] DECIMAL(16, 2)
		)			
		
		SELECT @vDevice = [Device] 
			  ,@lVehicleName = [VehicleName]
			  ,@lVehicleId = [VehicleId]
			  ,@lPlateId = [PlateId]
		FROM #Devices 
		WHERE [Id] = @lDeviceCount

		SELECT @pIsAttrack = COUNT(*) 
		FROM [General].[Vehicles]
		WHERE [DeviceReference] = @vDevice
		  AND [DeviceReference] IN (SELECT [Device]
									FROM [dbo].[Devices] 
									WHERE [UnitId] IN (SELECT [TerminalId]
														FROM [dbo].[DispositivosAVL] 
														WHERE [Modelo] = 18))		
		
		--Validate if it is from the Job use SvrDatetime to compare if not use RepDateTime
		INSERT INTO #GeneralData
		SELECT [RepDatetime],
			   [InputStatus], 
			   [VSSSpeed], 
			   [ReportId],
			   [Device],
			   [Odometer]
		FROM [dbo].[Reports] 
		WHERE [Device] = @vDevice 
		AND ([SvrDateTime] BETWEEN @vDateIni AND @VDateFin)
		AND DATEDIFF(DAY, [RepDatetime], @vDateIni) < 8
		ORDER BY [RepDatetime] ASC

		DECLARE @lCurrentDate DATE = (SELECT MIN([RepDatetime]) FROM #GeneralData)

		WHILE @lCurrentDate IS NOT NULL
		BEGIN

			CREATE TABLE #Data
			(
				[Id] int IDENTITY not null,
				[RepDatetime] DATETIME,			
				[InputStatus] TINYINT,
				[VSSSpeed] FLOAT, 
				[ReportId] INT,
				[Device] INT,
				[Odometer] DECIMAL(16, 2)
		    )

			INSERT INTO #Data
			SELECT [RepDatetime],
				   [InputStatus], 
				   [VSSSpeed], 
				   [ReportId],
				   [Device],
				   [Odometer]
			FROM #GeneralData
			WHERE CONVERT(DATE, [RepDatetime]) = @lCurrentDate 

			SELECT @lMaxSpeed = MAX([VSSSpeed])
			  ,@lAvgSpeed = AVG([VSSSpeed])
			FROM #Data
			WHERE [VSSSpeed] > 0
			AND ([ReportId] IN (@lDefaultReportId, @lStopDetail, @lSuddenStops, @lQuickAccelerations)
			OR [ReportId] IN (SELECT [items]
						      FROM dbo.Fn_Split(@lAbruptTurns, ',')))									  

			SELECT  [Id],
				[RepDatetime], 
				[InputStatus],
				[ReportId]  
			INTO #ChangeEvent
			FROM (
					SELECT [Id],
						   [RepDatetime], 
						   [InputStatus],
						   [ReportId]  
					FROM #Data 
					WHERE ReportId IN (0,1)
					UNION 
					SELECT TOP 1 [Id],
								 [RepDateTime],
								 [InputStatus],
								 [ReportId]
					FROM #Data
					ORDER BY [Id] DESC
			) TempData						

			SET @vLastIterator = 0

			SET @vIterator = (SELECT MIN([Id]) FROM #ChangeEvent)	
			
			SET @lLastEvent = (SELECT TOP 1 [LastEvent]
						   FROM [Efficiency].[VehicleGPSSummaryReport]
						   WHERE [VehicleId] = @lVehicleId
						   AND [StartDate] < (SELECT MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])) FROM #Data)
						   ORDER BY [InsertDate] DESC)	

			IF @lLastEvent IS NULL
		BEGIN
			SET @lLastEvent = (SELECT TOP 1 [ReportId]
							   FROM [Reports] 
							   WHERE [Device] = @vDevice 
							   AND [RepDateTime] BETWEEN DATEADD(WEEK, -1, @vDateIni) AND @vDateIni
							   AND [ReportId] IN (0,1) 																	 
							   ORDER BY [RepDateTime] DESC)
		END
			
			WHILE @vIterator IS NOT NULL AND @lLastEvent IS NOT NULL
			BEGIN	
				DECLARE @vIdentidad INT
				       ,@vIteratorStops INT
				       ,@vIteratorEnd INT
				       ,@vOdometer FLOAT
				       ,@vIdResults INT
				       ,@vDistance FLOAT

				IF @lLastEvent = 0
				BEGIN
					INSERT INTO #Result
					( 
						[VeloMax],
						[AvgSpeed],
						[VehicleName],
						[VehicleId],
						[PlateId],
						[StartDate], 
						[EndDate],
						[HoursOn], 
						[MinutesOn],
						[SecondsOn],
						[Event],
						[StartOn],
						[OnCount],
						[FirstDate],
						[LastDate]
					)
					SELECT  @lMaxSpeed,
							@lAvgSpeed,
							@lVehicleName,
							@lVehicleId,
							@lPlateId,
							MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])),
							MAX(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])),
							DATEDIFF(S, MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])), MAX(DATEADD(hh, @lTimeZoneParameter, [RepDatetime]))) / 3600,
							RIGHT('0' + CAST(DATEDIFF(S, MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])), MAX(DATEADD(hh, @lTimeZoneParameter, [RepDatetime]))) % 3600 / 60 AS VARCHAR(2)), 2),
							RIGHT('0' + CAST(DATEDIFF(S, MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])), MAX(DATEADD(hh, @lTimeZoneParameter, [RepDatetime]))) % 60 AS VARCHAR(2)), 2),
							@lLastEvent,
							MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])),
							CASE WHEN MIN([ReportId]) = 0 THEN 1 ELSE 0 END,
							MIN([RepDateTime]),
							MAX([RepDateTime])
					FROM #Data WHERE [Id] BETWEEN @vLastIterator AND @vIterator 

					SET @vIdentidad = @@Identity

					SET @lStartDate = (SELECT [StartDate] FROM #Result WHERE [Id] = @vIdentidad)
					SET @lEndDate = (SELECT [EndDate] FROM #Result WHERE [Id] = @vIdentidad)

					DELETE FROM #AuthorizedValues

					INSERT INTO #AuthorizedValues
					EXEC [Efficiency].[SP_VehicleGPSSummaryAuthorizedLapseTime_Retrieve] @pCustomerId, @lStartDate, @lEndDate

					UPDATE #Result
					SET [TotalHoursUnAuthorized] = a.[TotalHoursUnAuthorized],
						[TotalMinutesUnAuthorized] = a.[TotalMinutesUnAuthorized],
						[TotalSecondsUnAuthorized] = a.[TotalSecondsUnAuthorized],
						[TotalHoursAuthorized] = a.[TotalHoursAuthorized],
						[TotalMinutesAuthorized] = a.[TotalMinutesAuthorized],
						[TotalSecondsAuthorized] = a.[TotalSecondsAuthorized]
					FROM #AuthorizedValues a
					WHERE [Id] = @vIdentidad				

					IF @pIsAttrack = 0
					BEGIN 
						UPDATE #Result 
						SET [Stops] = v.[Stops]
						FROM (SELECT COUNT([VSSSpeed]) [Stops] 
							  FROM #Data 
							  WHERE [ReportId] = @lStopDetail 
								AND [Id] BETWEEN @vLastIterator AND @vIterator) v
						WHERE #Result.[Id] = @vIdentidad

						UPDATE #Result 
						SET [SuddenStops] = v.[SuddenStops]
						FROM (SELECT COUNT([VSSSpeed]) [SuddenStops] 
							  FROM #Data 
							  WHERE [ReportId] = @lSuddenStops 
								AND [Id] BETWEEN @vLastIterator AND @vIterator) v
						WHERE #Result.[Id] = @vIdentidad

						UPDATE #Result 
						SET [AbruptTurns] = v.[AbruptTurns]
						FROM (SELECT COUNT([VSSSpeed]) [AbruptTurns]
							  FROM #Data 
							  WHERE [ReportId] IN (SELECT [items] FROM [dbo].[Fn_Split](@lAbruptTurns, ',')) 
							  AND [Id] BETWEEN @vLastIterator AND @vIterator) v
						WHERE #Result.[Id] = @vIdentidad

						UPDATE #Result 
						SET [QuickAccelerations] = v.[QuickAccelerations]
						FROM (SELECT COUNT([VSSSpeed]) [QuickAccelerations] 
							  FROM #Data 
							  WHERE [ReportId] = @lQuickAccelerations 
							  AND [Id] BETWEEN @vLastIterator AND @vIterator) v
						WHERE #Result.[Id] = @vIdentidad
					END
					ELSE
					BEGIN 
						UPDATE #Result 
						SET [Stops] = v.[Stops]
						FROM (SELECT COUNT([VSSSpeed]) [Stops]
							  FROM #Data 
							  WHERE [VSSSpeed] = 0 
								AND [Id] BETWEEN @vLastIterator AND @vIterator) v
						WHERE #Result.[Id] = @vIdentidad
					END

					--GET SLOW MOTION		
					;WITH ONNoSpeed AS
					(
						SELECT * 
						FROM #Data 
					    WHERE [VSSSpeed] = 0 
						AND [Id] BETWEEN @vLastIterator AND @vIterator
					)
					,GetTimes AS 
					(		
						SELECT n.[Id],
							   n.[RepDateTime] [StarDate],
							   ISNULL(
								   (
									   SELECT [RepDateTime]
									   FROM #Data
									   WHERE [Id] = n.[Id] + 1
								   ), n.[RepDateTime]
								) [EndDate]	 
						FROM ONNoSpeed n
					)
					,TotalLapses AS
					(
						SELECT DATEDIFF(S, [StarDate], [EndDate]) AS [SlowMotionTime]
						FROM GetTimes
					)

					UPDATE #Result
					SET [SlowMotion] =	(
											SELECT CONVERT(VARCHAR(20), DATEADD(ms, SUM([SlowMotionTime]) * 1000, 0), 108)
											FROM TotalLapses
										)
					WHERE [Id] = @vIdentidad
				END
				ELSE
				BEGIN
					INSERT INTO #Result
					( 
						[VeloMax],
						[AvgSpeed],
						[VehicleName],
						[VehicleId],
						[PlateId],
						[StartDate], 
						[EndDate],
						[HoursOff], 
						[MinutesOff],
						[SecondsOff],
						[Event],
						[EndOff],
						[FirstDate],
						[LastDate]
					)
					SELECT  @lMaxSpeed,
							@lAvgSpeed,
							@lVehicleName,
							@lVehicleId,
							@lPlateId,
							MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])),
							MAX(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])),
							DATEDIFF(S, MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])), MAX(DATEADD(hh, @lTimeZoneParameter, [RepDatetime]))) / 3600,
							RIGHT('0' + CAST(DATEDIFF(S, MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])), MAX(DATEADD(hh, @lTimeZoneParameter, [RepDatetime]))) % 3600 / 60 AS VARCHAR(2)), 2),
							RIGHT('0' + CAST(DATEDIFF(S, MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])), MAX(DATEADD(hh, @lTimeZoneParameter, [RepDatetime]))) % 60 AS VARCHAR(2)), 2),
							@lLastEvent,
							MIN(DATEADD(hh, @lTimeZoneParameter, [RepDatetime])),
							MIN([RepDateTime]),
							MAX([RepDateTime])
					FROM #Data WHERE [Id] BETWEEN @vLastIterator AND @vIterator 
				END

				SET @lLastEvent = (SELECT [ReportId] FROM #ChangeEvent WHERE [Id] = @vIterator)	

				DELETE FROM #ChangeEvent 
				WHERE [Id] IN (@vLastIterator, @vIterator)
				
				SET @vLastIterator = @vIterator
				
				SELECT @vIterator = MIN([Id]) FROM #ChangeEvent WHERE [Id] > @vLastIterator													    
			
			END		

			/*JSA-001*/
			UPDATE #result
			SET [StartOn] = (SELECT MIN(repdatetime) FROM #data WHERE reportId = 0 GROUP BY ReportID)
			WHERE CONVERT(DATE,StartDate) = @lCurrentDate
			
			UPDATE #result
			SET [EndOff] = (SELECT MAX(repdatetime) FROM #data WHERE reportId = 1 GROUP BY ReportID)
			WHERE CONVERT(DATE,StartDate) = @lCurrentDate
			
			DROP TABLE #Data
			DROP TABLE #ChangeEvent

			SELECT @lCurrentDate = MIN([RepDatetime]) FROM #GeneralData WHERE CONVERT(DATE, [RepDatetime]) > @lCurrentDate

		END

		DROP TABLE #GeneralData

		SELECT @lDeviceCount = MIN([Id]) FROM #Devices WHERE [Id] > @lDeviceCount
	END 

	DECLARE @Id_Update TABLE(Id INT)

	INSERT INTO @Id_Update
	SELECT MIN([Id]) 
	FROM #Result 
	WHERE [Event] = 0
	GROUP BY [VehicleName], [PlateId]

	SELECT @vIteratorUpdate = MIN(Id) FROM @Id_Update

	WHILE @vIteratorUpdate IS NOT NULL
	BEGIN
		SET @lStartDate = (SELECT [StartDate] FROM #Result WHERE [Id] = @vIteratorUpdate)
		SET @lEndDate = (SELECT [EndDate] FROM #Result WHERE [Id] = @vIteratorUpdate)
		
		DELETE FROM #AuthorizedValues

		INSERT INTO #AuthorizedValues
		EXEC [Efficiency].[SP_VehicleGPSSummaryAuthorizedLapseTime_Retrieve] @pCustomerId, @lStartDate, @lEndDate

		UPDATE #Result
		SET [TotalHoursUnAuthorized] = a.[TotalHoursUnAuthorized],
			[TotalMinutesUnAuthorized] = a.[TotalMinutesUnAuthorized],
			[TotalSecondsUnAuthorized] = a.[TotalSecondsUnAuthorized],
			[TotalHoursAuthorized] = a.[TotalHoursAuthorized],
			[TotalMinutesAuthorized] = a.[TotalMinutesAuthorized],
			[TotalSecondsAuthorized] = a.[TotalSecondsAuthorized]
		FROM #AuthorizedValues a
		WHERE [Id] = @vIteratorUpdate
		AND [Event] = 0

		SELECT @vIteratorUpdate = MIN(Id) FROM @Id_Update WHERE Id > @vIteratorUpdate
	END

	CREATE TABLE #TempResult
	(
		[VehicleId] INT,
		[HoursOn] INT,
		[MinutesOn] INT,
		[SecondsOn] INT,
		[HoursOff] INT,
		[MinutesOff] INT,
		[SecondsOff] INT,
		[HoursAuthorize] INT,
		[MinutesAuthorize] INT,
		[SecondsAuthorize] INT,
		[HoursUnAuthorize] INT,
		[MinutesUnAuthorize] INT,
		[SecondsUnAuthorize] INT
	)

	INSERT INTO #TempResult
	(
		[VehicleId],
		[HoursOn],
		[MinutesOn],
		[SecondsOn],
		[HoursOff],
		[MinutesOff],
		[SecondsOff],
		[HoursAuthorize],
		[MinutesAuthorize],
		[SecondsAuthorize],
		[HoursUnAuthorize],
		[MinutesUnAuthorize],
		[SecondsUnAuthorize]
	)
	SELECT  [VehicleId],
			ROUND((ROUND(SUM([SecondsOn]) / 60, 0, 1) + SUM([MinutesOn])) / 60, 0, 1) + SUM([HoursOn]),
			(ROUND(SUM([SecondsOn]) / 60, 0, 1) + (SUM([MinutesOn]) % 60)) % 60,
			SUM([SecondsOn]) % 60,
			ROUND((ROUND(MIN([SecondsOff]) / 60, 0, 1) + MIN([MinutesOff])) / 60, 0, 1) + MIN([HoursOff]),
			(ROUND(MIN([SecondsOff]) / 60, 0, 1) + (MIN([MinutesOff]) % 60)) % 60,
			MIN([SecondsOff]) % 60,
			ROUND((ROUND(SUM([TotalSecondsAuthorized]) / 60, 0, 1) + SUM([TotalMinutesAuthorized])) / 60, 0, 1) + SUM([TotalHoursAuthorized]),
			(SUM([TotalSecondsAuthorized]) / 60 + SUM([TotalMinutesAuthorized])) % 60,
			SUM([TotalSecondsAuthorized]) % 60,
			ROUND((ROUND(SUM([TotalSecondsUnAuthorized]) / 60, 0, 1) + SUM([TotalMinutesUnAuthorized])) / 60, 0, 1) + SUM([TotalHoursUnAuthorized]),
			(SUM([TotalSecondsUnAuthorized]) / 60 + SUM([TotalMinutesUnAuthorized])) % 60,
			SUM([TotalSecondsUnAuthorized]) % 60			
    FROM #Result
	GROUP BY [VehicleId]

    SELECT v.[CustomerId]
		  ,v.[VehicleId]
		  ,r.[VehicleName]
		  ,r.[PlateId]
		  ,(SELECT TOP 1 u.[Name]
            FROM [General].[Users] u
            INNER JOIN [General].[VehiclesDrivers] vd
               ON u.[UserId] = vd.[UserId]
            INNER JOIN [General].[VehiclesByUser] vu
               ON u.[UserId] = vu.[UserId] 
               AND vd.[VehicleId] = vu.[VehicleId]
            WHERE vd.[VehicleId] = MIN(v.[VehicleId])
            AND (vu.[LastDateDriving] BETWEEN MIN(r.[FirstDate]) AND MAX(r.[LastDate]) OR vu.[LastDateDriving] IS NULL)) [EncryptDriverName]

		  ,ISNULL(MIN(r.[StartDate]), @pStartDate) [StartDate]
		  ,ISNULL(MAX(r.[EndDate]), @pEndDate) [EndDate]
		  
		  ,ISNULL(MIN(StartOn),@lDefaultDate) [StartOn] 
		  ,ISNULL(MAX(EndOff),@lDefaultDate) [EndOff] 

		  ,CONVERT(INT, MAX(ISNULL(r.[VeloMax], 0))) [MaxSpeed]
		  ,CONVERT(DECIMAL(12,2), MAX(ISNULL(r.[AvgSpeed], 0))) [AvgSpeed]
		  ,ISNULL([Efficiency].[Fn_GetOverSpeedLapseTime](MIN(r.[VehicleId]), MIN(r.[FirstDate]), MAX(r.[LastDate])), '0:00:00') [OverSpeedSummary]
		  ,SUM(ISNULL(r.[OnCount], 0)) [OnCount]
		  ,ISNULL(ISNULL([Efficiency].[Fn_GetStopsByLapse_Retrieve](v.[DeviceReference], MIN(r.[FirstDate]), MAX(r.[LastDate]), v.[CustomerId]), SUM(r.[Stops])), 0) [Stops]
		  ,ISNULL(SUM(r.[SuddenStops]), 0) [SuddenStops]
		  ,ISNULL(SUM(r.[AbruptTurns]), 0) [AbruptTurns]
		  ,ISNULL(SUM(r.[QuickAccelerations]), 0) [QuickAccelerations]
		  ,REPLACE(ISNULL(CONCAT(MIN(CAST(t.[HoursOn] AS VARCHAR(10))), ':', MIN(RIGHT('0' + CAST(t.[MinutesOn] AS VARCHAR(2)), 2)), ':', MIN(RIGHT('0' + CAST(t.[SecondsOn] AS VARCHAR(2)), 2))), '0:00:00'), '::', '0:00:00') [LapseOn]
		  ,REPLACE(ISNULL(CONCAT(MIN(CAST(t.[HoursOff] AS VARCHAR(10))), ':', MIN(RIGHT('0' + CAST(t.[MinutesOff] AS VARCHAR(2)), 2)), ':', MIN(RIGHT('0' + CAST(t.[SecondsOff] AS VARCHAR(2)), 2))), '0:00:00'), '::', '0:00:00') [LapseOff] 
		  ,REPLACE(ISNULL(CONCAT(MIN(CAST(t.[HoursAuthorize] AS VARCHAR(10))), ':', MIN(RIGHT('0' + CAST(t.[MinutesAuthorize] AS VARCHAR(2)), 2)), ':', MIN(RIGHT('0' + CAST(t.[SecondsAuthorize] AS VARCHAR(2)), 2))), '0:00:00'), '::', '0:00:00') [AuthorizedTime]
		  ,REPLACE(ISNULL(CONCAT(MIN(CAST(t.[HoursUnAuthorize] AS VARCHAR(10))), ':', MIN(RIGHT('0' + CAST(t.[MinutesUnAuthorize] AS VARCHAR(2)), 2)), ':', MIN(RIGHT('0' + CAST(t.[SecondsUnAuthorize] AS VARCHAR(2)), 2))), '0:00:00'), '::', '0:00:00') [UnAuthorizedTime]
		  ,CONVERT(DECIMAL(16,2), ISNULL(MAX(vc.[SpeedDelimited]), 0)) [AdminSpeed]
		  ,CONVERT(DECIMAL(16,3), (SELECT MAX([Odometer]) - MIN([Odometer]) 
									FROM [Reports] 
									WHERE Device = v.[DeviceReference] 
									AND [RepDateTime] BETWEEN MIN(r.[FirstDate]) 
									AND MAX(r.[LastDate]))) [Kilometers]
		  ,CASE WHEN @pCostCenterId IS NOT NULL THEN vcc.[Name] ELSE NULL END [CostCenterName]
		  ,(SELECT	COUNT(items)
			FROM	dbo.Fn_Split((SELECT [Efficiency].[Fn_GetOverSpeedSummaryByVehicle](MIN(r.[VehicleId]), MIN(r.[FirstDate]), MAX(r.[LastDate]), 1)), ',')
			WHERE items = 'R') [OverSpeedCount]
		  ,ISNULL(MAX(r.[SlowMotion]), '0:00:00') [SlowMotion]
		  ,ISNULL([Efficiency].[Fn_GetTemperatureAverage] (v.[DeviceReference], MIN(r.[FirstDate]), MAX(r.[LastDate])), 0.00) [TemperatureAVG]
		  ,CONVERT(INT, (SELECT TOP 1 [Event] FROM #Result WHERE [VehicleId] = v.[VehicleId] ORDER BY [Id] DESC)) [LastEvent]
	 FROM #Result r
	 LEFT JOIN #TempResult t
	  ON r.[VehicleId] = t.[VehicleId]
	 INNER JOIN [General].[Vehicles] v
	  ON v.[VehicleId] = r.[VehicleId]
	 INNER JOIN [General].[VehicleCategories] vc
	  ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
	 INNER JOIN [General].[VehicleCostCenters] vcc
	  ON v.[CostCenterId] = vcc.[CostCenterId]
	 GROUP BY r.[VehicleName], r.[PlateId], vcc.[Name], v.[CustomerId], v.[VehicleId], v.[DeviceReference], CONVERT(DATE, r.[StartDate])
	 	 
	 DROP TABLE #Devices
	 DROP TABLE #Result
	 DROP TABLE #TempResult
	 DROP TABLE #AuthorizedValues
	 
	 SET NOCOUNT OFF
END

