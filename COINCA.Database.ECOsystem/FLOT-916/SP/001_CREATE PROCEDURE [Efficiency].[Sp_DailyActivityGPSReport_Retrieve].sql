IF EXISTS(SELECT 1 FROM sys.objects WHERE name = 'Sp_DailyActivityGPSReport_Retrieve' AND type = 'P')
	DROP PROC [Efficiency].[Sp_DailyActivityGPSReport_Retrieve]
GO
-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/04/2018
-- Description:	Retrieve Daily Activity GPS Report
-- ================================================================================================
-- Modify: Henry Retana - 17/05/2018 - Add SlowMotion Retrieve
-- Modify: Stefano Quirós - 30/07/2018 - Change the form that sum the times, because when the time was greater than 24 crash
-- Modify: Maria de los Angeles Jimenez Chavarria - JAN/29/2019 - Send report program
-- Modify: Juan Carlos Santamaria V. - 11-19-2019 - Field StartOn EndOff formate date (dd/MM/yyyy hh:mm:ss tt)
-- ================================================================================================
CREATE PROCEDURE [Efficiency].[Sp_DailyActivityGPSReport_Retrieve] --7369, '8/1/2018 12:00:00 AM', '8/10/2018 11:59:59 PM', null, null, 1
(
	@pCustomerId INT,
	@pStartDate DATETIME,
    @pEndDate DATETIME,
	@pVehicleId INT = NULL,
	@pCostCenterId INT = NULL,
	@pVehicleGroupId INT = NULL,
	@pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@lResults TABLE ([items] INT)
	DECLARE @lCount INT
	DECLARE @lDefaultDate DATETIME = '1900-01-01'

	INSERT	@lResults
	EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId

	
	DECLARE @vDateIni Datetime,
			@vDateFin Datetime,
			@lTimeZoneParameter INT

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId

	SET @vDateIni = DATEADD(hh, @lTimeZoneParameter * -1, @pStartDate)
	SET @vDateFin = DATEADD(hh, @lTimeZoneParameter * -1, @pEndDate)

	SET @lCount = (SELECT COUNT(*) FROM	@lResults);

	SELECT a.[VehicleId],
		   ISNULL(a.[CostCenterName], vc.[Name]) [CostCenterName],
		   IIF (@pVehicleGroupId IS NULL, 'Todos', vg.[Name]) [VehicleGroupName],
		   a.[VehicleName],
		   a.[PlateId],	
		   MAX(a.[MaxSpeed]) [MaxSpeed], 

		   RTRIM(CONVERT(VARCHAR(20), SUM(CASE WHEN SUBSTRING(a.[LapseOn], 2, 1) = ':' 
										   THEN DATEDIFF(SECOND, 0, CASE WHEN a.[LapseOn] like '%-%' OR a.[LapseOn] like '%*%' 
																	THEN '0:00:00' 
																	ELSE a.[LapseOn]
																	END) 
										   ELSE LEFT(a.[LapseOn],2) * 3600 
										   END) / (60 * 60))) + ':' + 
										   RIGHT('0' + RTRIM(CONVERT(VARCHAR(2), (SUM(CASE WHEN SUBSTRING(a.[LapseOn], 2, 1) = ':' 
																					  THEN DATEDIFF(SECOND, 0, CASE WHEN a.[LapseOn] like '%-%' OR a.[LapseOn] like '%*%' 
																											   THEN '0:00:00' 
																											   ELSE a.[LapseOn] 
																											   END) 
																					   ELSE LEFT(a.[LapseOn],2) * 3600 
																					   END) / 60) % 60)), 2) + ':' + 
										   RIGHT('0' + RTRIM(CONVERT(VARCHAR(2), SUM(CASE WHEN SUBSTRING(a.[LapseOn], 2, 1) = ':' 
																					 THEN DATEDIFF(SECOND, 0, CASE WHEN a.[LapseOn] like '%-%' OR a.[LapseOn] like '%*%' 
																											  THEN '0:00:00' 
																											  ELSE a.[LapseOn] 
																											  END) 
																					  ELSE LEFT(a.[LapseOn],2) * 3600 
																					  END) % 60)),2) [LapseOn],		  
		   SUM(a.[Kilometers]) [Kilometers],
		   [StartOn] = CASE WHEN MIN(a.[StartOn]) != @lDefaultDate THEN FORMAT(MIN(a.[StartOn]), N'dd/MM/yyyy hh:mm:ss tt') ELSE '--' END,
		   [EndOff]  = CASE WHEN MAX(a.[EndOff]) !=  @lDefaultDate THEN FORMAT(MAX(a.[EndOff]), N'dd/MM/yyyy hh:mm:ss tt') ELSE '--'END,
		   DATEPART(DW, a.[StartDate]) - 1 [DayOW],
		   MAX(a.[AdminSpeed]) [AdminSpeed],

		   RTRIM(CONVERT(VARCHAR(20), SUM(DATEDIFF(SECOND, 0, ISNULL(CASE WHEN a.[SlowMotion] like '%-%' OR a.[SlowMotion] like '%*%' 
																	 THEN '0:00:00' 
																	 ELSE a.[SlowMotion] 
																	 END, '00:00:00'))) / (60 * 60))) + ':' + 
			RIGHT('0' + RTRIM(CONVERT(VARCHAR(2), (SUM(DATEDIFF(SECOND, 0, ISNULL(CASE WHEN a.[SlowMotion] like '%-%' OR a.[SlowMotion] like '%*%' THEN '0:00:00' ELSE a.[SlowMotion] END, '00:00:00'))) / 60) % 60)), 2) + ':' + 
			RIGHT('0' + RTRIM(CONVERT(VARCHAR(2), SUM(DATEDIFF(SECOND, 0, ISNULL(CASE WHEN a.[SlowMotion] like '%-%' OR a.[SlowMotion] like '%*%' THEN '0:00:00' ELSE a.[SlowMotion] END, '00:00:00'))) % 60)),2) [SlowMotion],
			CAST(a.[StartDate] AS DATE) [StartDate]
	FROM [Efficiency].[VehicleGPSSummaryReport] a
	INNER JOIN [General].[Vehicles] v
		ON a.[VehicleId] = v.[VehicleId]
	LEFT JOIN [General].[VehicleCostCenters] vc
		ON v.[CostCenterId] = vc.[CostCenterId]
	LEFT JOIN [General].[VehiclesByGroup] vbg
		ON v.[VehicleId] = vbg.[VehicleId]
	LEFT JOIN [General].[VehicleGroups] vg
		ON vbg.[VehicleGroupId] = vg.[VehicleGroupId]
	WHERE a.[CustomerId] = @pCustomerId
	AND (@lCount = 0 OR a.[VehicleId] IN (SELECT [items] 
									      FROM @lResults))
	AND a.[StartDate] BETWEEN @pStartDate AND @pEndDate
	AND (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
	AND (@pCostCenterId IS NULL OR v.[CostCenterId] = @pCostCenterId)	
	AND (@pVehicleGroupId IS NULL OR vg.[VehicleGroupId] = @pVehicleGroupId)
	GROUP BY a.[VehicleId],
			 a.[CostCenterName],
			 vc.[Name],
			 vg.[Name],
			 a.[VehicleName], 
			 DATEPART(DW, a.[StartDate]),
			 a.[PlateId],
			 a.[DriverName],
			 v.[DeviceReference],
			 CAST(a.[StartDate] AS DATE),
			 CAST(DATEADD(DAY, 1, a.[EndDate]) AS DATE)
	ORDER BY CAST(a.[StartDate] AS DATE)
    SET NOCOUNT OFF
END

