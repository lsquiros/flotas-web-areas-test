--USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_GeneralParametersByPartner_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_GeneralParametersByPartner_AddOrEdit]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 18/11/2016
-- Description:	Insert or Update General Parameters By Partner
-- ================================================================================================
-- Modify by Henry Retana - 11/12/2017 - Add Service Station Parameters
-- Modify by Henry Retana - 14/02/2018 - Add Preauthorized Days
-- Modify by Henry Retana - 18/06/2018 - Add the prices edit
-- Modify by Henry Retana - 07/08/2018 - Add Contract Information
-- Modify by Maria de los Angeles Jimenez Chavarria - 02/Nov/2018 - Include @pStationTolerance parameter.
-- Modify by Maria de los Angeles Jimenez Chavarria - 11/Dic/2018 - Add CardAlarmEmail in the select stament.
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_GeneralParametersByPartner_AddOrEdit]
(
	@pId INT = NULL
	,@pAlarmDays INT = NULL
	,@pRTN VARCHAR(100) = NULL
	,@pPhoneNumber VARCHAR(100) = NULL
	,@pAddress VARCHAR(MAX) = NULL
	,@pBacService VARCHAR(100) = NULL
	,@pBacName VARCHAR(200) = NULL
	,@pPartnerId INT = NULL
	,@pStationTolerance INT = NULL
	,@pCreditCardNumber VARCHAR(500) = NULL
	,@pAmount DECIMAL(16, 2) = NULL
	,@pActive BIT = NULL
	,@pCardAlarmEmail VARCHAR(MAX) = NULL
	,@pPreauthorizedDays INT = NULL
	,@pPreauthorizedEmail VARCHAR(MAX) = NULL
	,@pUserId INT
	,@pSMSPriceXML VARCHAR(MAX) = NULL
	,@pSMSInternalModem BIT = NULL
	,@pContractYears INT = NULL
	,@pSelectAllVehicles BIT = NULL
    ,@pContractAlertDays INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0
               ,@lRowCount INT = 0            
			   ,@lSMSPriceId INT = 0

		SELECT @lSMSPriceId = [Value]
		FROM [dbo].[GeneralParameters]
		WHERE [ParameterID] = 'CUSTOMER_PRICE_SMS'
			    
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
			
		--USE INTERNAL MODEM
		UPDATE  [General].[Partners]
		SET [SMSInternalModem] = @pSMSInternalModem
		WHERE [PartnerId] = @pPartnerId

		--PARAMETERS FOR SERVICE STATION VALIDATION
		IF @pCreditCardNumber IS NOT NULL 
			AND @pAmount IS NOT NULL 			  			   
		BEGIN
			DECLARE @lMasterCardId INT = NULL

			SELECT @lMasterCardId = [CustomerCreditCardId]
			FROM [Control].[CreditCard] cc
			INNER JOIN [General].[CustomerCreditCards] c
				ON cc.[CustomerId] = c.[CustomerId]
			WHERE cc.[CreditCardNumber] = @pCreditCardNumber
					
			IF @lMasterCardId IS NULL  RAISERROR ('La tarjeta no existe en Flotas.', 16, 1)

			IF EXISTS(SELECT *
						FROM [General].[ServiceStationsPOSCheckParameters]
						WHERE [PartnerId] = @pPartnerId)
			BEGIN
				UPDATE [General].[ServiceStationsPOSCheckParameters]
				SET [CreditCardNumber] = @pCreditCardNumber,
					[MasterCardId] = @lMasterCardId,
					[Amount] = @pAmount,
					[IsActive] = @pActive,
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				WHERE [PartnerId] = @pPartnerId
			END
			ELSE
			BEGIN
				INSERT INTO [General].[ServiceStationsPOSCheckParameters]
				(
					[PartnerId], 
					[CreditCardNumber],	
					[MasterCardId],
					[Amount],
					[IsActive],
					[InsertUserId],
					[InsertDate]
				)
				VALUES
				(
					@pPartnerId, 
					@pCreditCardNumber,
					@lMasterCardId,
					@pAmount,
					@pActive,
					@pUserId,
					GETDATE()
				)
			END
		END
		ELSE 
		BEGIN
			IF EXISTS(SELECT *
						FROM [General].[ServiceStationsPOSCheckParameters]
						WHERE [PartnerId] = @pPartnerId)
			BEGIN
				UPDATE [General].[ServiceStationsPOSCheckParameters]
				SET [IsActive] = 0,
					[ModifyUserId] = @pUserId,
					[ModifyDate] = GETDATE()
				WHERE [PartnerId] = @pPartnerId
			END
		END

		IF @pId IS NULL
		BEGIN
			INSERT INTO [General].[GeneralParametersByPartner]
			(
				[PartnerId]					
				,[InsertDate]
				,[AlarmDays]
				,[RTN]
				,[PhoneNumber]
				,[Address]
				,[BacService]
				,[BacName]
				,[StationTolerance]
				,[CardAlarmEmail]
				,[PreauthorizedDays]
				,[PreauthorizedEmail]
				,[ContractYears]
				,[SelectAllVehicles]
				,[ContractAlertDays]
			)
			VALUES	
			(	
				@pPartnerId					
				,GETUTCDATE()
				,@pAlarmDays
				,@pRTN
				,@pPhoneNumber
				,@pAddress
				,@pBacService
				,@pBacName
				,@pStationTolerance
				,@pCardAlarmEmail
				,@pPreauthorizedDays
				,@pPreauthorizedEmail
				,@pContractYears
				,@pSelectAllVehicles
				,@pContractAlertDays
			)						
		END
		ELSE
		BEGIN				
			UPDATE [General].[GeneralParametersByPartner]
			SET  [InsertDate] = GETUTCDATE()
				,[AlarmDays] = @pAlarmDays
				,[RTN] = @pRTN
				,[PhoneNumber] = @pPhoneNumber
				,[Address] = @pAddress
				,[BacService] = @pBacService
				,[BacName] = @pBacName
				,[StationTolerance] = @pStationTolerance
				,[CardAlarmEmail] = @pCardAlarmEmail
				,[PreauthorizedDays] = @pPreauthorizedDays
				,[PreauthorizedEmail] = @pPreauthorizedEmail
				,[ContractYears] = @pContractYears
				,[SelectAllVehicles] = @pSelectAllVehicles
				,[ContractAlertDays] = @pContractAlertDays
			WHERE [PartnerId] = @pPartnerId
				AND [Id] = @pId									
		END		
            
        SET @lRowCount = @@ROWCOUNT
            
		--INSERT DATA FOR SMS PRICES
		EXEC [General].[Sp_CustomerRangePrices_AddOrEdit] NULL, @pPartnerId, @lSMSPriceId, @pSMSPriceXML, @pUserId

        IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
		BEGIN
			COMMIT TRANSACTION
		END
			
		IF @lRowCount = 0
		BEGIN
			RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
		END			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
