--USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CardExpiration_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CardExpiration_Retrieve]
GO
-- ================================================================================================
-- Author:		María de los Ángeles Jiménez Chavarría
-- Create date: 13/Dic/2018
-- Description:	Retrieve the card expiration alarms. Alert is Send 0, 30 and 60 days before expiration.
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CardExpiration_Retrieve] 
AS BEGIN
	DECLARE @lTodayDate DATE = GETDATE() 
	
	SELECT	ccd.[CreditCardNumber],
			ccd.[ExpirationMonth],
			ccd.[ExpirationYear],
			c.[CustomerId],
			c.[Name] [EncryptedCustomerName],
			ai.[Email] [CustomerEmail],
			gp.[CardAlarmEmail] [PartnerEmail],
			DATEDIFF(DAY, @lTodayDate, (CONVERT(VARCHAR(4), ccd.[ExpirationYear]) + '' + IIF (ccd.[ExpirationMonth] > 9, CONVERT(VARCHAR(2),ccd.[ExpirationMonth]), ('0' + CONVERT(VARCHAR(1), ccd.[ExpirationMonth]))) + '01')) [RemainingTime]
	FROM [General].[Partners] p
	INNER JOIN [General].[GeneralParametersByPartner] gp
		ON p.[PartnerId] = gp.[PartnerId]
	INNER JOIN [General].[CustomersByPartner] cp
		ON p.[PartnerId] = cp.[PartnerId]
	INNER JOIN [General].[Customers] c
		ON cp.[CustomerId] = c.[CustomerId]
	INNER JOIN [Control].[CreditCard] ccd
		ON c.[CustomerId] = ccd.[CustomerId]
	INNER JOIN [General].[Status] s 
		ON ccd.[StatusId] = s.[StatusId]
	LEFT JOIN [General].[CustomerAdditionalInformation] ai
		ON ai.[CustomerId] = c.[CustomerId]
	WHERE 
	(gp.[CardAlarmEmail] IS NOT NULL) AND  
	(p.[IsDeleted] = 0 OR p.[IsDeleted] IS NULL) AND 
	c.[IsActive] = 1 AND
	(c.[IsDeleted] = 0 OR c.[IsDeleted] IS NULL) AND
	s.[IsActive] = 1 AND 
	s.[Code] NOT IN ('CUSTOMER_CREDIT_CARD_REJECTED_KEY','CUSTOMER_CREDIT_CARD_CANCELLED_KEY') AND 
	DATEDIFF(DAY, @lTodayDate, (CONVERT(VARCHAR(4), ccd.[ExpirationYear]) + '' + IIF (ccd.[ExpirationMonth] > 9, CONVERT(VARCHAR(2),ccd.[ExpirationMonth]), ('0' + CONVERT(VARCHAR(1), ccd.[ExpirationMonth]))) + '01')) IN (0, 30, 60)		
			
END