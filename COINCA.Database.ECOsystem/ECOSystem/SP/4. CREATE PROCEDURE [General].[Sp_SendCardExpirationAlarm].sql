--USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_SendCardExpirationAlarm]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_SendCardExpirationAlarm] 
GO
-- ================================================================================================
-- Author:		María de los Ángeles Jiménez Chavarría
-- Create date: 13/Dic/2018
-- Description:	Send card expiration alarms
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_SendCardExpirationAlarm]  
(
	@pCreditCardNumber NVARCHAR(520),  
	@pExpirationMonth VARCHAR(50),  
	@pExpirationYear INT,  
	@pCustomerId INT,  
	@pCustomerName VARCHAR(250),
	@pCustomerEmail VARCHAR (500) = NULL,   
	@pPartnerEmail VARCHAR (500) = NULL,
	@pRemainingTimeMessage VARCHAR(250)
)
AS  
BEGIN   
	SET NOCOUNT ON
	SET XACT_ABORT ON

	--=======================================
	DECLARE @lErrorMessage NVARCHAR(4000)
	DECLARE @lErrorSeverity INT
	DECLARE @lErrorState INT
	--=======================================

	DECLARE @lTimeZoneParameter INT = -6
	DECLARE @Message NVARCHAR(MAX)

	SELECT @lTimeZoneParameter = ISNULL(co.[TimeZone], -6) 
	FROM [General].[Countries] co   
	INNER JOIN [General].[Customers] cu 
		ON co.[CountryId] = cu.[CountryId]  
	WHERE cu.CustomerId = @pCustomerId  

	BEGIN TRY

		SELECT @Message = a.[Message] 
		FROM [General].[Values] a 
		INNER JOIN [General].[Types] b 
			ON a.[TypeId] = b.[TypeId]
		WHERE b.[Code] = 'CUSTOMER_ALARM_TRIGGER_FLEETCARD_EXPIRATION'

		SET @Message = REPLACE(@Message, '%RemainingTimeMessage%', @pRemainingTimeMessage)
		SET @Message = REPLACE(@Message, '%ExpirationMonth%', @pExpirationMonth)
		SET @Message = REPLACE(@Message, '%ExpirationYear%', @pExpirationYear)
		SET @Message = REPLACE(@Message, '%CreditCardNumber%', @pCreditCardNumber)
		SET @Message = REPLACE(@Message, '%time%', CONVERT(VARCHAR(10), DATEADD(Hour, @lTimeZoneParameter,GETUTCDATE()), 111) + ' ' + CONVERT(VARCHAR(8), DATEADD(Hour, @lTimeZoneParameter,GETUTCDATE()), 108))

		IF @pCustomerEmail IS NOT NULL
		INSERT INTO [General].[Emails] 
		(
			[To],  
			[Subject], 
			[Message], 
			[InsertDate]
		)  
		VALUES 
		(
			@pCustomerEmail,
			'Alarma por Vencimiento de Tarjeta', 
			@Message, 
			DATEADD(Hour, @lTimeZoneParameter,GETUTCDATE())
		) 

		IF @pPartnerEmail IS NOT NULL
		INSERT INTO [General].[Emails] 
		(
			[To],  
			[Subject], 
			[Message], 
			[InsertDate]
		)  
		VALUES 
		(
			@pPartnerEmail,
			'Alarma para el cliente: ' + @pCustomerName + ' por vencimiento de tarjeta', 
			@Message, 
			DATEADD(Hour, @lTimeZoneParameter,GETUTCDATE())
		) 

	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END

		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH

    SET NOCOUNT OFF
END  