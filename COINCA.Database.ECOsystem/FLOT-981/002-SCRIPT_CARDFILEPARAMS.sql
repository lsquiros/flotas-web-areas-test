--El campo Nombre de EMPRESA debe tener m�ximo 27 caracteres - desde & hasta *
--El campo Nombre de EMPRESA debe tener un espacio en blanco iniciando del &

--El campo Nombre de VEHICULO o Nombre de la PERSONA debe tener m�ximo 27 caracteres - desde # hasta &
--El campo Nombre de VEHICULO o Nombre de la PERSONA debe tener dos espacios en blanco iniciando del signo #

--En el campo Nombre de VEHICULO o Nombre de la PERSONA no se permiten los siguientes s�mbolos: #$%&/.� y tildes.
--En el campo Nombre de EMPRESA no se permiten los siguientes s�mbolos: #$%&/.� y tildes.

--Ell nombre del usuario que gener� las tarjetas se debe visualizar en el nombre del archivo TXT.

--En caso de la empresa DOS PINOS en el campo Nombre de VEHICULO o Nombre de la PERSONA siempre debe venir el Nombre del VEHICULO acompa�ado con una  �V� may�scula al inicio del nombre


ALTER TABLE [Control].[CardFileParams] ADD [Description] VARCHAR(128) 
GO


UPDATE [Control].[CardFileParams]  
SET  CardFileParamLenght = 25
	,Separator = '#  '
	,[Description] = '25 mas dos espacios despues de # en total 27'
WHERE [CardFileParamName] = 'CardHolderNameWithSpaces'

UPDATE [Control].[CardFileParams] 
SET CardFileParamLenght	= 26
	,Separator = '& '
	,[Description] = '26 mas un espacio despues de & en Total 27'
WHERE [CardFileParamName] = 'CardHolderCompany'

GO

SELECT * FROM [Control].[CardFileParams]  
