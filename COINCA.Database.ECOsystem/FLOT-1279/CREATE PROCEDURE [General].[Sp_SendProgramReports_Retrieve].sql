-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 28/02/2017 
-- Description:	Retrieve SendProgramReports Information
-- ================================================================================================
-- Marco Cabrera - 01-03-2017 - Add a where to retrieve 
-- Henry Retana - 10-05-2017 - Use Send Date as flag to get the report informtion
-- Henry Retana - 06-06-2017 - Use Customer TimeZone to get the dates
-- Marco Cabrera - 12-07-2017 - Add DriverId to the final result set
-- Henry Retana - 01/09/2017 - Add the validation for the day of the month
-- Henry Retana - 04/09/2017 - Add validations to reports by partner
-- Henry Retana - 06/11/2017 - Validates start and end date to 00:00:00 hours
-- Henry Retana - 09/11/2017 - Add VPOSBill for the download job
-- Henry Retana - 28/11/2017 - Add Is Alert for amount by Service Stations
-- Mari Jiménez - JAN/25/2019 - Add user email (id, name, email) and module
-- Mari Jiménez - APR/12/2019 - Include customer reports when @pDownloadJob = 1
-- Stefano Quirós - 23/12/2019 - Add Logic to retrieve the Reports By Partners Too
-- JuanK Santamaria - 02/06/2020 - Add Logic to retrieve the Reports By all users if userid is -1
-- ================================================================================================
ALTER PROCEDURE [General].[Sp_SendProgramReports_Retrieve] --1
(
	@pDownloadJob BIT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	IF @pDownloadJob IS NULL SET @pDownloadJob = 1
	DECLARE @lElapse INT

	--If the send date is not todays, set date as null
	UPDATE [General].[SendProgramReports]
	SET [SendDate] = NULL
	WHERE DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, -6, [SendDate])), 0)) < DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, -6, GETDATE())), 0))

	DECLARE @lId INT
	DECLARE @lReportTable TABLE
	(
		 [Id] INT
		,[CustomerId] INT
		,[PartnerId] INT
		,[Emails] VARCHAR(MAX)
		,[UserEmail] NVARCHAR(256)
		,[StartDate] DATETIME
		,[EndDate] DATETIME
		,[Elapse] INT
		,[Days] VARCHAR(30)
		,[VehicleId] INT 
		,[CostCenterId] INT
		,[CostCenterName] VARCHAR(300)
		,[DriverId] INT
		,[ReportName] VARCHAR(300)
		,[DownloadName] VARCHAR(300)
		,[UserId] INT
		,[TerminalDetail] BIT
		,[Send] BIT
		,[SendDate] DATETIME
		,[VPOSBill] INT
		,[ReportId] INT NULL
	) 

	INSERT INTO @lReportTable
	SELECT   s.[Id]
			,s.[CustomerId]
			,s.[PartnerId]
			,s.[Emails] 
			,NULL   
			,s.[StartDate]
			,s.[EndDate]
			,s.[Elapse]
			,s.[Days]
			,s.[VehicleId]
			,CASE s.[CostCenterId] WHEN -1 THEN NULL ELSE s.[CostCenterId] END
			,CASE s.[CostCenterId] WHEN -1 THEN NULL ELSE vc.[Name] END
			,s.DriverId
			,r.[ReportName] 
			,r.[DownloadName]       
			,s.[UserId]
			,s.[TerminalDetail]
			,0
			,s.[SendDate]
			,s.[VPOSBill]
			,r.[Id]
	FROM [General].[Reports] r
	INNER JOIN [General].[SendProgramReports] s
		ON r.[Id] = s.[ReportId] 
		AND r.[Module] IS NULL
	LEFT OUTER JOIN [General].[VehicleCostCenters] vc
		ON s.[CostCenterId] = vc.[CostCenterId]
	WHERE s.[Active] = 1
	AND s.[IsDownloaded] = 1
	AND (s.[Emails] IS NOT NULL 
		 OR s.[Emails] = '')


	If @pDownloadJob = 1
	BEGIN
		INSERT INTO @lReportTable
		SELECT   s.[Id]
				,s.[CustomerId]
				,s.[PartnerId]
				,s.[Emails]
				,asp.[Email] [UserEmail]
				,s.[StartDate]
				,s.[EndDate]
				,s.[Elapse]
				,s.[Days]
				,NULL
				,NULL
				,''
				,NULL
				,r.[ReportName] 
				,r.[DownloadName]       
				,s.[UserId]
				,0
				,0
				,s.[SendDate]
				,NULL
				,r.Id
		FROM [General].[SendProgramReports] s
		INNER JOIN [General].[Reports] r
			ON r.[Id] = s.[ReportId] OR
			  (s.[ReportId] = -1 AND s.[Module] = r.[Module])
		LEFT JOIN [General].[Users] u
			ON (s.[UserId] = u.[UserId] OR 
			   (s.[UserId] = - 1 AND u.[UserId] IN (SELECT a.[UserId]
													FROM [General].[Users] a
													INNER JOIN [General].[CustomerUsers] cu
														ON a.[UserId] = cu.[UserId]	
													WHERE cu.[CustomerId] = s.[CustomerId] AND
														  a.[IsDeleted] = 0 AND a.[IsActive] = 1)
													))
		LEFT JOIN [dbo].[AspNetUsers] asp
			ON u.[AspNetUserId] = asp.[Id]
		WHERE s.[Active] = 1 
			  AND s.[IsDownloaded] = 1 
			  AND((s.[CustomerId] IS NOT NULL 
		           AND r.[IsCustomer] = 1) OR (s.[PartnerId] IS NOT NULL 
											   AND r.[IsPartner] = 1))
		AND r.[SpName] IS NOT NULL AND
		(r.[IsCustom] IS NULL OR r.[IsCustom] = 0) AND
		(s.[Module] IS NOT NULL OR r.[Module] IS NOT NULL) AND
		(s.[Emails] IS NOT NULL OR s.[UserId] IS NOT NULL)
	END

	--IF VPOS BILL ALREADY SEND, REMOVE IT FROM THE RETRIEVE
	DELETE FROM @lReportTable
	WHERE [VPOSBill] IN (
							SELECT v.[Id]
							FROM @lReportTable r
							INNER JOIN [General].[VPOSBillEmailToSend] v
							ON r.[VPOSBill] = v.[Id]
							WHERE v.[Send] = 1
						)

	SELECT @lId = MIN([Id]) FROM @lReportTable 

	WHILE @lId IS NOT NULL
	BEGIN
		IF (SELECT TOP 1 [ReportName] FROM @lReportTable WHERE [Id] = @lId) <> 'TransactionsOfflineReport'
		BEGIN
			DECLARE @lSendDate DATETIME

			SELECT @lElapse = [Elapse]
				,@lSendDate = [SendDate]
			FROM @lReportTable
			WHERE [Id] = @lId

			IF @lElapse = 30 AND (@lSendDate IS NULL OR DATEADD(HOUR, -6, @lSendDate) > DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, -6, GETDATE())), 0)))
			BEGIN
				UPDATE @lReportTable
				SET [Send] = 1,
					[StartDate] = DATEADD(MONTH, -1, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)),
					[EndDate] = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)
				WHERE [Id] = @lId 
				AND DATEPART(D, GETDATE()) IN (ISNULL([Days], 1))
			END

			IF @lElapse = 7 AND (@lSendDate IS NULL OR DATEADD(HOUR, -6, @lSendDate) > DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, -6, GETDATE())), 0)))
			BEGIN
				UPDATE @lReportTable
				SET [Send] = 1,
					[StartDate] = DATEADD(DAY, -@lElapse, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)),
					[EndDate] = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)
				WHERE [Id] = @lId
				AND DATEPART(DW, GETDATE()) IN (SELECT [items] 
												FROM [dbo].[Fn_Split]([Days], ',') 
												WHERE ([items] IS NOT NULL 
													   OR [items] <> '')) 
			END
  
			IF @lElapse = 1 AND (@lSendDate IS NULL OR DATEADD(HOUR, -6, @lSendDate) > DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, -6, GETDATE())), 0)))
			BEGIN
				UPDATE @lReportTable
				SET [Send] = 1,
					[StartDate] = DATEADD(DAY, -@lElapse, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)),
					[EndDate] = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)
				WHERE [Id] = @lId
			END
		END
		ELSE 
		BEGIN 
			UPDATE @lReportTable
			SET [Send] = 1
			WHERE [Id] = @lId
			AND (SELECT [IsDownloaded] 
				 FROM [General].[SendProgramReports]
				 WHERE [Id] = @lId) = 0
		END

		SELECT @lId = MIN([Id]) 
		FROM @lReportTable 
		WHERE [Id] > @lId
	END

	IF @pDownloadJob = 0 
	BEGIN 
		SELECT s.[Id]
			,s.[CustomerId]
			,s.[PartnerId]
			,c.[Name] [CustomerName]
			,s.[Emails]
			,CASE WHEN s.[Emails] IS NULL 
				  THEN asp.[Email] 
			 ELSE NULL END [UserEmail]
			,CASE WHEN co.[TimeZone] IS NULL THEN s.[StartDate]
											ELSE DATEADD(HH, co.[TimeZone], s.[StartDate]) END [StartDate]
			,CASE WHEN co.[TimeZone] IS NULL THEN s.[EndDate] 
						ELSE DATEADD(HH, co.[TimeZone], s.[EndDate]) END [EndDate]
			,s.[VehicleId]     
			,v.[Name] [VehicleName]
			,s.[CostCenterId]
			,s.[CostCenterName]
			,s.[ReportName] 
			,s.[DownloadName]
			,s.[TerminalDetail]
			,s.[UserId]
			,s.[DriverId]
			,c.[UnitOfCapacityId]
			,d.[Name] [UnitOfCapacityName]
			,s.[VPOSBill]
		FROM @lReportTable s
		LEFT OUTER JOIN [General].[Vehicles] v
		ON s.[VehicleId] = v.[VehicleId]
		LEFT OUTER JOIN [General].[Customers] c
		ON s.[CustomerId] = c.[CustomerId]
		LEFT OUTER JOIN [General].[Types] d
			ON d.[TypeId] = c.[UnitOfCapacityId]
		LEFT OUTER JOIN [General].[Countries] co 
			ON c.[CountryId] = co.[CountryId]
		LEFT OUTER JOIN [General].[Users] u 
			ON u.[UserId] = s.[UserId]
		LEFT OUTER JOIN [AspNetUsers] asp 
			ON asp.[Id] = u.[AspNetUserId]
		WHERE  s.[Send] = 1
	END 
	ELSE 
	BEGIN 

		SELECT s.[Id]
			,s.[CustomerId]
			,s.[PartnerId]
			,c.[Name] [CustomerName]
			,s.[Emails]
			,CASE WHEN s.[Emails] IS NULL 
				  THEN asp.[Email] 
			 ELSE NULL END [UserEmail]
			,(SELECT Message FROM General.[Values] WHERE TypeId = (SELECT [Value] FROM GeneralParameters WHERE ParameterID = 'EMAIL_ATTACHMENT_TYPE_ID')) [Message]
			,CASE WHEN co.[TimeZone] IS NULL THEN ISNULL(s.[StartDate], CASE WHEN s.[Elapse] = 30 THEN DATEADD(MONTH, -1, DATEDIFF(DAY, 0, GETDATE()))
						ELSE DATEADD(DAY, - s.[Elapse], DATEDIFF(DAY, 0, GETDATE())) END) 
					ELSE DATEADD(HH, co.[TimeZone], ISNULL(s.[StartDate], CASE WHEN s.[Elapse] = 30 THEN DATEADD(MONTH, -1, DATEDIFF(DAY, 0, GETDATE()))
													ELSE DATEADD(DAY, - s.[Elapse], DATEDIFF(DAY, 0, GETDATE())) END) ) END [StartDate]
			,CASE WHEN co.[TimeZone] IS NULL THEN ISNULL(s.[EndDate], DATEDIFF(DAY, 0, GETDATE())) 
											ELSE DATEADD(HH, co.[TimeZone], ISNULL(s.[EndDate], DATEDIFF(DAY, 0, GETDATE()))) END  [EndDate]
			,s.[VehicleId]     
			,v.[Name] [VehicleName]
			,s.[CostCenterId]
			,s.[CostCenterName]
			,s.[ReportName] 
			,s.[DownloadName]
			,s.[TerminalDetail]
			,s.[UserId]
			,s.[DriverId]
			,s.[ReportId]
		INTO #ReportToSend
		FROM @lReportTable s
		LEFT OUTER JOIN [General].[Vehicles] v
		ON s.[VehicleId] = v.[VehicleId]
		LEFT OUTER JOIN [General].[Customers] c
		ON s.[CustomerId] = c.[CustomerId]
		LEFT OUTER JOIN [General].[Countries] co 
		ON c.[CountryId] = co.[CountryId]
		LEFT OUTER JOIN [General].[Users] u 
			ON u.[UserId] = s.[UserId]
		LEFT OUTER JOIN [AspNetUsers] asp 
			ON asp.[Id] = u.[AspNetUserId]
		WHERE  s.[Send] = 0
	END 

	SELECT 
		 [Id]
		,[CustomerId]
		,[PartnerId]
		,[CustomerName]
		,[Emails]
		,[UserEmail]
		,[Message]
		,[StartDate]
		,[EndDate]
		,[VehicleId]
		,[VehicleName]
		,[CostCenterId]
		,[CostCenterName]
		,[ReportName]
		,[DownloadName]
		,[TerminalDetail]
		,[UserId]
		,[DriverId]
		,[ReportId]
	FROM #ReportToSend WHERE UserId != -1
	-----------------------------------------
	UNION ALL  /*Emails with users For All Users*/
	-----------------------------------------
	SELECT 
		 rs.[Id]
		,rs.[CustomerId]
		,rs.[PartnerId]
		,rs.[CustomerName]
		,rs.[Emails]			
		,[UserEmail]		= b.Email
		,rs.[Message]
		,rs.[StartDate]
		,rs.[EndDate]
		,rs.[VehicleId]
		,rs.[VehicleName]
		,rs.[CostCenterId]
		,rs.[CostCenterName]
		,rs.[ReportName]
		,rs.[DownloadName]
		,rs.[TerminalDetail]
		,rs.[UserId]
		,rs.[DriverId]
		,[ReportId]
	FROM #ReportToSend rs	
	INNER JOIN [General].[CustomerUsers] e WITH(NOLOCK)
		ON rs.CustomerId = e.CustomerId
	INNER JOIN [General].[Users]		us WITH(NOLOCK)
		ON e.[UserId] = us.[UserId]
	INNER JOIN [dbo].[AspNetUsers]		b WITH(NOLOCK)
		ON us.[AspNetUserId] = b.[Id]
	WHERE us.[isActive] = 1
	AND us.[isDeleted] = 0
	AND rs.[UserId] = -1

	DROP TABLE #ReportToSend

	SET NOCOUNT OFF
END


go

--exec [General].[Sp_SendProgramReports_Retrieve]

