-- ================================================================================================
-- Author:		Maria de los Angeles Jimenez
-- Create date: JUL/04/2019
-- Description:	Insert partner menu option > survey report
-- ================================================================================================

DECLARE @NewPermId1 NVARCHAR(128) = NEWID()
	   ,@NewPermId2 NVARCHAR(128) = NEWID()
	   ,@NewPermId3 NVARCHAR(128) = NEWID()
	   ,@lParentId INT
	   ,@pParentName VARCHAR(25) = 'Reportes'
	   ,@pHeader INT = 14
	   ,@pOrder INT = 112

--Insert permissions
INSERT INTO [dbo].[AspNetPermissions]
VALUES
(
	@NewPermId1
	,'View_PartnerSurveyReport'
	,'SurveyReport'
	,'Index'
	,''
	,GETDATE()
)

INSERT INTO [dbo].[AspNetPermissions]
VALUES
(
	@NewPermId2
	,'ExcelReportDownload_PartnerSurveyReport'
	,'SurveyReport'
	,'ExcelReportDownload'
	,''
	,GETDATE()
)

INSERT INTO [dbo].[AspNetPermissions]
VALUES
(
	@NewPermId3
	,'DonwloadFile_PartnerSurveyReport'
	,'SurveyReport'
	,'DonwloadFile'
	,''
	,GETDATE()
)

--Search partner report submenu
SELECT @lParentId = [Id] FROM [dbo].[AspMenus] WHERE [Name] = @pParentName AND [Header] = @pHeader

--Reorder submenu options
UPDATE [dbo].[AspMenus] SET [Order] = [Order] - 3 WHERE [Parent] = @lParentId AND [Order] >= 110 AND [Order] < 115

--Insert menu option
INSERT INTO [dbo].[AspMenus]
VALUES
(
	'Resultados de las Encuestas',
	'',
	@NewPermId1,
	@lParentId,
	@pHeader,
	@pOrder,
	GETDATE()
)

DECLARE @lMenuOption INT = SCOPE_IDENTITY()

CREATE TABLE #Roles (
	[Id] INT IDENTITY,
	[RoleId] VARCHAR(MAX)
)

--PARTNER_ADMIN: Socio Administrador: A0465644-860E-4B8E-8779-7719B65D5E6E
INSERT INTO #Roles
SELECT [Id] FROM [AspNetRoles] 
WHERE ([Name] = 'PARTNER_ADMIN' OR [Parent] = 'A0465644-860E-4B8E-8779-7719B65D5E6E')
AND [Active] = 1

DECLARE @lRoleId VARCHAR(MAX)
	   ,@lId INT

WHILE EXISTS(SELECT * FROM #Roles)
BEGIN
	SELECT @lId = MIN([Id]) 
	FROM #Roles

	SELECT @lRoleId = [RoleId] 
	FROM #Roles 
	WHERE [Id] = @lId

	--Insert the menu and the permission to all 'Super Usuario Cliente' created on the platform
	INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES (@lRoleId, @lMenuOption) 

	INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES (@lRoleId, @NewPermId1) 

	INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES (@lRoleId, @NewPermId2) 

	INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES (@lRoleId, @NewPermId3) 

	DELETE FROM #Roles WHERE [RoleId] = @lRoleId
END

DROP TABLE #Roles