IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[SP_PartnerSurveyReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[SP_PartnerSurveyReport_Retrieve]
GO

-- ================================================================================================
-- Author:		Maria de los Angeles Jimenez 
-- Create date: JUL/09/2019
-- Description:	Retrieve the partner survey report
-- =======================================================================================================================================

CREATE PROCEDURE [General].[SP_PartnerSurveyReport_Retrieve]
(
	@pPartnerId INT = NULL,
	@pSurveyId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	IF @pSurveyId = -1
		SET @pSurveyId = NULL

	SELECT	@pSurveyId [SurveySelected]
			,sv.[Id] [SurveyId]
			,sv.[Name] [SurveyName]
			,sv.[Active] [SurveyActive]
			,sq.[Id] [QuestionId]
			,sq.[Text] [Question]
			,sq.[Order] [QuestionOrder]
			,ISNULL(sa.[Text], sc.[Text]) [Answer]
			,sc.[InsertDate] [Date]
			,u.[Name] [EncryptedUsername]
			,c.[Name] [EncryptedCustomer]
			,p.[Name] [PartnerName]
			,CASE	WHEN t.[Description] = 'Eliminar Tarjeta de Crédito'
					THEN COUNT(cc.[CreditCardId])
					ELSE 0
			END [Event]
	FROM [General].[Partners] p
	INNER JOIN [General].[PartnerSurvey] sv
		ON sv.[PartnerId] = p.[PartnerId]
	LEFT JOIN [General].[PartnerSurveyQuestion] sq
		ON sq.[PartnerSurveyId] = sv.[Id]
	LEFT JOIN [General].[PartnerSurveyAnswerByCustomer] sc
		ON sc.[PartnerSurveyQuestionId] = sq.[Id]
	LEFT JOIN [General].[PartnerSurveyAnswer] sa
		ON sc.[PartnerSurveyAnswerId] = sa.[Id]
	LEFT JOIN [General].[Users] u
		ON u.[UserId] = sc.[InsertUserId]
	LEFT JOIN [General].[Customers] c
		ON c.[CustomerId] = sc.[CustomerId]
	LEFT JOIN [General].[PartnerSurveyTargets] t
		ON t.[Id] = sv.[TargetId]
	LEFT JOIN [Control].[CreditCardHx] cc
		ON cc.[CustomerId] = sc.[CustomerId]
		AND cc.[StatusId] = 9 --Close card
		AND cc.[ModifyDate] BETWEEN DATEADD(MINUTE,-30, sc.[InsertDate]) AND sc.[InsertDate]
	WHERE (sv.[Id] = @pSurveyId OR @pSurveyId IS NULL) 
	AND sv.[PartnerId] = @pPartnerId
	AND (sv.[Delete] IS NULL OR sv.[Delete] = 0)
	AND (sq.[Delete] IS NULL OR sq.[Delete] = 0)
	GROUP BY sc.[Id], sv.[Id], sv.[Name], sv.[Active], sq.[Id], sq.[Text], sq.[Order], sa.[Text], sc.[Text], sc.[InsertDate], u.[Name], c.[Name], p.[Name], t.[Description]

	SET NOCOUNT OFF
END

