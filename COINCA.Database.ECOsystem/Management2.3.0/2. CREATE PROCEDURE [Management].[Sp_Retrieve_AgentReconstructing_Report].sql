USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_AgentReconstructing_Report]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_AgentReconstructing_Report] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 19/09/2018
-- Description:	Agent Reconstructing Retrieve
-- Modify by:   Marjorie Garbanzo - 08/10/2018 - Add MovementName and EventKey
-- Modify by:   Marjorie Garbanzo - 08/10/2018 - Add Stop Event in the report
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_AgentReconstructing_Report] 
(	
	@pUserId INT,
	@pStartDate DATETIME,
	@pEndDate DATETIME = NULL
)
AS
BEGIN	
	SET NOCOUNT ON

	DECLARE @lBeginJourneyEvent INT,
			@lStopEvent INT,
			@lLocation INT,
			@lLocationName VARCHAR(200) 

	--Get Begin Journey 
	SELECT @lBeginJourneyEvent = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'

	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'
	
	SELECT @lStopEvent = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'AGENT_STOP'

	SELECT @lLocationName = p.[MovementName]
	FROM [Management].[Parameters] p 
	INNER JOIN [Management].[UserEvent] u
		ON p.[CustomerId] = u.[CustomerId]
	WHERE u.[UserId] = @pUserId

	IF @pEndDate IS NULL SET @pEndDate = DATEADD(DAY, 1, @pStartDate)

	SELECT  u.[Id],
			u.[UserId],
			u.[EventDetailId],
			ed.[Name] [EventDetailName],
			u.[EventTypeId],
			c.[Name] [EventName] ,
			c.[Parent],
			u.[CommerceId],
			u.[Latitude],
			u.[Longitude],
			u.[BatteryLevel],
			u.[Speed],
			u.[EventDate],
			CAST(u.[Lapse] AS VARCHAR(10)) [Lapse]
	INTO #REPORTS
	FROM [Management].[UserEvent] u
	INNER JOIN [Management].[CatalogDetail] c
		ON u.[EventTypeId] = c.[Id]
	LEFT JOIN [Management].[EventDetail] ed
		ON u.[EventDetailId] = ed.[Id]
	WHERE u.[UserId] = @pUserId 
	AND u.[EventDate] BETWEEN @pStartDate AND @pEndDate
	ORDER BY u.[EventDate];

	WITH Temp AS
	(
		SELECT TOP 1 * 
		FROM #REPORTS
		UNION
		SELECT r.*
		FROM #REPORTS r,
				#REPORTS r2
		WHERE r.[Id] = (r2.[Id] + 1) 
		AND r.[EventTypeId] <> r2.[EventTypeId]
	)
	
	SELECT  t.[Id],
			t.[UserId],
			t.[EventTypeId],
			t.[EventDetailName],
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN ISNULL(@lLocationName, t.[EventName])
				 ELSE ISNULL(t.[EventDetailName], t.[EventName]) 
			END [EventName],
			c.[Name] [CommerceName],
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN ''
				 ELSE ISNULL(c.[Address], [Efficiency].[Fn_GetAddressByLatLon_Retrieve](t.[Longitude], t.[Latitude], NULL)) 
			END [Address],
			t.[Parent],
			
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN ''
				 ELSE t.[Latitude] 
			END [Latitude],
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN ''
				 ELSE t.[Longitude] 
			END [Longitude],
			t.[BatteryLevel],
			t.[EventDate],
			t.[EventDate] [StartDate], 
			(SELECT TOP 1 r.[EventDate] 
			 FROM temp r
			 WHERE r.[Id] > t.[Id]
			 ORDER BY r.[Id]) [EndDate],

			 CASE WHEN t.[EventTypeId] = @lStopEvent
				THEN CONCAT(ISNULL ( CAST((t.[Lapse] % (24 * 60 * 60)) / (60 * 60) AS VARCHAR(2)), '00'),':',
							ISNULL ( RIGHT('0' + CAST(((t.[Lapse] % (24 * 60 * 60)) % (60 * 60)) / 60 AS VARCHAR(2)), 2), '00'),':',
							ISNULL ( RIGHT('0' + CAST(((t.[Lapse] % (24 * 60 * 60)) % (60 * 60)) % 60 AS VARCHAR(2)), 2), '00'))

				ELSE CONCAT(ISNULL ( RIGHT('0' + CAST(DATEDIFF(S, t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																					FROM temp r
																					WHERE r.[Id] > t.[Id]
																					ORDER BY r.[Id])) / 3600 AS VARCHAR(2)), 2), '00'),':',
		  					ISNULL ( RIGHT('0' + CAST(DATEDIFF(S, t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																					 FROM temp r
																					 WHERE r.[Id] > t.[Id]
																					 ORDER BY r.[Id])) % 3600 / 60 AS VARCHAR(2)), 2), '00'),':',
							ISNULL ( RIGHT('0' + CAST(DATEDIFF(S, t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																					 FROM temp r
																					 WHERE r.[Id] > t.[Id]
																					 ORDER BY r.[Id])) % 60 AS VARCHAR(2)), 2), '00')) 
			END [Lapse],
			
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN [Management].[Fn_GetDistanceFromPointsByDates] (@pUserId,  t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																								 FROM temp r
																								 WHERE r.[Id] > t.[Id]
																								 ORDER BY r.[Id]))
				 WHEN t.[EventTypeId] = @lStopEvent
				 THEN [Management].[Fn_GetDistanceFromPointsByDates] (@pUserId,  t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																								 FROM temp r
																								 WHERE r.[Id] > t.[Id]
																								 ORDER BY r.[Id]))																	
				 ELSE 0 
			END [Distance],
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN (SELECT AVG(R.[Speed])
					   FROM temp r
					   WHERE r.[Id] > t.[Id])
				 WHEN t.[EventTypeId] = @lStopEvent
				 THEN t.[Speed]
				 ELSE 0 
			END [Speed],
			[Management].[Fn_GetEventColor] (t.[EventTypeId], @lStopEvent) [RowColor]
	FROM temp t
	LEFT JOIN [General].[Commerces] c
		ON t.[CommerceId] = c.[Id]
	WHERE [Parent] <> @lBeginJourneyEvent	
	AND [EventTypeId] <> @lBeginJourneyEvent 
	UNION 
	SELECT  rt.[Id],
			rt.[UserId],	
			rt.[EventTypeId],
			rt.[EventDetailName],
			ISNULL(rt.[EventDetailName], rt.[EventName]) [EventName],
			cc.[Name] [CommerceName],
			ISNULL(cc.[Address], [Efficiency].[Fn_GetAddressByLatLon_Retrieve](rt.[Longitude], rt.[Latitude], NULL)) [Address],
			rt.[Parent],
			rt.[Latitude],
			rt.[Longitude],
			rt.[BatteryLevel],			
			rt.[EventDate],
			rt.[EventDate] [StartDate],
			DATEADD(SECOND, 1, rt.[EventDate]) [EndDate],
			
			CASE WHEN rt.[EventTypeId]<> @lBeginJourneyEvent
				THEN  CONCAT(ISNULL ( RIGHT('0' + CAST(DATEDIFF(S, (SELECT TOP 1 r.[EventDate] 
																		FROM temp r
																		WHERE r.[EventTypeId] = @lBeginJourneyEvent
																		ORDER BY r.[Id]), rt.[EventDate]) / 3600 AS VARCHAR(2)), 2), '00'),':',
		  					ISNULL ( RIGHT('0' + CAST(DATEDIFF(S, (SELECT TOP 1 r.[EventDate] 
																		FROM temp r
																		WHERE r.[EventTypeId] = @lBeginJourneyEvent
																		ORDER BY r.[Id]), rt.[EventDate]) % 3600 / 60 AS VARCHAR(2)), 2), '00'),':',
							ISNULL ( RIGHT('0' + CAST(DATEDIFF(S, (SELECT TOP 1 r.[EventDate] 
																		FROM temp r
																		WHERE r.[EventTypeId] = @lBeginJourneyEvent
																		ORDER BY r.[Id]), rt.[EventDate]) % 60 AS VARCHAR(2)), 2), '00')) 
				ELSE '00:00:00' 
			END [Lapse],
			NULL [Distance],
			0 [Speed],
			NULL [RowColor]
	FROM #REPORTS rt
	LEFT JOIN [General].[Commerces] cc
		ON rt.[CommerceId] = cc.[Id]
	WHERE ([EventTypeId] = @lBeginJourneyEvent OR [Parent] = @lBeginJourneyEvent)
	ORDER BY [EventDate] 

	DROP TABLE #REPORTS

	SET NOCOUNT OFF
END