USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_ManagementAgentsReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_ManagementAgentsReport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Author:  Henry Retana
-- Create date: 13/10/2017  
-- Description: Retrieve Management Events
-- ================================================================================================  

CREATE PROCEDURE [Management].[Sp_Retrieve_ManagementAgentsReport]
(  
	 @pUserId INT = NULL
	,@pCustomerId INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL	
)  
AS  
BEGIN
	SET NOCOUNT ON
	
	DECLARE	 @lCount INT
			,@lUsertCount INT
			,@lTempBeginDate DATETIME
			,@lTempFinishDate DATETIME
			,@lEventTypeId INT
			,@lLatitude FLOAT
			,@lLongitude FLOAT
			,@lTempLatitude FLOAT
			,@lTempLongitude FLOAT
			,@lTempDateTime DATETIME
			,@lEventDate DATETIME
			,@lMovementTimeSTR VARCHAR(50)
			,@lStopTime VARCHAR(50)
			,@lHours VARCHAR(5)
			,@lMinutes VARCHAR(5)
			,@lSeconds VARCHAR(5)
			,@lWorkingGlobal INT = 0
			,@lWorkingTotalSeconds INT
			,@lProductiveTotalSeconds INT
			,@lAVGBeginJourney VARCHAR(100)
			,@lAVGFinishJourney VARCHAR(100)
			,@lBeginLunchDateTime VARCHAR(100)
			,@lFinishLunchDateTime VARCHAR(100)
			,@lBeginManageDateTime VARCHAR(100)
			,@lFinishManageDateTime VARCHAR(100)
			,@lWorkingTime VARCHAR(50)
			,@lRealLunchTime VARCHAR(50)
			,@lProductiveTime VARCHAR(50)
			,@lPorcProductiveTime VARCHAR(10)
			,@lUserName VARCHAR(500)
			,@lFinalUserId INT
			---EVENTS
			,@lLogIn INT
			,@lLogOut INT
			,@lStartVisit INT
			,@lEndVisit INT
			,@lLocation INT

	--GET THE IDS FOR THE EVENTS--
	SELECT @lLogout = c.[Id],
		   @lLogin = c.[Parent]
	FROM [Management].[CatalogDetail] c
	INNER JOIN [dbo].[GeneralParameters] p
		ON c.[Parent] = p.[Value]
	WHERE p.[ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'	

	SELECT @lEndVisit= c.[Id],
		   @lStartVisit = c.[Parent]
	FROM [Management].[CatalogDetail] c
	INNER JOIN [dbo].[GeneralParameters] p
		ON c.[Parent] = p.[Value]
	WHERE p.[ParameterID] = 'CUSTOMERS_CLIENT_BEGIN_VISIT'
	
	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'
	--**************************--
	
	CREATE TABLE #EVENTS 
	(
		 [Id] INT IDENTITY
		,[EventTypeId] INT
		,[Latitude] FLOAT
		,[Longitude] FLOAT
		,[EventDate] DATETIME
		,[Name] VARCHAR(300)		
	)

	CREATE TABLE #EVENTSDAY 
	(
		 [Id] INT
		,[EventTypeId] INT
		,[Latitude] FLOAT
		,[Longitude] FLOAT
		,[EventDate] DATETIME
		,[Name] VARCHAR(300)		
	)

	CREATE TABLE #RESULT 
	(
		 [AVGBeginJourney] VARCHAR(100)
		,[AVGFinishJourney] VARCHAR(100)
		,[WorkingTime] VARCHAR(50)
		,[BeginLunchDateTime] VARCHAR(100)
		,[FinishLunchDateTime] VARCHAR(100)
		,[RealLunchTime] VARCHAR(50)
		,[BeginManageDateTime] VARCHAR(100)
		,[FinishManageDateTime] VARCHAR(100)
		,[ProductiveTime] VARCHAR(50)
		,[PorcProductiveTime] VARCHAR(10)
		,[Kilometers] DECIMAL(16, 3)
		,[MovementTime] VARCHAR(50)
		,[StopTime] VARCHAR(50)
		,[Date] VARCHAR(50)
		,[EventDate] DATETIME
		,[UserId] INT
		,[AgentName] VARCHAR(500)
	)

	CREATE TABLE #USERS
	(
		 [Id] INT IDENTITY
		,[UserId] INT
		,[Name] VARCHAR(500)
	)

	INSERT INTO #USERS
	SELECT u.[UserId]
		  ,u.[Name]
	FROM [General].[DriversUsers] du
	INNER JOIN [General].[Users] u 
		ON u.[UserId] = du.[UserId]
	WHERE [CustomerId] = @pCustomerId
	AND (@pUserId IS NULL OR u.[UserId] = @pUserId)
	AND u.[IsAgent] = 1

	SELECT @lUsertCount = MIN([Id])
	FROM #USERS

	WHILE @lUsertCount IS NOT NULL
	BEGIN

		DECLARE  @lKilometersAVG DECIMAL(16, 2) = 0
				,@lTotalMovementTime INT = 0
				,@lTotalStopTime INT = 0

		SELECT @pUserId = [UserId],
			   @lFinalUserId = [UserId],
			   @lUserName = [Name] 			
		FROM #USERS
		WHERE [Id] = @lUsertCount

		INSERT INTO #EVENTS
		SELECT	 u.[EventTypeId]
				,u.[Latitude]
				,u.[Longitude]
				,u.[EventDate]
				,ISNULL(ed.[Name], cd.[Name])
		FROM [Management].[UserEvent] u
		INNER JOIN [Management].[CatalogDetail] cd
			ON u.[EventTypeId] = cd.[Id]
		LEFT JOIN [Management].[EventDetail] ed 
			ON ed.[Id] = u.[EventDetailId]
		WHERE u.[UserId] = @pUserId
		AND (u.[EventDate] BETWEEN @pStartDate AND @pEndDate)
		ORDER BY [EventDate] ASC

		IF EXISTS (SELECT * FROM #EVENTS)
		BEGIN
			SET @lTempBeginDate = @pStartDate
			SET @lTempFinishDate = @pEndDate

			WHILE @lTempBeginDate <= @lTempFinishDate
			BEGIN
				DECLARE @lKilometers DECIMAL(16, 2) = 0
					   ,@lMovementTime INT = 0

				INSERT INTO #EVENTSDAY
				SELECT [Id]
					  ,[EventTypeId]
					  ,[Latitude]
					  ,[Longitude]
					  ,[EventDate]
					  ,[Name] 
				FROM #EVENTS
				WHERE DATEPART(DAY, [EventDate]) = DATEPART(DAY, @lTempBeginDate)
				AND DATEPART(MONTH, [EventDate]) = DATEPART(MONTH, @lTempBeginDate)
				
				SELECT @lCount = MIN([Id])
				FROM #EVENTSDAY

				--INICIO WHILE DE KILOMETROS Y MOVEMENTTIME  
				WHILE @lCount IS NOT NULL
				BEGIN
					SELECT @lEventTypeId = [EventTypeId]
						  ,@lLatitude = [Latitude]
						  ,@lLongitude = [Longitude]
						  ,@lEventDate = [EventDate]
					FROM #EVENTSDAY
					WHERE Id = @lCount

					IF @lEventTypeId = @lEndVisit OR @lEventTypeId = @lLogin
					BEGIN
						SET @lTempLatitude = @lLatitude
						SET @lTempLongitude = @lLongitude
						SET @lTempDateTime = @lEventDate
					END

					IF (@lEventTypeId = @lStartVisit OR @lEventTypeId = @lLogout)
					   AND @lTempLatitude IS NOT NULL
					   AND @lTempLongitude IS NOT NULL
					   AND @lTempDateTime IS NOT NULL
					BEGIN
						DECLARE @lg GEOGRAPHY
							   ,@lh GEOGRAPHY

						SET @lg = GEOGRAPHY::STGeomFromText('POINT(' + CAST(@lTempLatitude AS VARCHAR(20)) + ' ' + CAST(@lTempLongitude AS VARCHAR(20)) + ')', 4326)
						SET @lh = GEOGRAPHY::STGeomFromText('POINT(' + CAST(@lLatitude AS VARCHAR(20)) + ' ' + CAST(@lLongitude AS VARCHAR(20)) + ')', 4326)

						SELECT @lKilometers = @lKilometers + @lg.STDistance(@lh)
					END

					SELECT @lCount = MIN([Id])
					FROM #EVENTSDAY
					WHERE [Id] > @lCount
				END

				--FINALIZA WHILE DEL COUNT   
				IF EXISTS (SELECT 1 FROM #EVENTSDAY)
				BEGIN
					--LOG IN
					SELECT TOP 1 @lAVGBeginJourney = CAST(RIGHT([EventDate], 8) AS VARCHAR(100))
					FROM #EVENTSDAY
					WHERE [EventTypeId] = @lLogIn

					SELECT TOP 1 @lAVGFinishJourney = CAST(RIGHT([EventDate], 8) AS VARCHAR(100))
					FROM #EVENTSDAY
					WHERE [EventTypeId] = @lLogOut

					SELECT @lHours = CAST(ROUND(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)) / 3600, 0, 1) AS VARCHAR)
						  ,@lMinutes = CAST(ROUND(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)) / 60, 0, 1) % 60 AS VARCHAR)
						  ,@lSeconds = CAST(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)) % 60 AS VARCHAR)
						  ,@lWorkingTotalSeconds = ROUND(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)), 0, 1)
					
					SELECT @lWorkingGlobal = @lWorkingGlobal + @lWorkingTotalSeconds
					
					SELECT @lWorkingTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)
					------------

					--LUNCH
					SELECT TOP 1 @lBeginLunchDateTime = CAST(RIGHT(e.[EventDate], 8) AS VARCHAR(100))
					FROM #EVENTSDAY e
					INNER JOIN [Management].[CatalogDetail] cd
						ON e.[EventTypeId] = cd.[Id]
					WHERE cd.[Value] = 'CUSTOMERS_LUNCH_TIME_BEGIN'

					SELECT TOP 1 @lFinishLunchDateTime = CAST(RIGHT(e.[EventDate], 8) AS VARCHAR(100))
					FROM #EVENTSDAY e
					INNER JOIN [Management].[CatalogDetail] cd
						ON e.[EventTypeId] = cd.[Id]
					WHERE cd.[Value] = 'CUSTOMERS_LUNCH_TIME_END'

					SELECT @lHours = CAST(ROUND(DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME)) / 3600, 0, 1) AS VARCHAR)
						  ,@lMinutes = CAST(ROUND(DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME)) / 60, 0, 1) % 60 AS VARCHAR)
						  ,@lSeconds = CAST(DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME)) % 60 AS VARCHAR)

					SELECT @lRealLunchTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)
					------------------

					--VISIT
					SELECT @lBeginManageDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, [EventDate], 108)), 108), 8) AS VARCHAR(100))
					FROM #EVENTSDAY
					WHERE [EventTypeId] = @lStartVisit

					SELECT @lFinishManageDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, [EventDate], 108)), 108), 8) AS VARCHAR(100))
					FROM #EVENTSDAY
					WHERE [EventTypeId] = @lEndVisit

					SELECT @lHours = CAST(ROUND(DATEDIFF(s, CAST(@lBeginManageDateTime AS DATETIME), CAST(@lFinishManageDateTime AS DATETIME)) / 3600, 0, 1) AS VARCHAR)
						  ,@lMinutes = CAST(ROUND(DATEDIFF(s, CAST(@lBeginManageDateTime AS DATETIME), CAST(@lFinishManageDateTime AS DATETIME)) / 60, 0, 1) % 60 AS VARCHAR)
						  ,@lSeconds = CAST(DATEDIFF(s, CAST(@lBeginManageDateTime AS DATETIME), CAST(@lFinishManageDateTime AS DATETIME)) % 60 AS VARCHAR)
						  ,@lProductiveTotalSeconds = ROUND(DATEDIFF(s, CAST(@lBeginManageDateTime AS DATETIME), CAST(@lFinishManageDateTime AS DATETIME)), 0, 1)

					SELECT @lProductiveTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

					--Movement and Stop 
				    SELECT * 
					INTO #FRAGMENTS
					FROM  
					(
						SELECT TOP 1 * 
						FROM #EVENTSDAY
						UNION
						SELECT r.*
						FROM #EVENTSDAY r,
								#EVENTSDAY r2
						WHERE r.[Id] = (r2.[Id] + 1) 
						AND r.[EventTypeId] <> r2.[EventTypeId]
					) [Fragments]
					
					SELECT @lMovementTime = SUM([SecondsMovement])
					FROM 
					(
						SELECT DATEDIFF(s, r.[EventDate], (SELECT TOP 1 [EventDate]
														   FROM #FRAGMENTS
														   WHERE [Id] > r.[Id]
														   ORDER BY [Id])) [SecondsMovement]
						FROM #FRAGMENTS r
						WHERE r.[EventTypeId] = @lLocation
					) [Sum]
					
					SELECT @lStopTime = SUM([SecondsMovement])
					FROM 
					(
						SELECT DATEDIFF(s, r.[EventDate], (SELECT TOP 1 [EventDate]
														   FROM #FRAGMENTS
														   WHERE [Id] > r.[Id]
														   ORDER BY [Id])) [SecondsMovement]
						FROM #FRAGMENTS r
						WHERE r.[EventTypeId] <> @lLocation
					) [Sum]
										
					DROP TABLE #FRAGMENTS

					SELECT @lTotalMovementTime = @lTotalMovementTime + @lMovementTime
					SELECT @lTotalStopTime = @lTotalStopTime + @lStopTime
					-------------------------------------------------------------------------------------

					SELECT @lHours = CAST(ROUND(@lMovementTime / 3600, 0, 1) AS VARCHAR)
						  ,@lMinutes = CAST(ROUND(@lMovementTime / 60, 0, 1) % 60 AS VARCHAR)
						  ,@lSeconds = CAST(@lMovementTime % 60 AS VARCHAR)

					SELECT @lMovementTimeSTR = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

					SELECT @lHours = CAST(ROUND(@lStopTime / 3600, 0, 1) AS VARCHAR)
						  ,@lMinutes = CAST(ROUND(@lStopTime / 60, 0, 1) % 60 AS VARCHAR)
						  ,@lSeconds = CAST(@lStopTime % 60 AS VARCHAR)

					SELECT @lStopTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

					SELECT @lPorcProductiveTime = CASE WHEN CAST(@lWorkingTotalSeconds AS DECIMAL) > 0 
													   THEN CAST(CAST((@lProductiveTotalSeconds / CAST(@lWorkingTotalSeconds AS DECIMAL)) * 100 AS DECIMAL(12, 0)) AS VARCHAR(20))
													   ELSE '0'
												  END
					
					INSERT INTO #RESULT
					SELECT LTRIM(@lAVGBeginJourney)
						  ,LTRIM(@lAVGFinishJourney)
						  ,@lWorkingTime
						  ,@lBeginLunchDateTime
						  ,@lFinishLunchDateTime
						  ,@lRealLunchTime
						  ,@lBeginManageDateTime
						  ,@lFinishManageDateTime
						  ,@lProductiveTime
						  ,ISNULL(@lPorcProductiveTime, 0)
						  ,ISNULL(@lKilometersAVG / 1000, 0)
						  ,@lMovementTimeSTR
						  ,@lStopTime
						  ,CONVERT(VARCHAR(8), @lTempBeginDate, 3)
						  ,@lTempBeginDate
						  ,@lFinalUserId
						  ,@lUserName

					SET @lTempLatitude = NULL
					SET @lTempLongitude = NULL
					SET @lTempDateTime = NULL
					SET @lKilometersAVG = @lKilometersAVG + @lKilometers
				END

				SELECT @lTempBeginDate = DATEADD(day, 1, @lTempBeginDate)

				DELETE FROM #EVENTSDAY 
			END

			--AVG
			SELECT @lAVGBeginJourney = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([AVGBeginJourney] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lAVGFinishJourney = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([AVGFinishJourney] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lHours = CAST(ROUND(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)) / 3600, 0, 1) AS VARCHAR)
				  ,@lMinutes = CAST(ROUND(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)) / 60, 0, 1) % 60 AS VARCHAR)
				  ,@lSeconds = CAST(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)) % 60 AS VARCHAR)
				  
			SELECT @lWorkingTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

			SELECT @lBeginLunchDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([BeginLunchDateTime] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lFinishLunchDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([FinishLunchDateTime] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lHours = CAST(ROUND(DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME)) / 3600, 0, 1) AS VARCHAR)
				  ,@lMinutes = CAST(ROUND(DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME)) / 60, 0, 1) % 60 AS VARCHAR)
				  ,@lSeconds = CAST(DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME)) % 60 AS VARCHAR)

			SELECT @lRealLunchTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

			SELECT @lBeginManageDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([BeginManageDateTime] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lFinishManageDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([FinishManageDateTime] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lHours = CAST(ROUND(DATEDIFF(s, CAST(@lBeginManageDateTime AS DATETIME), CAST(@lFinishManageDateTime AS DATETIME)) / 3600, 0, 1) AS VARCHAR)
				  ,@lMinutes = CAST(ROUND(DATEDIFF(s, CAST(@lBeginManageDateTime AS DATETIME), CAST(@lFinishManageDateTime AS DATETIME)) / 60, 0, 1) % 60 AS VARCHAR)
				  ,@lSeconds = CAST(DATEDIFF(s, CAST(@lBeginManageDateTime AS DATETIME), CAST(@lFinishManageDateTime AS DATETIME)) % 60 AS VARCHAR)
				  ,@lProductiveTotalSeconds = ROUND(DATEDIFF(s, CAST(@lBeginManageDateTime AS DATETIME), CAST(@lFinishManageDateTime AS DATETIME)), 0, 1)

			SELECT @lProductiveTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

			SELECT @lHours = CAST(ROUND(@lTotalMovementTime / 3600, 0, 1) AS VARCHAR)
				  ,@lMinutes = CAST(ROUND(@lTotalMovementTime / 60, 0, 1) % 60 AS VARCHAR)
				  ,@lSeconds = CAST(@lTotalMovementTime % 60 AS VARCHAR)

			SELECT @lMovementTimeSTR = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

			SELECT @lHours = CAST(ROUND(@lTotalStopTime / 3600, 0, 1) AS VARCHAR)
				  ,@lMinutes = CAST(ROUND(@lTotalStopTime / 60, 0, 1) % 60 AS VARCHAR)
				  ,@lSeconds = CAST(@lTotalStopTime % 60 AS VARCHAR)
				 
			SELECT @lStopTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

			SELECT @lPorcProductiveTime = CAST(CAST((@lProductiveTotalSeconds / CAST(@lWorkingTotalSeconds AS DECIMAL)) * 100 AS DECIMAL(12, 0)) AS VARCHAR(20))

			INSERT INTO #RESULT
			SELECT LTRIM(@lAVGBeginJourney)
				  ,LTRIM(@lAVGFinishJourney)
				  ,@lWorkingTime
				  ,@lBeginLunchDateTime
				  ,@lFinishLunchDateTime
				  ,@lRealLunchTime
				  ,@lBeginManageDateTime
				  ,@lFinishManageDateTime
				  ,@lProductiveTime
				  ,ISNULL(@lPorcProductiveTime, 0)
				  ,ISNULL(@lKilometersAVG / 1000, 0)
				  ,@lMovementTimeSTR
				  ,@lStopTime
				  ,'Promedio del Período'
				  ,NULL
				  ,@lFinalUserId
				  ,@lUserName

			DELETE FROM #EVENTS				
		END

		SELECT @lUsertCount = MIN([Id])
		FROM #USERS
		WHERE [Id] > @lUsertCount
	END

	SELECT [AVGBeginJourney]
		  ,[AVGFinishJourney]
		  ,[WorkingTime]
		  ,[BeginLunchDateTime]
		  ,[FinishLunchDateTime]
		  ,[RealLunchTime] 
		  ,[BeginManageDateTime]
		  ,[FinishManageDateTime]
		  ,[ProductiveTime]
		  ,[PorcProductiveTime]
		  ,[Kilometers]
		  ,[MovementTime]
		  ,[StopTime]
		  ,[Date]
		  ,[EventDate]
		  ,[UserId]
		  ,[AgentName]
	FROM #RESULT

	DROP TABLE #RESULT,
			   #EVENTS,
			   #EVENTSDAY,
			   #USERS

	SET NOCOUNT OFF
END
