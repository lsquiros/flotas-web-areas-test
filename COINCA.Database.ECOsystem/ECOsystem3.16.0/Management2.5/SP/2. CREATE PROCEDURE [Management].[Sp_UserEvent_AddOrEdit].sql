--USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_UserEvent_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_UserEvent_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 18/09/2018
-- Description:	Add or Edit the UserEvent
-- ================================================================================================
CREATE PROCEDURE [Management].[Sp_UserEvent_AddOrEdit]
(
	  @pEventDetailId INT = NULL						
	 ,@pCommerceId INT = NULL
	 ,@pTempCommerceId BIGINT = NULL
	 ,@pLatitude FLOAT = NULL
	 ,@pLongitude FLOAT = NULL
	 ,@pBatteryLevel INT = NULL
	 ,@pHeading DECIMAL(4,0) = NULL
	 ,@pSpeed DECIMAL(4,0) = NULL
	 ,@pAltitude DECIMAL(4,0) = NULL
	 ,@pLapse INT = NULL
	 ,@pEventDateTime DATETIME	
	 ,@pEventType VARCHAR(20) = NULL					
	 ,@pCustomerId INT	 				
	 ,@pUserId INT					
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0
			   ,@lEventTypeId INT = 0
			   ,@lJourneyBegin INT = 0
			   ,@lParentMainEventLast INT = -1
			    
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1
		END
		
		IF ((@pCommerceId IS NULL OR @pCommerceId = 0) AND (@pTempCommerceId IS NOT NULL))
		BEGIN
			SELECT @pCommerceId = [CommerceId] 
			FROM [Management].[TemporaryRelationshipCommerce]
			WHERE [TempCommerceId] = @pTempCommerceId
		END

		IF @pEventDetailId = 0 SET @pEventDetailId = NULL
		SET @lEventTypeId = (CASE WHEN @pEventType = 'location' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail] 
										WHERE [Value] = 'AGENT_LOCATION_REAL_TIME'
									)
								WHEN @pEventType = 'pin' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'CUSTOMERS_COMMERCE_PIN'
									)
								WHEN @pEventType = 'signal-off' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_LOSES_SIGNAL'
									)
								WHEN @pEventType = 'signal-on' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_RESTORE_SIGNAL'
									)
								WHEN @pEventType = 'gps-off' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_LOSES_SIGNAL_GPS'
									)
								WHEN @pEventType = 'gps-on' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_RESTORE_SIGNAL_GPS'
									)
								ELSE (
										SELECT [EventTypeId] 
										FROM [Management].[EventDetail]
										WHERE [Id] = @pEventDetailId
									)
							END )

		
		--GET THE JOURNEY END EVENT TYPE
		SELECT @lJourneyBegin = [Id]
		FROM [Management].[catalogDetail]
		WHERE [Value] = 'CUSTOMERS_JOURNEY_BEGIN'

		SELECT TOP(1) @lParentMainEventLast = [Parent] 
		FROM [Management].[UserEvent] ue
			INNER JOIN [Management].[CatalogDetail] cd
				ON ue.[EventTypeId] = cd.[Id]
		WHERE [CatalogId] = (SELECT Id FROM [Management].[Catalog]
										WHERE [Name] = 'Tipos de Eventos Principales')
			AND [EventTypeId] <> @lJourneyBegin
		ORDER BY [EventDate] ASC
		
		--EXECUTE STOP 
		IF ((@pLatitude IS NOT NULL AND @pLongitude IS NOT NULL) AND
			(@lParentMainEventLast <> 0))
		BEGIN
			EXEC [Management].[Sp_UserStop_AddOrEdit] @lEventTypeId
													 ,@pUserId
													 ,@pCustomerId
													 ,@pLatitude
													 ,@pLongitude
													 ,@pBatteryLevel
													 ,@pHeading 
													 ,@pSpeed 
													 ,@pAltitude 
													 ,@pEventDateTime
		END
						     
		INSERT INTO [Management].[UserEvent]
		(
			 [EventDetailId]
			,[EventTypeId]
			,[UserId]
			,[CommerceId]
			,[CustomerId]
			,[Latitude]
			,[Longitude]
			,[BatteryLevel]
			,[Heading]
			,[Speed]
			,[Altitude]
			,[Lapse]
			,[EventDate]
			,[Delete]
			,[InsertDate]
			,[InsertUserId]
		)
		VALUES
		(
			 @pEventDetailId
			,@lEventTypeId
			,@pUserId
			,@pCommerceId
			,@pCustomerId
			,@pLatitude
			,@pLongitude
			,@pBatteryLevel
			,@pHeading 
			,@pSpeed 
			,@pAltitude 
			,@pLapse 
			,@pEventDateTime
			,0
			,GETDATE()
			,@pUserId
		)

        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION	
				
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF 
END
