USE [ECOSYSTEM]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_AgentReconstructing_Report]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_AgentReconstructing_Report]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 19/09/2018
-- Description:	Agent Reconstructing Retrieve
-- Modify by:   Marjorie Garbanzo - 08/10/2018 - Add MovementName and EventKey
-- Modify by:   Marjorie Garbanzo - 08/10/2018 - Add Stop Event in the report
-- Modify by:   Marjorie Garbanzo - 29/10/2018 - Remove column of speed
--                                             - Change the data order by eventdate when generating the ID in the temporary table
-- Modify by: Henry Retana - 06/11/2018 - New data source added
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_AgentReconstructing_Report]
(	
	@pUserId INT,
	@pStartDate DATETIME,
	@pEndDate DATETIME = NULL
)
AS
BEGIN	
	SET NOCOUNT ON

	DECLARE @lBeginJourneyEvent INT,
			@lStopEvent INT,
			@lLocation INT,
			@lLocationName VARCHAR(200),
			@lDeletedCommerceDesc VARCHAR(100)

	CREATE TABLE #DATA 
	(	
		[Id] INT,
		[UserId] INT,
		[EventDetailId] INT,
		[EventDetailName] VARCHAR(200),
		[EventTypeId] INT,
		[EventName] VARCHAR(200),
		[Parent] INT,
		[CommerceId] INT,
		[Latitude] FLOAT,
		[Longitude] FLOAT,
		[BatteryLevel] INT,
		[Speed] DECIMAL(4, 0),
		[EventDate] DATETIME,
		[Lapse] INT
	)

	--Get Begin Journey 
	SELECT @lBeginJourneyEvent = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'

	SELECT @lDeletedCommerceDesc = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'DELETE_COMMERCE_DESC'

	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'
	
	SELECT @lStopEvent = [Id]
	FROM [Management].[CatalogDetail]
	WHERE [Value] = 'AGENT_STOP'

	SELECT @lLocationName = p.[MovementName]
	FROM [Management].[Parameters] p 
	INNER JOIN [Management].[UserEvent] u
		ON p.[CustomerId] = u.[CustomerId]
	WHERE u.[UserId] = @pUserId

	IF @pEndDate IS NULL SET @pEndDate = DATEADD(DAY, 1, @pStartDate)

	INSERT INTO #DATA
	EXEC [Management].[Sp_ReconstructionCleanData_Retrieve] @pUserId, @pStartDate, @pEndDate
		
	SELECT  t.[Id],
			t.[UserId],
			t.[EventTypeId],
			t.[EventDetailName],
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN ISNULL(@lLocationName, t.[EventName])
				 ELSE ISNULL(t.[EventDetailName], t.[EventName]) 
			END [EventName],
			CASE WHEN c.[IsDeleted] = 1 
				 THEN CONCAT(c.[Name], @lDeletedCommerceDesc)
				 ELSE c.[Name] 
			END [CommerceName],
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN ''
				 ELSE ISNULL(c.[Address], [Efficiency].[Fn_GetAddressByLatLon_Retrieve](t.[Longitude], t.[Latitude], NULL)) 
			END [Address],
			t.[Parent],
			
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN ''
				 ELSE t.[Latitude] 
			END [Latitude],
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN ''
				 ELSE t.[Longitude] 
			END [Longitude],
			t.[BatteryLevel],
			t.[EventDate],
			t.[EventDate] [StartDate], 
			(SELECT TOP 1 r.[EventDate] 
			 FROM #DATA r
			 WHERE r.[Id] > t.[Id]
			 ORDER BY r.[Id]) [EndDate],

			 CASE WHEN t.[EventTypeId] = @lStopEvent
				THEN [General].[Fn_GetTimeFormat](ISNULL((t.[Lapse] % (24 * 60 * 60)) / (60 * 60), '00'),
												  ISNULL(((t.[Lapse] % (24 * 60 * 60)) % (60 * 60)) / 60, '00'),
												  ISNULL(((t.[Lapse] % (24 * 60 * 60)) % (60 * 60)) % 60, '00'))

				ELSE [General].[Fn_GetTimeFormat](ISNULL(DATEDIFF(S, t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																					 FROM #DATA r
																					 WHERE r.[Id] > t.[Id]
																					 ORDER BY r.[Id])) / 3600, '00'),
		  										  ISNULL (DATEDIFF(S, t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																					  FROM #DATA r
																					  WHERE r.[Id] > t.[Id]
																					  ORDER BY r.[Id])) % 3600 / 60, '00'),
												  ISNULL (DATEDIFF(S, t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																					  FROM #DATA r
																					  WHERE r.[Id] > t.[Id]
																					  ORDER BY r.[Id])) % 60, '00')) 
			END [Lapse],
			
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN [Management].[Fn_GetDistanceFromPointsByDates] (@pUserId,  t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																								 FROM #DATA r
																								 WHERE r.[Id] > t.[Id]
																								 ORDER BY r.[Id]))
				 WHEN t.[EventTypeId] = @lStopEvent
				 THEN [Management].[Fn_GetDistanceFromPointsByDates] (@pUserId,  t.[EventDate], (SELECT TOP 1 r.[EventDate] 
																								 FROM #DATA r
																								 WHERE r.[Id] > t.[Id]
																								 ORDER BY r.[Id]))																	
				 ELSE 0.00 
			END [Distance],
			CASE WHEN t.[EventTypeId] = @lLocation
				 THEN (SELECT AVG(r.[Speed])
					   FROM #DATA r
					   WHERE r.[Id] > t.[Id])
				 WHEN t.[EventTypeId] = @lStopEvent
				 THEN t.[Speed]
				 ELSE 0 
			END [Speed],
			[Management].[Fn_GetEventColor] (t.[EventTypeId], t.[EventTypeId]) [RowColor]
	FROM #DATA t
	LEFT JOIN [General].[Commerces] c
		ON t.[CommerceId] = c.[Id]
	WHERE [Parent] <> @lBeginJourneyEvent	
	AND [EventTypeId] <> @lBeginJourneyEvent 
	UNION 
	SELECT  rt.[Id],
			rt.[UserId],	
			rt.[EventTypeId],
			rt.[EventDetailName],
			ISNULL(rt.[EventDetailName], rt.[EventName]) [EventName],
			CASE WHEN cc.[IsDeleted] = 1 
				 THEN CONCAT(cc.[Name], @lDeletedCommerceDesc)
				 ELSE cc.[Name] 
			END [CommerceName],
			ISNULL(cc.[Address], [Efficiency].[Fn_GetAddressByLatLon_Retrieve](rt.[Longitude], rt.[Latitude], NULL)) [Address],
			rt.[Parent],
			rt.[Latitude],
			rt.[Longitude],
			rt.[BatteryLevel],			
			rt.[EventDate],
			rt.[EventDate] [StartDate],
			DATEADD(SECOND, 1, rt.[EventDate]) [EndDate],
			
			CASE WHEN rt.[EventTypeId]<> @lBeginJourneyEvent
				THEN  [General].[Fn_GetTimeFormat](ISNULL (DATEDIFF(S, (SELECT TOP 1 r.[EventDate] 
																		FROM #DATA r
																		WHERE r.[EventTypeId] = @lBeginJourneyEvent
																		ORDER BY r.[Id]), rt.[EventDate]) / 3600, '00'),
		  										   ISNULL (DATEDIFF(S, (SELECT TOP 1 r.[EventDate] 
																	    FROM #DATA r
																	    WHERE r.[EventTypeId] = @lBeginJourneyEvent
																	    ORDER BY r.[Id]), rt.[EventDate]) % 3600 / 60, '00'),
												   ISNULL (DATEDIFF(S, (SELECT TOP 1 r.[EventDate] 
																	    FROM #DATA r
																	    WHERE r.[EventTypeId] = @lBeginJourneyEvent
																	    ORDER BY r.[Id]), rt.[EventDate]) % 60, '00')) 
				ELSE '00:00:00' 
			END [Lapse],
			0.00 [Distance],
			0 [Speed],
			NULL [RowColor]
	FROM #DATA rt
	LEFT JOIN [General].[Commerces] cc
		ON rt.[CommerceId] = cc.[Id]
	WHERE ([EventTypeId] = @lBeginJourneyEvent OR [Parent] = @lBeginJourneyEvent)
	ORDER BY [EventDate] 

	DROP TABLE #DATA

	SET NOCOUNT OFF
END