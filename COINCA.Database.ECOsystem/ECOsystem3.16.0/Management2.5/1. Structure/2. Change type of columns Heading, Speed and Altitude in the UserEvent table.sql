--USE [ECOsystem]
GO

-- ================================================================================
-- Author:       Marjorie Garbanzo Morales
-- Create date:  02/11/2018
-- Description:  Change type of columns Heading, Speed and Altitude in the UserEvent table
-- ================================================================================

ALTER TABLE [Management].[UserEvent]
DROP COLUMN Heading

GO

ALTER TABLE [Management].[UserEvent]
DROP COLUMN Speed

GO

ALTER TABLE [Management].[UserEvent]
DROP COLUMN Altitude

GO

ALTER TABLE [Management].[UserEvent]
ADD Heading DECIMAL(4,0)

GO

ALTER TABLE [Management].[UserEvent]
ADD Speed DECIMAL(4,0)

GO

ALTER TABLE [Management].[UserEvent]
ADD Altitude DECIMAL(4,0)


