--USE [ECOsystem]
GO

IF OBJECT_ID (N'[Management].[UserStop]', N'U') IS NOT NULL DROP TABLE [Management].[UserStop]
GO
 
 -- ================================================================================================
-- Author:		Marjorie Garbanzo
-- Create date: 02/11/2018
-- Description:	Change type of columns Heading, Speed and Altitude
-- ================================================================================================

CREATE TABLE [Management].[UserStop]
(
	[UserId] INT,
	[CustomerId] INT,
	[Latitude] FLOAT,
	[Longitude] FLOAT,
	[BatteryLevel] INT,
	[Heading] DECIMAL(4,0),
	[Speed] DECIMAL(4,0),
	[Altitude] DECIMAL(4,0),
	[EventDate] DATETIME,
	[InitStopDate] DATETIME	 
) 
GO

ALTER TABLE [Management].[UserStop] WITH CHECK ADD CONSTRAINT [FK_UserId_UserStops] FOREIGN KEY([UserId])
REFERENCES [General].[Users] ([UserId])

GO

ALTER TABLE [Management].[UserStop] WITH CHECK ADD CONSTRAINT [FK_CustomerId_UserStops] FOREIGN KEY([CustomerId])
REFERENCES [General].[Customers] ([CustomerId])
GO