USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_MeasuringAgentsMonthlyReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_MeasuringAgentsMonthlyReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:		Henry Retana
-- Create date: 21/07/2017
-- Description:	Retrieve data for Report Measuring Agents Monthly
-- ======================================================================================================
CREATE PROCEDURE [Management].[Sp_Retrieve_MeasuringAgentsMonthlyReport]
(
	@pCustomerId INT,
	@pStartDate DATETIME, 
	@pEndDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lCount INT,
			@lLogIn INT,
			@lLogOut INT,
			@lStartVisit INT,
			@lEndVisit INT,
			@lLocation INT,			
			@lUserId INT,
			@lMainEvents INT

	--GET THE IDS FOR THE EVENTS--
	SELECT @lLogout = c.[Id],
		   @lLogin = c.[Parent]
	FROM [Management].[CatalogDetail] c
	INNER JOIN [dbo].[GeneralParameters] p
		ON c.[Parent] = p.[Value]
	WHERE p.[ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'	

	SELECT @lEndVisit= c.[Id],
		   @lStartVisit = c.[Parent]
	FROM [Management].[CatalogDetail] c
	INNER JOIN [dbo].[GeneralParameters] p
		ON c.[Parent] = p.[Value]
	WHERE p.[ParameterID] = 'CUSTOMERS_CLIENT_BEGIN_VISIT'
	
	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'

	SELECT @lMainEvents = [Id] 
	FROM [Management].[Catalog]
	WHERE [Name] = 'Tipos de Eventos Principales'
	--**************************--

	CREATE TABLE #DATA 
	(	
		[Id] INT,
		[UserId] INT,
		[EventDetailId] INT,
		[EventDetailName] VARCHAR(200),
		[EventTypeId] INT,
		[EventName] VARCHAR(200),
		[Parent] INT,
		[CommerceId] INT,
		[Latitude] FLOAT,
		[Longitude] FLOAT,
		[BatteryLevel] INT,
		[Speed] DECIMAL(4, 0),
		[EventDate] DATETIME,
		[Lapse] INT
	)

	CREATE TABLE #TempData
	(
		[UserId] INT,
		[EventTypeId] INT,
		[Minutes] INT
	)

	CREATE TABLE #LOGINS
	(
		[UserId] INT,
		[LogIn] DATETIME,
		[LogOut] DATETIME
	)

	CREATE TABLE #Users
	(
		[Id] INT IDENTITY,
		[UserId] INT
	)

	CREATE TABLE #Result
	(
		[AgentWorkTime] VARCHAR(20), 
		[AgentClientTime] VARCHAR(20), 
		[AgentMovingTime] VARCHAR(20),
		[AgentLostTime] VARCHAR(20),
		[AgentLogIn] DATETIME,
		[AgentLogOut] DATETIME,
		[ClientDifference] INT,
		[MovingDifference] INT, 
		[LostDifference] INT, 
		[LogInDifference] INT,
		[LogOutDifference] INT,
		[ClientTime] VARCHAR(20),
		[LostTime] VARCHAR(20),
		[MovingTime] VARCHAR(20),
		[StartTime] INT,
		[EndTime] INT,
		[LogInScore] INT,
		[LogOutScore] INT,
		[ClientScore] INT,
		[MovingScore] INT,
		[LostScore] INT,		
		[EncryptDriverName] VARCHAR(500),
		[TopMargin] INT,
		[LowMargin] INT,
		[TopPercent] INT,
		[LowPercent] INT,
		[MiddlePercent] INT,
		[UserId] INT
	)

	INSERT INTO #Users
	SELECT DISTINCT du.[UserId]
	FROM [General].[DriversUsers] du
	INNER JOIN [Management].[UserEvent] m
		ON du.[UserId] = m.[UserId]
	WHERE du.[CustomerId] = @pCustomerId
	AND m.[EventDate] BETWEEN @pStartDate AND @pEndDate
	and du.UserId = 26025


	SELECT @lCount = MIN([Id])
	FROM #Users

	WHILE @lCount IS NOT NULL 
	BEGIN
		DECLARE @lRowId INT = NULL

		SELECT @lUserId = [UserId]
		FROM #Users
		WHERE [Id] = @lCount
				
		INSERT INTO #DATA
		EXEC [Management].[Sp_ReconstructionCleanData_Retrieve] @lUserId, @pStartDate, @pEndDate, 0
		
		SELECT @lRowId = MIN([Id])
		FROM #Data
	
		--GO THROUGH THE DATA 
		WHILE @lRowId IS NOT NULL 
		BEGIN 
			DECLARE @lEventTypeId INT = NULL,
					@lStartTime DATETIME = NULL,
					@lEndTime DATETIME = NULL,
					@lFinishId INT = NULL															

			SELECT @lEventTypeId = [EventTypeId], 
				   @lStartTime = [EventDate]
			FROM #Data
			WHERE [Id] = @lRowId

			--VALIDATE EVENTS 
			--Login
			IF @lEventTypeId = @lLogIn
			BEGIN 			
				
				INSERT INTO #LOGINS
				(
					[UserId],
					[LogIn]
				)				
				SELECT @lUserId,
					   CONVERT(CHAR(8),CAST(AVG(CAST([EventDate] AS FLOAT) - FLOOR(CAST([EventDate] AS FLOAT))) AS DATETIME), 108)
				FROM #Data
				WHERE [EventTypeId] = @lLogIn

				SELECT @lStartTime = CONVERT(CHAR(8),CAST(AVG(CAST([EventDate] AS FLOAT) - FLOOR(CAST([EventDate] AS FLOAT))) AS DATETIME), 108)
				FROM #Data
				WHERE [EventTypeId] = @lLogIn

				UPDATE 	#LOGINS
				SET [LogOut] = (SELECT CONVERT(CHAR(8),CAST(AVG(CAST([EventDate] AS FLOAT) - FLOOR(CAST([EventDate] AS FLOAT))) AS DATETIME), 108)
								FROM #Data
								WHERE [EventTypeId] = @lLogOut)
				WHERE [UserId] = @lUserId
								
				SELECT TOP 1 @lEndTime = (SELECT CONVERT(CHAR(8),CAST(AVG(CAST([EventDate] AS FLOAT) - FLOOR(CAST([EventDate] AS FLOAT))) AS DATETIME), 108)
										  FROM #Data
										  WHERE [EventTypeId] = @lLogOut)
				FROM #Data
				WHERE [EventTypeId] = @lLogOut 
				AND   [Id] > @lRowId
				ORDER BY [EventDate] DESC

				--VALIDATES NULL
				SELECT @lEndTime = ISNULL(@lEndTime, CAST(@pEndDate AS TIME)),
					   @lStartTime = ISNULL(@lStartTime, CAST(@pStartDate AS TIME))
				
				DELETE FROM #Data
				WHERE [EventTypeId] = @lLogIn
				AND [Id] <> @lRowId
			END
			--Visit
			ELSE IF @lEventTypeId = @lStartVisit
			BEGIN 
				SELECT TOP 1 @lFinishId = [Id],  
							 @lEndTime = [EventDate]
				FROM #Data
				WHERE [EventTypeId] = @lEndVisit
				AND   [Id] > @lRowId
				ORDER BY [Id] 
			END
			--Moving
			ELSE IF @lEventTypeId = @lLocation
			BEGIN 
				SELECT TOP 1 @lFinishId = [Id],
							 @lEndTime = [EventDate]
				FROM #Data
				WHERE [EventTypeId] <> @lLocation
				AND   [Id] > @lRowId				
				ORDER BY [Id]				
			END
			ELSE IF EXISTS (
								SELECT [Id]
								FROM [Management].[CatalogDetail]
								WHERE [Id] NOT IN (@lStartVisit, @lLogIn)
								AND [Id] = @lEventTypeId
								AND [CatalogId] = @lMainEvents
								AND [Parent] = 0
						   )	
			BEGIN 
				SELECT TOP 1 @lFinishId = [Id],  
							 @lEndTime = [EventDate]
				FROM #Data
				WHERE [EventTypeId] IN (
											SELECT [Id]
											FROM [Management].[CatalogDetail]
											WHERE [Parent] = @lEventTypeId
									   )
				AND   [Id] > @lRowId
				ORDER BY [Id]
			END
					
			--Insert data in temp table
			INSERT INTO #TempData
			VALUES 
			(
				@lUserId,
				@lEventTypeId,
				DATEDIFF(MINUTE, @lStartTime, @lEndTime)
			)

			SELECT @lRowId = MIN([Id])
			FROM #Data
			WHERE [Id] > @lRowId
		END 

		TRUNCATE TABLE #Data

		SELECT @lCount = MIN([Id])
		FROM #Users
		WHERE [Id] > @lCount
	END 

	SELECT @lUserId = MIN([UserId])
	FROM #TempData 
	
	WHILE @lUserId IS NOT NULL
	BEGIN
		DECLARE @lLogInTime DATETIME = NULL,
				@lLogOutTime DATETIME = NULL,
				@lLostTime INT = NULL,
				@lWorkTime VARCHAR(20) = NULL,						
				@lClientTime VARCHAR(20) = NULL,
				@lMovingTime VARCHAR(20) = NULL,			
				@lWorkMinutes INT = NULL,						
				@lClientMinutes INT = NULL,
				@lMovingMinutes INT = NULL

		SELECT @lLogInTime = ISNULL([LogIn], CAST(@pStartDate AS TIME)),
			   @lLogOutTime = ISNULL([LogOut], CAST(@pEndDate AS TIME))
		FROM #LOGINS 
		WHERE [UserId] = @lUserId

		--Get the final Times result 
		SELECT @lWorkTime = [General].[Fn_GetTimeFormat] (CAST(SUM([Minutes]) / 60 AS VARCHAR), RIGHT('0' + CAST(SUM([Minutes]) % 60 AS VARCHAR), 2), 0),
			   @lWorkMinutes = SUM([Minutes])
		FROM #TempData
		WHERE [EventTypeId] = @lLogIn
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]

		SELECT @lClientTime = [General].[Fn_GetTimeFormat] (CAST(SUM([Minutes]) / 60 AS VARCHAR), RIGHT('0' + CAST(SUM([Minutes]) % 60 AS VARCHAR), 2), 0),
			   @lClientMinutes = SUM([Minutes])	   
		FROM #TempData
		WHERE [EventTypeId] = @lStartVisit
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]

		SELECT @lMovingTime = [General].[Fn_GetTimeFormat] (CAST(SUM([Minutes]) / 60 AS VARCHAR), RIGHT('0' + CAST(SUM([Minutes]) % 60 AS VARCHAR), 2), 0),
			   @lMovingMinutes = SUM([Minutes])
		FROM #TempData
		WHERE [EventTypeId] = @lLocation
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]

		SELECT @lLostTime = SUM([Minutes])
		FROM #TempData
		WHERE [EventTypeId] IN (
									SELECT [Id]
									FROM [Management].[CatalogDetail]
									WHERE [Id] NOT IN (@lStartVisit, @lLogIn)
									AND [CatalogId] = @lMainEvents
									AND [Parent] = 0
						       )
		AND   [UserId] = @lUserId
		GROUP BY [EventTypeId]							 		

		INSERT INTO #Result
		SELECT ISNULL(@lWorkTime, '00:00:00'),
			   ISNULL(@lClientTime, '00:00:00'),
			   ISNULL(@lMovingTime, '00:00:00'),
			   ISNULL([General].[Fn_GetTimeFormat] (CAST(@lLostTime / 60 AS VARCHAR), RIGHT('0' + CAST(@lLostTime % 60 AS VARCHAR), 2), 0), 0),
			   ISNULL(@lLogInTime, 0),
			   ISNULL(@lLogOutTime, 0),
			   ISNULL(@lClientMinutes, 0) - [ClientMinutes],
			   ISNULL(@lMovingMinutes, 0) - [MovingMinutes], 
			   ISNULL([LostMinutes] - @lLostTime, 0),
			   ISNULL(DATEDIFF(MINUTE, @lLogInTime, DATEADD(HOUR, [TimeBeginJourney], CONVERT(DATETIME, DATEDIFF(DAY, 0, @lLogInTime)))), 0), 
			   ISNULL(DATEDIFF(MINUTE, DATEADD(HOUR, [TimeFinishJourney], CONVERT(DATETIME, DATEDIFF(DAY, 0, @lLogOutTime))), @lLogOutTime), 0),
			   [General].[Fn_GetTimeFormat] (CAST([ClientMinutes] / 60 AS VARCHAR), RIGHT('0' + CAST([ClientMinutes] % 60 AS VARCHAR), 2), 0),
			   [General].[Fn_GetTimeFormat] (CAST([LostMinutes] / 60 AS VARCHAR), RIGHT('0' + CAST([LostMinutes] % 60 AS VARCHAR), 2), 0),
			   [General].[Fn_GetTimeFormat] (CAST([MovingMinutes] / 60 AS VARCHAR), RIGHT('0' + CAST([MovingMinutes] % 60 AS VARCHAR), 2), 0),
			   [TimeBeginJourney],
			   [TimeFinishJourney],
			   --GET SCORES
			   [Management].[Fn_GetManagementScoreByParameters] 
			   (
					ISNULL(DATEDIFF(MINUTE, @lLogInTime, DATEADD(HOUR, [TimeBeginJourney], CONVERT(DATETIME, DATEDIFF(DAY, 0, @lLogInTime)))), 0), 
					NULL, 
					NULL, 
					NULL, 
					NULL, 
					[TopMargin], 
					[LowMargin], 
					[TopPercent], 
					[MiddlePercent], 
					[LowPercent]
			    ),
				[Management].[Fn_GetManagementScoreByParameters] 
				(
					NULL, 
					ISNULL(DATEDIFF(MINUTE, DATEADD(HOUR, [TimeFinishJourney], CONVERT(DATETIME, DATEDIFF(DAY, 0, @lLogOutTime))), @lLogOutTime), 0),
					NULL, 
					NULL, 
					NULL, 
					[TopMargin], 
					[LowMargin], 
					[TopPercent], 
					[MiddlePercent], 
					[LowPercent]
				),
				[Management].[Fn_GetManagementScoreByParameters] (NULL, NULL, ISNULL(@lClientMinutes, 0) - [ClientMinutes], NULL, NULL, [TopMargin], [LowMargin], [TopPercent], [MiddlePercent], [LowPercent]),
				[Management].[Fn_GetManagementScoreByParameters] (NULL, NULL, NULL, ISNULL(@lMovingMinutes, 0) - [MovingMinutes], NULL, [TopMargin], [LowMargin], [TopPercent], [MiddlePercent], [LowPercent]),
				[Management].[Fn_GetManagementScoreByParameters] (NULL, NULL, NULL, NULL, ISNULL(@lLostTime, 0) - [LostMinutes], [TopMargin], [LowMargin], [TopPercent], [MiddlePercent], [LowPercent]),
			    ---------------------
				(
					SELECT [Name]
					FROM [General].[DriversUsers] du
					INNER JOIN [General].[Users] u
						ON du.[UserId] = u.[UserId]
					WHERE [DriversUserId] = @lUserId
				),
			   [TopMargin],
			   [LowMargin],
			   [TopPercent],
			   [LowPercent],
			   [MiddlePercent],
			   @lUserId
		FROM [Management].[Parameters]
		WHERE [CustomerId] = @pCustomerId

		SELECT @lUserId = MIN([UserId])
		FROM #TempData
		WHERE [UserId] > @lUserId
	END

	--Returns Results with all the information
	SELECT r.userid, r.[AgentWorkTime], 
		   r.[AgentClientTime], 
		   r.[AgentMovingTime],
		   r.[AgentLostTime],
		   r.[AgentLogIn],
		   r.[AgentLogOut],
		   r.[ClientDifference],
		   r.[MovingDifference], 
		   r.[LostDifference], 
		   r.[LogInDifference],
		   r.[LogOutDifference],
		   r.[ClientTime],
		   r.[LostTime],
		   r.[MovingTime],
		   r.[StartTime],
		   r.[EndTime],
		   r.[LogInScore],
		   r.[LogOutScore],
		   r.[ClientScore],
		   r.[MovingScore],
		   r.[LostScore],
		   u.[Name] [EncryptDriverName],
		   r.[TopMargin],
		   r.[LowMargin],
		   r.[TopPercent],
		   r.[LowPercent],
		   r.[MiddlePercent]
	FROM #Result r
	INNER JOIN [General].[Users] u
		ON r.[UserId] = u.[UserId]

	DROP TABLE #Data
	DROP TABLE #Users
	DROP TABLE #TempData
	DROP TABLE #Result
	DROP TABLE #LOGINS

	SET NOCOUNT OFF
END