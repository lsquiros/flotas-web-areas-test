--USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_CatalogDetail_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_CatalogDetail_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 17/09/2018
-- Description:	Catalog Detail Retrieve
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_CatalogDetail_Retrieve]
(	
	 @pId INT,
     @pName VARCHAR(200) = NULL 
)
AS
BEGIN	
	SET NOCOUNT ON

	DECLARE @lIcons INT

	SELECT @lIcons = [Id] 
	FROM [Management].[Catalog]
	WHERE [Name] = 'Iconos'

    SELECT cd.[Id],
		   CASE WHEN @lIcons = @pId
				THEN CONCAT(cd.[Name], '|', cd.[Description])
				ELSE cd.[Name]
		   END [Name]
	FROM [Management].[Catalog] c
	INNER JOIN [Management].[CatalogDetail] cd
		ON c.[Id] = cd.[CatalogId]
	WHERE c.[Id] = @pId
		AND cd.[Active] = 1

	SET NOCOUNT OFF
END
