USE [ECOSYSTEM]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_AgentDetailInformation]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_AgentDetailInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 28/06/2017
-- Description:	Retrieve Agent Detail Information
-- Modify By: Henry Retana - 17/01/2018 -- Modify the retrieve events, add start and end events
-- Modify By: Sebastián Quesada - 29/05/2018 -- Modificación para mostrar parametro CommerceName
-- Modify By: Sebastián Quesada - 16/07/2018 -- Modificación para enviar parametro customerId a la funcion [Efficiency].[Fn_GetAddressByLatLon_Retrieve]
-- Modify By: Henry Retana - 29/01/2018 -- Use new tables and set commerce delete
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Retrieve_AgentDetailInformation]
(
	  @pIds VARCHAR(MAX) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON		
	
	DECLARE @lIds TABLE ([Id] INT)
	DECLARE @lDeletedCommerceDesc VARCHAR(100)

	IF @pIds IS NOT NULL
	BEGIN 
		INSERT INTO @lIds
		SELECT	items
		FROM	dbo.Fn_Split (@pIds, ',')
	END
	
	SELECT @lDeletedCommerceDesc = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'DELETE_COMMERCE_DESC'

	SELECT u.[UserId],
		   u.[Name] [EncryptName],
		   m.[Longitude],
		   m.[Latitude],
		   [Efficiency].[Fn_GetAddressByLatLon_Retrieve](m.[Longitude], m.[Latitude], c.[CustomerId]) [Address],
		   CASE WHEN c.[IsDeleted] = 1 
				THEN CONCAT(c.[Name], @lDeletedCommerceDesc) 
				ELSE c.[Name] 
		   END [CommerceName],
		   t.[Name] [EventName],
		   CONCAT(DATEDIFF(S, m.[InsertDate], GETDATE()) / 3600, ':', RIGHT('0' + CAST(DATEDIFF(S, m.[InsertDate], GETDATE()) % 3600 / 60 AS VARCHAR(2)), 2)) [Lapse], 
		   m.[BatteryLevel],
		   m.[EventDate]
	FROM [Management].[UserEvent] m
	INNER JOIN (
					SELECT MAX(ue.[Id]) [Id]
					FROM [Management].[UserEvent] ue
					INNER JOIN [General].[DriversUsers] u
						ON u.[UserId] = ue.[UserId]
					WHERE u.[UserId] IN (
											SELECT [Id]
											FROM @lIds
										)
					AND ue.[Latitude] IS NOT NULL
					GROUP BY ue.[UserId]
				) ma
		ON m.[Id] = ma.[Id]
	INNER JOIN [Management].[CatalogDetail] t
		ON m.[EventTypeId] = t.[Id]
	INNER JOIN [General].[DriversUsers] d
		ON d.[UserId] = m.[UserId]
	INNER JOIN [General].[Users] u
		ON u.[UserId] = d.[UserId]   
	LEFT JOIN [General].[Commerces] c
	    ON c.[Id] = m.[CommerceId]
	WHERE u.[UserId] IN (
							SELECT u.[Id]
							FROM @lIds u
						)
	ORDER BY m.[InsertDate] DESC

    SET NOCOUNT OFF
END