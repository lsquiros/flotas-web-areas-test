USE [ECOSYSTEM]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_ReconstructionCleanData_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_ReconstructionCleanData_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 06/11/2018
-- Description:	Reconstruction Clean Data Retrieve
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_ReconstructionCleanData_Retrieve]
(
	 @pUserId INT,
	 @pStartDate DATETIME,
	 @pEndDate DATETIME = NULL,
	 @pShowFragment BIT = 1
)
AS
BEGIN
	SET NOCOUNT ON	

	DECLARE @lBeginJourneyEvent INT,
			@lStopEvent INT,
			@lLocation INT,
			@lMainEvents INT,
			@lRangeId INT 

	--Get Begin Journey 
	SELECT @lBeginJourneyEvent = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'

	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'

	SELECT @lMainEvents = [Id] 
	FROM [Management].[Catalog]
	WHERE [Name] = 'Tipos de Eventos Principales'
		
	IF @pEndDate IS NULL SET @pEndDate = DATEADD(DAY, 1, @pStartDate)
	
	SELECT  CAST(ROW_NUMBER() OVER(ORDER BY u.[EventDate] ASC) AS INT) [Id],
			u.[UserId],
			u.[EventDetailId],
			ed.[Name] [EventDetailName],
			u.[EventTypeId],
			c.[Name] [EventName] ,
			c.[Parent],
			u.[CommerceId],
			u.[Latitude],
			u.[Longitude],
			u.[BatteryLevel],
			u.[Speed],
			u.[EventDate],
			CAST(u.[Lapse] AS VARCHAR(10)) [Lapse]		
	INTO #REPORTS
	FROM [Management].[UserEvent] u
	INNER JOIN [Management].[CatalogDetail] c
		ON u.[EventTypeId] = c.[Id]
	LEFT JOIN [Management].[EventDetail] ed
		ON u.[EventDetailId] = ed.[Id]
	WHERE u.[UserId] = @pUserId 
	AND u.[EventDate] BETWEEN @pStartDate AND @pEndDate
	AND u.[Delete] = 0
	ORDER BY u.[EventDate]	
	
	IF @pShowFragment = 0
	BEGIN
		DELETE FROM #REPORTS
		WHERE [EventTypeId] NOT IN (
										SELECT [Id]		
										FROM [Management].[CatalogDetail]
										WHERE [CatalogId] = @lMainEvents
										UNION 
										SELECT @lLocation
									)
		--UPDATE THE IDS 
		UPDATE #REPORTS 
		SET [Id] = REPORTS.[Number] 
		FROM #REPORTS
		INNER JOIN 
		(
			SELECT [Id],
				   CAST(ROW_NUMBER() OVER(ORDER BY [EventDate] ASC) AS INT) [Number]
			FROM #REPORTS
		) REPORTS 
		ON REPORTS.[Id] = #REPORTS.[Id]
	END 			

	--GET THE FRAGMENTS	
	SELECT * 		
	INTO #DATA
	FROM
	( 
		SELECT TOP 1 * 	
		FROM #REPORTS
		ORDER BY [EventDate]
		UNION
		SELECT r.*
		FROM #REPORTS r,
			 #REPORTS r2
		WHERE r.[Id] = (r2.[Id] + 1) 
		AND r.[EventTypeId] <> r2.[EventTypeId]
	) [Data]
	
	--;WITH managementevents as 
	--(	
	--	SELECT * 
	--	FROM #DATA 
	--	WHERE [EventTypeId] IN (
	--								SELECT [Parent] 
	--								FROM #DATA 
	--								WHERE [Parent] > 0
	--								AND [Parent] <> @lBeginJourneyEvent
	--							)
	--	OR [Parent] > 0			
	--	AND [Parent] <> @lBeginJourneyEvent
	--	AND [EventTypeId] <> @lBeginJourneyEvent
	--)
	--SELECT	CAST(ROW_NUMBER() OVER(ORDER BY t.[Id] ASC) AS INT) [Id],
	--		(
	--			SELECT TOP 1 m1.[Id]	
	--			FROM managementevents m1
	--			WHERE m1.[Id] = t.[Id]
	--			AND m1.[Parent] = 0
	--		) [Range1],
	--		(
	--			SELECT TOP 1 m1.[Id]	
	--			FROM managementevents m1
	--			WHERE m1.[Id] > t.[Id] 
	--			AND m1.Parent > 0
	--			ORDER BY m1.[Id] ASC
	--		) [Range2]
	--INTO #RANGES
	--FROM #DATA t
	--LEFT JOIN managementevents m
	--	ON t.[Id] = m.[Id]
	--WHERE t.[Id] BETWEEN (
	--						SELECT TOP 1 m1.[Id]	
	--						FROM managementevents m1
	--						WHERE m1.[Id] = t.[Id]
	--						AND m1.[Parent] = 0
	--					) AND
	--					(
	--						SELECT TOP 1 m1.[Id]	
	--						FROM managementevents m1
	--						WHERE m1.[Id] > t.[Id] 
	--						AND m1.Parent > 0
	--						ORDER BY m1.[Id] ASC
	--					)	
	--ORDER BY m.[Id]
	
	--SELECT @lRangeId = MIN([Id])
	--FROM #RANGES

	--WHILE @lRangeId IS NOT NULL
	--BEGIN
	--	 DECLARE @lRange1 INT, 
	--			 @lRange2 INT 

	--	 SELECT @lRange1 = range1, 
	--			@lRange2 = range2
	--	 FROM #RANGES
	--	 WHERE [Id] = @lRangeId

	--	 DELETE FROM #DATA
	--	 WHERE [Id] BETWEEN @lRange1 + 1 AND @lRange2 - 1

	--	 SELECT @lRangeId = MIN([Id])
	--	 FROM #RANGES
	--	 WHERE [Id] > @lRangeId 
	--END
	
	SELECT CAST(ROW_NUMBER() OVER(ORDER BY [Id] ASC) AS INT) [Id],
		   [UserId],
		   [EventDetailId],
		   [EventDetailName],
		   [EventTypeId],
		   [EventName],
		   [Parent],
		   [CommerceId],
		   [Latitude],
		   [Longitude],
		   [BatteryLevel],
		   [Speed],
		   [EventDate],
		   ISNULL([Lapse], 0) [Lapse]
	FROM #DATA

	DROP TABLE #REPORTS 
			   ,#DATA
			   --,#RANGES

	SET NOCOUNT OFF
END
