--USE [ECOsystem]

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/11/2018
-- Description:	Update Icons Catalog
-- ================================================================================================

DECLARE @lIcons INT

SELECT @lIcons = [Id] 
FROM [Management].[Catalog]
WHERE [Name] = 'Iconos'

--disabled cat ico
UPDATE [Management].[CatalogDetail] SET [Active] = 0 WHERE [Name] = 'mdi-cat' AND [CatalogId] = @lIcons

--update catalog 
UPDATE [Management].[CatalogDetail] SET [Name] =  'Usuario' WHERE [Name] = 'mdi-account' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Usuario' WHERE [Name] = 'mdi-account' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Gato' WHERE [Name] = 'mdi-cat' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Casa' WHERE [Name] = 'mdi-home' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Trabajo' WHERE [Name] = 'mdi-domain' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Comercio' WHERE [Name]   'mdi-store' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Tiempo Usuario' WHERE [Name] = 'mdi-account-clock' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Alerta' WHERE [Name] = 'mdi-alert-circle-outline' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Ejecutivo' WHERE [Name] = 'mdi-briefcase' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Agenda' WHERE [Name] = 'mdi-calendar-check' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Camar�' WHERE [Name] = 'mdi-camera' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Checkbox' WHERE [Name] = 'mdi-checkbox-marked' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Tiempo' WHERE [Name] = 'mdi-circle-slice-5' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Reloj' WHERE [Name] = 'mdi-clock' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Tiempo Inicio' WHERE [Name] = 'mdi-clock-start' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Tiempo Final' WHERE [Name] = 'mdi-clock-end' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'F�brica' WHERE [Name] = 'mdi-factory' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Mapa' WHERE [Name] = 'mdi-map-marker' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Mensaje' WHERE [Name] = 'mdi-message-text' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Parque' WHERE [Name] = 'mdi-nature-people' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Play' WHERE [Name] = 'mdi-play' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Estrella' WHERE [Name] = 'mdi-star' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Detener' WHERE [Name] = 'mdi-stop' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Etiqueta' WHERE [Name] = 'mdi-tag' AND [CatalogId] = @lIcons
UPDATE [Management].[CatalogDetail] SET [Name] =  'Temporizador' WHERE [Name] = 'mdi-timer' AND [CatalogId] = @lIcons
