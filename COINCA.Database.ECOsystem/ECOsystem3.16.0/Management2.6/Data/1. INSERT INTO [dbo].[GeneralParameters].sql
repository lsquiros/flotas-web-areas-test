USE [ECOsystem]
GO

-- ================================================================================
-- Author:       Marjorie Grabanzo Morales
-- Create date:  30/10/2018
-- Description:  Limpiar tabla para el calculo de paradas
-- ================================================================================

INSERT INTO [dbo].[GeneralParameters]
VALUES
(
	'DELETE_COMMERCE_DESC',
	'NA',
	'Descripción para comercios eliminados',
	'- Elimanado',
	0
)