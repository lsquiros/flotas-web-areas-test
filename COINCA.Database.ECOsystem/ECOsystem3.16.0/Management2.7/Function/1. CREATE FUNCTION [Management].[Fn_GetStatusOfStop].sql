--USE [ECOsystem]
GO
IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Fn_GetStatusOfStop]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [Management].[Fn_GetStatusOfStop]
GO

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:		Henry Retana 
-- Create date: 17/10/2018
-- Description:	Get the status of a stop
-- Modify by:   Marjorie Garbanzo - 08/11/2018 - Change the conditions of the calculation
-- ===============================================================================

CREATE FUNCTION [Management].[Fn_GetStatusOfStop]
(
	@pUserId INT
   ,@pCustomerId INT
   ,@pLatitude FLOAT 
   ,@pLongitude FLOAT
   ,@pSpeed INT
   ,@pEventDate DATETIME    
   ,@pStopHistory BIT
)
RETURNS @lResult TABLE 
(
    [Lapse] INT, 
    [IsStop] BIT,
	[Update] BIT
)
AS
BEGIN	
	DECLARE @lStopMinimumTime INT,
			@lStopMinimumDistance INT,
			@lStopMinimumSpeed INT

	--GET PARAMETERS
	SELECT  @lStopMinimumTime = [StopMinimumTime],
			@lStopMinimumDistance = [StopMinimumDistance],
			@lStopMinimumSpeed = [StopMinimumSpeed]
	FROM [Management].[Parameters] 
	WHERE [CustomerId] = @pCustomerId

	--SET DEFAULT RESULT
	INSERT INTO @lResult
	SELECT NULL,
		   CAST(0 AS BIT),
		   CAST(0 AS BIT)

	IF @pStopHistory = 1	
	BEGIN 
		DECLARE @lLatitude FLOAT,
				@lLongitude FLOAT,
				@lInitStopTime DATETIME 

		SELECT @lLatitude = [Latitude],
			   @lLongitude = [Longitude],
			   @lInitStopTime = [InitStopDate]
		FROM [Management].[UserStop]
		WHERE [UserId] = @pUserId
		AND [CustomerId] = @pCustomerId
		
		--GET THE POINTS 
		DECLARE @lSource GEOGRAPHY = GEOGRAPHY::Point(@pLatitude, @pLongitude, 4326),
			    @lTarget GEOGRAPHY = GEOGRAPHY::Point(@lLatitude, @lLongitude, 4326)	
				
		IF DATEDIFF(SECOND, @lInitStopTime, @pEventDate) >=  @lStopMinimumTime
		   AND (@lSource.STDistance(@lTarget) / 1000) <= @lStopMinimumDistance
		   AND @pSpeed <= @lStopMinimumSpeed
		BEGIN --Dentro de la parada
			--CLEAN DEFAULT
			DELETE FROM @lResult

			INSERT INTO @lResult
			SELECT DATEDIFF(SECOND, @lInitStopTime, @pEventDate),
				   CAST(1 AS BIT),
				   CAST(1 AS BIT)
		END
		ELSE IF (@lSource.STDistance(@lTarget) / 1000) <= @lStopMinimumDistance
		   AND @pSpeed <= @lStopMinimumSpeed
		BEGIN
			IF EXISTS (SELECT *
						FROM [Management].[UserStop]
						WHERE [EventDate] IS NOT NULL
						AND [UserId] = @pUserId
						AND [CustomerId] = @pCustomerId)
			BEGIN --Termina la parada
				DELETE FROM @lResult

				INSERT INTO @lResult
				SELECT DATEDIFF(SECOND, @lInitStopTime, @pEventDate),
					   CAST(1 AS BIT),
					   CAST(0 AS BIT)
			END	
			ELSE
			BEGIN -- Podria ser parada
				--CLEAN DEFAULT
				DELETE FROM @lResult

				INSERT INTO @lResult
				SELECT NULL,
					   CAST(0 AS BIT),
					   CAST(0 AS BIT)
			END
		END
		ELSE
		BEGIN
			IF EXISTS (SELECT *
						FROM [Management].[UserStop]
						WHERE [EventDate] IS NOT NULL
						AND [UserId] = @pUserId
						AND [CustomerId] = @pCustomerId)
			BEGIN --Termina parada
				DELETE FROM @lResult

				INSERT INTO @lResult
				SELECT DATEDIFF(SECOND, @lInitStopTime, @pEventDate),
					   CAST(1 AS BIT),
					   CAST(0 AS BIT)
			END	
			ELSE
			BEGIN -- No es parada
				--CLEAN DEFAULT
				DELETE FROM @lResult

				INSERT INTO @lResult
				SELECT NULL,
					   CAST(0 AS BIT),
					   CAST(1 AS BIT)
			END
		END


	--	IF DATEDIFF(SECOND, @lInitStopTime, @pEventDate) >  @lStopMinimumTime
	--	   AND (@lSource.STDistance(@lTarget) / 1000) < @lStopMinimumDistance
	--	   AND @pSpeed < @lStopMinimumSpeed
	--	BEGIN 
	--		--CLEAN DEFAULT
	--		DELETE FROM @lResult

	--		INSERT INTO @lResult
	--		SELECT NULL,
	--			   CAST(1 AS BIT),
	--			   CAST(1 AS BIT)
	--	END	
	--	ELSE IF EXISTS (
	--						SELECT *
	--						FROM [Management].[UserStop]
	--						WHERE [EventDate] IS NOT NULL
	--						AND [UserId] = @pUserId
	--						AND [CustomerId] = @pCustomerId
	--					)
	--	BEGIN 
	--		DELETE FROM @lResult

	--		INSERT INTO @lResult
	--		SELECT DATEDIFF(SECOND, @lInitStopTime, @pEventDate),
	--			   CAST(1 AS BIT),
	--			   CAST(0 AS BIT)
	--	END	
	--END 	
	--ELSE 
	--BEGIN 
	--	IF @lStopMinimumSpeed > @pSpeed
	--	BEGIN 
	--		INSERT INTO @lResult
	--		SELECT NULL,
	--			   CAST(0 AS BIT),
	--			   CAST(1 AS BIT)
	--	END 
	END 

	RETURN
END
	
