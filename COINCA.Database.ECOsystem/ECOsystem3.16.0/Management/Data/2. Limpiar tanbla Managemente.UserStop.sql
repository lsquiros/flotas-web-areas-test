USE [ECOsystem]
GO

-- ================================================================================
-- Author:       Marjorie Grabanzo Morales
-- Create date:  30/10/2018
-- Description:  Limpiar tabla para el calculo de paradas
-- ================================================================================

DELETE FROM [Management].[UserStop]