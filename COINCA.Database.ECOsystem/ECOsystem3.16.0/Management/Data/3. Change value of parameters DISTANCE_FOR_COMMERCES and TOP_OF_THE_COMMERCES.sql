--USE [ECOsystem]
GO

-- ================================================================================
-- Author:       Marjorie Garbanzo Morales
-- Create date:  02/11/2018
-- Description:  Change value of parameters DISTANCE_FOR_COMMERCES and TOP_OF_THE_COMMERCES
-- ================================================================================

UPDATE [dbo].[GeneralParameters]
SET [Value] = 200
    ,[NumericValue] = 200.00
WHERE ParameterId = 'DISTANCE_FOR_COMMERCES'

UPDATE [dbo].[GeneralParameters]
SET [Value] = 50
    ,[NumericValue] = 50.00
WHERE ParameterId = 'TOP_OF_THE_COMMERCES'