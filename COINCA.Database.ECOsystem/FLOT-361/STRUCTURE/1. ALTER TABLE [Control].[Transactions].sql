USE [ECOsystem]

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 05/03/2019
-- Description:	Add column to table transactions
-- ================================================================================================

ALTER TABLE [Control].[Transactions] 
ADD [IsDenied] BIT

ALTER TABLE [Control].[TransactionsHx]
ADD [IsDenied] BIT