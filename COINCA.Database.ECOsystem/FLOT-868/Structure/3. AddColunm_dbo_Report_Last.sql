USE ECOsystemDev
GO


-- =================================================
-- Author:		Juan Carlos Santamaria
-- Create date: 21-10-2019
-- Description:	alter table add column Status, On OFF Engine
-- =================================================
IF EXISTS(
		SELECT 1
		  FROM sys.objects a
	INNER JOIN sys.tables b on a.object_id = b.object_id
	INNER JOIN sys.columns c on b.object_id = c.object_id
		 WHERE a.name LIKE '%Report_Last%' and c.name = 'EngineStatus')
BEGIN
	ALTER TABLE dbo.Report_Last drop COLUMN  [EngineStatus]
END
GO

ALTER TABLE dbo.Report_Last ADD	EngineStatus bit 

GO

