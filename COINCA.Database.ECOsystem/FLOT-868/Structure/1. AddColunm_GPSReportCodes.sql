USE ECOsystemDev
go

-- =============================================
-- Author:      Juan Carlos Santamaria
-- Create Date: 2019-10-14
-- Description: Alter Table GPSReportCodes add column name ==> Name
-- =============================================
IF EXISTS(
		SELECT 1
		  FROM sys.objects a
	INNER JOIN sys.tables b on a.object_id = b.object_id
	INNER JOIN sys.columns c on b.object_id = c.object_id
		 WHERE a.name LIKE '%GPSReportCodes%' and c.name = 'Name')
BEGIN
	ALTER TABLE Efficiency.GPSReportCodes drop COLUMN  [Name]
END
GO


--- CREAMOS LA nueva columna.
	ALTER TABLE Efficiency.GPSReportCodes ADD [Name] VARCHAR(25) null;
GO

--- Insertamos los primeros valores
UPDATE Efficiency.GPSReportCodes
	SET [Name] = SUBSTRING([Description],1,25)
GO

UPDATE Efficiency.GPSReportCodes
SET  Name = 'Encendido' -- varchar
WHERE Value = 0

UPDATE Efficiency.GPSReportCodes
SET  Name = 'Apagado' -- varchar
WHERE Value = 1
go

SELECT * FROM Efficiency.GPSReportCodes

GO

