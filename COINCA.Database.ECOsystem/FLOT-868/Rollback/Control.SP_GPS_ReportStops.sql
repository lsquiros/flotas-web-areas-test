USE ECOsystemDev
GO

IF EXISTS(SELECT 1 FROM sys.objects WHERE name = 'SP_GPS_ReportStops'  and type = 'P')
	DROP PROC [Control].[SP_GPS_ReportStops]
GO 


-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 06/02/2017
-- Description:	Retrieve GPS Report stops information
-- Modify by Henry Retana - 25/04/2017
-- Description: Modification in the stop lapse time
-- Modify by:	Marco Cabrera
-- Description:	Add distance between events
-- Modify by: Albert EStrada  Description:	Low and high temperature, panic button Oct/23/2017
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to RepDateTime
-- Modify: Henry Retana - 17/07/2018
-- Add function to get the stops 
-- Stefano Quirós - Change the final date on GSPStops Details - 21/01/2019
-- ================================================================================================

ALTER PROCEDURE [Control].[SP_GPS_ReportStops] --28440, '04/01/2019 12:00:00 AM', '04/02/2019 12:00:00 AM', 1,0,0,0,0,0,0  --19277, '9/19/2018 12:00:00 AM', '9/19/2018 11:59:59 PM', 1,1,1,1,1,1,1
(
	@pVehicleId INT,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL,
	@pStopsDetail BIT,
	@pSuddenStops BIT,
	@pAbruptTurns BIT,
	@pQuickAccelerations BIT,
	@pHighTemperature BIT,
    @pLowTemperature BIT, 
    @pPanicButton BIT 
) 
AS
BEGIN
	DECLARE @vIterator INT,
			@vLastIterator INT = 1,
			@vDevice INT,
			@vDateIni DATETIME,
			@vDateFin DATETIME,
			@lTimeZoneParameter INT,
			@pIsAttrack INT = 0,
			@lStopDetail INT = NULL,
			@lSuddenStops INT = NULL,
			@lAbruptTurnsR INT = NULL,
			@lAbruptTurnsL INT = NULL,
			@lQuickAccelerations INT = NULL,
			@lHighTemperature INT = NULL,
			@lLowTemperature INT = NULL,
			@lPanicButton INT = NULL,
			@lStopTime INT = 2,
			@lCustomerId INT = NULL

	SELECT @pIsAttrack = COUNT(*) 
	FROM [General].[Vehicles]
	WHERE [VehicleId] = @pVehicleId
	AND [DeviceReference] IN (SELECT [Device]
							  FROM [dbo].[Devices] 
							  WHERE [UnitId] IN (SELECT [TerminalId]
												 FROM [dbo].[DispositivosAVL] 
												 WHERE [Modelo] = 18))
	
	SELECT @lStopTime = ISNULL([Value], 2)
	FROM [General].[ParametersByCustomer] p
	INNER JOIN [General].[Vehicles] v
		ON p.[CustomerId] = v.[CustomerId]
	WHERE v.[VehicleId] = @pVehicleId
	AND p.[Name] = 'VehicleStopLapse'
	AND p.[ResuourceKey] = 'VEHICLESTOPLAPSE'

	IF @pStopsDetail = 1 SELECT @lStopDetail = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'STOPDETAIL_PARAMETER'
	IF @pSuddenStops = 1 SELECT @lSuddenStops = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'SUDDENSTOPS_PARAMETER'
	IF @pAbruptTurns = 1 
	BEGIN
		SELECT TOP 1 @lAbruptTurnsR = [items] FROM [dbo].[Fn_Split]((SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'ABRUPTTURNS_PARAMETER'), ',') ORDER BY [items] DESC
		SELECT TOP 1 @lAbruptTurnsL = [items] FROM [dbo].[Fn_Split]((SELECT [Value] FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'ABRUPTTURNS_PARAMETER'), ',') ORDER BY [items] ASC
	END	
	IF @pQuickAccelerations = 1 SELECT @lQuickAccelerations = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'QUICKACCELERATIONS_PARAMETER'
	IF @pHighTemperature  = 1 SELECT @lHighTemperature = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'HIGT_TEMPERATURE'
    IF @pLowTemperature = 1 SELECT @lLowTemperature = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'LOW_TEMPERATURE'
    IF @pPanicButton  = 1 SELECT @lPanicButton = CONVERT(INT, [Value]) FROM [dbo].[GeneralParameters] WHERE [ParameterID] = 'PANIC_BUTTON'
														   
	CREATE TABLE #Data
	(
		[Id] INT IDENTITY NOT NULL,
		[RepDateTime] DATETIME,
		[Latitude] FLOAT,
		[Longitude] FLOAT,
		[Odometer] FLOAT,
		[InputStatus] TINYINT,
		[VSSSpeed] FLOAT, 
		[ReportId] INT
	)

	CREATE NONCLUSTERED INDEX [IX_Reports_ReportId] ON #Data
	(
		[ReportId] ASC
	)

	CREATE TABLE #Result
	(
		[Id] INT IDENTITY,
		[Evento] VARCHAR(20),
		[FechaInicial] DATETIME,
		[InputStatus] TINYINT,
		[Report] INT
	)
	
	CREATE TABLE #ResultStops
	(
		[Identifier] INT IDENTITY,
		[Id] INT,		
		[FechaInicial] DATETIME,
		[FechaFinal] DATETIME,
		[Latitud] FLOAT,
		[Longitud] FLOAT,
		[Report] INT,
		[ReportId] INT,
		[Odometer] FLOAT,
		[Distance] FLOAT
	)

	CREATE TABLE #ResultsOdometers
	(
		[RowNumber] INT,
		[Identifier] INT,
		[Id] INT,		
		[FechaInicial] DATETIME,
		[FechaFinal] DATETIME,
		[Latitud] FLOAT,
		[Longitud] FLOAT,
		[Report] INT,
		[ReportId] INT,
		[Odometer] FLOAT,
		[Distance] FLOAT
	)

	SELECT @vDevice = [DeviceReference],
		   @lCustomerId = [CustomerId]	 
	FROM [General].[Vehicles] 
	WHERE [VehicleId] = @pVehicleId

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	INNER JOIN [General].[Vehicles] v
		ON cu.[CustomerId] = v.[CustomerId]
	WHERE v.[VehicleId] = @pVehicleId		

	SET @vDateIni = DATEADD(hh,@lTimeZoneParameter * -1,@pStartDate)
	SET @vDateFin = DATEADD(hh,@lTimeZoneParameter * -1,@pEndDate)	
	
	IF(@vDateIni IS NOT NULL AND @vDateFin IS NOT NULL)
	BEGIN
		INSERT INTO #Data
		(
			[RepDateTime],
			[Latitude],
			[Longitude],
			[Odometer],
			[InputStatus],
			[VSSSpeed], 
			[ReportId]
		) 
		SELECT	[RepDateTime],
				[Latitude],
				[Longitude],
				[Odometer],
				[InputStatus],
				[VSSSpeed], 
				[ReportId]
		FROM [dbo].[Reports] 
		WHERE [Device] = @vDevice 
		AND [RepDateTime] BETWEEN @vDateIni AND @VDateFin
		ORDER BY [RepDateTime] ASC

		SELECT t1.[Id],
			   t1.[RepDateTime], 
			   t1.[InputStatus] 
		INTO #ChangeEvent 
		FROM #Data t1, #Data t2 
		WHERE t1.[Id] = (t2.[Id] + 1) 
		AND (t1.[InputStatus] % 2) != (t2.[InputStatus] % 2)
		
		SELECT @vIterator = MIN(id) FROM #ChangeEvent

		WHILE(@vIterator IS NOT NULL)
		BEGIN
			DECLARE @vIdentidad INT
			DECLARE @vIteratorStops INT
			DECLARE @vIteratorEnd INT
			DECLARE @vOdometer FLOAT
			DECLARE @vIdResults INT
			DECLARE @vDistance FLOAT

			INSERT INTO #Result
			(
				[FechaInicial]
			)
			SELECT MIN([RepDateTime])
			FROM #Data 
			WHERE [Id] BETWEEN @vLastIterator AND @vIterator						
			
			SET @vIdentidad = @@Identity

			SELECT @vOdometer = Odometer
			FROM #Data
			WHERE [Id] = @vLastIterator

			IF @pIsAttrack = 0
			BEGIN 
				--UPDATE VALIDATES IF THE STOP TIME IS HIGHER THAN 2 MINUTES
				IF @pStopsDetail = 1
					INSERT INTO #ResultStops 
					(					
						[Id],		
						[FechaInicial],
						[FechaFinal],
						[Latitud],
						[Longitud],
						[ReportId],
						[Odometer]
					)
					SELECT @vIdentidad, 
						   d.[RepDateTime],
						   ISNULL((SELECT TOP 1 [RepDateTime] 
								   FROM #Data 
								   WHERE ([Id] > d.[Id] AND [Id] BETWEEN @vLastIterator AND @vIterator) 
								   AND [ReportId] = 104
								   ORDER BY RepDateTime ASC), 
								   (SELECT TOP 1 [RepDateTime] 
								    FROM #Data WHERE [Id] = @vIterator)),
						   d.[Latitude], 
						   d.[Longitude], 
						   d.[ReportId], 
						   d.[Odometer]
					FROM #Data d
					WHERE  d.[ReportId] = @lStopDetail						
					AND d.[Id] BETWEEN @vLastIterator AND @vIterator
					AND DATEDIFF(MI, d.[RepDateTime], ISNULL((SELECT TOP 1 [RepDateTime] 
															  FROM #Data 
															  WHERE ([Id] > d.[Id] AND [Id] BETWEEN @vLastIterator AND @vIterator) 
															  AND [ReportId] = 104
															  ORDER BY RepDateTime ASC), 
															  (SELECT TOP 1 [RepDateTime] 
															   FROM #Data WHERE [Id] = @vIterator))) > @lStopTime

				IF @pSuddenStops = 1
					INSERT INTO #ResultStops 
					(					
						[Id],		
						[FechaInicial],
						[FechaFinal],
						[Latitud],
						[Longitud],
						[ReportId],
						[Odometer]
					)
					SELECT @vIdentidad, 
						   d.[RepDateTime],
						   NULL,
						   d.[Latitude], 
						   d.[Longitude], 
						   d.[ReportId], 
						   d.[Odometer]
					FROM #Data d
					WHERE  d.[ReportId] = @lSuddenStops
					AND d.[Id] BETWEEN @vLastIterator AND @vIterator

				IF @pAbruptTurns = 1
				BEGIN
					INSERT INTO #ResultStops 
					(					
						[Id],		
						[FechaInicial],
						[FechaFinal],
						[Latitud],
						[Longitud],
						[ReportId],
						[Odometer]
					)
					SELECT @vIdentidad, 
						   d.[RepDateTime],
						   NULL,
						   d.[Latitude], 
						   d.[Longitude], 
						   d.[ReportId], 
						   d.[Odometer]
					FROM #Data d
					WHERE  d.[ReportId] = @lAbruptTurnsR
					AND d.[Id] BETWEEN @vLastIterator AND @vIterator

					INSERT INTO #ResultStops 
					(					
						[Id],		
						[FechaInicial],
						[FechaFinal],
						[Latitud],
						[Longitud],
						[ReportId],
						[Odometer]
					)
					SELECT @vIdentidad, 
						   d.[RepDateTime],
						   NULL,
						   d.[Latitude], 
						   d.[Longitude], 
						   d.[ReportId], 
						   d.[Odometer]
					FROM #Data d
					WHERE  d.[ReportId] = @lAbruptTurnsL
					AND d.[Id] BETWEEN @vLastIterator AND @vIterator
				END

				IF @pQuickAccelerations = 1
					INSERT INTO #ResultStops 
					(					
						[Id],		
						[FechaInicial],
						[FechaFinal],
						[Latitud],
						[Longitud],
						[ReportId],
						[Odometer]
					)
					SELECT @vIdentidad, 
						   d.[RepDateTime],
						   NULL,
						   d.[Latitude], 
						   d.[Longitude], 
						   d.[ReportId], 
						   d.[Odometer]
					FROM #Data d
					WHERE  d.[ReportId] = @lQuickAccelerations
					AND d.[Id] BETWEEN @vLastIterator AND @vIterator
										
				----------------------------------------------------------------------------------------------------
				IF @pHighTemperature = 1
					INSERT INTO #ResultStops 
					(					
						[Id],		
						[FechaInicial],
						[FechaFinal],
						[Latitud],
						[Longitud],
						[ReportId],
						[Odometer]
					)
					SELECT @vIdentidad, 
						   d.[RepDateTime],
						   NULL,
						   d.[Latitude], 
						   d.[Longitude], 
						   d.[ReportId], 
						   d.[Odometer]
					FROM #Data d
					WHERE  d.[ReportId] = @lHighTemperature
					AND d.[Id] BETWEEN @vLastIterator AND @vIterator
					
				IF @pLowTemperature = 1
					INSERT INTO #ResultStops 
					(					
						[Id],		
						[FechaInicial],
						[FechaFinal],
						[Latitud],
						[Longitud],
						[ReportId],
						[Odometer]
					)
					SELECT @vIdentidad, 
						   d.[RepDateTime],
						   NULL,
						   d.[Latitude], 
						   d.[Longitude], 
						   d.[ReportId], 
						   d.[Odometer]
					FROM #Data d
					WHERE  d.[ReportId] = @lLowTemperature
					AND d.[Id] BETWEEN @vLastIterator AND @vIterator
										
				IF @pPanicButton  = 1
					INSERT INTO #ResultStops 
					(					
						[Id],		
						[FechaInicial],
						[FechaFinal],
						[Latitud],
						[Longitud],
						[ReportId],
						[Odometer]
					)
					SELECT @vIdentidad, 
						   d.[RepDateTime],
						   NULL,
						   d.[Latitude], 
						   d.[Longitude], 
						   d.[ReportId], 
						   d.[Odometer]
					FROM #Data d
					WHERE  d.[ReportId] = @lPanicButton
					AND d.[Id] BETWEEN @vLastIterator AND @vIterator					
			END
			ELSE 
			BEGIN
				INSERT INTO #ResultStops 
				(					
					[Id],		
					[FechaInicial],
					[FechaFinal],
					[Latitud],
					[Longitud],
					[Odometer]
				)
				SELECT	@vIdentidad, 
						d.[RepDateTime],
						(SELECT TOP 1 [RepDateTime] 
						 FROM #Data 
						 WHERE ([Id] > d.[Id] AND [Id] BETWEEN @vLastIterator AND @vIterator) 
						 AND [VSSSpeed] > 0),
						d.[Latitude], 
						d.[Longitude],
						d.[Odometer]
				FROM #Data d
				WHERE d.[VSSSpeed] = 0
				AND d.[Id] BETWEEN @vLastIterator AND @vIterator
			END

			INSERT INTO #ResultsOdometers
			SELECT ROW_NUMBER() OVER(ORDER BY [FechaInicial] ASC), 
				   R.*
			FROM #ResultStops R 
			WHERE [Id] = @vIdentidad 
			ORDER BY [FechaInicial] ASC

			SELECT @vIteratorEnd = MAX([RowNumber]), 
				   @vIteratorStops = MIN([RowNumber])
			FROM #ResultsOdometers

			IF @vIteratorStops <= @vIteratorEnd AND @vIteratorEnd IS NOT NULL
			BEGIN
				WHILE (@vIteratorStops <= @vIteratorEnd)
				BEGIN
					SELECT @vIdResults = [Identifier], 
						   @vDistance = [Odometer] - @vOdometer
					FROM #ResultsOdometers 
					WHERE [RowNumber] = @vIteratorStops

					UPDATE #ResultStops 
					SET [Distance] = @vDistance
					WHERE [Identifier] = @vIdResults

					SELECT @vOdometer = [Odometer]
					FROM #ResultsOdometers
					WHERE [RowNumber] = @vIteratorStops

					SET @vIteratorStops = @vIteratorStops + 1
				END
			END

			TRUNCATE TABLE #ResultsOdometers

			UPDATE #Result 
			SET #Result.[Evento] = v.[Evento]
			FROM (
					SELECT (CASE [InputStatus] % 2
								WHEN 0 
								THEN 1
								ELSE 2
							END) [Evento]
					FROM #ChangeEvent 
					WHERE [Id] = @vIterator
				) v
			WHERE #Result.[Id] = @vIdentidad

			DELETE FROM #ChangeEvent 
			WHERE [Id] = @vIterator

			SET @vLastIterator = @vIterator

			SELECT @vIterator = MIN([Id]) 
			FROM #ChangeEvent
		END
		
		SELECT @vIterator = MAX([Id]) 
		FROM #Data

		INSERT INTO #Result
		(
			[FechaInicial]
		)
		SELECT MIN([RepDateTime])
		FROM #Data 
		WHERE [Id] BETWEEN @vLastIterator AND @vIterator						
			
		SET @vIdentidad = @@Identity

		SELECT @vOdometer = Odometer
		FROM #Data
		WHERE [Id] = @vLastIterator

		IF @pIsAttrack = 0
		BEGIN 
			--UPDATE VALIDATES IF THE STOP TIME IS HIGHER THAN 2 MINUTES
			IF @pStopsDetail = 1
				INSERT INTO #ResultStops 
				(					
					[Id],		
					[FechaInicial],
					[FechaFinal],
					[Latitud],
					[Longitud],
					[ReportId],
					[Odometer]
				)
				SELECT @vIdentidad, 
						d.[RepDateTime],
						ISNULL((SELECT TOP 1 [RepDateTime] 
								FROM #Data 
								WHERE ([Id] > d.[Id] AND [Id] BETWEEN @vLastIterator AND @vIterator) 
								AND [ReportId] = 104
								ORDER BY RepDateTime ASC), 
								(SELECT TOP 1 [RepDateTime] 
								FROM #Data WHERE [Id] = @vIterator)),
						d.[Latitude], 
						d.[Longitude], 
						d.[ReportId], 
						d.[Odometer]
				FROM #Data d
				WHERE  d.[ReportId] = @lStopDetail						
				AND d.[Id] BETWEEN @vLastIterator AND @vIterator
				AND DATEDIFF(MI, d.[RepDateTime], ISNULL((SELECT TOP 1 [RepDateTime] 
															FROM #Data 
															WHERE ([Id] > d.[Id] AND [Id] BETWEEN @vLastIterator AND @vIterator) 
															AND [ReportId] = 104
															ORDER BY RepDateTime ASC), 
															(SELECT TOP 1 [RepDateTime] 
															FROM #Data WHERE [Id] = @vIterator))) > @lStopTime

			IF @pSuddenStops = 1
				INSERT INTO #ResultStops 
				(					
					[Id],		
					[FechaInicial],
					[FechaFinal],
					[Latitud],
					[Longitud],
					[ReportId],
					[Odometer]
				)
				SELECT @vIdentidad, 
						d.[RepDateTime],
						NULL,
						d.[Latitude], 
						d.[Longitude], 
						d.[ReportId], 
						d.[Odometer]
				FROM #Data d
				WHERE  d.[ReportId] = @lSuddenStops
				AND d.[Id] BETWEEN @vLastIterator AND @vIterator

			IF @pAbruptTurns = 1
			BEGIN
				INSERT INTO #ResultStops 
				(					
					[Id],		
					[FechaInicial],
					[FechaFinal],
					[Latitud],
					[Longitud],
					[ReportId],
					[Odometer]
				)
				SELECT @vIdentidad, 
						d.[RepDateTime],
						NULL,
						d.[Latitude], 
						d.[Longitude], 
						d.[ReportId], 
						d.[Odometer]
				FROM #Data d
				WHERE  d.[ReportId] = @lAbruptTurnsR
				AND d.[Id] BETWEEN @vLastIterator AND @vIterator

				INSERT INTO #ResultStops 
				(					
					[Id],		
					[FechaInicial],
					[FechaFinal],
					[Latitud],
					[Longitud],
					[ReportId],
					[Odometer]
				)
				SELECT @vIdentidad, 
						d.[RepDateTime],
						NULL,
						d.[Latitude], 
						d.[Longitude], 
						d.[ReportId], 
						d.[Odometer]
				FROM #Data d
				WHERE  d.[ReportId] = @lAbruptTurnsL
				AND d.[Id] BETWEEN @vLastIterator AND @vIterator
			END

			IF @pQuickAccelerations = 1
				INSERT INTO #ResultStops 
				(					
					[Id],		
					[FechaInicial],
					[FechaFinal],
					[Latitud],
					[Longitud],
					[ReportId],
					[Odometer]
				)
				SELECT @vIdentidad, 
						d.[RepDateTime],
						NULL,
						d.[Latitude], 
						d.[Longitude], 
						d.[ReportId], 
						d.[Odometer]
				FROM #Data d
				WHERE  d.[ReportId] = @lQuickAccelerations
				AND d.[Id] BETWEEN @vLastIterator AND @vIterator
										
			----------------------------------------------------------------------------------------------------
			IF @pHighTemperature = 1
				INSERT INTO #ResultStops 
				(					
					[Id],		
					[FechaInicial],
					[FechaFinal],
					[Latitud],
					[Longitud],
					[ReportId],
					[Odometer]
				)
				SELECT @vIdentidad, 
						d.[RepDateTime],
						NULL,
						d.[Latitude], 
						d.[Longitude], 
						d.[ReportId], 
						d.[Odometer]
				FROM #Data d
				WHERE  d.[ReportId] = @lHighTemperature
				AND d.[Id] BETWEEN @vLastIterator AND @vIterator
					
			IF @pLowTemperature = 1
				INSERT INTO #ResultStops 
				(					
					[Id],		
					[FechaInicial],
					[FechaFinal],
					[Latitud],
					[Longitud],
					[ReportId],
					[Odometer]
				)
				SELECT @vIdentidad, 
						d.[RepDateTime],
						NULL,
						d.[Latitude], 
						d.[Longitude], 
						d.[ReportId], 
						d.[Odometer]
				FROM #Data d
				WHERE  d.[ReportId] = @lLowTemperature
				AND d.[Id] BETWEEN @vLastIterator AND @vIterator
										
			IF @pPanicButton  = 1
				INSERT INTO #ResultStops 
				(					
					[Id],		
					[FechaInicial],
					[FechaFinal],
					[Latitud],
					[Longitud],
					[ReportId],
					[Odometer]
				)
				SELECT @vIdentidad, 
						d.[RepDateTime],
						NULL,
						d.[Latitude], 
						d.[Longitude], 
						d.[ReportId], 
						d.[Odometer]
				FROM #Data d
				WHERE  d.[ReportId] = @lPanicButton
				AND d.[Id] BETWEEN @vLastIterator AND @vIterator					
		END
		ELSE 
		BEGIN
			INSERT INTO #ResultStops 
			(					
				[Id],		
				[FechaInicial],
				[FechaFinal],
				[Latitud],
				[Longitud],
				[Odometer]
			)
			SELECT	@vIdentidad, 
					d.[RepDateTime],
					(SELECT TOP 1 [RepDateTime] 
						FROM #Data 
						WHERE ([Id] > d.[Id] AND [Id] BETWEEN @vLastIterator AND @vIterator) 
						AND [VSSSpeed] > 0),
					d.[Latitude], 
					d.[Longitude],
					d.[Odometer]
			FROM #Data d
			WHERE d.[VSSSpeed] = 0
			AND d.[Id] BETWEEN @vLastIterator AND @vIterator
		END

		INSERT INTO #ResultsOdometers
		SELECT ROW_NUMBER() OVER(ORDER BY [FechaInicial] ASC), 
				R.*
		FROM #ResultStops R 
		WHERE [Id] = @vIdentidad 
		ORDER BY [FechaInicial] ASC

		SELECT @vIteratorEnd = MAX([RowNumber]), 
				@vIteratorStops = MIN([RowNumber])
		FROM #ResultsOdometers

		IF @vIteratorStops <= @vIteratorEnd AND @vIteratorEnd IS NOT NULL
		BEGIN
			WHILE (@vIteratorStops <= @vIteratorEnd)
			BEGIN
				SELECT @vIdResults = [Identifier], 
					   @vDistance = [Odometer] - @vOdometer
				FROM #ResultsOdometers 
				WHERE [RowNumber] = @vIteratorStops

				UPDATE #ResultStops 
				SET [Distance] = @vDistance
				WHERE [Identifier] = @vIdResults

				SELECT @vOdometer = [Odometer]
				FROM #ResultsOdometers
				WHERE [RowNumber] = @vIteratorStops

				SET @vIteratorStops = @vIteratorStops + 1
			END
		END

		TRUNCATE TABLE #ResultsOdometers

		UPDATE #Result 
		SET #Result.[Evento] = v.[Evento]
		FROM (
				SELECT (CASE [InputStatus] % 2
							WHEN 0 
							THEN 1
							ELSE 2
						END) [Evento]
				FROM #ChangeEvent 
				WHERE [Id] = @vIterator
			) v
		WHERE #Result.[Id] = @vIdentidad
	END	

	--IF (SELECT COUNT(1)
	--	FROM #ResultStops) > 1
	--BEGIN
	--	UPDATE #ResultStops
	--	SET [FechaInicial] = (SELECT TOP 1 [FechaInicial]
	--						  FROM #Result
	--						  ORDER BY [Id] DESC)
	--	WHERE [FechaInicial] = (SELECT TOP 1 [FechaInicial]
	--							FROM #ResultStops
	--							ORDER BY [FechaInicial] DESC)
	--END 

	SELECT r.[Id] [GPSId]
		  ,DATEADD(hh, @lTimeZoneParameter, r.[FechaInicial]) [StartDate]
	      ,DATEADD(hh, @lTimeZoneParameter, r.[FechaFinal]) [EndDate]
		  ,r.[Latitud]
		  ,r.[Longitud]
		  ,CASE r.[ReportId] 
				WHEN @lStopDetail THEN 'Parada' 
				WHEN @lSuddenStops THEN 'Parada Brusca' 
				WHEN @lAbruptTurnsR THEN 'Giro Abrupto' 
				WHEN @lAbruptTurnsL THEN 'Giro Abrupto' 
				WHEN @lQuickAccelerations THEN 'Aceleración Repentina' 
				WHEN @lHighTemperature THEN 'Exceso de Temperatura' 
				WHEN @lLowTemperature  THEN 'Nivel Bajo de Temperatura' 
				WHEN @lPanicButton   THEN 'Botón de Pánico' 
		   END [Status]
		  ,r.[ReportId]
		  ,0 [MaxSpeed]
		  ,ROUND(r.[Distance],3) [Kilometers]
		  ,0 [Stops]
		  ,0 [SuddenStops]
		  ,0 [AbruptTurns]
		  ,0 [QuickAccelerations]
		  ,0 [LowTemperature]
		  ,0 [HighTemperature]
		  ,0 [PanicButton]
		  ,ISNULL([Efficiency].[Fn_GetAddressByLatLon_Retrieve](r.[Longitud], r.[Latitud], @lCustomerId), '') [Location]	   
	FROM #Result d
	INNER JOIN #ResultStops r
		ON r.[Id] = d.[Id]
	WHERE d.[Evento] = 1
	ORDER BY r.[FechaInicial]

	DROP TABLE #Data
	DROP TABLE #ChangeEvent
	DROP TABLE #Result
	DROP TABLE #ResultStops
	DROP TABLE #ResultsOdometers
END