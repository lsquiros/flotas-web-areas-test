USE ECOsystemDev
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name LIKE 'SP_VehicleReconstructing_Retrieve' AND type = 'P')
	DROP PROC [Efficiency].SP_VehicleReconstructing_Retrieve
GO 

-- ================================================================================================      
-- Author:  Henry Retana      
-- Create date: 01/03/2017      
-- Description: Retrieve Vehicle Reconstructing      
-- Modify the Summary Report, Henry Retana - 1/6/2017      
-- Modify: Creacion de rutas desde [Odometer], Esteban Solis, 2017-10-12  
-- Modify: Se agregan parametros de distancia minima y tiempo minimo de parada, Esteban Solis, 2017-12-28  
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to RepDateTime
-- Modify: Henry Retana - 02/05/2019 - Set the dates to midnight
-- Modify: Henry Retana - 14/06/2019 - Validate and check last event status
-- ================================================================================================      

ALTER PROCEDURE [Efficiency].[SP_VehicleReconstructing_Retrieve]
(
	 @pVehicleId INT
	,@pStartDate DATETIME
	,@pEndDate DATETIME = NULL
	,@pCustomerId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @vIterator INT
		   ,@vLastIterator INT = 1
		   ,@vDevice INT
		   ,@vDateIni DATETIME
		   ,@vDateFin DATETIME
		   ,@lTimeZoneParameter INT
		   ,@pCounter INT
		   ,@pIsAttrack INT = 0
		   ,@lStopDetail INT = NULL
		   ,@lMinimumDistance DECIMAL(16)
		   ,@lMinimumStopTime DECIMAL(16)
		   ,@lCustomerId INT = NULL

	-- SET DATES IN THE CORRECT FORMAT
	SET @pStartDate = DATEADD(DAY, DATEDIFF(DAY, 0, @pStartDate), 0)
	SET @pEndDate = ISNULL(@pEndDate, DATEADD(DAY, 1, @pStartDate))

	SELECT @pIsAttrack = COUNT(*)
	FROM [General].[Vehicles]
	WHERE [VehicleId] = @pVehicleId
	AND [DeviceReference] IN 
	(
		SELECT [Device]
		FROM [dbo].[Devices]
		WHERE [UnitId] IN
		(
			SELECT [TerminalId]
			FROM [dbo].[DispositivosAVL]
			WHERE [Modelo] = 18
		)
	)

	SELECT @lStopDetail = CONVERT(INT, [Value])
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'STOPDETAIL_PARAMETER'

	CREATE TABLE #Data 
	(
		 [Id] INT IDENTITY NOT NULL
		,[RepDatetime] DATETIME
		,[Latitude] FLOAT
		,[Longitude] FLOAT
		,[Odometer] FLOAT
		,[InputStatus] TINYINT
		,[VSSSpeed] FLOAT
		,[ReportId] INT
	)

	CREATE TABLE #Result 
	(
		 [Id] INT IDENTITY
		,[Event] VARCHAR(20)
		,[StartDate] DATETIME
		,[EndDate] DATETIME
		,[Latitude] FLOAT
		,[Longitude] FLOAT
		,[Odometer] FLOAT
		,[MaxSpeed] FLOAT
		,[Stops] INT
	)

	SELECT @vDevice = [DeviceReference],
		   @lCustomerId = [CustomerId]
	FROM [General].[Vehicles]
	WHERE [VehicleId] = @pVehicleId

	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu 
		ON co.[CountryId] = cu.[CountryId]
	INNER JOIN [General].[Vehicles] v 
		ON cu.[CustomerId] = v.[CustomerId]
	WHERE v.[VehicleId] = @pVehicleId

	SET @vDateIni = DATEADD(hh, @lTimeZoneParameter * - 1, @pStartDate)
	SET @vDateFin = DATEADD(hh, @lTimeZoneParameter * - 1, @pEndDate)

	IF (@vDateIni IS NOT NULL AND @vDateFin IS NOT NULL)
	BEGIN
		INSERT INTO #Data 
		(
			 [RepDatetime]
			,[Latitude]
			,[Longitude]
			,[Odometer]
			,[InputStatus]
			,[VssSpeed]
			,[ReportId]
		)
		SELECT [RepDatetime]
			  ,[Latitude]
			  ,[Longitude]
			  ,[Odometer]
			  ,[InputStatus]
			  ,[vssspeed]
			  ,[ReportId]
		FROM [dbo].[Reports]
		WHERE [Device] = @vDevice
		AND [RepDatetime] BETWEEN @vDateIni AND @VDateFin
		ORDER BY [RepDatetime] ASC
				
		SELECT t1.[Id]
			  ,t1.[RepDatetime]
			  ,t1.[Latitude]
			  ,t1.[Longitude]
			  ,t1.[InputStatus]
		INTO #ChangeEvent
		FROM #Data t1
			,#Data t2
		WHERE t1.[Id] = (t2.[Id] + 1)
		AND (t1.[InputStatus] % 2) != (t2.[InputStatus] % 2)

		SELECT @vIterator = MIN([Id])
		FROM #ChangeEvent

		WHILE (@vIterator IS NOT NULL)
		BEGIN
			DECLARE @vIdentidad INT

			INSERT INTO #Result 
			(
				 [StartDate]
				,[EndDate]
				,[Odometer]
				,[MaxSpeed]
			)
			SELECT MIN([RepDatetime])
				  ,MAX([RepDatetime])
				  ,(MAX([Odometer]) - MIN([Odometer]))
				  ,MAX([VSSSpeed])
			FROM #Data
			WHERE [Id] BETWEEN @vLastIterator AND @vIterator

			SET @vIdentidad = @@Identity

			IF @pIsAttrack = 0
			BEGIN
				UPDATE #Result
				SET [Stops] = v.[Stops]
				FROM 
				(
					SELECT COUNT([VSSSpeed]) [Stops]
					FROM #Data
					WHERE [ReportId] = @lStopDetail
					AND [Id] BETWEEN @vLastIterator AND @vIterator
				) v
				WHERE [Id] = @vIdentidad
			END
			ELSE
			BEGIN
				UPDATE #Result
				SET [Stops] = v.[Stops]
				FROM 
				(
					SELECT COUNT([VSSSpeed]) Stops
					FROM #Data
					WHERE [VSSSpeed] = 0
					AND [Id] BETWEEN @vLastIterator AND @vIterator
				) v
				WHERE [Id] = @vIdentidad
			END

			UPDATE #Result
			SET  [Event] = v.[Event]
				,[Latitude] = v.[Latitude]
				,[Longitude] = v.[Longitude]
			FROM 
			(
				SELECT CASE WHEN [InputStatus] % 2 = 0
							THEN 'Encendido'
							ELSE 'Apagado'
					   END [Event]
					  ,[Latitude]
					  ,[Longitude]
				FROM #ChangeEvent
				WHERE [Id] = @vIterator
			) v
			WHERE [Id] = @vIdentidad

			DELETE FROM #ChangeEvent
			WHERE [Id] = @vIterator

			SET @vLastIterator = @vIterator

			SELECT @vIterator = MIN([Id])
			FROM #ChangeEvent
		END

		SELECT @vIterator = MAX([Id])
		FROM #Data

		IF EXISTS (
					 SELECT *
					 FROM #Data
				  )
		BEGIN
			INSERT INTO #Result 
			(
				 [StartDate]
				,[EndDate]
				,[Odometer]
				,[MaxSpeed]
			)
			SELECT MIN([RepDatetime])
				  ,NULL
				  ,(MAX([Odometer]) - MIN([Odometer]))
				  ,MAX([VSSSpeed])
			FROM #Data
			WHERE [Id] BETWEEN @vLastIterator AND @vIterator
		END

		SET @vIdentidad = @@Identity

		IF @pIsAttrack = 0
		BEGIN
			UPDATE #Result
			SET [Stops] = v.[Stops]
			FROM 
			(
				SELECT COUNT([VSSSpeed]) [Stops]
				FROM #Data
				WHERE [ReportId] = @lStopDetail
				AND [Id] BETWEEN @vLastIterator AND @vIterator
			) v
			WHERE [Id] = @vIdentidad
		END
		ELSE
		BEGIN
			UPDATE #Result
			SET [Stops] = v.[Stops]
			FROM 
			(
				SELECT COUNT([VSSSpeed]) Stops
				FROM #Data
				WHERE [VSSSpeed] = 0
				AND [Id] BETWEEN @vLastIterator AND @vIterator
			) v
			WHERE [Id] = @vIdentidad
		END
	
		UPDATE #Result
		SET  [Event] = v.[Event]
			,[Latitude] = v.[Latitude]
			,[Longitude] = v.[Longitude]
		FROM 
		(
			SELECT CASE WHEN [InputStatus] % 2 = 1
						THEN 'Encendido'
						ELSE 'Apagado'
					END [Event]
					,[Latitude]
					,[Longitude]
			FROM #Data
			WHERE [Id] = @vIterator
		) v
		WHERE [Id] = @vIdentidad
	END

	UPDATE #Result
	SET [Stops] = 0
	   ,[MaxSpeed] = 0
	WHERE [Event] = 'Apagado'

	DROP TABLE #Data
	DROP TABLE #ChangeEvent

	SET @lMinimumDistance = 
	(
			SELECT pc.[Value]
			FROM [General].[Vehicles] v
			LEFT JOIN [General].[ParametersByCustomer] pc 
				ON pc.[CustomerId] = v.[CustomerId]
			WHERE v.[VehicleId] = @pVehicleId
			AND pc.[Name] = 'MinimumDistanceBetweenPoints'
	)
	SET @lMinimumStopTime = 
	(
			SELECT pc.[Value]
			FROM [General].[Vehicles] v
			LEFT JOIN [General].[ParametersByCustomer] pc 
				ON pc.[CustomerId] = v.[CustomerId]
			WHERE v.[VehicleId] = @pVehicleId
			AND pc.[Name] = 'MinimumStopTime'
	)

	SELECT [Id] [GPSId]
		  ,@pVehicleId [VehicleId]
		  ,DATEADD(hh, @lTimeZoneParameter, [StartDate]) AS StartDate
		  ,[StartDate] [StartDateUTC]
		  ,ISNULL(DATEADD(hh, @lTimeZoneParameter, [EndDate]), 
				  CASE WHEN CAST(@pStartDate AS DATE) = CAST(DATEADD(HOUR, -6, GETDATE()) AS DATE) 
		  			   THEN DATEADD(HOUR, -6, GETDATE()) 
		  			   ELSE @pEndDate
		  		  END) [EndDate]
		  ,ISNULL([EndDate], 
		  		  CASE WHEN CAST(@pStartDate AS DATE) = CAST(DATEADD(HOUR, -6, GETDATE()) AS DATE) 
		  			   THEN GETDATE() 
		  		       ELSE DATEADD(hh, @lTimeZoneParameter * -1, @pEndDate)
		  	      END) [EndDateUTC]
		  ,CASE WHEN [Event] = 'Encendido'
		  		THEN ISNULL([Efficiency].[Fn_GetOverSpeedSummaryByVehicle](@pVehicleId, [StartDate], [EndDate], 0), '')
		  	    ELSE ''
		   END [OverSpeedSummary]
		  ,[Event] [Status]
		  ,CONVERT(INT, [MaxSpeed]) [MaxSpeed]
		  ,[Odometer]
		  ,[Stops]
		  ,ISNULL([Efficiency].[Fn_GetAddressByLatLon_Retrieve]([Longitude], [Latitude], @lCustomerId), '') [Location]
		  ,CONVERT(VARCHAR(256), [Longitude]) [Longitud]
		  ,CONVERT(VARCHAR(256), [Latitude]) [Latitud]
		  ,ISNULL(@lMinimumDistance, 0) [MinimumDistanceBetweenPoints]
		  ,ISNULL(@lMinimumStopTime, 0) [MinimumStopTime]
	FROM #Result	
	ORDER BY 3

	DROP TABLE #Result

	SET NOCOUNT OFF
END

