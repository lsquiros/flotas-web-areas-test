USE ECOsystemDev
GO

 IF EXISTS(SELECT 1 FROM SYS.OBJECTS WHERE NAME = 'ProcessReport' AND TYPE = 'P')
	DROP PROC [dbo].[ProcessReport]
GO

-- ==================================================================================================================================================================================================
-- Author:		Marco Cabrera - 23/05/2017 - The ReportId in case of be in the table General. Types AND GeneralParameters generate a mail alerting the client
-- Modify By:   Stefano Quirós -- Add the insert on last driver to a vehicle, and re-order the sp to look better -- 
-- Modify Date: 05/09/2017
-- Modify By: 	Gerald Solano
-- Modify Date: 15/09/2017
-- Description: Se agrega filtro para capturar los ReportId devueltos después de ejecutar un comando
-- Modify By:   Stefano Quirós - Add the GPSDateTime to temperature table
-- Modify By: 	Esteban Solís - 30/11/2017 - Description: Se excluye el report id 33 del envio de alarma en tipo real
-- Modify By:   Jose Ramirez  - 31/01/2018 - Envio de voltaje de bateria
-- Modify By: 	Gerald Solano
-- Modify Date: 08/02/2018
-- Description: * Se inhabilita / comenta lógica para evitar registrar datos que no se usan en las plataformas
--              * Se solicita dejar la misma fecha de RepDateTime en el campo RTCDateTime en la tabla Reports / Report_Last solicitado por J.Chaves
--				* Se agrega la inserción de alarmas en una tabla llamada [General].[GPS_Alarms], esta tabla tendrá un trigger que permitirá realizar 
--				  más logica de negocio en las alarmas (ejem: Validar alarmas duplicadas)
-- Modify By:   Henry Retana - 20/12/2018 - Add column report to the [dbo].[Report_last] table
-- Modify By:   Gerald Solano - 25/04/2019 - Modify Report Last validation - Comment logic when validate the speed and ignition
-- Modify By:   Henry Retana/Marjorie Garbanzo - 15/05/2019 - Validates Travel time in the reports
-- ==================================================================================================================================================================================================

CREATE PROCEDURE [dbo].[ProcessReport]
(
	@dUnitID AS DECIMAL(20,0)
   ,@dtGPSdatetime AS DATETIME
   ,@dtRTCdatetime AS DATETIME
   ,@dtRepdatetime AS DATETIME
   ,@fLongitude AS FLOAT
   ,@fLatitude AS FLOAT
   ,@fHeading AS FLOAT
   ,@iReportID AS INT
   ,@fOdometer AS FLOAT
   ,@fHDOP AS FLOAT
   ,@tAllInputs AS TINYINT
   ,@tAllOutputs AS TINYINT
   ,@fVSSSpeed AS FLOAT
   ,@fAnalogInput AS FLOAT
   ,@sDriverID AS VARCHAR(16) = ''
   ,@fTemperature1 AS FLOAT = 2000
   ,@fTemperature2 AS FLOAT = 2000
   ,@iMCC AS INT = 0
   ,@iMNC AS INT = 0
   ,@iLAC AS INT = 0
   ,@iCellID AS INT = 0
   ,@iRXLevel AS INT = 0
   ,@iGSMQ AS INT = 0
   ,@iGSMS AS INT = 0
   ,@ISatellites AS INT = 0
   ,@bRealTime AS BIT = 0
   ,@fMainVolt AS FLOAT = NULL
   ,@fBatVolt AS FLOAT
   ,@iRPM AS INT = NULL
   ,@sNeighboring AS VARCHAR(72) = ''
   ,@sTextMessage AS VARCHAR(1024) = ''
   ,@iOBDMalfunctionIndLamp AS INT = NULL
   ,@fOBDFuelUsed AS FLOAT = NULL
   ,@iOBDFuelLevel AS INT = NULL
   ,@iOBDEngineRPM AS INT = NULL
   ,@iOBDEngineLoad AS INT = NULL
   ,@iOBDAccelerator AS INT = NULL
   ,@iOBDEngineCoolantTemp AS INT = NULL
   ,@fOBDMassAirFlowRate AS FLOAT = NULL
   ,@sOBDVIN AS VARCHAR(19) = NULL
   ,@pTempAccum10 DECIMAL(10,4)  =  NULL
   ,@pTempAccum11 DECIMAL(10,4)  = NULL
   ,@pTempAccum12 DECIMAL(10,4)  = NULL
   ,@pTempAccum13 DECIMAL(10,4)  = NULL
   ,@pTempAccum14 DECIMAL(10,4)  = NULL
   ,@pTempAccum15 DECIMAL(10,4)  = NULL
   ,@pTempAccum16 DECIMAL(10,4)  = NULL
   ,@pTempAccum17 DECIMAL(10,4)  = NULL

) 
AS 
BEGIN

	SET NOCOUNT ON

	DECLARE @iDevice AS INT
	       ,@sUnitID AS VARCHAR(20)
		   ,@vsDriverID AS VARCHAR(16)
		   ,@vfTemperature1 AS FLOAT
		   ,@vfTemperature2 AS FLOAT
		   ,@viMCC AS INT
		   ,@viMNC AS INT
		   ,@viLAC AS INT
		   ,@viCellID AS INT
		   ,@viRXLevel AS INT
		   ,@viGSMQ AS INT
		   ,@viGSMS AS INT
		   ,@viRegIStro AS BIGINT
		   ,@viCantidad AS INT
		   ,@vdtGPSdatetime AS DATETIME
		   ,@vbActualizarTexto AS BIT
		   ,@iRPMCalc AS BIT
		   ,@dFuelVolumeGalCalc AS FLOAT
		   ,@viDeviceStatus AS INT
		   ,@cTRANsacciON AS VARCHAR(16)
		   ,@RepORtJammingId AS INT
		   ,@VehiculoID AS INT
		   ,@HasCooler AS BIT
		   ,@TempSensorCount AS INT
		   ,@PromedioTemp AS FLOAT = NULL
		   ,@TempMax AS DECIMAL(18,2)
		   ,@TempMin AS DECIMAL(18,2)
		   ,@pSensor  VARCHAR(20) = NULL
		   ,@pLimiteTemperatura DECIMAL(18,2)
		    
	CREATE TABLE #TempSensor 
	(
		[idtemp] INT IDENTITY(1,1)
	   ,[idAcumulador] INT
	   ,[temperatura] FLOAT  NULL
	   ,[GPSDateTime] DATETIME
	)
						
	--Se extrae el número de repORte que se genera pOR commANDo 70
	SELECT @RepORtJammingId = [ValorNumerico] 
	FROM [dbo].[Parametros]
	WHERE [ParametroID] = 'RPT-JAMMER'

	EXEC [dbo].[UnitIDLookUp] @dUnitID = @dUnitID, @iDevice = @iDevice OUTPUT
	
	-- Se validan las temperaturas 
	SELECT TOP(1) @HasCooler = ISNULL([HasCooler], 0), 
				  @TempSensorCount = ISNULL([TempSensorCount], 0), 
				  @TempMax = [MaxTemperature], 
				  @TempMin = [MinTemperature] 
	FROM [General].[Vehicles] 
	WHERE [DeviceReference] = @iDevice

	IF (@HasCooler = 1)
	BEGIN		
		INSERT INTO #TempSensor 
		(
			[idAcumulador], 
			[temperatura], 
			[GPSDateTime]
		)
		SELECT 10, @pTempAccum10, @dtGPSdatetime  
		
		INSERT INTO #TempSensor 
		(
			[idAcumulador], 
			[temperatura], 
			[GPSDateTime]
		)
		SELECT 11, @pTempAccum11, @dtGPSdatetime  

		INSERT INTO #TempSensor 
		(
			[idAcumulador], 
			[temperatura], 
			[GPSDateTime]
		)
		SELECT 12, @pTempAccum12, @dtGPSdatetime

		INSERT INTO #TempSensor 
		(
			[idAcumulador], 
			[temperatura], 
			[GPSDateTime]
		)
		SELECT 13, @pTempAccum13, @dtGPSdatetime

		INSERT INTO #TempSensor 
		(
			[idAcumulador], 
			[temperatura], 
			[GPSDateTime]
		)
		SELECT 14, @pTempAccum14, @dtGPSdatetime

		INSERT INTO #TempSensor 
		(
			[idAcumulador], 
			[temperatura], 
			[GPSDateTime]
		)
		SELECT 15, @pTempAccum15, @dtGPSdatetime

		INSERT INTO #TempSensor 
		(
			[idAcumulador], 
			[temperatura], 
			[GPSDateTime]
		)
		SELECT 16, @pTempAccum16, @dtGPSdatetime 

		INSERT INTO #TempSensor 
		(
			[idAcumulador], 
			[temperatura], 
			[GPSDateTime]
		)
		SELECT 17, @pTempAccum17, @dtGPSdatetime
		
		DELETE #TempSensor 
		WHERE idtemp > @TempSensorCount
	
		IF EXISTS (
					SELECT * 
					FROM #TempSensor 
					WHERE temperatura LIKE '%999%'
				  )
		BEGIN 
			SET @fTemperature1 = 999

			SELECT @pSensor = COALESCE(@pSensor + ', ', '') + CONVERT(NVARCHAR,idtemp) 
			FROM #TempSensor

			SET @iReportID = 33
		END
		ELSE
		BEGIN	
			SET @PromedioTemp = (
									SELECT SUM(temperatura) 
									FROM #TempSensor 
									WHERE temperatura IS NOT NULL
								)
				
			IF @PromedioTemp IS NOT NULL
			BEGIN 
				SET @fTemperature1 = (@PromedioTemp / @TempSensorCount)	
						
				IF @TempMax IS NOT NULL
				BEGIN
					IF	(@fTemperature1 > @TempMax)
					BEGIN 
						SET @iReportID = 31
						SET @pLimiteTemperatura = @TempMax
					END
				END

				IF @TempMax IS NOT NULL
				BEGIN
					IF (@fTemperature1 < @TempMin)
					BEGIN
						SET @iReportID = 32
						SET @pLimiteTemperatura = @TempMin
					END
				END
			END
			ELSE
			BEGIN 
				SET @fTemperature1 = 999

				SELECT @pSensor = COALESCE(@pSensor + ', ', '')+ CONVERT(NVARCHAR,idtemp) 
				FROM #TempSensor

				SET @iReportID = 33
			END
		END
	END
	ELSE
	BEGIN 	
		SET @fTemperature1 = 0
	END
	
	----------------------------------------------

	--Validamos si el GPS tiene un Device asignado
	IF ISNULL(@iDevice,0) > 0
	BEGIN
		SET @cTRANsacciON = 'T' + CONVERT(VARCHAR(16), @iDevice) + 'T' + REPLACE(CONVERT(VARCHAR(24),GETDATE(),114),':','')

		BEGIN TRAN @cTRANsacciON

		SELECT @vsDriverID = NULL
		      ,@vfTemperature1 = NULL
			  ,@vfTemperature2 = NULL
			  ,@viMCC = NULL
			  ,@viMNC = NULL
			  ,@viLAC = NULL
			  ,@viCellID = NULL
			  ,@viRXLevel = NULL
			  ,@viGSMQ = NULL
			  ,@viGSMS = NULL

		IF LEN(LTRIM(RTRIM(@sDriverID))) > 0
		BEGIN
			SET @vsDriverID = @sDriverID
		END

		IF @fTemperature2 <> 2000
		BEGIN
			SET @vfTemperature2 = @fTemperature2
		END

		SET @vfTemperature1 = @fTemperature1

		IF @iMCC <= 0 EXEC [dbo].[GetCountryCodeGSM] @pDevice = @dUnitID, @pGSMCode = @iMCC OUTPUT

		IF @iMCC > 0 AND @iMNC > 0
		BEGIN
			SELECT @viMCC = @iMCC
			      ,@viMNC = @iMNC
				  ,@viGSMQ = @iGSMQ
				  ,@viGSMS = @iGSMS
		END

		IF @iLAC > 0
		BEGIN
			SELECT @viLAC = @iLAC
		END

		IF @iCellID > 0
		BEGIN
			SELECT @viCellID = @iCellID
		END

		IF @iRXLevel >= -111 AND @iRXLevel <= -48
		BEGIN
			SELECT @viRXLevel = @iRXLevel
		END
		
		--Se comenta debido a que se requiere saber que hay un estado de Remolcado.
		--IF @fVSSSpeed >= 10 AND @tAllInputs%2 = 0
		--BEGIN
		--	SET @fVSSSpeed = 0
		--END
		
		IF (@iReportID = @RepORtJammingId)
		BEGIN
			DECLARE @fechaUltimORegIStro AS DATETIME
			       ,@hORas AS INT
				   ,@paramRepeatAlarm AS INT
				   ,@dISpositivo AS INT

			SELECT DISTINCT @dISpositivo = [DISpositivo]
			FROM  [dbo].[DISpositivosAVL] 
			WHERE [TerminalId] = CONVERT(VARCHAR,@dUnitID)

			SELECT @fechaUltimORegIStro = [FechaHORaAlarma] 
			FROM [dbo].[Alarmas] 
			WHERE [DISpositivoAVLId] = @dISpositivo
			
			SET @hORas = DATEDIFF(HOUR,@fechaUltimORegIStro, GETDATE())

			SELECT @paramRepeatAlarm = [ValORNumerico]
			FROM [dbo].[Parametros]
			WHERE [ParametroID] = 'REPEATALAMR-JAMMER'

			IF @hORas > @paramRepeatAlarm OR @hORas IS NULL 
			BEGIN
				--Se extrae el valOR del vehiculo
				SELECT DISTINCT @VehiculoID = c.[Vehiculo]
				FROM  [DISpositivosAVL] d 
				INNER JOIN ComposiciONes c 
					ON c.[dISpositivo] = d.[dISpositivo] 
				WHERE d.terminalID = CONVERT(VARCHAR,@dUnitID)
				
				--Se validan los valores que no pueden ser NULL
				IF(@VehiculoID IS NOT NULL AND @dISpositivo IS NOT NULL)
				BEGIN		
					--Se inserta el valOR en alarmas y activa al alarma.
					INSERT INTO [Alarmas] 
					(
						[FechaHORaAlarma]
					   ,[VehiculoId]
					   ,[DISpositivoAVLId]
					   ,[HDOP]
					   ,[TipoAlarmaId]
					   ,[ObservaciONes]
					   ,[Latitud]
					   ,[LONgitud]
					   ,[Activa]
					   ,[FechaDesactivaciON]
					)
					VALUES
					(
						GETDATE()
					   ,@VehiculoID
					   ,@dISpositivo
					   ,@fHDOP
					   ,1
					   ,''
					   ,@fLatitude
					   ,@fLongitude
					   ,1
					   ,GETDATE()
					)
				END
			END
		END
		
		-- Se insertan los Saltos de AVL
		IF @fHDOP >= 20
		BEGIN
			INSERT INTO [SaltosAVL]
			(
				[Device]
			   ,[GPSDateTime]
			   ,[RTCDateTime]
			   ,[RepDateTime]
			   ,[SvrDateTime]
			   ,[LONgitude]
			   ,[Latitude]
			   ,[Heading]
			   ,[RepORtID]
			   ,[Odometer]
			   ,[HDOP]
			   ,[InputStatus]
			   ,[OutputStatus]
			   ,[VSSSpeed]
			   ,[AnalogInput]
			   ,[DriverID]
			   ,[Temperature1]
			   ,[Temperature2]
			   ,[MCC]
			   ,[MNC]
			   ,[LAC]
			   ,[CellID]
			   ,[RXLevel]
			   ,[GSMQuality]
			   ,[GSMStatus]
			   ,[Satellites]
			   ,[RealTime]
			   ,[MainVolt]
			   ,[BatVolt]
			)
			VALUES 
			(
				@iDevice
			   ,@dtGPSdatetime
			   ,@dtRepdatetime --@dtRTCdatetime -- GSOLANO: Se solicita dejar la misma fecha de RepDateTime en el campo RTCDateTime, solicitado por J.Chaves
			   ,@dtRepdatetime
			   ,GETDATE()
			   ,@fLongitude
			   ,@fLatitude
			   ,@fHeading
			   ,@iReportID
			   ,@fOdometer
			   ,@fHDOP
			   ,@tAllInputs
			   ,@tAllOutputs
			   ,@fVSSSpeed
			   ,@fAnalogInput
			   ,@vsDriverID
			   ,@vfTemperature1
			   ,@vfTemperature2
			   ,@viMCC
			   ,@viMNC
			   ,@viLAC
			   ,@viCellID
			   ,@viRXLevel
			   ,@viGSMQ
			   ,@viGSMS
			   ,@ISatellites
			   ,@bRealTime
			   ,@fMainVolt
			   ,@fBatVolt
		    )

			IF @@errOR <> 0
			BEGIN
				ROLLBACK TRAN @cTRANsacciON
				RAISERROR ('ErrOR al INSERTar regIStro de Saltos.', 16, 1)
				RETURN -1
			END
			COMMIT TRANSACTION
		END
		ELSE
		BEGIN
			-- Se inserta en Reports
			INSERT INTO [dbo].[Reports]
			(
				[Device]
			   ,[GPSDateTime]
			   ,[RTCDateTime]
			   ,[RepDateTime]
			   ,[SvrDateTime]
			   ,[LONgitude]
			   ,[Latitude]
			   ,[Heading]
			   ,[RepORtID]
			   ,[Odometer]
			   ,[HDOP]
			   ,[InputStatus]
			   ,[OutputStatus]
			   ,[VSSSpeed]
			   ,[AnalogInput]
			   ,[DriverID]
			   ,[Temperature1]
			   ,[Temperature2]
			   ,[MCC]
			   ,[MNC]
			   ,[LAC]
			   ,[CellID]
			   ,[RXLevel]
			   ,[GSMQuality]
			   ,[GSMStatus]
			   ,[Satellites]
			   ,[RealTime]
			   ,[MainVolt]
			   ,[BatVolt]
		    )
			VALUES 
			(
				@iDevice
			   ,@dtGPSdatetime
			   ,@dtRepdatetime --@dtRTCdatetime -- GSOLANO: Se solicita dejar la misma fecha de RepDateTime en el campo RTCDateTime, solicitado por J.Chaves
			   ,@dtRepdatetime
			   ,GETDATE()
			   ,@fLongitude
			   ,@fLatitude
			   ,@fHeading
			   ,@iReportID
			   ,@fOdometer
			   ,@fHDOP
			   ,@tAllInputs
			   ,@tAllOutputs
			   ,@fVSSSpeed
			   ,@fAnalogInput
			   ,@vsDriverID
			   ,@vfTemperature1
			   ,@vfTemperature2
			   ,@viMCC
			   ,@viMNC
			   ,@viLAC
			   ,@viCellID
			   ,@viRXLevel
			   ,@viGSMQ
			   ,@viGSMS
			   ,@ISatellites
			   ,@bRealTime
			   ,@fMainVolt
			   ,@fBatVolt
			)

			IF @@errOR <> 0
			BEGIN
			
				ROLLBACK TRAN @cTRANsacciON
				RAISERROR ('ErrOR al INSERTar regIStro de RepORts.', 16, 1)
				RETURN -1
			END
				
			SELECT @viRegIStro = SCOPE_IDENTITY()	
						
			IF (@HasCooler = 1)		
			BEGIN 
				/*Add temperature by device*/
				DECLARE @idTemporal int = 1
				DECLARE @AcumuladorTemp INT
				DECLARE	@temperatura decimal(10,4)
				DECLARE @lGPSDateTime Datetime

				WHILE EXISTS(
								SELECT * 
								FROM #TempSensor
							)
				BEGIN 
					DELETE #TempSensor WHERE idtemp > @TempSensorCount
					IF (@idTemporal <= @TempSensorCount)
					BEGIN 
						SELECT @AcumuladorTemp = [idAcumulador],
							   @temperatura = [temperatura],
							   @lGPSDateTime = [GPSDateTime]
						FROM #TempSensor 
						WHERE idtemp = @idTemporal

						EXEC  [dbo].[Sp_AddTemperatureByDevice] @viRegIStro,@iDevice,@AcumuladorTemp,@temperatura, @lGPSDateTime

						DELETE #TempSensor 
						WHERE idtemp = @idTemporal
								
						SET @idTemporal = @idTemporal + 1								
					END
				 END
			END
			/* FIN Add temperature by device*/
						
			/* 2014-01-15. WAB/JMBS. Procesamiento cONductOR más reciente */
			IF (@iReportID = 0)
			BEGIN
				DELETE FROM [Driver_Last]
				WHERE [Device] = @iDevice

				IF @@errOR <> 0
				BEGIN
					ROLLBACK TRAN @cTRANsacciON
					RAISERROR ('ErrOR al bORrar regIStro de Driver_Last.', 16, 2)
					RETURN -1
				END
			END

			IF (@iReportID = 2)
			BEGIN
				--Actualiza el conductor activo en el vehículo de acuerdo a la asignación de la llave dallas
				DECLARE @lDriverId INT = NULL
					   ,@lVehicleId INT = NULL
					
				SELECT @lVehicleId = [VehicleId]
				FROM [General].[Vehicles]
				WHERE [DeviceReference] = @iDevice
				
				SELECT @lDriverId = dd.[UserId]					      
				FROM [General].[DallasKeysByDriver] dd
				INNER JOIN [General].[DallasKeys] dk
					ON dk.[DallasId] = dd.[DallasId]
				WHERE dk.[DallasCode] = @vsDriverID	
					
				--Verificamos si existe un conductor asignado a la llave dallas y un vehículo asignado al device que está transmitiendo
				IF (@lVehicleId IS NOT NULL AND @lDriverId IS NOT NULL)
				BEGIN
					--Si existe un registro lo actualiza e inserta el nuevo
					IF EXISTS (
								 SELECT TOP 1 * 
							     FROM [General].[VehiclesByUser] 
								 WHERE [VehicleId] = @lVehicleId
							  )
					BEGIN
						UPDATE [General].[VehiclesByUser] 
						SET [LastDateDriving] = GETDATE()
						WHERE [VehicleId] = @lVehicleId AND [LastDateDriving] IS NULL

						INSERT INTO [General].[VehiclesByUser] 
						(
							[VehicleId]
						   ,[UserId]
						   ,[InsertDate]
						   ,[InsertUserId]
						)
						VALUES
						(
							@lVehicleId
						   ,@lDriverId
						   ,GETDATE()
						   ,@lDriverId
						)
					END
					--Sino solo inserta el registro nuevo
					ELSE
					BEGIN
						INSERT INTO [General].[VehiclesByUser] 
						(
							[VehicleId]
						   ,[UserId]
						   ,[InsertDate]
						   ,[InsertUserId]
						)
						VALUES
						(
							@lVehicleId
						   ,@lDriverId
						   ,GETDATE()
						   ,@lDriverId
						)
					END				
                END

				SELECT @viCantidad = count(1)
				FROM [Driver_Last]
				WHERE [Device] = @iDevice
				
				IF (@viCantidad = 0)
				BEGIN					
					INSERT INTO [Driver_Last] 
					(
						[Device]
					   ,[DriverID]
					   ,[RegISteredDate]
					)
					VALUES 
					(
						@iDevice
					   ,@vsDriverID
					   ,GETDATE()
					)
					
					IF @@errOR <> 0
					BEGIN
						ROLLBACK TRAN @cTRANsacciON
						RAISERROR ('ErrOR al INSERTar regIStro de Driver_Last.', 16, 3)
						RETURN -1
					END					
				END
			END
			/* Fin de: 2014-01-15. WAB/JMBS. Procesamiento cONductOR más reciente */		

			/* 2013-10-31. JMBS. Ajuste para RPM */
			SET @iRPMCalc = NULL

			/* 2013-11-14. JMBS. Ajuste para Volumen Combustible */
			SET @dFuelVolumeGalCalc = NULL
			SET @vbActualizarTexto = 0

			SELECT @viCantidad = COUNT(1),
				   @vdtGPSdatetime = MAX([RepDateTime])
			FROM [dbo].[Report_Last]
			WHERE [Device] = @iDevice

			--=======================================================================================
			--VALIDATES TRAVEL TIME 
			IF NOT EXISTS 
			(
					SELECT *
					FROM [Efficiency].[DrivingTravelVehicle]
					WHERE [DeviceId] = @iDevice 
					AND CONVERT(DATE, [ReportDate]) = CONVERT(DATE, @dtRepdatetime)
			)
			BEGIN
				INSERT INTO [Efficiency].[DrivingTravelVehicle]
				(
					[DeviceId],
					[ReportDate],
					[Odometer],					
					[InsertDate]						
				)
				VALUES 
				(
					@iDevice,
					@dtRepdatetime,
					@fOdometer,
					GETDATE()					
				)
			END
			ELSE
			BEGIN 
				IF (
						SELECT [ReportDate]
						FROM [Efficiency].[DrivingTravelVehicle]
						WHERE [DeviceId] = @iDevice 
						AND CONVERT(DATE, [ReportDate]) = CONVERT(DATE, @dtRepdatetime)
					) > @dtRepdatetime
				BEGIN
					UPDATE [Efficiency].[DrivingTravelVehicle]
					SET [Odometer] = @fOdometer,
						[ModifyDate] = GETDATE()
					WHERE [DeviceId] = @iDevice 
					AND CONVERT(DATE, [ReportDate]) = CONVERT(DATE, @dtRepdatetime)
				END
			END
			--=======================================================================================

			IF @viCantidad = 0
			BEGIN
				SET @vbActualizarTexto = 1

				INSERT INTO [dbo].[Report_Last]
				(
					[Device]
				   ,[GPSDateTime]
				   ,[RTCDateTime]
				   ,[RepDateTime]
				   ,[SvrDateTime]
				   ,[LONgitude]
				   ,[Latitude]
				   ,[Heading]
				   ,[RepORtID]
				   ,[Odometer]
				   ,[HDOP]
				   ,[InputStatus]
				   ,[OutputStatus]
				   ,[VSSSpeed]
				   ,[AnalogInput]
				   ,[DriverID]
				   ,[Temperature1]
				   ,[Temperature2]
				   ,[MCC]
				   ,[MNC]
				   ,[LAC]
				   ,[CellID]
				   ,[RXLevel]
				   ,[GSMQuality]
				   ,[GSMStatus]
				   ,[Satellites]
				   ,[RealTime]
				   ,[MainVolt]
				   ,[BatVolt]
				   ,[Report]
				)
				VALUES 
				(
					@iDevice
				   ,@dtGPSdatetime
				   ,@dtRepdatetime --@dtRTCdatetime -- GSOLANO: Se solicita dejar la misma fecha de RepDateTime en el campo RTCDateTime, solicitado por J.Chaves
				   ,@dtRepdatetime
				   ,GETDATE()
				   ,@fLongitude
				   ,@fLatitude
				   ,@fHeading
				   ,@iReportID
				   ,@fOdometer
				   ,@fHDOP
				   ,@tAllInputs
				   ,@tAllOutputs
				   ,@fVSSSpeed
				   ,@fAnalogInput
				   ,@vsDriverID
				   ,@vfTemperature1
				   ,@vfTemperature2
				   ,@viMCC
				   ,@viMNC
				   ,@viLAC
				   ,@viCellID
				   ,@viRXLevel
				   ,@viGSMQ
				   ,@viGSMS
				   ,@ISatellites
				   ,@bRealTime
				   ,@fMainVolt
				   ,@fBatVolt
				   ,@viRegIStro
				)

				IF @@errOR <> 0
				BEGIN
					ROLLBACK TRAN @cTRANsacciON
					RAISERROR ('ErrOR al INSERTar regIStro de RepORt_Last.', 16, 7)
					RETURN -1
				END
			END
			ELSE
			BEGIN
				/* 2013-07-04. JMBS. Se evalúa que repORte no esté más de 6 hORas (UTC) en el futuro, partiENDo del supuesto que zONa hORaria servidOR es UTC-6 */
				/* 2013-11-19. JMBS. Se evalúa que repORte esté dentro de las coORdenadas válidas de WGS84. */
				/* 2013-12-19. JMBS. Se filTRAN repORtes cON coORdenadas 0, 0. */
				/* 2014-01-15. JMBS. Se permite actualiza si nuevo repORte es mayOR o igual que repORte actual. */
				IF ((@dtRepdatetime >= @vdtGPSdatetime) AND 
						(@dtRepdatetime < DATEADD(hh,12,GETDATE())) AND 
							(@fLongitude >= -180 AND @fLongitude <= 180) AND 
								(@fLatitude >= -90 AND @fLatitude <= 90) AND 
									(@fLongitude <> 0 AND @fLatitude <> 0))
				BEGIN
					PRINT 'Actualizo'
					SET @vbActualizarTexto = 1

					UPDATE [dbo].[Report_Last]
					SET [GPSDateTime] = @dtGPSdatetime
					   ,[RTCDateTime] = @dtRepdatetime --@dtRTCdatetime -- GSOLANO: Se solicita dejar la misma fecha de RepDateTime en el campo RTCDateTime, solicitado por J.Chaves
					   ,[RepDateTime] = @dtRepdatetime
					   ,[SvrDateTime] = GETDATE()
					   ,[LONgitude] = @fLongitude
					   ,[Latitude] = @fLatitude
					   ,[Heading] = @fHeading
					   ,[RepORtID] = @iReportID
					   ,[Odometer] = @fOdometer
					   ,[HDOP] = @fHDOP
					   ,[InputStatus] = @tAllInputs
					   ,[OutputStatus] = @tAllOutputs
					   ,[VSSSpeed] = @fVSSSpeed
					   ,[AnalogInput] = @fAnalogInput
					   ,[DriverID] = @vsDriverID
					   ,[Temperature1] = @vfTemperature1
					   ,[Temperature2] = @vfTemperature2
					   ,[MCC] = @viMCC
					   ,[MNC] = @viMNC
					   ,[LAC] = @viLAC
					   ,[CellID] = @viCellID
					   ,[RXLevel] = @viRXLevel
					   ,[GSMQuality] = @viGSMQ
					   ,[GSMStatus] = @viGSMS
					   ,[Satellites] = @ISatellites
					   ,[RealTime] = @bRealTime
					   ,[MainVolt] = @fMainVolt
					   ,[BatVolt] = @fBatVolt
					   ,[Report] = @viRegIStro
					WHERE [Device] = @iDevice

					IF @@errOR <> 0
					BEGIN
						ROLLBACK TRAN @cTRANsacciON
						RAISERROR ('ErrOR al actualizar regIStro de RepORt_Last.', 16, 8)
						RETURN -1
					END
				END
			END
							
			-- Insertamos / actualizamos los datos para el proceso de Alarmas
			-------  Validar reporte 999
			IF @iReportID <> 33
			BEGIN
				--EXEC [General].[GPS_CreateAlarm] @iDevice, @fLatitude, @fLongitude, @dtRepdatetime, @iReportID, @fTemperature1,@pSensor,@pLimiteTemperatura, @fMainVolt
				IF EXISTS(
							SELECT 1 
							FROM [General].[GPS_Alarms] 
							WHERE [Device] = @iDevice
						 )
				BEGIN
					UPDATE [General].[GPS_Alarms]
					SET [Longitude] = @fLongitude,
						[Latitude] = @fLatitude,
						[ReportDate] = @dtRepdatetime, 
						[ReportId] = @iReportID, 
						[Temperature] = @fTemperature1, 
						[Sensor] = @pSensor, 
						[LimitTemperature] = @pLimiteTemperatura, 
						[MainVolt] = @fMainVolt,
						[ModifyDate] = GETDATE()
					WHERE Device = @iDevice
				END
				ELSE 
				BEGIN
					IF(@iDevice IS NOT NULL AND @iReportID IS NOT NULL)
					BEGIN
						INSERT INTO [General].[GPS_Alarms] ([Device], [Latitude], [Longitude], [ReportDate], [ReportId], [Temperature], [Sensor], [LimitTemperature], [MainVolt])
						VALUES (@iDevice, @fLatitude, @fLongitude, @dtRepdatetime, @iReportID, @fTemperature1,@pSensor,@pLimiteTemperatura, @fMainVolt)
					END
				END
			END
			-------		 

			/* 2014-01-17. Proceso para actualizar el Status (Estado) del AVL */
			IF @vbActualizarTexto = 1
			BEGIN			
				SET @viDeviceStatus = NULL
			
				SELECT @viCantidad = count(1)
				FROM [RepORt_Last_Status]
				WHERE Device = @iDevice
				
				SELECT top 1 @viDeviceStatus = Status
				FROM [DeviceStatus]
				WHERE ([IgnitiON] = (@tAllInputs & 1) 
						AND RepORtID IS NULL)
				OR (IgnitiON = (@tAllInputs & 1) 
					AND RepORtID = @iReportID)
				ORDER BY [PriORity] DESC
				
				IF (@viDeviceStatus IS NULL)
				BEGIN					
					SELECT @viDeviceStatus = Status
					FROM [DeviceStatus]
					WHERE [Unknown] = 1
				END
			END

			/* 2017-09-15. GSolano. Capturamos los ReportId de cada comando */
			-- Obtenemos los ReportId parametrizados para cada comando
			DECLARE @ReportId_OpenDoors INT = 0
			DECLARE @ReportId_TurnOff INT = 0
			DECLARE @ReportId_TurnOn INT = 0
			DECLARE @ReportId_DallasOff INT = 0
			DECLARE @ReportId_DallasOn INT = 0
			DECLARE @ReportId_Sleep INT = 0
			DECLARE @ReportId_WakeUp INT = 0
			DECLARE @ReportId_Internet INT = 0
			DECLARE @ReportId_Expiration INT = 0
			
			--ReportId OpenDoors
			SELECT @ReportId_OpenDoors = GPS_ReportId
			FROM [dbo].[TiposdeComandosSMS] 
			WHERE idTipoComando = 5
			
			--ReportId TurnOff
			SELECT @ReportId_TurnOff = GPS_ReportId
			FROM [dbo].[TiposdeComandosSMS] 
			WHERE idTipoComando = 6
			
			--ReportId TurnOn
			SELECT @ReportId_TurnOn = GPS_ReportId
			FROM [dbo].[TiposdeComandosSMS] 
			WHERE idTipoComando = 7
			
			--ReportId WakeUp
			SELECT @ReportId_WakeUp = GPS_ReportId
			FROM [dbo].[TiposdeComandosSMS] 
			WHERE idTipoComando = 9
			
			--ReportId Sleep
			SELECT @ReportId_Sleep = GPS_ReportId
			FROM [dbo].[TiposdeComandosSMS] 
			WHERE idTipoComando = 10

			--ReportId Dallas Off
			SELECT @ReportId_DallasOff = GPS_ReportId
			FROM [dbo].[TiposdeComandosSMS] 
			WHERE idTipoComando = 11

			--ReportId Dallas On
			SELECT @ReportId_DallasOn = GPS_ReportId
			FROM [dbo].[TiposdeComandosSMS] 
			WHERE idTipoComando = 12
			
			--ReportId Internet
			SELECT @ReportId_Internet = CAST([NumericValue] AS INT) 
			FROM [dbo].[GeneralParameters] 
			WHERE [ParameterID] = 'REPORT_COMMAND_INTERNET'
			
			--ReportId de expiración
			SELECT @ReportId_Expiration = CAST([NumericValue] AS INT) 
			FROM [dbo].[GeneralParameters] 
			WHERE [ParameterID] = 'ReportId_Expiration'
			
			-- Validamos el ReportId entrante
			IF(@iReportID = @ReportId_OpenDoors OR	
					@iReportID = @ReportId_TurnOff OR	
						@iReportID = @ReportId_TurnOn OR	
							@iReportID = @ReportId_Sleep OR	
								@iReportID = @ReportId_WakeUp OR
									@iReportID = @ReportId_Internet OR
										@iReportID = @ReportId_Expiration OR
											@iReportID = @ReportId_DallasOff OR
												@iReportID = @ReportId_DallasOn)
			BEGIN
				DECLARE @LastDateTime DATETIME = @dtRTCdatetime 	
				
				IF EXISTS(
							SELECT 1 
							FROM [General].[CommandStatePerDevice] 
							WHERE Device = @iDevice
						 ) -- Actualizamos el registro con el último ReportId 
				BEGIN
					UPDATE [General].[CommandStatePerDevice]
					SET [ReportId] = @iReportID,
						[Report] = @viRegIStro, 
						[LastDateTime] = @LastDateTime
					WHERE Device = @iDevice
				END
				ELSE --Creamos un nuevo registro del Device reportado
				BEGIN
					INSERT INTO [General].[CommandStatePerDevice]
					(
						[Device],
						[Report],
						[ReportId],
						[LastDateTime]
					)
					VALUES
					(
						@iDevice,
						@viRegIStro,
						@iReportID,
						@LastDateTime
					)
				END				
			END						
			COMMIT TRAN @cTRANsacciON
		END
	END	
	ELSE
	BEGIN
		SET @sUnitID = CONVERT(VARCHAR(20),@dUnitID)
		RAISERROR ('No se encontró Dispositivo referenciado por UnidID %s.', 16, 23, @sUnitID)
		RETURN -1
	END
	DROP TABLE #TempSensor
	
	SET NOCOUNT OFF
END


