USE ECOsystemDev
GO

IF EXISTS(SELECT 1 FROM sys.objects WHERE name = 'SP_VehicleTransmission_Report' AND type = 'P')
	DROP PROC [Efficiency].[SP_VehicleTransmission_Report]
GO

/*
EXECUTE [Efficiency].[SP_VehicleTransmission_Report]
	@pVehicleId = 2330
	,@pYear = NULL
	,@pMonth = NULL
	,@pStartDate = '20171128'
	,@pEndDate = '20171130'
*/
-- ================================================================================================  
-- Author:  Esteban Solís  
-- Create date: 27/03/2018  
-- Description: Retrieve Vehicle transmissions Report information 
-- Stefano Quirós - Change GPSDateTime to RepDateTime value - 20/09/2018
-- Stefano Quirós - Add filter CustomerId that works with send program reports
-- ================================================================================================  
CREATE PROCEDURE [Efficiency].[SP_VehicleTransmission_Report] --null, 10630, null, null, '11/6/2019 6:00:00 PM', '11/7/2019 6:00:00 PM', null
  (
	 @pVehicleId INT = NULL
	,@pCustomerId INT = NULL
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
	,@pUserId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @vDevice INT
		,@vDateIni DATETIME
		,@vDateFin DATETIME
		,@lTimeZoneParameter INT
		,@lOnDesc VARCHAR(10) = 'Encendido'
		,@lOffDesc VARCHAR(10) = 'Apagado'
		,@lStopTime INT = 2
		,@lCustomerId INT

	DECLARE @STATUS_VEHICLE_ON INT = 1
	,@STATUS_VEHICLE_OFF INT = 0
	,@REPORTID_OFF INT = 1

	IF OBJECT_ID('tempdb.dbo.#Data', 'U') IS NOT NULL
		DROP TABLE #Data;

	CREATE TABLE #Data (
		 [Id] INT IDENTITY(1, 1) NOT NULL
		,[RepDateTime] DATETIME
		,[Latitude] FLOAT
		,[Longitude] FLOAT
		,[Odometer] FLOAT
		,[InputStatus] TINYINT
		,[VSSSpeed] FLOAT
		,[ReportId] INT
		,[Status] BIT
		,[EventDetail] VARCHAR(1000)
		,[Location] VARCHAR(5000)
		,[Distance] FLOAT
		,[MainVolt] FLOAT
		,[BatteryStatus] VARCHAR(100)
		)

    IF @pCustomerId IS NOT NULL
	BEGIN
		SELECT TOP 1 @vDevice = [DeviceReference],
				     @lCustomerId = @pCustomerId
		FROM [General].[Vehicles]
		WHERE [CustomerId] = @pCustomerId
	END
	ELSE
	BEGIN
		SELECT @vDevice = [DeviceReference],
	           @lCustomerId = [CustomerId]
		FROM [General].[Vehicles]
		WHERE [VehicleId] = @pVehicleId
	END


	SELECT @lTimeZoneParameter = [TimeZone]	     
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @lCustomerId

	IF (
			@pYear IS NOT NULL
			AND @pMonth IS NOT NULL
			)
	BEGIN
		SET @vDateIni = DATEADD(hh, @lTimeZoneParameter * - 1, CAST(CAST(@pYear AS VARCHAR) + '-' + CAST(@pMonth AS VARCHAR) + '-' + CAST(1 AS VARCHAR) AS DATETIME))
		SET @vDateFin = DATEADD(hh, @lTimeZoneParameter * - 1, DATEADD(m, 1, @vDateIni))
	END
	ELSE
	BEGIN
		SET @vDateIni = DATEADD(hh, @lTimeZoneParameter * - 1, @pStartDate)
		SET @vDateFin = DATEADD(hh, @lTimeZoneParameter * - 1, @pEndDate)
	END

	IF (
			@vDateIni IS NOT NULL
			AND @vDateFin IS NOT NULL
			)
	BEGIN
		INSERT INTO #Data (
			 [RepDateTime]
			,[Latitude]
			,[Longitude]
			,[Odometer]
			,[InputStatus]
			,[VSSSpeed]
			,[ReportId]
			,[MainVolt]
			,[Status]
			)
		SELECT r.[RepDateTime]
			  ,r.[Latitude]
			  ,r.[Longitude]
			  ,r.[Odometer]
			  ,r.[InputStatus]
			  ,r.[VSSSpeed]
			  ,r.[ReportId]
			  ,r.[MainVolt]
			  ,CASE r.[InputStatus] % 2
			  	WHEN 0
			  		THEN @STATUS_VEHICLE_OFF
			  	ELSE @STATUS_VEHICLE_ON
			  	END
		FROM [dbo].[Reports] r
		WHERE r.[Device] = @vDevice
			AND r.[RepDateTime] BETWEEN @vDateIni
				AND @VDateFin
		ORDER BY r.[RepDateTime] ASC

		UPDATE d
		SET d.[Distance] = (d.[Odometer] - r.[Odometer])
		FROM #Data d
		CROSS APPLY (
			SELECT TOP 1 d1.[Odometer]
			FROM #data d1
			WHERE d1.[ReportId] = 0
				AND d1.[Id] < d.[Id]
			ORDER BY d1.[Id] DESC
			) AS r
		WHERE d.[ReportId] = @REPORTID_OFF
			AND d.[Distance] IS NULL
			AND d.[Status] = @STATUS_VEHICLE_OFF

		UPDATE d
		SET d.[Distance] = (d.[Odometer] - r.[Odometer])
		FROM #Data d
		CROSS APPLY (
			SELECT TOP 1 d1.[Odometer]
			FROM #data d1
			WHERE d1.[Id] < d.[Id]
			ORDER BY d1.[Id]
			) AS r
		WHERE d.[ReportId] = @REPORTID_OFF
			AND d.Distance IS NULL
			AND d.[Status] = @STATUS_VEHICLE_OFF

		UPDATE d
		SET d.[EventDetail] = c.[Description]
			,d.[Location] = ISNULL([Efficiency].[Fn_GetAddressByLatLon_Retrieve](d.[Longitude], d.[Latitude],@lCustomerId), '')
			,d.[BatteryStatus] = b.[Description]
		FROM #Data d
		JOIN [Efficiency].[GPSReportCodes] c ON d.[ReportId] = c.[Value]
		JOIN [Efficiency].[BatteryVoltRanges] b ON (
				d.[Status] = 0
				AND d.[MainVolt] >= b.[LowValueOn]
				AND d.[MainVolt] < b.[HighValueOn]
				)
			OR (
				d.[Status] = 1
				AND d.[MainVolt] >= b.[LowValueOff]
				AND d.[MainVolt] < b.[HighValueOff]
				)
	END

	-- Return result
	SELECT DATEADD(HOUR, @lTimeZoneParameter, d.[RepDateTime]) AS [ReportDate]
		  ,CONVERT(DECIMAL(16,6), d.[Latitude]) [Latitude]
		  ,CONVERT(DECIMAL(16,6), d.[Longitude]) [Longitude]
		  ,CONVERT(DECIMAL(16,6), d.[VSSSpeed]) AS [ReportSpeed]
		  ,d.[EventDetail]
		  ,d.[Location]
		  ,CONVERT(DECIMAL(18,2),d.[Distance]) AS [ReportDistance]
		  ,[BatteryStatus]
		  ,CASE 
		  	WHEN d.[Status] = @STATUS_VEHICLE_ON
		  		THEN 'SI'
		  	ELSE 'NO'
		  	END AS [IsVehicleOn]
	FROM #Data d

	--REMOVE TABLES  
	DROP TABLE #Data

	SET NOCOUNT OFF
END
