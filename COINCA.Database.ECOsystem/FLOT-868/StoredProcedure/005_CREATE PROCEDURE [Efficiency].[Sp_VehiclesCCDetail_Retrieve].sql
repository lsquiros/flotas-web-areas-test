USE ECOsystemDev
GO

IF EXISTS(SELECT 1 FROM sys.objects WHERE name = 'Sp_VehiclesCCDetail_Retrieve' AND type = 'P')
	DROP PROC [Efficiency].[Sp_VehiclesCCDetail_Retrieve]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 15/03/2019
-- Description:	Retrieve Information about the vehicles for the API
-- Modify By Stefano Quirós - 12/04/2019 - Add the Address and Volt parameters to the retrieve
-- Modify By María Jiménez - May/30/2019 - Add Temperature, HasCooler and SensorsXML
-- Modify By Henry Retana - 09/23/2019 - Validates if themperature is less than 1 then 0 
-- Modify By Juan C. santamaria -- Date 18-10-2019 -- Validate if Atrack Device -- ID: FLOT-868
-- ================================================================================================

CREATE PROCEDURE [Efficiency].[Sp_VehiclesCCDetail_Retrieve]
(
	@pCustomerId INT = NULL,
	@pUserId INT = NULL,
	@pVehicleId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT,
			@lTimeZoneParameter INT,
			@lIsAtrack INT,
			@lDevice   INT = 0

	INSERT	@Results
	EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	
	SET @count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	--GET THE TIME ZONE
	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu 
		ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId


	------- < FLOT-868 >------
	SELECT @lDevice = DeviceReference FROM [General].[Vehicles] WHERE VehicleId = @pVehicleId
	SET @lIsAtrack = (SELECT [Efficiency].[Fn_IsAtrackDevice](@lDevice))
	
	------------------------------------------------------------------------------------------
	IF @lIsAtrack = 0   /* diferent device to Atrack */
	------------------------------------------------------------------------------------------
	BEGIN 

		SELECT DISTINCT  v.[VehicleId]
						,v.[Name]
						,v.[PlateId]					
						,DATEADD(HOUR, @lTimeZoneParameter, r.[RepDateTime]) [RepDateTime]
						,r.[RepDateTime] [RepDateTimeUTC]
						,CASE r.[InputStatus] % 2  /* FLOT-868 Pendiente realizar el cambio en Report Last para que tenga el status en que esta JSA */
							  WHEN 0
							  THEN 0
							  ELSE 1
						 END [Status]
						,r.[RXLevel] [Signal]
						,CASE WHEN r.[VSSSpeed] < 1 
							  THEN 0 
							  ELSE r.[VSSSpeed]
						 END [Speed]
						,r.[Longitude]
						,r.[Latitude]
						,[Efficiency].[Fn_GetAddressByLatLon_Retrieve](r.[Longitude], r.[Latitude], @pCustomerId) [Address]
						,r.[Heading]
						,r.[MainVolt] [Voltage]
						,CASE WHEN r.[MainVolt] < 12 
							  THEN 1
							  ELSE 0
						 END [LowBattery]
						,[Efficiency].[Fn_GetVehicleHasCooler](v.VehicleId) [HasCooler]
						,CAST(r.[Temperature1] AS DECIMAL(12,2)) [Temperature]
						,v.MinTemperature [MinTemperature]
						,v.MaxTemperature [HigherCustomerTemperature]
						,CASE WHEN [Efficiency].[Fn_GetVehicleHasCooler](v.VehicleId) = 1
							THEN [Efficiency].[Fn_GetTemperatureSensorsToXML] (r.[Report], v.[DeviceReference])
							ELSE NULL
						END [SensorsXML]
		FROM [General].[Vehicles] v
		INNER JOIN dbo.[Report_Last] r
			ON v.[DeviceReference] = r.[Device]
		WHERE (@count = 0 OR v.[VehicleId] IN (
												SELECT items 
												FROM @Results
											  )) -- DYNAMIC FILTER 
		AND v.CustomerId = @pCustomerId
		AND ISNULL(v.[IntracKReference], 0)<> 0				  
		AND (v.[IsDeleted] IS NULL OR v.[IsDeleted] = 0) 
		AND (@pVehicleId IS NULL OR v.[VehicleId] = @pVehicleId)
	END

	------------------------------------------------------------------------------------------
	IF @lIsAtrack = 1   /* equal device to Atrack */
	------------------------------------------------------------------------------------------
	BEGIN 

		SELECT DISTINCT  v.[VehicleId]
						,v.[Name]
						,v.[PlateId]					
						,DATEADD(HOUR, @lTimeZoneParameter, r.[RepDateTime]) [RepDateTime]
						,r.[RepDateTime] [RepDateTimeUTC]
						,CASE r.[InputStatus] % 2
							  WHEN 0
							  THEN 0
							  ELSE 1
						 END [Status]
						,r.[RXLevel] [Signal]
						,CASE WHEN r.[VSSSpeed] < 1 
							  THEN 0 
							  ELSE r.[VSSSpeed] 
						 END [Speed]
						,r.[Longitude] 
						,r.[Latitude]
						,[Efficiency].[Fn_GetAddressByLatLon_Retrieve](r.[Longitude], r.[Latitude], @pCustomerId) [Address]
						,r.[Heading]
						,r.[MainVolt] [Voltage]
						,CASE WHEN r.[MainVolt] < 12 
							  THEN 1
							  ELSE 0
						 END [LowBattery]
						,[Efficiency].[Fn_GetVehicleHasCooler](v.VehicleId) [HasCooler]
						,CAST(r.[Temperature1] AS DECIMAL(12,2)) [Temperature]
						,v.MinTemperature [MinTemperature]
						,v.MaxTemperature [HigherCustomerTemperature]
						,CASE WHEN [Efficiency].[Fn_GetVehicleHasCooler](v.VehicleId) = 1
							THEN [Efficiency].[Fn_GetTemperatureSensorsToXML] (r.[Report], v.[DeviceReference])
							ELSE NULL
						END [SensorsXML]
		FROM [General].[Vehicles] v
		INNER JOIN dbo.[Report_Last] r
			ON v.[DeviceReference] = r.[Device]
		WHERE (@count = 0 OR v.[VehicleId] IN (
												SELECT items 
												FROM @Results
											  )) -- DYNAMIC FILTER 
		AND v.CustomerId = @pCustomerId
		AND ISNULL(v.[IntracKReference], 0)<> 0				  
		AND (v.[IsDeleted] IS NULL OR v.[IsDeleted] = 0) 
		AND (@pVehicleId IS NULL OR v.[VehicleId] = @pVehicleId)
	END



    SET NOCOUNT OFF
END

GO

