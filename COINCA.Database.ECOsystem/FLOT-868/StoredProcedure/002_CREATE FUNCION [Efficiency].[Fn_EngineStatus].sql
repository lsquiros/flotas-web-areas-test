USE ECOsystemDev
GO

IF (OBJECT_ID('[Efficiency].[Fn_EngineStatus]','fn') IS NOT NULL)
	DROP FUNCTION [Efficiency].[Fn_EngineStatus]
GO

-- ==================================================
-- Author:      Juan Carlos Santamaria V.
-- Create Date: 10-19-2019
-- Description: Return 1 OFF, 0 ON engine
-- ==================================================
CREATE FUNCTION [Efficiency].[Fn_EngineStatus]
(
@pDevice INT,
@pReportID INT,
@pInputStatus INT
)

RETURNS INT
AS 

BEGIN

	DECLARE @lEngineStatus BIT = NULL
		   ,@lIsAtrackDevice INT = 0

	SELECT @lEngineStatus = EngineStatus
	FROM [dbo].[Report_Last] WITH(NOLOCK)
	WHERE Device = @pDevice

	SELECT @lIsAtrackDevice = [Efficiency].[Fn_IsAtrackDevice](@pDevice)	
	
	IF @lIsAtrackDevice = 0
	BEGIN
		IF @lEngineStatus IS NULL
		BEGIN		
			IF @pReportID in (1,0)
			BEGIN				
				SET @lEngineStatus = @pReportID
			END
			ELSE
			BEGIN
				SELECT TOP 1 @lEngineStatus = rl.ReportID
				FROM [dbo].[Reports] rl
				WHERE rl.Device = @pDevice
				AND rl.ReportID IN (1,0)
			    ORDER BY rl.RepDATETIME DESC
			END
		END
		ELSE IF (@pReportID IN (1,0))
		BEGIN			
			SET @lEngineStatus = @pReportID
		END
	END
	--------------------------------------------------------------
	ELSE /* ATRACK ReportID => 101 = ON // 102 = OFF*/
	--------------------------------------------------------------
	BEGIN		
		IF @pReportID IN (101,102)
		BEGIN		 
			SELECT @lEngineStatus = CASE WHEN @pReportID = 101 THEN 0 ELSE 1 END
		    RETURN @lEngineStatus
		END
		ELSE
		BEGIN			
			SELECT TOP 1 @lEngineStatus = CASE WHEN ReportID = 101 THEN 0 ELSE 1 END
			FROM [dbo].[Reports] WITH(NOLOCK)
			WHERE Device = @pDevice
			AND ReportID IN (101,102)
		    ORDER BY RepDATETIME DESC
		END
	END
	
	RETURN @lEngineStatus

END

GO

