
IF EXISTS (SELECT 1 FROM sys.objects WHERE name LIKE 'Sp_ReportsOnOffStartingEngine' AND type = 'P')
	DROP PROC Sp_ReportsOnOffStartingEngine
GO

-- =============================================
-- Author:      Juan Carlos Santamaria V
-- Create Date: 10-10-2019
-- Description: Get the data from Reports for
--              turn ON and OFF events (CAR ENGINE)
-- =============================================
CREATE PROC Sp_ReportsOnOffStartingEngine
(
 @pStartDate  DATETIME = null
,@pEndDate    DATETIME = null
,@pDevice     INT      = 0
,@pAllEvents  BIT      = 0
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @lStarting INT = 0
		   ,@lDateIni DATETIME
		   ,@lDateFin DATETIME
		   ,@lCount	INT = 0
		   ,@lStatus INT = -1
		   ,@lIdFirstStatus INT 
		   ,@lFirstStatus INT
		   ,@lIsAtrack INT

	SET @lDateIni = @pStartDate 
	SET @lDateFin = @pEndDate 

	SELECT @lIsAtrack = [Efficiency].[Fn_IsAtrackDevice](@pDevice)
		
	IF @lIsAtrack = 0	
	BEGIN
		
		SELECT Id				= ROW_NUMBER() OVER(ORDER BY report ASC)
			  ,r.[Latitude]
			  ,r.[Longitude]
			  ,r.[Odometer]
			  ,r.[VSSSpeed]
			  ,r.[InputStatus]
			  ,InputStatus2	 = NULL
			  ,r.[ReportId]
			  ,[Event]          = grc.[Name]
			  ,EventType        = CASE WHEN r.ReportId = 1
			  						THEN 1 
			  						ELSE CASE WHEN r.ReportId = 0
			  								THEN 0 
			  								ELSE NULL
			  							END
			  						END 
			  ,r.[RepDateTime]
			  ,r.[MainVolt]
			  ,[Status]			= NULL
		INTO #ReportsFill
		FROM Reports r WITH(NOLOCK)
		LEFT JOIN [Efficiency].[GPSReportCodes] grc 
			ON r.ReportID = grc.[Value]
		WHERE [RepDateTime] BETWEEN @lDateIni AND @lDateFin
		AND (@pAllEvents = 1 OR r.ReportId IN (0,1))
		AND Device = @pDevice
	END
	ELSE IF @lIsAtrack = 1  	
	BEGIN
		
		SELECT Id				= ROW_NUMBER() OVER(ORDER BY report ASC)
			  ,r.[Latitude]
			  ,r.[Longitude]
			  ,r.[Odometer]
			  ,r.[VSSSpeed]
			  ,r.[InputStatus]
			  ,r.[ReportId]
			  ,[Event]          = 'Undefined by status'
			  ,EventType        = null
			  ,r.[RepDateTime]
			  ,r.[MainVolt]
			  ,[Status]         = CASE WHEN r.[InputStatus] % 2 = 1
			  			 		  THEN 1 
			  			 		  ELSE CASE WHEN r.[InputStatus] % 2 = 0
			  			 		  		THEN 0 
			  			 		  		ELSE NULL
			  			 		  	END
			  			  		  END 
		into #Data
		FROM [dbo].[Reports] r
		WHERE [Device] = @pDevice 
		AND [RepDateTime] BETWEEN @lDateIni AND @lDateFin
		ORDER BY [RepDateTime] ASC
						
		SELECT 
		  Id				= 	ROW_NUMBER() OVER(ORDER BY T1.ID ASC)
		, t1.[Latitude]
		, t1.[Longitude]
		, t1.[Odometer]
		, t1.[VSSSpeed]
		, t1.[InputStatus]
		, t1.[ReportId]
		, [Event]          = case when (t1.[InputStatus] = 0) then 'Encendido' else 'Apagado' end        
		, EventType        = case when (t1.[InputStatus]  != t2.[InputStatus]) then t1.[InputStatus] else null end 
		, t1.[RepDateTime]
		, t1.[MainVolt]
		, t1.[Status]        
		INTO #ChangeEvent 
		FROM #Data t1, #Data t2 
		WHERE t1.[Id] = (t2.[Id] + 1) 
		AND (t1.[InputStatus] % 2  != t2.[InputStatus] % 2)

		SELECT * FROM #ChangeEvent
		
		DROP TABLE #ChangeEvent;
		DROP TABLE #Data;

		RETURN
	END

	-------------------------------------------------------------------------
	/*---< WHILE SET THE ENGINE ON / OFF RANGE RANGES*/
	-------------------------------------------------------------------------	
	SELECT @lCount = MIN(id) FROM #ReportsFill
	
	WHILE @lCount is not null
	BEGIN

		SELECT @lStatus = ISNULL(EventType,@lStatus)
		FROM #ReportsFill
		WHERE Id = @lCount

		UPDATE #ReportsFill
	    SET [Status] = @lStatus
		WHERE id = @lCount

		SELECT @lCount = MIN(id) FROM #ReportsFill WHERE id > @lCount

	END
	
	-----------------------------------------------------------------------------
	/* ESTABLISH THE FIRST STATE FOR REPORTID, FOR THE FIRST UNLOADED RECORDS */
	----------------------------------------------------------------------------
	SELECT @lIdFirstStatus = MAX(id)+1
	FROM #ReportsFill
	WHERE [Status] = -1

	SELECT @lFirstStatus = [Status]
	FROM #ReportsFill
	WHERE id = @lIdFirstStatus

	UPDATE #ReportsFill 
	SET [Status] = CASE WHEN @lFirstStatus = 1 THEN 0 ELSE 1 END
	WHERE id < @lIdFirstStatus

	SELECT Id	
		  ,Latitude	
		  ,Longitude	
		  ,Odometer	
		  ,VSSSpeed	
		  ,InputStatus	
		  ,ReportId	
		  ,[Event] = CASE WHEN [Status] = 1 THEN 'Apagado' ELSE 'Encendido' END 	
		  ,EventType	
		  ,RepDateTime	
		  ,MainVolt	
		  ,[Status]
	FROM #ReportsFill

	DROP TABLE #ReportsFill;

	SET NOCOUNT OFF;
END

GO
