USE ECOsystemDev
GO


IF OBJECT_ID('[Efficiency].[Fn_IsAtrackDevice]', 'FN') IS NOT NULL
	DROP FUNCTION [Efficiency].[Fn_IsAtrackDevice]
GO


-- ==================================================
-- Author:      Juan Carlos Santamaria V.
-- Create Date: 10-19-2019
-- Description: Return 1 if AtrackDevice, 0 if not
-- ==================================================
CREATE FUNCTION [Efficiency].[Fn_IsAtrackDevice]
(
    @pDevice INT 
)
RETURNS INT
AS
BEGIN
    DECLARE @lResult INT = 0

	/*------< DEVICE MODEL ATRACK>------*/
	--13 AT1(E) 
	--18 AT5

	SELECT @lResult = COUNT(1)
	FROM General.Vehicles v WITH(NOLOCK)	
	INNER JOIN dbo.Devices d WITH(NOLOCK) 
		ON V.DeviceReference	 = D.Device 
	INNER JOIN dbo.DispositivosAVL da WITH(NOLOCK) 
		ON d.UnitID	= da.terminalID
 	WHERE da.modelo	IN (13,18)
	AND v.DeviceReference = @pDevice
		  
	IF @lResult > 0
		SET @lResult = 1
	
	RETURN @lResult

END
GO

