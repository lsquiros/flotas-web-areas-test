
IF EXISTS(SELECT 1 FROM sys.objects WHERE name = 'SP_VehicleTransmission_Report_TEST' AND type = 'P')
	DROP PROC [Efficiency].[SP_VehicleTransmission_Report_TEST]
GO

-- ================================================================================================  
-- Author:  Esteban Solís  
-- Create date: 27/03/2018  
-- Description: Retrieve Vehicle transmissions Report information 
-- Stefano Quirós - Change GPSDateTime to RepDateTime value - 20/09/2018 
-- Modify by: Juan C. Santamaria -- Date: 10-16-2019 -- Description: Turn On Turn Off Vehicle -- ID: FLOT-868
-- ================================================================================================      
CREATE PROCEDURE [Efficiency].[SP_VehicleTransmission_Report_TEST]
(
 @pVehicleId INT
,@pYear INT = NULL
,@pMonth INT = NULL
,@pStartDate DATETIME = NULL
,@pEndDate DATETIME = NULL

)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lDevice INT
		   ,@lDateIni DATETIME
		   ,@lDateFin DATETIME
		   ,@lTimeZoneParameter INT
		   ,@lOnDesc VARCHAR(10) = 'Encendido'
		   ,@lOffDesc VARCHAR(10) = 'Apagado'
		   ,@lStopTime INT = 2
		   ,@lCustomerId INT

	DECLARE @STATUS_VEHICLE_ON  INT = 0  ---FLOT 968
		   ,@STATUS_VEHICLE_OFF INT = 1  ---FLOT 968
		   ,@REPORTID_OFF INT = 1
	
	IF OBJECT_ID('tempdb.dbo.#Data', 'U') IS NOT NULL
		DROP TABLE #Data;

		CREATE TABLE #Data(
			 [Id]			INT IDENTITY NOT NULL
			,[Latitude]		FLOAT		NOT NULL
			,[Longitude]	FLOAT		NOT NULL
			,[Odometer]		FLOAT		NOT NULL
			,[VSSSpeed]		FLOAT		NOT NULL
			,[InputStatus]	TINYINT		NOT NULL
			,[ReportId]		INT			NOT NULL
			,[Event]		VARCHAR(25) NULL
			,[EventType]	INT			NULL
			,[RepDateTime]	DATETIME	NOT NULL
			,[MainVolt]     FLOAT       NULL
			,[Status]		BIT
			,[EventDetail]	VARCHAR(1000)
			,[Location]		VARCHAR(5000)
			,[Distance]		FLOAT
			,[BatteryStatus] VARCHAR(100)
			)

	SELECT @lDevice = [DeviceReference],
	       @lCustomerId = [CustomerId]
	FROM [General].[Vehicles]
	WHERE [VehicleId] = @pVehicleId


	SELECT @lTimeZoneParameter = [TimeZone]	     
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu 
		ON co.[CountryId] = cu.[CountryId]
	INNER JOIN [General].[Vehicles] v 
		ON cu.[CustomerId] = v.[CustomerId]
	WHERE v.[VehicleId] = @pVehicleId

	IF (
			@pYear IS NOT NULL
			AND @pMonth IS NOT NULL
			)
	BEGIN
		SET @lDateIni = DATEADD(hh, @lTimeZoneParameter * - 1, CAST(CAST(@pYear AS VARCHAR) + '-' + CAST(@pMonth AS VARCHAR) + '-' + CAST(1 AS VARCHAR) AS DATETIME))
		SET @lDateFin = DATEADD(hh, @lTimeZoneParameter * - 1, DATEADD(m, 1, @lDateIni))
	END
	ELSE
	BEGIN
		SET @lDateIni = DATEADD(hh, @lTimeZoneParameter * - 1, @pStartDate)
		SET @lDateFin = DATEADD(hh, @lTimeZoneParameter * - 1, @pEndDate)
	END

	IF (
			@lDateIni IS NOT NULL
			AND @lDateFin IS NOT NULL
			)
	BEGIN
	/*---------------< FLOT-968 OLD >--------------------
		--INSERT INTO #Data (
		--	 [RepDateTime]
		--	,[Latitude]
		--	,[Longitude]
		--	,[Odometer]
		--	,[InputStatus]
		--	,[VSSSpeed]
		--	,[ReportId]
		--	,[MainVolt]
		--	,[Status]
		--	)
		SELECT r.[RepDateTime]
			  ,r.[Latitude]
			  ,r.[Longitude]
			  ,r.[Odometer]
			  ,r.[InputStatus]
			  ,r.[VSSSpeed]
			  ,r.[ReportId]
			  ,r.[MainVolt]
			  ,CASE r.[InputStatus] % 2
			  	WHEN 0
			  		THEN @STATUS_VEHICLE_OFF
			  		ELSE @STATUS_VEHICLE_ON
			  	END
		FROM [dbo].[Reports] r
		WHERE r.[Device] = @lDevice
			AND r.[RepDateTime] BETWEEN @lDateIni
				AND @lDateFin
		ORDER BY r.[RepDateTime] ASC
	----------------------------------------------------------------*/
	--/*----------------------< NEW FLOT-868 >------------------------
	INSERT INTO #Data(Id,Latitude,Longitude,Odometer,VSSSpeed,InputStatus,ReportId,[Event],EventType,RepDateTime,MainVolt,[Status])
	EXEC sp_ReportsOnOffStartingEngine
		 @pStartDate  = @lDateIni
		,@pEndDate    = @lDateFin
		,@pDevice     = @lDevice
		,@pAllEvents  = 1  /*1: all events || 0: only On Off engine */
	------------------------------------------------------------------*/
	   
		UPDATE d
		SET d.[Distance] = (d.[Odometer] - r.[Odometer])
		FROM #Data d
		CROSS APPLY (
			SELECT TOP 1 d1.[Odometer]
			FROM #data d1
			WHERE d1.[ReportId] = 0
				AND d1.[Id] < d.[Id]
			ORDER BY d1.[Id] DESC
			) AS r
		WHERE d.[ReportId] = @REPORTID_OFF
			AND d.[Distance] IS NULL
			AND d.[Status] = @STATUS_VEHICLE_OFF

		UPDATE d
		SET d.[Distance] = (d.[Odometer] - r.[Odometer])
		FROM #Data d
		CROSS APPLY (
			SELECT TOP 1 d1.[Odometer]
			FROM #data d1
			WHERE d1.[Id] < d.[Id]
			ORDER BY d1.[Id]
			) AS r
		WHERE d.[ReportId] = @REPORTID_OFF
			AND d.Distance IS NULL
			AND d.[Status] = @STATUS_VEHICLE_OFF

		UPDATE d
		SET d.[EventDetail] = c.[Description]
			,d.[Location] = ISNULL([Efficiency].[Fn_GetAddressByLatLon_Retrieve](d.[Longitude], d.[Latitude],@lCustomerId), '')
			,d.[BatteryStatus] = b.[Description]
		FROM #Data d
		JOIN [Efficiency].[GPSReportCodes] c ON d.[ReportId] = c.[Value]
		JOIN [Efficiency].[BatteryVoltRanges] b ON (
				d.[Status] = 0
				AND d.[MainVolt] >= b.[LowValueOn]
				AND d.[MainVolt] < b.[HighValueOn]
				)
			OR (
				d.[Status] = 1
				AND d.[MainVolt] >= b.[LowValueOff]
				AND d.[MainVolt] < b.[HighValueOff]
				)
	END

	-- Return result
	SELECT DATEADD(HOUR, @lTimeZoneParameter, d.[RepDateTime]) AS [ReportDate]
		  ,d.[Latitude]
		  ,d.[Longitude]
		  ,d.[VSSSpeed] AS [ReportSpeed]
		  ,d.[EventDetail]
		  ,d.[Location]
		  ,CONVERT(DECIMAL(18,2),d.[Distance]) AS [ReportDistance]
		  ,[BatteryStatus]
		  ,CASE 
		  	WHEN d.[Status] = @STATUS_VEHICLE_ON
		  		THEN 'SI'
		  	ELSE 'NO'
		  	END AS [IsVehicleOn]
	FROM #Data d

	--REMOVE TABLES  
	DROP TABLE #Data

	SET NOCOUNT OFF
END

GO



