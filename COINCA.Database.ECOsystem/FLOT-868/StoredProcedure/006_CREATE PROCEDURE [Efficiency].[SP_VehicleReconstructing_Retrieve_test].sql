USE ECOsystemDev
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name LIKE 'SP_VehicleReconstructing_Retrieve' AND type = 'P')
	DROP PROC [Efficiency].SP_VehicleReconstructing_Retrieve
GO 

-- ================================================================================================      
-- Author:  Henry Retana      
-- Create date: 01/03/2017      
-- Description: Retrieve Vehicle Reconstructing      
-- Modify the Summary Report, Henry Retana - 1/6/2017      
-- Modify: Creacion de rutas desde [Odometer], Esteban Solis, 2017-10-12  
-- Modify: Se agregan parametros de distancia minima y tiempo minimo de parada, Esteban Solis, 2017-12-28  
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to RepDateTime
-- Modify: Henry Retana - 02/05/2019 - Set the dates to midnight
-- Modify: Henry Retana - 14/06/2019 - Validate and check last event status
-- Modify by: Juan C. Santamaria -- Date: 10-16-2019 -- Description: Turn On Turn Off Vehicle -- ID: FLOT-868
-- ================================================================================================

CREATE PROCEDURE [Efficiency].[SP_VehicleReconstructing_Retrieve]
(
	@pVehicleId INT, 
	@pStartDate DATETIME, 
	@pEndDate DATETIME = NULL, 
	@pCustomerId INT = NULL
)
AS
    BEGIN
		SET NOCOUNT ON;
		DECLARE @lIterator INT
			   ,@lLastIterator INT= 1
			   ,@lDevice INT
			   ,@lDateIni DATETIME
			   ,@lDateFin DATETIME
			   ,@lTimeZoneParameter INT
			   ,@lCounter INT
			   ,@lIsAttrack INT= 0
			   ,@lStopDetail INT= NULL
			   ,@lMinimumDistance DECIMAL(16)
			   ,@lMinimumStopTime DECIMAL(16)
			   ,@lCustomerId INT= NULL
			   ,@INGICION_ON VARCHAR(10)  = 'Encendido'
			   ,@INGICION_OFF VARCHAR(10) = 'Apagado'
			   ,@LALL_EVENTS  TINYINT = 1 
			
        -- SET DATES IN THE CORRECT FORMAT
        SET @pStartDate = DATEADD(DAY, DATEDIFF(DAY, 0, @pStartDate), 0);
        SET @pEndDate = ISNULL(@pEndDate, DATEADD(DAY, 1, @pStartDate));

        SELECT @lIsAttrack = COUNT(*)
        FROM [General].[Vehicles]
        WHERE [VehicleId] = @pVehicleId
        AND [DeviceReference] IN (SELECT [Device]
								  FROM [dbo].[Devices]
								  WHERE [UnitId] IN
									(SELECT [TerminalId]
									 FROM [dbo].[DispositivosAVL]
									 WHERE [Modelo] = 18
									)
								);

        SELECT @lStopDetail = CONVERT(INT, [Value])
        FROM [dbo].[GeneralParameters]
        WHERE [ParameterID] = 'STOPDETAIL_PARAMETER';

		--------< OLD FLOT-868 >---------
			-- CREATE TABLE #Data
			--([Id]          INT IDENTITY NOT NULL, 
			-- [RepDatetime] DATETIME, 
			-- [Latitude]    FLOAT, 
			-- [Longitude]   FLOAT, 
			-- [Odometer]    FLOAT, 
			-- [InputStatus] TINYINT, 
			-- [VSSSpeed]    FLOAT, 
			-- [ReportId]    INT
			--);

		------< new >-----
		CREATE TABLE #Data(
			[Id]			INT IDENTITY NOT NULL,
			[Latitude]		FLOAT		NOT NULL,
			[Longitude]		FLOAT		NOT NULL,
			[Odometer]		FLOAT		NOT NULL,
			[VSSSpeed]		FLOAT		NOT NULL,
			[InputStatus]	TINYINT		NOT NULL,
			[ReportId]		INT			NOT NULL,
			[Event]			VARCHAR(25) NULL,
			[EventType]		INT			NULL,
			[RepDateTime]	DATETIME	NOT NULL,
			[MainVolt]      FLOAT       NULL,
			[Status]		TINYINT		NULL
			) ON [PRIMARY] 
		CREATE NONCLUSTERED INDEX [IX_Reports_ReportId] ON #Data([ReportId] ASC)

        CREATE TABLE #Result
			([Id]        INT IDENTITY, 
			 [Event]     VARCHAR(20), 
			 [StartDate] DATETIME, 
			 [EndDate]   DATETIME, 
			 [Latitude]  FLOAT, 
			 [Longitude] FLOAT, 
			 [Odometer]  FLOAT, 
			 [MaxSpeed]  FLOAT, 
			 [Stops]     INT
			);

        SELECT @lDevice = [DeviceReference], 
               @lCustomerId = [CustomerId]
        FROM [General].[Vehicles]
        WHERE [VehicleId] = @pVehicleId;
        
		SELECT @lTimeZoneParameter = [TimeZone]
        FROM [General].[Countries] co
        INNER JOIN [General].[Customers] cu 
			ON co.[CountryId]  = cu.[CountryId]
        INNER JOIN [General].[Vehicles] v 
			ON cu.[CustomerId] = v.[CustomerId]
        WHERE v.[VehicleId] = @pVehicleId;
        
		SET @lDateIni = DATEADD(hh, @lTimeZoneParameter * -1, @pStartDate);
        SET @lDateFin = DATEADD(hh, @lTimeZoneParameter * -1, @pEndDate);
        
		IF(@lDateIni IS NOT NULL AND @lDateFin IS NOT NULL)
        BEGIN

			-------------< FLOT-868 OLD >--------------------
                --INSERT INTO #Data
                --([RepDatetime], 
                -- [Latitude], 
                -- [Longitude], 
                -- [Odometer], 
                -- [InputStatus], 
                -- [VssSpeed], 
                -- [ReportId]
                --)
                --       SELECT [RepDatetime], 
                --              [Latitude], 
                --              [Longitude], 
                --              [Odometer], 
                --              [InputStatus], 
                --              [vssspeed], 
                --              [ReportId]
                --       FROM [dbo].[Reports]
                --       WHERE [Device] = @lDevice
                --             AND [RepDatetime] BETWEEN @lDateIni AND @lDateFin
                --       ORDER BY [RepDatetime] ASC;

                --SELECT t1.[Id], 
                --       t1.[RepDatetime], 
                --       t1.[Latitude], 
                --       t1.[Longitude], 
                --       t1.[InputStatus]
                --INTO #ChangeEvent
                --FROM #Data t1, 
                --     #Data t2
                --WHERE t1.[Id] = (t2.[Id] + 1)
                --      AND (t1.[InputStatus] % 2) != (t2.[InputStatus] % 2);

			--/*----------------------< NEW >----------------------------------------------------------------
			INSERT INTO #Data([Id],[Latitude],[Longitude],[Odometer],[VSSSpeed],[InputStatus],[ReportId],[Event],[EventType],[RepDateTime],[MainVolt],[Status])
				EXEC sp_ReportsOnOffStartingEngine
					 @pStartDate  = @lDateIni
					,@pEndDate    = @lDateFin
					,@pDevice     = @lDevice
					,@pAllEvents  = @LALL_EVENTS  
       
          
				SELECT t1.[Id], 
                       t1.[RepDatetime], 
                       t1.[Latitude], 
                       t1.[Longitude], 
                       t1.[InputStatus],
					   t1.[Event]
				  INTO #ChangeEvent 
				  FROM #Data t1
				 WHERE EventType IS NOT NULL 
			ORDER BY Id
			--/*------------< FLOT-868 END CHANGE >-----------------------------------------------------------

				SELECT @lIterator = MIN([Id])
                FROM #ChangeEvent;
				
				WHILE(@lIterator IS NOT NULL)
				BEGIN
                        DECLARE @vIdentidad INT;
                        INSERT INTO #Result
                        ([StartDate], 
                         [EndDate], 
                         [Odometer], 
                         [MaxSpeed]
                        )
                               SELECT MIN([RepDatetime]), 
                                      MAX([RepDatetime]), 
                                      (MAX([Odometer]) - MIN([Odometer])), 
                                      MAX([VSSSpeed])
                               FROM #Data
                               WHERE [Id] BETWEEN @lLastIterator AND @lIterator;

                        SET @vIdentidad = @@Identity;
                        IF @lIsAttrack = 0
                            BEGIN
                                UPDATE #Result
                                  SET 
                                      [Stops] = v.[Stops]
                                FROM
                                (
                                    SELECT COUNT([VSSSpeed]) [Stops]
                                    FROM #Data
                                    WHERE [ReportId] = @lStopDetail
                                          AND [Id] BETWEEN @lLastIterator AND @lIterator
                                ) v
                                WHERE [Id] = @vIdentidad;
                        END;
                            ELSE
                            BEGIN
                                UPDATE #Result
                                  SET 
                                      [Stops] = v.[Stops]
                                FROM
                                (
                                    SELECT COUNT([VSSSpeed]) Stops
                                    FROM #Data
                                    WHERE [VSSSpeed] = 0
                                          AND [Id] BETWEEN @lLastIterator AND @lIterator
                                ) v
                                WHERE [Id] = @vIdentidad;
                        END;
                        UPDATE #Result
                          SET 
                              [Event] = v.[Event], 
                              [Latitude] = v.[Latitude], 
                              [Longitude] = v.[Longitude]
                        FROM
                        (
                            SELECT [Event], ---FLOT-868 ONLY NEED EVENT NAME, "CASE" IT'S NO NECESARY
                                   [Latitude], 
                                   [Longitude]
                            FROM #ChangeEvent
                            WHERE [Id] = @lIterator
                        ) v
                        WHERE [Id] = @vIdentidad;
                        DELETE FROM #ChangeEvent
                        WHERE [Id] = @lIterator;
                        SET @lLastIterator = @lIterator;
                        SELECT @lIterator = MIN([Id])
                        FROM #ChangeEvent;
				END;

                SELECT @lIterator = MAX([Id])
                FROM #Data;
                IF EXISTS
                (
                    SELECT *
                    FROM #Data
                )
                    BEGIN
                        INSERT INTO #Result
                        ([StartDate], 
                         [EndDate], 
                         [Odometer], 
                         [MaxSpeed]
                        )
                               SELECT MIN([RepDatetime]), 
                                      NULL, 
                                      (MAX([Odometer]) - MIN([Odometer])), 
                                      MAX([VSSSpeed])
                               FROM #Data
                               WHERE [Id] BETWEEN @lLastIterator AND @lIterator;
                END;
                SET @vIdentidad = @@Identity;
                IF @lIsAttrack = 0
                    BEGIN
                        UPDATE #Result
                          SET 
                              [Stops] = v.[Stops]
                        FROM
                        (
                            SELECT COUNT([VSSSpeed]) [Stops]
                            FROM #Data
                            WHERE [ReportId] = @lStopDetail
                                  AND [Id] BETWEEN @lLastIterator AND @lIterator
                        ) v
                        WHERE [Id] = @vIdentidad;
                END;
                    ELSE
                    BEGIN
                        UPDATE #Result
                          SET 
                              [Stops] = v.[Stops]
                        FROM
                        (
                            SELECT COUNT([VSSSpeed]) Stops
                            FROM #Data
                            WHERE [VSSSpeed] = 0
                                  AND [Id] BETWEEN @lLastIterator AND @lIterator
                        ) v
                        WHERE [Id] = @vIdentidad;
                END;
                UPDATE #Result
                  SET 
                      [Event] = v.[Event], 
                      [Latitude] = v.[Latitude], 
                      [Longitude] = v.[Longitude]
                FROM
                (
                    SELECT [Event], ---FLOT-868 ONLY NEED EVENT NAME, "CASE" IT'S NO NECESARY 
                           [Latitude], 
                           [Longitude]
                    FROM #Data
                    WHERE [Id] = @lIterator
                ) v
                WHERE [Id] = @vIdentidad;
        END;
        
		UPDATE #Result
        SET [Stops] = 0, 
            [MaxSpeed] = 0
        WHERE [Event] = @INGICION_OFF --FLOT-868 'Apagado';

        DROP TABLE #Data;
        DROP TABLE #ChangeEvent;
        
		SET @lMinimumDistance =
        (
            SELECT pc.[Value]
            FROM [General].[Vehicles] v
                 LEFT JOIN [General].[ParametersByCustomer] pc ON pc.[CustomerId] = v.[CustomerId]
            WHERE v.[VehicleId] = @pVehicleId
                  AND pc.[Name] = 'MinimumDistanceBetweenPoints'
        );

        SET @lMinimumStopTime =
        (
            SELECT pc.[Value]
            FROM [General].[Vehicles] v
                 LEFT JOIN [General].[ParametersByCustomer] pc ON pc.[CustomerId] = v.[CustomerId]
            WHERE v.[VehicleId] = @pVehicleId
                  AND pc.[Name] = 'MinimumStopTime'
        );

        SELECT [Id] [GPSId], 
               @pVehicleId [VehicleId], 
               DATEADD(hh, @lTimeZoneParameter, [StartDate]) AS StartDate, 
               [StartDate] [StartDateUTC], 
               ISNULL(DATEADD(hh, @lTimeZoneParameter, [EndDate]),
                                                                CASE
                                                                    WHEN CAST(@pStartDate AS DATE) = CAST(DATEADD(HOUR, -6, GETDATE()) AS DATE)
                                                                    THEN DATEADD(HOUR, -6, GETDATE())
                                                                    ELSE @pEndDate
                                                                END) [EndDate], 
               ISNULL([EndDate],
                      CASE
                          WHEN CAST(@pStartDate AS DATE) = CAST(DATEADD(HOUR, -6, GETDATE()) AS DATE)
                          THEN GETDATE()
                          ELSE DATEADD(hh, @lTimeZoneParameter * -1, @pEndDate)
                      END) [EndDateUTC],
               CASE
                   WHEN [Event] = @INGICION_ON --FLOT-868 'Encendido'
                   THEN ISNULL([Efficiency].[Fn_GetOverSpeedSummaryByVehicle](@pVehicleId, [StartDate], [EndDate], 0), '')
                   ELSE ''
               END [OverSpeedSummary], 
               [Event] [Status], 
               CONVERT(INT, [MaxSpeed]) [MaxSpeed], 
               [Odometer], 
               [Stops], 
               ISNULL([Efficiency].[Fn_GetAddressByLatLon_Retrieve]([Longitude], [Latitude], @lCustomerId), '') [Location], 
               CONVERT(VARCHAR(256), [Longitude]) [Longitud], 
               CONVERT(VARCHAR(256), [Latitude]) [Latitud], 
               ISNULL(@lMinimumDistance, 0) [MinimumDistanceBetweenPoints], 
               ISNULL(@lMinimumStopTime, 0) [MinimumStopTime]
        FROM #Result
        ORDER BY 3;
        DROP TABLE #Result;
        SET NOCOUNT OFF;
    END;


GO


