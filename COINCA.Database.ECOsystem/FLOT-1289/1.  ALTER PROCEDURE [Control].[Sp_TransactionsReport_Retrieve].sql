
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_TransactionsReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_TransactionsReport_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--=======================================================================================  
-- Author:  Danilo Hidalgo  
-- Create date: 05/12/2014  
-- Description: Retrieve TransactionsReport information -- Update Retrieve CustomerName  
-- Update 1/22/2015 - Dinamic Filter, Retrieve Vehicle Name  
-- Update 19/08/2016 - Cindy Vargas - Add the transaction country name to the retrieve information  
-- Update 29/08/2016 - Cindy Vargas - Add the transaction ExchangeValue and Amount to the retrieve   
-- information  
-- Modify 29/09/2016 - Henry Retana - Change the retrieve in the exchange value, change the country name  
-- Modify By: Stefano Quirós - Add the Time Zone Parameter - 12/06/2016 - Add the Driver Name on the   
-- consult - 02/13/2017  
-- Modify 05/05/2017 - Henry Retana - Add Offline Transactions  
-- Modify 08/29/2017 - Henry Retana - Validate Transaction Offline Flag in the transaccions  
-- Modify by Henry Retana - 19/09/2017  
-- Use function to get the driver name   
-- Modify 09/28/2017 - Esteban Solis - Added new function [General].[GetServiceStationName]  
-- Modify by Henry Retana - 14/11/2017  
-- Add Void Transactions Validation  
-- Modify: Esteban Solís  15/12/2017  
-- Added parameter @pDriverId
-- Modify: Albert Estrada  15/17/2017   Add Filter 
-- Modify By: Stefano Quirós - 22/12/2017 -- Add IsInternational to the retrieve
-- Modified By: Kevin Peña - 24/01/2018 -- Add Invoice Key Search
-- Modify by Henry Retana - 28/05/2018
-- Add [UnitOfCapacityId] 
-- Maria de los Angeles Jimenez Chavarria - FEB/01/2019 - Send report program
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- Juan Carlos Santamaria V. - 12/27/2019 - Add Column 'NOMBRE DE LA EMPRESA' Customer Branch - JSA-001
-- Modify by Marjorie Garbanzo - 28/05/2020 Add PaymentInstrumentCode and PaymentInstrumentType column
-- ========================================================================================================  																										   
CREATE PROCEDURE [Control].[Sp_TransactionsReport_Retrieve] --14031, null, 6, null, null, null, '2019-09-09 00:00:00.000', '2019-09-10 00:00:00.000', null, null, 1--30, null, 2, null, 2019, 6, null, null, null, null, 1
(
--DECLARE
	 @pCustomerId INT
	,@pDriverId INT = NULL
	,@pStatus INT = NULL
	,@pKey VARCHAR(800) = NULL
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
	,@pCostCenterId INT = NULL
	,@pVehicleGroupId INT = NULL
	,@pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON

	SET @pStatus = CASE WHEN @pStatus = 0 THEN 1 ELSE @pStatus END

	-- DYNAMIC FILTER  
	DECLARE @Results TABLE (items INT)
	DECLARE @count INT
	
	INSERT @Results
	EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId,'VH', @pCustomerId

	SET @count = (SELECT COUNT(1) FROM @Results)

	-- END  
	DECLARE @pIssueForId INT
	DECLARE @lTimeZoneParameter INT
	DECLARE @lConvertionValue DECIMAL(16, 8) = 3.78541178
	DECLARE @lPartnerUnit INT
	DECLARE @TERMINAL_NO_FLOTA CHAR(31) = 'Terminal NO existente en Flotas'
	DECLARE @lIsCustomerBranch INT = 0

	SET @pIssueForId = (
			SELECT [IssueForId]
			FROM [General].[Customers]
			WHERE [CustomerId] = @pCustomerId
			)

	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId

	SELECT @lPartnerUnit = [CapacityUnitId] 
	FROM [General].[Partners] p
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[PartnerId] = p.[PartnerId]
	WHERE [CustomerId] = @pCustomerId

	IF @pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
	BEGIN
		SET @pYear = NULL
	    SET @pMonth = NULL
	END

	SELECT *  /*JSA-001*/
	INTO #Customers
	FROM (
		SELECT   a.[CustomerId]
				,a.[Name]
				,a.[Name] AS CustomerBranchName
				,a.[CustomerId] AS CustomerBranch
				,a.[UnitOfCapacityId]
				,a.[CurrencyId]
				,a.[CountryId]
		FROM [General].[Customers] a
		WHERE a.[CustomerId] = @pCustomerId	
		------------------------------------
		UNION /*add CustomerBracnh */
		------------------------------------
		SELECT   cb.[CustomerId]
				,cb.[Name]
				,a.[Name] AS CustomerBranchName
				,cb.CustomerBranch
				,cb.[UnitOfCapacityId]
				,cb.[CurrencyId]
				,a.[CountryId]
		FROM [General].[Customers] a
		INNER JOIN [General].[Customers] cb on a.CustomerId = cb.CustomerBranch
		WHERE a.[CustomerId] = @pCustomerId
		AND cb.CustomerBranch IS NOT NULL
	) AS Customers

	SELECT @lIsCustomerBranch = COUNT(1)
	FROM #Customers

	IF @pIssueForId = 100
	BEGIN
		DECLARE @lTransactionsOffline BIT

		SET @lTransactionsOffline = CASE 
				WHEN @pStatus <> 1
					AND @pStatus = 7
					THEN 1
				ELSE 0
				END

		IF @pStatus = 1 OR @pStatus = 8
		BEGIN
			--DRIVER  
			SELECT t.[TransactionId] AS [TransactionId]
				,t.[CreditCardId] AS [CreditCardId]
				,cc.CreditCardNumber AS [CreditCardNumber]
				,cc.CreditCardNumber AS [PaymentInstrumentCode]
				,'Tarjeta' [PaymentInstrumentType]
				,ISNULL(t.[Invoice], '-') AS [Invoice]
				,CASE WHEN t.[IsInternal] = 1 AND t.[AuthorizationNumber] IS NULL
				 THEN CONVERT(VARCHAR(500), t.[TransactionId])
				 ELSE t.[AuthorizationNumber]
				 END [AuthorizationNumber]
				,CASE 
					WHEN [General].[GetServiceStationInfo](LTRIM(RTRIM(t.[ProcessorId])), c.[CustomerId], 0) IS NULL THEN @TERMINAL_NO_FLOTA
					ELSE [General].[GetServiceStationInfo](LTRIM(RTRIM(t.[ProcessorId])), c.[CustomerId], 0)
					END AS [MerchantDescription]
				,t.[TransactionPOS] AS [SystemTraceCode]
				,u.[Name] [HolderName]
				,
				--t.[Date] [Date],   
				DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date]
				,f.[Name] [FuelName]
				,t.[FuelAmount] [FuelAmount]
				,t.[Odometer] [Odometer]
				,CASE WHEN @lPartnerUnit = 1 THEN t.[Liters] * @lConvertionValue
				 ELSE t.[Liters] 
				 END [Liters]
				,v.[PlateId] [PlateId]
				,g.[Symbol] [CurrencySymbol]
				,(
					CASE 
						WHEN t.[IsReversed] = 1
							AND t.[IsVoid] = 1
							THEN 'Reversada'
						WHEN t.[IsDuplicated] = 1
							THEN 'Duplicada'
						WHEN t.[IsFloating] = 1
							THEN 'Flotante'
						WHEN t.[IsReversed] = 0
							AND t.[IsVoid] = 1
							THEN 'Anulada'
						ELSE 'Procesada'
						END
					) AS [State]
				,@pCostCenterId [CostCenterId]
				,@pVehicleGroupId [VehicleGroupId]
				,e.[Name] AS [CostCenterName]
				,CASE WHEN @pVehicleGroupId IS NULL 
				 THEN (SELECT TOP 1 vg.[Name] 
					   FROM [General].[VehicleGroups] vg
					   INNER JOIN [General].[VehiclesByGroup] vbg
							ON vbg.[VehicleGroupId] = vg.[VehicleGroupId] 
					   INNER JOIN [General].[Vehicles] v2
							ON v.[VehicleId] = vbg.[VehicleId]
					   WHERE v.[VehicleId] = v2.VehicleId)
				 ELSE (SELECT [Name] 
					   FROM [General].[VehicleGroups]					  
					   WHERE [VehicleGroupId] = @pVehicleGroupId)
				 END [VehicleGroupName]
				,t.[ProcessorId] AS [TerminalId]
				,v.[Name] AS [VehicleName]
				,c.[Name] AS [CustomerName]
				,c.[CustomerBranchName]
				,IsCustomerBranch = @lIsCustomerBranch
				,ISNULL(co.[Name], (
						SELECT TOP 1 coun.[Name]
						FROM [General].[Countries] coun
						WHERE coun.[CountryId] = c.[CountryId]
						)) AS [CountryName]
				,(
					CASE 
						WHEN t.[ExchangeValue] IS NULL
							THEN 'N/A'
						WHEN t.[ExchangeValue] IS NOT NULL
							THEN CONCAT (
									t.[CountryCode]
									,':'
									,t.[ExchangeValue]
									,'; '
									,(
										SELECT CONCAT (
												coun.[Code]
												,':'
												,er.[Value]
												)
										FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cue ON er.[CurrencyCode] = cue.[CurrencyCode]
										INNER JOIN [General].[Countries] coun ON coun.[CountryId] = c.[CountryId]
										WHERE cue.[Code] = coun.[Code]
											AND (
												(
													er.[Until] IS NOT NULL
													AND t.[InsertDate] BETWEEN er.[Since]
														AND er.[Until]
													)
												OR (
													er.[Until] IS NULL
													AND (
														t.[InsertDate] BETWEEN er.[Since]
															AND GETDATE()
														)
													)
												)
										)
									)
						END
					) AS [ExchangeValue]
				,(
					CASE 
						WHEN t.[IsInternational] = 1
							AND (
								t.[CountryCode] IS NOT NULL
								AND t.[CountryCode] <> ''
								)
							AND t.[ExchangeValue] IS NOT NULL
							THEN [Control].[GetTransactionRealAmount](t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
						WHEN t.[IsInternational] = 0
							THEN t.FuelAmount
						ELSE t.FuelAmount
						END
					) AS [RealAmount]
				,ISNULL((SELECT [InternationalCard] 
						FROM [General].[CustomerCreditCards]
						WHERE [CustomerId] = c.[CustomerId]
							  AND [StatusId] = 100
							  AND [InternationalCard] = 1), 0) [IsInternational]	
				,@pStatus [FilterType]
				,c.[UnitOfCapacityId]
			FROM [Control].[Transactions] t
			INNER JOIN [General].[Vehicles] v ON t.[VehicleId] = v.[VehicleId]
			--INNER JOIN [General].[Customers] c ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN #Customers c ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN [Control].[Fuels] f ON t.[FuelId] = f.[FuelId]
			INNER JOIN [Control].[CreditCardByDriver] cd ON t.[CreditCardId] = cd.[CreditCardId]
			INNER JOIN [Control].[CreditCard] cc ON cc.CreditCardId = t.CreditCardId
			INNER JOIN [General].[Users] u ON cd.[UserId] = u.[UserId]
			INNER JOIN [Control].[Currencies] g ON c.[CurrencyId] = g.[CurrencyId]
			LEFT JOIN [General].[VehicleCostCenters] e ON e.CostCenterId = t.CostCenterId			
			LEFT OUTER JOIN [General].[Countries] co ON t.[CountryCode] = co.[Code]
			WHERE (
					@count = 0
					OR v.[VehicleId] IN (
						SELECT items
						FROM @Results
						)
					) -- DYNAMIC FILTER  
				--AND c.[CustomerId] = @pCustomerId
				AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
				AND (t.[IsFloating] IS NULL	OR t.[IsFloating] = 0)
				AND 1 > (
					SELECT ISNULL(COUNT(1), 0)
					FROM CONTROL.Transactions t2
					WHERE t2.[CreditCardId] = t.[CreditCardId]
						AND t2.[TransactionPOS] = t.[TransactionPOS]
						AND t2.[ProcessorId] = t.[ProcessorId]
						AND (
							t2.IsReversed = 1
							OR t2.IsVoid = 1
							)
					)
				AND (
					t.[TransactionOffline] IS NULL
					OR t.[TransactionOffline] = 0
					)
				AND (
					@pKey IS NULL
					OR u.[Name] LIKE '%' + @pKey + '%'
					OR v.[PlateId] LIKE '%' + @pKey + '%'
					OR t.[ProcessorId] LIKE '%' + @pKey + '%' 
					OR t.[Invoice] LIKE '%' + @pKey + '%'
					OR t.[MerchantDescription] LIKE '%' + @pKey + '%' 
					OR v.[Name] LIKE '%' + @pKey + '%'
					)
				AND (
					@pDriverId IS NULL
					OR @pDriverId = cd.[UserId]
					)
				AND (
					(
						@pYear IS NOT NULL
						AND @pMonth IS NOT NULL
						AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
						AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear
						)
					OR (
						@pStartDate IS NOT NULL
						AND @pEndDate IS NOT NULL
						AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate
							AND @pEndDate
						)
					)
				AND (@pCostCenterId IS NULL OR t.[CostCenterId] = @pCostCenterId)
			ORDER BY t.[InsertDate] DESC
		END
		ELSE
		BEGIN
			--DRIVER  
			SELECT t.[TransactionId] AS [TransactionId]
				,t.[CreditCardId] AS [CreditCardId]
				,cc.CreditCardNumber AS [CreditCardNumber]
				,cc.CreditCardNumber AS [PaymentInstrumentCode]
				,'Tarjeta' [PaymentInstrumentType]
				,ISNULL(t.[Invoice], '-') AS [Invoice]
				,CASE WHEN t.[IsInternal] = 1 AND t.[AuthorizationNumber] IS NULL
				 THEN CONVERT(VARCHAR(500), t.[TransactionId])
				 ELSE t.[AuthorizationNumber]
				 END [AuthorizationNumber]
				,CASE 
					WHEN [General].[GetServiceStationInfo](LTRIM(RTRIM(t.[ProcessorId])), c.[CustomerId], 0) IS NULL THEN @TERMINAL_NO_FLOTA
					ELSE [General].[GetServiceStationInfo](LTRIM(RTRIM(t.[ProcessorId])), c.[CustomerId], 0)
					END AS [MerchantDescription]
				,t.[TransactionPOS] AS [SystemTraceCode]
				,u.[Name] [HolderName]
				,
				--t.[Date] [Date],   
				DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date]
				,f.[Name] [FuelName]
				,t.[FuelAmount] [FuelAmount]
				,t.[Odometer] [Odometer]
				,CASE WHEN @lPartnerUnit = 1 THEN t.[Liters] * @lConvertionValue
				 ELSE t.[Liters] 
				 END [Liters]
				,v.[PlateId] [PlateId]
				,g.[Symbol] [CurrencySymbol]
				,(
					CASE 
						WHEN t.[IsReversed] = 1
							AND t.[IsVoid] = 1
							THEN 'Reversada'
						WHEN t.[IsDuplicated] = 1
							THEN 'Duplicada'
						WHEN t.[IsFloating] = 1
							THEN 'Flotante'
						WHEN t.[IsReversed] = 0
							AND t.[IsVoid] = 1
							THEN 'Anulada'
						ELSE 'Procesada'
						END
					) AS [State]
				,@pCostCenterId [CostCenterId]
				,@pVehicleGroupId [VehicleGroupId]
				,e.[Name] AS [CostCenterName]
				,CASE WHEN @pVehicleGroupId IS NULL 
				 THEN (SELECT TOP 1 vg.[Name] 
					   FROM [General].[VehicleGroups] vg
					   INNER JOIN [General].[VehiclesByGroup] vbg
							ON vbg.[VehicleGroupId] = vg.[VehicleGroupId] 
					   INNER JOIN [General].[Vehicles] v2
							ON v.[VehicleId] = vbg.[VehicleId]
					   WHERE v.[VehicleId] = v2.VehicleId)
				 ELSE (SELECT [Name] 
					   FROM [General].[VehicleGroups]					  
					   WHERE [VehicleGroupId] = @pVehicleGroupId)
				 END [VehicleGroupName]
				,t.[ProcessorId] AS [TerminalId]
				,v.[Name] AS [VehicleName]
				,c.[Name] AS [CustomerName]
				,c.[CustomerBranchName]
				,IsCustomerBranch = @lIsCustomerBranch
				,ISNULL(co.[Name], (
						SELECT TOP 1 coun.[Name]
						FROM [General].[Countries] coun
						WHERE coun.[CountryId] = c.[CountryId]
						)) AS [CountryName]
				,(
					CASE 
						WHEN t.[ExchangeValue] IS NULL
							THEN 'N/A'
						WHEN t.[ExchangeValue] IS NOT NULL
							THEN CONCAT (
									t.[CountryCode]
									,':'
									,t.[ExchangeValue]
									,'; '
									,(
										SELECT CONCAT (
												coun.[Code]
												,':'
												,er.[Value]
												)
										FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cue ON er.[CurrencyCode] = cue.[CurrencyCode]
										INNER JOIN [General].[Countries] coun ON coun.[CountryId] = c.[CountryId]
										WHERE cue.[Code] = coun.[Code]
											AND (
												(
													er.[Until] IS NOT NULL
													AND t.[InsertDate] BETWEEN er.[Since]
														AND er.[Until]
													)
												OR (
													er.[Until] IS NULL
													AND (
														t.[InsertDate] BETWEEN er.[Since]
															AND GETDATE()
														)
													)
												)
										)
									)
						END
					) AS [ExchangeValue]
				,(
					CASE 
						WHEN t.[IsInternational] = 1
							AND (
								t.[CountryCode] IS NOT NULL
								AND t.[CountryCode] <> ''
								)
							AND t.[ExchangeValue] IS NOT NULL
							THEN [Control].[GetTransactionRealAmount](t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
						WHEN t.[IsInternational] = 0
							THEN t.FuelAmount
						ELSE t.FuelAmount
						END
					) AS [RealAmount]
				,ISNULL((SELECT [InternationalCard] 
						FROM [General].[CustomerCreditCards]
						WHERE [CustomerId] = c.[CustomerId]
							  AND [StatusId] = 100
							  AND [InternationalCard] = 1), 0) [IsInternational]	
				,(SELECT [General].[Fn_TransportData_Reverse_Retrieve] (t.[TransactionId])) [Response]
				,@pStatus [FilterType]
				,c.[UnitOfCapacityId]
			FROM [Control].[Transactions] t
			INNER JOIN [General].[Vehicles] v ON t.[VehicleId] = v.[VehicleId]
			--INNER JOIN [General].[Customers] c ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN #Customers c ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN [Control].[Fuels] f ON t.[FuelId] = f.[FuelId]
			INNER JOIN [Control].[CreditCardByDriver] cd ON t.[CreditCardId] = cd.[CreditCardId]
			INNER JOIN [Control].[CreditCard] cc ON cc.CreditCardId = t.CreditCardId
			INNER JOIN [General].[Users] u ON cd.[UserId] = u.[UserId]
			INNER JOIN [Control].[Currencies] g ON c.[CurrencyId] = g.[CurrencyId]
			LEFT JOIN [General].[VehicleCostCenters] e ON e.CostCenterId = t.CostCenterId
			LEFT OUTER JOIN [General].[Countries] co ON t.[CountryCode] = co.[Code]
			WHERE (
					@count = 0
					OR v.[VehicleId] IN (
						SELECT items
						FROM @Results
						)
					) -- DYNAMIC FILTER  
				--AND c.[CustomerId] = @pCustomerId
				AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
				AND (
					@pStatus IS NULL
					OR (
						ISNULL(t.[IsFloating], 0) = CASE 
							WHEN @pStatus = 2
								THEN 1
							ELSE 0
							END
						AND --Floating  
						ISNULL(t.[IsReversed], 0) = CASE 
							WHEN @pStatus = 3
								THEN 1
							ELSE 0
							END
						AND --Reversed  
						ISNULL(t.[IsDuplicated], 0) = CASE 
							WHEN @pStatus = 4
								THEN 1
							ELSE 0
							END
						AND --Duplicated  
						ISNULL(t.[IsAdjustment], 0) = CASE 
							WHEN @pStatus = 5
								THEN 1
							ELSE 0
							END
						AND --Adjustment  
						ISNULL(t.[IsVoid], 0) = CASE 
							WHEN @pStatus = 9
								OR @pStatus = 3
								THEN 1
							ELSE 0
							END
						AND ISNULL(t.[TransactionOffline], 0) = CASE 
							WHEN @lTransactionsOffline = 1
								THEN 1
							ELSE 0
							END
						)
					)
				AND (
					@pKey IS NULL
					OR u.[Name] LIKE '%' + @pKey + '%'
					OR v.[PlateId] LIKE '%' + @pKey + '%'
					OR t.[ProcessorId] LIKE '%' + @pKey + '%' 
					OR t.[Invoice] LIKE '%' + @pKey + '%'
					OR t.[MerchantDescription] LIKE '%' + @pKey + '%' 
					OR v.[Name] LIKE '%' + @pKey + '%'
					)
				AND (
					@pDriverId IS NULL
					OR @pDriverId = cd.[UserId]
					)
				AND (
					(
						@pYear IS NOT NULL
						AND @pMonth IS NOT NULL
						AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
						AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear
						)
					OR (
						@pStartDate IS NOT NULL
						AND @pEndDate IS NOT NULL
						AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate
							AND @pEndDate
						)
					)
				AND (@pCostCenterId IS NULL OR t.[CostCenterId] = @pCostCenterId)
			ORDER BY t.[InsertDate] DESC
		END
	END
	ELSE
	BEGIN
		IF (@pStatus = 1 OR @pStatus = 8)
		BEGIN
			SELECT t.[TransactionId] AS [TransactionId]
				,c.[Name] AS [CustomerName]
				,c.[CustomerBranchName]
				,IsCustomerBranch = @lIsCustomerBranch
				,t.[CreditCardId] AS [CreditCardId]
				,cc.CreditCardNumber AS [CreditCardNumber]
				,ISNULL(cc.[CreditCardNumber], pay.[Code]) [PaymentInstrumentCode]
				,(CASE WHEN t.[PaymentInstrumentId] IS NULL 
						THEN 'Tarjeta'
						ELSE pit.[Name] END) AS [PaymentInstrumentType]
				,ISNULL(t.[Invoice], '-') AS [Invoice]
				,CASE WHEN t.[IsInternal] = 1 AND t.[AuthorizationNumber] IS NULL
				 THEN CONVERT(VARCHAR(500), t.[TransactionId])
				 ELSE t.[AuthorizationNumber]
				 END [AuthorizationNumber]
				,CASE 
					WHEN [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0) IS NULL THEN @TERMINAL_NO_FLOTA
					ELSE [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0)
					END AS [MerchantDescription]
				,t.[TransactionPOS] AS [SystemTraceCode]
				,v.[PlateId] [HolderName]
				,
				--t.[Date] [Date],   
				DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date]
				,f.[Name] [FuelName]
				,t.[FuelAmount] [FuelAmount]
				,t.[Odometer] [Odometer]
				,CASE WHEN @lPartnerUnit = 1 THEN t.[Liters] * @lConvertionValue
				 ELSE t.[Liters] 
				 END [Liters]
				,v.[PlateId] [PlateId]
				,g.[Symbol] [CurrencySymbol]
				,(
					CASE 
						WHEN t.[IsReversed] = 1
							AND t.[IsVoid] = 1
							THEN 'Reversada'
						WHEN t.[IsDuplicated] = 1
							THEN 'Duplicada'
						WHEN t.[IsFloating] = 1
							THEN 'Flotante'
						WHEN t.[IsReversed] = 0
							AND t.[IsVoid] = 1
							THEN 'Anulada'
						ELSE 'Procesada'
						END
					) AS [State]
				,@pCostCenterId [CostCenterId]
				,@pVehicleGroupId [VehicleGroupId]
				,e.[Name] AS [CostCenterName]
				,CASE WHEN @pVehicleGroupId IS NULL 
				 THEN (SELECT TOP 1 vg.[Name] 
					   FROM [General].[VehicleGroups] vg
					   INNER JOIN [General].[VehiclesByGroup] vbg
							ON vbg.[VehicleGroupId] = vg.[VehicleGroupId] 
					   INNER JOIN [General].[Vehicles] v2
							ON v.[VehicleId] = vbg.[VehicleId]
					   WHERE v.[VehicleId] = v2.VehicleId)
				 ELSE (SELECT [Name] 
					   FROM [General].[VehicleGroups]					  
					   WHERE [VehicleGroupId] = @pVehicleGroupId)
				 END [VehicleGroupName]
				,t.[ProcessorId] AS [TerminalId]
				,v.[Name] AS [VehicleName]
				,ISNULL(co.[Name], (
						SELECT TOP 1 coun.[Name]
						FROM [General].[Countries] coun
						WHERE coun.[CountryId] = c.[CountryId]
						)) AS [CountryName]
				,(
					CASE 
						WHEN t.[ExchangeValue] IS NULL
							THEN 'N/A'
						WHEN t.[ExchangeValue] IS NOT NULL
							THEN CONCAT (
									t.[CountryCode]
									,':'
									,t.[ExchangeValue]
									,'; '
									,(
										SELECT CONCAT (
												coun.[Code]
												,':'
												,er.[Value]
												)
										FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cue ON er.[CurrencyCode] = cue.[CurrencyCode]
										INNER JOIN [General].[Countries] coun ON coun.[CountryId] = c.[CountryId]
										WHERE cue.[Code] = coun.[Code]
											AND (
												(
													er.[Until] IS NOT NULL
													AND t.[InsertDate] BETWEEN er.[Since]
														AND er.[Until]
													)
												OR (
													er.[Until] IS NULL
													AND (
														t.[InsertDate] BETWEEN er.[Since]
															AND GETDATE()
														)
													)
												)
										)
									)
						END
					) AS [ExchangeValue]
				,(
					CASE 
						WHEN t.[IsInternational] = 1
							AND (
								t.[CountryCode] IS NOT NULL
								AND t.[CountryCode] <> ''
								)
							AND t.[ExchangeValue] IS NOT NULL
							THEN [Control].[GetTransactionRealAmount](t.CreditCardId, ISNULL(t.[CountryCode], ''), ISNULL(t.[FuelAmount], 0), ISNULL(t.[ExchangeValue], 0), t.[Date])
						WHEN t.[IsInternational] = 0
							THEN t.FuelAmount
						ELSE t.FuelAmount
						END
					) AS [RealAmount]
			 
				,ISNULL((SELECT TOP 1 u.[Name]
						FROM [General].[Users] u
						INNER JOIN [General].[DriversUsers] du ON u.[UserId] = du.[UserId]
						WHERE du.[CustomerId] = c.[CustomerId]
							  AND du.[Code] = t.[DriverCode]
							  AND (u.[IsDeleted] IS NULL 
								   OR u.[IsDeleted] = 0)
		                      AND (u.[IsActive] IS NULL
		                           OR u.[IsActive] = 1)), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 0)	
					   )[DriverName]
				,ISNULL((SELECT [InternationalCard] 
						FROM [General].[CustomerCreditCards]
						WHERE [CustomerId] = c.[CustomerId]
							  AND [StatusId] = 100
							  AND [InternationalCard] = 1), 0) [IsInternational]	
				,@pStatus [FilterType]
				,c.[UnitOfCapacityId]
			FROM [Control].[Transactions] t
			INNER JOIN [General].[Vehicles] v 
				ON t.[VehicleId] = v.[VehicleId]
			INNER JOIN #Customers c 
				ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN [Control].[Fuels] f 
				ON t.[FuelId] = f.[FuelId]			
			INNER JOIN [Control].[Currencies] g 
				ON c.[CurrencyId] = g.[CurrencyId]
			LEFT JOIN [Control].[CreditCard] cc 
				ON cc.CreditCardId = t.CreditCardId
			LEFT JOIN [Control].[PaymentInstruments] pay 
				ON t.[PaymentInstrumentId] = pay.[Id]
			LEFT JOIN [Control].[PaymentInstrumentsTypes] pit 
				ON pay.[typeId] = pit.[Id]
			LEFT JOIN [General].[VehicleCostCenters] e 
				ON t.CostCenterId = e.CostCenterId
			LEFT OUTER JOIN [General].[Countries] co 
				ON t.[CountryCode] = co.[Code]
			WHERE (
					@count = 0
					OR v.[VehicleId] IN (
						SELECT items
						FROM @Results
						)
					) -- DYNAMIC FILTER  
				AND (
					t.[IsFloating] IS NULL
					OR t.[IsFloating] = 0
					)
				--AND c.[CustomerId] = @pCustomerId
				AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
				AND 1 > (
					SELECT ISNULL(COUNT(1), 0)
					FROM CONTROL.Transactions t2
					WHERE (t2.[CreditCardId] = t.[CreditCardId]
						   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
						AND t2.[TransactionPOS] = t.[TransactionPOS]
						AND t2.[ProcessorId] = t.[ProcessorId]
						AND (
							t2.IsReversed = 1
							OR t2.IsVoid = 1
							)
					)
				AND (
					t.[TransactionOffline] IS NULL
					OR t.[TransactionOffline] = 0
					)
				AND (
					@pKey IS NULL
					OR v.[Name] LIKE '%' + @pKey + '%'
					OR v.[PlateId] LIKE '%' + @pKey + '%'
					OR t.[Invoice] LIKE '%' + @pKey + '%' 
					OR t.[ProcessorId] LIKE '%' + @pKey + '%' 
					OR t.[MerchantDescription] LIKE '%' + @pKey + '%' 
					)
				AND (
					(
						@pYear IS NOT NULL
						AND @pMonth IS NOT NULL
						AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
						AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear
						)
					OR (
						@pStartDate IS NOT NULL
						AND @pEndDate IS NOT NULL
						AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate
							AND @pEndDate
						)
					)
				AND (@pCostCenterId IS NULL OR t.[CostCenterId] = @pCostCenterId)
			ORDER BY t.[InsertDate] DESC
		END
		ELSE
		BEGIN
			--SELECT 'ENTRE 3'  
			SELECT t.[TransactionId] AS [TransactionId]
				,c.[Name] AS [CustomerName]
				,c.[CustomerBranchName]
				,IsCustomerBranch = @lIsCustomerBranch
				,t.[CreditCardId] AS [CreditCardId]
				,cc.CreditCardNumber AS [CreditCardNumber]
				,ISNULL(cc.[CreditCardNumber], pay.[Code]) [PaymentInstrumentCode]
				,(CASE WHEN t.[PaymentInstrumentId] IS NULL 
						THEN 'Tarjeta'
						ELSE pit.[Name] END) AS [PaymentInstrumentType]
				,ISNULL(t.[Invoice], '-') AS [Invoice]
				,CASE 
					WHEN [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0) IS NULL THEN @TERMINAL_NO_FLOTA
					ELSE [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0)
					END AS [MerchantDescription]
				,t.[TransactionPOS] AS [SystemTraceCode]
				,v.[PlateId] [HolderName]
				,CASE WHEN t.[IsInternal] = 1 AND t.[AuthorizationNumber] IS NULL
				 THEN CONVERT(VARCHAR(500), t.[TransactionId])
				 ELSE t.[AuthorizationNumber]
				 END [AuthorizationNumber]
				--t.[Date] [Date],  
				,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date]
				,f.[Name] [FuelName]
				,t.[FuelAmount] [FuelAmount]
				,t.[Odometer] [Odometer]
				,CASE WHEN @lPartnerUnit = 1 THEN t.[Liters] * @lConvertionValue
				 ELSE t.[Liters] 
				 END [Liters]
				,v.[PlateId] [PlateId]
				,g.[Symbol] [CurrencySymbol]
				,(
					CASE 
						WHEN t.[IsReversed] = 1
							AND t.[IsVoid] = 1
							THEN 'Reversada'
						WHEN t.[IsDuplicated] = 1
							THEN 'Duplicada'
						WHEN t.[IsFloating] = 1
							THEN 'Flotante'
						WHEN t.[IsReversed] = 0
							AND t.[IsVoid] = 1
							THEN 'Anulada'
						ELSE 'Procesada'
						END
					) AS [State]
				,@pCostCenterId [CostCenterId]
				,@pVehicleGroupId [VehicleGroupId]
				,e.[Name] AS [CostCenterName]
				,CASE WHEN @pVehicleGroupId IS NULL 
				 THEN (SELECT TOP 1 vg.[Name] 
					   FROM [General].[VehicleGroups] vg
					   INNER JOIN [General].[VehiclesByGroup] vbg
							ON vbg.[VehicleGroupId] = vg.[VehicleGroupId] 
					   INNER JOIN [General].[Vehicles] v2
							ON v.[VehicleId] = vbg.[VehicleId]
					   WHERE v.[VehicleId] = v2.VehicleId)
				 ELSE (SELECT [Name] 
					   FROM [General].[VehicleGroups]					  
					   WHERE [VehicleGroupId] = @pVehicleGroupId)
				 END [VehicleGroupName]				
				,t.[ProcessorId] AS [TerminalId]
				,v.[Name] AS [VehicleName]
				,ISNULL(co.[Name], (
						SELECT TOP 1 coun.[Name]
						FROM [General].[Countries] coun
						WHERE coun.[CountryId] = c.[CountryId]
						)) AS [CountryName]
				,(
					CASE 
						WHEN t.[ExchangeValue] IS NULL
							THEN 'N/A'
						WHEN t.[ExchangeValue] IS NOT NULL
							THEN CONCAT (
									t.[CountryCode]
									,':'
									,t.[ExchangeValue]
									,'; '
									,(
										SELECT CONCAT (
												coun.[Code]
												,':'
												,er.[Value]
												)
										FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cue ON er.[CurrencyCode] = cue.[CurrencyCode]
										INNER JOIN [General].[Countries] coun ON coun.[CountryId] = c.[CountryId]
										WHERE cue.[Code] = coun.[Code]
											AND (
												(
													er.[Until] IS NOT NULL
													AND t.[InsertDate] BETWEEN er.[Since]
														AND er.[Until]
													)
												OR (
													er.[Until] IS NULL
													AND (
														t.[InsertDate] BETWEEN er.[Since]
															AND GETDATE()
														)
													)
												)
										)
									)
						END
					) AS [ExchangeValue]
				,(
					CASE 
						WHEN t.[IsInternational] = 1
							AND (
								t.[CountryCode] IS NOT NULL
								AND t.[CountryCode] <> ''
								)
							AND t.[ExchangeValue] IS NOT NULL
							THEN [Control].[GetTransactionRealAmount](t.CreditCardId, ISNULL(t.[CountryCode], ''), ISNULL(t.[FuelAmount], 0), ISNULL(t.[ExchangeValue], 0), t.[Date])
						WHEN t.[IsInternational] = 0
							THEN t.FuelAmount
						ELSE t.FuelAmount
						END
					) AS [RealAmount]
			 
				,ISNULL((SELECT TOP 1 u.[Name]
						FROM [General].[Users] u
						INNER JOIN [General].[DriversUsers] du ON u.[UserId] = du.[UserId]
						WHERE du.[CustomerId] = c.[CustomerId]
							  AND du.[Code] = t.[DriverCode]
							  AND (u.[IsDeleted] IS NULL 
								   OR u.[IsDeleted] = 0)
		                      AND (u.[IsActive] IS NULL
		                           OR u.[IsActive] = 1)), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 0)	
					   )[DriverName]
			    ,ISNULL((SELECT [InternationalCard] 
						FROM [General].[CustomerCreditCards]
						WHERE [CustomerId] = c.[CustomerId]
							  AND [StatusId] = 100
							  AND [InternationalCard] = 1), 0) [IsInternational]	
				,(SELECT [General].[Fn_TransportData_Reverse_Retrieve] (t.[TransactionId])) [Response]
				,@pStatus [FilterType]
				,c.[UnitOfCapacityId]
			FROM [Control].[Transactions] t
			INNER JOIN [General].[Vehicles] v 
				ON t.[VehicleId] = v.[VehicleId]
			--INNER JOIN [General].[Customers] c ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN #Customers c 
				ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN [Control].[Fuels] f 
				ON t.[FuelId] = f.[FuelId]
			INNER JOIN [Control].[Currencies] g 
				ON c.[CurrencyId] = g.[CurrencyId]
			LEFT JOIN [Control].[CreditCard] cc 
				ON cc.CreditCardId = t.CreditCardId
			LEFT JOIN [Control].[PaymentInstruments] pay 
				ON t.[PaymentInstrumentId] = pay.[Id]
			LEFT JOIN [Control].[PaymentInstrumentsTypes] pit 
				ON pay.[typeId] = pit.[Id]
			LEFT JOIN [General].[VehicleCostCenters] e 
				ON t.CostCenterId = e.CostCenterId
			LEFT OUTER JOIN [General].[Countries] co 
				ON t.[CountryCode] = co.[Code]
			WHERE (
					@count = 0
					OR v.[VehicleId] IN (
						SELECT items
						FROM @Results
						)
					) -- DYNAMIC FILTER  
				--AND c.[CustomerId] = @pCustomerId
				AND (
					@pStatus IS NULL
					OR @pStatus = 0
					OR (
						ISNULL(t.[IsFloating], 0) = CASE 
							WHEN @pStatus = 2
								THEN 1
							ELSE 0
							END
						AND --Floating  
						ISNULL(t.[IsReversed], 0) = CASE 
							WHEN @pStatus = 3
								THEN 1
							ELSE 0
							END
						AND --Reversed  
						ISNULL(t.[IsDuplicated], 0) = CASE 
							WHEN @pStatus = 4
								THEN 1
							ELSE 0
							END
						AND --Duplicated  
						ISNULL(t.[IsAdjustment], 0) = CASE 
							WHEN @pStatus = 5
								THEN 1
							ELSE 0
							END
						AND --Adjustment  
						ISNULL(t.[IsVoid], 0) = CASE 
							WHEN @pStatus = 9
								OR @pStatus = 3
								THEN 1
							ELSE 0
							END
						AND ISNULL(t.[TransactionOffline], 0) = CASE 
							WHEN @pStatus = 7
								THEN 1
							ELSE 0
							END
						) --Offline  
					)
				AND (
					@pKey IS NULL
					OR v.[PlateId] LIKE '%' + @pKey + '%' 
					OR v.[Name] LIKE '%' + @pKey + '%'
					OR v.[PlateId] LIKE '%' + @pKey + '%'
					OR t.[ProcessorId] LIKE '%' + @pKey + '%' 
					OR t.[Invoice] LIKE '%' + @pKey + '%'
					OR t.[MerchantDescription] LIKE '%' + @pKey + '%' 
					)
				AND (
					(
						@pYear IS NOT NULL
						AND @pMonth IS NOT NULL
						AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
						AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear
						)
					OR (
						@pStartDate IS NOT NULL
						AND @pEndDate IS NOT NULL
						AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate
							AND @pEndDate
						)
					)
				AND (@pCostCenterId IS NULL OR t.[CostCenterId] = @pCostCenterId)
			ORDER BY t.[InsertDate] DESC
		END
	END

	SET NOCOUNT OFF
END

