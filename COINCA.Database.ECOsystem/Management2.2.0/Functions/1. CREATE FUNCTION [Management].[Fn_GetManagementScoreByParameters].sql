USE [ECOsystem]
GO

IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Fn_GetManagementScoreByParameters]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [Management].[Fn_GetManagementScoreByParameters]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Henry Retana
-- Create date: 23/10/2018
-- Description:	Get the score by parameters
-- =============================================

CREATE FUNCTION [Management].[Fn_GetManagementScoreByParameters]
(
	@pLogInMinutes INT = NULL,
	@pLogOutMinutes INT = NULL,
	@pClientMinutes INT = NULL,
	@pMovingMinutes INT = NULL,
	@pLostMinutes INT = NULL,
	@pTopMargin INT, 
	@pLowMargin INT, 
	@pTopPercent INT, 
	@pMiddlePercent INT, 
	@pLowPercent INT
)
RETURNS INT
AS
BEGIN
	DECLARE @ResultOutput INT = 0
	 
	--VALIDATES LOG IN TIME
	IF @pLogInMinutes IS NOT NULL 
	BEGIN 
		IF @pLogInMinutes > 0 OR (@pLogInMinutes * -1) < @pLowMargin
			SET @ResultOutput = @pTopPercent
		ELSE IF (@pLogInMinutes * -1)  BETWEEN @pLowMargin AND @pTopMargin
			SET @ResultOutput = @pMiddlePercent 
		ELSE IF (@pLogInMinutes * -1) > @pTopMargin
			SET @ResultOutput = @pLowPercent 
	END 
	--VALIDATES LOG OUT TIME
	ELSE IF @pLogOutMinutes IS NOT NULL
	BEGIN 
		IF @pLogOutMinutes > 0 OR (@pLogOutMinutes * -1) < @pLowMargin
			SET @ResultOutput = @pTopPercent
		ELSE IF (@pLogOutMinutes * -1)  BETWEEN @pLowMargin AND @pTopMargin
			SET @ResultOutput = @pMiddlePercent 
		ELSE IF (@pLogOutMinutes * -1) > @pTopMargin
			SET @ResultOutput = @pLowPercent 		
	END 
	--VALIDATES CUSTOMER TIME
	ELSE IF @pClientMinutes IS NOT NULL 
	BEGIN 
		IF @pClientMinutes > 0 OR (@pClientMinutes * -1) < @pLowMargin
			SET @ResultOutput = @pTopPercent
		ELSE IF (@pClientMinutes * -1)  BETWEEN @pLowMargin AND @pTopMargin
			SET @ResultOutput = @pMiddlePercent 
		ELSE IF (@pClientMinutes * -1) > @pTopMargin
			SET @ResultOutput = @pLowPercent 
	END
	--VALIDATES MOVING TIME    
	ELSE IF @pMovingMinutes IS NOT NULL
	BEGIN 
		IF @pMovingMinutes < 0 OR @pMovingMinutes > @pLowMargin
			SET @ResultOutput = @pTopPercent
		ELSE IF @pMovingMinutes  BETWEEN @pLowMargin AND @pTopMargin
			SET @ResultOutput = @pMiddlePercent 
		ELSE IF @pMovingMinutes > @pTopMargin
			SET @ResultOutput = @pLowPercent 
	END 
	--VALIDATES LOST MINUTES
	ELSE IF @pLostMinutes IS NOT NULL
	BEGIN 
		IF @pLostMinutes < 0 OR @pLostMinutes < @pLowMargin
			SET @ResultOutput = @pTopPercent
		ELSE IF @pLostMinutes  BETWEEN @pLowMargin AND @pTopMargin
			SET @ResultOutput = @pMiddlePercent 
		ELSE IF @pLostMinutes > @pTopMargin
			SET @ResultOutput = @pLowPercent 
	END 

	RETURN @ResultOutput
END