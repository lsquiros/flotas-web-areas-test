/****** Object:  Index [IX_Transactions_122]    Script Date: 5/25/2020 2:40:32 AM ******/
CREATE NONCLUSTERED INDEX [IX_Transactions_PaymentInstrument_IsDenied] ON [Control].[Transactions]
(
	[PaymentInstrumentId] ASC,
	[IsDenied] ASC
)
INCLUDE ( 	[FuelAmount],
	[InsertDate],
	[IsFloating],
	[ProcessorId],
	[TransactionPOS]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


