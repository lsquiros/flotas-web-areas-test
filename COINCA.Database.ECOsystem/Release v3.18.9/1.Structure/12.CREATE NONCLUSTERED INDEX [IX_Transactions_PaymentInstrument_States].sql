/****** Object:  Index [IX_Transactions_F3FF]    Script Date: 5/25/2020 2:46:59 AM ******/
CREATE NONCLUSTERED INDEX [IX_Transactions_PaymentInstrument_States] ON [Control].[Transactions]
(
	[PaymentInstrumentId] ASC,
	[IsFloating] ASC,
	[IsDuplicated] ASC,
	[IsReversed] ASC,
	[IsDenied] ASC
)
INCLUDE ( 	[FuelAmount],
	[InsertDate],
	[Liters],
	[ProcessorId],
	[RealLiters],
	[TransactionOffline],
	[TransactionPOS]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


