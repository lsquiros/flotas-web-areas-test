/****** Object:  Index [IDX_Transactions_E380B9F102EE239F0DDD9688D86193CA]    Script Date: 5/25/2020 2:34:17 AM ******/
DROP INDEX [IDX_Transactions_E380B9F102EE239F0DDD9688D86193CA] ON [Control].[Transactions]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IDX_Transactions_E380B9F102EE239F0DDD9688D86193CA]    Script Date: 5/25/2020 2:34:17 AM ******/
CREATE NONCLUSTERED INDEX [IDX_Transactions_E380B9F102EE239F0DDD9688D86193CA] ON [Control].[Transactions]
(
	[ShowAlert] ASC
)
INCLUDE ( 	
    [CreditCardId],
	[PaymentInstrumentId],
	[Date],
	[Liters],
	[Odometer],
	[ProcessorId],
	[TransactionPOS],
	[VehicleId]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


