/****** Object:  Table [Control].[PaymentInstrumentsByType]    Script Date: 27/05/2020 10:09:33 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Control].[PaymentInstrumentsByType](
	[PaymentByTypeId] [int] IDENTITY(1,1) NOT NULL,
	[PaymentInstrumentId] [int] NULL,
	[CardRequestId] [int] NULL,
	[VehicleId] [int] NULL,
	[UserId] [int] NULL,
	[InsertDate] [datetime] NULL,
 CONSTRAINT [PK_PaymentInstrumentsByType] PRIMARY KEY CLUSTERED 
(
	[PaymentByTypeId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


