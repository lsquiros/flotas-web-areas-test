USE [ECOSystem]
GO

-- ================================================
-- Author:		Stefano Quir�s
-- Create date: 08/06/2020
-- Description:	Add Columns
-- ================================================

ALTER TABLE [Control].[CardRequest]
ADD PaymentMethod INT NULL