USE [ECOSystem]
GO

-- ================================================
-- Author:		Stefano Quir�s
-- Create date: 19/05/2020
-- Description:	Add Columns
-- ================================================

ALTER TABLE [Control].[PaymentInstruments] 
ADD [IsUsed] INT

ALTER TABLE [Control].[PaymentInstruments] 
ADD [CreditAvailable] DECIMAL(16,2)

ALTER TABLE [Control].[PaymentInstruments] 
ADD [AvailableLiters] DECIMAL(16,3)

ALTER TABLE [Control].[PaymentInstruments] 
DROP COLUMN [Name]

EXEC sp_RENAME 'Control.PaymentInstruments.Active', 'StatusId', 'COLUMN'

