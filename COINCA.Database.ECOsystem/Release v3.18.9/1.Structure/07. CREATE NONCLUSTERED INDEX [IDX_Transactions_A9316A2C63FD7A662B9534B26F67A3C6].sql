/****** Object:  Index [IDX_Transactions_A9316A2C63FD7A662B9534B26F67A3C6]    Script Date: 5/25/2020 2:30:16 AM ******/
DROP INDEX [IDX_Transactions_A9316A2C63FD7A662B9534B26F67A3C6] ON [Control].[Transactions]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IDX_Transactions_A9316A2C63FD7A662B9534B26F67A3C6]    Script Date: 5/25/2020 2:30:16 AM ******/
CREATE NONCLUSTERED INDEX [IDX_Transactions_A9316A2C63FD7A662B9534B26F67A3C6] ON [Control].[Transactions]
(
	[IsFloating] ASC,
	[IsDuplicated] ASC,
	[IsInternal] ASC,
	[IsReversed] ASC,
	[VehicleId] ASC
)
INCLUDE ( 	[CountryCode],
	[CreditCardId],
	[PaymentInstrumentId],
	[Date],
	[DriverCode],
	[FuelAmount],
	[FuelId],
	[InsertDate],
	[Invoice],
	[Liters],
	[ProcessorId],
	[TransactionPOS]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


