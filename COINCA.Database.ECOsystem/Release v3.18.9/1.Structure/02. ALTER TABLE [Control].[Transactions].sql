USE [ECOSystem]
GO

-- =======================================================================================
-- Author:		Stefano Quir�s
-- Create date: 22/05/2020
-- Description:	Add InstrumentTypeId to table Transactions and drop Foreign Key Constraint
-- =======================================================================================

ALTER TABLE [Control].[Transactions]
ADD [PaymentInstrumentId] INT NULL

ALTER TABLE [Control].[Transactions] 
ALTER COLUMN [CreditCardId] INT NULL

ALTER TABLE [Control].[Transactions]
DROP CONSTRAINT [FK_Transactions_CreditCard]