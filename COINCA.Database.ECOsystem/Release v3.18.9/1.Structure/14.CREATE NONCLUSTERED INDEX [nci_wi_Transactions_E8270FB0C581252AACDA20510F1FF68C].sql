/****** Object:  Index [nci_wi_Transactions_E8270FB0C581252AACDA20510F1FF68C]    Script Date: 5/25/2020 2:57:29 AM ******/
DROP INDEX [nci_wi_Transactions_E8270FB0C581252AACDA20510F1FF68C] ON [Control].[Transactions]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [nci_wi_Transactions_E8270FB0C581252AACDA20510F1FF68C]    Script Date: 5/25/2020 2:57:29 AM ******/
CREATE NONCLUSTERED INDEX [nci_wi_Transactions_E8270FB0C581252AACDA20510F1FF68C] ON [Control].[Transactions]
(
	[IsDuplicated] ASC,
	[IsFloating] ASC,
	[IsReversed] ASC,
	[Date] ASC
)
INCLUDE ( 	
    [CreditCardId],
	[PaymentInstrumentId],
	[FuelAmount],
	[IsDenied],
	[ProcessorId],
	[TransactionPOS],
	[VehicleId]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


