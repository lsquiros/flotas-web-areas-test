USE [ECOSystem]
GO

-- =======================================================================================
-- Author:		Stefano Quir�s
-- Create date: 22/05/2020
-- Description:	Add InstrumentTypeId to table LoGtransactionPOS
-- =======================================================================================

ALTER TABLE [Control].[LogTransactionsPOS]
ADD [PaymentInstrumentId] INT NULL