/****** Object:  Index [IX_Transactions_FC33]    Script Date: 5/25/2020 2:50:10 AM ******/
DROP INDEX [IX_Transactions_FC33] ON [Control].[Transactions]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_Transactions_FC33]    Script Date: 5/25/2020 2:50:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_Transactions_FC33] ON [Control].[Transactions]
(
	[IsDuplicated] ASC,
	[IsFloating] ASC,
	[IsReversed] ASC,
	[VehicleId] ASC,
	[IsDenied] ASC
)
INCLUDE ( 	
    [CreditCardId],
	[PaymentInstrumentId],
	[FuelAmount],
	[InsertDate],
	[Liters],
	[ProcessorId],
	[TransactionPOS]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


