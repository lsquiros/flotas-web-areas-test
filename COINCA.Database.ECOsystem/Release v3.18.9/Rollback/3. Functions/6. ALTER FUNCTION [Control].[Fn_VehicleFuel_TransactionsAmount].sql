USE ECOsystem
/****** Object:  UserDefinedFunction [Control].[Fn_VehicleFuel_TransactionsAmount]    Script Date: 05/06/2020 4:04:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Esteban Sol�s
-- Create date: 07-02-2018
-- Description:	Get fuel amount of th transactions by criteria
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- =============================================

ALTER FUNCTION [Control].[Fn_VehicleFuel_TransactionsAmount]
(
	 @pCustomerId INT = NULL
	,@pVehicleId INT = NULL
	,@pTimeZoneParameter INT = NULL
	,@pInsertDate DATE = NULL
)
RETURNS INT
AS
BEGIN
	DECLARE @RealAmount DECIMAL(16, 6);

	SET @RealAmount = ISNULL((
								SELECT SUM(x.[FuelAmount])
								FROM [Control].[Transactions] x
								INNER JOIN [General].[Vehicles] y 
									ON x.[VehicleId] = y.[VehicleId]
								WHERE y.[CustomerId] = @pCustomerId
								AND x.[IsFloating] = 0
								AND x.[IsReversed] = 0
								AND x.[IsDuplicated] = 0
								AND (x.[IsDenied] IS NULL OR x.[IsDenied] = 0)
								AND y.[Active] = 1
								AND 1 > (
											SELECT COUNT(1)
											FROM CONTROL.Transactions t2
											WHERE t2.[CreditCardId] = x.[CreditCardId]
											AND t2.[TransactionPOS] = x.[TransactionPOS]
											AND t2.[ProcessorId] = x.[ProcessorId]
											AND (
												t2.IsReversed = 1
												OR t2.IsVoid = 1
												)
										)
								AND y.VehicleId = @pVehicleId
								AND CONVERT(DATE, DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate])) >= @pInsertDate
								GROUP BY y.[VehicleId]
							 ), 0.0) 
	RETURN @RealAmount
END

GO


