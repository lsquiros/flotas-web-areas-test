USE ECOsystem
/****** Object:  UserDefinedFunction [General].[GetOdometer]    Script Date: 05/06/2020 4:05:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Albert Estrada
-- Create date: Sep/22/2017
-- Description:	Retorna el odometro
-- Modify by Marjorie Garbanzo - 21/05/2019 - Add filter IsDenied to the retrieve
-- =============================================
ALTER FUNCTION [General].[GetOdometer] 
(
	@VehicleID INT,
	@Device INT,
	@OdometerVehicle FLOAT 
)
RETURNS FLOAT
AS
BEGIN

	
	
	DECLARE @Odometer FLOAT 
	 SELECT @Odometer = max (odom) FROM 
			(
				SELECT    ISNULL(CAST([Odometer] AS INT), 0) AS odom  
						FROM  [dbo].[Report_Last]  WHERE [Device] = @Device
				UNION
				SELECT TOP 1  ISNULL([Odometer], 0) AS odom 
						FROM  [Control].[Transactions] t 
						WHERE [VehicleId] = @VehicleID 
							  AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)		
							  AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
							  AND 1 > (
										SELECT ISNULL(COUNT(1), 0)
										FROM CONTROL.Transactions t2
										WHERE t2.[CreditCardId] = t.[CreditCardId]
											AND t2.[TransactionPOS] = t.[TransactionPOS]
											AND t2.[ProcessorId] = t.[ProcessorId]
											AND (
												t2.IsReversed = 1
												OR t2.IsVoid = 1
												)
									)
						ORDER BY [InsertDate] DESC
				UNION 
				SELECT @OdometerVehicle AS odom
			)  as TableOdometer 
	RETURN @Odometer

END
GO


