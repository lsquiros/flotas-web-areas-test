USE ECOsystem
/****** Object:  UserDefinedFunction [Control].[Fn_TransactionsAmountByCustomer]    Script Date: 05/06/2020 4:03:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:		Stefano Quir�s
-- Create date: 20-05-2019
-- Description:	Get fuel amount of the transactions By Customer
-- ============================================================

ALTER FUNCTION [Control].[Fn_TransactionsAmountByCustomer] 
(
	 @pCustomerId INT = NULL
	,@pTimeZoneParameter INT = NULL
	,@pInsertDate DATE = NULL
	,@pFinalDate DATE = NULL
)
RETURNS DECIMAL(16, 6)
AS
BEGIN

	DECLARE @RealAmount DECIMAL(16, 6) = 0	
	
	SET @RealAmount = ISNULL((
				SELECT SUM(x.[FuelAmount])
				FROM [Control].[Transactions] x
				INNER JOIN [Control].[CreditCard] c
					ON c.[CreditCardId] = x.[CreditCardId]
				WHERE c.[CustomerId] = @pCustomerId
					AND x.[IsFloating] = 0
					AND x.[IsReversed] = 0
					AND x.[IsDuplicated] = 0
					AND 1 > (
						SELECT COUNT(1)
						FROM CONTROL.Transactions t2
						WHERE t2.[CreditCardId] = x.[CreditCardId]
							AND t2.[TransactionPOS] = x.[TransactionPOS]
							AND t2.[ProcessorId] = x.[ProcessorId]
							AND (
								t2.IsReversed = 1
								OR t2.IsVoid = 1
								)
						)
					AND (DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) >= @pInsertDate
							AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) <= @pFinalDate)
				), 0.0)

	RETURN @RealAmount;

END

GO


