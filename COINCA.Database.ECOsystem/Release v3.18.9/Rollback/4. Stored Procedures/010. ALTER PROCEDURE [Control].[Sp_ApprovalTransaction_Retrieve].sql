USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_ApprovalTransaction_Retrieve]    Script Date: 05/06/2020 2:44:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===========================================================================================================
-- Author:		Melvin Salas
-- Create date: March 17, 2016
-- Description:	Get TOP 100 transactions between period
-- Modified by Kevin Pe�a 
-- Added ServiceStation Name   
-- Modified by Melvin Salas 
-- Fixed DriversUsers
-- Update 19/08/2016 - Cindy Vargas - Add the transaction country name to the retrieve information
-- Update 29/08/2016 - Cindy Vargas - Add the transaction ExchangeValue and Amount to the retrieve information
-- Modify 28/9/16 - Henry Retana - Retrieve, country, exchange value and real amount
-- Modify 20/10/16 - Henry Retana - Modify the servicestation name
-- Modify 30/11/2016 - Stefano Quir�s - Add new Filter by CostCenter
-- Modify 12/01/16 - Henry Retana - Modify Drivers Retrieve
-- Modify 03/22/2017 - Stefano Quir�s - Rebuild the SP 
-- Modify 27/04/2017 - Henry Retana - Validates CostCenter Parameter 
-- Modify by Henry Retana - 19/09/2017
-- Use function to get the driver id and driver name
-- Modify By: Stefano Quir�s - 22/12/2017 - Add if the MasterCard is International to the retrieve
-- Modify By: Henry Retana - 11/01/2018 - Add xml Dynamic columns and avoid duplicates
-- Modify by: Esteban Solis 16/05/2018 Add CustomerId to dbo.Sp_UserDynamicFilter_Retrive call
-- Modify by Marjorie Garbanzo - Add filter IsDenied to the retrieve - 21/05/2019
-- Juan Carlos Santamaria V. - 12/27/2019 - Add Column 'NOMBRE DE LA EMPRESA' Customer Branch - JSA-001
-- Jason Bonilla Soto 23/01/2020 Add Column ElectronicInvoice
-- ============================================================================================================
ALTER PROCEDURE [Control].[Sp_ApprovalTransaction_Retrieve]
(
	 @pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
	,@pMonth INT = NULL
	,@pYear INT = NULL
	,@pSearch VARCHAR(50) = NULL
	,@pStatus UNIQUEIDENTIFIER = NULL
	,@pUserId INT = NULL
	,@pCustomerId INT = NULL
	,@pRetrieveType INT = NULL
	,@pCostCenterId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;

	-- GET DEFAULT TYPE
	DECLARE @Default UNIQUEIDENTIFIER,
			@lTimeZoneParameter INT,
			@lDynamicColumns VARCHAR(MAX),
			@lInternalValidate INT,
			@lConvertionValue DECIMAL(16, 5) = 3.78,
	        @lPartnerUnit INT

	IF @pCostCenterId = 0 SET @pCostCenterId = NULL
	
	SET @Default = (SELECT TOP 1 [Id]
					FROM [Control].[ApprovalTransactionTypes]
					WHERE [Default] = 1)

		-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	DECLARE @lIsCustomerBranch INT

	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	SELECT @lTimeZoneParameter = [TimeZone]	
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	SELECT @lDynamicColumns = [XMLData]
	FROM [General].[ApprovalTransactionsParameters]
	WHERE [CustomerId] = @pCustomerId

	SELECT @lInternalValidate = [Value]
	FROM [General].[ParametersByCustomer]
	WHERE [ResuourceKey] = 'APPROVALTRANSACTIONS'
	AND [CustomerId] = @pCustomerId

	SELECT @lPartnerUnit = [CapacityUnitId] 
	FROM [General].[Partners] p
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[PartnerId] = p.[PartnerId]
	WHERE [CustomerId] = @pCustomerId

	SELECT *  /*JSA-001*/
	INTO #Customers
	FROM (
		SELECT   a.[CustomerId]
				,a.[Name]
				,a.[Name] AS CustomerBranchName
				,a.[CustomerId] AS CustomerBranch
				,a.[UnitOfCapacityId]
				,a.[CurrencyId]
				,a.[CountryId]
		FROM [General].[Customers] a
		WHERE a.[CustomerId] = @pCustomerId	
		------------------------------------
		UNION /*add CustomerBracnh */
		------------------------------------
		SELECT   cb.[CustomerId]
				,cb.[Name]
				,a.[Name] AS CustomerBranchName
				,cb.CustomerBranch
				,cb.[UnitOfCapacityId]
				,cb.[CurrencyId]
				,a.[CountryId]
		FROM [General].[Customers] a
		INNER JOIN [General].[Customers] cb on a.CustomerId = cb.CustomerBranch
		WHERE a.[CustomerId] = @pCustomerId
		AND cb.CustomerBranch IS NOT NULL
	) AS Customers

	SELECT @lIsCustomerBranch = COUNT(1) 
	FROM #Customers
	
	IF (@pStatus = @Default)
	BEGIN
		-- GET TRANSACTIONS
		SELECT DISTINCT t.[TransactionId]
						,t.[Date]
						,v.[Name] AS [VehicleName]
						,v.VehicleId
						,v.[PlateId]
						,cc.[CreditCardNumber] AS [CreditCard]
						,t.[CostCenterId]
						,c.[Name] AS [CustomerName]
						,c.[CustomerBranchName]
						,IsCustomerBranch = @lIsCustomerBranch
						,vcc.[Name] AS [CostCenterName]
						,ISNULL((SELECT TOP 1 u.[Name]
								FROM [General].[Users] u
								INNER JOIN [General].[DriversUsers] du
								ON u.[UserId] = du.[UserId]
								WHERE du.[CustomerId] = c.[CustomerId] 
								AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 0)) [DriverName]
						,ISNULL((SELECT TOP 1 du.[Identification]
								FROM [General].[Users] u
								INNER JOIN [General].[DriversUsers] du
								ON u.[UserId] = du.[UserId]
								WHERE du.[CustomerId] = c.[CustomerId] 
								AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 1)) [DriverId]
						,vc.[Manufacturer]
						,CASE WHEN @lPartnerUnit = 1 THEN t.[Liters] * @lConvertionValue
						 ELSE t.[Liters] 
				         END [Liters]
						,t.[FuelAmount] AS [Amount]
						,f.[Name] AS [FuelName]
						,COALESCE(ssc.[Name], ss.[Name]) AS [ServiceStationName]
						,tt.[TerminalId] [TerminalId]
						,t.[ProcessorId] AS [AffiliateNumber]
						,ISNULL(case t.[Invoice]
								WHEN '' THEN NULL
								ELSE t.[Invoice]
								END, 'N/A') AS [Invoice]
						,(Select Count(1) from [Control].[Transactions] t2
							LEFT OUTER JOIN [Control].[ApprovalTransaction] at2
							ON at2.[TransactionId] = t2.[TransactionId] 
							WHERE t.[Invoice] = t2.[Invoice] AND t.[ProcessorId] = t2.[ProcessorId] AND (at2.[TypeId] is null or at2.[TypeId] = at.[TypeId])) [InvoiceCount]
						,ss.[Legal_Id] AS [LegalId]
						,att.[Name] [StatusName]
						,ISNULL(att.[Id], @Default) AS [Status]
						,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
											FROM [General].[Countries] coun
											WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName]
						,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
							WHEN t.[ExchangeValue] IS NOT NULL 
							THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
										(SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
										FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cue 
											ON er.[CurrencyCode] = cue.[CurrencyCode]
										INNER JOIN [General].[Countries] coun 
											ON coun.[CountryId] = c.[CountryId]
										WHERE cue.[Code] = coun.[Code]
											AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
												OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
						END) AS [ExchangeValue]
						,(CASE 
								WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL THEN 
								[Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
								WHEN t.[IsInternational] = 0 THEN
								t.FuelAmount
								ELSE t.FuelAmount END) AS [RealAmount]
						,ccc.[InternationalCard] [IsInternational]
						,@lDynamicColumns [DynamicColumnListXML]
						,at.[DynamicColumns] [DynamicColumnValuesXML] 
						,ISNULL(t.ElectronicInvoice,'') ElectronicInvoice
		FROM [Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v 
			ON t.[VehicleId] = v.[VehicleId]		
		INNER JOIN #Customers c
			ON v.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[CustomerCreditCards] ccc 
			ON ccc.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[VehicleCategories] vc 
			ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
		INNER JOIN [Control].[CreditCard] cc 
			ON cc.CreditCardId = t.CreditCardId
		INNER JOIN [General].[VehicleCostCenters] vcc 
			ON t.[CostCenterId] = vcc.[CostCenterId]
		INNER JOIN [Control].[Fuels] f 
			ON f.[FuelId] = t.[FuelId]
		LEFT JOIN [Control].[ApprovalTransaction] at 
			ON at.[TransactionId] = t.[TransactionId]
		LEFT JOIN [Control].[ApprovalTransactionTypes] att 
			ON att.[Id] = at.[TypeId]		
		LEFT OUTER JOIN [General].[TerminalsByServiceStations] tt 
			ON tt.[TerminalId] = t.[ProcessorId]
		LEFT OUTER JOIN [General].[ServiceStations] ss 
			--ON ss.[BacId] = tt.[BacId]
			ON ss.[ServiceStationId] = tt.[ServiceStationId]
		LEFT JOIN [General].[ServiceStationsCustomName] ssc 
			--ON ssc.[ServiceStationRef] = ss.[ServiceStationId]
			ON ssc.[ServiceStationId] = ss.[ServiceStationId]
				AND ssc.[InsertUserId] = c.[CustomerId]--@pCustomerId
		LEFT OUTER JOIN [General].[Countries] co 
			ON t.[CountryCode] = co.[Code]
		WHERE  ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear) 
			OR (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
			AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
		AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)		
	    AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		AND 1 > (
					SELECT ISNULL(COUNT(1), 0)
					FROM CONTROL.Transactions t2
					WHERE t2.[CreditCardId] = t.[CreditCardId]
						AND t2.[TransactionPOS] = t.[TransactionPOS]
						AND t2.[ProcessorId] = t.[ProcessorId]
						AND (
							t2.IsReversed = 1
							OR t2.IsVoid = 1
							)
					)
		--If parameter is 1 don�t show internal transactions
		AND ((@lInternalValidate = 0 OR @lInternalValidate IS NULL) OR t.[IsInternal] = 0)
		-- TEXT FILTER		
		AND (@pSearch IS NULL OR v.[Name] LIKE '%'+@pSearch+'%'	OR v.[PlateId] LIKE '%'+@pSearch+'%' OR vcc.[CostCenterId] LIKE '%'+@pSearch+'%' OR vcc.[Name] LIKE '%'+@pSearch+'%' OR t.[Invoice] LIKE '%'+@pSearch+'%')
		-- STATUS FILTER
		AND (@pStatus IS NULL OR @pStatus = ISNULL(att.[Id], @Default))
		AND (@pCostCenterId IS NULL OR t.[CostCenterId] = @pCostCenterId)
        AND (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER	
		ORDER BY t.[Date] ASC
	END
	ELSE
	BEGIN
				-- GET TRANSACTIONS
		SELECT DISTINCT t.[TransactionId]
						,t.[Date]
						,v.[Name] AS [VehicleName]
						,v.VehicleId
						,v.[PlateId]
						,c.[Name] AS [CustomerName]
						,c.[CustomerBranchName]
						,IsCustomerBranch = @lIsCustomerBranch
						,cc.[CreditCardNumber] AS [CreditCard]
						,t.[CostCenterId]
						,vcc.[Name] AS [CostCenterName]			
						,ISNULL((SELECT TOP 1 u.[Name]
								FROM [General].[Users] u
								INNER JOIN [General].[DriversUsers] du
								ON u.[UserId] = du.[UserId]
								WHERE du.[CustomerId] = c.[CustomerId] 
								AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 0)) [DriverName]
						,ISNULL((SELECT TOP 1 du.[Identification]
								FROM [General].[Users] u
								INNER JOIN [General].[DriversUsers] du
								ON u.[UserId] = du.[UserId]
								WHERE du.[CustomerId] = c.[CustomerId] 
								AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 1)) [DriverId]
						,vc.[Manufacturer]
						,CASE WHEN @lPartnerUnit = 1 THEN t.[Liters] * @lConvertionValue
						 ELSE t.[Liters] 
				         END [Liters]
						,t.[FuelAmount] AS [Amount]
						,f.[Name] AS [FuelName]
						,COALESCE(ssc.[Name], ss.[Name]) AS [ServiceStationName]
						,tt.[TerminalId] [TerminalId]
						,t.[ProcessorId] AS [AffiliateNumber]
						,ISNULL(case t.[Invoice]
							   WHEN '' THEN NULL
							   ELSE t.[Invoice]
							   END, 'N/A') AS [Invoice]
						,(Select Count(1) from [Control].[Transactions] t2
						 LEFT OUTER JOIN [Control].[ApprovalTransaction] at2
							ON at2.[TransactionId] = t2.[TransactionId] 
						 WHERE t.[Invoice] = t2.[Invoice] AND t.[ProcessorId] = t2.[ProcessorId] AND (at2.[TypeId] is null or at2.[TypeId] = at.[TypeId])) [InvoiceCount]
						,ss.[Legal_Id] AS [LegalId]
						,att.[Name] [StatusName]
						,ISNULL(att.[Id], @Default) AS [Status]
						,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
											FROM [General].[Countries] coun
											WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName]
						,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
						   WHEN t.[ExchangeValue] IS NOT NULL 
						   THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
									  (SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
										FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cue 
											ON er.[CurrencyCode] = cue.[CurrencyCode]
										INNER JOIN [General].[Countries] coun 
											ON coun.[CountryId] = c.[CountryId]
										WHERE cue.[Code] = coun.[Code]
											AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
												OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
					   END) AS [ExchangeValue]
					   ,(CASE 
								WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL THEN 
								[Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
								WHEN t.[IsInternational] = 0 THEN
								t.FuelAmount
								ELSE t.FuelAmount END) AS [RealAmount]
					  ,ccc.[InternationalCard] [IsInternational]
					  ,@lDynamicColumns [DynamicColumnListXML]
					  ,at.[DynamicColumns] [DynamicColumnValuesXML] 
					  ,ISNULL(t.ElectronicInvoice,'') ElectronicInvoice
		FROM [Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v 
			ON t.[VehicleId] = v.[VehicleId]
		INNER JOIN #Customers c
			ON v.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[CustomerCreditCards] ccc 
			ON ccc.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[VehicleCategories] vc 
			ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
		INNER JOIN [Control].[CreditCard] cc 
			ON cc.CreditCardId = t.CreditCardId
		INNER JOIN [General].[VehicleCostCenters] vcc 
			ON t.CostCenterId = vcc.CostCenterId
		INNER JOIN [Control].[Fuels] f 
			ON f.[FuelId] = t.[FuelId]
		INNER JOIN [Control].[ApprovalTransaction] at 
			ON at.[TransactionId] = t.[TransactionId]
		INNER JOIN [Control].[ApprovalTransactionTypes] att 
			ON att.[Id] = at.[TypeId]		
		LEFT OUTER JOIN [General].[TerminalsByServiceStations] tt 
			ON tt.[TerminalId] = t.[ProcessorId]
		LEFT OUTER JOIN [General].[ServiceStations] ss 
			--ON ss.[BacId] = tt.[BacId]
			ON ss.[ServiceStationId] = tt.[ServiceStationId]
		LEFT JOIN [General].[ServiceStationsCustomName] ssc 
			--ON ssc.[ServiceStationRef] = ss.[ServiceStationId]
			ON ssc.[ServiceStationId] = ss.[ServiceStationId]
				AND ssc.[InsertUserId] = c.[CustomerId]--@pCustomerId
		LEFT OUTER JOIN [General].[Countries] co 
			ON t.[CountryCode] = co.[Code]
		WHERE ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear) 
			OR (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
			AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
		AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)		
	    AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		AND 1 > (
				SELECT ISNULL(COUNT(1), 0)
				FROM CONTROL.Transactions t2
				WHERE t2.[CreditCardId] = t.[CreditCardId]
					AND t2.[TransactionPOS] = t.[TransactionPOS]
					AND t2.[ProcessorId] = t.[ProcessorId]
					AND (
						t2.IsReversed = 1
						OR t2.IsVoid = 1
						)
				)
		--If parameter is 1 don�t show internal transactions
		AND ((@lInternalValidate = 0 OR @lInternalValidate IS NULL) OR t.[IsInternal] = 0)
		-- TEXT FILTER		
		AND (@pSearch IS NULL OR v.[Name] LIKE '%'+@pSearch+'%'	OR v.[PlateId] LIKE '%'+@pSearch+'%' OR vcc.[CostCenterId] LIKE '%'+@pSearch+'%' OR vcc.[Name] LIKE '%'+@pSearch+'%' OR t.[Invoice] LIKE '%'+@pSearch+'%')
		-- STATUS FILTER
		AND (@pStatus IS NULL OR @pStatus = ISNULL(att.[Id], @Default))
		AND (@pCostCenterId IS NULL OR vcc.[CostCenterId] = @pCostCenterId)
        AND (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER			
		ORDER BY t.[Date] ASC
	END
END

GO


