USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_CustomerByCreditCardNumber_Retrieve]    Script Date: 05/06/2020 3:57:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Kevin Pe�a
-- Create date: 07/23/2015
-- Description:	Retrieve CustomerID from CreditCardNumber_Transactions
-- Modify By: Stefano Quir�s
-- Description: Retrieve the CreditCardId to insert in LogTransactionPOS
-- Modify By: Henry Retana 05/05/2017
-- Description: Retrieve parameters for transactions offline and send transactions
-- Modify By: Stefano Quir�s - Update script and add the join when the customer has
-- the creditcard by Drivers
-- Modify By: Henry Retana 26/03/2018
-- Description: Retrieve plate and driver code
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_CustomerByCreditCardNumber_Retrieve]
(
	 @pCreditCardNumber NVARCHAR(520)
)
AS
BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON	
	
	DECLARE @lCustomerId INT
	       ,@lPartnerId INT
		   ,@lCostCenterByDriver BIT = 0

	--GET CUSTOMER ID 
	SELECT 	@lCustomerId = cc.[CustomerId]
	       ,@lPartnerId = cp.[PartnerId]
	FROM  	[Control].[CreditCard] cc
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[CustomerId] = cc.[CustomerId]
	WHERE   cc.[CreditCardNumber] = @pCreditCardNumber
    
	IF EXISTS(SELECT * 
	          FROM [Partners].[PartnerTransactionsOffline]
			  WHERE [PartnerId] = @lPartnerId
			  AND [EndDate] < GETDATE()
			  AND [Active] = 1
			  AND ([History] IS NULL
			       OR [History] = 0))
	BEGIN
		UPDATE [Partners].[PartnerTransactionsOffline]
		SET [Active] = 0
		WHERE [PartnerId] = @lPartnerId
		AND [Active] = 1
		AND ([History] IS NULL
			OR [History] = 0)
		
		--NOTIFIES PROCESS END
		EXEC [General].[Sp_SendAlarmsFromProcedures] @lPartnerId, 0 		
		
		--SEND REPORT
		EXEC [Control].[Sp_SendProgramReportForTransactionsOffline_AddOrEdit] @lPartnerId
	END
		
	--VALIDATES IF THE CUSTOMER IS SET TO HAVE COSTCENTER BY DRIVERS
	IF ISNULL((SELECT TOP 1 [Value] 
			   FROM [General].[ParametersByCustomer]
			   WHERE [CustomerId] = @lCustomerId
			   AND [ResuourceKey] = 'COSTCENTERBYDRIVER'), 0) = 1
	BEGIN
		SET @lCostCenterByDriver = 1
	END
	 
	--RETRIEVE INFORMATION
	SELECT cc.[CustomerId]
		  ,cc.[CreditCardId]
		  ,ccbv.[VehicleId]
		  ,CASE WHEN @lCostCenterByDriver = 1 
				THEN ISNULL((SELECT TOP 1 du.[CostCenterId]
   				   		     FROM [General].[DriversUsers] du
							 INNER JOIN [General].[VehiclesByUser] vbu
								ON du.[UserId] = vbu.[UserId] 
								AND vbu.[VehicleId] = v.[VehicleId]
							 WHERE vbu.[LastDateDriving] IS NULL), v.[CostCenterId])
				ELSE v.[CostCenterId] END [CostCenterId]
		  ,CAST(CASE WHEN (pto.[EndDate] IS NOT NULL 
					  AND pto.[EndDate] > GETDATE()
					  AND pto.[Active] = 1)
					THEN ISNULL(c.[TransactionsOffline], 0)
					ELSE 0  
				END AS BIT) [TransactionOffline]
		  ,CAST(CASE WHEN (pto.[EndDate] IS NOT NULL 
					  AND (pto.[EndDate] < GETDATE()
						   OR pto.[Active] = 0))
					THEN ISNULL(pto.[SendTransactions], 0)
					ELSE 0  
				END AS BIT) [SendTransactions]
		  ,v.[PlateId] [VehiclePlate]
		  ,du.[Code] [DriverCode]
		  ,u.[Name] [EncryptedDriverName]
	FROM [Control].[CreditCard] cc
	LEFT OUTER JOIN [Control].[CreditCardByVehicle] ccbv
		ON cc.CreditCardId = ccbv.CreditCardId
	LEFT OUTER JOIN [Control].[CreditCardByDriver] ccbbd
		ON cc.CreditCardId = ccbbd.CreditCardId
	LEFT OUTER JOIN [General].[DriversUsers] du
		ON ccbbd.[UserId] = du.[UserId] 
	LEFT OUTER JOIN [General].[Users] u
		ON u.[UserId] = du.[UserId]		
	LEFT OUTER JOIN [General].[Vehicles] v
		ON ccbv.[VehicleId] = v.[VehicleId]
	INNER JOIN [General].[Customers] c
		ON v.[CustomerId] = c.[CustomerId] 
			OR du.[CustomerId] = c.[CustomerId]
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[CustomerId] = c.[CustomerId]
	LEFT JOIN [Partners].[PartnerTransactionsOffline] pto
		ON cp.[PartnerId] = pto.[PartnerId]
    WHERE [CreditCardNumber] = @pCreditCardNumber
	 AND ([History] IS NULL OR [History] = 0)

	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


