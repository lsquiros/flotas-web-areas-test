/****** Object:  StoredProcedure [Control].[Sp_CreditCardByNumber_Retrieve]    Script Date: 23/07/2020 5:23:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/10/2014
-- Description:	Retrieve CreditCard information by Number
-- Updated date: 08/12/2016 - Sinndy Vargas
-- Description: Obtain Vehicle Name
-- Updated date: 09/21/2016 - Gerald Solano
-- Description: Se quita la validacion del estado de la tarjeta, ahora se valida solo en las reglas de negocio
-- Stefano Quirós - Add the AvailableLiters Validation to the retrive - 14/01/2019
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_CreditCardByNumber_Retrieve] --'A/K2ZethPU6jkhe6US7ydwslQnbH+7vp'
(
	 @pCreditCardNumber nvarchar(520)
)
AS
BEGIN	
	SET NOCOUNT ON
	
	DECLARE @TipoEmision INT = 101 -- 101:VEHICULOS | 100:Conductores
	DECLARE @lConvertionValue DECIMAL(16, 8) = 3.78541178

	SELECT @TipoEmision = cust.[IssueForId]
	FROM [Control].[CreditCard] cc
	INNER JOIN [General].[Customers] cust
		ON cust.CustomerId = cc.CustomerId
	WHERE cc.[CreditCardNumber] = @pCreditCardNumber

	IF (@TipoEmision = 101) --- CONSULTA POR LAS EMISIONES POR VEHICULO
	BEGIN
		SELECT   a.[CreditCardId]
				,a.[CustomerId]
				,c.[Name] AS [EncryptedCustomerName]
				,a.[CreditCardNumber]
				,a.[ExpirationYear]
				,a.[ExpirationMonth]
				,a.[CreditAvailable]
				,CONVERT(DECIMAL(16,2), CASE WHEN p.[CapacityUnitId] = 1 
										THEN (a.[AvailableLiters] / @lConvertionValue)
										ELSE a.[AvailableLiters]
										END) [AvailableLiters]
				,p.[CapacityUnitId]
				,a.[StatusId]
				,b.[RowOrder] AS [Step]
				,b.[Name] AS [StatusName]
				,d.[Symbol] AS [CurrencySymbol]
				,c.[IssueForId]		
				,e.[UserId]
				,g.[VehicleId]
				,c.[CreditCardType]
				,f.Name AS [EncryptedDriverName]
				,'Vehículo: ' + h.[PlateId] AS [VehiclePlate]
				,h.[PlateId] AS [TransactionPlate]
				,h.[Name] AS [VehicleName]
				,a.CardRequestId
				,i.[EstimatedDelivery]
				,a.[RowVersion]
				,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = c.[CustomerId]
						AND [Name] = 'TypeOfDistribution'
						AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
					), 0) AS INT) [TypeBudgetDistribution]
		FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = cp.[PartnerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [Control].[CreditCardByVehicle] g
			ON g.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] = g.[VehicleId]
		INNER JOIN [Control].[CardRequest] i
			ON a.[CardRequestId] = i.[CardRequestId]
		WHERE a.[CreditCardNumber] = @pCreditCardNumber	
	END
	ELSE
	BEGIN  
		SELECT   a.[CreditCardId]
				,a.[CustomerId]
				,c.[Name] AS [EncryptedCustomerName]
				,a.[CreditCardNumber]
				,a.[ExpirationYear]
				,a.[ExpirationMonth]				
				,a.[CreditAvailable]
				,CONVERT(DECIMAL(16,2), CASE WHEN p.[CapacityUnitId] = 1 
										THEN (a.[AvailableLiters] / @lConvertionValue)
										ELSE a.[AvailableLiters]
										END) [AvailableLiters]
				,a.[StatusId]
				,b.[RowOrder] AS [Step]
				,b.[Name] AS [StatusName]
				,d.[Symbol] AS [CurrencySymbol]
				,c.[IssueForId]		
				,e.[UserId]
				,h.[VehicleId]
				,c.[CreditCardType]
				,f.Name AS [EncryptedDriverName]
				,'Vehículo: ' + h.[PlateId] AS [VehiclePlate]
				,h.[PlateId] AS [TransactionPlate]
				,h.[Name] AS [VehicleName]
				,a.CardRequestId
				,i.[EstimatedDelivery]
				,a.[RowVersion]
				,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = c.[CustomerId]
						AND [Name] = 'TypeOfDistribution'
						AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
					), 0) AS INT) [TypeBudgetDistribution]
		FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = cp.[PartnerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [General].[VehiclesDrivers] vd
			ON vd.[UserId] = f.[UserId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] =vd.[VehicleId]
		INNER JOIN [Control].[CardRequest] i
			ON a.[CardRequestId] = i.[CardRequestId]
		WHERE a.[CreditCardNumber] = @pCreditCardNumber
	END

    SET NOCOUNT OFF
END


