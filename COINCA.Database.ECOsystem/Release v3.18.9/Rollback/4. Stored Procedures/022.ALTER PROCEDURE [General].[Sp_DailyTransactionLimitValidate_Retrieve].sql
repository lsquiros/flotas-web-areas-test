USE ECOsystem
/****** Object:  StoredProcedure [General].[Sp_DailyTransactionLimitValidate_Retrieve]    Script Date: 05/06/2020 2:53:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author     :	Henry Retana
-- Create date: 10/07/2018
-- Description:	Retrieve Daily Transaction Limit Validate
-- Stefano Quir�s - Add filter IsDenied to the retrieve - 21/05/2019 
-- Stefano Quir�s - Add UID - 31/7/2019 
-- Juan Carlos Santamaria - Logic correction for rule 13 - 2020-04-23
-- ================================================================================================
ALTER PROCEDURE [General].[Sp_DailyTransactionLimitValidate_Retrieve] --@pUID = null,@pCreditCardNumber = 'VZXPYiiQb70DIsS29Fm8gXfcPFyshX6c'
(
    @pUID VARCHAR(200) = NULL
   ,@pCreditCardNumber VARCHAR(200) = NULL
)   
AS	
BEGIN	
	SET NOCOUNT ON
	
	DECLARE @lCustomerId INT,
			@lCreditCardId INT,
			@lAmount DECIMAL(16, 2),
			@lNumber INT = NULL,
			@lTxAmount DECIMAL(16, 2),
			@lTxNumber INT = NULL,
			@lTRName VARCHAR(100) = 'DailyTransactionLimit',
			@lxmlData XML,
			@lSymbol VARCHAR(10),
			@lStarDate DATETIME,
			@lEndDate DATETIME
	
	IF @pUID IS NOT NULL
	BEGIN
		--GET THE CARD INFORMATION
		SELECT @lCreditCardId = picc.[CreditCardId],
			   @lCustomerId = c.[CustomerId],
			   @lSymbol = cur.[Symbol],
			   @lStarDate = DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, co.[TimeZone], GETDATE())), 0) ,
			   @lEndDate = DATEADD(HOUR, co.[TimeZone], GETDATE())
		FROM [Control].[PaymentInstruments] [pi] 
		INNER JOIN [Control].[PaymentInstrumentsByCreditCard] picc
			ON picc.[PaymentInstrumentId] = [pi].[Id]	
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = [pi].[CustomerId]
		INNER JOIN [General].[Countries] co
			ON co.[CountryId] = c.[CountryId]
		INNER JOIN [Control].[Currencies] cur
			ON co.[Code] = cur.[Code]
		WHERE [pi].[Code] = @pUID
	END
	ELSE
	BEGIN
		--GET THE CARD INFORMATION
		SELECT @lCreditCardId = c.[CreditCardId],
			   @lCustomerId = c.[CustomerId],
			   @lSymbol = cur.[Symbol],
			   @lStarDate = DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, co.[TimeZone], GETDATE())), 0) ,
			   @lEndDate = DATEADD(HOUR, co.[TimeZone], GETDATE())
		FROM [Control].[CreditCard] c 	
		INNER JOIN [General].[Customers] cu 
			ON c.[CustomerId] = cu.[CustomerId]
		INNER JOIN [General].[Countries] co
			ON co.[CountryId] = cu.[CountryId]
		INNER JOIN [Control].[Currencies] cur
			ON co.[Code] = cur.[Code]
		WHERE c.[CreditCardNumber] = @pCreditCardNumber	
	END
	
   
	--RETRIEVE THE PARAMETERS FOR THE TR
	SELECT TOP 1 @lxmlData = CONVERT(XML, [Value])
	FROM [General].[ParametersByCustomer] 
	WHERE [Name] = @lTRName 
	AND [CustomerId] = @lCustomerId
	AND [ResuourceKey] = 'TR_PARAMETERS'

	--VALIDATES IF THE CREDITCARD IS BY DRIVER
	SELECT @lNumber = [DailyTransactionLimit]
	FROM [General].[DriversUsers] du 
	INNER JOIN [Control].[CreditCardByDriver] c
		ON du.[UserId] = c.[UserId]
	WHERE c.[CreditCardId] = @lCreditCardId	

	SELECT @lNumber = CASE WHEN @lNumber IS NULL 
						   THEN ISNULL(m.c.value('Number[1]', 'INT'), 0)
						   ELSE ISNULL(@lNumber, 0)
					  END,
		   @lAmount = ISNULL(m.c.value('Amount[1]', 'INT'), 0)
	FROM @lxmlData.nodes('//DailyTransactionLimitAddOrEdit/DailyTransactionLimitAddOrEdit') AS m(c)

	--GET THE DATA FROM THE TRANSACTIONS
	SELECT @lTxNumber = COUNT(1),
		   @lTxAmount = SUM([FuelAmount])
	FROM [Control].[Transactions] t	
	INNER JOIN [Control].[CreditCard] cc 
		ON cc.[CreditCardId] = t.[CreditCardId]
	WHERE cc.[CustomerId] = @lCustomerId
	AND t.[CreditCardId] = @lCreditCardId
	AND 1 > (
				SELECT ISNULL(COUNT(1), 0)
				FROM [Control].[Transactions] t2
				WHERE t2.[CreditCardId] = t.[CreditCardId]
				AND t2.[TransactionPOS] = t.[TransactionPOS]
				AND t2.[ProcessorId] = t.[ProcessorId]
				AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)
			)
	AND (t.[TransactionOffline] IS NULL OR t.[TransactionOffline] = 0)
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)	
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)	
	AND (t.[Date] BETWEEN @lStarDate AND @lEndDate)

	--RETURN THE VALIDATION 
	IF @lTxNumber >= @lNumber
	BEGIN 
		SELECT CAST(0 AS BIT) [Approved],
			   @lNumber [Number],
			   NULL [Amount],
			   @lSymbol [Symbol] 
	END 
	ELSE IF @lTxAmount >= @lAmount
	BEGIN
		SELECT CAST(0 AS BIT) [Approved],
			   NULL [Number],
			   @lAmount [Amount],
			   @lSymbol [Symbol]
	END 
	ELSE 
	BEGIN 
		SELECT CAST(1 AS BIT) [Approved],
			   NULL [Number],
			   NULL [Amount],
			   @lSymbol [Symbol]
	END 

    SET NOCOUNT OFF
END
GO


