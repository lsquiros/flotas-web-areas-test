USE ECOsystem
/****** Object:  StoredProcedure [General].[Sp_DashboardCoreIndicators_Retrieve]    Script Date: 05/06/2020 3:46:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 07/06/2016
-- Description:	Retrieve Dashboard Core Indicators
-- ================================================================================================
-- Modify by Gerald Solano - 10/07/2016 - Se cambi� el tipo de dato de la variable @ScorePercentAverage a Numeric(9,2)
-- Modify by Henry Retana - 26/07/2017 - Based on the Intrack Reference to get the vehicle
-- Modify by Stefano Quiros 13/04/2016 - 14/11/2017 - Add Void Transactions Validation - Add Left
-- Modify by Stefano Quiros 19/09/2018 - Join to VehiclesByCustomer table - 
-- Modify by Gerald Solano 04/10/2018 - Update query to get linked customer
-- Modify by Maria de los Angeles Jimenez Chavarria - 18/Dic/2018 - Hide deleted items.
-- Stefano Quir�s - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================  

ALTER PROCEDURE [General].[Sp_DashboardCoreIndicators_Retrieve] --14031
(
	 @pCustomerId INT
)
AS
BEGIN
	SET NOCOUNT ON

	--DECLARE  @pCustomerId INT = 77

	-- VEHICULOS CON AVL - INDICATOR
	DECLARE @RegisterVehicles INT = 0
	DECLARE @VehiclesWithAVL INT = 0
	DECLARE @PercentVehiclesAVL NUMERIC(5,3) = 0	
	DECLARE @lTimeZoneParameter INT 
	DECLARE	@lGetVehiclesByCustomer BIT = 0 

	--Validates if the Customer is linked to LV Vehicles 
	SELECT @lGetVehiclesByCustomer = CAST(COUNT(1) AS BIT)
	FROM [General].[PartnerLinkedToCustomer]
	WHERE CustomerId = @pCustomerId


	SELECT @lTimeZoneParameter = [TimeZone]	FROM [General].[Countries] co
								 INNER JOIN [General].[Customers] cu
								 ON co.[CountryId] = cu.[CountryId]
								 WHERE cu.[CustomerId] = @pCustomerId

	SELECT @VehiclesWithAVL = COUNT(1) 
	FROM [General].[Vehicles] V
	LEFT JOIN [General].[VehiclesByCustomers] vc
			ON vc.[VehicleId] = V.[VehicleId]
	WHERE (V.[CustomerId] = @pCustomerId
		   OR (@lGetVehiclesByCustomer = 1 AND vc.[CustomerId] = @pCustomerId))
		 AND (V.[IsDeleted] IS NULL OR V.[IsDeleted] = 0)
		 AND (V.[IntrackReference] IS NOT NULL AND V.[IntrackReference] > 0)
		 --AND (V.[DeviceReference] IS NOT NULL AND V.[DeviceReference] > 0)

	SELECT @RegisterVehicles = COUNT(1) 
	FROM [General].[Vehicles] V
	LEFT JOIN [General].[VehiclesByCustomers] vc
			ON vc.[VehicleId] = V.[VehicleId]
	WHERE (V.[CustomerId] = @pCustomerId
			 OR (@lGetVehiclesByCustomer = 1 AND vc.[CustomerId] = @pCustomerId))
		 AND (V.[IsDeleted] IS NULL OR V.[IsDeleted] = 0)

	-- ////////////////////////////////////////////


	-- PROMEDIO DE CALIFICACIONES - INDICATOR
	DECLARE @ScoreAboveAverage INT = 0
	DECLARE @ScoreBelowAverage INT = 0
	DECLARE @ScorePercentAverage NUMERIC(9,2) = 0
	DECLARE @ScoreAveragePoint INT = 75
	DECLARE @ScoreAverageCount INT = 0

	DECLARE @TempTableScores TABLE(
		[UserId] INT,
		[Average] FLOAT
	)

	INSERT INTO @TempTableScores
	SELECT q.[UserId], (SUM(q.[Average]) / 2) AS [Average] 
	FROM(
		SELECT dsd.[UserId], dsd.[ScoreType], COUNT(1) AS Quantity, (SUM(dsd.[Score]) / COUNT(1))  AS [Average]
		FROM [Operation].[DriversScoresDaily] dsd
		WHERE dsd.[CustomerId] = @pCustomerId
			  AND dsd.[ScoreMonth] = DATEPART(m, GETDATE()) 
			  AND dsd.[ScoreYear] = DATEPART(yyyy, GETDATE())
		GROUP BY dsd.[UserId], dsd.[ScoreType]	
	) q
	GROUP BY q.[UserId]
	ORDER BY q.[UserId] DESC

	SET @ScoreAverageCount = @@ROWCOUNT

	SELECT @ScorePercentAverage = (SUM([Average]) / COUNT(1))
	FROM @TempTableScores 

	SELECT @ScoreAboveAverage = COUNT(1)
	FROM @TempTableScores 
	WHERE [Average] >=  @ScoreAveragePoint

	SELECT @ScoreBelowAverage = COUNT(1)
	FROM @TempTableScores 
	WHERE [Average] <  @ScoreAveragePoint
	-- ////////////////////////////////////////////


	-- TARJETAS ACTIVAS / SOLICITADAS - INDICATOR
	DECLARE @ActiveCards INT = 0
	DECLARE @RequestCards INT = 0

	SELECT @ActiveCards = COUNT(1) FROM [Control].[CreditCard] cc
	WHERE cc.[CustomerId] = @pCustomerId AND cc.[StatusId] = 7

	SELECT @RequestCards = COUNT(1) FROM [Control].[CreditCard] cc
	WHERE cc.[CustomerId] = @pCustomerId AND cc.[StatusId] NOT IN (6, 7, 8, 9)
		
	SELECT @RequestCards += COUNT(1) FROM [Control].[CardRequest] cr
	LEFT JOIN [Control].[CreditCard] cc
				ON cc.[CardRequestId] = cr.[CardRequestId]
			   AND cc.[CustomerId] = cr.[CustomerId]
	WHERE cr.[CustomerId] = @pCustomerId AND cc.[CreditCardId] IS NULL AND (cr.[IsDeleted] IS NULL OR cr.[IsDeleted] = 0)

	-- ////////////////////////////////////////////

	--- TRANSACCIONES - PROCESADAS / DENEGADAS
	DECLARE @CurrentProcessTrans INT = 0
	DECLARE @CurrentDenyTrans INT = 0

	-- / DENEGADAS
	SELECT @CurrentDenyTrans = COUNT(1) 
	FROM [Control].[Transactions] t
	INNER JOIN [Control].[CreditCard] cc 
		ON cc.[CreditCardId] = t.[CreditCardId]
	WHERE cc.[CustomerId] = @pCustomerId
		  AND t.[IsDenied] = 1		  
		  AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, GETDATE()))
		  AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, GETDATE()))
		  AND DATEPART(d, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = DATEPART(d, DATEADD(HOUR, @lTimeZoneParameter, GETDATE())) 	


	-- / PROCESADAS
	SELECT @CurrentProcessTrans = Count(1)
	FROM [Control].[Transactions] t
		INNER JOIN [Control].[CreditCard] cc 
			ON cc.[CreditCardId] = t.[CreditCardId]
	WHERE cc.[CustomerId] = @pCustomerId
		  AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		  AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
		  AND 1 > (
				SELECT ISNULL(COUNT(1), 0) FROM Control.Transactions t2
						WHERE t2.[CreditCardId] = t.[CreditCardId] AND
							t2.[TransactionPOS] = t.[TransactionPOS] AND
							t2.[ProcessorId] = t.[ProcessorId] AND 
						   (t2.IsReversed = 1 OR t2.IsVoid = 1) 
			)
			AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, GETDATE()))
			AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, GETDATE()))
			AND DATEPART(d, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = DATEPART(d, DATEADD(HOUR, @lTimeZoneParameter, GETDATE()))
		

	-- ////////////////////////////////////////////


	-- RESULT CORE DASHBOARD - INDICATORS
	SELECT @RegisterVehicles AS [RegisterVehicles], 
		   @VehiclesWithAVL AS [VehiclesWithAVL],
		   @ScoreAverageCount AS [ScoreAverageCount],
		   ISNULL(@ScorePercentAverage, CONVERT(NUMERIC(4,2), 0)) AS [ScorePercentAverage],
		   @ScoreAboveAverage AS [ScoreAboveAverage],
		   @ScoreBelowAverage AS [ScoreBelowAverage],
		   @ActiveCards AS [ActiveCards],
		   @RequestCards AS [RequestCards],
		   @CurrentProcessTrans AS [CantProcTransaction],
		   @CurrentDenyTrans AS [CantDenyTransaction]
		   
		   
	SET NOCOUNT OFF 
END

GO


