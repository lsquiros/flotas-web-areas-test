﻿USE ECOsystem
/****** Object:  StoredProcedure [General].[Sp_CustomersFleetioTransactions_Retrieve]    Script Date: 05/06/2020 3:42:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 24/03/2020
-- Description:	Retrieve transactions for send to Fleetio
-- ================================================================================================
 ALTER PROCEDURE [General].[Sp_CustomersFleetioTransactions_Retrieve] --11761, 2032877
 (
	@pCustomerId INT
   ,@pTransactionId INT
 )
 AS
 BEGIN	
	SET NOCOUNT ON	

		DECLARE @lUSGallons DECIMAL(16, 5) = 3.78541
               ,@lUKGallons DECIMAL(16, 5) = 4.54609
			   ,@lUSGallonsToUkGallons DECIMAL(16, 6) = 0.832674

		SELECT DISTINCT t.[TransactionId]
					   ,NULLIF(CONVERT(INT, v.[FleetioId]), 0) [Vehicle_id]
					   ,v.[VehicleId] [VehicleId]
					   ,v.[PlateId]
					   ,[Date]
					   ,ffd.[FleetioId] [Fuel_type_id]
					   ,CONVERT(FLOAT, CASE WHEN p.[CapacityUnitId] = 1
					    THEN ([Liters] / @lUSGallons)
					    ELSE [Liters]
					    END) [Liters]
					   ,[BacId] [External_id]
					   ,CONVERT(FLOAT, CASE WHEN [Liters] = 0
					    THEN 0
						ELSE ([FuelAmount] / [Liters])
						END) [Price_per_volume_unit]
					   ,CONVERT(FLOAT, CASE WHEN p.[CapacityUnitId] = 1
					    THEN ([Liters] * @lUSGallonsToUkGallons)
					    ELSE ([Liters] * @lUKGallons)
					    END) [Uk_gallons]
					   ,CONVERT(FLOAT, CASE WHEN p.[CapacityUnitId] = 1
					    THEN [Liters]
					    ELSE ([Liters] * @lUSGallons)
					    END) [Us_gallons]
					   ,ss.[FleetioId] [Vendor_id]
					   ,ss.[ServiceStationId]
					   ,ss.[Name] [ServiceStationName]
					   ,CONVERT(FLOAT, [FuelAmount]) [FuelAmount]
					   ,t.[Odometer]
					   ,s.[Name] [City]
					   ,coun.[Name] [Country]
					   ,ss.[Address]
					   ,CONVERT(BIT, 0) [FromLog]
		FROM [Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v
			ON v.[VehicleId] = t.[VehicleId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = v.[CustomerId]
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = cp.[PartnerId]
		INNER JOIN [Control].[Fuels] f
			ON f.[FuelId] = t.[FuelId]
		LEFT JOIN [Control].[FuelFleetioDetailByCustomer] ffd
			ON ffd.[FuelId] = f.[FuelId]
			   AND c.[CustomerId] = ffd.[CustomerId] 
		LEFT JOIN [General].[TerminalsByServiceStations] ts
			ON ts.[TerminalId] = t.[ProcessorId]
		LEFT JOIN [General].[ServiceStations] ss
			ON ss.[ServiceStationId] = ts.[ServiceStationId]
		LEFT JOIN [General].[States] s
			ON s.[StateId] = ss.[ProvinceId]
		LEFT JOIN [General].[Countries]coun
			ON coun.[CountryId] = ss.[CountryId]
		WHERE t.[TransactionId] > @pTransactionId
		AND c.[CustomerId] = @pCustomerId
		AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
		AND (t.[IsFloating] IS NULL	OR t.[IsFloating] = 0)
		AND 1 > (
			SELECT ISNULL(COUNT(1), 0)
			FROM CONTROL.Transactions t2
			WHERE t2.[CreditCardId] = t.[CreditCardId]
				AND t2.[TransactionPOS] = t.[TransactionPOS]
				AND t2.[ProcessorId] = t.[ProcessorId]
				AND (
					t2.IsReversed = 1
					OR t2.IsVoid = 1
					)
			)
		AND (
			t.[TransactionOffline] IS NULL
			OR t.[TransactionOffline] = 0
			)

	 UNION
		SELECT DISTINCT t.[TransactionId]
					   ,NULLIF(CONVERT(INT, v.[FleetioId]), 0) [Vehicle_id]
					   ,v.[VehicleId] [VehicleId]
					   ,v.[PlateId]
					   ,[Date]
					   ,ffd.[FleetioId] [Fuel_type_id]
					   ,CONVERT(FLOAT, CASE WHEN p.[CapacityUnitId] = 1
					    THEN ([Liters] / @lUSGallons)
					    ELSE [Liters]
					    END) [Liters]
					   ,[BacId] [External_id]
					   ,CONVERT(FLOAT, CASE WHEN [Liters] = 0
					    THEN 0
					    ELSE ([FuelAmount] / [Liters])
					    END) [Price_per_volume_unit]
					   ,CONVERT(FLOAT, CASE WHEN p.[CapacityUnitId] = 1
					    THEN ([Liters] * @lUSGallonsToUkGallons)
					    ELSE ([Liters] * @lUKGallons)
					    END) [Uk_gallons]
					   ,CONVERT(FLOAT, CASE WHEN p.[CapacityUnitId] = 1
					    THEN [Liters]
					    ELSE ([Liters] * @lUSGallons)
					    END) [Us_gallons]
					   ,ss.[FleetioId] [Vendor_id]
					   ,ss.[ServiceStationId]
					   ,ss.[Name] [ServiceStationName]
					   ,CONVERT(FLOAT, [FuelAmount]) [FuelAmount]
					   ,t.[Odometer]
					   ,s.[Name] [City]
					   ,coun.[Name] [Country]
					   ,ss.[Address]
					   ,CONVERT(BIT, 1) [FromLog]
		FROM [General].[FleetioErrorLog] fl
		INNER JOIN [Control].[Transactions] t
			ON t.[TransactionId] = fl.[TransactionId]
		INNER JOIN [General].[Vehicles] v
			ON v.[VehicleId] = t.[VehicleId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = v.[CustomerId]
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = cp.[PartnerId]
		INNER JOIN [Control].[Fuels] f
			ON f.[FuelId] = t.[FuelId]
		LEFT JOIN [Control].[FuelFleetioDetailByCustomer] ffd
			ON ffd.[FuelId] = f.[FuelId]
			   AND c.[CustomerId] = ffd.[CustomerId] 
		LEFT JOIN [General].[TerminalsByServiceStations] ts
			ON ts.[TerminalId] = t.[ProcessorId]
		LEFT JOIN [General].[ServiceStations] ss
			ON ss.[ServiceStationId] = ts.[ServiceStationId]
		LEFT JOIN [General].[States] s
			ON s.[StateId] = ss.[ProvinceId]
		LEFT JOIN [General].[Countries]coun
			ON coun.[CountryId] = ss.[CountryId]
		WHERE [RegisterDate] > DATEADD(DAY, -7, GETDATE())
		      AND fl.[TransactionId] > 0
			  AND ([IsFixed] IS NULL
				   OR [IsFixed] = 0)
    SET NOCOUNT OFF
 END
GO


