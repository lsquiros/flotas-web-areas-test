USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_Transactions_Retrieve]    Script Date: 05/06/2020 3:31:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Cristian Mart�nez Hern�ndez.
-- Create date: 20/Oct/2014
-- Description:	Retrieve Alarm By Odometer
-- Modified by:	Gerald Solano
-- Modify date: 03/MAY/2016
-- Description:	Return the default transaction when this doesn't return data
-- Modify By: Stefano Quir�s - Add the Time Zone Parameter - 12/06/2016
-- Modified by:	Gerald Solano
-- Modify date: 16/ENE/2017
-- Description:	VALIDAMOS QUE @TimeSlot TENGA EL FORMATO ADECUADO 
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Modify by Henry Retana - 19/18/2017
-- Retorna solo transacciones procesadas 
-- Stefano Quir�s - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_Transactions_Retrieve] --18639
(
	@pVehicleId INT	--@pVehicleId
)
AS
BEGIN
	SET NOCOUNT ON
	
	--DECLARE @pVehicleId INT = 2303--2812
	DECLARE @HasRows INT = 0
	DECLARE @lTimeZoneParameter INT 

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	INNER JOIN [General].[Vehicles] v
		ON cu.[CustomerId] = v.[CustomerId]
	WHERE v.[VehicleId] = @pVehicleId

	SELECT @HasRows = COUNT(1)
	FROM  [Control].[Transactions] t WITH(READUNCOMMITTED)
	WHERE t.[VehicleId] = @pVehicleId
	AND t.[Date] > DATEADD(MONTH,-3, GETUTCDATE())
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)	
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)			
	AND 1 > (SELECT ISNULL(COUNT(1), 0)
			 FROM [Control].[Transactions] t2
			 WHERE t2.[CreditCardId] = t.[CreditCardId]
			 AND t2.[TransactionPOS] = t.[TransactionPOS]
			 AND t2.[ProcessorId] = t.[ProcessorId]
			 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
	AND (t.[TransactionOffline] IS NULL OR t.[TransactionOffline] = 0)
	AND t.[FixedOdometer] IS NULL
	
	IF @HasRows > 0 --GSOLANO
	BEGIN
		SELECT *
		FROM  [Control].[Transactions] t WITH(READUNCOMMITTED)
		WHERE t.[VehicleId] = @pVehicleId
		AND t.[Date] > DATEADD(MONTH,-3, GETUTCDATE())
		AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)	
		AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)			
		AND 1 > (SELECT ISNULL(COUNT(1), 0)
				 FROM [Control].[Transactions] t2
				 WHERE t2.[CreditCardId] = t.[CreditCardId]
				 AND t2.[TransactionPOS] = t.[TransactionPOS]
				 AND t2.[ProcessorId] = t.[ProcessorId]
				 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
		AND (t.[TransactionOffline] IS NULL OR t.[TransactionOffline] = 0)
		AND t.[FixedOdometer] IS NULL
		ORDER BY [Date] DESC
	END
	ELSE
	BEGIN --GSOLANO: New changes		
		DECLARE @CustomerId INT = 0
		DECLARE @TimeSlot VARCHAR(8) = ''
		DECLARE @TransDate DATETIME = DATEADD(HOUR, @lTimeZoneParameter, GETDATE())

		--OBTENEMOS EL CLIENTE
		SELECT @CustomerId = [CustomerId]
		FROM [General].[Vehicles]
		WHERE [VehicleId] = @pVehicleId
		
		--OBTENEMOS EL PARAMETRO DEL TIEMPO ENTRE TRANSACCIONES
		SELECT @TimeSlot = [Value] 
		FROM [General].[ParametersByCustomer]
		WHERE [Name] = 'TimeSlotTransactions' 
		AND [CustomerId] = @CustomerId
		
		--VALIDAMOS QUE @TimeSlot TENGA EL FORMATO ADECUADO
		IF(LEN(@TimeSlot) < 8) 
		BEGIN 
			SET @TimeSlot = '00:00:00'
		END
		
		SET @TransDate = @TransDate - @TimeSlot
		SET @TransDate = DATEADD(HOUR, -1, @TransDate)  -- '01:00:00'

		SELECT 0 AS [TransactionId],
			   @TransDate AS [Date],
			   0 AS [Odometer],
			   0.0 AS [Liters]		
	END  
		  	
	SET NOCOUNT OFF
END
GO


