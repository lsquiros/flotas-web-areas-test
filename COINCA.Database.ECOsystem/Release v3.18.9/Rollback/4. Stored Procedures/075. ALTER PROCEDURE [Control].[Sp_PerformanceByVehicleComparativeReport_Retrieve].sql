USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_PerformanceByVehicleComparativeReport_Retrieve]    Script Date: 05/06/2020 3:52:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Berman Rometro
-- Create date: 03/06/2015
-- Description:	Retrieve Performance By Vehicle Comparative Report information
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- Dinamic Filter - Oct/07/2017 - Albert Estrada  add column @pshowZeros
-- Modify: Henry Retana - 25/01/2018
-- Change GPSDateTime to GPSDateTime
-- ================================================================================================

ALTER PROCEDURE [Control].[Sp_PerformanceByVehicleComparativeReport_Retrieve]
(
	 @pCustomerId INT					--@pCustomerId: Customer Id
	,@pVehicleId INT = NULL				--@pVehicleId: Id of the vehicle
	,@pYear INT = NULL					--@pYear: Year
	,@pMonth INT = NULL					--@pMonth: Month
	,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
	,@pEndDate DATETIME = NULL			--@pEndDate: End Date
	,@pManufacturer VARCHAR(250) = NULL	--@pManufacturer: Manufacturer
	,@pVehicleModel VARCHAR(250) = NULL	--@pVehicleModel: Vehicle Model
	,@pVehicleYear INT = NULL			--@pVehicleYear: Vehicle Year
	,@pVehicleType VARCHAR(250) = NULL	--@pVehicleType: Vehicle Type
	,@pGroupBy INT						--@pGroupBy: Group By
	,@pCostCenterId int=null 
	,@pUnitId int=null
	,@pshowZeros bit =null
	,@pVehicleGroupId int = null
	,@pUserId INT						--@pUserId: User Id
)
AS
BEGIN
	SET NOCOUNT ON
		

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	IF(@pYear IS NOT NULL AND @pMonth IS NOT NULL)
	BEGIN
		SELECT   @pStartDate = DATEADD(MONTH,@pMonth-1,DATEADD(YEAR,@pYear-1900,0)) /*First*/
				,@pEndDate = DATEADD(SECOND,-1,DATEADD(MONTH,@pMonth,DATEADD(YEAR,@pYear-1900,0))) /*Last*/
	END

	IF(@pStartDate IS NULL OR @pEndDate IS NULL)
	BEGIN
		print '1'
	END
	
	;WITH TmpValues ( Number) as
	(
		  SELECT 0
		  UNION ALL
		  SELECT Number + 1
		  FROM TmpValues
		  WHERE Number < 5000
	)
	SELECT   a.[VehicleId]
		   	,a.[PlateId]
			,a.[Name] AS [VehicleName]
			,b.[Manufacturer]
			,b.[Year] AS [VehicleYear]
			,b.[VehicleModel]
			,b.[Type] AS [VehicleType]
			,d.[Name] AS [UnitName]
			,c.[Name] AS [CostCenterName]
			,t.[TrxDate] AS [TrxDate]
			,DATEPART(wk, t.[TrxDate]) AS [TrxWeekNumber]
			,t.[SumTrxLiters] AS [TrxReportedLiters]
			,t.[SumTrxOdometer] AS [TrxTraveledOdometer]
			,t.[AvgTrxPerformance] AS [TrxPerformance]
			--,t.[SumGpsOdometer] AS [GpsTraveledOdometer]
			,t.[AvgGpsPerformance] AS [GpsPerformance]
			,t.[SumGpsHoursOn] AS [GpsHoursOn]		
			,@pGroupBy AS [GroupBy]
			,b.[DefaultPerformance]
			,cast(((select  top 1 isnull(odometer,0) from dbo.reports  where device =a.devicereference and RepDateTime BETWEEN @pStartDate AND @pEndDate order by RepDateTime desc) 
			 -
			 (select  top 1 isnull(odometer,0) from dbo.reports  where device =a.devicereference and RepDateTime BETWEEN @pStartDate AND @pEndDate) ) as decimal (15,2))  
			   as GpsTraveledOdometer
	FROM [General].[Vehicles] a
	LEFT JOIN [General].[VehiclesByGroup] vg
		ON vg.vehicleId = a.vehicleId
	INNER JOIN [General].[VehicleCategories] b
		ON a.[VehicleCategoryId] = b.[VehicleCategoryId]
	INNER JOIN [General].[VehicleCostCenters] c
		ON a.[CostCenterId] = c.[CostCenterId]
	INNER JOIN [General].[VehicleUnits] d
		ON c.[UnitId] = d.[UnitId]
	INNER JOIN(
					SELECT u.[VehicleId]
						  ,u.[TrxDate]
						  ,SUM([SumTrxLiters])AS [SumTrxLiters]
						  ,SUM([SumTrxOdometer])AS [SumTrxOdometer]
						  ,SUM([AvgTrxPerformance])AS [AvgTrxPerformance]
						  ,SUM([SumGpsOdometer])AS [SumGpsOdometer]
						  ,SUM([AvgGpsPerformance])AS [AvgGpsPerformance]
						  ,SUM([SumGpsHoursOn])AS [SumGpsHoursOn]
					FROM(
							SELECT	 a2.[VehicleId]
									,CONVERT(DATE,a2.[TrxDate]) AS [TrxDate]
									,SUM(a2.[TrxReportedLiters]) AS [SumTrxLiters]
									,ABS(MAX(a2.[TrxReportedOdometer]) - MAX(a2.[TrxPreviousOdometer])) AS [SumTrxOdometer]
									,AVG(a2.[TrxPerformance]) AS [AvgTrxPerformance]
									,ABS(MAX(a2.[GpsReportedOdometer]) - MAX(a2.[GpsPreviousOdometer])) AS [SumGpsOdometer]
									,AVG(a2.[GpsPerformance]) AS [AvgGpsPerformance]
									,SUM(a2.[GpsHoursOn]) AS [SumGpsHoursOn]
							FROM (
									SELECT	 a.[VehicleId]
											,a.[TrxDate]
											,a.[TrxReportedLiters]
											,a.[TrxReportedOdometer] 
											,a.[TrxPreviousOdometer]
											,a.[TrxPerformance]
											,a.[GpsReportedOdometer]
											,a.[GpsPreviousOdometer]
											,a.[GpsPerformance]
											,a.[GpsHoursOn]
									FROM [General].[PerformanceByVehicle] a
									INNER JOIN [General].[Vehicles] b
										ON a.[VehicleId] = b.[VehicleId]
									WHERE b.[CustomerId] = @pCustomerId
									AND b.[Active] = 1
									AND (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
									AND a.[TrxDate] BETWEEN @pStartDate AND @pEndDate
									AND 1 > (
												SELECT ISNULL(Count(1), 0)
												FROM (
													SELECT TOP 1 [CreditCardId],[TransactionPOS],[ProcessorId] 
													FROM [Control].[Transactions]
													WHERE [TransactionId] = a.TransactionId
												)tx
												INNER JOIN  [Control].[Transactions] tx2
													ON  tx2.[CreditCardId] = tx.[CreditCardId] AND
														tx2.[TransactionPOS] = tx.[TransactionPOS] AND
														tx2.[ProcessorId] = tx.[ProcessorId]
												WHERE tx2.IsReversed = 1
												OR tx2.[IsVoid] = 1
											)
									)a2
							GROUP BY a2.[VehicleId], CONVERT(DATE,a2.[TrxDate])				
							UNION
							SELECT	 a.[VehicleId]
									,CONVERT(DATE,r.[TrxDate]) AS [TrxDate]
									,NULL AS [SumTrxLiters]
									,NULL AS [SumTrxOdometer]
									,NULL AS [AvgTrxPerformance]
									,NULL AS [SumGpsOdometer]
									,NULL AS [AvgGpsPerformance]
									,NULL AS [SumGpsHoursOn]
							 FROM [General].[Vehicles] a
							 CROSS JOIN(
											SELECT
												 DATEADD(DAY, x.[Number], @pStartDate) AS [TrxDate]
											FROM [TmpValues] x
											WHERE x.[Number] <= DATEDIFF(DAY, @pStartDate, @pEndDate)
									)r
								WHERE a.[CustomerId] = @pCustomerId
								AND a.[Active] = 1
								AND (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
							)u
							GROUP BY u.[VehicleId]
									,u.[TrxDate]
				)t
					ON a.[VehicleId] = t.[VehicleId]		
				WHERE a.[CustomerId] = @pCustomerId
				AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
				AND a.[Active] = 1
				AND (@pVehicleId IS NULL OR a.[VehicleId] = @pVehicleId)
				AND (@pManufacturer IS NULL OR b.Manufacturer = @pManufacturer)
				AND (@pVehicleModel IS NULL OR b.VehicleModel = @pVehicleModel)
				AND (@pVehicleYear IS NULL OR b.[Year] = @pVehicleYear)
				AND (@pVehicleType IS NULL OR b.[Type] = @pVehicleType)
				AND (@pCostCenterId IS NULL OR C.CostCenterId = @pCostCenterId)
				AND (@pVehicleGroupId IS NULL OR vg.VehicleGroupId = @pVehicleGroupId)				
				AND isnull(t.[SumTrxLiters],0) > (CASE WHEN @pshowZeros = 0 THEN -1 ELSE 0 END)
				ORDER BY a.VehicleId, t.TrxDate	
	OPTION (MAXRECURSION 5000)
		 
	SET NOCOUNT OFF
END 
 

 
GO


