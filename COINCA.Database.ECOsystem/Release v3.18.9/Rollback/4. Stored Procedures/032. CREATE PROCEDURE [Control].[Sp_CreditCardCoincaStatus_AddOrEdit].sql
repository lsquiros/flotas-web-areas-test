USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_CreditCardCoincaStatus_AddOrEdit]    Script Date: 05/06/2020 3:01:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author: �lvaro Zamora.
-- Create date: 10/07/2018
-- Description:	Change the sp's structure called Sp_CreditCard_AddOrEdit to receive an XML, the status of the credit card and the user modifier.
-- =====================================================================================================
ALTER PROCEDURE [Control].[Sp_CreditCardCoincaStatus_AddOrEdit]
(
	@pXMLDoc VARCHAR(MAX)
	,@pStatusId INT
	,@pUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0                        
			DECLARE @lxmlData XML = CONVERT(XML,@pXMLDoc)

            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END

			UPDATE [Control].[CreditCard]
			SET    [StatusId] = COALESCE(@pStatusId,[StatusId])
				  ,[ModifyDate] = GETUTCDATE()
				  ,[ModifyUserId] = @pUserId			
			WHERE [CreditCardId] IN (SELECT Row.col.value('./@Id', 'INT') FROM @lxmlData.nodes('/xmldata/CreditCardId') Row(col))

            SET @lRowCount = @@ROWCOUNT
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


