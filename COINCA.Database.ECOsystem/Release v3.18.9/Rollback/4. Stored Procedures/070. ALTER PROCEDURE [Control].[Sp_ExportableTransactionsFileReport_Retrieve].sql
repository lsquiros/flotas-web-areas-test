USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_ExportableTransactionsFileReport_Retrieve]    Script Date: 05/06/2020 3:48:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================  
-- Author:  Esteban Sol�s
-- Create date: 8/1/2017  
-- Description: Retrieve TransactionsSAPReport information 
-- Henry Retana - 21/05/2019 - Add Denied Filters 
-- ================================================================================================  

ALTER PROCEDURE [Control].[Sp_ExportableTransactionsFileReport_Retrieve] 
(
	@pCustomerId INT,
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL,
	@pCostCenterId INT = NULL,
	@pUserId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	-- CONSTANTS
	DECLARE @EXPORTABLE_REPORT_DOCTYPE VARCHAR(100),
			@EXPORTABLE_REPORT_CURRENCY VARCHAR(100),
			@EXPORTABLE_REPORT_RECTYPE VARCHAR(100),
			@EXPORTABLE_REPORT_PACKAGE VARCHAR(100)

	SET @EXPORTABLE_REPORT_DOCTYPE = 'EXPORTABLE_REPORT_DOCTYPE'
	SET @EXPORTABLE_REPORT_CURRENCY = 'EXPORTABLE_REPORT_CURRENCY'
	SET @EXPORTABLE_REPORT_RECTYPE = 'EXPORTABLE_REPORT_RECTYPE'
	SET @EXPORTABLE_REPORT_PACKAGE = 'EXPORTABLE_REPORT_PACKAGE'

	-- GET DEFAULT TYPE  
	DECLARE @Authorized UNIQUEIDENTIFIER

	SET @Authorized = (
						SELECT TOP 1 [Id]
						FROM [Control].[ApprovalTransactionTypes]
						WHERE [Editable] = 0
					  )

	DECLARE @lTimeZoneParameter INT

	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu 
		ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId

	-- GET PARAMETER VALUE  
	DECLARE @ONLY_AUTHORIZED_TRANSACTIONS INT

	SELECT @ONLY_AUTHORIZED_TRANSACTIONS = [Value]
	FROM [General].[ParametersByCustomer]
	WHERE [ResuourceKey] = 'ONLY_AUTHORIZED_TRANSACTIONS'
	AND [CustomerId] = @pCustomerId

	SELECT CONVERT(VARCHAR(100), ISNULL((
											SELECT TOP 1 cs.[SAPProvider]
											FROM [General].[ServiceStationsCustomName] cs
											WHERE [ServiceStationId] = ss.[ServiceStationId]
												AND [InsertUserId] = @pCustomerId
											), ss.[SAPProv])) AS [Proveedor],
		t.[Invoice] AS [Numero],
		(
			SELECT [Value]
			FROM [General].[ParametersByCustomer]
			WHERE [ResuourceKey] = @EXPORTABLE_REPORT_DOCTYPE
				AND [CustomerId] = @pCustomerId
			) AS [TipoDocumento],
		(
			SELECT [Value]
			FROM [General].[ParametersByCustomer]
			WHERE [ResuourceKey] = @EXPORTABLE_REPORT_CURRENCY
				AND [CustomerId] = @pCustomerId
			) AS [Moneda],
		(
			SELECT [Value]
			FROM [General].[ParametersByCustomer]
			WHERE [ResuourceKey] = @EXPORTABLE_REPORT_RECTYPE
				AND [CustomerId] = @pCustomerId
			) AS [TipoAsiento],
		(
			SELECT [Value]
			FROM [General].[ParametersByCustomer]
			WHERE [ResuourceKey] = @EXPORTABLE_REPORT_PACKAGE
				AND [CustomerId] = @pCustomerId
			) AS [Paquete],
		DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) AS [FechaDocumento],
		DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) AS [FechaRige],
		'' AS [Aplicacion],
		t.[FuelAmount] AS [Monto],
		t.[FuelAmount] AS [SubTotal],
		DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) AS [FechaVence],
		e.[Code] AS [SubTipo]
	FROM [Control].[Transactions] t
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]
	INNER JOIN [Control].[Fuels] f 
		ON t.[FuelId] = f.[FuelId]
	INNER JOIN [Control].[CreditCardByVehicle] cv 
		ON t.[CreditCardId] = cv.[CreditCardId]
	INNER JOIN [Control].[Currencies] g 
		ON c.[CurrencyId] = g.[CurrencyId]
	INNER JOIN [Control].[CreditCard] cc
		ON cc.CreditCardId = t.CreditCardId
	INNER JOIN [General].[VehicleCostCenters] e 
		ON t.CostCenterId = e.CostCenterId
	-- AUTHORIZED JOIN  
	LEFT JOIN [Control].[ApprovalTransaction] [at] 
		ON [at].[TransactionId] = t.[TransactionId]
	LEFT OUTER JOIN [General].[ServiceStations] ss 
		ON t.[ProcessorId] = ss.[Terminal]
	WHERE c.[CustomerId] = @pCustomerId
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
	AND 1 > (
				SELECT ISNULL(COUNT(1), 0)
				FROM [Control].[Transactions] t2
				WHERE t2.[CreditCardId] = t.[CreditCardId]
				AND t2.[TransactionPOS] = t.[TransactionPOS]
				AND t2.[ProcessorId] = t.[ProcessorId]
				AND (
						t2.[IsReversed] = 1
						OR t2.[IsVoid] = 1
					)
			)
	AND (
			(
				@pYear IS NOT NULL
				AND @pMonth IS NOT NULL
				AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
				AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear
				)
			OR (
				@pStartDate IS NOT NULL
				AND @pEndDate IS NOT NULL
				AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate
					AND @pEndDate
				)
			)
		AND (
			@ONLY_AUTHORIZED_TRANSACTIONS IS NULL
			OR @ONLY_AUTHORIZED_TRANSACTIONS = 0
			OR (
				@ONLY_AUTHORIZED_TRANSACTIONS = 1
				AND [TypeId] = @Authorized
				)
			) -- Authorized join  
		AND (
			@pCostCenterId IS NULL
			OR @pCostCenterId = 0
			OR @pCostCenterId = t.CostCenterId
			)
	ORDER BY t.[InsertDate] DESC

	SET NOCOUNT OFF
END
GO


