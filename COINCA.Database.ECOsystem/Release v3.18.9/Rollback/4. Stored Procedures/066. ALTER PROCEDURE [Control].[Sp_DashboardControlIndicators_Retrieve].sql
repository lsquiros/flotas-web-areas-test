USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_DashboardControlIndicators_Retrieve]    Script Date: 05/06/2020 3:44:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 07/06/2016
-- Description:	Retrieve Dashboard Control Indicators 
-- Modify By: Stefano Quir�s - Add the Time Zone Parameter - 12/06/2016
-- Modify by: Stefano Quiros 13/04/2016 - 14/11/2017 - Add Void Transactions Validation
-- Stefano Quir�s - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_DashboardControlIndicators_Retrieve] --30
(
	 @pCustomerId INT
)
AS
BEGIN
	SET NOCOUNT ON

	--DECLARE  @pCustomerId INT = 30

	--SEMANA ANTERIOR FECHA INICIAL
	DECLARE @lastWKFirstDate DATETIME = DATEADD(wk, -1, DATEADD(DAY, 1-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE())))
	--SEMANA ANTERIOR FECHA FINAL
	DECLARE @lastWKFinalDate DATETIME = DATEADD(wk, 0, DATEADD(DAY, 0-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE()))) + ' 23:59:59.000'

	--SEMANA ACTUAL FECHA INICIAL
	DECLARE @currentWKFirstDate DATETIME = DATEADD(wk, 0, DATEADD(DAY, 1-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE())))
	--SEMANA ACTUAL FECHA INICIAL
	DECLARE @currentWKFinalDate DATETIME = DATEADD(wk, 1, DATEADD(DAY, 0-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE()))) + ' 23:59:59.000'

	--SUMATORIA DE MONTOS DE SEMANA ANTERIOR Y ACTUAL
	DECLARE @lastWKTotalAmount DECIMAL(16,2) = 0
	DECLARE @currentWKTotalAmount DECIMAL(16,2) = 0

	DECLARE @lTimeZoneParameter INT 

	SELECT @lTimeZoneParameter = [TimeZone]	FROM [General].[Countries] co
								 INNER JOIN [General].[Customers] cu
								 ON co.[CountryId] = cu.[CountryId]
								 WHERE cu.[CustomerId] = @pCustomerId

	--TABLA TEMPORAL DE LAS TRANSACCIONES PROCEDAS DE ESTA SEMANA Y LA SEMANA ANTERIOR
	DECLARE @TempTransProcesed TABLE(
		TransId INT,
		CreditCardId INT,
		Amount DECIMAL(16,2),
		InsertDate DATETIME
	)
	
	INSERT INTO @TempTransProcesed
	SELECT t.[TransactionId], t.[CreditCardId], t.[FuelAmount], t.[InsertDate]
	FROM [Control].[Transactions] t
		INNER JOIN [Control].[CreditCard] cc 
			ON cc.[CreditCardId] = t.[CreditCardId]
	WHERE cc.[CustomerId] = @pCustomerId
		  AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		  AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)	
		  AND 1 > (
				SELECT ISNULL(COUNT(1), 0) FROM Control.Transactions t2
						WHERE t2.[CreditCardId] = t.[CreditCardId] AND
							t2.[TransactionPOS] = t.[TransactionPOS] AND
							t2.[ProcessorId] = t.[ProcessorId] AND 
						   (t2.IsReversed = 1 OR t2.IsVoid = 1) 
			)
		  AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) >= @lastWKFirstDate
		  AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) <= @currentWKFinalDate
	-- ////////////////////////////////////////////


	-- COMPRAS SEMANA ANTERIOR
	SELECT @lastWKTotalAmount = SUM([Amount]) 
	FROM @TempTransProcesed
	WHERE DATEADD(HOUR, @lTimeZoneParameter, [InsertDate]) >= @lastWKFirstDate
		  AND DATEADD(HOUR, @lTimeZoneParameter, [InsertDate]) <= @lastWKFinalDate
	-- ////////////////////////////////////////////


	-- COMPRAS SEMANA ACTUAL
	SELECT @currentWKTotalAmount = SUM([Amount]) 
	FROM @TempTransProcesed
	WHERE DATEADD(HOUR, @lTimeZoneParameter, [InsertDate]) >= @currentWKFirstDate
		  AND DATEADD(HOUR, @lTimeZoneParameter, [InsertDate]) <= @currentWKFinalDate
	-- ////////////////////////////////////////////


	-- PORCENTAJE DE CRECIMIENTO 
	DECLARE @Growth DECIMAL(16,2) = ((@currentWKTotalAmount - @lastWKTotalAmount) / @lastWKTotalAmount) * 100
	-- ////////////////////////////////////////////


	-- CANTIDAD DE TRANSACCIONES PROCESADAS DEL D�A ACTUAL
	DECLARE @CurrentDateIni DATETIME =  DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0)
	DECLARE @CurrentDateFini DATETIME =  DATEADD(HOUR, @lTimeZoneParameter, GETDATE())
	DECLARE @CurrentCantTrans INT = 0

	SELECT @CurrentCantTrans = COUNT(1) 
	FROM @TempTransProcesed
	WHERE DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, [InsertDate])) = DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, GETDATE()))
		  AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, [InsertDate])) = DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, GETDATE()))
		  AND DATEPART(d, DATEADD(HOUR, @lTimeZoneParameter, [InsertDate])) = DATEPART(d, DATEADD(HOUR, @lTimeZoneParameter, GETDATE())) 		
	-- ////////////////////////////////////////////


	-- CANTIDAD DE TRANSACCIONES DENEGADAS DEL D�A ACTUAL
	DECLARE @CurrentDenyTrans INT = 0

	SELECT @CurrentDenyTrans = COUNT(1) 
	FROM [Control].[Transactions] t
	INNER JOIN [Control].[CreditCard] cc 
		ON cc.[CreditCardId] = t.[CreditCardId]
	WHERE cc.[CustomerId] = @pCustomerId
		  AND t.[IsDenied] = 1		  
		  AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, GETDATE()))
		  AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, GETDATE()))
		  AND DATEPART(d, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = DATEPART(d, DATEADD(HOUR, @lTimeZoneParameter, GETDATE())) 	
	-- ////////////////////////////////////////////


	-- RESULT CONTROL DASHBOARD - INDICATORS
	SELECT ISNULL(@lastWKTotalAmount, CONVERT(DECIMAL(16,2), 0)) AS [LastWeekTotalAmount]
		  ,ISNULL(@currentWKTotalAmount, CONVERT(DECIMAL(16,2), 0)) AS [WeekTotalAmount]
		  ,ISNULL(@Growth, CONVERT(DECIMAL(16,2), 0)) AS [GrowthDownOrUp]
		  ,ISNULL(@CurrentCantTrans, 0) AS [CountProcessTransactions]
		  ,ISNULL(@CurrentDenyTrans, 0) AS [CountDenyTransactions]

	-- ////////////////////////////////////////////
		   
	SET NOCOUNT OFF 
END

GO


