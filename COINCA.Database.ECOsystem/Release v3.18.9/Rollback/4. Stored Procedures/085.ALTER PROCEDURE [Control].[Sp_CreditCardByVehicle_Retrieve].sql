/****** Object:  StoredProcedure [Control].[Sp_CreditCardByVehicle_Retrieve]    Script Date: 23/07/2020 5:22:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Author:  Stefano Quirós  
-- Create date: 10/11/2018 
-- Description: Retrieve the CreditCards assign to the vehicles
-- ================================================================================================  

ALTER PROCEDURE [Control].[Sp_CreditCardByVehicle_Retrieve]
(
	 @pVehicleId INT
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT cc.[CreditCardNumber]
		  ,cc.[ExpirationYear]
		  ,cc.[ExpirationMonth]
		  ,cc.[StatusId]
		  ,s.[Name] AS [StatusName]
	FROM [Control].[CreditCard] cc
	INNER JOIN [General].[Status] s
		ON s.[StatusId] = cc.[StatusId]
	INNER JOIN [Control].[CreditCardByVehicle] cv
		ON cv.[CreditCardId] = cc.[CreditCardId]
	WHERE cv.[VehicleId] = @pVehicleId
	

	SET NOCOUNT OFF
END

