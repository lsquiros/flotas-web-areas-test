USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_AlarmCreditCount_Retrieve]    Script Date: 05/06/2020 3:32:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo.
-- Create date: 04/18/2016
-- Description:	Alarm System information
-- Modify by Stefano Quir�s - 14/11/2017 - Add Void Transactions Validation
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- ================================================================================================

ALTER PROCEDURE [Control].[Sp_AlarmCreditCount_Retrieve]
(
	 @pCustomerId INT = NULL
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pMinPct INT = NULL
	,@pUserId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	
	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''	    
		
	SELECT @lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
	INNER JOIN [General].[Customers] b
		ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId

	DECLARE @lInsertDate DATE
   
    SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)

	SELECT ISNULL(SUM(1), 0) Count
	FROM [General].[Vehicles] a
	LEFT JOIN 
	(	
		SELECT	y.[VehicleId]
				,SUM(x.[FuelAmount]) AS [RealAmount]
		FROM	[Control].[Transactions] x
		INNER JOIN [General].[Vehicles] y 
			ON x.[VehicleId] = y.[VehicleId]
		WHERE y.[CustomerId] = @pCustomerId
		AND x.[IsFloating] = 0
		AND x.[IsReversed] = 0
		AND x.[IsDuplicated] = 0
		AND (x.[IsDenied] IS NULL OR x.[IsDenied] = 0)
		AND y.[Active] = 1
		AND 1 > (
					SELECT COUNT(*) 
					FROM [Control].[Transactions] t2    
					WHERE t2.[CreditCardId] = x.[CreditCardId] 
					AND t2.[TransactionPOS] = x.[TransactionPOS] 
					AND t2.[ProcessorId] = x.[ProcessorId]
					AND (t2.IsReversed = 1 OR t2.IsVoid = 1) 
				)
		AND CONVERT(DATE, x.[Date]) >= @lInsertDate
		GROUP BY y.[VehicleId]
	)t
			ON a.[VehicleId] = t.[VehicleId]			
	INNER JOIN 
	(
		SELECT   y.[VehicleId]
				,SUM(x.[Amount]) [AssignedAmount]
				,SUM(ISNULL(x.[AdditionalAmount], 0)) [AdditionalAmount]
		FROM [Control].[FuelDistribution] x
		INNER JOIN [General].[Vehicles] y
			ON x.[VehicleId] = y.[VehicleId]
		WHERE y.[CustomerId] = @pCustomerId
		AND y.[Active] = 1
		AND CONVERT(DATE, x.[InsertDate]) >= @lInsertDate
		AND x.[Amount] > 0
		GROUP BY y.[VehicleId]
	) s
		ON a.[VehicleId] = s.[VehicleId]	
	WHERE (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	AND CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / (s.[AssignedAmount] + ISNULL(s.[AdditionalAmount], 0)), 2)*100) >= @pmINpCT
			
    SET NOCOUNT OFF
END



GO


