/****** Object:  StoredProcedure [Control].[Sp_CustomerCredits_Retrieve]    Script Date: 23/07/2020 5:23:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: NOV/09/2014
-- Description:	Retrieve CustomerCredit information
-- ================================================================================================
-- Gerald Solano  - AUG/25/2016 - Retrieve CustomerCredit Default information
-- Stefano Quiros - Make a new calculation when call the assigned credit from FuelsByCredit
-- Stefano Quirós - JUN/12/2016 - Add the Time Zone Parameter
-- Mari Jiménez   - JUN/25/2019 - Include CreditAllocated
-- Henry Retana   - 10/09/2019 - Validates the distribution type for the customer
-- ================================================================================================

ALTER PROCEDURE [Control].[Sp_CustomerCredits_Retrieve]
(
	  @pCustomerId INT
	 ,@pYear INT = NULL
	 ,@pMonth INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	DECLARE @lAssignedSum DECIMAL(16,2)
    DECLARE @lTotalSum DECIMAL(16,2)
	DECLARE @lAvailableSum DECIMAL(16,2)
	DECLARE @lTimeZoneParameter INT 
	DECLARE @lFuelsCount INT
	DECLARE @lCustomerCreditCardId INT 
	DECLARE @lCostCenterFuelDistribution BIT
	DECLARE @lInsertDate DATE

	CREATE TABLE #FuelsPerCustomer
	(
		[CustomerId] INT
	   ,[FuelId] INT
	   ,[Assigned] DECIMAL
       ,[Total] DECIMAL
	)

	SELECT  @lTimeZoneParameter = [TimeZone],
			@lCurrencySymbol = c.[Symbol],
			@lCostCenterFuelDistribution = ISNULL(cu.[CostCenterFuelDistribution], 0)
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	INNER JOIN [Control].[Currencies] c
		ON cu.[CurrencyId] = c.[CurrencyId]
	WHERE cu.[CustomerId] = @pCustomerId

	SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)

	DECLARE @LOCAL_DATE DATETIME = DATEADD(HOUR, @lTimeZoneParameter, GETDATE())
	DECLARE @LOCAL_MONTH INT = DATEPART(mm,@LOCAL_DATE)
	DECLARE @LOCAL_YEAR INT =  DATEPART(yyyy,@LOCAL_DATE) 

	IF @pYear = @LOCAL_YEAR AND @pMonth <> @LOCAL_MONTH
	BEGIN
		 SET @pYear = @LOCAL_YEAR
		 SET @pMonth = @LOCAL_MONTH
	END 	

	IF EXISTS
	(
		SELECT 1
		FROM [Control].[CustomerCredits] a
		WHERE a.[CustomerId] = @pCustomerId
	)
	BEGIN		
		SELECT @lFuelsCount = COUNT([FuelId]) 
		FROM [Control].[PartnerFuel] pf
		INNER JOIN [General].[CustomersByPartner] cp
			ON pf.[PartnerId] = cp.[PartnerId]
		WHERE cp.[CustomerId] = @pCustomerId 
		AND [EndDate] > GETDATE() 
		AND [ScheduledId] IS NULL

		--Validates the fuel dristribution by cost center
		IF @lCostCenterFuelDistribution = 1 
		BEGIN 
			INSERT INTO #FuelsPerCustomer
			SELECT TOP (@lFuelsCount) [CustomerId]
									 ,[FuelId] 
									 ,[AssignedAmount] [Assigned]
									 ,[TotalAmount] [Total]
			FROM [Control].[FuelsByCreditByCostCenter]
			WHERE [CustomerId] = @pCustomerId	
			AND [InsertDate] >= @lInsertDate	
			ORDER BY [InsertDate] DESC
		END 
		ELSE 
		BEGIN 
			INSERT INTO #FuelsPerCustomer
			SELECT TOP (@lFuelsCount) [CustomerId]
									 ,[FuelId] 
									 ,[Assigned]
									 ,[Total]
			FROM [Control].[FuelsByCredit]
			WHERE [CustomerId] = @pCustomerId	
			AND [InsertDate] >= @lInsertDate	
			ORDER BY [InsertDate] DESC
		END
		
		Select @lAssignedSum = SUM([Assigned]) 
		FROM #FuelsPerCustomer

        SELECT @lTotalSum = SUM([Total]) 
		FROM #FuelsPerCustomer

		SELECT TOP 1 @lCustomerCreditCardId = [CustomerCreditId] 
		FROM [Control].[CustomerCredits] 
		WHERE [CustomerId] = @pCustomerId
		ORDER BY [InsertDate] DESC

		UPDATE [Control].[CustomerCredits] 
		SET [CreditAssigned] = ISNULL(@lAssignedSum, 0)
		WHERE [CustomerId] = @pCustomerId
		AND [CustomerCreditId] = @lCustomerCreditCardId

		SELECT TOP 1	a.[CustomerCreditId],
						a.[CustomerId],
						a.[Year],
						a.[Month],
						a.[CreditAmount],
						a.[CreditAssigned],
						a.[CreditAvailable],
						@lTotalSum [CreditAllocated],
						(a.[CreditAssigned] + a.[CreditAvailable]) [CreditCardLimit],
						a.[CreditTypeId],
						@lCurrencySymbol AS [CurrencySymbol],
						a.[RowVersion]
		FROM [Control].[CustomerCredits] a
		WHERE a.[CustomerId] = @pCustomerId
		ORDER BY [InsertDate] DESC
	END	

    DROP TABLE #FuelsPerCustomer

    SET NOCOUNT OFF
END