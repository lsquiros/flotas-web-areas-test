USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_CreditCardByUID_Retrieve]    Script Date: 05/06/2020 2:51:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Stefano Quir�s Ruiz
-- Create date: 28/6/2019
-- Description:	Retrieve CreditCard information UID (Sticker)
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_CreditCardByUID_Retrieve] 
(
	 @pUID VARCHAR(30)
)
AS
BEGIN	
	SET NOCOUNT ON

		DECLARE @lConvertionValue DECIMAL(16, 8) = 3.78541178

		SELECT   a.[CreditCardId]
				,a.[CustomerId]
				,c.[Name] AS [EncryptedCustomerName]
				,a.[CreditCardNumber]
				,a.[ExpirationYear]
				,a.[ExpirationMonth]
				,a.[CreditAvailable]
				,CONVERT(DECIMAL(16,3), CASE WHEN p.[CapacityUnitId] = 1 
										THEN (a.[AvailableLiters] / @lConvertionValue)
										ELSE a.[AvailableLiters]
										END) [AvailableLiters]
				,p.[CapacityUnitId]
				,a.[StatusId]
				,b.[RowOrder] AS [Step]
				,b.[Name] AS [StatusName]
				,d.[Symbol] AS [CurrencySymbol]
				,c.[IssueForId]		
				,e.[UserId]
				,g.[VehicleId]
				,c.[CreditCardType]
				,f.Name AS [EncryptedDriverName]
				,'Veh�culo: ' + h.[PlateId] AS [VehiclePlate]
				,h.[PlateId] AS [TransactionPlate]
				,h.[Name] AS [VehicleName]
				,a.CardRequestId
				,i.[EstimatedDelivery]
				,a.[RowVersion]
				,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = c.[CustomerId]
						AND [Name] = 'TypeOfDistribution'
						AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
					), 0) AS INT) [TypeBudgetDistribution]
				,[PaymentInstrumentId]
				,pt.[Name] [PaymentIntrumentName]
		FROM [Control].[CreditCard] a
		INNER JOIN [Control].[PaymentInstrumentsByCreditCard] pcc
			ON pcc.[CreditCardId] = a.[CreditCardId]
		INNER JOIN [Control].[PaymentInstruments] [pi]
			ON [pi].[Id] = pcc.[PaymentInstrumentId]
		INNER JOIN [Control].[PaymentInstrumentsTypes] pt
			ON pt.[Id] = [pi].[TypeId]
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = cp.[PartnerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [Control].[CreditCardByVehicle] g
			ON g.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] = g.[VehicleId]
		INNER JOIN [Control].[CardRequest] i
			ON a.[CardRequestId] = i.[CardRequestId]
		WHERE [pi].[Code] = @pUID	

   SET NOCOUNT OFF
END
GO


