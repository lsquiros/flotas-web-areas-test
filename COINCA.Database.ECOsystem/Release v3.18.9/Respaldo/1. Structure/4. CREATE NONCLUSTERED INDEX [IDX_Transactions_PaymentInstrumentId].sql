/****** Object:  Index [IDX_Transactions]    Script Date: 5/25/2020 2:22:20 AM ******/
CREATE NONCLUSTERED INDEX [IDX_Transactions_PaymentInstrumentId] ON [Control].[Transactions]
(
	[PaymentInstrumentId] ASC,
	[ProcessorId] ASC,
	[TransactionPOS] ASC
)
INCLUDE ( 	
    [IsReversed],
	[IsVoid]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


