/****** Object:  Index [IDX_Transactions_4EE8D]    Script Date: 5/25/2020 2:25:44 AM ******/
DROP INDEX [IDX_Transactions_4EE8D] ON [Control].[Transactions]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IDX_Transactions_4EE8D]    Script Date: 5/25/2020 2:25:44 AM ******/
CREATE NONCLUSTERED INDEX [IDX_Transactions_4EE8D] ON [Control].[Transactions]
(
	[IsReversed] ASC,
	[ProcessorId] ASC,
	[TransactionPOS] ASC,
	[IsDuplicated] ASC,
	[IsVoid] ASC
)
INCLUDE ( 	
    [CreditCardId],
	[PaymentInstrumentId],
	[FuelAmount],
	[FuelId],
	[InsertDate],
	[IsAdjustment],
	[Liters],
	[Odometer],
	[VehicleId]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


