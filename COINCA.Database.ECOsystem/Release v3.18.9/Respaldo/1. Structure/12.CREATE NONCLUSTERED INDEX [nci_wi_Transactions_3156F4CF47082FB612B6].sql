/****** Object:  Index [nci_wi_Transactions_3156F4CF47082FB612B6]    Script Date: 5/25/2020 2:52:47 AM ******/
DROP INDEX [nci_wi_Transactions_3156F4CF47082FB612B6] ON [Control].[Transactions]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [nci_wi_Transactions_3156F4CF47082FB612B6]    Script Date: 5/25/2020 2:52:47 AM ******/
CREATE NONCLUSTERED INDEX [nci_wi_Transactions_3156F4CF47082FB612B6] ON [Control].[Transactions]
(
	[IsReversed] ASC,
	[IsDuplicated] ASC,
	[IsFloating] ASC,
	[IsVoid] ASC,
	[ProcessorId] ASC,
	[TransactionPOS] ASC
)
INCLUDE ( 	
    [CreditCardId],
	[PaymentInstrumentId],
	[Date],
	[FuelAmount],
	[VehicleId]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


