USE [ECOSystem]
GO

-- =======================================================================================
-- Author:		Stefano Quir�s
-- Create date: 22/05/2020
-- Description:	Add InstrumentTypeId to table Transactionshx and drop Foreign Key Constraint
-- =======================================================================================

ALTER TABLE [Control].[Transactionshx]
ADD [PaymentInstrumentId] INT NULL

ALTER TABLE [Control].[TransactionsHx] 
ALTER COLUMN [CreditCardId] INT NULL