/****** Object:  Index [nci_wi_Transactions_C43A4B541A1AD28AB464]    Script Date: 5/25/2020 2:55:02 AM ******/
CREATE NONCLUSTERED INDEX [nci_wi_Transactions_PaymentInstrument_Date] ON [Control].[Transactions]
(
	[PaymentInstrumentId] ASC,
	[IsReversed] ASC,
	[ProcessorId] ASC,
	[TransactionPOS] ASC
)
INCLUDE ( 	[FuelAmount],
	[InsertDate],
	[TransactionId]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


