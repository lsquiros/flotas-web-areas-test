/****** Object:  Index [IDX_nci_Transactions]    Script Date: 5/25/2020 2:15:56 AM ******/
DROP INDEX [IDX_nci_Transactions] ON [Control].[Transactions]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IDX_nci_Transactions]    Script Date: 5/25/2020 2:15:56 AM ******/
CREATE NONCLUSTERED INDEX [IDX_nci_Transactions] ON [Control].[Transactions]
(
	[TransactionOffline] ASC
)
INCLUDE ( 	
    [CreditCardId],
	[PaymentInstrumentId],
	[DriverCode],
	[FuelAmount],
	[Invoice],
	[Liters],
	[MerchantDescription],
	[Odometer],
	[ProcessorId],
	[VehicleId]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


