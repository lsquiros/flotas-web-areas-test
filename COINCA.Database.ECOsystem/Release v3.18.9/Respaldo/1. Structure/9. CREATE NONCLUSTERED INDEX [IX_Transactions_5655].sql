/****** Object:  Index [IX_Transactions_5655]    Script Date: 5/25/2020 2:44:19 AM ******/
DROP INDEX [IX_Transactions_5655] ON [Control].[Transactions]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_Transactions_5655]    Script Date: 5/25/2020 2:44:19 AM ******/
CREATE NONCLUSTERED INDEX [IX_Transactions_5655] ON [Control].[Transactions]
(
	[IsFloating] ASC,
	[VehicleId] ASC,
	[IsDenied] ASC
)
INCLUDE ( 	
    [CreditCardId],
	[PaymentInstrumentId],
	[Date],
	[InsertDate],
	[Liters],
	[Odometer],
	[ProcessorId],
	[RealLiters],
	[ShowAlert],
	[TransactionPOS]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


