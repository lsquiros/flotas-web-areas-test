--USE [ECOsystem]
GO
IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Fn_TransactionsAmountByCustomer]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [Control].[Fn_TransactionsAmountByCustomer] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- Author:		Stefano Quirós
-- Create date: 20-05-2019
-- Description:	Get fuel amount of the transactions By Customer
-- Modify by Marjorie Garbanzo - 01/06/2020 Add logic to include transactions with different payment methods
-- ============================================================

CREATE FUNCTION [Control].[Fn_TransactionsAmountByCustomer] 
(
	 @pCustomerId INT = NULL
	,@pTimeZoneParameter INT = NULL
	,@pInsertDate DATE = NULL
	,@pFinalDate DATE = NULL
)
RETURNS DECIMAL(16, 6)
AS
BEGIN

	DECLARE @RealAmount DECIMAL(16, 6) = 0	
	
	SET @RealAmount = ISNULL((
				SELECT SUM(x.[FuelAmount])
				FROM [Control].[Transactions] x
				INNER JOIN [Control].[CreditCard] c
					ON c.[CreditCardId] = x.[CreditCardId]
				WHERE c.[CustomerId] = @pCustomerId
					AND x.[IsFloating] = 0
					AND x.[IsReversed] = 0
					AND x.[IsDuplicated] = 0
					AND 1 > (
						SELECT COUNT(1)
						FROM CONTROL.Transactions t2
						WHERE (t2.[CreditCardId] = x.[CreditCardId]
								OR t2.[PaymentInstrumentId] = x.[PaymentInstrumentId]) 
							AND t2.[TransactionPOS] = x.[TransactionPOS]
							AND t2.[ProcessorId] = x.[ProcessorId]
							AND (
								t2.IsReversed = 1
								OR t2.IsVoid = 1
								)
						)
					AND (DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) >= @pInsertDate
							AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) <= @pFinalDate)
				), 0.0)

	RETURN @RealAmount;

END

