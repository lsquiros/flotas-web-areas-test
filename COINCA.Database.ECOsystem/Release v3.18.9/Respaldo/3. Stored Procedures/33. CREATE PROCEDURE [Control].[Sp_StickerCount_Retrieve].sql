/****** Object:  StoredProcedure [Control].[Sp_StickerCount_Retrieve]    Script Date: 27/05/2020 10:08:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      Jason Bonilla
-- Create Date: 20/05/2020
-- Description: Retrieve Sticker Available Count
-- =============================================
CREATE PROCEDURE [Control].[Sp_StickerCount_Retrieve]
AS
BEGIN
    SELECT COUNT(*) AS [StickerCount]
	FROM [Control].[PaymentInstruments] 
	WHERE [CustomerId] IS NULL
END
GO


