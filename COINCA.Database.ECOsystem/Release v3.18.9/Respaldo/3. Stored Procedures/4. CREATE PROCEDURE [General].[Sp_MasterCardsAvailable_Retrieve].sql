﻿USE [ECOSystem]
GO
IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_MasterCardsAvailable_Retrieve' AND TYPE = 'P')
	DROP PROC [General].[Sp_MasterCardsAvailable_Retrieve]
GO

-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 24/04/2020
-- Description:	Retrieve the Available Alert of the Master Cards
-- ================================================================================================
 CREATE PROCEDURE [General].[Sp_MasterCardsAvailable_Retrieve]
 (	
	 @pPartnerId INT = NULL
	,@pCustomerId INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
	,@pCostCenterId INT = NULL
	,@pVehicleGroupId INT = NULL
	,@pUserId INT = NULL
 )
 AS
BEGIN	
	SET NOCOUNT ON	

		DECLARE @lTimeZone INT = (SELECT [TimeZone]
								  FROM [General].[Partners] p 
								  INNER JOIN [General].[Countries] c
									ON c.[CountryId] = p.[CountryId]
								  WHERE [PartnerId] = @pPartnerId)

		DECLARE @lDate DATE = DATEADD(HOUR, @lTimeZone, GETDATE())
		DECLARE @lMasterCardPercent INT = (SELECT [MasterCardPercent]
										   FROM [General].[GeneralParametersByPartner]
										   WHERE [PartnerId] = @pPartnerId)

		SELECT c.[CustomerId]
		      ,c.[Name] [CustomerName]
		      ,p.[PartnerId]
			  ,p.[Name] [PartnerName]
		      ,[General].[Fn_LatestMonthlyClosing_Retrieve] (c.[CustomerId]) [LastClosingDate]
		INTO #Customers
		FROM [General].[Customers] c
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = p.[PartnerId]
		WHERE p.[PartnerId] = @pPartnerId
			  AND c.[IsActive] = 1
			  AND (c.[IsDeleted] IS NULL
			       OR c.[IsDeleted] = 0)
				   					
		
		SELECT c.[CustomerName] [EncryptCustomerName]
			  ,cc.[CreditCardNumber]
			  ,cc.[ExpirationMonth]
			  ,cc.[ExpirationYear]
			  ,c.[PartnerName]
			  ,CONVERT(INT, (SUM(FuelAmount) * 100) / cc.[CreditLimit]) [Percent]
			  ,(cc.[CreditLimit] - SUM(FuelAmount)) [Available]
		INTO #Result
		FROM [General].[CustomerCreditCards] cc
		INNER JOIN #Customers c
			ON c.[CustomerId] = cc.[CustomerId]
		INNER JOIN [General].[GeneralParametersByPartner] gp
			ON gp.[PartnerId] = c.[PartnerId]
		INNER JOIN [Control].[CreditCard] ccs
			ON ccs.[CustomerId] = c.[CustomerId]
		INNER JOIN [Control].[Transactions] t
			ON t.[CreditCardId] = ccs.[CreditCardId]
			AND t.[Date] >= c.[LastClosingDate]
		WHERE c.[PartnerId] =  @pPartnerId 
		AND [MasterCardAlertActive] = 1
		AND cc.[InternationalCard] = 0
	    AND (
				(
				 [MasterCardAlertPeriodicity] = 7 
				 AND [MasterCardAlertNameDay] = DATEPART(dw,@lDate) 
		         AND ([LastMasterCardAlertNotificationSent] IS NULL 
					  OR DATEDIFF(WEEK, [LastMasterCardAlertNotificationSent], @lDate) >= [ExpireCreditCardRepeat])
				)
			    OR (
				    [MasterCardAlertPeriodicity] = 30 
					AND [MasterCardAlertNameDay] = Day(@lDate)
					AND ([LastMasterCardAlertNotificationSent] IS NULL 
						 OR DATEDIFF(MONTH, [LastMasterCardAlertNotificationSent], @lDate) >= [ExpireCreditCardRepeat])
		           )
		    )
		AND (t.[IsDenied] IS NULL 
			 OR t.[IsDenied] = 0)
		AND (t.[IsFloating] IS NULL
			 OR t.[IsFloating] = 0)
		AND 1 > (
				 SELECT ISNULL(COUNT(1), 0)
				 FROM CONTROL.Transactions t2
				 WHERE (t2.[CreditCardId] = nt.[CreditCardId]
						OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId])
				       AND t2.[TransactionPOS] = t.[TransactionPOS]
				 	   AND t2.[ProcessorId] = t.[ProcessorId]
				 	   AND (
				 			t2.IsReversed = 1
				 		    OR t2.IsVoid = 1
				 		   )
				)
		AND (t.[TransactionOffline] IS NULL
			 OR t.[TransactionOffline] = 0)
		GROUP BY c.[CustomerName]
			    ,cc.[CreditCardNumber]
			    ,c.[PartnerName]	
				,cc.[CreditLimit]
				,cc.[ExpirationMonth]
			    ,cc.[ExpirationYear]
		HAVING ((SUM(FuelAmount) * 100) / cc.[CreditLimit]) >= @lMasterCardPercent	 
	

		DECLARE @lCount INT = (SELECT COUNT(*) 
							FROM #Result)
		   
		IF @lCount > 0
		BEGIN			
			UPDATE [General].[GeneralParametersByPartner]
			SET [LastMasterCardAlertNotificationSent] = GETDATE()
			WHERE [PartnerId] = @pPartnerId		
		END 
	
		SELECT * FROM #Result 

		DROP TABLE #Result
		DROP TABLE #Customers

    SET NOCOUNT OFF
END

