/****** Object:  StoredProcedure [Control].[Sp_Transactions_AddFromAPI]    Script Date: 6/1/2020 2:27:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/30/2014
-- Description:	Insert Transaction information from API
-- Update By:	Gerald Solano
-- Create date: 15/06/2016
-- Description:	UPDATE COLUMN TO POS IF THE REVERSE IS TRUE 
-- Modify By: Stefano Quirós Ruiz
-- Description: TRIM to the PlateId
-- Modify: 8/8/16 - Henry Retana
-- Add the data IsInternational and Country Code to the transaction records
-- Modify: 17/8/16 - Henry Retana
-- Add the data IsPreauthorized and Authorization Code to the transaction records
-- Modify: 14/9/16 - Gerald Solano
-- Se modifica la inserción del valor de la columna Date para que se use el DateAdd
-- Modify: 30/11/16 - Gerald Solano
-- Se valida que no apliquen duplicidad de reversiones 
-- Modify: 04/05/17 - Henry Retana
-- Add the transaction offline flag to the transactions
-- Modify: 25/05/17 - Kevin Peña
-- Add ServiceStationLocation_ByTransaction exec after History Control
-- Modify: 15/11/17 - Henry Retana
-- Validates credit when the transaction is void
-- Modify: 17/04/18 - Esteban Solís
-- Validate is customer's partner has to be authorized or consider transaction as internal
-- Modify: 24/04/18 - Henry Retana
-- Validates if the price is from the service station or the partner
-- Modify: 30/04/18 - Henry Retana
-- Validates approve internal transactions
-- Modify: 10/09/18 - Henry Retana
-- Validates Vehicle Odometer, based on the parameter and vehiclegps
-- Modify: 05/11/2018 - Add VehicleId Parameter
-- Stefano Quirós - Add liters to restart or add to the available - 14/01/2019
-- Henry Retana - 05/03/19
-- Add Denied transaction to table
-- Stefano Quirós - 24/06/2019 - Validate the ReversionType to restore the available of the creditcard
-- Stefano Quirós - 01/08/2019 - Add parameter UID to is related to the CreditCardId
-- when CreditCardNumber is not send
-- Juan Carlos Santamaria - 26/12/2019 Fuel price validation, Validation of the amount of fuel -- changeId:JSA-001
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_Transactions_AddFromAPI]
(
	 @pPlateId VARCHAR(10)	
	,@pFuelId INT = NULL
	,@pCreditCardNumber NVARCHAR(520) = NULL
	,@pDate DATETIME
	,@pOdometer INT
	,@pLiters DECIMAL(16,3)
	,@pFuelAmount DECIMAL(16,2)	
	,@pInvoice VARCHAR(12) = NULL
	,@pTransactionPOS VARCHAR(250) = NULL
	,@pSchemePOS VARCHAR(MAX) = NULL
	,@pMerchantDescription VARCHAR(200) = NULL
	,@pProcessorId VARCHAR(250) = NULL
	,@pIsFloating BIT = NULL
	,@pIsReversed BIT = NULL
	,@pIsDuplicated BIT = NULL
	,@pIsVoid BIT = NULL
	,@pIsDenied BIT = NULL
	,@pLoggedUserId INT
	,@pCustomerId INT
	,@pDriverCode VARCHAR(MAX)	
	,@pIsInternational BIT = NULL
	,@pCountryCode NVARCHAR(10) = NULL
	,@pAuthorizationNumber NVARCHAR(20) = NULL
	,@pTransactionOffline BIT = NULL
	,@pVehicleId INT = NULL
	,@pReverseType VARCHAR(30) = NULL
	,@pUID VARCHAR(200) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY

		--JSA-001
		IF @pLiters = 0
		BEGIN
			RAISERROR ('La cantidad de combustible (ltr/Gal) NO PUEDE estar en CERO',16,1);
		END
		
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0         
            DECLARE @lTransactionId INT = 0
            DECLARE @lCreditCardId INT
			DECLARE @lPaymentInstrumentId INT
			DECLARE @IsInternal BIT
			DECLARE @CostCenter INT
			DECLARE @ReverseBy VARCHAR(100) = ''
			DECLARE @ExchangeValue DECIMAL(10, 2)
			DECLARE @lMerchantDescription VARCHAR(200)
			DECLARE @lTimeZoneParameter INT 
			DECLARE @lOdometerType BIT
			DECLARE @lHasGPS BIT 
			DECLARE @lVehicleId INT = NULL
			DECLARE @lIssueForId INT = NULL
			DECLARE @lCustomerId INT = NULL

			SELECT @lTimeZoneParameter = [TimeZone]
			FROM [General].[Countries] co
			INNER JOIN [General].[Customers] cu ON co.[CountryId] = cu.[CountryId]
			WHERE cu.[CustomerId] = @pCustomerId

			IF (@pMerchantDescription IS NULL OR @pMerchantDescription = '')
			BEGIN
				SET @pMerchantDescription = [General].[GetServiceStationInfo](@pProcessorId, @pCustomerId, 0)
			END

			SET @IsInternal =  CASE WHEN EXISTS (
													SELECT 1 
													FROM [General].[Partners] p
													INNER JOIN [General].[CustomersByPartner] c 
														ON p.[PartnerId] = c.[PartnerId]
													WHERE c.[CustomerId] = @pCustomerId
													AND p.TypeId = 3
												)
									THEN 1
									ELSE 
										CASE WHEN EXISTS (
															SELECT 1 
															FROM [General].[Terminal] 
															WHERE [TerminalId] = @pProcessorId 
															AND [CustomerId] = @pCustomerId
														 ) 
											THEN 1
											ELSE 0
										END
								END
			
			IF @pUID IS NULL OR NULLIF(@pUID, '') IS NULL
			BEGIN
				SELECT @lCreditCardId = [CreditCardId]
				FROM [Control].[CreditCard]
				WHERE [CreditCardNumber] = @pCreditCardNumber				
			END
			ELSE
			BEGIN
				SELECT @lPaymentInstrumentId = [Id]
				FROM [Control].[PaymentInstruments] [pi]
				WHERE [pi].[Code] = @pUID
			END

			IF @lCreditCardId IS NULL
			BEGIN
				SELECT TOP 1 @lVehicleId = [VehicleId]
				FROM [Control].[PaymentInstrumentsByType] pit
				WHERE [PaymentInstrumentId] = @lPaymentInstrumentId
				ORDER BY [InsertDate] DESC
			END
			ELSE
			BEGIN
				SELECT TOP 1 @lVehicleId = [VehicleId]
				FROM [Control].[CreditCardByVehicle] cc
				WHERE [CreditCardId] = @lCreditCardId
				ORDER BY [InsertDate] DESC
			END
			
			IF @lVehicleId IS NULL
			BEGIN
				SELECT TOP 1 @lVehicleId = [VehicleId]
				FROM [Control].[CreditCardByDriver] cc
				INNER JOIN  [General].[VehiclesByUser] vu
					ON vu.[UserId] = cc.[UserId]
				WHERE [CreditCardId] = @lCreditCardId
				AND [LastDateDriving] IS NULL
				ORDER BY vu.[InsertDate] DESC
			END

			IF @lVehicleId IS NULL
			BEGIN
				SELECT TOP 1 @lVehicleId = [VehicleId]
				FROM [Control].[CreditCardByDriver] cc
				INNER JOIN [General].[VehiclesDrivers] vd
					ON vd.[UserId] = cc.[UserId]
				WHERE [CreditCardId] = @lCreditCardId
				ORDER BY vd.[InsertDate] DESC
			END

			
			IF @lVehicleId IS NULL
			BEGIN
				RAISERROR ('Por favor verifique la configuración de su Flota (Conductores asignados a sus vehículos)', 16, 1)
			END

			--COST CENTER BY DRIVER VALIDATION
			IF ISNULL((SELECT TOP 1 [Value] 
			           FROM [General].[ParametersByCustomer]
			           WHERE [CustomerId] = @pCustomerId
			           AND [ResuourceKey] = 'COSTCENTERBYDRIVER'), 0) = 1
			BEGIN
				SELECT TOP 1 @CostCenter = du.[CostCenterId]
   				FROM [General].[DriversUsers] du
				INNER JOIN [General].[VehiclesByUser] vbu
				ON du.[UserId] = vbu.[UserId] 
				AND vbu.[VehicleId] = @lVehicleId
				WHERE vbu.[LastDateDriving] IS NULL
			END
			ELSE 
			BEGIN
				SELECT @CostCenter = a.[CostCenterId]
				FROM [General].[Vehicles] a
				WHERE [VehicleId] = @lVehicleId
			END    

			-- UPDATE COLUMN TO POS IF THE REVERSE IS TRUE 
			IF @pIsReversed = 1
			BEGIN
				SET @ReverseBy = 'POS'
			END
            
			--GET THE EXCHANGE VALUE
			IF @pIsInternational = 1
			BEGIN
				SET @ExchangeValue = (SELECT [Value] 
									  FROM [Control].[ExchangeRates] er
									  INNER JOIN [Control].[Currencies] cr
										ON er.[CurrencyCode] = cr.[CurrencyCode]
									  WHERE cr.[Code] = @pCountryCode
									  AND er.[Until] IS NULL)
			END

			SELECT @lIssueForId = [IssueForId] 
				  ,@lCustomerId = c.[CustomerId]
			FROM [General].[Customers] c
			LEFT JOIN [Control].[CreditCard] cc
				ON cc.[CustomerId] = c.[CustomerId]
				AND cc.[CreditCardId] = @lCreditCardId		
			LEFT JOIN [Control].[PaymentInstruments] pay
				ON pay.[CustomerId] = c.[CustomerId]
				AND pay.[Id] = @lPaymentInstrumentId

			IF @lIssueForId = 100
			BEGIN
				SET @pFuelId = NULL

				SELECT @pFuelId = [FuelId]
				FROM [Stations].[FuelPriceByServiceStations] fs			
				INNER JOIN [General].[ServiceStations] s
					ON s.[ServiceStationId] = fs.[ServiceStationId]
				INNER JOIN [General].[TerminalsByServiceStations] tss
					ON tss.[ServiceStationId] = s.[ServiceStationId]
				WHERE fs.[EndDate] IS NULL
				AND fs.[Price] > 0
				AND tss.[TerminalId] = @pProcessorId
				AND CONVERT(INT, fs.[Price]) = CONVERT(INT, (@pFuelAmount / @pLiters))
				
				IF @pFuelId IS NULL
				BEGIN
				SELECT @pFuelId = [FuelId]	
					FROM [Control].[PartnerFuel] pf 
					INNER JOIN [General].[CustomersByPartner] cp 
						ON cp.[PartnerId] = pf.[PartnerId]					
					WHERE cp.[CustomerId] = @lCustomerId
						AND pf.[EndDate] >= DATEADD(HOUR, -6, GETUTCDATE())
						AND pf.[History] IS NULL
						AND pf.[ScheduledId] IS NULL
						AND CONVERT(INT, pf.[LiterPrice]) = CONVERT(INT, (@pFuelAmount / @pLiters))
				END		
			END

			--GET FUELID 
			IF @pFuelId IS NULL
			BEGIN
				SET @pFuelId = (SELECT TOP 1 d.[DefaultFuelId]			
							    FROM [General].[Vehicles] a	
								INNER JOIN [General].[VehicleCategories] d
									ON a.[VehicleCategoryId] = d.[VehicleCategoryId]
								INNER JOIN [Control].[Fuels] b
									ON d.[DefaultFuelId] = b.[FuelId]	
								WHERE a.CustomerId = @pCustomerId 
								AND a.[VehicleId] = @lVehicleId)
			END
			
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
			
			-- SE VALIDA SI UNA REVERSIÓN YA FUE APLICADA  ////////////
			DECLARE @HasReverse INT = 0
				   ,@lReverseVehicleId INT = NULL

			IF @pIsReversed = 1
			BEGIN
				SELECT @HasReverse = 1, 
					   @lTransactionId = [TransactionId]
				FROM [Control].[Transactions]
				WHERE [TransactionPOS] = @pTransactionPOS 
				AND [ProcessorId] = @pProcessorId 
				AND [IsReversed] = 1
				
				SELECT @lReverseVehicleId = [VehicleId]
				FROM [Control].[Transactions]
				WHERE [TransactionPOS] = @pTransactionPOS 
				AND [ProcessorId] = @pProcessorId
				AND [VehicleId] = @lVehicleId

			END
			-- ////////////////////////////////////////////////////////
			
			IF @HasReverse = 0 OR @HasReverse IS NULL
			BEGIN
				--Get the liter price based on the service station or the partner
				DECLARE @lLiterPrice DECIMAL(19,2),
						@lServiceStation INT,
						@lPartnerId INT,
						@lInsertDate DATETIME = CONVERT(DATE, DATEADD(HOUR, -6, GETDATE()))

				SELECT @lServiceStation = [ServiceStationId]
				FROM [General].[TerminalsByServiceStations] 
				WHERE [TerminalId] = @pProcessorId
				
				SELECT TOP 1 @lPartnerId = pf.[PartnerId] 
				FROM [Control].[PartnerFuel] pf
				INNER JOIN [General].[CustomersByPartner] cp
					ON pf.[PartnerId] = cp.[PartnerId]
				WHERE pf.[FuelId] = @pFuelId 
				AND cp.[CustomerId] = @pCustomerId

				SELECT @lLiterPrice = ISNULL([Control].[Fn_FuelPriceBySSOrPartner_Retrieve] (GETDATE(), @pFuelId, @lServiceStation, NULL),
											 [Control].[Fn_FuelPriceBySSOrPartner_Retrieve] (GETDATE(), @pFuelId, NULL, @lPartnerId))
				
				--JSA-001
				IF @lLiterPrice = 0
				BEGIN
					RAISERROR ('El precio del combustible NO PUEDE estar en CERO',16,1);
				END 

				INSERT INTO [Control].[Transactions]
				(
				     [VehicleId]
					,[FuelId]
					,[CreditCardId]
					,[Date]
					,[Odometer]
					,[Liters]
					,[FuelAmount]					
					,[TransactionPOS]
					,[SchemePOS]
					,[ProcessorId]
					,[IsFloating]
					,[IsReversed]
					,[IsDuplicated]
					,[IsVoid]
					,[InsertDate]
					,[InsertUserId]
					,[Invoice]
					,[MerchantDescription]
					,[IsInternal]
					,[DriverCode]
					,[CostCenterId]					
					,[IsInternational]
					,[CountryCode]
					,[ExchangeValue]
					,[AuthorizationNumber]
					,[TransactionOffline]
					,[RealLiters]
					,[IsDenied]
					,[PaymentInstrumentId]
				)
				VALUES	
				(	
					@lVehicleId
				   ,@pFuelId
				   ,@lCreditCardId
				   ,DATEADD(HOUR, @lTimeZoneParameter, GETUTCDATE())   --@pDate GSOLANO: Se hace uso del dateadd para registrar la fecha y hora local correcta 
				   ,@pOdometer
				   ,@pLiters
				   ,@pFuelAmount					
				   ,@pTransactionPOS
				   ,@pSchemePOS
				   ,@pProcessorId
				   ,CASE WHEN @IsInternal = 1 
				    THEN 0
				    ELSE @pIsFloating
					END
				   ,@pIsReversed
				   ,@pIsDuplicated
				   ,@pIsVoid
				   ,GETUTCDATE()
				   ,@pLoggedUserId
				   ,@pInvoice
				   ,@pMerchantDescription
				   ,@IsInternal
				   ,@pDriverCode
				   ,@CostCenter					
				   ,@pIsInternational
				   ,@pCountryCode
				   ,@ExchangeValue
				   ,@pAuthorizationNumber
				   ,@pTransactionOffline
				   ,(@pFuelAmount / @lLiterPrice)
				   ,@pIsDenied
				   ,@lPaymentInstrumentId
				)
			   			
				SELECT @lTransactionId = SCOPE_IDENTITY()
				
				--Set transaction as not approved
				IF @IsInternal = 1
				BEGIN 
					EXEC [General].[Sp_ValidateApproveInternalTransactions_Add] @pCustomerId, @lTransactionId
				END							
			
				IF (@pTransactionOffline = 1)
				BEGIN
					INSERT INTO [Control].[TransactionsOfflineReportToSend]
					(
						[TransactionId], 
						[CustomerId], 
						[Send], 
						[InsertDate]
					)
					VALUES 
					(
						@lTransactionId, 
						@pCustomerId, 
						0, 
						GETDATE()
					)										
				END

				--IF (@pIsReversed = 0 and @pIsDuplicated = 0)
				IF (@pIsDuplicated = 0 AND @pIsFloating = 0 AND @pIsDenied = 0) OR (@IsInternal = 1 AND @pIsDenied = 0) 
				BEGIN
					IF @pIsReversed = 0 AND @pIsVoid = 1 SET @pIsReversed = 1 --VALIDATES REVERSION WHEN THE TRANSATION IS BEING VOID
					IF @pReverseType = 'REVERSE' OR @IsInternal = 1
					BEGIN
						Exec [Control].[Sp_CreditCard_CreditAvailable_Edit] @lPaymentInstrumentId, @lCreditCardId, @pFuelAmount, @pIsReversed, @pLiters
					END
				END
			
				-- History control --
				INSERT INTO [Control].[TransactionsHx]
				(
					[TransactionId]
				   ,[CreditCardId]
				   ,[VehicleId]
				   ,[FuelId]
				   ,[Date]
				   ,[Odometer]
				   ,[FuelAmount]
				   ,[TransactionPOS]
				   ,[SchemePOS]
				   ,[ProcessorId]
				   ,[IsAdjustment]
				   ,[IsFloating]
				   ,[IsReversed]
				   ,[IsDuplicated]
				   ,[IsVoid]
				   ,[FixedOdometer]
				   ,[Liters]
				   ,[InsertDate]
				   ,[InsertUserId]
				   ,[ModifyDate]
				   ,[ModifyUserId]
				   ,[Invoice]
				   ,[MerchantDescription]
				   ,[DriverCode]
				   ,[CostCenterId]			   
				   ,[IsInternational]
				   ,[CountryCode]
				   ,[ExchangeValue]
				   ,[AuthorizationNumber]
				   ,[TransactionOffline]
				   ,[RealLiters]
				   ,[IsDenied]
				   ,[PaymentInstrumentId]
				)
				SELECT 
					[TransactionId]
				   ,[CreditCardId]
				   ,[VehicleId]
				   ,[FuelId]
				   ,[Date]
				   ,[Odometer]
				   ,[FuelAmount]
				   ,[TransactionPOS]
				   ,[SchemePOS]
				   ,[ProcessorId]
				   ,[IsAdjustment]
				   ,[IsFloating]
				   ,[IsReversed]
				   ,[IsDuplicated]
				   ,[IsVoid]
				   ,[FixedOdometer]
				   ,[Liters]
				   ,[InsertDate]
				   ,[InsertUserId]
				   ,[ModifyDate]
				   ,[ModifyUserId]
				   ,[Invoice]
				   ,[MerchantDescription] 
				   ,[DriverCode]
				   ,[CostCenterId]			   
				   ,[IsInternational]
				   ,[CountryCode]
				   ,[ExchangeValue]
				   ,[AuthorizationNumber]
				   ,[TransactionOffline]
				   ,(@pFuelAmount / @lLiterPrice)
				   ,[IsDenied]
				   ,[PaymentInstrumentId]
				FROM [Control].[Transactions] 
				WHERE [TransactionId] = @lTransactionId
				
				-- Service Station Location by Transaction --			
				EXEC [General].[Sp_ServiceStationLocation_byTransaction] @pProcessorId, @lVehicleId	
				
				SELECT @lHasGPS = CONVERT(BIT, COUNT(1)) 
				FROM [General].[Vehicles] 
				WHERE ([DeviceReference] IS NOT NULL AND [DeviceReference] > 0) 
				AND ([IntrackReference] IS NOT NULL AND [IntrackReference] > 0)
				AND [VehicleId] = @lVehicleId

				IF NOT EXISTS(SELECT * FROM [General].[ParametersByCustomer]
							  WHERE [CustomerId] = @lCustomerId
							  AND [ResuourceKey] = 'TYPEODOMETERALARM'
							  AND [Value] = 1) OR @lHasGPS = 0 
				BEGIN 
					--Si la transacción es reversada agarra el lastodometer y lo devuelve al odometer
					IF @lReverseVehicleId IS NOT NULL
					BEGIN
						UPDATE [General].[Vehicles]
						SET [Odometer] = [LastOdometer]	
						WHERE [VehicleId] = @lVehicleId
					END
					ELSE
					BEGIN
						UPDATE [General].[Vehicles]
						SET [LastOdometer] = [Odometer]
						   ,[Odometer] = @pOdometer
						WHERE [VehicleId] = @lVehicleId
					END
				END
			END -- END IF HAS_REVERSION

            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
	
	SELECT @lTransactionId AS [TransactionId], @IsInternal AS [IsInternal]
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

