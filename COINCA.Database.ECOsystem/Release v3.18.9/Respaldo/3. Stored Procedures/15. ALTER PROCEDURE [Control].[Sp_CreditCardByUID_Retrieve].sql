/****** Object:  StoredProcedure [Control].[Sp_CreditCardByUID_Retrieve]    Script Date: 5/29/2020 4:33:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Stefano Quirós Ruiz
-- Create date: 28/6/2019
-- Description:	Retrieve CreditCard information UID (Sticker)
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_CreditCardByUID_Retrieve] 
(
	 @pUID VARCHAR(30)
)
AS
BEGIN	
	SET NOCOUNT ON

		DECLARE @lConvertionValue DECIMAL(16, 8) = 3.78541178

		SELECT   pit.[PaymentInstrumentId]
				,[pi].[CustomerId]
				,c.[Name] AS [EncryptedCustomerName]
				,[pi].[Code]
				,ISNULL([pi].[CreditAvailable], 0) [CreditAvailable]
				,CONVERT(DECIMAL(16,3), CASE WHEN p.[CapacityUnitId] = 1 
										THEN ([pi].[AvailableLiters] / @lConvertionValue)
										ELSE [pi].[AvailableLiters]
										END) [AvailableLiters]
				,p.[CapacityUnitId]
				,[pi].[StatusId]
				,b.[RowOrder] AS [Step]
				,b.[Name] AS [StatusName]
				,d.[Symbol] AS [CurrencySymbol]
				,c.[IssueForId]	
				,pit.[VehicleId]
				,c.[CreditCardType]
				,'Vehículo: ' + h.[PlateId] AS [VehiclePlate]
				,h.[PlateId] AS [TransactionPlate]
				,h.[Name] AS [VehicleName]
				,pit.CardRequestId
				,i.[EstimatedDelivery]
				,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = c.[CustomerId]
						AND [Name] = 'TypeOfDistribution'
						AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
					), 0) AS INT) [TypeBudgetDistribution]
				,pt.[Name] [PaymentIntrumentName]
		FROM [Control].[PaymentInstruments] [pi]	
		INNER JOIN [Control].[PaymentInstrumentsByType] pit
			ON [pi].[Id] = pit.[PaymentInstrumentId]		
		INNER JOIN [Control].[PaymentInstrumentsTypes] pt
			ON pt.[Id] = [pi].[TypeId]
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = [pi].[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = [pi].[CustomerId]
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = cp.[PartnerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		INNER JOIN [General].[Vehicles] h
			ON h.[VehicleId] = pit.[VehicleId]
		INNER JOIN [Control].[CardRequest] i
			ON PIT.[CardRequestId] = i.[CardRequestId]
		WHERE [pi].[Code] = @pUID	

   SET NOCOUNT OFF
END
