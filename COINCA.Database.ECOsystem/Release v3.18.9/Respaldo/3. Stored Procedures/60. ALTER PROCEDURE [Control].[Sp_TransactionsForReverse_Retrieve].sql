/****** Object:  StoredProcedure [Control].[Sp_TransactionsForReverse_Retrieve]    Script Date: 03/06/2020 7:09:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 07/01/2014
-- Description:	Retrieve Transactions Info For Reverse Operations
-- Stefano Quirós - Comment the IsFloating validation because the transaction will be processed when 
-- we receive the RESULT_ADVICE from BAC - 28/11/2018
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================

ALTER PROCEDURE [Control].[Sp_TransactionsForReverse_Retrieve]
(
	 @pTransactionPOS VARCHAR(250)			--@pTransactionPOS: TransactionPOS (External System TraceNumber)
	,@pProcessorId VARCHAR(250)				--@pProcessorId: ProcessorId (External terminalId)
)
AS
BEGIN
	
	SET NOCOUNT ON
			
	SELECT TOP 1
	     a.[TransactionId]
		 ,a.[CreditCardId]
		 ,a.[VehicleId]
		 ,a.[FuelId]
		 ,a.[Odometer]
		 ,a.[FuelAmount]
		 ,a.[Liters]
		 ,b.[CreditCardNumber] [EncryptedCreditCardNumber]
		 ,pay.[Code] [EncryptedUID]
		 ,c.[PlateId]
    FROM [Control].[Transactions] a
		LEFT JOIN [Control].[CreditCard] b
			ON b.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [Control].[PaymentInstruments] pay
			ON a.[PaymentInstrumentId] = pay.[Id]
		INNER JOIN [General].[Vehicles] c
			ON c.[VehicleId] = a.[VehicleId]
	WHERE a.[TransactionPOS] = @pTransactionPOS
	  AND a.[ProcessorId] = @pProcessorId
	  --AND a.[IsFloating] = 0
	  AND a.[IsReversed] = 0
	  AND a.[IsDuplicated] = 0
	  AND a.[IsVoid] = 0
	  AND (a.[IsDenied] IS NULL OR a.[IsDenied] = 0)
	  AND COALESCE(a.[IsAdjustment],0) = 0
	ORDER BY a.[InsertDate] DESC
	
    SET NOCOUNT OFF
END