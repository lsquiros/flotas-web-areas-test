/****** Object:  StoredProcedure [General].[Sp_Retrieve_VehicleSticker]    Script Date: 01/06/2020 12:07:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      Jason Bonilla
-- Create Date: 29/05/2020
-- Description: Validate Vehicle Sticker
-- =============================================
CREATE PROCEDURE [General].[Sp_Retrieve_VehicleSticker]
(
    @pVehicleId INT
)
AS
BEGIN
    SELECT COUNT(*) FROM [Control].[PaymentInstrumentsByType] WHERE VehicleId = @pVehicleId
END
GO


