-- ================================================================================================      
-- Author:  Berman Romero L.      
-- Create date: 10/21/2014      
-- Description: Retrieve Current Fuels Report By Vehicle information      
-- Dinamic Filter - 1/22/2015 - Melvin Salas      
-- Modify by: Stefano Quiros 13/04/2016      
-- Modify by: Henry Retana - 09/06/2017      
-- Modifica el proceso para obtener los vehiculos      
-- Modify by: Marco Cabrera Chaves 30-03-2017      
-- Agrega la condicion para que no se muestren los vehiculos eliminados      
-- Modify By:   Albert Estrada Oct/17/2017      
-- Description: Add Columns      
-- Modify By:   Esteban Solis Nov/08/2017      
-- Description: Add Columns for excel export     
-- Modify by: Stefano Quiros 13/04/2016 - 14/11/2017 - Add Void Transactions Validation    
-- Modify by Esteban Solís - 30-01-2018 -  Add column Litters and change calculation of real litters 
-- Modify by Esteban Solís - 09-02-2018 - Fix issue with % showing zeros only
-- Stefano Quirós - Add the Available liters to the retrieve - 16/01/2019
-- Modify By: Juan k Santamaria Fix add convert Lt to Gal AvailableLiters, Date: 17-03-2020
-- ================================================================================================
      
ALTER PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicle_Retrieve] --30, 2020, 1, null, null, 1--11224,2020, 1, null, null, 1
(
--DECLARE
	 @pCustomerId INT --@pCustomerId: Customer Id      
	,@pYear INT = NULL --@pYear: Year      
	,@pMonth INT = NULL --@pMonth: Month      
	,@pStartDate DATETIME = NULL --@pStartDate: Start Date      
	,@pEndDate DATETIME = NULL --@pEndDate: End Date      
	,@pUserId INT --@pUserId: User Id
--SELECT 
--	 @pCustomerId   = 59
--	,@pYear			= 2020
--	,@pMonth		= 6
--	,@pStartDate	= null
--	,@pEndDate		= null
--	,@pUserId		= 45088
)
AS
BEGIN
	SET NOCOUNT ON

		DECLARE @lInsertDate DATE = CASE WHEN @pStartDate IS NULL 
									THEN DATEADD(YEAR, @pYear-1900, DATEADD(MONTH, @pMonth-1, DATEADD(day, 1-1, 0)))
								ELSE @pStartDate
								END

	DECLARE @lFinalDate DATE = CASE WHEN @pEndDate IS NULL 
									THEN DATEADD(YEAR, @pYear-1900, DATEADD(MONTH, (@pMonth + 1)-1, DATEADD(day, 1-1, 0)))
								ELSE DATEADD(DAY, 1, @pEndDate) 
								END


	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	-- DYNAMIC FILTER      
	DECLARE @Results TABLE (items INT)
	DECLARE @count INT
	DECLARE @lTimeZoneParameter INT = 0 
	DECLARE @lCustomerCapacityUnit INT
	DECLARE @lEquival DECIMAL(18,8) = 3.78541178


	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu 
	ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId

	INSERT @Results
	EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId

	SET @count = (
			SELECT COUNT(1)
			FROM @Results
			)

	-- END   

	SELECT @lCustomerCapacityUnit = c.[UnitOfCapacityId]
	FROM [General].[Customers] c
    WHERE c.[CustomerId] = @pCustomerId

	
	IF (@lCustomerCapacityUnit = 0) SET @lEquival = 1

	SELECT @lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
	INNER JOIN [General].[Customers] b ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId

	SELECT fd.[VehicleId]
	      ,MAX([Id]) [Id]
		  ,[FuelId]
	INTO #VehiclesDistributionIds 
	FROM [Control].[FuelDistribution] fd
	INNER JOIN [General].[Vehicles] v
		ON v.[VehicleId] = fd.[VehicleId]
	WHERE [CustomerId] = @pCustomerId
	AND fd.[InsertDate] >= @lInsertDate 
	AND fd.[InsertDate] < @lFinalDate
	GROUP BY fd.[VehicleId]
			,fd.[FuelId]
			,CONVERT(DATE, fd.[InsertDate])

	SELECT fd.[Amount]
	      ,fd.[AdditionalAmount]
		  ,fd.[Liters]
		  ,fd.[AdditionalLiters]
		  ,fd.[VehicleId]
		  ,fd.[FuelId]
	INTO #VehiclesDistribution
	FROM #VehiclesDistributionIds vd
	INNER JOIN [Control].[FuelDistribution] fd
		ON vd.[Id] = fd.[Id]

	SELECT v.[VehicleId] AS [VehicleId]
		  ,v.[Active]
		  ,v.[PlateId] AS [PlateNumber]
		  ,v.[Name] AS [VehicleName]
		  ,ISNULL(CONVERT(DECIMAL (16, 2), [Control].[Fn_FuelDistribution_TransactionsAmount]([pi].[Id]
																							, cc.[CreditCardId]
																							, @lTimeZoneParameter
																							, @lInsertDate
																							, @lFinalDate
																							, t.FuelId
																							 ), 2), 0) [RealAmount]  		    
		  ,(SELECT ISNULL(Convert(Decimal(16,2), SUM([Amount]), 2), 0) 
		    FROM #VehiclesDistribution vd
		    WHERE [VehicleId] = v.VehicleId
			AND vd.[FuelId] = t.[FuelId]) [AssignedAmount] 		       
		  ,CASE WHEN (SELECT Convert(Decimal(16,2), ISNULL(SUM([Amount]), 0) + ISNULL(SUM([AdditionalAmount]), 0), 2)
		  			FROM #VehiclesDistribution vdx
		  			WHERE [VehicleId] = v.[VehicleId]
					AND vdx.[FuelId] = t.[FuelId]) = 0
		   THEN 0
		   ELSE ISNULL(CONVERT(DECIMAL(16, 2), ROUND(ISNULL([Control].[Fn_FuelDistribution_TransactionsAmount]([pi].[Id],cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate,  t.FuelId), 0) / (SELECT Convert(Decimal(16,2), ISNULL(SUM([Amount]), 0) + ISNULL(SUM([AdditionalAmount]), 0), 2)
		  																																											   FROM #VehiclesDistribution
		  																																											   WHERE [VehicleId] = v.[VehicleId]), 2) * 100), 0) 
		  END [RealAmountPct]  
		  ,@lCurrencySymbol AS [CurrencySymbol]
		  ,v.CostCenterId
		  ,(SELECT ISNULL(Convert(Decimal(16,2), SUM([Liters]) / @lEquival, 2), 0) 
		    FROM #VehiclesDistribution vd
		    WHERE [VehicleId] = v.[VehicleId]
			AND vd.[FuelId] = t.[FuelId])  [AssignedLiters]  
		  ,cc.[CreditCardId]
		  ,@lTimeZoneParameter [TimeZoneParameter]
		  ,@lInsertDate [InsertDate]
		  ,@lFinalDate [FinalDate]
		  ,ISNULL(CONVERT(DECIMAL (16, 2), [Control].[Fn_FuelDistribution_TransactionsLiters]([pi].[Id], cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate, 0, t.FuelId), 2),0) [RealLiters]   
		  ,ISNULL(CONVERT(DECIMAL (16, 2), [Control].[Fn_FuelDistribution_TransactionsLiters]([pi].[Id], cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate, 1, t.FuelId), 2),0) [Liters]   
		  ,(SELECT ISNULL(Convert(Decimal(16,2), SUM([AdditionalAmount]), 2), 0) 
		    FROM #VehiclesDistribution 
		    WHERE [VehicleId] = v.[VehicleId]
			AND [FuelId] = t.[FuelId]) [AditionalAmount]      		
		  ,(SELECT ISNULL(Convert(Decimal(16,2), SUM([AdditionalLiters]) / @lEquival, 2), 0) 
		    FROM #VehiclesDistribution 
		    WHERE [VehicleId] = v.[VehicleId]
			AND [FuelId] = t.[FuelId]) [AditionalLiters]   		      
		  ,f.[Name] [FuelName]
		  ,(SELECT TOP 1 [VehicleGroupId]
		    FROM [General].[VehiclesByGroup] vbg 
		    WHERE v.VehicleId = vbg.VehicleId ) [VehicleGroupId]
			--AND [FuelId] = t.[FuelId]) [VehicleGroupId]
		  ,vcc.[UnitId]
		  ,(SELECT TOP 1 vg.[Name]
			FROM [General].[VehiclesByGroup] vbg
			LEFT JOIN [General].[VehicleGroups] vg 
				ON vg.VehicleGroupId = vbg.VehicleGroupId
			WHERE v.VehicleId = vbg.VehicleId) [VehicleGroupName]
			--AND [FuelId] = t.[FuelId]) [VehicleGroupName]
		  ,cc.[CreditAvailable] [AvailableBalance]
		  ,CONVERT(DECIMAL(18,3),(cc.[AvailableLiters] / @lEquival)) [AvailableLiters]
		  ,@lCustomerCapacityUnit [CustomerCapacityUnit]
		  ,cc.CreditCardId
	FROM [General].[Vehicles] v
	INNER JOIN [General].[VehicleCategories] vc
		ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
	LEFT JOIN [Control].[CreditCardByVehicle] ccv
		ON ccv.[VehicleId] = v.[VehicleId]
	LEFT JOIN [Control].[CreditCard] cc
		ON cc.[CreditCardId] = ccv.[CreditCardId]
			AND cc.[StatusId] IN (7, 8)
	LEFT JOIN [Control].[PaymentInstrumentsByType] pit
		ON pit.[VehicleId] = v.[VehicleId]
	LEFT JOIN [Control].[PaymentInstruments] [pi]
		ON [pi].[Id] = pit.[PaymentInstrumentId]
		   AND [pi].[StatusId] IN (7, 8)
	INNER JOIN [Control].[Transactions] t
		ON t.[VehicleId] = v.[VehicleId]
	LEFT JOIN [General].[VehicleCostCenters] vcc 
		ON v.CostCenterId = vcc.CostCenterId
	INNER JOIN [control].[fuels] f 
		ON f.FuelId = t.FuelId
	WHERE (
			@count = 0
			OR v.[VehicleId] IN (
				SELECT items
				FROM @Results
				)
			) -- DYNAMIC FILTER      
		AND v.[CustomerId] = @pCustomerId
		AND (
			v.isDeleted = 0
			OR v.isDeleted IS NULL
			)
		AND cc.CreditCardId IS NOT NULL
	GROUP BY t.[FuelId]
			,v.[VehicleId]
			,v.[Active]
			,v.[PlateId]
			,v.[Name]
			,cc.[CreditCardId]
			,[pi].[Id]
			,v.[CostCenterId]
			,f.[Name]
			,vcc.[UnitId]
			,cc.[CreditAvailable]
			,cc.[AvailableLiters]
			,cc.CreditCardId

			DROP TABLE #VehiclesDistributionIds
			DROP TABLE #VehiclesDistribution
	
	SET NOCOUNT OFF
END
go


--select * from [General].[VehicleCategories]