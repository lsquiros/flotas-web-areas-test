/****** Object:  StoredProcedure [General].[Sp_VehiclePerformanceValidate_Retrieve]    Script Date: 6/1/2020 2:14:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author     :	Henry Retana
-- Create date: 11/07/2018
-- Description:	Retrieve Vehicle Performance Validate
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- Stefano Quirós - Add UID - 2019
-- ================================================================================================

ALTER PROCEDURE [General].[Sp_VehiclePerformanceValidate_Retrieve]
(
	@pUID VARCHAR(200) = NULL,
	@pCreditCardNumber VARCHAR(200),
	@pOdometer BIGINT = NULL, 
	@pLiters DECIMAL(12, 2) = NULL
)   
AS	
BEGIN	
	SET NOCOUNT ON
	
	DECLARE @lDefaultPerformance DECIMAL(12, 2),
			@lMinPerformance DECIMAL(12, 2),
			@lMaxPerformance DECIMAL(12, 2),
			@lTransactionPerformance DECIMAL(12, 2),
			@lLastOdometer INT,
			@lTRName VARCHAR(100) = 'VehiclePerformance',
			@lCreditCardId INT,
			@lPaymentInstrumentId INT,
			@lTimeZoneParameter INT,
			@lCustomerId INT,
			@lParameters VARCHAR(50)
		
		IF @pUID IS NOT NULL
		BEGIN
			--GET THE CARD INFORMATION
			SELECT @lPaymentInstrumentId = [pi].[Id],
				   @lCustomerId = c.[CustomerId],
				   @lTimeZoneParameter = co.[TimeZone],
				   @lDefaultPerformance = [DefaultPerformance]
			FROM [Control].[PaymentInstruments] [pi] 
		    INNER JOIN [Control].[PaymentInstrumentsByType] pit
				ON pit.[PaymentInstrumentId] = [pi].[Id] 	
			INNER JOIN [General].[Customers] c 
				ON [pi].[CustomerId] = c.[CustomerId]	
			INNER JOIN [General].[Vehicles] v
				ON [pit].[VehicleId] = v.[VehicleId]
			INNER JOIN [General].[VehicleCategories] vc
				ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
			INNER JOIN [General].[Countries] co
				ON co.[CountryId] = c.[CountryId]
			INNER JOIN [Control].[Currencies] cur
				ON co.[Code] = cur.[Code]
			WHERE [pi].[Code] = @pUID
		END
		ELSE
		BEGIN
			--GET THE CARD INFORMATION
			SELECT @lCreditCardId = c.[CreditCardId],
				   @lCustomerId = c.[CustomerId],
				   @lTimeZoneParameter = co.[TimeZone],
				   @lDefaultPerformance = [DefaultPerformance]
			FROM [Control].[CreditCard] c 	
			INNER JOIN [General].[Customers] cu 
				ON c.[CustomerId] = cu.[CustomerId]	
			INNER JOIN [Control].[CreditCardByVehicle] cv
				ON c.[CreditCardId] = cv.[CreditCardId]
			INNER JOIN [General].[Vehicles] v
				ON cv.[VehicleId] = v.[VehicleId]
			INNER JOIN [General].[VehicleCategories] vc
				ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
			INNER JOIN [General].[Countries] co
				ON co.[CountryId] = cu.[CountryId]
			INNER JOIN [Control].[Currencies] cur
				ON co.[Code] = cur.[Code]
			WHERE c.[CreditCardNumber] = @pCreditCardNumber
		END
		
   
	--RETRIEVE THE PARAMETERS FOR THE TR
	SELECT TOP 1 @lParameters  = [Value]
	FROM [General].[ParametersByCustomer] 
	WHERE [Name] = @lTRName
	AND [CustomerId] = @lCustomerId
	AND [ResuourceKey] = 'TR_PARAMETERS'

	SELECT @lMinPerformance = CAST(SUBSTRING(@lParameters, 1, (CHARINDEX('-', @lParameters + '-') - 1)) AS DECIMAL(12, 2))
	SELECT @lMaxPerformance = CAST(SUBSTRING(@lParameters, CHARINDEX('-', @lParameters + '-') + 1, (CHARINDEX('-', @lParameters + '-'))) AS DECIMAL(12, 2))
		
	--GET THE DATA FROM THE TRANSACTIONS
	SELECT TOP 1 @lLastOdometer = t.[Odometer]
	FROM [Control].[Transactions] t	
	LEFT JOIN [Control].[CreditCard] cc 
		ON cc.[CreditCardId] = t.[CreditCardId]
		AND t.[CreditCardId] = @lCreditCardId
		AND cc.[CustomerId] = @lCustomerId
	LEFT JOIN [Control].[PaymentInstruments] [pi]
		ON [pi].[Id] = t.[PaymentInstrumentId]
		AND t.[PaymentInstrumentId] = @lPaymentInstrumentId
		AND [pi].[CustomerId] = @lCustomerId
	WHERE 1 > (
				SELECT ISNULL(COUNT(1), 0)
				FROM [Control].[Transactions] t2
				WHERE (t2.[CreditCardId] = t.[CreditCardId]
					   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
				AND t2.[TransactionPOS] = t.[TransactionPOS]
				AND t2.[ProcessorId] = t.[ProcessorId]
				AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)
			)
	AND (t.[TransactionOffline] IS NULL OR t.[TransactionOffline] = 0)
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)	
	ORDER BY t.[Date] DESC

	--GET THE PERFORMANCES
	SELECT  @lTransactionPerformance = (@pOdometer - @lLastOdometer) / @pLiters,
			@lMinPerformance = (@lDefaultPerformance - (@lDefaultPerformance * (@lMinPerformance * 0.01))),
			@lMaxPerformance = (@lDefaultPerformance + (@lDefaultPerformance * (@lMaxPerformance * 0.01)))

	--VALIDATES IF THE PERFORMANCES ARE IN THE RANGE
	IF @lTransactionPerformance >= @lMinPerformance
	AND @lTransactionPerformance <= @lMaxPerformance
	BEGIN 
		SELECT CAST(1 AS BIT) [Approved]
	END 
	ELSE
	BEGIN 
		SELECT CAST(0 AS BIT) [Approved],
			   @lTransactionPerformance [TransactionPerformance],
			   @lMinPerformance [MinPerformance],
			   @lMaxPerformance [MaxPerformance]			
	END 
	
    SET NOCOUNT OFF
END