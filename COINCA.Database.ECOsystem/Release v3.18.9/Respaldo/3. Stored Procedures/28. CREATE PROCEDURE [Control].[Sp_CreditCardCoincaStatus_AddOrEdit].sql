/****** Object:  StoredProcedure [Control].[Sp_CreditCardCoincaStatus_AddOrEdit]    Script Date: 28/05/2020 11:34:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author: �lvaro Zamora.
-- Create date: 10/07/2018
-- Description:	Change the sp's structure called Sp_CreditCard_AddOrEdit to receive an XML, the status of the credit card and the user modifier.
-- =====================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardCoincaStatus_AddOrEdit]
(
	@pXMLDoc VARCHAR(MAX)
	,@pStatusId INT
	,@pUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0                        
			DECLARE @lxmlData XML = CONVERT(XML,@pXMLDoc)

			SELECT 
				Row.col.value('./@Id', 'INT')[Id], 
				Row.col.value('./@PaymentType', 'INT')[PaymentType]
			INTO #PaymentList
			FROM @lxmlData.nodes('/xmldata/CreditCardId') Row(col)


			UPDATE [Control].[PaymentInstruments]
			SET    [StatusId] = COALESCE(@pStatusId,[StatusId])
				  ,[ModifyDate] = GETUTCDATE()
				  ,[ModifyUserId] = @pUserId			
			WHERE [Id] IN (SELECT [Id] FROM #PaymentList WHERE [PaymentType] <> 0)

			UPDATE [Control].[CreditCard]
			SET    [StatusId] = COALESCE(@pStatusId,[StatusId])
				  ,[ModifyDate] = GETUTCDATE()
				  ,[ModifyUserId] = @pUserId			
			WHERE [CreditCardId] IN (SELECT [Id] FROM #PaymentList WHERE [PaymentType] = 0)
			
    END TRY
    BEGIN CATCH
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

GO


