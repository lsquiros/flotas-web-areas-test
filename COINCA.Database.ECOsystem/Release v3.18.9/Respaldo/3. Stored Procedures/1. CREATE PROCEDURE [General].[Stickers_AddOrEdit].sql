USE [ECOSystem]
GO
IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Stickers_AddOrEdit' AND TYPE = 'P')
	DROP PROC [General].[Stickers_AddOrEdit]
GO
-- ===================================================
-- Author:		Stefano Quir�s
-- Create date: 13/05/2020
-- Description:	AddStickers to the Inventory
-- ===================================================
CREATE PROCEDURE [General].[Stickers_AddOrEdit] --1027, 1, NULL, NULL, '2020-05-11 23:15:01.640', '2020-05-12 23:15:01.640'
(
	@pUID VARCHAR(200)
   ,@pInsertUserId INT
)
AS
BEGIN	
	SET NOCOUNT ON
	IF NOT EXISTS(SELECT * FROM [Control].[PaymentInstruments] WHERE [Code] = @pUID)
	BEGIN
		INSERT INTO [Control].[PaymentInstruments]
		(
			[Code]
		   ,[TypeId]
		   ,[StatusId]
		   ,[Delete]
		   ,[InsertUserId]
		   ,[InsertDate]
		)
		VALUES
		(
			@pUID
		   ,(SELECT [Id] FROM [Control].[PaymentInstrumentsTypes] WHERE [Name] = 'Sticker')
		   ,0
		   ,0
		   ,@pInsertUserId
		   ,GETDATE()
		)
	END

	SELECT ISNULL(SCOPE_IDENTITY(), 0)

	SET NOCOUNT OFF
END