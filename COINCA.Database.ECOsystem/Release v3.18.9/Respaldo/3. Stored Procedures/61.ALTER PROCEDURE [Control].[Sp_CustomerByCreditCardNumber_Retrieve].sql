/****** Object:  StoredProcedure [Control].[Sp_CustomerByCreditCardNumber_Retrieve]    Script Date: 03/06/2020 7:28:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Kevin Peña
-- Create date: 07/23/2015
-- Description:	Retrieve CustomerID from CreditCardNumber_Transactions
-- Modify By: Stefano Quirós
-- Description: Retrieve the CreditCardId to insert in LogTransactionPOS
-- Modify By: Henry Retana 05/05/2017
-- Description: Retrieve parameters for transactions offline and send transactions
-- Modify By: Stefano Quirós - Update script and add the join when the customer has
-- the creditcard by Drivers
-- Modify By: Henry Retana 26/03/2018
-- Description: Retrieve plate and driver code
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_CustomerByCreditCardNumber_Retrieve] --'VZXPYiiQb720BeD+vrMALG+EpkaFyPk/', null
(
	 @pCreditCardNumber NVARCHAR(520) = null
	,@pUID VARCHAR(200) = null
)
AS
BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON	
	
	DECLARE @lCustomerId INT
	       ,@lPartnerId INT
		   ,@lCostCenterByDriver BIT = 0

	IF (@pCreditCardNumber IS NOT NULL)
	BEGIN
		--GET CUSTOMER ID 
		SELECT 	@lCustomerId = cc.[CustomerId]
		       ,@lPartnerId = cp.[PartnerId]
		FROM [General].[CustomersByPartner] cp
		INNER JOIN [Control].[CreditCard] cc
			ON cp.[CustomerId] = cc.[CustomerId]			   
		WHERE cc.[CreditCardNumber] = @pCreditCardNumber
	END
	ELSE
	BEGIN
		--GET CUSTOMER ID 
		SELECT 	@lCustomerId = pay.[CustomerId]
		       ,@lPartnerId = cp.[PartnerId]
		FROM [General].[CustomersByPartner] cp
		INNER JOIN [Control].[PaymentInstruments] pay
			ON cp.[CustomerId] = pay.[CustomerId] 
		WHERE pay.[Code] = @pUID
	END
    
	IF EXISTS(SELECT * 
	          FROM [Partners].[PartnerTransactionsOffline]
			  WHERE [PartnerId] = @lPartnerId
			  AND [EndDate] < GETDATE()
			  AND [Active] = 1
			  AND ([History] IS NULL
			       OR [History] = 0))
	BEGIN
		UPDATE [Partners].[PartnerTransactionsOffline]
		SET [Active] = 0
		WHERE [PartnerId] = @lPartnerId
		AND [Active] = 1
		AND ([History] IS NULL
			OR [History] = 0)
		
		--NOTIFIES PROCESS END
		EXEC [General].[Sp_SendAlarmsFromProcedures] @lPartnerId, 0 		
		
		--SEND REPORT
		EXEC [Control].[Sp_SendProgramReportForTransactionsOffline_AddOrEdit] @lPartnerId
	END
		
	--VALIDATES IF THE CUSTOMER IS SET TO HAVE COSTCENTER BY DRIVERS
	IF ISNULL((SELECT TOP 1 [Value] 
			   FROM [General].[ParametersByCustomer]
			   WHERE [CustomerId] = @lCustomerId
			   AND [ResuourceKey] = 'COSTCENTERBYDRIVER'), 0) = 1
	BEGIN
		SET @lCostCenterByDriver = 1
	END
	 
	--RETRIEVE INFORMATION
	SELECT cc.[CustomerId]
		  ,cc.[CreditCardId]
		  ,ccbv.[VehicleId]
		  ,CASE WHEN @lCostCenterByDriver = 1 
				THEN ISNULL((SELECT TOP 1 du.[CostCenterId]
   				   		     FROM [General].[DriversUsers] du
							 INNER JOIN [General].[VehiclesByUser] vbu
								ON du.[UserId] = vbu.[UserId] 
								AND vbu.[VehicleId] = v.[VehicleId]
							 WHERE vbu.[LastDateDriving] IS NULL), v.[CostCenterId])
				ELSE v.[CostCenterId] END [CostCenterId]
		  ,CAST(CASE WHEN (pto.[EndDate] IS NOT NULL 
					  AND pto.[EndDate] > GETDATE()
					  AND pto.[Active] = 1)
					THEN ISNULL(c.[TransactionsOffline], 0)
					ELSE 0  
				END AS BIT) [TransactionOffline]
		  ,CAST(CASE WHEN (pto.[EndDate] IS NOT NULL 
					  AND (pto.[EndDate] < GETDATE()
						   OR pto.[Active] = 0))
					THEN ISNULL(pto.[SendTransactions], 0)
					ELSE 0  
				END AS BIT) [SendTransactions]
		  ,v.[PlateId] [VehiclePlate]
		  ,du.[Code] [DriverCode]
		  ,u.[Name] [EncryptedDriverName]
	FROM [General].[Customers] c	
	LEFT JOIN [Control].[PaymentInstruments] pay
		ON c.[CustomerId] = pay.[CustomerId]
	LEFT JOIN [Control].[CreditCard] cc
		ON cc.[CustomerId] = c.[CustomerId] 
	LEFT OUTER JOIN [Control].[CreditCardByVehicle] ccbv
		ON cc.CreditCardId = ccbv.CreditCardId
	LEFT OUTER JOIN [Control].[CreditCardByDriver] ccbbd
		ON cc.CreditCardId = ccbbd.CreditCardId
	LEFT OUTER JOIN [General].[DriversUsers] du
		ON ccbbd.[UserId] = du.[UserId] 
	LEFT OUTER JOIN [General].[Users] u
		ON u.[UserId] = du.[UserId]		
		   OR du.[CustomerId] = c.[CustomerId]
	LEFT OUTER JOIN [General].[Vehicles] v
		ON ccbv.[VehicleId] = v.[VehicleId]	
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[CustomerId] = c.[CustomerId]
	LEFT JOIN [Partners].[PartnerTransactionsOffline] pto
		ON cp.[PartnerId] = pto.[PartnerId]
    WHERE ([History] IS NULL OR [History] = 0)
		   AND (pay.[Code] = @pUID 
			    OR [CreditCardNumber] = @pCreditCardNumber)

	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

