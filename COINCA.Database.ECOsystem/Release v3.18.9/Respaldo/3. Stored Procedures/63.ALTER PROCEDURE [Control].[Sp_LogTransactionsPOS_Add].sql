/****** Object:  StoredProcedure [Control].[Sp_LogTransactionsPOS_Add]    Script Date: 6/5/2020 11:39:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gerald Solano
-- Create date: 25/06/2015
-- Description:	Insert log de transaction.
-- Modify By:   Stefano Quirós
-- Description: Insert the CreditCardId in table LogTransactionPOS
-- Modify: 8/8/16 - Henry Retana
-- Add the data IsInternational and Country Code to the transaction records
-- Modify By: Stefano Quirós - Add the LTPId field - 09/01/2018
-- ================================================================================================

ALTER PROCEDURE [Control].[Sp_LogTransactionsPOS_Add]
(
		 @pCCTransactionPOSId INT = NULL
		,@pResponseCode  varchar(250) = NULL
		,@pResponseCodeDescription Varchar(MAX)= Null
		,@pMessage Varchar(MAX)= Null
		,@pTransportData Varchar(MAX)= Null
		,@pIsSuccess bit
		,@pIsFail bit
		,@pCustomerID INT
		,@pCreditCardId INT = NULL
		,@pPaymentInstrumentId INT = NULL
		,@pIsInternational BIT = NULL
		,@pCountryCode NVARCHAR(10) = NULL
		,@pLTPDId INT = NULL
)
AS
BEGIN
	--SET NOCOUNT ON
	--   SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lErrorNumber INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
			DECLARE @ExchangeValue DECIMAL(10, 2)
       
			--GET THE EXCHANGE VALUE
			IF @pIsInternational = 1
			BEGIN
				SET @ExchangeValue = (SELECT [Value] FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cr
											ON er.[CurrencyCode] = cr.[CurrencyCode]
										WHERE cr.[Code] = @pCountryCode
											AND er.[Until] IS NULL)
			END
				
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
				INSERT INTO [Control].[LogTransactionsPOS]
						([CCTransactionPOSId]
						,[ResponseCode]
						,[ResponseCodeDescription]
						,[Message]
						,[TransportData]
						,[IsSuccess]
						,[IsFail]
						,[InsertDate]
						,[ModifyDate]
						,[CustomerId]
						,[CreditCardId]
						,[PaymentInstrumentId]
						,[IsInternational]
						,[CountryCode]
						,[ExchangeValue]
						,[LTPDId]
						)
				VALUES	(@pCCTransactionPOSId
						,@pResponseCode
						,@pResponseCodeDescription
						,@pMessage
						,@pTransportData
						,@pIsSuccess
						,@pIsFail
						,GETUTCDATE()
						,GETUTCDATE()
						,@pCustomerID
						,@pCreditCardId
						,@pPaymentInstrumentId
						,@pIsInternational
						,@pCountryCode
						,@ExchangeValue
						,@pLTPDId
					)	
			SET @lRowCount = @@ROWCOUNT
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
				SELECT @@IDENTITY AS 'Identity';
			 END
			
			IF @lRowCount = 0
			BEGIN
				SELECT -1 AS 'Identity';
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
         END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END    
           
