/****** Object:  StoredProcedure [Control].[Sp_TransactionsInProcess_Retrieve]    Script Date: 5/31/2020 11:07:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Control].[Sp_TransactionsInProcess_Retrieve]
(
  @pCreditCardNumber nvarchar(520) = NULL
 ,@pUID VARCHAR(200) = NULL
)
AS
BEGIN
 
 SET NOCOUNT ON 

 DECLARE @lStatus INT 
 DECLARE @lValue BIT 

 SET @lValue = ISNULL((SELECT p.[Value]
        FROM [General].[ParametersByCustomer] p
        RIGHT JOIN [Control].[CreditCard] c
			ON p.[CustomerId] = c.[CustomerId] 
			AND c.[CreditCardNumber] = @pCreditCardNumber
		RIGHT JOIN [Control].[PaymentInstruments] [pi]
			ON [pi].[CustomerId] = p.[CustomerId]
			AND [pi].[Code] = @pUID
         WHERE p.[Name] = 'AllowProcess'
        AND p.[ResuourceKey] = 'ALLOWINPROCESSTRANSACTIONS'), 0)
 IF @lValue = 1 
 BEGIN 

 IF @pCreditCardNumber IS NOT NULL
 BEGIN
	SELECT @lStatus = [StatusId]
	FROM  [Control].[CreditCard]
	WHERE [CreditCardNumber] = @pCreditCardNumber
END
ELSE
BEGIN
	SELECT @lStatus = [StatusId]
	FROM  [Control].[PaymentInstruments]
	WHERE [Code] = @pUID
END

  IF @lStatus = 1 OR @lStatus = 5
   SELECT CAST(1 AS BIT)
  ELSE
   SELECT CAST(0 AS BIT)
 END   
 ELSE 
 BEGIN
  SELECT CAST(0 AS BIT)
 END  
 
    SET NOCOUNT OFF
END