/****** Object:  StoredProcedure [Control].[Sp_TransactionPriceByLiterValidation_Retrieve]    Script Date: 5/25/2020 1:02:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Danilo Hidalgo
-- Create date: 03/06/2015
-- Description:	Retrieve Validation Transaction Price by Liter
-- Modify: Henry Retana - 11/21/2016
-- Validates if the fuel price today is not valid and check if the next fuel price will be used
-- Modify: Henry Retana - 19/04/2016
-- Validates if the fuel price is from a service station
-- Modify: Henry Retana - 01/06/2016
-- Remove logic to automatic changes when there is an schedule price
-- Stefano Quirós - Add FuelId to the retrieve - 31/01/2020
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_TransactionPriceByLiterValidation_Retrieve] --37032, 8.025, 262.58, 'FBCN0255', 'A/K2ZethPU7CHZwaPthFZm4gwwQItXqn'
(
	@pVehicleId INT,
	@pLiters FLOAT,
	@pFuelAmount FLOAT,
	@pTerminalId VARCHAR(50),
	@pCreditCardNumber NVARCHAR(520) = NULL,
	@pUID VARCHAR(200) = NULL

)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lFuelId INT = NULL
	DECLARE @pLiterPrice DECIMAL(16, 2)
	DECLARE @pAlternativeLiterPrice DECIMAL
	DECLARE @lPartnerCapacityValue INT 
	DECLARE @lConvertionValue DECIMAL (16, 13) = 3.78541
	DECLARE @lFuelPermit BIT
	DECLARE @lCustomerId INT
	DECLARE @lDownErrorMargin DECIMAL(4, 2)
	DECLARE @lUpErrorMargin DECIMAL(4, 2)
	DECLARE @lValue VARCHAR(50)
	DECLARE @lTeoricalPrice DECIMAL(16, 2) = (@pFuelAmount / @pLiters)
	DECLARE @lTypeBudgetDistribution INT
	DECLARE @lTimeZoneParameter INT
	DECLARE @lClosingDate DATETIME
	DECLARE @lCapacityUnitId INT 
	DECLARE @lIssueForId INT
	DECLARE @lPaymentInstrumentId INT = (SELECT [Id]
										 FROM [Control].[PaymentInstruments]
										 WHERE [Code] = @pUID)
	DECLARE @lCreditCardId INT = (SELECT [CreditCardId] 
								  FROM [Control].[CreditCard]
								  WHERE [CreditCardNumber] = @pCreditCardNumber)



	SELECT @lPartnerCapacityValue = p.[CapacityUnitId]
	      ,@lFuelPermit = [FuelPermit]
		  ,@lCustomerId = c.[CustomerId]
		  ,@lTypeBudgetDistribution = ISNULL((SELECT [Value]
									   FROM [General].[ParametersByCustomer]
									   WHERE [CustomerId] = c.[CustomerId]
									   		 AND [Name] = 'TypeOfDistribution'
									   		 AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'), 0)
		  ,@lTimeZoneParameter = [TimeZone]
		  ,@lCapacityUnitId = c.[UnitOfCapacityId]
		  ,@lIssueForId = [IssueForId]
	FROM [General].[Partners] p
	INNER JOIN [General].[CustomersByPartner] cp
		ON p.[PartnerId] = cp.[PartnerId]
	INNER JOIN [General].[Vehicles] v
		ON v.[CustomerId] = cp.[CustomerId]
	INNER JOIN [General].[Customers] c
		ON c.[CustomerId] = v.[CustomerId]
	INNER JOIN [General].[Countries] coun
		ON coun.[CountryId] = c.[CountryId]
	WHERE v.[VehicleId] = @pVehicleId

	SELECT @lValue = [Value]
	FROM [General].[ParametersbyCustomer]
	WHERE [CustomerId] = @lCustomerId
		  AND [Name] = 'FuelsMargin'

	SELECT @lDownErrorMargin = CONVERT(DECIMAL(4,2), SUBSTRING(@lValue, 1, CHARINDEX('-', @lValue, 1) -1))/100
	      ,@lUpErrorMargin = CONVERT(DECIMAL(4,2), SUBSTRING(@lValue, CHARINDEX('-', @lValue, 1) +1, LEN(@lValue)))/100
		  ,@lClosingDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@lCustomerId)

	IF @lPartnerCapacityValue = 1
	BEGIN
		SET @lTeoricalPrice = @lTeoricalPrice / @lConvertionValue
	END

	
	CREATE TABLE #Fuels
	(
		[FuelId] INT
	)

	CREATE TABLE #Result
	(
		[FuelId] INT
	   ,[LiterPrice] DECIMAL(16, 2)
	   ,[MarginDown] DECIMAL(16, 2)
	   ,[MarginTop] DECIMAL(16, 2)
	   ,[Diff] INT
	)

		
	IF @lFuelPermit = 1
	BEGIN
		INSERT INTO #Fuels
		SELECT vc.[DefaultFuelId]
		FROM [General].[Vehicles] v	
		INNER JOIN [General].[VehicleCategories] vc 
			ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
		WHERE v.[VehicleId] = @pVehicleId 
		UNION
		SELECT fvc.[FuelId]
		FROM [General].[Vehicles] v
		INNER JOIN [General].[VehicleCategories] vc
			ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
		INNER JOIN [General].[FuelsByVehicleCategory] fvc
			ON fvc.[VehicleCategoryId] = vc.[VehicleCategoryId]
		WHERE v.[VehicleId] = @pVehicleId
	END
	ELSE
	BEGIN
		INSERT INTO #Fuels
		SELECT vc.[DefaultFuelId]
		FROM [General].[Vehicles] v	
		INNER JOIN [General].[VehicleCategories] vc 
			ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
		WHERE v.[VehicleId] = @pVehicleId
	END

	IF (SELECT COUNT(1) FROM #Fuels) = 1
	BEGIN
		SET @lFuelId = (SELECT TOP 1 [FuelId] FROM #Fuels)
	END

	INSERT INTO #Result
	SELECT ISNULL(@lFuelId, [FuelId])
	      ,[Price] 
	      ,[Price] - ([Price] * @lDownErrorMargin) 
	      ,[Price] + ([Price] * @lUpErrorMargin) 
	      ,ABS(@lTeoricalPrice - [Price])
	FROM [Stations].[FuelPriceByServiceStations] fs			
	INNER JOIN [General].[ServiceStations] s
		ON s.[ServiceStationId] = fs.[ServiceStationId]
	INNER JOIN [General].[TerminalsByServiceStations] tss
		ON tss.[ServiceStationId] = s.[ServiceStationId]
	WHERE (
			(
			 @lFuelId IS NULL 
			 AND fs.[FuelId] IN (SELECT [FuelId] FROM #Fuels)
			 AND @lTeoricalPrice BETWEEN [Price] - ([Price] * @lDownErrorMargin) AND [Price] + ([Price] * @lUpErrorMargin)
			)
		  OR fs.[FuelId] = @lFuelId
		  )
		  AND fs.[EndDate] IS NULL
		  AND fs.[Price] > 0
		  AND tss.[TerminalId] = @pTerminalId	

	SELECT @pLiterPrice = [LiterPrice]
		  ,@lFuelId = [FuelId]
	FROM #Result
	WHERE [Diff] = (SELECT MIN([Diff])
					FROM #Result)

	--VALIDATES IF THE SERVICE STATION HAS FUEL PRICE
	IF @pLiterPrice IS NOT NULL AND @pLiterPrice > 0 
	BEGIN 
		SELECT ISNULL(SUM([reg]), 0)  [reg]
		      ,[DefaultFuelId]
			  ,MAX([TypeBudgetDistribution]) [TypeBudgetDistribution]
			  ,MAX([CreditAvailable]) [CreditAvailable]
			  ,CONVERT(DECIMAL(16,2), MAX([AvailableLiters])) [AvailableLiters]
			  ,[FuelPermit]	
			  ,MAX([CapacityUnitId]) [CapacityUnitId]
			  ,(SELECT [Name] 
				FROM [Control].[Fuels] 
				WHERE [FuelId] = [DefaultFuelId]) [FuelName]
		FROM 
		(
			SELECT TOP 1 1 [reg]
			              ,@lFuelId [DefaultFuelId]
						  ,@lTypeBudgetDistribution [TypeBudgetDistribution]
				          ,CASE WHEN @lIssueForId = 100 
						   THEN cc.[CreditAvailable]
						   ELSE (ISNULL(fd.[Amount], 0) + ISNULL(fd.[AdditionalAmount], 0)) - [Control].[Fn_FuelDistribution_TransactionsAmount](@lPaymentInstrumentId, @lCreditCardId, @lTimeZoneParameter, @lClosingDate, NULL, @lFuelId)
						   END [CreditAvailable]
				          ,CASE WHEN @lIssueForId = 100
						   THEN cc.[AvailableLiters]
						   ELSE (ISNULL(fd.[Liters], 0) + ISNULL(fd.[AdditionalLiters], 0)) - [Control].[Fn_FuelDistribution_TransactionsLiters](@lPaymentInstrumentId, @lCreditCardId, @lTimeZoneParameter, @lClosingDate, NULL, 1, @lFuelId) 
						   END [AvailableLiters]
						  ,@lFuelPermit [FuelPermit]
						  ,@lCapacityUnitId [CapacityUnitId]
			FROM [Control].[PartnerFuel] c
			INNER JOIN [General].[Partners] e 
				ON c.[PartnerId] = e.[PartnerId]
				AND c.[FuelId] = @lFuelId
			LEFT JOIN [Control].[FuelDistribution] fd
				ON fd.[VehicleId] = @pVehicleId
				AND fd.[FuelId] = @lFuelId
			LEFT JOIN [Control].[CreditCard] cc
				ON cc.[CreditCardId] = @lCreditCardId
			LEFT JOIN [Control].[PaymentInstruments] [pi]
				ON [pi].[Id] = @lPaymentInstrumentId

			WHERE ABS(((@lTeoricalPrice / NULLIF(@pLiterPrice, 0)) - 1) * 100) <= e.[FuelErrorPercent]
			ORDER BY fd.[InsertDate] DESC
		 ) a
		 GROUP BY [DefaultFuelId], [FuelPermit]

	END 
	ELSE 
	BEGIN
		INSERT INTO #Result
		SELECT ISNULL(@lFuelId, [FuelId])
					,[LiterPrice] 
					,[LiterPrice] - ([LiterPrice] * @lDownErrorMargin) 
					,[LiterPrice] + ([LiterPrice] * @lUpErrorMargin) 
					,ABS(@lTeoricalPrice - [LiterPrice]) 
		FROM [General].[Vehicles] a
		INNER JOIN [General].[CustomersByPartner] b 
			ON a.[CustomerId] = b.[CustomerId]
		INNER JOIN [Control].[PartnerFuel] c 
			ON b.[PartnerId] = c.[PartnerId]		
		WHERE a.[VehicleId] = @pVehicleId
			  AND 
			  (
			  	(
			  	 @lFuelId IS NULL 
			  	 AND c.[FuelId] IN (SELECT [FuelId] FROM #Fuels)
			  	 AND @lTeoricalPrice BETWEEN [LiterPrice] - ([LiterPrice] * @lDownErrorMargin) AND [LiterPrice] + ([LiterPrice] * @lUpErrorMargin)
			  	)
			    OR c.[FuelId] = @lFuelId
			  )
			  AND b.[IsDefault] = 1 
			  AND c.[EndDate] >= DATEADD(HOUR, -6, GETUTCDATE())
			  AND c.[History] IS NULL
			  AND c.[ScheduledId] IS NULL		

		SELECT @pLiterPrice = [LiterPrice]
			  ,@lFuelId = [FuelId]
		FROM #Result
		WHERE [Diff] = (SELECT MIN([Diff])
					    FROM #Result)

		SELECT ISNULL(SUM([reg]), 0) [reg]
		      ,[DefaultFuelId]	
			  ,MAX([TypeBudgetDistribution]) [TypeBudgetDistribution]
			  ,MAX([CreditAvailable]) [CreditAvailable]
			  ,CONVERT(DECIMAL(16,2), MAX([AvailableLiters])) [AvailableLiters]
			  ,[FuelPermit]
			  ,MAX([CapacityUnitId]) [CapacityUnitId]
			  ,(SELECT [Name] 
				FROM [Control].[Fuels] 
				WHERE [FuelId] = [DefaultFuelId]) [FuelName]
		FROM 
		(
			SELECT TOP 1 1 [reg]
						  ,@lFuelId [DefaultFuelId]						  
						  ,@lTypeBudgetDistribution [TypeBudgetDistribution]
				          ,CASE WHEN @lIssueForId = 100 
						   THEN cc.[CreditAvailable]
						   ELSE (ISNULL(fd.[Amount], 0) + ISNULL(fd.[AdditionalAmount], 0)) - [Control].[Fn_FuelDistribution_TransactionsAmount](@lPaymentInstrumentId, @lCreditCardId, @lTimeZoneParameter, @lClosingDate, NULL, @lFuelId)
						   END [CreditAvailable]
				          ,CASE WHEN @lIssueForId = 100
						   THEN cc.[AvailableLiters]
						   ELSE (ISNULL(fd.[Liters], 0) + ISNULL(fd.[AdditionalLiters], 0)) - [Control].[Fn_FuelDistribution_TransactionsLiters](@lPaymentInstrumentId, @lCreditCardId, @lTimeZoneParameter, @lClosingDate, NULL, 1, @lFuelId) 
						   END [AvailableLiters]
						  ,@lFuelPermit [FuelPermit]
						  ,@lCapacityUnitId [CapacityUnitId]
			FROM [General].[Vehicles] a
				INNER JOIN [General].[CustomersByPartner] b 
					ON a.[CustomerId] = b.[CustomerId]
				INNER JOIN [Control].[PartnerFuel] c 
					ON b.[PartnerId] = c.[PartnerId]
				INNER JOIN [General].[Partners] e 
					ON b.[PartnerId] = e.[PartnerId]
					AND c.[FuelId] = @lFuelId
				LEFT JOIN [Control].[FuelDistribution] fd
					ON fd.[VehicleId] = a.[VehicleId]
					AND fd.[FuelId] = @lFuelId
				LEFT JOIN [Control].[CreditCard] cc
					ON cc.[CreditCardId] = @lCreditCardId
				LEFT JOIN [Control].[PaymentInstruments] [pi]
					ON [pi].[Id] = @lPaymentInstrumentId
			WHERE a.[VehicleId] = @pVehicleId 				
				AND ABS(((@lTeoricalPrice / NULLIF(@pLiterPrice, 0)) - 1) * 100) <= e.[FuelErrorPercent]	
			ORDER BY fd.[InsertDate] DESC		
		) a
		GROUP BY [DefaultFuelId], [FuelPermit]


	END

	DROP TABLE #Fuels
	DROP TABLE #Result
	
	SET NOCOUNT OFF
END

