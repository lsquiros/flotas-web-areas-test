/****** Object:  StoPredProcedure [General].[Sp_GetOdometerLastByVehicle]    Script Date: 6/1/2020 2:05:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================================================
-- Author:		Henry Retana
-- Create date: 06/04/2016
-- Description:	Retrieve the Last Odometer by Vehicle
-- Modify Stefano Quiros 28/6/2016 Validates if the customer has no odometers returns 0
-- Modify Gerald Solano 21/12/2016 Se restructuró el código para discriminar las transacciones reversadas
-- Modify Stefano Quirós - 7/01/2019 - Modify the consult and add the filter [IsFloating]
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- =============================================================================================================

ALTER PROCEDURE [General].[Sp_GetOdometerLastByVehicle]
(	 
	 @pVehicleId INT,
	 @pDate DATETIME
)
AS
BEGIN
	
  SET NOCOUNT ON
	
	--DECLARE @pVehicleId INT = 18220
	--DECLARE @pDate DATETIME = GETDATE()

	DECLARE @Odometer INT = 0
	
	SELECT TOP 1 @Odometer = [Odometer]
	FROM [Control].[Transactions] t
	WHERE [VehicleId] = @pVehicleId
	AND 1 > 
	(
		SELECT ISNULL(COUNT(1), 0)
		FROM CONTROL.Transactions t2
		WHERE (t2.[CreditCardId] = t.[CreditCardId]
			   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
		AND t2.[TransactionPOS] = t.[TransactionPOS]
		AND t2.[ProcessorId] = t.[ProcessorId]
		AND 
		(
			t2.[IsReversed] = 1
			OR t2.[IsVoid] = 1
		)
	)
	AND [IsFloating] = 0
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
	ORDER BY [InsertDate] DESC	
	
	SELECT @Odometer AS [OdometerLast] 
	
  SET NOCOUNT OFF
  
END
