/****** Object:  StoredProcedure [Control].[Sp_CustomerByUID_Retrieve]    Script Date: 5/29/2020 2:54:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Stefano Quirós
-- Create date: 07/31/2019
-- Description:	Retrieve CustomerID from UID
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_CustomerByUID_Retrieve]
(
	 @pUID VARCHAR(200)
)
AS
BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON	
	
	DECLARE @lCustomerId INT
	       ,@lPartnerId INT
		   ,@lCostCenterByDriver BIT = 0

	--GET CUSTOMER ID 
	SELECT 	@lCustomerId = [pi].[CustomerId]
	       ,@lPartnerId = cp.[PartnerId]
	FROM [Control].[PaymentInstruments] [pi]
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[CustomerId] = [pi].[CustomerId]
	WHERE   [pi].[Code] = @pUID
    
	IF EXISTS(SELECT * 
	          FROM [Partners].[PartnerTransactionsOffline]
			  WHERE [PartnerId] = @lPartnerId
			  AND [EndDate] < GETDATE()
			  AND [Active] = 1
			  AND ([History] IS NULL
			       OR [History] = 0))
	BEGIN
		UPDATE [Partners].[PartnerTransactionsOffline]
		SET [Active] = 0
		WHERE [PartnerId] = @lPartnerId
		AND [Active] = 1
		AND ([History] IS NULL
			OR [History] = 0)
		
		--NOTIFIES PROCESS END
		EXEC [General].[Sp_SendAlarmsFromProcedures] @lPartnerId, 0 		
		
		--SEND REPORT
		EXEC [Control].[Sp_SendProgramReportForTransactionsOffline_AddOrEdit] @lPartnerId
	END
		
	--VALIDATES IF THE CUSTOMER IS SET TO HAVE COSTCENTER BY DRIVERS
	IF ISNULL((SELECT TOP 1 [Value] 
			   FROM [General].[ParametersByCustomer]
			   WHERE [CustomerId] = @lCustomerId
			   AND [ResuourceKey] = 'COSTCENTERBYDRIVER'), 0) = 1
	BEGIN
		SET @lCostCenterByDriver = 1
	END
	 
	--RETRIEVE INFORMATION
	SELECT c.[CustomerId]
		  ,pit.[PaymentInstrumentId]--picc.[CreditCardId]
		  ,v.[VehicleId]
		  ,CASE WHEN @lCostCenterByDriver = 1 
				THEN ISNULL(du.[CostCenterId], v.[CostCenterId])
				ELSE v.[CostCenterId] END [CostCenterId]
		  ,CAST(CASE WHEN (pto.[EndDate] IS NOT NULL 
					  AND pto.[EndDate] > GETDATE()
					  AND pto.[Active] = 1)
					THEN ISNULL(c.[TransactionsOffline], 0)
					ELSE 0  
				END AS BIT) [TransactionOffline]
		  ,CAST(CASE WHEN (pto.[EndDate] IS NOT NULL 
					  AND (pto.[EndDate] < GETDATE()
						   OR pto.[Active] = 0))
					THEN ISNULL(pto.[SendTransactions], 0)
					ELSE 0  
				END AS BIT) [SendTransactions]
		  ,v.[PlateId] [VehiclePlate]
		  ,du.[Code] [DriverCode]
		  ,u.[Name] [EncryptedDriverName]
	FROM [Control].[PaymentInstruments] [pi]
	INNER JOIN [Control].[PaymentInstrumentsByType] pit
		ON pit.[PaymentInstrumentId] = [pi].[Id]
	INNER JOIN [General].[Vehicles] v
		ON v.[VehicleId] = pit.[VehicleId]	
	LEFT JOIN [General].[VehiclesByUser] vbu
		ON vbu.[VehicleId] = v.[VehicleId]
		   AND vbu.[LastDateDriving] IS NULL
	LEFT JOIN [General].[DriversUsers] du
		ON du.[UserId] = vbu.[UserId]
	LEFT JOIN [General].[Users] u
		ON u.[UserId] = du.[UserId]	
	INNER JOIN [General].[Customers] c
		ON [pi].[CustomerId] = c.[CustomerId] 
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[CustomerId] = c.[CustomerId]
	LEFT JOIN [Partners].[PartnerTransactionsOffline] pto
		ON cp.[PartnerId] = pto.[PartnerId]
		AND ([History] IS NULL OR [History] = 0)
    WHERE [pi].[Code] = @pUID
	 

	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

