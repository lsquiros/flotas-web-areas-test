USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AdditionalVehicleFuelLowPorcentage_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_AdditionalVehicleFuelLowPorcentage_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================    
-- Author:  Danilo Hidalgo.    
-- Create date: 04/13/2016    
-- Description: Retrieve Additional Vehicle Fuel information    
-- Modify By:   Stefano Quirós Ruiz    
-- Description: Top 200 of the consult and add new column on the retrieve AvailableFuel - 12/01/2016    
-- Change balance liters calculate    
-- Henry Retana 20/06/2017    
-- Add the customer alarm parameter for the customer    
-- Esteban Solis 2017-10-13  
-- Remove check for amount on fuel table  
-- Modify by Stefano Quirós - 14/11/2017 - Add new logic to retrieve last monthly Closing   
-- and Add Void Transactions Validation  
-- Modify By: Esteban Solís  - 11/12/2017  - Added [General].[Fn_LatestMonthlyClosing_Retrieve] to get last closing date
-- Modify by Esteban Solís - 11/12/2017 - Fix issue with insert date on transactions
-- Modify: 28/02/2018 - Henry Retana - Validates Gallons and Liters
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- Modify by Marjorie Garbanzo - 01/06/2020 Add logic to include transactions with different payment methods
-- ================================================================================================    
    
CREATE PROCEDURE [Control].[Sp_AdditionalVehicleFuelLowPorcentage_Retrieve]
(    	 
	  @pCustomerId INT = NULL
	 ,@pUserId INT = NULL
)    
AS    
BEGIN 
	SET NOCOUNT ON    
    
	-- DYNAMIC FILTER    
	DECLARE @Results TABLE (items INT)    
	DECLARE @count INT    
	INSERT @Results    
	EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId   
	SET  @count = (SELECT COUNT(*) FROM @Results)    
	-- END    
		
	DECLARE @pPorcent INT = NULL 
		    ,@lInsertDate DATETIME
			,@pYear INT = NULL
			,@pMonth INT = NULL
			,@lFuelByCreditId INT   

	--GET THE PARAMETER BASED ON THE CUSTOMER INFORMATION    
	SELECT @pPorcent = ISNULL([ParameterValue], @pPorcent)    
	FROM [General].[AlertsByCustomer]    
	WHERE [AlertId] = 1     
	AND [CustomerId] = @pCustomerId    
    
	DECLARE @tableFuelsTemp TABLE 
	(    
		 [AdditionalFuelByCreditId] INT NULL
		,[FuelId] INT NULL    
		,[Year] INT NULL  
		,[Month] INT NULL    
		,[Total] DECIMAL(16,2) NULL
		,[Assigned] DECIMAL(18, 0) NULL
		,[Available] DECIMAL(21, 2) NULL    
		,[FuelName] VARCHAR(50)
		,[AssignedLiters] DECIMAL(16,3)
		,[Liters] DECIMAL(16,3)
		,[LiterPrice]  DECIMAL (16,2)
		,[CurrencySymbol] NVARCHAR(4)
		,[RowVersion] VARCHAR(1000)
		,[UnitOfCapacityId] INT
		,[UnitOfCapacity] VARCHAR(100)    
	)    
   
	SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)
	SET @pYear = DATEPART(YEAR, @lInsertDate)
	SET @pMonth = DATEPART(MONTH, @lInsertDate)
		 
    
	INSERT INTO @tableFuelsTemp    
	EXEC [Control].[Sp_FuelsByCredit_Retrieve] @pCustomerId, @pYear, @pMonth
    
	DECLARE @lCustomerCapacityUnit INT     
	DECLARE @lEquival DECIMAL(18,8) = 3.78541178    
             
	SELECT @lCustomerCapacityUnit = [UnitOfCapacityId] 
	FROM [General].[Customers] 
	WHERE [CustomerId] = @pCustomerId    

	DECLARE @lTimeZoneParameter INT 
	DECLARE @PartnerCapacityUnit INT
		
	SELECT @PartnerCapacityUnit = p.[CapacityUnitId],
		   @lCustomerCapacityUnit = c.[UnitOfCapacityId],
		   @lTimeZoneParameter = [TimeZone]
	FROM [General].[Partners] p
	INNER JOIN [General].[CustomersByPartner] cbp
		ON p.[PartnerId] = cbp.[PartnerId]
	INNER JOIN [General].[Customers] c
		ON cbp.[CustomerId] = c.[CustomerId]
	INNER JOIN [General].[Countries] co
		ON co.[CountryId] = c.[CountryId]
	WHERE cbp.[CustomerId] = @pCustomerId	
	
	IF (@lCustomerCapacityUnit = 0) SET @lEquival = 1
    
	SELECT TOP 201   a.[Id]    
					,a.[VehicleId]    
					,a.[Month]    
					,a.[Year]      
					,a.[Liters]    
					,a.[Amount]    
					,ISNULL(a.[AdditionalLiters],0) AS [AdditionalLiters]    
					,ISNULL(a.[AdditionalAmount], CONVERT(DECIMAL(16,2),0)) AS [AdditionalAmount]    
					,ISNULL(t.[RealLiters], 0) [RealLiters]    
					,ISNULL(t.[RealAmount], 0) [RealAmount]       
					,(ISNULL(a.[Amount],0) + ISNULL(a.[AdditionalAmount], CONVERT(DECIMAL(16,2),0))  - ISNULL(t.[RealAmount], 0)) / (SELECT TOP 1 tmp.[LiterPrice] * @lEquival 
																																		FROM @tableFuelsTemp tmp 
																																		WHERE tmp.[FuelId] = d.[FuelId]) [BalanceLiters]    
					,ISNULL(a.[Amount], 0) + ISNULL(a.[AdditionalAmount], 0) - ISNULL(t.[RealAmount], 0) [BalanceAmount]    
					,d.[Name] AS [FuelName]    
					,d.[FuelId]    
					,d.[FuelId] AS [DefaultFuelId]    
					,b.[PlateId]    
					,b.[Name] AS VehicleName    
					,c.[VehicleModel]    
					,f.[Symbol] AS [CurrencySymbol] 
					,ISNULL((SELECT Top 1 ISNULL(tmp.[LiterPrice] * @lEquival, 0) 
				  		   		    	FROM @tableFuelsTemp tmp 
				  		   		    	WHERE tmp.[FuelId] = d.[FuelId]), 0) [LiterPrice]
					,(SELECT TOP 1 tmp.[Available] 
						FROM @tableFuelsTemp tmp 
						WHERE tmp.[FuelId] = d.[FuelId] 
						AND tmp.[Year] = @pYear 
						AND tmp.[Month] = @pMonth)  As [AvailableFuel]        
	FROM [Control].[FuelDistribution] a    
	INNER JOIN [General].[Vehicles] b    
		ON a.[VehicleId] = b.[VehicleId]    
	INNER JOIN [General].[VehicleCategories] c    
		ON b.[VehicleCategoryId] = c.[VehicleCategoryId]    
	INNER JOIN [Control].[Fuels] d    
		ON c.[DefaultFuelId] = d.[FuelId]    
	INNER JOIN [General].[Customers] e    
		ON b.[CustomerId] = e.[CustomerId]    
	INNER JOIN [Control].[Currencies] f    
		ON e.[CurrencyId] = f.[CurrencyId]    
	LEFT JOIN ( 
				SELECT y.[VehicleId]    
						,SUM(x.[FuelAmount]) AS [RealAmount]    
						,SUM(x.[Liters]) AS [RealLiters]    
				FROM [Control].[Transactions] x    
				INNER JOIN [General].[Vehicles] y 
					ON x.[VehicleId] = y.[VehicleId]    
				WHERE y.[CustomerId] = @pCustomerId    
				AND x.[IsFloating] = 0    
				AND x.[IsReversed] = 0    
				AND x.[IsDuplicated] = 0    
				AND (x.[IsDenied] IS NULL OR x.[IsDenied] = 0)
				AND y.[Active] = 1    
				AND 1 > (    
							SELECT COUNT(*) 
							FROM [Control].[Transactions] t2    
							WHERE (t2.[CreditCardId] = x.[CreditCardId] 
									OR t2.[PaymentInstrumentId] = x.[PaymentInstrumentId])
							AND t2.[TransactionPOS] = x.[TransactionPOS] 
							AND t2.[ProcessorId] = x.[ProcessorId]
							AND (t2.IsReversed = 1 OR t2.IsVoid = 1)  
						)   
				AND CONVERT(DATE, DATEADD(HOUR, @lTimeZoneParameter, x.[InsertDate])) >= @lInsertDate   
				GROUP BY y.[VehicleId]
			)t    
			ON a.[VehicleId] = t.[VehicleId]    
	WHERE (@pCustomerId IS NULL OR b.[CustomerId] = @pCustomerId)    
	AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER  		
	AND CONVERT(DATETIME, a.[InsertDate]) >= @lInsertDate    
	AND a.[Amount] > 0
	AND CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / (a.[Amount] + ISNULL(a.[AdditionalAmount], 0)), 2) * 100) >= @pPorcent           
	ORDER BY [Id] DESC        
     
    SET NOCOUNT OFF    
END 

