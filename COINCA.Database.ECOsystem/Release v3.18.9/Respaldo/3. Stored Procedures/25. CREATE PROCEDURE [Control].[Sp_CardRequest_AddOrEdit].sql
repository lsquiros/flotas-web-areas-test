/****** Object:  StoredProcedure [Control].[Sp_CardRequest_AddOrEdit]    Script Date: 27/05/2020 10:04:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Author:  Berman Romero L.  
-- Create date: 09/11/2014  
-- Description: Insert or Update CustomerCreditCards information  
-- ================================================================================================
-- Modify: Henry Retana - 10/02/2017 - Add the DriverUserId for the card request   
-- Modify: Esteban Sol�s - 06/03/2018 - Add validation to prevent multiple card request to a vehicle/driver on insert
-- Modify: Maria de los Angeles Jimenez Chavarria - Jan/09/2018 - Create masive card requests
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_CardRequest_AddOrEdit] (
	@pCardRequestId INT = NULL
	,@pCustomerId INT
	,@pPlateId VARCHAR(10) = NULL
	,@pDriverName VARCHAR(250) = NULL
	,@pDriverIdentification VARCHAR(50) = NULL
	,@pStateId INT = NULL
	,@pCountyId INT = NULL
	,@pCityId INT = NULL
	,@pDeliveryState VARCHAR(50) = NULL
	,@pDeliveryCounty VARCHAR(50) = NULL
	,@pDeliveryCity VARCHAR(50) = NULL
	,@pAddressLine1 VARCHAR(1024)
	,@pAddressLine2 VARCHAR(1024) = NULL
	,@pPaymentReference VARCHAR(50) = NULL
	,@pAuthorizedPerson VARCHAR(520)
	,@pContactPhone VARCHAR(50)
	,@pEstimatedDelivery DATETIME = NULL
	,@pLoggedUserId INT
	,@pDriverUserId INT = NULL
	,@pRowVersion TIMESTAMP
	,@pPaymentMethod INT = NULL
	)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
		DECLARE @lErrorSeverity INT
		DECLARE @lErrorState INT
		DECLARE @lHasRequests BIT = 0
		DECLARE @lPlateId VARCHAR(10) = NULL
		DECLARE @lDriverUserId INT = NULL


		DECLARE @PlateIdTableTemp AS TABLE (
			[PlateId] VARCHAR(10),
			[Name] VARCHAR(10)
		)

		DECLARE @DriverUserIdTableTemp AS TABLE (
			[UserId] INT,
			[EncryptedName] VARCHAR(250)
		)

		IF @pPaymentMethod IS NULL
		BEGIN
			SET @pPaymentMethod = 0
		END

		IF @pPlateId IS NOT NULL 
		BEGIN
			IF @pPlateId = '-1'
			BEGIN
				INSERT INTO @PlateIdTableTemp
				EXEC ('[General].[Sp_VehiclesWithNoCreditCard_Retrieve] ' + @pCustomerId)
			END
			ELSE 
			BEGIN
				INSERT INTO @PlateIdTableTemp ([PlateId], [Name]) VALUES (@pPlateId, @pPlateId)
			END
		END

		IF @pPlateId IS NULL AND @pDriverUserId IS NOT NULL
		BEGIN
			IF @pDriverUserId = '-1'
			BEGIN
				INSERT INTO @DriverUserIdTableTemp
				EXEC ('[General].[Sp_DriversWithNoCreditCard_Retrieve] ' + @pCustomerId)
			END
			ELSE 
			BEGIN
				INSERT INTO @DriverUserIdTableTemp ([UserId], [EncryptedName]) VALUES (@pDriverUserId, @pDriverUserId)
			END
		END

		WHILE EXISTS(SELECT * FROM @PlateIdTableTemp) OR EXISTS(SELECT * FROM @DriverUserIdTableTemp) 
		BEGIN
			SELECT TOP 1 @lPlateId = [PlateId] FROM @PlateIdTableTemp
			SELECT TOP 1 @lDriverUserId = [UserId] FROM @DriverUserIdTableTemp

			DELETE FROM @PlateIdTableTemp WHERE [PlateId] = @lPlateId
			DELETE FROM @DriverUserIdTableTemp WHERE [UserId] = @lDriverUserId

			IF @lDriverUserId IS NOT NULL
			BEGIN
				SELECT @pDriverName = u.[Name]
					,@pDriverIdentification = du.[Identification]
				FROM [General].[Users] u
				INNER JOIN [General].[DriversUsers] du ON u.[UserId] = du.[UserId]
				WHERE du.[UserId] = @lDriverUserId
			END

			IF (@pCardRequestId IS NULL)
			BEGIN
				IF @lPlateId IS NOT NULL
				BEGIN
					SET @lHasRequests = [General].[Fn_HasRequestOrCreditCardActive](@pCustomerId, @lPlateId, NULL)
				END

				IF @lDriverUserId IS NOT NULL
				BEGIN
					SET @lHasRequests = [General].[Fn_HasRequestOrCreditCardActive](@pCustomerId, NULL, @lDriverUserId)
				END

				IF @lHasRequests = 0
				BEGIN
					INSERT INTO [Control].[CardRequest] (
						[CustomerId]
						,[PlateId]
						,[DriverName]
						,[DriverIdentification]
						,[StateId]
						,[CountyId]
						,[CityId]
						,[DeliveryState]
						,[DeliveryCounty]
						,[DeliveryCity]
						,[AddressLine1]
						,[AddressLine2]
						,[PaymentReference]
						,[AuthorizedPerson]
						,[ContactPhone]
						,[EstimatedDelivery]
						,[InsertDate]
						,[InsertUserId]
						,[DriverUserId]
						,[PaymentMethod]
						)
					VALUES (
						@pCustomerId
						,@lPlateId
						,@pDriverName
						,@pDriverIdentification
						,@pStateId
						,@pCountyId
						,@pCityId
						,@pDeliveryState
						,@pDeliveryCounty
						,@pDeliveryCity
						,@pAddressLine1
						,@pAddressLine2
						,@pPaymentReference
						,@pAuthorizedPerson
						,@pContactPhone
						,@pEstimatedDelivery
						,GETUTCDATE()
						,@pLoggedUserId
						,@lDriverUserId
						,@pPaymentMethod
						)
					IF @pPaymentMethod > 0
					BEGIN
						INSERT INTO [Control].[PaymentInstrumentsByType]
						VALUES(
							null,
							SCOPE_IDENTITY(),
							(SELECT [VehicleId] FROM [General].[Vehicles] WHERE [PlateId] = @lPlateId),
							@pLoggedUserId,
							GETDATE()
						)
					END
				END
				ELSE
				BEGIN
					RAISERROR (
							'Error updating database row, Please try again. There is a previous request or an active credit card for this vehicle/driver'
							,16
							,1
							)
				END
			
			END
			ELSE
			BEGIN
				UPDATE [Control].[CardRequest]
				SET [PlateId] = @lPlateId
					,[DriverName] = @pDriverName
					,[DriverIdentification] = @pDriverIdentification
					,[StateId] = @pStateId
					,[CountyId] = @pCountyId
					,[CityId] = @pCityId
					,[DeliveryState] = @pDeliveryState
					,[DeliveryCounty] = @pDeliveryCounty
					,[DeliveryCity] = @pDeliveryCity
					,[AddressLine1] = @pAddressLine1
					,[AddressLine2] = @pAddressLine2
					,[PaymentReference] = @pPaymentReference
					,[AuthorizedPerson] = @pAuthorizedPerson
					,[ContactPhone] = @pContactPhone
					,[EstimatedDelivery] = @pEstimatedDelivery
					,[ModifyDate] = GETUTCDATE()
					,[ModifyUserId] = @pLoggedUserId
					,[DriverUserId] = @lDriverUserId
				WHERE [CardRequestId] = @pCardRequestId
					AND [RowVersion] = @pRowVersion
			END

		END
	END TRY

	BEGIN CATCH
		IF (@@TRANCOUNT > 0 AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END

		SELECT @lErrorMessage = ERROR_MESSAGE()
			,@lErrorSeverity = ERROR_SEVERITY()
			,@lErrorState = ERROR_STATE()

		RAISERROR (@lErrorMessage ,@lErrorSeverity ,@lErrorState)
	END CATCH

	SET NOCOUNT OFF
	SET XACT_ABORT OFF
END
GO


