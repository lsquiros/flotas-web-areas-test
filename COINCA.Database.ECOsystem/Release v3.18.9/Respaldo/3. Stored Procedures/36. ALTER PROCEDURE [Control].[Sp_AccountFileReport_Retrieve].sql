USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_AccountFileReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_AccountFileReport_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 11/08/2017
-- Description:	Retrieve Transactions SAP File Report information
-- Modify 09/28/2017 - Esteban Solis - New table struction ServiceStationCustomName
-- Modify by Stefano Quirós - 14/11/2017 - Add Void Transactions Validation
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- Modify by Marjorie Garbanzo - 01/06/2020 Add logic to include transactions with different payment methods
-- ================================================================================================

CREATE PROCEDURE [Control].[Sp_AccountFileReport_Retrieve]
(	
	@pMonth INT = NULL,
	@pYear INT = NULL,	
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL,
	@pCustomerId INT,	
	@pUserId INT 
)
AS
BEGIN

	SET NOCOUNT ON	
	
	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	DECLARE @lTimeZoneParameter INT

	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
	ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	-- TABLE FOR RETURN FINAL RESULTS
	DECLARE @TmpResults TABLE (
		TransactionDate DATETIME,
		VehicleName VARCHAR(250),
		CostCenter VARCHAR(250),
		Odometers INT,
		Liters DECIMAL(16,3),
		Amount DECIMAL(16,2),
		Fuel VARCHAR(50),
		ProcessorId VARCHAR(250),
		MerchantDescription VARCHAR(200),
		Number_Provider VARCHAR(20),
		Number_Provider2 VARCHAR(20),
		SAPProv VARCHAR(20)
	)

	INSERT INTO @TmpResults
	SELECT DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [TransactionDate],
		   v.[Name] AS [VehicleName],
		   e.[Name] AS [CostCenter],
		   t.[Odometer] AS [Odometers],			
		   t.[Liters],		
		   t.[FuelAmount] AS [Amount],
		   f.[Name] AS [Fuel],
		   t.[ProcessorId],
		   t.[MerchantDescription],
		   CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 cs.[SAPProvider]
		   								  FROM [General].[ServiceStationsCustomName] cs 
		   								  WHERE [ServiceStationId] = ss.[ServiceStationId]
		   									AND [InsertUserId] = @pCustomerId), ss.[SAPProv])) AS [Number_Provider] 
		   ,CONVERT(VARCHAR(100),(SELECT TOP 1 ssc.[SAPProvider] 
		   						   FROM [General].[ServiceStationsCustomName] ssc 
		   						   WHERE ssc.[ServiceStationId] = (SELECT top 1 ss3.[ServiceStationId] 
		   															FROM [General].[ServiceStations] ss3 
		   															WHERE ss3.[ServiceStationId] = tss.[ServiceStationId])
		   						   AND [InsertUserId] = @pCustomerId))	AS [Number_Provider2]
		   ,CONVERT(VARCHAR(100),(SELECT TOP 1 ss2.[SAPProv] 
		   						   FROM [General].[ServiceStations] ss2 
		   						   WHERE ss2.[ServiceStationId] = tss.[ServiceStationId])) AS [SAPProv]			   	
	FROM [Control].[Transactions] t
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]
	INNER JOIN [General].[VehicleCategories] vc
		ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]
	INNER JOIN [Control].[Fuels] f 
		ON t.[FuelId] = f.[FuelId]		
	INNER JOIN [General].[VehicleCostCenters] e
	    ON t.CostCenterId = e.CostCenterId	
	LEFT OUTER JOIN [General].[ServiceStations] ss
		ON t.[ProcessorId] = ss.[Terminal]
	LEFT OUTER JOIN [General].[TerminalsByServiceStations] tss
		ON t.[ProcessorId] = tss.[TerminalId]	
	WHERE (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	AND c.[CustomerId] = @pCustomerId 
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
	AND 1 > (SELECT ISNULL(COUNT(1), 0) 
			 FROM [Control].[Transactions] t2
			 WHERE (t2.[CreditCardId] = t.[CreditCardId] 
					OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
			 AND t2.[TransactionPOS] = t.[TransactionPOS] 
			 AND t2.[ProcessorId] = t.[ProcessorId] 
			 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))		
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear) OR
			(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
			AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
	ORDER BY t.[Date] DESC
	
	-- return results
	SELECT [TransactionDate],
		   [VehicleName],
		   [CostCenter],
		   [Odometers],
		   [Liters],
		   [Amount],
		   [Fuel],
		   --[ProcessorId],
		   ISNULL(Number_Provider, ISNULL(Number_Provider2,SAPProv)) AS [ServiceStationId]
	FROM @TmpResults

	SET NOCOUNT OFF
END


