/****** Object:  StoredProcedure [Control].[Sp_SetTransactionReverse_AddOrEdit]    Script Date: 6/1/2020 3:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 26/08/2016
-- Description:	Set the transaction REVERSE
-- Stefano Quirós - Add the liters to restart or add to the available - 14/01/2019
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_SetTransactionReverse_AddOrEdit]
(
	@pTransactionId INT,
	@pTotalAmount DECIMAL(16,2)
)
AS
BEGIN

	SET NOCOUNT ON	
	
	BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lErrorNumber INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
			DECLARE @Reverse INT = 1
			DECLARE @lLiters DECIMAL(16, 3)

			DECLARE @CreditCardId INT
			DECLARE @lPaymentInstrumentId INT
			DECLARE @CAuthorizationNumber NVARCHAR(50)

			--GET THE CREDIT CARD ID FROM THE TRANSACTION
			SELECT @CreditCardId = [CreditCardId]
				  ,@lLiters = [Liters]
				  ,@lPaymentInstrumentId = [PaymentInstrumentId]
			FROM [Control].[Transactions]
			WHERE [TransactionId] = @pTransactionId
       
			--UPDATE THE TRANSACTION 
			UPDATE [Control].[Transactions]
			SET [IsFloating] = 0 
				,[IsReversed] = 1
			WHERE [TransactionId] = @pTransactionId

			SET @lRowCount = @@ROWCOUNT

			IF @lRowCount > 0
			BEGIN 
				Exec [Control].[Sp_CreditCard_CreditAvailable_Edit] @lPaymentInstrumentId, @CreditCardId, @pTotalAmount, @Reverse, @lLiters
			END			
			ELSE
			BEGIN
				RAISERROR ('Error al intentar reversar la transaccion. Por favor intentelo nuevamente.', 16, 1)
			END			

			--GET THE TRANSACTIONS FOR THE CREDIT CARD
			SELECT t.[TransactionId] [CCTransactionVPOSId]
				   ,t.[Date]		 [Date]
				   ,v.[PlateId]	     [Plate]   
				   ,t.[FuelAmount]   [TotalAmount]
				   ,t.[Liters]       [Liters]
				   ,t.[ProcessorId]  [TerminalId]
				   ,[General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0)  [ServiceStationName]
				   ,cu.[Symbol]       [CurrencySymbol]
			FROM [Control].[Transactions] t 			
			INNER JOIN [General].[Vehicles] v
				ON t.[VehicleId] = v.[VehicleId]
			INNER JOIN [General].[Customers] c 
				ON v.[CustomerId] = c.[CustomerId]
			INNER JOIN [Control].[Currencies] cu
				ON c.[CurrencyId] = cu.[CurrencyId]
			WHERE [IsFloating] = 1
				AND [AuthorizationNumber] IS NOT NULL
				AND (t.[CreditCardId] = @CreditCardId
				     OR t.[PaymentInstrumentId] = @lPaymentInstrumentId)
			ORDER BY t.[Date] DESC
			
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
				SELECT @@IDENTITY AS 'Identity';
			END
			
			IF @lRowCount = 0
			BEGIN
				SELECT -1 AS 'Identity';
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH

	SET NOCOUNT OFF
END
