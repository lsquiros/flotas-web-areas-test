USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_CreditCardStatus_AddOrEdit]    Script Date: 10/06/2020 1:27:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 14/02/17
-- Description:	Update the status Id for the credit card list
-- Modify - Henry Retana, 18/04/2017 - Add the credit card hist
-- Modify - Stefano Quir�s - Add the calculate of the creditcard when change the status - And add to
-- the calculate the available liters - 16/01/19
-- ================================================================================================

 ALTER PROCEDURE [Control].[Sp_CreditCardStatus_AddOrEdit]
(
	@pXMLDoc VARCHAR(MAX),
	@pStatusId INT,
	@pCustomerName VARCHAR(500),
	@pUserId INT
  
)
AS
BEGIN	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		BEGIN TRANSACTION
		DECLARE @lErrorMessage NVARCHAR(4000)
        DECLARE @lErrorSeverity INT
        DECLARE @lErrorState INT        
		DECLARE @lxmlData XML = CONVERT(XML,@pXMLDoc)
		DECLARE @lTemp TABLE( Id INT, TypeId INT)
		DECLARE @lCustomerId INT
		DECLARE @lCreditCardId INT
		DECLARE @lTimeZoneParameter INT
		DECLARE @lIssueForId INT
		DECLARE @lCreditCardCount INT

        

		CREATE TABLE #Results 
		(	
			 [Id] INT IDENTITY
			--,[VehicleId] INT
			--,[DriverId] INT
			,[Available] DECIMAL(16, 2)
			,[Assigned] DECIMAL(16, 2)
			,[AssignedLiters] DECIMAL(16, 3)
			,[RealAmount] DECIMAL(16, 2)
			,[RealLiters] DECIMAL(16, 3)
			,[TotalAssigned] DECIMAL(16, 2)
			,[Total] DECIMAL(16, 2)
			,[CreditCardId] INT
			,[PaymentInstrumentId] INT
			,[AvailableLiters] DECIMAL(16, 3)
		)

        INSERT INTO @lTemp
		SELECT Row.col.value('./@Id', 'INT'),Row.col.value('./@TypeId', 'INT') 
        FROM @lxmlData.nodes('/xmldata/CreditCardId') Row(col)


		SELECT @lCreditCardId = MIN([Id])
		FROM @lTemp t

		------------------------------------------------------------
		WHILE @lCreditCardId IS NOT NULL
		BEGIN
			DECLARE @TypeId INT = (SELECT [TypeId] FROM @lTemp WHERE [Id] = @lCreditCardId)

			IF @TypeId = 1
			BEGIN
				UPDATE	[Control].[PaymentInstruments]
				SET		[StatusId] = COALESCE(@pStatusId,[StatusId]),
						[ModifyDate] = GETUTCDATE(),
						[ModifyUserId] = @pUserId			
				WHERE [Id] = @lCreditCardId
			END
			ELSE
			BEGIN
				UPDATE	[Control].[CreditCard]
				SET		[StatusId] = COALESCE(@pStatusId,[StatusId]),
						[ModifyDate] = GETUTCDATE(),
						[ModifyUserId] = @pUserId			
				WHERE [CreditCardId] = @lCreditCardId
			END

			SELECT @lCreditCardId = MIN(Id) 
			FROM @lTemp 
			WHERE Id > @lCreditCardId
		END
		------------------------------------------------------------

		--UPDATE	[Control].[CreditCard]
		--SET		[StatusId] = COALESCE(@pStatusId,[StatusId]),
		--		[ModifyDate] = GETUTCDATE(),
		--		[ModifyUserId] = @pUserId			
		--WHERE [CreditCardId] IN (SELECT Id FROM @lTemp)

		--SELECT @lCreditCardId = MIN([Id])
		--FROM @lTemp t

		SELECT @lCreditCardId = MIN([Id])
		FROM @lTemp t 
		WHERE [TypeId] <> 1

		SELECT	@lCustomerId = c.[CustomerId],
				@lIssueForId = [IssueForId],
				@lTimeZoneParameter = coun.[TimeZone]
		FROM [Control].[CreditCard] cc
		INNER JOIN [General].[Customers] c
			ON cc.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Countries] coun
			ON coun.[CountryId] = c.[CountryId]
		WHERE [CreditCardId] = @lCreditCardId		

		WHILE @lCreditCardId IS NOT NULL
		BEGIN
			IF @lIssueForId = 101
			BEGIN		
				DECLARE	@lVehicleId INT,
						@lFuelId INT

				SELECT	@lVehicleId = v.[VehicleId],
						@lFuelId = [DefaultFuelId]
				FROM [Control].[CreditCardByVehicle] ccv
				INNER JOIN [General].[Vehicles] v
					ON v.[VehicleId] = ccv.[VehicleId]
				INNER JOIN [General].[VehicleCategories] vc
					ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
				WHERE ccv.[CreditCardId] = @lCreditCardId
					
				IF @pStatusId <> 9
				BEGIN
					INSERT INTO #Results
					(
						 [Available]
						,[Assigned]
						,[AssignedLiters]
						,[RealAmount]
						,[RealLiters]
						,[TotalAssigned]
						,[Total]
						,[CreditCardId]
						,[PaymentInstrumentId]
						,[AvailableLiters]
					)
					EXEC [Control].[Sp_GetDistributionData] @pVehicleId = @lVehicleId
															,@pCustomerId = @lCustomerId
															,@pFuelId = @lFuelId
															,@pTimeZoneParameter = @lTimeZoneParameter

					UPDATE [Control].[CreditCard]
					SET [CreditAvailable] = (SELECT ISNULL([Available], 0) FROM #Results WHERE [CreditCardId] = @lCreditCardId)	
						,[AvailableLiters] = (SELECT ISNULL([AvailableLiters], 0) FROM #Results WHERE [CreditCardId] = @lCreditCardId)			
					WHERE [CreditCardId] = @lCreditCardId
				END
			END
			ELSE 
			BEGIN

				DECLARE @lDriverId INT

				SELECT @lDriverId = [UserId]
				FROM [Control].[CreditCardByDriver] ccd
				WHERE [CreditCardId] = @lCreditCardId

				IF @pStatusId <> 9
				BEGIN
					INSERT INTO #Results
					(
						 [Available]
						,[Assigned]
						,[AssignedLiters]
						,[RealAmount]
						,[RealLiters]
						,[TotalAssigned]
						,[Total]
						,[CreditCardId]
						,[PaymentInstrumentId]
						,[AvailableLiters]
					)
					EXEC [Control].[Sp_GetDistributionData] @pDriverId = @lDriverId
															,@pCustomerId = @lCustomerId												   
															,@pTimeZoneParameter = @lTimeZoneParameter

					UPDATE [Control].[CreditCard]
					SET [CreditAvailable] = (SELECT ISNULL([Available], 0) FROM #Results WHERE [CreditCardId] = @lCreditCardId)	
						,[AvailableLiters] = (SELECT ISNULL([AvailableLiters], 0) FROM #Results WHERE [CreditCardId] = @lCreditCardId)		
					WHERE [CreditCardId] = @lCreditCardId
				END
			END
				
			SELECT @lCreditCardId = MIN(Id) 
			FROM @lTemp 
			WHERE Id > @lCreditCardId
		END

		--Update the credit card hist
		INSERT INTO [Control].[CreditCardHx] 
		(
			 [CreditCardId]
			,[CustomerId]
			,[CreditCardNumber]
			,[ExpirationYear]
			,[ExpirationMonth]
			,[CreditAvailable]
			,[StatusId]
			,[CardRequestId]
			,[InsertDate]
			,[InsertUserId]
			,[ModifyDate]
			,[ModifyUserId]
		)
		SELECT [CreditCardId]
			,[CustomerId]
			,[CreditCardNumber]
			,[ExpirationYear]
			,[ExpirationMonth]
			,[CreditAvailable]
			,[StatusId]
			,[CardRequestId]
			,[InsertDate]
			,[InsertUserId]
			,[ModifyDate]
			,[ModifyUserId] 
		FROM [Control].[CreditCard] 
		WHERE [CreditCardId] IN (SELECT Id FROM @lTemp)
		
		SELECT @lCreditCardCount = COUNT(*)
		FROM @lTemp
			
		EXEC [Control].[Sp_CancelCreditCardAlert_Send]	@pCustomerId = @lCustomerId,
														@pCustomerName = @pCustomerName,
														@pStatusId = @pStatusId,
														@pCreditCardCount = @lCreditCardCount,
														@pUserId = @pUserId
		
		COMMIT TRANSACTION			
    END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END


GO


