/****** Object:  StoredProcedure [Control].[Sp_Setting_Odometers_Retrieve]    Script Date: 6/2/2020 10:46:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Stefano Quiros
-- Create date: 3/31/2016
-- Description:	Retrieve Setting_Odometers information
-- ================================================================================================
-- Stefano Quirós - 11/25/2016 - Fix: Inside the Subquery of the Odometer and Liters Last, I put a validation that ignored the reversed transactions.
-- Stefano Quirós - 12/06/2016 - Add the Time Zone Parameter
-- Henry Retana - 12/14/2016 - Add Odometer Before Last and ShowAlert Validation
-- Marco Cabrera - Add a validation in case of liters is 0 and cause a division between 0 error
-- Henry Retana - 06/06/2017 - Add ExportReport parameter, it will return all the rows for the filters and not only the 100
-- Henry Retana - 05/09/2017 - Add Driver Name and driver identification to the retrieve
-- Henry Retana - 11/09/2017 - Add second validation
-- Henry Retana - 19/09/2017 - Get the driver information
-- Henry Retana - 15/11/2017 - Add Void Transactions Validation
-- Mari Jiménez - APR/04/2019 - Default parameters values. Include @lUnitCapacityName
-- Stefano Quirós Ruiz - Fix the Filter type Default on null and add the IsDenied flag
-- Stefano Quirós - Change the filter from InsertDate to Date on the conditions and on the Select - 29/112019
-- Juan C. Santamaria - Get First Odometer by first transactions - 2020-03-05
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_Setting_Odometers_Retrieve] --36, null, null, 1, 2019, 7, null, 1, null, 1
(
	@pCustomerId INT,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL,
	@pUserId INT,
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pKey VARCHAR(800) = NULL,
	@pAlert BIT = 1,
	@pFilterType INT = null, --Stefano Quirós - Change 1 to Null
	@pExportReport BIT = 1
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lUnitCapacityName VARCHAR(100) = (	
									SELECT TOP 1 d.[Name] 
									FROM [General].[Customers] c
									INNER JOIN [General].[Types] d
										ON d.[TypeId] = c.[UnitOfCapacityId]
									WHERE [CustomerId] = @pCustomerId)

	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	DECLARE @lTimeZoneParameter INT 
		   ,@lParameterO DECIMAL(16,2)
		   ,@lTop BIGINT = 0X7FFFFFFFFFFFFFFF --Hexadecimal number, max number of rows available in sql
		   ,@lOdometerDistance INT
		   ,@lFlag INT

	--Get the parameter information for the customer
	SELECT @lParameterO = [ParameterValue],
		   @lOdometerDistance = [OdometerDifference],
		   @lFlag = [OdometerDifferenceValidation]
	FROM [General].[AlertsByCustomer]
	WHERE [AlertId] = 2 
	  AND [CustomerId] = @pCustomerId

	IF @lFlag <> 1
	BEGIN
		IF @lParameterO IS NULL 
			SET @lParameterO = 0.15
		ELSE
			SET @lParameterO = (@lParameterO/100)

		SET @lOdometerDistance = NULL
	END
	ELSE 
	BEGIN 
		SET @lParameterO = NULL 

		IF @lOdometerDistance IS NULL SET @lOdometerDistance = 1000
	END

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	SET @lTop = CASE WHEN @pExportReport = 0 OR @pExportReport IS NULL THEN 100 ELSE @lTop END

	IF @pAlert = 0
	BEGIN
		SELECT TOP (@lTop)		
			t.[Date],
			c.customerid,
			v.[Name] [VehicleName],
			v.[PlateId],

			CASE WHEN (SELECT COUNT(*)
					   FROM [Control].[Transactions] t
					   WHERE t.[VehicleId] = v.[VehicleId]
					   AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
					   AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
					   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					   		    FROM [Control].[Transactions] t2
					   		    WHERE (t2.[CreditCardId] = t.[CreditCardId]
									   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId]) 
					   		    AND t2.[TransactionPOS] = t.[TransactionPOS] 
					   		    AND t2.[ProcessorId] = t.[ProcessorId] 
					   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))) > 2 
			THEN (SELECT TOP 1 TT.[Odometer] 
				  FROM(SELECT TOP 3 NT.* 
					   FROM [Control].[Transactions] NT
					   WHERE NT.[VehicleId] = t.[VehicleId] 
					   AND NT.[Date] <= t.[Date] 
					   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
					   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
					   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					   		    FROM [Control].[Transactions] t2
					   		    WHERE (t2.[CreditCardId] = nt.[CreditCardId]
									   OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
					   		    AND t2.[TransactionPOS] = nt.[TransactionPOS] 
					   		    AND t2.[ProcessorId] = nt.[ProcessorId] 
					   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
					            ORDER BY NT.[Date] DESC ) TT 
					   ORDER BY TT.[Date] ASC)
			ELSE 0
			END AS [OdometerBeforeLast],

			CASE WHEN (SELECT COUNT(*)
					   FROM [Control].[Transactions] t
					   WHERE t.[VehicleId] = v.[VehicleId]
					   AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
					   AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
					   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					   		    FROM [Control].[Transactions] t2
					   		    WHERE (t2.[CreditCardId] = t.[CreditCardId]
									   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId]) 
					   		    AND t2.[TransactionPOS] = t.[TransactionPOS] 
					   		    AND t2.[ProcessorId] = t.[ProcessorId] 
					   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))) > 1
			THEN (SELECT TOP 1 TT.[Odometer] 
				  FROM(SELECT TOP 2 NT.* 
					   FROM [Control].[Transactions] NT
					   WHERE NT.[VehicleId] = t.[VehicleId] 
					   AND NT.[Date] <= t.[Date] 
					   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
					   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
					   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					   			FROM [Control].[Transactions] t2
					   			WHERE (t2.[CreditCardId] = nt.[CreditCardId]
									   OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
					   			AND t2.[TransactionPOS] = nt.[TransactionPOS] 
					   			AND t2.[ProcessorId] = nt.[ProcessorId] 
					   			AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
					            ORDER BY NT.[Date] DESC ) TT 
					  ORDER BY TT.[Date] ASC) 
			ELSE (SELECT TOP 1 FirstOdometer FROM [General].[Vehicles] vx WHERE vx.[VehicleId] = t.[VehicleId])
			END AS [OdometerLast],

			t.[Odometer],
			(SELECT TOP 1 TT.[Liters] 
				FROM(SELECT TOP 2 NT.* 
					FROM [Control].[Transactions] NT
					WHERE NT.[VehicleId] = t.[VehicleId] 
					AND NT.[DATE] <= t.[Date] 
					AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
					AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
					AND 1 > (SELECT ISNULL(COUNT(1), 0) 
							 FROM [Control].[Transactions] t2
							 WHERE (t2.[CreditCardId] = nt.[CreditCardId]
									OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
							 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
							 AND t2.[ProcessorId] = nt.[ProcessorId] 
							 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
					ORDER BY NT.[Date] DESC ) TT 
					ORDER BY TT.[Date] ASC) AS [LitersLast],	
			t.[Liters],				
			t.[FuelAmount],	
			vc.[DefaultPerformance],	
			t.[TransactionId],
			ISNULL(t.[ShowAlert], 0) [ShowAlert],
			(t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
			   					FROM(SELECT TOP 2 NT.* 
			   						FROM [Control].[Transactions] NT
			   						WHERE NT.[VehicleId] = t.[VehicleId] 
			   						AND NT.[Date] <= t.[Date] 
			   						AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
									AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0) 
									AND 1 > (SELECT ISNULL(COUNT(1), 0) 
											 FROM [Control].[Transactions] t2
											 WHERE (t2.[CreditCardId] = nt.[CreditCardId]
													OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
											 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
											 AND t2.[ProcessorId] = nt.[ProcessorId] 
											 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
			   						ORDER BY NT.[Date] DESC ) TT 
			   						ORDER BY TT.[Date] ASC)) [Travel],
			   (CASE WHEN t.[Liters] <> 0 THEN (CONVERT(DECIMAL(12,2), ((t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
														FROM( SELECT TOP 2 NT.* 
															 FROM [Control].[Transactions] NT
															 WHERE NT.[VehicleId] = t.[VehicleId] 
															 AND NT.[Date] <= t.[Date] 
															 AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
															 AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0) 
															 AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																	 FROM [Control].[Transactions] t2
																	 WHERE (t2.[CreditCardId] = nt.[CreditCardId]
																			OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
																	 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
																	 AND t2.[ProcessorId] = nt.[ProcessorId] 
																	 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
															 ORDER BY NT.[Date] DESC ) TT 
															 ORDER BY TT.[Date] ASC)) / t.[Liters])))
															 ELSE 0 END) [Performance],
			  ISNULL((SELECT TOP 1 u.[Name]
					  FROM [General].[Users] u
					  INNER JOIN [General].[DriversUsers] du
					  	ON u.[UserId] = du.[UserId]
					  WHERE du.[CustomerId] = c.[CustomerId] 
					  AND du.[Code] = t.[DriverCode]
					  AND (u.[IsDeleted] IS NULL 
						   OR u.[IsDeleted] = 0)
		              AND (u.[IsActive] IS NULL
						   OR u.[IsActive] = 1)), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[Date], 0)) [DriverName],
			  ISNULL((SELECT TOP 1 du.[Identification]
					  FROM [General].[Users] u
					  INNER JOIN [General].[DriversUsers] du
					  	ON u.[UserId] = du.[UserId]
					  WHERE du.[CustomerId] = c.[CustomerId] 
					  AND du.[Code] = t.[DriverCode]
					  AND (u.[IsDeleted] IS NULL 
						   OR u.[IsDeleted] = 0)
		              AND (u.[IsActive] IS NULL
						   OR u.[IsActive] = 1)), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[Date], 1)) [DriverIdentification]
			,@lUnitCapacityName [UnitFuelName]
		FROM [Control].[Transactions] t
		INNER JOIN [General].[Vehicles] v 
			ON t.[VehicleId] = v.[VehicleId]
		INNER JOIN [General].[Customers] c 
			ON v.[CustomerId] = c.[CustomerId]	
		INNER JOIN [General].[VehicleCategories] vc	
			ON v.[VehicleCategoryId] = vc.[VehicleCategoryId] 				
		WHERE (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
		AND c.[CustomerId] = @pCustomerId 
		AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
		AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				 FROM [Control].[Transactions] t2
				 WHERE (t2.[CreditCardId] = t.[CreditCardId]
						OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId]) 
				 AND t2.[TransactionPOS] = t.[TransactionPOS] 
				 AND t2.[ProcessorId] = t.[ProcessorId] 
				 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))	
		AND (@pKey IS NULL 
			OR v.[Name] like '%'+@pKey+'%'			
			OR v.[PlateId] like '%'+@pKey+'%')			
		AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,t.[Date]) = @pMonth
			AND DATEPART(yyyy,t.[Date]) = @pYear) 
			OR (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
			AND t.[Date] BETWEEN @pStartDate AND @pEndDate))
		AND (@pFilterType IS NULL OR ((@lParameterO IS NOT NULL AND (CASE WHEN t.[Liters] <> 0  
																		 THEN ((t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
																								FROM( SELECT TOP 2 NT.* 
																										FROM [Control].[Transactions] NT
																										WHERE NT.[VehicleId] = t.[VehicleId] 
																										AND NT.[Date] <= t.[Date] 
																										 AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
																										 AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0) 
																										 AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																												 FROM [Control].[Transactions] t2
																												 WHERE (t2.[CreditCardId] = nt.[CreditCardId]
																														OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
																												 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
																												 AND t2.[ProcessorId] = nt.[ProcessorId] 
																												 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
																										ORDER BY NT.[Date] DESC ) TT 
																										ORDER BY TT.[Date] ASC)) / t.[Liters]) 
																			ELSE 0 END)
																	NOT BETWEEN (vc.[DefaultPerformance] - (vc.[DefaultPerformance] * @lParameterO)) 
																	AND (vc.[DefaultPerformance] + (vc.[DefaultPerformance] * @lParameterO)))
										OR (@lOdometerDistance IS NOT NULL AND ((t.[Odometer] < (SELECT TOP 1 TT.[Odometer] 
																								 FROM(SELECT TOP 2 NT.* 
																									  FROM [Control].[Transactions] NT
																									  WHERE NT.[VehicleId] = t.[VehicleId] 
																									  AND NT.[Date] <= t.[Date] 
																									  AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
																									  AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
																									  AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																												FROM [Control].[Transactions] t2
																												WHERE (t2.[CreditCardId] = nt.[CreditCardId]
																													   OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
																												AND t2.[TransactionPOS] = nt.[TransactionPOS] 
																												AND t2.[ProcessorId] = nt.[ProcessorId] 
																												AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
																										ORDER BY NT.[Date] DESC ) TT 
																								ORDER BY TT.[Date] ASC))
												OR ((t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
																	 FROM(SELECT TOP 2 NT.* 
																	      FROM [Control].[Transactions] NT
																		  WHERE NT.[VehicleId] = t.[VehicleId] 
																		  AND NT.[Date] <= t.[Date] 
																		  AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
																		  AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0) 
																		  AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																					FROM [Control].[Transactions] t2
																					WHERE (t2.[CreditCardId] = nt.[CreditCardId]
																						   OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
																					AND t2.[TransactionPOS] = nt.[TransactionPOS] 
																					AND t2.[ProcessorId] = nt.[ProcessorId] 
																					AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
																			ORDER BY NT.[Date] DESC ) TT 
																			ORDER BY TT.[Date] ASC)) > @lOdometerDistance))))
			 )
		ORDER BY t.[Date] DESC
	END
	ELSE 
	BEGIN 

		;WITH TempTable AS 
		(
			SELECT 
				  t.[Date],
				   c.CustomerId,
				   v.[Name] [VehicleName],
				   v.[PlateId],
				   t.[Date] [InsertDate],

				   CASE WHEN (SELECT COUNT(*)
					   FROM [Control].[Transactions] t
					   WHERE t.[VehicleId] = v.[VehicleId]
					   AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
					   AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
					   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
					   		    FROM [Control].[Transactions] t2
					   		    WHERE (t2.[CreditCardId] = t.[CreditCardId]
									   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId]) 
					   		    AND t2.[TransactionPOS] = t.[TransactionPOS] 
					   		    AND t2.[ProcessorId] = t.[ProcessorId] 
					   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))) > 2 
				   THEN (SELECT TOP 1 TT.[Odometer] 
				   	  FROM(SELECT TOP 3 NT.* 
				   		   FROM [Control].[Transactions] NT
				   		   WHERE NT.[VehicleId] = t.[VehicleId] 
				   		   AND NT.[Date] <= t.[Date] 
				   		   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
						   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0) 
				   		   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				   		   		    FROM [Control].[Transactions] t2
				   		   		    WHERE (t2.[CreditCardId] = nt.[CreditCardId]
										   OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
				   		   		    AND t2.[TransactionPOS] = nt.[TransactionPOS] 
				   		   		    AND t2.[ProcessorId] = nt.[ProcessorId] 
				   		   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   		            ORDER BY NT.[Date] DESC ) TT 
				   		   ORDER BY TT.[Date] ASC)
				   ELSE 0
				   END AS [OdometerBeforeLast],
				   
				   CASE WHEN (SELECT COUNT(*)
				   		   FROM [Control].[Transactions] t
				   		   WHERE t.[VehicleId] = v.[VehicleId]
				   		   AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
						   AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
				   		   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				   		   		    FROM [Control].[Transactions] t2
				   		   		    WHERE (t2.[CreditCardId] = t.[CreditCardId]
					                       OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId]) 
				   		   		    AND t2.[TransactionPOS] = t.[TransactionPOS] 
				   		   		    AND t2.[ProcessorId] = t.[ProcessorId] 
				   		   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))) > 1
				   THEN (SELECT TOP 1 TT.[Odometer] 
				   	  FROM(SELECT TOP 2 NT.* 
				   		   FROM [Control].[Transactions] NT
				   		   WHERE NT.[VehicleId] = t.[VehicleId] 
				   		   AND NT.[Date] <= t.[Date] 
				   		   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
						   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0) 
				   		   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				   		   			FROM [Control].[Transactions] t2
				   		   			WHERE (t2.[CreditCardId] = nt.[CreditCardId]
					                       OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
				   		   			AND t2.[TransactionPOS] = nt.[TransactionPOS] 
				   		   			AND t2.[ProcessorId] = nt.[ProcessorId] 
				   		   			AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   		            ORDER BY NT.[Date] DESC ) TT 
				   		  ORDER BY TT.[Date] ASC) 
				   ELSE (SELECT TOP 1 FirstOdometer FROM [General].[Vehicles] vx WHERE vx.[VehicleId] = t.[VehicleId])
				   END AS [OdometerLast],

				   t.[Odometer],
				   (SELECT TOP 1 TT.[Liters] 
				   	FROM(SELECT TOP 2 NT.* 
				   		FROM [Control].[Transactions] NT
				   		WHERE NT.[VehicleId] = t.[VehicleId] 
				   		AND NT.[DATE] <= t.[Date] 
				   		AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
						AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0) 
						AND 1 > (SELECT ISNULL(COUNT(1), 0) 
								FROM [Control].[Transactions] t2
								WHERE (t2.[CreditCardId] = nt.[CreditCardId]
					                   OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
								AND t2.[TransactionPOS] = nt.[TransactionPOS] 
								AND t2.[ProcessorId] = nt.[ProcessorId] 
								AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   		ORDER BY NT.[Date] DESC ) TT 
				   		ORDER BY TT.[Date] ASC) AS [LitersLast],	
				   t.[Liters],				
				   t.[FuelAmount],	
				   vc.[DefaultPerformance],	
				   t.[TransactionId],
				   ISNULL(t.[ShowAlert], 0) [ShowAlert],
				   (t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
				   					FROM(SELECT TOP 2 NT.* 
				   						FROM [Control].[Transactions] NT
				   						WHERE NT.[VehicleId] = t.[VehicleId] 
				   						AND NT.[Date] <= t.[Date] 
				   						AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
										AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0) 
										AND 1 > (SELECT ISNULL(COUNT(1), 0) 
												FROM [Control].[Transactions] t2
												WHERE (t2.[CreditCardId] = nt.[CreditCardId]
													   OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
												AND t2.[TransactionPOS] = nt.[TransactionPOS] 
												AND t2.[ProcessorId] = nt.[ProcessorId] 
												AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				   						ORDER BY NT.[Date] DESC ) TT 
				   						ORDER BY TT.[Date] ASC)) [Travel],
				   CASE WHEN @lParameterO IS NOT NULL 
							 THEN  ((t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
													 FROM(
															SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
															WHERE NT.[VehicleId] = t.[VehicleId] 
															AND NT.[Date] <= t.[Date] 
															AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
															AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)  
															AND 1 > (SELECT ISNULL(COUNT(1), 0) 
																	 FROM [Control].[Transactions] t2
																	 WHERE (t2.[CreditCardId] = nt.[CreditCardId]
																			OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId]) 
																	 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
																	 AND t2.[ProcessorId] = nt.[ProcessorId] 
																	 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
															ORDER BY NT.[Date] DESC ) TT 
													ORDER BY TT.[Date] ASC)) / t.[Liters])
							ELSE (t.[Odometer] - (SELECT TOP 1 TT.[Odometer] 
												  FROM(
														SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
														WHERE NT.[VehicleId] = t.[VehicleId] 
														AND NT.[Date] <= t.[Date]
														AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
													    AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0) 
														AND 1 > (  SELECT ISNULL(COUNT(1), 0) 
					   											FROM [Control].[Transactions] t2
					   											WHERE (t2.[CreditCardId] = NT.[CreditCardId]
																	   OR t2.[PaymentInstrumentId] = NT.[PaymentInstrumentId]) 
					   											AND   t2.[TransactionPOS] = NT.[TransactionPOS] 
					   											AND   t2.[ProcessorId] = NT.[ProcessorId] 
					   											AND   t2.IsReversed = 1
					   										)
														ORDER BY NT.[Date] DESC ) TT 
												 ORDER BY TT.[Date] ASC)) END [Performance],
				  ISNULL((SELECT TOP 1 u.[Name]
						  FROM [General].[Users] u
						  INNER JOIN [General].[DriversUsers] du
						  	ON u.[UserId] = du.[UserId]
						  WHERE du.[CustomerId] = c.[CustomerId] 
						  AND du.[Code] = t.[DriverCode]
						  AND (u.[IsDeleted] IS NULL 
								 OR u.[IsDeleted] = 0)
		                    AND (u.[IsActive] IS NULL
								 OR u.[IsActive] = 1)), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[Date], 0)) [DriverName],
				  ISNULL((SELECT TOP 1 du.[Identification]
						  FROM [General].[Users] u
						  INNER JOIN [General].[DriversUsers] du
						  	ON u.[UserId] = du.[UserId]
						  WHERE du.[CustomerId] = c.[CustomerId] 
						  AND du.[Code] = t.[DriverCode]
						  AND (u.[IsDeleted] IS NULL 
							   OR u.[IsDeleted] = 0)
		                  AND (u.[IsActive] IS NULL
							   OR u.[IsActive] = 1)), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[Date], 1)) [DriverIdentification]
			FROM [Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v 
					ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c 
					ON v.[CustomerId] = c.[CustomerId]	
				INNER JOIN [General].[VehicleCategories] vc	
					ON v.[VehicleCategoryId] = vc.[VehicleCategoryId] 				
				WHERE(@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
				AND c.[CustomerId] = @pCustomerId 
				AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
				AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
				AND 1 > (SELECT ISNULL(COUNT(1), 0) 
						 FROM [Control].[Transactions] t2
						 WHERE (t2.[CreditCardId] = t.[CreditCardId]
					            OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId]) 
						 AND t2.[TransactionPOS] = t.[TransactionPOS] 
						 AND t2.[ProcessorId] = t.[ProcessorId] 
						 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))			
				AND (t.[ShowAlert] IS NULL OR t.[ShowAlert] = 0)
        )

		SELECT *,
			@lUnitCapacityName [UnitFuelName] 
			FROM TempTable t
		WHERE (@lOdometerDistance IS NOT NULL AND ([Performance] < 0 OR [Performance] > @lOdometerDistance))
		OR (@lParameterO IS NOT NULL AND(t.[Performance] NOT BETWEEN (t.[DefaultPerformance] - (t.[DefaultPerformance] * @lParameterO)) 
										   AND (t.[DefaultPerformance] + (t.[DefaultPerformance] * @lParameterO))))
		ORDER BY t.[Date] DESC
	END	

	SET NOCOUNT OFF
END