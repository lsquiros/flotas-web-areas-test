/****** Object:  StoredProcedure [Control].[Sp_PaymentMethods_Retrieve]    Script Date: 27/05/2020 10:08:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      Jason Bonilla Soto
-- Create Date: 25/05/2020
-- Description: Retrieve List of Payment Methods
-- =============================================
CREATE PROCEDURE [Control].[Sp_PaymentMethods_Retrieve]
AS
BEGIN
    SELECT
		0 [Id],
		'Tarjeta' [Name]
	FROM [Control].[PaymentInstrumentsTypes]
	UNION
	SELECT
		[Id],
		[Name]
	FROM [Control].[PaymentInstrumentsTypes]
END
GO


