USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_FleetCardBalanceInquiry_Retrieve]    Script Date: 05/06/2020 3:49:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 20/10/2017
-- Description:	Retrieve FleetCard Balance
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Stefano Quir�s - Return the Available Liters when customer is by Liters - 30/01/2019
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- Juan Carlos Santamaria - 16-12-2019 - validation of liters and gallons as appropriate by customer
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_FleetCardBalanceInquiry_Retrieve]
(
	 @pCreditCard VARCHAR(200), 
	 @pCarTag VARCHAR(100), 
	 @pPlateId VARCHAR(100) 
)
AS
BEGIN	
	SET NOCOUNT ON
	 BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
			DECLARE @lLITERSTOGALONS FLOAT = 3.78541178
									
			IF EXISTS (SELECT *
					   FROM [Control].[CreditCardByVehicle] cv
					   INNER JOIN [Control].[CreditCard] c
						   ON cv.[CreditCardId] = c.[CreditCardId]
					   INNER JOIN [General].[Vehicles] v
						   ON cv.[VehicleId] = v.[VehicleId]
					   LEFT JOIN [General].[VehiclesDrivers] vd
						   ON vd.[VehicleId] = v.[VehicleId]
					   LEFT JOIN [General].[DriversUsers] du
						   ON du.[UserId] = vd.[UserId]
					   WHERE (v.[PlateId] = @pPlateId OR (@pCarTag IS NOT NULL OR du.[Code] = @pCarTag))
					   AND c.[CreditCardNumber] = @pCreditCard) --IF THE CARD IS LINKED WITH THE VEHICLE
			BEGIN
			print 1
				--Get the results
				SELECT TOP 1 cu.[Name] [CustomerName],
							 CASE WHEN CAST(ISNULL((
														SELECT [Value]
														FROM [General].[ParametersByCustomer]
														WHERE [CustomerId] = c.[CustomerId]
														AND [Name] = 'TypeOfDistribution'
														AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
												   ), 0) AS INT) = 1
							 THEN CASE WHEN cu.UnitOfCapacityId = 0 THEN CONVERT(DECIMAL(16,2), c.[AvailableLiters]) 
																	ELSE (CONVERT(DECIMAL(16,2), c.[AvailableLiters] / @lLITERSTOGALONS)) END
							 ELSE c.[CreditAvailable]
							 END [CreditAvailable],
							 CASE WHEN v.[Odometer] IS NULL OR v.[Odometer] = 0
							  	 THEN (SELECT TOP 1 t.[Odometer]
							 		   FROM [Control].[Transactions] t
							 		   WHERE t.[VehicleId] = v.[VehicleId]
									   AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
									   AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
									   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
												 FROM [Control].[Transactions] t2
												 WHERE t2.[CreditCardId] = t.[CreditCardId] 
												 AND t2.[TransactionPOS] = t.[TransactionPOS] 
												 AND t2.[ProcessorId] = t.[ProcessorId] 
												 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
							 		   ORDER BY t.[TransactionId] DESC)
							 	 ELSE v.[Odometer]
							 END [Odometer]
				FROM [Control].[CreditCardByVehicle] cv
				INNER JOIN [Control].[CreditCard] c
					ON cv.[CreditCardId] = c.[CreditCardId]
				INNER JOIN [General].[Vehicles] v
					ON cv.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] cu
					ON v.[CustomerId] = cu.[CustomerId]
				LEFT JOIN [General].[VehiclesDrivers] vd
					ON vd.[VehicleId] = v.[VehicleId]
				LEFT JOIN [General].[DriversUsers] du
					ON du.[UserId] = vd.[UserId]
				WHERE (v.[PlateId] = @pPlateId OR (@pCarTag IS NOT NULL OR du.[Code] = @pCarTag))
				AND c.[CreditCardNumber] = @pCreditCard
			END			
			ELSE IF EXISTS(SELECT *
					       FROM [General].[DriversUsers] du
					       INNER JOIN [Control].[CreditCardByDriver] cd
							  ON du.[UserId] = cd.[UserId]
					       INNER JOIN [Control].[CreditCard] c
					       	  ON cd.[CreditCardId] = c.[CreditCardId]
						   LEFT JOIN [General].[VehiclesByUser] vbu
							  ON du.[UserId] = vbu.[UserId]
						   LEFT JOIN [General].[Vehicles] v
							  ON vbu.[VehicleId] = v.[VehicleId]
						   WHERE (v.[PlateId] = @pPlateId OR (@pCarTag IS NOT NULL AND du.[Code] = @pCarTag))
					       AND c.[CreditCardNumber] = @pCreditCard 
						   AND vbu.[LastDateDriving] IS NULL)--IF THE CARD IS LINKED WITH THE DRIVER 
			BEGIN 
				SELECT TOP 1 cu.[Name] [CustomerName],
					         CASE WHEN CAST(ISNULL((
														SELECT [Value]
														FROM [General].[ParametersByCustomer]
														WHERE [CustomerId] = c.[CustomerId]
														AND [Name] = 'TypeOfDistribution'
														AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
												   ), 0) AS INT) = 1
							 THEN CASE WHEN cu.UnitOfCapacityId = 0 THEN CONVERT(DECIMAL(16,2), c.[AvailableLiters]) 
																	ELSE (CONVERT(DECIMAL(16,2), c.[AvailableLiters] / @lLITERSTOGALONS)) END
							 ELSE c.[CreditAvailable]
							 END [CreditAvailable],
						     CASE WHEN v.[Odometer] IS NULL OR v.[Odometer] = 0
							  	  THEN (SELECT TOP 1 t.[Odometer]
							  		    FROM [Control].[Transactions] t
							  		    WHERE t.[VehicleId] = v.[VehicleId]
							  		    AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
										AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
										AND 1 > (SELECT ISNULL(COUNT(1), 0) 
												 FROM [Control].[Transactions] t2
												 WHERE t2.[CreditCardId] = t.[CreditCardId] 
												 AND t2.[TransactionPOS] = t.[TransactionPOS] 
												 AND t2.[ProcessorId] = t.[ProcessorId] 
												 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))	
							  		    ORDER BY t.[TransactionId] DESC)
							  	 ELSE v.[Odometer]
						     END [Odometer]
				FROM [General].[DriversUsers] du
				INNER JOIN [Control].[CreditCardByDriver] cd
					ON du.[UserId] = cd.[UserId]
				INNER JOIN [Control].[CreditCard] c
					ON cd.[CreditCardId] = c.[CreditCardId]
				INNER JOIN [General].[Customers] cu
					ON c.[CustomerId] = cu.[CustomerId]
				LEFT JOIN [General].[VehiclesByUser] vbu
					ON du.[UserId] = vbu.[UserId]
				LEFT JOIN [General].[Vehicles] v
					ON vbu.[VehicleId] = v.[VehicleId]
				WHERE (v.[PlateId] = @pPlateId OR (@pCarTag IS NOT NULL AND du.[Code] = @pCarTag))
				AND c.[CreditCardNumber] = @pCreditCard 
				AND vbu.[LastDateDriving] IS NULL
				ORDER BY vbu.[LastDateDriving] ASC
			END
			ELSE 
				RAISERROR ('La tarjeta no esta asociada al Conductor o Veh�culo.', 16, 1)
	END TRY
    BEGIN CATCH		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH

    SET NOCOUNT OFF
END

GO


