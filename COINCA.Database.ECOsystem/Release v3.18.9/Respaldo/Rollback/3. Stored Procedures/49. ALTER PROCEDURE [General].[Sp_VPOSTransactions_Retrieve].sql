USE ECOsystem
/****** Object:  StoredProcedure [General].[Sp_VPOSTransactions_Retrieve]    Script Date: 05/06/2020 3:45:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 09/11/2017
-- Description:	Retrieve Partner information
-- Modify: 11/05/18 - Marjorie Garbanzo
-- Add the column IsInternal in the datas return
-- Modify by Marjorie Garbanzo - Add filter IsDenied to the retrieve - 21/05/2019
-- ================================================================================================
ALTER PROCEDURE [General].[Sp_VPOSTransactions_Retrieve] 
(				
	 @pStartDate DATETIME = NULL,
	 @pEndDate DATETIME = NULL,
	 @pYear INT = NULL,
	 @pMonth INT = NULL,
	 @pCustomerId INT
)
AS
BEGIN
	SET NOCOUNT ON	
		
	SELECT  t.[TransactionId] [CCTransactionVPOSId]
		   ,t.[CreditCardId]
		   ,cc.[CustomerId]
		   ,t.[Date]		 [Date]
		   ,v.[PlateId]	     [Plate]   
		   ,t.[FuelAmount]   [TotalAmount]
		   ,t.[Liters]       [Liters]
		   ,t.[ProcessorId]  [TerminalId]
		   ,CAST(t.[Odometer] AS DECIMAL(12, 2)) [Odometer]
		   ,t.[AuthorizationNumber] [AuthorizationNumber]		   
		   ,ISNULL([General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0), 'N/A')  [ServiceStationName]
		   ,cu.[Symbol]      [CurrencySymbol]
		   ,c.[Name] [EncryptCustomerName]
		   ,t.[Invoice]
		   ,cc.[CreditCardNumber] [AccountNumber]
		   ,t.[TransactionPOS] [SystemTraceNumber]
		   ,p.[PartnerId]
		   ,t.[IsInternal] [IsInternalStation]
		   ,t.[IsFloating] [IsFloatingTransaction]
	FROM [Control].[Transactions] t 
	INNER JOIN [Control].[CreditCard] cc
		ON t.[CreditCardId] = cc.[CreditCardId]
	INNER JOIN [General].[Vehicles] v
		ON t.[VehicleId] = v.[VehicleId]
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]
	INNER JOIN [Control].[Currencies] cu
		ON c.[CurrencyId] = cu.[CurrencyId]
	INNER JOIN [General].[CustomersByPartner] p
		ON cc.[CustomerId] = p.[CustomerId]	
	WHERE c.[CustomerId] = @pCustomerId
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(MONTH, t.[Date]) = @pMonth
			AND DATEPART(YEAR, t.[Date]) = @pYear)
	    OR (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
			AND t.[Date] BETWEEN @pStartDate AND @pEndDate))
	AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)
	AND 1 > (SELECT ISNULL(COUNT(1), 0) 
			  FROM [Control].[Transactions] t2
			  WHERE t2.[CreditCardId] = t.[CreditCardId] 
			  AND t2.[TransactionPOS] = t.[TransactionPOS] 
			  AND t2.[ProcessorId] = t.[ProcessorId] 
			  AND (t2.[IsReversed] = 1
				   OR t2.[IsVoid] = 1))
	ORDER BY t.[Date] DESC
	
    SET NOCOUNT OFF
END
GO


