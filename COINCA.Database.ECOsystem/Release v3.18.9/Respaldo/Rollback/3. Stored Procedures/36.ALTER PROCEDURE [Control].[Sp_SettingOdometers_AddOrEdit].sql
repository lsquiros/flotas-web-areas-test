USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_SettingOdometers_AddOrEdit]    Script Date: 05/06/2020 3:27:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Stefano Quiros Ruiz
-- Create date: 04/07/2016
-- Description:	Update Odometers information
-- Modify: Henry Retana - 11/09/2017
-- Add the edit for the odometer last and before last
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Modify By: Stefano Quir�s
-- Add validation if the transaction is the last of the vehicle, if not doesn't update
-- the odometer on the vehicle - Add the validation when transaction is unique
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- ================================================================================================

ALTER PROCEDURE [Control].[Sp_SettingOdometers_AddOrEdit]
(
  	 @pXmlData VARCHAR(MAX),
	 @pCustomerId INT,
	 @pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    		
	DECLARE @lCount INT
    DECLARE @lxmlData XML = CONVERT(XML, @pXmlData)
	
	CREATE TABLE #OdometerChanges
	(	
		[Id] INT IDENTITY,
		[TransactionId] INT,
		[Odometer] DECIMAL(16,2),
		[OdometerLast] DECIMAL(16,2),
		[OdometerBeforeLast] DECIMAL(16,2)
	)
			
	INSERT INTO #OdometerChanges
	SELECT m.c.value('TransactionId[1]', 'INT'),
	       m.c.value('Odometer[1]', 'DECIMAL(16,2)'),
		   m.c.value('OdometerLast[1]', 'DECIMAL(16,2)'),		  
		   m.c.value('OdometerBeforeLast[1]', 'DECIMAL(16,2)')		  
	FROM @lxmlData.nodes('//SettingOdometers') AS m(c)		
				
	SELECT @lCount = MIN([Id]) 
	FROM #OdometerChanges 

	WHILE (@lCount IS NOT NULL)
	BEGIN
		DECLARE @lTransactionId INT = (SELECT [TransactionId] 
									   FROM #OdometerChanges 
									   WHERE [Id] = @lCount),
				@lVehicleId INT,
				@lDate DATETIME

		SELECT @lVehicleId = [VehicleId],
			   @lDate = [Date]
		FROM [Control].[Transactions] 
		WHERE [TRANSACTIONID] = @lTransactionId
		

		UPDATE [Control].[Transactions] 
		SET [Odometer] = (SELECT [Odometer] 
					      FROM #OdometerChanges 
						  WHERE [Id] = @lCount)
		,[ModifyUserId] = @pUserId
		,[ModifyDate] = GETDATE()
		WHERE [TransactionId] = @lTransactionId

		-- Stefano: Update only if the transaction is the last transaction of the vehicle
		IF NOT EXISTS(SELECT * FROM [Control].[Transactions] WHERE [TransactionId] > @lTransactionId AND [VehicleId] = @lVehicleId)
		BEGIN
			---------------------------------------
			--Update Vehicle Odometer
			UPDATE [general].[Vehicles] 
			SET [Odometer] = (SELECT [Odometer] 
						      FROM #OdometerChanges 
							  WHERE [Id] = @lCount)
			WHERE [VehicleId] = @lVehicleId
			-----------------------------------------
		END

		DECLARE @TransacCount INT = (SELECT COUNT(*)
									 FROM [Control].[Transactions] t
									 WHERE t.[VehicleId] = @lVehicleId
									 AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
									 AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
									 AND 1 > (SELECT ISNULL(COUNT(1), 0) 
									 		  FROM [Control].[Transactions] t2
									 		  WHERE t2.[CreditCardId] = t.[CreditCardId] 
									 		  AND t2.[TransactionPOS] = t.[TransactionPOS] 
									 		  AND t2.[ProcessorId] = t.[ProcessorId] 
									 		  AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)))

		IF @TransacCount > 1	
		BEGIN
			--Update Odometer Last
			UPDATE [Control].[Transactions] 
			SET [Odometer] = (SELECT [OdometerLast] 
						      FROM #OdometerChanges 
							  WHERE [Id] = @lCount)
			WHERE [TransactionId] IN (SELECT TOP 1 t.[TransactionId] 
									  FROM(SELECT TOP 2 NT.[TransactionId] 
									       FROM [Control].[Transactions] nt
									       WHERE nt.[VehicleId] = @lVehicleId
									       AND nt.[Date] <= @lDate
										   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
										   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
										   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
													 FROM [Control].[Transactions] t2
													 WHERE t2.[CreditCardId] = nt.[CreditCardId] 
													 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
													 AND t2.[ProcessorId] = nt.[ProcessorId] 
													 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))	
										   ORDER BY NT.[Date] DESC ) t 
									  ORDER BY t.[TransactionId] ASC)
		END
		IF @TransacCount > 2
		BEGIN
			--Update Odometer before Last
			UPDATE [Control].[Transactions] 
			SET [Odometer] = (SELECT [OdometerBeforeLast] 
						      FROM #OdometerChanges 
							  WHERE [Id] = @lCount)
			WHERE [TransactionId] IN (SELECT TOP 1 t.[TransactionId] 
									  FROM(SELECT TOP 3 NT.[TransactionId] 
										   FROM [Control].[Transactions] nt
										   WHERE nt.[VehicleId] = @lVehicleId
										   AND nt.[Date] <= @lDate
										   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0) 
										   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
	   									   AND 1 > (SELECT ISNULL(COUNT(1), 0) 
													 FROM [Control].[Transactions] t2
													 WHERE t2.[CreditCardId] = nt.[CreditCardId] 
													 AND t2.[TransactionPOS] = nt.[TransactionPOS] 
													 AND t2.[ProcessorId] = nt.[ProcessorId] 
													 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))	
											ORDER BY NT.[Date] DESC ) t 
									  ORDER BY t.[TransactionId] ASC)
		END

			SELECT @lCount = MIN([Id]) 
			FROM #OdometerChanges 
			WHERE [Id] > @lCount
		
	END
		
	--UPDATE THE COUNT FOR THE SETTING ODOMETERS	
	EXEC [Control].[Sp_SettingOdometerAlert_Count] @pCustomerId, @pUserId
		
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO


