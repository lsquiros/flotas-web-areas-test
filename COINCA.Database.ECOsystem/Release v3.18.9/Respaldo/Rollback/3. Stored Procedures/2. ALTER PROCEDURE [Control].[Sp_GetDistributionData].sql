/****** Object:  StoredProcedure [Control].[Sp_GetDistributionData]    Script Date: 05/06/2020 2:36:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:		Stefano Quir�s
-- Create date: March 06, 2018
-- Description:	Retrieve the Distribution Data - 15/01/2019 - Add the AvailableLiters
-- Stefano Quir�s - Modify the InsertDate that respect the Datetime value and not 
-- only the Date type
-- Stefano Quir�s - Change a little bit the Logic when the distribution is by CostCenter
-- 09/05/2019
-- ==================================================================================

ALTER PROCEDURE [Control].[Sp_GetDistributionData] 
(
	@pVehicleId INT = NULL
   ,@pDriverId INT = NULL
   ,@pCustomerId INT
   ,@pAmount DECIMAL(16,2) = NULL
   ,@pAdditionalAmount DECIMAL(16,2) = NULL
   ,@pLiters DECIMAL(16,3) = NULL
   ,@pAdditionalLiters DECIMAL(16,3) = NULL
   ,@pFuelId INT = NULL
   ,@pTimeZoneParameter INT
)
AS
BEGIN
SET NOCOUNT ON;
		
		DECLARE @lInsertDate DATETIME = [General].[Fn_LatestMonthlyClosing_Retrieve](@pCustomerId)		
		DECLARE @lFuelAssigned DECIMAL(16, 2) = 0		
		DECLARE @lRealAmount DECIMAL(16, 2) = 0
		DECLARE @lRealLiters DECIMAL(16,3) = 0
		DECLARE @lTotal DECIMAL(16, 2) = 0
		DECLARE @lFuelByCreditId INT = 0
		DECLARE @lCreditCardId INT = 0
		DECLARE @lCreditCardAvailable DECIMAL(16, 2) = 0
		DECLARE @lCostCenterId INT	
		DECLARE @lCostCenterFuelDistribution BIT
		DECLARE @lFuelPermit BIT
		
		SELECT @lCostCenterFuelDistribution = ISNULL([CostCenterFuelDistribution], 0)
		      ,@lFuelPermit = ISNULL([FuelPermit], 0)
		FROM [General].[Customers] 
		WHERE [CustomerId] = @pCustomerId

		SELECT @lCostCenterId = [CostCenterId]
		FROM [General].[Vehicles]
		WHERE [VehicleId] = @pVehicleId

		IF (@pVehicleId IS NOT NULL)
		BEGIN
			--Valida lo asignado actualmente menos al veh�culo que se est� asignando actualmente
			SET @lFuelAssigned = COALESCE((SELECT ISNULL((SUM([Amount]) + SUM([AdditionalAmount])), 0)
										   FROM [Control].[CreditCard] cc
										   INNER JOIN [Control].[CreditCardByVehicle] ccv
												ON ccv.[CreditCardId] = cc.[CreditCardId]
										   INNER JOIN [General].[Vehicles] v
										   		ON v.[VehicleId] = ccv.[VehicleId]
										   INNER JOIN (SELECT vc.[DefaultFuelId] [FuelId]
										   					      ,v.[VehicleId]
										   						  ,vc.[VehicleModel]
										   						  ,vc.[VehicleCategoryId]
										   					FROM [General].[Vehicles] v	
										   					INNER JOIN [General].[VehicleCategories] vc 
										   						ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
										   					INNER JOIN [Control].[Fuels] f	
										   						ON f.[FuelId] = vc.[DefaultFuelId]
										   					WHERE v.[CustomerId] = @pCustomerId
										   					UNION
										   					SELECT fvc.[FuelId] 
										   					      ,v.[VehicleId]
										   						  ,vc.[VehicleModel]
										   						  ,fvc.[VehicleCategoryId]
										   					FROM [General].[Vehicles] v
										   					INNER JOIN [General].[VehicleCategories] vc
										   						ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
										   					INNER JOIN [General].[FuelsByVehicleCategory] fvc
										   						ON fvc.[VehicleCategoryId] = vc.[VehicleCategoryId]
										   					INNER JOIN [Control].[Fuels] f	
										   						ON f.[FuelId] = fvc.[FuelId]
										   					WHERE v.[CustomerId] = @pCustomerId) vc
												ON vc.[VehicleId] = v.[VehicleId]
										   LEFT JOIN [Control].[FuelDistribution] fd
										   		ON fd.[VehicleId] = v.[VehicleId]
										   		AND fd.[FuelId] = vc.[FuelId]
										   		AND fd.[InsertDate] >= @lInsertDate 
										   WHERE v.[CustomerId] = @pCustomerId
											     AND vc.[FuelId] = @pFuelId
											     AND (fd.[VehicleId] <> @pVehicleId
											          AND fd.[FuelId] <> @pFuelId)
											     AND cc.[StatusId] IN (7, 8)
											     AND (v.[IsDeleted] IS NULL
											     	  OR v.[IsDeleted] = 0)
											     AND (@lCostCenterFuelDistribution = 0 
												      OR v.[CostCenterId] = @lCostCenterId)), 0)	
										   
		END
		ELSE
		BEGIN
			--Valida lo asignado actualmente a los veh�culos m�s lo que se est� asignando al veh�culo actual
			SET @lFuelAssigned = COALESCE((SELECT (SUM(fd.[Amount]) + SUM(fd.[AdditionalAmount])) 
										   FROM [Control].[FuelDistribution] fd    
										   INNER JOIN [General].[DriversUsers] du
												ON du.[UserId] = fd.[DriverId]   
										   INNER JOIN [Control].[CreditCardByDriver] cd
												ON cd.[UserId] = du.[UserId]
										   INNER JOIN [Control].[CreditCard] ccc
										   		ON cd.[CreditCardId] = ccc.[CreditCardId] 
									       WHERE fd.[InsertDate] >= @lInsertDate
									       AND du.[CustomerId] = @pCustomerId 
										   AND ccc.[StatusId] IN (7, 8)
										   AND fd.[DriverId] <> @pDriverId),0)		
		END		

		-- Selecciona el Id de la tarjeta ligada al veh�culo    
		SELECT @lCreditCardId = CC.[CreditCardId]
		FROM [Control].[CreditCard] cc
		LEFT JOIN [Control].[CreditCardByVehicle] cv
			ON cc.[CreditCardId] = cv.[CreditCardId]
			AND cc.[StatusId] IN (7,8)
		LEFT JOIN [Control].[CreditCardByDriver] cd
			ON cd.[CreditCardId] = cc.[CreditCardId]  
			AND cc.[StatusId] IN (7,8)
		WHERE (@pVehicleId IS NOT NULL AND cv.VehicleId = @pVehicleId)
				OR (@pDriverId IS NOT NULL AND cd.[UserId] = @pDriverId)
			
		
		SET @lRealAmount = [Control].[Fn_FuelDistribution_TransactionsAmount](@lCreditCardId, @pTimeZoneParameter, @lInsertDate, NULL, CASE WHEN @lFuelPermit = 1 
																																	   THEN @pFuelId
																																	   ELSE NULL
																																	   END)	
		SET @lRealLiters = [Control].[Fn_FuelDistribution_TransactionsLiters](@lCreditCardId, @pTimeZoneParameter, @lInsertDate, NULL, 1, CASE WHEN @lFuelPermit = 1 
																																		  THEN @pFuelId
																																		  ELSE NULL
																																		  END)			

		IF @lCostCenterFuelDistribution = 1
		BEGIN

			SELECT TOP 1 @lFuelByCreditId = [Id]
			FROM [Control].[FuelsByCreditByCostCenter]
			WHERE [CustomerId] = @pCustomerId
			AND [CostCenterId] = @lCostCenterId
			AND [FuelId] = @pFuelId
			ORDER BY [InsertDate] DESC, [TotalAmount] DESC

			-- GEt total assigned to vehicle      
			SELECT @lTotal = [TotalAmount]
			FROM [Control].[FuelsByCreditByCostCenter]
			WHERE [Id] = @lFuelByCreditId	
		END
		ELSE
		BEGIN
			SELECT TOP 1 @lFuelByCreditId = [FuelByCreditId]
			FROM [Control].[FuelsByCredit]
			WHERE [CustomerId] = @pCustomerId
			AND [FuelId] = @pFuelId
			ORDER BY [InsertDate] DESC, [Total] DESC

			-- GEt total assigned to vehicle      
			SELECT @lTotal = Total
			FROM [Control].[FuelsByCredit]
			WHERE [FuelByCreditId] = @lFuelByCreditId	
		END				

		DECLARE @lTmpAmount DECIMAL(16, 2) = ISNULL(@pAmount,  (SELECT TOP 1 ISNULL([Amount], 0)
																FROM [Control].[FuelDistribution] 
																WHERE [InsertDate] >= @lInsertDate 
																AND (
																		(
																		 @pVehicleId IS NOT NULL 
																		 AND [VehicleId] = @pVehicleId
																		 AND [FuelId] = @pFuelId
																		) 
																	  OR (@pDriverId IS NOT NULL AND [DriverId] = @pDriverId))))

		DECLARE @pTmpLiters DECIMAL(16, 3) = ISNULL(@pLiters,  (SELECT TOP 1 ISNULL([Liters], 0)
																FROM [Control].[FuelDistribution] 
																WHERE [InsertDate] >= @lInsertDate 
																AND (
																		(
																		 @pVehicleId IS NOT NULL 
																		 AND [VehicleId] = @pVehicleId
																		 AND [FuelId] = @pFuelId
																		)  
																	  OR (@pDriverId IS NOT NULL AND [DriverId] = @pDriverId))))

		DECLARE @lTmpAdditionalAmount DECIMAL(16, 2) = ISNULL(@pAdditionalAmount, (SELECT TOP 1 ISNULL([AdditionalAmount], 0) 
																				   FROM [Control].[FuelDistribution] 
																				   WHERE [InsertDate] >= @lInsertDate 
																				   AND (
																							(
																							 @pVehicleId IS NOT NULL 
																							 AND [VehicleId] = @pVehicleId
																							 AND [FuelId] = @pFuelId
																							)   
																						 OR (@pDriverId IS NOT NULL AND [DriverId] = @pDriverId))))

		DECLARE @lTmpAdditionalLiters DECIMAL(16, 3) = ISNULL(@pAdditionalLiters, (SELECT TOP 1 ISNULL([AdditionalLiters], 0) 
																		   FROM [Control].[FuelDistribution] 
																		   WHERE [InsertDate] >= @lInsertDate 
																		   AND (
																					(
																					 @pVehicleId IS NOT NULL 
																					 AND [VehicleId] = @pVehicleId
																					 AND [FuelId] = @pFuelId
																					)    
																				 OR (@pDriverId IS NOT NULL AND [DriverId] = @pDriverId))))

		DECLARE @lAssigned DECIMAL(16, 2) = @lTmpAmount + @lTmpAdditionalAmount

		DECLARE @lAssignedLiters DECIMAL(16, 2) = @pTmpLiters + @lTmpAdditionalLiters
		
		DECLARE @lAvailable DECIMAL(16, 2) = (@lTmpAmount + @lTmpAdditionalAmount) - @lRealAmount

		DECLARE @lAvailableLiters DECIMAL(16, 3) = (@pTmpLiters + @lTmpAdditionalLiters) - @lRealLiters

		DECLARE @lTotalAssigned DECIMAL(16, 2) = @lFuelAssigned + @lAssigned

		SELECT @lAvailable 
		      ,@lAssigned
			  ,@lAssignedLiters 
			  ,@lRealAmount
			  ,@lRealLiters 
			  ,@lTotalAssigned 
			  ,@lTotal
			  ,@lCreditCardId
			  ,@lAvailableLiters
END
GO


