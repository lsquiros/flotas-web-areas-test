USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_AlarmOdometerCount_Retrieve]    Script Date: 05/06/2020 3:33:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Stefano Quiros
-- Create date: 4/21/2016
-- Description:	Retrieve Alarm Odometer Count
-- Modify By: Stefano Quir�s - Add the Time Zone Parameter - 12/06/2016
-- Modify by Stefano Quir�s - 14/11/2017 - Add Void Transactions Validation
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- ================================================================================================

ALTER Procedure [Control].[Sp_AlarmOdometerCount_Retrieve]
(
	 @pCustomerId INT = NULL
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pParameter DECIMAL = NULL
	,@pUserId INT = NULL					
)
AS
BEGIN

	SET NOCOUNT ON	

	
	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	DECLARE @lTimeZoneParameter INT 

	DECLARE @tmpOdometerCount AS TABLE(
		[Date] DATETIME,
		[VehicleName] NVARCHAR(520),
		[PlateId] VARCHAR(250),
		[OdometerLast] INT,
		[Odometer] INT,
		[LitersLast] DECIMAL, 
		[Liters] DECIMAL,
		[FuelAmount] DECIMAL,
		[DefaultPerformance] DECIMAL,
		[TransactionId] INT
	)

	SELECT @lTimeZoneParameter = [TimeZone]	FROM [General].[Countries] co
								 INNER JOIN [General].[Customers] cu
								 ON co.[CountryId] = cu.[CountryId] 
								 WHERE cu.[CustomerId] = @pCustomerId

	INSERT INTO @tmpOdometerCount 
	SELECT 				
		DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date],
		v.[Name] [VehicleName],
		v.[PlateId],
		(SELECT TOP 1 TT.[Odometer] FROM(
			SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
			WHERE NT.[VehicleId] = t.[VehicleId] AND NT.[Date] <= t.[Date] ORDER BY NT.[Date] DESC ) TT ORDER BY TT.[Date] ASC) AS [OdometerLast],
		t.[Odometer],
		(SELECT TOP 1 TT.[Liters] FROM(
			SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
			WHERE NT.[VehicleId] = t.[VehicleId] AND NT.[DATE] <= t.[InsertDate] ORDER BY NT.[Date] DESC ) TT ORDER BY TT.[Date] ASC) AS [LitersLast],	
		t.[Liters],				
		t.[FuelAmount],	
		vc.[DefaultPerformance],	
		t.[TransactionId]
	FROM [Control].[Transactions] t
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]	
	INNER JOIN [General].[VehicleCategories] vc	
		ON v.[VehicleCategoryId] = vc.[VehicleCategoryId] 				
	WHERE (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER	
	AND c.[CustomerId] = @pCustomerId 
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
	AND 1 > (SELECT ISNULL(COUNT(1), 0) 
		     FROM Control.Transactions t2
			 WHERE t2.[CreditCardId] = t.[CreditCardId] 
			 AND t2.[TransactionPOS] = t.[TransactionPOS] 
			 AND t2.[ProcessorId] = t.[ProcessorId] 
			 AND (t2.IsReversed = 1 OR t2.IsVoid = 1)
			 )		
	AND (@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear)
					
	-- RETURN RESULTS
	SELECT ISNULL(SUM(1), 0) Count 
	FROM @tmpOdometerCount 
	WHERE CONVERT(DECIMAL(16,2),(([Odometer] - [OdometerLast]) / [Liters]), 2) >= CONVERT(DECIMAL(16,2),ROUND(ISNULL([DefaultPerformance],0) + ([DefaultPerformance] * ISNULL(@pParameter, 0)), 2))
    OR CONVERT(DECIMAL(16,2),(([Odometer] - [OdometerLast]) / [Liters]), 2) <= CONVERT(DECIMAL(16,2),ROUND(ISNULL([DefaultPerformance],0) - ([DefaultPerformance] * ISNULL(@pParameter, 0)), 2))	
	SET NOCOUNT OFF
END


GO


