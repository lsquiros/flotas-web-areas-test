USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_TransactionsInProcess_Retrieve]    Script Date: 05/06/2020 2:51:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [Control].[Sp_TransactionsInProcess_Retrieve]
(
  @pCreditCardNumber nvarchar(520)
)
AS
BEGIN
 
 SET NOCOUNT ON 

 DECLARE @lStatus INT 
 DECLARE @lValue BIT 

 SET @lValue = ISNULL((SELECT p.[Value]
        FROM [General].[ParametersByCustomer] p
        RIGHT JOIN [Control].[CreditCard] c
        ON p.[CustomerId] = c.[CustomerId] 
        WHERE c.[CreditCardNumber] = @pCreditCardNumber
        AND p.[Name] = 'AllowProcess'
        AND p.[ResuourceKey] = 'ALLOWINPROCESSTRANSACTIONS'), 0)
 IF @lValue = 1 
 BEGIN 
  SELECT @lStatus = [StatusId]
  FROM  [Control].[CreditCard]
  WHERE [CreditCardNumber] = @pCreditCardNumber

  IF @lStatus = 1 OR @lStatus = 5
   SELECT CAST(1 AS BIT)
  ELSE
   SELECT CAST(0 AS BIT)
 END   
 ELSE 
 BEGIN
  SELECT CAST(0 AS BIT)
 END  
 
    SET NOCOUNT OFF
END
GO


