USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_TransactionsDetailedReport_Retrieve]    Script Date: 05/06/2020 3:34:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 2/2/2016
-- Description:	Retrieve TransactionsDetailReport information
-- Modify By: Stefano Quir�s - Add the Time Zone Parameter - 12/06/2016 - Add the DriverId and 
-- DriverName
-- Modify by Henry Retana - 19/09/2017
-- Use function to get the driver id and driver name 
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Modify by Henry Retana - 29/01/2018
-- Validates Invoice Column
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- ================================================================================================

ALTER PROCEDURE [Control].[Sp_TransactionsDetailedReport_Retrieve]
(
	@pCustomerId INT,					--@pCustomerId: CustomerId		
	@pYear INT = NULL,					--@pYear: Year
	@pMonth INT = NULL,					--@pMonth: Month
	@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
	@pEndDate DATETIME = NULL,			--@pEndDate: End Date
	@pKey VARCHAR(800) = NULL,			--@pKey :Key
	@pUserId INT						--@pUserId: User Id						
)
AS
BEGIN

	SET NOCOUNT ON
	
	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	DECLARE @lTimeZoneParameter INT 	
	DECLARE @lConvertionValue DECIMAL(16, 5) = 3.78
	DECLARE @lPartnerUnit INT

	SELECT @lTimeZoneParameter = [TimeZone]	
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	SELECT @lPartnerUnit = [CapacityUnitId] 
	FROM [General].[Partners] p
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[PartnerId] = p.[PartnerId]
	WHERE [CustomerId] = @pCustomerId

	SELECT  DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date],
			v.[Name] [VehicleName],
			v.[PlateId],
			cc.[CreditCardNumber],
			e.[Code] AS [CostCenter],
			ISNULL((SELECT TOP 1 u.[Name]
					FROM [General].[Users] u
					INNER JOIN [General].[DriversUsers] du
					ON u.[UserId] = du.[UserId]
					WHERE du.[CustomerId] = c.[CustomerId] 
					AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 0)) [DriverName],
			ISNULL((SELECT TOP 1 du.[Identification]
					FROM [General].[Users] u
					INNER JOIN [General].[DriversUsers] du
					ON u.[UserId] = du.[UserId]
					WHERE du.[CustomerId] = c.[CustomerId] 
					AND du.[Code] = t.[DriverCode]), [General].[Fn_GetDriverInformationBasedOnDate](v.[VehicleId], t.[InsertDate], 1)) [DriverId],		
			(SELECT TOP 1 TT.[Odometer] FROM(
				SELECT TOP 2 NT.* FROM [Control].[Transactions] NT
				WHERE NT.[VehicleId] = t.[VehicleId] AND NT.[Date] <= t.[Date] ORDER BY NT.[Date] DESC ) TT ORDER BY TT.[Date] ASC) AS [OdometerLast],
			t.[Odometer],
			  CASE WHEN @lPartnerUnit = 1 THEN (SELECT TOP 1 TT.[Liters] 
			 FROM(SELECT TOP 2 NT.* 
			      FROM [Control].[Transactions] NT
		    	  WHERE NT.[VehicleId] = t.[VehicleId] 
				  AND NT.[DATE] <= t.[InsertDate] 
				  ORDER BY NT.[Date] DESC ) TT 
			 ORDER BY TT.[Date] ASC) * @lConvertionValue
			 ELSE (SELECT TOP 1 TT.[Liters] 
			 FROM(SELECT TOP 2 NT.* 
			      FROM [Control].[Transactions] NT
		    	  WHERE NT.[VehicleId] = t.[VehicleId] 
				  AND NT.[DATE] <= t.[InsertDate] 
				  ORDER BY NT.[Date] DESC ) TT 
			 ORDER BY TT.[Date] ASC)
			 END [LitersLast],	
		    CASE WHEN @lPartnerUnit = 1 THEN t.[Liters] * @lConvertionValue
				 ELSE t.[Liters] 
				 END [Liters],	
			t.[TransactionPOS] AS [Reference],
			t.[FuelAmount],
			f.[Name] AS [FuelName],
			t.[MerchantDescription] AS [ServiceStation],
			t.[TransactionPOS] AS [Reference],
			DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [FechaValor],
			ISNULL(t.[Invoice], '-') AS [Invoice]
	FROM [Control].[Transactions] t
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]		
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]
	INNER JOIN [Control].[Fuels] f 
		ON t.[FuelId] = f.[FuelId]
	INNER JOIN [Control].[CreditCardByVehicle] cv 
		ON t.[CreditCardId] = cv.[CreditCardId]
	INNER JOIN [Control].[Currencies] g 
		ON c.[CurrencyId] = g.[CurrencyId]
	INNER JOIN [Control].[CreditCard] cc
		ON cc.CreditCardId = t.CreditCardId
	LEFT JOIN [General].[VehicleCostCenters] e
		ON t.CostCenterId = e.CostCenterId
	WHERE (@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
	AND c.[CustomerId] = @pCustomerId
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
	AND 1 > (SELECT ISNULL(COUNT(1), 0) 
			 FROM [Control].[Transactions] t2
			 WHERE t2.[CreditCardId] = t.[CreditCardId] 
			 AND t2.[TransactionPOS] = t.[TransactionPOS] 
			 AND t2.[ProcessorId] = t.[ProcessorId] 
			 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))		
	AND (@pKey IS NULL 
		OR e.[Code] like '%'+@pKey+'%')				
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear) OR
		(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
				AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))	
	ORDER BY e.[Code] DESC

	SET NOCOUNT OFF
END

GO


