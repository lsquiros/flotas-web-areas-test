USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_Retrieve]    Script Date: 05/06/2020 3:00:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Author:  Berman Romero L.  
-- Create date: 09/11/2014  
-- Description: Retrieve CreditCard information  
-- Dinamic Filter - 1/22/2015 - Melvin Salas  
-- Modify: Henry Retana  7/14/16  
-- Preauthorized  filter by creditcarNumber  
-- Modify: Henry Retana  14/9/17  
-- Modify the retrieve based on the status   
-- Modify: Kevin Pena  24/10/17  
-- Modify WHERE sentence when CustomerId is NULL (Coinca)  
-- Modify: Esteban Sol�s  15/12/2017  
-- Added parameter @pDriverId
-- Modify: �lvaro Zamora  12/07/2018  
-- Added [InsertDate] and INNER JOIN 'j' with Countries table and Country's Name.
-- Modify: �lvaro Zamora  26/07/2018  
-- Se cambia el nombre del resultado de tipo de tarjeta de cr�dito, de 'C' a 'Cr�dito' y de 'D' a 'Debito'.
-- Stefano Quir�s - Add AvailableLiters to the retrieve - 14/01/2019
-- ================================================================================================  
ALTER PROCEDURE [Control].[Sp_CreditCard_Retrieve] --30, null, null, null, 1, 7
(
	 @pCustomerId INT = NULL 
	,@pCreditCardId INT = NULL
	,@pKey VARCHAR(800) = NULL
	,@pDriverId INT = NULL 
	,@pUserId INT 
	,@pStatusId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lStatus INT = 1
	-- DYNAMIC FILTER  
	DECLARE @Results TABLE (items INT)
	DECLARE @count INT

	INSERT @Results
	EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId

	SET @count = (
					SELECT COUNT(*)
					FROM @Results
				  )

	-- END  
	IF @pCustomerId IS NULL
	BEGIN
		SELECT @pCustomerId = [CustomerId]
		FROM [Control].[CreditCard]
		WHERE [CreditCardNumber] = @pKey

		IF @pCustomerId IS NULL
		BEGIN
			SELECT @pCustomerId = [CustomerId]
			FROM [General].[Vehicles]
			WHERE [PlateId] = @pKey
		END
	END

	DECLARE @lStatusTable TABLE ([Id] INT)

	INSERT INTO @lStatusTable
	VALUES (9)

	IF @pCustomerId = 0
	BEGIN
		IF ISNULL((
					SELECT p.[Value]
					FROM [General].[ParametersByCustomer] p
					RIGHT JOIN [Control].[CreditCard] c ON p.[CustomerId] = c.[CustomerId]
					WHERE c.[CreditCardNumber] = @pKey
						AND p.[Name] = 'AllowProcess'
						AND p.[ResuourceKey] = 'ALLOWINPROCESSTRANSACTIONS'
					), 0) = 0
		BEGIN
			INSERT INTO @lStatusTable
			VALUES (@lStatus)
		END
	END

	SELECT	 a.[CreditCardId]
			,a.[CustomerId]
			,c.[Name] AS [EncryptedCustomerName]
			,a.[CreditCardNumber]
			,a.[ExpirationYear]
			,a.[ExpirationMonth]
			,a.[CreditAvailable]
			,a.[AvailableLiters]
			,a.[StatusId]
			,b.[RowOrder] AS [Step]
			,b.[Name] AS [StatusName]
			,d.[Symbol] AS [CurrencySymbol]
			,c.[IssueForId]
			,e.[UserId]
			,g.[VehicleId]
			,CASE c.[CreditCardType]
				WHEN 'C' THEN 'Cr�dito'
				WHEN 'D' THEN 'D�bito'
				ELSE 'Cr�dito'
			END AS [CreditCardType]
			,f.Name AS [EncryptedDriverName]
			,'Veh�culo: ' + CASE WHEN c.[CustomerId] = 30 
							THEN h.[Name] + ' - ' + h.[PlateId]
							ELSE h.[PlateId]
							END AS [VehiclePlate]
			,a.CardRequestId
			,i.[EstimatedDelivery]
			,i.[InsertDate]
			,a.[RowVersion]
			,b.[Usage]
			,ISNULL((
						SELECT TOP 1 p.[PartnerId]
						FROM [General].[CustomersByPartner] p
						WHERE p.[CustomerId] = a.[CustomerId]
					), 0) [PartnerId]
			,j.[Name] AS [Country]
	FROM [Control].[CreditCard] a
	INNER JOIN [General].[Status] b 
		ON b.[StatusId] = a.[StatusId]
	INNER JOIN [General].[Customers] c 
		ON c.[CustomerId] = a.[CustomerId]
	INNER JOIN [Control].[Currencies] d 
		ON d.[CurrencyId] = c.[CurrencyId]
	LEFT JOIN [Control].[CreditCardByDriver] e 
		ON e.[CreditCardId] = a.[CreditCardId]
	LEFT JOIN [General].[Users] f 
		ON f.[UserId] = e.[UserId]
	LEFT JOIN [Control].[CreditCardByVehicle] g 
		ON g.[CreditCardId] = a.[CreditCardId]
	LEFT JOIN [General].[Vehicles] h 
		ON h.[VehicleId] = g.[VehicleId]
	INNER JOIN [Control].[CardRequest] i 
		ON a.[CardRequestId] = i.[CardRequestId]
	INNER JOIN [General].[Countries] j
		ON c.[CountryId] = j.[CountryId]
	WHERE (@pCustomerId = 0 OR (@pCustomerId = 0 AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')
		   OR (@pCustomerId IS NULL AND b.[Usage] = 'CUSTOMER_CREDIT_CARD_SUPER_ADMIN')
		   OR a.[CustomerId] = @pCustomerId)
	AND (@count = 0 OR g.[VehicleId] IN (
											SELECT items
											FROM @Results
										)) -- DYNAMIC FILTER  
	AND (@pCreditCardId IS NULL OR a.[CreditCardId] = @pCreditCardId)
	AND (@pKey IS NULL
		OR f.[Name] LIKE '%' + @pKey + '%'
		OR h.[PlateId] LIKE '%' + @pKey + '%'		
		OR Convert(VARCHAR(50), a.[CreditAvailable]) LIKE '%' + @pKey + '%'
		OR Convert(VARCHAR(50), a.[ExpirationYear]) LIKE '%' + @pKey + '%'
		OR a.[CreditCardNumber] = @pKey
		OR j.[Name] LIKE '%' + @pKey + '%')
	AND (@pDriverId IS NULL OR @pDriverId = e.[UserId])
	AND a.[StatusId] NOT IN (
								SELECT [Id]
								FROM @lStatusTable
							 )
	AND (@pStatusId = - 1 OR @pStatusId IS NULL OR a.[StatusId] = @pStatusId)
	ORDER BY [CreditCardId] DESC

	SET NOCOUNT OFF
END

GO


