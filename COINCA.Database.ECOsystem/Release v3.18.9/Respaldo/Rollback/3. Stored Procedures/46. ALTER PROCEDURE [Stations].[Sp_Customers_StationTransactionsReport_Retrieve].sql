USE ECOsystem
/****** Object:  StoredProcedure [Stations].[Sp_Customers_StationTransactionsReport_Retrieve]    Script Date: 05/06/2020 3:41:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 06/04/2017
-- Description:	Obtiene la información para las transacciones en estación de servicio
-- Modify by Henry Retana - 29/9/2017
-- Add new function to get the service station info
-- Modify by: Stefano Quiros 13/04/2016 - 14/11/2017 - Add Void Transactions Validation
-- ================================================================================================

ALTER PROCEDURE [Stations].[Sp_Customers_StationTransactionsReport_Retrieve] 
(
	@pCustomerId INT = NULL,
	@pServiceStationId INT = NULL,
	@pXMLServiceStationIds VARCHAR(MAX) = NULL,
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL	
)
AS
BEGIN

	SET NOCOUNT ON	
	
	DECLARE @lxmlData XML = CONVERT(XML, @pXMLServiceStationIds)
	CREATE TABLE #ServiceStations ([ServiceStationId] INT)

	IF @pCustomerId = -1 SET @pCustomerId = NULL

	--Valida si el id de la estacion de servicio viene individual o de forma masiva
	IF @pServiceStationId IS NULL
	BEGIN
		INSERT INTO #ServiceStations
		SELECT Row.col.value('./@ServiceStationId', 'INT') 
		FROM @lxmlData.nodes('/xmldata/ServiceStations') Row(col)
	END
	ELSE
	BEGIN
		INSERT INTO #ServiceStations
		VALUES ( @pServiceStationId )
	END	

	SELECT DISTINCT c.[CustomerId]
				   ,c.[Name] [EncryptName]
	FROM [Control].[Transactions] t
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]	
	LEFT OUTER JOIN [General].[Countries] co
		ON t.[CountryCode] = co.[Code]
	WHERE (@pCustomerId IS NULL OR c.[CustomerId] = @pCustomerId)
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
	AND  1 > (SELECT ISNULL(COUNT(1), 0) 
			  FROM [Control].[Transactions] t2
			  WHERE t2.[CreditCardId] = t.[CreditCardId] 
			  AND	t2.[TransactionPOS] = t.[TransactionPOS] 
			  AND	t2.[ProcessorId] = t.[ProcessorId] 
			  AND  (t2.IsReversed = 1 OR t2.IsVoid = 1) 
			 )						
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, co.[TimeZone], t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, co.[TimeZone], t.[InsertDate])) = @pYear) OR
		(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
				AND DATEADD(HOUR, co.[TimeZone], t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
	AND (t.[ProcessorId] IN (SELECT tss.[TerminalId]
							 FROM [General].[ServiceStations] ss
							 INNER JOIN [General].[TerminalsByServiceStations] tss
							 	ON ss.[ServiceStationId] = tss.[ServiceStationId]
							 WHERE ss.[ServiceStationId] IN (SELECT [ServiceStationId]
															 FROM #ServiceStations))
		OR t.[ProcessorId] IN (SELECT ss.[Terminal]
							   FROM [General].[ServiceStations] ss							
							   WHERE ss.[ServiceStationId] IN (SELECT [ServiceStationId]
															   FROM #ServiceStations)))
	ORDER BY c.[CustomerId]

	DROP TABLE #ServiceStations
	SET NOCOUNT OFF
END


GO


