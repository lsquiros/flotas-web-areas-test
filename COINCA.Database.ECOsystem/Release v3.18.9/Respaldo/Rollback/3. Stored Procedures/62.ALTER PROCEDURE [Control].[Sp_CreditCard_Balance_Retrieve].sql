USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_CreditCard_Balance_Retrieve]    Script Date: 05/06/2020 3:58:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CreditCard Balance information
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_CreditCard_Balance_Retrieve]
(
	  @pCreditCardNumber NVARCHAR(520)		--@pCustomerId: Customer Id
	 ,@pExpirationYear INT = NULL			--@pExpirationYear: Expiration Year
	 ,@pExpirationMonth INT	= NULL			--@pExpirationMonth: Expiration Month
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	SELECT a.[CreditAvailable]
    FROM [Control].[CreditCard] a
	WHERE a.[CreditCardNumber] = @pCreditCardNumber
	AND (@pExpirationYear IS NULL OR a.[ExpirationYear] = @pExpirationYear)
	AND (@pExpirationMonth IS NULL OR a.[ExpirationMonth] = @pExpirationMonth)
	  
    SET NOCOUNT OFF
END
GO


