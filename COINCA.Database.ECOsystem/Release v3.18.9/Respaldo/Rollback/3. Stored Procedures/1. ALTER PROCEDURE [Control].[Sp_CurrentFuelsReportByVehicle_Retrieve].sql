/****** Object:  StoredProcedure [Control].[Sp_CurrentFuelsReportByVehicle_Retrieve]    Script Date: 05/06/2020 2:34:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================      
-- Author:  Berman Romero L.      
-- Create date: 10/21/2014      
-- Description: Retrieve Current Fuels Report By Vehicle information      
-- Dinamic Filter - 1/22/2015 - Melvin Salas      
-- Modify by: Stefano Quiros 13/04/2016      
-- Modify by: Henry Retana - 09/06/2017      
-- Modifica el proceso para obtener los vehiculos      
-- Modify by: Marco Cabrera Chaves 30-03-2017      
-- Agrega la condicion para que no se muestren los vehiculos eliminados      
-- Modify By:   Albert Estrada Oct/17/2017      
-- Description: Add Columns      
-- Modify By:   Esteban Solis Nov/08/2017      
-- Description: Add Columns for excel export     
-- Modify by: Stefano Quiros 13/04/2016 - 14/11/2017 - Add Void Transactions Validation    
-- Modify by Esteban Sol�s - 30-01-2018 -  Add column Litters and change calculation of real litters 
-- Modify by Esteban Sol�s - 09-02-2018 - Fix issue with % showing zeros only
-- Stefano Quir�s - Add the Available liters to the retrieve - 16/01/2019
-- Modify By: Juan k Santamaria Fix add convert Lt to Gal AvailableLiters, Date: 17-03-2020
-- ================================================================================================
      
ALTER PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicle_Retrieve] --11224,2020, 1, null, null, 1
(
	 @pCustomerId INT --@pCustomerId: Customer Id      
	,@pYear INT = NULL --@pYear: Year      
	,@pMonth INT = NULL --@pMonth: Month      
	,@pStartDate DATETIME = NULL --@pStartDate: Start Date      
	,@pEndDate DATETIME = NULL --@pEndDate: End Date      
	,@pUserId INT --@pUserId: User Id
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lInsertDate DATE = CASE WHEN @pStartDate IS NULL 
									THEN DATEADD(YEAR, @pYear-1900, DATEADD(MONTH, @pMonth-1, DATEADD(day, 1-1, 0)))
								ELSE @pStartDate
								END

	DECLARE @lFinalDate DATE = CASE WHEN @pEndDate IS NULL 
									THEN DATEADD(YEAR, @pYear-1900, DATEADD(MONTH, (@pMonth + 1)-1, DATEADD(day, 1-1, 0)))
								ELSE DATEADD(DAY, 1, @pEndDate) 
								END


	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	-- DYNAMIC FILTER      
	DECLARE @Results TABLE (items INT)
	DECLARE @count INT
	DECLARE @lTimeZoneParameter INT = 0 
	DECLARE @lCustomerCapacityUnit INT
	DECLARE @lEquival DECIMAL(18,8) = 3.78541178


	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu 
	ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId

	INSERT @Results
	EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId

	SET @count = (
			SELECT COUNT(1)
			FROM @Results
			)

	-- END   

	SELECT @lCustomerCapacityUnit = c.[UnitOfCapacityId]
	FROM [General].[Customers] c
    WHERE c.[CustomerId] = @pCustomerId

	
	IF (@lCustomerCapacityUnit = 0) SET @lEquival = 1

	SELECT @lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
	INNER JOIN [General].[Customers] b ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId

	SELECT fd.[VehicleId], MAX([Id]) [Id]
	INTO #VehiclesDistributionIds 
	FROM [Control].[FuelDistribution] fd
	INNER JOIN [General].[Vehicles] v
		ON v.[VehicleId] = fd.[VehicleId]
	WHERE [CustomerId] = @pCustomerId
	AND fd.[InsertDate] >= @lInsertDate 
	AND fd.[InsertDate] < @lFinalDate
	GROUP BY fd.[VehicleId], CONVERT(DATE, fd.[InsertDate])

	SELECT fd.[Amount], fd.[AdditionalAmount], fd.[Liters], fd.[AdditionalLiters], fd.[VehicleId]
	INTO #VehiclesDistribution
	FROM #VehiclesDistributionIds vd
	INNER JOIN [Control].[FuelDistribution] fd
		ON vd.[Id] = fd.[Id]

	SELECT v.VehicleId AS [VehicleId]
		  ,v.Active
		  ,v.[PlateId] AS [PlateNumber]
		  ,v.[Name] AS [VehicleName]
		  ,ISNULL(CONVERT(DECIMAL (16, 2), [Control].[Fn_FuelDistribution_TransactionsAmount](cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate,  c.FuelId), 2), 0) [RealAmount]  		    
		  ,(SELECT ISNULL(Convert(Decimal(16,2), SUM([Amount]), 2), 0) 
		    FROM #VehiclesDistribution
		    WHERE [VehicleId] = v.VehicleId) [AssignedAmount] 		       
		  ,CASE WHEN (SELECT Convert(Decimal(16,2), ISNULL(SUM([Amount]), 0) + ISNULL(SUM([AdditionalAmount]), 0), 2)
		  			FROM #VehiclesDistribution 
		  			WHERE [VehicleId] = v.[VehicleId]) = 0
		   THEN 0
		   ELSE ISNULL(CONVERT(DECIMAL(16, 2), ROUND(ISNULL([Control].[Fn_FuelDistribution_TransactionsAmount](cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate,  c.FuelId), 0) / (SELECT Convert(Decimal(16,2), ISNULL(SUM([Amount]), 0) + ISNULL(SUM([AdditionalAmount]), 0), 2)
		  																																											   FROM #VehiclesDistribution
		  																																											   WHERE [VehicleId] = v.[VehicleId]), 2) * 100), 0) 
		  END [RealAmountPct]  
		  ,@lCurrencySymbol AS [CurrencySymbol]
		  ,v.CostCenterId
		  ,(SELECT ISNULL(Convert(Decimal(16,2), SUM([Liters]) / @lEquival, 2), 0) 
		    FROM #VehiclesDistribution 
		    WHERE [VehicleId] = v.[VehicleId])  [AssignedLiters]  
		  ,cc.[CreditCardId]
		  ,@lTimeZoneParameter
		  ,@lInsertDate
		  ,@lFinalDate
		  ,ISNULL(CONVERT(DECIMAL (16, 2), [Control].[Fn_FuelDistribution_TransactionsLiters](cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate, 0, c.FuelId), 2),0) [RealLiters]   
		  ,ISNULL(CONVERT(DECIMAL (16, 2), [Control].[Fn_FuelDistribution_TransactionsLiters](cc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, @lFinalDate, 1, c.FuelId), 2),0) [Liters]   
		  ,(SELECT ISNULL(Convert(Decimal(16,2), SUM([AdditionalAmount]), 2), 0) 
		    FROM #VehiclesDistribution 
		    WHERE [VehicleId] = v.[VehicleId]) [AditionalAmount]      		
		  ,(SELECT ISNULL(Convert(Decimal(16,2), SUM([AdditionalLiters]) / @lEquival, 2), 0) 
		    FROM #VehiclesDistribution 
		    WHERE [VehicleId] = v.[VehicleId]) [AditionalLiters]   		      
		  ,f.[Name] [FuelName]
		  ,vbg.[VehicleGroupId]
		  ,vcc.[UnitId]
		  ,vg.[Name] [VehicleGroupName]
		  ,cc.[CreditAvailable] [AvailableBalance]
		  ,CONVERT(DECIMAL(18,3),(cc.[AvailableLiters] / @lEquival)) [AvailableLiters]
		  ,@lCustomerCapacityUnit [CustomerCapacityUnit]
	FROM [General].[Vehicles] v
	INNER JOIN [Control].[CreditCardByVehicle] ccv
		ON ccv.[VehicleId] = v.[VehicleId]
	INNER JOIN [Control].[CreditCard] cc
		ON cc.[CreditCardId] = ccv.[CreditCardId]
			AND cc.[StatusId] IN (7, 8)
	INNER JOIN (SELECT vc.[DefaultFuelId] [FuelId]
					      ,v.[VehicleId]
						  ,vc.[VehicleModel]
					FROM [General].[Vehicles] v	
					INNER JOIN [General].[VehicleCategories] vc 
						ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
					INNER JOIN [Control].[Fuels] f	
						ON f.[FuelId] = vc.[DefaultFuelId]
					WHERE v.[CustomerId] = @pCustomerId
					UNION
					SELECT fvc.[FuelId] 
					      ,v.[VehicleId]
						  ,vc.[VehicleModel]
					FROM [General].[Vehicles] v
					INNER JOIN [General].[VehicleCategories] vc
						ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
					INNER JOIN [General].[FuelsByVehicleCategory] fvc
						ON fvc.[VehicleCategoryId] = vc.[VehicleCategoryId]
					INNER JOIN [Control].[Fuels] f	
						ON f.[FuelId] = fvc.[FuelId]
					WHERE v.[CustomerId] = @pCustomerId
				   ) c
		ON c.VehicleId = v.VehicleId
	LEFT JOIN [General].[VehiclesByGroup] vbg 
		ON v.VehicleId = vbg.VehicleId
	LEFT JOIN [General].[VehicleGroups] vg 
		ON vg.VehicleGroupId = vbg.VehicleGroupId
	LEFT JOIN [General].[VehicleCostCenters] vcc 
		ON v.CostCenterId = vcc.CostCenterId
	INNER JOIN [control].[fuels] f 
		ON f.FuelId = c.FuelId
	WHERE (
			@count = 0
			OR v.[VehicleId] IN (
				SELECT items
				FROM @Results
				)
			) -- DYNAMIC FILTER      
		AND v.[CustomerId] = @pCustomerId
		AND (
			v.isDeleted = 0
			OR v.isDeleted IS NULL
			)
	SET NOCOUNT OFF
END
GO


