USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve]    Script Date: 05/06/2020 3:28:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================  
-- Author:  Berman Romero L.  
-- Create date: 10/21/2014  
-- Description: Retrieve Current Fuels Report By Vehicle information  
-- Modify By: Stefano Quiros Ruiz 13/04/2016 -- Add the condition to retrieve the correct fuel price  
-- Modify by Stefano Quir�s - 14/11/2017 - Add Void Transactions Validation  
-- Modify by Esteban Sol�s - 30-01-2018 -  Add column Litters and change calculation of real litters
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- Juan K Santamaria - 13/2/2020 - if Client as been Configured on Galons, Convert Liters Reals to Galons --JSA-001
--                                 UnitOfCapacityId = 1 is Galons
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_AccumulatedFuelsReportByVehicle_Retrieve] 
(
	 @pCustomerId INT
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
	,@pUserId INT
	,@pFlag INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lExchangeRate DECIMAL(16, 8) = 1.0
	DECLARE @count INT = NULL
	DECLARE @GalonsConvert DECIMAL(16, 6) = 0.264172
	DECLARE @UnitOfCapacityId INT = (Select UnitOfCapacityId from General.Customers where CustomerId = @pCustomerId)

	--Verifies if the SP is nested or is not  
	IF @pFlag = 1
	BEGIN
		-- DYNAMIC FILTER  
		DECLARE @Results TABLE (items INT)

		INSERT @Results
		EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId

		SET @count = (
				SELECT COUNT(*)
				FROM @Results
				)
			-- END  
	END
	ELSE
	BEGIN
		SET @count = 0
	END

	SELECT @lExchangeRate = a.[ExchangeRate]
	FROM [Control].[PartnerCurrency] a
	INNER JOIN [General].[CustomersByPartner] b 
		ON a.[PartnerId] = b.[PartnerId]
	INNER JOIN [General].[Customers] c 
		ON c.[CustomerId] = b.[CustomerId]
		AND c.[CurrencyId] = a.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	AND a.EndDate IS NULL;

	WITH TmpValues(Number) AS 
	(
		SELECT 0
		UNION ALL
		SELECT Number + 1
		FROM TmpValues
		WHERE Number < 100
	)
	SELECT q.[VehicleId]
		  ,q.[PlateNumber]
		  ,q.[VehicleName]
		  ,q.[Year]
		  ,q.[Month]
		  ,SUM(q.[Liters]) AS [Liters]
	      ,SUM(q.[RealLiters]) AS [RealLiters]
	FROM (
			SELECT a.[VehicleId]
				  ,a.[PlateId] AS [PlateNumber]
				  ,a.[Name] AS [VehicleName]
				  ,s.[Month]
				  ,s.[Year]
				  ,s.Liters
				  ,CASE WHEN @UnitOfCapacityId = 1 
				   THEN (s.RealLiters * @GalonsConvert) 
				   ELSE s.RealLiters 
				   END AS RealLiters
			FROM [General].[Vehicles] a
			INNER JOIN [General].[VehicleCategories] b 
				ON b.[VehicleCategoryId] = a.[VehicleCategoryId]
			INNER JOIN 
			(
				SELECT y.[VehicleId]
					,DATEPART(m, x.[Date]) AS [Month]
					,DATEPART(yyyy, x.[Date]) AS [Year]
					,SUM(x.[FuelAmount]) AS [RealAmount]
					,SUM(ISNULL(x.[RealLiters],0.0)) AS [RealLiters]
					,SUM(ISNULL(x.[Liters],0.0)) AS [Liters]
				FROM [Control].[Transactions] x
				INNER JOIN [General].[Vehicles] y ON x.[VehicleId] = y.[VehicleId]
				WHERE y.[CustomerId] = @pCustomerId
				AND (@count = 0
						OR x.[VehicleId] IN (
											SELECT items
											FROM @Results
											)
						) -- DYNAMIC FILTER  
				AND x.[IsFloating] = 0
				AND x.[IsReversed] = 0
				AND x.[IsDuplicated] = 0
				AND (x.[IsDenied] IS NULL OR x.[IsDenied] = 0)
				AND 1 > (
							SELECT COUNT(1)
							FROM CONTROL.Transactions t2
							WHERE t2.[CreditCardId] = x.[CreditCardId]
								AND t2.[TransactionPOS] = x.[TransactionPOS]
								AND t2.[ProcessorId] = x.[ProcessorId]
								AND (
									t2.IsReversed = 1
									OR t2.IsVoid = 1
									)
							)
				AND y.[Active] = 1
				AND (y.[IsDeleted] IS NULL OR y.[IsDeleted] = 0)
				AND (
					(
						@pYear IS NOT NULL
						AND @pMonth IS NOT NULL
						AND DATEPART(m, x.[Date]) = @pMonth
						AND DATEPART(yyyy, x.[Date]) = @pYear
						)
					OR (
						@pStartDate IS NOT NULL
						AND @pEndDate IS NOT NULL
						AND x.[Date] BETWEEN @pStartDate
							AND @pEndDate
						)
					)
				GROUP BY y.[VehicleId]
					,DATEPART(m, x.[Date])
					,DATEPART(yyyy, x.[Date])
			) s 
			ON s.[VehicleId] = a.[VehicleId]		
		UNION		
		SELECT a.[VehicleId]
			  ,a.[PlateId] AS [PlateNumber]
			  ,a.[Name] AS [VehicleName]
			  ,r.[Month]
			  ,r.[Year]
			  ,0 AS Liters
			  ,0
		FROM [General].[Vehicles] a
		CROSS JOIN 
		(
			SELECT DATEPART(m, DATEADD(m, x.[Number], @pStartDate)) AS [Month]
				,DATEPART(yyyy, DATEADD(m, x.[Number], @pStartDate)) AS [Year]
			FROM TmpValues x
			WHERE x.[Number] <= DATEDIFF(m, @pStartDate, @pEndDate)
			UNION
			SELECT @pMonth AS [Month]
				  ,@pYear AS [Year]
			WHERE @pMonth IS NOT NULL
			AND @pYear IS NOT NULL
		) r
		WHERE a.[CustomerId] = @pCustomerId
		AND (
			@count = 0
			OR a.[VehicleId] IN (
									SELECT items
									FROM @Results
								)
			) -- DYNAMIC FILTER  
		AND a.[Active] = 1
		AND (a.[IsDeleted] IS NULL OR a.[IsDeleted] = 0)
	) q
	GROUP BY q.[VehicleId]
		,q.[PlateNumber]
		,q.[VehicleName]
		,q.[Year]
		,q.[Month]

	SET NOCOUNT OFF
END
GO


