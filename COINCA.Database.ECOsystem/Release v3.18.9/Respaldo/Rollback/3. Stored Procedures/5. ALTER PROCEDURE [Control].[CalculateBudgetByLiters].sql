﻿USE ECOsystem
/****** Object:  StoredProcedure [Control].[CalculateBudgetByLiters]    Script Date: 05/06/2020 2:42:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

​
-- =================================================================================================  
-- Author:  Stefano Quirós  
-- Create date: 17/10/2017  
-- Description: Re-Calcula el presupuesto de acuerdo a los litros
-- ================================================================================================= 
-- Modify by Stefano Quirós - 01/05/2018 - Validate if price is null, put 1  
-- Modify by Esteban Solís  - 30/01/2018 - Added logic for customer with amount distribution. 
--										   Added logic for additional driver fuel
-- Modify by María de los Ángeles Jiménez Chvarría - DIC/24/2018 - Add credit card available liters.
-- =================================================================================================s  

ALTER PROCEDURE [Control].[CalculateBudgetByLiters]
(
	@pPartnerId INT
)
AS
BEGIN
	SET NOCOUNT ON
​
	CREATE TABLE #CustomersToProcess 
	(
		[CustomerId] INT
		,[IssueForId] INT
		,[DistributionType] INT
	)
​
	CREATE TABLE #VehiclesToProcess 
	(
		[VehicleId] INT
	)
​
	CREATE TABLE #DriversToProcess 
	(
		[DriverId] INT
	)
​
	CREATE TABLE #TableFuelsTemp 
	(
		[FuelByCreditId] INT NULL
		,[FuelId] INT NULL
		,[Year] INT NULL
		,[Month] INT NULL
		,[Total] DECIMAL(16, 2) NULL
		,[Assigned] DECIMAL(18, 0) NULL
		,[Available] DECIMAL(21, 2) NULL
		,[FuelName] VARCHAR(50) NULL
        ,[AssignedLiters] DECIMAL(16, 2)
		,[Liters] DECIMAL(16, 3) NULL
		,[LiterPrice] DECIMAL(16, 2) NULL
		,[CurrencySymbol] NVARCHAR(50)
		,[RowVersion] VARCHAR(200)
        ,[UnitOfCapacityId] INT
		,[UnitOfCapacity] VARCHAR(100)
	)
​
	CREATE TABLE #DistributionData
	(
		[Available] DECIMAL(16, 2)
	   ,[Assigned] DECIMAL(16, 2)
	   ,[RealAmount] DECIMAL(16, 2)
	   ,[TotalAssigned] DECIMAL(16,2)
	   ,[Total] DECIMAL(16, 2)
	   ,[CreditCardId] INT
	)
​
	DECLARE @lCustomerId INT = 0
	DECLARE @lIssueForId INT = 0 --Valida si la distribución es por vehiculos o por conductores  
	DECLARE @DISTRIBUTION_TYPE_LITER INT = 1
	DECLARE @DISTRIBUTION_TYPE_AMOUNT INT = 0
​
	INSERT INTO #CustomersToProcess
	SELECT	 c.[CustomerId]
			,c.[IssueForId]
			,ISNULL(pc.[Value], 0)
	FROM [General].[CustomersByPartner] cp
	INNER JOIN [General].[Customers] c 
		ON cp.[CustomerId] = c.[CustomerId]
	LEFT JOIN [General].[ParametersByCustomer] pc 
		ON c.[CustomerId] = pc.[CustomerId]
		AND pc.[Name] = 'TypeOfDistribution'
		AND pc.[ResuourceKey] = 'TYPEOFDISTRIBUTION'
	WHERE cp.[PartnerId] = @pPartnerId
​
	SELECT @lCustomerId = MIN([CustomerId])
	FROM #CustomersToProcess
​
	SELECT @lIssueForId = [IssueForId]
	FROM #CustomersToProcess
	WHERE [CustomerId] = @lCustomerId
​
	WHILE @lCustomerId IS NOT NULL
	BEGIN
		DECLARE @lVehicleId INT = 0
		       ,@lDriverId INT = 0
		       ,@lLiterPrice DECIMAL(16, 2)
		       ,@lYear INT = DATEPART(YEAR, GETDATE())
		       ,@lMonth INT = DATEPART(MONTH, GETDATE())
		       ,@lDistributionType INT = NULL
		       ,@lInsertDate DATE
			   ,@lTimeZoneParameter INT = NULL
​
		SELECT TOP 1 @lTimeZoneParameter = co.[TimeZone] 
		FROM [General].[Customers] cu
		INNER JOIN [General].[Countries] co 	
			ON co.[CountryId] = cu.[CountryId]
		WHERE cu.[CustomerId] = @lCustomerId
​
		SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@lCustomerId)
​
		SET @lDistributionType = (
				SELECT [DistributionType]
				FROM #CustomersToProcess
				WHERE [CustomerId] = @lCustomerId
				)
​
		INSERT INTO #TableFuelsTemp
		EXEC [Control].[Sp_FuelsByCredit_Retrieve] @lCustomerId
			,@lYear
			,@lMonth
​
		DECLARE @Tipo INT
		DECLARE @Equival DECIMAL(18, 8) = 3.78541178 -- CUSTOMER WITH GALLONS  
		DECLARE @PartnerCapacityUnit INT
​
		--SELECT @Tipo = [UnitOfCapacityId]
		--FROM [General].[Customers]
		--WHERE [CustomerId] = @lCustomerId
​
		SELECT @PartnerCapacityUnit = [CapacityUnitId]
		FROM [General].[Partners] p
		INNER JOIN [General].[CustomersByPartner] cbp ON p.[PartnerId] = cbp.[PartnerId]
		WHERE cbp.[CustomerId] = @lCustomerId
​
		--IF (@Tipo = 1 AND @PartnerCapacityUnit = 1)
		--   OR (@Tipo = 0 AND @PartnerCapacityUnit = 0)
		--	SET @Equival = 1 
​
		IF @lIssueForId = 101 OR @lIssueForId IS NULL
		BEGIN
			INSERT INTO #VehiclesToProcess
			SELECT [VehicleId]
			FROM [General].[Vehicles]
			WHERE [CustomerId] = @lCustomerId
			AND ([IsDeleted] IS NULL OR [IsDeleted] = 0)
			AND ([Active] IS NULL OR [Active] = 1)
​
			SELECT @lVehicleId = MIN([VehicleId])
			FROM #VehiclesToProcess
​
			WHILE @lVehicleId IS NOT NULL
			BEGIN
				DECLARE @lAmount FLOAT = 0
					   ,@lAdditionalAmount FLOAT = 0
					   ,@lRealAmount FLOAT = 0
					   ,@lLiters DECIMAL(16,3) = 0
					   ,@lAdditionalLiters DECIMAL(16,3) = 0
					   ,@lRealLiters INT = 0
					   ,@lCreditCardId INT = NULL					   
					   ,@lLastVehicleFuelId INT = NULL
​
				SELECT TOP 1 @lLiterPrice = ISNULL(tmp.[LiterPrice], 1)
											FROM #TableFuelsTemp tmp
											INNER JOIN [Control].[Fuels] f 
												ON f.[FuelId] = tmp.[FuelId]
											INNER JOIN [General].[VehicleCategories] vc 
												ON f.[FuelId] = vc.[DefaultFuelId]
											INNER JOIN [General].[Vehicles] v 
												ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
											WHERE tmp.[FuelId] = f.[FuelId]
												AND v.[VehicleId] = @lVehicleId
				
											--CASE  WHEN @PartnerCapacityUnit = 1
											--THEN (
											--		SELECT TOP 1 ISNULL(tmp.[LiterPrice] / @Equival, 1)
											--		FROM #TableFuelsTemp tmp
											--		INNER JOIN [Control].[Fuels] f 
											--			ON f.[FuelId] = tmp.[FuelId]
											--		INNER JOIN [General].[VehicleCategories] vc 
											--			ON f.[FuelId] = vc.[DefaultFuelId]
											--		INNER JOIN [General].[Vehicles] v 
											--			ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
											--		WHERE tmp.[FuelId] = f.[FuelId]
											--			AND v.[VehicleId] = @lVehicleId
											--	)
											--ELSE (
											--		SELECT TOP 1 ISNULL(tmp.[LiterPrice], 1)
											--		FROM #TableFuelsTemp tmp
											--		INNER JOIN [Control].[Fuels] f 
											--			ON f.[FuelId] = tmp.[FuelId]
											--		INNER JOIN [General].[VehicleCategories] vc 
											--			ON f.[FuelId] = vc.[DefaultFuelId]
											--		INNER JOIN [General].[Vehicles] v 
											--			ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
											--		WHERE tmp.[FuelId] = f.[FuelId]
											--			AND v.[VehicleId] = @lVehicleId
											--	)
											--END
​
				SELECT TOP 1 @lLastVehicleFuelId = [Id]
				FROM [Control].[FuelDistribution]
				WHERE [VehicleId] = @lVehicleId
				ORDER BY [InsertDate] DESC
​
				UPDATE [Control].[FuelDistribution]
				SET  [Amount] = CASE WHEN @lDistributionType = @DISTRIBUTION_TYPE_LITER
									 THEN ISNULL([Liters] * @lLiterPrice, 0)
									 ELSE ISNULL([Amount], 0)
							    END
					,[Liters] = CASE WHEN @lDistributionType = @DISTRIBUTION_TYPE_AMOUNT
									THEN ISNULL([Amount] / @lLiterPrice, 0)
									ELSE ISNULL([Liters], 0)
								END
					,[AdditionalAmount] = CASE  WHEN @lDistributionType = @DISTRIBUTION_TYPE_LITER
												THEN ISNULL([AdditionalLiters] * @lLiterPrice, 0)
												ELSE ISNULL([AdditionalAmount], 0)
										  END
					,[AdditionalLiters] = CASE  WHEN @lDistributionType = @DISTRIBUTION_TYPE_AMOUNT
												THEN ISNULL([AdditionalAmount] / @lLiterPrice, 0)
												ELSE ISNULL([AdditionalLiters], 0)
										 END
				WHERE [Id] = @lLastVehicleFuelId
					
				--Update Data			
				SELECT TOP 1 @lAmount = vf.[Amount]
							,@lAdditionalAmount = ISNULL(vf.[AdditionalAmount], 0)	
							,@lLiters = vf.[Liters]
							,@lAdditionalLiters = ISNULL(vf.[AdditionalLiters], 0)						
							,@lCreditCardId = cc.[CreditCardId]
				FROM [Control].[FuelDistribution] vf
				INNER JOIN [Control].[CreditCardByVehicle] cv 
					ON vf.[VehicleId] = cv.[VehicleId]
				INNER JOIN [Control].[CreditCard] cc 
					ON cc.[CreditCardId] = cv.[CreditCardId]
					AND cc.[StatusId] IN (7, 8)
				WHERE vf.[VehicleId] = @lVehicleId
				ORDER BY vf.[InsertDate] DESC
				
				SET @lRealAmount = [Control].[Fn_FuelDistribution_TransactionsAmount](@lCreditCardId, @lTimeZoneParameter, @lInsertDate, NULL, NULL)
				SET @lRealLiters = [Control].[Fn_FuelDistribution_TransactionsLiters](@lCreditCardId, @lTimeZoneParameter, @lInsertDate, NULL, 0, NULL)
​
				UPDATE [Control].[CreditCard]
				SET [CreditAvailable] = (@lAmount + @lAdditionalAmount) - @lRealAmount,
				[AvailableLiters] = (@lLiters + @lAdditionalLiters) - @lRealLiters
				WHERE [CreditCardId] = @lCreditCardId
​
				DELETE FROM #VehiclesToProcess
				WHERE [VehicleId] = @lVehicleId
​
				SELECT @lVehicleId = MIN([VehicleId])
				FROM #VehiclesToProcess
			END
		END
		ELSE
		BEGIN
			INSERT INTO #DriversToProcess
			SELECT [UserId]
			FROM [General].[DriversUsers]
			WHERE [CustomerId] = @lCustomerId
​
			SELECT @lDriverId = MIN([DriverId])
			FROM #DriversToProcess
​
			WHILE @lDriverId IS NOT NULL
			BEGIN
				DECLARE @lLastDriverFuelId INT = NULL
				SELECT @lAmount = 0
				      ,@lAdditionalAmount = 0
					  ,@lRealAmount = 0
					  ,@lLiters = 0
					  ,@lAdditionalLiters = 0	
					  ,@lRealLiters = 0
				      ,@lCreditCardId = NULL
					  
​
				SELECT TOP 1 @lLiterPrice =  ISNULL(tmp.[LiterPrice], 1)
											 FROM #TableFuelsTemp tmp
											 INNER JOIN [Control].[Fuels] f 
											 	ON f.[FuelId] = tmp.[FuelId]
											 INNER JOIN [General].[VehicleCategories] vc 
											 	ON f.[FuelId] = vc.[DefaultFuelId]
											 INNER JOIN [General].[Vehicles] v 
											 	ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
											 INNER JOIN [General].[VehiclesByUser] vbu 
											 	ON vbu.[VehicleId] = v.[VehicleId]
											 WHERE tmp.[FuelId] = f.[FuelId]
											 	AND vbu.[UserId] = @lDriverId
											 	AND vbu.[LastDateDriving] IS NULL
											--CASE  WHEN  @PartnerCapacityUnit = 1 
											--THEN (
											--		SELECT TOP 1 ISNULL(tmp.[LiterPrice] / @Equival, 1)
											--		FROM #TableFuelsTemp tmp
											--		INNER JOIN [Control].[Fuels] f 
											--			ON f.[FuelId] = tmp.[FuelId]
											--		INNER JOIN [General].[VehicleCategories] vc 
											--			ON f.[FuelId] = vc.[DefaultFuelId]
											--		INNER JOIN [General].[Vehicles] v 
											--			ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
											--		INNER JOIN [General].[VehiclesByUser] vbu 
											--			ON vbu.[VehicleId] = v.[VehicleId]
											--		WHERE tmp.[FuelId] = f.[FuelId]
											--			AND vbu.[UserId] = @lDriverId
											--			AND vbu.[LastDateDriving] IS NULL
											--	)
											--ELSE (
											--		SELECT TOP 1 ISNULL(tmp.[LiterPrice], 1)
											--		FROM #TableFuelsTemp tmp
											--		INNER JOIN [Control].[Fuels] f 
											--			ON f.[FuelId] = tmp.[FuelId]
											--		INNER JOIN [General].[VehicleCategories] vc 
											--			ON f.[FuelId] = vc.[DefaultFuelId]
											--		INNER JOIN [General].[Vehicles] v 
											--			ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
											--		INNER JOIN [General].[VehiclesByUser] vbu 
											--			ON vbu.[VehicleId] = v.[VehicleId]
											--		WHERE tmp.[FuelId] = f.[FuelId]
											--			AND vbu.[UserId] = @lDriverId
											--			AND vbu.[LastDateDriving] IS NULL
											--		)
											--END
​
				SELECT TOP 1 @lLastDriverFuelId = [Id] 
				FROM [Control].[FuelDistribution]
				WHERE [DriverId] = @lDriverId
				ORDER BY [InsertDate] DESC
​
				UPDATE [Control].[FuelDistribution]
				SET [Amount] = CASE WHEN @lDistributionType = @DISTRIBUTION_TYPE_LITER
									THEN [Liters] * @lLiterPrice
									ELSE [Amount]
							   END
					,[Liters] = CASE WHEN @lDistributionType = @DISTRIBUTION_TYPE_AMOUNT
									 THEN ISNULL([Amount] / @lLiterPrice, 0)
									 ELSE ISNULL([Liters], 0)
								END
					,[AdditionalAmount] = CASE  WHEN @lDistributionType = @DISTRIBUTION_TYPE_LITER
												THEN [AdditionalLiters] * @lLiterPrice
												ELSE [AdditionalAmount]
										 END
					,[AdditionalLiters] = CASE  WHEN @lDistributionType = @DISTRIBUTION_TYPE_AMOUNT
												THEN ISNULL([AdditionalAmount] / @lLiterPrice, 0)
												ELSE ISNULL([AdditionalLiters], 0)
										  END
				WHERE @lLastDriverFuelId = [Id]
​
				SELECT TOP 1 @lAmount = df.[Amount]	
							,@lAdditionalAmount = ISNULL(df.[AdditionalAmount], 0)
							,@lLiters = df.[Liters]
							,@lAdditionalLiters = ISNULL(df.[AdditionalLiters], 0)													
							,@lCreditCardId = cc.[CreditCardId]
				FROM [Control].[FuelDistribution] df
				INNER JOIN [Control].[CreditCardByDriver] cd 
					ON df.[DriverId] = cd.[UserId]
				INNER JOIN [Control].[CreditCard] cc 
					ON cd.[CreditCardId] = cc.[CreditCardId]
					AND cc.[StatusId] IN (7, 8)
				WHERE df.[DriverId] = @lDriverId
				ORDER BY df.[InsertDate] DESC
​
				SET @lRealAmount = [Control].[Fn_FuelDistribution_TransactionsAmount](@lCreditCardId, @lTimeZoneParameter, @lInsertDate, NULL, NULL)
				SET @lRealLiters = [Control].[Fn_FuelDistribution_TransactionsLiters](@lCreditCardId, @lTimeZoneParameter, @lInsertDate, NULL, 0, NULL)
​
				UPDATE [Control].[CreditCard]
				SET [CreditAvailable] = (@lAmount + @lAdditionalAmount) - @lRealAmount,
				[AvailableLiters] = (@lLiters + @lAdditionalLiters) - @lRealLiters
				WHERE [CreditCardId] = @lCreditCardId
​
				DELETE FROM #DriversToProcess
				WHERE [DriverId] = @lDriverId
​
				SELECT @lDriverId = MIN([DriverId])
				FROM #DriversToProcess
			END
		END
​
		SELECT @lCustomerId = MIN([CustomerId])
		FROM #CustomersToProcess
		WHERE [CustomerId] > @lCustomerId
​
		SELECT @lIssueForId = [IssueForId]
		FROM #CustomersToProcess
		WHERE [CustomerId] = @lCustomerId
	END
END
​
GO


