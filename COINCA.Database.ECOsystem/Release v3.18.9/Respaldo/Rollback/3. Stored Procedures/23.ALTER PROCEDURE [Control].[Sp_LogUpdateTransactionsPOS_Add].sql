USE ECOsystem
/****** Object:  StoredProcedure [Control].[Sp_LogUpdateTransactionsPOS_Add]    Script Date: 05/06/2020 2:57:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ===========================================================================================================
-- Author:		Gerald Solano
-- Create date: 14/06/2016
-- Description:	Insert log Update de transaction.
-- Modify by:	Gerald Solano
-- Modify date: 05/09/2016
-- Description:	Modify column name and parameter name
-- Stefano Quir�s - 28/11/2018 - Modify Transaction from floating to process only when we receive a 
-- RESULT_ADVICE from BAC -- and add the liters to restart to the available too - 14/01/2019
-- ===========================================================================================================
ALTER PROCEDURE [Control].[Sp_LogUpdateTransactionsPOS_Add]
(
		@pCCTransactionPOSId INT = NULL
		,@pResponseCode  varchar(20) = NULL
		,@pSystemTraceNumber  varchar(50) = NULL
		,@pAuthorizationNumber  varchar(50) = NULL
		,@pTerminalPOS  varchar(50) = NULL
		,@pCustomerID INT
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lErrorNumber INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
			DECLARE @lCreditCardId INT = 0
			DECLARE @lFuelAmount DECIMAL(16,2)
			DECLARE @lLiters DECIMAL(16, 3)
       
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END
				INSERT INTO [Control].[LogUpdateTransactionsPOS]
						([CCTransactionPOSId]
						,[ResponseCode]
						,[SystemTraceNumber]
						,[AuthorizationNumber]
						,[TerminalPOS]
						,[InsertDate]
						,[ModifyDate]
						,[CustomerId]
						)
				VALUES	(@pCCTransactionPOSId
						,@pResponseCode
						,@pSystemTraceNumber
						,@pAuthorizationNumber
						,@pTerminalPOS
						,GETUTCDATE()
						,GETUTCDATE()
						,@pCustomerID
					)	
			SET @lRowCount = @@ROWCOUNT
			
			-- Establece que la reversi�n fue realizada por el Socio / BAC
			IF LTRIM(RTRIM(@pResponseCode)) <> '00' AND LTRIM(RTRIM(@pResponseCode)) <> '08'
			BEGIN
				UPDATE [Control].[Transactions]
				SET [ReverseBy] = 'UPDATE_PARTNER_SERVICE'
				   ,[IsFloating] = 0
				   ,[ModifyDate] = GETDATE()
				WHERE [TransactionId] = @pCCTransactionPOSId
			END
			ELSE
			BEGIN
				UPDATE [Control].[Transactions]
				SET [IsFloating] = 0
				   ,[AuthorizationNumber] = @pAuthorizationNumber
				   ,[ModifyDate] = GETDATE()
				WHERE [TransactionId] = @pCCTransactionPOSId

				UPDATE [Control].[TransactionsHx]
				SET [IsFloating] = 0
				   ,[AuthorizationNumber] = @pAuthorizationNumber
				   ,[ModifyDate] = GETDATE()
				WHERE [TransactionId] = @pCCTransactionPOSId

				SELECT @lCreditCardId = [CreditCardId] 
					  ,@lFuelAmount = [FuelAmount]
					  ,@lLiters = [Liters]
				FROM [Control].[Transactions]
				WHERE [TransactionId] = @pCCTransactionPOSId

				Exec [Control].[Sp_CreditCard_CreditAvailable_Edit] @lCreditCardId, @lFuelAmount, 0, @lLiters
			END

            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
				SELECT @@IDENTITY AS 'Identity';
			 END
			
			IF @lRowCount = 0
			BEGIN
				SELECT -1 AS 'Identity';
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
         END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END
GO


