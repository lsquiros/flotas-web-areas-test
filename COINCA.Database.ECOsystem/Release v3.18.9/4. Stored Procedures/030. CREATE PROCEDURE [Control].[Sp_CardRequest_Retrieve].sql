/****** Object:  StoredProcedure [Control].[Sp_CardRequest_Retrieve]    Script Date: 01/06/2020 12:09:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Control].[Sp_CardRequest_Retrieve] --null, 6431, 0, null
(
	 @pCardRequestId INT = NULL
	,@pCustomerId INT
	,@pPendingOnly BIT = 0
	,@pKey varchar(max)=null
)
AS
BEGIN
	SET NOCOUNT ON
		
	SELECT 
		 a.[CardRequestId]
		,a.[CustomerId]
		,a.[PlateId]
		,a.[DriverName]
		,a.[DriverIdentification]
		,a.[AddressLine1]
		,a.[AddressLine2]
		,a.[PaymentReference]
		,a.[AuthorizedPerson]
		,a.[ContactPhone]
		,a.[EstimatedDelivery]
		,b.[StatusId]
		,CASE WHEN b.[StatusId] IS NULL 
				THEN (SELECT x.[Name]
						FROM [General].[Status] x
					 WHERE x.StatusId = 0)
			ELSE c.[Name] END AS [StatusName]
		,e.[IssueForId]
		,e.[CountryId]
		,a.[StateId]		
		,f.[Name] AS [StateName]
		,a.[CountyId]
		,g.[Name] AS [CountyName]
		,a.[CityId]
		,h.[Name] AS [CityName]
		,a.[DeliveryState]
		,a.[DeliveryCounty]
		,a.[DeliveryCity]
		,a.[RowVersion]
		,a.[DriverUserId]
		,(SELECT TOP(1) CASE WHEN h.[ModifyDate] IS NULL 
							THEN h.[InsertDate]
							ELSE h.[ModifyDate] END AS [Date]
			FROM [Control].[CreditCardHx] h
			WHERE h.[StatusId] = 7
			AND h.[CustomerId] = a.[CustomerId]
			AND h.[CreditCardId] = b.[CreditCardId]
			ORDER BY CreditCardHxId) AS [ActivatedDate]
		,(SELECT TOP(1) CASE WHEN h.[ModifyDate] IS NULL 
							THEN h.[InsertDate]
							ELSE h.[ModifyDate] END AS [Date]
			FROM [Control].[CreditCardHx] h
			WHERE h.[StatusId] = 9
			AND h.[CustomerId] = a.[CustomerId]
			AND h.[CreditCardId] = b.[CreditCardId]
			ORDER BY CreditCardHxId) AS [ClosedDate]
		,a.[PaymentMethod]
		,CASE WHEN a.[PaymentMethod] = 0 OR a.[PaymentMethod] IS NULL
		 THEN 'Tarjeta'
		 ELSE (SELECT [Name] FROM [Control].[PaymentInstrumentsTypes] WHERE [Id] = 1)
		 END [PaymentName]
	 FROM [Control].[CardRequest] a
		LEFT JOIN [Control].[CreditCard] b
			ON a.[CardRequestId] = b.[CardRequestId]
		   AND a.[CustomerId] = b.[CustomerId]

		LEFT JOIN [General].[Status] c
			ON c.[StatusId] = b.[StatusId]
		INNER JOIN [General].[Customers] e
			ON a.[CustomerId] = e.[CustomerId]
		LEFT JOIN [General].[States] f
			ON a.[StateId] = f.[StateId]
		LEFT JOIN [General].[Counties] g
			ON a.[CountyId] = g.[CountyId]
		LEFT JOIN [General].[Cities] h
			ON a.[CityId] = h.[CityId]		
	WHERE a.[CustomerId] = @pCustomerId
		AND (@pCardRequestId IS NULL OR a.[CardRequestId] = @pCardRequestId)
		AND ((@pPendingOnly = 0 OR b.[CreditCardId] IS NULL)
			  OR b.[StatusId] = 0)
		AND (@pKey IS NULL
				OR a.[PlateId] like '%'+@pKey+'%'
				OR a.[AuthorizedPerson] like '%'+@pKey+'%'
				OR a.[ContactPhone] like '%'+@pKey+'%'
		)
		AND (a.IsDeleted IS NULL OR a.IsDeleted = 0)
		AND a.[CardRequestId] NOT IN(
									 SELECT [CardRequestId] 
									 FROM [Control].[PaymentInstrumentsByType] 
									 WHERE [PaymentInstrumentId] IS NOT NULL
									 UNION
									 SELECT [CardRequestId]
									 FROM [Control].[CreditCard]
									)
	ORDER BY [CardRequestId] DESC
	
    SET NOCOUNT OFF
END
GO


