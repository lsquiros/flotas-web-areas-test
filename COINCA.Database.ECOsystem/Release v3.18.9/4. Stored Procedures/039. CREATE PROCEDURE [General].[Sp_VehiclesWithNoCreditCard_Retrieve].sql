/****** Object:  StoredProcedure [General].[Sp_VehiclesWithNoCreditCard_Retrieve]    Script Date: 27/05/2020 10:06:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Author:  Henry Retana  
-- Create date: 10/02/2017  
-- Description: Retrieve the vehicles which are not linked to a credit card  
-- ========================================================================================================================== 
-- Modify By Stefano Quir�s Ruiz - 05/04/2017 - Add filter to ignore the deleted vehicles  
-- Modify By Henry Retana - 21/6/2017 - Modification in order to use the CustomerId from the Card Request instead of Vehicles  
-- Modify by Esteban Sol�s - 06/03/2018 - Add validation to prevent multiple card request to a vehicle/driver on insert
-- Modify by Maria de los Angeles Jimenez Chavarria - 18/Dic/2018 - Hide deleted items.
-- Stefano Quir�s - 13/11/2019 - Add option that return the CreditCard that have less than a month to Expire
-- ==========================================================================================================================
ALTER PROCEDURE [General].[Sp_VehiclesWithNoCreditCard_Retrieve] --43--8662
(
	@pCustomerId INT
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON;

	WITH Vehicles
	AS (
		SELECT DISTINCT [VehicleId]
		FROM [Control].[CreditCardByVehicle] ccv
		INNER JOIN [Control].[CreditCard] cc
			ON cc.[CreditCardId] = ccv.[CreditCardId]
		WHERE (cc.[CreditCardId] IN (SELECT [CreditcardId]
								     FROM [Control].[CreditCard]
								     WHERE [CustomerId] = @pCustomerId
								           AND [StatusId] NOT IN (2,6,9)			   
									)	
			  AND DATEDIFF(MONTH, GETDATE(), CAST(CAST([ExpirationYear] AS varchar) + '-' + CAST([ExpirationMonth] AS varchar) + '-' + CAST(1 AS varchar) AS DATETIME)) > 1
			  )	
		UNION
		
		SELECT DISTINCT v.[VehicleId]
		FROM [Control].[CardRequest] c
		INNER JOIN [General].[Vehicles] v ON v.[PlateId] = c.[PlateId]
		WHERE c.[CustomerId] = @pCustomerId
			AND c.[CardRequestId] NOT IN (
				SELECT [CardRequestId]
				FROM [Control].[CreditCard]
				WHERE [CustomerId] = @pCustomerId
				--AND [StatusId] NOT IN (7,8,5)
			)
			AND (c.[IsDeleted] IS NULL OR c.[IsDeleted] = 0)
		)

	SELECT DISTINCT v.[PlateId] [Name]
				   ,v.[PlateId]
	FROM [General].[Vehicles] v
	WHERE v.[VehicleId] NOT IN (SELECT [VehicleId]
								FROM Vehicles)
		  AND v.[CustomerId] = @pCustomerId
		  AND (v.[IsDeleted] IS NULL
			   OR v.[IsDeleted] = 0)
		  AND v.[VehicleId] NOT IN
		  (
			SELECT [VehicleId] 
			FROM [Control].[PaymentInstrumentsByType] 
			WHERE [PaymentInstrumentId] IS NOT NULL
		  )
	SET NOCOUNT OFF
	SET XACT_ABORT OFF
END;
GO


