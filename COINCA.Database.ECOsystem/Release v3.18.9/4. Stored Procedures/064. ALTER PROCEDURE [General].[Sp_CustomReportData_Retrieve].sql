USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_CustomReportData_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_CustomReportData_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Esteban Solís
-- Create date: 27/04/2017
-- Description:	Retrieve Report data information
-- Modify: 16/05/18 - Esteban Solís
-- Fix issue with vehicles tables 
-- Modify: 21/05/18 - Esteban Solís
-- Changed order of joins tables to fix issue with fuels
-- Modify: 07/06/18 - Esteban Solís
-- Added validations in order to consider only processed transactions
-- Modify: 28/08/18 - Marjorie Garbanzo - Include logic for the Where that identifies the 
-- last driver of a vehicle
-- Modify By: Stefano Quirós - 24/06/2019 - Change the Inner Join to Left Join, when no 
-- have data on one or another table
-- Modify by Marjorie Garbanzo - 01/06/2020 Add logic to include transactions with different payment methods
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_CustomReportData_Retrieve] --169, 11240, '2019-06-01 00:00:00.000', '2019-06-21 00:00:00.000' , 1
( 
	 @pReportId INT --Report Id
	,@pCustomerId INT -- Cutomer Id
	,@pStartDate DATETIME = NULL -- Start Date
	,@pEndDate DATETIME = NULL -- End Date
	,@pUserId INT
)
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE @lSql NVARCHAR(MAX) = 'SELECT '
		,@lId INT
		,@lJoins VARCHAR(MAX)
		,@lHasDates BIT = 0
		,@lWhere VARCHAR(MAX)
		,@lResult VARCHAR(MAX)
		,@lTimeZoneParameter INT
	-- temp tables
	DECLARE @lFormulas AS TABLE (id INT)

	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId

	SELECT c.[Id]
		,c.[ReportColumnId]
		,c.[Formula]
		,'['+c.[FormulaName]+']' [FormulaName]
		,rc.[Source] [SourceColum]
		,rs.[Source] [TableSource]
		,rs.[PK]
		,rs.[Parent] [ParentTable]
		,rs.[FK]
		,rc.[SysTypeId]
		,rc.[FunctionCall]
		,c.[FinalSummation]
	INTO #ReportColumns
	FROM [General].[CustomReport] c
	LEFT JOIN [General].[ReportColumns] rc ON c.[ReportColumnId] = rc.[Id]
	LEFT JOIN [General].[ReportSources] rs ON rc.[ReportSourceId] = rs.[Id]
	WHERE c.[ReportId] = @pReportId
	ORDER BY c.[Order]

	SELECT DISTINCT
	     rs.[Id] [SourceId]
		,rs.[Source] [TableSource]
		,rs.[PK]
		,rs.[Parent] [Parent]
		,rs.[FK]
		,convert(VARCHAR(200), ' ') AS [ParentTable]
		,CASE 
			WHEN rs.parent IS NULL
				THEN 9999
			ELSE	CASE 
						WHEN rs.[Source] = 'General.Vehicles' 
						THEN 9998 
						ELSE 1 
					END 
			END AS [nivel]
	INTO #ReportJoins
	FROM [General].[CustomReport] c
	LEFT JOIN [General].[ReportColumns] rc ON c.[ReportColumnId] = rc.[Id]
	LEFT JOIN [General].[ReportSources] rs ON rc.[ReportSourceId] = rs.[Id]
	WHERE c.[ReportId] = @pReportId
		AND rs.[Source] IS NOT NULL

	SELECT @lId = MIN([Id])
	FROM #ReportColumns
	WHERE [FormulaName] IS NOT NULL

	WHILE @lId IS NOT NULL
	BEGIN
		INSERT INTO @lFormulas
		SELECT DISTINCT items
		FROM dbo.Fn_Split((
					SELECT [Formula]
					FROM #ReportColumns
					WHERE id = @lId
					), ',')
		WHERE items NOT LIKE '%[^0-9]%'

		SELECT @lId = MIN([Id])
		FROM #ReportColumns
		WHERE [Id] > @lId
			AND [FormulaName] IS NOT NULL
	END

	INSERT INTO #ReportJoins
	SELECT rs.[Id] [SourceId]
		,rs.[Source] [TableSource]
		,rs.[PK]
		,rs.[Parent] [Parent]
		,rs.[FK]
		,convert(VARCHAR(200), ' ') AS [ParentTable]
		,CASE 
			WHEN rs.parent IS NULL
				THEN 9999
			ELSE	CASE 
						WHEN rs.[Source] = 'General.Vehicles' 
						THEN 9998 
						ELSE 1 
					END 
			END AS [nivel]
	FROM [General].[ReportColumns] rc
	INNER JOIN [General].[ReportSources] rs ON rc.[ReportSourceId] = rs.[Id]
	WHERE rs.[Source] IS NOT NULL
		AND rc.[Id] IN (
			SELECT [Id]
			FROM @lFormulas
			)
		AND rs.[Id] NOT IN (
			SELECT SourceId
			FROM #ReportJoins
			)

	SELECT @lId = MIN([Id])
	FROM #ReportColumns

	WHILE @lId IS NOT NULL
	BEGIN
		DECLARE @lColumnId INT

		SELECT @lColumnId = [ReportColumnId]
		FROM #ReportColumns
		WHERE [Id] = @lId

		IF @lColumnId = - 1
		BEGIN
			DECLARE @lFormula VARCHAR(200)
				,@lFormulaName VARCHAR(200)

			SELECT @lFormula = [Formula]
				,@lFormulaName = [FormulaName]
			FROM #ReportColumns
			WHERE [Id] = @lId

			IF @lSql <> 'SELECT '
				SET @lSql = CONCAT (
						@lSql
						,', '
						)
			SET @lSql = CONCAT (
					@lSql
					,(
						SELECT CONVERT(VARCHAR(MAX),[General].[Fn_CustomReportFormula_Retrieve](@lFormula, 0))
						)
					,' AS '
					,@lFormulaName
					)
		END
		ELSE
		BEGIN
			IF @lSql <> 'SELECT '
				SET @lSql = CONCAT (
						@lSql
						,', '
						)

			SET @lSql = CONCAT (
					@lSql
					,(  
							SELECT CASE WHEN [SourceColum] = 'Date'
									THEN
										CONCAT (
										'(CONVERT(VARCHAR(10), '
										,[TableSource]
										,'.'
										,[SourceColum]									
										,',103)+'' ''+CONVERT(VARCHAR(8),'
										,[TableSource]
										,'.'
										,[SourceColum]
										,',14) '
										,') AS '
										,[SourceColum]
										)												
									ELSE 
										CASE WHEN [FunctionCall] IS NOT NULL
										THEN
										CONCAT (
										'CONVERT(VARCHAR(MAX), '
										,[FunctionCall]									
										,') AS '
										,[SourceColum]
										)
										ELSE 
										CONCAT (
										'CONVERT(VARCHAR(MAX), '
										,[TableSource]
										,'.'
										,[SourceColum]									
										,') AS '
										,[SourceColum]
										)
										END
								END
							FROM #ReportColumns
							WHERE [Id] = @lId
						)
					)
		END

		SELECT @lId = MIN([Id])
		FROM #ReportColumns
		WHERE [Id] > @lId
	END



	-- set table sources for joins
	UPDATE rj
	SET ParentTable = rs.[Source]
	FROM #ReportJoins rj
	JOIN [General].[ReportSources] rs ON rj.[Parent] = rs.[Id]

	SET @lJoins = ''

	-- set tables to complete join list
	DECLARE @count INT = 0
	DECLARE @next BIT = 1
	DECLARE @level INT = 1

	WHILE @next = 1
	BEGIN
		SET @count = (
				SELECT count(1)
				FROM #ReportJoins
				)

		INSERT INTO #ReportJoins
		SELECT DISTINCT 0
			,ParentTable
			,rs.[PK]
			,rs.[Parent]
			,rs.[FK]
			,convert(VARCHAR(200), ' ')
			,CASE 
				WHEN rs.[Parent] IS NULL
					THEN 99999
				ELSE	CASE 
							WHEN rs.[Source] = 'General.Vehicles' 
							THEN 9998 
							ELSE @level + 1
						END 
				END AS [nivel]
		FROM #ReportJoins rj
		LEFT JOIN [General].[ReportSources] rs ON rj.[Parent] = rs.[Id]
		WHERE rj.nivel = @level
			AND rj.parent IS NOT NULL
			AND rs.Source NOT IN (
				SELECT [TableSource]
				FROM #ReportJoins
				)
				
		UPDATE rj
		SET ParentTable = rs.[Source]
		FROM #ReportJoins rj
		JOIN [General].[ReportSources] rs ON rj.[Parent] = rs.[Id]

		IF @count = (
				SELECT count(1)
				FROM #ReportJoins
				)
		BEGIN
			SET @next = 0
		END

		SET @level = @level + 10
	END
	
	IF NOT EXISTS (
			SELECT 1
			FROM #ReportJoins
			WHERE TableSource = 'Control.Transactions'
			)
	BEGIN
		INSERT INTO #ReportJoins
		SELECT rs.[Id]
			,rs.[Source]
			,rs.[PK]
			,rs.[Parent]
			,rs.[FK]
			,convert(VARCHAR(200), ' ')
			,999
		FROM [General].[ReportSources] rs
		WHERE rs.[Source] = 'Control.Transactions'
	END		

	-- If report uses Transactions but no Vehicles tables then use CreditCard table for joins
	IF EXISTS (
			SELECT 1
			FROM #ReportJoins
			WHERE TableSource = 'Control.Transactions'
			)
		AND NOT EXISTS (
			SELECT 1
			FROM #ReportJoins
			WHERE TableSource = 'General.Vehicles'
			)
	BEGIN
		INSERT INTO #ReportJoins
		SELECT rs.[Id]
			,rs.[Source]
			,rs.[PK]
			,rs.[Parent]
			,rs.[FK]
			,convert(VARCHAR(200), ' ')
			,1
		FROM [General].[ReportSources] rs
		WHERE rs.[Id] = 3
	END


	-- set table sources for joins
	UPDATE rj
	SET ParentTable = rs.[Source]
	FROM #ReportJoins rj
	JOIN [General].[ReportSources] rs ON rj.[Parent] = rs.[Id]

	UPDATE  r
	SET r.Nivel = r.Nivel + 1
	FROM #ReportJoins r1
	JOIN #ReportJoins r on r1.Parent = r.SourceId
	WHERE r1.Nivel >= r.Nivel

	-- create join section
	SELECT @lJoins = CONCAT (
			@lJoins
			,CONCAT (
				CASE 
					WHEN Parent IS NULL OR @lJoins = ''
						THEN ''
					ELSE 'LEFT JOIN '
					END
				,TableSource
				,' '
				,CASE 
					WHEN Parent IS NULL OR @lJoins = ''
						THEN ' '
					ELSE CONCAT (
							' ON '
							,TableSource
							,'.'
							,PK
							,'='
							,ParentTable
							,'.'
							,CASE 
								WHEN FK IS NULL
									THEN PK
								ELSE FK
								END
							,' '
							)
					END
				)
			)
	FROM #ReportJoins
	ORDER BY nivel DESC, Parent ASC

	-- create where section
	SET @lWhere = ''

	IF EXISTS (
			SELECT 1
			FROM #ReportJoins
			WHERE TableSource = 'General.Vehicles'
			)
	BEGIN
		SET @lWhere = CONCAT (
				@lWhere
				,'General.Vehicles.[CustomerId] = @pCustomerId'
				)
	END
	ELSE
	BEGIN
		SET @lWhere = CONCAT (
				@lWhere
				,'Control.CreditCard.[CustomerId] = @pCustomerId'
				)
	END

	IF EXISTS (
			SELECT 1
			FROM #ReportJoins
			WHERE TableSource = 'General.VehiclesByUser'
			)
	BEGIN
		SET @lWhere = CONCAT (
				@lWhere
				,CASE 
					WHEN @lWhere <> ''
						THEN ' AND '
					ELSE ''
					END
				,'(General.VehiclesByUser.[LastDateDriving] BETWEEN @pStartDate AND @pEndDate OR General.VehiclesByUser.[LastDateDriving] IS NULL) '
				
				)
		SET @lHasDates = 1
	END

	IF EXISTS (
			SELECT 1
			FROM #ReportJoins
			WHERE TableSource = 'Control.Transactions'
			)
	BEGIN
		SET @lWhere = CONCAT (
				@lWhere
				,CASE 
					WHEN @lWhere <> ''
						THEN ' AND '
					ELSE ''
					END
				,'DATEADD(HOUR, @lTimeZoneParameter, Control.Transactions.[InsertDate]) BETWEEN @pStartDate AND @pEndDate '
				,N' AND 1 > (
					SELECT ISNULL(COUNT(1), 0)
					FROM CONTROL.Transactions t2
					WHERE (t2.[CreditCardId] = Control.Transactions.[CreditCardId]
							OR t2.[PaymentInstrumentId] = Control.Transactions.[PaymentInstrumentId])
						AND t2.[TransactionPOS] = Control.Transactions.[TransactionPOS]
						AND t2.[ProcessorId] = Control.Transactions.[ProcessorId]
						AND (
							t2.IsReversed = 1
							OR t2.IsVoid = 1
							)
					)
				AND (
					Control.Transactions.[TransactionOffline] IS NULL
					OR Control.Transactions.[TransactionOffline] = 0
					) 
				AND (
					Control.Transactions.[IsFloating] IS NULL
					OR Control.Transactions.[IsFloating] = 0
					)
					AND (
				    Control.Transactions.[IsDenied] IS NULL 
					OR Control.Transactions.[IsDenied] = 0)'
				)
		SET @lHasDates = 1
	END

	-- query to excetute
	SET @lSql = CONCAT (
			'SELECT isnull(cast( ( '
			,@lSql
			,' FROM '
			,@lJoins
			,' WHERE '
			,@lWhere
			,' FOR XML RAW, Root(''CustomReportData''),ELEMENTS XSINIL' 
			,' ) AS VARCHAR(MAX)),(SELECT NULL FOR XML RAW, Root(''CustomReportData''),ELEMENTS XSINIL))'
			)

	-- Execute query
	IF @lHasDates = 0
	BEGIN
		EXECUTE sp_executesql @lSql
			,N'@pCustomerId INT'
			,@pCustomerId
	END
	ELSE
	BEGIN
		EXECUTE sp_executesql @lSql
			,N'@pCustomerId int, @pStartDate datetime, @pEndDate datetime, @lTimeZoneParameter int'
			,@pCustomerId
			,@pStartDate
			,@pEndDate
			,@lTimeZoneParameter
	END

	DROP TABLE #ReportColumns

	DROP TABLE #ReportJoins

	SET NOCOUNT OFF
END

