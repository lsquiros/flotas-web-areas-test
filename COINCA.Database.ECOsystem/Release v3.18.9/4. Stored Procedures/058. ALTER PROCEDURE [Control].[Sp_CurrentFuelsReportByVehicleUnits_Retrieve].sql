USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	-- ================================================================================================
	-- Author:		Berman Romero L.
	-- Create date: Oct/21/2014
	-- Description:	Retrieve Current Fuels Report By Sub Unit information
	-- ================================================================================================
	-- Modify by:	Melvin Salas
	-- Modify date:	Jan/22/2016
	-- Description:	Add Dynamics Filter
	-- ================================================================================================
	-- Modify by:	Melvin Salas
	-- Modify date:	Jun/24/2016
	-- Description:	Fix divide by zero error encountered
	-- ================================================================================================
	-- Modify by: Stefano Quiros 13/04/2016 - 14/11/2017 - Add Void Transactions Validation
	-- Modify by: Esteban Solis 16/05/2018 Add CustomerId to dbo.Sp_UserDynamicFilter_Retrive call
	-- ================================================================================================
    -- Modify by Marjorie Garbanzo - 21/05/2019 - Add filter IsDenied to the retrieve
    -- Modify by Marjorie Garbanzo - 01/06/2020 - Add logic to include transactions with different payment methods
	
	CREATE PROCEDURE [Control].[Sp_CurrentFuelsReportByVehicleUnits_Retrieve]
	(
		 @pCustomerId INT					--@pCustomerId: Customer Id
		,@pYear INT = NULL					--@pYear: Year
		,@pMonth INT = NULL					--@pMonth: Month
		,@pStartDate DATETIME = NULL		--@pStartDate: Start Date
		,@pEndDate DATETIME = NULL			--@pEndDate: End Date
		,@pUserId INT						--@pUserId: User Id
	)
	AS
	BEGIN
	
		--DECLARE @pCustomerId INT = 22	
		--DECLARE @pYear INT = 2015	
		--DECLARE @pMonth INT = 9	
		--DECLARE @pStartDate DATETIME = NULL	
		--DECLARE @pEndDate DATETIME = NULL	

		SET NOCOUNT ON	
	
		DECLARE @lCurrencySymbol NVARCHAR(4) = ''

		-- DYNAMIC FILTER
		DECLARE	@Results TABLE (items INT)
		DECLARE @count INT
		INSERT	@Results
		EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH',@pCustomerId
		SET		@count = (SELECT COUNT(*) FROM	@Results)
		-- END
	
		SELECT
			@lCurrencySymbol = a.Symbol
		FROM [Control].[Currencies] a
			INNER JOIN [General].[Customers] b
				ON a.[CurrencyId] = b.[CurrencyId]
		WHERE b.[CustomerId] = @pCustomerId
	
		SELECT
			 a.[Name] AS [VehicleUnitName]
			,ISNULL(t.[RealAmount],0) AS [RealAmount]
			,s.[AssignedAmount]
			,IIF(s.[AssignedAmount]=0,0,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100)) AS [RealAmountPct]
			,@lCurrencySymbol AS [CurrencySymbol]
			,a.UnitId
		FROM [General].[VehicleUnits] a
			LEFT JOIN (SELECT
							 z.[UnitId]
							,SUM(x.[FuelAmount]) AS [RealAmount]
						FROM [Control].[Transactions] x
							INNER JOIN [General].[Vehicles] y
								ON x.[VehicleId] = y.[VehicleId]
							INNER JOIN [General].[VehicleCostCenters] z
								ON z.CostCenterId = y.CostCenterId
						WHERE y.[CustomerId] = @pCustomerId
						AND (@count = 0 OR x.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
						  AND x.[IsFloating] = 0
						  AND x.[IsReversed] = 0
						  AND x.[IsDuplicated] = 0
						AND (x.[IsDenied] = 0 OR x.[IsDenied] IS NULL)
						   AND 1 > (
								SELECT COUNT(*) FROM Control.Transactions t2
									WHERE (t2.[CreditCardId] = x.[CreditCardId]
											OR t2.[PaymentInstrumentId] = x.[PaymentInstrumentId])
									AND t2.[TransactionPOS] = x.[TransactionPOS]
									AND t2.[ProcessorId] = x.[ProcessorId] 
									AND (t2.IsReversed = 1 OR t2.IsVoid = 1) 
							)
						  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
									AND DATEPART(m,x.[Date]) = @pMonth
									AND DATEPART(yyyy,x.[Date]) = @pYear) OR
								(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
									AND x.[Date] BETWEEN @pStartDate AND @pEndDate))
						GROUP BY z.[UnitId])t
				ON a.UnitId = t.UnitId
			INNER JOIN (SELECT
							 z.[UnitId]
							,SUM(x.[Amount]) AS [AssignedAmount]
						FROM [Control].[FuelDistribution] x
							INNER JOIN [General].[Vehicles] y
								ON x.[VehicleId] = y.[VehicleId]
							INNER JOIN [General].[VehicleCostCenters] z
								ON z.CostCenterId = y.CostCenterId
						WHERE y.[CustomerId] = @pCustomerId
						AND (@count = 0 OR x.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
						  AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
									AND x.[Month] = @pMonth
									AND x.[Year] = @pYear) OR
								(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
									AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
									AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
						GROUP BY z.[UnitId])s
				ON a.[UnitId] = s.[UnitId]
		
	
		SET NOCOUNT OFF
	END

