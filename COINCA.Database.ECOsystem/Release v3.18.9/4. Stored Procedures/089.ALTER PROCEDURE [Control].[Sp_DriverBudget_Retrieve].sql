/****** Object:  StoredProcedure [Control].[Sp_DriverBudget_Retrieve]    Script Date: 7/29/2020 11:32:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================  
-- Author:  Esteban Solís  
-- Create date: 18/12/2017
-- Description: Retrieve budget by driver information  
-- Modify By: Stefano Quirós - Change the origin of data from Fuel Distribution table
-- ================================================================================================  
ALTER PROCEDURE [Control].[Sp_DriverBudget_Retrieve] --43834--35677
(
	@pDriverId INT = NULL --@pVehicleId: PK of the table  
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lInsertDate DATETIME
	       ,@lCustomerId INT

	SELECT @lCustomerId = [CustomerId] 
	FROM [General].[DriversUsers] 
	WHERE [UserId] = @pDriverId

	SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@lCustomerId)

	SELECT TOP 1 (fd.[Amount] + fd.[AdditionalAmount]) [TotalBudget]
				 ,cc.[CreditAvailable] [Available]
				 ,fd.[AdditionalAmount] [AdditionalBudget]
	FROM [Control].[CreditCardByDriver] ccd
	INNER JOIN [Control].[CreditCard] cc 
		ON ccd.[CreditCardId] = cc.[CreditCardId]
	INNER JOIN [Control].[FuelDistribution] fd
		ON fd.[DriverId] = ccd.[UserId]
	WHERE ccd.[UserId] = @pDriverId
		AND cc.[StatusId] IN (7, 8)
		AND CONVERT(DATETIME, fd.[InsertDate]) >= @lInsertDate
	ORDER BY fd.[InsertDate] DESC

	SET NOCOUNT OFF
END