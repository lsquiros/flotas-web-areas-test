/****** Object:  StoredProcedure [Control].[Sp_TransactionsOfflineReport_Retrieve]    Script Date: 02/06/2020 10:50:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 10/05/2017
-- Description:	Obtiene la información de las transacciones Offline
-- Modify by Marjorie Garbanzo - Add filter IsDenied to the retrieve - 21/05/2019
-- Modify by Jason Bonilla - 01/06/2020 - Add PaymentInstrumentCode and Transactions Validations
-- ================================================================================================

ALTER PROCEDURE [Control].[Sp_TransactionsOfflineReport_Retrieve]
(
	@pCustomerId INT					
)
AS
BEGIN

	SET NOCOUNT ON	
	DECLARE @lTimeZoneParameter INT
	DECLARE @lConvertionValue DECIMAL(16, 5) = 3.78
	DECLARE @lPartnerUnit INT


	SELECT @lPartnerUnit = [CapacityUnitId] 
	FROM [General].[Partners] p
	INNER JOIN [General].[CustomersByPartner] cp
		ON cp.[PartnerId] = p.[PartnerId]
	WHERE [CustomerId] = @pCustomerId

	SELECT @lTimeZoneParameter = [TimeZone]	
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId

	SELECT  DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date],
		    v.[Name] [VehicleName],
		    v.[PlateId],
		    cc.[CreditCardNumber]
			,ISNULL(cc.[CreditCardNumber], pay.[Code]) [PaymentInstrumentCode]
			,(CASE WHEN t.[PaymentInstrumentId] IS NULL
			  THEN 'Tarjeta'
			  ELSE pit.[Name] END) AS [PaymentInstrumentType],
		    e.[Code] AS [CostCenter],
		    (SELECT TOP 1 du.[Identification] 
			 FROM [General].[DriversUsers] du			
		     WHERE t.[DriverCode] = du.[Code] 
		     AND du.[CustomerId] = @pCustomerId) AS [DriverId],
		    (SELECT TOP 1 u.[Name] 
			 FROM [General].[DriversUsers] du		
		     INNER JOIN [General].[Users] u
		    	ON du.[UserId] = u.[UserId]
		      WHERE t.[DriverCode] = du.[Code]
		      AND du.[CustomerId] = @pCustomerId) AS [DriverName],
		    (SELECT TOP 1 TT.[Odometer] 
			 FROM(SELECT TOP 2 NT.* 
			      FROM [Control].[Transactions] NT
		    	  WHERE NT.[VehicleId] = t.[VehicleId] 
						AND NT.[Date] <= t.[Date] 
						AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)		
						AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
						AND 1 > (
									SELECT ISNULL(COUNT(1), 0)
									FROM CONTROL.Transactions t2
									WHERE (t2.[CreditCardId] = t.[CreditCardId]
										   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
										AND t2.[TransactionPOS] = t.[TransactionPOS]
										AND t2.[ProcessorId] = t.[ProcessorId]
										AND (
											t2.IsReversed = 1
											OR t2.IsVoid = 1
											)
								)
				  ORDER BY NT.[Date] DESC ) TT 
			 ORDER BY TT.[Date] ASC) AS [OdometerLast],
		    t.[Odometer],
		    CASE WHEN @lPartnerUnit = 1 THEN (SELECT TOP 1 TT.[Liters] 
			 FROM(SELECT TOP 2 NT.* 
			      FROM [Control].[Transactions] NT
		    	  WHERE NT.[VehicleId] = t.[VehicleId] 
						AND NT.[DATE] <= t.[InsertDate] 
						AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)		
						AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
						AND 1 > (
									SELECT ISNULL(COUNT(1), 0)
									FROM CONTROL.Transactions t2
									WHERE (t2.[CreditCardId] = t.[CreditCardId]
										   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
										AND t2.[TransactionPOS] = t.[TransactionPOS]
										AND t2.[ProcessorId] = t.[ProcessorId]
										AND (
											t2.IsReversed = 1
											OR t2.IsVoid = 1
											)
								)
				  ORDER BY NT.[Date] DESC ) TT 
			 ORDER BY TT.[Date] ASC) * @lConvertionValue
			 ELSE (SELECT TOP 1 TT.[Liters] 
			 FROM(SELECT TOP 2 NT.* 
			      FROM [Control].[Transactions] NT
		    	  WHERE NT.[VehicleId] = t.[VehicleId] 
						AND NT.[DATE] <= t.[InsertDate] 
						AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)		
						AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
						AND 1 > (
									SELECT ISNULL(COUNT(1), 0)
									FROM CONTROL.Transactions t2
									WHERE (t2.[CreditCardId] = t.[CreditCardId]
										   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
										AND t2.[TransactionPOS] = t.[TransactionPOS]
										AND t2.[ProcessorId] = t.[ProcessorId]
										AND (
											t2.IsReversed = 1
											OR t2.IsVoid = 1
											)
								)
				  ORDER BY NT.[Date] DESC ) TT 
			 ORDER BY TT.[Date] ASC)
			 END [LitersLast],	
		    CASE WHEN @lPartnerUnit = 1 THEN t.[Liters] * @lConvertionValue
				 ELSE t.[Liters] 
				 END [Liters],		
		    t.[TransactionPOS] AS [Reference],
		    t.[FuelAmount],
		    f.[Name] AS [FuelName],
		    t.[MerchantDescription] AS [ServiceStation],
		    t.[TransactionPOS] AS [Reference],
		    DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [FechaValor],
		    ISNULL(t.[Invoice], '-') AS [Asignacion],
			c.[UnitOfCapacityId],
			CASE [UnitOfCapacityId] WHEN 0 
			THEN 'Litros'
			ELSE 'Galones'	
			END	[CustomerCapacityUnitName]	
	FROM [Control].[Transactions] t
	INNER JOIN [Control].[TransactionsOfflineReportToSend] ts
		ON ts.[TransactionId] = t.[TransactionId]
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]		
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]
	INNER JOIN [Control].[Fuels] f 
		ON t.[FuelId] = f.[FuelId]
	INNER JOIN [Control].[CreditCardByVehicle] cv 
		ON t.[CreditCardId] = cv.[CreditCardId]
	INNER JOIN [Control].[Currencies] g 
		ON c.[CurrencyId] = g.[CurrencyId]
	LEFT JOIN [Control].[CreditCard] cc
		ON cc.CreditCardId = t.CreditCardId
	LEFT JOIN [Control].[PaymentInstruments] pay
		ON t.[PaymentInstrumentId] = pay.[Id]
	LEFT JOIN [Control].[PaymentInstrumentsTypes] pit
		ON pay.[typeId] = pit.[Id]
	INNER JOIN [General].[VehicleCostCenters] e
		ON v.CostCenterId = e.CostCenterId
	WHERE c.[CustomerId] = @pCustomerId
	AND t.[TransactionOffline] = 1
	AND (ts.[Send] IS NULL OR ts.[Send] = 0)
	ORDER BY e.[Code] DESC

	SET NOCOUNT OFF
END

