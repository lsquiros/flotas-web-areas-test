/****** Object:  StoredProcedure [Control].[Sp_PaymentMethod_AddOrEdit]    Script Date: 27/05/2020 10:06:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      Jason Bonilla
-- Create Date: 11/05/2020
-- Description: <Description, , >
-- =============================================
CREATE PROCEDURE [Control].[Sp_PaymentMethod_AddOrEdit]
(
    @pCardRequestId INT,
	@pPaymentMethod INT,
	@pCustomerId INT
)
AS
BEGIN
    DECLARE @paymentId INT = (SELECT TOP 1 [Id]
							  FROM [Control].[PaymentInstruments] 
							  WHERE [TypeId] = @pPaymentMethod
							  AND ([IsUsed] is null OR [IsUsed] = 0)
							 )

	DECLARE @vehicleId INT = (
								SELECT [VehicleId] 
								FROM [Control].[PaymentInstrumentsByType]
								WHERE [CardRequestId] = @pCardRequestId
							 )

	UPDATE [Control].[PaymentInstruments]
	SET [IsUsed] = 1,
	[CustomerId] = @pCustomerId
	WHERE [Id] = @paymentId

	UPDATE [Control].[PaymentInstrumentsByType]
	SET [PaymentInstrumentId] = @paymentId
	WHERE [CardRequestId] = @pCardRequestId

	UPDATE [Control].[FuelDistribution] 
	SET [Amount] = 0,
		[Liters] = 0,
		[AdditionalAmount] = 0,
		[AdditionalLiters] = 0
	WHERE [VehicleId] = @vehicleId
END
GO


