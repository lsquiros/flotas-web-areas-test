/****** Object:  StoredProcedure [Control].[Sp_CreditCard_CreditAvailable_Edit]    Script Date: 6/1/2020 2:46:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/11/2014
-- Description:	Update CreditCard CreditAvailable amount
-- Stefano Quirós - Add Liters Parameter to restart or add the new available 
-- liters field on the creditcard - 14/01/2019
-- ==============================================================================================================
ALTER PROCEDURE [Control].[Sp_CreditCard_CreditAvailable_Edit]
(
	 @lPaymentInstrumentId INT = NULL
	,@pCreditCardId INT = NULL				 		--@pCreditCardId:Credit Card Id, PK of the table
	,@pTransactionAmount NUMERIC(16,2)		--@pTransactionAmount: Transaction Amount
	,@pIsReversed BIT = NULL
	,@pLiters DECIMAL(16,3)
)
AS
BEGIN
	
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
			DECLARE @lErrorMessage NVARCHAR(4000)
            DECLARE @lErrorSeverity INT
            DECLARE @lErrorState INT
            DECLARE @lLocalTran BIT = 0
            DECLARE @lRowCount INT = 0
            DECLARE @lCount INT = 0
			DECLARE @lConvertionValue DECIMAL(16, 8) = 3.78541178
			DECLARE @lPartnerCapacityValue INT
            
            IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRANSACTION
				SET @lLocalTran = 1
			END

			SELECT @lPartnerCapacityValue = [CapacityUnitId] 
			FROM [General].[Partners] p
			INNER JOIN [General].[CustomersByPartner] cp
				ON cp.[PartnerId] = p.[PartnerId] 
			LEFT JOIN [Control].[CreditCard] cc
				ON cc.[CustomerId] = cp.[CustomerId]
				AND cc.[CreditCardId] = @pCreditCardId
			LEFT JOIN [Control].[PaymentInstruments] [pi]
				ON [pi].[CustomerId] = cp.[CustomerId]
				AND [pi].[Id] = @lPaymentInstrumentId
			
			IF(@pIsReversed = 1) -- SI LA TRANSACCIÓN FUE REVERSADA RESTAURAMOS EL MONTO DISPONIBLE
			BEGIN
				IF @pCreditCardId IS NOT NULL
				BEGIN
					UPDATE [Control].[CreditCard]
					SET  [CreditAvailable] = [CreditAvailable] + @pTransactionAmount
						,[AvailableLiters] = [AvailableLiters] + CASE WHEN @lPartnerCapacityValue = 1
																 THEN @pLiters * @lConvertionValue
																 ELSE @pLiters
																 END
					WHERE [CreditCardId] = @pCreditCardId
				END
				ELSE
				BEGIN
					UPDATE [Control].[PaymentInstruments]
					SET  [CreditAvailable] = [CreditAvailable] + @pTransactionAmount
						,[AvailableLiters] = [AvailableLiters] + CASE WHEN @lPartnerCapacityValue = 1
																 THEN @pLiters * @lConvertionValue
																 ELSE @pLiters
																 END
					WHERE [Id] = @lPaymentInstrumentId
				END
			END
			ELSE
			BEGIN
				IF @pCreditCardId IS NOT NULL
				BEGIN				
					UPDATE	[Control].[CreditCard]
					SET		[CreditAvailable] = [CreditAvailable] - @pTransactionAmount
					       ,[AvailableLiters] = [AvailableLiters] - CASE WHEN @lPartnerCapacityValue = 1
																	THEN @pLiters * @lConvertionValue
																	ELSE @pLiters
																	END
					WHERE	[CreditCardId] = @pCreditCardId
				END
				ELSE
				BEGIN
					UPDATE [Control].[PaymentInstruments]
					SET  [CreditAvailable] = [CreditAvailable] - @pTransactionAmount
						,[AvailableLiters] = [AvailableLiters] - CASE WHEN @lPartnerCapacityValue = 1
																 THEN @pLiters * @lConvertionValue
																 ELSE @pLiters
																 END
					WHERE [Id] = @lPaymentInstrumentId
				END
			END 

			SET @lRowCount = @@ROWCOUNT
            				            
            IF (@@TRANCOUNT > 0 AND @lLocalTran = 1)
			BEGIN
				COMMIT TRANSACTION
			END
			
			IF @lRowCount = 0
			BEGIN
				RAISERROR ('Error updating database row, Please try again. TimeStamp verification failed.', 16, 1)
			END
			
    END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT > 0  AND XACT_STATE() > 0)
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF
END

