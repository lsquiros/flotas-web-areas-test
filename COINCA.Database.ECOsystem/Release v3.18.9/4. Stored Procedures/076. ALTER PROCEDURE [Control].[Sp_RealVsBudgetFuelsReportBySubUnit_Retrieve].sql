USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_RealVsBudgetFuelsReportBySubUnit_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportBySubUnit_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: Oct/21/2014
-- Description:	Retrieve Current Fuels Report By Sub Unit information
-- Modify by:	Melvin Salas
-- Modify date:	Jan/22/2016
-- Description:	Add Dynamics Filter
-- Modify by:	Melvin Salas
-- Modify date:	Jun/24/2016
-- Description:	Fix divide by zero error encountered
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Modify by Marjorie Garbanzo - 21/05/2019 - Add filter IsDenied to the retrieve
-- Modify by Marjorie Garbanzo - 01/06/2020 Add logic to include transactions with different payment methods
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportBySubUnit_Retrieve]
(
	 @pCustomerId INT
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
	,@pUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
		
	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	SELECT @lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
	INNER JOIN [General].[Customers] b
		ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT   a.[Name] AS [CostCenterName]
			,ISNULL(t.[RealAmount],0) AS [RealAmount]
			,s.[AssignedAmount]
			,IIF ( s.[AssignedAmount] = 0, 0, CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / s.[AssignedAmount], 2)*100) ) AS [RealAmountPct]
			,@lCurrencySymbol AS [CurrencySymbol]
	FROM [General].[VehicleCostCenters] a
	LEFT JOIN (SELECT y.[CostCenterId]
						,SUM(x.[FuelAmount]) AS [RealAmount]
				FROM [Control].[Transactions] x
				INNER JOIN [General].[Vehicles] y
					ON x.[VehicleId] = y.[VehicleId]
				WHERE y.[CustomerId] = @pCustomerId
				AND (@count = 0 OR x.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER						
				AND x.[IsReversed] = 0
				AND x.[IsDuplicated] = 0
				AND (x.[IsFloating] IS NULL OR x.[IsFloating] = 0) 
				AND (x.[IsDenied] = 0 OR x.[IsDenied] IS NULL)
				AND 1 > (SELECT ISNULL(COUNT(1), 0) 
						FROM [Control].[Transactions] t2
						WHERE (t2.[CreditCardId] = x.[CreditCardId] 
								OR t2.[PaymentInstrumentId] = x.[PaymentInstrumentId])
						AND t2.[TransactionPOS] = x.[TransactionPOS] 
						AND t2.[ProcessorId] = x.[ProcessorId] 
						AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,x.[Date]) = @pMonth
						AND DATEPART(yyyy,x.[Date]) = @pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND x.[Date] BETWEEN @pStartDate AND @pEndDate))								
			GROUP BY y.[CostCenterId]) t
		ON a.CostCenterId = t.CostCenterId
	INNER JOIN (SELECT y.[CostCenterId]
					  ,SUM(x.[Amount]) AS [AssignedAmount]
				FROM [Control].[FuelDistribution] x
				INNER JOIN [General].[Vehicles] y
					ON x.[VehicleId] = y.[VehicleId]
				WHERE y.[CustomerId] = @pCustomerId
				AND (@count = 0 OR x.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
				AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND x.[Month] = @pMonth
						AND x.[Year] = @pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
						AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
				GROUP BY y.[CostCenterId]) s
		ON a.[CostCenterId] = s.[CostCenterId]
	ORDER BY 4 desc
		
	
	SET NOCOUNT OFF
END

