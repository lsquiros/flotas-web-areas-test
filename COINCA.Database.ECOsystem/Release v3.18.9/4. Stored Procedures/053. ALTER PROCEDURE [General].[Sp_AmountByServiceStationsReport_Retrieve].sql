USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_AmountByServiceStationsReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_AmountByServiceStationsReport_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================================  
-- Author:  Henry Retana  
-- Create date: 28/04/2017  
-- Description: Get the Amount Information for Service Stations  
-- Modify by Stefano Quirós - 14/11/2017 - Add Void Transactions Validation  
-- Modify by Esteban Solís - 14/11/2017 - Add TopMonetaryLimit field  
-- Modify By: Stefano Quirós - 18/04/2018 - Add new table to the retrieve
-- Modify by:	Maria de los Angeles Jimenez Chavarria
-- Modify date:	Nov/05/2018
-- Description:	Ignore deleted ServiceStations.
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- Modify by Marjorie Garbanzo - 01/06/2020 Add logic to include transactions with different payment methods
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_AmountByServiceStationsReport_Retrieve]  
(    
	  @pEndDate DATETIME  
	 ,@pHasTerminals BIT   
	 ,@pPartnerId INT
	 ,@pIsAlert BIT = NULL  
)  
AS  
BEGIN  
	SET NOCOUNT ON;  
  
	DECLARE @pStartDate DATETIME = DATEADD(MONTH, DATEDIFF(MONTH, 0, @pEndDate), 0)  
		   ,@lTolerance DECIMAL(10, 2)

	--Get the % Tolerance for the Amount
	IF @pIsAlert = 1 
	BEGIN
		SELECT @lTolerance = ([StationTolerance] * 0.01)
		FROM [General].[GeneralParametersByPartner]
		WHERE [PartnerId] = @pPartnerId
	END
	ELSE 
		SET @pIsAlert = NULL

	IF @pHasTerminals = 1  
	BEGIN    
		SELECT ISNULL(s.[BacId], 'No Afiliado') AS [BacId]  
			  ,ISNULL(s.[Name], 'No Estación de Servicio') AS [ServiceStationName]  
			  ,ISNULL(t.[ProcessorId], '') AS [TerminalId]  
			  ,ISNULL(c.[Name], '') AS [CountryName]     
			  ,SUM(CASE WHEN t.[InsertDate] BETWEEN @pStartDate AND @pEndDate THEN t.[FuelAmount] ELSE 0 END) AS [MonthlyAmount]  
			  ,ISNULL(s.[TopMonetaryLimit], 0) AS [TopMonetaryLimit]
			  ,(ISNULL(s.[TopMonetaryLimit], 0) - SUM(CASE WHEN t.[InsertDate] BETWEEN @pStartDate AND @pEndDate THEN t.[FuelAmount] ELSE 0 END)) AS AvailableAmount
		FROM [Control].[Transactions] t  
		LEFT JOIN [General].[TerminalsByServiceStations] tcc  
			ON t.[ProcessorId] = tcc.[TerminalId]  
		LEFT JOIN [General].[ServiceStations] s  
			ON tcc.[ServiceStationId] = s.[ServiceStationId]  
		LEFT JOIN [Control].[ServiceStatationsByPartner] ssp
			ON ssp.[ServiceStationId] = s.[ServiceStationId]
		LEFT JOIN [General].[Countries] c  
			ON s.[CountryId] = c.[CountryId]  
		WHERE (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)  
		AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0) 
		AND 1 > (  
					SELECT ISNULL(COUNT(1), 0) 
					FROM [Control].[Transactions] t2  
					WHERE (t2.[CreditCardId] = t.[CreditCardId] 
							OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
					AND  t2.[TransactionPOS] = t.[TransactionPOS] 					
					AND  t2.[ProcessorId] = t.[ProcessorId] 
					AND  (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)  
				)  
		AND t.[ProcessorId] NOT IN ('COINCA01'  
									,'COINCATEST001'  
									,'TERMINAL009_QA'  
									,'TERMINAL010_QA'  
									,'TEST003'  
									,'TEST014'  
									,'TEST0806'  
									,'TEST800'  
									,'TESTF1120')  
		AND t.[InsertDate] BETWEEN @pStartDate AND @pEndDate  
		AND ssp.[PartnerId] = @pPartnerId  
		GROUP BY s.[BacId], s.[Name], t.[ProcessorId], c.[Name], s.[TopMonetaryLimit]  
		ORDER BY 1    
	END  
	ELSE  
	BEGIN    
		SELECT ISNULL(s.[BacId], 'No Afiliado') AS [BacId]  
			  ,ISNULL(s.[Name], 'No Estación de Servicio') AS [ServiceStationName]  
			  ,ISNULL(c.[Name], '') AS [CountryName]     
			  ,SUM(CASE WHEN t.[InsertDate] BETWEEN @pStartDate AND @pEndDate THEN t.[FuelAmount] ELSE 0 END) AS [MonthlyAmount]  
			  ,ISNULL(s.[TopMonetaryLimit], 0) AS [TopMonetaryLimit]
			  ,(ISNULL(s.[TopMonetaryLimit], 0) - SUM(CASE WHEN t.[InsertDate] BETWEEN @pStartDate AND @pEndDate THEN t.[FuelAmount] ELSE 0 END)) AS AvailableAmount
		FROM [Control].[Transactions] t  
		LEFT JOIN [General].[TerminalsByServiceStations] tcc  
			ON t.[ProcessorId] = tcc.[TerminalId]  
		LEFT JOIN [General].[ServiceStations] s  
			ON tcc.[ServiceStationId] = s.[ServiceStationId]  
		LEFT JOIN [Control].[ServiceStatationsByPartner] ssp
			ON ssp.[ServiceStationId] = s.[ServiceStationId]
		LEFT JOIN [General].[Countries] c  
			ON s.[CountryId] = c.[CountryId]  
		WHERE (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
		AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)  
		AND 1 > (  
					SELECT ISNULL(COUNT(1), 0) 
					FROM [Control].[Transactions] t2  
					WHERE t2.[CreditCardId] = t.[CreditCardId] 
					AND  t2.[TransactionPOS] = t.[TransactionPOS] 					
					AND  t2.[ProcessorId] = t.[ProcessorId] 
					AND  (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1) 
				)  
		AND t.[ProcessorId] NOT IN ('COINCA01'  
									,'COINCATEST001'  
									,'TERMINAL009_QA'  
									,'TERMINAL010_QA'  
									,'TEST003'  
									,'TEST014'  
									,'TEST0806'  
									,'TEST800'  
									,'TESTF1120')  
		AND t.[InsertDate] BETWEEN @pStartDate AND @pEndDate  
		AND ssp.[PartnerId] = @pPartnerId 		
		AND (s.[IsDeleted] IS NULL OR s.[IsDeleted] = 0)
		GROUP BY s.[BacId], s.[Name], c.[Name], s.[TopMonetaryLimit]
		HAVING (@pIsAlert IS NULL
				OR ([General].[Fn_GetServiceStationsCloseToLimit_Retrieve](ISNULL(s.[TopMonetaryLimit], 0),
																	       SUM(CASE WHEN t.[InsertDate] BETWEEN @pStartDate AND @pEndDate 
																				    THEN t.[FuelAmount] 
																				    ELSE 0 
																			   END),
																	        @lTolerance) > 0))   			
		ORDER BY 1   
	END    
	SET NOCOUNT OFF
END  