/****** Object:  StoredProcedure [Control].[Sp_CreditCardByNumber_Retrieve]    Script Date: 6/16/2020 1:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Napoleón Alvarado
-- Create date: 12/10/2014
-- Description:	Retrieve CreditCard information by Number
-- Updated date: 08/12/2016 - Sinndy Vargas
-- Description: Obtain Vehicle Name
-- Updated date: 09/21/2016 - Gerald Solano
-- Description: Se quita la validacion del estado de la tarjeta, ahora se valida solo en las reglas de negocio
-- Stefano Quirós - Add the AvailableLiters Validation to the retrive - 14/01/2019
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_CreditCardByNumber_Retrieve] --'A/K2ZethPU6jkhe6US7ydwslQnbH+7vp'
(
	 @pCreditCardNumber nvarchar(520)
)
AS
BEGIN	
	SET NOCOUNT ON
	
	DECLARE @lTipoEmision INT = 101 -- 101:VEHICULOS | 100:Conductores
	DECLARE @lConvertionValue DECIMAL(16, 8) = 3.78541178
	DECLARE @lCreditCardId INT
	DECLARE @lPaymentInstrumentId INT

	SELECT @lTipoEmision = c.[IssueForId]
	      ,@lCreditCardId = cc.[CreditCardId]
	FROM [Control].[CreditCard] cc
	INNER JOIN [General].[Customers] c
		ON c.CustomerId = cc.CustomerId
	WHERE cc.[CreditCardNumber] = @pCreditCardNumber

	IF @lCreditCardId IS NULL
	BEGIN
		SELECT @lTipoEmision = c.[IssueForId]
	          ,@lPaymentInstrumentId = [pi].[Id]
	FROM [Control].[PaymentInstruments] [pi]
	INNER JOIN [General].[Customers] c
		ON c.CustomerId = [pi].CustomerId
	WHERE [pi].[Code] = @pCreditCardNumber
	END

	IF (@lTipoEmision = 101) --- CONSULTA POR LAS EMISIONES POR VEHICULO
	BEGIN
		IF @lCreditCardId IS NOT NULL
		BEGIN
			SELECT   a.[CreditCardId]
					,a.[CustomerId]
					,c.[Name] AS [EncryptedCustomerName]
					,a.[CreditCardNumber] [PaymentInstrumentCode]
					,a.[ExpirationYear]
					,a.[ExpirationMonth]
					,a.[CreditAvailable]
					,CONVERT(DECIMAL(16,2), CASE WHEN p.[CapacityUnitId] = 1 
											THEN (a.[AvailableLiters] / @lConvertionValue)
											ELSE a.[AvailableLiters]
											END) [AvailableLiters]
					,p.[CapacityUnitId]
					,a.[StatusId]
					,b.[RowOrder] AS [Step]
					,b.[Name] AS [StatusName]
					,d.[Symbol] AS [CurrencySymbol]
					,c.[IssueForId]		
					,e.[UserId]
					,g.[VehicleId]
					,c.[CreditCardType]
					,f.Name AS [EncryptedDriverName]
					,'Vehículo: ' + h.[PlateId] AS [VehiclePlate]
					,h.[PlateId] AS [TransactionPlate]
					,h.[Name] AS [VehicleName]
					,a.CardRequestId
					,i.[EstimatedDelivery]
					,a.[RowVersion]
					,CAST(ISNULL((
						SELECT [Value]
						FROM [General].[ParametersByCustomer]
						WHERE [CustomerId] = c.[CustomerId]
							AND [Name] = 'TypeOfDistribution'
							AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
						), 0) AS INT) [TypeBudgetDistribution]
			FROM [Control].[CreditCard] a
			INNER JOIN [General].[Status] b
				ON b.[StatusId] = a.[StatusId]
			INNER JOIN [General].[Customers] c
				ON c.[CustomerId] = a.[CustomerId]
			INNER JOIN [General].[CustomersByPartner] cp
				ON cp.[CustomerId] = c.[CustomerId]
			INNER JOIN [General].[Partners] p
				ON p.[PartnerId] = cp.[PartnerId]
			INNER JOIN [Control].[Currencies] d
				ON d.[CurrencyId] = c.[CurrencyId]
			LEFT JOIN [Control].[CreditCardByDriver] e
				ON e.[CreditCardId] = a.[CreditCardId]
			LEFT JOIN [General].[Users] f
				ON f.[UserId] = e.[UserId]
			LEFT JOIN [Control].[CreditCardByVehicle] g
				ON g.[CreditCardId] = a.[CreditCardId]
			LEFT JOIN [General].[Vehicles] h
				ON h.[VehicleId] = g.[VehicleId]
			INNER JOIN [Control].[CardRequest] i
				ON a.[CardRequestId] = i.[CardRequestId]
			WHERE a.[CreditCardId] = @lCreditCardId	
		END
		ELSE
		BEGIN
			SELECT   a.[Id] [CreditCardId]
					,a.[CustomerId]
					,c.[Name] AS [EncryptedCustomerName]
					,a.[Code] [PaymentInstrumentCode]
					,a.[CreditAvailable]
					,CONVERT(DECIMAL(16,2), CASE WHEN p.[CapacityUnitId] = 1 
											THEN (a.[AvailableLiters] / @lConvertionValue)
											ELSE a.[AvailableLiters]
											END) [AvailableLiters]
					,p.[CapacityUnitId]
					,a.[StatusId]
					,b.[RowOrder] AS [Step]
					,b.[Name] AS [StatusName]
					,d.[Symbol] AS [CurrencySymbol]
					,c.[IssueForId]	
					,g.[VehicleId]
					,c.[CreditCardType]
					,'Vehículo: ' + h.[PlateId] AS [VehiclePlate]
					,h.[PlateId] AS [TransactionPlate]
					,h.[Name] AS [VehicleName]
					,g.[CardRequestId]
					,i.[EstimatedDelivery]
					,CAST(ISNULL((
						SELECT [Value]
						FROM [General].[ParametersByCustomer]
						WHERE [CustomerId] = c.[CustomerId]
							AND [Name] = 'TypeOfDistribution'
							AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
						), 0) AS INT) [TypeBudgetDistribution]
			FROM [Control].[PaymentInstruments] a
			INNER JOIN [General].[Status] b
				ON b.[StatusId] = a.[StatusId]
			INNER JOIN [General].[Customers] c
				ON c.[CustomerId] = a.[CustomerId]
			INNER JOIN [General].[CustomersByPartner] cp
				ON cp.[CustomerId] = c.[CustomerId]
			INNER JOIN [General].[Partners] p
				ON p.[PartnerId] = cp.[PartnerId]
			INNER JOIN [Control].[Currencies] d
				ON d.[CurrencyId] = c.[CurrencyId]
			LEFT JOIN [Control].[PaymentInstrumentsByType] g
				ON g.[PaymentInstrumentId] = a.[Id]
			LEFT JOIN [General].[Vehicles] h
				ON h.[VehicleId] = g.[VehicleId]
			INNER JOIN [Control].[CardRequest] i
				ON g.[CardRequestId] = i.[CardRequestId]
			WHERE a.[Id] = @lPaymentInstrumentId	
		END
	END
	ELSE
	BEGIN  
		SELECT   a.[CreditCardId]
				,a.[CustomerId]
				,c.[Name] AS [EncryptedCustomerName]
				,a.[CreditCardNumber]
				,a.[ExpirationYear]
				,a.[ExpirationMonth]				
				,a.[CreditAvailable]
				,CONVERT(DECIMAL(16,2), CASE WHEN p.[CapacityUnitId] = 1 
										THEN (a.[AvailableLiters] / @lConvertionValue)
										ELSE a.[AvailableLiters]
										END) [AvailableLiters]
				,a.[StatusId]
				,b.[RowOrder] AS [Step]
				,b.[Name] AS [StatusName]
				,d.[Symbol] AS [CurrencySymbol]
				,c.[IssueForId]		
				,e.[UserId]
				,h.[VehicleId]
				,c.[CreditCardType]
				,f.Name AS [EncryptedDriverName]
				,'Vehículo: ' + h.[PlateId] AS [VehiclePlate]
				,h.[PlateId] AS [TransactionPlate]
				,h.[Name] AS [VehicleName]
				,a.CardRequestId
				,i.[EstimatedDelivery]
				,a.[RowVersion]
				,CAST(ISNULL((
					SELECT [Value]
					FROM [General].[ParametersByCustomer]
					WHERE [CustomerId] = c.[CustomerId]
						AND [Name] = 'TypeOfDistribution'
						AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'
					), 0) AS INT) [TypeBudgetDistribution]
		FROM [Control].[CreditCard] a
		INNER JOIN [General].[Status] b
			ON b.[StatusId] = a.[StatusId]
		INNER JOIN [General].[Customers] c
			ON c.[CustomerId] = a.[CustomerId]
		INNER JOIN [General].[CustomersByPartner] cp
			ON cp.[CustomerId] = c.[CustomerId]
		INNER JOIN [General].[Partners] p
			ON p.[PartnerId] = cp.[PartnerId]
		INNER JOIN [Control].[Currencies] d
			ON d.[CurrencyId] = c.[CurrencyId]
		LEFT JOIN [Control].[CreditCardByDriver] e
			ON e.[CreditCardId] = a.[CreditCardId]
		LEFT JOIN [General].[Users] f
			ON f.[UserId] = e.[UserId]
		LEFT JOIN [General].[VehiclesDrivers] vd
			ON vd.[UserId] = f.[UserId]
		LEFT JOIN [General].[Vehicles] h
			ON h.[VehicleId] =vd.[VehicleId]
		INNER JOIN [Control].[CardRequest] i
			ON a.[CardRequestId] = i.[CardRequestId]
		WHERE a.[CreditCardId] = @lCreditCardId
	END

    SET NOCOUNT OFF
END


