USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_PerformanceByVehicle_Add]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_PerformanceByVehicle_Add]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero
-- Create date: 01/18/2015
-- Description:	Add Performance By Vehicle -- Massive process --
-- ================================================================================================
-- Stefano Quirós - Add validation that only process when IntrackReference is different to 0 or null
-- Stefano Quirós - MAY/21/2019 - Add filter IsDenied to the retrieve
-- Stefano Quirós y Marí Jiménez   - SET/06/2019 - Optimization task
-- Modify by Marjorie Garbanzo - 01/06/2020 Add logic to include transactions with different payment methods
-- ================================================================================================
CREATE PROCEDURE [General].[Sp_PerformanceByVehicle_Add]
AS 
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON

	DECLARE @lErrorMessage NVARCHAR(4000),
			@lErrorSeverity INT,
			@lErrorState INT
   	BEGIN TRY
			DECLARE @lTransactionId INT,
					@lLastInsertDate DATETIME,
					@lTrxDate DATETIME,
					@lPreviousTrxDate DATETIME,
					@lIntrackReference INT,
					@lIndex INT,
					@lMaxIndex INT

			SELECT TOP 1 @lTransactionId = [TransactionId]
						,@lLastInsertDate = [InsertDate]
			FROM [General].[PerformanceByVehicle]
			ORDER BY [TransactionId] DESC
		
			CREATE TABLE #TransactionsTemp
			(	 
				 [Index] INT IDENTITY(1,1) NOT NULL
				,[TransactionId] INT NULL
				,[VehicleId] INT NULL
				,[DeviceReference] INT NULL
				,[IntrackReference] INT NULL
				,[TrxDate] DATETIME NULL
				,[TrxLiters] DECIMAL(16,2) NULL
				,[TrxOdometer] DECIMAL(16,2) NULL
				,[GpsOdometer] DECIMAL(16,2) NULL
				,[PreviousTrxOdometer] DECIMAL(16,2) NULL
				,[PreviousGpsOdometer] DECIMAL(16,2) NULL
				,[PreviousTrxDate] DATETIME NULL
				,[GpsHoursOn] DECIMAL(16,2) NULL
			)
			INSERT INTO #TransactionsTemp
			(
				 [TransactionId]
				,[VehicleId]
				,[TrxDate]
				,[TrxOdometer]
				,[PreviousTrxOdometer]
				,[PreviousTrxDate]
				,[TrxLiters]
				,[DeviceReference]
				,[IntrackReference]
				,[GpsOdometer]
				,[PreviousGpsOdometer]
			)
			SELECT a.[TransactionId]
				  ,MAX(a.[VehicleId])
				  ,a.[Date]
				  ,a.Odometer AS [TrxOdometer]
				  ,CASE WHEN (SELECT COUNT(*)
				  		   FROM [Control].[Transactions] t
				  		   WHERE t.[VehicleId] = MAX(v.[VehicleId])
				  		   AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)
				  		   AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
				  		   AND 1 > (SELECT ISNULL(COUNT(1), 0)
				  		   		    FROM [Control].[Transactions] t2
				  		   		    WHERE (t2.[CreditCardId] = t.[CreditCardId]
											OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
				  		   		    AND t2.[TransactionPOS] = t.[TransactionPOS]
				  		   		    AND t2.[ProcessorId] = t.[ProcessorId]
				  		   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))) > 1
				  THEN (SELECT TOP 1 TT.[Odometer]
				  	  FROM(SELECT TOP 2 NT.*
				  		   FROM [Control].[Transactions] NT
				  		   WHERE NT.[VehicleId] = MAX(a.[VehicleId])
				  		   AND NT.[Date] <= a.[Date]
				  		   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
				  		   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
				  		   AND 1 > (SELECT ISNULL(COUNT(1), 0)
				  		   			FROM [Control].[Transactions] t2
				  		   			WHERE (t2.[CreditCardId] = nt.[CreditCardId]
												OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId])
				  		   			AND t2.[TransactionPOS] = nt.[TransactionPOS]
				  		   			AND t2.[ProcessorId] = nt.[ProcessorId]
				  		   			AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				  		            ORDER BY NT.[Date] DESC ) TT
				  		  ORDER BY TT.[Date] ASC)
				  ELSE 0
				  END AS [PreviousTrxOdometer]
			     ,CASE WHEN (SELECT COUNT(*)
						     FROM [Control].[Transactions] t
						     WHERE t.[VehicleId] = MAX(v.[VehicleId])
						     AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0)
						     AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
						     AND 1 > (SELECT ISNULL(COUNT(1), 0)
						     		  FROM [Control].[Transactions] t2
						     		  WHERE (t2.[CreditCardId] = t.[CreditCardId]
											OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
						     		  AND t2.[TransactionPOS] = t.[TransactionPOS]
						     		  AND t2.[ProcessorId] = t.[ProcessorId]
						     		  AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))) > 1
				  THEN (SELECT TOP 1 TT.[Date]
				  	  FROM(SELECT TOP 2 NT.*
				  		   FROM [Control].[Transactions] NT
				  		   WHERE NT.[VehicleId] = MAX(v.[VehicleId])
				  		   AND NT.[Date] <= a.[Date]
				  		   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
				  		   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
				  		   AND 1 > (SELECT ISNULL(COUNT(1), 0)
				  		   			FROM [Control].[Transactions] t2
				  		   			WHERE (t2.[CreditCardId] = nt.[CreditCardId]
											OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId])
				  		   			AND t2.[TransactionPOS] = nt.[TransactionPOS]
				  		   			AND t2.[ProcessorId] = nt.[ProcessorId]
				  		   			AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				  		            ORDER BY NT.[Date] DESC ) TT
				  		  ORDER BY TT.[Date] ASC)
				  ELSE 0
				  END AS [PreviousTrxDate]
				 ,a.[Liters]
				 ,MAX(v.[DeviceReference])
				 ,MAX(v.[IntrackReference])
				 ,MAX([Actual].[Odometer]) [GpsOdometer]
				 ,MIN([Actual].[Odometer]) [GpsPreviousOdometer]			
			FROM [Control].[Transactions] a WITH(READUNCOMMITTED)
			INNER JOIN [General].[Vehicles] v
				ON v.[VehicleId] = a.[VehicleId]	
			LEFT JOIN (SELECT [Odometer], [Device], [RepDATETIME]
					   FROM [dbo].[Reports]
					   WHERE [RepDATETIME] > @lLastInsertDate
			           ) [Actual]
							ON [RepDATETIME] BETWEEN (SELECT TOP 1 TT.[Date]
							    					  FROM(SELECT TOP 2 NT.*
													  	   FROM [Control].[Transactions] NT
													  	   WHERE NT.[VehicleId] = v.[VehicleId]
													  	   AND NT.[InsertDate] <= a.[InsertDate]
													  	   AND (nt.[IsFloating] IS NULL OR nt.[IsFloating] = 0)
													  	   AND (nt.[IsDenied] IS NULL OR nt.[IsDenied] = 0)
													  	   AND 1 > (SELECT ISNULL(COUNT(1), 0)
													  	            FROM [Control].[Transactions] t2
													  	   		    WHERE (t2.[CreditCardId] = nt.[CreditCardId]
																			OR t2.[PaymentInstrumentId] = nt.[PaymentInstrumentId])
													  	   		    AND t2.[TransactionPOS] = nt.[TransactionPOS]
													  	   		    AND t2.[ProcessorId] = nt.[ProcessorId]
													  	   		    AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1)
													  	      	    )
														   ORDER BY NT.[InsertDate] DESC
														   ) TT
													  ORDER BY TT.[InsertDate] ASC
											        )
					   AND a.[InsertDate]
					   AND v.[DeviceReference] IS NOT NULL
					   AND v.[DeviceReference] > 0
					   AND [Actual].[Device] = v.[DeviceReference]	
			WHERE  [TransactionId] > @lTransactionId AND (a.[IsAdjustment] = 0 OR a.[IsAdjustment] IS NULL)
			  AND a.[IsFloating] = 0
			  AND a.[IsReversed] = 0
			  AND a.[IsDuplicated] = 0
			  AND (a.[IsDenied] IS NULL OR a.[IsDenied] = 0)			 
			  AND 1 > 	(
							SELECT ISNULL(COUNT(1), 0)
							FROM [Control].Transactions t2
							WHERE (t2.[CreditCardId] = a.[CreditCardId]
									OR t2.[PaymentInstrumentId] = a.[PaymentInstrumentId])
							AND t2.[TransactionPOS] = a.[TransactionPOS]
							AND t2.[ProcessorId] = a.[ProcessorId]
							AND (
									t2.IsReversed = 1
									OR t2.IsVoid = 1
								)
						)	
			GROUP BY  a.[TransactionId], a.[Date], a.Odometer, a.[Liters], a.[InsertDate]	
			ORDER BY MAX(v.[VehicleId]), a.[TransactionId]

			UPDATE #TransactionsTemp
			SET [GpsHoursOn] = a.[GpsHoursOn]
			FROM
			(
				SELECT
					t.[Index],
					(
						SELECT [HoursOn]
						FROM [General].[Fn_GpsHobbsMeterByVehicle](
							 NULL
							,NULL
							,t.[PreviousTrxDate]
							,t.[TrxDate]
							,t.[IntrackReference]
						)
					) AS [GpsHoursOn]
				FROM #TransactionsTemp t
				WHERE t.[IntrackReference] <> 0 AND t.[IntrackReference] IS NOT NULL
			) a
			WHERE #TransactionsTemp.[Index] = a.[Index]

		INSERT [General].[PerformanceByVehicle]
		(
			 [VehicleId]
			,[TransactionId]
			,[TrxDate]
			,[TrxPreviousDate]
			,[TrxReportedLiters]
			,[TrxPreviousOdometer]
			,[TrxReportedOdometer]
			,[TrxPerformance]
			,[GpsPreviousOdometer]
			,[GpsReportedOdometer]
			,[GpsPerformance]
			,[GpsHoursOn]
			,[GpsHoursOnStr]
			,[InsertUserId]
			,[InsertDate]
		)
		SELECT
			 t.[VehicleId]
			,t.[TransactionId]
			,t.[TrxDate]
			,t.[PreviousTrxDate]
			,t.[TrxLiters]
			,t.[PreviousTrxOdometer]
			,t.[TrxOdometer]
			,ABS(t.[TrxOdometer] - CASE WHEN t.[PreviousTrxOdometer] IS NULL THEN t.[TrxOdometer] ELSE t.[PreviousTrxOdometer] END)
					/ CASE WHEN t.[TrxLiters] = 0 THEN 1 ELSE t.[TrxLiters] END
			 AS [PerformanceByOdometer]
			,t.[PreviousGpsOdometer]
			,t.[GpsOdometer]
			,ABS(ISNULL(t.[GpsOdometer],0) - CASE WHEN t.[PreviousGpsOdometer] IS NULL THEN ISNULL(t.[GpsOdometer],0)
														ELSE ISNULL(t.[PreviousGpsOdometer],0) END)
					/ CASE WHEN t.[TrxLiters] = 0 THEN 1 ELSE t.[TrxLiters] END
			 AS [GpsPerformance]
			,t.[GpsHoursOn]
			,(
				RIGHT('0' + CAST (FLOOR(t.[GpsHoursOn]) AS VARCHAR), 2) + ':' +
				RIGHT('0' + CAST(FLOOR((((t.[GpsHoursOn] * 3600) % 3600) / 60)) AS VARCHAR), 2) + ':' +
				RIGHT('0' + CAST (FLOOR((t.[GpsHoursOn] * 3600) % 60) AS VARCHAR), 2)
			) AS [GpsHoursOnStr]
			,0 AS [InsertUserId]
			,GETUTCDATE() AS [InsertDate]
		FROM #TransactionsTemp t
		DROP TABLE #TransactionsTemp		
    END TRY
	BEGIN CATCH		
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE()
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)
	END CATCH
    SET XACT_ABORT OFF
	SET NOCOUNT OFF
END

