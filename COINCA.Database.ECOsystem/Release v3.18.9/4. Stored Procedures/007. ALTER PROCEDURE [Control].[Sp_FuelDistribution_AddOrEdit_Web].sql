USE [ECOSystem]
GO
IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'Sp_FuelDistribution_AddOrEdit_Web' AND TYPE = 'P')
	DROP PROC [Control].[Sp_FuelDistribution_AddOrEdit_Web]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:		Stefano Quirós
-- Create date: March 06, 2018
-- Description:	Update Fuel Distribution
-- Modify By: Stefano Quirós 
-- Add the update to the AvailableLiters field - 16/01/2019
-- Stefano Quirós - Change a little bit the Logic when the distribution is by CostCenter
-- 10/09/2019
-- =========================================================================

CREATE PROCEDURE [Control].[Sp_FuelDistribution_AddOrEdit_Web]
(
	@pCustomerId INT,
	@pXmlData VARCHAR(MAX),
	@pLoggedUserId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @lxmlData XML = CONVERT(XML, @pXmlData)	
	DECLARE @lTimeZoneParameter INT
	DECLARE @lId INT = 0
	DECLARE @lClosingDate DATETIME = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)
	DECLARE @lFuelPermit BIT
	DECLARE @lValidateFuelDistribution DECIMAL(16, 2) = NULL
	DECLARE @lTypeDistribution INT = CAST(ISNULL((SELECT [Value]
												  FROM [General].[ParametersByCustomer]
												  WHERE [CustomerId] = @pCustomerId
												  AND [Name] = 'TypeOfDistribution'
												  AND [ResuourceKey] = 'TYPEOFDISTRIBUTION'), 0) AS INT)
	DECLARE @lCostCenterFuelDistribution BIT
		
	SELECT @lCostCenterFuelDistribution = ISNULL([CostCenterFuelDistribution], 0)
	      ,@lFuelPermit = [FuelPermit]
	FROM [General].[Customers] 
	WHERE [CustomerId] = @pCustomerId

	SELECT @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu 
	ON co.[CountryId] = cu.[CountryId]
	WHERE cu.[CustomerId] = @pCustomerId

	CREATE TABLE #Results
	(
		[Id] INT
	   ,[FuelId] INT
	   ,[CreditCardId] INT
	   ,[PaymentInstrumentId] INT
	   ,[Result] INT
	)	

	CREATE TABLE #DistributionData
	(
		[Available] DECIMAL(16, 2)
	   ,[Assigned] DECIMAL(16, 2)
	   ,[AssignedLiters] DECIMAL(16, 3)
	   ,[RealAmount] DECIMAL(16, 2)
	   ,[RealLiters] DECIMAL(16, 3)
	   ,[TotalAssigned] DECIMAL(16,2)
	   ,[Total] DECIMAL(16, 2)
	   ,[CreditCardId] INT
	   ,[PaymentInstrumentId] INT
	   ,[AvailableLiters] DECIMAL(16, 3)
	)
	
	SELECT m.c.value('Id[1][not(@xsi:nil = "true")]', 'INT') [Id] ,
	       m.c.value('VehicleId[1][not(@xsi:nil = "true")]', 'INT') [VehicleId], 
		   m.c.value('DriverId[1][not(@xsi:nil = "true")]', 'INT') [DriverId],
		   m.c.value('Month[1][not(@xsi:nil = "true")]', 'INT') [Month], 
		   m.c.value('Year[1][not(@xsi:nil = "true")]', 'INT') [Year], 
		   m.c.value('Amount[1][not(@xsi:nil = "true")]', 'DECIMAL(16,2)') [Amount],
		   m.c.value('Liters[1][not(@xsi:nil = "true")]', 'DECIMAL(16,2)') [Liters],
		   m.c.value('AdditionalAmount[1][not(@xsi:nil = "true")]', 'DECIMAL(16,2)') [AdditionalAmount],
		   m.c.value('AdditionalLiters[1][not(@xsi:nil = "true")]', 'DECIMAL(16,2)') [AdditionalLiters],
		   m.c.value('FuelName[1][not(@xsi:nil = "true")]', 'VARCHAR(50)') [FuelName],		   
		   m.c.value('FuelId[1]', 'INT') [FuelId],
		   m.c.value('PlateId[1][not(@xsi:nil = "true")]', 'VARCHAR(100)') [PlateId],
		   m.c.value('VehicleName[1][not(@xsi:nil = "true")]', 'VARCHAR(100)') [VehicleName],
		   m.c.value('VehicleModel[1][not(@xsi:nil = "true")]', 'VARCHAR(100)') [VehicleModel],
		   m.c.value('CurrencySymbol[1][not(@xsi:nil = "true")]', 'VARCHAR(10)') [CurrencySymbol],
		   m.c.value('LiterPrice[1][not(@xsi:nil = "true")]', 'DECIMAL(16,2)') [LiterPrice],
		   m.c.value('EncryptedName[1][not(@xsi:nil = "true")]', 'VARCHAR(300)') [EncryptedName],
		   m.c.value('EncryptedIdentification[1][not(@xsi:nil = "true")]', 'VARCHAR(300)') [EncryptedIdentification],
		   m.c.value('Total[1][not(@xsi:nil = "true")]', 'DECIMAL(16,2)') [ValidateFuelDistribution],
		   m.c.value('Symbol[1][not(@xsi:nil = "true")]', 'VARCHAR(1)') [Symbol]
	INTO #FuelDistribution
	FROM @lxmlData.nodes('//FuelDistribution') AS m(c)		

	SELECT @lId = MIN([Id]) FROM #FuelDistribution

	IF @lId IS NULL
	BEGIN
		INSERT INTO [Control].[FuelDistribution]
		(
			[VehicleId]
           ,[Month]
		   ,[Year]
		   ,[InsertDate]
		   ,[InsertUserId]
		   ,[FuelId]
		   ,[Amount]
		   ,[Liters]
		   ,[AdditionalAmount]
		   ,[AdditionalLiters]
		)
		SELECT [VehicleId]
		      ,DATEPART(MONTH, GETDATE())
			  ,DATEPART(YEAR, GETDATE())
			  ,GETDATE()
			  ,@pLoggedUserId
			  ,[FuelId]
			  ,[Amount]
			  ,[Liters]
			  ,[AdditionalAmount]
			  ,[AdditionalLiters]
		FROM #FuelDistribution
		WHERE [Id] IS NULL

		UPDATE fd 
		SET fd.[Id] = cfd.[Id]
		   ,fd.[VehicleId] = v.[VehicleId]
		   ,fd.[FuelName] = f.[Name]
		   ,fd.[FuelId] = f.[FuelId]
		   ,fd.[VehicleName] = v.[Name]
		   ,fd.[VehicleModel] = vc.[VehicleModel]
		   ,fd.[CurrencySymbol] = cu.[Symbol]
		   ,fd.[LiterPrice] = pf.[LiterPrice]
		   ,fd.[Month] = DATEPART(MONTH, GETDATE())
		   ,fd.[Year] = DATEPART(YEAR, GETDATE())
		FROM #FuelDistribution fd
		INNER JOIN [General].[Vehicles] v
		       ON fd.[PlateId] = v.[PlateId] 
		INNER JOIN [General].[Customers] c
			ON v.[CustomerId] = c.[CustomerId]
		INNER JOIN [Control].[Currencies] cu
			ON c.[CurrencyId] = cu.[CurrencyId]
		INNER JOIN [Control].[FuelDistribution] cfd
			ON cfd.[VehicleId] = v.[VehicleId]
		INNER JOIN [General].[VehicleCategories] vc
			ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
		INNER JOIN [Control].[Fuels] f
			ON fd.[FuelId] = f.[FuelId]
		INNER JOIN [Control].[PartnerFuel] pf
			ON pf.[FuelId] = f.[FuelId]
		WHERE cfd.[VehicleId] = fd.[VehicleId]--fd.[PlateId] = v.[PlateId] 
		      AND fd.[FuelId] = cfd.[FuelId]
			  AND cfd.[InsertDate] >= @lClosingDate;

		SELECT @lId = MIN([Id]) FROM #FuelDistribution
	END

	WHILE @lId IS NOT NULL
	BEGIN

		DECLARE @lVehicleId INT = 0 
		DECLARE @lDriverId INT = 0
		DECLARE @lAmount DECIMAL(16, 2) = 0
		DECLARE @lLiters DECIMAL(16, 2) = 0
		DECLARE @lAdditionalAmount DECIMAL(16, 2) = 0
		DECLARE @lAdditionalLiters DECIMAL(16, 2) = 0
		DECLARE @lFuelId INT = 0
		DECLARE @lCreditCardId INT
		DECLARE @lPaymentInstrumentId INT
		
		DECLARE @lAssigned DECIMAL(16, 2) = 0
		DECLARE @lAssignedLiters DECIMAL(16, 3) = 0
		DECLARE @lRealAmount DECIMAL(16, 2) = 0
		DECLARE @lRealLiters DECIMAL(16, 3)
		DECLARE @lTotalAssigned DECIMAL(16, 2) = 0
		DECLARE @lAvailable DECIMAL(16, 2) = 0
		DECLARE @lAvailableLiters DECIMAL (16, 3) = 0
		DECLARE @lTotal DECIMAL(16, 2) = 0
		
		DECLARE @lResult INT = NULL

		SELECT @lVehicleId = f.[VehicleId]
			  ,@lDriverId = f.[DriverId]
			  ,@lAmount = CASE WHEN f.[Amount] IS NULL AND f.[Liters] IS NOT NULL 
							THEN f.[Liters] * f.[LiterPrice]
							ELSE f.[Amount]
						  END
			  ,@lLiters = CASE WHEN f.[Liters] IS NULL AND f.[Amount] IS NOT NULL 
							THEN f.[Amount] / f.[LiterPrice]
							ELSE f.[Liters]
						  END	
			  ,@lAdditionalAmount = CASE WHEN f.[DriverId] IS NULL 
										THEN (CASE WHEN f.[Symbol] = '+'  
											  THEN fd.[AdditionalAmount] + f.[AdditionalAmount]  
											  ELSE fd.[AdditionalAmount] - f.[AdditionalAmount]  
											  END)
										ELSE f.[AdditionalAmount]
									END
			 ,@lAdditionalLiters =  CASE WHEN f.[DriverId] IS NULL 
										 THEN (CASE WHEN f.[Symbol] = '+'  
											   THEN fd.[AdditionalLiters] + f.[AdditionalLiters]  
											   ELSE fd.[AdditionalLiters] - f.[AdditionalLiters]  
											   END)
										ELSE f.[AdditionalLiters]
									END
			  ,@lFuelId = f.[FuelId]	
			  ,@lValidateFuelDistribution = [ValidateFuelDistribution]		  	  
		FROM #FuelDistribution f
		INNER JOIN [Control].[FuelDistribution] fd
			ON f.[Id] = fd.[Id]
		WHERE f.[Id] = @lId		
			  
		INSERT INTO #DistributionData
		EXEC [Control].[Sp_GetDistributionData] @lVehicleId, @lDriverId, @pCustomerId, @lAmount, @lAdditionalAmount, @lLiters, @lAdditionalLiters, @lFuelId, @lTimeZoneParameter

		SELECT @lAssigned = [Assigned]
			  ,@lAssignedLiters = [AssignedLiters]
			  ,@lRealAmount = [RealAmount]
			  ,@lRealLiters = [RealLiters]
			  ,@lTotalAssigned = [TotalAssigned]
			  ,@lAvailable = [Available]
			  ,@lAvailableLiters = [AvailableLiters]
			  ,@lTotal = [Total]
			  ,@lCreditCardId = [CreditCardId]
			  ,@lPaymentInstrumentId = [PaymentInstrumentId]
		FROM #DistributionData 

		DECLARE @lAssignedLorA DECIMAL(16, 3) = CASE WHEN @lTypeDistribution = 0 THEN @lAssigned ELSE @lAssignedLiters END
		DECLARE @lRealLorA DECIMAL(16, 3) = CASE WHEN @lTypeDistribution = 0 THEN @lRealAmount ELSE @lRealLiters END

		--Validación de si lo que se está asignando es mayor a las transacciones del período 
		EXEC [Control].[Sp_ValidateAvailable] @lDriverId, @lAssignedLorA, @lRealLorA, @lTotalAssigned, @lTotal, @lAvailable, @lCreditCardId, @pResult = @lResult OUTPUT 

		IF @lResult = 50002 AND @lValidateFuelDistribution IS NOT NULL--@lAdditionalAmount IS NOT NULL AND @lAdditionalAmount <> 0
		BEGIN
			SET @lResult = 0
		END
		
		IF @lResult = 0
		BEGIN		
			DECLARE @lBeforeAmount DECIMAL(16,2) = (SELECT [Amount]
			FROM [Control].[FuelDistribution]
			WHERE [Id] = @lId 
			AND ((@lVehicleId IS NOT NULL AND [VehicleId] = @lVehicleId) 
				  OR (@lDriverId IS NOT NULL AND [DriverId] = @lDriverId)))

			UPDATE [Control].[FuelDistribution] 
			SET [Amount] = CASE WHEN @lAmount IS NULL
							THEN [Amount]
							ELSE @lAmount
						   END 
			   ,[AdditionalAmount] = CASE WHEN @lAdditionalAmount IS NULL
										THEN [AdditionalAmount]
										ELSE @lAdditionalAmount
						             END 
			   ,[Liters] = CASE WHEN @lLiters IS NULL OR @lDriverId IS NOT NULL
							THEN [Liters]
							ELSE @lLiters
						   END  					   
			   ,[AdditionalLiters] = CASE WHEN @lAdditionalLiters IS NULL 
							THEN [AdditionalLiters]
							ELSE @lAdditionalLiters
						   END 
			   ,[ModifyDate] = GETDATE()
		       ,[ModifyUserId] = @pLoggedUserId
			WHERE [Id] = @lId 
			AND (
					(
					 @lVehicleId IS NOT NULL 
					 AND [VehicleId] = @lVehicleId
					 AND [FuelId] = @lFuelId
					) 
					OR 
				   (
					@lDriverId IS NOT NULL 
					AND [DriverId] = @lDriverId
				   )
				) 

            SELECT MAX(y.[VehicleId]) [VehicleId]
				  ,y.[FuelId]
				  ,(SUM([Amount]) + SUM([AdditionalAmount])) - [Control].[Fn_FuelDistribution_TransactionsAmount](@lPaymentInstrumentId, @lCreditCardId, @lTimeZoneParameter, @lClosingDate, NULL, y.[FuelId]) [Available]
			INTO #VehicleFuel
			FROM [Control].[FuelDistribution] fd
			INNER JOIN (SELECT vc.[DefaultFuelId] [FuelId]
							  ,v.[VehicleId]
						FROM [General].[Vehicles] v	
						INNER JOIN [General].[VehicleCategories] vc 
							ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
						INNER JOIN [Control].[Fuels] f	
								ON f.[FuelId] = vc.[DefaultFuelId]
						WHERE v.[CustomerId] = @pCustomerId
						UNION
						SELECT fvc.[FuelId] 
						      ,v.[VehicleId]
						FROM [General].[Vehicles] v
						INNER JOIN [General].[VehicleCategories] vc
								ON vc.[VehicleCategoryId] = v.[VehicleCategoryId]
						INNER JOIN [General].[FuelsByVehicleCategory] fvc
								ON fvc.[VehicleCategoryId] = vc.[VehicleCategoryId]
						INNER JOIN [Control].[Fuels] f	
								ON f.[FuelId] = fvc.[FuelId]
						WHERE v.[CustomerId] = @pCustomerId
						) y
							ON y.[FuelId] = fd.[FuelId]
							AND y.[VehicleId] = fd.[VehicleId] 
			WHERE y.[VehicleId] = @lVehicleId
			AND fd.[InsertDate] >= @lClosingDate
			GROUP BY y.[FuelId]

			IF @lCreditCardId IS NOT NULL
			BEGIN
				UPDATE [Control].[CreditCard]
				SET CreditAvailable = CASE WHEN @lVehicleId IS NOT NULL AND @lFuelPermit = 1
									  THEN (SELECT SUM([Available]) 
											FROM #VehicleFuel)
									  ELSE @lAvailable
									  END
				   ,[AvailableLiters] = CASE WHEN @lVehicleId IS NOT NULL AND @lFuelPermit = 1
									    THEN (SELECT SUM([Liters]) + SUM([AdditionalLiters]) 
										      FROM [Control].[FuelDistribution]
											  WHERE [VehicleId] = @lVehicleId
											  AND [InsertDate] >= @lClosingDate) - [Control].[Fn_FuelDistribution_TransactionsLiters](null, @lCreditCardId, @lTimeZoneParameter, @lClosingDate, NULL, 1, NULL)
									    ELSE @lAvailableLiters
									    END
				WHERE CreditCardId = @lCreditCardId
			END
			ELSE
			BEGIN
				UPDATE [Control].[PaymentInstruments]
				SET CreditAvailable = CASE WHEN @lVehicleId IS NOT NULL AND @lFuelPermit = 1
									  THEN (SELECT SUM([Available]) 
											FROM #VehicleFuel)
									  ELSE @lAvailable
									  END
				   ,[AvailableLiters] = CASE WHEN @lVehicleId IS NOT NULL AND @lFuelPermit = 1
									    THEN (SELECT SUM([Liters]) + SUM([AdditionalLiters]) 
										      FROM [Control].[FuelDistribution]
											  WHERE [VehicleId] = @lVehicleId
											  AND [InsertDate] >= @lClosingDate) - [Control].[Fn_FuelDistribution_TransactionsLiters](@lPaymentInstrumentId, null, @lTimeZoneParameter, @lClosingDate, NULL, 1, NULL)
									    ELSE @lAvailableLiters
									    END
				WHERE Id = @lPaymentInstrumentId
			END

			IF @lVehicleId IS NOT NULL
			BEGIN
				DECLARE @lFuelByCreditId INT

				IF @lCostCenterFuelDistribution = 1
				BEGIN
					
					DECLARE @lCostCenterId INT = (SELECT [CostCenterId]
					FROM [General].[Vehicles]
					WHERE [VehicleId] = @lVehicleId)
					
					SELECT TOP 1 @lFuelByCreditId = [Id]
					FROM [Control].[FuelsByCreditByCostCenter]
					WHERE [CostCenterId] = @lCostCenterId 
						  AND [FuelId] = @lFuelId
					ORDER BY [InsertDate] DESC

					UPDATE [Control].[FuelsByCreditByCostCenter]
					SET [AssignedAmount] = ([AssignedAmount] - @lBeforeAmount) + @lAmount
					WHERE [Id] = @lFuelByCreditId 
				
				END
				ELSE
				BEGIN
					SELECT TOP 1 @lFuelByCreditId = [FuelByCreditId]
					FROM [Control].[FuelsByCredit]
					WHERE [FuelId] = @lFuelId
						  AND [CustomerId] = @pCustomerId
					ORDER BY [InsertDate] DESC

					UPDATE [Control].[FuelsByCredit]
					SET [Assigned]= ([Assigned] - @lBeforeAmount) + @lAmount
					WHERE [FuelByCreditId] = @lFuelByCreditId 
					
				END
			END	
			DROP TABLE #VehicleFuel	
		END			

		INSERT INTO #Results
		SELECT ISNULL(@lVehicleId, @lDriverId)
		      ,@lFuelId
			  ,@lCreditCardId
			  ,@lPaymentInstrumentId
			  ,@lResult
		
		DELETE FROM #DistributionData

		SELECT @lId = MIN([Id]) FROM #FuelDistribution WHERE [Id] >  @lId
	END

	SELECT  fd.[Id]
		   ,fd.[VehicleId]
		   ,fd.[DriverId]
		   ,fd.[Month]
		   ,fd.[Year]
	       ,CASE WHEN fd.[Amount] IS NULL AND fd.[Liters] IS NOT NULL 
							THEN fd.[Liters] * fd.[LiterPrice]
							ELSE fd.[Amount]
						  END [Amount]
		   ,CASE WHEN fd.[Liters] IS NULL AND fd.[Amount] IS NOT NULL 
							THEN fd.[Amount] / [LiterPrice]
							ELSE fd.[Liters]
						  END [Liters]
		   ,fd.[AdditionalAmount]
		   ,fd.[AdditionalLiters]
		   ,r.[Result]
		   ,fd.[CurrencySymbol]
		   ,fd.[FuelId]
		   ,fd.[FuelName]
		   ,fd.[PlateId]
		   ,fd.[VehicleName]
		   ,fd.[VehicleModel]
		   ,fd.[LiterPrice]
		   ,fd.[EncryptedName]
		   ,fd.[EncryptedIdentification]
		   ,fd.[FuelId]
		   ,(ISNULL(fd.[Amount], 0) + ISNULL(fd.[AdditionalAmount], 0)) - [Control].[Fn_FuelDistribution_TransactionsAmount](r.[PaymentInstrumentId], r.[CreditCardId], @lTimeZoneParameter, @lClosingDate, NULL, fd.[FuelId]) [Available]
		   ,(ISNULL(fd.[Amount], 0) + ISNULL(fd.[AdditionalAmount], 0)) [Assigned]
		   ,(ISNULL(fd.[Liters], 0) + ISNULL(fd.[AdditionalLiters], 0)) - [Control].[Fn_FuelDistribution_TransactionsLiters](r.[PaymentInstrumentId], r.[CreditCardId], @lTimeZoneParameter, @lClosingDate, NULL, NULL, fd.[FuelId]) [AvailableLiters]
		   ,fd.[Symbol]
		   ,CASE WHEN r.[PaymentInstrumentId] IS NOT NULL
			THEN (SELECT [Name]
				  FROM [Control].[PaymentInstrumentsTypes] pit
				  INNER JOIN [Control].[PaymentInstruments] [pi]
					ON [pi].[TypeId] = pit.[Id] 
				  WHERE [pi].[Id] = r.[PaymentInstrumentId]) 
			ELSE 'Tarjeta'
			END [PaymentInstrumentName]	
	FROM #FuelDistribution fd
	INNER JOIN #Results r
		ON (
		    (
			 fd.[VehicleId] = r.[Id]
			 AND fd.[FuelId] = r.[FuelId]
			)  
			OR fd.[DriverId] = r.[Id]
		   )  
END
