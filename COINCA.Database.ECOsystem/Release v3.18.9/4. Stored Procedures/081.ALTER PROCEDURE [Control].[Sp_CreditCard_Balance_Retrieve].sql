/****** Object:  StoredProcedure [Control].[Sp_CreditCard_Balance_Retrieve]    Script Date: 6/4/2020 6:19:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 09/11/2014
-- Description:	Retrieve CreditCard Balance information
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_CreditCard_Balance_Retrieve]
(
	  @pCreditCardNumber NVARCHAR(520) = NULL		--@pCustomerId: Customer Id
	 ,@pExpirationYear INT = NULL			--@pExpirationYear: Expiration Year
	 ,@pExpirationMonth INT	= NULL			--@pExpirationMonth: Expiration Month
	 ,@pEncryptedUID VARCHAR(200) = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON
	IF @pCreditCardNumber IS NOT NULL
	BEGIN
		SELECT a.[CreditAvailable]
		FROM [Control].[CreditCard] a
		WHERE a.[CreditCardNumber] = @pCreditCardNumber
		AND (@pExpirationYear IS NULL OR a.[ExpirationYear] = @pExpirationYear)
		AND (@pExpirationMonth IS NULL OR a.[ExpirationMonth] = @pExpirationMonth)
	END
	ELSE
	BEGIN
		SELECT a.[CreditAvailable]
		FROM [Control].[PaymentInstruments] a
		WHERE a.[Code] = @pEncryptedUID
	END
	  
    SET NOCOUNT OFF
END
