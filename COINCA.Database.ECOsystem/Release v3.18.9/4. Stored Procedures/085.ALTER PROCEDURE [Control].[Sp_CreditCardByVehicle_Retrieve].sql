USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardByVehicle_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardByVehicle_Retrieve]  
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Author:  Stefano Quirós  
-- Create date: 10/11/2018 
-- Description: Retrieve the CreditCards assign to the vehicles
-- Modify by Marjorie Garbanzo - 28/05/2020 Add PaymentInstrumentCode and PaymentInstrumentTypeName column
-- ================================================================================================  

CREATE PROCEDURE [Control].[Sp_CreditCardByVehicle_Retrieve] --29351
(
	 @pVehicleId INT
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT cc.[CreditCardNumber]
		  ,cc.[ExpirationYear]
		  ,cc.[ExpirationMonth]
		  ,cc.[StatusId]
		  ,s.[Name] AS [StatusName]
		  ,cc.[CreditCardNumber] AS [PaymentInstrumentCode]
		  ,'Tarjeta' AS [PaymentInstrumentTypeName]
	FROM [Control].[CreditCard] cc
	INNER JOIN [General].[Status] s
		ON s.[StatusId] = cc.[StatusId]
	LEFT JOIN [Control].[CreditCardByVehicle] cv
		ON cv.[CreditCardId] = cc.[CreditCardId]
	WHERE cv.[VehicleId] = @pVehicleId
	UNION
	SELECT NULL
		  ,NULL
		  ,NULL
		  ,pay.[StatusId]
		  ,s.[Name] AS [StatusName]
		  ,pay.[Code] AS [PaymentInstrumentCode]
		  ,pit.[Name] AS [PaymentInstrumentTypeName]
	FROM [Control].[PaymentInstrumentsByType] pbt
	INNER JOIN [Control].[PaymentInstruments] pay
		ON pbt.[PaymentInstrumentId] = pay.[Id]
	INNER JOIN [Control].[PaymentInstrumentsTypes] pit
		ON pay.[typeId] = pit.[Id]
	INNER JOIN [General].[Status] s
		ON s.[StatusId] = pay.[StatusId]
	WHERE pbt.[VehicleId] = @pVehicleId

	SET NOCOUNT OFF
END 