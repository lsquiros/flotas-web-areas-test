/****** Object:  StoredProcedure [Control].[Sp_TransactionsExportSAPReport_Retrieve]    Script Date: 02/06/2020 10:51:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 20/09/2017
-- Description:	Retrieve Transactions SAP File Report information for Dos Pinos
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Modify by Marjorie Garbanzo - Add filter IsDenied to the retrieve - 21/05/2019
-- Modify by Jason Bonilla - 01/06/2020 - Add PaymentInstrumentCode and Transactions Validations
-- ================================================================================================

ALTER PROCEDURE [Control].[Sp_TransactionsExportSAPReport_Retrieve]
(	
	@pMonth INT = NULL,
	@pYear INT = NULL,	
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL,
	@pCustomerId INT,	
	@pUserId INT 
)
AS
BEGIN
	SET NOCOUNT ON	
	
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT,
			@lTimeZoneParameter INT

	-- DYNAMIC FILTER
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	
	SELECT @lTimeZoneParameter = [TimeZone]
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId
	
	SELECT v.[ExternalId] [VehicleSAP],
	       DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) [Date],
		   [General].[Fn_GetDriverInformationBasedOnDate] (v.[VehicleId], t.[InsertDate], 1) [Responsable],		   
		   --(SELECT TOP 1 [Number_Provider]
		   --FROM [General].[ServiceStationsCustomName] ssc
		   --WHERE ssc.[ServiceStationRef] = (SELECT TOP 1 ss3.[ServiceStationId] 
		   --  		 		  					 FROM [General].[ServiceStations] ss3 
		   --  									 WHERE ss3.[BacId] = tss.[BacId])
		   --AND [InsertUserId] = @pCustomerId) [ServiceStation],
		   '' [ServiceStation],
		   t.[Liters],
		   '' [Route],
		   t.[Odometer]
	FROM [Control].[Transactions] t
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]	
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]		
	INNER JOIN [General].[VehicleCostCenters] e
	    ON t.CostCenterId = e.CostCenterId
	INNER JOIN [General].[VehicleUnits] vu
		ON e.[UnitId] = vu.[UnitId] 
	LEFT OUTER JOIN [General].[ServiceStations] ss
		ON t.[ProcessorId] = ss.[Terminal]
	LEFT OUTER JOIN [General].[TerminalsByServiceStations] tss
		ON t.[ProcessorId] = tss.[TerminalId]	
	WHERE (@count = 0 OR v.[VehicleId] IN (SELECT items 
										   FROM @Results)) 
	AND c.[CustomerId] = @pCustomerId 
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
	AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)
	AND 1 > (SELECT ISNULL(COUNT(1), 0) 
			 FROM [Control].[Transactions] t2
			 WHERE (t2.[CreditCardId] = t.[CreditCardId] 
					OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
			 AND t2.[TransactionPOS] = t.[TransactionPOS] 
			 AND t2.[ProcessorId] = t.[ProcessorId] 
			 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))	
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear) 
			OR (@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL 
				AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))
	ORDER BY t.[Date] DESC

	SET NOCOUNT OFF
END


