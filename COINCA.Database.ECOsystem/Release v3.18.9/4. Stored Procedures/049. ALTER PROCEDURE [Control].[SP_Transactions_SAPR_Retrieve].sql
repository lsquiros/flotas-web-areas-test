/****** Object:  StoredProcedure [Control].[SP_Transactions_SAPR_Retrieve]    Script Date: 02/06/2020 12:33:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================================================================================
-- Author:		Henry Retana
-- Create date: 11/05/2015
-- Description:	Retrieve all the process transactions which are going to be displayed in the txt report. 
-- Update, sorting by merchant description, also there is a change when retrieve the merchant name
-- Modify By: Stefano Quirós - Add the Time Zone Parameter - 12/06/2016
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- Modify by Jason Bonilla - 01/06/2020 - Add PaymentInstrumentCode and Transactions Validations
-- =======================================================================================================================================

ALTER PROCEDURE [Control].[SP_Transactions_SAPR_Retrieve]
(
		@pCustomerId INT,					--@pCustomerId: CustomerId
		@pKey VARCHAR(800) = NULL,			--@pKey :Key
		@pYear INT = NULL,					--@pYear: Year
		@pMonth INT = NULL,					--@pMonth: Month
		@pStartDate DATETIME = NULL,		--@pStartDate: Start Date
		@pEndDate DATETIME = NULL			--@pEndDate: End Date
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @lTimeZoneParameter INT 

	SELECT @lTimeZoneParameter = [TimeZone]	
	FROM [General].[Countries] co 
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId] = @pCustomerId
		
	SELECT cc.[CreditCardNumber],	
			t.[TransactionPOS] AS [SystemTraceCode],
			t.[Date],
			t.[ProcessorId] AS [TerminalId],
			t.[MerchantDescription] [MerchantDescription],	
			t.[Odometer], 
			t.[Liters],				
			t.[FuelAmount],	  
			(SELECT tps.[TransportData] 
			FROM [Control].[LogTransactionsPOS] tps
			WHERE tps.[CCTransactionPOSId] = t.[TransactionId]) AS [TransportData],			
			ISNULL(t.[Invoice], '1') AS [Invoice],			
			(SELECT m.[MCC_CODE_SAPR] FROM [General].[Parameters] m) as [MCC]
			,ISNULL(cc.[CreditCardNumber], pay.[Code]) [PaymentInstrumentCode]
			,(CASE WHEN t.[PaymentInstrumentId] IS NULL
			  THEN 'Tarjeta'
			  ELSE pit.[Name] END) AS [PaymentInstrumentType]
	FROM  [Control].[Transactions] t
	INNER JOIN [General].[Vehicles] v 
		ON t.[VehicleId] = v.[VehicleId]
	INNER JOIN [General].[Customers] c 
		ON v.[CustomerId] = c.[CustomerId]
	INNER JOIN [Control].[Fuels] f 
		ON t.[FuelId] = f.[FuelId]
	INNER JOIN [Control].[CreditCardByVehicle] cv 
		ON t.[CreditCardId] = cv.[CreditCardId]			
	LEFT JOIN [Control].[CreditCard] cc
		ON cc.CreditCardId = t.CreditCardId
	LEFT JOIN [Control].[PaymentInstruments] pay
		ON t.[PaymentInstrumentId] = pay.[Id]
	LEFT JOIN [Control].[PaymentInstrumentsTypes] pit
		ON pay.[typeId] = pit.[Id]

	WHERE	c.[CustomerId] = @pCustomerId   
	AND t.IsReversed = 0 			
	AND t.IsDuplicated = 0 
	AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
	AND (t.[IsDenied] IS NULL OR t.[IsDenied] = 0)
	AND 1 > (SELECT ISNULL(COUNT(1), 0) 
				FROM [Control].[Transactions] t2
				WHERE (t2.[CreditCardId] = t.[CreditCardId] 
					   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
				AND t2.[TransactionPOS] = t.[TransactionPOS] 
				AND t2.[ProcessorId] = t.[ProcessorId] 
				AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))	
	AND (@pKey IS NULL 
		OR v.[PlateId] like '%'+@pKey+'%')
	AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
			AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
			AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear) OR
		(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
			AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate AND @pEndDate))	
	ORDER BY [MerchantDescription] ASC, t.[Date] DESC

	SET NOCOUNT OFF
END

