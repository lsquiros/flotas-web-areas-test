
-- ================================================================================================    
-- Author:  Danilo Hidalgo.    
-- Create date: 04/13/2016    
-- Description: Retrieve Additional Vehicle Fuel information    
-- Modify By:   Stefano Quirós Ruiz    
-- Description: Top 200 of the consult and add new column on the retrieve AvailableFuel - 12/01/2016    
-- Change balance liters calculate    
-- Henry Retana 20/06/2017    
-- Add the customer alarm parameter for the customer    
-- Esteban Solis 2017-10-13  
-- Remove check for amount on fuel table  
-- Modify by Stefano Quirós - 14/11/2017 - Add new logic to retrieve last monthly Closing   
-- and Add Void Transactions Validation  
-- Modify By: Esteban Solís  - 11/12/2017  - Added [General].[Fn_LatestMonthlyClosing_Retrieve] to get last closing date
-- Modify by Esteban Solís - 11/12/2017 - Fix issue with insert date on transactions
-- Modify: 28/02/2018 - Henry Retana - Validates Gallons and Liters
-- Henry Retana - 21/05/2019 - Add Denied Filters
-- Juank Santamaria - 21-07-2020 - Add Field Symbol with plus (+)
-- Juank Santamaria - 23-07-2020 - QUERIE is Equals [Control].[SP_FuelDistribution_Retrieve]
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_AdditionalVehicleFuelLowPorcentage_Retrieve]
(    	 
--declare
	  @pCustomerId INT = null
	 ,@pUserId INT = null
)    
AS    
BEGIN 
	SET NOCOUNT ON    
    
	-- DYNAMIC FILTER    
	DECLARE @Results TABLE (items INT)    
  	 DECLARE @count INT
			,@lIssueForId INT
			,@lFuelPermit BIT
			,@lCustomerCapacityUnit VARCHAR(100)
			,@lTypeOfDistributionId VARCHAR(MAX)
			,@lTypeOfDistribution VARCHAR(25)
			,@pPorcent INT = NULL 
			,@lInsertDate DATETIME
			,@pYear INT = NULL
			,@pMonth INT = NULL
			,@lFuelByCreditId INT   
			,@lIssueFor VARCHAR(100)

	INSERT @Results    
	EXEC dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pCustomerId   
	SET  @count = (SELECT COUNT(*) FROM @Results)    
	-- END    
		
	--GET THE PARAMETER BASED ON THE CUSTOMER INFORMATION    
	SELECT @pPorcent = ISNULL([ParameterValue], @pPorcent)    
	FROM [General].[AlertsByCustomer]    
	WHERE [AlertId] = 1     
	AND [CustomerId] = @pCustomerId    
    
	DECLARE @tableFuelsTemp TABLE 
	(    
		 [AdditionalFuelByCreditId] INT NULL
		,[FuelId] INT NULL    
		,[Year] INT NULL  
		,[Month] INT NULL    
		,[Total] DECIMAL(16,2) NULL
		,[Assigned] DECIMAL(18, 0) NULL
		,[Available] DECIMAL(21, 2) NULL    
		,[FuelName] VARCHAR(50)
		,[AssignedLiters] DECIMAL(16,3)
		,[Liters] DECIMAL(16,3)
		,[LiterPrice]  DECIMAL (16,2)
		,[CurrencySymbol] NVARCHAR(4)
		,[RowVersion] VARCHAR(1000)
		,[UnitOfCapacityId] INT
		,[UnitOfCapacity] VARCHAR(100)    
	)    
   
	SET @lInsertDate = [General].[Fn_LatestMonthlyClosing_Retrieve] (@pCustomerId)
	SET @pYear = DATEPART(YEAR, @lInsertDate)
	SET @pMonth = DATEPART(MONTH, @lInsertDate)
		 
    
	INSERT INTO @tableFuelsTemp    
	EXEC [Control].[Sp_FuelsByCredit_Retrieve] @pCustomerId, @pYear, @pMonth
    
	DECLARE @lCustomerCapacityUnitId INT     
	DECLARE @lEquival DECIMAL(18,8) = 3.78541178    
             
	SELECT @lCustomerCapacityUnitId = [UnitOfCapacityId] 
	FROM [General].[Customers] 
	WHERE [CustomerId] = @pCustomerId    

	DECLARE @lTimeZoneParameter INT 
	DECLARE @PartnerCapacityUnit INT
		
	SELECT @PartnerCapacityUnit = p.[CapacityUnitId],
		   @lCustomerCapacityUnitId = c.[UnitOfCapacityId],
		   @lIssueForId = c.[IssueForId], 
           @lFuelPermit = [FuelPermit], 
		   @lTimeZoneParameter = [TimeZone]
	FROM [General].[Partners] p
	INNER JOIN [General].[CustomersByPartner] cbp
		ON p.[PartnerId] = cbp.[PartnerId]
	INNER JOIN [General].[Customers] c
		ON cbp.[CustomerId] = c.[CustomerId]
	INNER JOIN [General].[Countries] co
		ON co.[CountryId] = c.[CountryId]
	WHERE cbp.[CustomerId] = @pCustomerId

	SELECT @lCustomerCapacityUnit = [Name]
    FROM [General].[Types]
    WHERE [TypeId] = @lCustomerCapacityUnitId;

    SELECT @lIssueFor = [Name]
    FROM [General].[Types]
    WHERE [TypeId] = @lIssueForId

	SELECT @lTypeOfDistributionId = [Value]
	FROM [General].[ParametersByCustomer]
	WHERE [Name] = 'TypeOfDistribution'
	AND [CustomerId] = @pCustomerId;
	
	IF @lTypeOfDistributionId = '1'
		SET @lTypeOfDistribution = 'DISTRIBUTION_BY_LITERS';	
	ELSE
	BEGIN
		SET @lTypeOfDistributionId = '0';
		SET @lTypeOfDistribution = 'DISTRIBUTION_BY_AMOUNT';
	END;
	
	IF (@lCustomerCapacityUnitId = 0) SET @lEquival = 1
    
	SELECT TOP 201   a.[Id]    
					,a.[VehicleId]    
					,a.[Month]    
					,a.[Year]      
					,a.[Liters]    
					,a.[Amount]    
					,ISNULL(a.[AdditionalLiters],0) AS [AdditionalLiters]    
					,ISNULL(a.[AdditionalAmount], CONVERT(DECIMAL(16,2),0)) AS [AdditionalAmount]  
                    ,d.[Name] [FuelName]
					,d.[FuelId]
					,b.[PlateId]
					,b.[Name] AS VehicleName    
					,c.[VehicleModel]    
					,f.[Symbol] AS [CurrencySymbol]
					,ISNULL((SELECT Top 1 ISNULL(tmp.[LiterPrice] * @lEquival, 0) 
				  		   		    	FROM @tableFuelsTemp tmp 
				  		   		    	WHERE tmp.[FuelId] = d.[FuelId]), 0) [LiterPrice] 
					,b.[CostCenterId]
					,@lCustomerCapacityUnitId as [UnitOfCapacityId]
					,@lCustomerCapacityUnit AS [UnitOfCapacity]
					,@lIssueForId [IssueForId]
                    ,@lIssueFor AS [IssueFor] 
                    ,@lTypeOfDistributionId [TypeOfDistributionId] 
                    ,@lTypeOfDistribution [TypeOfDistribution]
					,(ISNULL(a.[Amount], 0) + ISNULL(a.[AdditionalAmount], 0)) - [Control].[Fn_FuelDistribution_TransactionsAmount]([pi].[Id], ccc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, NULL, d.[FuelId]) [Available], 
                    (ISNULL(a.[Amount], 0) + ISNULL(a.[AdditionalAmount], 0)) [Assigned], 
                    (ISNULL(a.[Liters], 0) + ISNULL(a.[AdditionalLiters], 0)) - [Control].[Fn_FuelDistribution_TransactionsLiters]([pi].[Id], ccc.[CreditCardId], @lTimeZoneParameter, @lInsertDate, NULL, NULL, d.[FuelId]) [AvailableLiters], 
                    '+' [Symbol],
                    CASE
                        WHEN [pi].[Id] IS NOT NULL
                        THEN [pits].[Name]
                        ELSE 'Tarjeta'
                    END [PaymentInstrumentName]
	FROM [Control].[FuelDistribution] a    
	INNER JOIN [General].[Vehicles] b    
		ON a.[VehicleId] = b.[VehicleId]    
	INNER JOIN [General].[VehicleCategories] c    
		ON b.[VehicleCategoryId] = c.[VehicleCategoryId]   
	LEFT JOIN [Control].[CreditCardByVehicle] cv ON cv.[VehicleId] = b.[VehicleId]
    LEFT JOIN [Control].[CreditCard] ccc ON cv.[CreditCardId] = ccc.[CreditCardId]
    LEFT JOIN [Control].[PaymentInstrumentsByType] pit ON pit.[VehicleId] = b.[VehicleId]
    LEFT JOIN [Control].[PaymentInstruments] [pi] ON [pi].[Id] = pit.[PaymentInstrumentId]
    LEFT JOIN [Control].[PaymentInstrumentsTypes] pits ON pits.[Id] = [pi].[TypeId]
	INNER JOIN [Control].[Fuels] d    
		ON c.[DefaultFuelId] = d.[FuelId]    
	INNER JOIN [General].[Customers] e    
		ON b.[CustomerId] = e.[CustomerId]    
	INNER JOIN [Control].[Currencies] f    
		ON e.[CurrencyId] = f.[CurrencyId]    
	LEFT JOIN ( 
				SELECT y.[VehicleId]    
						,SUM(x.[FuelAmount]) AS [RealAmount]    
						,SUM(x.[Liters]) AS [RealLiters]    
				FROM [Control].[Transactions] x    
				INNER JOIN [General].[Vehicles] y 
					ON x.[VehicleId] = y.[VehicleId]    
				WHERE y.[CustomerId] = @pCustomerId    
				AND x.[IsFloating] = 0    
				AND x.[IsReversed] = 0    
				AND x.[IsDuplicated] = 0    
				AND (x.[IsDenied] IS NULL OR x.[IsDenied] = 0)
				AND y.[Active] = 1    
				AND 1 > (    
							SELECT COUNT(*) 
							FROM [Control].[Transactions] t2    
							WHERE (t2.[CreditCardId] = x.[CreditCardId] 
									OR t2.[PaymentInstrumentId] = x.[PaymentInstrumentId])
							AND t2.[TransactionPOS] = x.[TransactionPOS] 
							AND t2.[ProcessorId] = x.[ProcessorId]
							AND (t2.IsReversed = 1 OR t2.IsVoid = 1)  
						)   
				AND CONVERT(DATE, DATEADD(HOUR, @lTimeZoneParameter, x.[InsertDate])) >= @lInsertDate   
				GROUP BY y.[VehicleId]
			)t    
			ON a.[VehicleId] = t.[VehicleId]    
	WHERE (@pCustomerId IS NULL OR b.[CustomerId] = @pCustomerId)    
	AND (@count = 0 OR a.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER  		
	AND (ccc.[StatusId] IN(7, 8) OR [pi].[StatusId] IN(7, 8))
	AND CONVERT(DATETIME, a.[InsertDate]) >= @lInsertDate    
	AND a.[Amount] > 0
	AND CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / (a.[Amount] + ISNULL(a.[AdditionalAmount], 0)), 2) * 100) >= @pPorcent           
	ORDER BY [Id] DESC        
     
    SET NOCOUNT OFF    
END 

