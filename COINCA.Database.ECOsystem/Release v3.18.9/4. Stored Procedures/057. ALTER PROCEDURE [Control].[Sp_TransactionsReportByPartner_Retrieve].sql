/****** Object:  StoredProcedure [Control].[Sp_TransactionsReportByPartner_Retrieve]    Script Date: 02/06/2020 10:44:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Kevin Peña
-- Create date: 11/10/2015
-- Description:	Retrieve TransactionsReport information By Partner, Update to retrieve TerminalId
-- ================================================================================================
-- Modify: Melvin Salas - 1/22/2015 - Dinamic Filter
-- Modify: Cindy Vargas - 19/08/2016 - Add the transaction country name to the retrieve information
-- Modify: Cindy Vargas - 29/08/2016 - Add the transaction ExchangeValue and Amount to the retrieve information
-- Modify: Stefano Quirós - 9/5/2016 - Add column FuelAmount to tmp table TransactionReport 
-- Modify: Henry Retana - 28/9/16 - Change the amount retrieve
-- Modify: Stefano Quirós - 12/06/2016 - Add the Time Zone Parameter
-- Modify: Henry Retana - 05/05/2017 - Add Offline Transactions
-- Modify: Henry Retana - 14/11/2017 - Add Void Transactions Validation
-- Modify: Stefano Quirós - 30/12/2017 - Add IsInternational field to the retrieve
-- Modify: Maria de los Angeles Jimenez Chavarria - Jan/01/2018 - Invoice lenght
-- Modify by Marjorie Garbanzo - 21/05/2019 - Add filter IsDenied to the retrieve
-- Modify by Stefano Quirós - 21/05/2019 - Add condition @pStatus = 1 when  @pIssueForId = 100
-- Modify by Juan K Satamaria v- 14/02/2020 - Eneblaed trasactions by drivers JSA-001
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_TransactionsReportByPartner_Retrieve]
(
	@pPartnerId INT,
	@pStatus INT = NULL,
	@pKey VARCHAR(800) = NULL,
	@pYear INT = NULL,
	@pMonth INT = NULL,
	@pStartDate DATETIME = NULL,
	@pEndDate DATETIME = NULL,	
	@pUserId INT
)
AS
BEGIN
	SET NOCOUNT ON

	IF @pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
	BEGIN
		SET @pYear = NULL
		SET @pMonth = NULL
	END

	-- DYNAMIC FILTER
	--DECLARE	@Results TABLE (items INT)
	--DECLARE @count INT
	--INSERT	@Results
	--EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH', @pPartnerId
	--SET	@count = (SELECT COUNT(*) FROM	@Results)
	-- END

	DECLARE @lTimeZoneParameter INT 

	SELECT @lTimeZoneParameter = [TimeZone]	FROM [General].[Countries] c 
								 INNER JOIN [General].[Partners] p
								 ON c.[CountryId] = p.[CountryId] 
								 WHERE p.[PartnerId] = @pPartnerId

	IF OBJECT_ID('tempdb..#tmp_CustomersByPartner') IS NOT NULL
		DROP TABLE #tmp_CustomersByPartner

	IF OBJECT_ID('tempdb..#tmp_TransactionsReports') IS NOT NULL
		DROP TABLE #tmp_TransactionsReports

		IF OBJECT_ID('tempdb..#tmp_transactions_driver') IS NOT NULL
		DROP TABLE #tmp_TransactionsReports

	CREATE TABLE #tmp_CustomersByPartner (
		RowId INT identity(1, 1) NOT NULL,
		CustomerId INT
		)

	CREATE TABLE #tmp_TransactionsReports (
		 [CustomerName] VARCHAR(250)
		,[CreditCardId] INT
		,[CreditCardNumber] NVARCHAR(520)
		,[PaymentInstrumentCode] NVARCHAR(520)
		,[PaymentInstrumentType] VARCHAR(100)
		,[Invoice] VARCHAR(20) NULL
		,[MerchantDescription] VARCHAR(75) NULL
		,[SystemTraceCode] VARCHAR(250) NULL
		,[HolderName] VARCHAR(250) NULL
		,[Date] DATETIME
		,[FuelName] VARCHAR(50)
		,[FuelAmount] DECIMAL(16, 2)
		,[Odometer] INT
		,[Liters] DECIMAL(16, 2)
		,[PlateId] VARCHAR(10)
		,[CurrencySymbol] NVARCHAR(50) NULL
		,[State] VARCHAR(250) NULL
		,[CostCenterName] VARCHAR(250) NULL
		,[TerminalId] VARCHAR(20) NULL
		,[VehicleName] VARCHAR(50) NULL
		,[CountryName] VARCHAR(250) NULL
		,[ExchangeValue] NVARCHAR(50)
		,[RealAmount] DECIMAL(16, 2)
		,[IsInternational] BIT NULL
		,[TransactionId] INT     
		,[CustomerId] INT        
		)

	CREATE TABLE #tmp_transactions_driver(
		 [TransactionId] INT,
		 [Invoice] VARCHAR(20),
		 [MerchantDescription]  VARCHAR(75),
		 [SystemTraceCode] VARCHAR(250),
		 [CreditCardNumber] NVARCHAR(520),
		 [PaymentInstrumentCode] NVARCHAR(520),
		 [PaymentInstrumentType] VARCHAR(100),
		 [HolderName] VARCHAR(250),
		 [Date] DATETIME,
		 [FuelName] VARCHAR(50),
		 [FuelAmount] DECIMAL(16,2),
		 [Odometer] INT,
		 [Liters] DECIMAL(16,2),
		 [PlateId] VARCHAR(10),
		 [CurrencySymbol] NVARCHAR(50),
		 [State] VARCHAR(250),
		 [CostCenterName] VARCHAR(250),
		 [TerminalId] VARCHAR(20) NULL,
		 [VehicleName] VARCHAR(50) NULL,
		 [CountryName] VARCHAR(250) NULL,
		 [ExchangeValue] NVARCHAR(50),
		 [RealAmount] DECIMAL(16, 2),
		 [IsInternational] BIT NULL,
		 [CreditCardId] INT,              
		 [CustomerName] VARCHAR(250),	  
		 [CustomerId] INT				  
		)

	INSERT INTO #tmp_CustomersByPartner
	SELECT [CustomerId]
	FROM [General].[CustomersByPartner]
	WHERE PartnerId = @pPartnerId
	--AND CustomerId IN (6666) -- JSA FOR TESTING PURPOSES

	DECLARE @TempID INT
	DECLARE @pCustomerId INT

	WHILE EXISTS (SELECT *FROM #tmp_CustomersByPartner)
	BEGIN
		SELECT TOP 1 @pCustomerId = [CustomerId],@TempID = [RowId]
		FROM #tmp_CustomersByPartner

		DELETE FROM #tmp_CustomersByPartner WHERE RowId = @TempID

		DECLARE @pIssueForId INT

		SET @pIssueForId = (
				SELECT [IssueForId]
				FROM [General].[Customers]
				WHERE [CustomerId] = @pCustomerId
				)

		IF @pIssueForId = 100
		BEGIN
			IF (@pStatus = 1)
			BEGIN
				--DRIVER
				INSERT INTO #tmp_transactions_driver
				SELECT t.[TransactionId] [TransactionId]
					,ISNULL(t.[Invoice], '-') AS [Invoice]
					,CASE 
						WHEN [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0) IS NULL THEN ''
						ELSE [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0)
						END AS [MerchantDescription]
					,t.[TransactionPOS] AS [SystemTraceCode]
					,cr.[CreditCardNumber] [CreditCardNumber]
					,cr.[CreditCardNumber] [PaymentInstrumentCode]
					,'Tarjeta' [PaymentInstrumentType]
					,u.[Name] [HolderName]
					,t.[Date] [Date]
					,f.[Name] [FuelName]
					,t.[FuelAmount] [FuelAmount]
					,t.[Odometer] [Odometer]
					,t.[Liters] [Liters]
					,v.[PlateId] [PlateId]
					,g.[Symbol] [CurrencySymbol]
					,(
						CASE 
							WHEN t.[IsReversed] = 1 AND t.[IsVoid] = 1
								THEN 'Reversada'
							WHEN t.[IsDuplicated] = 1
								THEN 'Duplicada'
							WHEN t.[IsFloating] = 1
								THEN 'Flotante'
							WHEN t.[IsReversed] = 0 AND t.[IsVoid] = 1 
								THEN 'Anulada'
							ELSE 'Procesada'
							END
						) AS [State]
					,e.[Name] AS [CostCenterName]
					,t.[ProcessorId] AS [TerminalId]
					,v.[Name] AS [VehicleName]
					,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
							    FROM [General].[Countries] coun
								WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName]
					,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
						   WHEN t.[ExchangeValue] IS NOT NULL 
						   THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
									  (SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
										FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cue 
											ON er.[CurrencyCode] = cue.[CurrencyCode]
										INNER JOIN [General].[Countries] coun 
											ON coun.[CountryId] = c.[CountryId]
										WHERE cue.[Code] = coun.[Code]
											AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
												OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
					   END) AS [ExchangeValue]
					, (CASE 
						WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL THEN 
						[Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
						WHEN t.[IsInternational] = 0 THEN
						t.FuelAmount
						ELSE t.FuelAmount END) AS [RealAmount]
					,ccc.[InternationalCard] [IsInternational]
					,t.[CreditCardId] 
					,c.[Name] AS CustomerName
					,c.[CustomerId]
				FROM [Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v 
					ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c 
					ON v.[CustomerId] = c.[CustomerId]
				INNER JOIN [General].[CustomerCreditCards] ccc 
					ON ccc.[CustomerId] = c.[CustomerId] AND ccc.[StatusId] = 100
				INNER JOIN [Control].[Fuels] f 
					ON t.[FuelId] = f.[FuelId]
				INNER JOIN [Control].[CreditCardByDriver] cd 
					ON t.[CreditCardId] = cd.[CreditCardId]
				INNER JOIN [Control].[CreditCard] cr 
					ON cd.CreditCardId = cr.CreditCardId
				INNER JOIN [General].[Users] u 
					ON cd.[UserId] = u.[UserId]
				INNER JOIN [Control].[Currencies] g 
					ON c.[CurrencyId] = g.[CurrencyId]
				INNER JOIN [General].[VehicleCostCenters] e 
					ON t.CostCenterId = e.CostCenterId			
				LEFT OUTER JOIN [General].[Countries] co 
					ON t.[CountryCode] = co.[Code]			
				WHERE 
					--(@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
					c.[CustomerId] = @pCustomerId
					AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)		
					AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
					AND 1 > (
						SELECT ISNULL(COUNT(1), 0) FROM Control.Transactions t2
						WHERE t2.[CreditCardId] = t.[CreditCardId] AND
							t2.[TransactionPOS] = t.[TransactionPOS] AND
							t2.[ProcessorId] = t.[ProcessorId] AND 
							(t2.IsReversed = 1 OR t2.IsVoid = 1)
						)
					AND (
						@pKey IS NULL
						OR v.[PlateId] LIKE '%' + @pKey + '%'
						OR t.[ProcessorId] LIKE '%' + @pKey + '%' 
						OR t.[MerchantDescription] LIKE '%' + @pKey + '%' 
						)
					AND (
						(
							@pYear IS NOT NULL
							AND @pMonth IS NOT NULL
							AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
							AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear
							)
						OR (
							@pStartDate IS NOT NULL
							AND @pEndDate IS NOT NULL
							AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate
								AND @pEndDate
							)
						)
				ORDER BY t.[Date] DESC
			END
			ELSE
			BEGIN
				--DRIVER
				INSERT INTO #tmp_transactions_driver
				SELECT t.[TransactionId] [TransactionId]
					,ISNULL(t.[Invoice], '-') AS [Invoice]
					,CASE 
						WHEN [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0) IS NULL THEN ''
						ELSE [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0)
						END AS [MerchantDescription]
					,t.[TransactionPOS] AS [SystemTraceCode]
					,cr.[CreditCardNumber] [CreditCardNumber]
					,cr.[CreditCardNumber] [CreditCardNumber]
					,cr.[CreditCardNumber] [PaymentInstrumentCode]
					,u.[Name] [HolderName]
					,t.[Date] [Date]
					,f.[Name] [FuelName]
					,t.[FuelAmount] [FuelAmount]
					,t.[Odometer] [Odometer]
					,t.[Liters] [Liters]
					,v.[PlateId] [PlateId]
					,g.[Symbol] [CurrencySymbol]
					,(
						CASE 
							WHEN t.[IsReversed] = 1 AND t.[IsVoid] = 1
								THEN 'Reversada'
							WHEN t.[IsDuplicated] = 1
								THEN 'Duplicada'
							WHEN t.[IsFloating] = 1
								THEN 'Flotante'
							WHEN t.[IsReversed] = 0 AND t.[IsVoid] = 1 
								THEN 'Anulada'
							ELSE 'Procesada'
							END
						) AS [State]
					,e.[Name] AS [CostCenterName]
					,t.[ProcessorId] AS [TerminalId]
					,v.[Name] AS [VehicleName]
					,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
							    FROM [General].[Countries] coun
								WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName]
					,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
						   WHEN t.[ExchangeValue] IS NOT NULL 
						   THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
									  (SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
										FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cue 
											ON er.[CurrencyCode] = cue.[CurrencyCode]
										INNER JOIN [General].[Countries] coun 
											ON coun.[CountryId] = c.[CountryId]
										WHERE cue.[Code] = coun.[Code]
											AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
												OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
					   END) AS [ExchangeValue]
					, (CASE 
						WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL THEN 
						[Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
						WHEN t.[IsInternational] = 0 THEN
						t.FuelAmount
						ELSE t.FuelAmount END) AS [RealAmount]
					,ccc.[InternationalCard] [IsInternational]
					,t.[CreditCardId] 
					,c.[Name] AS CustomerName
					,c.[CustomerId]
				FROM [Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v 
					ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c 
					ON v.[CustomerId] = c.[CustomerId]
				INNER JOIN [General].[CustomerCreditCards] ccc 
					ON ccc.[CustomerId] = c.[CustomerId] AND ccc.[StatusId] = 100
				INNER JOIN [Control].[Fuels] f 
					ON t.[FuelId] = f.[FuelId]
				INNER JOIN [Control].[CreditCardByDriver] cd 
					ON t.[CreditCardId] = cd.[CreditCardId]
				INNER JOIN [Control].[CreditCard] cr 
					ON cd.CreditCardId = cr.CreditCardId
				INNER JOIN [General].[Users] u 
					ON cd.[UserId] = u.[UserId]
				INNER JOIN [Control].[Currencies] g 
					ON c.[CurrencyId] = g.[CurrencyId]
				INNER JOIN [General].[VehicleCostCenters] e 
					ON t.CostCenterId = e.CostCenterId			
				LEFT OUTER JOIN [General].[Countries] co 
					ON t.[CountryCode] = co.[Code]			
				WHERE 
					--(@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
					c.[CustomerId] = @pCustomerId
					AND (
						@pStatus IS NULL
						OR (
							ISNULL(t.[IsFloating], 0) = CASE 
								WHEN @pStatus = 2
									THEN 1
								ELSE 0
								END
							AND --Floating
							ISNULL(t.[IsReversed], 0) = CASE 
								WHEN @pStatus = 3
									THEN 1
								ELSE 0
								END
							AND --Reversed
							ISNULL(t.[IsDuplicated], 0) = CASE 
								WHEN @pStatus = 4
									THEN 1
								ELSE 0
								END
							AND --Duplicated
							ISNULL(t.[IsAdjustment], 0) = CASE 
								WHEN @pStatus = 5
									THEN 1
								ELSE 0
								END
							AND 
							ISNULL(t.[IsVoid], 0) = CASE 
								WHEN @pStatus = 9 OR @pStatus = 3 
									THEN 1 ELSE 0 
								END 
							AND--Offline
							ISNULL(t.[TransactionOffline], 0) = CASE 
								WHEN @pStatus = 7
									THEN 1
								ELSE 0
								END
							)
						) --Adjustment
					AND (
						@pKey IS NULL
						OR u.[Name] LIKE '%' + @pKey + '%'
						OR v.[PlateId] LIKE '%' + @pKey + '%'
						OR t.[ProcessorId] LIKE '%' + @pKey + '%' 
						OR t.[MerchantDescription] LIKE '%' + @pKey + '%' 
						)
					AND (
						(
							@pYear IS NOT NULL
							AND @pMonth IS NOT NULL
							AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
							AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear
							)
						OR (
							@pStartDate IS NOT NULL
							AND @pEndDate IS NOT NULL
							AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate
								AND @pEndDate
							)
						)
				ORDER BY t.[Date] DESC
			END
		END
		ELSE
		BEGIN
			--VEHICLE
			IF (@pStatus = 1)
			BEGIN
				INSERT INTO #tmp_TransactionsReports
				SELECT c.[Name] [CustomerName]
					,t.[CreditCardId] [CreditCardId]
					,cc.[CreditCardNumber] AS [CreditCardNumber]
					,ISNULL(cc.[CreditCardNumber], pay.[Code]) [PaymentInstrumentCode]
					,(CASE WHEN t.[PaymentInstrumentId] IS NULL
					  THEN 'Tarjeta'
					  ELSE pit.[Name] END) AS [PaymentInstrumentType]
					,ISNULL(t.[Invoice], '-') AS [Invoice]
					,CASE 
					WHEN [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0) IS NULL THEN ''
					ELSE [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0)
					END AS [MerchantDescription]
					,t.[TransactionPOS] AS [SystemTraceCode]
					,v.[PlateId] [HolderName]
					,t.[Date] [Date]
					,f.[Name] [FuelName]
					,t.[FuelAmount] [FuelAmount]
					,t.[Odometer] [Odometer]
					,t.[Liters] [Liters]
					,v.[PlateId] [PlateId]
					,g.[Symbol] [CurrencySymbol]
					,(
						CASE 
							WHEN t.[IsReversed] = 1 AND t.[IsVoid] = 1
								THEN 'Reversada'
							WHEN t.[IsDuplicated] = 1
								THEN 'Duplicada'
							WHEN t.[IsFloating] = 1
								THEN 'Flotante'
							WHEN t.[IsReversed] = 0 AND t.[IsVoid] = 1 
								THEN 'Anulada'
							ELSE 'Procesada'
							END
						) AS [State]
					,e.[Name] AS [CostCenterName]
					,t.[ProcessorId] AS [TerminalId]
					,v.[Name] AS [VehicleName]
					,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
						    FROM [General].[Countries] coun
							WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName]
					,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
						   WHEN t.[ExchangeValue] IS NOT NULL 
						   THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
									  (SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
										FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cue 
											ON er.[CurrencyCode] = cue.[CurrencyCode]
										INNER JOIN [General].[Countries] coun 
											ON coun.[CountryId] = c.[CountryId]
										WHERE cue.[Code] = coun.[Code]
											AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
												OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
					   END) AS [ExchangeValue]
					,(CASE 
						WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL THEN 
						[Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
						WHEN t.[IsInternational] = 0 THEN
						t.FuelAmount
						ELSE t.FuelAmount END) AS [RealAmount]
					,ccc.[InternationalCard] [IsInternational]
					,t.[TransactionId]
					,c.[CustomerId]
				FROM [Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v 
					ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c 
					ON v.[CustomerId] = c.[CustomerId]
				INNER JOIN [General].[CustomerCreditCards] ccc 
					ON ccc.[CustomerId] = c.[CustomerId] AND ccc.[StatusId] = 100
				INNER JOIN [Control].[Fuels] f 
					ON t.[FuelId] = f.[FuelId]
				INNER JOIN [Control].[CreditCardByVehicle] cv 
					ON t.[CreditCardId] = cv.[CreditCardId]
				INNER JOIN [Control].[Currencies] g 
					ON c.[CurrencyId] = g.[CurrencyId]
				LEFT JOIN [Control].[CreditCard] cc 
					ON cc.CreditCardId = cv.CreditCardId
				LEFT JOIN [Control].[PaymentInstruments] pay
					ON t.[PaymentInstrumentId] = pay.[Id]
				LEFT JOIN [Control].[PaymentInstrumentsTypes] pit
					ON pay.[typeId] = pit.[Id]
				INNER JOIN [General].[VehicleCostCenters] e 
					ON t.CostCenterId = e.CostCenterId				
				LEFT OUTER JOIN [General].[Countries] co 
					ON t.[CountryCode] = co.[Code]
				WHERE 
				--(@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
					c.[CustomerId] = @pCustomerId
					AND (t.[IsDenied] = 0 OR t.[IsDenied] IS NULL)		
					AND (t.[IsFloating] IS NULL OR t.[IsFloating] = 0) 
					AND 1 > (
						SELECT ISNULL(COUNT(1), 0) FROM Control.Transactions t2
						WHERE (t2.[CreditCardId] = t.[CreditCardId] 
							   OR t2.[PaymentInstrumentId] = t.[PaymentInstrumentId])
							AND
							t2.[TransactionPOS] = t.[TransactionPOS] AND
							t2.[ProcessorId] = t.[ProcessorId] AND 
							(t2.IsReversed = 1 OR t2.IsVoid = 1)
						)
					AND (
						@pKey IS NULL
						OR v.[PlateId] LIKE '%' + @pKey + '%'
						OR t.[ProcessorId] LIKE '%' + @pKey + '%' 
						OR t.[MerchantDescription] LIKE '%' + @pKey + '%' 
						)
					AND (
						(
							@pYear IS NOT NULL
							AND @pMonth IS NOT NULL
							AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
							AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear
							)
						OR (
							@pStartDate IS NOT NULL
							AND @pEndDate IS NOT NULL
							AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate
								AND @pEndDate
							)
						)
				ORDER BY DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) DESC
			END
			ELSE
			BEGIN
				--SELECT 'ENTRE 3'
				INSERT INTO #tmp_TransactionsReports
				SELECT c.[Name] [CustomerName]
					,t.[CreditCardId] [CreditCardId]
					,cc.[CreditCardNumber] [CreditCardNumber]
					,ISNULL(cc.[CreditCardNumber], pay.[Code]) [PaymentInstrumentCode]
					,(CASE WHEN t.[PaymentInstrumentId] IS NULL
					  THEN 'Tarjeta'
					  ELSE pit.[Name] END) AS [PaymentInstrumentType]
					,ISNULL(t.[Invoice], '-') AS [Invoice]
					,CASE 
					WHEN [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0) IS NULL THEN ''
					ELSE [General].[GetServiceStationInfo](t.[ProcessorId], c.[CustomerId], 0)
					END AS [MerchantDescription]
					,t.[TransactionPOS] AS [SystemTraceCode]
					,v.[PlateId] [HolderName]
					,t.[Date] [Date]
					,f.[Name] [FuelName]
					,t.[FuelAmount] [FuelAmount]
					,t.[Odometer] [Odometer]
					,t.[Liters] [Liters]					
					,v.[PlateId] [PlateId]
					,g.[Symbol] [CurrencySymbol]
					,(
						CASE 
							WHEN t.[IsReversed] = 1 AND t.[IsVoid] = 1
								THEN 'Reversada'
							WHEN t.[IsDuplicated] = 1
								THEN 'Duplicada'
							WHEN t.[IsFloating] = 1
								THEN 'Flotante'
							WHEN t.[IsReversed] = 0 AND t.[IsVoid] = 1 
								THEN 'Anulada'
							ELSE 'Procesada'
						END
						) AS [State]
					,e.[Name] AS [CostCenterName]
					,t.[ProcessorId] AS [TerminalId]
					,v.[Name] AS [VehicleName]
					,ISNULL(co.[Name], (SELECT TOP 1 coun.[Name] 
						    FROM [General].[Countries] coun
							WHERE coun.[CountryId] = c.[CountryId] )) AS [CountryName]
					,(CASE WHEN t.[ExchangeValue] IS NULL THEN 'N/A'
						   WHEN t.[ExchangeValue] IS NOT NULL 
						   THEN CONCAT(t.[CountryCode], ':', t.[ExchangeValue], '; ', 
									  (SELECT  CONCAT(coun.[Code] , ':' , er.[Value])
										FROM [Control].[ExchangeRates] er
										INNER JOIN [Control].[Currencies] cue 
											ON er.[CurrencyCode] = cue.[CurrencyCode]
										INNER JOIN [General].[Countries] coun 
											ON coun.[CountryId] = c.[CountryId]
										WHERE cue.[Code] = coun.[Code]
											AND (( er.[Until] IS NOT NULL AND t.[InsertDate] BETWEEN er.[Since] AND er.[Until])
												OR (er.[Until] IS NULL AND (t.[InsertDate] BETWEEN er.[Since] AND GETDATE())))))
					   END) AS [ExchangeValue]
					, (CASE 
						WHEN t.[IsInternational] = 1 AND (t.[CountryCode] IS NOT NULL AND t.[CountryCode] <> '') AND t.[ExchangeValue] IS NOT NULL THEN 
						[Control].[GetTransactionRealAmount] (t.CreditCardId, t.[CountryCode], t.[FuelAmount], t.[ExchangeValue], t.[Date])
						WHEN t.[IsInternational] = 0 THEN
						t.FuelAmount
						ELSE t.FuelAmount END) AS [RealAmount]
					,ccc.[InternationalCard] [IsInternational]
					,t.[TransactionId]
					,c.[CustomerId]
				FROM [Control].[Transactions] t
				INNER JOIN [General].[Vehicles] v 
					ON t.[VehicleId] = v.[VehicleId]
				INNER JOIN [General].[Customers] c 
					ON v.[CustomerId] = c.[CustomerId]
				INNER JOIN [General].[CustomerCreditCards] ccc 
					ON ccc.[CustomerId] = c.[CustomerId] AND ccc.[StatusId] = 100
				INNER JOIN [Control].[Fuels] f 
					ON t.[FuelId] = f.[FuelId]
				INNER JOIN [Control].[CreditCardByVehicle] cv 
					ON t.[CreditCardId] = cv.[CreditCardId]
				INNER JOIN [Control].[Currencies] g 
					ON c.[CurrencyId] = g.[CurrencyId]
				LEFT JOIN [Control].[CreditCard] cc 
					ON cc.CreditCardId = t.CreditCardId
				LEFT JOIN [Control].[PaymentInstruments] pay
					ON t.[PaymentInstrumentId] = pay.[Id]
				LEFT JOIN [Control].[PaymentInstrumentsTypes] pit
					ON pay.[typeId] = pit.[Id]
				INNER JOIN [General].[VehicleCostCenters] e 
					ON t.CostCenterId = e.CostCenterId
				LEFT OUTER JOIN [General].[Countries] co 
					ON t.[CountryCode] = co.[Code]
				WHERE 
				--(@count = 0 OR v.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
				c.[CustomerId] = @pCustomerId
					AND (
						@pStatus IS NULL
						OR @pStatus = 0
						OR (
							ISNULL(t.[IsFloating], 0) = CASE 
								WHEN @pStatus = 2
									THEN 1
								ELSE 0
								END
							AND --Floating
							ISNULL(t.[IsReversed], 0) = CASE 
								WHEN @pStatus = 3
									THEN 1
								ELSE 0
								END
							AND --Reversed
							ISNULL(t.[IsDuplicated], 0) = CASE 
								WHEN @pStatus = 4
									THEN 1
								ELSE 0
								END
							AND --Duplicated
							ISNULL(t.[IsAdjustment], 0) = CASE 
								WHEN @pStatus = 5
									THEN 1
								ELSE 0
								END
							AND 
							ISNULL(t.[IsVoid], 0) = CASE 
								WHEN @pStatus = 9 OR @pStatus = 3 
									THEN 1 
								ELSE 0 
								END 
							AND --Offline
							ISNULL(t.[TransactionOffline], 0) = CASE 
								WHEN @pStatus = 7
									THEN 1
								ELSE 0
								END
							)
						)
					AND (
						@pKey IS NULL
						OR v.[PlateId] LIKE '%' + @pKey + '%'
						OR t.[ProcessorId] LIKE '%' + @pKey + '%' 
						OR t.[MerchantDescription] LIKE '%' + @pKey + '%' 

						)
					AND (
						(
							@pYear IS NOT NULL
							AND @pMonth IS NOT NULL
							AND DATEPART(m, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pMonth
							AND DATEPART(yyyy, DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate])) = @pYear
							)
						OR (
							@pStartDate IS NOT NULL
							AND @pEndDate IS NOT NULL
							AND DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) BETWEEN @pStartDate
								AND @pEndDate
							)
						)
				ORDER BY DATEADD(HOUR, @lTimeZoneParameter, t.[InsertDate]) DESC

			END
			
		END --fin del while customer exist
		
	END

	SET NOCOUNT OFF

		SELECT   
			 [CostCenterName]
			,[CountryName]
			,[CreditCardId]
			,[CreditCardNumber]
			,[PaymentInstrumentCode]
			,[PaymentInstrumentType]
			,[CurrencySymbol]
			,[CustomerId]
			,[CustomerName]
			,[Date]
			,[ExchangeValue]
			,[FuelAmount]
			,[FuelName]
			,[HolderName]
			,[Invoice]
			,[IsInternational]
			,[Liters]
			,[MerchantDescription]
			,[Odometer]
			,[PlateId]
			,[RealAmount]
			,[State]
			,[SystemTraceCode]
			,[TerminalId]
			,[TransactionId]
			,[VehicleName]
			,'V' as Flag
	 FROM #tmp_TransactionsReports
	 ------------------------------
	 UNION ALL  /*union with driver*/
	 ------------------------------
		SELECT   
			 [CostCenterName]
			,[CountryName]
			,[CreditCardId]
			,[CreditCardNumber]
			,[PaymentInstrumentCode]
			,[PaymentInstrumentType]
			,[CurrencySymbol]
			,[CustomerId]
			,[CustomerName]
			,[Date]
			,[ExchangeValue]
			,[FuelAmount]
			,[FuelName]
			,[HolderName]
			,[Invoice]
			,[IsInternational]
			,[Liters]
			,[MerchantDescription]
			,[Odometer]
			,[PlateId]
			,[RealAmount]
			,[State]
			,[SystemTraceCode]
			,[TerminalId]
			,[TransactionId]
			,[VehicleName]
			,'D' as Flag
	FROM #tmp_transactions_driver 

	DROP TABLE #tmp_TransactionsReports
	DROP TABLE #tmp_CustomersByPartner
	DROP TABLE #tmp_transactions_driver
END


