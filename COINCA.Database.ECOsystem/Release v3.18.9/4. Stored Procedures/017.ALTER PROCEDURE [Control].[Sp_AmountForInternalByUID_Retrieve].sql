/****** Object:  StoredProcedure [Control].[Sp_AmountForInternalByUID_Retrieve]    Script Date: 5/29/2020 2:43:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 07/31/2019
-- Description:	Get the amount for internal transactions Using the UID to Retrieve the data
-- ================================================================================================
ALTER PROCEDURE [Control].[Sp_AmountForInternalByUID_Retrieve]
(	     
	  @pUID VARCHAR(200)
	 ,@pAmount DECIMAL(10,2) = NULL 
	 ,@pPlateId NVARCHAR(100)
	 ,@pProcessorId NVARCHAR(100)
	 ,@pFuelUnits DECIMAL(10,3)
)
AS
BEGIN

	SET NOCOUNT ON	

	DECLARE @lFuelPrice DECIMAL(10,2)
	DECLARE @lIsInternal BIT
	DECLARE @lCustomerId INT
	DECLARE @lVehicleId INT
	DECLARE @lServiceStationId INT 
	DECLARE @lEquival DECIMAL(18,8) = 3.78541178 -- Ltr to Gal

	--Get the customer Id by the fleet card
	SELECT @lCustomerId = [CustomerId]
	FROM [Control].[PaymentInstruments]
	WHERE [Code] = @pUID

	SELECT @lEquival = CASE WHEN c.[UnitOfCapacityId] = 1 
					   THEN @lEquival 
					   ELSE 1 
					   END
	FROM [General].[Customers] c	
    WHERE c.[CustomerId] = @lCustomerId

	SET @lIsInternal = CASE WHEN EXISTS (SELECT * 
										 FROM [General].[Terminal] 
										 WHERE [TerminalId] = @pProcessorId 
										 AND [CustomerId] = @lCustomerId) 
							THEN 1
							ELSE 0
						END

	IF @lIsInternal = 1
	BEGIN
	    --Get the vehicle Id from the credit card by vehicle
        SELECT @lVehicleId = pit.[VehicleId]
		FROM [Control].[PaymentInstruments] [pi]		
		INNER JOIN [Control].[PaymentInstrumentsByType] pit
			ON [pi].[Id] = pit.[PaymentInstrumentId]
		WHERE [pi].[Code] = @pUID		

		--Get the fuel price for the service station 
		SELECT @lServiceStationId = [ServiceStationId]
		FROM [General].[TerminalsByServiceStations]
		WHERE [TerminalId] = @pProcessorId

		SELECT TOP 1 @lFuelPrice = s.[Price]
		FROM [Stations].[FuelPriceByServiceStations] s
		INNER JOIN [General].[VehicleCategories] vc
			ON s.[FuelId] = vc.[DefaultFuelId]
		INNER JOIN [General].[Vehicles] v
			ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
		WHERE s.[ServiceStationId] = @lServiceStationId
		AND v.[VehicleId] = @lVehicleId
		AND s.[EndDate] IS NULL
		AND s.[ScheduledId] IS NULL 
		AND s.[History] IS NULL 

		IF @lFuelPrice IS NULL 
		BEGIN 
			--Get the service station for the partner
			SELECT @lFuelPrice = pf.[LiterPrice]		
			FROM [General].[Vehicles] v			
			INNER JOIN [General].[VehicleCategories] vc
				ON v.[VehicleCategoryId] = vc.[VehicleCategoryId]
			INNER JOIN [General].[CustomersByPartner] cp
				ON v.[CustomerId] = cp.[CustomerId]
			INNER JOIN [Control].[PartnerFuel] pf
				ON vc.[DefaultFuelId] = pf.[FuelId]
					AND pf.[EndDate] > GETDATE()
					AND pf.[PartnerId] = cp.[PartnerId]
					AND pf.[ScheduledId] IS NULL
			WHERE v.[VehicleId] = @lVehicleId		
		END

		--Get the fuel price by the amount of units
		SET @lFuelPrice = @lFuelPrice * @pFuelUnits * @lEquival

		--If the result is null, returns 0
		IF @lFuelPrice IS NULL SET @lFuelPrice = 0

		--Return the fuel price
		SELECT @lFuelPrice
	END
	ELSE 
	BEGIN 
		SELECT @pAmount
	END

	SET NOCOUNT OFF
END