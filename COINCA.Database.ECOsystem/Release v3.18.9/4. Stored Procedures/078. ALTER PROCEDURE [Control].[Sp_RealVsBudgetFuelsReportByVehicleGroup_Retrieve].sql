USE [ECOsystem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_RealVsBudgetFuelsReportByVehicleGroup_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportByVehicleGroup_Retrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Berman Romero L.
-- Create date: 10/21/2014
-- Description:	Retrieve Current Fuels Report By Vehicle Group information
-- Dinamic Filter - 1/22/2015 - Melvin Salas
-- Modify by Henry Retana - 15/11/2017
-- Add Void Transactions Validation
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- Modify by Marjorie Garbanzo - 01/06/2020 Add logic to include transactions with different payment methods
-- ================================================================================================
CREATE PROCEDURE [Control].[Sp_RealVsBudgetFuelsReportByVehicleGroup_Retrieve]
(
	 @pCustomerId INT
	,@pYear INT = NULL
	,@pMonth INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL
	,@pUserId INT
)
AS
BEGIN
	
	SET NOCOUNT ON	
	
	DECLARE @lCurrencySymbol NVARCHAR(4) = ''
	-- DYNAMIC FILTER
	DECLARE	@Results TABLE (items INT)
	DECLARE @count INT
	INSERT	@Results
	EXEC	dbo.Sp_UserDynamicFilter_Retrive @pUserId, 'VH'
	SET		@count = (SELECT COUNT(*) FROM	@Results)
	-- END
	
	SELECT @lCurrencySymbol = a.Symbol
	FROM [Control].[Currencies] a
	INNER JOIN [General].[Customers] b
		ON a.[CurrencyId] = b.[CurrencyId]
	WHERE b.[CustomerId] = @pCustomerId
	
	SELECT   a.[Name] AS [VehicleGroupName]
			,ISNULL(t.[RealAmount],0) AS [RealAmount]
			,s.[AssignedAmount]
			,CONVERT(DECIMAL(16,2),ROUND(ISNULL(t.[RealAmount],0) / NULLIF(s.[AssignedAmount],0), 2)*100) AS [RealAmountPct]
			,@lCurrencySymbol AS [CurrencySymbol]
	FROM [General].[VehicleGroups] a
	LEFT JOIN (SELECT z.[VehicleGroupId]
					 ,SUM(x.[FuelAmount]) AS [RealAmount]
				FROM [Control].[Transactions] x
				INNER JOIN [General].[Vehicles] y
					ON x.[VehicleId] = y.[VehicleId]
				INNER JOIN [General].[VehiclesByGroup] z
					ON z.VehicleId = y.VehicleId
				WHERE y.[CustomerId] = @pCustomerId
				AND (@count = 0 OR x.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
				AND x.[IsReversed] = 0
				AND x.[IsDuplicated] = 0
				AND (x.[IsFloating] IS NULL OR x.[IsFloating] = 0) 
				AND (x.[IsDenied] IS NULL OR x.[IsDenied] = 0)	
				AND 1 > (SELECT ISNULL(COUNT(1), 0) 
						 FROM [Control].[Transactions] t2
						 WHERE (t2.[CreditCardId] = x.[CreditCardId] 
								OR t2.[PaymentInstrumentId] = x.[PaymentInstrumentId])
						 AND t2.[TransactionPOS] = x.[TransactionPOS] 
						 AND t2.[ProcessorId] = x.[ProcessorId] 
						 AND (t2.[IsReversed] = 1 OR t2.[IsVoid] = 1))
				AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,x.[Date]) = @pMonth
						AND DATEPART(yyyy,x.[Date]) = @pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND x.[Date] BETWEEN @pStartDate AND @pEndDate))								
				GROUP BY z.[VehicleGroupId]) t
			ON a.[VehicleGroupId] = t.[VehicleGroupId]
		INNER JOIN (SELECT z.[VehicleGroupId]
						  ,SUM(x.[Amount]) AS [AssignedAmount]
					FROM [Control].[FuelDistribution] x
					INNER JOIN [General].[Vehicles] y
						ON x.[VehicleId] = y.[VehicleId]
					INNER JOIN [General].[VehiclesByGroup] z
						ON z.[VehicleId] = y.[VehicleId]
					WHERE y.[CustomerId] = @pCustomerId						
					AND (@count = 0 OR x.[VehicleId] IN (SELECT items FROM @Results)) -- DYNAMIC FILTER
					AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
							AND x.[Month] = @pMonth
							AND x.[Year] = @pYear) OR
						(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
							AND x.[Month] BETWEEN DATEPART(m,@pStartDate) AND DATEPART(m,@pEndDate)
							AND x.[Year] BETWEEN DATEPART(yyyy,@pStartDate) AND DATEPART(yyyy,@pEndDate)))
					GROUP BY z.[VehicleGroupId])s
			ON a.[VehicleGroupId] = s.[VehicleGroupId]
	ORDER BY 4 desc	
	
	SET NOCOUNT OFF
END

