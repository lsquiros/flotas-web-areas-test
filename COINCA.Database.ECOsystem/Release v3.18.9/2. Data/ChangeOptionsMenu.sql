USE [ECOsystem]
GO
-- ===========================================================================================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 20/05/2020
-- Description:	Change the texts of the options menu
-- ===========================================================================================================

UPDATE [dbo].[aspmenus]
SET Name = 'Medios de Pago'
WHERE Header = 8 AND Name = 'Tarjetas de Cr�dito' 

UPDATE [dbo].[aspmenus]
SET Name = 'Solicitud de Medios de Pago'
WHERE Header = 8 AND Name = 'Solicitud de Tarjetas' 

UPDATE [dbo].[aspmenus]
SET Name = 'Mantenimiento de Medios de Pago'
WHERE Header = 8 AND Name = 'Mantenimiento de Tarjetas' 

