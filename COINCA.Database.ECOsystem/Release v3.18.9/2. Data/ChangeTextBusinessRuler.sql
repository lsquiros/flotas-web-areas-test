USE [ECOsystem]
GO
-- ===========================================================================================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 20/05/2020
-- Description:	Change the texts of the business rules
-- ===========================================================================================================

UPDATE [Control].[TransactionsRules]
SET RuleDescription = 'Validar que el medio de pago est� registrada en el sistema.'
WHERE RuleName = 'InvalidCreditCard'

UPDATE [Control].[TransactionsRules]
SET RuleDescription = 'Validar el estado del medio de pago.'
WHERE RuleName = 'StatusCreditCard'

UPDATE [Control].[TransactionsRules]
SET RuleDescription = 'Validar la fecha de expiraci�n del medio de pago.'
WHERE RuleName = 'ExpirationCreditCard'

UPDATE [Control].[TransactionsRules]
SET RuleDescription = 'Validar el cr�dito disponible del medio de pago.'
WHERE RuleName = 'BalanceCreditCard'

UPDATE [Control].[TransactionsRules]
SET RuleDescription = 'Validar el medio de pago vinculada al c�digo de un conductor.'
WHERE RuleName = 'DriverCodeCreditCard'

UPDATE [Control].[TransactionsRules]
SET RuleDescription = 'Validar el medio de pago vinculada a la placa de un veh�culo.'
WHERE RuleName = 'PlateCreditCard'

DELETE FROM [Control].[TransactionsRules]
WHERE RuleName = 'UIDVehicle'
