USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Stefano Quir�s
-- Create date: 07/05/2020
-- Description:	Add Stickers Insertion
-- ================================================================================================

DECLARE @NewPermID NVARCHAR(128) = NEWID()
	   ,@lParentId INT

INSERT INTO [dbo].[AspNetPermissions]
VALUES
(
	@NewPermID
	,'View_Stickers'
	,'Stickers'
	,'Index'
	,''
	,GETDATE()
)

INSERT INTO [dbo].[AspMenus]
VALUES
(
	'Inserci�n de Stickers',
	'~/Content/Images/menu_credit_cards.png',
	@NewPermID,
	238,
	9,
	93,
	GETDATE()
)

DECLARE @NewMenuID INT = SCOPE_IDENTITY()

--INSERT PERMISSIONS TO THE MENU AND ROLE
INSERT INTO [dbo].[AspMenusByRoles]([RoleId], [MenuId]) VALUES('4b356447-c286-45d6-bace-d1cc1ae298db', @NewMenuID) --Super Usuario Coinca

INSERT INTO [dbo].[AspNetRolePermissions]([RoleId], [PermissionId]) VALUES('4b356447-c286-45d6-bace-d1cc1ae298db', @NewPermID) --Super Usuario Coinca

--Select * from aspmenus where header = 9 order by [order] 

--Update aspmenus set [Order] = 93, parent = 238--Ico = '~/Content/Images/menu_credit_cards.png' 
--where Id = 238