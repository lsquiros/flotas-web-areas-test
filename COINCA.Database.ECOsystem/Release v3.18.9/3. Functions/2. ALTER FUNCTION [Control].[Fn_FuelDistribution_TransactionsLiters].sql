--USE [ECOsystem]
GO
IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Fn_FuelDistribution_TransactionsLiters]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [Control].[Fn_FuelDistribution_TransactionsLiters]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================
-- Author:		Stefano Quirós
-- Create date: 13-03-2018
-- Description:	Get Real Liters of the transactions
-- Modify by Esteban Solís - 03-05-2018 - Added logic to get
--			liters and real liters values
-- Stefano Quirós - Add filter IsDenied to the retrieve - 21/05/2019
-- Modify by Marjorie Garbanzo - 01/06/2020 Add logic to include transactions with different payment methods
-- =================================================

CREATE FUNCTION [Control].[Fn_FuelDistribution_TransactionsLiters]
(
	 @lPaymentIntrumentId INT = NULL
	,@pCreditCardId INT = NULL
	,@pTimeZoneParameter INT = NULL
	,@pInsertDate DATE = NULL	
	,@pFinalDate DATE = NULL
	,@pUseLitersEntered BIT = 0
	,@pFuelId INT = NULL
)
RETURNS DECIMAL(16, 6)
AS
BEGIN

	DECLARE @RealLiters DECIMAL(16, 6) = 0	
	
	SET @RealLiters = ISNULL((
				SELECT case when @pUseLitersEntered = 0 then SUM(x.[RealLiters]) else sum(x.[Liters]) end 
				FROM [Control].[Transactions] x
				WHERE ((@pCreditCardId IS NOT NULL 
					    AND x.[CreditCardId] = @pCreditCardId)
					  OR [PaymentInstrumentId] = @lPaymentIntrumentId)
					AND x.[IsFloating] = 0
					AND x.[IsReversed] = 0
					AND x.[IsDuplicated] = 0
					AND (x.[IsDenied] IS NULL OR x.[IsDenied] = 0)	
					AND 1 > (
						SELECT COUNT(1)
						FROM [Control].[Transactions] t2
						WHERE (t2.[CreditCardId] = x.[CreditCardId]
								OR t2.[PaymentInstrumentId] = x.[PaymentInstrumentId])
							AND t2.[TransactionPOS] = x.[TransactionPOS]
							AND t2.[ProcessorId] = x.[ProcessorId]
							AND (
								t2.IsReversed = 1
								OR t2.IsVoid = 1
								)
						)
					--NUEVO FILTRO DONDE LA FECHA DE INSERCION SEA IGUAL A LA FECHA DEL ULTIMO CIERRE      
					AND ((@pFinalDate IS NOT NULL
							AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) >= @pInsertDate
							AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) <= @pFinalDate)
						  OR (@pFinalDate IS NULL AND DATEADD(HOUR, @pTimeZoneParameter, x.[InsertDate]) >= @pInsertDate))
					AND (@pFuelId IS NULL
						 OR x.[FuelId] = @pFuelId)
				), 0.0)

	RETURN @RealLiters;

END