/****** Object:  UserDefinedFunction [General].[Fn_HasRequestOrCreditCardActive]    Script Date: 7/14/2020 4:12:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================  
-- Author:      Esteban Solis  
-- Create Date: 2018-03-06  
-- Description: Determina si un vehiculo/conductor tiene tarjeta/solicitudes de tarjetas activas
-- ========================================================================================== 
-- Modify by Maria de los Angeles Jimenez Chavarria - 18/Dic/2018 - Hide deleted items.
-- Modify By Stefano Quirós - 12/04/2019 - Add the filter "Cancelada" to the request by driver
-- Modify by Stefano Quirós - Change the filter from NOT IN to IN on the Status - 18/10/19
-- ========================================================================================== 
ALTER FUNCTION [General].[Fn_HasRequestOrCreditCardActive]  --(43, 'H01', NULL)
(
	 @pCustomerId INT
	,@pVehicleId INT = NULL
	,@pDriverId INT = NULL
	)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result BIT

	-- Add the T-SQL statements to compute the return value here
	IF @pVehicleId IS NOT NULL
	BEGIN
		IF EXISTS (
				SELECT 1
				FROM [Control].[CreditCardByVehicle] cv
				INNER JOIN [General].[Vehicles] v 
					ON v.[VehicleId] = cv.[VehicleId]
				INNER JOIN [Control].[CreditCard] cc
					ON cc.[CreditCardId] = cv.[CreditCardId]
				WHERE v.[VehicleId] = @pVehicleId
					  AND cv.[CreditCardId] IN (SELECT [CreditcardId]
												FROM [Control].[CreditCard]
												WHERE [CustomerId] = @pCustomerId
													  AND [StatusId] NOT IN (2, 6, 9)
						                        )
				      AND DATEDIFF(MONTH, GETDATE(), CAST(CAST([ExpirationYear] AS varchar) + '-' + CAST([ExpirationMonth] AS varchar) + '-' + CAST(1 AS varchar) AS DATETIME)) > 1
				)
		BEGIN
			SET @Result = 1
		END
		ELSE
		BEGIN
			SET @Result = 0
		END
	END

	IF @pDriverId IS NOT NULL
	BEGIN
		IF EXISTS (
				SELECT 1
				FROM [Control].[CreditCardByDriver] cv
				INNER JOIN [Control].[CreditCard] cc
					ON cc.[CreditCardId] = cv.[CreditCardId]
				WHERE [UserId] = @pDriverId
					AND cc.[CreditCardId] IN (
						SELECT [CreditcardId]
						FROM [Control].[CreditCard]
						WHERE [CustomerId] = @pCustomerId
							AND [StatusId] NOT IN (
								2
								,6
								,9
								)
						)
				  AND DATEDIFF(MONTH, GETDATE(), CAST(CAST([ExpirationYear] AS varchar) + '-' + CAST([ExpirationMonth] AS varchar) + '-' + CAST(1 AS varchar) AS DATETIME)) > 1
				)
		BEGIN
			SET @Result = 1
		END
		ELSE
		BEGIN
			SET @Result = 0
		END
	END

	-- Return the result of the function
	RETURN @Result
END
