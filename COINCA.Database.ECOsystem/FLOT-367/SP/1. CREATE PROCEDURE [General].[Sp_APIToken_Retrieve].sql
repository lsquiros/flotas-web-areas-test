USE [ECOSystemDev]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[General].[Sp_APIToken_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [General].[Sp_APIToken_Retrieve]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author     :	Henry Retana
-- Create date: 11/03/2019
-- Description:	Retrieve API Token Based on Customer or Partner Id
-- ================================================================================================

CREATE PROCEDURE [General].[Sp_APIToken_Retrieve]
(
	@pCustomerId INT = NULL, 
	@pPartnerId INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT [Token] 
	FROM [General].[ClientTokenApi]
	WHERE [CustomerId] = @pCustomerId
	OR [PartnerId] = @pPartnerId
	AND [Active] = 1 

	SET NOCOUNT OFF
END
