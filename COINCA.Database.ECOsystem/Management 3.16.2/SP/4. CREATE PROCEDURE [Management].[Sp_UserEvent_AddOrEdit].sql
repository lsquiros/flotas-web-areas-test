USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_UserEvent_AddOrEdit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_UserEvent_AddOrEdit]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Marjorie Garbanzo Morales
-- Create date: 18/09/2018
-- Description:	Add or Edit the UserEvent
-- Modify by:   Marjorie Garbanzo - 08/11/2018 - Do not validate stops within events
-- Modify by:   Marjorie Garbanzo - 21/11/2018 - Add Accuracy validate
-- Modify by:   Marjorie Garbanzo - 24/11/2018 - Improvement in the condition of entry and exit of the stop
-- Modify by:   Henry Retana - 29/11/2018 - Add validate to the distance
-- Modify by:   Marjorie Garbanzo - 30/11/2018 - Delete history in the Journey Begin event
-- Modify by:   Marjorie Garbanzo - 14/12/2018 - Modify @lGreaterAccuracy value of 75 the 100
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_UserEvent_AddOrEdit]
(
	  @pEventDetailId INT = NULL						
	 ,@pCommerceId INT = NULL
	 ,@pTempCommerceId BIGINT = NULL
	 ,@pLatitude FLOAT = NULL
	 ,@pLongitude FLOAT = NULL
	 ,@pBatteryLevel INT = NULL
	 ,@pAccuracy DECIMAL(4,0) = NULL
	 ,@pHeading DECIMAL(4,0) = NULL
	 ,@pSpeed DECIMAL(4,0) = NULL
	 ,@pAltitude DECIMAL(4,0) = NULL
	 ,@pLapse INT = NULL
	 ,@pEventDateTime DATETIME	
	 ,@pEventType VARCHAR(20) = NULL					
	 ,@pCustomerId INT	 				
	 ,@pUserId INT					
)
AS
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY
		DECLARE @lErrorMessage NVARCHAR(4000)
               ,@lErrorSeverity INT
               ,@lErrorState INT
               ,@lLocalTran BIT = 0
			   ,@lEventTypeId INT = 0
			   ,@lJourneyBegin INT = 0
			   ,@lJourneyEnd INT = 0
			   ,@lParentMainEventLast INT = -1
			   ,@lGreaterAccuracy DECIMAL(4,0) = 100 --------------------PARAMETERS
               ,@lDelete BIT = 1
			   ,@lDistance DECIMAL(4,3) = 0.030 --------------------PARAMETERS
			    
        IF (@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @lLocalTran = 1 
		END

		IF ((@pCommerceId IS NULL OR @pCommerceId = 0) AND (@pTempCommerceId IS NOT NULL))
		BEGIN
			SELECT @pCommerceId = [CommerceId] 
			FROM [Management].[TemporaryRelationshipCommerce]
			WHERE [TempCommerceId] = @pTempCommerceId
		END

		IF @pEventDetailId = 0 SET @pEventDetailId = NULL
		
		SET @lEventTypeId = (CASE WHEN @pEventType = 'location' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail] 
										WHERE [Value] = 'AGENT_LOCATION_REAL_TIME'
									)
								WHEN @pEventType = 'pin' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'CUSTOMERS_COMMERCE_PIN'
									)
								WHEN @pEventType = 'signal-off' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_LOSES_SIGNAL'
									)
								WHEN @pEventType = 'signal-on' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_RESTORE_SIGNAL'
									)
								WHEN @pEventType = 'gps-off' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_LOSES_SIGNAL_GPS'
									)
								WHEN @pEventType = 'gps-on' 
								THEN (
										SELECT [Id] 
										FROM [Management].[CatalogDetail]
										WHERE [Value] = 'AGENT_RESTORE_SIGNAL_GPS'
									)
								ELSE (
										SELECT [EventTypeId] 
										FROM [Management].[EventDetail]
										WHERE [Id] = @pEventDetailId
									)
							END )
				
		
		-- GET THE JOURNEY BEGIN EVENT TYPE
		SELECT @lJourneyBegin = [Id]
		FROM [Management].[catalogDetail]
		WHERE [Value] = 'CUSTOMERS_JOURNEY_BEGIN'
			
		--GET THE JOURNEY END EVENT TYPE
		SELECT @lJourneyEnd = [Id]
		FROM [Management].[catalogDetail]
		WHERE [Value] = 'CUSTOMERS_JOURNEY_END'

		-- CHECK IF THE CURRENT EVENT IS JORNEY BEGIN
		IF @lEventTypeId = @lJourneyBegin
		BEGIN
			--REMOVE HISTORIC
			DELETE FROM [Management].[UserStop]
			WHERE [UserId] = @pUserId
			AND [CustomerId] = @pCustomerId
		END
			
		-- Validate type of event and presicion to identify if it calculates or not stop
		IF ((@pEventType <> 'location') OR (@pEventType = 'location' AND @pAccuracy <= @lGreaterAccuracy))
		BEGIN
			SET @lDelete = 0 
			
			--GET THE PARENT OF THE MAIN EVENTS
			SELECT TOP(1) @lParentMainEventLast = cd.[Parent]
			FROM [Management].[UserEvent] ue
				INNER JOIN [Management].[CatalogDetail] cd
					ON ue.[EventTypeId] = cd.[Id]
			WHERE cd.[CatalogId] = (SELECT Id FROM [Management].[Catalog]
											WHERE [Name] = 'Tipos de Eventos Principales')
				AND [EventTypeId] <> @lJourneyBegin
				AND [UserID] = @pUserId
				AND [CustomerId] = @pCustomerId
			ORDER BY [EventDate] DESC
		
			--EXECUTE STOP 
			IF NOT EXISTS(SELECT * FROM [Management].[UserEvent] 
							WHERE [UserID] = @pUserId
								AND [CustomerId] = @pCustomerId) 
				OR
				(@pEventDateTime > (SELECT MAX([EventDate])
										FROM [Management].[UserEvent] 
										WHERE [UserID] = @pUserId
											AND [CustomerId] = @pCustomerId))
			BEGIN					
				IF ((@pLatitude IS NOT NULL AND @pLongitude IS NOT NULL) 
					AND ((EXISTS( SELECT * FROM [Management].[CatalogDetail] cd
								INNER JOIN [Management].[Catalog] c 
									ON cd.[CatalogId] = c.[Id]
							WHERE c.[Name] = 'Tipos de Eventos Secundarios'
								AND cd.[Id] = @lEventTypeId)
						AND
						(NOT EXISTS(SELECT TOP(1)* FROM [Management].[UserEvent] ue
										INNER JOIN [Management].[CatalogDetail] cd ON ue.[EventTypeId] = cd.[Id]
									WHERE cd.[CatalogId] = (SELECT Id FROM [Management].[Catalog] WHERE [Name] = 'Tipos de Eventos Principales')
										AND [EventTypeId] <> @lJourneyBegin
										AND [UserID] = @pUserId
										AND [CustomerId] = @pCustomerId
										AND cd.[Parent] = 0
									ORDER BY [EventDate] DESC))
					)
					OR 
					(@lParentMainEventLast <> 0 OR @lParentMainEventLast IS NULL))
				)
				BEGIN
					EXEC [Management].[Sp_UserStop_AddOrEdit] @lEventTypeId
															 ,@pUserId
															 ,@pCustomerId
															 ,@pLatitude
															 ,@pLongitude
															 ,@pBatteryLevel
															 ,@pHeading 
															 ,@pSpeed 
															 ,@pAltitude 
															 ,@pEventDateTime
				END
				ELSE
				BEGIN
					--REMOVE HISTORIC
					DELETE FROM [Management].[UserStop]
					WHERE [UserId] = @pUserId
					AND [CustomerId] = @pCustomerId
				END
			END
		END	 
		
		--CHECK IF THE CURRENT EVENT IS JORNEY END
		IF  @lEventTypeId = @lJourneyEnd
		BEGIN
			--REMOVE HISTORIC
			DELETE FROM [Management].[UserStop]
			WHERE [UserId] = @pUserId
			AND [CustomerId] = @pCustomerId
		END
			
		--Validates the range around the last point 
		IF @pEventType = 'location' AND @lDelete = 0
		BEGIN 
			SELECT TOP 1 *
			INTO #TEMPRECORD
			FROM [Management].[UserEvent]			
			WHERE [CustomerId] = @pCustomerId
			AND [UserId] = @pUserId	
			AND ([Delete] = 0 OR [Delete] IS NULL)		
			ORDER BY [Id] DESC

			--CHECK IF THE RECORD IS MOVEMENT 
			IF EXISTS (
						 SELECT TOP 1 *
						 FROM #TEMPRECORD
						 WHERE [EventTypeId] IN (
													SELECT [Id] 
													FROM [Management].[CatalogDetail] 
													WHERE [Value] = 'AGENT_LOCATION_REAL_TIME'
												)
					 )				
			BEGIN 
				DECLARE @lLastLat FLOAT = NULL,
						@lLastLon FLOAT = NULL,
						@lResult DECIMAL(12, 3) = NULL

				SELECT @lLastLat = [Latitude],
					   @lLastLon = [Longitude]
				FROM #TEMPRECORD
			
				DECLARE @lSource GEOGRAPHY = GEOGRAPHY::Point(@pLatitude, @pLongitude, 4326)
				DECLARE @lTarget GEOGRAPHY = GEOGRAPHY::Point(@lLastLat, @lLastLon, 4326)

				SET @lResult = @lSource.STDistance(@lTarget) / 1000

				IF @lResult < @lDistance
				BEGIN
					SET @lDelete = 1
				END 
			END

			DROP TABLE #TEMPRECORD
		END

		INSERT INTO [Management].[UserEvent]
		(
			[EventDetailId]
			,[EventTypeId]
			,[UserId]
			,[CommerceId]
			,[CustomerId]
			,[Latitude]
			,[Longitude]
			,[BatteryLevel]
			,[Heading]
			,[Speed]
			,[Altitude]
			,[Lapse]
			,[EventDate]
			,[Delete]
			,[InsertDate]
			,[InsertUserId]
		)
		VALUES
		(
			 @pEventDetailId
			,@lEventTypeId
			,@pUserId
			,@pCommerceId
			,@pCustomerId
			,@pLatitude
			,@pLongitude
			,@pBatteryLevel
			,@pHeading 
			,@pSpeed 
			,@pAltitude 
			,@pLapse 
			,@pEventDateTime
			,@lDelete
			,GETDATE()
			,@pUserId
		)

        IF @@TRANCOUNT > 0 AND @lLocalTran = 1 COMMIT TRANSACTION	
				
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0  AND XACT_STATE() > 0 ROLLBACK TRANSACTION	
		SELECT @lErrorMessage = ERROR_MESSAGE(), @lErrorSeverity = ERROR_SEVERITY(), @lErrorState = ERROR_STATE() 
		RAISERROR (@lErrorMessage, @lErrorSeverity, @lErrorState)		
	END CATCH
        
	SET NOCOUNT OFF
    SET XACT_ABORT OFF 
END
