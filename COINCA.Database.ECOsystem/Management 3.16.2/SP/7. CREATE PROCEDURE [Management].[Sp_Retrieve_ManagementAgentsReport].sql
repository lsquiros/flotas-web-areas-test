USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Retrieve_ManagementAgentsReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Retrieve_ManagementAgentsReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================  
-- Author:  Henry Retana
-- Create date: 13/10/2017  
-- Description: Retrieve Management Events
-- ================================================================================================  

CREATE PROCEDURE [Management].[Sp_Retrieve_ManagementAgentsReport]
(  
	 @pUserId INT = NULL
	,@pCustomerId INT = NULL
	,@pStartDate DATETIME = NULL
	,@pEndDate DATETIME = NULL	
)  
AS  
BEGIN
	SET NOCOUNT ON
	
	DECLARE	 @lCount INT
			,@lUsertCount INT
			,@lTempBeginDate DATETIME
			,@lTempFinishDate DATETIME
			,@lEventTypeId INT
			,@lLatitude FLOAT
			,@lLongitude FLOAT
			,@lTempLatitude FLOAT
			,@lTempLongitude FLOAT
			,@lTempDateTime DATETIME
			,@lEventDate DATETIME
			,@lMovementTimeSTR VARCHAR(50)
			,@lStopTime VARCHAR(50)
			,@lHours VARCHAR(5)
			,@lMinutes VARCHAR(5)
			,@lSeconds VARCHAR(5)
			,@lWorkingTotalSeconds INT
			,@lProductiveTotalSeconds INT
			,@lAVGBeginJourney VARCHAR(100)
			,@lAVGFinishJourney VARCHAR(100)
			,@lBeginLunchDateTime VARCHAR(100)
			,@lFinishLunchDateTime VARCHAR(100)
			,@lBeginManageDateTime VARCHAR(100)
			,@lFinishManageDateTime VARCHAR(100)
			,@lWorkingTime VARCHAR(50)
			,@lRealLunchTime VARCHAR(50)
			,@lProductiveTime VARCHAR(50)
			,@lPorcProductiveTime VARCHAR(10)
			,@lUserName VARCHAR(500)
			,@lFinalUserId INT
			---EVENTS
			,@lLogIn INT
			,@lLogOut INT
			,@lStartVisit INT
			,@lEndVisit INT
			,@lLocation INT

			,@lTimeZone INT

	SELECT @lTimeZone = co.[TimeZone]
	FROM [General].[Customers] c
	INNER JOIN [General].[Countries] co
		ON co.[CountryId] = c.[CountryId]
	WHERE c.[CustomerId] = @pCustomerId 

	--GET THE IDS FOR THE EVENTS--
	SELECT @lLogout = c.[Id],
		   @lLogin = c.[Parent]
	FROM [Management].[CatalogDetail] c
	INNER JOIN [dbo].[GeneralParameters] p
		ON c.[Parent] = p.[Value]
	WHERE p.[ParameterID] = 'CUSTOMERS_BEGIN_JOURNEY'	

	SELECT @lEndVisit= c.[Id],
		   @lStartVisit = c.[Parent]
	FROM [Management].[CatalogDetail] c
	INNER JOIN [dbo].[GeneralParameters] p
		ON c.[Parent] = p.[Value]
	WHERE p.[ParameterID] = 'CUSTOMERS_CLIENT_BEGIN_VISIT'
	
	SELECT @lLocation = [Value]
	FROM [dbo].[GeneralParameters]
	WHERE [ParameterID] = 'AGENT_LOCATION_REAL_TIME'
	--**************************--
	
	CREATE TABLE #EVENTS 
	(	
		[Id] INT,
		[UserId] INT,
		[EventDetailId] INT,
		[EventDetailName] VARCHAR(200),
		[EventTypeId] INT,
		[EventName] VARCHAR(200),
		[Parent] INT,
		[CommerceId] INT,
		[Latitude] FLOAT,
		[Longitude] FLOAT,
		[BatteryLevel] INT,
		[Speed] DECIMAL(4, 0),
		[EventDate] DATETIME,
		[Lapse] INT,
		[Distance] DECIMAL(12, 2)
	)

	CREATE TABLE #EVENTSSTOPDAY
	(	
		[Id] INT,
		[UserId] INT,
		[EventDetailId] INT,
		[EventDetailName] VARCHAR(200),
		[EventTypeId] INT,
		[EventName] VARCHAR(200),
		[Parent] INT,
		[CommerceId] INT,
		[Latitude] FLOAT,
		[Longitude] FLOAT,
		[BatteryLevel] INT,
		[Speed] DECIMAL(4, 0),
		[EventDate] DATETIME,
		[Lapse] INT,
		[Distance] DECIMAL(12, 2)
	)

	CREATE TABLE #EVENTSDAY 
	(
		 [Id] INT
		,[EventTypeId] INT
		,[Latitude] FLOAT
		,[Longitude] FLOAT
		,[EventDate] DATETIME
		,[Name] VARCHAR(300)		
		,[Lapse] INT
		,[Distance] DECIMAL(12, 2)
	)

	CREATE TABLE #RESULT 
	(
		 [AVGBeginJourney] VARCHAR(100)
		,[AVGFinishJourney] VARCHAR(100)
		,[WorkingTime] VARCHAR(50)
		,[BeginLunchDateTime] VARCHAR(100)
		,[FinishLunchDateTime] VARCHAR(100)
		,[RealLunchTime] VARCHAR(50)
		,[BeginManageDateTime] VARCHAR(100)
		,[FinishManageDateTime] VARCHAR(100)
		,[ProductiveTime] VARCHAR(50)
		,[PorcProductiveTime] VARCHAR(10)
		,[Kilometers] DECIMAL(16, 3)
		,[MovementTime] VARCHAR(50)
		,[StopTime] VARCHAR(50)
		,[Date] VARCHAR(50)
		,[EventDate] DATETIME
		,[UserId] INT
		,[AgentName] VARCHAR(500)
		,[TotalLunch] INT
		,[TotalProductive] INT
		,[TotalMovement] INT
		,[TotalStop] INT
		,[WorkingGlobal] INT
	)

	CREATE TABLE #USERS
	(
		 [Id] INT IDENTITY
		,[UserId] INT
		,[Name] VARCHAR(500)
	)

	INSERT INTO #USERS
	SELECT u.[UserId]
		  ,u.[Name]
	FROM [General].[DriversUsers] du
	INNER JOIN [General].[Users] u 
		ON u.[UserId] = du.[UserId]
	WHERE [CustomerId] = @pCustomerId
	AND (@pUserId IS NULL OR u.[UserId] = @pUserId)
	AND u.[IsAgent] = 1

	SELECT @lUsertCount = MIN([Id])
	FROM #USERS

	WHILE @lUsertCount IS NOT NULL
	BEGIN

		DECLARE  @lKilometersAVG DECIMAL(16, 2) = 0
				,@lTotalMovementTime INT = 0
				,@lTotalStopTime INT = 0
				,@lTotalProductiveTime INT = 0
				,@lTotalLunchTime INT = 0

		SELECT @pUserId = [UserId],
			   @lFinalUserId = [UserId],
			   @lUserName = [Name] 			
		FROM #USERS
		WHERE [Id] = @lUsertCount

		INSERT INTO #EVENTS
		EXEC [Management].[Sp_ReconstructionCleanData_Retrieve] @pUserId, @pStartDate, @pEndDate, 0
		
		IF EXISTS (SELECT * FROM #EVENTS)
		BEGIN
			SET @lTempBeginDate = @pStartDate
			SET @lTempFinishDate = @pEndDate

			WHILE @lTempBeginDate <= @lTempFinishDate
			BEGIN
				DECLARE @lKilometers DECIMAL(12, 2) = 0
					   ,@lMovementTime INT = 0
					   ,@lProductSeconds INT = 0
					   ,@lHasEndSession BIT = 1
					   ,@lDeclareStopEndDate DATETIME 

				INSERT INTO #EVENTSDAY
				SELECT [Id]
					  ,[EventTypeId]
					  ,[Latitude]
					  ,[Longitude]
					  ,[EventDate]
					  ,ISNULL([EventDetailName], [EventName])
					  ,[Lapse]
					  ,[Distance]
				FROM #EVENTS
				WHERE DATEPART(DAY, [EventDate]) = DATEPART(DAY, @lTempBeginDate)
				AND DATEPART(MONTH, [EventDate]) = DATEPART(MONTH, @lTempBeginDate)

				SELECT @lDeclareStopEndDate = DATEADD(DAY, 1, @lTempBeginDate)

				--VALIDATE STOP TIME				
				INSERT INTO #EVENTSSTOPDAY
				EXEC [Management].[Sp_ReconstructionCleanData_Retrieve] @pUserId, @lTempBeginDate, @lDeclareStopEndDate, 1
				
				SELECT @lKilometers = SUM([Distance])
				FROM #EVENTSDAY
				WHERE [Distance] IS NOT NULL
				
				--FINALIZA WHILE DEL COUNT   
				IF EXISTS (SELECT 1 FROM #EVENTSDAY)
				BEGIN				
					SELECT @lAVGBeginJourney = NULL
						  ,@lAVGFinishJourney = NULL
						  ,@lWorkingTime = NULL
						  ,@lRealLunchTime = NULL
						  ,@lProductiveTime = NULL
						  ,@lPorcProductiveTime = NULL
						  ,@lBeginLunchDateTime = NULL
						  ,@lFinishLunchDateTime = NULL
						  ,@lBeginManageDateTime = NULL
						  ,@lFinishManageDateTime = NULL	
						  ,@lMovementTime = NULL
						  ,@lStopTime = NULL
						  ,@lTotalMovementTime = 0
						  ,@lTotalStopTime = 0
						  ,@lTotalProductiveTime = 0
						  ,@lTotalLunchTime = 0

					--LOG IN
					SELECT TOP 1 @lAVGBeginJourney = CAST(RIGHT([EventDate], 8) AS VARCHAR(100))
					FROM #EVENTSDAY
					WHERE [EventTypeId] = @lLogIn
					ORDER BY [EventDate] ASC

					SELECT TOP 1 @lAVGFinishJourney = CAST(RIGHT([EventDate], 8) AS VARCHAR(100))
					FROM #EVENTSDAY
					WHERE [EventTypeId] = @lLogOut
					AND [EventDate] > (
										SELECT TOP 1 [EventDate]
										FROM #EVENTSDAY
										WHERE [EventTypeId] = @lLogIn 
										ORDER BY [EventDate] ASC
									  )
					ORDER BY [EventDate] DESC

					--VALIDATES SESSION END
					SELECT @lHasEndSession = CASE WHEN @lAVGFinishJourney IS NULL 
												  THEN 0 
												  ELSE 1 
											 END,
						   @lAVGFinishJourney =	CASE WHEN @lAVGFinishJourney IS NULL 
													 THEN  CASE WHEN @lDeclareStopEndDate = DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, @lTimeZone, GETDATE())), 1)   
																THEN CAST(RIGHT(DATEADD(HOUR, @lTimeZone, GETDATE()), 8) AS VARCHAR(100))
																ELSE CAST(RIGHT(DATEADD(S, -1, @lDeclareStopEndDate), 8) AS VARCHAR(100)) 
														   END	
													 ELSE @lAVGFinishJourney
												END

					SELECT @lHours = CAST(ROUND(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)) / 3600, 0, 1) AS VARCHAR)
						  ,@lMinutes = CAST(ROUND(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)) / 60, 0, 1) % 60 AS VARCHAR)
						  ,@lSeconds = CAST(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)) % 60 AS VARCHAR)
						  ,@lWorkingTotalSeconds = ROUND(DATEDIFF(s, CAST(@lAVGBeginJourney AS DATETIME), CAST(@lAVGFinishJourney AS DATETIME)), 0, 1)
					
					SELECT @lWorkingTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)
					------------

					--LUNCH
					SELECT TOP 1 @lBeginLunchDateTime = CAST(RIGHT(e.[EventDate], 8) AS VARCHAR(100))
					FROM #EVENTSDAY e
					INNER JOIN [Management].[CatalogDetail] cd
						ON e.[EventTypeId] = cd.[Id]
					WHERE cd.[Value] = 'CUSTOMERS_LUNCH_TIME_BEGIN'

					SELECT TOP 1 @lFinishLunchDateTime = CAST(RIGHT(e.[EventDate], 8) AS VARCHAR(100))
					FROM #EVENTSDAY e
					INNER JOIN [Management].[CatalogDetail] cd
						ON e.[EventTypeId] = cd.[Id]
					WHERE cd.[Value] = 'CUSTOMERS_LUNCH_TIME_END'

					SELECT @lHours = CAST(ROUND(DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME)) / 3600, 0, 1) AS VARCHAR)
						  ,@lMinutes = CAST(ROUND(DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME)) / 60, 0, 1) % 60 AS VARCHAR)
						  ,@lSeconds = CAST(DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME)) % 60 AS VARCHAR)

					SELECT @lRealLunchTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

					SELECT @lTotalLunchTime = @lTotalLunchTime + DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME))
					------------------
					
				    --Movement and Stop and management
					DECLARE @lEndMovement DATETIME = NULL 

					SET @lEndMovement = CASE WHEN @lDeclareStopEndDate = DATEADD(DAY, DATEDIFF(DAY, 0, DATEADD(HOUR, @lTimeZone, GETDATE())), 1)   
											 THEN DATEADD(HOUR, @lTimeZone, GETDATE())
											 ELSE DATEADD(S, -1, @lDeclareStopEndDate)
										END

				    SELECT * 
					INTO #FRAGMENTS
					FROM  
					(
						SELECT TOP 1 * 
						FROM #EVENTSDAY
						UNION
						SELECT r.*
						FROM #EVENTSDAY r,
							 #EVENTSDAY r2
						WHERE r.[Id] = (r2.[Id] + 1) 
						AND r.[EventTypeId] <> r2.[EventTypeId]
					) [Fragments]

					SELECT @lMovementTime = SUM([SecondsMovement])
					FROM 
					(
						SELECT DATEDIFF(s, r.[EventDate], ISNULL((
																	SELECT TOP 1 [EventDate]
																	FROM #FRAGMENTS
																	WHERE [Id] > r.[Id]
																	ORDER BY [Id]
																), @lEndMovement)) [SecondsMovement]
						FROM #FRAGMENTS r
						WHERE r.[EventTypeId] = @lLocation
					) [Sum]
					
					SELECT @lStopTime = SUM([Lapse])
					FROM #EVENTSSTOPDAY
					WHERE [EventTypeId] = (
											 SELECT [Id]
											 FROM [Management].[CatalogDetail] 
											 WHERE [Value] = 'AGENT_STOP'
										  )

					SELECT @lMovementTime = @lMovementTime - ISNULL(@lStopTime, 0)

					--Remove lunch time from movement
					SELECT @lMovementTime = @lMovementTime - ISNULL(DATEDIFF(s, CAST(@lBeginLunchDateTime AS DATETIME), CAST(@lFinishLunchDateTime AS DATETIME)), 0)
					
					--VALIDATES - 
					SELECT @lMovementTime = CASE WHEN @lMovementTime < 0 THEN 0 ELSE @lMovementTime END 

					--VISIT
					SELECT @lBeginManageDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, [EventDate], 108)), 108), 8) AS VARCHAR(100))
					FROM #EVENTSDAY
					WHERE [EventTypeId] = @lStartVisit

					SELECT @lFinishManageDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, [EventDate], 108)), 108), 8) AS VARCHAR(100))
					FROM #EVENTSDAY
					WHERE [EventTypeId] = @lEndVisit

					SELECT @lProductSeconds = ISNULL(SUM([SecondsManagement]), 0)
					FROM 
					(
						SELECT DATEDIFF(s, r.[EventDate], (SELECT TOP 1 [EventDate]
														   FROM #FRAGMENTS
														   WHERE [Id] > r.[Id]
														   AND [EventTypeId] = @lEndVisit
														   ORDER BY [Id])) [SecondsManagement]
						FROM #FRAGMENTS r
						WHERE r.[EventTypeId] = @lStartVisit
					) [Sum]

					SELECT @lHours = CAST(ROUND(@lProductSeconds / 3600, 0, 1) AS VARCHAR)
						  ,@lMinutes = CAST(ROUND(@lProductSeconds / 60, 0, 1) % 60 AS VARCHAR)
						  ,@lSeconds = CAST(@lProductSeconds % 60 AS VARCHAR)
						  ,@lProductiveTotalSeconds = ROUND(@lProductSeconds, 0, 1)

					SELECT @lProductiveTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)
									
					DROP TABLE #FRAGMENTS

					SELECT @lTotalProductiveTime = @lTotalProductiveTime + @lProductSeconds
					SELECT @lTotalMovementTime = @lTotalMovementTime + @lMovementTime
					SELECT @lTotalStopTime = @lTotalStopTime + @lStopTime
					-------------------------------------------------------------------------------------

					SELECT @lHours = CAST(ROUND(@lMovementTime / 3600, 0, 1) AS VARCHAR)
						  ,@lMinutes = CAST(ROUND(@lMovementTime / 60, 0, 1) % 60 AS VARCHAR)
						  ,@lSeconds = CAST(@lMovementTime % 60 AS VARCHAR)

					SELECT @lMovementTimeSTR = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

					SELECT @lHours = CAST(ROUND(@lStopTime / 3600, 0, 1) AS VARCHAR)
						  ,@lMinutes = CAST(ROUND(@lStopTime / 60, 0, 1) % 60 AS VARCHAR)
						  ,@lSeconds = CAST(@lStopTime % 60 AS VARCHAR)

					SELECT @lStopTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

					SELECT @lPorcProductiveTime = CASE WHEN CAST(@lWorkingTotalSeconds AS DECIMAL) > 0 
													   THEN CAST(CAST((@lProductiveTotalSeconds / CAST(@lWorkingTotalSeconds AS DECIMAL)) * 100 AS DECIMAL(12, 0)) AS VARCHAR(20))
													   ELSE '0'
												  END
					
					INSERT INTO #RESULT
					SELECT LTRIM(@lAVGBeginJourney)
						  ,CASE WHEN @lHasEndSession = 0 
							    THEN NULL 
								ELSE LTRIM(@lAVGFinishJourney)
						   END	
						  ,@lWorkingTime
						  ,@lBeginLunchDateTime
						  ,@lFinishLunchDateTime
						  ,@lRealLunchTime
						  ,@lBeginManageDateTime
						  ,@lFinishManageDateTime
						  ,@lProductiveTime
						  ,ISNULL(@lPorcProductiveTime, 0)
						  ,ISNULL(@lKilometers, 0)
						  ,@lMovementTimeSTR
						  ,@lStopTime
						  ,CONVERT(VARCHAR(8), @lTempBeginDate, 3)
						  ,@lTempBeginDate
						  ,@lFinalUserId
						  ,@lUserName
						  ,@lTotalLunchTime
						  ,@lTotalProductiveTime						  
						  ,@lTotalMovementTime
						  ,@lTotalStopTime
						  ,@lWorkingTotalSeconds

					SET @lTempLatitude = NULL
					SET @lTempLongitude = NULL
					SET @lTempDateTime = NULL
					SET @lKilometersAVG = @lKilometersAVG + @lKilometers
				END

				SELECT @lTempBeginDate = DATEADD(day, 1, @lTempBeginDate)

				DELETE FROM #EVENTSDAY
				DELETE FROM #EVENTSSTOPDAY 
			END

			--AVG
			SELECT @lAVGBeginJourney = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([AVGBeginJourney] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lAVGFinishJourney = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([AVGFinishJourney] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lHours = CAST(ROUND(AVG([WorkingGlobal]) / 3600, 0, 1) AS VARCHAR)
				  ,@lMinutes = CAST(ROUND(AVG([WorkingGlobal]) / 60, 0, 1) % 60 AS VARCHAR)
				  ,@lSeconds = CAST(AVG([WorkingGlobal]) % 60 AS VARCHAR)
			FROM #RESULT  
			 
			SELECT @lWorkingTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

			SELECT @lBeginLunchDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([BeginLunchDateTime] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lFinishLunchDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([FinishLunchDateTime] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lHours = CAST(ROUND(AVG([TotalLunch]) / 3600, 0, 1) AS VARCHAR)
				  ,@lMinutes = CAST(ROUND(AVG([TotalLunch]) / 60, 0, 1) % 60 AS VARCHAR)
				  ,@lSeconds = CAST(AVG([TotalLunch]) % 60 AS VARCHAR)
		    FROM #RESULT
					
			SELECT @lRealLunchTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

			SELECT @lBeginManageDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([BeginManageDateTime] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lFinishManageDateTime = CAST(RIGHT(CONVERT(DATETIME, AVG(CONVERT(FLOAT, CAST([FinishManageDateTime] AS DATETIME), 108)), 108), 8) AS VARCHAR(100))
			FROM #RESULT

			SELECT @lHours = CAST(ROUND(AVG([TotalProductive]) / 3600, 0, 1) AS VARCHAR)
				  ,@lMinutes = CAST(ROUND(AVG([TotalProductive]) / 60, 0, 1) % 60 AS VARCHAR)
				  ,@lSeconds = CAST(AVG([TotalProductive]) % 60 AS VARCHAR)
				  ,@lProductiveTotalSeconds = ROUND(AVG([TotalProductive]), 0, 1)
			FROM #RESULT
			  
			SELECT @lProductiveTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

			SELECT @lHours = CAST(ROUND(AVG([TotalMovement]) / 3600, 0, 1) AS VARCHAR)
				  ,@lMinutes = CAST(ROUND(AVG([TotalMovement]) / 60, 0, 1) % 60 AS VARCHAR)
				  ,@lSeconds = CAST(AVG([TotalMovement]) % 60 AS VARCHAR)
			FROM #RESULT

			SELECT @lMovementTimeSTR = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

			SELECT @lHours = CAST(ROUND(AVG([TotalStop]) / 3600, 0, 1) AS VARCHAR)
				  ,@lMinutes = CAST(ROUND(AVG([TotalStop]) / 60, 0, 1) % 60 AS VARCHAR)
				  ,@lSeconds = CAST(AVG([TotalStop])  % 60 AS VARCHAR)
			FROM #RESULT
				 
			SELECT @lStopTime = [General].[Fn_GetTimeFormat] (@lHours, @lMinutes, @lSeconds)

			SELECT @lPorcProductiveTime = CAST(CAST((@lProductiveTotalSeconds / CAST(@lWorkingTotalSeconds AS DECIMAL)) * 100 AS DECIMAL(12, 0)) AS VARCHAR(20))

			INSERT INTO #RESULT
			SELECT LTRIM(@lAVGBeginJourney)
				  ,LTRIM(@lAVGFinishJourney)
				  ,@lWorkingTime
				  ,@lBeginLunchDateTime
				  ,@lFinishLunchDateTime
				  ,@lRealLunchTime
				  ,@lBeginManageDateTime
				  ,@lFinishManageDateTime
				  ,@lProductiveTime
				  ,ISNULL(@lPorcProductiveTime, 0)
				  ,ISNULL(@lKilometersAVG, 0)
				  ,@lMovementTimeSTR
				  ,@lStopTime
				  ,'Promedio del Período'
				  ,NULL
				  ,@lFinalUserId
				  ,@lUserName
				  ,NULL
				  ,NULL
				  ,NULL
				  ,NULL
				  ,NULL

			DELETE FROM #EVENTS				
		END

		SELECT @lUsertCount = MIN([Id])
		FROM #USERS
		WHERE [Id] > @lUsertCount
	END

	SELECT [AVGBeginJourney]
		  ,[AVGFinishJourney]
		  ,[WorkingTime]
		  ,[BeginLunchDateTime]
		  ,[FinishLunchDateTime]
		  ,[RealLunchTime] 
		  ,[BeginManageDateTime]
		  ,[FinishManageDateTime]
		  ,[ProductiveTime]
		  ,[PorcProductiveTime]
		  ,[Kilometers]
		  ,[MovementTime]
		  ,[StopTime]
		  ,[Date]
		  ,[EventDate]
		  ,[UserId]
		  ,[AgentName]
	FROM #RESULT

	DROP TABLE #RESULT,
			   #EVENTS,
			   #EVENTSDAY,
			   #USERS,
			   #EVENTSSTOPDAY

	SET NOCOUNT OFF
END
