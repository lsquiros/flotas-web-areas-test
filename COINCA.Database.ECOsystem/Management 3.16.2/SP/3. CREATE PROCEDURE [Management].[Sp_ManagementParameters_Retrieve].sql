USE [ECOSystemQA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_ManagementParameters_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_ManagementParameters_Retrieve]
GO
/****** Object:  StoredProcedure [Management].[Sp_ManagementParameters_Retrieve]    Script Date: 08/01/2019 08:21:33 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 21/07/2017
-- Description:	Retrieve management parameters
-- Modify by:   Marjorie Garbanzo - 14/12/2018 - Add ManagementMinDistance Parameter
-- Modify by:   Marjorie Garbanzo - 08/01/2019 - Convert ManagementMinDistance Parameter to int
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_ManagementParameters_Retrieve]
(
	@pCustomerId INT
)
AS
BEGIN	
	SET NOCOUNT ON;

	SELECT [ClientMinutes],
		   [LostMinutes],
		   [MovingMinutes],
		   [TopMargin],
		   [LowMargin],
		   [TopPercent],
		   [LowPercent],
		   [MiddlePercent],
		   [GeofenceDistance],
		   [TimeBeginJourney],
		   [TimeFinishJourney],
		   [DataSync],
		   [MovementName], 
		   [ReconstructionType],
		   [StopMinimumTime],
		   [StopMinimumDistance],
		   [StopMinimumSpeed],
		   CAST([ManagementMinDistance] / 0.001 AS INT) [ManagementMinDistance]
	FROM [Management].[Parameters]
	WHERE [CustomerId] = @pCustomerId	
		
	SET NOCOUNT OFF
END
