USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Management].[Sp_Survey_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Management].[Sp_Survey_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 19/09/2018
-- Description:	Survey Retrieve
-- Modify by:   Marjorie Garbanzo - 19/11/2018 - Exclude Survey without questions
-- Modify by:   Henry Retana - 27/12/2018 - Survey without questions
-- ================================================================================================

CREATE PROCEDURE [Management].[Sp_Survey_Retrieve]
(	
	 @pId INT = NULL,
	 @pCustomerId INT,
	 @pUserId INT = NULL
)
AS
BEGIN	
	SET NOCOUNT ON
	
   SELECT  sv.[Id],
		   sv.[Name],
		   sv.[Description],
		   sv.[QuestionsStr]
	FROM (
		   SELECT  s.[Id],
				   s.[Name],
				   s.[Description],
				   CAST((SELECT q.[Id],
								q.[Text],
								q.[Description],
								q.[QuestionTypeId],
								q.[Order],
								CAST(q.[Delete] AS BIT) [Delete],
								(
								   SELECT [Value]
								   FROM [Management].[CatalogDetail]
								   WHERE [Id] = q.[QuestionTypeId]
								) [TypeId],
								(SELECT o.[Id],
										o.[Text],
										o.[Order],
										o.[NextQuestionId],
										CAST(o.[Delete] AS BIT) [Delete]
								 FROM [Management].[Option] o
								 WHERE o.[Delete] = 0
								 AND o.[QuestionId] = q.[Id] FOR XML PATH('Option'), ROOT('Option'), ELEMENTS XSINIL)  [OptionsStr]
						 FROM [Management].[Question] q
						 WHERE q.[Delete] = 0
						 AND q.[SurveyId] = s.[Id] FOR XML PATH('Question'), ROOT('Question'), ELEMENTS XSINIL) AS VARCHAR(MAX)) [QuestionsStr]
			FROM [Management].[Survey] s
			WHERE s.[Delete] = 0
				AND s.[CustomerId] = @pCustomerId
				AND (@pId IS NULL OR s.[Id] = @pId)
		) sv

	SET NOCOUNT OFF
END
