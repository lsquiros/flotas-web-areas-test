--USE [ECOsystem]
GO

 -- ================================================================================================
-- Author:		Henry Retana
-- Create date: 18/12/2018
-- Description:	Add finish Other event 
-- ================================================================================================

DECLARE @lEventId INT

SELECT @lEventId = [Id]
FROM [Management].[CatalogDetail]
WHERE [Name] = 'Iniciar Otro'
AND [CatalogId] = 1

INSERT INTO [Management].[CatalogDetail]
(
	[CatalogId],
	[Name],
	[Value],
	[Description],
	[Parent],
	[Active],
	[InsertDate]
)
SELECT  [CatalogId],
		'Finalizar Otro',
		'CUSTOMERS_OTHER_FINISH',
		[Description],
		@lEventId,
		[Active],
		GETDATE()
FROM [Management].[CatalogDetail]
WHERE [Id] = @lEventId