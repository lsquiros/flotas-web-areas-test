USE [ECOsystem]
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 28/12/2018
-- Description:	Update Menu Names for Management
-- ================================================================================================

UPDATE [dbo].[AspMenus] 
SET [Name] = 'Visitas por Zona Geopol�tica' 
WHERE [Name] = 'Visitas por Zona Geop�litica' 
AND [Header] = 22

UPDATE [dbo].[AspMenus] 
SET [Name] = 'Gesti�n de Agentes' 
WHERE [Name] = 'Gestion de Agentes' 
AND [Header] = 22

UPDATE [dbo].[AspMenus] 
SET [Name] = 'Resumen Gesti�n de Agentes' 
WHERE [Name] = 'Resumen Gestion de Agentes' 
AND [Header] = 22