--USE [ECOsystem]
GO

 -- ================================================================================================
-- Author:		Marjorie Garbanzo 
-- Create date: 29/01/2019
-- Description:	Add column for the location point accuracy
-- ================================================================================================

ALTER TABLE [Management].[UserEvent]
ADD [Accuracy] DECIMAL(4,0)