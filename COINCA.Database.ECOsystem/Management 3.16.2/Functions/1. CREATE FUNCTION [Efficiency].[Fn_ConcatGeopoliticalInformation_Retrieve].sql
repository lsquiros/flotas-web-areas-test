--USE [ECOsystem]
GO

IF EXISTS (SELECT 1	FROM sys.objects WHERE object_id = OBJECT_ID(N'[Efficiency].[Fn_ConcateGeographicInformation_Retrieve]') AND type IN (N'FN',N'IF',N'TF',N'FS',N'FT'))
	DROP FUNCTION [Efficiency].[Fn_ConcateGeographicInformation_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Henry Retana
-- Create date: 07/07/2017
-- Description:	Retrieve Geographic Information
-- ================================================================================================

CREATE FUNCTION [Efficiency].[Fn_ConcatGeopoliticalInformation_Retrieve]
(
	@pLatitude FLOAT,		
    @pLongitude FLOAT, 	
    @pCountryCode VARCHAR(3)
)
RETURNS VARCHAR(300)
AS
BEGIN
	DECLARE @lResult VARCHAR(300) 

	SELECT TOP 1 @lResult = CONCAT(s.[Name], ',', co.[Name], ',', ci.[Name])
	FROM [General].[Countries] c
	INNER JOIN [General].[States] s 
		ON c.[CountryId] = s.[CountryId]
	INNER JOIN [General].[Counties] co 
		ON co.[StateId] = s.[StateId] 
	LEFT JOIN [General].[Cities] ci
		ON ci.[CountyId] = co.[CountyId]
	WHERE c.[Code] = @pCountryCode
	ORDER BY  (ACOS(SIN(@pLatitude * PI() / 180) * SIN(ISNULL(ci.[Latitude], co.[Latitude]) * PI()  / 180) + 
			   COS(@pLatitude * PI() / 180) * COS(ISNULL(ci.[Latitude], co.[Latitude]) * PI() / 180) * 
		       COS((@pLongitude - ISNULL(ci.[Longitude], co.[Longitude])) * PI() / 180)) * 180 / PI()) * 60 * 1.1515	
	
    RETURN @lResult
END