USE [ECOsystem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Control].[Sp_CreditCardsReport_Retrieve]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [Control].[Sp_CreditCardsReport_Retrieve]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author     :	Danilo Hidalgo
-- Create date: 01/20/2015
-- Description:	Retrieve Credit Cards Report information
-- Modify By  : Gerald Solano 
-- Modify date: 19/04/2016 
-- Description: Add new columns(Costo Emision, TJ Activas, TJ Bloqueadas) in the retrieve data 
-- Modify By:   Stefano Quirós Ruiz
-- Description: Add and filter for a new column [ModifyDate]
-- Modify By:   Gerald Solano
-- Modify date: 13/07/2016 
-- Description: @CantCardAdmin contabiliza las tarjetas activas y las bloqueadas
-- Modify By:   Gerald Solano
-- Modify date: 20/07/2016 
-- Description: @CantCardEmision contabiliza Entregadas
-- Modify By:   Gerald Solano
-- Modify date: 22/07/2016 
-- Description: @CantCardAdmin reste los indicadores de Activas y Bloqueadas del Período
-- Modify By:   Gerald Solano
-- Modify date: 23/08/2016 
-- Description: SE SUMARON LAS TARJETAS QUE FUERON ACTIVAS / BLOQUEADAS Y QUE PARA LA FECHA ACTUAL SE CERRARON (Y NO ENTRARON EN LOS RANGOS DE CONSULTA)
-- Modify By: Stefano Quirós - Add the Time Zone Parameter - 12/06/2016
-- Modify By:   Gerald Solano
-- Modify date: 01/02/2017 
-- Description: Se ajustaron nuevamente los indicadores de Activas, Bloqueadas y Cerradas
-- Modify By:   Henry Retana
-- Modify date: 03/15/2017 
-- Description: Modificaciones en la forma de obtener la zona horaria para validar las fechas
-- Modify By:	Marco Cabrera
-- Modify By:	24/03/2017
-- Description: Add the amount of reposition cards
-- ==========================================================================================================
-- Modify by:	Maria de los Angeles Jimenez Chavarria
-- Modify date:	Nov/05/2018
-- Description:	Optimization task
-- ==========================================================================================================
-- Modify: Stefano Quirós Ruiz
-- Date: 03/04/2019
-- Description: Change the source on Customer to bring the Client Account Number to the CustomerCredits table
-- Modify By: Stefano Quirós - 10/06/2019 - Change the calculate of the Administrative Card and the logic of 
-- Close, Block and active cards of the period
-- Stefano Quirós - Add SAP Code Column and Legal Document clumn to the retrieve - 11/11/2019
-- ==========================================================================================================
CREATE PROCEDURE [Control].[Sp_CreditCardsReport_Retrieve] --64, 1026, 4, 2019, 5, null, null
(
	@pCustomerId INT = NULL,		--@pCustomerId: CustomerId
	@pPartnerId INT = NULL,		    --@pPartnerId: PartnerId
	@pCountryId INT = NULL,			--@pCountryId: CountryId
	@pYear INT = NULL,				--@pYear: Year
	@pMonth INT = NULL,				--@pMonth: Month
	@pStartDate DATETIME = NULL,	--@pStartDate: Start Date
	@pEndDate DATETIME = NULL		--@pEndDate: End Date
)
AS
BEGIN	
	SET NOCOUNT ON

	DECLARE @CantCardAdmin INT = 0
	DECLARE @CantCardEmision INT = 0
	DECLARE @CantCloseCards INT = 0
	DECLARE @CantBlockCards INT = 0
	DECLARE @CantActiveCards INT = 0
	DECLARE @CurrentCustomerId INT = -1
	DECLARE @lTimeZoneParameter INT
	DECLARE @CantCardReposition INT = 0 
	DECLARE @CreditCardId INT
	DECLARE @Accum INT = 0

	DECLARE @tmpCards AS TABLE(
		[CreditCardId] INT,
		[CreditCardNumber] NVARCHAR(520),
		[ApplicantName] VARCHAR(250),
		[StatusName] VARCHAR(50), 
		[RequestDate] DATETIME,
		[ModifyDate] DATETIME,
		[DeliveryDate] DATETIME,
		[CardRequestId] INT,
		[HasBeenActive] INT DEFAULT 0
	)

	DECLARE @tmpCardsHistoric AS TABLE(
		[CreditCardId] INT,
		[CreditCardNumber] NVARCHAR(520),
		[ApplicantName] VARCHAR(250),
		[StatusName] VARCHAR(50), 
		[RequestDate] DATETIME,
		[ModifyDate] DATETIME,
		[DeliveryDate] DATETIME,
		[CardRequestId] INT,
		[HasBeenActive] INT DEFAULT 0
	)

	DECLARE @tmpCardsFixCloseCards AS TABLE(
		[CreditCardId] INT,
		[CreditCardNumber] NVARCHAR(520),
		[ApplicantName] VARCHAR(250),
		[StatusName] VARCHAR(50), 
		[RequestDate] DATETIME,
		[ModifyDate] DATETIME,
		[DeliveryDate] DATETIME,
		[CardRequestId] INT,
		[HasBeenActive] INT DEFAULT 0
	)

	DECLARE @tmpFinalResult TABLE(
		 [ClientName] VARCHAR(250)
		,[AccountNumber] VARCHAR(MAX)
		,[CreateDate] DATETIME
		,[Active] BIT
		,[TotalCantCards] INT -- Total Administrativas
		,[CostAdmin] DECIMAL(16,2)
		,[CurrentCantCards] INT --Emitidas rango actual
		,[CostEmision] DECIMAL(16,2)
		,[StatusName] VARCHAR(100)
		,[CantCloseCards] INT --Tarjetas Cerradas
		,[CantBlockCards] INT --Tarjetas Bloqueadas
		,[CantActiveCards] INT --Tarjetas Activas
		,[CurrencySymbol] VARCHAR(50)
		,[CantCardReposition] INT -- Tarjetas Repuestas
		,[AmountCardReposition] DECIMAL(16,2) -- Monto a pagar por la reposicion
		,[LegalDocument] VARCHAR(50)
		,[SAPCode] VARCHAR(50)
	)

	DECLARE @CreditCards TABLE(
		CreditCardId INT
	)

	DECLARE @Customers TABLE (
		CustomerId INT INDEX IDX CLUSTERED
	)

	-- VALIDAMOS FORZOSAMENTE QUE SI VIENE EL DATE MES Y AÑO ENTONCES LAS VARIABLES STAR_DATE Y END_DATE 
	-- DEBEN ESTAR NULAS PARA NO VALIDAR 2 VECES EL PROCESO 
	IF (@pYear IS NOT NULL AND @pMonth IS NOT NULL)
	BEGIN
		 SET @pStartDate = NULL
		 SET @pEndDate = NULL
	END 

	--IF @pEndDate IS NOT NULL -- FIXED END DATE RANGE
	--BEGIN
	--	SET @pEndDate =  @pEndDate + '23:59:59' 
	--END 

	-- FECHAS PARA MANEJAR EL HISTORICO DE TARJETAS ADMINISTRADAS
	DECLARE @DateByPartsIni DATETIME
	DECLARE @DateByPartsEnd DATETIME

	IF(@pYear IS NOT NULL AND @pMonth IS NOT NULL)
	BEGIN
		SET @DateByPartsIni =
			CAST(
				CAST(@pYear AS VARCHAR(4)) +
				RIGHT('0' + CAST(@pMonth AS VARCHAR(2)), 2) +
				RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
			AS DATETIME)

		SET @DateByPartsEnd = DATEADD(s,-1, DATEADD(mm, DATEDIFF(m,0,@DateByPartsIni)+1,0))
	END 


	IF @pCustomerId IS NULL SET @pCustomerId = -1

	IF @pPartnerId IS NULL
		SELECT @pPartnerId = [PartnerId]
		FROM [General].[CustomersByPartner]
		WHERE [CustomerId] = @pCustomerId 

	-- GET DIFFERENCE UTC HOUR - gsolano
	SELECT TOP 1 @lTimeZoneParameter = [TimeZone] 
	FROM [General].[Countries] co
	INNER JOIN [General].[Customers] cu
		ON co.[CountryId] = cu.[CountryId] 
	WHERE cu.[CustomerId]  IN (SELECT TOP 1 CustomerId FROM [General].[CustomersByPartner] WHERE PartnerId = @pPartnerId)
			   

	-- IF CustomerId is -1 then get all customers by PartnerId	
	IF @pCustomerId = -1 BEGIN

		INSERT INTO @Customers
		SELECT c.CustomerId
		FROM [General].[Customers] c
		INNER JOIN [General].[CustomersByPartner] cp 
			ON cp.[CustomerId] = c.[CustomerId]
		WHERE (c.IsDeleted IS NULL OR c.IsDeleted = 0)
			  AND c.[IsActive] = 1
			  AND (@pPartnerId IS NULL OR cp.[PartnerId] = @pPartnerId)

		SELECT @CurrentCustomerId = MIN(CustomerId)
		FROM @Customers
		WHERE [CustomerId] > @CurrentCustomerId
	END
	ELSE BEGIN
		SET @CurrentCustomerId = @pCustomerId
	END

	-- Loop through each customer record 
	WHILE(@CurrentCustomerId IS NOT NULL)
	BEGIN
						
			-- CLEAN TABLE
			DELETE FROM @tmpCards
			DELETE FROM @tmpCardsHistoric
			DELETE FROM @tmpCardsFixCloseCards

			INSERT INTO @tmpCards
			SELECT DISTINCT
				a.[CreditCardId],
				a.[CreditCardNumber],
				c.[Name] [ApplicantName],
				(
					SELECT top 1 s.Name FROM [Control].[CreditCardHx] ccx
					INNER JOIN [General].[Status] s ON s.StatusId = ccx.StatusId
					WHERE ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate])) = @pMonth
								AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate])) = @pYear) 
							OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate]) BETWEEN @pStartDate AND @pEndDate))
							AND ccx.[CreditCardId] = a.[CreditCardId] 
					ORDER BY ccx.CreditCardHxId DESC
				) [StatusName],
				DATEADD(HOUR, @lTimeZoneParameter, a.[InsertDate]) [RequestDate],
				(
					SELECT top 1 DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate]) FROM [Control].[CreditCardHx] ccx
					WHERE ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate])) = @pMonth
								AND DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate])) = @pYear) 
							OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate]) BETWEEN @pStartDate AND @pEndDate))
							AND ccx.[CreditCardId] = a.[CreditCardId] 
					ORDER BY ccx.CreditCardHxId DESC
				) [ModifyDate],
				e.[InsertDate] [DeliveryDate],
				b.[CardRequestId] [RequestNumber],
				(SELECT COUNT(1) FROM [Control].[CreditCardHx] ccx 
					WHERE ccx.[CreditCardId] = a.[CreditCardId] 
						  AND ccx.[StatusId] = 7) [HasBeenActive] -- PARA CONTROL DE TARJETAS CERRADAS QUE FUERON ACTIVAS ALGUNA VEZ
			FROM 
				[Control].[CreditCard] a
				INNER JOIN [Control].[CardRequest] b 
					ON a.[CardRequestId] = b.[CardRequestId]
				INNER JOIN [General].[Users] c    
					ON b.[InsertUserId] = c.[UserId]
				LEFT JOIN [Control].[CreditCardHx] e 
					ON a.[CreditCardId] = e.[CreditCardId] AND e.[StatusId] = 5
				INNER JOIN [General].[Customers] f   
					ON a.[CustomerId] = f.[CustomerId]				
			WHERE 
				a.[CustomerId] = @CurrentCustomerId 
				AND (@pCountryId IS NULL OR f.[CountryId] = @pCountryId)		
			
			
			INSERT INTO @tmpCardsHistoric
			SELECT DISTINCT
				tc.[CreditCardId],
				tc.[CreditCardNumber],
				tc.[ApplicantName],
				(
					SELECT top 1 s.Name FROM [Control].[CreditCardHx] ccx
					INNER JOIN [General].[Status] s ON s.StatusId = ccx.StatusId
					WHERE ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate]) <= @DateByPartsEnd) 
							OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate]) <= @pEndDate))
							AND ccx.[CreditCardId] = tc.[CreditCardId] 
					ORDER BY ccx.CreditCardHxId DESC
				) [StatusName],
				tc.[RequestDate],
				(
					SELECT top 1 DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate]) FROM [Control].[CreditCardHx] ccx
					WHERE ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
								AND DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate]) <= @DateByPartsEnd) 
							OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
								AND DATEADD(HOUR, @lTimeZoneParameter, ccx.[ModifyDate]) <= @pEndDate))
							AND ccx.[CreditCardId] = tc.[CreditCardId] 
					ORDER BY ccx.CreditCardHxId DESC
				) [ModifyDate],
				tc.[DeliveryDate],
				tc.[CardRequestId],
				tc.[HasBeenActive]
			FROM @tmpCards tc
			WHERE ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND tc.[RequestDate] < @DateByPartsEnd) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND tc.[RequestDate] < @pEndDate)
				) 

			SELECT @CantCardAdmin = COUNT(1) FROM @tmpCardsHistoric 
			WHERE [StatusName]  IN ('Activa','Bloqueada') 

			-- GET ALL CLOSE CARDS FOR CUSTOMER
			SELECT @CantCloseCards = COUNT(1) FROM @tmpCards
			WHERE [StatusName] = 'Cerrada' 
					AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
							AND DATEPART(m,[ModifyDate]) = @pMonth
							AND DATEPART(yyyy,[ModifyDate]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
							AND [ModifyDate] <= @pEndDate)
						)					

			-- GET ALL BLOCK CARDS FOR CUSTOMER
			SELECT @CantBlockCards = COUNT(1) FROM @tmpCards
			WHERE [StatusName] = 'Bloqueada' 
					AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
							AND DATEPART(m,[ModifyDate]) = @pMonth
							AND DATEPART(yyyy,[ModifyDate]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
							AND [ModifyDate] BETWEEN @pStartDate AND @pEndDate)
					)					

			-- GET ALL ACTIVE CARDS FOR CUSTOMER
			SELECT @CantActiveCards = COUNT(1) FROM @tmpCards
			WHERE [StatusName] = 'Activa' 
					AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
							AND DATEPART(m,[ModifyDate]) = @pMonth
							AND DATEPART(yyyy,[ModifyDate]) = @pYear) OR
							(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
							AND [ModifyDate] BETWEEN @pStartDate AND @pEndDate)
					)

			-- GET CARDS FOR DATE RANGE 
			SELECT @CantCardEmision = COUNT(1) FROM @tmpCards
			WHERE ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
					AND DATEPART(m,[RequestDate]) = @pMonth
					AND DATEPART(yyyy,[RequestDate]) = @pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
					AND [RequestDate] BETWEEN @pStartDate AND @pEndDate))
			
			INSERT INTO @CreditCards		
			SELECT CreditCardId FROM @tmpCards
			WHERE ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
					AND DATEPART(m,[RequestDate]) = @pMonth
					AND DATEPART(yyyy,[RequestDate]) = @pYear) OR
					(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
					AND [RequestDate] BETWEEN @pStartDate AND @pEndDate))

			SELECT @CreditCardId = MIN(CreditCardId) FROM @CreditCards

			WHILE @CreditCardId IS NOT NULL
			BEGIN
				EXEC [General].[GetCardReposition] @CreditCardId, @Accum OUTPUT

				SET @CantCardReposition = @CantCardReposition + @Accum

				DELETE FROM @CreditCards WHERE CreditCardId = @CreditCardId

				SELECT @CreditCardId = MIN(CreditCardId) FROM @CreditCards
				SET @Accum = 0
			END

			-- TARJETAS MODIFICADAS - SI EL FILTRO ES MENOR A LA FECHA ACTUAL - SUMAMOS LAS TARJETAS QUE FUERON ACTIVAS / BLOQUEADAS Y QUE PARA LA FECHA ACTUAL SE CERRARON (Y NO ENTRARON EN LOS RANGOS DE CONSULTA)
			IF(@pEndDate < DATEADD(HOUR, @lTimeZoneParameter, GETDATE()) 
					OR (@pYear IS NOT NULL AND @pYear < DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, GETDATE())))
 					OR (@pMonth IS NOT NULL AND @pYear IS NOT NULL AND @pYear = DATEPART(yyyy,DATEADD(HOUR, @lTimeZoneParameter, GETDATE())) AND @pMonth < DATEPART(m,DATEADD(HOUR, @lTimeZoneParameter, GETDATE())) )
			   )
			BEGIN
				DECLARE @FIXCantCloseCards INT = 0

				INSERT INTO @tmpCardsFixCloseCards
				SELECT * FROM @tmpCards 
				WHERE [StatusName]  = 'Cerrada'
						AND ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,[RequestDate]) < @pMonth
						AND DATEPART(yyyy,[RequestDate]) = @pYear) OR
						(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND [RequestDate] < @pStartDate))

				SELECT @FIXCantCloseCards = COUNT(1) FROM @tmpCardsFixCloseCards 
				WHERE  ((@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,[ModifyDate]) > @pMonth
						AND DATEPART(yyyy,[ModifyDate]) = @pYear) OR
						(@pStartDate IS NOT NULL AND @pEndDate IS NOT NULL
						AND [ModifyDate] > @pEndDate))
						AND HasBeenActive > 0


				IF(@pYear IS NOT NULL AND @pMonth IS NOT NULL)
				BEGIN
					-- Clean temp
					DELETE FROM @tmpCardsFixCloseCards

					INSERT INTO @tmpCardsFixCloseCards
					SELECT * FROM @tmpCards 
					WHERE [StatusName] = 'Cerrada' 					
							AND DATEPART(yyyy,[RequestDate]) < @pYear

					SELECT @FIXCantCloseCards += COUNT(1) FROM @tmpCardsFixCloseCards
					WHERE (@pYear IS NOT NULL AND @pMonth IS NOT NULL 
						AND DATEPART(m,[ModifyDate]) > @pMonth
						AND DATEPART(yyyy,[ModifyDate]) = @pYear)
						AND HasBeenActive > 0
				END 			

				--SELECT @FIXCantCloseCards, @pStartDate, @pMonth, @pYear
				IF(@FIXCantCloseCards > 0)
				BEGIN
					-- SUMAMOS LAS TARJETAS QUE FUERON ACTIVAS / BLOQUEADAS Y QUE PARA LA FECHA ACTUAL SE CERRARON (Y NO ENTRARON EN LOS RANGOS DE CONSULTA)
					SET @CantCardAdmin = @CantCardAdmin + @FIXCantCloseCards
				END

			END 


			-- INSERT THE FINAL SELECT FOR THIS CLIENT
			INSERT INTO @tmpFinalResult
			SELECT 
				c.[Name] AS [ClientName]
				,(SELECT TOP 1 [CreditCardNumber] 
				  FROM [General].[CustomerCreditCards] 
				  WHERE [CustomerId] = @CurrentCustomerId AND [InternationalCard] = 0) [AccountNumber]
				,c.[InsertDate] AS [CreateDate]
				,c.[IsActive] AS [Active]
				,@CantCardAdmin AS [TotalCantCards] -- Total Administrativas
				,ISNULL(c.[CostCardAdmin], 0.00) AS [CostAdmin]
				,@CantCardEmision - ISNULL(@CantCardReposition, 0) AS [CurrentCantCards] --Emitidas rango actual
				,ISNULL(c.[CostCardEmision], 0.00)AS [CostEmision]
				,IIF(c.[CreditCardType] = 'C', 'Crédito', 'Débito') AS [StatusName]
				,@CantCloseCards AS [CantCloseCards] --Tarjetas Cerradas
				,@CantBlockCards AS [CantBlockCards] --Tarjetas Bloqueadas
				,@CantActiveCards AS [CantActiveCards] --Tarjetas Activas
				,crr.[Symbol] AS [CurrencySymbol]
				,@CantCardReposition AS [CantCardReposition]
				,(@CantCardReposition * CostCardReposition) AS [AmountCardReposition]
				,ca.[LegalDocument]
				,c.[SAPCode] 
			FROM [General].[Customers] c
			INNER JOIN [Control].[Currencies] crr  
				ON c.[CurrencyId] = crr.[CurrencyId]
			LEFT JOIN [General].[CustomerAdditionalInformation] ca
				ON ca.[CustomerId] = c.[CustomerId]
			WHERE c.CustomerId = @CurrentCustomerId AND (@pCountryId IS NULL OR c.CountryId = @pCountryId)

			SET @CantCardReposition = 0
		
		IF @pCustomerId = -1 BEGIN
		--- NEXT CUSTOMER
			SELECT @CurrentCustomerId = MIN(CustomerId)
			FROM @Customers
			WHERE [CustomerId] > @CurrentCustomerId
		END
		ELSE BEGIN
			SET @CurrentCustomerId = NULL
		END
	END -- END WHILE

	-- RETURN RESULTS
	SELECT DISTINCT * FROM @tmpFinalResult
	
    SET NOCOUNT OFF
END