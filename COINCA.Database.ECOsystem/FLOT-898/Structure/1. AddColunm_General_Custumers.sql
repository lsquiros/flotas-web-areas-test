-- =================================================
-- Author:		Juan Carlos Santamaria
-- Create date: 11-11-2019
-- Description:	Add Field SAPCODE
-- =================================================
IF EXISTS(
		SELECT *
		  FROM sys.objects a
	INNER JOIN sys.tables b on a.object_id = b.object_id
	INNER JOIN sys.columns c on b.object_id = c.object_id
		 WHERE a.name LIKE 'Customers' and c.name = 'SapCode')
BEGIN
	ALTER TABLE [General].[Customers] drop COLUMN  [SapCode]
END
GO

ALTER TABLE [General].[Customers] ADD	[SapCode] VARCHAR(50) 

GO