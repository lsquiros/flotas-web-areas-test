﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="SeeRoute.aspx.cs" Inherits="ECO_Maps.SeeRoute1" EnableEventValidation="false"%>
<%@ Register TagPrefix="aspmap" Namespace="AspMap.Web" Assembly="AspMapNET" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
          <%: Styles.Render("~/bundles/css") %>       
</head>
<body>
    <form id="SeeRouteForm" runat="server">
        <asp:ScriptManager ID="ScriptManager2" runat="server">
            <Scripts></Scripts>
        </asp:ScriptManager>
    <div id="body">
        <section class="content-wrapper main-content clear-fix">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="layerMap" runat="server" Value="false" />
            <asp:HiddenField ID="loaded" runat="server" Value="false" />
            <asp:HiddenField ID="width" runat="server" Value="0" />
            <asp:HiddenField ID="height" runat="server" Value="0" />
            <div class="GeoFences-Content">
                <div class="div100">
                    <div class="ContentSearch">
                        <div class="div80 f-right">
                          <%--   <div class="LabelSearch">
                                <strong>
                                    <asp:Label ID="lblProvincia" class="control-label" runat="server" Text="Ver Trayecto: "></asp:Label>
                                </strong>
                            </div>
                           <div class="ControlSearch">
                                <asp:DropDownList AutoPostBack="true" name="ddlFilterRoute" id="ddlFilterRoute" runat="server" class="select2-container form-control select2-container-active select2-dropdown-open" OnSelectedIndexChanged="ddlHour_SelectedIndexChanged">
                                    <asp:ListItem Value="0" Selected="True">Sin Trayecto</asp:ListItem>
                                    <asp:ListItem Value="1">Ultima Hora</asp:ListItem>
                                    <asp:ListItem Value="2">Ultimo Día</asp:ListItem>
                                    <asp:ListItem Value="3">Ultima Semana</asp:ListItem>
                                    <asp:ListItem Value="4">Ultimas 2 Semanas</asp:ListItem>
                                    <asp:ListItem Value="5">Ultimo Mes</asp:ListItem>
                                </asp:DropDownList>
                            </div>--%>
                        </div>
                    </div>
                </div>
                <div class="div100">
                    <div class="ContentMap" >
                        <asp:Panel ID="pnlCoinca" runat="server" Visible="true">
                            <aspmap:Map id="mapCoinca" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnRefreshAnimationLayer="mapCoinca_RefreshAnimationLayer" ></aspmap:Map>
                        </asp:Panel>
                        <asp:Panel ID="pnlGoogle" runat="server" Visible="false">
                            <aspmap:Map id="mapGoogle" Visible="false" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnRefreshAnimationLayer="mapGoogle_RefreshAnimationLayer"  ></aspmap:Map>
                            <asp:Panel ID="pnlSatellite" runat="server" CssClass="pnlSatellite">
                                <asp:CheckBox ID="chkSatellite" runat="server" Text="Vista Satelital" AutoPostBack="true" OnCheckedChanged="chkSatellite_CheckedChanged"/>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="mapMenuControls" runat="server" CssClass="mapMenu-controls" Visible="true">
                            <div class="LeftMenu" >
                                <center>
                                    <div id="mapMenu" class="btn-group-vertical" onclick="fncMenuRefresh();">
                                        <asp:ImageButton ID="GoogleMap" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Google.png" OnClientClick="changeMap('GoogleRoad')" ></asp:ImageButton>
                                        <asp:ImageButton ID="CoincaMap" Visible="false" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Coinca.png" OnClientClick="changeMap('Coinca')" ></asp:ImageButton>
                                    </div>
                                </center>
                            </div>
                        </asp:Panel>
                        <div class="mapMenu-controls">
                            <asp:LinkButton ID="btnHideMapMenu" runat="server" Text="Ocultar Menu" class="btn btn-primary btnHide-mapMenu-controls" title="Ocultar Menu" OnClick="btnHideMapMenu_Click" Visible="true">
                                <i class="glyphicon glyphicon-chevron-left"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnShowMapMenu" runat="server" Text="Mostrar Menu" class="btn btn-primary btnShow-mapMenu-controls" title="Mostrar Menu" OnClick="btnShowMapMenu_Click" Visible="false">
                                <i class="glyphicon glyphicon-chevron-right"></i>
                            </asp:LinkButton>
                        </div>
                        <asp:Label Visible="false" ID="lblLatitudPunto" runat="server"></asp:Label>
                        <asp:Label Visible="false" ID="lblLongitudPunto" runat="server"></asp:Label>
                        <asp:Label Visible="false" ID="lblTotalDistancia" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div style="visibility:hidden;">
                <asp:Button ID="btnResize" runat="server" Text="Button" OnClick="btnResize_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%: Scripts.Render("~/bundles/jquery") %>
    <script src="Scripts/jquery.ba-resize.min.js"></script>

    <script>

        $(function () {
            resize();
        });

        $(window).resize(function () {
            resize();
        });

        function resize() {
            var width = $(window).width() - 5;
            var height = $(window).height() - 30;
            $('#<% =width.ClientID %>').attr('value', width);
            $('#<% =height.ClientID %>').attr('value', height);
            $("#ContentSearch").width(width);
            $('#<%=btnResize.ClientID %>').click();
        }

    </script>

            </section>
    </div>
    </form>
</body>
</html>